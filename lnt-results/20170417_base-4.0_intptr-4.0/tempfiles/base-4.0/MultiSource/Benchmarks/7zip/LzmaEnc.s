	.text
	.file	"LzmaEnc.bc"
	.globl	LzmaEncProps_Init
	.p2align	4, 0x90
	.type	LzmaEncProps_Init,@function
LzmaEncProps_Init:                      # @LzmaEncProps_Init
	.cfi_startproc
# BB#0:
	movl	$0, 36(%rdi)
	movq	$5, (%rdi)
	movl	$-1, 44(%rdi)
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 20(%rdi)
	movdqu	%xmm0, 8(%rdi)
	movl	$0, 40(%rdi)
	retq
.Lfunc_end0:
	.size	LzmaEncProps_Init, .Lfunc_end0-LzmaEncProps_Init
	.cfi_endproc

	.globl	LzmaEncProps_Normalize
	.p2align	4, 0x90
	.type	LzmaEncProps_Normalize,@function
LzmaEncProps_Normalize:                 # @LzmaEncProps_Normalize
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	movl	$5, %eax
	cmovnsl	%ecx, %eax
	movl	%eax, (%rdi)
	cmpl	$0, 4(%rdi)
	jne	.LBB1_5
# BB#1:
	cmpl	$5, %eax
	jg	.LBB1_3
# BB#2:
	leal	14(%rax,%rax), %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	jmp	.LBB1_4
.LBB1_3:
	cmpl	$6, %eax
	movl	$33554432, %ecx         # imm = 0x2000000
	movl	$67108864, %edx         # imm = 0x4000000
	cmovel	%ecx, %edx
.LBB1_4:
	movl	%edx, 4(%rdi)
.LBB1_5:
	cmpl	$0, 8(%rdi)
	jns	.LBB1_7
# BB#6:
	movl	$3, 8(%rdi)
.LBB1_7:
	cmpl	$0, 12(%rdi)
	jns	.LBB1_9
# BB#8:
	movl	$0, 12(%rdi)
.LBB1_9:
	cmpl	$0, 16(%rdi)
	jns	.LBB1_11
# BB#10:
	movl	$2, 16(%rdi)
.LBB1_11:
	movl	20(%rdi), %edx
	testl	%edx, %edx
	jns	.LBB1_13
# BB#12:
	xorl	%edx, %edx
	cmpl	$4, %eax
	setg	%dl
	movl	%edx, 20(%rdi)
.LBB1_13:
	movl	24(%rdi), %esi
	testl	%esi, %esi
	jns	.LBB1_15
# BB#14:
	cmpl	$7, %eax
	movl	$32, %eax
	movl	$64, %esi
	cmovll	%eax, %esi
	movl	%esi, 24(%rdi)
.LBB1_15:
	movl	28(%rdi), %eax
	testl	%eax, %eax
	jns	.LBB1_17
# BB#16:
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	movl	%eax, 28(%rdi)
.LBB1_17:
	cmpl	$0, 32(%rdi)
	jns	.LBB1_19
# BB#18:
	movl	$4, 32(%rdi)
.LBB1_19:
	cmpl	$0, 36(%rdi)
	jne	.LBB1_21
# BB#20:
	shrl	%esi
	addl	$16, %esi
	testl	%eax, %eax
	sete	%cl
	shrl	%cl, %esi
	movl	%esi, 36(%rdi)
.LBB1_21:
	cmpl	$0, 44(%rdi)
	jns	.LBB1_23
# BB#22:
	cmpl	$1, %edx
	movl	$1, %ecx
	movl	$1, %edx
	sbbl	$-1, %edx
	testl	%eax, %eax
	cmovel	%ecx, %edx
	movl	%edx, 44(%rdi)
.LBB1_23:
	retq
.Lfunc_end1:
	.size	LzmaEncProps_Normalize, .Lfunc_end1-LzmaEncProps_Normalize
	.cfi_endproc

	.globl	LzmaEncProps_GetDictSize
	.p2align	4, 0x90
	.type	LzmaEncProps_GetDictSize,@function
LzmaEncProps_GetDictSize:               # @LzmaEncProps_GetDictSize
	.cfi_startproc
# BB#0:
	movl	(%rdi), %edx
	movl	4(%rdi), %eax
	testl	%edx, %edx
	movl	$5, %ecx
	cmovnsl	%edx, %ecx
	testl	%eax, %eax
	jne	.LBB2_4
# BB#1:
	cmpl	$5, %ecx
	jg	.LBB2_3
# BB#2:
	leal	14(%rcx,%rcx), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	retq
.LBB2_3:
	cmpl	$6, %ecx
	movl	$33554432, %ecx         # imm = 0x2000000
	movl	$67108864, %eax         # imm = 0x4000000
	cmovel	%ecx, %eax
.LBB2_4:                                # %LzmaEncProps_Normalize.exit
	retq
.Lfunc_end2:
	.size	LzmaEncProps_GetDictSize, .Lfunc_end2-LzmaEncProps_GetDictSize
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
.LCPI3_1:
	.quad	8                       # 0x8
	.quad	8                       # 0x8
	.text
	.globl	LzmaEnc_FastPosInit
	.p2align	4, 0x90
	.type	LzmaEnc_FastPosInit,@function
LzmaEnc_FastPosInit:                    # @LzmaEnc_FastPosInit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movw	$256, (%r14)            # imm = 0x100
	movl	$2, %r12d
	movl	$2, %ebp
	.p2align	4, 0x90
.LBB3_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_8 Depth 2
                                        #     Child Loop BB3_11 Depth 2
                                        #     Child Loop BB3_13 Depth 2
	movl	%r12d, %ecx
	shrl	%ecx
	decl	%ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	movslq	%ebp, %rbp
	leaq	(%r14,%rbp), %rdi
	cmpl	$1, %ebx
	movl	$1, %r15d
	cmoval	%ebx, %r15d
	leal	-1(%r15), %edx
	incq	%rdx
	movl	%r12d, %esi
	callq	memset
	cmpl	$4, %r15d
	jae	.LBB3_3
# BB#2:                                 #   in Loop: Header=BB3_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_13
	.p2align	4, 0x90
.LBB3_3:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	%r15d, %eax
	andl	$-4, %eax
	je	.LBB3_4
# BB#5:                                 # %vector.ph
                                        #   in Loop: Header=BB3_1 Depth=1
	movd	%rbp, %xmm0
	leal	-4(%rax), %ecx
	movl	%ecx, %esi
	shrl	$2, %esi
	incl	%esi
	andl	$7, %esi
	je	.LBB3_6
# BB#7:                                 # %vector.body.prol.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	negl	%esi
	pxor	%xmm1, %xmm1
	xorl	%edx, %edx
	movdqa	.LCPI3_1(%rip), %xmm2   # xmm2 = [8,8]
	movdqa	.LCPI3_0(%rip), %xmm3   # xmm3 = [1,1]
	.p2align	4, 0x90
.LBB3_8:                                # %vector.body.prol
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	paddq	%xmm3, %xmm0
	paddq	%xmm3, %xmm1
	addl	$4, %edx
	incl	%esi
	jne	.LBB3_8
	jmp	.LBB3_9
.LBB3_4:                                #   in Loop: Header=BB3_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_13
.LBB3_6:                                #   in Loop: Header=BB3_1 Depth=1
	pxor	%xmm1, %xmm1
	xorl	%edx, %edx
	movdqa	.LCPI3_1(%rip), %xmm2   # xmm2 = [8,8]
.LBB3_9:                                # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpl	$28, %ecx
	jb	.LBB3_12
# BB#10:                                # %vector.ph.new
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	%eax, %ecx
	subl	%edx, %ecx
	.p2align	4, 0x90
.LBB3_11:                               # %vector.body
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	paddq	%xmm2, %xmm0
	paddq	%xmm2, %xmm1
	addl	$-32, %ecx
	jne	.LBB3_11
.LBB3_12:                               # %middle.block
                                        #   in Loop: Header=BB3_1 Depth=1
	paddq	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm0, %xmm1
	movd	%xmm1, %rbp
	cmpl	%eax, %r15d
	je	.LBB3_14
	.p2align	4, 0x90
.LBB3_13:                               # %scalar.ph
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%eax
	incq	%rbp
	cmpl	%ebx, %eax
	jb	.LBB3_13
.LBB3_14:                               # %.loopexit
                                        #   in Loop: Header=BB3_1 Depth=1
	incl	%r12d
	cmpl	$26, %r12d
	jne	.LBB3_1
# BB#15:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	LzmaEnc_FastPosInit, .Lfunc_end3-LzmaEnc_FastPosInit
	.cfi_endproc

	.globl	LzmaEnc_SaveState
	.p2align	4, 0x90
	.type	LzmaEnc_SaveState,@function
LzmaEnc_SaveState:                      # @LzmaEnc_SaveState
	.cfi_startproc
# BB#0:                                 # %.preheader46
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	254004(%rbx), %rdi
	leaq	215220(%rbx), %rsi
	movl	$18504, %edx            # imm = 0x4848
	callq	memcpy
	leaq	272508(%rbx), %rdi
	leaq	233724(%rbx), %rsi
	movl	$18504, %edx            # imm = 0x4848
	callq	memcpy
	movl	210408(%rbx), %eax
	movl	%eax, 291028(%rbx)
	movups	213584(%rbx), %xmm0
	movups	213600(%rbx), %xmm1
	movups	%xmm1, 252384(%rbx)
	movups	%xmm0, 252368(%rbx)
	movups	214064(%rbx), %xmm0
	movups	214080(%rbx), %xmm1
	movups	%xmm1, 252864(%rbx)
	movups	%xmm0, 252848(%rbx)
	movups	213616(%rbx), %xmm0
	movups	213632(%rbx), %xmm1
	movups	%xmm1, 252416(%rbx)
	movups	%xmm0, 252400(%rbx)
	movups	214096(%rbx), %xmm0
	movups	214112(%rbx), %xmm1
	movups	%xmm1, 252896(%rbx)
	movups	%xmm0, 252880(%rbx)
	movups	213648(%rbx), %xmm0
	movups	213664(%rbx), %xmm1
	movups	%xmm1, 252448(%rbx)
	movups	%xmm0, 252432(%rbx)
	movups	214128(%rbx), %xmm0
	movups	214144(%rbx), %xmm1
	movups	%xmm1, 252928(%rbx)
	movups	%xmm0, 252912(%rbx)
	movups	213680(%rbx), %xmm0
	movups	213696(%rbx), %xmm1
	movups	%xmm1, 252480(%rbx)
	movups	%xmm0, 252464(%rbx)
	movups	214160(%rbx), %xmm0
	movups	214176(%rbx), %xmm1
	movups	%xmm1, 252960(%rbx)
	movups	%xmm0, 252944(%rbx)
	movups	213712(%rbx), %xmm0
	movups	213728(%rbx), %xmm1
	movups	%xmm1, 252512(%rbx)
	movups	%xmm0, 252496(%rbx)
	movups	214192(%rbx), %xmm0
	movups	214208(%rbx), %xmm1
	movups	%xmm1, 252992(%rbx)
	movups	%xmm0, 252976(%rbx)
	movups	213744(%rbx), %xmm0
	movups	213760(%rbx), %xmm1
	movups	%xmm1, 252544(%rbx)
	movups	%xmm0, 252528(%rbx)
	movups	214224(%rbx), %xmm0
	movups	214240(%rbx), %xmm1
	movups	%xmm1, 253024(%rbx)
	movups	%xmm0, 253008(%rbx)
	movups	213776(%rbx), %xmm0
	movups	213792(%rbx), %xmm1
	movups	%xmm1, 252576(%rbx)
	movups	%xmm0, 252560(%rbx)
	movups	214256(%rbx), %xmm0
	movups	214272(%rbx), %xmm1
	movups	%xmm1, 253056(%rbx)
	movups	%xmm0, 253040(%rbx)
	movups	213808(%rbx), %xmm0
	movups	213824(%rbx), %xmm1
	movups	%xmm1, 252608(%rbx)
	movups	%xmm0, 252592(%rbx)
	movups	214288(%rbx), %xmm0
	movups	214304(%rbx), %xmm1
	movups	%xmm1, 253088(%rbx)
	movups	%xmm0, 253072(%rbx)
	movups	213840(%rbx), %xmm0
	movups	213856(%rbx), %xmm1
	movups	%xmm1, 252640(%rbx)
	movups	%xmm0, 252624(%rbx)
	movups	214320(%rbx), %xmm0
	movups	214336(%rbx), %xmm1
	movups	%xmm1, 253120(%rbx)
	movups	%xmm0, 253104(%rbx)
	movups	213872(%rbx), %xmm0
	movups	213888(%rbx), %xmm1
	movups	%xmm1, 252672(%rbx)
	movups	%xmm0, 252656(%rbx)
	movups	214352(%rbx), %xmm0
	movups	214368(%rbx), %xmm1
	movups	%xmm1, 253152(%rbx)
	movups	%xmm0, 253136(%rbx)
	movups	213904(%rbx), %xmm0
	movups	213920(%rbx), %xmm1
	movups	%xmm1, 252704(%rbx)
	movups	%xmm0, 252688(%rbx)
	movups	214384(%rbx), %xmm0
	movups	214400(%rbx), %xmm1
	movups	%xmm1, 253184(%rbx)
	movups	%xmm0, 253168(%rbx)
	movups	213936(%rbx), %xmm0
	movups	213952(%rbx), %xmm1
	movups	%xmm1, 252736(%rbx)
	movups	%xmm0, 252720(%rbx)
	movups	214416(%rbx), %xmm0
	movups	214432(%rbx), %xmm1
	movups	%xmm1, 253216(%rbx)
	movups	%xmm0, 253200(%rbx)
	movups	214560(%rbx), %xmm0
	movups	%xmm0, 253344(%rbx)
	movups	214544(%rbx), %xmm0
	movups	%xmm0, 253328(%rbx)
	movups	214528(%rbx), %xmm0
	movups	%xmm0, 253312(%rbx)
	movups	214512(%rbx), %xmm0
	movups	%xmm0, 253296(%rbx)
	movups	214448(%rbx), %xmm0
	movups	214464(%rbx), %xmm1
	movups	214480(%rbx), %xmm2
	movups	214496(%rbx), %xmm3
	movups	%xmm3, 253280(%rbx)
	movups	%xmm2, 253264(%rbx)
	movups	%xmm1, 253248(%rbx)
	movups	%xmm0, 253232(%rbx)
	movups	214688(%rbx), %xmm0
	movups	%xmm0, 253472(%rbx)
	movups	214672(%rbx), %xmm0
	movups	%xmm0, 253456(%rbx)
	movups	214656(%rbx), %xmm0
	movups	%xmm0, 253440(%rbx)
	movups	214640(%rbx), %xmm0
	movups	%xmm0, 253424(%rbx)
	movups	214576(%rbx), %xmm0
	movups	214592(%rbx), %xmm1
	movups	214608(%rbx), %xmm2
	movups	214624(%rbx), %xmm3
	movups	%xmm3, 253408(%rbx)
	movups	%xmm2, 253392(%rbx)
	movups	%xmm1, 253376(%rbx)
	movups	%xmm0, 253360(%rbx)
	movups	214816(%rbx), %xmm0
	movups	%xmm0, 253600(%rbx)
	movups	214800(%rbx), %xmm0
	movups	%xmm0, 253584(%rbx)
	movups	214784(%rbx), %xmm0
	movups	%xmm0, 253568(%rbx)
	movups	214768(%rbx), %xmm0
	movups	%xmm0, 253552(%rbx)
	movups	214704(%rbx), %xmm0
	movups	214720(%rbx), %xmm1
	movups	214736(%rbx), %xmm2
	movups	214752(%rbx), %xmm3
	movups	%xmm3, 253536(%rbx)
	movups	%xmm2, 253520(%rbx)
	movups	%xmm1, 253504(%rbx)
	movups	%xmm0, 253488(%rbx)
	movups	214944(%rbx), %xmm0
	movups	%xmm0, 253728(%rbx)
	movups	214928(%rbx), %xmm0
	movups	%xmm0, 253712(%rbx)
	movups	214912(%rbx), %xmm0
	movups	%xmm0, 253696(%rbx)
	movups	214896(%rbx), %xmm0
	movups	%xmm0, 253680(%rbx)
	movups	214832(%rbx), %xmm0
	movups	214848(%rbx), %xmm1
	movups	214864(%rbx), %xmm2
	movups	214880(%rbx), %xmm3
	movups	%xmm3, 253664(%rbx)
	movups	%xmm2, 253648(%rbx)
	movups	%xmm1, 253632(%rbx)
	movups	%xmm0, 253616(%rbx)
	movq	213984(%rbx), %rax
	movq	%rax, 252768(%rbx)
	movups	213968(%rbx), %xmm0
	movups	%xmm0, 252752(%rbx)
	movq	214008(%rbx), %rax
	movq	%rax, 252792(%rbx)
	movups	213992(%rbx), %xmm0
	movups	%xmm0, 252776(%rbx)
	movq	214032(%rbx), %rax
	movq	%rax, 252816(%rbx)
	movups	214016(%rbx), %xmm0
	movups	%xmm0, 252800(%rbx)
	movq	214056(%rbx), %rax
	movq	%rax, 252840(%rbx)
	movups	214040(%rbx), %xmm0
	movups	%xmm0, 252824(%rbx)
	leaq	253744(%rbx), %rdi
	leaq	214960(%rbx), %rsi
	movl	$228, %edx
	callq	memcpy
	movups	215188(%rbx), %xmm0
	movups	215204(%rbx), %xmm1
	movups	%xmm1, 253988(%rbx)
	movups	%xmm0, 253972(%rbx)
	movups	210392(%rbx), %xmm0
	movups	%xmm0, 291012(%rbx)
	movq	252360(%rbx), %rdi
	movq	213576(%rbx), %rsi
	movb	252228(%rbx), %cl
	movl	$768, %eax              # imm = 0x300
	shll	%cl, %eax
	movslq	%eax, %rdx
	addq	%rdx, %rdx
	popq	%rbx
	jmp	memcpy                  # TAILCALL
.Lfunc_end4:
	.size	LzmaEnc_SaveState, .Lfunc_end4-LzmaEnc_SaveState
	.cfi_endproc

	.globl	LzmaEnc_RestoreState
	.p2align	4, 0x90
	.type	LzmaEnc_RestoreState,@function
LzmaEnc_RestoreState:                   # @LzmaEnc_RestoreState
	.cfi_startproc
# BB#0:                                 # %.preheader46
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	215220(%rbx), %rdi
	leaq	254004(%rbx), %rsi
	movl	$18504, %edx            # imm = 0x4848
	callq	memcpy
	leaq	233724(%rbx), %rdi
	leaq	272508(%rbx), %rsi
	movl	$18504, %edx            # imm = 0x4848
	callq	memcpy
	movl	291028(%rbx), %eax
	movl	%eax, 210408(%rbx)
	movups	252368(%rbx), %xmm0
	movups	252384(%rbx), %xmm1
	movups	%xmm1, 213600(%rbx)
	movups	%xmm0, 213584(%rbx)
	movups	252848(%rbx), %xmm0
	movups	252864(%rbx), %xmm1
	movups	%xmm1, 214080(%rbx)
	movups	%xmm0, 214064(%rbx)
	movups	252400(%rbx), %xmm0
	movups	252416(%rbx), %xmm1
	movups	%xmm1, 213632(%rbx)
	movups	%xmm0, 213616(%rbx)
	movups	252880(%rbx), %xmm0
	movups	252896(%rbx), %xmm1
	movups	%xmm1, 214112(%rbx)
	movups	%xmm0, 214096(%rbx)
	movups	252432(%rbx), %xmm0
	movups	252448(%rbx), %xmm1
	movups	%xmm1, 213664(%rbx)
	movups	%xmm0, 213648(%rbx)
	movups	252912(%rbx), %xmm0
	movups	252928(%rbx), %xmm1
	movups	%xmm1, 214144(%rbx)
	movups	%xmm0, 214128(%rbx)
	movups	252464(%rbx), %xmm0
	movups	252480(%rbx), %xmm1
	movups	%xmm1, 213696(%rbx)
	movups	%xmm0, 213680(%rbx)
	movups	252944(%rbx), %xmm0
	movups	252960(%rbx), %xmm1
	movups	%xmm1, 214176(%rbx)
	movups	%xmm0, 214160(%rbx)
	movups	252496(%rbx), %xmm0
	movups	252512(%rbx), %xmm1
	movups	%xmm1, 213728(%rbx)
	movups	%xmm0, 213712(%rbx)
	movups	252976(%rbx), %xmm0
	movups	252992(%rbx), %xmm1
	movups	%xmm1, 214208(%rbx)
	movups	%xmm0, 214192(%rbx)
	movups	252528(%rbx), %xmm0
	movups	252544(%rbx), %xmm1
	movups	%xmm1, 213760(%rbx)
	movups	%xmm0, 213744(%rbx)
	movups	253008(%rbx), %xmm0
	movups	253024(%rbx), %xmm1
	movups	%xmm1, 214240(%rbx)
	movups	%xmm0, 214224(%rbx)
	movups	252560(%rbx), %xmm0
	movups	252576(%rbx), %xmm1
	movups	%xmm1, 213792(%rbx)
	movups	%xmm0, 213776(%rbx)
	movups	253040(%rbx), %xmm0
	movups	253056(%rbx), %xmm1
	movups	%xmm1, 214272(%rbx)
	movups	%xmm0, 214256(%rbx)
	movups	252592(%rbx), %xmm0
	movups	252608(%rbx), %xmm1
	movups	%xmm1, 213824(%rbx)
	movups	%xmm0, 213808(%rbx)
	movups	253072(%rbx), %xmm0
	movups	253088(%rbx), %xmm1
	movups	%xmm1, 214304(%rbx)
	movups	%xmm0, 214288(%rbx)
	movups	252624(%rbx), %xmm0
	movups	252640(%rbx), %xmm1
	movups	%xmm1, 213856(%rbx)
	movups	%xmm0, 213840(%rbx)
	movups	253104(%rbx), %xmm0
	movups	253120(%rbx), %xmm1
	movups	%xmm1, 214336(%rbx)
	movups	%xmm0, 214320(%rbx)
	movups	252656(%rbx), %xmm0
	movups	252672(%rbx), %xmm1
	movups	%xmm1, 213888(%rbx)
	movups	%xmm0, 213872(%rbx)
	movups	253136(%rbx), %xmm0
	movups	253152(%rbx), %xmm1
	movups	%xmm1, 214368(%rbx)
	movups	%xmm0, 214352(%rbx)
	movups	252688(%rbx), %xmm0
	movups	252704(%rbx), %xmm1
	movups	%xmm1, 213920(%rbx)
	movups	%xmm0, 213904(%rbx)
	movups	253168(%rbx), %xmm0
	movups	253184(%rbx), %xmm1
	movups	%xmm1, 214400(%rbx)
	movups	%xmm0, 214384(%rbx)
	movups	252720(%rbx), %xmm0
	movups	252736(%rbx), %xmm1
	movups	%xmm1, 213952(%rbx)
	movups	%xmm0, 213936(%rbx)
	movups	253200(%rbx), %xmm0
	movups	253216(%rbx), %xmm1
	movups	%xmm1, 214432(%rbx)
	movups	%xmm0, 214416(%rbx)
	movups	253344(%rbx), %xmm0
	movups	%xmm0, 214560(%rbx)
	movups	253328(%rbx), %xmm0
	movups	%xmm0, 214544(%rbx)
	movups	253312(%rbx), %xmm0
	movups	%xmm0, 214528(%rbx)
	movups	253296(%rbx), %xmm0
	movups	%xmm0, 214512(%rbx)
	movups	253232(%rbx), %xmm0
	movups	253248(%rbx), %xmm1
	movups	253264(%rbx), %xmm2
	movups	253280(%rbx), %xmm3
	movups	%xmm3, 214496(%rbx)
	movups	%xmm2, 214480(%rbx)
	movups	%xmm1, 214464(%rbx)
	movups	%xmm0, 214448(%rbx)
	movups	253472(%rbx), %xmm0
	movups	%xmm0, 214688(%rbx)
	movups	253456(%rbx), %xmm0
	movups	%xmm0, 214672(%rbx)
	movups	253440(%rbx), %xmm0
	movups	%xmm0, 214656(%rbx)
	movups	253424(%rbx), %xmm0
	movups	%xmm0, 214640(%rbx)
	movups	253360(%rbx), %xmm0
	movups	253376(%rbx), %xmm1
	movups	253392(%rbx), %xmm2
	movups	253408(%rbx), %xmm3
	movups	%xmm3, 214624(%rbx)
	movups	%xmm2, 214608(%rbx)
	movups	%xmm1, 214592(%rbx)
	movups	%xmm0, 214576(%rbx)
	movups	253600(%rbx), %xmm0
	movups	%xmm0, 214816(%rbx)
	movups	253584(%rbx), %xmm0
	movups	%xmm0, 214800(%rbx)
	movups	253568(%rbx), %xmm0
	movups	%xmm0, 214784(%rbx)
	movups	253552(%rbx), %xmm0
	movups	%xmm0, 214768(%rbx)
	movups	253488(%rbx), %xmm0
	movups	253504(%rbx), %xmm1
	movups	253520(%rbx), %xmm2
	movups	253536(%rbx), %xmm3
	movups	%xmm3, 214752(%rbx)
	movups	%xmm2, 214736(%rbx)
	movups	%xmm1, 214720(%rbx)
	movups	%xmm0, 214704(%rbx)
	movups	253728(%rbx), %xmm0
	movups	%xmm0, 214944(%rbx)
	movups	253712(%rbx), %xmm0
	movups	%xmm0, 214928(%rbx)
	movups	253696(%rbx), %xmm0
	movups	%xmm0, 214912(%rbx)
	movups	253680(%rbx), %xmm0
	movups	%xmm0, 214896(%rbx)
	movups	253616(%rbx), %xmm0
	movups	253632(%rbx), %xmm1
	movups	253648(%rbx), %xmm2
	movups	253664(%rbx), %xmm3
	movups	%xmm3, 214880(%rbx)
	movups	%xmm2, 214864(%rbx)
	movups	%xmm1, 214848(%rbx)
	movups	%xmm0, 214832(%rbx)
	movq	252768(%rbx), %rax
	movq	%rax, 213984(%rbx)
	movups	252752(%rbx), %xmm0
	movups	%xmm0, 213968(%rbx)
	movq	252792(%rbx), %rax
	movq	%rax, 214008(%rbx)
	movups	252776(%rbx), %xmm0
	movups	%xmm0, 213992(%rbx)
	movq	252816(%rbx), %rax
	movq	%rax, 214032(%rbx)
	movups	252800(%rbx), %xmm0
	movups	%xmm0, 214016(%rbx)
	movq	252840(%rbx), %rax
	movq	%rax, 214056(%rbx)
	movups	252824(%rbx), %xmm0
	movups	%xmm0, 214040(%rbx)
	leaq	214960(%rbx), %rdi
	leaq	253744(%rbx), %rsi
	movl	$228, %edx
	callq	memcpy
	movups	253972(%rbx), %xmm0
	movups	253988(%rbx), %xmm1
	movups	%xmm1, 215204(%rbx)
	movups	%xmm0, 215188(%rbx)
	movups	291012(%rbx), %xmm0
	movups	%xmm0, 210392(%rbx)
	movq	213576(%rbx), %rdi
	movq	252360(%rbx), %rsi
	movb	252228(%rbx), %cl
	movl	$768, %eax              # imm = 0x300
	shll	%cl, %eax
	movslq	%eax, %rdx
	addq	%rdx, %rdx
	popq	%rbx
	jmp	memcpy                  # TAILCALL
.Lfunc_end5:
	.size	LzmaEnc_RestoreState, .Lfunc_end5-LzmaEnc_RestoreState
	.cfi_endproc

	.globl	LzmaEnc_SetProps
	.p2align	4, 0x90
	.type	LzmaEnc_SetProps,@function
LzmaEnc_SetProps:                       # @LzmaEnc_SetProps
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	movl	(%rsi), %ecx
	movl	4(%rsi), %ebp
	movl	8(%rsi), %eax
	movl	12(%rsi), %ebx
	movl	16(%rsi), %r14d
	movl	20(%rsi), %edi
	movl	24(%rsi), %r10d
	movl	28(%rsi), %r12d
	movl	32(%rsi), %r11d
	movl	36(%rsi), %r8d
	movl	40(%rsi), %edx
	movl	%edx, -16(%rsp)         # 4-byte Spill
	movl	44(%rsi), %edx
	movl	%edx, -12(%rsp)         # 4-byte Spill
	testl	%ecx, %ecx
	movl	$5, %esi
	cmovnsl	%ecx, %esi
	movl	%ebp, %ecx
	testl	%ecx, %ecx
	jne	.LBB6_5
# BB#1:
	cmpl	$5, %esi
	jg	.LBB6_3
# BB#2:
	leal	14(%rsi,%rsi), %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	jmp	.LBB6_4
.LBB6_3:
	cmpl	$6, %esi
	movl	$33554432, %ecx         # imm = 0x2000000
	movl	$67108864, %edx         # imm = 0x4000000
	cmovel	%ecx, %edx
.LBB6_4:
	movl	%edx, %ecx
.LBB6_5:
	testl	%eax, %eax
	movl	$3, %r15d
	cmovnsl	%eax, %r15d
	xorl	%r13d, %r13d
	testl	%ebx, %ebx
	cmovnsl	%ebx, %r13d
	testl	%r14d, %r14d
	movl	$2, %r9d
	cmovnsl	%r14d, %r9d
	xorl	%ebp, %ebp
	cmpl	$4, %esi
	setg	%bpl
	testl	%edi, %edi
	cmovnsl	%edi, %ebp
	cmpl	$7, %esi
	movl	$32, %eax
	movl	$64, %edx
	cmovll	%eax, %edx
	testl	%r10d, %r10d
	cmovnsl	%r10d, %edx
	xorl	%esi, %esi
	testl	%ebp, %ebp
	setne	%sil
	testl	%r12d, %r12d
	cmovnsl	%r12d, %esi
	testl	%r11d, %r11d
	movl	$4, %r14d
	cmovnsl	%r11d, %r14d
	testl	%r8d, %r8d
	jne	.LBB6_7
# BB#6:
	movl	%edx, %r8d
	shrl	%r8d
	addl	$16, %r8d
	testl	%esi, %esi
	movl	%ecx, %eax
	sete	%cl
	shrl	%cl, %r8d
	movl	%eax, %ecx
.LBB6_7:
	movl	-12(%rsp), %ebx         # 4-byte Reload
	testl	%ebx, %ebx
	jns	.LBB6_9
# BB#8:
	cmpl	$1, %ebp
	movl	$1, %eax
	movl	$1, %ebx
	sbbl	$-1, %ebx
	testl	%esi, %esi
	cmovel	%eax, %ebx
.LBB6_9:                                # %LzmaEncProps_Normalize.exit
	cmpl	$8, %r15d
	movl	$5, %eax
	jg	.LBB6_18
# BB#10:                                # %LzmaEncProps_Normalize.exit
	cmpl	$4, %r13d
	jg	.LBB6_18
# BB#11:                                # %LzmaEncProps_Normalize.exit
	cmpl	$4, %r9d
	jg	.LBB6_18
# BB#12:                                # %LzmaEncProps_Normalize.exit
	cmpl	$1073741824, %ecx       # imm = 0x40000000
	ja	.LBB6_18
# BB#13:
	movq	-8(%rsp), %rdi          # 8-byte Reload
	movl	%ecx, 252344(%rdi)
	movl	%r8d, 252348(%rdi)
	cmpl	$5, %edx
	movl	$5, %eax
	cmoval	%edx, %eax
	cmpl	$273, %eax              # imm = 0x111
	movl	$273, %ecx              # imm = 0x111
	cmovbl	%eax, %ecx
	movl	%ecx, 210384(%rdi)
	movl	%r15d, 213556(%rdi)
	movl	%r13d, 213560(%rdi)
	movl	%r9d, 213564(%rdi)
	xorl	%eax, %eax
	testl	%ebp, %ebp
	sete	%al
	movl	%eax, 252232(%rdi)
	movl	%esi, 1672(%rdi)
	testl	%esi, %esi
	je	.LBB6_14
# BB#15:
	movl	$2, %eax
	cmpl	$2, %r14d
	movl	-16(%rsp), %ecx         # 4-byte Reload
	jl	.LBB6_17
# BB#16:
	cmpl	$5, %r14d
	movl	$4, %eax
	cmovll	%r14d, %eax
	jmp	.LBB6_17
.LBB6_14:
	movl	$4, %eax
	movl	-16(%rsp), %ecx         # 4-byte Reload
.LBB6_17:
	movq	-8(%rsp), %rdx          # 8-byte Reload
	movl	%eax, 1656(%rdx)
	movl	%r8d, 1620(%rdx)
	movl	%ecx, 252312(%rdx)
	xorl	%eax, %eax
	cmpl	$1, %ebx
	setg	%al
	movl	%eax, 252336(%rdx)
	xorl	%eax, %eax
.LBB6_18:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	LzmaEnc_SetProps, .Lfunc_end6-LzmaEnc_SetProps
	.cfi_endproc

	.globl	LzmaEnc_InitPriceTables
	.p2align	4, 0x90
	.type	LzmaEnc_InitPriceTables,@function
LzmaEnc_InitPriceTables:                # @LzmaEnc_InitPriceTables
	.cfi_startproc
# BB#0:
	movl	$8, %eax
	.p2align	4, 0x90
.LBB7_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_4 Depth 2
                                        #     Child Loop BB7_6 Depth 2
                                        #     Child Loop BB7_9 Depth 2
                                        #     Child Loop BB7_11 Depth 2
	movl	%eax, %esi
	imull	%esi, %esi
	xorl	%ecx, %ecx
	cmpl	$65536, %esi            # imm = 0x10000
	jb	.LBB7_2
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB7_1 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_4:                                # %.lr.ph
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edx
	shrl	%edx
	incl	%ecx
	cmpl	$131071, %esi           # imm = 0x1FFFF
	movl	%edx, %esi
	ja	.LBB7_4
	jmp	.LBB7_5
	.p2align	4, 0x90
.LBB7_2:                                #   in Loop: Header=BB7_1 Depth=1
	movl	%esi, %edx
.LBB7_5:                                # %._crit_edge
                                        #   in Loop: Header=BB7_1 Depth=1
	imull	%edx, %edx
	addl	%ecx, %ecx
	cmpl	$65536, %edx            # imm = 0x10000
	jb	.LBB7_7
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph.1
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %esi
	shrl	%esi
	incl	%ecx
	cmpl	$131071, %edx           # imm = 0x1FFFF
	movl	%esi, %edx
	ja	.LBB7_6
.LBB7_7:                                # %._crit_edge.1
                                        #   in Loop: Header=BB7_1 Depth=1
	imull	%edx, %edx
	addl	%ecx, %ecx
	cmpl	$65536, %edx            # imm = 0x10000
	jb	.LBB7_8
	.p2align	4, 0x90
.LBB7_9:                                # %.lr.ph.2
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %esi
	shrl	%esi
	incl	%ecx
	cmpl	$131071, %edx           # imm = 0x1FFFF
	movl	%esi, %edx
	ja	.LBB7_9
	jmp	.LBB7_10
	.p2align	4, 0x90
.LBB7_8:                                #   in Loop: Header=BB7_1 Depth=1
	movl	%edx, %esi
.LBB7_10:                               # %._crit_edge.2
                                        #   in Loop: Header=BB7_1 Depth=1
	imull	%esi, %esi
	addl	%ecx, %ecx
	cmpl	$65536, %esi            # imm = 0x10000
	jb	.LBB7_12
	.p2align	4, 0x90
.LBB7_11:                               # %.lr.ph.3
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edx
	shrl	%edx
	incl	%ecx
	cmpl	$131071, %esi           # imm = 0x1FFFF
	movl	%edx, %esi
	ja	.LBB7_11
.LBB7_12:                               # %._crit_edge.3
                                        #   in Loop: Header=BB7_1 Depth=1
	movl	$161, %edx
	subl	%ecx, %edx
	movq	%rax, %rcx
	shrq	$4, %rcx
	movl	%edx, (%rdi,%rcx,4)
	addq	$16, %rax
	cmpq	$2048, %rax             # imm = 0x800
	jb	.LBB7_1
# BB#13:
	retq
.Lfunc_end7:
	.size	LzmaEnc_InitPriceTables, .Lfunc_end7-LzmaEnc_InitPriceTables
	.cfi_endproc

	.globl	LzmaEnc_Construct
	.p2align	4, 0x90
	.type	LzmaEnc_Construct,@function
LzmaEnc_Construct:                      # @LzmaEnc_Construct
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 112
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 252280(%r15)
	leaq	1560(%r15), %r14
	movq	%r14, %rdi
	callq	MatchFinder_Construct
	leaq	64(%r15), %rdi
	callq	MatchFinderMt_Construct
	movq	%r14, 1552(%r15)
	movl	$0, 44(%rsp)
	movq	$5, 8(%rsp)
	movl	$-1, 52(%rsp)
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 28(%rsp)
	movdqu	%xmm0, 16(%rsp)
	movl	$0, 48(%rsp)
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	LzmaEnc_SetProps
	movw	$256, 199484(%r15)      # imm = 0x100
	movl	$-24, %ebx
	movl	$2, %eax
	.p2align	4, 0x90
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	leal	26(%rbx), %esi
	movl	%esi, %ecx
	shrl	%ecx
	decl	%ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	movslq	%eax, %r13
	leaq	199484(%r15,%r13), %rdi
	cmpl	$1, %ebp
	movl	$1, %eax
	cmovbel	%eax, %ebp
	leal	-1(%rbp), %r12d
	leaq	1(%r12), %r14
	movq	%r14, %rdx
	callq	memset
	leal	-1(%r13,%rbp), %eax
	incl	%eax
	leal	27(%rbx), %esi
	movslq	%eax, %rbp
	leaq	199484(%r15,%rbp), %rdi
	movq	%r14, %rdx
	callq	memset
	leaq	1(%rbp,%r12), %rax
	addl	$2, %ebx
	jne	.LBB8_1
# BB#2:                                 # %.preheader.i.preheader
	movl	$8, %eax
	.p2align	4, 0x90
.LBB8_3:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_6 Depth 2
                                        #     Child Loop BB8_9 Depth 2
                                        #     Child Loop BB8_12 Depth 2
                                        #     Child Loop BB8_14 Depth 2
	movl	%eax, %esi
	imull	%esi, %esi
	xorl	%ecx, %ecx
	cmpl	$65536, %esi            # imm = 0x10000
	jb	.LBB8_4
# BB#5:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB8_3 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_6:                                # %.lr.ph.i
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edx
	shrl	%edx
	incl	%ecx
	cmpl	$131071, %esi           # imm = 0x1FFFF
	movl	%edx, %esi
	ja	.LBB8_6
	jmp	.LBB8_7
	.p2align	4, 0x90
.LBB8_4:                                #   in Loop: Header=BB8_3 Depth=1
	movl	%esi, %edx
.LBB8_7:                                # %._crit_edge.i
                                        #   in Loop: Header=BB8_3 Depth=1
	imull	%edx, %edx
	addl	%ecx, %ecx
	cmpl	$65536, %edx            # imm = 0x10000
	jb	.LBB8_8
	.p2align	4, 0x90
.LBB8_9:                                # %.lr.ph.1.i
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %esi
	shrl	%esi
	incl	%ecx
	cmpl	$131071, %edx           # imm = 0x1FFFF
	movl	%esi, %edx
	ja	.LBB8_9
	jmp	.LBB8_10
	.p2align	4, 0x90
.LBB8_8:                                #   in Loop: Header=BB8_3 Depth=1
	movl	%edx, %esi
.LBB8_10:                               # %._crit_edge.1.i
                                        #   in Loop: Header=BB8_3 Depth=1
	imull	%esi, %esi
	addl	%ecx, %ecx
	cmpl	$65536, %esi            # imm = 0x10000
	jb	.LBB8_11
	.p2align	4, 0x90
.LBB8_12:                               # %.lr.ph.2.i
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edx
	shrl	%edx
	incl	%ecx
	cmpl	$131071, %esi           # imm = 0x1FFFF
	movl	%edx, %esi
	ja	.LBB8_12
	jmp	.LBB8_13
	.p2align	4, 0x90
.LBB8_11:                               #   in Loop: Header=BB8_3 Depth=1
	movl	%esi, %edx
.LBB8_13:                               # %._crit_edge.2.i
                                        #   in Loop: Header=BB8_3 Depth=1
	imull	%edx, %edx
	addl	%ecx, %ecx
	cmpl	$65536, %edx            # imm = 0x10000
	jb	.LBB8_15
	.p2align	4, 0x90
.LBB8_14:                               # %.lr.ph.3.i
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %esi
	shrl	%esi
	incl	%ecx
	cmpl	$131071, %edx           # imm = 0x1FFFF
	movl	%esi, %edx
	ja	.LBB8_14
.LBB8_15:                               # %._crit_edge.3.i
                                        #   in Loop: Header=BB8_3 Depth=1
	movl	$161, %edx
	subl	%ecx, %edx
	movq	%rax, %rcx
	shrq	$4, %rcx
	movl	%edx, 207676(%r15,%rcx,4)
	addq	$16, %rax
	cmpq	$2048, %rax             # imm = 0x800
	jb	.LBB8_3
# BB#16:                                # %LzmaEnc_InitPriceTables.exit
	movq	$0, 213576(%r15)
	movq	$0, 252360(%r15)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	LzmaEnc_Construct, .Lfunc_end8-LzmaEnc_Construct
	.cfi_endproc

	.globl	LzmaEnc_Create
	.p2align	4, 0x90
	.type	LzmaEnc_Create,@function
LzmaEnc_Create:                         # @LzmaEnc_Create
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 16
.Lcfi40:
	.cfi_offset %rbx, -16
	movl	$291032, %esi           # imm = 0x470D8
	callq	*(%rdi)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB9_2
# BB#1:
	movq	%rbx, %rdi
	callq	LzmaEnc_Construct
.LBB9_2:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	LzmaEnc_Create, .Lfunc_end9-LzmaEnc_Create
	.cfi_endproc

	.globl	LzmaEnc_FreeLits
	.p2align	4, 0x90
	.type	LzmaEnc_FreeLits,@function
LzmaEnc_FreeLits:                       # @LzmaEnc_FreeLits
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 32
.Lcfi44:
	.cfi_offset %rbx, -24
.Lcfi45:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	213576(%r14), %rsi
	movq	%rbx, %rdi
	callq	*8(%rbx)
	movq	252360(%r14), %rsi
	movq	%rbx, %rdi
	callq	*8(%rbx)
	movq	$0, 213576(%r14)
	movq	$0, 252360(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	LzmaEnc_FreeLits, .Lfunc_end10-LzmaEnc_FreeLits
	.cfi_endproc

	.globl	LzmaEnc_Destruct
	.p2align	4, 0x90
	.type	LzmaEnc_Destruct,@function
LzmaEnc_Destruct:                       # @LzmaEnc_Destruct
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 32
.Lcfi49:
	.cfi_offset %rbx, -32
.Lcfi50:
	.cfi_offset %r14, -24
.Lcfi51:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	leaq	64(%rbx), %rdi
	movq	%r14, %rsi
	callq	MatchFinderMt_Destruct
	leaq	1560(%rbx), %rdi
	movq	%r14, %rsi
	callq	MatchFinder_Free
	movq	213576(%rbx), %rsi
	movq	%r15, %rdi
	callq	*8(%r15)
	movq	252360(%rbx), %rsi
	movq	%r15, %rdi
	callq	*8(%r15)
	movq	$0, 213576(%rbx)
	movq	$0, 252360(%rbx)
	movq	252280(%rbx), %rsi
	movq	%r15, %rdi
	callq	*8(%r15)
	movq	$0, 252280(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	LzmaEnc_Destruct, .Lfunc_end11-LzmaEnc_Destruct
	.cfi_endproc

	.globl	LzmaEnc_Destroy
	.p2align	4, 0x90
	.type	LzmaEnc_Destroy,@function
LzmaEnc_Destroy:                        # @LzmaEnc_Destroy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 32
.Lcfi55:
	.cfi_offset %rbx, -32
.Lcfi56:
	.cfi_offset %r14, -24
.Lcfi57:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	64(%rbx), %rdi
	movq	%r15, %rsi
	callq	MatchFinderMt_Destruct
	leaq	1560(%rbx), %rdi
	movq	%r15, %rsi
	callq	MatchFinder_Free
	movq	213576(%rbx), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	252360(%rbx), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	$0, 213576(%rbx)
	movq	$0, 252360(%rbx)
	movq	252280(%rbx), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	$0, 252280(%rbx)
	movq	8(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.Lfunc_end12:
	.size	LzmaEnc_Destroy, .Lfunc_end12-LzmaEnc_Destroy
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.text
	.globl	LzmaEnc_Init
	.p2align	4, 0x90
	.type	LzmaEnc_Init,@function
LzmaEnc_Init:                           # @LzmaEnc_Init
	.cfi_startproc
# BB#0:
	movq	$0, 252248(%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 210392(%rdi)
	movl	$0, 210408(%rdi)
	movl	$-1, 252240(%rdi)
	movq	$1, 252256(%rdi)
	movb	$0, 252244(%rdi)
	movq	252280(%rdi), %rax
	movq	%rax, 252264(%rdi)
	movq	$0, 252296(%rdi)
	movl	$0, 252304(%rdi)
	leaq	214112(%rdi), %rax
	movq	$-24, %rcx
	movaps	.LCPI13_0(%rip), %xmm0  # xmm0 = [1024,1024,1024,1024,1024,1024,1024,1024]
	.p2align	4, 0x90
.LBB13_1:                               # %.preheader75
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -528(%rax)
	movups	%xmm0, -48(%rax)
	movups	%xmm0, -512(%rax)
	movups	%xmm0, -32(%rax)
	movw	$1024, 213992(%rdi,%rcx) # imm = 0x400
	movw	$1024, 214016(%rdi,%rcx) # imm = 0x400
	movw	$1024, 214040(%rdi,%rcx) # imm = 0x400
	movw	$1024, 214064(%rdi,%rcx) # imm = 0x400
	movups	%xmm0, -496(%rax)
	movups	%xmm0, -16(%rax)
	movups	%xmm0, -480(%rax)
	movups	%xmm0, (%rax)
	movw	$1024, 213994(%rdi,%rcx) # imm = 0x400
	movw	$1024, 214018(%rdi,%rcx) # imm = 0x400
	movw	$1024, 214042(%rdi,%rcx) # imm = 0x400
	movw	$1024, 214066(%rdi,%rcx) # imm = 0x400
	addq	$64, %rax
	addq	$4, %rcx
	jne	.LBB13_1
# BB#2:
	movl	213556(%rdi), %ecx
	addl	213560(%rdi), %ecx
	cmpl	$23, %ecx
	ja	.LBB13_16
# BB#3:                                 # %.lr.ph
	movl	$768, %r10d             # imm = 0x300
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r10d
	movq	213576(%rdi), %rcx
	cmpl	$1, %r10d
	movl	$1, %r9d
	cmovaq	%r10, %r9
	cmpq	$16, %r9
	jae	.LBB13_5
# BB#4:
	xorl	%edx, %edx
	jmp	.LBB13_15
.LBB13_5:                               # %min.iters.checked
	movl	$4294967280, %edx       # imm = 0xFFFFFFF0
	andq	%r9, %rdx
	je	.LBB13_6
# BB#7:                                 # %vector.body.preheader
	leaq	-16(%rdx), %r8
	movl	%r8d, %esi
	shrl	$4, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB13_8
# BB#9:                                 # %vector.body.prol.preheader
	negq	%rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB13_10:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, (%rcx,%rax,2)
	movups	%xmm0, 16(%rcx,%rax,2)
	addq	$16, %rax
	incq	%rsi
	jne	.LBB13_10
	jmp	.LBB13_11
.LBB13_6:
	xorl	%edx, %edx
	jmp	.LBB13_15
.LBB13_8:
	xorl	%eax, %eax
.LBB13_11:                              # %vector.body.prol.loopexit
	cmpq	$112, %r8
	jb	.LBB13_14
# BB#12:                                # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rax, %rsi
	leaq	240(%rcx,%rax,2), %rax
	.p2align	4, 0x90
.LBB13_13:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax)
	movups	%xmm0, -224(%rax)
	movups	%xmm0, -208(%rax)
	movups	%xmm0, -192(%rax)
	movups	%xmm0, -176(%rax)
	movups	%xmm0, -160(%rax)
	movups	%xmm0, -144(%rax)
	movups	%xmm0, -128(%rax)
	movups	%xmm0, -112(%rax)
	movups	%xmm0, -96(%rax)
	movups	%xmm0, -80(%rax)
	movups	%xmm0, -64(%rax)
	movups	%xmm0, -48(%rax)
	movups	%xmm0, -32(%rax)
	movups	%xmm0, -16(%rax)
	movups	%xmm0, (%rax)
	addq	$256, %rax              # imm = 0x100
	addq	$-128, %rsi
	jne	.LBB13_13
.LBB13_14:                              # %middle.block
	cmpq	%rdx, %r9
	je	.LBB13_16
	.p2align	4, 0x90
.LBB13_15:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movw	$1024, (%rcx,%rdx,2)    # imm = 0x400
	incq	%rdx
	cmpq	%r10, %rdx
	jb	.LBB13_15
.LBB13_16:                              # %vector.body232
	movups	%xmm0, 214448(%rdi)
	movups	%xmm0, 214464(%rdi)
	movups	%xmm0, 214480(%rdi)
	movups	%xmm0, 214496(%rdi)
	movups	%xmm0, 214512(%rdi)
	movups	%xmm0, 214528(%rdi)
	movups	%xmm0, 214544(%rdi)
	movups	%xmm0, 214560(%rdi)
	movups	%xmm0, 214576(%rdi)
	movups	%xmm0, 214592(%rdi)
	movups	%xmm0, 214608(%rdi)
	movups	%xmm0, 214624(%rdi)
	movups	%xmm0, 214640(%rdi)
	movups	%xmm0, 214656(%rdi)
	movups	%xmm0, 214672(%rdi)
	movups	%xmm0, 214688(%rdi)
	movups	%xmm0, 214704(%rdi)
	movups	%xmm0, 214720(%rdi)
	movups	%xmm0, 214736(%rdi)
	movups	%xmm0, 214752(%rdi)
	movups	%xmm0, 214768(%rdi)
	movups	%xmm0, 214784(%rdi)
	movups	%xmm0, 214800(%rdi)
	movups	%xmm0, 214816(%rdi)
	movups	%xmm0, 214832(%rdi)
	movups	%xmm0, 214848(%rdi)
	movups	%xmm0, 214864(%rdi)
	movups	%xmm0, 214880(%rdi)
	movups	%xmm0, 214896(%rdi)
	movups	%xmm0, 214912(%rdi)
	movups	%xmm0, 214928(%rdi)
	movups	%xmm0, 214944(%rdi)
	movups	%xmm0, 214960(%rdi)
	movups	%xmm0, 214976(%rdi)
	movups	%xmm0, 214992(%rdi)
	movups	%xmm0, 215008(%rdi)
	movups	%xmm0, 215024(%rdi)
	movups	%xmm0, 215040(%rdi)
	movups	%xmm0, 215056(%rdi)
	movups	%xmm0, 215072(%rdi)
	movups	%xmm0, 215088(%rdi)
	movups	%xmm0, 215104(%rdi)
	movups	%xmm0, 215120(%rdi)
	movups	%xmm0, 215136(%rdi)
	movups	%xmm0, 215152(%rdi)
	movups	%xmm0, 215168(%rdi)
	movw	$1024, 215184(%rdi)     # imm = 0x400
	movw	$1024, 215186(%rdi)     # imm = 0x400
	movw	$1024, 215222(%rdi)     # imm = 0x400
	movw	$1024, 215220(%rdi)     # imm = 0x400
	movups	%xmm0, 215224(%rdi)
	movups	%xmm0, 215240(%rdi)
	movups	%xmm0, 215256(%rdi)
	movups	%xmm0, 215272(%rdi)
	movups	%xmm0, 215288(%rdi)
	movups	%xmm0, 215304(%rdi)
	movups	%xmm0, 215320(%rdi)
	movups	%xmm0, 215336(%rdi)
	movups	%xmm0, 215352(%rdi)
	movups	%xmm0, 215368(%rdi)
	movups	%xmm0, 215384(%rdi)
	movups	%xmm0, 215400(%rdi)
	movups	%xmm0, 215416(%rdi)
	movups	%xmm0, 215432(%rdi)
	movups	%xmm0, 215448(%rdi)
	movups	%xmm0, 215464(%rdi)
	movups	%xmm0, 215480(%rdi)
	movups	%xmm0, 215496(%rdi)
	movups	%xmm0, 215512(%rdi)
	movups	%xmm0, 215528(%rdi)
	movups	%xmm0, 215544(%rdi)
	movups	%xmm0, 215560(%rdi)
	movups	%xmm0, 215576(%rdi)
	movups	%xmm0, 215592(%rdi)
	movups	%xmm0, 215608(%rdi)
	movups	%xmm0, 215624(%rdi)
	movups	%xmm0, 215640(%rdi)
	movups	%xmm0, 215656(%rdi)
	movups	%xmm0, 215672(%rdi)
	movups	%xmm0, 215688(%rdi)
	movups	%xmm0, 215704(%rdi)
	movups	%xmm0, 215720(%rdi)
	movups	%xmm0, 215736(%rdi)
	movups	%xmm0, 215752(%rdi)
	movups	%xmm0, 215768(%rdi)
	movups	%xmm0, 215784(%rdi)
	movups	%xmm0, 215800(%rdi)
	movups	%xmm0, 215816(%rdi)
	movups	%xmm0, 215832(%rdi)
	movups	%xmm0, 215848(%rdi)
	movups	%xmm0, 215864(%rdi)
	movups	%xmm0, 215880(%rdi)
	movups	%xmm0, 215896(%rdi)
	movups	%xmm0, 215912(%rdi)
	movups	%xmm0, 215928(%rdi)
	movups	%xmm0, 215944(%rdi)
	movups	%xmm0, 215960(%rdi)
	movups	%xmm0, 215976(%rdi)
	movups	%xmm0, 215992(%rdi)
	movups	%xmm0, 216008(%rdi)
	movups	%xmm0, 216024(%rdi)
	movups	%xmm0, 216040(%rdi)
	movups	%xmm0, 216056(%rdi)
	movups	%xmm0, 216072(%rdi)
	movups	%xmm0, 216088(%rdi)
	movups	%xmm0, 216104(%rdi)
	movups	%xmm0, 216120(%rdi)
	movups	%xmm0, 216136(%rdi)
	movups	%xmm0, 216152(%rdi)
	movups	%xmm0, 216168(%rdi)
	movups	%xmm0, 216184(%rdi)
	movups	%xmm0, 216200(%rdi)
	movups	%xmm0, 216216(%rdi)
	movups	%xmm0, 216232(%rdi)
	movw	$1024, 233726(%rdi)     # imm = 0x400
	movw	$1024, 233724(%rdi)     # imm = 0x400
	movups	%xmm0, 233728(%rdi)
	movups	%xmm0, 233744(%rdi)
	movups	%xmm0, 233760(%rdi)
	movups	%xmm0, 233776(%rdi)
	movups	%xmm0, 233792(%rdi)
	movups	%xmm0, 233808(%rdi)
	movups	%xmm0, 233824(%rdi)
	movups	%xmm0, 233840(%rdi)
	movups	%xmm0, 233856(%rdi)
	movups	%xmm0, 233872(%rdi)
	movups	%xmm0, 233888(%rdi)
	movups	%xmm0, 233904(%rdi)
	movups	%xmm0, 233920(%rdi)
	movups	%xmm0, 233936(%rdi)
	movups	%xmm0, 233952(%rdi)
	movups	%xmm0, 233968(%rdi)
	movups	%xmm0, 233984(%rdi)
	movups	%xmm0, 234000(%rdi)
	movups	%xmm0, 234016(%rdi)
	movups	%xmm0, 234032(%rdi)
	movups	%xmm0, 234048(%rdi)
	movups	%xmm0, 234064(%rdi)
	movups	%xmm0, 234080(%rdi)
	movups	%xmm0, 234096(%rdi)
	movups	%xmm0, 234112(%rdi)
	movups	%xmm0, 234128(%rdi)
	movups	%xmm0, 234144(%rdi)
	movups	%xmm0, 234160(%rdi)
	movups	%xmm0, 234176(%rdi)
	movups	%xmm0, 234192(%rdi)
	movups	%xmm0, 234208(%rdi)
	movups	%xmm0, 234224(%rdi)
	movups	%xmm0, 234240(%rdi)
	movups	%xmm0, 234256(%rdi)
	movups	%xmm0, 234272(%rdi)
	movups	%xmm0, 234288(%rdi)
	movups	%xmm0, 234304(%rdi)
	movups	%xmm0, 234320(%rdi)
	movups	%xmm0, 234336(%rdi)
	movups	%xmm0, 234352(%rdi)
	movups	%xmm0, 234368(%rdi)
	movups	%xmm0, 234384(%rdi)
	movups	%xmm0, 234400(%rdi)
	movups	%xmm0, 234416(%rdi)
	movups	%xmm0, 234432(%rdi)
	movups	%xmm0, 234448(%rdi)
	movups	%xmm0, 234464(%rdi)
	movups	%xmm0, 234480(%rdi)
	movups	%xmm0, 234496(%rdi)
	movups	%xmm0, 234512(%rdi)
	movups	%xmm0, 234528(%rdi)
	movups	%xmm0, 234544(%rdi)
	movups	%xmm0, 234560(%rdi)
	movups	%xmm0, 234576(%rdi)
	movups	%xmm0, 234592(%rdi)
	movups	%xmm0, 234608(%rdi)
	movups	%xmm0, 234624(%rdi)
	movups	%xmm0, 234640(%rdi)
	movups	%xmm0, 234656(%rdi)
	movups	%xmm0, 234672(%rdi)
	movups	%xmm0, 234688(%rdi)
	movups	%xmm0, 234704(%rdi)
	movups	%xmm0, 234720(%rdi)
	movups	%xmm0, 234736(%rdi)
	movups	%xmm0, 215188(%rdi)
	movups	%xmm0, 215204(%rdi)
	movq	$0, 2856(%rdi)
	movl	$0, 210388(%rdi)
	movb	213564(%rdi), %cl
	movl	$1, %eax
	movl	$1, %edx
	shll	%cl, %edx
	decl	%edx
	movl	%edx, 213572(%rdi)
	movb	213560(%rdi), %cl
	shll	%cl, %eax
	decl	%eax
	movl	%eax, 213568(%rdi)
	retq
.Lfunc_end13:
	.size	LzmaEnc_Init, .Lfunc_end13-LzmaEnc_Init
	.cfi_endproc

	.globl	LzmaEnc_InitPrices
	.p2align	4, 0x90
	.type	LzmaEnc_InitPrices,@function
LzmaEnc_InitPrices:                     # @LzmaEnc_InitPrices
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 48
.Lcfi63:
	.cfi_offset %rbx, -48
.Lcfi64:
	.cfi_offset %r12, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, 252232(%r14)
	jne	.LBB14_4
# BB#1:
	movq	%r14, %rdi
	callq	FillDistancesPrices
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB14_2:                               # %.lr.ph.i.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %edx
	andl	$1, %edx
	movl	%eax, %ecx
	shrl	%ecx
	movzwl	215190(%r14), %esi
	leal	2(%rdx), %edi
	andl	$1, %ecx
	leal	4(%rcx,%rdx,2), %ebp
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	negl	%edx
	andl	$2032, %edx             # imm = 0x7F0
	xorl	%esi, %edx
	shrl	$4, %edx
	movl	%eax, %esi
	shrl	$2, %esi
	movzwl	215188(%r14,%rdi,2), %edi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	negl	%ecx
	andl	$2032, %ecx             # imm = 0x7F0
	xorl	%edi, %ecx
	shrl	$4, %ecx
	movl	207676(%r14,%rcx,4), %ecx
	addl	207676(%r14,%rdx,4), %ecx
	andl	$1, %esi
	movl	%eax, %edx
	shrl	$3, %edx
	movzwl	215188(%r14,%rbp,2), %edi
	leal	(%rsi,%rbp,2), %ebp
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	negl	%esi
	andl	$2032, %esi             # imm = 0x7F0
	xorl	%edi, %esi
	shrl	$4, %esi
	addl	207676(%r14,%rsi,4), %ecx
	andl	$1, %edx
	movzwl	215188(%r14,%rbp,2), %esi
	negl	%edx
	andl	$2032, %edx             # imm = 0x7F0
	xorl	%esi, %edx
	shrl	$4, %edx
	addl	207676(%r14,%rdx,4), %ecx
	movl	%ecx, 213484(%r14,%rax,4)
	incq	%rax
	cmpq	$16, %rax
	jne	.LBB14_2
# BB#3:                                 # %FillAlignPrices.exit
	movl	$0, 213548(%r14)
.LBB14_4:
	movl	210384(%r14), %eax
	decl	%eax
	movl	%eax, 252160(%r14)
	leaq	215220(%r14), %r15
	movl	%eax, 233656(%r14)
	movb	213564(%r14), %cl
	movl	$1, %r12d
	shll	%cl, %r12d
	leaq	207676(%r14), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_5:                               # %.lr.ph.i15
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	callq	LenPriceEnc_UpdateTable
	incl	%ebp
	cmpl	%ebp, %r12d
	jne	.LBB14_5
# BB#6:                                 # %LenPriceEnc_UpdateTables.exit16
	movb	213564(%r14), %cl
	addq	$233724, %r14           # imm = 0x390FC
	movl	$1, %r15d
	shll	%cl, %r15d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_7:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	callq	LenPriceEnc_UpdateTable
	incl	%ebp
	cmpl	%ebp, %r15d
	jne	.LBB14_7
# BB#8:                                 # %LenPriceEnc_UpdateTables.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	LzmaEnc_InitPrices, .Lfunc_end14-LzmaEnc_InitPrices
	.cfi_endproc

	.p2align	4, 0x90
	.type	FillDistancesPrices,@function
FillDistancesPrices:                    # @FillDistancesPrices
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 24
	subq	$392, %rsp              # imm = 0x188
.Lcfi70:
	.cfi_def_cfa_offset 416
.Lcfi71:
	.cfi_offset %rbx, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movl	$4, %r8d
	.p2align	4, 0x90
.LBB15_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_7 Depth 2
	movzbl	199484(%rdi,%r8), %eax
	movl	%eax, %r10d
	shrl	%r10d
	leal	-1(%r10), %ecx
	movl	%eax, %edx
	andl	$1, %edx
	orl	$2, %edx
	shll	%cl, %edx
	testl	%ecx, %ecx
	je	.LBB15_2
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB15_1 Depth=1
	leaq	214960(%rdi,%rdx,2), %r9
	addq	%rax, %rax
	subq	%rax, %r9
	addq	$-2, %r9
	movl	%r8d, %ebp
	subl	%edx, %ebp
	testb	$1, %cl
	jne	.LBB15_5
# BB#4:                                 #   in Loop: Header=BB15_1 Depth=1
	xorl	%eax, %eax
	movl	$1, %r11d
	cmpl	$2, %r10d
	jne	.LBB15_7
	jmp	.LBB15_8
	.p2align	4, 0x90
.LBB15_2:                               #   in Loop: Header=BB15_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB15_8
	.p2align	4, 0x90
.LBB15_5:                               # %.lr.ph.i.prol
                                        #   in Loop: Header=BB15_1 Depth=1
	movl	%ebp, %r11d
	andl	$1, %r11d
	shrl	%ebp
	movzwl	2(%r9), %eax
	movl	%r11d, %ecx
	negl	%ecx
	andl	$2032, %ecx             # imm = 0x7F0
	xorl	%eax, %ecx
	shrl	$4, %ecx
	movl	207676(%rdi,%rcx,4), %eax
	orl	$2, %r11d
	leal	-2(%r10), %ecx
	cmpl	$2, %r10d
	je	.LBB15_8
	.p2align	4, 0x90
.LBB15_7:                               # %.lr.ph.i
                                        #   Parent Loop BB15_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %esi
	andl	$1, %esi
	movl	%ebp, %edx
	shrl	%edx
	movl	%r11d, %ebx
	movzwl	(%r9,%rbx,2), %r10d
	leal	(%rsi,%r11,2), %ebx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	negl	%esi
	andl	$2032, %esi             # imm = 0x7F0
	xorl	%r10d, %esi
	shrl	$4, %esi
	addl	207676(%rdi,%rsi,4), %eax
	andl	$1, %edx
	shrl	$2, %ebp
	movzwl	(%r9,%rbx,2), %esi
	leal	(%rdx,%rbx,2), %r11d
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	negl	%edx
	andl	$2032, %edx             # imm = 0x7F0
	xorl	%esi, %edx
	shrl	$4, %edx
	addl	207676(%rdi,%rdx,4), %eax
	addl	$-2, %ecx
	jne	.LBB15_7
.LBB15_8:                               # %RcTree_ReverseGetPrice.exit
                                        #   in Loop: Header=BB15_1 Depth=1
	movl	%eax, -128(%rsp,%r8,4)
	incq	%r8
	cmpq	$128, %r8
	jne	.LBB15_1
# BB#9:                                 # %.preheader77
	leaq	211456(%rdi), %r8
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB15_10:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_12 Depth 2
                                        #       Child Loop BB15_13 Depth 3
                                        #     Child Loop BB15_17 Depth 2
                                        #     Child Loop BB15_19 Depth 2
	cmpl	$0, 213552(%rdi)
	je	.LBB15_18
# BB#11:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB15_10 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB15_12:                              # %.lr.ph
                                        #   Parent Loop BB15_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_13 Depth 3
	movl	%edx, %ecx
	orl	$64, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB15_13:                              # %.lr.ph.i74
                                        #   Parent Loop BB15_10 Depth=1
                                        #     Parent Loop BB15_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ecx, %ebp
	shrl	%ebp
	leal	(%rbp,%rbp), %ebx
	movq	%r9, %rax
	shlq	$7, %rax
	addq	%rdi, %rax
	movzwl	214448(%rbx,%rax), %eax
	andl	$1, %ecx
	negl	%ecx
	andl	$2032, %ecx             # imm = 0x7F0
	xorl	%eax, %ecx
	shrl	$4, %ecx
	addl	207676(%rdi,%rcx,4), %esi
	cmpl	$1, %ebp
	movl	%ebp, %ecx
	jne	.LBB15_13
# BB#14:                                # %RcTree_GetPrice.exit
                                        #   in Loop: Header=BB15_12 Depth=2
	movl	%edx, %eax
	movq	%r9, %rcx
	shlq	$8, %rcx
	addq	%rdi, %rcx
	movl	%esi, 210412(%rcx,%rax,4)
	incl	%edx
	movl	213552(%rdi), %esi
	cmpl	%esi, %edx
	jb	.LBB15_12
# BB#15:                                # %.preheader76
                                        #   in Loop: Header=BB15_10 Depth=1
	cmpl	$15, %esi
	jb	.LBB15_18
# BB#16:                                # %.lr.ph82.preheader
                                        #   in Loop: Header=BB15_10 Depth=1
	movl	$14, %edx
	.p2align	4, 0x90
.LBB15_17:                              # %.lr.ph82
                                        #   Parent Loop BB15_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %eax
	andl	$536870910, %eax        # imm = 0x1FFFFFFE
	movl	%edx, %esi
	movl	210412(%rcx,%rsi,4), %ebp
	leal	-80(%rbp,%rax,8), %eax
	movl	%eax, 210412(%rcx,%rsi,4)
	incl	%edx
	cmpl	213552(%rdi), %edx
	jb	.LBB15_17
.LBB15_18:                              # %.lr.ph85.preheader
                                        #   in Loop: Header=BB15_10 Depth=1
	movq	%r9, %rcx
	shlq	$8, %rcx
	movq	%r9, %rax
	shlq	$9, %rax
	movups	210412(%rdi,%rcx), %xmm0
	movups	%xmm0, 211436(%rdi,%rax)
	movq	%r8, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB15_19:                              # %.lr.ph85
                                        #   Parent Loop BB15_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	199488(%rdi,%rsi), %eax
	leaq	(%rdi,%rcx), %rbp
	movl	-112(%rsp,%rsi,4), %ebx
	addl	210412(%rbp,%rax,4), %ebx
	movl	%ebx, -4(%rdx)
	movzbl	199489(%rdi,%rsi), %eax
	movl	-108(%rsp,%rsi,4), %ebx
	addl	210412(%rbp,%rax,4), %ebx
	movl	%ebx, (%rdx)
	addq	$2, %rsi
	addq	$8, %rdx
	cmpq	$124, %rsi
	jne	.LBB15_19
# BB#20:                                # %._crit_edge
                                        #   in Loop: Header=BB15_10 Depth=1
	incq	%r9
	addq	$512, %r8               # imm = 0x200
	cmpq	$4, %r9
	jne	.LBB15_10
# BB#21:
	movl	$0, 252328(%rdi)
	addq	$392, %rsp              # imm = 0x188
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end15:
	.size	FillDistancesPrices, .Lfunc_end15-FillDistancesPrices
	.cfi_endproc

	.globl	LzmaEnc_PrepareForLzma2
	.p2align	4, 0x90
	.type	LzmaEnc_PrepareForLzma2,@function
LzmaEnc_PrepareForLzma2:                # @LzmaEnc_PrepareForLzma2
	.cfi_startproc
# BB#0:
	movq	%rsi, 1632(%rdi)
	movl	$1, 252352(%rdi)
	movl	%edx, %esi
	movq	%rcx, %rdx
	movq	%r8, %rcx
	jmp	LzmaEnc_AllocAndInit    # TAILCALL
.Lfunc_end16:
	.size	LzmaEnc_PrepareForLzma2, .Lfunc_end16-LzmaEnc_PrepareForLzma2
	.cfi_endproc

	.p2align	4, 0x90
	.type	LzmaEnc_AllocAndInit,@function
LzmaEnc_AllocAndInit:                   # @LzmaEnc_AllocAndInit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 64
.Lcfi80:
	.cfi_offset %rbx, -56
.Lcfi81:
	.cfi_offset %r12, -48
.Lcfi82:
	.cfi_offset %r13, -40
.Lcfi83:
	.cfi_offset %r14, -32
.Lcfi84:
	.cfi_offset %r15, -24
.Lcfi85:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r12
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movl	252344(%rbx), %eax
	xorl	%ecx, %ecx
	cmpl	$2, %eax
	jb	.LBB17_31
# BB#1:
	movl	$2, %ecx
	je	.LBB17_31
# BB#2:
	movl	$4, %ecx
	cmpl	$5, %eax
	jb	.LBB17_31
# BB#3:
	movl	$6, %ecx
	cmpl	$9, %eax
	jb	.LBB17_31
# BB#4:
	movl	$8, %ecx
	cmpl	$17, %eax
	jb	.LBB17_31
# BB#5:
	movl	$10, %ecx
	cmpl	$33, %eax
	jb	.LBB17_31
# BB#6:
	movl	$12, %ecx
	cmpl	$65, %eax
	jb	.LBB17_31
# BB#7:
	movl	$14, %ecx
	cmpl	$129, %eax
	jb	.LBB17_31
# BB#8:
	movl	$16, %ecx
	cmpl	$257, %eax              # imm = 0x101
	jb	.LBB17_31
# BB#9:
	movl	$18, %ecx
	cmpl	$513, %eax              # imm = 0x201
	jb	.LBB17_31
# BB#10:
	movl	$20, %ecx
	cmpl	$1025, %eax             # imm = 0x401
	jb	.LBB17_31
# BB#11:
	movl	$22, %ecx
	cmpl	$2049, %eax             # imm = 0x801
	jb	.LBB17_31
# BB#12:
	movl	$24, %ecx
	cmpl	$4097, %eax             # imm = 0x1001
	jb	.LBB17_31
# BB#13:
	movl	$26, %ecx
	cmpl	$8193, %eax             # imm = 0x2001
	jb	.LBB17_31
# BB#14:
	movl	$28, %ecx
	cmpl	$16385, %eax            # imm = 0x4001
	jb	.LBB17_31
# BB#15:
	movl	$30, %ecx
	cmpl	$32769, %eax            # imm = 0x8001
	jb	.LBB17_31
# BB#16:
	movl	$32, %ecx
	cmpl	$65537, %eax            # imm = 0x10001
	jb	.LBB17_31
# BB#17:
	movl	$34, %ecx
	cmpl	$131073, %eax           # imm = 0x20001
	jb	.LBB17_31
# BB#18:
	movl	$36, %ecx
	cmpl	$262145, %eax           # imm = 0x40001
	jb	.LBB17_31
# BB#19:
	movl	$38, %ecx
	cmpl	$524289, %eax           # imm = 0x80001
	jb	.LBB17_31
# BB#20:
	movl	$40, %ecx
	cmpl	$1048577, %eax          # imm = 0x100001
	jb	.LBB17_31
# BB#21:
	movl	$42, %ecx
	cmpl	$2097153, %eax          # imm = 0x200001
	jb	.LBB17_31
# BB#22:
	movl	$44, %ecx
	cmpl	$4194305, %eax          # imm = 0x400001
	jb	.LBB17_31
# BB#23:
	movl	$46, %ecx
	cmpl	$8388609, %eax          # imm = 0x800001
	jb	.LBB17_31
# BB#24:
	movl	$48, %ecx
	cmpl	$16777217, %eax         # imm = 0x1000001
	jb	.LBB17_31
# BB#25:
	movl	$50, %ecx
	cmpl	$33554433, %eax         # imm = 0x2000001
	jb	.LBB17_31
# BB#26:
	movl	$52, %ecx
	cmpl	$67108865, %eax         # imm = 0x4000001
	jb	.LBB17_31
# BB#27:
	movl	$54, %ecx
	cmpl	$134217729, %eax        # imm = 0x8000001
	jb	.LBB17_31
# BB#28:
	movl	$56, %ecx
	cmpl	$268435457, %eax        # imm = 0x10000001
	jb	.LBB17_31
# BB#29:
	movl	$58, %ecx
	cmpl	$536870913, %eax        # imm = 0x20000001
	jb	.LBB17_31
# BB#30:
	xorl	%ecx, %ecx
	cmpl	$1073741824, %eax       # imm = 0x40000000
	seta	%cl
	leal	60(%rcx,%rcx), %ecx
.LBB17_31:
	movl	%ecx, 213552(%rbx)
	movl	$0, 252332(%rbx)
	movl	$0, 252340(%rbx)
	cmpq	$0, 252280(%rbx)
	jne	.LBB17_34
# BB#32:
	movl	$65536, %esi            # imm = 0x10000
	movq	%r12, %rdi
	callq	*(%r12)
	movq	%rax, 252280(%rbx)
	testq	%rax, %rax
	je	.LBB17_51
# BB#33:
	addq	$65536, %rax            # imm = 0x10000
	movq	%rax, 252272(%rbx)
.LBB17_34:
	cmpl	$0, 252336(%rbx)
	je	.LBB17_36
# BB#35:
	cmpl	$0, 1672(%rbx)
	setne	%cl
	cmpl	$0, 252232(%rbx)
	sete	%al
	andb	%cl, %al
	jmp	.LBB17_37
.LBB17_36:
	xorl	%eax, %eax
.LBB17_37:
	movzbl	%al, %eax
	movl	%eax, 56(%rbx)
	movl	213560(%rbx), %r13d
	addl	213556(%rbx), %r13d
	movq	213576(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB17_40
# BB#38:
	cmpq	$0, 252360(%rbx)
	je	.LBB17_40
# BB#39:
	cmpl	%r13d, 252228(%rbx)
	je	.LBB17_43
.LBB17_40:                              # %._crit_edge.i
	movq	%r12, %rdi
	callq	*8(%r12)
	movq	252360(%rbx), %rsi
	movq	%r12, %rdi
	callq	*8(%r12)
	movq	$0, 213576(%rbx)
	movq	$0, 252360(%rbx)
	movl	$768, %eax              # imm = 0x300
	movl	%r13d, %ecx
	shll	%cl, %eax
	movslq	%eax, %rbp
	addq	%rbp, %rbp
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	*(%r12)
	movq	%rax, 213576(%rbx)
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	*(%r12)
	movq	%rax, 252360(%rbx)
	movq	213576(%rbx), %rsi
	testq	%rax, %rax
	je	.LBB17_46
# BB#41:                                # %._crit_edge.i
	testq	%rsi, %rsi
	je	.LBB17_46
# BB#42:
	movl	%r13d, 252228(%rbx)
	movl	56(%rbx), %eax
.LBB17_43:
	movl	252344(%rbx), %esi
	xorl	%ecx, %ecx
	cmpl	$16777216, %esi         # imm = 0x1000000
	seta	%cl
	movl	%ecx, 1676(%rbx)
	leal	4096(%rsi), %ecx
	movl	%r15d, %edi
	subl	%esi, %edi
	cmpl	%r15d, %ecx
	movl	$4096, %edx             # imm = 0x1000
	cmovbl	%edi, %edx
	testl	%eax, %eax
	je	.LBB17_47
# BB#44:
	leaq	64(%rbx), %rbp
	movl	210384(%rbx), %ecx
	movl	$273, %r8d              # imm = 0x111
	movq	%rbp, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r14, %r9
	callq	MatchFinderMt_Create
	testl	%eax, %eax
	jne	.LBB17_52
# BB#45:
	movq	%rbp, 48(%rbx)
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	MatchFinderMt_CreateVTable
	jmp	.LBB17_49
.LBB17_46:                              # %.critedge.i
	movq	%r12, %rdi
	callq	*8(%r12)
	movq	252360(%rbx), %rsi
	movq	%r12, %rdi
	callq	*8(%r12)
	movq	$0, 213576(%rbx)
	movq	$0, 252360(%rbx)
.LBB17_51:
	movl	$2, %eax
	jmp	.LBB17_52
.LBB17_47:
	leaq	1560(%rbx), %rbp
	movl	210384(%rbx), %ecx
	movl	$273, %r8d              # imm = 0x111
	movq	%rbp, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r14, %r9
	callq	MatchFinder_Create
	testl	%eax, %eax
	je	.LBB17_51
# BB#48:
	movq	%rbp, 48(%rbx)
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	MatchFinder_CreateVTable
.LBB17_49:
	movq	%rbx, %rdi
	callq	LzmaEnc_Init
	movq	%rbx, %rdi
	callq	LzmaEnc_InitPrices
	movq	$0, 252320(%rbx)
	xorl	%eax, %eax
.LBB17_52:                              # %LzmaEnc_Alloc.exit.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	LzmaEnc_AllocAndInit, .Lfunc_end17-LzmaEnc_AllocAndInit
	.cfi_endproc

	.globl	LzmaEnc_MemPrepare
	.p2align	4, 0x90
	.type	LzmaEnc_MemPrepare,@function
LzmaEnc_MemPrepare:                     # @LzmaEnc_MemPrepare
	.cfi_startproc
# BB#0:
	movl	$1, 1660(%rdi)
	movq	%rsi, 1624(%rdi)
	movq	%rdx, 1664(%rdi)
	movl	$1, 252352(%rdi)
	movl	%ecx, %esi
	movq	%r8, %rdx
	movq	%r9, %rcx
	jmp	LzmaEnc_AllocAndInit    # TAILCALL
.Lfunc_end18:
	.size	LzmaEnc_MemPrepare, .Lfunc_end18-LzmaEnc_MemPrepare
	.cfi_endproc

	.globl	LzmaEnc_Finish
	.p2align	4, 0x90
	.type	LzmaEnc_Finish,@function
LzmaEnc_Finish:                         # @LzmaEnc_Finish
	.cfi_startproc
# BB#0:
	cmpl	$0, 56(%rdi)
	je	.LBB19_1
# BB#2:
	addq	$64, %rdi
	jmp	MatchFinderMt_ReleaseStream # TAILCALL
.LBB19_1:
	retq
.Lfunc_end19:
	.size	LzmaEnc_Finish, .Lfunc_end19-LzmaEnc_Finish
	.cfi_endproc

	.globl	LzmaEnc_GetNumAvailableBytes
	.p2align	4, 0x90
	.type	LzmaEnc_GetNumAvailableBytes,@function
LzmaEnc_GetNumAvailableBytes:           # @LzmaEnc_GetNumAvailableBytes
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	48(%rax), %rdi
	jmpq	*16(%rax)               # TAILCALL
.Lfunc_end20:
	.size	LzmaEnc_GetNumAvailableBytes, .Lfunc_end20-LzmaEnc_GetNumAvailableBytes
	.cfi_endproc

	.globl	LzmaEnc_GetCurBuf
	.p2align	4, 0x90
	.type	LzmaEnc_GetCurBuf,@function
LzmaEnc_GetCurBuf:                      # @LzmaEnc_GetCurBuf
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 16
.Lcfi87:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	48(%rbx), %rdi
	callq	*24(%rbx)
	movl	210388(%rbx), %ecx
	subq	%rcx, %rax
	popq	%rbx
	retq
.Lfunc_end21:
	.size	LzmaEnc_GetCurBuf, .Lfunc_end21-LzmaEnc_GetCurBuf
	.cfi_endproc

	.globl	LzmaEnc_CodeOneMemBlock
	.p2align	4, 0x90
	.type	LzmaEnc_CodeOneMemBlock,@function
LzmaEnc_CodeOneMemBlock:                # @LzmaEnc_CodeOneMemBlock
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi91:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi93:
	.cfi_def_cfa_offset 80
.Lcfi94:
	.cfi_offset %rbx, -48
.Lcfi95:
	.cfi_offset %r12, -40
.Lcfi96:
	.cfi_offset %r14, -32
.Lcfi97:
	.cfi_offset %r15, -24
.Lcfi98:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movl	%r8d, %r12d
	movq	%rcx, %r15
	movq	%rdi, %rbx
	movq	$MyWrite, (%rsp)
	movq	%rdx, 8(%rsp)
	movq	(%r15), %rax
	movq	%rax, 16(%rsp)
	movl	$0, 24(%rsp)
	movl	$0, 252312(%rbx)
	movl	$0, 252332(%rbx)
	movl	$0, 252340(%rbx)
	testl	%esi, %esi
	je	.LBB22_2
# BB#1:
	movq	%rbx, %rdi
	callq	LzmaEnc_Init
.LBB22_2:
	movq	%rbx, %rdi
	callq	LzmaEnc_InitPrices
	movl	252320(%rbx), %ebp
	movl	$-1, 252240(%rbx)
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqu	%xmm0, 252248(%rbx)
	movb	$0, 252244(%rbx)
	movq	252280(%rbx), %rax
	movq	%rax, 252264(%rbx)
	movq	$0, 252296(%rbx)
	movl	$0, 252304(%rbx)
	movq	%rsp, %rax
	movq	%rax, 252288(%rbx)
	movl	(%r14), %ecx
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r12d, %edx
	callq	LzmaEnc_CodeOneBlock
	movl	252320(%rbx), %ecx
	subl	%ebp, %ecx
	movl	%ecx, (%r14)
	movq	16(%rsp), %rcx
	subq	%rcx, (%r15)
	cmpl	$0, 24(%rsp)
	movl	$7, %ecx
	cmovnel	%ecx, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	LzmaEnc_CodeOneMemBlock, .Lfunc_end22-LzmaEnc_CodeOneMemBlock
	.cfi_endproc

	.p2align	4, 0x90
	.type	MyWrite,@function
MyWrite:                                # @MyWrite
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 32
.Lcfi102:
	.cfi_offset %rbx, -24
.Lcfi103:
	.cfi_offset %r14, -16
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movq	16(%r14), %rax
	cmpq	%rbx, %rax
	jae	.LBB23_2
# BB#1:
	movl	$1, 24(%r14)
	movq	%rax, %rbx
.LBB23_2:
	movq	8(%r14), %rdi
	movq	%rbx, %rdx
	callq	memcpy
	subq	%rbx, 16(%r14)
	addq	%rbx, 8(%r14)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end23:
	.size	MyWrite, .Lfunc_end23-MyWrite
	.cfi_endproc

	.p2align	4, 0x90
	.type	LzmaEnc_CodeOneBlock,@function
LzmaEnc_CodeOneBlock:                   # @LzmaEnc_CodeOneBlock
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi107:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi108:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 56
	subq	$376, %rsp              # imm = 0x178
.Lcfi110:
	.cfi_def_cfa_offset 432
.Lcfi111:
	.cfi_offset %rbx, -56
.Lcfi112:
	.cfi_offset %r12, -48
.Lcfi113:
	.cfi_offset %r13, -40
.Lcfi114:
	.cfi_offset %r14, -32
.Lcfi115:
	.cfi_offset %r15, -24
.Lcfi116:
	.cfi_offset %rbp, -16
	movl	%ecx, 296(%rsp)         # 4-byte Spill
	movl	%edx, %r15d
	movl	%esi, 300(%rsp)         # 4-byte Spill
	movq	%rdi, %r13
	cmpl	$0, 252352(%r13)
	je	.LBB24_2
# BB#1:
	movq	48(%r13), %rdi
	callq	*(%r13)
	movl	$0, 252352(%r13)
.LBB24_2:
	movl	252340(%r13), %eax
	movl	252332(%r13), %ecx
	orl	%eax, %ecx
	jne	.LBB24_18
# BB#3:
	cmpl	$0, 252304(%r13)
	je	.LBB24_15
# BB#4:
	movl	$9, 252340(%r13)
	movl	$9, %eax
	cmpl	$0, 1696(%r13)
	jne	.LBB24_16
	jmp	.LBB24_5
.LBB24_15:
	xorl	%eax, %eax
	cmpl	$0, 1696(%r13)
	je	.LBB24_5
.LBB24_16:                              # %.thread.i
	movl	$8, 252340(%r13)
	movl	$8, %eax
	jmp	.LBB24_17
.LBB24_5:
	testl	%eax, %eax
	je	.LBB24_6
.LBB24_17:
	movl	$1, 252332(%r13)
.LBB24_18:                              # %CheckErrors.exit.thread
	addq	$376, %rsp              # imm = 0x178
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_6:
	movq	252320(%r13), %rcx
	testq	%rcx, %rcx
	movq	%rcx, %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movl	%ecx, %r14d
	jne	.LBB24_49
# BB#7:
	movq	48(%r13), %rdi
	callq	*16(%r13)
	testl	%eax, %eax
	je	.LBB24_19
# BB#8:
	movq	48(%r13), %rdi
	callq	*16(%r13)
	movl	%eax, 2872(%r13)
	movq	48(%r13), %rdi
	leaq	208188(%r13), %rsi
	callq	*32(%r13)
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	testl	%eax, %eax
	je	.LBB24_11
# BB#9:
	addl	$-2, %eax
	movl	208188(%r13,%rax,4), %eax
	cmpl	210384(%r13), %eax
	jne	.LBB24_11
# BB#10:
	movq	48(%r13), %rdi
	callq	*24(%r13)
.LBB24_11:                              # %ReadMatchDistances.exit
	movl	210388(%r13), %esi
	incl	%esi
	movl	%esi, 210388(%r13)
	movl	210408(%r13), %eax
	movq	%rax, %rdx
	shlq	$5, %rdx
	movzwl	213584(%r13,%rdx), %edi
	movl	252240(%r13), %ecx
	shrl	$11, %ecx
	imull	%edi, %ecx
	movl	%ecx, 252240(%r13)
	movl	$2048, %ebp             # imm = 0x800
	subl	%edi, %ebp
	shrl	$5, %ebp
	addl	%edi, %ebp
	movw	%bp, 213584(%r13,%rdx)
	cmpl	$16777216, %ecx         # imm = 0x1000000
	jae	.LBB24_30
# BB#12:
	shll	$8, %ecx
	movl	%ecx, 252240(%r13)
	movq	252248(%r13), %rcx
	movq	%rcx, %rdx
	shrq	$32, %rdx
	jne	.LBB24_21
# BB#13:
	cmpl	$-16777216, %ecx        # imm = 0xFF000000
	jb	.LBB24_21
# BB#14:                                # %._crit_edge.i
	movq	252256(%r13), %rdx
	incq	%rdx
	jmp	.LBB24_29
.LBB24_19:
	movq	%r13, %rdi
	movq	264(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	jmp	.LBB24_20
.LBB24_21:
	movb	252244(%r13), %al
	movq	252264(%r13), %rbp
.LBB24_22:                              # %RangeEnc_FlushStream.exit._crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	shrq	$32, %rcx
	addb	%al, %cl
	movb	%cl, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_27
# BB#23:                                #   in Loop: Header=BB24_22 Depth=1
	cmpl	$0, 252304(%r13)
	jne	.LBB24_27
# BB#24:                                #   in Loop: Header=BB24_22 Depth=1
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_26
# BB#25:                                #   in Loop: Header=BB24_22 Depth=1
	movl	$9, 252304(%r13)
.LBB24_26:                              #   in Loop: Header=BB24_22 Depth=1
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
.LBB24_27:                              # %RangeEnc_FlushStream.exit.i
                                        #   in Loop: Header=BB24_22 Depth=1
	decq	252256(%r13)
	movq	252248(%r13), %rcx
	movb	$-1, %al
	jne	.LBB24_22
# BB#28:
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, 252244(%r13)
	movl	210388(%r13), %esi
	movl	210408(%r13), %eax
	movl	$1, %edx
.LBB24_29:                              # %RangeEnc_ShiftLow.exit
	movq	%rdx, 252256(%r13)
	andq	$16777215, %rcx         # imm = 0xFFFFFF
	shlq	$8, %rcx
	movq	%rcx, 252248(%r13)
.LBB24_30:                              # %RangeEnc_EncodeBit.exit
	movl	%eax, %eax
	movl	kLiteralNextStates(,%rax,4), %eax
	movl	%eax, 210408(%r13)
	movq	48(%r13), %rdi
	negl	%esi
	callq	*8(%r13)
	movq	213576(%r13), %r14
	movzbl	%al, %ebx
	orl	$256, %ebx              # imm = 0x100
	.p2align	4, 0x90
.LBB24_31:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_39 Depth 2
	movl	%ebx, %eax
	shrl	$8, %eax
	movzwl	(%r14,%rax,2), %edx
	movl	252240(%r13), %esi
	movl	%esi, %ecx
	shrl	$11, %ecx
	imull	%edx, %ecx
	testb	%bl, %bl
	js	.LBB24_33
# BB#32:                                #   in Loop: Header=BB24_31 Depth=1
	movl	%ecx, 252240(%r13)
	movl	$2048, %esi             # imm = 0x800
	subl	%edx, %esi
	shrl	$5, %esi
	addl	%edx, %esi
	jmp	.LBB24_34
.LBB24_33:                              #   in Loop: Header=BB24_31 Depth=1
	movl	%ecx, %edi
	addq	%rdi, 252248(%r13)
	subl	%ecx, %esi
	movl	%esi, 252240(%r13)
	movl	%edx, %ecx
	shrl	$5, %ecx
	subl	%ecx, %edx
	movl	%esi, %ecx
	movl	%edx, %esi
.LBB24_34:                              #   in Loop: Header=BB24_31 Depth=1
	movw	%si, (%r14,%rax,2)
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB24_47
# BB#35:                                #   in Loop: Header=BB24_31 Depth=1
	shll	$8, %ecx
	movl	%ecx, 252240(%r13)
	movq	252248(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB24_38
# BB#36:                                #   in Loop: Header=BB24_31 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB24_38
# BB#37:                                # %._crit_edge.i234
                                        #   in Loop: Header=BB24_31 Depth=1
	movq	252256(%r13), %rcx
	incq	%rcx
	jmp	.LBB24_46
.LBB24_38:                              #   in Loop: Header=BB24_31 Depth=1
	movb	252244(%r13), %cl
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_39:                              # %RangeEnc_FlushStream.exit._crit_edge.i237
                                        #   Parent Loop BB24_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_44
# BB#40:                                #   in Loop: Header=BB24_39 Depth=2
	cmpl	$0, 252304(%r13)
	jne	.LBB24_44
# BB#41:                                #   in Loop: Header=BB24_39 Depth=2
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_43
# BB#42:                                #   in Loop: Header=BB24_39 Depth=2
	movl	$9, 252304(%r13)
.LBB24_43:                              #   in Loop: Header=BB24_39 Depth=2
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_44:                              # %RangeEnc_FlushStream.exit.i238
                                        #   in Loop: Header=BB24_39 Depth=2
	decq	252256(%r13)
	movq	252248(%r13), %rax
	movb	$-1, %cl
	jne	.LBB24_39
# BB#45:                                #   in Loop: Header=BB24_31 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 252244(%r13)
	movl	$1, %ecx
.LBB24_46:                              # %RangeEnc_ShiftLow.exit240
                                        #   in Loop: Header=BB24_31 Depth=1
	movq	%rcx, 252256(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 252248(%r13)
.LBB24_47:                              # %RangeEnc_EncodeBit.exit.i
                                        #   in Loop: Header=BB24_31 Depth=1
	addl	%ebx, %ebx
	cmpl	$65536, %ebx            # imm = 0x10000
	jb	.LBB24_31
# BB#48:                                # %.thread
	decl	210388(%r13)
	movq	264(%rsp), %rax         # 8-byte Reload
	leal	1(%rax), %r14d
.LBB24_49:
	movq	48(%r13), %rdi
	callq	*16(%r13)
	testl	%eax, %eax
	je	.LBB24_748
# BB#50:                                # %CheckErrors.exit489.thread510.preheader
	leaq	208188(%r13), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	leaq	210392(%r13), %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	leaq	2908(%r13), %rax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	leaq	2876(%r13), %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	leaq	252240(%r13), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	leaq	233724(%r13), %rax
	movq	%rax, 352(%rsp)         # 8-byte Spill
	leaq	207676(%r13), %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	leaq	215220(%r13), %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	leaq	2924(%r13), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	leaq	3260(%r13), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB24_51:                              # %CheckErrors.exit489.thread510
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_61 Depth 2
                                        #     Child Loop BB24_72 Depth 2
                                        #     Child Loop BB24_78 Depth 2
                                        #     Child Loop BB24_85 Depth 2
                                        #     Child Loop BB24_92 Depth 2
                                        #     Child Loop BB24_117 Depth 2
                                        #     Child Loop BB24_142 Depth 2
                                        #     Child Loop BB24_223 Depth 2
                                        #     Child Loop BB24_238 Depth 2
                                        #     Child Loop BB24_243 Depth 2
                                        #     Child Loop BB24_248 Depth 2
                                        #     Child Loop BB24_133 Depth 2
                                        #     Child Loop BB24_169 Depth 2
                                        #     Child Loop BB24_174 Depth 2
                                        #     Child Loop BB24_187 Depth 2
                                        #     Child Loop BB24_198 Depth 2
                                        #     Child Loop BB24_251 Depth 2
                                        #     Child Loop BB24_217 Depth 2
                                        #     Child Loop BB24_259 Depth 2
                                        #     Child Loop BB24_262 Depth 2
                                        #     Child Loop BB24_264 Depth 2
                                        #     Child Loop BB24_273 Depth 2
                                        #     Child Loop BB24_278 Depth 2
                                        #     Child Loop BB24_280 Depth 2
                                        #     Child Loop BB24_288 Depth 2
                                        #       Child Loop BB24_289 Depth 3
                                        #         Child Loop BB24_294 Depth 4
                                        #         Child Loop BB24_315 Depth 4
                                        #         Child Loop BB24_325 Depth 4
                                        #         Child Loop BB24_323 Depth 4
                                        #       Child Loop BB24_340 Depth 3
                                        #       Child Loop BB24_346 Depth 3
                                        #       Child Loop BB24_349 Depth 3
                                        #       Child Loop BB24_357 Depth 3
                                        #         Child Loop BB24_361 Depth 4
                                        #         Child Loop BB24_366 Depth 4
                                        #         Child Loop BB24_369 Depth 4
                                        #         Child Loop BB24_379 Depth 4
                                        #         Child Loop BB24_384 Depth 4
                                        #         Child Loop BB24_388 Depth 4
                                        #         Child Loop BB24_392 Depth 4
                                        #         Child Loop BB24_398 Depth 4
                                        #       Child Loop BB24_407 Depth 3
                                        #       Child Loop BB24_413 Depth 3
                                        #       Child Loop BB24_416 Depth 3
                                        #       Child Loop BB24_419 Depth 3
                                        #       Child Loop BB24_422 Depth 3
                                        #         Child Loop BB24_423 Depth 4
                                        #           Child Loop BB24_429 Depth 5
                                        #           Child Loop BB24_425 Depth 5
                                        #           Child Loop BB24_434 Depth 5
                                        #           Child Loop BB24_438 Depth 5
                                        #           Child Loop BB24_442 Depth 5
                                        #           Child Loop BB24_447 Depth 5
                                        #     Child Loop BB24_462 Depth 2
                                        #     Child Loop BB24_456 Depth 2
                                        #     Child Loop BB24_529 Depth 2
                                        #     Child Loop BB24_565 Depth 2
                                        #     Child Loop BB24_577 Depth 2
                                        #       Child Loop BB24_585 Depth 3
                                        #     Child Loop BB24_615 Depth 2
                                        #       Child Loop BB24_620 Depth 3
                                        #     Child Loop BB24_630 Depth 2
                                        #       Child Loop BB24_638 Depth 3
                                        #     Child Loop BB24_597 Depth 2
                                        #       Child Loop BB24_605 Depth 3
                                        #     Child Loop BB24_547 Depth 2
                                        #     Child Loop BB24_651 Depth 2
                                        #     Child Loop BB24_705 Depth 2
                                        #     Child Loop BB24_721 Depth 2
                                        #     Child Loop BB24_696 Depth 2
                                        #     Child Loop BB24_670 Depth 2
                                        #     Child Loop BB24_686 Depth 2
                                        #     Child Loop BB24_482 Depth 2
                                        #     Child Loop BB24_510 Depth 2
                                        #       Child Loop BB24_518 Depth 3
                                        #     Child Loop BB24_492 Depth 2
                                        #       Child Loop BB24_500 Depth 3
                                        #     Child Loop BB24_742 Depth 2
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	cmpl	$0, 252232(%r13)
	je	.LBB24_54
# BB#52:                                #   in Loop: Header=BB24_51 Depth=1
	cmpl	$0, 210388(%r13)
	je	.LBB24_57
# BB#53:                                #   in Loop: Header=BB24_51 Depth=1
	movl	2864(%r13), %ebp
	movl	2868(%r13), %r15d
	jmp	.LBB24_66
	.p2align	4, 0x90
.LBB24_54:                              #   in Loop: Header=BB24_51 Depth=1
	movl	2860(%r13), %eax
	cmpl	%eax, 2856(%r13)
	jne	.LBB24_63
# BB#55:                                #   in Loop: Header=BB24_51 Depth=1
	movq	$0, 2856(%r13)
	cmpl	$0, 210388(%r13)
	je	.LBB24_129
# BB#56:                                #   in Loop: Header=BB24_51 Depth=1
	movl	2864(%r13), %r15d
	movl	2868(%r13), %edi
	jmp	.LBB24_163
.LBB24_57:                              #   in Loop: Header=BB24_51 Depth=1
	movq	48(%r13), %rdi
	callq	*16(%r13)
	movl	%eax, 2872(%r13)
	movq	48(%r13), %rdi
	movq	256(%rsp), %rsi         # 8-byte Reload
	callq	*32(%r13)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	je	.LBB24_64
# BB#58:                                #   in Loop: Header=BB24_51 Depth=1
	leal	-2(%r15), %eax
	movl	208188(%r13,%rax,4), %ebp
	cmpl	210384(%r13), %ebp
	jne	.LBB24_65
# BB#59:                                #   in Loop: Header=BB24_51 Depth=1
	movq	48(%r13), %rdi
	callq	*24(%r13)
	movl	2872(%r13), %ecx
	cmpl	$273, %ecx              # imm = 0x111
	movl	$273, %edx              # imm = 0x111
	cmovael	%edx, %ecx
	cmpl	%ecx, %ebp
	jae	.LBB24_65
# BB#60:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	decq	%rax
	leal	-1(%r15), %edx
	movl	208188(%r13,%rdx,4), %esi
	incl	%esi
	movq	%rax, %rdx
	subq	%rsi, %rdx
	.p2align	4, 0x90
.LBB24_61:                              # %.lr.ph.i.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %esi
	movzbl	(%rax,%rsi), %ebx
	cmpb	(%rdx,%rsi), %bl
	jne	.LBB24_65
# BB#62:                                #   in Loop: Header=BB24_61 Depth=2
	incl	%ebp
	cmpl	%ecx, %ebp
	jb	.LBB24_61
	jmp	.LBB24_65
.LBB24_63:                              #   in Loop: Header=BB24_51 Depth=1
	leaq	(%rax,%rax,2), %rcx
	shlq	$4, %rcx
	movl	2900(%r13,%rcx), %r12d
	movl	2904(%r13,%rcx), %ebx
	movl	%r12d, 2860(%r13)
	subl	%eax, %r12d
	jmp	.LBB24_468
.LBB24_64:                              #   in Loop: Header=BB24_51 Depth=1
	xorl	%ebp, %ebp
.LBB24_65:                              #   in Loop: Header=BB24_51 Depth=1
	incl	210388(%r13)
.LBB24_66:                              #   in Loop: Header=BB24_51 Depth=1
	movl	2872(%r13), %r14d
	movl	$-1, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, %eax
	cmpl	$2, %r14d
	jae	.LBB24_68
# BB#67:                                #   in Loop: Header=BB24_51 Depth=1
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB24_469
	.p2align	4, 0x90
.LBB24_68:                              #   in Loop: Header=BB24_51 Depth=1
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpl	$273, %r14d             # imm = 0x111
	movl	$273, %eax              # imm = 0x111
	cmovael	%eax, %r14d
	movq	48(%r13), %rdi
	callq	*24(%r13)
	leaq	-1(%rax), %rsi
	movb	-1(%rax), %cl
	movl	210392(%r13), %edx
	incl	%edx
	subq	%rdx, %rsi
	negq	%rdx
	cmpl	$2, %r14d
	movb	(%rsi), %bl
	jbe	.LBB24_98
# BB#69:                                # %.split239.us.preheader.i
                                        #   in Loop: Header=BB24_51 Depth=1
	xorl	%r8d, %r8d
	cmpb	%bl, %cl
	movl	$0, %r12d
	jne	.LBB24_75
# BB#70:                                #   in Loop: Header=BB24_51 Depth=1
	movb	(%rax), %bl
	cmpb	(%rax,%rdx), %bl
	movl	$0, %r12d
	jne	.LBB24_75
# BB#71:                                # %.lr.ph231.us.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%r14d, %esi
	addq	%rax, %rdx
	movl	$2, %r12d
	.p2align	4, 0x90
.LBB24_72:                              # %.lr.ph231.us.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rax,%r12), %ebx
	cmpb	-1(%rdx,%r12), %bl
	jne	.LBB24_74
# BB#73:                                #   in Loop: Header=BB24_72 Depth=2
	incq	%r12
	cmpq	%rsi, %r12
	jb	.LBB24_72
.LBB24_74:                              # %.critedge.us.i
                                        #   in Loop: Header=BB24_51 Depth=1
	xorl	%ebx, %ebx
	cmpl	210384(%r13), %r12d
	movl	%r12d, %edx
	jae	.LBB24_135
.LBB24_75:                              # %.split239.us.1253.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210396(%r13), %esi
	incl	%esi
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpb	-1(%rdx), %cl
	jne	.LBB24_82
# BB#76:                                #   in Loop: Header=BB24_51 Depth=1
	negq	%rsi
	movb	(%rax), %dl
	cmpb	(%rax,%rsi), %dl
	jne	.LBB24_82
# BB#77:                                # %.lr.ph231.us.1.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%r14d, %edi
	addq	%rax, %rsi
	movl	$2, %edx
	.p2align	4, 0x90
.LBB24_78:                              # %.lr.ph231.us.1.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rax,%rdx), %ebx
	cmpb	-1(%rsi,%rdx), %bl
	jne	.LBB24_80
# BB#79:                                #   in Loop: Header=BB24_78 Depth=2
	incq	%rdx
	cmpq	%rdi, %rdx
	jb	.LBB24_78
.LBB24_80:                              # %.critedge.us.1.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	$1, %ebx
	cmpl	210384(%r13), %edx
	jae	.LBB24_135
# BB#81:                                #   in Loop: Header=BB24_51 Depth=1
	xorl	%r8d, %r8d
	cmpl	%r12d, %edx
	seta	%r8b
	cmoval	%edx, %r12d
.LBB24_82:                              # %.split239.us.2254.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210400(%r13), %esi
	incl	%esi
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpb	-1(%rdx), %cl
	jne	.LBB24_89
# BB#83:                                #   in Loop: Header=BB24_51 Depth=1
	negq	%rsi
	movb	(%rax), %dl
	cmpb	(%rax,%rsi), %dl
	jne	.LBB24_89
# BB#84:                                # %.lr.ph231.us.2.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%r14d, %edi
	addq	%rax, %rsi
	movl	$2, %edx
	.p2align	4, 0x90
.LBB24_85:                              # %.lr.ph231.us.2.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rax,%rdx), %ebx
	cmpb	-1(%rsi,%rdx), %bl
	jne	.LBB24_87
# BB#86:                                #   in Loop: Header=BB24_85 Depth=2
	incq	%rdx
	cmpq	%rdi, %rdx
	jb	.LBB24_85
.LBB24_87:                              # %.critedge.us.2.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	$2, %ebx
	cmpl	210384(%r13), %edx
	jae	.LBB24_135
# BB#88:                                #   in Loop: Header=BB24_51 Depth=1
	cmpl	%r12d, %edx
	cmoval	%edx, %r12d
	movl	$2, %edx
	cmoval	%edx, %r8d
.LBB24_89:                              # %.split239.us.3255.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210404(%r13), %edx
	incl	%edx
	movq	%rax, %rsi
	subq	%rdx, %rsi
	cmpb	-1(%rsi), %cl
	jne	.LBB24_113
# BB#90:                                #   in Loop: Header=BB24_51 Depth=1
	negq	%rdx
	movb	(%rax), %cl
	cmpb	(%rax,%rdx), %cl
	jne	.LBB24_113
# BB#91:                                # %.lr.ph231.us.3.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%r14d, %ecx
	movq	%rax, %rsi
	addq	%rdx, %rsi
	movl	$2, %edx
	.p2align	4, 0x90
.LBB24_92:                              # %.lr.ph231.us.3.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rax,%rdx), %ebx
	cmpb	-1(%rsi,%rdx), %bl
	jne	.LBB24_94
# BB#93:                                #   in Loop: Header=BB24_92 Depth=2
	incq	%rdx
	cmpq	%rcx, %rdx
	jb	.LBB24_92
.LBB24_94:                              # %.critedge.us.3.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	$3, %ebx
	cmpl	210384(%r13), %edx
	jae	.LBB24_135
# BB#95:                                #   in Loop: Header=BB24_51 Depth=1
	cmpl	%r12d, %edx
	cmoval	%edx, %r12d
	movl	$3, %eax
	cmoval	%eax, %r8d
	cmpl	210384(%r13), %ebp
	jb	.LBB24_114
	jmp	.LBB24_96
.LBB24_98:                              # %.split239.preheader.i
                                        #   in Loop: Header=BB24_51 Depth=1
	xorl	%r8d, %r8d
	cmpb	%bl, %cl
	movl	$0, %r12d
	jne	.LBB24_101
# BB#99:                                #   in Loop: Header=BB24_51 Depth=1
	movb	(%rax), %bl
	cmpb	(%rax,%rdx), %bl
	movl	$0, %r12d
	jne	.LBB24_101
# BB#100:                               # %.critedge.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	$2, %r12d
	cmpl	$2, 210384(%r13)
	jbe	.LBB24_153
.LBB24_101:                             # %.split239.1259.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210396(%r13), %edx
	incl	%edx
	movq	%rax, %rsi
	subq	%rdx, %rsi
	cmpb	-1(%rsi), %cl
	jne	.LBB24_105
# BB#102:                               #   in Loop: Header=BB24_51 Depth=1
	negq	%rdx
	movb	(%rax), %bl
	cmpb	(%rax,%rdx), %bl
	jne	.LBB24_105
# BB#103:                               # %.critedge.1.i
                                        #   in Loop: Header=BB24_51 Depth=1
	cmpl	$3, 210384(%r13)
	jb	.LBB24_154
# BB#104:                               #   in Loop: Header=BB24_51 Depth=1
	shrl	%r12d
	movl	%r12d, %r8d
	xorl	$1, %r8d
	movl	$2, %r12d
.LBB24_105:                             # %.split239.2260.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210400(%r13), %edx
	incl	%edx
	movq	%rax, %rsi
	subq	%rdx, %rsi
	cmpb	-1(%rsi), %cl
	jne	.LBB24_109
# BB#106:                               #   in Loop: Header=BB24_51 Depth=1
	negq	%rdx
	movb	(%rax), %bl
	cmpb	(%rax,%rdx), %bl
	jne	.LBB24_109
# BB#107:                               # %.critedge.2.i
                                        #   in Loop: Header=BB24_51 Depth=1
	cmpl	$3, 210384(%r13)
	jb	.LBB24_156
# BB#108:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$2, %r12d
	movl	$2, %edx
	cmovbl	%edx, %r12d
	cmovbl	%edx, %r8d
.LBB24_109:                             # %.split239.3261.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210404(%r13), %edx
	incl	%edx
	movq	%rax, %rsi
	subq	%rdx, %rsi
	cmpb	-1(%rsi), %cl
	jne	.LBB24_113
# BB#110:                               #   in Loop: Header=BB24_51 Depth=1
	negq	%rdx
	movb	(%rax), %cl
	cmpb	(%rax,%rdx), %cl
	jne	.LBB24_113
# BB#111:                               # %.critedge.3.i
                                        #   in Loop: Header=BB24_51 Depth=1
	cmpl	$3, 210384(%r13)
	jb	.LBB24_157
# BB#112:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$2, %r12d
	movl	$2, %eax
	cmovbl	%eax, %r12d
	movl	$3, %eax
	cmovbl	%eax, %r8d
.LBB24_113:                             # %.us-lcssa241.us.i
                                        #   in Loop: Header=BB24_51 Depth=1
	cmpl	210384(%r13), %ebp
	jae	.LBB24_96
.LBB24_114:                             #   in Loop: Header=BB24_51 Depth=1
	xorl	%edi, %edi
	cmpl	$2, %ebp
	jb	.LBB24_121
# BB#115:                               #   in Loop: Header=BB24_51 Depth=1
	leal	-1(%r15), %eax
	movl	208188(%r13,%rax,4), %edi
	cmpl	$3, %r15d
	jb	.LBB24_120
# BB#116:                               # %.lr.ph222.preheader.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%r15d, %eax
	movl	$4294967293, %ecx       # imm = 0xFFFFFFFD
	addq	%rcx, %rax
.LBB24_117:                             # %.lr.ph222.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rax), %ecx
	movl	208188(%r13,%rcx,4), %ecx
	leal	1(%rcx), %edx
	cmpl	%edx, %ebp
	jne	.LBB24_120
# BB#118:                               #   in Loop: Header=BB24_117 Depth=2
	movl	%edi, %esi
	shrl	$7, %esi
	movl	%eax, %edx
	movl	208188(%r13,%rdx,4), %edx
	cmpl	%edx, %esi
	jbe	.LBB24_120
# BB#119:                               #   in Loop: Header=BB24_117 Depth=2
	addl	$-2, %r15d
	addq	$-2, %rax
	cmpl	$3, %r15d
	movl	%edx, %edi
	movl	%ecx, %ebp
	jae	.LBB24_117
.LBB24_120:                             # %.critedge1.i
                                        #   in Loop: Header=BB24_51 Depth=1
	cmpl	$127, %edi
	movl	%ebp, %eax
	movl	$1, %ecx
	cmoval	%ecx, %eax
	cmpl	$2, %ebp
	cmovnel	%ebp, %eax
	movl	%eax, %ebp
.LBB24_121:                             #   in Loop: Header=BB24_51 Depth=1
	cmpl	$2, %r12d
	jb	.LBB24_136
# BB#122:                               #   in Loop: Header=BB24_51 Depth=1
	leal	1(%r12), %eax
	cmpl	%ebp, %eax
	jae	.LBB24_127
# BB#123:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$512, %edi              # imm = 0x200
	jb	.LBB24_125
# BB#124:                               #   in Loop: Header=BB24_51 Depth=1
	leal	2(%r12), %eax
	cmpl	%ebp, %eax
	jae	.LBB24_127
.LBB24_125:                             #   in Loop: Header=BB24_51 Depth=1
	cmpl	$32768, %edi            # imm = 0x8000
	jb	.LBB24_136
# BB#126:                               #   in Loop: Header=BB24_51 Depth=1
	leal	3(%r12), %eax
	cmpl	%ebp, %eax
	jb	.LBB24_136
.LBB24_127:                             #   in Loop: Header=BB24_51 Depth=1
	movl	%r12d, %esi
	decl	%esi
	movl	4(%rsp), %r14d          # 4-byte Reload
	je	.LBB24_155
# BB#128:                               #   in Loop: Header=BB24_51 Depth=1
	addl	%esi, 210388(%r13)
	movq	48(%r13), %rdi
	movl	%r8d, %ebx
	jmp	.LBB24_160
.LBB24_96:                              #   in Loop: Header=BB24_51 Depth=1
	decl	%r15d
	movl	208188(%r13,%r15,4), %eax
	addl	$4, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%ebp, %esi
	decl	%esi
	je	.LBB24_469
# BB#97:                                #   in Loop: Header=BB24_51 Depth=1
	addl	%esi, 210388(%r13)
	movq	48(%r13), %rdi
	callq	*40(%r13)
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jmp	.LBB24_469
.LBB24_129:                             #   in Loop: Header=BB24_51 Depth=1
	movq	48(%r13), %rdi
	callq	*16(%r13)
	movl	%eax, 2872(%r13)
	movq	48(%r13), %rdi
	movq	256(%rsp), %rsi         # 8-byte Reload
	callq	*32(%r13)
	movl	%eax, %edi
	testl	%edi, %edi
	je	.LBB24_152
# BB#130:                               #   in Loop: Header=BB24_51 Depth=1
	leal	-2(%rdi), %eax
	movl	208188(%r13,%rax,4), %r15d
	cmpl	210384(%r13), %r15d
	jne	.LBB24_162
# BB#131:                               #   in Loop: Header=BB24_51 Depth=1
	movq	%rdi, %rbx
	movq	48(%r13), %rdi
	callq	*24(%r13)
	movl	2872(%r13), %ecx
	cmpl	$273, %ecx              # imm = 0x111
	movl	$273, %edx              # imm = 0x111
	cmovael	%edx, %ecx
	cmpl	%ecx, %r15d
	jae	.LBB24_161
# BB#132:                               # %.lr.ph.i.i247.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	decq	%rax
	movq	%rbx, %rdi
	leal	-1(%rdi), %edx
	movl	208188(%r13,%rdx,4), %esi
	incl	%esi
	movq	%rax, %rdx
	subq	%rsi, %rdx
.LBB24_133:                             # %.lr.ph.i.i247
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r15d, %esi
	movzbl	(%rax,%rsi), %ebx
	cmpb	(%rdx,%rsi), %bl
	jne	.LBB24_162
# BB#134:                               #   in Loop: Header=BB24_133 Depth=2
	incl	%r15d
	cmpl	%ecx, %r15d
	jb	.LBB24_133
	jmp	.LBB24_162
.LBB24_135:                             # %.us-lcssa240.us.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%edx, %esi
	decl	%esi
	movl	%edx, %r12d
	movl	%ebx, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	4(%rsp), %r14d          # 4-byte Reload
	jne	.LBB24_159
	jmp	.LBB24_470
.LBB24_136:                             #   in Loop: Header=BB24_51 Depth=1
	cmpl	$3, %r14d
	jb	.LBB24_469
# BB#137:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$2, %ebp
	movl	4(%rsp), %r14d          # 4-byte Reload
	jb	.LBB24_470
# BB#138:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%edi, %r12d
	movq	48(%r13), %rdi
	callq	*16(%r13)
	movl	%eax, 2872(%r13)
	movq	48(%r13), %rdi
	movq	256(%rsp), %rsi         # 8-byte Reload
	callq	*32(%r13)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	je	.LBB24_218
# BB#139:                               #   in Loop: Header=BB24_51 Depth=1
	leal	-2(%r15), %eax
	movl	208188(%r13,%rax,4), %ebx
	cmpl	210384(%r13), %ebx
	jne	.LBB24_144
# BB#140:                               #   in Loop: Header=BB24_51 Depth=1
	movq	48(%r13), %rdi
	callq	*24(%r13)
	movl	2872(%r13), %edi
	cmpl	$273, %edi              # imm = 0x111
	movl	$273, %ecx              # imm = 0x111
	cmovael	%ecx, %edi
	cmpl	%edi, %ebx
	jae	.LBB24_144
# BB#141:                               # %.lr.ph.i188.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	decq	%rax
	leal	-1(%r15), %edx
	movl	208188(%r13,%rdx,4), %esi
	incl	%esi
	movq	%rax, %rdx
	subq	%rsi, %rdx
.LBB24_142:                             # %.lr.ph.i188.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %esi
	movzbl	(%rax,%rsi), %ecx
	cmpb	(%rdx,%rsi), %cl
	jne	.LBB24_144
# BB#143:                               #   in Loop: Header=BB24_142 Depth=2
	incl	%ebx
	cmpl	%edi, %ebx
	jb	.LBB24_142
.LBB24_144:                             # %ReadMatchDistances.exit190.i
                                        #   in Loop: Header=BB24_51 Depth=1
	incl	210388(%r13)
	movl	%r15d, 2868(%r13)
	movl	%ebx, 2864(%r13)
	cmpl	$2, %ebx
	jb	.LBB24_219
# BB#145:                               #   in Loop: Header=BB24_51 Depth=1
	decl	%r15d
	movl	208188(%r13,%r15,4), %eax
	cmpl	%ebp, %ebx
	movl	%r12d, %edi
	jb	.LBB24_147
# BB#146:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	%edi, %eax
	jb	.LBB24_470
.LBB24_147:                             #   in Loop: Header=BB24_51 Depth=1
	leal	1(%rbp), %esi
	movl	%eax, %edx
	shrl	$7, %edx
	cmpl	%esi, %ebx
	sete	%cl
	cmpl	%edi, %edx
	setbe	%dl
	cmpl	%esi, %ebx
	ja	.LBB24_470
# BB#148:                               #   in Loop: Header=BB24_51 Depth=1
	andb	%dl, %cl
	jne	.LBB24_470
# BB#149:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$3, %ebp
	jb	.LBB24_219
# BB#150:                               #   in Loop: Header=BB24_51 Depth=1
	incl	%ebx
	cmpl	%ebp, %ebx
	jb	.LBB24_219
# BB#151:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%r12d, %ecx
	shrl	$7, %ecx
	cmpl	%eax, %ecx
	ja	.LBB24_470
	jmp	.LBB24_219
.LBB24_152:                             #   in Loop: Header=BB24_51 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB24_162
.LBB24_153:                             #   in Loop: Header=BB24_51 Depth=1
	movl	$1, %esi
	xorl	%ebx, %ebx
	jmp	.LBB24_158
.LBB24_154:                             #   in Loop: Header=BB24_51 Depth=1
	movl	$2, %r12d
	movl	$1, %ebx
	movl	$1, %esi
	jmp	.LBB24_158
.LBB24_155:                             #   in Loop: Header=BB24_51 Depth=1
	movl	%r8d, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB24_470
.LBB24_156:                             #   in Loop: Header=BB24_51 Depth=1
	movl	$1, %esi
	movl	$2, %ebx
	movl	$2, %r12d
	jmp	.LBB24_158
.LBB24_157:                             #   in Loop: Header=BB24_51 Depth=1
	movl	$2, %r12d
	movl	$1, %esi
	movl	$3, %ebx
.LBB24_158:                             # %.us-lcssa240.us.thread.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	4(%rsp), %r14d          # 4-byte Reload
.LBB24_159:                             # %.us-lcssa240.us.thread.i
                                        #   in Loop: Header=BB24_51 Depth=1
	addl	%esi, 210388(%r13)
	movq	48(%r13), %rdi
.LBB24_160:                             # %GetOptimumFast.exit
                                        #   in Loop: Header=BB24_51 Depth=1
	callq	*40(%r13)
	movl	%ebx, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
                                        # kill: %R12D<def> %R12D<kill> %R12<kill> %R12<def>
	movq	%r12, 8(%rsp)           # 8-byte Spill
	jmp	.LBB24_470
.LBB24_161:                             #   in Loop: Header=BB24_51 Depth=1
	movq	%rbx, %rdi
.LBB24_162:                             #   in Loop: Header=BB24_51 Depth=1
	incl	210388(%r13)
.LBB24_163:                             #   in Loop: Header=BB24_51 Depth=1
	movl	2872(%r13), %ebp
	movl	$1, %r12d
	cmpl	$2, %ebp
	jae	.LBB24_165
# BB#164:                               #   in Loop: Header=BB24_51 Depth=1
	movl	$-1, %ebx
	jmp	.LBB24_468
.LBB24_165:                             #   in Loop: Header=BB24_51 Depth=1
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	cmpl	$273, %ebp              # imm = 0x111
	movl	$273, %eax              # imm = 0x111
	cmovael	%eax, %ebp
	movq	48(%r13), %rdi
	callq	*24(%r13)
	leaq	-1(%rax), %rdx
	movzbl	-1(%rax), %edi
	movq	312(%rsp), %rcx         # 8-byte Reload
	movdqu	(%rcx), %xmm0
	movdqa	%xmm0, 208(%rsp)
	movl	210392(%r13), %ecx
	incl	%ecx
	subq	%rcx, %rdx
	negq	%rcx
	cmpl	$2, %ebp
	movb	(%rdx), %dl
	jbe	.LBB24_177
# BB#166:                               # %.split.us.preheader.i253
                                        #   in Loop: Header=BB24_51 Depth=1
	xorl	%r10d, %r10d
	cmpb	%dl, %dil
	jne	.LBB24_171
# BB#167:                               #   in Loop: Header=BB24_51 Depth=1
	movb	(%rax), %dl
	cmpb	(%rax,%rcx), %dl
	jne	.LBB24_171
# BB#168:                               # %.lr.ph1117.us.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%ebp, %edx
	addq	%rax, %rcx
	movl	$2, %r10d
.LBB24_169:                             # %.lr.ph1117.us.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rax,%r10), %ebx
	cmpb	-1(%rcx,%r10), %bl
	jne	.LBB24_171
# BB#170:                               #   in Loop: Header=BB24_169 Depth=2
	incq	%r10
	cmpq	%rdx, %r10
	jb	.LBB24_169
.LBB24_171:                             # %.split.us.11199.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%r10d, 112(%rsp)
	movl	210396(%r13), %ecx
	incl	%ecx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpb	-1(%rdx), %dil
	jne	.LBB24_183
# BB#172:                               #   in Loop: Header=BB24_51 Depth=1
	negq	%rcx
	movb	(%rax), %dl
	cmpb	(%rax,%rcx), %dl
	jne	.LBB24_183
# BB#173:                               # %.lr.ph1117.us.1.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%ebp, %esi
	addq	%rax, %rcx
	movl	$2, %edx
.LBB24_174:                             # %.lr.ph1117.us.1.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rax,%rdx), %ebx
	cmpb	-1(%rcx,%rdx), %bl
	jne	.LBB24_176
# BB#175:                               #   in Loop: Header=BB24_174 Depth=2
	incq	%rdx
	cmpq	%rsi, %rdx
	jb	.LBB24_174
.LBB24_176:                             # %.critedge.us.1.i264
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%edx, 116(%rsp)
	cmpl	%edx, %r10d
	sbbl	%ebx, %ebx
	andl	$1, %ebx
	jmp	.LBB24_184
.LBB24_177:                             # %.split.preheader.i252
                                        #   in Loop: Header=BB24_51 Depth=1
	cmpb	%dl, %dil
	jne	.LBB24_179
# BB#178:                               #   in Loop: Header=BB24_51 Depth=1
	movb	(%rax), %dl
	movl	$2, %r10d
	cmpb	(%rax,%rcx), %dl
	je	.LBB24_180
.LBB24_179:                             #   in Loop: Header=BB24_51 Depth=1
	xorl	%r10d, %r10d
.LBB24_180:                             # %.split.11208.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%r10d, 112(%rsp)
	movl	210396(%r13), %ecx
	incl	%ecx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpb	-1(%rdx), %dil
	jne	.LBB24_190
# BB#181:                               #   in Loop: Header=BB24_51 Depth=1
	negq	%rcx
	movb	(%rax), %dl
	cmpb	(%rax,%rcx), %dl
	jne	.LBB24_190
# BB#182:                               # %.critedge.1.i267
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	$2, 116(%rsp)
	movl	%r10d, %ebx
	shrl	%ebx
	xorl	$1, %ebx
	jmp	.LBB24_191
.LBB24_183:                             #   in Loop: Header=BB24_51 Depth=1
	movl	$0, 116(%rsp)
	xorl	%ebx, %ebx
.LBB24_184:                             # %.split.us.21200.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210400(%r13), %ecx
	incl	%ecx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpb	-1(%rdx), %dil
	jne	.LBB24_194
# BB#185:                               #   in Loop: Header=BB24_51 Depth=1
	negq	%rcx
	movb	(%rax), %dl
	cmpb	(%rax,%rcx), %dl
	jne	.LBB24_194
# BB#186:                               # %.lr.ph1117.us.2.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	%rbx, %r8
	movl	%ebp, %esi
	addq	%rax, %rcx
	movl	$2, %edx
.LBB24_187:                             # %.lr.ph1117.us.2.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rax,%rdx), %ebx
	cmpb	-1(%rcx,%rdx), %bl
	jne	.LBB24_189
# BB#188:                               #   in Loop: Header=BB24_187 Depth=2
	incq	%rdx
	cmpq	%rsi, %rdx
	jb	.LBB24_187
.LBB24_189:                             # %.critedge.us.2.i265
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%edx, 120(%rsp)
	movq	%r8, %rbx
	cmpl	112(%rsp,%rbx,4), %edx
	movl	$2, %ecx
	cmoval	%ecx, %ebx
	jmp	.LBB24_195
.LBB24_190:                             #   in Loop: Header=BB24_51 Depth=1
	movl	$0, 116(%rsp)
	xorl	%ebx, %ebx
.LBB24_191:                             # %.split.21209.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210400(%r13), %ecx
	incl	%ecx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpb	-1(%rdx), %dil
	jne	.LBB24_201
# BB#192:                               #   in Loop: Header=BB24_51 Depth=1
	negq	%rcx
	movb	(%rax), %dl
	cmpb	(%rax,%rcx), %dl
	jne	.LBB24_201
# BB#193:                               # %.critedge.2.i268
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	$2, 120(%rsp)
	movl	%ebx, %ecx
	cmpl	$2, 112(%rsp,%rcx,4)
	movl	$2, %ecx
	cmovbl	%ecx, %ebx
	jmp	.LBB24_202
.LBB24_194:                             #   in Loop: Header=BB24_51 Depth=1
	movl	$0, 120(%rsp)
.LBB24_195:                             # %.split.us.31201.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210404(%r13), %ecx
	incl	%ecx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpb	-1(%rdx), %dil
	jne	.LBB24_205
# BB#196:                               #   in Loop: Header=BB24_51 Depth=1
	negq	%rcx
	movb	(%rax), %dl
	cmpb	(%rax,%rcx), %dl
	jne	.LBB24_205
# BB#197:                               # %.lr.ph1117.us.3.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	%rbx, %r8
	movl	%ebp, %esi
	addq	%rax, %rcx
	movl	$2, %edx
.LBB24_198:                             # %.lr.ph1117.us.3.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rax,%rdx), %ebx
	cmpb	-1(%rcx,%rdx), %bl
	jne	.LBB24_200
# BB#199:                               #   in Loop: Header=BB24_198 Depth=2
	incq	%rdx
	cmpq	%rsi, %rdx
	jb	.LBB24_198
.LBB24_200:                             # %.critedge.us.3.i266
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%edx, 124(%rsp)
	movq	%r8, %rbx
	movl	%ebx, %ecx
	cmpl	112(%rsp,%rcx,4), %edx
	movl	$3, %ecx
	cmoval	%ecx, %ebx
	jmp	.LBB24_206
.LBB24_201:                             #   in Loop: Header=BB24_51 Depth=1
	movl	$0, 120(%rsp)
.LBB24_202:                             # %.split.31210.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210404(%r13), %ecx
	incl	%ecx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpb	-1(%rdx), %dil
	jne	.LBB24_205
# BB#203:                               #   in Loop: Header=BB24_51 Depth=1
	negq	%rcx
	movb	(%rax), %dl
	cmpb	(%rax,%rcx), %dl
	jne	.LBB24_205
# BB#204:                               # %.critedge.3.i269
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	$2, 124(%rsp)
	movl	%ebx, %ecx
	cmpl	$2, 112(%rsp,%rcx,4)
	movl	$3, %ecx
	cmovbl	%ecx, %ebx
	jmp	.LBB24_206
.LBB24_205:                             #   in Loop: Header=BB24_51 Depth=1
	movl	$0, 124(%rsp)
.LBB24_206:                             # %.us-lcssa.us.i254
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%ebx, %ecx
	movl	112(%rsp,%rcx,4), %ebp
	movl	210384(%r13), %ecx
	cmpl	%ecx, %ebp
	jae	.LBB24_211
# BB#207:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	%ecx, %r15d
	jae	.LBB24_213
# BB#208:                               #   in Loop: Header=BB24_51 Depth=1
	movl	208(%rsp), %ecx
	incl	%ecx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	movzbl	-1(%rdx), %ebx
	cmpl	$2, %r15d
	sbbb	%cl, %cl
	cmpb	%bl, %dil
	setne	%dl
	cmpl	$1, %ebp
	ja	.LBB24_215
# BB#209:                               #   in Loop: Header=BB24_51 Depth=1
	andb	%dl, %cl
	je	.LBB24_215
# BB#210:                               #   in Loop: Header=BB24_51 Depth=1
	movl	$-1, %ebx
	jmp	.LBB24_468
.LBB24_211:                             #   in Loop: Header=BB24_51 Depth=1
	movl	%ebp, %esi
	decl	%esi
	je	.LBB24_468
# BB#212:                               #   in Loop: Header=BB24_51 Depth=1
	addl	%esi, 210388(%r13)
	movq	48(%r13), %rdi
	callq	*40(%r13)
	movl	%ebp, %r12d
	jmp	.LBB24_468
.LBB24_213:                             #   in Loop: Header=BB24_51 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	decl	%eax
	movl	208188(%r13,%rax,4), %ebx
	addl	$4, %ebx
	movl	%r15d, %esi
	decl	%esi
	je	.LBB24_468
# BB#214:                               #   in Loop: Header=BB24_51 Depth=1
	addl	%esi, 210388(%r13)
	movq	48(%r13), %rdi
	callq	*40(%r13)
	movl	%r15d, %r12d
	jmp	.LBB24_468
.LBB24_215:                             #   in Loop: Header=BB24_51 Depth=1
	movq	%rbp, 160(%rsp)         # 8-byte Spill
	movl	210408(%r13), %esi
	movl	%esi, 2880(%r13)
	movl	213572(%r13), %r14d
	movl	4(%rsp), %ecx           # 4-byte Reload
	andl	%ecx, %r14d
	movl	213568(%r13), %edx
	andl	%ecx, %edx
	movl	213556(%r13), %ecx
	shll	%cl, %edx
	movzbl	-2(%rax), %ebp
	movl	$8, %eax
	subl	%ecx, %eax
	movl	%eax, %ecx
	shrl	%cl, %ebp
	addl	%edx, %ebp
	shll	$8, %ebp
	leal	(%rbp,%rbp,2), %eax
	addq	%rax, %rax
	addq	213576(%r13), %rax
	movq	%rsi, %rdx
	shlq	$5, %rdx
	addq	%r13, %rdx
	movzwl	213584(%rdx,%r14,2), %ecx
	shrq	$2, %rcx
	andl	$16380, %ecx            # imm = 0x3FFC
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	cmpq	$7, %rsi
	movq	%rdx, %rsi
	leaq	213584(%rsi,%r14,2), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movl	207676(%r13,%rcx), %ecx
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	leal	256(%rdi), %ecx
	jae	.LBB24_250
# BB#216:                               #   in Loop: Header=BB24_51 Depth=1
	xorl	%ebp, %ebp
.LBB24_217:                             #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %edx
	shrl	$8, %edx
	movzwl	(%rax,%rdx,2), %edx
	movl	%ecx, %ebx
	shrl	$7, %ebx
	andl	$1, %ebx
	negl	%ebx
	andl	$2032, %ebx             # imm = 0x7F0
	xorl	%edx, %ebx
	shrl	$4, %ebx
	addl	207676(%r13,%rbx,4), %ebp
	addl	%ecx, %ecx
	cmpl	$65536, %ecx            # imm = 0x10000
	jb	.LBB24_217
	jmp	.LBB24_252
.LBB24_218:                             # %ReadMatchDistances.exit190.i.thread
                                        #   in Loop: Header=BB24_51 Depth=1
	incl	210388(%r13)
	movl	%r15d, 2868(%r13)
	movl	$0, 2864(%r13)
.LBB24_219:                             #   in Loop: Header=BB24_51 Depth=1
	movq	48(%r13), %rdi
	callq	*24(%r13)
	movb	-1(%rax), %r8b
	movq	%rax, %rsi
	decq	%rsi
	leal	-1(%rbp), %r9d
	movl	210392(%r13), %ebx
	incl	%ebx
	movq	%rsi, %rdi
	subq	%rbx, %rdi
	cmpl	$2, %r9d
	movb	(%rdi), %bl
	jbe	.LBB24_225
# BB#220:                               # %.split.us.preheader.i
                                        #   in Loop: Header=BB24_51 Depth=1
	cmpb	%bl, %r8b
	jne	.LBB24_235
# BB#221:                               #   in Loop: Header=BB24_51 Depth=1
	movb	(%rax), %bl
	cmpb	1(%rdi), %bl
	jne	.LBB24_235
# BB#222:                               # %.lr.ph.us.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	$2, %ebx
.LBB24_223:                             # %.lr.ph.us.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %edx
	movzbl	(%rsi,%rdx), %ecx
	cmpb	(%rdi,%rdx), %cl
	jne	.LBB24_235
# BB#224:                               #   in Loop: Header=BB24_223 Depth=2
	incl	%ebx
	cmpl	%r9d, %ebx
	jb	.LBB24_223
	jmp	.LBB24_470
.LBB24_225:                             # %.split.preheader.i
                                        #   in Loop: Header=BB24_51 Depth=1
	cmpb	%bl, %r8b
	jne	.LBB24_227
# BB#226:                               #   in Loop: Header=BB24_51 Depth=1
	movb	(%rax), %cl
	cmpb	1(%rdi), %cl
	je	.LBB24_470
.LBB24_227:                             # %.thread201.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210396(%r13), %ecx
	incl	%ecx
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	cmpb	(%rdx), %r8b
	jne	.LBB24_229
# BB#228:                               #   in Loop: Header=BB24_51 Depth=1
	negq	%rcx
	movb	(%rax), %dl
	cmpb	(%rax,%rcx), %dl
	je	.LBB24_470
.LBB24_229:                             # %.thread201.1.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210400(%r13), %ecx
	incl	%ecx
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	cmpb	(%rdx), %r8b
	jne	.LBB24_231
# BB#230:                               #   in Loop: Header=BB24_51 Depth=1
	negq	%rcx
	movb	(%rax), %dl
	cmpb	(%rax,%rcx), %dl
	je	.LBB24_470
.LBB24_231:                             # %.thread201.2.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210404(%r13), %ecx
	incl	%ecx
	subq	%rcx, %rsi
	cmpb	(%rsi), %r8b
	jne	.LBB24_233
# BB#232:                               #   in Loop: Header=BB24_51 Depth=1
	negq	%rcx
	movb	(%rax), %dl
	cmpb	(%rax,%rcx), %dl
	je	.LBB24_470
	jmp	.LBB24_233
.LBB24_235:                             # %.thread201.us.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210396(%r13), %ecx
	incl	%ecx
	movq	%rsi, %rdi
	subq	%rcx, %rdi
	cmpb	(%rdi), %r8b
	jne	.LBB24_240
# BB#236:                               #   in Loop: Header=BB24_51 Depth=1
	movb	(%rax), %cl
	cmpb	1(%rdi), %cl
	jne	.LBB24_240
# BB#237:                               # %.lr.ph.us.1.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	$2, %ebx
.LBB24_238:                             # %.lr.ph.us.1.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %ecx
	movzbl	(%rsi,%rcx), %edx
	cmpb	(%rdi,%rcx), %dl
	jne	.LBB24_240
# BB#239:                               #   in Loop: Header=BB24_238 Depth=2
	incl	%ebx
	cmpl	%r9d, %ebx
	jb	.LBB24_238
	jmp	.LBB24_470
.LBB24_240:                             # %.thread201.us.1.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210400(%r13), %ecx
	incl	%ecx
	movq	%rsi, %rdi
	subq	%rcx, %rdi
	cmpb	(%rdi), %r8b
	jne	.LBB24_245
# BB#241:                               #   in Loop: Header=BB24_51 Depth=1
	movb	(%rax), %cl
	cmpb	1(%rdi), %cl
	jne	.LBB24_245
# BB#242:                               # %.lr.ph.us.2.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	$2, %ebx
.LBB24_243:                             # %.lr.ph.us.2.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %ecx
	movzbl	(%rsi,%rcx), %edx
	cmpb	(%rdi,%rcx), %dl
	jne	.LBB24_245
# BB#244:                               #   in Loop: Header=BB24_243 Depth=2
	incl	%ebx
	cmpl	%r9d, %ebx
	jb	.LBB24_243
	jmp	.LBB24_470
.LBB24_245:                             # %.thread201.us.2.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210404(%r13), %ecx
	incl	%ecx
	movq	%rsi, %rdi
	subq	%rcx, %rdi
	cmpb	(%rdi), %r8b
	jne	.LBB24_233
# BB#246:                               #   in Loop: Header=BB24_51 Depth=1
	movb	(%rax), %al
	cmpb	1(%rdi), %al
	jne	.LBB24_233
# BB#247:                               # %.lr.ph.us.3.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	$2, %eax
.LBB24_248:                             # %.lr.ph.us.3.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ecx
	movzbl	(%rsi,%rcx), %edx
	cmpb	(%rdi,%rcx), %dl
	jne	.LBB24_233
# BB#249:                               #   in Loop: Header=BB24_248 Depth=2
	incl	%eax
	cmpl	%r9d, %eax
	jb	.LBB24_248
	jmp	.LBB24_470
.LBB24_233:                             # %.us-lcssa.us.i
                                        #   in Loop: Header=BB24_51 Depth=1
	addl	$4, %r12d
	movl	%ebp, %esi
	addl	$-2, %esi
	je	.LBB24_454
# BB#234:                               #   in Loop: Header=BB24_51 Depth=1
	addl	%esi, 210388(%r13)
	movq	48(%r13), %rdi
	callq	*40(%r13)
	movl	%r12d, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jmp	.LBB24_470
.LBB24_250:                             #   in Loop: Header=BB24_51 Depth=1
	movl	$256, %r8d              # imm = 0x100
	xorl	%ebp, %ebp
	movl	%ebx, %r11d
.LBB24_251:                             #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r11,%r11), %r12d
	movl	%r8d, %edx
	andl	%r12d, %edx
	movl	%ecx, %r9d
	shrl	$8, %r9d
	addl	%r8d, %r9d
	addl	%edx, %r9d
	movzwl	(%rax,%r9,2), %edx
	movl	%ecx, %ebx
	shrl	$7, %ebx
	andl	$1, %ebx
	negl	%ebx
	andl	$2032, %ebx             # imm = 0x7F0
	xorl	%edx, %ebx
	shrl	$4, %ebx
	addl	207676(%r13,%rbx,4), %ebp
	xorl	%ecx, %r11d
	addl	%r11d, %r11d
	addl	%ecx, %ecx
	notl	%r11d
	andl	%r11d, %r8d
	cmpl	$65536, %ecx            # imm = 0x10000
	movl	%r12d, %r11d
	jb	.LBB24_251
.LBB24_252:                             # %LitEnc_GetPriceMatched.exit.i
                                        #   in Loop: Header=BB24_51 Depth=1
	addl	56(%rsp), %ebp          # 4-byte Folded Reload
	movl	%ebp, 2924(%r13)
	movl	$-1, 2952(%r13)
	movl	$0, 2932(%r13)
	movq	64(%rsp), %rax          # 8-byte Reload
	movzwl	(%rax), %eax
	xorl	$2032, %eax             # imm = 0x7F0
	shrl	$2, %eax
	andl	$16380, %eax            # imm = 0x3FFC
	movl	207676(%r13,%rax), %r11d
	movq	144(%rsp), %rcx         # 8-byte Reload
	movzwl	213968(%r13,%rcx,2), %eax
	xorl	$2032, %eax             # imm = 0x7F0
	shrl	$2, %eax
	andl	$16380, %eax            # imm = 0x3FFC
	movl	207676(%r13,%rax), %r8d
	addl	%r11d, %r8d
	movl	$-1, %ebx
	cmpb	8(%rsp), %dil           # 1-byte Folded Reload
	jne	.LBB24_255
# BB#253:                               #   in Loop: Header=BB24_51 Depth=1
	movzwl	213992(%r13,%rcx,2), %eax
	shrq	$2, %rax
	andl	$16380, %eax            # imm = 0x3FFC
	movzwl	214064(%rsi,%r14,2), %edx
	shrq	$2, %rdx
	andl	$16380, %edx            # imm = 0x3FFC
	movl	207676(%r13,%rax), %eax
	addl	%r8d, %eax
	addl	207676(%r13,%rdx), %eax
	cmpl	%ebp, %eax
	movl	$1, %r12d
	jae	.LBB24_256
# BB#254:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%eax, 2924(%r13)
	movl	$0, 2952(%r13)
	movl	$0, 2932(%r13)
	xorl	%ebx, %ebx
	jmp	.LBB24_256
.LBB24_255:                             #   in Loop: Header=BB24_51 Depth=1
	movl	$1, %r12d
.LBB24_256:                             #   in Loop: Header=BB24_51 Depth=1
	movq	160(%rsp), %rcx         # 8-byte Reload
	cmpl	%ecx, %r15d
	cmovael	%r15d, %ecx
	cmpl	$2, %ecx
	jb	.LBB24_468
# BB#257:                               # %.preheader1035.preheader.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	$0, 2948(%r13)
	movdqa	208(%rsp), %xmm0
	movq	344(%rsp), %rax         # 8-byte Reload
	movdqu	%xmm0, (%rax)
	movl	%ecx, %edx
	leal	3(%rcx), %edi
	movq	%rcx, %r9
	leal	-2(%rcx), %eax
	andl	$3, %edi
	je	.LBB24_260
# BB#258:                               # %.preheader1035.i.prol.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	leaq	(%rdx,%rdx,2), %rbp
	shlq	$4, %rbp
	addq	304(%rsp), %rbp         # 8-byte Folded Reload
	negl	%edi
.LBB24_259:                             # %.preheader1035.i.prol
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1073741824, (%rbp)     # imm = 0x40000000
	decq	%rdx
	addq	$-48, %rbp
	incl	%edi
	jne	.LBB24_259
.LBB24_260:                             # %.preheader1035.i.prol.loopexit
                                        #   in Loop: Header=BB24_51 Depth=1
	cmpl	$3, %eax
	jb	.LBB24_263
# BB#261:                               # %.preheader1035.preheader.i.new
                                        #   in Loop: Header=BB24_51 Depth=1
	leaq	(%rdx,%rdx,2), %rax
	shlq	$4, %rax
	addq	304(%rsp), %rax         # 8-byte Folded Reload
.LBB24_262:                             # %.preheader1035.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1073741824, (%rax)     # imm = 0x40000000
	movl	$1073741824, -48(%rax)  # imm = 0x40000000
	movl	$1073741824, -96(%rax)  # imm = 0x40000000
	movl	$1073741824, -144(%rax) # imm = 0x40000000
	addq	$-192, %rax
	addl	$-4, %edx
	cmpl	$1, %edx
	ja	.LBB24_262
.LBB24_263:                             # %.preheader1034.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%r10d, %edx
	xorl	%edi, %edi
	cmpl	$2, %edx
	jae	.LBB24_266
	jmp	.LBB24_264
.LBB24_265:                             # %.loopexit1033..preheader1034_crit_edge.i
                                        #   in Loop: Header=BB24_264 Depth=2
	movl	116(%rsp,%rdi,4), %edx
	movq	%rax, %rdi
	cmpl	$2, %edx
	jb	.LBB24_264
.LBB24_266:                             #   in Loop: Header=BB24_51 Depth=1
	movl	210408(%r13), %eax
	movzwl	213992(%r13,%rax,2), %ebp
	shrl	$4, %ebp
	testq	%rdi, %rdi
	je	.LBB24_269
# BB#267:                               #   in Loop: Header=BB24_51 Depth=1
	xorl	$127, %ebp
	movl	207676(%r13,%rbp,4), %ebp
	movzwl	214016(%r13,%rax,2), %ebx
	shrl	$4, %ebx
	cmpq	$1, %rdi
	jne	.LBB24_270
# BB#268:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%ebx, %eax
	jmp	.LBB24_272
.LBB24_269:                             #   in Loop: Header=BB24_51 Depth=1
	movl	%ebp, %ebp
	movl	207676(%r13,%rbp,4), %ebp
	shlq	$5, %rax
	addq	%r13, %rax
	movzwl	214064(%rax,%r14,2), %eax
	xorq	$2032, %rax             # imm = 0x7F0
	jmp	.LBB24_271
.LBB24_270:                             #   in Loop: Header=BB24_51 Depth=1
	xorl	$127, %ebx
	addl	207676(%r13,%rbx,4), %ebp
	movzwl	214040(%r13,%rax,2), %ebx
	movl	$2, %eax
	subl	%edi, %eax
	andl	$2032, %eax             # imm = 0x7F0
	xorq	%rbx, %rax
.LBB24_271:                             # %GetPureRepPrice.exit980.i
                                        #   in Loop: Header=BB24_51 Depth=1
	shrq	$4, %rax
.LBB24_272:                             # %GetPureRepPrice.exit980.i
                                        #   in Loop: Header=BB24_51 Depth=1
	addl	%r8d, %ebp
	addl	207676(%r13,%rax,4), %ebp
	.p2align	4, 0x90
.LBB24_273:                             #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-2(%rdx), %eax
	imulq	$1088, %r14, %rbx       # imm = 0x440
	addq	%r13, %rbx
	movl	234752(%rbx,%rax,4), %eax
	addl	%ebp, %eax
	movl	%edx, %edx
	leaq	(%rdx,%rdx,2), %rbx
	shlq	$4, %rbx
	cmpl	2876(%r13,%rbx), %eax
	jae	.LBB24_275
# BB#274:                               #   in Loop: Header=BB24_273 Depth=2
	leaq	2876(%r13,%rbx), %rcx
	movl	%eax, (%rcx)
	movl	$0, 2900(%r13,%rbx)
	movl	%edi, 2904(%r13,%rbx)
	movl	$0, 2884(%r13,%rbx)
.LBB24_275:                             #   in Loop: Header=BB24_273 Depth=2
	decl	%edx
	cmpl	$1, %edx
	ja	.LBB24_273
.LBB24_264:                             # %.loopexit1033.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%rdi), %rax
	cmpq	$4, %rax
	jne	.LBB24_265
# BB#276:                               #   in Loop: Header=BB24_51 Depth=1
	leal	1(%r10), %r8d
	cmpl	$1, %r10d
	movl	$2, %eax
	cmovbel	%eax, %r8d
	cmpl	%r15d, %r8d
	movq	32(%rsp), %r10          # 8-byte Reload
	ja	.LBB24_287
# BB#277:                               # %.preheader1032.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210408(%r13), %eax
	movzwl	213968(%r13,%rax,2), %eax
	shrq	$2, %rax
	andl	$16380, %eax            # imm = 0x3FFC
	addl	207676(%r13,%rax), %r11d
	movl	$-2, %edx
.LBB24_278:                             # %.preheader1032.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	$2, %edx
	cmpl	208188(%r13,%rdx,4), %r8d
	ja	.LBB24_278
	jmp	.LBB24_280
.LBB24_279:                             #   in Loop: Header=BB24_280 Depth=2
	incl	%r8d
.LBB24_280:                             # %.preheader1031.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1(%rdx), %eax
	movl	208188(%r13,%rax,4), %edi
	leal	-2(%r8), %eax
	imulq	$1088, %r14, %rcx       # imm = 0x440
	addq	%r13, %rcx
	movl	216248(%rcx,%rax,4), %ebx
	addl	%r11d, %ebx
	cmpl	$5, %r8d
	movl	$3, %ecx
	cmovael	%ecx, %eax
	cmpq	$127, %rdi
	ja	.LBB24_282
# BB#281:                               #   in Loop: Header=BB24_280 Depth=2
	movl	%eax, %eax
	shlq	$9, %rax
	addq	%r13, %rax
	movl	211436(%rax,%rdi,4), %eax
	jmp	.LBB24_283
.LBB24_282:                             #   in Loop: Header=BB24_280 Depth=2
	movl	$524287, %esi           # imm = 0x7FFFF
	subl	%edi, %esi
	sarl	$31, %esi
	andl	$12, %esi
	leal	6(%rsi), %ecx
	movl	%edi, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	movzbl	199484(%r13,%rbp), %ecx
	leal	12(%rsi,%rsi), %esi
	addq	%rcx, %rsi
	movl	%edi, %ecx
	andl	$15, %ecx
	movl	%eax, %eax
	shlq	$8, %rax
	addq	%r13, %rax
	movl	210412(%rax,%rsi,4), %eax
	addl	213484(%r13,%rcx,4), %eax
.LBB24_283:                             #   in Loop: Header=BB24_280 Depth=2
	addl	%eax, %ebx
	movl	%r8d, %eax
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	cmpl	2876(%r13,%rax), %ebx
	jae	.LBB24_285
# BB#284:                               #   in Loop: Header=BB24_280 Depth=2
	leaq	2876(%r13,%rax), %rcx
	movl	%ebx, (%rcx)
	movl	$0, 2900(%r13,%rax)
	addl	$4, %edi
	movl	%edi, 2904(%r13,%rax)
	movl	$0, 2884(%r13,%rax)
.LBB24_285:                             #   in Loop: Header=BB24_280 Depth=2
	movl	%edx, %eax
	cmpl	208188(%r13,%rax,4), %r8d
	jne	.LBB24_279
# BB#286:                               #   in Loop: Header=BB24_280 Depth=2
	addl	$2, %edx
	cmpl	%r10d, %edx
	jne	.LBB24_279
.LBB24_287:                             # %.thread1024.outer.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	xorl	%r15d, %r15d
	movl	4(%rsp), %eax           # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	%r9, %r11
.LBB24_288:                             # %.thread1024.outer.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_289 Depth 3
                                        #         Child Loop BB24_294 Depth 4
                                        #         Child Loop BB24_315 Depth 4
                                        #         Child Loop BB24_325 Depth 4
                                        #         Child Loop BB24_323 Depth 4
                                        #       Child Loop BB24_340 Depth 3
                                        #       Child Loop BB24_346 Depth 3
                                        #       Child Loop BB24_349 Depth 3
                                        #       Child Loop BB24_357 Depth 3
                                        #         Child Loop BB24_361 Depth 4
                                        #         Child Loop BB24_366 Depth 4
                                        #         Child Loop BB24_369 Depth 4
                                        #         Child Loop BB24_379 Depth 4
                                        #         Child Loop BB24_384 Depth 4
                                        #         Child Loop BB24_388 Depth 4
                                        #         Child Loop BB24_392 Depth 4
                                        #         Child Loop BB24_398 Depth 4
                                        #       Child Loop BB24_407 Depth 3
                                        #       Child Loop BB24_413 Depth 3
                                        #       Child Loop BB24_416 Depth 3
                                        #       Child Loop BB24_419 Depth 3
                                        #       Child Loop BB24_422 Depth 3
                                        #         Child Loop BB24_423 Depth 4
                                        #           Child Loop BB24_429 Depth 5
                                        #           Child Loop BB24_425 Depth 5
                                        #           Child Loop BB24_434 Depth 5
                                        #           Child Loop BB24_438 Depth 5
                                        #           Child Loop BB24_442 Depth 5
                                        #           Child Loop BB24_447 Depth 5
                                        # kill: %R15D<def> %R15D<kill> %R15<kill> %R15<def>
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r15, %rdx
	movq	%rdx, 224(%rsp)         # 8-byte Spill
                                        # kill: %R15D<def> %R15D<kill> %R15<kill> %R15<def>
	movq	%r11, 160(%rsp)         # 8-byte Spill
.LBB24_289:                             # %.thread1024.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB24_294 Depth 4
                                        #         Child Loop BB24_315 Depth 4
                                        #         Child Loop BB24_325 Depth 4
                                        #         Child Loop BB24_323 Depth 4
	movq	144(%rsp), %rdx         # 8-byte Reload
	movl	%edx, %r14d
	movl	%r15d, %r12d
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	movl	%ecx, %ebx
	leal	1(%r12), %r15d
	cmpl	%r11d, %r15d
	je	.LBB24_455
# BB#290:                               #   in Loop: Header=BB24_289 Depth=3
	movb	%al, 80(%rsp)           # 1-byte Spill
	movq	48(%r13), %rdi
	callq	*16(%r13)
	movl	%eax, 2872(%r13)
	movq	48(%r13), %rdi
	movq	256(%rsp), %rsi         # 8-byte Reload
	callq	*32(%r13)
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	testl	%eax, %eax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	je	.LBB24_296
# BB#291:                               #   in Loop: Header=BB24_289 Depth=3
	leal	-2(%rax), %eax
	movl	208188(%r13,%rax,4), %ebp
	cmpl	210384(%r13), %ebp
	jne	.LBB24_297
# BB#292:                               #   in Loop: Header=BB24_289 Depth=3
	movq	48(%r13), %rdi
	callq	*24(%r13)
	movl	2872(%r13), %ecx
	cmpl	$273, %ecx              # imm = 0x111
	movl	$273, %edx              # imm = 0x111
	cmovael	%edx, %ecx
	cmpl	%ecx, %ebp
	jae	.LBB24_297
# BB#293:                               # %.lr.ph.i984.i.preheader
                                        #   in Loop: Header=BB24_289 Depth=3
	decq	%rax
	movq	248(%rsp), %rdx         # 8-byte Reload
	leal	-1(%rdx), %edx
	movl	208188(%r13,%rdx,4), %esi
	incl	%esi
	movq	%rax, %rdx
	subq	%rsi, %rdx
.LBB24_294:                             # %.lr.ph.i984.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_289 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%ebp, %esi
	movzbl	(%rax,%rsi), %ebx
	cmpb	(%rdx,%rsi), %bl
	jne	.LBB24_297
# BB#295:                               #   in Loop: Header=BB24_294 Depth=4
	incl	%ebp
	cmpl	%ecx, %ebp
	jb	.LBB24_294
	jmp	.LBB24_297
.LBB24_296:                             #   in Loop: Header=BB24_289 Depth=3
	xorl	%ebp, %ebp
.LBB24_297:                             # %ReadMatchDistances.exit986.i
                                        #   in Loop: Header=BB24_289 Depth=3
	incl	210388(%r13)
	cmpl	210384(%r13), %ebp
	jae	.LBB24_461
# BB#298:                               #   in Loop: Header=BB24_289 Depth=3
	movl	%r15d, %eax
	leaq	(%rax,%rax,2), %rbx
	shlq	$4, %rbx
	movl	2884(%r13,%rbx), %ecx
	movl	2900(%r13,%rbx), %eax
	testl	%ecx, %ecx
	je	.LBB24_302
# BB#299:                               #   in Loop: Header=BB24_289 Depth=3
	decl	%eax
	cmpl	$0, 2888(%r13,%rbx)
	je	.LBB24_303
# BB#300:                               #   in Loop: Header=BB24_289 Depth=3
	movl	2892(%r13,%rbx), %edx
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$4, %rdx
	movl	2880(%r13,%rdx), %edx
	cmpl	$3, 2896(%r13,%rbx)
	ja	.LBB24_304
# BB#301:                               #   in Loop: Header=BB24_289 Depth=3
	leaq	kRepNextStates(,%rdx,4), %rdx
	jmp	.LBB24_305
.LBB24_302:                             #   in Loop: Header=BB24_289 Depth=3
	leaq	(%rax,%rax,2), %rdx
	shlq	$4, %rdx
	leaq	2880(%r13,%rdx), %rdx
	jmp	.LBB24_306
.LBB24_303:                             #   in Loop: Header=BB24_289 Depth=3
	movl	%eax, %edx
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$4, %rdx
	leaq	2880(%r13,%rdx), %rdx
	jmp	.LBB24_305
.LBB24_304:                             #   in Loop: Header=BB24_289 Depth=3
	leaq	kMatchNextStates(,%rdx,4), %rdx
.LBB24_305:                             #   in Loop: Header=BB24_289 Depth=3
	movl	(%rdx), %edx
	leaq	kLiteralNextStates(,%rdx,4), %rdx
.LBB24_306:                             #   in Loop: Header=BB24_289 Depth=3
	leal	1(%r14), %esi
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	movl	(%rdx), %edx
	cmpl	%r12d, %eax
	movq	%r14, 168(%rsp)         # 8-byte Spill
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%rbp, 240(%rsp)         # 8-byte Spill
	jne	.LBB24_309
# BB#307:                               #   in Loop: Header=BB24_289 Depth=3
	cmpl	$0, 2904(%r13,%rbx)
	je	.LBB24_318
# BB#308:                               #   in Loop: Header=BB24_289 Depth=3
	movl	kLiteralNextStates(,%rdx,4), %r12d
	jmp	.LBB24_321
.LBB24_309:                             #   in Loop: Header=BB24_289 Depth=3
	testl	%ecx, %ecx
	je	.LBB24_312
# BB#310:                               #   in Loop: Header=BB24_289 Depth=3
	cmpl	$0, 2888(%r13,%rbx)
	je	.LBB24_312
# BB#311:                               #   in Loop: Header=BB24_289 Depth=3
	movl	2892(%r13,%rbx), %eax
	movl	2896(%r13,%rbx), %ecx
	movl	kRepNextStates(,%rdx,4), %r12d
	cmpl	$3, %ecx
	jbe	.LBB24_314
	jmp	.LBB24_320
.LBB24_312:                             #   in Loop: Header=BB24_289 Depth=3
	movl	2904(%r13,%rbx), %ecx
	cmpl	$3, %ecx
	ja	.LBB24_319
# BB#313:                               # %.thread.i259
                                        #   in Loop: Header=BB24_289 Depth=3
	movl	kRepNextStates(,%rdx,4), %r12d
	movl	%eax, %eax
.LBB24_314:                             #   in Loop: Header=BB24_289 Depth=3
	movl	%ecx, %edx
	leaq	(%rax,%rax,2), %rax
	movq	%rax, %rsi
	shlq	$4, %rsi
	addq	%r13, %rsi
	movl	2908(%rsi,%rdx,4), %edx
	movl	%edx, 208(%rsp)
	movl	$1, %edx
	testl	%ecx, %ecx
	je	.LBB24_317
	.p2align	4, 0x90
.LBB24_315:                             # %.lr.ph.i260
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_289 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	-1(%rdx), %edi
	movl	2908(%rsi,%rdi,4), %edi
	movl	%edx, %ebp
	movl	%edi, 208(%rsp,%rbp,4)
	incl	%edx
	cmpl	%ecx, %edx
	jbe	.LBB24_315
# BB#316:                               # %.preheader1029.i
                                        #   in Loop: Header=BB24_289 Depth=3
	cmpl	$3, %edx
	ja	.LBB24_321
.LBB24_317:                             # %.lr.ph1067.preheader.i
                                        #   in Loop: Header=BB24_289 Depth=3
	movl	%edx, %ecx
	leaq	208(%rsp,%rcx,4), %rdi
	leaq	(%rcx,%rax,4), %rax
	leaq	2908(%r13,%rax,4), %rsi
	movl	$3, %eax
	subl	%edx, %eax
	leaq	4(,%rax,4), %rdx
	callq	memcpy
	jmp	.LBB24_321
.LBB24_318:                             #   in Loop: Header=BB24_289 Depth=3
	movl	kShortRepNextStates(,%rdx,4), %r12d
	jmp	.LBB24_321
.LBB24_319:                             # %.thread1010.i
                                        #   in Loop: Header=BB24_289 Depth=3
	movl	kMatchNextStates(,%rdx,4), %r12d
	movl	%eax, %eax
.LBB24_320:                             # %.loopexit.loopexit1126.i
                                        #   in Loop: Header=BB24_289 Depth=3
	addl	$-4, %ecx
	movl	%ecx, 208(%rsp)
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	movl	2916(%r13,%rax), %ecx
	leaq	212(%rsp), %rdx
	movl	%ecx, 8(%rdx)
	movq	2908(%r13,%rax), %rax
	movq	%rax, (%rdx)
.LBB24_321:                             # %.loopexit.i
                                        #   in Loop: Header=BB24_289 Depth=3
	movl	%r12d, 2880(%r13,%rbx)
	movaps	208(%rsp), %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movups	%xmm0, 2908(%r13,%rbx)
	movl	2876(%r13,%rbx), %ebx
	movq	48(%r13), %rdi
	callq	*24(%r13)
	movl	%ebx, %r10d
	movq	%rax, %rbp
	leaq	-1(%rbp), %rcx
	movdqa	32(%rsp), %xmm0         # 16-byte Reload
	movd	%xmm0, %r14d
	leal	1(%r14), %eax
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	subq	%rax, %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movzbl	(%rcx), %r11d
	movl	213572(%r13), %ecx
	movq	144(%rsp), %rax         # 8-byte Reload
	andl	%eax, %ecx
	movl	%r12d, %esi
	movq	%rsi, %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	shlq	$5, %rsi
	addq	%r13, %rsi
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movzwl	213584(%rsi,%rcx,2), %r9d
	movl	213568(%r13), %esi
	andl	%eax, %esi
	movl	213556(%r13), %ecx
	shll	%cl, %esi
	movl	$8, %edx
	subl	%ecx, %edx
	movzbl	-2(%rbp), %edi
	movl	%edx, %ecx
	shrl	%cl, %edi
	addl	%esi, %edi
	movq	%r9, %rcx
	shrq	$2, %rcx
	andl	$16380, %ecx            # imm = 0x3FFC
	movl	207676(%r13,%rcx), %r8d
	addl	%r10d, %r8d
	shll	$8, %edi
	leal	(%rdi,%rdi,2), %edx
	addq	%rdx, %rdx
	addq	213576(%r13), %rdx
	cmpl	$7, %r12d
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movzbl	-1(%rbp), %eax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movl	%r11d, 72(%rsp)         # 4-byte Spill
	leal	256(%rax), %ecx
	jae	.LBB24_324
# BB#322:                               #   in Loop: Header=BB24_289 Depth=3
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB24_323:                             #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_289 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%ecx, %eax
	shrl	$8, %eax
	movzwl	(%rdx,%rax,2), %eax
	movl	%ecx, %esi
	shrl	$7, %esi
	andl	$1, %esi
	negl	%esi
	andl	$2032, %esi             # imm = 0x7F0
	xorl	%eax, %esi
	shrl	$4, %esi
	addl	207676(%r13,%rsi,4), %ebx
	addl	%ecx, %ecx
	cmpl	$65536, %ecx            # imm = 0x10000
	jb	.LBB24_323
	jmp	.LBB24_326
.LBB24_324:                             #   in Loop: Header=BB24_289 Depth=3
	movl	$256, %r12d             # imm = 0x100
	xorl	%ebx, %ebx
	movl	%r11d, %ebp
	.p2align	4, 0x90
.LBB24_325:                             #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_289 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%rbp,%rbp), %esi
	movl	%r12d, %edi
	andl	%esi, %edi
	movl	%ecx, %r11d
	shrl	$8, %r11d
	addl	%r12d, %r11d
	addl	%edi, %r11d
	movzwl	(%rdx,%r11,2), %edi
	movl	%ecx, %eax
	shrl	$7, %eax
	andl	$1, %eax
	negl	%eax
	andl	$2032, %eax             # imm = 0x7F0
	xorl	%edi, %eax
	shrl	$4, %eax
	addl	207676(%r13,%rax,4), %ebx
	xorl	%ecx, %ebp
	addl	%ebp, %ebp
	addl	%ecx, %ecx
	notl	%ebp
	andl	%ebp, %r12d
	cmpl	$65536, %ecx            # imm = 0x10000
	movl	%esi, %ebp
	jb	.LBB24_325
.LBB24_326:                             # %LitEnc_GetPriceMatched.exit977.i
                                        #   in Loop: Header=BB24_289 Depth=3
	addl	%ebx, %r8d
	movq	56(%rsp), %rax          # 8-byte Reload
	leal	2(%rax), %eax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	leaq	(%rax,%rax,2), %rdx
	shlq	$4, %rdx
	leaq	2876(%r13,%rdx), %rbx
	movl	2876(%r13,%rdx), %ecx
	cmpl	%ecx, %r8d
	jae	.LBB24_328
# BB#327:                               #   in Loop: Header=BB24_289 Depth=3
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	213584(%rax,%r12,2), %rax
	movl	%r8d, (%rbx)
	movl	%r15d, 2900(%r13,%rdx)
	movl	$-1, 2904(%r13,%rdx)
	movl	$0, 2884(%r13,%rdx)
	movw	(%rax), %r9w
	movl	$1, %esi
	movl	%r8d, %ecx
	movq	160(%rsp), %r11         # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB24_329
.LBB24_328:                             #   in Loop: Header=BB24_289 Depth=3
	xorl	%esi, %esi
	movq	160(%rsp), %r11         # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB24_329:                             #   in Loop: Header=BB24_289 Depth=3
	movq	64(%rsp), %rdi          # 8-byte Reload
	xorl	$2032, %r9d             # imm = 0x7F0
	shrl	$2, %r9d
	andl	$16380, %r9d            # imm = 0x3FFC
	addl	207676(%r13,%r9), %r10d
	movzwl	213968(%r13,%rdi,2), %eax
	xorl	$2032, %eax             # imm = 0x7F0
	shrl	$2, %eax
	andl	$16380, %eax            # imm = 0x3FFC
	movl	207676(%r13,%rax), %eax
	addl	%r10d, %eax
	movl	%eax, 156(%rsp)         # 4-byte Spill
	movq	200(%rsp), %rax         # 8-byte Reload
	cmpb	%al, 72(%rsp)           # 1-byte Folded Reload
	jne	.LBB24_334
# BB#330:                               #   in Loop: Header=BB24_289 Depth=3
	cmpl	%r15d, 2900(%r13,%rdx)
	movb	80(%rsp), %r9b          # 1-byte Reload
	jae	.LBB24_332
# BB#331:                               #   in Loop: Header=BB24_289 Depth=3
	cmpl	$0, 2904(%r13,%rdx)
	je	.LBB24_335
.LBB24_332:                             #   in Loop: Header=BB24_289 Depth=3
	movzwl	213992(%r13,%rdi,2), %eax
	shrq	$2, %rax
	andl	$16380, %eax            # imm = 0x3FFC
	movq	104(%rsp), %rdi         # 8-byte Reload
	movzwl	214064(%rdi,%r12,2), %edi
	shrq	$2, %rdi
	andl	$16380, %edi            # imm = 0x3FFC
	movl	207676(%r13,%rax), %eax
	addl	156(%rsp), %eax         # 4-byte Folded Reload
	addl	207676(%r13,%rdi), %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	cmpl	%ecx, %eax
	ja	.LBB24_335
# BB#333:                               #   in Loop: Header=BB24_289 Depth=3
	leaq	2900(%r13,%rdx), %rcx
	movl	%eax, (%rbx)
	movl	%r15d, (%rcx)
	movl	$0, 2904(%r13,%rdx)
	movl	$0, 2884(%r13,%rdx)
	movl	$1, %esi
	jmp	.LBB24_335
.LBB24_334:                             #   in Loop: Header=BB24_289 Depth=3
	movb	80(%rsp), %r9b          # 1-byte Reload
.LBB24_335:                             #   in Loop: Header=BB24_289 Depth=3
	movl	2872(%r13), %eax
	movl	$4094, %edx             # imm = 0xFFE
	subl	56(%rsp), %edx          # 4-byte Folded Reload
	cmpl	%eax, %edx
	cmovael	%eax, %edx
	movb	%r9b, %al
	incb	%al
	movq	136(%rsp), %rbx         # 8-byte Reload
	leal	1(%rbx), %ecx
	cmpl	$2, %edx
	jb	.LBB24_289
# BB#336:                               #   in Loop: Header=BB24_288 Depth=2
	movl	210384(%r13), %ecx
	cmpl	%ecx, %edx
	movl	%edx, %eax
	cmoval	%ecx, %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movq	200(%rsp), %rax         # 8-byte Reload
	cmpb	%al, 72(%rsp)           # 1-byte Folded Reload
	movl	%edx, 200(%rsp)         # 4-byte Spill
	je	.LBB24_355
# BB#337:                               #   in Loop: Header=BB24_288 Depth=2
	testl	%esi, %esi
	jne	.LBB24_355
# BB#338:                               #   in Loop: Header=BB24_288 Depth=2
	incl	%ecx
	cmpl	%edx, %ecx
	cmoval	%edx, %ecx
	cmpl	$2, %ecx
	jb	.LBB24_355
# BB#339:                               # %.lr.ph1069.i.preheader
                                        #   in Loop: Header=BB24_288 Depth=2
	movl	$1, %eax
	movq	176(%rsp), %rsi         # 8-byte Reload
.LBB24_340:                             # %.lr.ph1069.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, %edx
	movzbl	(%rbp,%rdx), %ebx
	cmpb	(%rsi,%rdx), %bl
	jne	.LBB24_342
# BB#341:                               #   in Loop: Header=BB24_340 Depth=3
	incl	%eax
	cmpl	%ecx, %eax
	jb	.LBB24_340
.LBB24_342:                             # %.critedge3.i
                                        #   in Loop: Header=BB24_288 Depth=2
	leal	-1(%rax), %ebx
	cmpl	$2, %ebx
	jb	.LBB24_355
# BB#343:                               #   in Loop: Header=BB24_288 Depth=2
	movq	168(%rsp), %rcx         # 8-byte Reload
	addl	$2, %ecx
	andl	213572(%r13), %ecx
	movl	kLiteralNextStates(,%rdi,4), %esi
	movq	%rsi, %rdx
	shlq	$5, %rdx
	addq	%r13, %rdx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movzwl	213584(%rdx,%rcx,2), %ecx
	xorl	$2032, %ecx             # imm = 0x7F0
	shrl	$2, %ecx
	andl	$16380, %ecx            # imm = 0x3FFC
	movl	207676(%r13,%rcx), %edi
	movzwl	213968(%r13,%rsi,2), %ecx
	xorl	$2032, %ecx             # imm = 0x7F0
	shrl	$2, %ecx
	andl	$16380, %ecx            # imm = 0x3FFC
	movl	207676(%r13,%rcx), %ecx
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	addl	232(%rsp), %ebx         # 4-byte Folded Reload
	cmpl	%ebx, %r11d
	jae	.LBB24_351
# BB#344:                               # %.lr.ph1074.preheader.i
                                        #   in Loop: Header=BB24_288 Depth=2
	movq	%rsi, 176(%rsp)         # 8-byte Spill
	movl	%r11d, %ecx
	movl	%ebx, %esi
	movl	%ebx, %ebp
	subl	%r11d, %ebp
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	leaq	-1(%rsi), %r12
	subq	%rcx, %r12
	testb	$7, %bpl
	je	.LBB24_347
# BB#345:                               # %.lr.ph1074.i.prol.preheader
                                        #   in Loop: Header=BB24_288 Depth=2
	leaq	(%rcx,%rcx,2), %rbp
	shlq	$4, %rbp
	addq	192(%rsp), %rbp         # 8-byte Folded Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	leal	1(%rax,%rsi), %esi
	subb	%r11b, %sil
	addb	80(%rsp), %sil          # 1-byte Folded Reload
	movzbl	%sil, %r11d
	andl	$7, %r11d
	negq	%r11
.LBB24_346:                             # %.lr.ph1074.i.prol
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incq	%rcx
	movl	$1073741824, (%rbp)     # imm = 0x40000000
	addq	$48, %rbp
	incq	%r11
	jne	.LBB24_346
.LBB24_347:                             # %.lr.ph1074.i.prol.loopexit
                                        #   in Loop: Header=BB24_288 Depth=2
	cmpq	$7, %r12
	jb	.LBB24_350
# BB#348:                               # %.lr.ph1074.preheader.i.new
                                        #   in Loop: Header=BB24_288 Depth=2
	movq	224(%rsp), %rsi         # 8-byte Reload
	leal	(%rax,%rsi), %esi
	movq	136(%rsp), %rbp         # 8-byte Reload
	leal	1(%rbp,%rsi), %ebp
	subq	%rcx, %rbp
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$4, %rcx
	addq	184(%rsp), %rcx         # 8-byte Folded Reload
.LBB24_349:                             # %.lr.ph1074.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$1073741824, -336(%rcx) # imm = 0x40000000
	movl	$1073741824, -288(%rcx) # imm = 0x40000000
	movl	$1073741824, -240(%rcx) # imm = 0x40000000
	movl	$1073741824, -192(%rcx) # imm = 0x40000000
	movl	$1073741824, -144(%rcx) # imm = 0x40000000
	movl	$1073741824, -96(%rcx)  # imm = 0x40000000
	movl	$1073741824, -48(%rcx)  # imm = 0x40000000
	movl	$1073741824, (%rcx)     # imm = 0x40000000
	addq	$384, %rcx              # imm = 0x180
	addq	$-8, %rbp
	jne	.LBB24_349
.LBB24_350:                             #   in Loop: Header=BB24_288 Depth=2
	movl	%ebx, %r11d
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	movq	176(%rsp), %rsi         # 8-byte Reload
	jmp	.LBB24_352
.LBB24_351:                             # %.._crit_edge_crit_edge.i
                                        #   in Loop: Header=BB24_288 Depth=2
	movl	%ebx, %r9d
.LBB24_352:                             # %._crit_edge.i263
                                        #   in Loop: Header=BB24_288 Depth=2
	addl	$-3, %eax
	movq	168(%rsp), %rbx         # 8-byte Reload
	imulq	$1088, %rbx, %rcx       # imm = 0x440
	addq	%r13, %rcx
	movzwl	213992(%r13,%rsi,2), %esi
	shrq	$2, %rsi
	andl	$16380, %esi            # imm = 0x3FFC
	movzwl	214064(%rdx,%rbx,2), %edx
	xorl	$2032, %edx             # imm = 0x7F0
	shrl	$2, %edx
	andl	$16380, %edx            # imm = 0x3FFC
	addl	%r8d, %edi
	addl	56(%rsp), %edi          # 4-byte Folded Reload
	addl	234752(%rcx,%rax,4), %edi
	addl	207676(%r13,%rsi), %edi
	addl	207676(%r13,%rdx), %edi
	leaq	(%r9,%r9,2), %rax
	shlq	$4, %rax
	cmpl	2876(%r13,%rax), %edi
	jae	.LBB24_354
# BB#353:                               #   in Loop: Header=BB24_288 Depth=2
	leaq	2876(%r13,%rax), %rcx
	movl	%edi, (%rcx)
	movq	232(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, 2900(%r13,%rax)
	movl	$0, 2904(%r13,%rax)
	movq	$1, 2884(%r13,%rax)
.LBB24_354:                             # %.preheader1027.i
                                        #   in Loop: Header=BB24_288 Depth=2
	movq	64(%rsp), %rdi          # 8-byte Reload
	movb	80(%rsp), %r9b          # 1-byte Reload
.LBB24_355:                             # %.preheader1027.i
                                        #   in Loop: Header=BB24_288 Depth=2
	movq	96(%rsp), %r8           # 8-byte Reload
	movl	28(%rsp), %edx          # 4-byte Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	214064(%rax,%r12,2), %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	movq	224(%rsp), %rax         # 8-byte Reload
	leal	1(%rax), %eax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movl	$2, 72(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%r10d, 92(%rsp)         # 4-byte Spill
	jmp	.LBB24_357
.LBB24_356:                             # %._crit_edge1213.i
                                        #   in Loop: Header=BB24_357 Depth=3
	movl	212(%rsp,%rcx,4), %r14d
	movq	%rax, 56(%rsp)          # 8-byte Spill
.LBB24_357:                             #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB24_361 Depth 4
                                        #         Child Loop BB24_366 Depth 4
                                        #         Child Loop BB24_369 Depth 4
                                        #         Child Loop BB24_379 Depth 4
                                        #         Child Loop BB24_384 Depth 4
                                        #         Child Loop BB24_388 Depth 4
                                        #         Child Loop BB24_392 Depth 4
                                        #         Child Loop BB24_398 Depth 4
	incl	%r14d
	movq	%rbp, %r12
	subq	%r14, %r12
	movb	(%rbp), %al
	cmpb	(%r12), %al
	jne	.LBB24_374
# BB#358:                               #   in Loop: Header=BB24_357 Depth=3
	movb	(%r8), %al
	cmpb	1(%r12), %al
	jne	.LBB24_374
# BB#359:                               # %.preheader.i
                                        #   in Loop: Header=BB24_357 Depth=3
	movl	$2, %ebx
	cmpl	$3, %edx
	jb	.LBB24_363
# BB#360:                               # %.lr.ph1077.i.preheader
                                        #   in Loop: Header=BB24_357 Depth=3
	movl	$2, %ebx
.LBB24_361:                             # %.lr.ph1077.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_357 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%ebx, %eax
	movzbl	(%rbp,%rax), %ecx
	cmpb	(%r12,%rax), %cl
	jne	.LBB24_363
# BB#362:                               #   in Loop: Header=BB24_361 Depth=4
	incl	%ebx
	cmpl	%edx, %ebx
	jb	.LBB24_361
.LBB24_363:                             # %.critedge4.preheader.i
                                        #   in Loop: Header=BB24_357 Depth=3
	leal	(%rbx,%r15), %r14d
	cmpl	%r14d, %r11d
	jae	.LBB24_371
# BB#364:                               # %.critedge4.preheader1124.i
                                        #   in Loop: Header=BB24_357 Depth=3
	movl	%r11d, %eax
	movl	%r14d, %ecx
	movl	%r14d, %edx
	subl	%r11d, %edx
	decq	%rcx
	subq	%rax, %rcx
	testb	$7, %dl
	je	.LBB24_367
# BB#365:                               # %.critedge4.i.prol.preheader
                                        #   in Loop: Header=BB24_357 Depth=3
	leaq	(%rax,%rax,2), %rdx
	shlq	$4, %rdx
	addq	192(%rsp), %rdx         # 8-byte Folded Reload
	movq	272(%rsp), %rsi         # 8-byte Reload
	leal	(%rsi,%rbx), %esi
	subb	%r11b, %sil
	addb	%r9b, %sil
	movzbl	%sil, %esi
	andl	$7, %esi
	negq	%rsi
.LBB24_366:                             # %.critedge4.i.prol
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_357 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incq	%rax
	movl	$1073741824, (%rdx)     # imm = 0x40000000
	addq	$48, %rdx
	incq	%rsi
	jne	.LBB24_366
.LBB24_367:                             # %.critedge4.i.prol.loopexit
                                        #   in Loop: Header=BB24_357 Depth=3
	cmpq	$7, %rcx
	movl	%r14d, %r11d
	jb	.LBB24_371
# BB#368:                               # %.critedge4.preheader1124.i.new
                                        #   in Loop: Header=BB24_357 Depth=3
	movq	272(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%rbx), %ecx
	addl	136(%rsp), %ecx         # 4-byte Folded Reload
	subq	%rax, %rcx
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	addq	184(%rsp), %rax         # 8-byte Folded Reload
.LBB24_369:                             # %.critedge4.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_357 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	$1073741824, -336(%rax) # imm = 0x40000000
	movl	$1073741824, -288(%rax) # imm = 0x40000000
	movl	$1073741824, -240(%rax) # imm = 0x40000000
	movl	$1073741824, -192(%rax) # imm = 0x40000000
	movl	$1073741824, -144(%rax) # imm = 0x40000000
	movl	$1073741824, -96(%rax)  # imm = 0x40000000
	movl	$1073741824, -48(%rax)  # imm = 0x40000000
	movl	$1073741824, (%rax)     # imm = 0x40000000
	addq	$384, %rax              # imm = 0x180
	addq	$-8, %rcx
	jne	.LBB24_369
# BB#370:                               #   in Loop: Header=BB24_357 Depth=3
	movl	%r14d, %r11d
.LBB24_371:                             # %.critedge4._crit_edge.i
                                        #   in Loop: Header=BB24_357 Depth=3
	movzwl	213992(%r13,%rdi,2), %eax
	shrl	$4, %eax
	movq	56(%rsp), %rdx          # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB24_375
# BB#372:                               #   in Loop: Header=BB24_357 Depth=3
	xorl	$127, %eax
	movl	207676(%r13,%rax,4), %r9d
	movzwl	214016(%r13,%rdi,2), %eax
	shrl	$4, %eax
	cmpq	$1, %rdx
	jne	.LBB24_376
# BB#373:                               #   in Loop: Header=BB24_357 Depth=3
	movl	%eax, %eax
	jmp	.LBB24_378
.LBB24_374:                             #   in Loop: Header=BB24_357 Depth=3
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB24_404:                             #   in Loop: Header=BB24_357 Depth=3
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx), %rax
	cmpq	$4, %rax
	jne	.LBB24_356
	jmp	.LBB24_405
.LBB24_375:                             #   in Loop: Header=BB24_357 Depth=3
	movl	%eax, %eax
	movl	207676(%r13,%rax,4), %r9d
	movq	328(%rsp), %rax         # 8-byte Reload
	movzwl	(%rax), %eax
	xorq	$2032, %rax             # imm = 0x7F0
	jmp	.LBB24_377
.LBB24_376:                             #   in Loop: Header=BB24_357 Depth=3
	xorl	$127, %eax
	addl	207676(%r13,%rax,4), %r9d
	movzwl	214040(%r13,%rdi,2), %ecx
	movl	$2, %eax
	subl	%edx, %eax
	andl	$2032, %eax             # imm = 0x7F0
	xorq	%rcx, %rax
.LBB24_377:                             # %GetPureRepPrice.exit.i
                                        #   in Loop: Header=BB24_357 Depth=3
	shrq	$4, %rax
.LBB24_378:                             # %GetPureRepPrice.exit.i
                                        #   in Loop: Header=BB24_357 Depth=3
	movq	%rdx, %rdi
	addl	156(%rsp), %r9d         # 4-byte Folded Reload
	addl	207676(%r13,%rax,4), %r9d
	movl	%ebx, %eax
	.p2align	4, 0x90
.LBB24_379:                             #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_357 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	-2(%rax), %ecx
	imulq	$1088, 32(%rsp), %rdx   # 8-byte Folded Reload
                                        # imm = 0x440
	addq	%r13, %rdx
	movl	234752(%rdx,%rcx,4), %ecx
	addl	%r9d, %ecx
	leal	(%r15,%rax), %ebp
	leaq	(%rbp,%rbp,2), %rbp
	shlq	$4, %rbp
	cmpl	2876(%r13,%rbp), %ecx
	jae	.LBB24_381
# BB#380:                               #   in Loop: Header=BB24_379 Depth=4
	leaq	2876(%r13,%rbp), %rsi
	movl	%ecx, (%rsi)
	movl	%r15d, 2900(%r13,%rbp)
	movl	%edi, 2904(%r13,%rbp)
	movl	$0, 2884(%r13,%rbp)
.LBB24_381:                             #   in Loop: Header=BB24_379 Depth=4
	decl	%eax
	cmpl	$1, %eax
	ja	.LBB24_379
# BB#382:                               #   in Loop: Header=BB24_357 Depth=3
	movq	%rdi, %rax
	leal	1(%rbx), %edi
	testq	%rax, %rax
	movl	72(%rsp), %eax          # 4-byte Reload
	cmovel	%edi, %eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	210384(%r13), %ebp
	addl	%edi, %ebp
	movl	200(%rsp), %eax         # 4-byte Reload
	cmpl	%eax, %ebp
	cmoval	%eax, %ebp
	cmpl	%ebp, %edi
	movl	%edi, %eax
	jae	.LBB24_386
# BB#383:                               # %.lr.ph1087.i.preheader
                                        #   in Loop: Header=BB24_357 Depth=3
	movl	%edi, %eax
.LBB24_384:                             # %.lr.ph1087.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_357 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%eax, %esi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzbl	(%rcx,%rsi), %ecx
	cmpb	(%r12,%rsi), %cl
	jne	.LBB24_386
# BB#385:                               #   in Loop: Header=BB24_384 Depth=4
	incl	%eax
	cmpl	%ebp, %eax
	jb	.LBB24_384
.LBB24_386:                             # %.critedge5.i
                                        #   in Loop: Header=BB24_357 Depth=3
	movl	%eax, %ecx
	subl	%edi, %ecx
	cmpl	$2, %ecx
	jb	.LBB24_395
# BB#387:                               #   in Loop: Header=BB24_357 Depth=3
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movq	%r11, 160(%rsp)         # 8-byte Spill
	movq	144(%rsp), %rcx         # 8-byte Reload
	leal	(%rbx,%rcx), %esi
	movl	213556(%r13), %ecx
	movl	213572(%r13), %edi
	movq	%rdi, 104(%rsp)         # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	andl	%esi, %edi
	leal	-2(%rbx), %ebp
	movl	234752(%rdx,%rbp,4), %edx
	movl	%edx, 176(%rsp)         # 4-byte Spill
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	kRepNextStates(,%rdx,4), %edx
	movq	%rdx, %rbp
	movq	%rbp, 280(%rsp)         # 8-byte Spill
	shlq	$5, %rdx
	addq	%r13, %rdx
	movzwl	213584(%rdx,%rdi,2), %edx
	shrq	$2, %rdx
	andl	$16380, %edx            # imm = 0x3FFC
	movl	207676(%r13,%rdx), %edx
	movl	%edx, 232(%rsp)         # 4-byte Spill
	movl	213568(%r13), %edi
	movl	%esi, %r10d
	andl	%esi, %edi
	shll	%cl, %edi
	leal	-1(%rbx), %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movzbl	(%rsi,%rdx), %ebp
	movl	$8, %edx
	subl	%ecx, %edx
	movl	%edx, %ecx
	shrl	%cl, %ebp
	addl	%edi, %ebp
	shll	$8, %ebp
	leal	(%rbp,%rbp,2), %r11d
	addq	%r11, %r11
	addq	213576(%r13), %r11
	movl	%ebx, %ecx
	movzbl	(%rsi,%rcx), %ebx
	movzbl	(%r12,%rcx), %edi
	orl	$256, %ebx              # imm = 0x100
	movl	$256, %esi              # imm = 0x100
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB24_388:                             #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_357 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%rdi,%rdi), %r12d
	movl	%esi, %ebp
	andl	%r12d, %ebp
	movl	%ebx, %r8d
	shrl	$8, %r8d
	addl	%esi, %r8d
	addl	%ebp, %r8d
	movzwl	(%r11,%r8,2), %ebp
	movl	%ebx, %edx
	shrl	$7, %edx
	andl	$1, %edx
	negl	%edx
	andl	$2032, %edx             # imm = 0x7F0
	xorl	%ebp, %edx
	shrl	$4, %edx
	addl	207676(%r13,%rdx,4), %ecx
	xorl	%ebx, %edi
	addl	%edi, %edi
	addl	%ebx, %ebx
	notl	%edi
	andl	%edi, %esi
	cmpl	$65536, %ebx            # imm = 0x10000
	movl	%r12d, %edi
	jb	.LBB24_388
# BB#389:                               # %LitEnc_GetPriceMatched.exit969.i
                                        #   in Loop: Header=BB24_357 Depth=3
	incl	%r10d
	movq	104(%rsp), %rbp         # 8-byte Reload
	andl	%r10d, %ebp
	movq	280(%rsp), %rdx         # 8-byte Reload
	movl	kLiteralNextStates(,%rdx,4), %esi
	movq	%rsi, %rdi
	shlq	$5, %rdi
	addq	%r13, %rdi
	movzwl	213584(%rdi,%rbp,2), %edx
	xorl	$2032, %edx             # imm = 0x7F0
	shrl	$2, %edx
	andl	$16380, %edx            # imm = 0x3FFC
	movl	207676(%r13,%rdx), %edx
	movl	%edx, 280(%rsp)         # 4-byte Spill
	movq	%rsi, 336(%rsp)         # 8-byte Spill
	movzwl	213968(%r13,%rsi,2), %edx
	xorl	$2032, %edx             # imm = 0x7F0
	shrl	$2, %edx
	andl	$16380, %edx            # imm = 0x3FFC
	movl	207676(%r13,%rdx), %edx
	movl	%edx, 292(%rsp)         # 4-byte Spill
	movq	168(%rsp), %r8          # 8-byte Reload
	leal	1(%r8,%r14), %esi
	movq	160(%rsp), %r11         # 8-byte Reload
	cmpl	%esi, %r11d
	jae	.LBB24_396
# BB#390:                               # %.lr.ph1092.preheader.i
                                        #   in Loop: Header=BB24_357 Depth=3
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movl	%r11d, %r12d
	movl	%esi, %edx
	movl	%esi, %ebx
	subl	%r11d, %ebx
	leaq	-1(%rdx), %rbp
	subq	%r12, %rbp
	testb	$7, %bl
	movl	92(%rsp), %r10d         # 4-byte Reload
	je	.LBB24_393
# BB#391:                               # %.lr.ph1092.i.prol.preheader
                                        #   in Loop: Header=BB24_357 Depth=3
	leaq	(%r12,%r12,2), %rbx
	shlq	$4, %rbx
	addq	192(%rsp), %rbx         # 8-byte Folded Reload
	addl	272(%rsp), %eax         # 4-byte Folded Reload
	subb	%r11b, %al
	addb	80(%rsp), %al           # 1-byte Folded Reload
	movzbl	%al, %eax
	andl	$7, %eax
	negq	%rax
.LBB24_392:                             # %.lr.ph1092.i.prol
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_357 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incq	%r12
	movl	$1073741824, (%rbx)     # imm = 0x40000000
	addq	$48, %rbx
	incq	%rax
	jne	.LBB24_392
.LBB24_393:                             # %.lr.ph1092.i.prol.loopexit
                                        #   in Loop: Header=BB24_357 Depth=3
	cmpq	$7, %rbp
	jae	.LBB24_397
# BB#394:                               #   in Loop: Header=BB24_357 Depth=3
	movl	%esi, %r11d
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	168(%rsp), %r8          # 8-byte Reload
	movq	104(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB24_401
.LBB24_395:                             #   in Loop: Header=BB24_357 Depth=3
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	movb	80(%rsp), %r9b          # 1-byte Reload
	movl	28(%rsp), %edx          # 4-byte Reload
	jmp	.LBB24_404
.LBB24_396:                             # %LitEnc_GetPriceMatched.exit969.._crit_edge1093_crit_edge.i
                                        #   in Loop: Header=BB24_357 Depth=3
	movl	%esi, %edx
	movl	92(%rsp), %r10d         # 4-byte Reload
	jmp	.LBB24_400
.LBB24_397:                             # %.lr.ph1092.preheader.i.new
                                        #   in Loop: Header=BB24_357 Depth=3
	leaq	(%r12,%r12,2), %rax
	shlq	$4, %rax
	addq	184(%rsp), %rax         # 8-byte Folded Reload
	movq	168(%rsp), %r8          # 8-byte Reload
	movq	104(%rsp), %rbp         # 8-byte Reload
.LBB24_398:                             # %.lr.ph1092.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_357 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	$1073741824, -336(%rax) # imm = 0x40000000
	movl	$1073741824, -288(%rax) # imm = 0x40000000
	movl	$1073741824, -240(%rax) # imm = 0x40000000
	movl	$1073741824, -192(%rax) # imm = 0x40000000
	movl	$1073741824, -144(%rax) # imm = 0x40000000
	movl	$1073741824, -96(%rax)  # imm = 0x40000000
	movl	$1073741824, -48(%rax)  # imm = 0x40000000
	addq	$8, %r12
	movl	$1073741824, (%rax)     # imm = 0x40000000
	addq	$384, %rax              # imm = 0x180
	cmpq	%rdx, %r12
	jb	.LBB24_398
# BB#399:                               #   in Loop: Header=BB24_357 Depth=3
	movl	%esi, %r11d
.LBB24_400:                             # %._crit_edge1093.i
                                        #   in Loop: Header=BB24_357 Depth=3
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB24_401:                             # %._crit_edge1093.i
                                        #   in Loop: Header=BB24_357 Depth=3
	addl	$-2, %r8d
	imulq	$1088, %rbp, %rsi       # imm = 0x440
	addq	%r13, %rsi
	movq	336(%rsp), %rax         # 8-byte Reload
	movzwl	213992(%r13,%rax,2), %eax
	shrq	$2, %rax
	andl	$16380, %eax            # imm = 0x3FFC
	movzwl	214064(%rdi,%rbp,2), %edi
	xorl	$2032, %edi             # imm = 0x7F0
	shrl	$2, %edi
	andl	$16380, %edi            # imm = 0x3FFC
	movl	176(%rsp), %ebp         # 4-byte Reload
	addl	%r9d, %ebp
	addl	232(%rsp), %ebp         # 4-byte Folded Reload
	addl	%ecx, %ebp
	addl	280(%rsp), %ebp         # 4-byte Folded Reload
	addl	292(%rsp), %ebp         # 4-byte Folded Reload
	addl	234752(%rsi,%r8,4), %ebp
	addl	207676(%r13,%rax), %ebp
	addl	207676(%r13,%rdi), %ebp
	leaq	(%rdx,%rdx,2), %rax
	shlq	$4, %rax
	cmpl	2876(%r13,%rax), %ebp
	jae	.LBB24_403
# BB#402:                               #   in Loop: Header=BB24_357 Depth=3
	incl	%r14d
	leaq	2876(%r13,%rax), %rcx
	movl	%ebp, (%rcx)
	movl	%r14d, 2900(%r13,%rax)
	movl	$0, 2904(%r13,%rax)
	movl	$1, 2884(%r13,%rax)
	movl	$1, 2888(%r13,%rax)
	movl	%r15d, 2892(%r13,%rax)
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 2896(%r13,%rax)
.LBB24_403:                             #   in Loop: Header=BB24_357 Depth=3
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	movb	80(%rsp), %r9b          # 1-byte Reload
	movl	28(%rsp), %edx          # 4-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
	jmp	.LBB24_404
.LBB24_405:                             #   in Loop: Header=BB24_288 Depth=2
	movq	240(%rsp), %rcx         # 8-byte Reload
	cmpl	%edx, %ecx
	movl	72(%rsp), %ebx          # 4-byte Reload
	jbe	.LBB24_409
# BB#406:                               # %.preheader1026.i.preheader
                                        #   in Loop: Header=BB24_288 Depth=2
	xorl	%ecx, %ecx
.LBB24_407:                             # %.preheader1026.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ecx, %eax
	addl	$2, %ecx
	cmpl	208188(%r13,%rax,4), %edx
	ja	.LBB24_407
# BB#408:                               #   in Loop: Header=BB24_288 Depth=2
	movq	%rcx, 248(%rsp)         # 8-byte Spill
	movl	%edx, 208188(%r13,%rax,4)
	movl	%edx, %ecx
.LBB24_409:                             #   in Loop: Header=BB24_288 Depth=2
	cmpl	%ebx, %ecx
	jb	.LBB24_288
# BB#410:                               #   in Loop: Header=BB24_288 Depth=2
	movzwl	213968(%r13,%rdi,2), %eax
	shrq	$2, %rax
	andl	$16380, %eax            # imm = 0x3FFC
	addl	207676(%r13,%rax), %r10d
	movq	%rcx, %r8
	leal	(%rcx,%r15), %eax
	cmpl	%eax, %r11d
	jae	.LBB24_418
# BB#411:                               # %.lr.ph1101.preheader.i
                                        #   in Loop: Header=BB24_288 Depth=2
	movl	%r11d, %ecx
	movl	%eax, %edx
	movl	%eax, %edi
	subl	%r11d, %edi
	leaq	-1(%rdx), %rsi
	subq	%rcx, %rsi
	testb	$7, %dil
	je	.LBB24_414
# BB#412:                               # %.lr.ph1101.i.prol.preheader
                                        #   in Loop: Header=BB24_288 Depth=2
	leaq	(%rcx,%rcx,2), %rdi
	shlq	$4, %rdi
	addq	192(%rsp), %rdi         # 8-byte Folded Reload
	movq	224(%rsp), %rbp         # 8-byte Reload
	leal	1(%r8,%rbp), %ebp
	subb	%r11b, %bpl
	addb	%bpl, %r9b
	movzbl	%r9b, %ebp
	andl	$7, %ebp
	negq	%rbp
.LBB24_413:                             # %.lr.ph1101.i.prol
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incq	%rcx
	movl	$1073741824, (%rdi)     # imm = 0x40000000
	addq	$48, %rdi
	incq	%rbp
	jne	.LBB24_413
.LBB24_414:                             # %.lr.ph1101.i.prol.loopexit
                                        #   in Loop: Header=BB24_288 Depth=2
	cmpq	$7, %rsi
	jb	.LBB24_417
# BB#415:                               # %.lr.ph1101.preheader.i.new
                                        #   in Loop: Header=BB24_288 Depth=2
	leaq	(%rcx,%rcx,2), %rsi
	shlq	$4, %rsi
	addq	184(%rsp), %rsi         # 8-byte Folded Reload
.LBB24_416:                             # %.lr.ph1101.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$1073741824, -336(%rsi) # imm = 0x40000000
	movl	$1073741824, -288(%rsi) # imm = 0x40000000
	movl	$1073741824, -240(%rsi) # imm = 0x40000000
	movl	$1073741824, -192(%rsi) # imm = 0x40000000
	movl	$1073741824, -144(%rsi) # imm = 0x40000000
	movl	$1073741824, -96(%rsi)  # imm = 0x40000000
	movl	$1073741824, -48(%rsi)  # imm = 0x40000000
	addq	$8, %rcx
	movl	$1073741824, (%rsi)     # imm = 0x40000000
	addq	$384, %rsi              # imm = 0x180
	cmpq	%rdx, %rcx
	jb	.LBB24_416
.LBB24_417:                             #   in Loop: Header=BB24_288 Depth=2
	movl	%eax, %r11d
.LBB24_418:                             # %.preheader1025.i
                                        #   in Loop: Header=BB24_288 Depth=2
	movl	$-2, %edx
.LBB24_419:                             #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addl	$2, %edx
	cmpl	208188(%r13,%rdx,4), %ebx
	ja	.LBB24_419
# BB#420:                               #   in Loop: Header=BB24_288 Depth=2
	leal	1(%rdx), %eax
	movl	208188(%r13,%rax,4), %eax
	movl	$524287, %esi           # imm = 0x7FFFF
	subl	%eax, %esi
	sarl	$31, %esi
	andl	$12, %esi
	leal	6(%rsi), %ecx
	movl	%eax, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	movzbl	199484(%r13,%rdi), %ecx
	leal	12(%rcx,%rsi,2), %ecx
	movl	%r10d, 92(%rsp)         # 4-byte Spill
	jmp	.LBB24_422
.LBB24_421:                             #   in Loop: Header=BB24_422 Depth=3
	movl	$524287, %esi           # imm = 0x7FFFF
	subl	%eax, %esi
	sarl	$31, %esi
	andl	$12, %esi
	leal	6(%rsi), %ecx
	movl	%eax, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	movzbl	199484(%r13,%rdi), %ecx
	leal	12(%rcx,%rsi,2), %ecx
.LBB24_422:                             # %.thread1014.outer.outer.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB24_423 Depth 4
                                        #           Child Loop BB24_429 Depth 5
                                        #           Child Loop BB24_425 Depth 5
                                        #           Child Loop BB24_434 Depth 5
                                        #           Child Loop BB24_438 Depth 5
                                        #           Child Loop BB24_442 Depth 5
                                        #           Child Loop BB24_447 Depth 5
	movl	%ecx, %r8d
.LBB24_423:                             # %.thread1014.outer.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_422 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB24_429 Depth 5
                                        #           Child Loop BB24_425 Depth 5
                                        #           Child Loop BB24_434 Depth 5
                                        #           Child Loop BB24_438 Depth 5
                                        #           Child Loop BB24_442 Depth 5
                                        #           Child Loop BB24_447 Depth 5
	leal	4(%rax), %r9d
	cmpl	$128, %eax
	movl	%eax, %ecx
	movl	%edx, %r14d
	jae	.LBB24_428
# BB#424:                               # %.thread1014.i.us.preheader
                                        #   in Loop: Header=BB24_423 Depth=4
	decl	%ebx
	movl	%ebx, %edi
	.p2align	4, 0x90
.LBB24_425:                             # %.thread1014.i.us
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_422 Depth=3
                                        #         Parent Loop BB24_423 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	leal	-1(%rdi), %eax
	leal	1(%r15,%rdi), %esi
	leal	1(%rdi), %edi
	imulq	$1088, %r12, %rdx       # imm = 0x440
	addq	%r13, %rdx
	movl	216248(%rdx,%rax,4), %edx
	addl	%r10d, %edx
	cmpl	$5, %edi
	movl	$3, %ebp
	cmovaeq	%rbp, %rax
	shlq	$9, %rax
	addq	%r13, %rax
	addl	211436(%rax,%rcx,4), %edx
	leaq	(%rsi,%rsi,2), %rax
	shlq	$4, %rax
	cmpl	2876(%r13,%rax), %edx
	jae	.LBB24_427
# BB#426:                               #   in Loop: Header=BB24_425 Depth=5
	leaq	2876(%r13,%rax), %rsi
	movl	%edx, (%rsi)
	movl	%r15d, 2900(%r13,%rax)
	movl	%r9d, 2904(%r13,%rax)
	movl	$0, 2884(%r13,%rax)
.LBB24_427:                             #   in Loop: Header=BB24_425 Depth=5
	cmpl	208188(%r13,%r14,4), %edi
	jne	.LBB24_425
	jmp	.LBB24_432
.LBB24_428:                             # %.thread1014.i.preheader
                                        #   in Loop: Header=BB24_423 Depth=4
	andl	$15, %eax
	decl	%ebx
	movl	%ebx, %edi
	.p2align	4, 0x90
.LBB24_429:                             # %.thread1014.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_422 Depth=3
                                        #         Parent Loop BB24_423 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	leal	-1(%rdi), %esi
	leal	1(%r15,%rdi), %ebp
	leal	1(%rdi), %edi
	imulq	$1088, %r12, %rdx       # imm = 0x440
	addq	%r13, %rdx
	movl	216248(%rdx,%rsi,4), %ebx
	addl	%r10d, %ebx
	cmpl	$5, %edi
	movl	$3, %edx
	cmovaeq	%rdx, %rsi
	shlq	$8, %rsi
	addq	%r13, %rsi
	movl	213484(%r13,%rax,4), %edx
	addl	210412(%rsi,%r8,4), %edx
	addl	%ebx, %edx
	leaq	(%rbp,%rbp,2), %rsi
	shlq	$4, %rsi
	cmpl	2876(%r13,%rsi), %edx
	jae	.LBB24_431
# BB#430:                               #   in Loop: Header=BB24_429 Depth=5
	leaq	2876(%r13,%rsi), %rbp
	movl	%edx, (%rbp)
	movl	%r15d, 2900(%r13,%rsi)
	movl	%r9d, 2904(%r13,%rsi)
	movl	$0, 2884(%r13,%rsi)
.LBB24_431:                             #   in Loop: Header=BB24_429 Depth=5
	cmpl	208188(%r13,%r14,4), %edi
	jne	.LBB24_429
.LBB24_432:                             # %.us-lcssa.us.loopexit813
                                        #   in Loop: Header=BB24_423 Depth=4
	leal	(%r15,%rdi), %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	incl	%ecx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %r12
	subq	%rcx, %r12
	leal	1(%rdi), %esi
	movl	210384(%r13), %eax
	addl	%esi, %eax
	movl	200(%rsp), %ecx         # 4-byte Reload
	cmpl	%ecx, %eax
	cmoval	%ecx, %eax
	cmpl	%eax, %esi
	movl	%esi, 72(%rsp)          # 4-byte Spill
	movl	%esi, %r10d
	jae	.LBB24_436
# BB#433:                               # %.lr.ph1105.i.preheader
                                        #   in Loop: Header=BB24_423 Depth=4
	movl	72(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %r10d
	.p2align	4, 0x90
.LBB24_434:                             # %.lr.ph1105.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_422 Depth=3
                                        #         Parent Loop BB24_423 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	%r10d, %ecx
	movzbl	(%rbp,%rcx), %ebx
	cmpb	(%r12,%rcx), %bl
	jne	.LBB24_436
# BB#435:                               #   in Loop: Header=BB24_434 Depth=5
	incl	%r10d
	cmpl	%eax, %r10d
	jb	.LBB24_434
.LBB24_436:                             # %.critedge7.i
                                        #   in Loop: Header=BB24_423 Depth=4
	movl	72(%rsp), %ebx          # 4-byte Reload
	subl	%ebx, %r10d
	cmpl	$2, %r10d
	jb	.LBB24_452
# BB#437:                               #   in Loop: Header=BB24_423 Depth=4
	movq	%r11, 160(%rsp)         # 8-byte Spill
	movq	144(%rsp), %rax         # 8-byte Reload
	leal	(%rdi,%rax), %r11d
	movl	213556(%r13), %ecx
	movl	213572(%r13), %eax
	movl	%eax, 136(%rsp)         # 4-byte Spill
	movl	%eax, %ebx
	andl	%r11d, %ebx
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	kMatchNextStates(,%rax,4), %eax
	movq	%rax, %rsi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	shlq	$5, %rax
	addq	%r13, %rax
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movzwl	213584(%rax,%rbx,2), %eax
	shrq	$2, %rax
	andl	$16380, %eax            # imm = 0x3FFC
	movl	207676(%r13,%rax), %eax
	movl	%eax, 96(%rsp)          # 4-byte Spill
	andl	213568(%r13), %r11d
	shll	%cl, %r11d
	leal	-1(%rdi), %eax
	movq	%rbp, %rbx
	movzbl	(%rbx,%rax), %ebp
	movl	$8, %eax
	subl	%ecx, %eax
	movl	%eax, %ecx
	shrl	%cl, %ebp
	addl	%r11d, %ebp
	shll	$8, %ebp
	leal	(%rbp,%rbp,2), %r11d
	addq	%r11, %r11
	addq	213576(%r13), %r11
	movl	%edi, %eax
	movzbl	(%rbx,%rax), %edi
	movzbl	(%r12,%rax), %ebp
	orl	$256, %edi              # imm = 0x100
	movl	$256, %esi              # imm = 0x100
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB24_438:                             #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_422 Depth=3
                                        #         Parent Loop BB24_423 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	leal	(%rbp,%rbp), %ecx
	movl	%esi, %ebx
	andl	%ecx, %ebx
	movl	%edi, %eax
	shrl	$8, %eax
	addl	%esi, %eax
	addl	%ebx, %eax
	movzwl	(%r11,%rax,2), %eax
	movl	%edi, %ebx
	shrl	$7, %ebx
	andl	$1, %ebx
	negl	%ebx
	andl	$2032, %ebx             # imm = 0x7F0
	xorl	%eax, %ebx
	shrl	$4, %ebx
	addl	207676(%r13,%rbx,4), %r12d
	xorl	%edi, %ebp
	addl	%ebp, %ebp
	addl	%edi, %edi
	notl	%ebp
	andl	%ebp, %esi
	cmpl	$65536, %edi            # imm = 0x10000
	movl	%ecx, %ebp
	jb	.LBB24_438
# BB#439:                               # %LitEnc_GetPriceMatched.exit964.i
                                        #   in Loop: Header=BB24_423 Depth=4
	movq	80(%rsp), %rsi          # 8-byte Reload
	incl	%esi
	andl	136(%rsp), %esi         # 4-byte Folded Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	kLiteralNextStates(,%rax,4), %ecx
	movq	%rcx, %rdi
	shlq	$5, %rdi
	addq	%r13, %rdi
	movzwl	213584(%rdi,%rsi,2), %eax
	xorl	$2032, %eax             # imm = 0x7F0
	shrl	$2, %eax
	andl	$16380, %eax            # imm = 0x3FFC
	movl	207676(%r13,%rax), %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movzwl	213968(%r13,%rcx,2), %eax
	xorl	$2032, %eax             # imm = 0x7F0
	shrl	$2, %eax
	andl	$16380, %eax            # imm = 0x3FFC
	movl	207676(%r13,%rax), %eax
	movl	%eax, 156(%rsp)         # 4-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	leal	1(%r10,%rax), %eax
	movq	160(%rsp), %r11         # 8-byte Reload
	cmpl	%eax, %r11d
	jae	.LBB24_445
# BB#440:                               # %.lr.ph1110.preheader.i
                                        #   in Loop: Header=BB24_423 Depth=4
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movl	%r11d, %esi
	movl	%eax, %ebx
	movl	%eax, %ecx
	subl	%r11d, %ecx
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	leaq	-1(%rbx), %rbp
	subq	%rsi, %rbp
	andq	$7, %rcx
	movl	72(%rsp), %ebx          # 4-byte Reload
	je	.LBB24_443
# BB#441:                               # %.lr.ph1110.i.prol.preheader
                                        #   in Loop: Header=BB24_423 Depth=4
	leaq	(%rsi,%rsi,2), %r11
	shlq	$4, %r11
	addq	192(%rsp), %r11         # 8-byte Folded Reload
	negq	%rcx
.LBB24_442:                             # %.lr.ph1110.i.prol
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_422 Depth=3
                                        #         Parent Loop BB24_423 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incq	%rsi
	movl	$1073741824, (%r11)     # imm = 0x40000000
	addq	$48, %r11
	incq	%rcx
	jne	.LBB24_442
.LBB24_443:                             # %.lr.ph1110.i.prol.loopexit
                                        #   in Loop: Header=BB24_423 Depth=4
	cmpq	$7, %rbp
	jae	.LBB24_446
# BB#444:                               #   in Loop: Header=BB24_423 Depth=4
	movl	%eax, %r11d
	movl	96(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB24_449
.LBB24_445:                             # %LitEnc_GetPriceMatched.exit964.._crit_edge1111_crit_edge.i
                                        #   in Loop: Header=BB24_423 Depth=4
	movl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	72(%rsp), %ebx          # 4-byte Reload
	movl	96(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB24_450
.LBB24_446:                             # %.lr.ph1110.preheader.i.new
                                        #   in Loop: Header=BB24_423 Depth=4
	movq	136(%rsp), %rcx         # 8-byte Reload
	subq	%rsi, %rcx
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$4, %rsi
	addq	184(%rsp), %rsi         # 8-byte Folded Reload
	movl	96(%rsp), %ebp          # 4-byte Reload
.LBB24_447:                             # %.lr.ph1110.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_288 Depth=2
                                        #       Parent Loop BB24_422 Depth=3
                                        #         Parent Loop BB24_423 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	$1073741824, -336(%rsi) # imm = 0x40000000
	movl	$1073741824, -288(%rsi) # imm = 0x40000000
	movl	$1073741824, -240(%rsi) # imm = 0x40000000
	movl	$1073741824, -192(%rsi) # imm = 0x40000000
	movl	$1073741824, -144(%rsi) # imm = 0x40000000
	movl	$1073741824, -96(%rsi)  # imm = 0x40000000
	movl	$1073741824, -48(%rsi)  # imm = 0x40000000
	movl	$1073741824, (%rsi)     # imm = 0x40000000
	addq	$384, %rsi              # imm = 0x180
	addq	$-8, %rcx
	jne	.LBB24_447
# BB#448:                               #   in Loop: Header=BB24_423 Depth=4
	movl	%eax, %r11d
.LBB24_449:                             # %._crit_edge1111.i
                                        #   in Loop: Header=BB24_423 Depth=4
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	240(%rsp), %rcx         # 8-byte Reload
.LBB24_450:                             # %._crit_edge1111.i
                                        #   in Loop: Header=BB24_423 Depth=4
	addl	$-2, %r10d
	imulq	$1088, %rsi, %rax       # imm = 0x440
	addq	%r13, %rax
	movzwl	213992(%r13,%rcx,2), %ecx
	shrq	$2, %rcx
	andl	$16380, %ecx            # imm = 0x3FFC
	movzwl	214064(%rdi,%rsi,2), %esi
	xorl	$2032, %esi             # imm = 0x7F0
	shrl	$2, %esi
	andl	$16380, %esi            # imm = 0x3FFC
	addl	%edx, %ebp
	addl	%r12d, %ebp
	addl	104(%rsp), %ebp         # 4-byte Folded Reload
	addl	156(%rsp), %ebp         # 4-byte Folded Reload
	addl	234752(%rax,%r10,4), %ebp
	addl	207676(%r13,%rcx), %ebp
	addl	207676(%r13,%rsi), %ebp
	movq	136(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	cmpl	2876(%r13,%rax), %ebp
	jae	.LBB24_452
# BB#451:                               #   in Loop: Header=BB24_423 Depth=4
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rdx
	incl	%edx
	leaq	2876(%r13,%rax), %rcx
	movl	%ebp, (%rcx)
	movl	%edx, 2900(%r13,%rax)
	movl	$0, 2904(%r13,%rax)
	movl	$1, 2884(%r13,%rax)
	movl	$1, 2888(%r13,%rax)
	movl	%r15d, 2892(%r13,%rax)
	movl	%r9d, 2896(%r13,%rax)
.LBB24_452:                             #   in Loop: Header=BB24_423 Depth=4
	movl	92(%rsp), %r10d         # 4-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	leal	2(%r14), %edx
	cmpl	248(%rsp), %edx         # 4-byte Folded Reload
	je	.LBB24_288
# BB#453:                               #   in Loop: Header=BB24_423 Depth=4
	addl	$3, %r14d
	movl	208188(%r13,%r14,4), %eax
	cmpl	$128, %eax
	jb	.LBB24_423
	jmp	.LBB24_421
.LBB24_454:                             #   in Loop: Header=BB24_51 Depth=1
	movl	$2, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%r12d, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB24_470
.LBB24_455:                             #   in Loop: Header=BB24_51 Depth=1
	movl	%r11d, %eax
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	movl	2900(%r13,%rax), %esi
	movl	2904(%r13,%rax), %eax
	movl	%r11d, 2856(%r13)
.LBB24_456:                             #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r11d, %r10d
	movl	%esi, %r11d
	movl	%r10d, %edx
	leaq	(%rdx,%rdx,2), %rbp
	shlq	$4, %rbp
	movl	%r11d, %edx
	leaq	(%rdx,%rdx,2), %rbx
	shlq	$4, %rbx
	leaq	2904(%r13,%rbx), %rdi
	cmpl	$0, 2884(%r13,%rbp)
	je	.LBB24_459
# BB#457:                               #   in Loop: Header=BB24_456 Depth=2
	movl	$-1, (%rdi)
	movl	$0, 2884(%r13,%rbx)
	leal	-1(%rdx), %esi
	leaq	2900(%r13,%rbx), %r9
	movl	%esi, 2900(%r13,%rbx)
	movl	$-1, %r8d
	cmpl	$0, 2888(%r13,%rbp)
	je	.LBB24_460
# BB#458:                               #   in Loop: Header=BB24_456 Depth=2
	movl	%esi, %ebx
	leaq	(%rbx,%rbx,2), %rbx
	shlq	$4, %rbx
	movl	$0, 2884(%r13,%rbx)
	movl	2892(%r13,%rbp), %ecx
	movl	%ecx, 2900(%r13,%rbx)
	movl	2896(%r13,%rbp), %ecx
	movl	%ecx, 2904(%r13,%rbx)
	jmp	.LBB24_460
.LBB24_459:                             # %._crit_edge.i995.i
                                        #   in Loop: Header=BB24_456 Depth=2
	movl	(%rdi), %r8d
	leaq	2900(%r13,%rbx), %r9
	movl	2900(%r13,%rbx), %esi
.LBB24_460:                             #   in Loop: Header=BB24_456 Depth=2
	movl	%eax, (%rdi)
	movl	%r10d, (%r9)
	testl	%edx, %edx
	movl	%r8d, %eax
	jne	.LBB24_456
	jmp	.LBB24_467
.LBB24_461:                             #   in Loop: Header=BB24_51 Depth=1
	movq	248(%rsp), %rax         # 8-byte Reload
	movl	%eax, 2868(%r13)
	movl	%ebp, 2864(%r13)
	movl	%r15d, %eax
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	movl	2900(%r13,%rax), %esi
	movl	2904(%r13,%rax), %eax
	movl	%r15d, 2856(%r13)
.LBB24_462:                             #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r15d, %r10d
	movl	%esi, %r15d
	movl	%r10d, %edx
	leaq	(%rdx,%rdx,2), %rbp
	shlq	$4, %rbp
	movl	%r15d, %edx
	leaq	(%rdx,%rdx,2), %rbx
	shlq	$4, %rbx
	leaq	2904(%r13,%rbx), %rdi
	cmpl	$0, 2884(%r13,%rbp)
	je	.LBB24_465
# BB#463:                               #   in Loop: Header=BB24_462 Depth=2
	movl	$-1, (%rdi)
	movl	$0, 2884(%r13,%rbx)
	leal	-1(%rdx), %esi
	leaq	2900(%r13,%rbx), %r9
	movl	%esi, 2900(%r13,%rbx)
	movl	$-1, %r8d
	cmpl	$0, 2888(%r13,%rbp)
	je	.LBB24_466
# BB#464:                               #   in Loop: Header=BB24_462 Depth=2
	movl	%esi, %ebx
	leaq	(%rbx,%rbx,2), %rbx
	shlq	$4, %rbx
	movl	$0, 2884(%r13,%rbx)
	movl	2892(%r13,%rbp), %ecx
	movl	%ecx, 2900(%r13,%rbx)
	movl	2896(%r13,%rbp), %ecx
	movl	%ecx, 2904(%r13,%rbx)
	jmp	.LBB24_466
.LBB24_465:                             # %._crit_edge.i.i
                                        #   in Loop: Header=BB24_462 Depth=2
	movl	(%rdi), %r8d
	leaq	2900(%r13,%rbx), %r9
	movl	2900(%r13,%rbx), %esi
.LBB24_466:                             #   in Loop: Header=BB24_462 Depth=2
	movl	%eax, (%rdi)
	movl	%r10d, (%r9)
	testl	%edx, %edx
	movl	%r8d, %eax
	jne	.LBB24_462
.LBB24_467:                             # %Backward.exit996.i
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	2900(%r13), %r12d
	movl	2904(%r13), %ebx
	movl	%r12d, 2860(%r13)
	.p2align	4, 0x90
.LBB24_468:                             # %GetOptimum.exit
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r12, 8(%rsp)           # 8-byte Spill
.LBB24_469:                             # %GetOptimumFast.exit
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	4(%rsp), %r14d          # 4-byte Reload
.LBB24_470:                             # %GetOptimumFast.exit
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	213572(%r13), %r15d
	andl	%r14d, %r15d
	movl	210408(%r13), %eax
	movq	%rax, %rdx
	shlq	$5, %rdx
	addq	%r13, %rdx
	leaq	213584(%rdx,%r15,2), %rcx
	movzwl	213584(%rdx,%r15,2), %esi
	movl	252240(%r13), %edi
	movl	%edi, %edx
	shrl	$11, %edx
	imull	%esi, %edx
	movq	32(%rsp), %rbx          # 8-byte Reload
	cmpl	$-1, %ebx
	movq	8(%rsp), %r12           # 8-byte Reload
	jne	.LBB24_476
# BB#471:                               # %GetOptimumFast.exit
                                        #   in Loop: Header=BB24_51 Depth=1
	cmpl	$1, %r12d
	jne	.LBB24_476
# BB#472:                               #   in Loop: Header=BB24_51 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	%edx, (%rbp)
	movl	$2048, %eax             # imm = 0x800
	subl	%esi, %eax
	shrl	$5, %eax
	addl	%esi, %eax
	movw	%ax, (%rcx)
	cmpl	$16777215, %edx         # imm = 0xFFFFFF
	ja	.LBB24_490
# BB#473:                               #   in Loop: Header=BB24_51 Depth=1
	shll	$8, %edx
	movl	%edx, 252240(%r13)
	movq	252248(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB24_481
# BB#474:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB24_481
# BB#475:                               # %._crit_edge.i276
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	252256(%r13), %rcx
	incq	%rcx
	jmp	.LBB24_489
	.p2align	4, 0x90
.LBB24_476:                             #   in Loop: Header=BB24_51 Depth=1
	movl	%edx, %ebp
	addq	%rbp, 252248(%r13)
	subl	%edx, %edi
	movl	%edi, 252240(%r13)
	movl	%esi, %edx
	shrl	$5, %edx
	subl	%edx, %esi
	movw	%si, (%rcx)
	cmpl	$16777215, %edi         # imm = 0xFFFFFF
	ja	.LBB24_480
# BB#477:                               #   in Loop: Header=BB24_51 Depth=1
	shll	$8, %edi
	movl	%edi, 252240(%r13)
	movq	252248(%r13), %rcx
	movq	%rcx, %rdx
	shrq	$32, %rdx
	jne	.LBB24_528
# BB#478:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$-16777216, %ecx        # imm = 0xFF000000
	jb	.LBB24_528
# BB#479:                               # %._crit_edge.i309
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	252256(%r13), %rdx
	incq	%rdx
	jmp	.LBB24_536
.LBB24_480:                             #   in Loop: Header=BB24_51 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB24_537
.LBB24_481:                             #   in Loop: Header=BB24_51 Depth=1
	movb	252244(%r13), %cl
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_482:                             # %RangeEnc_FlushStream.exit._crit_edge.i279
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_487
# BB#483:                               #   in Loop: Header=BB24_482 Depth=2
	cmpl	$0, 252304(%r13)
	jne	.LBB24_487
# BB#484:                               #   in Loop: Header=BB24_482 Depth=2
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_486
# BB#485:                               #   in Loop: Header=BB24_482 Depth=2
	movl	$9, 252304(%r13)
.LBB24_486:                             #   in Loop: Header=BB24_482 Depth=2
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_487:                             # %RangeEnc_FlushStream.exit.i280
                                        #   in Loop: Header=BB24_482 Depth=2
	decq	252256(%r13)
	movq	252248(%r13), %rax
	movb	$-1, %cl
	jne	.LBB24_482
# BB#488:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 252244(%r13)
	movl	$1, %ecx
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB24_489:                             # %RangeEnc_ShiftLow.exit282
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	%rcx, 252256(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 252248(%r13)
.LBB24_490:                             # %RangeEnc_EncodeBit.exit271
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	48(%r13), %rdi
	callq	*24(%r13)
	movl	210388(%r13), %esi
	movq	%rax, %rdx
	subq	%rsi, %rdx
	negq	%rsi
	movl	213568(%r13), %edi
	andl	%r14d, %edi
	movl	213556(%r13), %ecx
	shll	%cl, %edi
	movzbl	-1(%rax,%rsi), %esi
	movl	$8, %eax
	subl	%ecx, %eax
	movl	%eax, %ecx
	shrl	%cl, %esi
	addl	%edi, %esi
	shll	$8, %esi
	leal	(%rsi,%rsi,2), %r14d
	addq	%r14, %r14
	addq	213576(%r13), %r14
	movzbl	(%rdx), %ebx
	cmpl	$6, 210408(%r13)
	ja	.LBB24_509
# BB#491:                               #   in Loop: Header=BB24_51 Depth=1
	orl	$256, %ebx              # imm = 0x100
	.p2align	4, 0x90
.LBB24_492:                             #   Parent Loop BB24_51 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_500 Depth 3
	movl	%ebx, %eax
	shrl	$8, %eax
	movzwl	(%r14,%rax,2), %edx
	movl	(%rbp), %esi
	movl	%esi, %ecx
	shrl	$11, %ecx
	imull	%edx, %ecx
	testb	%bl, %bl
	js	.LBB24_494
# BB#493:                               #   in Loop: Header=BB24_492 Depth=2
	movl	%ecx, (%rbp)
	movl	$2048, %esi             # imm = 0x800
	subl	%edx, %esi
	shrl	$5, %esi
	addl	%edx, %esi
	jmp	.LBB24_495
	.p2align	4, 0x90
.LBB24_494:                             #   in Loop: Header=BB24_492 Depth=2
	movl	%ecx, %edi
	addq	%rdi, 252248(%r13)
	subl	%ecx, %esi
	movl	%esi, 252240(%r13)
	movl	%edx, %ecx
	shrl	$5, %ecx
	subl	%ecx, %edx
	movl	%esi, %ecx
	movl	%edx, %esi
.LBB24_495:                             #   in Loop: Header=BB24_492 Depth=2
	movw	%si, (%r14,%rax,2)
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB24_508
# BB#496:                               #   in Loop: Header=BB24_492 Depth=2
	shll	$8, %ecx
	movl	%ecx, 252240(%r13)
	movq	252248(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB24_499
# BB#497:                               #   in Loop: Header=BB24_492 Depth=2
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB24_499
# BB#498:                               # %._crit_edge.i291
                                        #   in Loop: Header=BB24_492 Depth=2
	movq	252256(%r13), %rcx
	incq	%rcx
	jmp	.LBB24_507
	.p2align	4, 0x90
.LBB24_499:                             #   in Loop: Header=BB24_492 Depth=2
	movb	252244(%r13), %cl
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_500:                             # %RangeEnc_FlushStream.exit._crit_edge.i294
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_492 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_505
# BB#501:                               #   in Loop: Header=BB24_500 Depth=3
	cmpl	$0, 252304(%r13)
	jne	.LBB24_505
# BB#502:                               #   in Loop: Header=BB24_500 Depth=3
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_504
# BB#503:                               #   in Loop: Header=BB24_500 Depth=3
	movl	$9, 252304(%r13)
.LBB24_504:                             #   in Loop: Header=BB24_500 Depth=3
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_505:                             # %RangeEnc_FlushStream.exit.i295
                                        #   in Loop: Header=BB24_500 Depth=3
	decq	252256(%r13)
	movq	252248(%r13), %rax
	movb	$-1, %cl
	jne	.LBB24_500
# BB#506:                               #   in Loop: Header=BB24_492 Depth=2
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 252244(%r13)
	movl	$1, %ecx
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB24_507:                             # %RangeEnc_ShiftLow.exit297
                                        #   in Loop: Header=BB24_492 Depth=2
	movq	%rcx, 252256(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 252248(%r13)
.LBB24_508:                             # %RangeEnc_EncodeBit.exit.i285
                                        #   in Loop: Header=BB24_492 Depth=2
	addl	%ebx, %ebx
	cmpl	$65536, %ebx            # imm = 0x10000
	jb	.LBB24_492
	jmp	.LBB24_527
.LBB24_509:                             #   in Loop: Header=BB24_51 Depth=1
	movq	312(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	subq	%rax, %rdx
	movzbl	-1(%rdx), %r15d
	orl	$256, %ebx              # imm = 0x100
	movl	$256, %r12d             # imm = 0x100
	.p2align	4, 0x90
.LBB24_510:                             #   Parent Loop BB24_51 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_518 Depth 3
	addl	%r15d, %r15d
	movl	%r15d, %ecx
	andl	%r12d, %ecx
	movl	%ebx, %eax
	shrl	$8, %eax
	addl	%r12d, %eax
	addl	%ecx, %eax
	movzwl	(%r14,%rax,2), %edx
	movl	(%rbp), %esi
	movl	%esi, %ecx
	shrl	$11, %ecx
	imull	%edx, %ecx
	testb	%bl, %bl
	js	.LBB24_512
# BB#511:                               #   in Loop: Header=BB24_510 Depth=2
	movl	%ecx, (%rbp)
	movl	$2048, %esi             # imm = 0x800
	subl	%edx, %esi
	shrl	$5, %esi
	addl	%edx, %esi
	jmp	.LBB24_513
	.p2align	4, 0x90
.LBB24_512:                             #   in Loop: Header=BB24_510 Depth=2
	movl	%ecx, %edi
	addq	%rdi, 252248(%r13)
	subl	%ecx, %esi
	movl	%esi, 252240(%r13)
	movl	%edx, %ecx
	shrl	$5, %ecx
	subl	%ecx, %edx
	movl	%esi, %ecx
	movl	%edx, %esi
.LBB24_513:                             #   in Loop: Header=BB24_510 Depth=2
	movw	%si, (%r14,%rax,2)
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB24_526
# BB#514:                               #   in Loop: Header=BB24_510 Depth=2
	shll	$8, %ecx
	movl	%ecx, 252240(%r13)
	movq	252248(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB24_517
# BB#515:                               #   in Loop: Header=BB24_510 Depth=2
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB24_517
# BB#516:                               # %._crit_edge.i.i300
                                        #   in Loop: Header=BB24_510 Depth=2
	movq	252256(%r13), %rcx
	incq	%rcx
	jmp	.LBB24_525
	.p2align	4, 0x90
.LBB24_517:                             #   in Loop: Header=BB24_510 Depth=2
	movb	252244(%r13), %cl
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_518:                             # %RangeEnc_FlushStream.exit._crit_edge.i.i
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_510 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_523
# BB#519:                               #   in Loop: Header=BB24_518 Depth=3
	cmpl	$0, 252304(%r13)
	jne	.LBB24_523
# BB#520:                               #   in Loop: Header=BB24_518 Depth=3
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_522
# BB#521:                               #   in Loop: Header=BB24_518 Depth=3
	movl	$9, 252304(%r13)
.LBB24_522:                             #   in Loop: Header=BB24_518 Depth=3
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_523:                             # %RangeEnc_FlushStream.exit.i.i
                                        #   in Loop: Header=BB24_518 Depth=3
	decq	252256(%r13)
	movq	252248(%r13), %rax
	movb	$-1, %cl
	jne	.LBB24_518
# BB#524:                               #   in Loop: Header=BB24_510 Depth=2
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 252244(%r13)
	movl	$1, %ecx
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB24_525:                             # %RangeEnc_ShiftLow.exit.i
                                        #   in Loop: Header=BB24_510 Depth=2
	movq	%rcx, 252256(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 252248(%r13)
.LBB24_526:                             # %RangeEnc_EncodeBit.exit.i302
                                        #   in Loop: Header=BB24_510 Depth=2
	addl	%ebx, %ebx
	movl	%r15d, %eax
	xorl	%ebx, %eax
	notl	%eax
	andl	%eax, %r12d
	cmpl	$65536, %ebx            # imm = 0x10000
	jb	.LBB24_510
.LBB24_527:                             # %LitEnc_Encode.exit286
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210408(%r13), %eax
	movl	kLiteralNextStates(,%rax,4), %eax
	movl	%eax, 210408(%r13)
	jmp	.LBB24_649
.LBB24_528:                             #   in Loop: Header=BB24_51 Depth=1
	movb	252244(%r13), %al
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_529:                             # %RangeEnc_FlushStream.exit._crit_edge.i312
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rcx
	addb	%al, %cl
	movb	%cl, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_534
# BB#530:                               #   in Loop: Header=BB24_529 Depth=2
	cmpl	$0, 252304(%r13)
	jne	.LBB24_534
# BB#531:                               #   in Loop: Header=BB24_529 Depth=2
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_533
# BB#532:                               #   in Loop: Header=BB24_529 Depth=2
	movl	$9, 252304(%r13)
.LBB24_533:                             #   in Loop: Header=BB24_529 Depth=2
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_534:                             # %RangeEnc_FlushStream.exit.i313
                                        #   in Loop: Header=BB24_529 Depth=2
	decq	252256(%r13)
	movq	252248(%r13), %rcx
	movb	$-1, %al
	jne	.LBB24_529
# BB#535:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, 252244(%r13)
	movl	210408(%r13), %eax
	movl	$1, %edx
.LBB24_536:                             # %RangeEnc_ShiftLow.exit315
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rdx, 252256(%r13)
	andq	$16777215, %rcx         # imm = 0xFFFFFF
	shlq	$8, %rcx
	movq	%rcx, 252248(%r13)
.LBB24_537:                             # %RangeEnc_EncodeBit.exit304
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%eax, %ecx
	movzwl	213968(%r13,%rcx,2), %edx
	movl	252240(%r13), %edi
	movl	%edi, %ebp
	shrl	$11, %ebp
	imull	%edx, %ebp
	cmpl	$3, %ebx
	ja	.LBB24_542
# BB#538:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%ebp, %eax
	addq	%rax, 252248(%r13)
	subl	%ebp, %edi
	movl	%edi, 252240(%r13)
	movl	%edx, %eax
	shrl	$5, %eax
	subl	%eax, %edx
	movw	%dx, 213968(%r13,%rcx,2)
	cmpl	$16777215, %edi         # imm = 0xFFFFFF
	ja	.LBB24_555
# BB#539:                               #   in Loop: Header=BB24_51 Depth=1
	shll	$8, %edi
	movl	%edi, 252240(%r13)
	movq	252248(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB24_546
# BB#540:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB24_546
# BB#541:                               # %._crit_edge.i322
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	252256(%r13), %rcx
	incq	%rcx
	jmp	.LBB24_554
.LBB24_542:                             #   in Loop: Header=BB24_51 Depth=1
	movl	%ebp, 252240(%r13)
	movl	$2048, %edi             # imm = 0x800
	subl	%edx, %edi
	shrl	$5, %edi
	addl	%edx, %edi
	movw	%di, 213968(%r13,%rcx,2)
	cmpl	$16777215, %ebp         # imm = 0xFFFFFF
	ja	.LBB24_573
# BB#543:                               #   in Loop: Header=BB24_51 Depth=1
	shll	$8, %ebp
	movl	%ebp, 252240(%r13)
	movq	252248(%r13), %rcx
	movq	%rcx, %rdx
	shrq	$32, %rdx
	jne	.LBB24_564
# BB#544:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$-16777216, %ecx        # imm = 0xFF000000
	jb	.LBB24_564
# BB#545:                               # %._crit_edge.i413
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	252256(%r13), %rdx
	incq	%rdx
	jmp	.LBB24_572
.LBB24_546:                             #   in Loop: Header=BB24_51 Depth=1
	movb	252244(%r13), %cl
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_547:                             # %RangeEnc_FlushStream.exit._crit_edge.i325
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_552
# BB#548:                               #   in Loop: Header=BB24_547 Depth=2
	cmpl	$0, 252304(%r13)
	jne	.LBB24_552
# BB#549:                               #   in Loop: Header=BB24_547 Depth=2
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_551
# BB#550:                               #   in Loop: Header=BB24_547 Depth=2
	movl	$9, 252304(%r13)
.LBB24_551:                             #   in Loop: Header=BB24_547 Depth=2
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_552:                             # %RangeEnc_FlushStream.exit.i326
                                        #   in Loop: Header=BB24_547 Depth=2
	decq	252256(%r13)
	movq	252248(%r13), %rax
	movb	$-1, %cl
	jne	.LBB24_547
# BB#553:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 252244(%r13)
	movl	$1, %ecx
	movq	16(%rsp), %rsi          # 8-byte Reload
.LBB24_554:                             # %RangeEnc_ShiftLow.exit328
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	%rcx, 252256(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 252248(%r13)
.LBB24_555:                             # %RangeEnc_EncodeBit.exit317
                                        #   in Loop: Header=BB24_51 Depth=1
	testl	%ebx, %ebx
	je	.LBB24_560
# BB#556:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%ebx, %eax
	movl	210392(%r13,%rax,4), %r14d
	movl	210408(%r13), %eax
	movzwl	213992(%r13,%rax,2), %edx
	movl	252240(%r13), %ecx
	movl	%ecx, %edi
	shrl	$11, %edi
	imull	%edx, %edi
	addq	%rdi, 252248(%r13)
	subl	%edi, %ecx
	movl	%ecx, 252240(%r13)
	movl	%edx, %edi
	shrl	$5, %edi
	subl	%edi, %edx
	movw	%dx, 213992(%r13,%rax,2)
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB24_659
# BB#557:                               #   in Loop: Header=BB24_51 Depth=1
	shll	$8, %ecx
	movl	%ecx, 252240(%r13)
	movq	252248(%r13), %rcx
	movq	%rcx, %rdx
	shrq	$32, %rdx
	jne	.LBB24_650
# BB#558:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$-16777216, %ecx        # imm = 0xFF000000
	jb	.LBB24_650
# BB#559:                               # %._crit_edge.i361
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	252256(%r13), %rdx
	incq	%rdx
	jmp	.LBB24_658
.LBB24_560:                             #   in Loop: Header=BB24_51 Depth=1
	movl	210408(%r13), %eax
	movzwl	213992(%r13,%rax,2), %edx
	movl	252240(%r13), %ecx
	shrl	$11, %ecx
	imull	%edx, %ecx
	movl	%ecx, 252240(%r13)
	movl	$2048, %edi             # imm = 0x800
	subl	%edx, %edi
	shrl	$5, %edi
	addl	%edx, %edi
	movw	%di, 213992(%r13,%rax,2)
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB24_678
# BB#561:                               #   in Loop: Header=BB24_51 Depth=1
	shll	$8, %ecx
	movl	%ecx, 252240(%r13)
	movq	252248(%r13), %rcx
	movq	%rcx, %rdx
	shrq	$32, %rdx
	jne	.LBB24_669
# BB#562:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$-16777216, %ecx        # imm = 0xFF000000
	jb	.LBB24_669
# BB#563:                               # %._crit_edge.i335
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	252256(%r13), %rdx
	incq	%rdx
	jmp	.LBB24_677
.LBB24_564:                             #   in Loop: Header=BB24_51 Depth=1
	movb	252244(%r13), %al
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_565:                             # %RangeEnc_FlushStream.exit._crit_edge.i416
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rcx
	addb	%al, %cl
	movb	%cl, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_570
# BB#566:                               #   in Loop: Header=BB24_565 Depth=2
	cmpl	$0, 252304(%r13)
	jne	.LBB24_570
# BB#567:                               #   in Loop: Header=BB24_565 Depth=2
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_569
# BB#568:                               #   in Loop: Header=BB24_565 Depth=2
	movl	$9, 252304(%r13)
.LBB24_569:                             #   in Loop: Header=BB24_565 Depth=2
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_570:                             # %RangeEnc_FlushStream.exit.i417
                                        #   in Loop: Header=BB24_565 Depth=2
	decq	252256(%r13)
	movq	252248(%r13), %rcx
	movb	$-1, %al
	jne	.LBB24_565
# BB#571:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, 252244(%r13)
	movl	210408(%r13), %eax
	movl	$1, %edx
.LBB24_572:                             # %RangeEnc_ShiftLow.exit419
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	%rdx, 252256(%r13)
	andq	$16777215, %rcx         # imm = 0xFFFFFF
	shlq	$8, %rcx
	movq	%rcx, 252248(%r13)
.LBB24_573:                             # %RangeEnc_EncodeBit.exit408
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%eax, %eax
	movl	kMatchNextStates(,%rax,4), %eax
	movl	%eax, 210408(%r13)
	leal	-2(%r12), %ebp
	xorl	%r8d, %r8d
	cmpl	$0, 252232(%r13)
	sete	%r8b
	movq	360(%rsp), %rdi         # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%ebp, %edx
	movl	%r15d, %ecx
	movq	320(%rsp), %r9          # 8-byte Reload
	callq	LenEnc_Encode2
	leal	-4(%rbx), %edx
	cmpl	$127, %edx
	movl	%edx, 64(%rsp)          # 4-byte Spill
	ja	.LBB24_575
# BB#574:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%edx, %eax
	movzbl	199484(%r13,%rax), %edx
	jmp	.LBB24_576
.LBB24_575:                             #   in Loop: Header=BB24_51 Depth=1
	movl	$524291, %eax           # imm = 0x80003
	subl	%ebx, %eax
	sarl	$31, %eax
	andl	$12, %eax
	leal	6(%rax), %ecx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	movzbl	199484(%r13,%rdx), %ecx
	leal	12(%rcx,%rax,2), %edx
.LBB24_576:                             #   in Loop: Header=BB24_51 Depth=1
	cmpl	$5, %r12d
	movl	%ebp, %ecx
	movl	$3, %eax
	cmovaeq	%rax, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	$6, %r15d
	movl	$1, %ebx
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB24_577:                             #   Parent Loop BB24_51 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_585 Depth 3
	decl	%r15d
	movl	%edx, %r14d
	movl	%edx, %r12d
	movl	%r15d, %ecx
	shrl	%cl, %r12d
	movl	%ebx, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	shlq	$7, %rdi
	addq	%r13, %rdi
	movzwl	214448(%rdi,%rcx,2), %edx
	movl	252240(%r13), %esi
	movl	%esi, %eax
	shrl	$11, %eax
	imull	%edx, %eax
	andl	$1, %r12d
	leaq	214448(%rdi,%rcx,2), %rcx
	jne	.LBB24_579
# BB#578:                               #   in Loop: Header=BB24_577 Depth=2
	movl	%eax, (%rbp)
	movl	$2048, %esi             # imm = 0x800
	subl	%edx, %esi
	shrl	$5, %esi
	addl	%edx, %esi
	jmp	.LBB24_580
	.p2align	4, 0x90
.LBB24_579:                             #   in Loop: Header=BB24_577 Depth=2
	movl	%eax, %edi
	addq	%rdi, 252248(%r13)
	subl	%eax, %esi
	movl	%esi, 252240(%r13)
	movl	%edx, %eax
	shrl	$5, %eax
	subl	%eax, %edx
	movl	%esi, %eax
	movl	%edx, %esi
.LBB24_580:                             #   in Loop: Header=BB24_577 Depth=2
	movw	%si, (%rcx)
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB24_593
# BB#581:                               #   in Loop: Header=BB24_577 Depth=2
	shll	$8, %eax
	movl	%eax, 252240(%r13)
	movq	252248(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB24_584
# BB#582:                               #   in Loop: Header=BB24_577 Depth=2
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB24_584
# BB#583:                               # %._crit_edge.i428
                                        #   in Loop: Header=BB24_577 Depth=2
	movq	252256(%r13), %rcx
	incq	%rcx
	jmp	.LBB24_592
	.p2align	4, 0x90
.LBB24_584:                             #   in Loop: Header=BB24_577 Depth=2
	movb	252244(%r13), %cl
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_585:                             # %RangeEnc_FlushStream.exit._crit_edge.i431
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_577 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_590
# BB#586:                               #   in Loop: Header=BB24_585 Depth=3
	cmpl	$0, 252304(%r13)
	jne	.LBB24_590
# BB#587:                               #   in Loop: Header=BB24_585 Depth=3
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_589
# BB#588:                               #   in Loop: Header=BB24_585 Depth=3
	movl	$9, 252304(%r13)
.LBB24_589:                             #   in Loop: Header=BB24_585 Depth=3
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_590:                             # %RangeEnc_FlushStream.exit.i432
                                        #   in Loop: Header=BB24_585 Depth=3
	decq	252256(%r13)
	movq	252248(%r13), %rax
	movb	$-1, %cl
	jne	.LBB24_585
# BB#591:                               #   in Loop: Header=BB24_577 Depth=2
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 252244(%r13)
	movl	$1, %ecx
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB24_592:                             # %RangeEnc_ShiftLow.exit434
                                        #   in Loop: Header=BB24_577 Depth=2
	movq	%rcx, 252256(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 252248(%r13)
.LBB24_593:                             # %RangeEnc_EncodeBit.exit.i422
                                        #   in Loop: Header=BB24_577 Depth=2
	addl	%ebx, %ebx
	orl	%r12d, %ebx
	testl	%r15d, %r15d
	movl	%r14d, %edx
	jne	.LBB24_577
# BB#594:                               # %RcTree_Encode.exit
                                        #   in Loop: Header=BB24_51 Depth=1
	cmpl	$4, %edx
	jb	.LBB24_648
# BB#595:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%edx, %ebx
	shrl	%ebx
	leal	-1(%rbx), %ecx
	movl	%edx, %eax
	andl	$1, %eax
	orl	$2, %eax
	movl	%ecx, 144(%rsp)         # 4-byte Spill
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movl	64(%rsp), %r12d         # 4-byte Reload
	subl	%eax, %r12d
	cmpl	$13, %r14d
	ja	.LBB24_614
# BB#596:                               # %.lr.ph.i435
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%eax, %eax
	leaq	214960(%r13,%rax,2), %rcx
	movl	%r14d, %eax
	addq	%rax, %rax
	subq	%rax, %rcx
	addq	$-2, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	$1, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB24_597:                             #   Parent Loop BB24_51 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_605 Depth 3
	movl	%ebx, %ebx
	movq	32(%rsp), %rax          # 8-byte Reload
	movzwl	(%rax,%rbx,2), %ecx
	movl	(%rbp), %edx
	movl	%edx, %eax
	shrl	$11, %eax
	imull	%ecx, %eax
	movl	%r12d, %r14d
	andl	$1, %r14d
	jne	.LBB24_599
# BB#598:                               #   in Loop: Header=BB24_597 Depth=2
	movl	%eax, (%rbp)
	movl	$2048, %edx             # imm = 0x800
	subl	%ecx, %edx
	shrl	$5, %edx
	addl	%ecx, %edx
	jmp	.LBB24_600
	.p2align	4, 0x90
.LBB24_599:                             #   in Loop: Header=BB24_597 Depth=2
	movl	%eax, %esi
	addq	%rsi, 252248(%r13)
	subl	%eax, %edx
	movl	%edx, 252240(%r13)
	movl	%ecx, %eax
	shrl	$5, %eax
	subl	%eax, %ecx
	movl	%edx, %eax
	movl	%ecx, %edx
.LBB24_600:                             #   in Loop: Header=BB24_597 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	movw	%dx, (%rcx,%rbx,2)
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB24_613
# BB#601:                               #   in Loop: Header=BB24_597 Depth=2
	shll	$8, %eax
	movl	%eax, 252240(%r13)
	movq	252248(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB24_604
# BB#602:                               #   in Loop: Header=BB24_597 Depth=2
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB24_604
# BB#603:                               # %._crit_edge.i444
                                        #   in Loop: Header=BB24_597 Depth=2
	movq	252256(%r13), %rcx
	incq	%rcx
	jmp	.LBB24_612
	.p2align	4, 0x90
.LBB24_604:                             #   in Loop: Header=BB24_597 Depth=2
	movb	252244(%r13), %cl
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_605:                             # %RangeEnc_FlushStream.exit._crit_edge.i447
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_597 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_610
# BB#606:                               #   in Loop: Header=BB24_605 Depth=3
	cmpl	$0, 252304(%r13)
	jne	.LBB24_610
# BB#607:                               #   in Loop: Header=BB24_605 Depth=3
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_609
# BB#608:                               #   in Loop: Header=BB24_605 Depth=3
	movl	$9, 252304(%r13)
.LBB24_609:                             #   in Loop: Header=BB24_605 Depth=3
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_610:                             # %RangeEnc_FlushStream.exit.i448
                                        #   in Loop: Header=BB24_605 Depth=3
	decq	252256(%r13)
	movq	252248(%r13), %rax
	movb	$-1, %cl
	jne	.LBB24_605
# BB#611:                               #   in Loop: Header=BB24_597 Depth=2
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 252244(%r13)
	movl	$1, %ecx
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB24_612:                             # %RangeEnc_ShiftLow.exit450
                                        #   in Loop: Header=BB24_597 Depth=2
	movq	%rcx, 252256(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 252248(%r13)
.LBB24_613:                             # %RangeEnc_EncodeBit.exit.i438
                                        #   in Loop: Header=BB24_597 Depth=2
	addl	%ebx, %ebx
	orl	%r14d, %ebx
	shrl	%r12d
	incl	%r15d
	cmpl	144(%rsp), %r15d        # 4-byte Folded Reload
	jne	.LBB24_597
	jmp	.LBB24_648
.LBB24_614:                             #   in Loop: Header=BB24_51 Depth=1
	movl	%r12d, %r14d
	shrl	$4, %r14d
	addl	$-5, %ebx
	movq	252248(%r13), %rdx
	.p2align	4, 0x90
.LBB24_615:                             #   Parent Loop BB24_51 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_620 Depth 3
	movl	252240(%r13), %edi
	movl	%edi, %esi
	shrl	%esi
	movl	%esi, 252240(%r13)
	decl	%ebx
	movl	%r14d, %eax
	movl	%ebx, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	negl	%eax
	andl	%esi, %eax
	addq	%rdx, %rax
	movq	%rax, 252248(%r13)
	cmpl	$33554431, %edi         # imm = 0x1FFFFFF
	ja	.LBB24_628
# BB#616:                               #   in Loop: Header=BB24_615 Depth=2
	shll	$8, %esi
	movl	%esi, (%rbp)
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB24_619
# BB#617:                               #   in Loop: Header=BB24_615 Depth=2
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB24_619
# BB#618:                               # %._crit_edge.i456
                                        #   in Loop: Header=BB24_615 Depth=2
	movq	252256(%r13), %rcx
	incq	%rcx
	jmp	.LBB24_627
	.p2align	4, 0x90
.LBB24_619:                             #   in Loop: Header=BB24_615 Depth=2
	movb	252244(%r13), %cl
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_620:                             # %RangeEnc_FlushStream.exit._crit_edge.i459
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_615 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_625
# BB#621:                               #   in Loop: Header=BB24_620 Depth=3
	cmpl	$0, 252304(%r13)
	jne	.LBB24_625
# BB#622:                               #   in Loop: Header=BB24_620 Depth=3
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_624
# BB#623:                               #   in Loop: Header=BB24_620 Depth=3
	movl	$9, 252304(%r13)
.LBB24_624:                             #   in Loop: Header=BB24_620 Depth=3
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_625:                             # %RangeEnc_FlushStream.exit.i460
                                        #   in Loop: Header=BB24_620 Depth=3
	decq	252256(%r13)
	movq	252248(%r13), %rax
	movb	$-1, %cl
	jne	.LBB24_620
# BB#626:                               #   in Loop: Header=BB24_615 Depth=2
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 252244(%r13)
	movl	$1, %ecx
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB24_627:                             # %RangeEnc_ShiftLow.exit462
                                        #   in Loop: Header=BB24_615 Depth=2
	movq	%rcx, 252256(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 252248(%r13)
.LBB24_628:                             #   in Loop: Header=BB24_615 Depth=2
	testl	%ebx, %ebx
	movq	%rax, %rdx
	jne	.LBB24_615
# BB#629:                               # %RangeEnc_EncodeDirectBits.exit
                                        #   in Loop: Header=BB24_51 Depth=1
	andl	$15, %r12d
	movl	$1, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB24_630:                             #   Parent Loop BB24_51 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_638 Depth 3
	movl	%ebx, %ebx
	movzwl	215188(%r13,%rbx,2), %ecx
	movl	252240(%r13), %edx
	movl	%edx, %eax
	shrl	$11, %eax
	imull	%ecx, %eax
	movl	%r12d, %r14d
	andl	$1, %r14d
	jne	.LBB24_632
# BB#631:                               #   in Loop: Header=BB24_630 Depth=2
	movl	%eax, (%rbp)
	movl	$2048, %edx             # imm = 0x800
	subl	%ecx, %edx
	shrl	$5, %edx
	addl	%ecx, %edx
	jmp	.LBB24_633
	.p2align	4, 0x90
.LBB24_632:                             #   in Loop: Header=BB24_630 Depth=2
	movl	%eax, %esi
	addq	%rsi, 252248(%r13)
	subl	%eax, %edx
	movl	%edx, 252240(%r13)
	movl	%ecx, %eax
	shrl	$5, %eax
	subl	%eax, %ecx
	movl	%edx, %eax
	movl	%ecx, %edx
.LBB24_633:                             #   in Loop: Header=BB24_630 Depth=2
	movw	%dx, 215188(%r13,%rbx,2)
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB24_646
# BB#634:                               #   in Loop: Header=BB24_630 Depth=2
	shll	$8, %eax
	movl	%eax, 252240(%r13)
	movq	252248(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB24_637
# BB#635:                               #   in Loop: Header=BB24_630 Depth=2
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB24_637
# BB#636:                               # %._crit_edge.i476
                                        #   in Loop: Header=BB24_630 Depth=2
	movq	252256(%r13), %rcx
	incq	%rcx
	jmp	.LBB24_645
	.p2align	4, 0x90
.LBB24_637:                             #   in Loop: Header=BB24_630 Depth=2
	movb	252244(%r13), %cl
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_638:                             # %RangeEnc_FlushStream.exit._crit_edge.i479
                                        #   Parent Loop BB24_51 Depth=1
                                        #     Parent Loop BB24_630 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_643
# BB#639:                               #   in Loop: Header=BB24_638 Depth=3
	cmpl	$0, 252304(%r13)
	jne	.LBB24_643
# BB#640:                               #   in Loop: Header=BB24_638 Depth=3
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_642
# BB#641:                               #   in Loop: Header=BB24_638 Depth=3
	movl	$9, 252304(%r13)
.LBB24_642:                             #   in Loop: Header=BB24_638 Depth=3
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_643:                             # %RangeEnc_FlushStream.exit.i480
                                        #   in Loop: Header=BB24_638 Depth=3
	decq	252256(%r13)
	movq	252248(%r13), %rax
	movb	$-1, %cl
	jne	.LBB24_638
# BB#644:                               #   in Loop: Header=BB24_630 Depth=2
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 252244(%r13)
	movl	$1, %ecx
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB24_645:                             # %RangeEnc_ShiftLow.exit482
                                        #   in Loop: Header=BB24_630 Depth=2
	movq	%rcx, 252256(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 252248(%r13)
.LBB24_646:                             # %RangeEnc_EncodeBit.exit.i469
                                        #   in Loop: Header=BB24_630 Depth=2
	addl	%ebx, %ebx
	orl	%r14d, %ebx
	shrl	%r12d
	incl	%r15d
	cmpl	$4, %r15d
	jne	.LBB24_630
# BB#647:                               # %RcTree_ReverseEncode.exit471
                                        #   in Loop: Header=BB24_51 Depth=1
	incl	213548(%r13)
.LBB24_648:                             # %RcTree_ReverseEncode.exit
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210400(%r13), %eax
	movl	%eax, 210404(%r13)
	movl	210396(%r13), %eax
	movl	%eax, 210400(%r13)
	movl	210392(%r13), %eax
	movl	%eax, 210396(%r13)
	movl	64(%rsp), %eax          # 4-byte Reload
	movl	%eax, 210392(%r13)
	incl	252328(%r13)
.LBB24_649:                             #   in Loop: Header=BB24_51 Depth=1
	movl	4(%rsp), %r14d          # 4-byte Reload
	movq	8(%rsp), %r12           # 8-byte Reload
	jmp	.LBB24_736
.LBB24_650:                             #   in Loop: Header=BB24_51 Depth=1
	movb	252244(%r13), %al
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_651:                             # %RangeEnc_FlushStream.exit._crit_edge.i364
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rcx
	addb	%al, %cl
	movb	%cl, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_656
# BB#652:                               #   in Loop: Header=BB24_651 Depth=2
	cmpl	$0, 252304(%r13)
	jne	.LBB24_656
# BB#653:                               #   in Loop: Header=BB24_651 Depth=2
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_655
# BB#654:                               #   in Loop: Header=BB24_651 Depth=2
	movl	$9, 252304(%r13)
.LBB24_655:                             #   in Loop: Header=BB24_651 Depth=2
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_656:                             # %RangeEnc_FlushStream.exit.i365
                                        #   in Loop: Header=BB24_651 Depth=2
	decq	252256(%r13)
	movq	252248(%r13), %rcx
	movb	$-1, %al
	jne	.LBB24_651
# BB#657:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, 252244(%r13)
	movl	210408(%r13), %eax
	movl	$1, %edx
	movq	16(%rsp), %rsi          # 8-byte Reload
.LBB24_658:                             # %RangeEnc_ShiftLow.exit367
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	%rdx, 252256(%r13)
	andq	$16777215, %rcx         # imm = 0xFFFFFF
	shlq	$8, %rcx
	movq	%rcx, 252248(%r13)
.LBB24_659:                             # %RangeEnc_EncodeBit.exit356
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%eax, %ecx
	movzwl	214016(%r13,%rcx,2), %ebx
	movl	252240(%r13), %edi
	movl	%edi, %edx
	shrl	$11, %edx
	imull	%ebx, %edx
	cmpl	$1, 32(%rsp)            # 4-byte Folded Reload
	jne	.LBB24_664
# BB#660:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%edx, 252240(%r13)
	movl	$2048, %eax             # imm = 0x800
	subl	%ebx, %eax
	shrl	$5, %eax
	addl	%ebx, %eax
	movw	%ax, 214016(%r13,%rcx,2)
	cmpl	$16777215, %edx         # imm = 0xFFFFFF
	ja	.LBB24_732
# BB#661:                               #   in Loop: Header=BB24_51 Depth=1
	shll	$8, %edx
	movl	%edx, 252240(%r13)
	movq	252248(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB24_695
# BB#662:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB24_695
# BB#663:                               # %._crit_edge.i374
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	252256(%r13), %rcx
	incq	%rcx
	jmp	.LBB24_703
.LBB24_664:                             #   in Loop: Header=BB24_51 Depth=1
	movl	%edx, %ebp
	addq	%rbp, 252248(%r13)
	subl	%edx, %edi
	movl	%edi, 252240(%r13)
	movl	%ebx, %edx
	shrl	$5, %edx
	subl	%edx, %ebx
	movw	%bx, 214016(%r13,%rcx,2)
	cmpl	$16777215, %edi         # imm = 0xFFFFFF
	ja	.LBB24_668
# BB#665:                               #   in Loop: Header=BB24_51 Depth=1
	shll	$8, %edi
	movl	%edi, 252240(%r13)
	movq	252248(%r13), %rcx
	movq	%rcx, %rdx
	shrq	$32, %rdx
	movq	32(%rsp), %rbx          # 8-byte Reload
	jne	.LBB24_704
# BB#666:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$-16777216, %ecx        # imm = 0xFF000000
	jb	.LBB24_704
# BB#667:                               # %._crit_edge.i387
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	252256(%r13), %rdx
	incq	%rdx
	jmp	.LBB24_712
.LBB24_668:                             #   in Loop: Header=BB24_51 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB24_713
.LBB24_669:                             #   in Loop: Header=BB24_51 Depth=1
	movb	252244(%r13), %al
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_670:                             # %RangeEnc_FlushStream.exit._crit_edge.i338
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rcx
	addb	%al, %cl
	movb	%cl, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_675
# BB#671:                               #   in Loop: Header=BB24_670 Depth=2
	cmpl	$0, 252304(%r13)
	jne	.LBB24_675
# BB#672:                               #   in Loop: Header=BB24_670 Depth=2
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_674
# BB#673:                               #   in Loop: Header=BB24_670 Depth=2
	movl	$9, 252304(%r13)
.LBB24_674:                             #   in Loop: Header=BB24_670 Depth=2
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_675:                             # %RangeEnc_FlushStream.exit.i339
                                        #   in Loop: Header=BB24_670 Depth=2
	decq	252256(%r13)
	movq	252248(%r13), %rcx
	movb	$-1, %al
	jne	.LBB24_670
# BB#676:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, 252244(%r13)
	movl	210408(%r13), %eax
	movl	$1, %edx
	movq	16(%rsp), %rsi          # 8-byte Reload
.LBB24_677:                             # %RangeEnc_ShiftLow.exit341
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	%rdx, 252256(%r13)
	andq	$16777215, %rcx         # imm = 0xFFFFFF
	shlq	$8, %rcx
	movq	%rcx, 252248(%r13)
.LBB24_678:                             # %RangeEnc_EncodeBit.exit330
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%eax, %ecx
	shlq	$5, %rcx
	addq	%r13, %rcx
	leaq	214064(%rcx,%r15,2), %rax
	movzwl	214064(%rcx,%r15,2), %edx
	movl	252240(%r13), %ebp
	movl	%ebp, %ecx
	shrl	$11, %ecx
	imull	%edx, %ecx
	cmpl	$1, %r12d
	jne	.LBB24_680
# BB#679:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%ecx, (%rsi)
	movl	$2048, %edi             # imm = 0x800
	subl	%edx, %edi
	shrl	$5, %edi
	addl	%edx, %edi
	jmp	.LBB24_681
.LBB24_680:                             #   in Loop: Header=BB24_51 Depth=1
	movl	%ecx, %edi
	addq	%rdi, 252248(%r13)
	subl	%ecx, %ebp
	movl	%ebp, 252240(%r13)
	movl	%edx, %ecx
	shrl	$5, %ecx
	subl	%ecx, %edx
	movl	%ebp, %ecx
	movl	%edx, %edi
.LBB24_681:                             #   in Loop: Header=BB24_51 Depth=1
	movw	%di, (%rax)
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB24_733
# BB#682:                               #   in Loop: Header=BB24_51 Depth=1
	shll	$8, %ecx
	movl	%ecx, 252240(%r13)
	movq	252248(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB24_685
# BB#683:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB24_685
# BB#684:                               # %._crit_edge.i348
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	252256(%r13), %rcx
	incq	%rcx
	jmp	.LBB24_693
.LBB24_685:                             #   in Loop: Header=BB24_51 Depth=1
	movb	252244(%r13), %cl
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_686:                             # %RangeEnc_FlushStream.exit._crit_edge.i351
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_691
# BB#687:                               #   in Loop: Header=BB24_686 Depth=2
	cmpl	$0, 252304(%r13)
	jne	.LBB24_691
# BB#688:                               #   in Loop: Header=BB24_686 Depth=2
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_690
# BB#689:                               #   in Loop: Header=BB24_686 Depth=2
	movl	$9, 252304(%r13)
.LBB24_690:                             #   in Loop: Header=BB24_686 Depth=2
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_691:                             # %RangeEnc_FlushStream.exit.i352
                                        #   in Loop: Header=BB24_686 Depth=2
	decq	252256(%r13)
	movq	252248(%r13), %rax
	movb	$-1, %cl
	jne	.LBB24_686
# BB#692:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 252244(%r13)
	movl	$1, %ecx
	movq	16(%rsp), %rsi          # 8-byte Reload
.LBB24_693:                             # %RangeEnc_ShiftLow.exit354
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	%rcx, 252256(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 252248(%r13)
	cmpl	$1, %r12d
	je	.LBB24_734
	jmp	.LBB24_694
.LBB24_695:                             #   in Loop: Header=BB24_51 Depth=1
	movb	252244(%r13), %cl
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_696:                             # %RangeEnc_FlushStream.exit._crit_edge.i377
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_701
# BB#697:                               #   in Loop: Header=BB24_696 Depth=2
	cmpl	$0, 252304(%r13)
	jne	.LBB24_701
# BB#698:                               #   in Loop: Header=BB24_696 Depth=2
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_700
# BB#699:                               #   in Loop: Header=BB24_696 Depth=2
	movl	$9, 252304(%r13)
.LBB24_700:                             #   in Loop: Header=BB24_696 Depth=2
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_701:                             # %RangeEnc_FlushStream.exit.i378
                                        #   in Loop: Header=BB24_696 Depth=2
	decq	252256(%r13)
	movq	252248(%r13), %rax
	movb	$-1, %cl
	jne	.LBB24_696
# BB#702:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 252244(%r13)
	movl	$1, %ecx
	movq	16(%rsp), %rsi          # 8-byte Reload
.LBB24_703:                             # %RangeEnc_ShiftLow.exit380
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	%rcx, 252256(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 252248(%r13)
	jmp	.LBB24_732
.LBB24_704:                             #   in Loop: Header=BB24_51 Depth=1
	movb	252244(%r13), %al
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_705:                             # %RangeEnc_FlushStream.exit._crit_edge.i390
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rcx
	addb	%al, %cl
	movb	%cl, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_710
# BB#706:                               #   in Loop: Header=BB24_705 Depth=2
	cmpl	$0, 252304(%r13)
	jne	.LBB24_710
# BB#707:                               #   in Loop: Header=BB24_705 Depth=2
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_709
# BB#708:                               #   in Loop: Header=BB24_705 Depth=2
	movl	$9, 252304(%r13)
.LBB24_709:                             #   in Loop: Header=BB24_705 Depth=2
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_710:                             # %RangeEnc_FlushStream.exit.i391
                                        #   in Loop: Header=BB24_705 Depth=2
	decq	252256(%r13)
	movq	252248(%r13), %rcx
	movb	$-1, %al
	jne	.LBB24_705
# BB#711:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, 252244(%r13)
	movl	210408(%r13), %eax
	movl	$1, %edx
.LBB24_712:                             # %RangeEnc_ShiftLow.exit393
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rdx, 252256(%r13)
	andq	$16777215, %rcx         # imm = 0xFFFFFF
	shlq	$8, %rcx
	movq	%rcx, 252248(%r13)
.LBB24_713:                             # %RangeEnc_EncodeBit.exit382
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	%eax, %eax
	movzwl	214040(%r13,%rax,2), %edx
	movl	252240(%r13), %ebp
	movl	%ebp, %ecx
	shrl	$11, %ecx
	imull	%edx, %ecx
	cmpl	$2, %ebx
	jne	.LBB24_715
# BB#714:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%ecx, (%rsi)
	movl	$2048, %edi             # imm = 0x800
	subl	%edx, %edi
	shrl	$5, %edi
	addl	%edx, %edi
	jmp	.LBB24_716
.LBB24_715:                             #   in Loop: Header=BB24_51 Depth=1
	movl	%ecx, %edi
	addq	%rdi, 252248(%r13)
	subl	%ecx, %ebp
	movl	%ebp, 252240(%r13)
	movl	%edx, %ecx
	shrl	$5, %ecx
	subl	%ecx, %edx
	movl	%ebp, %ecx
	movl	%edx, %edi
.LBB24_716:                             #   in Loop: Header=BB24_51 Depth=1
	movw	%di, 214040(%r13,%rax,2)
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB24_729
# BB#717:                               #   in Loop: Header=BB24_51 Depth=1
	shll	$8, %ecx
	movl	%ecx, 252240(%r13)
	movq	252248(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB24_720
# BB#718:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB24_720
# BB#719:                               # %._crit_edge.i400
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	252256(%r13), %rcx
	incq	%rcx
	jmp	.LBB24_728
.LBB24_720:                             #   in Loop: Header=BB24_51 Depth=1
	movb	252244(%r13), %cl
	movq	252264(%r13), %rbp
	.p2align	4, 0x90
.LBB24_721:                             # %RangeEnc_FlushStream.exit._crit_edge.i403
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 252264(%r13)
	cmpq	252272(%r13), %rbp
	jne	.LBB24_726
# BB#722:                               #   in Loop: Header=BB24_721 Depth=2
	cmpl	$0, 252304(%r13)
	jne	.LBB24_726
# BB#723:                               #   in Loop: Header=BB24_721 Depth=2
	movq	252280(%r13), %rsi
	movq	252288(%r13), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB24_725
# BB#724:                               #   in Loop: Header=BB24_721 Depth=2
	movl	$9, 252304(%r13)
.LBB24_725:                             #   in Loop: Header=BB24_721 Depth=2
	addq	%rbp, 252296(%r13)
	movq	252280(%r13), %rbp
	movq	%rbp, 252264(%r13)
	.p2align	4, 0x90
.LBB24_726:                             # %RangeEnc_FlushStream.exit.i404
                                        #   in Loop: Header=BB24_721 Depth=2
	decq	252256(%r13)
	movq	252248(%r13), %rax
	movb	$-1, %cl
	jne	.LBB24_721
# BB#727:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 252244(%r13)
	movl	$1, %ecx
	movq	16(%rsp), %rsi          # 8-byte Reload
.LBB24_728:                             # %RangeEnc_ShiftLow.exit406
                                        #   in Loop: Header=BB24_51 Depth=1
	movq	%rcx, 252256(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 252248(%r13)
.LBB24_729:                             # %RangeEnc_EncodeBit.exit395
                                        #   in Loop: Header=BB24_51 Depth=1
	cmpl	$3, %ebx
	jne	.LBB24_731
# BB#730:                               #   in Loop: Header=BB24_51 Depth=1
	movl	210400(%r13), %eax
	movl	%eax, 210404(%r13)
.LBB24_731:                             #   in Loop: Header=BB24_51 Depth=1
	movl	210396(%r13), %eax
	movl	%eax, 210400(%r13)
.LBB24_732:                             # %RangeEnc_EncodeBit.exit369
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	210392(%r13), %eax
	movl	%eax, 210396(%r13)
	movl	%r14d, 210392(%r13)
	movl	4(%rsp), %r14d          # 4-byte Reload
.LBB24_733:                             # %RangeEnc_EncodeBit.exit343
                                        #   in Loop: Header=BB24_51 Depth=1
	cmpl	$1, %r12d
	jne	.LBB24_694
.LBB24_734:                             #   in Loop: Header=BB24_51 Depth=1
	movl	$kShortRepNextStates, %eax
	jmp	.LBB24_735
.LBB24_694:                             #   in Loop: Header=BB24_51 Depth=1
	leal	-2(%r12), %edx
	xorl	%r8d, %r8d
	cmpl	$0, 252232(%r13)
	sete	%r8b
	movq	352(%rsp), %rdi         # 8-byte Reload
	movl	%r15d, %ecx
	movq	320(%rsp), %r9          # 8-byte Reload
	callq	LenEnc_Encode2
	movl	$kRepNextStates, %eax
.LBB24_735:                             #   in Loop: Header=BB24_51 Depth=1
	movl	210408(%r13), %ecx
	movl	(%rax,%rcx,4), %eax
	movl	%eax, 210408(%r13)
.LBB24_736:                             #   in Loop: Header=BB24_51 Depth=1
	movl	210388(%r13), %eax
	subl	%r12d, %eax
	movl	%eax, 210388(%r13)
	addl	%r12d, %r14d
	testl	%eax, %eax
	jne	.LBB24_51
# BB#737:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$0, 252232(%r13)
	jne	.LBB24_744
# BB#738:                               #   in Loop: Header=BB24_51 Depth=1
	cmpl	$128, 252328(%r13)
	jb	.LBB24_740
# BB#739:                               #   in Loop: Header=BB24_51 Depth=1
	movq	%r13, %rdi
	callq	FillDistancesPrices
.LBB24_740:                             #   in Loop: Header=BB24_51 Depth=1
	cmpl	$16, 213548(%r13)
	jb	.LBB24_744
# BB#741:                               # %.lr.ph.i.preheader.i.preheader
                                        #   in Loop: Header=BB24_51 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB24_742:                             # %.lr.ph.i.preheader.i
                                        #   Parent Loop BB24_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %edx
	andl	$1, %edx
	movl	%eax, %ecx
	shrl	%ecx
	movzwl	215190(%r13), %esi
	leal	2(%rdx), %edi
	andl	$1, %ecx
	leal	4(%rcx,%rdx,2), %ebp
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	negl	%edx
	andl	$2032, %edx             # imm = 0x7F0
	xorl	%esi, %edx
	shrl	$4, %edx
	movl	%eax, %esi
	shrl	$2, %esi
	movzwl	215188(%r13,%rdi,2), %edi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	negl	%ecx
	andl	$2032, %ecx             # imm = 0x7F0
	xorl	%edi, %ecx
	shrl	$4, %ecx
	movl	207676(%r13,%rcx,4), %ecx
	addl	207676(%r13,%rdx,4), %ecx
	andl	$1, %esi
	movl	%eax, %edx
	shrl	$3, %edx
	movzwl	215188(%r13,%rbp,2), %edi
	leal	(%rsi,%rbp,2), %ebp
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	negl	%esi
	andl	$2032, %esi             # imm = 0x7F0
	xorl	%edi, %esi
	shrl	$4, %esi
	addl	207676(%r13,%rsi,4), %ecx
	andl	$1, %edx
	movzwl	215188(%r13,%rbp,2), %esi
	negl	%edx
	andl	$2032, %edx             # imm = 0x7F0
	xorl	%esi, %edx
	shrl	$4, %edx
	addl	207676(%r13,%rdx,4), %ecx
	movl	%ecx, 213484(%r13,%rax,4)
	incq	%rax
	cmpq	$16, %rax
	jne	.LBB24_742
# BB#743:                               # %FillAlignPrices.exit
                                        #   in Loop: Header=BB24_51 Depth=1
	movl	$0, 213548(%r13)
	.p2align	4, 0x90
.LBB24_744:                             #   in Loop: Header=BB24_51 Depth=1
	movq	48(%r13), %rdi
	callq	*16(%r13)
	testl	%eax, %eax
	je	.LBB24_748
# BB#745:                               #   in Loop: Header=BB24_51 Depth=1
	movl	%r14d, %eax
	subl	264(%rsp), %eax         # 4-byte Folded Reload
	cmpl	$0, 300(%rsp)           # 4-byte Folded Reload
	je	.LBB24_749
# BB#746:                               #   in Loop: Header=BB24_51 Depth=1
	addl	$4396, %eax             # imm = 0x112C
	cmpl	296(%rsp), %eax         # 4-byte Folded Reload
	jae	.LBB24_748
# BB#747:                               #   in Loop: Header=BB24_51 Depth=1
	movq	252264(%r13), %rax
	movq	252296(%r13), %rcx
	leaq	8192(%rcx,%rax), %rax
	subq	252280(%r13), %rax
	addq	252256(%r13), %rax
	cmpq	368(%rsp), %rax         # 8-byte Folded Reload
	jb	.LBB24_51
	jmp	.LBB24_748
.LBB24_749:                             #   in Loop: Header=BB24_51 Depth=1
	cmpl	$32768, %eax            # imm = 0x8000
	jb	.LBB24_51
# BB#750:
	movl	%eax, %eax
	addq	%rax, 252320(%r13)
	movl	252340(%r13), %eax
	testl	%eax, %eax
	jne	.LBB24_18
# BB#751:
	cmpl	$0, 252304(%r13)
	je	.LBB24_755
# BB#752:
	movl	$9, 252340(%r13)
	movl	$9, %eax
	cmpl	$0, 1696(%r13)
	jne	.LBB24_16
	jmp	.LBB24_753
.LBB24_748:                             # %CheckErrors.exit489
	movl	%r14d, %eax
	subl	264(%rsp), %eax         # 4-byte Folded Reload
	addq	%rax, 252320(%r13)
	movq	%r13, %rdi
	movl	%r14d, %esi
.LBB24_20:
	addq	$376, %rsp              # imm = 0x178
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	Flush                   # TAILCALL
.LBB24_755:
	xorl	%eax, %eax
	cmpl	$0, 1696(%r13)
	jne	.LBB24_16
.LBB24_753:
	testl	%eax, %eax
	jne	.LBB24_17
# BB#754:
	xorl	%eax, %eax
	jmp	.LBB24_18
.Lfunc_end24:
	.size	LzmaEnc_CodeOneBlock, .Lfunc_end24-LzmaEnc_CodeOneBlock
	.cfi_endproc

	.globl	LzmaEnc_Encode
	.p2align	4, 0x90
	.type	LzmaEnc_Encode,@function
LzmaEnc_Encode:                         # @LzmaEnc_Encode
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi119:
	.cfi_def_cfa_offset 32
.Lcfi120:
	.cfi_offset %rbx, -24
.Lcfi121:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdi, %rbx
	movq	%rdx, 1632(%rbx)
	movl	$1, 252352(%rbx)
	movq	%rsi, 252288(%rbx)
	xorl	%esi, %esi
	movq	%r8, %rdx
	movq	%r9, %rcx
	callq	LzmaEnc_AllocAndInit
	testl	%eax, %eax
	je	.LBB25_2
# BB#1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB25_2:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	LzmaEnc_Encode2         # TAILCALL
.Lfunc_end25:
	.size	LzmaEnc_Encode, .Lfunc_end25-LzmaEnc_Encode
	.cfi_endproc

	.p2align	4, 0x90
	.type	LzmaEnc_Encode2,@function
LzmaEnc_Encode2:                        # @LzmaEnc_Encode2
	.cfi_startproc
# BB#0:                                 # %.preheader
	pushq	%rbp
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 32
.Lcfi125:
	.cfi_offset %rbx, -32
.Lcfi126:
	.cfi_offset %r14, -24
.Lcfi127:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	LzmaEnc_CodeOneBlock
	movl	%eax, %r14d
	testl	%r14d, %r14d
	je	.LBB26_1
.LBB26_9:                               # %._crit_edge
	cmpl	$0, 56(%rbx)
	je	.LBB26_11
.LBB26_10:
	addq	$64, %rbx
	movq	%rbx, %rdi
	callq	MatchFinderMt_ReleaseStream
.LBB26_11:                              # %LzmaEnc_Finish.exit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB26_1:                               # %.lr.ph
	xorl	%r14d, %r14d
	testq	%rbp, %rbp
	je	.LBB26_5
	.p2align	4, 0x90
.LBB26_2:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 252332(%rbx)
	jne	.LBB26_9
# BB#3:                                 #   in Loop: Header=BB26_2 Depth=1
	movq	252320(%rbx), %rsi
	movq	252264(%rbx), %rdx
	addq	252296(%rbx), %rdx
	subq	252280(%rbx), %rdx
	addq	252256(%rbx), %rdx
	movq	%rbp, %rdi
	callq	*(%rbp)
	testl	%eax, %eax
	jne	.LBB26_4
# BB#7:                                 # %.backedge
                                        #   in Loop: Header=BB26_2 Depth=1
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	LzmaEnc_CodeOneBlock
	testl	%eax, %eax
	je	.LBB26_2
	jmp	.LBB26_8
	.p2align	4, 0x90
.LBB26_5:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 252332(%rbx)
	jne	.LBB26_9
# BB#6:                                 # %.backedge.us
                                        #   in Loop: Header=BB26_5 Depth=1
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	LzmaEnc_CodeOneBlock
	testl	%eax, %eax
	je	.LBB26_5
.LBB26_8:
	movl	%eax, %r14d
	cmpl	$0, 56(%rbx)
	jne	.LBB26_10
	jmp	.LBB26_11
.LBB26_4:
	movl	$10, %r14d
	cmpl	$0, 56(%rbx)
	jne	.LBB26_10
	jmp	.LBB26_11
.Lfunc_end26:
	.size	LzmaEnc_Encode2, .Lfunc_end26-LzmaEnc_Encode2
	.cfi_endproc

	.globl	LzmaEnc_WriteProperties
	.p2align	4, 0x90
	.type	LzmaEnc_WriteProperties,@function
LzmaEnc_WriteProperties:                # @LzmaEnc_WriteProperties
	.cfi_startproc
# BB#0:
	movl	$5, %eax
	cmpq	$5, (%rdx)
	jb	.LBB27_4
# BB#1:
	movl	252344(%rdi), %eax
	movq	$5, (%rdx)
	imull	$5, 213564(%rdi), %ecx
	addl	213560(%rdi), %ecx
	leal	(%rcx,%rcx,8), %ecx
	addl	213556(%rdi), %ecx
	movb	%cl, (%rsi)
	movl	$4096, %ecx             # imm = 0x1000
	cmpl	$4096, %eax             # imm = 0x1000
	jbe	.LBB27_3
# BB#2:
	movl	$6144, %ecx             # imm = 0x1800
	cmpl	$6144, %eax             # imm = 0x1800
	jbe	.LBB27_3
# BB#5:
	movl	$8192, %ecx             # imm = 0x2000
	cmpl	$8192, %eax             # imm = 0x2000
	jbe	.LBB27_3
# BB#6:
	movl	$12288, %ecx            # imm = 0x3000
	cmpl	$12289, %eax            # imm = 0x3001
	jb	.LBB27_3
# BB#7:
	movl	$16384, %ecx            # imm = 0x4000
	cmpl	$16385, %eax            # imm = 0x4001
	jb	.LBB27_3
# BB#8:
	movl	$24576, %ecx            # imm = 0x6000
	cmpl	$24577, %eax            # imm = 0x6001
	jb	.LBB27_3
# BB#9:
	movl	$32768, %ecx            # imm = 0x8000
	cmpl	$32769, %eax            # imm = 0x8001
	jb	.LBB27_3
# BB#10:
	movl	$49152, %ecx            # imm = 0xC000
	cmpl	$49153, %eax            # imm = 0xC001
	jb	.LBB27_3
# BB#11:
	movl	$65536, %ecx            # imm = 0x10000
	cmpl	$65537, %eax            # imm = 0x10001
	jb	.LBB27_3
# BB#12:
	movl	$98304, %ecx            # imm = 0x18000
	cmpl	$98305, %eax            # imm = 0x18001
	jb	.LBB27_3
# BB#13:
	movl	$131072, %ecx           # imm = 0x20000
	cmpl	$131073, %eax           # imm = 0x20001
	jb	.LBB27_3
# BB#14:
	movl	$196608, %ecx           # imm = 0x30000
	cmpl	$196609, %eax           # imm = 0x30001
	jb	.LBB27_3
# BB#15:
	movl	$262144, %ecx           # imm = 0x40000
	cmpl	$262145, %eax           # imm = 0x40001
	jb	.LBB27_3
# BB#16:
	movl	$393216, %ecx           # imm = 0x60000
	cmpl	$393217, %eax           # imm = 0x60001
	jb	.LBB27_3
# BB#17:
	movl	$524288, %ecx           # imm = 0x80000
	cmpl	$524289, %eax           # imm = 0x80001
	jb	.LBB27_3
# BB#18:
	movl	$786432, %ecx           # imm = 0xC0000
	cmpl	$786433, %eax           # imm = 0xC0001
	jb	.LBB27_3
# BB#19:
	movl	$1048576, %ecx          # imm = 0x100000
	cmpl	$1048577, %eax          # imm = 0x100001
	jb	.LBB27_3
# BB#20:
	movl	$1572864, %ecx          # imm = 0x180000
	cmpl	$1572865, %eax          # imm = 0x180001
	jb	.LBB27_3
# BB#21:
	movl	$2097152, %ecx          # imm = 0x200000
	cmpl	$2097153, %eax          # imm = 0x200001
	jb	.LBB27_3
# BB#22:
	movl	$3145728, %ecx          # imm = 0x300000
	cmpl	$3145729, %eax          # imm = 0x300001
	jb	.LBB27_3
# BB#23:
	movl	$4194304, %ecx          # imm = 0x400000
	cmpl	$4194305, %eax          # imm = 0x400001
	jb	.LBB27_3
# BB#24:
	movl	$6291456, %ecx          # imm = 0x600000
	cmpl	$6291457, %eax          # imm = 0x600001
	jb	.LBB27_3
# BB#25:
	movl	$8388608, %ecx          # imm = 0x800000
	cmpl	$8388609, %eax          # imm = 0x800001
	jb	.LBB27_3
# BB#26:
	movl	$12582912, %ecx         # imm = 0xC00000
	cmpl	$12582913, %eax         # imm = 0xC00001
	jb	.LBB27_3
# BB#27:
	movl	$16777216, %ecx         # imm = 0x1000000
	cmpl	$16777217, %eax         # imm = 0x1000001
	jb	.LBB27_3
# BB#28:
	movl	$25165824, %ecx         # imm = 0x1800000
	cmpl	$25165825, %eax         # imm = 0x1800001
	jb	.LBB27_3
# BB#29:
	movl	$33554432, %ecx         # imm = 0x2000000
	cmpl	$33554433, %eax         # imm = 0x2000001
	jb	.LBB27_3
# BB#30:
	movl	$50331648, %ecx         # imm = 0x3000000
	cmpl	$50331649, %eax         # imm = 0x3000001
	jb	.LBB27_3
# BB#31:
	movl	$67108864, %ecx         # imm = 0x4000000
	cmpl	$67108865, %eax         # imm = 0x4000001
	jb	.LBB27_3
# BB#32:
	movl	$100663296, %ecx        # imm = 0x6000000
	cmpl	$100663297, %eax        # imm = 0x6000001
	jb	.LBB27_3
# BB#33:
	movl	$134217728, %ecx        # imm = 0x8000000
	cmpl	$134217729, %eax        # imm = 0x8000001
	jb	.LBB27_3
# BB#34:
	movl	$201326592, %ecx        # imm = 0xC000000
	cmpl	$201326593, %eax        # imm = 0xC000001
	jb	.LBB27_3
# BB#35:
	movl	$268435456, %ecx        # imm = 0x10000000
	cmpl	$268435457, %eax        # imm = 0x10000001
	jb	.LBB27_3
# BB#36:
	movl	$402653184, %ecx        # imm = 0x18000000
	cmpl	$402653185, %eax        # imm = 0x18000001
	jb	.LBB27_3
# BB#37:
	movl	$536870912, %ecx        # imm = 0x20000000
	cmpl	$536870913, %eax        # imm = 0x20000001
	jb	.LBB27_3
# BB#38:
	movl	$805306368, %ecx        # imm = 0x30000000
	cmpl	$805306369, %eax        # imm = 0x30000001
	jb	.LBB27_3
# BB#39:
	movl	$1073741824, %ecx       # imm = 0x40000000
	cmpl	$1073741825, %eax       # imm = 0x40000001
	jb	.LBB27_3
# BB#40:
	movl	$1610612736, %ecx       # imm = 0x60000000
	cmpl	$1610612737, %eax       # imm = 0x60000001
	jb	.LBB27_3
# BB#41:
	movl	$-2147483648, %ecx      # imm = 0x80000000
	cmpl	$-2147483647, %eax      # imm = 0x80000001
	jb	.LBB27_3
# BB#42:
	cmpl	$-1073741824, %eax      # imm = 0xC0000000
	movl	$-1073741824, %ecx      # imm = 0xC0000000
	cmoval	%eax, %ecx
.LBB27_3:                               # %.loopexit.loopexit30
	movb	%cl, 1(%rsi)
	movb	%ch, 2(%rsi)  # NOREX
	movl	%ecx, %eax
	shrl	$16, %eax
	movb	%al, 3(%rsi)
	shrl	$24, %ecx
	movb	%cl, 4(%rsi)
	xorl	%eax, %eax
.LBB27_4:                               # %.loopexit
	retq
.Lfunc_end27:
	.size	LzmaEnc_WriteProperties, .Lfunc_end27-LzmaEnc_WriteProperties
	.cfi_endproc

	.globl	LzmaEnc_MemEncode
	.p2align	4, 0x90
	.type	LzmaEnc_MemEncode,@function
LzmaEnc_MemEncode:                      # @LzmaEnc_MemEncode
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi130:
	.cfi_def_cfa_offset 64
.Lcfi131:
	.cfi_offset %rbx, -24
.Lcfi132:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movq	72(%rsp), %rdx
	movq	80(%rsp), %rax
	movq	%r8, 1664(%rbx)
	movq	$MyWrite, 8(%rsp)
	movq	%rsi, 16(%rsp)
	movq	(%r14), %rsi
	movq	%rsi, 24(%rsp)
	movl	$0, 32(%rsp)
	movl	%r9d, 252312(%rbx)
	leaq	8(%rsp), %rsi
	movq	%rsi, 252288(%rbx)
	movl	$1, 1660(%rbx)
	movq	%rcx, 1624(%rbx)
	movq	%r8, 1664(%rbx)
	movl	$1, 252352(%rbx)
	xorl	%esi, %esi
	movq	%rax, %rcx
	callq	LzmaEnc_AllocAndInit
	testl	%eax, %eax
	jne	.LBB28_2
# BB#1:
	movq	64(%rsp), %rsi
	movq	%rbx, %rdi
	callq	LzmaEnc_Encode2
.LBB28_2:
	movq	24(%rsp), %rcx
	subq	%rcx, (%r14)
	cmpl	$0, 32(%rsp)
	movl	$7, %ecx
	cmovel	%eax, %ecx
	movl	%ecx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end28:
	.size	LzmaEnc_MemEncode, .Lfunc_end28-LzmaEnc_MemEncode
	.cfi_endproc

	.globl	LzmaEncode
	.p2align	4, 0x90
	.type	LzmaEncode,@function
LzmaEncode:                             # @LzmaEncode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi135:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi136:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi137:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi139:
	.cfi_def_cfa_offset 128
.Lcfi140:
	.cfi_offset %rbx, -56
.Lcfi141:
	.cfi_offset %r12, -48
.Lcfi142:
	.cfi_offset %r13, -40
.Lcfi143:
	.cfi_offset %r14, -32
.Lcfi144:
	.cfi_offset %r15, -24
.Lcfi145:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %r12
	movq	%rcx, %r13
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	152(%rsp), %r14
	movl	$291032, %esi           # imm = 0x470D8
	movq	%r14, %rdi
	callq	*(%r14)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB29_1
# BB#2:
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	160(%rsp), %r13
	movq	%rbx, %rdi
	callq	LzmaEnc_Construct
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	LzmaEnc_SetProps
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB29_7
# BB#3:
	movq	128(%rsp), %rdx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	LzmaEnc_WriteProperties
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB29_7
# BB#4:
	movl	136(%rsp), %eax
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, 1664(%rbx)
	movq	$MyWrite, 40(%rsp)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 48(%rsp)
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rcx
	movq	%rcx, 56(%rsp)
	movl	$0, 64(%rsp)
	movl	%eax, 252312(%rbx)
	leaq	40(%rsp), %rax
	movq	%rax, 252288(%rbx)
	movl	$1, 1660(%rbx)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1624(%rbx)
	movq	%rdx, 1664(%rbx)
	movl	$1, 252352(%rbx)
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movq	%r13, %r15
	movq	%r13, %rcx
	callq	LzmaEnc_AllocAndInit
	testl	%eax, %eax
	jne	.LBB29_6
# BB#5:
	movq	144(%rsp), %rsi
	movq	%rbx, %rdi
	callq	LzmaEnc_Encode2
.LBB29_6:                               # %LzmaEnc_MemEncode.exit
	movq	56(%rsp), %rcx
	subq	%rcx, (%rbp)
	cmpl	$0, 64(%rsp)
	movl	$7, %ebp
	cmovel	%eax, %ebp
	movq	%r15, %r13
.LBB29_7:
	leaq	64(%rbx), %rdi
	movq	%r13, %rsi
	callq	MatchFinderMt_Destruct
	movq	%rbx, %rdi
	addq	$1560, %rdi             # imm = 0x618
	movq	%r13, %rsi
	callq	MatchFinder_Free
	movq	213576(%rbx), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	252360(%rbx), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	$0, 213576(%rbx)
	movq	$0, 252360(%rbx)
	movq	252280(%rbx), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	movq	$0, 252280(%rbx)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*8(%r14)
	jmp	.LBB29_8
.LBB29_1:
	movl	$2, %ebp
.LBB29_8:                               # %LzmaEnc_Create.exit.thread
	movl	%ebp, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end29:
	.size	LzmaEncode, .Lfunc_end29-LzmaEncode
	.cfi_endproc

	.p2align	4, 0x90
	.type	LenPriceEnc_UpdateTable,@function
LenPriceEnc_UpdateTable:                # @LenPriceEnc_UpdateTable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi148:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi149:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi150:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 56
.Lcfi152:
	.cfi_offset %rbx, -56
.Lcfi153:
	.cfi_offset %r12, -48
.Lcfi154:
	.cfi_offset %r13, -40
.Lcfi155:
	.cfi_offset %r14, -32
.Lcfi156:
	.cfi_offset %r15, -24
.Lcfi157:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	%esi, %r9d
	movzwl	(%rdi), %eax
	shrl	$4, %eax
	movl	(%rdx,%rax,4), %r14d
	xorl	$127, %eax
	movl	(%rdx,%rax,4), %r8d
	movzwl	2(%rdi), %eax
	shrl	$4, %eax
	movl	(%rdx,%rax,4), %r10d
	addl	%r8d, %r10d
	xorl	$127, %eax
	addl	(%rdx,%rax,4), %r8d
	shll	$3, %esi
	leaq	4(%rdi,%rsi,2), %r12
	movl	18436(%rdi), %r11d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB30_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_3 Depth 2
	cmpq	%r11, %r15
	jae	.LBB30_16
# BB#2:                                 #   in Loop: Header=BB30_1 Depth=1
	movl	%r15d, %eax
	orl	$8, %eax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB30_3:                               # %.lr.ph.i.i
                                        #   Parent Loop BB30_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ecx
	shrl	%ecx
	leal	(%rcx,%rcx), %ebx
	movzwl	(%r12,%rbx), %ebx
	andl	$1, %eax
	negl	%eax
	andl	$2032, %eax             # imm = 0x7F0
	xorl	%ebx, %eax
	shrl	$4, %eax
	addl	(%rdx,%rax,4), %ebp
	cmpl	$1, %ecx
	movl	%ecx, %eax
	jne	.LBB30_3
# BB#4:                                 # %RcTree_GetPrice.exit.i
                                        #   in Loop: Header=BB30_1 Depth=1
	addl	%r14d, %ebp
	imulq	$1088, %r9, %r13        # imm = 0x440
	addq	%rdi, %r13
	movl	%ebp, 1028(%r13,%r15,4)
	incq	%r15
	cmpq	$8, %r15
	jb	.LBB30_1
# BB#5:                                 # %.preheader57.i
	cmpl	$15, %r15d
	ja	.LBB30_11
# BB#6:                                 # %.lr.ph66.i
	leaq	260(%rdi,%rsi,2), %rsi
	movl	%r15d, %r15d
	.p2align	4, 0x90
.LBB30_7:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_9 Depth 2
	cmpq	%r11, %r15
	jae	.LBB30_16
# BB#8:                                 #   in Loop: Header=BB30_7 Depth=1
	leal	-8(%r15), %ebp
	orl	$8, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB30_9:                               # %.lr.ph.i55.i
                                        #   Parent Loop BB30_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %ecx
	shrl	%ecx
	leal	(%rcx,%rcx), %eax
	movzwl	(%rsi,%rax), %eax
	andl	$1, %ebp
	negl	%ebp
	andl	$2032, %ebp             # imm = 0x7F0
	xorl	%eax, %ebp
	shrl	$4, %ebp
	addl	(%rdx,%rbp,4), %ebx
	cmpl	$1, %ecx
	movl	%ecx, %ebp
	jne	.LBB30_9
# BB#10:                                # %RcTree_GetPrice.exit56.i
                                        #   in Loop: Header=BB30_7 Depth=1
	addl	%r10d, %ebx
	movl	%ebx, 1028(%r13,%r15,4)
	incq	%r15
	cmpq	$16, %r15
	jb	.LBB30_7
.LBB30_11:                              # %.preheader.i
	cmpl	%r11d, %r15d
	jae	.LBB30_16
# BB#12:                                # %.lr.ph.preheader.i
	movl	%r15d, %ecx
	.p2align	4, 0x90
.LBB30_13:                              # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_14 Depth 2
	leal	-16(%rcx), %ebp
	orl	$256, %ebp              # imm = 0x100
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB30_14:                              # %.lr.ph.i51.i
                                        #   Parent Loop BB30_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %eax
	shrl	%eax
	leal	(%rax,%rax), %ebx
	movzwl	516(%rdi,%rbx), %ebx
	andl	$1, %ebp
	negl	%ebp
	andl	$2032, %ebp             # imm = 0x7F0
	xorl	%ebx, %ebp
	shrl	$4, %ebp
	addl	(%rdx,%rbp,4), %esi
	cmpl	$1, %eax
	movl	%eax, %ebp
	jne	.LBB30_14
# BB#15:                                # %RcTree_GetPrice.exit52.i
                                        #   in Loop: Header=BB30_13 Depth=1
	addl	%r8d, %esi
	movl	%esi, 1028(%r13,%rcx,4)
	incq	%rcx
	cmpq	%r11, %rcx
	jne	.LBB30_13
.LBB30_16:                              # %LenEnc_SetPrices.exit
	movl	18436(%rdi), %eax
	movl	%eax, 18440(%rdi,%r9,4)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	LenPriceEnc_UpdateTable, .Lfunc_end30-LenPriceEnc_UpdateTable
	.cfi_endproc

	.p2align	4, 0x90
	.type	Flush,@function
Flush:                                  # @Flush
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi158:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi159:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi161:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi162:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi163:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi164:
	.cfi_def_cfa_offset 144
.Lcfi165:
	.cfi_offset %rbx, -56
.Lcfi166:
	.cfi_offset %r12, -48
.Lcfi167:
	.cfi_offset %r13, -40
.Lcfi168:
	.cfi_offset %r14, -32
.Lcfi169:
	.cfi_offset %r15, -24
.Lcfi170:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movl	$1, 252332(%rdi)
	cmpl	$0, 252312(%rdi)
	movq	%rdi, (%rsp)            # 8-byte Spill
	je	.LBB31_1
# BB#2:
	andl	213572(%rdi), %r15d
	leaq	252240(%rdi), %rbx
	movl	210408(%rdi), %r9d
	movq	%rdi, %rax
	movq	%r9, %rdx
	shlq	$5, %rdx
	addq	%rax, %rdx
	movzwl	213584(%rdx,%r15,2), %esi
	movl	252240(%rax), %ecx
	movl	%ecx, %edi
	shrl	$11, %edi
	imull	%esi, %edi
	leaq	252248(%rax), %r8
	addq	%rdi, 252248(%rax)
	subl	%edi, %ecx
	movq	%rax, %rbp
	movl	%ecx, 252240(%rax)
	movl	%esi, %edi
	shrl	$5, %edi
	subl	%edi, %esi
	movw	%si, 213584(%rdx,%r15,2)
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	ja	.LBB31_15
# BB#3:
	shll	$8, %ecx
	movl	%ecx, (%rbx)
	movq	(%r8), %rcx
	movq	%rcx, %rdx
	shrq	$32, %rdx
	jne	.LBB31_6
# BB#4:
	cmpl	$-16777216, %ecx        # imm = 0xFF000000
	jb	.LBB31_6
# BB#5:                                 # %._crit_edge.i.i
	leaq	252256(%rbp), %r14
	movq	252256(%rbp), %rdx
	incq	%rdx
	jmp	.LBB31_14
.LBB31_1:                               # %.WriteEndMarker.exit_crit_edge
	leaq	252248(%rdi), %rbp
	leaq	252244(%rdi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	252264(%rdi), %r10
	leaq	252272(%rdi), %r11
	leaq	252304(%rdi), %r12
	leaq	252280(%rdi), %r8
	leaq	252288(%rdi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	252296(%rdi), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	252256(%rdi), %r15
	jmp	.LBB31_79
.LBB31_6:
	movb	252244(%rbp), %al
	leaq	252256(%rbp), %r14
	movq	252264(%rbp), %rbx
	movq	(%rsp), %rbp            # 8-byte Reload
	.p2align	4, 0x90
.LBB31_7:                               # %RangeEnc_FlushStream.exit._crit_edge.i.i
                                        # =>This Inner Loop Header: Depth=1
	shrq	$32, %rcx
	addb	%al, %cl
	movb	%cl, (%rbx)
	incq	%rbx
	movq	%rbx, 252264(%rbp)
	cmpq	252272(%rbp), %rbx
	jne	.LBB31_12
# BB#8:                                 #   in Loop: Header=BB31_7 Depth=1
	cmpl	$0, 252304(%rbp)
	jne	.LBB31_12
# BB#9:                                 #   in Loop: Header=BB31_7 Depth=1
	movq	252280(%rbp), %rsi
	movq	252288(%rbp), %rdi
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbx
	je	.LBB31_11
# BB#10:                                #   in Loop: Header=BB31_7 Depth=1
	movl	$9, 252304(%rbp)
.LBB31_11:                              #   in Loop: Header=BB31_7 Depth=1
	addq	%rbx, 252296(%rbp)
	movq	252280(%rbp), %rbx
	movq	%rbx, 252264(%rbp)
	movq	24(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB31_12:                              # %RangeEnc_FlushStream.exit.i.i
                                        #   in Loop: Header=BB31_7 Depth=1
	decq	(%r14)
	movq	(%r8), %rcx
	movb	$-1, %al
	jne	.LBB31_7
# BB#13:
	movl	%ecx, %eax
	shrl	$24, %eax
	movq	(%rsp), %rdx            # 8-byte Reload
	movb	%al, 252244(%rdx)
	movl	210408(%rdx), %r9d
	movl	$1, %edx
	movq	56(%rsp), %rbx          # 8-byte Reload
.LBB31_14:                              # %RangeEnc_ShiftLow.exit.i
	movq	%rdx, (%r14)
	andq	$16777215, %rcx         # imm = 0xFFFFFF
	shlq	$8, %rcx
	movq	%rcx, (%r8)
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB31_15:                              # %RangeEnc_EncodeBit.exit.i
	movl	%r9d, %edx
	movzwl	213968(%rbp,%rdx,2), %esi
	movl	252240(%rbp), %ecx
	shrl	$11, %ecx
	imull	%esi, %ecx
	movl	%ecx, 252240(%rbp)
	movl	$2048, %edi             # imm = 0x800
	subl	%esi, %edi
	shrl	$5, %edi
	addl	%esi, %edi
	movw	%di, 213968(%rbp,%rdx,2)
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB31_28
# BB#16:
	shll	$8, %ecx
	movl	%ecx, (%rbx)
	movq	(%r8), %rcx
	movq	%rcx, %rdx
	shrq	$32, %rdx
	jne	.LBB31_19
# BB#17:
	cmpl	$-16777216, %ecx        # imm = 0xFF000000
	jb	.LBB31_19
# BB#18:                                # %._crit_edge.i28.i
	leaq	252256(%rbp), %r14
	movq	252256(%rbp), %rdx
	incq	%rdx
	jmp	.LBB31_27
.LBB31_19:
	movb	252244(%rbp), %al
	leaq	252256(%rbp), %r14
	movq	252264(%rbp), %rbx
	movq	(%rsp), %rbp            # 8-byte Reload
	.p2align	4, 0x90
.LBB31_20:                              # %RangeEnc_FlushStream.exit._crit_edge.i31.i
                                        # =>This Inner Loop Header: Depth=1
	shrq	$32, %rcx
	addb	%al, %cl
	movb	%cl, (%rbx)
	incq	%rbx
	movq	%rbx, 252264(%rbp)
	cmpq	252272(%rbp), %rbx
	jne	.LBB31_25
# BB#21:                                #   in Loop: Header=BB31_20 Depth=1
	cmpl	$0, 252304(%rbp)
	jne	.LBB31_25
# BB#22:                                #   in Loop: Header=BB31_20 Depth=1
	movq	252280(%rbp), %rsi
	movq	252288(%rbp), %rdi
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbx
	je	.LBB31_24
# BB#23:                                #   in Loop: Header=BB31_20 Depth=1
	movl	$9, 252304(%rbp)
.LBB31_24:                              #   in Loop: Header=BB31_20 Depth=1
	addq	%rbx, 252296(%rbp)
	movq	252280(%rbp), %rbx
	movq	%rbx, 252264(%rbp)
	movq	24(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB31_25:                              # %RangeEnc_FlushStream.exit.i32.i
                                        #   in Loop: Header=BB31_20 Depth=1
	decq	(%r14)
	movq	(%r8), %rcx
	movb	$-1, %al
	jne	.LBB31_20
# BB#26:
	movl	%ecx, %eax
	shrl	$24, %eax
	movq	(%rsp), %rdx            # 8-byte Reload
	movb	%al, 252244(%rdx)
	movl	210408(%rdx), %r9d
	movl	$1, %edx
	movq	56(%rsp), %rbx          # 8-byte Reload
.LBB31_27:                              # %RangeEnc_ShiftLow.exit34.i
	movq	%rdx, (%r14)
	andq	$16777215, %rcx         # imm = 0xFFFFFF
	shlq	$8, %rcx
	movq	%rcx, (%r8)
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB31_28:                              # %RangeEnc_EncodeBit.exit23.i
	movl	%r9d, %eax
	movl	kMatchNextStates(,%rax,4), %eax
	movl	%eax, 210408(%rbp)
	leaq	215220(%rbp), %rdi
	xorl	%r8d, %r8d
	cmpl	$0, 252232(%rbp)
	sete	%r8b
	leaq	207676(%rbp), %r9
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movl	%r15d, %ecx
	callq	LenEnc_Encode2
	movq	(%rsp), %rdx            # 8-byte Reload
	leaq	252244(%rdx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	252264(%rdx), %r10
	leaq	252272(%rdx), %r11
	leaq	252304(%rdx), %r9
	leaq	252280(%rdx), %r8
	leaq	252288(%rdx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	252296(%rdx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	252256(%rdx), %r15
	movl	$6, %ecx
	movl	$1, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%r11, 64(%rsp)          # 8-byte Spill
	movq	%r10, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB31_29:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_37 Depth 2
	decl	%ecx
	movl	$63, %r13d
	movl	%ecx, 16(%rsp)          # 4-byte Spill
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r13d
	movl	%eax, %r12d
	movzwl	214448(%rdx,%r12,2), %ecx
	movl	252240(%rdx), %edx
	movl	%edx, %eax
	shrl	$11, %eax
	imull	%ecx, %eax
	andl	$1, %r13d
	jne	.LBB31_31
# BB#30:                                #   in Loop: Header=BB31_29 Depth=1
	movl	%eax, (%rbx)
	movl	$2048, %edx             # imm = 0x800
	subl	%ecx, %edx
	shrl	$5, %edx
	addl	%ecx, %edx
	jmp	.LBB31_32
	.p2align	4, 0x90
.LBB31_31:                              #   in Loop: Header=BB31_29 Depth=1
	movl	%eax, %esi
	addq	%rsi, (%rbp)
	subl	%eax, %edx
	movl	%edx, (%rbx)
	movl	%ecx, %eax
	shrl	$5, %eax
	subl	%eax, %ecx
	movl	%edx, %eax
	movl	%ecx, %edx
.LBB31_32:                              #   in Loop: Header=BB31_29 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	movw	%dx, 214448(%rcx,%r12,2)
	movq	%rcx, %rdx
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB31_45
# BB#33:                                #   in Loop: Header=BB31_29 Depth=1
	shll	$8, %eax
	movl	%eax, (%rbx)
	movq	(%rbp), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB31_36
# BB#34:                                #   in Loop: Header=BB31_29 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB31_36
# BB#35:                                # %._crit_edge.i40.i
                                        #   in Loop: Header=BB31_29 Depth=1
	movq	(%r15), %rcx
	incq	%rcx
	jmp	.LBB31_44
	.p2align	4, 0x90
.LBB31_36:                              #   in Loop: Header=BB31_29 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movb	(%rcx), %cl
	movq	(%r10), %rbx
	.p2align	4, 0x90
.LBB31_37:                              # %RangeEnc_FlushStream.exit._crit_edge.i43.i
                                        #   Parent Loop BB31_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbx)
	incq	%rbx
	movq	%rbx, (%r10)
	cmpq	(%r11), %rbx
	jne	.LBB31_42
# BB#38:                                #   in Loop: Header=BB31_37 Depth=2
	cmpl	$0, (%r9)
	jne	.LBB31_42
# BB#39:                                #   in Loop: Header=BB31_37 Depth=2
	movq	(%r8), %rsi
	subq	%rsi, %rbx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rbx, %rdx
	movq	%r8, %r14
	movq	%r9, %rbp
	callq	*(%rdi)
	movq	%rbp, %r9
	cmpq	%rax, %rbx
	je	.LBB31_41
# BB#40:                                #   in Loop: Header=BB31_37 Depth=2
	movl	$9, (%r9)
.LBB31_41:                              #   in Loop: Header=BB31_37 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	%rbx, (%rax)
	movq	(%r14), %rbx
	movq	48(%rsp), %r10          # 8-byte Reload
	movq	%rbx, (%r10)
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%r14, %r8
	movq	64(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB31_42:                              # %RangeEnc_FlushStream.exit.i44.i
                                        #   in Loop: Header=BB31_37 Depth=2
	decq	(%r15)
	movq	(%rbp), %rax
	movb	$-1, %cl
	jne	.LBB31_37
# BB#43:                                #   in Loop: Header=BB31_29 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movb	%cl, (%rdx)
	movl	$1, %ecx
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	(%rsp), %rdx            # 8-byte Reload
.LBB31_44:                              # %RangeEnc_ShiftLow.exit46.i
                                        #   in Loop: Header=BB31_29 Depth=1
	movq	%rcx, (%r15)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, (%rbp)
.LBB31_45:                              # %RangeEnc_EncodeBit.exit.i.i
                                        #   in Loop: Header=BB31_29 Depth=1
	addl	%r12d, %r12d
	orl	%r12d, %r13d
	movl	16(%rsp), %ecx          # 4-byte Reload
	testl	%ecx, %ecx
	movl	%r13d, %eax
	jne	.LBB31_29
# BB#46:                                # %RcTree_Encode.exit.preheader.i
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movq	(%rbp), %rdx
	movl	$26, %r13d
	movq	16(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB31_47:                              # %RcTree_Encode.exit.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_52 Depth 2
	movl	(%rbx), %r9d
	movl	%r9d, %esi
	shrl	%esi
	movl	%esi, (%rbx)
	decl	%r13d
	movl	$67108863, %eax         # imm = 0x3FFFFFF
	movl	%r13d, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	negl	%eax
	andl	%esi, %eax
	addq	%rdx, %rax
	movq	%rax, (%rbp)
	cmpl	$33554431, %r9d         # imm = 0x1FFFFFF
	ja	.LBB31_60
# BB#48:                                #   in Loop: Header=BB31_47 Depth=1
	shll	$8, %esi
	movl	%esi, (%rbx)
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB31_51
# BB#49:                                #   in Loop: Header=BB31_47 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB31_51
# BB#50:                                # %._crit_edge.i52.i
                                        #   in Loop: Header=BB31_47 Depth=1
	movq	(%r15), %rcx
	incq	%rcx
	jmp	.LBB31_59
	.p2align	4, 0x90
.LBB31_51:                              #   in Loop: Header=BB31_47 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movb	(%rcx), %cl
	movq	(%r10), %rbx
	.p2align	4, 0x90
.LBB31_52:                              # %RangeEnc_FlushStream.exit._crit_edge.i55.i
                                        #   Parent Loop BB31_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbx)
	incq	%rbx
	movq	%rbx, (%r10)
	cmpq	(%r11), %rbx
	jne	.LBB31_57
# BB#53:                                #   in Loop: Header=BB31_52 Depth=2
	cmpl	$0, (%r12)
	jne	.LBB31_57
# BB#54:                                #   in Loop: Header=BB31_52 Depth=2
	movq	(%r8), %rsi
	subq	%rsi, %rbx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rbx, %rdx
	movq	%r8, %r14
	callq	*(%rdi)
	cmpq	%rax, %rbx
	je	.LBB31_56
# BB#55:                                #   in Loop: Header=BB31_52 Depth=2
	movl	$9, (%r12)
.LBB31_56:                              #   in Loop: Header=BB31_52 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	%rbx, (%rax)
	movq	(%r14), %rbx
	movq	48(%rsp), %r10          # 8-byte Reload
	movq	%rbx, (%r10)
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%r14, %r8
	movq	64(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB31_57:                              # %RangeEnc_FlushStream.exit.i56.i
                                        #   in Loop: Header=BB31_52 Depth=2
	decq	(%r15)
	movq	(%rbp), %rax
	movb	$-1, %cl
	jne	.LBB31_52
# BB#58:                                #   in Loop: Header=BB31_47 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movb	%cl, (%rdx)
	movl	$1, %ecx
	movq	56(%rsp), %rbx          # 8-byte Reload
.LBB31_59:                              # %RangeEnc_ShiftLow.exit58.i
                                        #   in Loop: Header=BB31_47 Depth=1
	movq	%rcx, (%r15)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, (%rbp)
.LBB31_60:                              #   in Loop: Header=BB31_47 Depth=1
	testl	%r13d, %r13d
	movq	%rax, %rdx
	jne	.LBB31_47
# BB#61:                                # %RangeEnc_EncodeDirectBits.exit.i.preheader
	movl	$1, %r13d
	xorl	%eax, %eax
	movl	$15, %edi
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	(%rsp), %rdx            # 8-byte Reload
	.p2align	4, 0x90
.LBB31_62:                              # %RangeEnc_EncodeDirectBits.exit.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_70 Depth 2
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movl	%r13d, %r13d
	movzwl	215188(%rdx,%r13,2), %ecx
	movl	252240(%rdx), %edx
	movl	%edx, %eax
	shrl	$11, %eax
	imull	%ecx, %eax
	movl	%edi, %esi
	andl	$1, %esi
	movl	%esi, 80(%rsp)          # 4-byte Spill
	jne	.LBB31_64
# BB#63:                                #   in Loop: Header=BB31_62 Depth=1
	movl	%eax, (%rbx)
	movl	$2048, %edx             # imm = 0x800
	subl	%ecx, %edx
	shrl	$5, %edx
	addl	%ecx, %edx
	jmp	.LBB31_65
	.p2align	4, 0x90
.LBB31_64:                              #   in Loop: Header=BB31_62 Depth=1
	movl	%eax, %esi
	addq	%rsi, (%rbp)
	subl	%eax, %edx
	movl	%edx, (%rbx)
	movl	%ecx, %eax
	shrl	$5, %eax
	subl	%eax, %ecx
	movl	%edx, %eax
	movl	%ecx, %edx
.LBB31_65:                              #   in Loop: Header=BB31_62 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	movw	%dx, 215188(%rcx,%r13,2)
	movq	%rcx, %rdx
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB31_78
# BB#66:                                #   in Loop: Header=BB31_62 Depth=1
	shll	$8, %eax
	movl	%eax, (%rbx)
	movq	(%rbp), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB31_69
# BB#67:                                #   in Loop: Header=BB31_62 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB31_69
# BB#68:                                # %._crit_edge.i66.i
                                        #   in Loop: Header=BB31_62 Depth=1
	movq	(%r15), %rcx
	incq	%rcx
	jmp	.LBB31_77
	.p2align	4, 0x90
.LBB31_69:                              #   in Loop: Header=BB31_62 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movb	(%rcx), %cl
	movq	(%r10), %rbx
	movl	%edi, 76(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB31_70:                              # %RangeEnc_FlushStream.exit._crit_edge.i69.i
                                        #   Parent Loop BB31_62 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbx)
	incq	%rbx
	movq	%rbx, (%r10)
	cmpq	(%r11), %rbx
	jne	.LBB31_75
# BB#71:                                #   in Loop: Header=BB31_70 Depth=2
	cmpl	$0, (%r12)
	jne	.LBB31_75
# BB#72:                                #   in Loop: Header=BB31_70 Depth=2
	movq	(%r8), %rsi
	subq	%rsi, %rbx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rbx, %rdx
	movq	%r8, %r14
	callq	*(%rdi)
	cmpq	%rax, %rbx
	je	.LBB31_74
# BB#73:                                #   in Loop: Header=BB31_70 Depth=2
	movl	$9, (%r12)
.LBB31_74:                              #   in Loop: Header=BB31_70 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	%rbx, (%rax)
	movq	(%r14), %rbx
	movq	48(%rsp), %r10          # 8-byte Reload
	movq	%rbx, (%r10)
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%r14, %r8
	movq	64(%rsp), %r11          # 8-byte Reload
	movl	76(%rsp), %edi          # 4-byte Reload
	.p2align	4, 0x90
.LBB31_75:                              # %RangeEnc_FlushStream.exit.i70.i
                                        #   in Loop: Header=BB31_70 Depth=2
	decq	(%r15)
	movq	(%rbp), %rax
	movb	$-1, %cl
	jne	.LBB31_70
# BB#76:                                #   in Loop: Header=BB31_62 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movb	%cl, (%rdx)
	movl	$1, %ecx
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	(%rsp), %rdx            # 8-byte Reload
.LBB31_77:                              # %RangeEnc_ShiftLow.exit72.i
                                        #   in Loop: Header=BB31_62 Depth=1
	movq	%rcx, (%r15)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, (%rbp)
.LBB31_78:                              # %RangeEnc_EncodeBit.exit.i60.i
                                        #   in Loop: Header=BB31_62 Depth=1
	addl	%r13d, %r13d
	orl	80(%rsp), %r13d         # 4-byte Folded Reload
	shrl	%edi
	movl	84(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	$4, %eax
	jne	.LBB31_62
.LBB31_79:                              # %WriteEndMarker.exit
	movq	(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r10, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB31_80:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_84 Depth 2
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB31_83
# BB#81:                                #   in Loop: Header=BB31_80 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB31_83
# BB#82:                                # %._crit_edge.i.i11
                                        #   in Loop: Header=BB31_80 Depth=1
	movq	(%r15), %rcx
	incq	%rcx
	jmp	.LBB31_91
	.p2align	4, 0x90
.LBB31_83:                              #   in Loop: Header=BB31_80 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movb	(%rcx), %cl
	movq	(%r10), %rbx
	.p2align	4, 0x90
.LBB31_84:                              # %RangeEnc_FlushStream.exit._crit_edge.i.i14
                                        #   Parent Loop BB31_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbx)
	incq	%rbx
	movq	%rbx, (%r10)
	cmpq	(%r11), %rbx
	jne	.LBB31_89
# BB#85:                                #   in Loop: Header=BB31_84 Depth=2
	cmpl	$0, (%r12)
	jne	.LBB31_89
# BB#86:                                #   in Loop: Header=BB31_84 Depth=2
	movq	%r15, %r14
	movq	%r11, %r15
	movq	%r8, %r12
	movq	(%r8), %rsi
	subq	%rsi, %rbx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rbx, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbx
	je	.LBB31_88
# BB#87:                                #   in Loop: Header=BB31_84 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	$9, (%rax)
.LBB31_88:                              #   in Loop: Header=BB31_84 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	%rbx, (%rax)
	movq	%r12, %r8
	movq	(%r8), %rbx
	movq	48(%rsp), %r10          # 8-byte Reload
	movq	%rbx, (%r10)
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	%r15, %r11
	movq	%r14, %r15
	.p2align	4, 0x90
.LBB31_89:                              # %RangeEnc_FlushStream.exit.i.i15
                                        #   in Loop: Header=BB31_84 Depth=2
	decq	(%r15)
	movq	(%rbp), %rax
	movb	$-1, %cl
	jne	.LBB31_84
# BB#90:                                #   in Loop: Header=BB31_80 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movb	%cl, (%rdx)
	movl	$1, %ecx
.LBB31_91:                              # %RangeEnc_ShiftLow.exit.i16
                                        #   in Loop: Header=BB31_80 Depth=1
	movq	%rcx, (%r15)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, (%rbp)
	incl	%r13d
	cmpl	$5, %r13d
	jne	.LBB31_80
# BB#92:                                # %RangeEnc_FlushData.exit
	cmpl	$0, (%r12)
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB31_96
# BB#93:
	movq	(%r10), %rbx
	movq	%r8, %r15
	movq	(%r8), %rsi
	subq	%rsi, %rbx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rbx, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbx
	je	.LBB31_95
# BB#94:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	$9, (%rax)
.LBB31_95:
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	%rbx, (%rax)
	movq	(%r15), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
.LBB31_96:                              # %RangeEnc_FlushStream.exit
	movl	252340(%rbp), %eax
	testl	%eax, %eax
	jne	.LBB31_105
# BB#97:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	$0, (%rax)
	je	.LBB31_98
# BB#99:
	movl	$9, 252340(%rbp)
	movl	$9, %eax
	cmpl	$0, 1696(%rbp)
	jne	.LBB31_101
	jmp	.LBB31_103
.LBB31_98:
	xorl	%eax, %eax
	cmpl	$0, 1696(%rbp)
	je	.LBB31_103
.LBB31_101:                             # %.thread.i
	movl	$8, 252340(%rbp)
	movl	$8, %eax
	jmp	.LBB31_102
.LBB31_103:
	testl	%eax, %eax
	je	.LBB31_104
.LBB31_102:
	movl	$1, 252332(%rbp)
.LBB31_105:                             # %CheckErrors.exit
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB31_104:
	xorl	%eax, %eax
	jmp	.LBB31_105
.Lfunc_end31:
	.size	Flush, .Lfunc_end31-Flush
	.cfi_endproc

	.p2align	4, 0x90
	.type	LenEnc_Encode2,@function
LenEnc_Encode2:                         # @LenEnc_Encode2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi173:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi174:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi175:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi176:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi177:
	.cfi_def_cfa_offset 96
.Lcfi178:
	.cfi_offset %rbx, -56
.Lcfi179:
	.cfi_offset %r12, -48
.Lcfi180:
	.cfi_offset %r13, -40
.Lcfi181:
	.cfi_offset %r14, -32
.Lcfi182:
	.cfi_offset %r15, -24
.Lcfi183:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movl	%r8d, %r12d
	movl	%ecx, %r15d
	movq	%rsi, %rbx
	movzwl	(%rdi), %esi
	movl	(%rbx), %ebp
	movl	%ebp, %ecx
	shrl	$11, %ecx
	imull	%esi, %ecx
	cmpl	$7, %edx
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movl	%r12d, 20(%rsp)         # 4-byte Spill
	ja	.LBB32_32
# BB#1:
	movl	%ecx, (%rbx)
	movl	$2048, %eax             # imm = 0x800
	subl	%esi, %eax
	shrl	$5, %eax
	addl	%esi, %eax
	movw	%ax, (%rdi)
	cmpl	$16777216, %ecx         # imm = 0x1000000
	jae	.LBB32_14
# BB#2:
	shll	$8, %ecx
	movl	%ecx, (%rbx)
	movq	8(%rbx), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB32_5
# BB#3:
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB32_5
# BB#4:                                 # %._crit_edge.i.i
	movq	16(%rbx), %rcx
	incq	%rcx
	jmp	.LBB32_13
.LBB32_32:
	movl	%ecx, %eax
	addq	8(%rbx), %rax
	movq	%rax, 8(%rbx)
	subl	%ecx, %ebp
	movl	%ebp, (%rbx)
	movl	%esi, %edx
	shrl	$5, %edx
	subl	%edx, %esi
	movw	%si, (%rdi)
	cmpl	$16777215, %ebp         # imm = 0xFFFFFF
	ja	.LBB32_45
# BB#33:
	shll	$8, %ebp
	movl	%ebp, (%rbx)
	movq	%rax, %rdx
	shrq	$32, %rdx
	jne	.LBB32_36
# BB#34:
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB32_36
# BB#35:                                # %._crit_edge.i34.i
	movq	16(%rbx), %rdx
	incq	%rdx
	jmp	.LBB32_44
.LBB32_5:
	movb	4(%rbx), %cl
	movq	24(%rbx), %rbp
	.p2align	4, 0x90
.LBB32_6:                               # %RangeEnc_FlushStream.exit._crit_edge.i.i
                                        # =>This Inner Loop Header: Depth=1
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 24(%rbx)
	cmpq	32(%rbx), %rbp
	jne	.LBB32_11
# BB#7:                                 #   in Loop: Header=BB32_6 Depth=1
	cmpl	$0, 64(%rbx)
	jne	.LBB32_11
# BB#8:                                 #   in Loop: Header=BB32_6 Depth=1
	movq	40(%rbx), %rsi
	movq	48(%rbx), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB32_10
# BB#9:                                 #   in Loop: Header=BB32_6 Depth=1
	movl	$9, 64(%rbx)
.LBB32_10:                              #   in Loop: Header=BB32_6 Depth=1
	addq	%rbp, 56(%rbx)
	movq	40(%rbx), %rbp
	movq	%rbp, 24(%rbx)
	.p2align	4, 0x90
.LBB32_11:                              # %RangeEnc_FlushStream.exit.i.i
                                        #   in Loop: Header=BB32_6 Depth=1
	decq	16(%rbx)
	movq	8(%rbx), %rax
	movb	$-1, %cl
	jne	.LBB32_6
# BB#12:
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 4(%rbx)
	movl	$1, %ecx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	4(%rsp), %edx           # 4-byte Reload
.LBB32_13:                              # %RangeEnc_ShiftLow.exit.i
	movq	%rcx, 16(%rbx)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 8(%rbx)
.LBB32_14:                              # %RangeEnc_EncodeBit.exit.i
	leal	(,%r15,8), %eax
	leaq	4(%rdi,%rax,2), %r15
	movl	$3, %r14d
	movl	$1, %eax
	.p2align	4, 0x90
.LBB32_15:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_23 Depth 2
	decl	%r14d
	movl	%edx, %r12d
	movl	%r14d, %ecx
	shrl	%cl, %r12d
	movl	%eax, %r13d
	movzwl	(%r15,%r13,2), %ecx
	movl	(%rbx), %ebp
	movl	%ebp, %eax
	shrl	$11, %eax
	imull	%ecx, %eax
	andl	$1, %r12d
	jne	.LBB32_17
# BB#16:                                #   in Loop: Header=BB32_15 Depth=1
	movl	%eax, (%rbx)
	movl	$2048, %esi             # imm = 0x800
	subl	%ecx, %esi
	shrl	$5, %esi
	addl	%ecx, %esi
	jmp	.LBB32_18
	.p2align	4, 0x90
.LBB32_17:                              #   in Loop: Header=BB32_15 Depth=1
	movl	%eax, %esi
	addq	%rsi, 8(%rbx)
	subl	%eax, %ebp
	movl	%ebp, (%rbx)
	movl	%ecx, %eax
	shrl	$5, %eax
	subl	%eax, %ecx
	movl	%ebp, %eax
	movl	%ecx, %esi
.LBB32_18:                              #   in Loop: Header=BB32_15 Depth=1
	movw	%si, (%r15,%r13,2)
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB32_31
# BB#19:                                #   in Loop: Header=BB32_15 Depth=1
	shll	$8, %eax
	movl	%eax, (%rbx)
	movq	8(%rbx), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB32_22
# BB#20:                                #   in Loop: Header=BB32_15 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB32_22
# BB#21:                                # %._crit_edge.i21.i
                                        #   in Loop: Header=BB32_15 Depth=1
	movq	16(%rbx), %rcx
	incq	%rcx
	jmp	.LBB32_30
	.p2align	4, 0x90
.LBB32_22:                              #   in Loop: Header=BB32_15 Depth=1
	movb	4(%rbx), %cl
	movq	24(%rbx), %rbp
	.p2align	4, 0x90
.LBB32_23:                              # %RangeEnc_FlushStream.exit._crit_edge.i24.i
                                        #   Parent Loop BB32_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 24(%rbx)
	cmpq	32(%rbx), %rbp
	jne	.LBB32_28
# BB#24:                                #   in Loop: Header=BB32_23 Depth=2
	cmpl	$0, 64(%rbx)
	jne	.LBB32_28
# BB#25:                                #   in Loop: Header=BB32_23 Depth=2
	movq	40(%rbx), %rsi
	movq	48(%rbx), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB32_27
# BB#26:                                #   in Loop: Header=BB32_23 Depth=2
	movl	$9, 64(%rbx)
.LBB32_27:                              #   in Loop: Header=BB32_23 Depth=2
	addq	%rbp, 56(%rbx)
	movq	40(%rbx), %rbp
	movq	%rbp, 24(%rbx)
	.p2align	4, 0x90
.LBB32_28:                              # %RangeEnc_FlushStream.exit.i25.i
                                        #   in Loop: Header=BB32_23 Depth=2
	decq	16(%rbx)
	movq	8(%rbx), %rax
	movb	$-1, %cl
	jne	.LBB32_23
# BB#29:                                #   in Loop: Header=BB32_15 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 4(%rbx)
	movl	$1, %ecx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	4(%rsp), %edx           # 4-byte Reload
.LBB32_30:                              # %RangeEnc_ShiftLow.exit27.i
                                        #   in Loop: Header=BB32_15 Depth=1
	movq	%rcx, 16(%rbx)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 8(%rbx)
.LBB32_31:                              # %RangeEnc_EncodeBit.exit.i.i
                                        #   in Loop: Header=BB32_15 Depth=1
	addl	%r13d, %r13d
	orl	%r13d, %r12d
	testl	%r14d, %r14d
	movl	%r12d, %eax
	jne	.LBB32_15
	jmp	.LBB32_110
.LBB32_36:
	movb	4(%rbx), %cl
	movq	24(%rbx), %rbp
	.p2align	4, 0x90
.LBB32_37:                              # %RangeEnc_FlushStream.exit._crit_edge.i37.i
                                        # =>This Inner Loop Header: Depth=1
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 24(%rbx)
	cmpq	32(%rbx), %rbp
	jne	.LBB32_42
# BB#38:                                #   in Loop: Header=BB32_37 Depth=1
	cmpl	$0, 64(%rbx)
	jne	.LBB32_42
# BB#39:                                #   in Loop: Header=BB32_37 Depth=1
	movq	40(%rbx), %rsi
	movq	48(%rbx), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB32_41
# BB#40:                                #   in Loop: Header=BB32_37 Depth=1
	movl	$9, 64(%rbx)
.LBB32_41:                              #   in Loop: Header=BB32_37 Depth=1
	addq	%rbp, 56(%rbx)
	movq	40(%rbx), %rbp
	movq	%rbp, 24(%rbx)
	.p2align	4, 0x90
.LBB32_42:                              # %RangeEnc_FlushStream.exit.i38.i
                                        #   in Loop: Header=BB32_37 Depth=1
	decq	16(%rbx)
	movq	8(%rbx), %rax
	movb	$-1, %cl
	jne	.LBB32_37
# BB#43:
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 4(%rbx)
	movl	(%rbx), %ebp
	movl	$1, %edx
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB32_44:                              # %RangeEnc_ShiftLow.exit40.i
	movq	%rdx, 16(%rbx)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 8(%rbx)
.LBB32_45:                              # %RangeEnc_EncodeBit.exit29.i
	movzwl	2(%rdi), %r8d
	movl	%ebp, %esi
	shrl	$11, %esi
	imull	%r8d, %esi
	cmpl	$15, 4(%rsp)            # 4-byte Folded Reload
	ja	.LBB32_78
# BB#46:
	movl	%esi, (%rbx)
	movl	$2048, %ecx             # imm = 0x800
	subl	%r8d, %ecx
	shrl	$5, %ecx
	addl	%r8d, %ecx
	movw	%cx, 2(%rdi)
	cmpl	$16777215, %esi         # imm = 0xFFFFFF
	ja	.LBB32_47
# BB#48:
	shll	$8, %esi
	movl	%esi, (%rbx)
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB32_51
# BB#49:
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB32_51
# BB#50:                                # %._crit_edge.i47.i
	movq	16(%rbx), %rcx
	incq	%rcx
	jmp	.LBB32_59
.LBB32_78:
	movl	%esi, %edx
	addq	%rax, %rdx
	movq	%rdx, 8(%rbx)
	subl	%esi, %ebp
	movl	%ebp, (%rbx)
	movl	%r8d, %eax
	shrl	$5, %eax
	subl	%eax, %r8d
	movw	%r8w, 2(%rdi)
	cmpl	$16777215, %ebp         # imm = 0xFFFFFF
	ja	.LBB32_79
# BB#80:
	shll	$8, %ebp
	movl	%ebp, (%rbx)
	movq	%rdx, %rax
	shrq	$32, %rax
	jne	.LBB32_83
# BB#81:
	cmpl	$-16777216, %edx        # imm = 0xFF000000
	jb	.LBB32_83
# BB#82:                                # %._crit_edge.i77.i
	movq	16(%rbx), %rax
	incq	%rax
	jmp	.LBB32_91
.LBB32_47:
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB32_60
.LBB32_79:
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB32_92
.LBB32_51:
	movb	4(%rbx), %cl
	movq	24(%rbx), %rbp
	.p2align	4, 0x90
.LBB32_52:                              # %RangeEnc_FlushStream.exit._crit_edge.i50.i
                                        # =>This Inner Loop Header: Depth=1
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 24(%rbx)
	cmpq	32(%rbx), %rbp
	jne	.LBB32_57
# BB#53:                                #   in Loop: Header=BB32_52 Depth=1
	cmpl	$0, 64(%rbx)
	jne	.LBB32_57
# BB#54:                                #   in Loop: Header=BB32_52 Depth=1
	movq	40(%rbx), %rsi
	movq	48(%rbx), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB32_56
# BB#55:                                #   in Loop: Header=BB32_52 Depth=1
	movl	$9, 64(%rbx)
.LBB32_56:                              #   in Loop: Header=BB32_52 Depth=1
	addq	%rbp, 56(%rbx)
	movq	40(%rbx), %rbp
	movq	%rbp, 24(%rbx)
	.p2align	4, 0x90
.LBB32_57:                              # %RangeEnc_FlushStream.exit.i51.i
                                        #   in Loop: Header=BB32_52 Depth=1
	decq	16(%rbx)
	movq	8(%rbx), %rax
	movb	$-1, %cl
	jne	.LBB32_52
# BB#58:
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 4(%rbx)
	movl	$1, %ecx
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB32_59:                              # %RangeEnc_ShiftLow.exit53.i
	movl	4(%rsp), %ebp           # 4-byte Reload
	movq	%rcx, 16(%rbx)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 8(%rbx)
.LBB32_60:                              # %RangeEnc_EncodeBit.exit42.i
	leal	(,%r15,8), %ecx
	leaq	260(%rdi,%rcx,2), %r15
	addl	$-8, %ebp
	movl	$3, %r14d
	movl	$1, %edx
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB32_61:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_69 Depth 2
	decl	%r14d
	movl	%ebp, %r12d
	movl	%r14d, %ecx
	shrl	%cl, %r12d
	movl	%edx, %r13d
	movzwl	(%r15,%r13,2), %edx
	movl	(%rbx), %esi
	movl	%esi, %ecx
	shrl	$11, %ecx
	imull	%edx, %ecx
	andl	$1, %r12d
	jne	.LBB32_63
# BB#62:                                #   in Loop: Header=BB32_61 Depth=1
	movl	%ecx, (%rbx)
	movl	$2048, %esi             # imm = 0x800
	subl	%edx, %esi
	shrl	$5, %esi
	addl	%edx, %esi
	jmp	.LBB32_64
	.p2align	4, 0x90
.LBB32_63:                              #   in Loop: Header=BB32_61 Depth=1
	movl	%ecx, %ebp
	addq	%rbp, %rax
	movl	4(%rsp), %ebp           # 4-byte Reload
	movq	%rax, 8(%rbx)
	subl	%ecx, %esi
	movl	%esi, (%rbx)
	movl	%edx, %ecx
	shrl	$5, %ecx
	subl	%ecx, %edx
	movl	%esi, %ecx
	movl	%edx, %esi
.LBB32_64:                              #   in Loop: Header=BB32_61 Depth=1
	movw	%si, (%r15,%r13,2)
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB32_77
# BB#65:                                #   in Loop: Header=BB32_61 Depth=1
	shll	$8, %ecx
	movl	%ecx, (%rbx)
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB32_68
# BB#66:                                #   in Loop: Header=BB32_61 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB32_68
# BB#67:                                # %._crit_edge.i64.i
                                        #   in Loop: Header=BB32_61 Depth=1
	movq	16(%rbx), %rcx
	incq	%rcx
	jmp	.LBB32_76
	.p2align	4, 0x90
.LBB32_68:                              #   in Loop: Header=BB32_61 Depth=1
	movb	4(%rbx), %cl
	movq	24(%rbx), %rbp
	.p2align	4, 0x90
.LBB32_69:                              # %RangeEnc_FlushStream.exit._crit_edge.i67.i
                                        #   Parent Loop BB32_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rax
	addb	%cl, %al
	movb	%al, (%rbp)
	incq	%rbp
	movq	%rbp, 24(%rbx)
	cmpq	32(%rbx), %rbp
	jne	.LBB32_74
# BB#70:                                #   in Loop: Header=BB32_69 Depth=2
	cmpl	$0, 64(%rbx)
	jne	.LBB32_74
# BB#71:                                #   in Loop: Header=BB32_69 Depth=2
	movq	40(%rbx), %rsi
	movq	48(%rbx), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB32_73
# BB#72:                                #   in Loop: Header=BB32_69 Depth=2
	movl	$9, 64(%rbx)
.LBB32_73:                              #   in Loop: Header=BB32_69 Depth=2
	addq	%rbp, 56(%rbx)
	movq	40(%rbx), %rbp
	movq	%rbp, 24(%rbx)
	.p2align	4, 0x90
.LBB32_74:                              # %RangeEnc_FlushStream.exit.i68.i
                                        #   in Loop: Header=BB32_69 Depth=2
	decq	16(%rbx)
	movq	8(%rbx), %rax
	movb	$-1, %cl
	jne	.LBB32_69
# BB#75:                                #   in Loop: Header=BB32_61 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 4(%rbx)
	movl	$1, %ecx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB32_76:                              # %RangeEnc_ShiftLow.exit70.i
                                        #   in Loop: Header=BB32_61 Depth=1
	movq	%rcx, 16(%rbx)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 8(%rbx)
.LBB32_77:                              # %RangeEnc_EncodeBit.exit.i57.i
                                        #   in Loop: Header=BB32_61 Depth=1
	addl	%r13d, %r13d
	orl	%r13d, %r12d
	testl	%r14d, %r14d
	movl	%r12d, %edx
	jne	.LBB32_61
	jmp	.LBB32_110
.LBB32_83:
	movb	4(%rbx), %al
	movq	24(%rbx), %rbp
	.p2align	4, 0x90
.LBB32_84:                              # %RangeEnc_FlushStream.exit._crit_edge.i80.i
                                        # =>This Inner Loop Header: Depth=1
	shrq	$32, %rdx
	addb	%al, %dl
	movb	%dl, (%rbp)
	incq	%rbp
	movq	%rbp, 24(%rbx)
	cmpq	32(%rbx), %rbp
	jne	.LBB32_89
# BB#85:                                #   in Loop: Header=BB32_84 Depth=1
	cmpl	$0, 64(%rbx)
	jne	.LBB32_89
# BB#86:                                #   in Loop: Header=BB32_84 Depth=1
	movq	40(%rbx), %rsi
	movq	48(%rbx), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB32_88
# BB#87:                                #   in Loop: Header=BB32_84 Depth=1
	movl	$9, 64(%rbx)
.LBB32_88:                              #   in Loop: Header=BB32_84 Depth=1
	addq	%rbp, 56(%rbx)
	movq	40(%rbx), %rbp
	movq	%rbp, 24(%rbx)
	.p2align	4, 0x90
.LBB32_89:                              # %RangeEnc_FlushStream.exit.i81.i
                                        #   in Loop: Header=BB32_84 Depth=1
	decq	16(%rbx)
	movq	8(%rbx), %rdx
	movb	$-1, %al
	jne	.LBB32_84
# BB#90:
	movl	%edx, %eax
	shrl	$24, %eax
	movb	%al, 4(%rbx)
	movl	$1, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB32_91:                              # %RangeEnc_ShiftLow.exit83.i
	movl	4(%rsp), %ebp           # 4-byte Reload
	movq	%rax, 16(%rbx)
	andq	$16777215, %rdx         # imm = 0xFFFFFF
	shlq	$8, %rdx
	movq	%rdx, 8(%rbx)
.LBB32_92:                              # %RangeEnc_EncodeBit.exit72.i
	addl	$-16, %ebp
	movl	$8, %r14d
	movl	$1, %eax
	movl	%ebp, %r13d
	.p2align	4, 0x90
.LBB32_93:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_101 Depth 2
	decl	%r14d
	movl	%ebp, %r15d
	movl	%r14d, %ecx
	shrl	%cl, %r15d
	movl	%eax, %r12d
	movzwl	516(%rdi,%r12,2), %ecx
	movl	(%rbx), %esi
	movl	%esi, %eax
	shrl	$11, %eax
	imull	%ecx, %eax
	andl	$1, %r15d
	jne	.LBB32_95
# BB#94:                                #   in Loop: Header=BB32_93 Depth=1
	movl	%eax, (%rbx)
	movl	$2048, %esi             # imm = 0x800
	subl	%ecx, %esi
	shrl	$5, %esi
	addl	%ecx, %esi
	jmp	.LBB32_96
	.p2align	4, 0x90
.LBB32_95:                              #   in Loop: Header=BB32_93 Depth=1
	movl	%eax, %ebp
	addq	%rbp, %rdx
	movl	%r13d, %ebp
	movq	%rdx, 8(%rbx)
	subl	%eax, %esi
	movl	%esi, (%rbx)
	movl	%ecx, %eax
	shrl	$5, %eax
	subl	%eax, %ecx
	movl	%esi, %eax
	movl	%ecx, %esi
.LBB32_96:                              #   in Loop: Header=BB32_93 Depth=1
	movw	%si, 516(%rdi,%r12,2)
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB32_109
# BB#97:                                #   in Loop: Header=BB32_93 Depth=1
	shll	$8, %eax
	movl	%eax, (%rbx)
	movq	%rdx, %rax
	shrq	$32, %rax
	jne	.LBB32_100
# BB#98:                                #   in Loop: Header=BB32_93 Depth=1
	cmpl	$-16777216, %edx        # imm = 0xFF000000
	jb	.LBB32_100
# BB#99:                                # %._crit_edge.i94.i
                                        #   in Loop: Header=BB32_93 Depth=1
	movq	16(%rbx), %rax
	incq	%rax
	jmp	.LBB32_108
	.p2align	4, 0x90
.LBB32_100:                             #   in Loop: Header=BB32_93 Depth=1
	movb	4(%rbx), %al
	movq	24(%rbx), %rbp
	.p2align	4, 0x90
.LBB32_101:                             # %RangeEnc_FlushStream.exit._crit_edge.i97.i
                                        #   Parent Loop BB32_93 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rdx
	addb	%al, %dl
	movb	%dl, (%rbp)
	incq	%rbp
	movq	%rbp, 24(%rbx)
	cmpq	32(%rbx), %rbp
	jne	.LBB32_106
# BB#102:                               #   in Loop: Header=BB32_101 Depth=2
	cmpl	$0, 64(%rbx)
	jne	.LBB32_106
# BB#103:                               #   in Loop: Header=BB32_101 Depth=2
	movq	40(%rbx), %rsi
	movq	48(%rbx), %rdi
	subq	%rsi, %rbp
	movq	%rbp, %rdx
	callq	*(%rdi)
	cmpq	%rax, %rbp
	je	.LBB32_105
# BB#104:                               #   in Loop: Header=BB32_101 Depth=2
	movl	$9, 64(%rbx)
.LBB32_105:                             #   in Loop: Header=BB32_101 Depth=2
	addq	%rbp, 56(%rbx)
	movq	40(%rbx), %rbp
	movq	%rbp, 24(%rbx)
	.p2align	4, 0x90
.LBB32_106:                             # %RangeEnc_FlushStream.exit.i98.i
                                        #   in Loop: Header=BB32_101 Depth=2
	decq	16(%rbx)
	movq	8(%rbx), %rdx
	movb	$-1, %al
	jne	.LBB32_101
# BB#107:                               #   in Loop: Header=BB32_93 Depth=1
	movl	%edx, %eax
	shrl	$24, %eax
	movb	%al, 4(%rbx)
	movl	$1, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%r13d, %ebp
.LBB32_108:                             # %RangeEnc_ShiftLow.exit100.i
                                        #   in Loop: Header=BB32_93 Depth=1
	movq	%rax, 16(%rbx)
	andq	$16777215, %rdx         # imm = 0xFFFFFF
	shlq	$8, %rdx
	movq	%rdx, 8(%rbx)
.LBB32_109:                             # %RangeEnc_EncodeBit.exit.i87.i
                                        #   in Loop: Header=BB32_93 Depth=1
	addl	%r12d, %r12d
	orl	%r12d, %r15d
	testl	%r14d, %r14d
	movl	%r15d, %eax
	jne	.LBB32_93
.LBB32_110:                             # %LenEnc_Encode.exit
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	je	.LBB32_112
# BB#111:
	movl	%esi, %eax
	decl	18440(%rdi,%rax,4)
	je	.LBB32_113
.LBB32_112:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB32_113:
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	24(%rsp), %rdx          # 8-byte Reload
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	LenPriceEnc_UpdateTable # TAILCALL
.Lfunc_end32:
	.size	LenEnc_Encode2, .Lfunc_end32-LenEnc_Encode2
	.cfi_endproc

	.type	kLiteralNextStates,@object # @kLiteralNextStates
	.section	.rodata,"a",@progbits
	.p2align	4
kLiteralNextStates:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	4                       # 0x4
	.long	5                       # 0x5
	.size	kLiteralNextStates, 48

	.type	kShortRepNextStates,@object # @kShortRepNextStates
	.p2align	4
kShortRepNextStates:
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	11                      # 0xb
	.size	kShortRepNextStates, 48

	.type	kRepNextStates,@object  # @kRepNextStates
	.p2align	4
kRepNextStates:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	11                      # 0xb
	.size	kRepNextStates, 48

	.type	kMatchNextStates,@object # @kMatchNextStates
	.p2align	4
kMatchNextStates:
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.size	kMatchNextStates, 48


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
