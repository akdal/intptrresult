	.text
	.file	"init_viterbi.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	7                       # 0x7
	.quad	128                     # 0x80
.LCPI0_1:
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	1                       # 0x1
.LCPI0_2:
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
.LCPI0_3:
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
.LCPI0_4:
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.byte	1                       # 0x1
.LCPI0_5:
	.zero	16,1
	.text
	.globl	init_viterbi
	.p2align	4, 0x90
	.type	init_viterbi,@function
init_viterbi:                           # @init_viterbi
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$896, %rsp              # imm = 0x380
.Lcfi3:
	.cfi_def_cfa_offset 928
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	xorl	%esi, %esi
	movl	$19632, %edx            # imm = 0x4CB0
	callq	memset
	movq	%rsp, %rdi
	xorl	%esi, %esi
	movl	$896, %edx              # imm = 0x380
	callq	memset
	decl	%r14d
	cmpl	$4, %r14d
	ja	.LBB0_8
# BB#1:
	jmpq	*.LJTI0_0(,%r14,8)
.LBB0_2:
	movb	$1, 24(%r15)
	movb	$1, %cl
	movl	$1, %eax
	xorl	%edx, %edx
	jmp	.LBB0_7
.LBB0_3:
	movw	$1, 24(%r15)
	movb	$1, 31(%r15)
	movb	$1, %cl
	movl	$2, %eax
	movl	$1, %edx
	jmp	.LBB0_7
.LBB0_4:
	movw	$1, 24(%r15)
	movb	$1, 26(%r15)
	movb	$1, 31(%r15)
	movb	$1, 32(%r15)
	movl	$3, %eax
	xorl	%ecx, %ecx
	movl	$2, %edx
	jmp	.LBB0_7
.LBB0_5:
	movl	$65537, 24(%r15)        # imm = 0x10001
	movb	$1, 28(%r15)
	movb	$1, 31(%r15)
	movb	$1, 32(%r15)
	movb	$0, 33(%r15)
	movb	$1, 34(%r15)
	movl	$5, %eax
	xorl	%ecx, %ecx
	movl	$4, %edx
	jmp	.LBB0_7
.LBB0_6:
	movl	$1, 24(%r15)
	movw	$1, 28(%r15)
	movb	$1, 34(%r15)
	movl	$16843009, 30(%r15)     # imm = 0x1010101
	movw	$256, 35(%r15)          # imm = 0x100
	movl	$7, %eax
	xorl	%ecx, %ecx
	movl	$6, %edx
.LBB0_7:                                # %.sink.split
	movb	%cl, 31(%r15,%rdx)
	movq	%rax, 40(%r15)
.LBB0_8:                                # %vector.body
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [7,128]
	movups	%xmm0, (%r15)
	movq	$150, 16(%r15)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1]
	movaps	%xmm0, 768(%rsp)
	movaps	.LCPI0_2(%rip), %xmm1   # xmm1 = [0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1]
	movaps	%xmm1, 640(%rsp)
	movaps	.LCPI0_3(%rip), %xmm2   # xmm2 = [0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1]
	movaps	%xmm2, 512(%rsp)
	movaps	.LCPI0_4(%rip), %xmm3   # xmm3 = [0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1]
	movaps	%xmm3, 384(%rsp)
	xorps	%xmm5, %xmm5
	movaps	%xmm5, 256(%rsp)
	movaps	%xmm5, 128(%rsp)
	movaps	%xmm5, (%rsp)
	movaps	%xmm0, 784(%rsp)
	movaps	%xmm1, 656(%rsp)
	movaps	%xmm2, 528(%rsp)
	movaps	%xmm3, 400(%rsp)
	movaps	.LCPI0_5(%rip), %xmm4   # xmm4 = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
	movaps	%xmm4, 272(%rsp)
	movaps	%xmm5, 144(%rsp)
	movaps	%xmm5, 16(%rsp)
	movaps	%xmm0, 800(%rsp)
	movaps	%xmm1, 672(%rsp)
	movaps	%xmm2, 544(%rsp)
	movaps	%xmm3, 416(%rsp)
	movaps	%xmm5, 288(%rsp)
	movaps	%xmm4, 160(%rsp)
	movaps	%xmm5, 32(%rsp)
	movaps	%xmm0, 816(%rsp)
	movaps	%xmm1, 688(%rsp)
	movaps	%xmm2, 560(%rsp)
	movaps	%xmm3, 432(%rsp)
	movaps	%xmm4, 304(%rsp)
	movaps	%xmm4, 176(%rsp)
	movaps	%xmm5, 48(%rsp)
	movaps	%xmm0, 832(%rsp)
	movaps	%xmm1, 704(%rsp)
	movaps	%xmm2, 576(%rsp)
	movaps	%xmm3, 448(%rsp)
	movaps	%xmm5, 320(%rsp)
	movaps	%xmm5, 192(%rsp)
	movaps	%xmm4, 64(%rsp)
	movaps	%xmm0, 848(%rsp)
	movaps	%xmm1, 720(%rsp)
	movaps	%xmm2, 592(%rsp)
	movaps	%xmm3, 464(%rsp)
	movaps	%xmm4, 336(%rsp)
	movaps	%xmm5, 208(%rsp)
	movaps	%xmm4, 80(%rsp)
	movaps	%xmm0, 864(%rsp)
	movaps	%xmm1, 736(%rsp)
	movaps	%xmm2, 608(%rsp)
	movaps	%xmm3, 480(%rsp)
	movaps	%xmm5, 352(%rsp)
	movaps	%xmm4, 224(%rsp)
	movaps	%xmm4, 96(%rsp)
	movaps	%xmm0, 880(%rsp)
	movaps	%xmm1, 752(%rsp)
	movaps	%xmm2, 624(%rsp)
	movaps	%xmm3, 496(%rsp)
	movaps	%xmm4, 368(%rsp)
	movaps	%xmm4, 240(%rsp)
	movaps	%xmm4, 112(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph87
                                        # =>This Inner Loop Header: Depth=1
	movzbl	128(%rsp,%rax), %esi
	movzbl	256(%rsp,%rax), %ecx
	movzbl	(%rsp,%rax), %ebx
	xorb	%sil, %bl
	movzbl	640(%rsp,%rax), %edi
	movl	%ecx, %edx
	xorb	%dil, %dl
	xorb	%bl, %dl
	movb	%dl, 48(%r15,%rax)
	xorb	%sil, %cl
	xorb	512(%rsp,%rax), %cl
	xorb	%dil, %cl
	movb	%cl, 176(%r15,%rax)
	incq	%rax
	movq	8(%r15), %rcx
	cmpq	%rcx, %rax
	jb	.LBB0_9
# BB#10:                                # %.preheader
	testq	%rcx, %rcx
	je	.LBB0_13
# BB#11:                                # %.lr.ph.preheader
	xorl	%eax, %eax
	movabsq	$4696837146684686336, %rcx # imm = 0x412E848000000000
	.p2align	4, 0x90
.LBB0_12:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, 304(%r15,%rax,8)
	incq	%rax
	cmpq	8(%r15), %rax
	jb	.LBB0_12
.LBB0_13:                               # %._crit_edge
	movq	$0, 304(%r15)
	addq	$896, %rsp              # imm = 0x380
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	init_viterbi, .Lfunc_end0-init_viterbi
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_2
	.quad	.LBB0_3
	.quad	.LBB0_4
	.quad	.LBB0_5
	.quad	.LBB0_6


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
