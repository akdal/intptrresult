	.text
	.file	"list.bc"
	.globl	list_Copy
	.p2align	4, 0x90
	.type	list_Copy,@function
list_Copy:                              # @list_Copy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
# BB#2:
	movq	8(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r15, 8(%r14)
	movq	$0, (%r14)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB0_5
# BB#3:                                 # %.lr.ph.preheader
	movq	%r14, %r15
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, (%r15)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r15
	jne	.LBB0_4
	jmp	.LBB0_5
.LBB0_1:
	xorl	%r14d, %r14d
.LBB0_5:                                # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	list_Copy, .Lfunc_end0-list_Copy
	.cfi_endproc

	.globl	list_CopyWithElement
	.p2align	4, 0x90
	.type	list_CopyWithElement,@function
list_CopyWithElement:                   # @list_CopyWithElement
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -48
.Lcfi15:
	.cfi_offset %r12, -40
.Lcfi16:
	.cfi_offset %r13, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rsi, %r13
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB1_1
# BB#2:
	movq	8(%r15), %rdi
	callq	*%r13
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r12, 8(%r14)
	movq	$0, (%r14)
	movq	(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB1_5
# BB#3:                                 # %.lr.ph.preheader
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	*%r13
	movq	%rax, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, (%r12)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r12
	jne	.LBB1_4
	jmp	.LBB1_5
.LBB1_1:
	xorl	%r14d, %r14d
.LBB1_5:                                # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	list_CopyWithElement, .Lfunc_end1-list_CopyWithElement
	.cfi_endproc

	.globl	list_InsertNext
	.p2align	4, 0x90
	.type	list_InsertNext,@function
list_InsertNext:                        # @list_InsertNext
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, (%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	list_InsertNext, .Lfunc_end2-list_InsertNext
	.cfi_endproc

	.globl	list_NMapCar
	.p2align	4, 0x90
	.type	list_NMapCar,@function
list_NMapCar:                           # @list_NMapCar
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 32
.Lcfi28:
	.cfi_offset %rbx, -24
.Lcfi29:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB3_3
	.p2align	4, 0x90
.LBB3_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	*%r14
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_1
.LBB3_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	list_NMapCar, .Lfunc_end3-list_NMapCar
	.cfi_endproc

	.globl	list_Apply
	.p2align	4, 0x90
	.type	list_Apply,@function
list_Apply:                             # @list_Apply
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -24
.Lcfi34:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB4_3
	.p2align	4, 0x90
.LBB4_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	*%r14
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_1
.LBB4_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	list_Apply, .Lfunc_end4-list_Apply
	.cfi_endproc

	.globl	list_Reverse
	.p2align	4, 0x90
	.type	list_Reverse,@function
list_Reverse:                           # @list_Reverse
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -32
.Lcfi39:
	.cfi_offset %r14, -24
.Lcfi40:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB5_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r14, (%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r14
	jne	.LBB5_3
	jmp	.LBB5_4
.LBB5_1:
	xorl	%eax, %eax
.LBB5_4:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	list_Reverse, .Lfunc_end5-list_Reverse
	.cfi_endproc

	.globl	list_NReverse
	.p2align	4, 0x90
	.type	list_NReverse,@function
list_NReverse:                          # @list_NReverse
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 48
.Lcfi46:
	.cfi_offset %rbx, -40
.Lcfi47:
	.cfi_offset %r12, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB6_8
# BB#1:                                 # %.lr.ph30.preheader
	xorl	%r15d, %r15d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph30
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r15, (%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r15
	jne	.LBB6_2
# BB#3:                                 # %.preheader
	testq	%r14, %r14
	je	.LBB6_6
# BB#4:                                 # %.lr.ph.preheader
	movq	%rax, %rcx
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rsi
	movq	%rsi, 8(%rdx)
	movq	(%rdx), %rdx
	movq	(%rcx), %rcx
	testq	%rdx, %rdx
	jne	.LBB6_5
.LBB6_6:                                # %._crit_edge
	testq	%rax, %rax
	je	.LBB6_8
	.p2align	4, 0x90
.LBB6_7:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB6_7
.LBB6_8:                                # %list_Delete.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	list_NReverse, .Lfunc_end6-list_NReverse
	.cfi_endproc

	.globl	list_PointerSort
	.p2align	4, 0x90
	.type	list_PointerSort,@function
list_PointerSort:                       # @list_PointerSort
	.cfi_startproc
# BB#0:
	movl	$list_PointerLower, %esi
	jmp	list_MergeSort          # TAILCALL
.Lfunc_end7:
	.size	list_PointerSort, .Lfunc_end7-list_PointerSort
	.cfi_endproc

	.globl	list_Sort
	.p2align	4, 0x90
	.type	list_Sort,@function
list_Sort:                              # @list_Sort
	.cfi_startproc
# BB#0:
	jmp	list_MergeSort          # TAILCALL
.Lfunc_end8:
	.size	list_Sort, .Lfunc_end8-list_Sort
	.cfi_endproc

	.p2align	4, 0x90
	.type	list_PointerLower,@function
list_PointerLower:                      # @list_PointerLower
	.cfi_startproc
# BB#0:
	cmpl	%esi, %edi
	sbbl	%eax, %eax
	andl	$1, %eax
	retq
.Lfunc_end9:
	.size	list_PointerLower, .Lfunc_end9-list_PointerLower
	.cfi_endproc

	.globl	list_SortedInOrder
	.p2align	4, 0x90
	.type	list_SortedInOrder,@function
list_SortedInOrder:                     # @list_SortedInOrder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 48
.Lcfi55:
	.cfi_offset %rbx, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movl	$1, %r14d
	testq	%rbp, %rbp
	jne	.LBB10_1
	jmp	.LBB10_5
	.p2align	4, 0x90
.LBB10_6:                               #   in Loop: Header=BB10_1 Depth=1
	movq	(%rbp), %rbp
.LBB10_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB10_5
# BB#2:                                 # %.preheader
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	8(%rbp), %rdi
	movq	8(%rbx), %rsi
	callq	*%r15
	testl	%eax, %eax
	jne	.LBB10_6
# BB#3:                                 #   in Loop: Header=BB10_1 Depth=1
	movq	8(%rbx), %rdi
	movq	8(%rbp), %rsi
	callq	*%r15
	testl	%eax, %eax
	je	.LBB10_6
# BB#4:
	xorl	%r14d, %r14d
.LBB10_5:                               # %.loopexit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	list_SortedInOrder, .Lfunc_end10-list_SortedInOrder
	.cfi_endproc

	.globl	list_Merge
	.p2align	4, 0x90
	.type	list_Merge,@function
list_Merge:                             # @list_Merge
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 48
.Lcfi64:
	.cfi_offset %rbx, -48
.Lcfi65:
	.cfi_offset %r12, -40
.Lcfi66:
	.cfi_offset %r13, -32
.Lcfi67:
	.cfi_offset %r14, -24
.Lcfi68:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r13
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB11_1
# BB#2:
	testq	%r13, %r13
	je	.LBB11_3
# BB#4:
	movq	8(%r12), %rdi
	movq	8(%r13), %rsi
	callq	*%r14
	testl	%eax, %eax
	je	.LBB11_5
# BB#6:
	movq	(%r12), %rax
	testq	%rax, %rax
	movq	%r12, %r15
	movq	%r12, %rbx
	jne	.LBB11_7
	jmp	.LBB11_14
.LBB11_1:
	movq	%r13, %r15
	jmp	.LBB11_16
.LBB11_3:
	movq	%r12, %r15
	jmp	.LBB11_16
.LBB11_5:                               # %.thread
	movq	%r13, %r15
	movq	(%r13), %r13
	movq	%r12, %rax
.LBB11_7:                               # %.lr.ph.preheader
	movq	%r15, %r12
	movq	%rax, %rbx
	jmp	.LBB11_8
	.p2align	4, 0x90
.LBB11_12:                              #   in Loop: Header=BB11_9 Depth=2
	movq	%rbx, (%r12)
	movq	(%rbx), %rax
	testq	%rax, %rax
	movq	%rbx, %r12
	jne	.LBB11_9
	jmp	.LBB11_13
.LBB11_11:                              # %.thread62
                                        #   in Loop: Header=BB11_8 Depth=1
	movq	%r13, (%r12)
	movq	%r13, %r12
	movq	(%r13), %r13
.LBB11_8:                               # %.lr.ph.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_9 Depth 2
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB11_9:                               # %.lr.ph
                                        #   Parent Loop BB11_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rbx
	testq	%r13, %r13
	je	.LBB11_15
# BB#10:                                #   in Loop: Header=BB11_9 Depth=2
	movq	8(%rbx), %rdi
	movq	8(%r13), %rsi
	callq	*%r14
	testl	%eax, %eax
	jne	.LBB11_12
	jmp	.LBB11_11
.LBB11_13:
	movq	%r15, %r12
.LBB11_14:                              # %.critedge
	movq	%r13, (%rbx)
	movq	%r12, %r15
	jmp	.LBB11_16
.LBB11_15:
	movq	%rbx, (%r12)
.LBB11_16:
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	list_Merge, .Lfunc_end11-list_Merge
	.cfi_endproc

	.globl	list_Split
	.p2align	4, 0x90
	.type	list_Split,@function
list_Split:                             # @list_Split
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB12_4
# BB#1:
	movq	(%rdi), %r8
	testq	%r8, %r8
	je	.LBB12_4
# BB#2:                                 # %.preheader
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.LBB12_3
# BB#5:
	movq	%rdi, %r9
	.p2align	4, 0x90
.LBB12_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r8, %rcx
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB12_7
# BB#8:                                 #   in Loop: Header=BB12_6 Depth=1
	movq	(%rcx), %r8
	movq	(%rax), %rax
	testq	%rax, %rax
	movq	%rcx, %r9
	jne	.LBB12_6
	jmp	.LBB12_9
.LBB12_4:
	movq	$0, (%rsi)
	movq	%rdi, (%rdx)
	retq
.LBB12_3:
	movq	%rdi, %rcx
	jmp	.LBB12_9
.LBB12_7:
	movq	%rcx, %r8
	movq	%r9, %rcx
.LBB12_9:                               # %.critedge
	movq	%rdi, (%rsi)
	movq	%r8, (%rdx)
	movq	$0, (%rcx)
	retq
.Lfunc_end12:
	.size	list_Split, .Lfunc_end12-list_Split
	.cfi_endproc

	.globl	list_MergeSort
	.p2align	4, 0x90
	.type	list_MergeSort,@function
list_MergeSort:                         # @list_MergeSort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi73:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi75:
	.cfi_def_cfa_offset 64
.Lcfi76:
	.cfi_offset %rbx, -56
.Lcfi77:
	.cfi_offset %r12, -48
.Lcfi78:
	.cfi_offset %r13, -40
.Lcfi79:
	.cfi_offset %r14, -32
.Lcfi80:
	.cfi_offset %r15, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	testq	%r13, %r13
	je	.LBB13_1
# BB#2:
	movq	(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB13_25
# BB#3:                                 # %.preheader.i
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB13_4
# BB#5:                                 # %.lr.ph.i15.preheader
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB13_6:                               # %.lr.ph.i15
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rax
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB13_7
# BB#8:                                 #   in Loop: Header=BB13_6 Depth=1
	movq	(%rax), %rbx
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	movq	%rax, %rdx
	jne	.LBB13_6
	jmp	.LBB13_9
.LBB13_1:
	xorl	%r13d, %r13d
	jmp	.LBB13_25
.LBB13_4:
	movq	%r13, %rax
	jmp	.LBB13_9
.LBB13_7:
	movq	%rax, %rbx
	movq	%rdx, %rax
.LBB13_9:                               # %.critedge.i16
	movq	$0, (%rax)
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	list_MergeSort
	movq	%rax, %r12
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	list_MergeSort
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.LBB13_10
# BB#11:
	testq	%rbx, %rbx
	je	.LBB13_24
# BB#12:
	movq	8(%r12), %rdi
	movq	8(%rbx), %rsi
	callq	*%r14
	testl	%eax, %eax
	je	.LBB13_13
# BB#14:
	movq	(%r12), %r15
	testq	%r15, %r15
	movq	%r12, %r13
	movq	%r12, %rax
	jne	.LBB13_15
	jmp	.LBB13_23
.LBB13_10:
	movq	%rbx, %r13
	jmp	.LBB13_25
.LBB13_13:                              # %.thread.i
	movq	%rbx, %r13
	movq	(%rbx), %rbx
	movq	%r12, %r15
.LBB13_15:                              # %.lr.ph.preheader.i
	movq	%r13, %rbp
	testq	%rbx, %rbx
	je	.LBB13_20
# BB#16:                                # %.lr.ph.i.preheader.preheader
	movq	%rbx, %rax
.LBB13_17:                              # %.lr.ph.i.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_18 Depth 2
	movq	%rbp, %rbx
	movq	%rax, %rbp
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB13_18:                              # %.lr.ph.i
                                        #   Parent Loop BB13_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %r15
	movq	8(%r15), %rdi
	movq	8(%rbp), %rsi
	callq	*%r14
	testl	%eax, %eax
	je	.LBB13_19
# BB#21:                                #   in Loop: Header=BB13_18 Depth=2
	movq	%r15, (%rbx)
	movq	(%r15), %rax
	testq	%rax, %rax
	movq	%r15, %rbx
	jne	.LBB13_18
	jmp	.LBB13_22
.LBB13_19:                              # %.thread62.i
                                        #   in Loop: Header=BB13_17 Depth=1
	movq	%rbp, (%rbx)
	movq	(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB13_17
.LBB13_20:                              # %.us-lcssa.us
	movq	%r15, (%rbp)
	jmp	.LBB13_25
.LBB13_22:
	movq	%r13, %r12
	movq	%rbp, %rbx
	movq	%r15, %rax
.LBB13_23:                              # %.critedge.i
	movq	%rbx, (%rax)
.LBB13_24:                              # %list_Merge.exit
	movq	%r12, %r13
.LBB13_25:                              # %list_Merge.exit
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	list_MergeSort, .Lfunc_end13-list_MergeSort
	.cfi_endproc

	.globl	list_InsertionSort
	.p2align	4, 0x90
	.type	list_InsertionSort,@function
list_InsertionSort:                     # @list_InsertionSort
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi86:
	.cfi_def_cfa_offset 48
.Lcfi87:
	.cfi_offset %rbx, -40
.Lcfi88:
	.cfi_offset %r12, -32
.Lcfi89:
	.cfi_offset %r14, -24
.Lcfi90:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB14_6
# BB#1:                                 # %.preheader.preheader
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB14_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_3 Depth 2
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB14_6
	.p2align	4, 0x90
.LBB14_3:                               #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movq	8(%r12), %rsi
	callq	*%r15
	testl	%eax, %eax
	je	.LBB14_4
# BB#7:                                 #   in Loop: Header=BB14_3 Depth=2
	movq	8(%r12), %rax
	movq	8(%rbx), %rcx
	movq	%rcx, 8(%r12)
	movq	%rax, 8(%rbx)
.LBB14_4:                               # %.backedge
                                        #   in Loop: Header=BB14_3 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB14_3
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB14_2
.LBB14_6:                               # %._crit_edge33
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	list_InsertionSort, .Lfunc_end14-list_InsertionSort
	.cfi_endproc

	.globl	list_NumberSort
	.p2align	4, 0x90
	.type	list_NumberSort,@function
list_NumberSort:                        # @list_NumberSort
	.cfi_startproc
# BB#0:
	movq	%rsi, NumberFunction(%rip)
	movl	$list_PointerNumberedLower, %esi
	jmp	list_MergeSort          # TAILCALL
.Lfunc_end15:
	.size	list_NumberSort, .Lfunc_end15-list_NumberSort
	.cfi_endproc

	.p2align	4, 0x90
	.type	list_PointerNumberedLower,@function
list_PointerNumberedLower:              # @list_PointerNumberedLower
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 32
.Lcfi94:
	.cfi_offset %rbx, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	callq	*NumberFunction(%rip)
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	*NumberFunction(%rip)
	cmpl	%eax, %ebp
	sbbl	%eax, %eax
	andl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end16:
	.size	list_PointerNumberedLower, .Lfunc_end16-list_PointerNumberedLower
	.cfi_endproc

	.globl	list_GreaterNumberSort
	.p2align	4, 0x90
	.type	list_GreaterNumberSort,@function
list_GreaterNumberSort:                 # @list_GreaterNumberSort
	.cfi_startproc
# BB#0:
	movq	%rsi, NumberFunction(%rip)
	movl	$list_PointerNumberedGreater, %esi
	jmp	list_MergeSort          # TAILCALL
.Lfunc_end17:
	.size	list_GreaterNumberSort, .Lfunc_end17-list_GreaterNumberSort
	.cfi_endproc

	.p2align	4, 0x90
	.type	list_PointerNumberedGreater,@function
list_PointerNumberedGreater:            # @list_PointerNumberedGreater
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi98:
	.cfi_def_cfa_offset 32
.Lcfi99:
	.cfi_offset %rbx, -24
.Lcfi100:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	callq	*NumberFunction(%rip)
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	*NumberFunction(%rip)
	cmpl	%ebp, %eax
	sbbl	%eax, %eax
	andl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end18:
	.size	list_PointerNumberedGreater, .Lfunc_end18-list_PointerNumberedGreater
	.cfi_endproc

	.globl	list_NNumberMerge
	.p2align	4, 0x90
	.type	list_NNumberMerge,@function
list_NNumberMerge:                      # @list_NNumberMerge
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi107:
	.cfi_def_cfa_offset 64
.Lcfi108:
	.cfi_offset %rbx, -56
.Lcfi109:
	.cfi_offset %r12, -48
.Lcfi110:
	.cfi_offset %r13, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	%rdx, NumberFunction(%rip)
	testq	%rbx, %rbx
	je	.LBB19_1
# BB#2:
	testq	%r15, %r15
	je	.LBB19_15
# BB#3:
	movq	8(%rbx), %rdi
	movq	8(%r15), %r14
	callq	*%rdx
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	*NumberFunction(%rip)
	cmpl	%eax, %ebp
	jbe	.LBB19_5
# BB#4:                                 # %.thread.i
	movq	%r15, %r14
	movq	(%r15), %r15
	movq	%rbx, %rbp
	jmp	.LBB19_6
.LBB19_1:
	movq	%r15, %r14
	jmp	.LBB19_16
.LBB19_5:
	movq	(%rbx), %rbp
	testq	%rbp, %rbp
	movq	%rbx, %r14
	movq	%rbx, %rax
	je	.LBB19_14
.LBB19_6:                               # %.lr.ph.preheader.i
	movq	%r14, %r12
	testq	%r15, %r15
	jne	.LBB19_8
	jmp	.LBB19_11
	.p2align	4, 0x90
.LBB19_10:                              # %.thread62.i
                                        #   in Loop: Header=BB19_8 Depth=1
	movq	%r12, (%rbx)
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.LBB19_11
.LBB19_8:                               # %.lr.ph.i.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_9 Depth 2
	movq	%r12, %rbx
	movq	%r15, %r12
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB19_9:                               # %.lr.ph.i
                                        #   Parent Loop BB19_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rbp
	movq	8(%rbp), %rdi
	movq	8(%r12), %r15
	callq	*NumberFunction(%rip)
	movl	%eax, %r13d
	movq	%r15, %rdi
	callq	*NumberFunction(%rip)
	cmpl	%eax, %r13d
	ja	.LBB19_10
# BB#12:                                #   in Loop: Header=BB19_9 Depth=2
	movq	%rbp, (%rbx)
	movq	(%rbp), %rax
	testq	%rax, %rax
	movq	%rbp, %rbx
	jne	.LBB19_9
# BB#13:
	movq	%r14, %rbx
	movq	%r12, %r15
	movq	%rbp, %rax
.LBB19_14:                              # %.critedge.i
	movq	%r15, (%rax)
.LBB19_15:                              # %list_Merge.exit
	movq	%rbx, %r14
.LBB19_16:                              # %list_Merge.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB19_11:                              # %.us-lcssa.us
	movq	%rbp, (%r12)
	jmp	.LBB19_16
.Lfunc_end19:
	.size	list_NNumberMerge, .Lfunc_end19-list_NNumberMerge
	.cfi_endproc

	.globl	list_DequeueNext
	.p2align	4, 0x90
	.type	list_DequeueNext,@function
list_DequeueNext:                       # @list_DequeueNext
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB20_1
# BB#2:
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB20_3
# BB#4:
	movq	(%rcx), %rdx
	movq	8(%rcx), %rax
	movq	%rdx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rcx, (%rdx)
	retq
.LBB20_1:
	xorl	%eax, %eax
	retq
.LBB20_3:
	xorl	%eax, %eax
	retq
.Lfunc_end20:
	.size	list_DequeueNext, .Lfunc_end20-list_DequeueNext
	.cfi_endproc

	.globl	list_NthElement
	.p2align	4, 0x90
	.type	list_NthElement,@function
list_NthElement:                        # @list_NthElement
	.cfi_startproc
# BB#0:
	jmp	.LBB21_1
	.p2align	4, 0x90
.LBB21_3:                               # %.lr.ph
                                        #   in Loop: Header=BB21_1 Depth=1
	movl	%esi, %edx
	testb	$1, %al
	movl	%ecx, %esi
	cmovnel	%edx, %esi
	movq	(%rdi), %rdi
.LBB21_1:                               # =>This Inner Loop Header: Depth=1
	testq	%rdi, %rdi
	sete	%al
	setne	%dl
	movl	%esi, %ecx
	decl	%ecx
	je	.LBB21_4
# BB#2:                                 #   in Loop: Header=BB21_1 Depth=1
	testb	%dl, %dl
	jne	.LBB21_3
.LBB21_4:                               # %._crit_edge
	testb	%al, %al
	je	.LBB21_6
# BB#5:
	xorl	%eax, %eax
	retq
.LBB21_6:
	movq	8(%rdi), %rax
	retq
.Lfunc_end21:
	.size	list_NthElement, .Lfunc_end21-list_NthElement
	.cfi_endproc

	.globl	list_DeleteWithElement
	.p2align	4, 0x90
	.type	list_DeleteWithElement,@function
list_DeleteWithElement:                 # @list_DeleteWithElement
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 32
.Lcfi117:
	.cfi_offset %rbx, -32
.Lcfi118:
	.cfi_offset %r14, -24
.Lcfi119:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB22_2
	.p2align	4, 0x90
.LBB22_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r15
	movq	8(%rbx), %rdi
	callq	*%r14
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	testq	%r15, %r15
	movq	%r15, %rbx
	jne	.LBB22_1
.LBB22_2:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	list_DeleteWithElement, .Lfunc_end22-list_DeleteWithElement
	.cfi_endproc

	.globl	list_DeleteWithElementCount
	.p2align	4, 0x90
	.type	list_DeleteWithElementCount,@function
list_DeleteWithElementCount:            # @list_DeleteWithElementCount
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi120:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi121:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi124:
	.cfi_def_cfa_offset 48
.Lcfi125:
	.cfi_offset %rbx, -40
.Lcfi126:
	.cfi_offset %r14, -32
.Lcfi127:
	.cfi_offset %r15, -24
.Lcfi128:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	je	.LBB23_2
	.p2align	4, 0x90
.LBB23_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r15
	movq	8(%rbx), %rdi
	callq	*%r14
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	incl	%ebp
	testq	%r15, %r15
	movq	%r15, %rbx
	jne	.LBB23_1
.LBB23_2:                               # %._crit_edge
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	list_DeleteWithElementCount, .Lfunc_end23-list_DeleteWithElementCount
	.cfi_endproc

	.globl	list_DeleteElement
	.p2align	4, 0x90
	.type	list_DeleteElement,@function
list_DeleteElement:                     # @list_DeleteElement
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi132:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi133:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi135:
	.cfi_def_cfa_offset 64
.Lcfi136:
	.cfi_offset %rbx, -56
.Lcfi137:
	.cfi_offset %r12, -48
.Lcfi138:
	.cfi_offset %r13, -40
.Lcfi139:
	.cfi_offset %r14, -32
.Lcfi140:
	.cfi_offset %r15, -24
.Lcfi141:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB24_4
	.p2align	4, 0x90
.LBB24_1:                               # %.lr.ph41
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rsi
	movq	%r15, %rdi
	callq	*%r14
	movq	(%r12), %rcx
	testl	%eax, %eax
	je	.LBB24_5
# BB#2:                                 #   in Loop: Header=BB24_1 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r12, (%rax)
	testq	%rcx, %rcx
	movq	%rcx, %r12
	jne	.LBB24_1
.LBB24_4:
	xorl	%r12d, %r12d
	jmp	.LBB24_11
.LBB24_5:                               # %.critedge.thread.preheader
	testq	%rcx, %rcx
	je	.LBB24_11
# BB#6:                                 # %.lr.ph.preheader
	movq	%r12, %r13
	movq	%r12, %rbx
.LBB24_7:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_8 Depth 2
	movq	%rbx, %rbp
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB24_8:                               #   Parent Loop BB24_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	*%r14
	testl	%eax, %eax
	je	.LBB24_10
# BB#9:                                 # %.critedge.thread
                                        #   in Loop: Header=BB24_8 Depth=2
	movq	(%rbx), %rax
	movq	%rax, (%rbp)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	movq	(%r13), %rbx
	testq	%rbx, %rbx
	jne	.LBB24_8
	jmp	.LBB24_11
	.p2align	4, 0x90
.LBB24_10:                              # %.critedge.thread.outer.loopexit
                                        #   in Loop: Header=BB24_7 Depth=1
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	movq	%rbx, %r13
	jne	.LBB24_7
.LBB24_11:                              # %.critedge
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	list_DeleteElement, .Lfunc_end24-list_DeleteElement
	.cfi_endproc

	.globl	list_DeleteElementIf
	.p2align	4, 0x90
	.type	list_DeleteElementIf,@function
list_DeleteElementIf:                   # @list_DeleteElementIf
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi144:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi145:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi146:
	.cfi_def_cfa_offset 48
.Lcfi147:
	.cfi_offset %rbx, -48
.Lcfi148:
	.cfi_offset %r12, -40
.Lcfi149:
	.cfi_offset %r13, -32
.Lcfi150:
	.cfi_offset %r14, -24
.Lcfi151:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB25_4
	.p2align	4, 0x90
.LBB25_1:                               # %.lr.ph39
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rdi
	callq	*%r14
	movq	(%r15), %rcx
	testl	%eax, %eax
	je	.LBB25_5
# BB#2:                                 #   in Loop: Header=BB25_1 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r15, (%rax)
	testq	%rcx, %rcx
	movq	%rcx, %r15
	jne	.LBB25_1
.LBB25_4:
	xorl	%r15d, %r15d
	jmp	.LBB25_11
.LBB25_5:                               # %.critedge.thread.preheader
	testq	%rcx, %rcx
	je	.LBB25_11
# BB#6:                                 # %.lr.ph.preheader
	movq	%r15, %r12
	movq	%r15, %rbx
.LBB25_7:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_8 Depth 2
	movq	%rbx, %r13
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB25_8:                               #   Parent Loop BB25_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	*%r14
	testl	%eax, %eax
	je	.LBB25_10
# BB#9:                                 # %.critedge.thread
                                        #   in Loop: Header=BB25_8 Depth=2
	movq	(%rbx), %rax
	movq	%rax, (%r13)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	jne	.LBB25_8
	jmp	.LBB25_11
	.p2align	4, 0x90
.LBB25_10:                              # %.critedge.thread.outer.loopexit
                                        #   in Loop: Header=BB25_7 Depth=1
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	movq	%rbx, %r12
	jne	.LBB25_7
.LBB25_11:                              # %.critedge
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end25:
	.size	list_DeleteElementIf, .Lfunc_end25-list_DeleteElementIf
	.cfi_endproc

	.globl	list_DeleteElementIfFree
	.p2align	4, 0x90
	.type	list_DeleteElementIfFree,@function
list_DeleteElementIfFree:               # @list_DeleteElementIfFree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi155:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi156:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi158:
	.cfi_def_cfa_offset 64
.Lcfi159:
	.cfi_offset %rbx, -56
.Lcfi160:
	.cfi_offset %r12, -48
.Lcfi161:
	.cfi_offset %r13, -40
.Lcfi162:
	.cfi_offset %r14, -32
.Lcfi163:
	.cfi_offset %r15, -24
.Lcfi164:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB26_4
	.p2align	4, 0x90
.LBB26_1:                               # %.lr.ph45
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rdi
	callq	*%r15
	movq	(%r12), %rbp
	testl	%eax, %eax
	je	.LBB26_5
# BB#2:                                 #   in Loop: Header=BB26_1 Depth=1
	movq	8(%r12), %rdi
	callq	*%r14
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r12, (%rax)
	testq	%rbp, %rbp
	movq	%rbp, %r12
	jne	.LBB26_1
.LBB26_4:
	xorl	%r12d, %r12d
	jmp	.LBB26_11
.LBB26_5:                               # %.critedge.thread.preheader
	testq	%rbp, %rbp
	je	.LBB26_11
# BB#6:                                 # %.lr.ph.preheader
	movq	%r12, %rax
	movq	%r12, %rbx
.LBB26_7:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_8 Depth 2
	movq	%rbx, %r13
	movq	%rbp, %rbx
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB26_8:                               #   Parent Loop BB26_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	*%r15
	testl	%eax, %eax
	je	.LBB26_10
# BB#9:                                 # %.critedge.thread
                                        #   in Loop: Header=BB26_8 Depth=2
	movq	8(%rbx), %rdi
	callq	*%r14
	movq	(%rbx), %rax
	movq	%rax, (%r13)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	jne	.LBB26_8
	jmp	.LBB26_11
	.p2align	4, 0x90
.LBB26_10:                              # %.critedge.thread.outer.loopexit
                                        #   in Loop: Header=BB26_7 Depth=1
	movq	(%rbx), %rbp
	testq	%rbp, %rbp
	movq	%rbx, %rax
	jne	.LBB26_7
.LBB26_11:                              # %.critedge
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	list_DeleteElementIfFree, .Lfunc_end26-list_DeleteElementIfFree
	.cfi_endproc

	.globl	list_DeleteElementFree
	.p2align	4, 0x90
	.type	list_DeleteElementFree,@function
list_DeleteElementFree:                 # @list_DeleteElementFree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi165:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi166:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi167:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi168:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi169:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi170:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi171:
	.cfi_def_cfa_offset 64
.Lcfi172:
	.cfi_offset %rbx, -56
.Lcfi173:
	.cfi_offset %r12, -48
.Lcfi174:
	.cfi_offset %r13, -40
.Lcfi175:
	.cfi_offset %r14, -32
.Lcfi176:
	.cfi_offset %r15, -24
.Lcfi177:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbp
	testq	%rbp, %rbp
	je	.LBB27_4
	.p2align	4, 0x90
.LBB27_1:                               # %.lr.ph47
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	movq	%r12, %rdi
	callq	*%r15
	movq	(%rbp), %r13
	testl	%eax, %eax
	je	.LBB27_5
# BB#2:                                 #   in Loop: Header=BB27_1 Depth=1
	movq	8(%rbp), %rdi
	callq	*%r14
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbp, (%rax)
	testq	%r13, %r13
	movq	%r13, %rbp
	jne	.LBB27_1
.LBB27_4:
	xorl	%ebp, %ebp
	jmp	.LBB27_11
.LBB27_5:                               # %.critedge.thread.preheader
	testq	%r13, %r13
	je	.LBB27_11
# BB#6:                                 # %.lr.ph.preheader
	movq	%rbp, %rax
	movq	%rbp, %rbx
.LBB27_7:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_8 Depth 2
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	%r13, %rbx
	movq	%rax, %r13
	.p2align	4, 0x90
.LBB27_8:                               #   Parent Loop BB27_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	callq	*%r15
	testl	%eax, %eax
	je	.LBB27_10
# BB#9:                                 # %.critedge.thread
                                        #   in Loop: Header=BB27_8 Depth=2
	movq	(%rbx), %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rax, (%rcx)
	movq	8(%rbx), %rdi
	callq	*%r14
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	movq	(%r13), %rbx
	testq	%rbx, %rbx
	jne	.LBB27_8
	jmp	.LBB27_11
	.p2align	4, 0x90
.LBB27_10:                              # %.critedge.thread.outer.loopexit
                                        #   in Loop: Header=BB27_7 Depth=1
	movq	(%rbx), %r13
	testq	%r13, %r13
	movq	%rbx, %rax
	jne	.LBB27_7
.LBB27_11:                              # %.critedge
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	list_DeleteElementFree, .Lfunc_end27-list_DeleteElementFree
	.cfi_endproc

	.globl	list_DeleteOneElement
	.p2align	4, 0x90
	.type	list_DeleteOneElement,@function
list_DeleteOneElement:                  # @list_DeleteOneElement
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi178:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi179:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi180:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi181:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi182:
	.cfi_def_cfa_offset 48
.Lcfi183:
	.cfi_offset %rbx, -48
.Lcfi184:
	.cfi_offset %r12, -40
.Lcfi185:
	.cfi_offset %r13, -32
.Lcfi186:
	.cfi_offset %r14, -24
.Lcfi187:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB28_1
# BB#2:
	movq	8(%r14), %rsi
	movq	%r12, %rdi
	callq	*%r15
	testl	%eax, %eax
	je	.LBB28_3
# BB#8:
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	movq	%rax, %r14
	jmp	.LBB28_7
.LBB28_1:
	xorl	%r14d, %r14d
	jmp	.LBB28_7
.LBB28_3:
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB28_4:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r13
	movq	(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB28_7
# BB#5:                                 #   in Loop: Header=BB28_4 Depth=1
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	callq	*%r15
	testl	%eax, %eax
	je	.LBB28_4
# BB#6:
	movq	(%rbx), %rax
	movq	%rax, (%r13)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
.LBB28_7:                               # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end28:
	.size	list_DeleteOneElement, .Lfunc_end28-list_DeleteOneElement
	.cfi_endproc

	.globl	list_PointerDeleteElement
	.p2align	4, 0x90
	.type	list_PointerDeleteElement,@function
list_PointerDeleteElement:              # @list_PointerDeleteElement
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB29_1
	.p2align	4, 0x90
.LBB29_2:                               # %.lr.ph39
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rax
	cmpq	%rsi, 8(%rdi)
	jne	.LBB29_3
# BB#5:                                 #   in Loop: Header=BB29_2 Depth=1
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rdi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rdi
	jne	.LBB29_2
# BB#6:
	xorl	%edi, %edi
	movq	%rdi, %rax
	retq
.LBB29_1:
	xorl	%edi, %edi
	movq	%rdi, %rax
	retq
.LBB29_3:                               # %.critedge.thread.preheader
	testq	%rax, %rax
	je	.LBB29_11
# BB#4:                                 # %.lr.ph.preheader
	movq	%rdi, %r8
	movq	%rdi, %rcx
.LBB29_9:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_7 Depth 2
	movq	%rcx, %r9
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB29_7:                               #   Parent Loop BB29_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, 8(%rcx)
	jne	.LBB29_8
# BB#10:                                # %.critedge.thread
                                        #   in Loop: Header=BB29_7 Depth=2
	movq	(%rcx), %rax
	movq	%rax, (%r9)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rcx, (%rax)
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	jne	.LBB29_7
	jmp	.LBB29_11
.LBB29_8:                               # %.critedge.thread.outer.loopexit
                                        #   in Loop: Header=BB29_9 Depth=1
	movq	(%rcx), %rax
	testq	%rax, %rax
	movq	%rcx, %r8
	jne	.LBB29_9
.LBB29_11:                              # %.critedge
	movq	%rdi, %rax
	retq
.Lfunc_end29:
	.size	list_PointerDeleteElement, .Lfunc_end29-list_PointerDeleteElement
	.cfi_endproc

	.globl	list_PointerDeleteElementFree
	.p2align	4, 0x90
	.type	list_PointerDeleteElementFree,@function
list_PointerDeleteElementFree:          # @list_PointerDeleteElementFree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi188:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi189:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi190:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi191:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi192:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi193:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi194:
	.cfi_def_cfa_offset 64
.Lcfi195:
	.cfi_offset %rbx, -56
.Lcfi196:
	.cfi_offset %r12, -48
.Lcfi197:
	.cfi_offset %r13, -40
.Lcfi198:
	.cfi_offset %r14, -32
.Lcfi199:
	.cfi_offset %r15, -24
.Lcfi200:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB30_4
	.p2align	4, 0x90
.LBB30_1:                               # %.lr.ph45
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %r13
	cmpq	%r15, 8(%r12)
	jne	.LBB30_5
# BB#2:                                 #   in Loop: Header=BB30_1 Depth=1
	movq	%r15, %rdi
	callq	*%r14
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r12, (%rax)
	testq	%r13, %r13
	movq	%r13, %r12
	jne	.LBB30_1
.LBB30_4:
	xorl	%r12d, %r12d
	jmp	.LBB30_11
.LBB30_5:                               # %.critedge.thread.preheader
	testq	%r13, %r13
	je	.LBB30_11
# BB#6:                                 # %.lr.ph.preheader
	movq	%r12, %rax
	movq	%r12, %rbx
.LBB30_7:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_8 Depth 2
	movq	%rbx, %rcx
	movq	%r13, %rbx
	movq	%rcx, %r13
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB30_8:                               #   Parent Loop BB30_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%r15, 8(%rbx)
	jne	.LBB30_10
# BB#9:                                 # %.critedge.thread
                                        #   in Loop: Header=BB30_8 Depth=2
	movq	(%rbx), %rax
	movq	%rax, (%r13)
	movq	8(%rbx), %rdi
	callq	*%r14
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	jne	.LBB30_8
	jmp	.LBB30_11
.LBB30_10:                              # %.critedge.thread.outer.loopexit
                                        #   in Loop: Header=BB30_7 Depth=1
	movq	(%rbx), %r13
	testq	%r13, %r13
	movq	%rbx, %rax
	jne	.LBB30_7
.LBB30_11:                              # %.critedge
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	list_PointerDeleteElementFree, .Lfunc_end30-list_PointerDeleteElementFree
	.cfi_endproc

	.globl	list_PointerDeleteOneElement
	.p2align	4, 0x90
	.type	list_PointerDeleteOneElement,@function
list_PointerDeleteOneElement:           # @list_PointerDeleteOneElement
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB31_1
# BB#2:
	cmpq	%rsi, 8(%rdi)
	je	.LBB31_8
# BB#3:
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB31_4:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.LBB31_7
# BB#5:                                 #   in Loop: Header=BB31_4 Depth=1
	cmpq	%rsi, 8(%rax)
	jne	.LBB31_4
# BB#6:
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
.LBB31_7:                               # %.loopexit
	movq	%rdi, %rax
	retq
.LBB31_1:
	xorl	%edi, %edi
	movq	%rdi, %rax
	retq
.LBB31_8:
	movq	(%rdi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rdi, (%rcx)
	retq
.Lfunc_end31:
	.size	list_PointerDeleteOneElement, .Lfunc_end31-list_PointerDeleteOneElement
	.cfi_endproc

	.globl	list_DeleteFromList
	.p2align	4, 0x90
	.type	list_DeleteFromList,@function
list_DeleteFromList:                    # @list_DeleteFromList
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.LBB32_11
	.p2align	4, 0x90
.LBB32_1:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rsi, 8(%rdx)
	jne	.LBB32_2
# BB#3:                                 #   in Loop: Header=BB32_1 Depth=1
	movq	(%rdx), %rcx
	movq	memory_ARRAY+128(%rip), %r8
	movslq	32(%r8), %rax
	addq	%rax, memory_FREEDBYTES(%rip)
	movq	(%r8), %rax
	movq	%rax, (%rdx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rdx, (%rax)
	movq	%rcx, (%rdi)
	movl	$1, %eax
	testq	%rcx, %rcx
	movq	%rcx, %rdx
	jne	.LBB32_1
	jmp	.LBB32_4
.LBB32_2:
	movq	%rdx, %rcx
.LBB32_4:                               # %.loopexit30
	testq	%rcx, %rcx
	je	.LBB32_11
# BB#5:                                 # %.preheader
	movq	(%rcx), %r8
	testq	%r8, %r8
	je	.LBB32_11
# BB#6:                                 # %.lr.ph.preheader
	movq	%rcx, %r9
.LBB32_8:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_9 Depth 2
	movq	%rcx, %rdi
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB32_9:                               #   Parent Loop BB32_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, 8(%rcx)
	jne	.LBB32_7
# BB#10:                                #   in Loop: Header=BB32_9 Depth=2
	movq	(%rcx), %rax
	movq	%rax, (%rdi)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rcx, (%rax)
	movq	(%r9), %rcx
	movl	$1, %eax
	testq	%rcx, %rcx
	jne	.LBB32_9
	jmp	.LBB32_11
	.p2align	4, 0x90
.LBB32_7:                               # %.outer.loopexit
                                        #   in Loop: Header=BB32_8 Depth=1
	movq	(%rcx), %r8
	testq	%r8, %r8
	movq	%rcx, %r9
	jne	.LBB32_8
.LBB32_11:                              # %.loopexit
	retq
.Lfunc_end32:
	.size	list_DeleteFromList, .Lfunc_end32-list_DeleteFromList
	.cfi_endproc

	.globl	list_DeleteOneFromList
	.p2align	4, 0x90
	.type	list_DeleteOneFromList,@function
list_DeleteOneFromList:                 # @list_DeleteOneFromList
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB33_7
# BB#1:
	movq	(%rcx), %rdx
	cmpq	%rsi, 8(%rcx)
	je	.LBB33_2
	.p2align	4, 0x90
.LBB33_3:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdi
	movq	%rdx, %rcx
	testq	%rcx, %rcx
	je	.LBB33_7
# BB#4:                                 #   in Loop: Header=BB33_3 Depth=1
	movq	(%rcx), %rdx
	cmpq	%rsi, 8(%rcx)
	jne	.LBB33_3
# BB#5:
	movq	%rdx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rcx, (%rax)
	jmp	.LBB33_6
.LBB33_2:                               # %.thread
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rcx, (%rax)
	movq	%rdx, (%rdi)
.LBB33_6:                               # %.thread30
	movl	$1, %eax
.LBB33_7:                               # %.thread30
	retq
.Lfunc_end33:
	.size	list_DeleteOneFromList, .Lfunc_end33-list_DeleteOneFromList
	.cfi_endproc

	.globl	list_IsSetOfPointers
	.p2align	4, 0x90
	.type	list_IsSetOfPointers,@function
list_IsSetOfPointers:                   # @list_IsSetOfPointers
	.cfi_startproc
# BB#0:
	movl	$1, %eax
	testq	%rdi, %rdi
	je	.LBB34_7
.LBB34_1:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_3 Depth 2
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB34_7
# BB#2:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB34_1 Depth=1
	movq	8(%rdi), %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB34_3:                               # %.lr.ph.i
                                        #   Parent Loop BB34_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdx, 8(%rsi)
	je	.LBB34_4
# BB#5:                                 #   in Loop: Header=BB34_3 Depth=2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB34_3
# BB#6:                                 # %.loopexit
                                        #   in Loop: Header=BB34_1 Depth=1
	testq	%rcx, %rcx
	movq	%rcx, %rdi
	jne	.LBB34_1
.LBB34_7:                               # %list_PointerMember.exit
	retq
.LBB34_4:
	xorl	%eax, %eax
	retq
.Lfunc_end34:
	.size	list_IsSetOfPointers, .Lfunc_end34-list_IsSetOfPointers
	.cfi_endproc

	.globl	list_DeleteDuplicates
	.p2align	4, 0x90
	.type	list_DeleteDuplicates,@function
list_DeleteDuplicates:                  # @list_DeleteDuplicates
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi201:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi202:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi203:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi204:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi205:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi206:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi207:
	.cfi_def_cfa_offset 80
.Lcfi208:
	.cfi_offset %rbx, -56
.Lcfi209:
	.cfi_offset %r12, -48
.Lcfi210:
	.cfi_offset %r13, -40
.Lcfi211:
	.cfi_offset %r14, -32
.Lcfi212:
	.cfi_offset %r15, -24
.Lcfi213:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbp
	testq	%rbp, %rbp
	je	.LBB35_17
# BB#1:                                 # %.lr.ph.preheader
	movq	(%rbp), %rax
	movq	%rbp, %rbx
	testq	%rax, %rax
	je	.LBB35_16
# BB#2:                                 # %.lr.ph41.i.preheader.preheader
	movq	%rbp, %r14
.LBB35_3:                               # %.lr.ph41.i.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_4 Depth 2
                                        #     Child Loop BB35_12 Depth 2
                                        #       Child Loop BB35_10 Depth 3
	movq	8(%rbx), %r12
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB35_4:                               # %.lr.ph41.i
                                        #   Parent Loop BB35_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	callq	*%r15
	movq	(%rbx), %rcx
	testl	%eax, %eax
	je	.LBB35_5
# BB#8:                                 #   in Loop: Header=BB35_4 Depth=2
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB35_4
	jmp	.LBB35_9
	.p2align	4, 0x90
.LBB35_5:                               # %.critedge.thread.preheader.i
                                        #   in Loop: Header=BB35_3 Depth=1
	testq	%rcx, %rcx
	je	.LBB35_7
# BB#6:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB35_3 Depth=1
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %r13
	movq	%rbx, %rbp
.LBB35_12:                              # %.lr.ph.i
                                        #   Parent Loop BB35_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB35_10 Depth 3
	movq	%rbp, %r14
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB35_10:                              #   Parent Loop BB35_3 Depth=1
                                        #     Parent Loop BB35_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rsi
	movq	%r12, %rdi
	callq	*%r15
	testl	%eax, %eax
	je	.LBB35_11
# BB#13:                                # %.critedge.thread.i
                                        #   in Loop: Header=BB35_10 Depth=3
	movq	(%rbp), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbp, (%rax)
	movq	(%r13), %rbp
	testq	%rbp, %rbp
	jne	.LBB35_10
	jmp	.LBB35_14
	.p2align	4, 0x90
.LBB35_11:                              # %.critedge.thread.outer.loopexit.i
                                        #   in Loop: Header=BB35_12 Depth=2
	movq	(%rbp), %rcx
	testq	%rcx, %rcx
	movq	%rbp, %r13
	jne	.LBB35_12
	.p2align	4, 0x90
.LBB35_14:                              # %list_DeleteElement.exit
                                        #   in Loop: Header=BB35_3 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rbx, (%rax)
	testq	%rbx, %rbx
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB35_15
	jmp	.LBB35_17
.LBB35_7:                               # %list_DeleteElement.exit.thread24
                                        #   in Loop: Header=BB35_3 Depth=1
	movq	%rbx, (%r14)
.LBB35_15:                              # %.lr.ph.backedge
                                        #   in Loop: Header=BB35_3 Depth=1
	movq	(%rbx), %rax
	testq	%rax, %rax
	movq	%rbx, %r14
	jne	.LBB35_3
	jmp	.LBB35_16
.LBB35_9:
	movq	%r14, %rbx
.LBB35_16:                              # %list_DeleteElement.exit.thread
	movq	$0, (%rbx)
.LBB35_17:                              # %._crit_edge
	movq	%rbp, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end35:
	.size	list_DeleteDuplicates, .Lfunc_end35-list_DeleteDuplicates
	.cfi_endproc

	.globl	list_DeleteDuplicatesFree
	.p2align	4, 0x90
	.type	list_DeleteDuplicatesFree,@function
list_DeleteDuplicatesFree:              # @list_DeleteDuplicatesFree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi214:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi215:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi216:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi217:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi218:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi219:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi220:
	.cfi_def_cfa_offset 80
.Lcfi221:
	.cfi_offset %rbx, -56
.Lcfi222:
	.cfi_offset %r12, -48
.Lcfi223:
	.cfi_offset %r13, -40
.Lcfi224:
	.cfi_offset %r14, -32
.Lcfi225:
	.cfi_offset %r15, -24
.Lcfi226:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbp
	testq	%rbp, %rbp
	je	.LBB36_17
# BB#1:                                 # %.lr.ph.preheader
	movq	(%rbp), %rax
	movq	%rbp, %r14
	testq	%rax, %rax
	je	.LBB36_16
# BB#2:                                 # %.lr.ph47.i.preheader.preheader
	movq	%rbp, (%rsp)            # 8-byte Spill
.LBB36_3:                               # %.lr.ph47.i.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB36_4 Depth 2
                                        #     Child Loop BB36_12 Depth 2
                                        #       Child Loop BB36_10 Depth 3
	movq	8(%r14), %r13
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB36_4:                               # %.lr.ph47.i
                                        #   Parent Loop BB36_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r14), %rsi
	movq	%r13, %rdi
	callq	*%r12
	movq	(%r14), %rbx
	testl	%eax, %eax
	je	.LBB36_5
# BB#8:                                 #   in Loop: Header=BB36_4 Depth=2
	movq	8(%r14), %rdi
	callq	*%r15
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	testq	%rbx, %rbx
	movq	%rbx, %r14
	jne	.LBB36_4
	jmp	.LBB36_9
	.p2align	4, 0x90
.LBB36_5:                               # %.critedge.thread.preheader.i
                                        #   in Loop: Header=BB36_3 Depth=1
	testq	%rbx, %rbx
	je	.LBB36_7
# BB#6:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB36_3 Depth=1
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%r14, %rbp
.LBB36_12:                              # %.lr.ph.i
                                        #   Parent Loop BB36_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB36_10 Depth 3
	movq	%rbp, %rax
	movq	%rbx, %rbp
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB36_10:                              #   Parent Loop BB36_3 Depth=1
                                        #     Parent Loop BB36_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rsi
	movq	%r13, %rdi
	callq	*%r12
	testl	%eax, %eax
	je	.LBB36_11
# BB#13:                                # %.critedge.thread.i
                                        #   in Loop: Header=BB36_10 Depth=3
	movq	(%rbp), %rax
	movq	%rax, (%rbx)
	movq	8(%rbp), %rdi
	callq	*%r15
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbp, (%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	jne	.LBB36_10
	jmp	.LBB36_14
	.p2align	4, 0x90
.LBB36_11:                              # %.critedge.thread.outer.loopexit.i
                                        #   in Loop: Header=BB36_12 Depth=2
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jne	.LBB36_12
	.p2align	4, 0x90
.LBB36_14:                              # %list_DeleteElementFree.exit
                                        #   in Loop: Header=BB36_3 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%r14, (%rax)
	testq	%r14, %r14
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB36_15
	jmp	.LBB36_17
.LBB36_7:                               # %list_DeleteElementFree.exit.thread25
                                        #   in Loop: Header=BB36_3 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%r14, (%rax)
.LBB36_15:                              # %.lr.ph.backedge
                                        #   in Loop: Header=BB36_3 Depth=1
	movq	(%r14), %rax
	testq	%rax, %rax
	movq	%r14, (%rsp)            # 8-byte Spill
	jne	.LBB36_3
	jmp	.LBB36_16
.LBB36_9:
	movq	(%rsp), %r14            # 8-byte Reload
.LBB36_16:                              # %list_DeleteElementFree.exit.thread
	movq	$0, (%r14)
.LBB36_17:                              # %._crit_edge
	movq	%rbp, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end36:
	.size	list_DeleteDuplicatesFree, .Lfunc_end36-list_DeleteDuplicatesFree
	.cfi_endproc

	.globl	list_PointerDeleteDuplicates
	.p2align	4, 0x90
	.type	list_PointerDeleteDuplicates,@function
list_PointerDeleteDuplicates:           # @list_PointerDeleteDuplicates
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB37_17
# BB#1:                                 # %.lr.ph.preheader
	movq	(%rdi), %rax
	movq	%rdi, %rcx
	testq	%rax, %rax
	je	.LBB37_16
# BB#2:                                 # %.lr.ph39.i.preheader.preheader
	movq	%rdi, %r8
.LBB37_3:                               # %.lr.ph39.i.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_4 Depth 2
                                        #     Child Loop BB37_12 Depth 2
                                        #       Child Loop BB37_10 Depth 3
	movq	8(%rcx), %r11
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB37_4:                               # %.lr.ph39.i
                                        #   Parent Loop BB37_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	cmpq	%r11, 8(%rcx)
	jne	.LBB37_5
# BB#8:                                 #   in Loop: Header=BB37_4 Depth=2
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rcx, (%rdx)
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.LBB37_4
	jmp	.LBB37_9
	.p2align	4, 0x90
.LBB37_5:                               # %.critedge.thread.preheader.i
                                        #   in Loop: Header=BB37_3 Depth=1
	testq	%rax, %rax
	je	.LBB37_7
# BB#6:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB37_3 Depth=1
	movq	%rcx, %r9
	movq	%rcx, %rsi
.LBB37_12:                              # %.lr.ph.i
                                        #   Parent Loop BB37_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB37_10 Depth 3
	movq	%rsi, %r10
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB37_10:                              #   Parent Loop BB37_3 Depth=1
                                        #     Parent Loop BB37_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r11, 8(%rsi)
	jne	.LBB37_11
# BB#13:                                # %.critedge.thread.i
                                        #   in Loop: Header=BB37_10 Depth=3
	movq	(%rsi), %rax
	movq	%rax, (%r10)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rsi, (%rax)
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	jne	.LBB37_10
	jmp	.LBB37_14
	.p2align	4, 0x90
.LBB37_11:                              # %.critedge.thread.outer.loopexit.i
                                        #   in Loop: Header=BB37_12 Depth=2
	movq	(%rsi), %rax
	testq	%rax, %rax
	movq	%rsi, %r9
	jne	.LBB37_12
	.p2align	4, 0x90
.LBB37_14:                              # %list_PointerDeleteElement.exit
                                        #   in Loop: Header=BB37_3 Depth=1
	movq	%rcx, (%r8)
	testq	%rcx, %rcx
	jne	.LBB37_15
	jmp	.LBB37_17
.LBB37_7:                               # %list_PointerDeleteElement.exit.thread23
                                        #   in Loop: Header=BB37_3 Depth=1
	movq	%rcx, (%r8)
.LBB37_15:                              # %.lr.ph.backedge
                                        #   in Loop: Header=BB37_3 Depth=1
	movq	(%rcx), %rax
	testq	%rax, %rax
	movq	%rcx, %r8
	jne	.LBB37_3
	jmp	.LBB37_16
.LBB37_9:
	movq	%r8, %rcx
.LBB37_16:                              # %list_PointerDeleteElement.exit.thread
	movq	$0, (%rcx)
.LBB37_17:                              # %._crit_edge
	movq	%rdi, %rax
	retq
.Lfunc_end37:
	.size	list_PointerDeleteDuplicates, .Lfunc_end37-list_PointerDeleteDuplicates
	.cfi_endproc

	.globl	list_NPointerUnion
	.p2align	4, 0x90
	.type	list_NPointerUnion,@function
list_NPointerUnion:                     # @list_NPointerUnion
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB38_1
# BB#2:
	testq	%rsi, %rsi
	je	.LBB38_8
# BB#3:                                 # %.preheader.i.preheader
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB38_4:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB38_4
# BB#5:
	movq	%rsi, (%rax)
	testq	%rdi, %rdi
	jne	.LBB38_8
	jmp	.LBB38_7
.LBB38_1:
	movq	%rsi, %rdi
	testq	%rdi, %rdi
	je	.LBB38_7
.LBB38_8:                               # %.lr.ph.i.preheader
	movq	(%rdi), %rax
	movq	%rdi, %rcx
	testq	%rax, %rax
	je	.LBB38_16
# BB#9:                                 # %.lr.ph39.i.i.preheader.preheader
	movq	%rdi, %r8
.LBB38_10:                              # %.lr.ph39.i.i.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB38_11 Depth 2
                                        #     Child Loop BB38_21 Depth 2
                                        #       Child Loop BB38_19 Depth 3
	movq	8(%rcx), %r11
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB38_11:                              # %.lr.ph39.i.i
                                        #   Parent Loop BB38_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	cmpq	%r11, 8(%rcx)
	jne	.LBB38_12
# BB#17:                                #   in Loop: Header=BB38_11 Depth=2
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rcx, (%rdx)
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.LBB38_11
	jmp	.LBB38_18
	.p2align	4, 0x90
.LBB38_12:                              # %.critedge.thread.preheader.i.i
                                        #   in Loop: Header=BB38_10 Depth=1
	testq	%rax, %rax
	je	.LBB38_14
# BB#13:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB38_10 Depth=1
	movq	%rcx, %r9
	movq	%rcx, %rsi
.LBB38_21:                              # %.lr.ph.i.i
                                        #   Parent Loop BB38_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB38_19 Depth 3
	movq	%rsi, %r10
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB38_19:                              #   Parent Loop BB38_10 Depth=1
                                        #     Parent Loop BB38_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r11, 8(%rsi)
	jne	.LBB38_20
# BB#22:                                # %.critedge.thread.i.i
                                        #   in Loop: Header=BB38_19 Depth=3
	movq	(%rsi), %rax
	movq	%rax, (%r10)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rsi, (%rax)
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	jne	.LBB38_19
	jmp	.LBB38_23
	.p2align	4, 0x90
.LBB38_20:                              # %.critedge.thread.outer.loopexit.i.i
                                        #   in Loop: Header=BB38_21 Depth=2
	movq	(%rsi), %rax
	testq	%rax, %rax
	movq	%rsi, %r9
	jne	.LBB38_21
	.p2align	4, 0x90
.LBB38_23:                              # %list_PointerDeleteElement.exit.i
                                        #   in Loop: Header=BB38_10 Depth=1
	movq	%rcx, (%r8)
	testq	%rcx, %rcx
	jne	.LBB38_15
	jmp	.LBB38_24
.LBB38_14:                              # %list_PointerDeleteElement.exit.thread23.i
                                        #   in Loop: Header=BB38_10 Depth=1
	movq	%rcx, (%r8)
.LBB38_15:                              # %.lr.ph.i.backedge
                                        #   in Loop: Header=BB38_10 Depth=1
	movq	(%rcx), %rax
	testq	%rax, %rax
	movq	%rcx, %r8
	jne	.LBB38_10
.LBB38_16:                              # %list_PointerDeleteElement.exit.thread.i
	movq	$0, (%rcx)
	movq	%rdi, %rax
	retq
.LBB38_18:
	movq	%r8, %rcx
	movq	$0, (%rcx)
	movq	%rdi, %rax
	retq
.LBB38_7:
	xorl	%edi, %edi
	movq	%rdi, %rax
	retq
.LBB38_24:                              # %list_PointerDeleteDuplicates.exit
	movq	%rdi, %rax
	retq
.Lfunc_end38:
	.size	list_NPointerUnion, .Lfunc_end38-list_NPointerUnion
	.cfi_endproc

	.globl	list_NUnion
	.p2align	4, 0x90
	.type	list_NUnion,@function
list_NUnion:                            # @list_NUnion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi227:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi228:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi229:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi230:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi231:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi232:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi233:
	.cfi_def_cfa_offset 80
.Lcfi234:
	.cfi_offset %rbx, -56
.Lcfi235:
	.cfi_offset %r12, -48
.Lcfi236:
	.cfi_offset %r13, -40
.Lcfi237:
	.cfi_offset %r14, -32
.Lcfi238:
	.cfi_offset %r15, -24
.Lcfi239:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rdi, %rbp
	testq	%rbp, %rbp
	je	.LBB39_1
# BB#2:
	testq	%rsi, %rsi
	je	.LBB39_8
# BB#3:                                 # %.preheader.i.preheader
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB39_4:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB39_4
# BB#5:
	movq	%rsi, (%rax)
	testq	%rbp, %rbp
	jne	.LBB39_8
	jmp	.LBB39_7
.LBB39_1:
	movq	%rsi, %rbp
	testq	%rbp, %rbp
	je	.LBB39_7
.LBB39_8:                               # %.lr.ph.i.preheader
	movq	(%rbp), %rax
	movq	%rbp, %rbx
	testq	%rax, %rax
	je	.LBB39_16
# BB#9:                                 # %.lr.ph41.i.i.preheader.preheader
	movq	%rbp, %r14
.LBB39_10:                              # %.lr.ph41.i.i.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_11 Depth 2
                                        #     Child Loop BB39_21 Depth 2
                                        #       Child Loop BB39_19 Depth 3
	movq	8(%rbx), %r12
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB39_11:                              # %.lr.ph41.i.i
                                        #   Parent Loop BB39_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	callq	*%r15
	movq	(%rbx), %rcx
	testl	%eax, %eax
	je	.LBB39_12
# BB#17:                                #   in Loop: Header=BB39_11 Depth=2
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB39_11
	jmp	.LBB39_18
	.p2align	4, 0x90
.LBB39_12:                              # %.critedge.thread.preheader.i.i
                                        #   in Loop: Header=BB39_10 Depth=1
	testq	%rcx, %rcx
	je	.LBB39_14
# BB#13:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB39_10 Depth=1
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %r13
	movq	%rbx, %rbp
.LBB39_21:                              # %.lr.ph.i.i
                                        #   Parent Loop BB39_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB39_19 Depth 3
	movq	%rbp, %r14
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB39_19:                              #   Parent Loop BB39_10 Depth=1
                                        #     Parent Loop BB39_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rsi
	movq	%r12, %rdi
	callq	*%r15
	testl	%eax, %eax
	je	.LBB39_20
# BB#22:                                # %.critedge.thread.i.i
                                        #   in Loop: Header=BB39_19 Depth=3
	movq	(%rbp), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbp, (%rax)
	movq	(%r13), %rbp
	testq	%rbp, %rbp
	jne	.LBB39_19
	jmp	.LBB39_23
	.p2align	4, 0x90
.LBB39_20:                              # %.critedge.thread.outer.loopexit.i.i
                                        #   in Loop: Header=BB39_21 Depth=2
	movq	(%rbp), %rcx
	testq	%rcx, %rcx
	movq	%rbp, %r13
	jne	.LBB39_21
	.p2align	4, 0x90
.LBB39_23:                              # %list_DeleteElement.exit.i
                                        #   in Loop: Header=BB39_10 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rbx, (%rax)
	testq	%rbx, %rbx
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB39_15
	jmp	.LBB39_24
.LBB39_14:                              # %list_DeleteElement.exit.thread24.i
                                        #   in Loop: Header=BB39_10 Depth=1
	movq	%rbx, (%r14)
.LBB39_15:                              # %.lr.ph.i.backedge
                                        #   in Loop: Header=BB39_10 Depth=1
	movq	(%rbx), %rax
	testq	%rax, %rax
	movq	%rbx, %r14
	jne	.LBB39_10
.LBB39_16:                              # %list_DeleteElement.exit.thread.i
	movq	$0, (%rbx)
	jmp	.LBB39_24
.LBB39_18:
	movq	%r14, %rbx
	movq	$0, (%rbx)
	jmp	.LBB39_24
.LBB39_7:
	xorl	%ebp, %ebp
.LBB39_24:                              # %list_DeleteDuplicates.exit
	movq	%rbp, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end39:
	.size	list_NUnion, .Lfunc_end39-list_NUnion
	.cfi_endproc

	.globl	list_NListTimes
	.p2align	4, 0x90
	.type	list_NListTimes,@function
list_NListTimes:                        # @list_NListTimes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi240:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi241:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi242:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi243:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi244:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi245:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi246:
	.cfi_def_cfa_offset 96
.Lcfi247:
	.cfi_offset %rbx, -56
.Lcfi248:
	.cfi_offset %r12, -48
.Lcfi249:
	.cfi_offset %r13, -40
.Lcfi250:
	.cfi_offset %r14, -32
.Lcfi251:
	.cfi_offset %r15, -24
.Lcfi252:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	xorl	%eax, %eax
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	testq	%rdi, %rdi
	je	.LBB40_20
# BB#1:
	testq	%r14, %r14
	je	.LBB40_20
# BB#2:                                 # %.preheader.preheader
	xorl	%ecx, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB40_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_7 Depth 2
                                        #     Child Loop BB40_12 Depth 2
                                        #     Child Loop BB40_15 Depth 2
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %r15
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB40_4
# BB#5:                                 #   in Loop: Header=BB40_3 Depth=1
	movq	8(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%rbp, 8(%r12)
	movq	$0, (%r12)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB40_8
# BB#6:                                 # %.lr.ph.i28.preheader
                                        #   in Loop: Header=BB40_3 Depth=1
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB40_7:                               # %.lr.ph.i28
                                        #   Parent Loop BB40_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, (%rbp)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %rbp
	jne	.LBB40_7
	jmp	.LBB40_8
	.p2align	4, 0x90
.LBB40_4:                               #   in Loop: Header=BB40_3 Depth=1
	xorl	%r12d, %r12d
.LBB40_8:                               # %list_Copy.exit
                                        #   in Loop: Header=BB40_3 Depth=1
	testq	%r15, %r15
	je	.LBB40_9
# BB#10:                                #   in Loop: Header=BB40_3 Depth=1
	movq	8(%r15), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%rbx, 8(%r13)
	movq	$0, (%r13)
	movq	(%r15), %rbp
	testq	%rbp, %rbp
	je	.LBB40_13
# BB#11:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB40_3 Depth=1
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB40_12:                              # %.lr.ph.i.i
                                        #   Parent Loop BB40_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, (%rbx)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%rax, %rbx
	jne	.LBB40_12
.LBB40_13:                              # %list_Copy.exit.i
                                        #   in Loop: Header=BB40_3 Depth=1
	testq	%r12, %r12
	movq	24(%rsp), %rbx          # 8-byte Reload
	je	.LBB40_17
# BB#14:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB40_3 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB40_15:                              # %.preheader.i
                                        #   Parent Loop BB40_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB40_15
# BB#16:                                #   in Loop: Header=BB40_3 Depth=1
	movq	%r12, (%rax)
.LBB40_17:                              # %list_Append.exit
                                        #   in Loop: Header=BB40_3 Depth=1
	movq	%r13, %r12
	jmp	.LBB40_18
	.p2align	4, 0x90
.LBB40_9:                               #   in Loop: Header=BB40_3 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB40_18:                              # %list_Append.exit
                                        #   in Loop: Header=BB40_3 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%rbx, (%rax)
	movq	(%r14), %r14
	testq	%r14, %r14
	movq	%rax, %rcx
	jne	.LBB40_3
# BB#19:                                # %._crit_edge
                                        #   in Loop: Header=BB40_3 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	testq	%rcx, %rcx
	movq	%rax, %rcx
	movq	32(%rsp), %r14          # 8-byte Reload
	jne	.LBB40_3
.LBB40_20:                              # %.loopexit
	movq	16(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB40_24
	.p2align	4, 0x90
.LBB40_21:                              # %.lr.ph.i26
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_22 Depth 2
	movq	(%rcx), %r8
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB40_23
	.p2align	4, 0x90
.LBB40_22:                              # %.lr.ph.i40
                                        #   Parent Loop BB40_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rbx
	movq	memory_ARRAY+128(%rip), %rdi
	movslq	32(%rdi), %rbp
	addq	%rbp, memory_FREEDBYTES(%rip)
	movq	(%rdi), %rdi
	movq	%rdi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rdi
	movq	%rdx, (%rdi)
	testq	%rbx, %rbx
	movq	%rbx, %rdx
	jne	.LBB40_22
.LBB40_23:                              # %list_Delete.exit
                                        #   in Loop: Header=BB40_21 Depth=1
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rcx, (%rdx)
	testq	%r8, %r8
	movq	%r8, %rcx
	jne	.LBB40_21
.LBB40_24:                              # %list_DeleteWithElement.exit27
	testq	%r14, %r14
	je	.LBB40_28
	.p2align	4, 0x90
.LBB40_25:                              # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_26 Depth 2
	movq	(%r14), %rcx
	movq	8(%r14), %rdx
	testq	%rdx, %rdx
	je	.LBB40_27
	.p2align	4, 0x90
.LBB40_26:                              # %.lr.ph.i44
                                        #   Parent Loop BB40_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rbx
	movq	memory_ARRAY+128(%rip), %rdi
	movslq	32(%rdi), %rbp
	addq	%rbp, memory_FREEDBYTES(%rip)
	movq	(%rdi), %rdi
	movq	%rdi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rdi
	movq	%rdx, (%rdi)
	testq	%rbx, %rbx
	movq	%rbx, %rdx
	jne	.LBB40_26
.LBB40_27:                              # %list_Delete.exit45
                                        #   in Loop: Header=BB40_25 Depth=1
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r14)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r14, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r14
	jne	.LBB40_25
.LBB40_28:                              # %list_DeleteWithElement.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end40:
	.size	list_NListTimes, .Lfunc_end40-list_NListTimes
	.cfi_endproc

	.globl	list_NIntersect
	.p2align	4, 0x90
	.type	list_NIntersect,@function
list_NIntersect:                        # @list_NIntersect
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi253:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi254:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi255:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi256:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi257:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi258:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi259:
	.cfi_def_cfa_offset 64
.Lcfi260:
	.cfi_offset %rbx, -56
.Lcfi261:
	.cfi_offset %r12, -48
.Lcfi262:
	.cfi_offset %r13, -40
.Lcfi263:
	.cfi_offset %r14, -32
.Lcfi264:
	.cfi_offset %r15, -24
.Lcfi265:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r15
	movq	%rdi, %r14
	xorl	%r12d, %r12d
	testq	%r14, %r14
	je	.LBB41_16
# BB#1:                                 # %.lr.ph74
	testq	%r15, %r15
	je	.LBB41_6
.LBB41_2:                               # %.lr.ph74.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB41_3 Depth 2
	movq	8(%r14), %r13
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB41_3:                               # %.lr.ph.i34
                                        #   Parent Loop BB41_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	movq	%r13, %rdi
	callq	*%rbp
	testl	%eax, %eax
	jne	.LBB41_7
# BB#4:                                 #   in Loop: Header=BB41_3 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB41_3
# BB#5:                                 # %.loopexit43
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB41_2
	jmp	.LBB41_16
	.p2align	4, 0x90
.LBB41_6:                               # %.lr.ph74.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB41_6
	jmp	.LBB41_16
.LBB41_7:                               # %.critedge
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.LBB41_15
# BB#8:                                 # %.lr.ph.preheader
	movq	%r14, %r13
	movq	%r15, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB41_9:                               # %.lr.ph.i.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB41_10 Depth 2
	movq	8(%r12), %rbx
	.p2align	4, 0x90
.LBB41_10:                              # %.lr.ph.i
                                        #   Parent Loop BB41_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r15), %rsi
	movq	%rbx, %rdi
	callq	*%rbp
	testl	%eax, %eax
	jne	.LBB41_13
# BB#11:                                #   in Loop: Header=BB41_10 Depth=2
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB41_10
# BB#12:                                # %.loopexit
                                        #   in Loop: Header=BB41_9 Depth=1
	movq	(%r12), %rax
	movq	%rax, (%r13)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r12, (%rax)
	movq	(%r13), %r12
	jmp	.LBB41_14
	.p2align	4, 0x90
.LBB41_13:                              # %.outer
                                        #   in Loop: Header=BB41_9 Depth=1
	movq	(%r13), %r13
	movq	(%r12), %r12
.LBB41_14:                              # %.outer
                                        #   in Loop: Header=BB41_9 Depth=1
	testq	%r12, %r12
	movq	(%rsp), %r15            # 8-byte Reload
	jne	.LBB41_9
.LBB41_15:
	movq	%r14, %r12
.LBB41_16:                              # %.critedge.thread
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end41:
	.size	list_NIntersect, .Lfunc_end41-list_NIntersect
	.cfi_endproc

	.globl	list_NPointerIntersect
	.p2align	4, 0x90
	.type	list_NPointerIntersect,@function
list_NPointerIntersect:                 # @list_NPointerIntersect
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.LBB42_16
# BB#1:                                 # %.lr.ph72
	testq	%rsi, %rsi
	je	.LBB42_6
.LBB42_2:                               # %.lr.ph72.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB42_3 Depth 2
	movq	8(%rdi), %rcx
	movq	%rsi, %rdx
	.p2align	4, 0x90
.LBB42_3:                               # %.lr.ph.i32
                                        #   Parent Loop BB42_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, 8(%rdx)
	je	.LBB42_7
# BB#4:                                 #   in Loop: Header=BB42_3 Depth=2
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB42_3
# BB#5:                                 # %.loopexit41
                                        #   in Loop: Header=BB42_2 Depth=1
	movq	(%rdi), %r8
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rdi, (%rcx)
	testq	%r8, %r8
	movq	%r8, %rdi
	jne	.LBB42_2
	jmp	.LBB42_16
	.p2align	4, 0x90
.LBB42_6:                               # %.lr.ph72.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rdi, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rdi
	jne	.LBB42_6
	jmp	.LBB42_16
.LBB42_7:                               # %.critedge
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.LBB42_15
# BB#8:                                 # %.lr.ph.preheader
	movq	%rdi, %r8
	.p2align	4, 0x90
.LBB42_9:                               # %.lr.ph.i.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB42_10 Depth 2
	movq	8(%rax), %rdx
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB42_10:                              # %.lr.ph.i
                                        #   Parent Loop BB42_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdx, 8(%rcx)
	je	.LBB42_13
# BB#11:                                #   in Loop: Header=BB42_10 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB42_10
# BB#12:                                # %.loopexit
                                        #   in Loop: Header=BB42_9 Depth=1
	movq	(%rax), %rcx
	movq	%rcx, (%r8)
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.LBB42_9
	jmp	.LBB42_15
	.p2align	4, 0x90
.LBB42_13:                              # %.outer
                                        #   in Loop: Header=BB42_9 Depth=1
	movq	(%r8), %r8
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB42_9
.LBB42_15:
	movq	%rdi, %rax
.LBB42_16:                              # %.critedge.thread
	retq
.Lfunc_end42:
	.size	list_NPointerIntersect, .Lfunc_end42-list_NPointerIntersect
	.cfi_endproc

	.globl	list_NInsert
	.p2align	4, 0x90
	.type	list_NInsert,@function
list_NInsert:                           # @list_NInsert
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	%rsi, (%rdi)
	testq	%rsi, %rsi
	je	.LBB43_3
	.p2align	4, 0x90
.LBB43_1:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rdi
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	jne	.LBB43_1
.LBB43_3:                               # %._crit_edge12
	movq	%rax, (%rdi)
	retq
.Lfunc_end43:
	.size	list_NInsert, .Lfunc_end43-list_NInsert
	.cfi_endproc

	.globl	list_HasIntersection
	.p2align	4, 0x90
	.type	list_HasIntersection,@function
list_HasIntersection:                   # @list_HasIntersection
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.LBB44_1
	jmp	.LBB44_5
	.p2align	4, 0x90
.LBB44_7:                               # %.loopexit
                                        #   in Loop: Header=BB44_1 Depth=1
	movq	(%rdi), %rdi
.LBB44_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB44_3 Depth 2
	testq	%rdi, %rdi
	je	.LBB44_5
# BB#2:                                 # %.lr.ph.split
                                        #   in Loop: Header=BB44_1 Depth=1
	movq	8(%rdi), %rcx
	movq	%rsi, %rdx
	.p2align	4, 0x90
.LBB44_3:                               # %.lr.ph.i
                                        #   Parent Loop BB44_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, 8(%rdx)
	je	.LBB44_4
# BB#6:                                 #   in Loop: Header=BB44_3 Depth=2
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB44_3
	jmp	.LBB44_7
.LBB44_4:
	movl	$1, %eax
.LBB44_5:                               # %list_PointerMember.exit
	retq
.Lfunc_end44:
	.size	list_HasIntersection, .Lfunc_end44-list_HasIntersection
	.cfi_endproc

	.globl	list_NPointerDifference
	.p2align	4, 0x90
	.type	list_NPointerDifference,@function
list_NPointerDifference:                # @list_NPointerDifference
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	jne	.LBB45_3
	jmp	.LBB45_15
	.p2align	4, 0x90
.LBB45_1:                               # %list_PointerDeleteElement.exit
                                        #   in Loop: Header=BB45_4 Depth=2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB45_4
	jmp	.LBB45_15
	.p2align	4, 0x90
.LBB45_2:                               #   in Loop: Header=BB45_3 Depth=1
	xorl	%edi, %edi
	movq	(%rsi), %rsi
.LBB45_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB45_4 Depth 2
                                        #       Child Loop BB45_6 Depth 3
                                        #       Child Loop BB45_11 Depth 3
                                        #         Child Loop BB45_12 Depth 4
	testq	%rsi, %rsi
	je	.LBB45_15
.LBB45_4:                               # %.lr.ph
                                        #   Parent Loop BB45_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB45_6 Depth 3
                                        #       Child Loop BB45_11 Depth 3
                                        #         Child Loop BB45_12 Depth 4
	testq	%rdi, %rdi
	je	.LBB45_2
# BB#5:                                 # %.lr.ph39.i.preheader
                                        #   in Loop: Header=BB45_4 Depth=2
	movq	8(%rsi), %r10
	.p2align	4, 0x90
.LBB45_6:                               # %.lr.ph39.i
                                        #   Parent Loop BB45_3 Depth=1
                                        #     Parent Loop BB45_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdi), %rcx
	cmpq	%r10, 8(%rdi)
	jne	.LBB45_9
# BB#7:                                 #   in Loop: Header=BB45_6 Depth=3
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rax
	addq	%rax, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rax
	movq	%rax, (%rdi)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rdi, (%rax)
	testq	%rcx, %rcx
	movq	%rcx, %rdi
	jne	.LBB45_6
# BB#8:                                 #   in Loop: Header=BB45_4 Depth=2
	xorl	%edi, %edi
	jmp	.LBB45_1
	.p2align	4, 0x90
.LBB45_9:                               # %.critedge.thread.preheader.i
                                        #   in Loop: Header=BB45_4 Depth=2
	testq	%rcx, %rcx
	je	.LBB45_1
# BB#10:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB45_4 Depth=2
	movq	%rdi, %r8
	movq	%rdi, %rdx
.LBB45_11:                              # %.lr.ph.i
                                        #   Parent Loop BB45_3 Depth=1
                                        #     Parent Loop BB45_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB45_12 Depth 4
	movq	%rdx, %r9
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB45_12:                              #   Parent Loop BB45_3 Depth=1
                                        #     Parent Loop BB45_4 Depth=2
                                        #       Parent Loop BB45_11 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%r10, 8(%rdx)
	jne	.LBB45_14
# BB#13:                                # %.critedge.thread.i
                                        #   in Loop: Header=BB45_12 Depth=4
	movq	(%rdx), %rax
	movq	%rax, (%r9)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rdx, (%rax)
	movq	(%r8), %rdx
	testq	%rdx, %rdx
	jne	.LBB45_12
	jmp	.LBB45_1
	.p2align	4, 0x90
.LBB45_14:                              # %.critedge.thread.outer.loopexit.i
                                        #   in Loop: Header=BB45_11 Depth=3
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	movq	%rdx, %r8
	jne	.LBB45_11
	jmp	.LBB45_1
.LBB45_15:                              # %.loopexit
	movq	%rdi, %rax
	retq
.Lfunc_end45:
	.size	list_NPointerDifference, .Lfunc_end45-list_NPointerDifference
	.cfi_endproc

	.globl	list_NDifference
	.p2align	4, 0x90
	.type	list_NDifference,@function
list_NDifference:                       # @list_NDifference
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi266:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi267:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi268:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi269:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi270:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi271:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi272:
	.cfi_def_cfa_offset 64
.Lcfi273:
	.cfi_offset %rbx, -56
.Lcfi274:
	.cfi_offset %r12, -48
.Lcfi275:
	.cfi_offset %r13, -40
.Lcfi276:
	.cfi_offset %r14, -32
.Lcfi277:
	.cfi_offset %r15, -24
.Lcfi278:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r13
	testq	%r13, %r13
	jne	.LBB46_4
	jmp	.LBB46_16
.LBB46_1:                               #   in Loop: Header=BB46_5 Depth=2
	movq	(%rsp), %rbx            # 8-byte Reload
.LBB46_2:                               # %list_DeleteElement.exit
                                        #   in Loop: Header=BB46_5 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB46_16
.LBB46_5:                               # %.lr.ph
                                        #   Parent Loop BB46_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB46_7 Depth 3
                                        #       Child Loop BB46_12 Depth 3
                                        #         Child Loop BB46_13 Depth 4
	testq	%r13, %r13
	je	.LBB46_3
# BB#6:                                 # %.lr.ph41.i.preheader
                                        #   in Loop: Header=BB46_5 Depth=2
	movq	8(%rbx), %r12
	.p2align	4, 0x90
.LBB46_7:                               # %.lr.ph41.i
                                        #   Parent Loop BB46_4 Depth=1
                                        #     Parent Loop BB46_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	callq	*%r14
	movq	(%r13), %rcx
	testl	%eax, %eax
	je	.LBB46_10
# BB#8:                                 #   in Loop: Header=BB46_7 Depth=3
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r13, (%rax)
	testq	%rcx, %rcx
	movq	%rcx, %r13
	jne	.LBB46_7
# BB#9:                                 #   in Loop: Header=BB46_5 Depth=2
	xorl	%r13d, %r13d
	jmp	.LBB46_2
	.p2align	4, 0x90
.LBB46_10:                              # %.critedge.thread.preheader.i
                                        #   in Loop: Header=BB46_5 Depth=2
	testq	%rcx, %rcx
	je	.LBB46_2
# BB#11:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB46_5 Depth=2
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	%r13, %rbp
	movq	%r13, %rbx
	jmp	.LBB46_12
	.p2align	4, 0x90
.LBB46_3:                               #   in Loop: Header=BB46_4 Depth=1
	xorl	%r13d, %r13d
	movq	(%rbx), %rbx
.LBB46_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB46_5 Depth 2
                                        #       Child Loop BB46_7 Depth 3
                                        #       Child Loop BB46_12 Depth 3
                                        #         Child Loop BB46_13 Depth 4
	testq	%rbx, %rbx
	jne	.LBB46_5
	jmp	.LBB46_16
	.p2align	4, 0x90
.LBB46_12:                              # %.lr.ph.i
                                        #   Parent Loop BB46_4 Depth=1
                                        #     Parent Loop BB46_5 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB46_13 Depth 4
	movq	%rbx, %r15
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB46_13:                              #   Parent Loop BB46_4 Depth=1
                                        #     Parent Loop BB46_5 Depth=2
                                        #       Parent Loop BB46_12 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	callq	*%r14
	testl	%eax, %eax
	je	.LBB46_15
# BB#14:                                # %.critedge.thread.i
                                        #   in Loop: Header=BB46_13 Depth=4
	movq	(%rbx), %rax
	movq	%rax, (%r15)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	jne	.LBB46_13
	jmp	.LBB46_1
	.p2align	4, 0x90
.LBB46_15:                              # %.critedge.thread.outer.loopexit.i
                                        #   in Loop: Header=BB46_12 Depth=3
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	movq	%rbx, %rbp
	jne	.LBB46_12
	jmp	.LBB46_1
.LBB46_16:                              # %.loopexit
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end46:
	.size	list_NDifference, .Lfunc_end46-list_NDifference
	.cfi_endproc

	.globl	list_NMultisetDifference
	.p2align	4, 0x90
	.type	list_NMultisetDifference,@function
list_NMultisetDifference:               # @list_NMultisetDifference
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi279:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi280:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi281:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi282:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi283:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi284:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi285:
	.cfi_def_cfa_offset 64
.Lcfi286:
	.cfi_offset %rbx, -56
.Lcfi287:
	.cfi_offset %r12, -48
.Lcfi288:
	.cfi_offset %r13, -40
.Lcfi289:
	.cfi_offset %r14, -32
.Lcfi290:
	.cfi_offset %r15, -24
.Lcfi291:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r14
	testq	%r14, %r14
	jne	.LBB47_5
	jmp	.LBB47_10
	.p2align	4, 0x90
.LBB47_3:                               #   in Loop: Header=BB47_6 Depth=2
	xorl	%r14d, %r14d
	jmp	.LBB47_2
	.p2align	4, 0x90
.LBB47_4:                               #   in Loop: Header=BB47_5 Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	movq	%rax, %r14
	movq	(%r12), %r12
.LBB47_5:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB47_6 Depth 2
                                        #       Child Loop BB47_8 Depth 3
	testq	%r12, %r12
	jne	.LBB47_6
	jmp	.LBB47_10
.LBB47_1:                               #   in Loop: Header=BB47_6 Depth=2
	movq	(%rbx), %rax
	movq	%rax, (%rbp)
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	.p2align	4, 0x90
.LBB47_2:                               # %list_DeleteOneElement.exit
                                        #   in Loop: Header=BB47_6 Depth=2
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.LBB47_10
.LBB47_6:                               # %.lr.ph
                                        #   Parent Loop BB47_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB47_8 Depth 3
	testq	%r14, %r14
	je	.LBB47_3
# BB#7:                                 #   in Loop: Header=BB47_6 Depth=2
	movq	8(%r12), %r13
	movq	8(%r14), %rsi
	movq	%r13, %rdi
	callq	*%r15
	testl	%eax, %eax
	movq	%r14, %rbx
	jne	.LBB47_4
	.p2align	4, 0x90
.LBB47_8:                               # %.preheader.i
                                        #   Parent Loop BB47_5 Depth=1
                                        #     Parent Loop BB47_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbx, %rbp
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB47_2
# BB#9:                                 #   in Loop: Header=BB47_8 Depth=3
	movq	8(%rbx), %rsi
	movq	%r13, %rdi
	callq	*%r15
	testl	%eax, %eax
	je	.LBB47_8
	jmp	.LBB47_1
.LBB47_10:                              # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end47:
	.size	list_NMultisetDifference, .Lfunc_end47-list_NMultisetDifference
	.cfi_endproc

	.globl	list_PointerReplaceMember
	.p2align	4, 0x90
	.type	list_PointerReplaceMember,@function
list_PointerReplaceMember:              # @list_PointerReplaceMember
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	jne	.LBB48_2
	jmp	.LBB48_4
	.p2align	4, 0x90
.LBB48_5:                               #   in Loop: Header=BB48_2 Depth=1
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB48_4
.LBB48_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rsi, 8(%rdi)
	jne	.LBB48_5
# BB#3:
	movq	%rdx, 8(%rdi)
	movl	$1, %eax
.LBB48_4:                               # %.loopexit
	retq
.Lfunc_end48:
	.size	list_PointerReplaceMember, .Lfunc_end48-list_PointerReplaceMember
	.cfi_endproc

	.globl	list_DeleteAssocListWithValues
	.p2align	4, 0x90
	.type	list_DeleteAssocListWithValues,@function
list_DeleteAssocListWithValues:         # @list_DeleteAssocListWithValues
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi292:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi293:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi294:
	.cfi_def_cfa_offset 32
.Lcfi295:
	.cfi_offset %rbx, -32
.Lcfi296:
	.cfi_offset %r14, -24
.Lcfi297:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB49_4
# BB#1:                                 # %.lr.ph.preheader
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB49_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	callq	*%r14
	movq	8(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB49_2
	.p2align	4, 0x90
.LBB49_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB49_3
.LBB49_4:                               # %list_Delete.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end49:
	.size	list_DeleteAssocListWithValues, .Lfunc_end49-list_DeleteAssocListWithValues
	.cfi_endproc

	.globl	list_AssocListValue
	.p2align	4, 0x90
	.type	list_AssocListValue,@function
list_AssocListValue:                    # @list_AssocListValue
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	jne	.LBB50_2
	jmp	.LBB50_4
	.p2align	4, 0x90
.LBB50_5:                               #   in Loop: Header=BB50_2 Depth=1
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB50_4
.LBB50_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdi), %rcx
	cmpq	%rsi, 8(%rcx)
	jne	.LBB50_5
# BB#3:
	movq	(%rcx), %rax
.LBB50_4:                               # %.loopexit
	retq
.Lfunc_end50:
	.size	list_AssocListValue, .Lfunc_end50-list_AssocListValue
	.cfi_endproc

	.globl	list_AssocListPair
	.p2align	4, 0x90
	.type	list_AssocListPair,@function
list_AssocListPair:                     # @list_AssocListPair
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	jne	.LBB51_2
	jmp	.LBB51_4
	.p2align	4, 0x90
.LBB51_3:                               #   in Loop: Header=BB51_2 Depth=1
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB51_4
.LBB51_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdi), %rax
	cmpq	%rsi, 8(%rax)
	jne	.LBB51_3
	jmp	.LBB51_5
.LBB51_4:
	xorl	%eax, %eax
.LBB51_5:                               # %._crit_edge
	retq
.Lfunc_end51:
	.size	list_AssocListPair, .Lfunc_end51-list_AssocListPair
	.cfi_endproc

	.globl	list_MultisetDistribution
	.p2align	4, 0x90
	.type	list_MultisetDistribution,@function
list_MultisetDistribution:              # @list_MultisetDistribution
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi298:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi299:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi300:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi301:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi302:
	.cfi_def_cfa_offset 48
.Lcfi303:
	.cfi_offset %rbx, -48
.Lcfi304:
	.cfi_offset %r12, -40
.Lcfi305:
	.cfi_offset %r13, -32
.Lcfi306:
	.cfi_offset %r14, -24
.Lcfi307:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	testq	%r15, %r15
	je	.LBB52_10
# BB#1:                                 # %.lr.ph.preheader
	movabsq	$4294967296, %r12       # imm = 0x100000000
	.p2align	4, 0x90
.LBB52_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB52_4 Depth 2
	movq	8(%r15), %rbx
	testq	%r14, %r14
	je	.LBB52_6
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB52_2 Depth=1
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB52_4:                               # %.lr.ph.i
                                        #   Parent Loop BB52_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rax
	cmpq	%rbx, 8(%rax)
	je	.LBB52_7
# BB#5:                                 #   in Loop: Header=BB52_4 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB52_4
	jmp	.LBB52_6
	.p2align	4, 0x90
.LBB52_7:                               # %list_AssocListPair.exit
                                        #   in Loop: Header=BB52_2 Depth=1
	testq	%rax, %rax
	je	.LBB52_6
# BB#8:                                 #   in Loop: Header=BB52_2 Depth=1
	movq	(%rax), %rcx
	shlq	$32, %rcx
	addq	%r12, %rcx
	sarq	$32, %rcx
	movq	%rcx, (%rax)
	jmp	.LBB52_9
	.p2align	4, 0x90
.LBB52_6:                               # %list_AssocListPair.exit.thread
                                        #   in Loop: Header=BB52_2 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%rbx, 8(%r13)
	movq	$1, (%r13)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
.LBB52_9:                               #   in Loop: Header=BB52_2 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB52_2
.LBB52_10:                              # %._crit_edge
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end52:
	.size	list_MultisetDistribution, .Lfunc_end52-list_MultisetDistribution
	.cfi_endproc

	.globl	list_CompareElementDistribution
	.p2align	4, 0x90
	.type	list_CompareElementDistribution,@function
list_CompareElementDistribution:        # @list_CompareElementDistribution
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	xorl	%ecx, %ecx
	cmpl	(%rsi), %eax
	setg	%cl
	movl	$-1, %eax
	cmovgel	%ecx, %eax
	retq
.Lfunc_end53:
	.size	list_CompareElementDistribution, .Lfunc_end53-list_CompareElementDistribution
	.cfi_endproc

	.globl	list_CompareElementDistributionLEQ
	.p2align	4, 0x90
	.type	list_CompareElementDistributionLEQ,@function
list_CompareElementDistributionLEQ:     # @list_CompareElementDistributionLEQ
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	(%rsi), %ecx
	setle	%al
	retq
.Lfunc_end54:
	.size	list_CompareElementDistributionLEQ, .Lfunc_end54-list_CompareElementDistributionLEQ
	.cfi_endproc

	.globl	list_CompareMultisetsByElementDistribution
	.p2align	4, 0x90
	.type	list_CompareMultisetsByElementDistribution,@function
list_CompareMultisetsByElementDistribution: # @list_CompareMultisetsByElementDistribution
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi308:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi309:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi310:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi311:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi312:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi313:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi314:
	.cfi_def_cfa_offset 64
.Lcfi315:
	.cfi_offset %rbx, -56
.Lcfi316:
	.cfi_offset %r12, -48
.Lcfi317:
	.cfi_offset %r13, -40
.Lcfi318:
	.cfi_offset %r14, -32
.Lcfi319:
	.cfi_offset %r15, -24
.Lcfi320:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r15
	movabsq	$4294967296, %r13       # imm = 0x100000000
	xorl	%r14d, %r14d
	testq	%r15, %r15
	jne	.LBB55_2
	jmp	.LBB55_10
	.p2align	4, 0x90
.LBB55_7:                               # %list_AssocListPair.exit.i
                                        #   in Loop: Header=BB55_2 Depth=1
	testq	%rax, %rax
	je	.LBB55_6
# BB#8:                                 #   in Loop: Header=BB55_2 Depth=1
	movq	(%rax), %rcx
	shlq	$32, %rcx
	addq	%r13, %rcx
	sarq	$32, %rcx
	movq	%rcx, (%rax)
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB55_2
	jmp	.LBB55_10
	.p2align	4, 0x90
.LBB55_6:                               # %list_AssocListPair.exit.thread.i
                                        #   in Loop: Header=BB55_2 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	$1, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB55_10
.LBB55_2:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB55_4 Depth 2
	movq	8(%r15), %rbp
	testq	%r14, %r14
	je	.LBB55_6
# BB#3:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB55_2 Depth=1
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB55_4:                               # %.lr.ph.i.i
                                        #   Parent Loop BB55_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rax
	cmpq	%rbp, 8(%rax)
	je	.LBB55_7
# BB#5:                                 #   in Loop: Header=BB55_4 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB55_4
	jmp	.LBB55_6
.LBB55_10:                              # %list_MultisetDistribution.exit
	xorl	%r15d, %r15d
	testq	%r12, %r12
	jne	.LBB55_12
	jmp	.LBB55_20
	.p2align	4, 0x90
.LBB55_17:                              # %list_AssocListPair.exit.i26
                                        #   in Loop: Header=BB55_12 Depth=1
	testq	%rax, %rax
	je	.LBB55_16
# BB#18:                                #   in Loop: Header=BB55_12 Depth=1
	movq	(%rax), %rcx
	shlq	$32, %rcx
	addq	%r13, %rcx
	sarq	$32, %rcx
	movq	%rcx, (%rax)
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB55_12
	jmp	.LBB55_20
	.p2align	4, 0x90
.LBB55_16:                              # %list_AssocListPair.exit.thread.i29
                                        #   in Loop: Header=BB55_12 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	$1, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.LBB55_20
.LBB55_12:                              # %.lr.ph.i17
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB55_14 Depth 2
	movq	8(%r12), %rbp
	testq	%r15, %r15
	je	.LBB55_16
# BB#13:                                # %.lr.ph.i.i23.preheader
                                        #   in Loop: Header=BB55_12 Depth=1
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB55_14:                              # %.lr.ph.i.i23
                                        #   Parent Loop BB55_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rax
	cmpq	%rbp, 8(%rax)
	je	.LBB55_17
# BB#15:                                #   in Loop: Header=BB55_14 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB55_14
	jmp	.LBB55_16
.LBB55_20:                              # %list_MultisetDistribution.exit34
	movl	$list_CompareElementDistributionLEQ, %esi
	movq	%r14, %rdi
	callq	list_MergeSort
	movq	%rax, %rbx
	movl	$list_CompareElementDistributionLEQ, %esi
	movq	%r15, %rdi
	callq	list_MergeSort
	testq	%rax, %rax
	sete	%cl
	testq	%rbx, %rbx
	je	.LBB55_25
# BB#21:                                # %.lr.ph.i35.preheader
	movl	$-1, %r8d
	movq	%rax, %rdx
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB55_22:                              # %.lr.ph.i35
                                        # =>This Inner Loop Header: Depth=1
	testb	$1, %cl
	jne	.LBB55_26
# BB#23:                                #   in Loop: Header=BB55_22 Depth=1
	movq	8(%rdi), %rcx
	movq	8(%rdx), %rbp
	movl	(%rcx), %esi
	xorl	%ecx, %ecx
	cmpl	(%rbp), %esi
	setg	%cl
	cmovll	%r8d, %ecx
	testl	%ecx, %ecx
	jne	.LBB55_27
# BB#24:                                #   in Loop: Header=BB55_22 Depth=1
	movq	(%rdi), %rdi
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	sete	%cl
	testq	%rdi, %rdi
	jne	.LBB55_22
.LBB55_25:                              # %._crit_edge.i
	notb	%cl
	movzbl	%cl, %ecx
	andl	$1, %ecx
	negl	%ecx
	testq	%rbx, %rbx
	jne	.LBB55_28
	jmp	.LBB55_29
.LBB55_26:                              # %.thread25.i
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	sete	%cl
.LBB55_27:                              # %list_CompareDistributions.exit
	testq	%rbx, %rbx
	je	.LBB55_29
	.p2align	4, 0x90
.LBB55_28:                              # %.lr.ph.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	memory_ARRAY+128(%rip), %rdi
	movslq	32(%rdi), %rbp
	addq	%rbp, memory_FREEDBYTES(%rip)
	movq	(%rdi), %rdi
	movq	%rdi, (%rsi)
	movq	memory_ARRAY+128(%rip), %rdi
	movq	%rsi, (%rdi)
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rbx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rbx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	jne	.LBB55_28
.LBB55_29:                              # %list_DeleteDistribution.exit
	testq	%rax, %rax
	je	.LBB55_31
	.p2align	4, 0x90
.LBB55_30:                              # %.lr.ph.i.i.i41
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdx
	movq	8(%rax), %rsi
	movq	memory_ARRAY+128(%rip), %rdi
	movslq	32(%rdi), %rbp
	addq	%rbp, memory_FREEDBYTES(%rip)
	movq	(%rdi), %rdi
	movq	%rdi, (%rsi)
	movq	memory_ARRAY+128(%rip), %rdi
	movq	%rsi, (%rdi)
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rax)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rax, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rax
	jne	.LBB55_30
.LBB55_31:                              # %list_DeleteDistribution.exit42
	movl	%ecx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end55:
	.size	list_CompareMultisetsByElementDistribution, .Lfunc_end55-list_CompareMultisetsByElementDistribution
	.cfi_endproc

	.globl	list_Length
	.p2align	4, 0x90
	.type	list_Length,@function
list_Length:                            # @list_Length
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.LBB56_3
	.p2align	4, 0x90
.LBB56_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB56_1
.LBB56_3:                               # %._crit_edge
	retq
.Lfunc_end56:
	.size	list_Length, .Lfunc_end56-list_Length
	.cfi_endproc

	.globl	list_Bytes
	.p2align	4, 0x90
	.type	list_Bytes,@function
list_Bytes:                             # @list_Bytes
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.LBB57_3
	.p2align	4, 0x90
.LBB57_1:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdi
	addl	$16, %eax
	testq	%rdi, %rdi
	jne	.LBB57_1
.LBB57_3:                               # %list_Length.exit
	retq
.Lfunc_end57:
	.size	list_Bytes, .Lfunc_end57-list_Bytes
	.cfi_endproc

	.type	NumberFunction,@object  # @NumberFunction
	.local	NumberFunction
	.comm	NumberFunction,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
