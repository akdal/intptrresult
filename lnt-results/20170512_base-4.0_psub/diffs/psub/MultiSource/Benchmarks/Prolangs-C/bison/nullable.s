	.text
	.file	"nullable.bc"
	.globl	set_nullable
	.p2align	4, 0x90
	.type	set_nullable,@function
set_nullable:                           # @set_nullable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	nvars(%rip), %edi
	xorl	%eax, %eax
	callq	mallocate
	movslq	ntokens(%rip), %rcx
	subq	%rcx, %rax
	movq	%rax, nullable(%rip)
	movl	nvars(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %r14
	movl	nrules(%rip), %eax
	leal	2(%rax,%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %rbx
	movl	nvars(%rip), %edi
	shll	$3, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %r15
	movslq	ntokens(%rip), %rax
	shlq	$3, %rax
	subq	%rax, %r15
	movl	nvars(%rip), %edi
	addl	nitems(%rip), %edi
	shll	$4, %edi
	addl	$16, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %r12
	movq	ritem(%rip), %rsi
	movzwl	(%rsi), %r13d
	movq	%r14, %r10
	testw	%r13w, %r13w
	je	.LBB0_1
# BB#3:                                 # %.lr.ph117.lr.ph.preheader
	movq	%r12, %r11
.LBB0_6:                                # %.lr.ph117
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
                                        #       Child Loop BB0_9 Depth 3
                                        #     Child Loop BB0_18 Depth 2
	movq	rlhs(%rip), %r8
	movq	nullable(%rip), %r9
	movl	ntokens(%rip), %edi
	.p2align	4, 0x90
.LBB0_7:                                #   Parent Loop BB0_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_9 Depth 3
	movswl	%r13w, %eax
	addq	$2, %rsi
	testw	%ax, %ax
	js	.LBB0_14
# BB#8:                                 # %.lr.ph111.preheader
                                        #   in Loop: Header=BB0_7 Depth=2
	xorl	%ebp, %ebp
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph111
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%edi, %eax
	movb	$1, %dl
	jl	.LBB0_11
# BB#10:                                # %.lr.ph111
                                        #   in Loop: Header=BB0_9 Depth=3
	movl	%ebp, %edx
.LBB0_11:                               # %.lr.ph111
                                        #   in Loop: Header=BB0_9 Depth=3
	movswl	(%rcx), %eax
	addq	$2, %rcx
	testl	%eax, %eax
	movb	%dl, %bpl
	jg	.LBB0_9
# BB#12:                                # %._crit_edge112
                                        #   in Loop: Header=BB0_7 Depth=2
	testb	%dl, %dl
	jne	.LBB0_13
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_7 Depth=2
	negl	%eax
	cltq
	movzwl	(%r8,%rax,2), %eax
	movswq	%ax, %rax
	cmpb	$0, (%r9,%rax)
	je	.LBB0_4
# BB#15:                                #   in Loop: Header=BB0_7 Depth=2
	movq	%rsi, %rcx
.LBB0_13:                               # %.backedge89
                                        #   in Loop: Header=BB0_7 Depth=2
	movzwl	(%rcx), %r13d
	testw	%r13w, %r13w
	movq	%rcx, %rsi
	jne	.LBB0_7
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_16:                               #   in Loop: Header=BB0_6 Depth=1
	testw	%r13w, %r13w
	jle	.LBB0_5
# BB#17:                                # %.lr.ph129
                                        #   in Loop: Header=BB0_6 Depth=1
	cltq
	negq	%rax
	.p2align	4, 0x90
.LBB0_18:                               #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incw	(%rbx,%rax,2)
	movswq	%r13w, %rcx
	movq	(%r15,%rcx,8), %rdx
	movq	%rdx, (%r11)
	movw	%ax, 8(%r11)
	movq	%r11, (%r15,%rcx,8)
	addq	$16, %r11
	movzwl	(%rsi), %r13d
	addq	$2, %rsi
	testw	%r13w, %r13w
	jg	.LBB0_18
	jmp	.LBB0_5
	.p2align	4, 0x90
.LBB0_4:                                # %.outer86
                                        #   in Loop: Header=BB0_6 Depth=1
	movb	$1, (%r9,%rax)
	movw	%ax, (%r10)
	addq	$2, %r10
.LBB0_5:                                # %.loopexit84
                                        #   in Loop: Header=BB0_6 Depth=1
	movzwl	(%rsi), %r13d
	testw	%r13w, %r13w
	jne	.LBB0_6
.LBB0_1:                                # %.preheader
	cmpq	%r10, %r14
	jae	.LBB0_25
# BB#2:                                 # %.lr.ph106.preheader
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph106
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_22 Depth 2
	movswq	(%rcx), %rax
	addq	$2, %rcx
	movq	(%r15,%rax,8), %rax
	jmp	.LBB0_22
.LBB0_24:                               # %.outer
                                        #   in Loop: Header=BB0_22 Depth=2
	movb	$1, (%rsi,%rdx)
	movw	%dx, (%r10)
	addq	$2, %r10
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_21:                               #   in Loop: Header=BB0_22 Depth=2
	movswq	8(%rax), %rdx
	movq	(%rax), %rax
	decw	(%rbx,%rdx,2)
	jne	.LBB0_22
# BB#23:                                #   in Loop: Header=BB0_22 Depth=2
	movq	rlhs(%rip), %rsi
	movzwl	(%rsi,%rdx,2), %edx
	movq	nullable(%rip), %rsi
	movswq	%dx, %rdx
	cmpb	$0, (%rsi,%rdx)
	je	.LBB0_24
.LBB0_22:                               # %.backedge
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	jne	.LBB0_21
# BB#19:                                # %.loopexit
                                        #   in Loop: Header=BB0_20 Depth=1
	cmpq	%r10, %rcx
	jb	.LBB0_20
.LBB0_25:                               # %._crit_edge
	testq	%r14, %r14
	je	.LBB0_27
# BB#26:
	movq	%r14, %rdi
	callq	free
.LBB0_27:
	testq	%rbx, %rbx
	je	.LBB0_29
# BB#28:
	movq	%rbx, %rdi
	callq	free
.LBB0_29:
	movslq	ntokens(%rip), %rax
	shlq	$3, %rax
	addq	%rax, %r15
	je	.LBB0_31
# BB#30:
	movq	%r15, %rdi
	callq	free
.LBB0_31:
	testq	%r12, %r12
	je	.LBB0_32
# BB#33:
	movq	%r12, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB0_32:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	set_nullable, .Lfunc_end0-set_nullable
	.cfi_endproc

	.globl	free_nullable
	.p2align	4, 0x90
	.type	free_nullable,@function
free_nullable:                          # @free_nullable
	.cfi_startproc
# BB#0:
	movslq	ntokens(%rip), %rdi
	addq	nullable(%rip), %rdi
	je	.LBB1_1
# BB#2:
	jmp	free                    # TAILCALL
.LBB1_1:
	retq
.Lfunc_end1:
	.size	free_nullable, .Lfunc_end1-free_nullable
	.cfi_endproc

	.type	nullable,@object        # @nullable
	.comm	nullable,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
