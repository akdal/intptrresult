	.text
	.file	"z31.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16,12
	.text
	.globl	MemInit
	.p2align	4, 0x90
	.type	MemInit,@function
MemInit:                                # @MemInit
	.cfi_startproc
# BB#0:
	movb	$0, zz_lengths+11(%rip)
	movb	$0, zz_lengths+12(%rip)
	movb	$5, zz_lengths(%rip)
	movb	$12, zz_lengths+10(%rip)
	movb	$12, zz_lengths+9(%rip)
	movw	$3084, zz_lengths+6(%rip) # imm = 0xC0C
	movl	$202116108, zz_lengths+2(%rip) # imm = 0xC0C0C0C
	movabsq	$868082074056920076, %rax # imm = 0xC0C0C0C0C0C0C0C
	movq	%rax, zz_lengths+92(%rip)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12]
	movups	%xmm0, zz_lengths+77(%rip)
	movups	%xmm0, zz_lengths+61(%rip)
	movups	%xmm0, zz_lengths+45(%rip)
	movups	%xmm0, zz_lengths+29(%rip)
	movups	%xmm0, zz_lengths+13(%rip)
	movb	$21, zz_lengths+8(%rip)
	movb	$12, zz_lengths+149(%rip)
	movb	$12, zz_lengths+148(%rip)
	movq	%rax, zz_lengths+102(%rip)
	movw	$3084, zz_lengths+110(%rip) # imm = 0xC0C
	movl	$202116108, zz_lengths+114(%rip) # imm = 0xC0C0C0C
	movb	$12, zz_lengths+125(%rip)
	movw	$3084, zz_lengths+123(%rip) # imm = 0xC0C
	movl	$202116108, zz_lengths+119(%rip) # imm = 0xC0C0C0C
	movq	%rax, zz_lengths+132(%rip)
	movq	%rax, zz_lengths+127(%rip)
	movb	$16, zz_lengths+146(%rip)
	movl	$269488144, zz_lengths+142(%rip) # imm = 0x10101010
	movb	$9, zz_lengths+1(%rip)
	movb	$9, zz_lengths+101(%rip)
	movb	$9, zz_lengths+100(%rip)
	movb	$11, zz_lengths+141(%rip)
	movb	$11, zz_lengths+140(%rip)
	movb	$8, zz_lengths+147(%rip)
	retq
.Lfunc_end0:
	.size	MemInit, .Lfunc_end0-MemInit
	.cfi_endproc

	.globl	GetMemory
	.p2align	4, 0x90
	.type	GetMemory,@function
GetMemory:                              # @GetMemory
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	GetMemory.next_free(%rip), %rax
	movslq	%edi, %rbx
	leaq	(%rax,%rbx,8), %rcx
	cmpq	GetMemory.top_free(%rip), %rcx
	jbe	.LBB1_4
# BB#1:
	movl	$1020, %edi             # imm = 0x3FC
	movl	$8, %esi
	callq	calloc
	movq	%rax, GetMemory.next_free(%rip)
	testq	%rax, %rax
	jne	.LBB1_3
# BB#2:
	movl	$31, %edi
	movl	$1, %esi
	movl	$.L.str, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
	movq	GetMemory.next_free(%rip), %rax
.LBB1_3:
	leaq	8160(%rax), %rcx
	movq	%rcx, GetMemory.top_free(%rip)
.LBB1_4:
	leaq	(%rax,%rbx,8), %rcx
	movq	%rcx, GetMemory.next_free(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	GetMemory, .Lfunc_end1-GetMemory
	.cfi_endproc

	.type	zz_lengths,@object      # @zz_lengths
	.comm	zz_lengths,150,16
	.type	GetMemory.next_free,@object # @GetMemory.next_free
	.local	GetMemory.next_free
	.comm	GetMemory.next_free,8,8
	.type	GetMemory.top_free,@object # @GetMemory.top_free
	.local	GetMemory.top_free
	.comm	GetMemory.top_free,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"exiting now (run out of memory)"
	.size	.L.str, 32

	.type	zz_free,@object         # @zz_free
	.comm	zz_free,2120,16
	.type	zz_hold,@object         # @zz_hold
	.comm	zz_hold,8,8
	.type	zz_tmp,@object          # @zz_tmp
	.comm	zz_tmp,8,8
	.type	zz_res,@object          # @zz_res
	.comm	zz_res,8,8
	.type	zz_size,@object         # @zz_size
	.comm	zz_size,4,4
	.type	xx_link,@object         # @xx_link
	.comm	xx_link,8,8
	.type	xx_tmp,@object          # @xx_tmp
	.comm	xx_tmp,8,8
	.type	xx_res,@object          # @xx_res
	.comm	xx_res,8,8
	.type	xx_hold,@object         # @xx_hold
	.comm	xx_hold,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
