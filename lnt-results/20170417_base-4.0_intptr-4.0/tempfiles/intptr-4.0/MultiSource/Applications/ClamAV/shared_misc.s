	.text
	.file	"shared_misc.bc"
	.globl	freshdbdir
	.p2align	4, 0x90
	.type	freshdbdir,@function
freshdbdir:                             # @freshdbdir
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	callq	cl_retdbdir
	movq	%rax, %r14
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	getcfg
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_15
# BB#1:
	movl	$.L.str.1, %esi
	movq	%r15, %rdi
	callq	cfgopt
	movq	%rax, %r12
	cmpw	$0, 20(%r12)
	jne	.LBB0_3
# BB#2:
	movl	$.L.str.2, %esi
	movq	%r15, %rdi
	callq	cfgopt
	movq	%rax, %r12
	cmpw	$0, 20(%r12)
	je	.LBB0_17
.LBB0_3:
	movq	8(%r12), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_17
# BB#4:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	strlen
	leaq	30(%rbx,%rax), %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	8(%r12), %rdx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	leaq	8(%rsp), %rdx
	movl	$1, %edi
	movq	%rbx, %rsi
	callq	__xstat
	cmpl	$-1, %eax
	jne	.LBB0_6
# BB#5:
	movq	8(%r12), %rdx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
.LBB0_6:
	movq	%rbx, %rdi
	callq	cl_cvdhead
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_16
# BB#7:
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	sprintf
	leaq	8(%rsp), %rdx
	movl	$1, %edi
	movq	%rbx, %rsi
	callq	__xstat
	cmpl	$-1, %eax
	jne	.LBB0_9
# BB#8:
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	sprintf
.LBB0_9:
	movq	%rbx, %rdi
	callq	cl_cvdhead
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	je	.LBB0_13
# BB#10:
	movl	8(%r13), %eax
	cmpl	8(%rbp), %eax
	jbe	.LBB0_12
# BB#11:
	movq	8(%r12), %r14
.LBB0_12:
	movq	%rbp, %rdi
	callq	cl_cvdfree
	jmp	.LBB0_14
.LBB0_15:
	movq	%r14, %rdi
	callq	__strdup
	movq	%rax, %rbx
	jmp	.LBB0_18
.LBB0_16:
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB0_17
.LBB0_13:
	movq	8(%r12), %r14
.LBB0_14:
	movq	%r13, %rdi
	callq	cl_cvdfree
.LBB0_17:
	movq	%r14, %rdi
	callq	__strdup
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	freecfg
.LBB0_18:
	movq	%rbx, %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	freshdbdir, .Lfunc_end0-freshdbdir
	.cfi_endproc

	.globl	print_version
	.p2align	4, 0x90
	.type	print_version,@function
print_version:                          # @print_version
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
	subq	$160, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 192
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	callq	freshdbdir
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	strlen
	leaq	30(%rax), %rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB1_1
# BB#2:
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	sprintf
	leaq	16(%rsp), %rdx
	movl	$1, %edi
	movq	%r14, %rsi
	callq	__xstat
	cmpl	$-1, %eax
	jne	.LBB1_4
# BB#3:
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	sprintf
.LBB1_4:
	movq	%rbx, %rdi
	callq	free
	movq	%r14, %rdi
	callq	cl_cvdhead
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_6
# BB#5:
	movl	48(%rbx), %eax
	movq	%rax, 8(%rsp)
	movl	8(%rbx), %ebp
	leaq	8(%rsp), %rdi
	callq	ctime
	movq	%rax, %rcx
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	movq	%rcx, %rdx
	callq	printf
	movq	%rbx, %rdi
	callq	cl_cvdfree
	jmp	.LBB1_7
.LBB1_1:
	movq	%rbx, %rdi
	jmp	.LBB1_8
.LBB1_6:
	movl	$.Lstr, %edi
	callq	puts
.LBB1_7:
	movq	%r14, %rdi
.LBB1_8:
	callq	free
	addq	$160, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	print_version, .Lfunc_end1-print_version
	.cfi_endproc

	.globl	filecopy
	.p2align	4, 0x90
	.type	filecopy,@function
filecopy:                               # @filecopy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$8360, %rsp             # imm = 0x20A8
.Lcfi26:
	.cfi_def_cfa_offset 8416
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r12
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, %r13d
	movl	$-1, %r15d
	cmpl	$-1, %r13d
	je	.LBB2_7
# BB#1:
	movl	$577, %esi              # imm = 0x241
	movl	$420, %edx              # imm = 0x1A4
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB2_8
# BB#2:
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	leaq	160(%rsp), %rbp
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	$8192, %edx             # imm = 0x2000
	movl	%r13d, %edi
	movq	%rbp, %rsi
	callq	read
	testl	%eax, %eax
	jle	.LBB2_6
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=1
	movslq	%eax, %r14
	movl	%ebx, %edi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	write
	cmpq	%r14, %rax
	jge	.LBB2_3
# BB#5:
	movl	%r13d, %edi
	callq	close
	movl	%ebx, %edi
	callq	close
	jmp	.LBB2_7
.LBB2_8:
	movl	%r13d, %edi
	callq	close
	jmp	.LBB2_7
.LBB2_6:
	movl	%r13d, %edi
	callq	close
	movl	%ebx, %edi
	callq	close
	movl	%eax, %r15d
	leaq	16(%rsp), %rdx
	movl	$1, %edi
	movq	%r12, %rsi
	callq	__xstat
	movl	40(%rsp), %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	chmod
.LBB2_7:
	movl	%r15d, %eax
	addq	$8360, %rsp             # imm = 0x20A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	filecopy, .Lfunc_end2-filecopy
	.cfi_endproc

	.globl	dircopy
	.p2align	4, 0x90
	.type	dircopy,@function
dircopy:                                # @dircopy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$1176, %rsp             # imm = 0x498
.Lcfi39:
	.cfi_def_cfa_offset 1232
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rsp, %rdx
	movl	$1, %edi
	callq	__xstat
	cmpl	$-1, %eax
	je	.LBB3_1
.LBB3_2:
	movq	%r14, %rdi
	callq	opendir
	movq	%rax, %rbp
	movl	$-1, %ecx
	testq	%rbp, %rbp
	je	.LBB3_13
# BB#3:                                 # %.preheader
	movq	%rbp, %rdi
	callq	readdir
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB3_12
# BB#4:                                 # %.lr.ph.preheader
	leaq	656(%rsp), %r12
	leaq	144(%rsp), %r13
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%r15)
	je	.LBB3_11
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=1
	cmpb	$46, 19(%r15)
	jne	.LBB3_10
# BB#7:                                 #   in Loop: Header=BB3_5 Depth=1
	cmpb	$0, 20(%r15)
	je	.LBB3_11
# BB#8:                                 #   in Loop: Header=BB3_5 Depth=1
	cmpb	$46, 20(%r15)
	jne	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_5 Depth=1
	cmpb	$0, 21(%r15)
	je	.LBB3_11
	.p2align	4, 0x90
.LBB3_10:                               # %.thread
                                        #   in Loop: Header=BB3_5 Depth=1
	addq	$19, %r15
	movl	$512, %esi              # imm = 0x200
	movl	$.L.str.9, %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r15, %r8
	callq	snprintf
	movl	$512, %esi              # imm = 0x200
	movl	$.L.str.9, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbx, %rcx
	movq	%r15, %r8
	callq	snprintf
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	filecopy
	cmpl	$-1, %eax
	je	.LBB3_14
.LBB3_11:                               # %.backedge
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	%rbp, %rdi
	callq	readdir
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB3_5
.LBB3_12:                               # %._crit_edge
	movq	%rbp, %rdi
	callq	closedir
	xorl	%ecx, %ecx
	jmp	.LBB3_13
.LBB3_1:
	movl	$493, %esi              # imm = 0x1ED
	movq	%rbx, %rdi
	callq	mkdir
	movl	$-1, %ecx
	testl	%eax, %eax
	jne	.LBB3_13
	jmp	.LBB3_2
.LBB3_14:
	movq	%rbx, %rdi
	callq	cli_rmdirs
	movq	%rbp, %rdi
	callq	closedir
	movl	$-1, %ecx
.LBB3_13:
	movl	%ecx, %eax
	addq	$1176, %rsp             # imm = 0x498
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	dircopy, .Lfunc_end3-dircopy
	.cfi_endproc

	.globl	isnumb
	.p2align	4, 0x90
	.type	isnumb,@function
isnumb:                                 # @isnumb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 32
.Lcfi49:
	.cfi_offset %rbx, -32
.Lcfi50:
	.cfi_offset %r14, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movb	(%rbx), %r14b
	movl	$1, %ebp
	testb	%r14b, %r14b
	je	.LBB4_5
# BB#1:                                 # %.lr.ph
	callq	__ctype_b_loc
	movq	(%rax), %rax
	incq	%rbx
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movzbl	%r14b, %ecx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB4_4
# BB#2:                                 #   in Loop: Header=BB4_3 Depth=1
	movzbl	(%rbx), %r14d
	incq	%rbx
	testb	%r14b, %r14b
	jne	.LBB4_3
	jmp	.LBB4_5
.LBB4_4:
	xorl	%ebp, %ebp
.LBB4_5:                                # %._crit_edge
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end4:
	.size	isnumb, .Lfunc_end4-isnumb
	.cfi_endproc

	.globl	cvd_unpack
	.p2align	4, 0x90
	.type	cvd_unpack,@function
cvd_unpack:                             # @cvd_unpack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 48
.Lcfi57:
	.cfi_offset %rbx, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, %ebx
	movl	$-1, %ebp
	cmpl	$-1, %ebx
	je	.LBB5_4
# BB#1:
	xorl	%r15d, %r15d
	movl	$512, %esi              # imm = 0x200
	xorl	%edx, %edx
	movl	%ebx, %edi
	callq	lseek
	movl	%ebx, %edi
	cmpq	$-1, %rax
	je	.LBB5_2
# BB#3:
	movq	%r14, %rsi
	callq	cli_untgz
	cmpl	$-1, %eax
	cmovel	%eax, %r15d
	movl	%r15d, %ebp
	jmp	.LBB5_4
.LBB5_2:
	callq	close
.LBB5_4:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	cvd_unpack, .Lfunc_end5-cvd_unpack
	.cfi_endproc

	.globl	daemonize
	.p2align	4, 0x90
	.type	daemonize,@function
daemonize:                              # @daemonize
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 16
.Lcfi62:
	.cfi_offset %rbx, -16
	movl	$.L.str.10, %edi
	movl	$2, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB6_1
# BB#2:
	xorl	%esi, %esi
	movl	%ebx, %edi
	callq	dup2
	movl	$1, %esi
	movl	%ebx, %edi
	callq	dup2
	movl	$2, %esi
	movl	%ebx, %edi
	callq	dup2
	cmpl	$3, %ebx
	jl	.LBB6_5
# BB#3:
	movl	%ebx, %edi
	jmp	.LBB6_4
.LBB6_1:                                # %.preheader.preheader
	xorl	%edi, %edi
	callq	close
	movl	$1, %edi
	callq	close
	movl	$2, %edi
.LBB6_4:                                # %.loopexit
	callq	close
.LBB6_5:                                # %.loopexit
	callq	fork
	testl	%eax, %eax
	jne	.LBB6_6
# BB#7:
	popq	%rbx
	jmp	setsid                  # TAILCALL
.LBB6_6:
	xorl	%edi, %edi
	callq	exit
.Lfunc_end6:
	.size	daemonize, .Lfunc_end6-daemonize
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"/usr/local/etc/clamd.conf"
	.size	.L.str, 26

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"DatabaseDirectory"
	.size	.L.str.1, 18

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"DataDirectory"
	.size	.L.str.2, 14

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s/daily.cvd"
	.size	.L.str.3, 13

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%s/daily.inc/daily.info"
	.size	.L.str.4, 24

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"ClamAV devel-20071218/%d/%s"
	.size	.L.str.5, 28

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"."
	.size	.L.str.7, 2

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	".."
	.size	.L.str.8, 3

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%s/%s"
	.size	.L.str.9, 6

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"/dev/null"
	.size	.L.str.10, 10

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"ClamAV devel-20071218"
	.size	.Lstr, 22


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
