	.text
	.file	"gsmatrix.bc"
	.globl	gs_make_identity
	.p2align	4, 0x90
	.type	gs_make_identity,@function
gs_make_identity:                       # @gs_make_identity
	.cfi_startproc
# BB#0:
	movups	gs_identity_matrix+80(%rip), %xmm0
	movups	%xmm0, 80(%rdi)
	movups	gs_identity_matrix+64(%rip), %xmm0
	movups	%xmm0, 64(%rdi)
	movups	gs_identity_matrix+48(%rip), %xmm0
	movups	%xmm0, 48(%rdi)
	movups	gs_identity_matrix+32(%rip), %xmm0
	movups	%xmm0, 32(%rdi)
	movups	gs_identity_matrix+16(%rip), %xmm0
	movups	%xmm0, 16(%rdi)
	movups	gs_identity_matrix(%rip), %xmm0
	movups	%xmm0, (%rdi)
	retq
.Lfunc_end0:
	.size	gs_make_identity, .Lfunc_end0-gs_make_identity
	.cfi_endproc

	.globl	gs_make_translation
	.p2align	4, 0x90
	.type	gs_make_translation,@function
gs_make_translation:                    # @gs_make_translation
	.cfi_startproc
# BB#0:
	movups	gs_identity_matrix+80(%rip), %xmm2
	movups	%xmm2, 80(%rdi)
	movups	gs_identity_matrix+64(%rip), %xmm2
	movups	%xmm2, 64(%rdi)
	movups	gs_identity_matrix+48(%rip), %xmm2
	movups	%xmm2, 48(%rdi)
	movups	gs_identity_matrix+32(%rip), %xmm2
	movups	%xmm2, 32(%rdi)
	movups	gs_identity_matrix+16(%rip), %xmm2
	movups	%xmm2, 16(%rdi)
	movups	gs_identity_matrix(%rip), %xmm2
	movups	%xmm2, (%rdi)
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 64(%rdi)
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 80(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	gs_make_translation, .Lfunc_end1-gs_make_translation
	.cfi_endproc

	.globl	gs_make_scaling
	.p2align	4, 0x90
	.type	gs_make_scaling,@function
gs_make_scaling:                        # @gs_make_scaling
	.cfi_startproc
# BB#0:
	movups	gs_identity_matrix+80(%rip), %xmm2
	movups	%xmm2, 80(%rdi)
	movups	gs_identity_matrix+64(%rip), %xmm2
	movups	%xmm2, 64(%rdi)
	movups	gs_identity_matrix+48(%rip), %xmm2
	movups	%xmm2, 48(%rdi)
	movups	gs_identity_matrix+32(%rip), %xmm2
	movups	%xmm2, 32(%rdi)
	movups	gs_identity_matrix+16(%rip), %xmm2
	movups	%xmm2, 16(%rdi)
	movups	gs_identity_matrix(%rip), %xmm2
	movups	%xmm2, (%rdi)
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rdi)
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 48(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	gs_make_scaling, .Lfunc_end2-gs_make_scaling
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4580687790476533049     # double 0.017453292519943295
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	gs_make_rotation
	.p2align	4, 0x90
	.type	gs_make_rotation,@function
gs_make_rotation:                       # @gs_make_rotation
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	mulsd	.LCPI3_0(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movups	gs_identity_matrix+80(%rip), %xmm1
	movups	%xmm1, 80(%rbx)
	movups	gs_identity_matrix+64(%rip), %xmm1
	movups	%xmm1, 64(%rbx)
	movups	gs_identity_matrix+48(%rip), %xmm1
	movups	%xmm1, 48(%rbx)
	movups	gs_identity_matrix+32(%rip), %xmm1
	movups	%xmm1, 32(%rbx)
	movups	gs_identity_matrix+16(%rip), %xmm1
	movups	%xmm1, 16(%rbx)
	movups	gs_identity_matrix(%rip), %xmm1
	movups	%xmm1, (%rbx)
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	cos
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 48(%rbx)
	movss	%xmm0, (%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 16(%rbx)
	xorps	.LCPI3_1(%rip), %xmm0
	movss	%xmm0, 32(%rbx)
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end3:
	.size	gs_make_rotation, .Lfunc_end3-gs_make_rotation
	.cfi_endproc

	.globl	gs_matrix_multiply
	.p2align	4, 0x90
	.type	gs_matrix_multiply,@function
gs_matrix_multiply:                     # @gs_matrix_multiply
	.cfi_startproc
# BB#0:
	movss	(%rdi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	48(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	64(%rdi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	80(%rdi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm12          # xmm12 = mem[0],zero,zero,zero
	movss	48(%rsi), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movl	16(%rsi), %r8d
	movss	32(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, -8(%rsp)
	movq	16(%rdi), %rax
	movq	32(%rdi), %rcx
	movq	%rcx, %rdi
	orq	%rax, %rdi
	addq	%rdi, %rdi
	je	.LBB4_1
# BB#7:
	movd	%eax, %xmm0
	movd	%ecx, %xmm1
	movaps	%xmm4, %xmm5
	mulss	%xmm12, %xmm5
	movaps	%xmm3, %xmm6
	mulss	%xmm0, %xmm6
	addss	%xmm5, %xmm6
	movss	%xmm6, (%rdx)
	movd	%r8d, %xmm5
	mulss	%xmm5, %xmm4
	mulss	%xmm9, %xmm0
	addss	%xmm4, %xmm0
	movss	%xmm0, 16(%rdx)
	movaps	%xmm5, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm2, %xmm4
	mulss	%xmm9, %xmm4
	addss	%xmm0, %xmm4
	movss	%xmm4, 48(%rdx)
	mulss	%xmm12, %xmm1
	mulss	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	movss	%xmm2, 32(%rdx)
	mulss	%xmm11, %xmm12
	mulss	%xmm7, %xmm3
	addss	%xmm12, %xmm3
	addss	64(%rsi), %xmm3
	movss	%xmm3, 64(%rdx)
	mulss	%xmm5, %xmm11
	mulss	%xmm9, %xmm7
	addss	%xmm11, %xmm7
	addss	80(%rsi), %xmm7
	movss	%xmm7, 80(%rdx)
	xorl	%eax, %eax
	retq
.LBB4_1:
	movabsq	$9223372036854775807, %rcx # imm = 0x7FFFFFFFFFFFFFFF
	movaps	%xmm11, %xmm6
	mulss	%xmm12, %xmm6
	addss	64(%rsi), %xmm6
	movss	%xmm6, 64(%rdx)
	movaps	%xmm7, %xmm0
	mulss	%xmm9, %xmm0
	addss	80(%rsi), %xmm0
	xorps	%xmm8, %xmm8
	testl	%r8d, %r8d
	je	.LBB4_2
# BB#3:
	movd	%r8d, %xmm10
	movaps	%xmm4, %xmm1
	mulss	%xmm10, %xmm1
	mulss	%xmm10, %xmm11
	addss	%xmm11, %xmm0
	jmp	.LBB4_4
.LBB4_2:
	xorps	%xmm1, %xmm1
.LBB4_4:
	movss	%xmm0, 80(%rdx)
	movss	%xmm1, 16(%rdx)
	mulss	%xmm12, %xmm4
	movss	%xmm4, (%rdx)
	movq	-8(%rsp), %rax
	testq	%rcx, %rax
	je	.LBB4_6
# BB#5:
	movd	%eax, %xmm0
	movaps	%xmm2, %xmm8
	mulss	%xmm0, %xmm8
	mulss	%xmm0, %xmm7
	addss	%xmm6, %xmm7
	movss	%xmm7, 64(%rdx)
.LBB4_6:
	movss	%xmm8, 32(%rdx)
	mulss	%xmm9, %xmm2
	movss	%xmm2, 48(%rdx)
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	gs_matrix_multiply, .Lfunc_end4-gs_matrix_multiply
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_1:
	.long	1065353216              # float 1
	.text
	.globl	gs_matrix_invert
	.p2align	4, 0x90
	.type	gs_matrix_invert,@function
gs_matrix_invert:                       # @gs_matrix_invert
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rcx
	movq	32(%rdi), %rax
	movq	%rax, %rdx
	orq	%rcx, %rdx
	addq	%rdx, %rdx
	je	.LBB5_1
# BB#4:
	movd	%ecx, %xmm1
	movd	%eax, %xmm2
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	48(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm3, %xmm4
	movdqa	%xmm1, %xmm5
	mulss	%xmm2, %xmm5
	subss	%xmm5, %xmm4
	movl	$-23, %eax
	xorps	%xmm5, %xmm5
	ucomiss	%xmm5, %xmm4
	jne	.LBB5_5
	jnp	.LBB5_7
.LBB5_5:                                # %.critedge
	movss	64(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	divss	%xmm4, %xmm3
	movss	%xmm3, (%rsi)
	movdqa	.LCPI5_0(%rip), %xmm6   # xmm6 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm6, %xmm1
	divss	%xmm4, %xmm1
	movss	%xmm1, 16(%rsi)
	pxor	%xmm6, %xmm2
	divss	%xmm4, %xmm2
	movss	%xmm2, 32(%rsi)
	divss	%xmm4, %xmm0
	movss	%xmm0, 48(%rsi)
	mulss	%xmm5, %xmm3
	movss	80(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	addss	%xmm3, %xmm2
	xorps	%xmm6, %xmm2
	movss	%xmm2, 64(%rsi)
	mulss	%xmm5, %xmm1
	mulss	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm6, %xmm0
	movss	%xmm0, 80(%rsi)
	jmp	.LBB5_6
.LBB5_1:
	movabsq	$9223372036854775807, %r8 # imm = 0x7FFFFFFFFFFFFFFF
	movq	(%rdi), %rcx
	testq	%r8, %rcx
	movl	$-23, %eax
	je	.LBB5_7
# BB#2:
	movq	48(%rdi), %rdx
	testq	%r8, %rdx
	je	.LBB5_7
# BB#3:
	movd	%ecx, %xmm0
	movd	%edx, %xmm1
	movss	.LCPI5_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	divss	%xmm0, %xmm3
	movss	%xmm3, (%rsi)
	mulss	64(%rdi), %xmm3
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm3
	movss	%xmm3, 64(%rsi)
	movl	$0, 16(%rsi)
	movl	$0, 32(%rsi)
	divss	%xmm1, %xmm2
	movss	%xmm2, 48(%rsi)
	mulss	80(%rdi), %xmm2
	xorps	%xmm0, %xmm2
	movss	%xmm2, 80(%rsi)
.LBB5_6:
	xorl	%eax, %eax
.LBB5_7:
	retq
.Lfunc_end5:
	.size	gs_matrix_invert, .Lfunc_end5-gs_matrix_invert
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	-4578331233687175168    # double -360
.LCPI6_1:
	.quad	4645040803167600640     # double 360
.LCPI6_2:
	.quad	4580687790476533049     # double 0.017453292519943295
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_3:
	.long	1065353216              # float 1
.LCPI6_4:
	.long	3212836864              # float -1
	.text
	.globl	gs_matrix_rotate
	.p2align	4, 0x90
	.type	gs_matrix_rotate,@function
gs_matrix_rotate:                       # @gs_matrix_rotate
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 48
.Lcfi6:
	.cfi_offset %rbx, -24
.Lcfi7:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	ucomisd	.LCPI6_0(%rip), %xmm0
	jb	.LBB6_8
# BB#1:
	movsd	.LCPI6_1(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jb	.LBB6_8
# BB#2:
	cvttsd2si	%xmm0, %eax
	cltq
	imulq	$-1240768329, %rax, %rcx # imm = 0xB60B60B7
	shrq	$32, %rcx
	addl	%ecx, %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	sarl	$6, %eax
	addl	%ecx, %eax
	imull	$90, %eax, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB6_8
	jp	.LBB6_8
# BB#3:
	xorl	%ecx, %ecx
	andl	$3, %eax
	je	.LBB6_4
# BB#5:                                 # %.lr.ph
	xorps	%xmm0, %xmm0
	cmpl	$1, %eax
	jne	.LBB6_10
# BB#6:
	movl	$1, %ecx
	cvtsi2ssl	%ecx, %xmm7
	jmp	.LBB6_9
.LBB6_8:
	mulsd	.LCPI6_2(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	callq	sin
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	movss	12(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	cvtsd2ss	%xmm0, %xmm0
.LBB6_9:
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	mulss	%xmm1, %xmm3
	movss	32(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm5
	mulss	%xmm4, %xmm5
	addss	%xmm3, %xmm5
	movss	%xmm5, (%r14)
	movaps	%xmm0, %xmm3
	mulss	%xmm2, %xmm3
	movss	48(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm6
	mulss	%xmm5, %xmm6
	addss	%xmm3, %xmm6
	movss	%xmm6, 16(%r14)
	mulss	%xmm0, %xmm4
	mulss	%xmm7, %xmm1
	subss	%xmm1, %xmm4
	movss	%xmm4, 32(%r14)
	mulss	%xmm5, %xmm0
	mulss	%xmm2, %xmm7
	subss	%xmm7, %xmm0
	movss	%xmm0, 48(%r14)
	movl	64(%rbx), %eax
	movl	%eax, 64(%r14)
	movl	80(%rbx), %eax
	movl	%eax, 80(%r14)
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB6_4:
	movss	.LCPI6_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	cvtsi2ssl	%ecx, %xmm7
	jmp	.LBB6_9
.LBB6_10:                               # %.lr.ph.1
	cmpl	$2, %eax
	jne	.LBB6_12
# BB#11:
	movss	.LCPI6_4(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	cvtsi2ssl	%ecx, %xmm7
	jmp	.LBB6_9
.LBB6_12:                               # %.lr.ph.2
	movl	$-1, %ecx
	cvtsi2ssl	%ecx, %xmm7
	jmp	.LBB6_9
.Lfunc_end6:
	.size	gs_matrix_rotate, .Lfunc_end6-gs_matrix_rotate
	.cfi_endproc

	.globl	gs_point_transform
	.p2align	4, 0x90
	.type	gs_point_transform,@function
gs_point_transform:                     # @gs_point_transform
	.cfi_startproc
# BB#0:
	movabsq	$9223372036854775807, %rax # imm = 0x7FFFFFFFFFFFFFFF
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	64(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm0, %xmm2
	cvtss2sd	%xmm3, %xmm3
	addsd	%xmm2, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, (%rsi)
	movss	48(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	80(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm1, %xmm2
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm2, %xmm4
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm4, %xmm2
	movss	%xmm2, 4(%rsi)
	movq	32(%rdi), %rcx
	movq	%rcx, %rdx
	addq	%rdx, %rdx
	je	.LBB7_2
# BB#1:
	movd	%ecx, %xmm4
	cvtss2sd	%xmm4, %xmm4
	mulsd	%xmm1, %xmm4
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm3, %xmm1
	addsd	%xmm4, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rsi)
.LBB7_2:
	movq	16(%rdi), %rcx
	testq	%rax, %rcx
	je	.LBB7_4
# BB#3:
	movd	%ecx, %xmm1
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 4(%rsi)
.LBB7_4:
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	gs_point_transform, .Lfunc_end7-gs_point_transform
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	gs_point_transform_inverse
	.p2align	4, 0x90
	.type	gs_point_transform_inverse,@function
gs_point_transform_inverse:             # @gs_point_transform_inverse
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rcx
	movq	32(%rdi), %rax
	movq	%rax, %rdx
	orq	%rcx, %rdx
	addq	%rdx, %rdx
	je	.LBB8_1
# BB#2:
	movd	%ecx, %xmm10
	movd	%eax, %xmm2
	movss	(%rdi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	48(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm6
	mulss	%xmm5, %xmm6
	movdqa	%xmm10, %xmm7
	mulss	%xmm2, %xmm7
	subss	%xmm7, %xmm6
	movl	$-23, %eax
	xorps	%xmm7, %xmm7
	ucomiss	%xmm7, %xmm6
	jne	.LBB8_3
	jnp	.LBB8_7
.LBB8_3:
	movss	64(%rdi), %xmm9         # xmm9 = mem[0],zero,zero,zero
	divss	%xmm6, %xmm5
	movdqa	.LCPI8_0(%rip), %xmm8   # xmm8 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm8, %xmm10
	divss	%xmm6, %xmm10
	pxor	%xmm8, %xmm2
	divss	%xmm6, %xmm2
	divss	%xmm6, %xmm4
	movaps	%xmm9, %xmm6
	mulss	%xmm5, %xmm6
	movss	80(%rdi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm6, %xmm3
	mulss	%xmm10, %xmm9
	mulss	%xmm4, %xmm7
	addss	%xmm9, %xmm7
	cvtss2sd	%xmm5, %xmm5
	mulsd	%xmm0, %xmm5
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm3, %xmm5
	cvtsd2ss	%xmm5, %xmm5
	movss	%xmm5, (%rsi)
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm4, %xmm3
	mulsd	%xmm1, %xmm3
	xorps	%xmm4, %xmm4
	cvtss2sd	%xmm7, %xmm4
	subsd	%xmm4, %xmm3
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm3, %xmm4
	movss	%xmm4, 4(%rsi)
	movd	%xmm2, %eax
	testl	%eax, %eax
	je	.LBB8_5
# BB#4:
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm5, %xmm1
	addsd	%xmm2, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rsi)
.LBB8_5:
	movd	%xmm10, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.LBB8_7
# BB#6:
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm10, %xmm1
	mulsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm4, %xmm0
	addsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 4(%rsi)
.LBB8_7:                                # %gs_point_transform.exit
	retq
.LBB8_1:
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	64(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm3, %xmm0
	cvtss2sd	%xmm2, %xmm2
	divsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rsi)
	movss	48(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	80(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm2, %xmm1
	cvtss2sd	%xmm0, %xmm0
	divsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	gs_point_transform_inverse, .Lfunc_end8-gs_point_transform_inverse
	.cfi_endproc

	.globl	gs_distance_transform
	.p2align	4, 0x90
	.type	gs_distance_transform,@function
gs_distance_transform:                  # @gs_distance_transform
	.cfi_startproc
# BB#0:
	movabsq	$9223372036854775807, %rax # imm = 0x7FFFFFFFFFFFFFFF
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm0, %xmm2
	cvtsd2ss	%xmm2, %xmm3
	movss	%xmm3, (%rsi)
	movss	48(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm1, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	movss	%xmm2, 4(%rsi)
	movq	32(%rdi), %rcx
	movq	%rcx, %rdx
	addq	%rdx, %rdx
	je	.LBB9_2
# BB#1:
	movd	%ecx, %xmm4
	cvtss2sd	%xmm4, %xmm4
	mulsd	%xmm1, %xmm4
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm3, %xmm1
	addsd	%xmm4, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rsi)
.LBB9_2:
	movq	16(%rdi), %rcx
	testq	%rax, %rcx
	je	.LBB9_4
# BB#3:
	movd	%ecx, %xmm1
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 4(%rsi)
.LBB9_4:
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	gs_distance_transform, .Lfunc_end9-gs_distance_transform
	.cfi_endproc

	.globl	gs_distance_transform_inverse
	.p2align	4, 0x90
	.type	gs_distance_transform_inverse,@function
gs_distance_transform_inverse:          # @gs_distance_transform_inverse
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rcx
	movq	32(%rdi), %rax
	movq	%rax, %rdx
	orq	%rcx, %rdx
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	addq	%rdx, %rdx
	je	.LBB10_1
# BB#2:
	movd	%ecx, %xmm3
	movd	%eax, %xmm4
	movss	48(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm6
	mulss	%xmm5, %xmm6
	movdqa	%xmm3, %xmm7
	mulss	%xmm4, %xmm7
	subss	%xmm7, %xmm6
	movl	$-23, %eax
	xorps	%xmm7, %xmm7
	ucomiss	%xmm7, %xmm6
	jne	.LBB10_3
	jnp	.LBB10_5
.LBB10_3:                               # %.critedge
	cvtss2sd	%xmm6, %xmm6
	cvtss2sd	%xmm5, %xmm5
	mulsd	%xmm0, %xmm5
	cvtss2sd	%xmm4, %xmm4
	mulsd	%xmm1, %xmm4
	subsd	%xmm4, %xmm5
	divsd	%xmm6, %xmm5
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm5, %xmm4
	movss	%xmm4, (%rsi)
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm3, %xmm1
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm2
	divsd	%xmm6, %xmm2
	movapd	%xmm2, %xmm1
	jmp	.LBB10_4
.LBB10_1:
	cvtss2sd	%xmm2, %xmm2
	divsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rsi)
	movss	48(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	divsd	%xmm0, %xmm1
.LBB10_4:                               # %.sink.split
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsi)
	xorl	%eax, %eax
.LBB10_5:
	retq
.Lfunc_end10:
	.size	gs_distance_transform_inverse, .Lfunc_end10-gs_distance_transform_inverse
	.cfi_endproc

	.globl	gs_bbox_transform_inverse
	.p2align	4, 0x90
	.type	gs_bbox_transform_inverse,@function
gs_bbox_transform_inverse:              # @gs_bbox_transform_inverse
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 48
.Lcfi12:
	.cfi_offset %rbx, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	callq	gs_point_transform_inverse
	testl	%eax, %eax
	js	.LBB11_18
# BB#1:
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	(%r15), %xmm0
	cvtss2sd	%xmm0, %xmm2
	movq	16(%rbx), %rax
	movq	32(%rbx), %rcx
	movd	%ecx, %xmm5
	movq	%rcx, %rdx
	orq	%rax, %rdx
	movabsq	$9223372036854775807, %rcx # imm = 0x7FFFFFFFFFFFFFFF
	andq	%rdx, %rcx
	movss	(%rbx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movd	%eax, %xmm0
	punpckldq	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	movss	48(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	je	.LBB11_2
# BB#3:
	mulss	%xmm4, %xmm0
	movdqa	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm0
	movl	$-23, %eax
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB11_4
	jnp	.LBB11_18
.LBB11_4:                               # %.critedge.i
	cvtss2sd	%xmm0, %xmm8
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm4, %xmm0
	cvtss2sd	%xmm3, %xmm6
	movapd	%xmm2, %xmm7
	unpcklpd	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0]
	movq	%xmm0, %xmm0            # xmm0 = xmm0[0],zero
	mulpd	%xmm7, %xmm0
	cvtps2pd	%xmm5, %xmm6
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
	mulpd	%xmm6, %xmm2
	subpd	%xmm2, %xmm0
	movlhps	%xmm8, %xmm8            # xmm8 = xmm8[0,0]
	jmp	.LBB11_5
.LBB11_2:
	unpcklps	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	cvtps2pd	%xmm0, %xmm8
	movq	%xmm2, %xmm0            # xmm0 = xmm2[0],zero
.LBB11_5:
	movss	12(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	4(%r15), %xmm2
	cvtss2sd	%xmm2, %xmm2
	testq	%rcx, %rcx
	je	.LBB11_6
# BB#7:
	movaps	%xmm3, %xmm7
	mulss	%xmm4, %xmm7
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm1, %xmm6
	mulss	%xmm5, %xmm6
	subss	%xmm6, %xmm7
	movl	$-23, %eax
	xorps	%xmm6, %xmm6
	ucomiss	%xmm6, %xmm7
	jne	.LBB11_8
	jnp	.LBB11_18
.LBB11_8:                               # %.critedge.i47
	cvtss2sd	%xmm7, %xmm7
	cvtss2sd	%xmm5, %xmm5
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	cvtps2pd	%xmm4, %xmm3
	movdqa	%xmm2, %xmm4
	pslldq	$8, %xmm4               # xmm4 = zero,zero,zero,zero,zero,zero,zero,zero,xmm4[0,1,2,3,4,5,6,7]
	mulpd	%xmm3, %xmm4
	cvtss2sd	%xmm1, %xmm1
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movq	%xmm5, %xmm1            # xmm1 = xmm5[0],zero
	mulpd	%xmm2, %xmm1
	subpd	%xmm1, %xmm4
	movlhps	%xmm7, %xmm7            # xmm7 = xmm7[0,0]
	movapd	%xmm4, %xmm2
	jmp	.LBB11_9
.LBB11_6:
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	cvtps2pd	%xmm3, %xmm7
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
.LBB11_9:
	divpd	%xmm8, %xmm0
	xorps	%xmm6, %xmm6
	cvtsd2ss	%xmm0, %xmm6
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	divpd	%xmm7, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	xorpd	%xmm4, %xmm4
	ucomiss	%xmm6, %xmm4
	movss	8(%rsp), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm3
	addss	%xmm7, %xmm3
	movaps	%xmm3, %xmm8
	ja	.LBB11_11
# BB#10:
	movaps	%xmm7, %xmm8
.LBB11_11:
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	cvtsd2ss	%xmm0, %xmm0
	cmpltss	%xmm4, %xmm6
	movaps	%xmm6, %xmm5
	andnps	%xmm3, %xmm5
	andps	%xmm7, %xmm6
	orps	%xmm5, %xmm6
	ucomiss	%xmm1, %xmm4
	movaps	%xmm6, %xmm7
	addss	%xmm1, %xmm7
	jbe	.LBB11_13
# BB#12:
	addss	%xmm1, %xmm8
.LBB11_13:
	cvtsd2ss	%xmm2, %xmm2
	cmpltss	%xmm4, %xmm1
	movaps	%xmm1, %xmm3
	andps	%xmm6, %xmm3
	andnps	%xmm7, %xmm1
	orps	%xmm3, %xmm1
	ucomiss	%xmm0, %xmm4
	movaps	%xmm0, %xmm3
	addss	%xmm9, %xmm3
	movaps	%xmm3, %xmm6
	ja	.LBB11_15
# BB#14:
	movaps	%xmm9, %xmm6
.LBB11_15:
	cmpltss	%xmm4, %xmm0
	movaps	%xmm0, %xmm5
	andnps	%xmm3, %xmm5
	andps	%xmm9, %xmm0
	orps	%xmm5, %xmm0
	ucomiss	%xmm2, %xmm4
	movaps	%xmm0, %xmm5
	addss	%xmm2, %xmm5
	jbe	.LBB11_17
# BB#16:
	addss	%xmm2, %xmm6
.LBB11_17:
	cmpltss	%xmm4, %xmm2
	movaps	%xmm2, %xmm3
	andps	%xmm0, %xmm3
	andnps	%xmm5, %xmm2
	orps	%xmm3, %xmm2
	movss	%xmm8, (%r14)
	movss	%xmm6, 4(%r14)
	movss	%xmm1, 8(%r14)
	movss	%xmm2, 12(%r14)
	xorl	%eax, %eax
.LBB11_18:                              # %gs_distance_transform_inverse.exit.thread
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	gs_bbox_transform_inverse, .Lfunc_end11-gs_bbox_transform_inverse
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI12_0:
	.quad	4661225614328463360     # double 4096
	.text
	.globl	gs_point_transform2fixed
	.p2align	4, 0x90
	.type	gs_point_transform2fixed,@function
gs_point_transform2fixed:               # @gs_point_transform2fixed
	.cfi_startproc
# BB#0:
	movabsq	$9223372036854775807, %r8 # imm = 0x7FFFFFFFFFFFFFFF
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	.LCPI12_0(%rip), %xmm3  # xmm3 = mem[0],zero
	mulsd	%xmm3, %xmm2
	cvttsd2si	%xmm2, %rdx
	addq	96(%rdi), %rdx
	movq	%rdx, (%rsi)
	movss	48(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm3, %xmm2
	cvttsd2si	%xmm2, %r9
	addq	104(%rdi), %r9
	movq	%r9, 8(%rsi)
	movq	32(%rdi), %rcx
	movq	%rcx, %rax
	addq	%rax, %rax
	je	.LBB12_2
# BB#1:
	movd	%ecx, %xmm2
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	.LCPI12_0(%rip), %xmm2
	cvttsd2si	%xmm2, %rax
	addq	%rdx, %rax
	movq	%rax, (%rsi)
.LBB12_2:
	movq	16(%rdi), %rax
	testq	%r8, %rax
	je	.LBB12_4
# BB#3:
	movd	%eax, %xmm1
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	.LCPI12_0(%rip), %xmm1
	cvttsd2si	%xmm1, %rax
	addq	%r9, %rax
	movq	%rax, 8(%rsi)
.LBB12_4:
	xorl	%eax, %eax
	retq
.Lfunc_end12:
	.size	gs_point_transform2fixed, .Lfunc_end12-gs_point_transform2fixed
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI13_0:
	.quad	4661225614328463360     # double 4096
	.text
	.globl	gs_distance_transform2fixed
	.p2align	4, 0x90
	.type	gs_distance_transform2fixed,@function
gs_distance_transform2fixed:            # @gs_distance_transform2fixed
	.cfi_startproc
# BB#0:
	movabsq	$9223372036854775807, %r8 # imm = 0x7FFFFFFFFFFFFFFF
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	.LCPI13_0(%rip), %xmm3  # xmm3 = mem[0],zero
	mulsd	%xmm3, %xmm2
	cvttsd2si	%xmm2, %rdx
	movq	%rdx, (%rsi)
	movss	48(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm3, %xmm2
	cvttsd2si	%xmm2, %r9
	movq	%r9, 8(%rsi)
	movq	32(%rdi), %rcx
	movq	%rcx, %rax
	addq	%rax, %rax
	je	.LBB13_2
# BB#1:
	movd	%ecx, %xmm2
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	.LCPI13_0(%rip), %xmm2
	cvttsd2si	%xmm2, %rax
	addq	%rdx, %rax
	movq	%rax, (%rsi)
.LBB13_2:
	movq	16(%rdi), %rax
	testq	%r8, %rax
	je	.LBB13_4
# BB#3:
	movd	%eax, %xmm1
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	.LCPI13_0(%rip), %xmm1
	cvttsd2si	%xmm1, %rax
	addq	%r9, %rax
	movq	%rax, 8(%rsi)
.LBB13_4:
	xorl	%eax, %eax
	retq
.Lfunc_end13:
	.size	gs_distance_transform2fixed, .Lfunc_end13-gs_distance_transform2fixed
	.cfi_endproc

	.type	gs_identity_matrix,@object # @gs_identity_matrix
	.data
	.globl	gs_identity_matrix
	.p2align	3
gs_identity_matrix:
	.long	1065353216              # float 1
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	1065353216              # float 1
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.size	gs_identity_matrix, 96


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
