	.text
	.file	"clamscan_others.bc"
	.globl	fileinfo
	.p2align	4, 0x90
	.type	fileinfo,@function
fileinfo:                               # @fileinfo
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$144, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 160
.Lcfi2:
	.cfi_offset %rbx, -16
	movl	%esi, %ebx
	movq	%rdi, %rax
	movq	%rsp, %rdx
	movl	$1, %edi
	movq	%rax, %rsi
	callq	__xstat
	cmpl	$-1, %eax
	je	.LBB0_1
# BB#2:
	movswl	%bx, %eax
	decl	%eax
	cmpl	$4, %eax
	ja	.LBB0_8
# BB#3:
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_9:
	movl	48(%rsp), %eax
	jmp	.LBB0_10
.LBB0_1:
	movl	$-1, %eax
	jmp	.LBB0_10
.LBB0_5:
	movl	88(%rsp), %eax
	jmp	.LBB0_10
.LBB0_7:
	movl	32(%rsp), %eax
	jmp	.LBB0_10
.LBB0_4:
	movl	24(%rsp), %eax
	jmp	.LBB0_10
.LBB0_6:
	movl	28(%rsp), %eax
.LBB0_10:
	addq	$144, %rsp
	popq	%rbx
	retq
.LBB0_8:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	logg
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	fileinfo, .Lfunc_end0-fileinfo
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_9
	.quad	.LBB0_4
	.quad	.LBB0_5
	.quad	.LBB0_6
	.quad	.LBB0_7

	.text
	.globl	checkaccess
	.p2align	4, 0x90
	.type	checkaccess,@function
checkaccess:                            # @checkaccess
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 48
.Lcfi8:
	.cfi_offset %rbx, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	callq	geteuid
	testl	%eax, %eax
	je	.LBB1_1
# BB#13:
	movq	%rbx, %rdi
	movl	%r15d, %esi
	callq	access
	xorl	%ebp, %ebp
	testl	%eax, %eax
.LBB1_14:
	sete	%bpl
.LBB1_15:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_1:
	movq	%rbp, %rdi
	callq	getpwnam
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB1_2
# BB#3:
	movl	$-2, %ebp
	callq	fork
	cmpl	$-1, %eax
	je	.LBB1_15
# BB#4:
	testl	%eax, %eax
	je	.LBB1_5
# BB#12:
	leaq	4(%rsp), %rdi
	callq	wait
	movl	$65407, %eax            # imm = 0xFF7F
	andl	4(%rsp), %eax
	xorl	%ebp, %ebp
	cmpl	$256, %eax              # imm = 0x100
	jmp	.LBB1_14
.LBB1_2:
	movl	$-1, %ebp
	jmp	.LBB1_15
.LBB1_5:
	movl	20(%r14), %edi
	callq	setgid
	testl	%eax, %eax
	jne	.LBB1_6
# BB#8:
	movl	16(%r14), %edi
	callq	setuid
	testl	%eax, %eax
	jne	.LBB1_9
# BB#10:
	movq	%rbx, %rdi
	movl	%r15d, %esi
	callq	access
	testl	%eax, %eax
	jne	.LBB1_16
# BB#11:
	movl	$1, %edi
	callq	exit
.LBB1_6:
	movq	stderr(%rip), %rdi
	movl	20(%r14), %edx
	movl	$.L.str.1, %esi
	jmp	.LBB1_7
.LBB1_9:
	movq	stderr(%rip), %rdi
	movl	16(%r14), %edx
	movl	$.L.str.2, %esi
.LBB1_7:
	xorl	%eax, %eax
	callq	fprintf
	xorl	%edi, %edi
	callq	exit
.LBB1_16:
	xorl	%edi, %edi
	callq	exit
.Lfunc_end1:
	.size	checkaccess, .Lfunc_end1-checkaccess
	.cfi_endproc

	.globl	match_regex
	.p2align	4, 0x90
	.type	match_regex,@function
match_regex:                            # @match_regex
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 64
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rsp, %rdi
	movl	$1, %edx
	callq	cli_regcomp
	testl	%eax, %eax
	je	.LBB2_2
# BB#1:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	logg
	movl	$2, %ebx
	jmp	.LBB2_3
.LBB2_2:
	movq	%rsp, %r15
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	cli_regexec
	xorl	%ebx, %ebx
	cmpl	$1, %eax
	setne	%bl
	movq	%r15, %rdi
	callq	cli_regfree
.LBB2_3:
	movl	%ebx, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	match_regex, .Lfunc_end2-match_regex
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"!fileinfo(): Unknown option.\n"
	.size	.L.str, 30

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"ERROR: setgid(%d) failed.\n"
	.size	.L.str.1, 27

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"ERROR: setuid(%d) failed.\n"
	.size	.L.str.2, 27

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"!%s: Could not parse regular expression %s.\n"
	.size	.L.str.3, 45


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
