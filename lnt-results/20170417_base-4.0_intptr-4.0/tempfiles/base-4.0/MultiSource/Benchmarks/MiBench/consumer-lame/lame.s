	.text
	.file	"lame.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4652007308841189376     # double 1000
.LCPI0_2:
	.quad	4666723172467343360     # double 1.0E+4
.LCPI0_6:
	.quad	4616639978017495450     # double 4.4000000000000004
.LCPI0_8:
	.quad	4589168020290535424     # double 0.0625
.LCPI0_9:
	.quad	-4597612269591855104    # double -18
.LCPI0_10:
	.quad	4624352392379367424     # double 14.5
.LCPI0_11:
	.quad	4607182418800017408     # double 1
.LCPI0_12:
	.quad	4629418941960159232     # double 31
.LCPI0_13:
	.quad	4609753056924675352     # double 1.5707963267948966
.LCPI0_14:
	.quad	9218868437227405312     # double +Inf
.LCPI0_15:
	.quad	-4503599627370496       # double -Inf
.LCPI0_16:
	.quad	-4618441417868443648    # double -0.75
.LCPI0_17:
	.quad	4581933205405275724     # double 0.021774193548387097
.LCPI0_18:
	.quad	4604930618986332160     # double 0.75
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_1:
	.long	1095761920              # float 13
.LCPI0_3:
	.long	1065353216              # float 1
.LCPI0_4:
	.long	1073741824              # float 2
.LCPI0_5:
	.long	1093664768              # float 11
.LCPI0_7:
	.long	1091567616              # float 9
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_19:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
.LCPI0_20:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI0_21:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
.LCPI0_22:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.text
	.globl	lame_init_params
	.p2align	4, 0x90
	.type	lame_init_params,@function
lame_init_params:                       # @lame_init_params
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, bs+32(%rip)
	movupd	%xmm0, bs+16(%rip)
	movupd	%xmm0, bs(%rip)
	movq	$0, bs+48(%rip)
	movl	$l3_side, %edi
	xorl	%esi, %esi
	movl	$528, %edx              # imm = 0x210
	callq	memset
	movq	$0, 168(%rbx)
	callq	InitFormatBitStream
	cmpl	$1, 8(%rbx)
	jne	.LBB0_1
# BB#2:
	movl	$3, 36(%rbx)
	movl	$3, %r8d
	jmp	.LBB0_3
.LBB0_1:                                # %._crit_edge
	movl	36(%rbx), %r8d
.LBB0_3:
	xorl	%ecx, %ecx
	cmpl	$3, %r8d
	setne	%cl
	incl	%ecx
	movl	%ecx, 204(%rbx)
	movl	16(%rbx), %esi
	testl	%esi, %esi
	je	.LBB0_5
# BB#4:                                 # %._crit_edge371
	leaq	48(%rbx), %r14
	movl	48(%rbx), %edx
.LBB0_22:
	xorl	%edi, %edi
	cmpl	$24000, %esi            # imm = 0x5DC0
	setg	%dil
	incl	%edi
	movl	%edi, 200(%rbx)
	movl	$800, 184(%rbx)         # imm = 0x320
	shll	$6, %edi
	leal	(%rdi,%rdi,8), %edi
	movl	%edi, 188(%rbx)
	testl	%edx, %edx
	jne	.LBB0_24
# BB#23:
	cmpl	$24001, %esi            # imm = 0x5DC1
	movl	$64, %ebp
	movl	$128, %edx
	cmovll	%ebp, %edx
	movl	%edx, (%r14)
.LBB0_24:
	movl	$1065353216, 216(%rbx)  # imm = 0x3F800000
	movl	12(%rbx), %ebp
	cmpl	%ebp, %esi
	jne	.LBB0_26
# BB#25:
	movss	.LCPI0_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB0_27
.LBB0_26:
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%esi, %xmm1
	divss	%xmm1, %xmm0
	movss	%xmm0, 216(%rbx)
.LBB0_27:
	movq	(%rbx), %rbp
	testq	%rbp, %rbp
	js	.LBB0_28
# BB#29:
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rbp, %xmm1
	jmp	.LBB0_30
.LBB0_28:
	movq	%rbp, %rax
	shrq	%rax
	andl	$1, %ebp
	orq	%rax, %rbp
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rbp, %xmm1
	addss	%xmm1, %xmm1
.LBB0_30:
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%edi, %xmm2
	mulss	%xmm2, %xmm0
	divss	%xmm0, %xmm1
	addss	.LCPI0_4(%rip), %xmm1
	cvttss2si	%xmm1, %rax
	movq	%rax, 176(%rbx)
	cmpl	$319, %edx              # imm = 0x13F
	jle	.LBB0_31
# BB#32:
	movl	$0, 88(%rbx)
	xorl	%edi, %edi
	jmp	.LBB0_33
.LBB0_31:                               # %._crit_edge374
	movl	88(%rbx), %edi
.LBB0_33:
	imull	%ecx, %esi
	shll	$4, %esi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	mulsd	.LCPI0_0(%rip), %xmm1
	divsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	testl	%edi, %edi
	je	.LBB0_36
# BB#34:
	ucomiss	.LCPI0_5(%rip), %xmm0
	jbe	.LBB0_36
# BB#35:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	92(%rbx), %xmm0
	addsd	.LCPI0_6(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
.LBB0_36:
	cmpl	$0, 40(%rbx)
	jne	.LBB0_40
# BB#37:
	movss	.LCPI0_7(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB0_40
# BB#38:
	cmpl	$3, %r8d
	je	.LBB0_40
# BB#39:
	movl	$0, 36(%rbx)
.LBB0_40:
	cmpl	$0, 104(%rbx)
	jne	.LBB0_43
# BB#41:
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI0_8(%rip), %xmm0
	callq	log
	mulsd	.LCPI0_9(%rip), %xmm0
	addsd	.LCPI0_10(%rip), %xmm0
	callq	floor
	addsd	.LCPI0_11(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	$30, %eax
	jg	.LBB0_43
# BB#42:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	.LCPI0_12(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 232(%rbx)
	movss	%xmm0, 236(%rbx)
.LBB0_43:
	movl	108(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB0_45
# BB#44:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	16(%rbx), %xmm1
	divsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movl	116(%rbx), %ecx
	xorl	%edx, %edx
	testl	%ecx, %ecx
	cmovnsl	%ecx, %edx
	addl	%eax, %edx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edx, %xmm2
	addsd	%xmm2, %xmm2
	divsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	.LCPI0_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	minss	%xmm0, %xmm3
	movss	%xmm3, 240(%rbx)
	minss	%xmm1, %xmm2
	movss	%xmm2, 244(%rbx)
.LBB0_45:
	movl	104(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB0_51
# BB#46:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	16(%rbx), %xmm1
	divsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 236(%rbx)
	movl	112(%rbx), %ecx
	testl	%ecx, %ecx
	js	.LBB0_48
# BB#47:
	subl	%ecx, %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	addsd	%xmm2, %xmm2
	divsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	leaq	232(%rbx), %rax
	movss	%xmm1, 232(%rbx)
	xorpd	%xmm2, %xmm2
	ucomiss	%xmm1, %xmm2
	ja	.LBB0_49
	jmp	.LBB0_50
.LBB0_5:
	movl	12(%rbx), %edx
	movl	%edx, 16(%rbx)
	movl	$48000, %esi            # imm = 0xBB80
	cmpl	$47999, %edx            # imm = 0xBB7F
	jg	.LBB0_10
# BB#6:
	movl	$44100, %esi            # imm = 0xAC44
	cmpl	$44099, %edx            # imm = 0xAC43
	jg	.LBB0_10
# BB#7:
	movl	$32000, %esi            # imm = 0x7D00
	cmpl	$31999, %edx            # imm = 0x7CFF
	jg	.LBB0_10
# BB#8:
	movl	$24000, %esi            # imm = 0x5DC0
	cmpl	$23999, %edx            # imm = 0x5DBF
	jg	.LBB0_10
# BB#9:
	cmpl	$22049, %edx            # imm = 0x5621
	movl	$22050, %edx            # imm = 0x5622
	movl	$16000, %esi            # imm = 0x3E80
	cmovgl	%edx, %esi
.LBB0_10:
	movl	%esi, 16(%rbx)
	leaq	48(%rbx), %r14
	movl	48(%rbx), %edx
	testl	%edx, %edx
	jle	.LBB0_22
# BB#11:
	cmpl	$0, 88(%rbx)
	jne	.LBB0_22
# BB#12:
	movl	%esi, %edi
	imull	%ecx, %edi
	shll	$4, %edi
	cvtsi2sdl	%edi, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movsd	.LCPI0_0(%rip), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	divsd	%xmm2, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	ucomiss	.LCPI0_1(%rip), %xmm1
	jbe	.LBB0_22
# BB#13:
	mulsd	.LCPI0_2(%rip), %xmm0
	movl	%ecx, %esi
	shll	$4, %esi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	divsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %esi
	movl	%esi, 16(%rbx)
	cmpl	$16000, %esi            # imm = 0x3E80
	jg	.LBB0_15
# BB#14:
	movl	$16000, 16(%rbx)        # imm = 0x3E80
	movl	$16000, %esi            # imm = 0x3E80
	jmp	.LBB0_22
.LBB0_48:
	leaq	232(%rbx), %rax
	movapd	%xmm0, %xmm2
.LBB0_49:                               # %.sink.split
	movss	%xmm2, (%rax)
	movapd	%xmm2, %xmm1
.LBB0_50:
	movss	.LCPI0_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	minss	%xmm1, %xmm3
	movss	%xmm3, (%rax)
	minss	%xmm0, %xmm2
	movss	%xmm2, 236(%rbx)
.LBB0_51:
	cmpl	$0, 256(%rbx)
	jne	.LBB0_77
# BB#52:
	movss	232(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm8, %xmm8
	ucomiss	%xmm8, %xmm0
	jbe	.LBB0_63
# BB#53:                                # %.preheader360
	xorl	%ebp, %ebp
	xorpd	%xmm7, %xmm7
	movl	$999, %r15d             # imm = 0x3E7
	movsd	.LCPI0_12(%rip), %xmm6  # xmm6 = mem[0],zero
	movsd	.LCPI0_13(%rip), %xmm5  # xmm5 = mem[0],zero
	movsd	.LCPI0_14(%rip), %xmm9  # xmm9 = mem[0],zero
	movsd	.LCPI0_15(%rip), %xmm10 # xmm10 = mem[0],zero
	movsd	.LCPI0_11(%rip), %xmm11 # xmm11 = mem[0],zero
	jmp	.LBB0_54
.LBB0_15:
	cmpl	$22050, %esi            # imm = 0x5622
	jg	.LBB0_17
# BB#16:
	movl	$22050, 16(%rbx)        # imm = 0x5622
	movl	$22050, %esi            # imm = 0x5622
	jmp	.LBB0_22
.LBB0_17:
	cmpl	$24000, %esi            # imm = 0x5DC0
	jg	.LBB0_19
# BB#18:
	movl	$24000, 16(%rbx)        # imm = 0x5DC0
	movl	$24000, %esi            # imm = 0x5DC0
	jmp	.LBB0_22
.LBB0_19:
	cmpl	$32000, %esi            # imm = 0x7D00
	jg	.LBB0_21
# BB#20:
	movl	$32000, 16(%rbx)        # imm = 0x7D00
	movl	$32000, %esi            # imm = 0x7D00
	jmp	.LBB0_22
.LBB0_21:
	cmpl	$44101, %esi            # imm = 0xAC45
	movl	$44100, %edi            # imm = 0xAC44
	movl	$48000, %esi            # imm = 0xBB80
	cmovll	%edi, %esi
	movl	%esi, 16(%rbx)
	jmp	.LBB0_22
.LBB0_60:                               # %cdce.call
                                        #   in Loop: Header=BB0_54 Depth=1
	movsd	%xmm7, (%rsp)           # 8-byte Spill
	callq	cos
	movsd	.LCPI0_11(%rip), %xmm11 # xmm11 = mem[0],zero
	movsd	.LCPI0_15(%rip), %xmm10 # xmm10 = mem[0],zero
	movsd	.LCPI0_14(%rip), %xmm9  # xmm9 = mem[0],zero
	movsd	.LCPI0_13(%rip), %xmm5  # xmm5 = mem[0],zero
	movsd	(%rsp), %xmm7           # 8-byte Reload
                                        # xmm7 = mem[0],zero
	movsd	.LCPI0_12(%rip), %xmm6  # xmm6 = mem[0],zero
	xorps	%xmm8, %xmm8
	jmp	.LBB0_61
	.p2align	4, 0x90
.LBB0_54:                               # =>This Inner Loop Header: Depth=1
	movapd	%xmm7, %xmm2
	divsd	%xmm6, %xmm2
	movss	236(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm1, %xmm3
	ucomisd	%xmm3, %xmm2
	jb	.LBB0_56
# BB#55:                                #   in Loop: Header=BB0_54 Depth=1
	movl	248(%rbx), %eax
	cmpl	%ebp, %eax
	cmovgl	%ebp, %eax
	movl	%eax, 248(%rbx)
.LBB0_56:                               #   in Loop: Header=BB0_54 Depth=1
	movss	232(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm2
	jbe	.LBB0_61
# BB#57:                                #   in Loop: Header=BB0_54 Depth=1
	ucomisd	%xmm2, %xmm3
	jbe	.LBB0_61
# BB#58:                                #   in Loop: Header=BB0_54 Depth=1
	cmpl	%ebp, %r15d
	cmovgl	%ebp, %r15d
	subsd	%xmm2, %xmm0
	mulsd	%xmm5, %xmm0
	subss	%xmm4, %xmm1
	cvtss2sd	%xmm1, %xmm1
	divsd	%xmm1, %xmm0
	ucomisd	%xmm9, %xmm0
	jae	.LBB0_60
# BB#59:                                #   in Loop: Header=BB0_54 Depth=1
	ucomisd	%xmm0, %xmm10
	jae	.LBB0_60
.LBB0_61:                               # %cdce.end
                                        #   in Loop: Header=BB0_54 Depth=1
	addsd	%xmm11, %xmm7
	incl	%ebp
	cmpl	$32, %ebp
	jne	.LBB0_54
# BB#62:
	cmpl	$999, %r15d             # imm = 0x3E7
	movl	248(%rbx), %eax
	cmovel	%eax, %r15d
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r15d, %xmm0
	addsd	.LCPI0_16(%rip), %xmm0
	divsd	%xmm6, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 232(%rbx)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	%xmm6, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 236(%rbx)
.LBB0_63:
	movss	244(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm8, %xmm0
	jbe	.LBB0_66
# BB#64:
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm1
	movsd	.LCPI0_17(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	ja	.LBB0_65
.LBB0_66:
	xorpd	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB0_77
# BB#67:                                # %.preheader359
	xorl	%ebp, %ebp
	xorpd	%xmm5, %xmm5
	movl	$-1, %r15d
	movsd	.LCPI0_12(%rip), %xmm4  # xmm4 = mem[0],zero
	movsd	.LCPI0_13(%rip), %xmm6  # xmm6 = mem[0],zero
	movsd	.LCPI0_14(%rip), %xmm7  # xmm7 = mem[0],zero
	movsd	.LCPI0_15(%rip), %xmm8  # xmm8 = mem[0],zero
	movsd	.LCPI0_11(%rip), %xmm9  # xmm9 = mem[0],zero
	jmp	.LBB0_68
.LBB0_65:
	movq	$0, 240(%rbx)
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$64, %esi
	movl	$1, %edx
	callq	fwrite
	movss	244(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB0_66
.LBB0_74:                               # %cdce.call356
                                        #   in Loop: Header=BB0_68 Depth=1
	movsd	%xmm5, (%rsp)           # 8-byte Spill
	callq	cos
	movsd	.LCPI0_11(%rip), %xmm9  # xmm9 = mem[0],zero
	movsd	.LCPI0_15(%rip), %xmm8  # xmm8 = mem[0],zero
	movsd	.LCPI0_14(%rip), %xmm7  # xmm7 = mem[0],zero
	movsd	.LCPI0_13(%rip), %xmm6  # xmm6 = mem[0],zero
	movsd	(%rsp), %xmm5           # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movsd	.LCPI0_12(%rip), %xmm4  # xmm4 = mem[0],zero
	jmp	.LBB0_75
	.p2align	4, 0x90
.LBB0_68:                               # =>This Inner Loop Header: Depth=1
	movapd	%xmm5, %xmm2
	divsd	%xmm4, %xmm2
	movss	240(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	ucomisd	%xmm2, %xmm0
	jb	.LBB0_70
# BB#69:                                #   in Loop: Header=BB0_68 Depth=1
	movl	252(%rbx), %eax
	cmpl	%ebp, %eax
	cmovll	%ebp, %eax
	movl	%eax, 252(%rbx)
.LBB0_70:                               #   in Loop: Header=BB0_68 Depth=1
	ucomisd	%xmm0, %xmm2
	jbe	.LBB0_75
# BB#71:                                #   in Loop: Header=BB0_68 Depth=1
	movss	244(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm3, %xmm0
	ucomisd	%xmm2, %xmm0
	jbe	.LBB0_75
# BB#72:                                #   in Loop: Header=BB0_68 Depth=1
	cmpl	%ebp, %r15d
	cmovll	%ebp, %r15d
	subsd	%xmm2, %xmm0
	mulsd	%xmm6, %xmm0
	subss	%xmm1, %xmm3
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm3, %xmm1
	divsd	%xmm1, %xmm0
	ucomisd	%xmm7, %xmm0
	jae	.LBB0_74
# BB#73:                                #   in Loop: Header=BB0_68 Depth=1
	ucomisd	%xmm0, %xmm8
	jae	.LBB0_74
.LBB0_75:                               # %cdce.end357
                                        #   in Loop: Header=BB0_68 Depth=1
	addsd	%xmm9, %xmm5
	incl	%ebp
	cmpl	$32, %ebp
	jne	.LBB0_68
# BB#76:
	movl	252(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	%xmm4, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 240(%rbx)
	cmpl	$-1, %r15d
	cmovel	%eax, %r15d
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r15d, %xmm0
	addsd	.LCPI0_18(%rip), %xmm0
	divsd	%xmm4, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 244(%rbx)
.LBB0_77:
	movl	$0, 228(%rbx)
	xorl	%eax, %eax
	cmpl	$3, 36(%rbx)
	setne	%al
	incl	%eax
	movl	%eax, 204(%rbx)
	movslq	16(%rbx), %rdi
	leaq	192(%rbx), %rsi
	callq	SmpFrqIndex
	movl	%eax, 224(%rbx)
	testl	%eax, %eax
	js	.LBB0_118
# BB#78:
	movl	(%r14), %edi
	movl	16(%rbx), %edx
	movl	192(%rbx), %esi
	callq	BitrateIndex
	movl	%eax, 220(%rbx)
	testl	%eax, %eax
	js	.LBB0_118
# BB#79:
	xorl	%ebp, %ebp
	cmpl	$0, 88(%rbx)
	je	.LBB0_89
# BB#80:
	movl	100(%rbx), %edi
	testl	%edi, %edi
	je	.LBB0_81
# BB#82:
	movl	16(%rbx), %edx
	movl	192(%rbx), %esi
	callq	BitrateIndex
	movl	%eax, 212(%rbx)
	testl	%eax, %eax
	js	.LBB0_118
# BB#83:                                # %._crit_edge379
	movl	96(%rbx), %edi
	testl	%edi, %edi
	jne	.LBB0_86
	jmp	.LBB0_85
.LBB0_81:
	movl	92(%rbx), %eax
	movl	96(%rbx), %edi
	xorl	%ecx, %ecx
	cmpl	$255, %edi
	setg	%cl
	addl	$13, %ecx
	testl	%eax, %eax
	movl	$14, %edx
	cmovnel	%ecx, %edx
	cmpl	$3, %eax
	movl	$12, %ecx
	cmovlel	%edx, %ecx
	cmpl	$7, %eax
	movl	$9, %eax
	cmovlel	%ecx, %eax
	movl	%eax, 212(%rbx)
	testl	%edi, %edi
	je	.LBB0_85
.LBB0_86:
	movl	16(%rbx), %edx
	movl	192(%rbx), %esi
	callq	BitrateIndex
	movl	%eax, 208(%rbx)
	testl	%eax, %eax
	jns	.LBB0_87
.LBB0_118:
	movq	stderr(%rip), %rdi
	callq	display_bitrates
	movl	$1, %edi
	callq	exit
.LBB0_85:
	movl	$1, 208(%rbx)
.LBB0_87:
	movl	88(%rbx), %eax
	testl	%eax, %eax
	je	.LBB0_89
# BB#88:
	movl	28(%rbx), %ecx
	cmpl	$3, %ecx
	movl	$2, %edx
	cmovll	%ecx, %edx
	movl	%edx, 28(%rbx)
	movl	%eax, %ebp
.LBB0_89:                               # %.thread
	cmpl	$3, 36(%rbx)
	jne	.LBB0_91
# BB#90:
	movl	$0, 44(%rbx)
.LBB0_91:
	testl	%ebp, %ebp
	jne	.LBB0_93
# BB#92:
	movl	$0, 24(%rbx)
.LBB0_93:
	movq	136(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_96
# BB#94:
	cmpb	$45, (%rax)
	jne	.LBB0_97
# BB#95:
	movl	$0, 24(%rbx)
	cmpb	$45, (%rax)
	jne	.LBB0_97
.LBB0_96:                               # %.thread343
	movl	$0, id3tag(%rip)
.LBB0_97:                               # %.thread391
	cmpl	$0, 20(%rbx)
	je	.LBB0_99
# BB#98:
	movl	$0, 24(%rbx)
.LBB0_99:
	movl	$bs, %edi
	callq	init_bit_stream_w
	movl	28(%rbx), %eax
	cmpq	$9, %rax
	ja	.LBB0_102
# BB#100:
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_111:                              # %.sink.split409
	movl	$2, 28(%rbx)
.LBB0_112:
	movl	$1, 272(%rbx)
	movapd	.LCPI0_21(%rip), %xmm0  # xmm0 = [0,1,1,0]
	jmp	.LBB0_113
.LBB0_114:
	movl	$1, 272(%rbx)
	movapd	.LCPI0_20(%rip), %xmm0  # xmm0 = [0,1,1,1]
.LBB0_113:                              # %.preheader358.preheader
	movupd	%xmm0, 256(%rbx)
	movl	$1, 276(%rbx)
	jmp	.LBB0_102
.LBB0_109:                              # %.thread349
	movl	$5, 28(%rbx)
.LBB0_110:
	movl	$1, 272(%rbx)
	movapd	.LCPI0_22(%rip), %xmm0  # xmm0 = [0,0,1,0]
	jmp	.LBB0_108
.LBB0_106:                              # %.thread346
	movl	$7, 28(%rbx)
.LBB0_107:
	movl	$1, 272(%rbx)
	xorpd	%xmm0, %xmm0
.LBB0_108:                              # %.preheader358.preheader
	movupd	%xmm0, 256(%rbx)
	movl	$0, 276(%rbx)
.LBB0_102:                              # %.preheader358.preheader
	movq	$-88, %rax
	jmp	.LBB0_103
	.p2align	4, 0x90
.LBB0_117:                              # %.preheader358.1
                                        #   in Loop: Header=BB0_103 Depth=1
	movslq	192(%rbx), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movslq	224(%rbx), %rdx
	addq	%rcx, %rdx
	imulq	$148, %rdx, %rcx
	movl	sfBandIndex+92(%rcx,%rax), %ecx
	movl	%ecx, scalefac_band+92(%rax)
	addq	$8, %rax
.LBB0_103:                              # %.preheader358
                                        # =>This Inner Loop Header: Depth=1
	movslq	192(%rbx), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movslq	224(%rbx), %rdx
	addq	%rcx, %rdx
	imulq	$148, %rdx, %rcx
	movl	sfBandIndex+88(%rcx,%rax), %ecx
	movl	%ecx, scalefac_band+88(%rax)
	testq	%rax, %rax
	jne	.LBB0_117
# BB#104:                               # %.preheader.preheader
	movl	192(%rbx), %eax
	movl	224(%rbx), %ecx
	leal	(%rax,%rax,2), %edx
	addl	%ecx, %edx
	movslq	%edx, %rdx
	imulq	$148, %rdx, %rdx
	movups	sfBandIndex+92(%rdx), %xmm0
	movups	%xmm0, scalefac_band+92(%rip)
	movups	sfBandIndex+108(%rdx), %xmm0
	movups	%xmm0, scalefac_band+108(%rip)
	movups	sfBandIndex+124(%rdx), %xmm0
	movups	%xmm0, scalefac_band+124(%rip)
	movl	sfBandIndex+140(%rdx), %esi
	movl	%esi, scalefac_band+140(%rip)
	movl	sfBandIndex+144(%rdx), %edx
	movl	%edx, scalefac_band+144(%rip)
	cmpl	$0, 24(%rbx)
	je	.LBB0_116
# BB#105:
	movl	$1, %esi
	subl	%eax, %esi
	movl	36(%rbx), %edx
	movl	$bs, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	InitVbrTag              # TAILCALL
.LBB0_116:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_101:                              # %.thread393
	leaq	256(%rbx), %rax
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, (%rax)
	movq	$0, 16(%rax)
	jmp	.LBB0_102
.LBB0_115:
	movl	$1, 272(%rbx)
	movaps	.LCPI0_19(%rip), %xmm0  # xmm0 = [1,1,3,2]
	movups	%xmm0, 256(%rbx)
	movl	$2, 276(%rbx)
	movl	$-99, %edi
	callq	exit
.Lfunc_end0:
	.size	lame_init_params, .Lfunc_end0-lame_init_params
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_115
	.quad	.LBB0_114
	.quad	.LBB0_112
	.quad	.LBB0_111
	.quad	.LBB0_111
	.quad	.LBB0_110
	.quad	.LBB0_109
	.quad	.LBB0_107
	.quad	.LBB0_106
	.quad	.LBB0_101

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4652007308841189376     # double 1000
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_1:
	.long	1065353216              # float 1
.LCPI1_2:
	.long	1140457472              # float 500
	.text
	.globl	lame_print_config
	.p2align	4, 0x90
	.type	lame_print_config,@function
lame_print_config:                      # @lame_print_config
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 64
.Lcfi15:
	.cfi_offset %rbx, -48
.Lcfi16:
	.cfi_offset %r12, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cvtsi2sdl	16(%rbx), %xmm0
	divsd	.LCPI1_0(%rip), %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movss	216(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movl	204(%rbx), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2ssl	48(%rbx), %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	stderr(%rip), %rdi
	callq	lame_print_version
	cmpl	$2, 8(%rbx)
	jne	.LBB1_3
# BB#1:
	cmpl	$1, 204(%rbx)
	jne	.LBB1_3
# BB#2:
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$67, %esi
	movl	$1, %edx
	callq	fwrite
.LBB1_3:
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rsp)           # 4-byte Spill
	movss	216(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI1_1(%rip), %xmm0
	jne	.LBB1_4
	jnp	.LBB1_5
.LBB1_4:
	movss	(%rsp), %xmm0           # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movq	stderr(%rip), %rdi
	cvttss2si	%xmm1, %edx
	cvttss2si	%xmm0, %ecx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB1_5:
	movss	244(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm1
	jbe	.LBB1_7
# BB#6:
	movq	stderr(%rip), %rdi
	movss	240(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	(%rsp), %xmm3           # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	.LCPI1_2(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	cvtss2sd	%xmm0, %xmm0
	mulss	%xmm3, %xmm1
	mulss	%xmm2, %xmm1
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.7, %esi
	movb	$2, %al
	callq	fprintf
	xorps	%xmm2, %xmm2
.LBB1_7:
	movss	232(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jbe	.LBB1_9
# BB#8:
	movq	stderr(%rip), %rdi
	movss	(%rsp), %xmm3           # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	.LCPI1_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movss	236(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	mulss	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm1
	movl	$.L.str.8, %esi
	movb	$2, %al
	callq	fprintf
.LBB1_9:
	cmpl	$0, 20(%rbx)
	movq	stderr(%rip), %r14
	movq	128(%rbx), %r12
	jne	.LBB1_10
# BB#11:
	cmpb	$45, (%r12)
	jne	.LBB1_14
# BB#12:
	cmpb	$0, 1(%r12)
	jne	.LBB1_14
# BB#13:
	movl	$.L.str.12, %r15d
	jmp	.LBB1_15
.LBB1_10:
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	fprintf
	jmp	.LBB1_22
.LBB1_14:                               # %.thread
	movl	$47, %esi
	movq	%r12, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %r15
	cmoveq	%r12, %r15
.LBB1_15:
	movq	136(%rbx), %r12
	cmpb	$45, (%r12)
	jne	.LBB1_18
# BB#16:
	cmpb	$0, 1(%r12)
	jne	.LBB1_18
# BB#17:
	movl	$.L.str.13, %ecx
	jmp	.LBB1_19
.LBB1_18:                               # %.thread101
	movl	$47, %esi
	movq	%r12, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rcx
	cmoveq	%r12, %rcx
.LBB1_19:
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	fprintf
	cmpl	$0, 88(%rbx)
	movq	stderr(%rip), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	16(%rbx), %xmm0
	divsd	.LCPI1_0(%rip), %xmm0
	movslq	36(%rbx), %rax
	jne	.LBB1_20
# BB#21:
	shll	$4, %ebp
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ebp, %xmm1
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	divss	8(%rsp), %xmm2          # 4-byte Folded Reload
	movl	28(%rbx), %r9d
	movl	48(%rbx), %edx
	movq	lame_print_config.mode_names(,%rax,8), %rcx
	movl	$2, %r8d
	subl	192(%rbx), %r8d
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm1
	movl	$.L.str.15, %esi
	movb	$2, %al
	callq	fprintf
	jmp	.LBB1_22
.LBB1_20:
	movl	28(%rbx), %r9d
	movl	92(%rbx), %edx
	movq	lame_print_config.mode_names(,%rax,8), %rcx
	movl	$2, %r8d
	subl	192(%rbx), %r8d
	movl	$.L.str.14, %esi
	movb	$1, %al
	callq	fprintf
.LBB1_22:
	movq	stderr(%rip), %rdi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fflush                  # TAILCALL
.Lfunc_end1:
	.size	lame_print_config, .Lfunc_end1-lame_print_config
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4652007308841189376     # double 1000
.LCPI2_1:
	.quad	4620693217682128896     # double 8
.LCPI2_2:
	.quad	4472406533629990549     # double 1.0000000000000001E-9
.LCPI2_5:
	.quad	-4616189618054758400    # double -1
.LCPI2_6:
	.quad	4607182418800017408     # double 1
.LCPI2_7:
	.quad	4598175219545276416     # double 0.25
.LCPI2_8:
	.quad	4599976659396224614     # double 0.34999999999999998
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_3:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
.LCPI2_4:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	lame_encode_frame
	.p2align	4, 0x90
	.type	lame_encode_frame,@function
lame_encode_frame:                      # @lame_encode_frame
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$36600, %rsp            # imm = 0x8EF8
.Lcfi26:
	.cfi_def_cfa_offset 36656
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movl	%r9d, %r15d
	movq	%r8, %r13
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	$0, 48(%rsp)
	leaq	1136(%rsp), %rdi
	xorl	%esi, %esi
	movl	$3904, %edx             # imm = 0xF40
	callq	memset
	leaq	14256(%rsp), %rdi
	xorl	%esi, %esi
	movl	$3904, %edx             # imm = 0xF40
	callq	memset
	leaq	160(%rsp), %rdi
	xorl	%esi, %esi
	movl	$976, %edx              # imm = 0x3D0
	callq	memset
	movq	%rbp, 16(%rsp)
	movq	%r12, 24(%rsp)
	movl	$0, 228(%rbx)
	movq	168(%rbx), %r14
	testq	%r14, %r14
	jne	.LBB2_2
# BB#1:
	cvtsi2sdl	16(%rbx), %xmm0
	divsd	.LCPI2_0(%rip), %xmm0
	movl	48(%rbx), %eax
	movq	$0, lame_encode_frame.sentBits(%rip)
	movb	$1, lame_encode_frame.bitsPerSlot(%rip)
	imull	188(%rbx), %eax
	cvtsi2sdl	%eax, %xmm1
	mulsd	.LCPI2_1(%rip), %xmm0
	divsd	%xmm0, %xmm1
	movapd	%xmm1, 32(%rsp)         # 16-byte Spill
	movapd	%xmm1, %xmm0
	addsd	.LCPI2_2(%rip), %xmm0
	callq	floor
	movapd	32(%rsp), %xmm1         # 16-byte Reload
	subsd	%xmm0, %xmm1
	movapd	.LCPI2_3(%rip), %xmm0   # xmm0 = [nan,nan]
	andpd	%xmm1, %xmm0
	cmpltsd	.LCPI2_2(%rip), %xmm0
	andnpd	%xmm1, %xmm0
	movsd	%xmm0, lame_encode_frame.frac_SpF(%rip)
	movapd	.LCPI2_4(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, lame_encode_frame.slot_lag(%rip)
	xorpd	%xmm1, %xmm1
	cmpneqsd	%xmm0, %xmm1
	movd	%xmm1, %rax
	andl	$1, %eax
	movl	%eax, 196(%rbx)
.LBB2_2:
	movl	64(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB2_5
# BB#3:
	testl	%eax, %eax
	je	.LBB2_4
# BB#6:
	cmpl	$0, 88(%rbx)
	je	.LBB2_8
.LBB2_4:
	movl	$0, 196(%rbx)
	cmpl	$0, 20(%rbx)
	jne	.LBB2_14
	jmp	.LBB2_11
.LBB2_5:
	movl	$1, 196(%rbx)
	cmpl	$0, 20(%rbx)
	jne	.LBB2_14
	jmp	.LBB2_11
.LBB2_8:
	cmpl	$0, 72(%rbx)
	je	.LBB2_82
# BB#9:
	movl	$0, 196(%rbx)
.LBB2_10:
	cmpl	$0, 20(%rbx)
	jne	.LBB2_14
.LBB2_11:
	cmpl	$0, 32(%rbx)
	jne	.LBB2_14
# BB#12:
	cmpl	$0, 192(%rbx)
	movl	$200, %eax
	movl	$50, %ecx
	cmoveq	%rax, %rcx
	movq	%r14, %rax
	cqto
	idivq	%rcx
	testq	%rdx, %rdx
	jne	.LBB2_14
# BB#13:
	movl	16(%rbx), %edi
	movq	176(%rbx), %rdx
	movl	188(%rbx), %ecx
	movq	%r14, %rsi
	callq	timestatus
.LBB2_14:
	cmpl	$0, 272(%rbx)
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r12, 80(%rsp)          # 8-byte Spill
	je	.LBB2_49
# BB#15:
	movslq	200(%rbx), %r12
	testq	%r12, %r12
	movsd	lame_encode_frame.ms_ratio-8(,%r12,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	jle	.LBB2_39
# BB#16:                                # %.preheader132.lr.ph
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movl	%r15d, 8(%rsp)          # 4-byte Spill
	movl	204(%rbx), %edx
	movdqa	16(%rsp), %xmm0
	movq	%xmm0, 72(%rsp)         # 8-byte Folded Spill
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movq	%xmm0, 64(%rsp)         # 8-byte Folded Spill
	xorl	%r14d, %r14d
	movl	$l3_side+72, %r15d
	movl	$l3_side+312, %r13d
	.p2align	4, 0x90
.LBB2_17:                               # %.preheader132
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_26 Depth 2
                                        #     Child Loop BB2_28 Depth 2
                                        #     Child Loop BB2_32 Depth 2
                                        #     Child Loop BB2_36 Depth 2
	testl	%edx, %edx
	jle	.LBB2_29
# BB#18:                                # %.lr.ph141
                                        #   in Loop: Header=BB2_17 Depth=1
	leaq	(%r14,%r14,8), %rax
	shlq	$6, %rax
	addq	$304, %rax              # imm = 0x130
	movslq	%edx, %r10
	cmpl	$4, %edx
	jae	.LBB2_20
# BB#19:                                #   in Loop: Header=BB2_17 Depth=1
	xorl	%edx, %edx
	jmp	.LBB2_28
	.p2align	4, 0x90
.LBB2_20:                               # %min.iters.checked
                                        #   in Loop: Header=BB2_17 Depth=1
	movq	%r10, %rdx
	andq	$-4, %rdx
	je	.LBB2_23
# BB#21:                                # %vector.body.preheader
                                        #   in Loop: Header=BB2_17 Depth=1
	leaq	-4(%rdx), %rdi
	movq	%rdi, %rsi
	shrq	$2, %rsi
	btl	$2, %edi
	jb	.LBB2_24
# BB#22:                                # %vector.body.prol
                                        #   in Loop: Header=BB2_17 Depth=1
	movdqa	32(%rsp), %xmm0
	movq	72(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,2), %r8
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,2), %rbx
	movd	%xmm0, %rdi
	leaq	(%rdi,%rax,2), %r9
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %rdi
	leaq	(%rdi,%rax,2), %rdi
	movd	%rbx, %xmm0
	movd	%r8, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movd	%r9, %xmm0
	movd	%rdi, %xmm2
	punpcklqdq	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0]
	movdqa	%xmm1, 18160(%rsp)
	movdqa	%xmm0, 18176(%rsp)
	movl	$4, %ebx
	testq	%rsi, %rsi
	jne	.LBB2_25
	jmp	.LBB2_27
.LBB2_23:                               #   in Loop: Header=BB2_17 Depth=1
	xorl	%edx, %edx
	jmp	.LBB2_28
.LBB2_24:                               #   in Loop: Header=BB2_17 Depth=1
	xorl	%ebx, %ebx
	testq	%rsi, %rsi
	je	.LBB2_27
.LBB2_25:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB2_17 Depth=1
	movq	%rdx, %rsi
	subq	%rbx, %rsi
	leaq	64(%rsp), %rcx
	leaq	(%rcx,%rbx,8), %rdi
	leaq	18208(%rsp), %rcx
	leaq	(%rcx,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB2_26:                               # %vector.body
                                        #   Parent Loop BB2_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqa	-48(%rdi), %xmm0
	movdqa	-32(%rdi), %xmm1
	movd	%xmm0, %rbp
	leaq	(%rbp,%rax,2), %r8
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %rcx
	leaq	(%rcx,%rax,2), %rcx
	movd	%xmm1, %rbp
	leaq	(%rbp,%rax,2), %r9
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	movd	%xmm0, %rbp
	leaq	(%rbp,%rax,2), %rbp
	movd	%r8, %xmm0
	movd	%rcx, %xmm1
	punpcklqdq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movd	%r9, %xmm1
	movd	%rbp, %xmm2
	punpcklqdq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	movdqa	%xmm0, -48(%rbx)
	movdqa	%xmm1, -32(%rbx)
	movdqa	-16(%rdi), %xmm0
	movdqa	(%rdi), %xmm1
	movd	%xmm0, %rcx
	leaq	(%rcx,%rax,2), %r8
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %rbp
	leaq	(%rbp,%rax,2), %rbp
	movd	%xmm1, %rcx
	leaq	(%rcx,%rax,2), %r9
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	movd	%xmm0, %rcx
	leaq	(%rcx,%rax,2), %rcx
	movd	%r8, %xmm0
	movd	%rbp, %xmm1
	punpcklqdq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movd	%r9, %xmm1
	movd	%rcx, %xmm2
	punpcklqdq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	movdqa	%xmm0, -16(%rbx)
	movdqa	%xmm1, (%rbx)
	addq	$64, %rdi
	addq	$64, %rbx
	addq	$-8, %rsi
	jne	.LBB2_26
.LBB2_27:                               # %middle.block
                                        #   in Loop: Header=BB2_17 Depth=1
	cmpq	%rdx, %r10
	movq	32(%rsp), %rbx          # 8-byte Reload
	je	.LBB2_29
	.p2align	4, 0x90
.LBB2_28:                               # %scalar.ph
                                        #   Parent Loop BB2_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax), %rcx
	addq	16(%rsp,%rdx,8), %rcx
	movq	%rcx, 18160(%rsp,%rdx,8)
	incq	%rdx
	cmpq	%r10, %rdx
	jl	.LBB2_28
.LBB2_29:                               # %._crit_edge142
                                        #   in Loop: Header=BB2_17 Depth=1
	leaq	lame_encode_frame.ms_ratio(,%r14,8), %rcx
	leaq	lame_encode_frame.ms_ener_ratio(,%r14,8), %r9
	movq	%r14, %rax
	shlq	$4, %rax
	leaq	96(%rsp,%rax), %r10
	leaq	128(%rsp,%rax), %rax
	subq	$8, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	leaq	18168(%rsp), %rsi
	movl	%r14d, %edx
	leaq	56(%rsp), %r8
	movq	%rbx, %rbp
	leaq	5048(%rsp), %rbx
	pushq	%rbx
	movq	%rbp, %rbx
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	leaq	14288(%rsp), %rax
	pushq	%rax
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	leaq	1176(%rsp), %rax
	pushq	%rax
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	callq	L3psycho_anal
	addq	$48, %rsp
.Lcfi39:
	.cfi_adjust_cfa_offset -48
	movslq	204(%rbx), %rdx
	testq	%rdx, %rdx
	jle	.LBB2_37
# BB#30:                                # %.lr.ph145
                                        #   in Loop: Header=BB2_17 Depth=1
	leaq	-1(%rdx), %rcx
	movq	%rdx, %rsi
	andq	$3, %rsi
	je	.LBB2_33
# BB#31:                                # %.prol.preheader177
                                        #   in Loop: Header=BB2_17 Depth=1
	movq	%r15, %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_32:                               #   Parent Loop BB2_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	5040(%rsp,%rax,4), %ebp
	movl	%ebp, (%rdi)
	incq	%rax
	addq	$120, %rdi
	cmpq	%rax, %rsi
	jne	.LBB2_32
	jmp	.LBB2_34
	.p2align	4, 0x90
.LBB2_33:                               #   in Loop: Header=BB2_17 Depth=1
	xorl	%eax, %eax
.LBB2_34:                               # %.prol.loopexit178
                                        #   in Loop: Header=BB2_17 Depth=1
	cmpq	$3, %rcx
	jb	.LBB2_37
# BB#35:                                # %.lr.ph145.new
                                        #   in Loop: Header=BB2_17 Depth=1
	imulq	$120, %rax, %rcx
	addq	%r13, %rcx
	.p2align	4, 0x90
.LBB2_36:                               #   Parent Loop BB2_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	5040(%rsp,%rax,4), %esi
	movl	%esi, -240(%rcx)
	movl	5044(%rsp,%rax,4), %esi
	movl	%esi, -120(%rcx)
	movl	5048(%rsp,%rax,4), %esi
	movl	%esi, (%rcx)
	movl	5052(%rsp,%rax,4), %esi
	movl	%esi, 120(%rcx)
	addq	$4, %rax
	addq	$480, %rcx              # imm = 0x1E0
	cmpq	%rdx, %rax
	jl	.LBB2_36
.LBB2_37:                               # %._crit_edge146
                                        #   in Loop: Header=BB2_17 Depth=1
	incq	%r14
	movslq	200(%rbx), %r12
	addq	$240, %r15
	addq	$240, %r13
	cmpq	%r12, %r14
	jl	.LBB2_17
# BB#38:
	movl	8(%rsp), %r15d          # 4-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
.LBB2_39:                               # %._crit_edge148
	leaq	1136(%rsp), %r14
	testl	%r12d, %r12d
	jle	.LBB2_62
.LBB2_40:                               # %.preheader.lr.ph
	movslq	204(%rbx), %rcx
	testq	%rcx, %rcx
	jle	.LBB2_62
# BB#41:                                # %.preheader.us.preheader
	movslq	%r12d, %r8
	movl	%ecx, %edx
	andl	$1, %edx
	xorl	%esi, %esi
	movl	$l3_side+188, %edi
	.p2align	4, 0x90
.LBB2_42:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_46 Depth 2
	testq	%rdx, %rdx
	jne	.LBB2_44
# BB#43:                                #   in Loop: Header=BB2_42 Depth=1
	xorl	%ebp, %ebp
	cmpl	$1, %ecx
	jne	.LBB2_45
	jmp	.LBB2_47
	.p2align	4, 0x90
.LBB2_44:                               #   in Loop: Header=BB2_42 Depth=1
	imulq	$240, %rsi, %rbp
	movl	$0, l3_side+76(%rbp)
	xorl	%ebx, %ebx
	cmpl	$0, l3_side+72(%rbp)
	setne	%bl
	movl	%ebx, l3_side+68(%rbp)
	movl	$1, %ebp
	cmpl	$1, %ecx
	je	.LBB2_47
.LBB2_45:                               # %.preheader.us.new
                                        #   in Loop: Header=BB2_42 Depth=1
	imulq	$120, %rbp, %rbx
	addq	%rdi, %rbx
	.p2align	4, 0x90
.LBB2_46:                               #   Parent Loop BB2_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, -112(%rbx)
	xorl	%eax, %eax
	cmpl	$0, -116(%rbx)
	setne	%al
	movl	%eax, -120(%rbx)
	movl	$0, 8(%rbx)
	xorl	%eax, %eax
	cmpl	$0, 4(%rbx)
	setne	%al
	movl	%eax, (%rbx)
	addq	$2, %rbp
	addq	$240, %rbx
	cmpq	%rcx, %rbp
	jl	.LBB2_46
.LBB2_47:                               # %._crit_edge.us
                                        #   in Loop: Header=BB2_42 Depth=1
	incq	%rsi
	addq	$240, %rdi
	cmpq	%r8, %rsi
	jl	.LBB2_42
# BB#48:
	movq	32(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB2_62
.LBB2_49:                               # %.preheader131
	movslq	200(%rbx), %r12
	testq	%r12, %r12
	jle	.LBB2_61
# BB#50:                                # %.preheader130.lr.ph
	movslq	204(%rbx), %rcx
	testq	%rcx, %rcx
	leaq	1136(%rsp), %r14
	jle	.LBB2_80
# BB#51:                                # %.preheader130.us.preheader
	leaq	-1(%rcx), %r8
	movl	%ecx, %esi
	andl	$3, %esi
	leaq	96(%rsp), %rax
	xorl	%r11d, %r11d
	movl	$l3_side+72, %r10d
	movl	$l3_side+432, %r9d
	movabsq	$4649368480934526976, %rbx # imm = 0x4085E00000000000
	.p2align	4, 0x90
.LBB2_52:                               # %.preheader130.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_54 Depth 2
                                        #     Child Loop BB2_58 Depth 2
	testq	%rsi, %rsi
	je	.LBB2_55
# BB#53:                                # %.prol.preheader171
                                        #   in Loop: Header=BB2_52 Depth=1
	movq	%r10, %rbp
	movq	%rax, %rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_54:                               #   Parent Loop BB2_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, (%rbp)
	movq	%rbx, (%rdi)
	incq	%rdx
	addq	$8, %rdi
	addq	$120, %rbp
	cmpq	%rdx, %rsi
	jne	.LBB2_54
	jmp	.LBB2_56
	.p2align	4, 0x90
.LBB2_55:                               #   in Loop: Header=BB2_52 Depth=1
	xorl	%edx, %edx
.LBB2_56:                               # %.prol.loopexit172
                                        #   in Loop: Header=BB2_52 Depth=1
	cmpq	$3, %r8
	jb	.LBB2_59
# BB#57:                                # %.preheader130.us.new
                                        #   in Loop: Header=BB2_52 Depth=1
	imulq	$120, %rdx, %rbp
	addq	%r9, %rbp
	.p2align	4, 0x90
.LBB2_58:                               #   Parent Loop BB2_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, -360(%rbp)
	movq	%rbx, (%rax,%rdx,8)
	movl	$0, -240(%rbp)
	movq	%rbx, 8(%rax,%rdx,8)
	movl	$0, -120(%rbp)
	movq	%rbx, 16(%rax,%rdx,8)
	movl	$0, (%rbp)
	movq	%rbx, 24(%rax,%rdx,8)
	addq	$4, %rdx
	addq	$480, %rbp              # imm = 0x1E0
	cmpq	%rcx, %rdx
	jl	.LBB2_58
.LBB2_59:                               # %._crit_edge138.us
                                        #   in Loop: Header=BB2_52 Depth=1
	incq	%r11
	addq	$16, %rax
	addq	$240, %r10
	addq	$240, %r9
	cmpq	%r12, %r11
	jl	.LBB2_52
# BB#60:
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	32(%rsp), %rbx          # 8-byte Reload
	testl	%r12d, %r12d
	jg	.LBB2_40
	jmp	.LBB2_62
.LBB2_61:
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	leaq	1136(%rsp), %r14
.LBB2_62:                               # %._crit_edge135
	movq	16(%rsp), %rsi
	leaq	18160(%rsp), %rcx
	movl	$l3_side, %r8d
	movq	%rbx, %rdi
	movq	80(%rsp), %rdx          # 8-byte Reload
	callq	mdct_sub48
	xorl	%eax, %eax
	cmpl	$1, 36(%rbx)
	sete	%cl
	jne	.LBB2_65
# BB#63:
	movl	l3_side+72(%rip), %eax
	cmpl	l3_side+192(%rip), %eax
	jne	.LBB2_66
# BB#64:
	movl	l3_side+312(%rip), %eax
	cmpl	l3_side+432(%rip), %eax
	sete	%al
	jmp	.LBB2_67
.LBB2_65:
	movb	%cl, %al
	testl	%eax, %eax
	jne	.LBB2_68
	jmp	.LBB2_70
.LBB2_66:
	xorl	%eax, %eax
.LBB2_67:
	movzbl	%al, %eax
	testl	%eax, %eax
	je	.LBB2_70
.LBB2_68:
	movsd	lame_encode_frame.ms_ratio(%rip), %xmm0 # xmm0 = mem[0],zero
	addsd	lame_encode_frame.ms_ratio+8(%rip), %xmm0
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	addsd	48(%rsp), %xmm1
	mulsd	.LCPI2_7(%rip), %xmm1
	movsd	.LCPI2_8(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB2_70
# BB#69:
	movl	$2, 228(%rbx)
.LBB2_70:
	cmpl	$0, 44(%rbx)
	je	.LBB2_72
# BB#71:
	movl	$2, 228(%rbx)
	movb	$1, %al
	jmp	.LBB2_73
.LBB2_72:                               # %._crit_edge
	cmpl	$2, 228(%rbx)
	sete	%al
.LBB2_73:
	testb	%al, %al
	leaq	128(%rsp), %rax
	leaq	96(%rsp), %rsi
	cmovneq	%rax, %rsi
	leaq	14256(%rsp), %rax
	cmovneq	%rax, %r14
	cmpl	$0, 88(%rbx)
	je	.LBB2_75
# BB#74:
	leaq	5040(%rsp), %rax
	leaq	18160(%rsp), %rcx
	movl	$lame_encode_frame.ms_ratio, %edx
	movl	$l3_side, %r9d
	movq	%r14, %r8
	movq	%rbx, %rdi
	leaq	160(%rsp), %rbp
	pushq	%rbp
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	callq	VBR_iteration_loop
	addq	$16, %rsp
.Lcfi42:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_76
.LBB2_75:
	leaq	5040(%rsp), %rax
	leaq	18160(%rsp), %rcx
	movl	$lame_encode_frame.ms_ratio, %edx
	movl	$l3_side, %r9d
	movq	%r14, %r8
	movq	%rbx, %rdi
	leaq	160(%rsp), %rbp
	pushq	%rbp
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	callq	iteration_loop
	addq	$16, %rsp
.Lcfi45:
	.cfi_adjust_cfa_offset -16
.LBB2_76:
	leaq	12(%rsp), %rsi
	leaq	92(%rsp), %rdx
	movq	%rbx, %rdi
	callq	getframebits
	movl	12(%rsp), %esi
	leaq	5040(%rsp), %rdx
	leaq	160(%rsp), %r8
	movl	$l3_side, %ecx
	movl	$bs, %r9d
	movq	%rbx, %rdi
	callq	III_format_bitstream
	movq	bs+40(%rip), %rdx
	movq	lame_encode_frame.sentBits(%rip), %rax
	subq	%rax, %rdx
	movq	%rdx, lame_encode_frame.frameBits(%rip)
	movq	%rdx, %r8
	andq	$7, %r8
	jne	.LBB2_81
.LBB2_77:
	addq	%rdx, %rax
	movq	%rax, lame_encode_frame.sentBits(%rip)
	movl	$bs, %edx
	movq	%r13, %rdi
	movl	%r15d, %esi
	callq	copy_buffer
	movl	%eax, %ebp
	cmpl	$0, 24(%rbx)
	je	.LBB2_79
# BB#78:
	movq	lame_encode_frame.sentBits(%rip), %rdi
	shrq	$3, %rdi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	AddVbrFrame
.LBB2_79:
	incq	168(%rbx)
	movl	%ebp, %eax
	addq	$36600, %rsp            # imm = 0x8EF8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_80:
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	jmp	.LBB2_40
.LBB2_81:
	movq	stderr(%rip), %rdi
	movq	%rdx, %rcx
	shrq	$3, %rcx
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	lame_encode_frame.frameBits(%rip), %rdx
	movq	lame_encode_frame.sentBits(%rip), %rax
	jmp	.LBB2_77
.LBB2_82:
	movsd	lame_encode_frame.frac_SpF(%rip), %xmm0 # xmm0 = mem[0],zero
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jne	.LBB2_83
	jnp	.LBB2_10
.LBB2_83:
	movsd	lame_encode_frame.slot_lag(%rip), %xmm1 # xmm1 = mem[0],zero
	movsd	.LCPI2_5(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm1
	jbe	.LBB2_85
# BB#84:
	subsd	%xmm0, %xmm1
	movsd	%xmm1, lame_encode_frame.slot_lag(%rip)
	jmp	.LBB2_4
.LBB2_85:
	movl	$1, 196(%rbx)
	movsd	.LCPI2_6(%rip), %xmm2   # xmm2 = mem[0],zero
	subsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm1
	movsd	%xmm1, lame_encode_frame.slot_lag(%rip)
	cmpl	$0, 20(%rbx)
	jne	.LBB2_14
	jmp	.LBB2_11
.Lfunc_end2:
	.size	lame_encode_frame, .Lfunc_end2-lame_encode_frame
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4602678819172646912     # double 0.5
.LCPI3_2:
	.quad	-4616189618054758400    # double -1
.LCPI3_3:
	.quad	4547007122018943789     # double 1.0E-4
.LCPI3_4:
	.quad	4607182418800017408     # double 1
.LCPI3_5:
	.quad	-4611686018427387904    # double -2
.LCPI3_6:
	.quad	4618441417868443648     # double 6
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_1:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	fill_buffer_resample
	.p2align	4, 0x90
	.type	fill_buffer_resample,@function
fill_buffer_resample:                   # @fill_buffer_resample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 128
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movl	%r8d, %r15d
	movq	%rcx, %r12
	movl	%edx, %ebp
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movslq	128(%rsp), %rbx
	cmpq	$0, 168(%rdi)
	je	.LBB3_1
.LBB3_3:                                # %.thread
	movl	$0, fill_buffer_resample.init(,%rbx,4)
	jmp	.LBB3_4
.LBB3_1:
	cmpl	$0, fill_buffer_resample.init(,%rbx,4)
	jne	.LBB3_4
# BB#2:
	movl	$1, fill_buffer_resample.init(,%rbx,4)
	movq	$0, fill_buffer_resample.itime(,%rbx,8)
	leaq	(%rbx,%rbx), %rax
	movw	$0, fill_buffer_resample.inbuf_old+8(%rax,%rax,4)
	movq	$0, fill_buffer_resample.inbuf_old(%rax,%rax,4)
	cmpq	$0, 168(%rdi)
	jne	.LBB3_3
.LBB3_4:                                # %.thread152
	movss	216(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	cvtss2sd	%xmm0, %xmm1
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	addsd	%xmm1, %xmm0
	callq	floor
	movsd	fill_buffer_resample.itime(,%rbx,8), %xmm2 # xmm2 = mem[0],zero
	testl	%ebp, %ebp
	jle	.LBB3_5
# BB#6:                                 # %.lr.ph
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movapd	16(%rsp), %xmm1         # 16-byte Reload
	subsd	%xmm0, %xmm1
	andpd	.LCPI3_1(%rip), %xmm1
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	movslq	%ebp, %r13
	xorl	%ebp, %ebp
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	leaq	(%rbx,%rbx,4), %rbx
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_7:                                # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	mulss	4(%rsp), %xmm0          # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	subsd	%xmm2, %xmm0
	callq	floor
	cvttsd2si	%xmm0, %edx
	leal	2(%rdx), %r14d
	cmpl	%r15d, %r14d
	jge	.LBB3_22
# BB#8:                                 #   in Loop: Header=BB3_7 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	64(%rsp), %xmm7         # 8-byte Reload
                                        # xmm7 = mem[0],zero
	subsd	%xmm0, %xmm7
	testl	%edx, %edx
	js	.LBB3_10
# BB#9:                                 # %.thread135
                                        #   in Loop: Header=BB3_7 Depth=1
	movslq	%edx, %rax
	movzwl	(%r12,%rax,2), %eax
.LBB3_11:                               #   in Loop: Header=BB3_7 Depth=1
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	movslq	%edx, %rcx
	leaq	2(%r12,%rcx,2), %rcx
	jmp	.LBB3_12
	.p2align	4, 0x90
.LBB3_10:                               #   in Loop: Header=BB3_7 Depth=1
	movslq	%edx, %rcx
	addq	%rcx, %rcx
	movzwl	fill_buffer_resample.inbuf_old+10(%rcx,%rbx,2), %eax
	cmpl	$-1, %edx
	je	.LBB3_11
# BB#25:                                #   in Loop: Header=BB3_7 Depth=1
	leaq	fill_buffer_resample.inbuf_old+12(%rcx,%rbx,2), %rcx
	movaps	16(%rsp), %xmm0         # 16-byte Reload
.LBB3_12:                               #   in Loop: Header=BB3_7 Depth=1
	movapd	%xmm7, %xmm1
	addsd	.LCPI3_2(%rip), %xmm1
	movzwl	(%rcx), %ecx
	movsd	.LCPI3_3(%rip), %xmm2   # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB3_14
# BB#13:                                #   in Loop: Header=BB3_7 Depth=1
	movswl	%cx, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	mulsd	%xmm0, %xmm7
	cwtl
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm7
	addsd	.LCPI3_0(%rip), %xmm7
	movapd	%xmm7, %xmm0
	callq	floor
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	cvttsd2si	%xmm0, %eax
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_14:                               #   in Loop: Header=BB3_7 Depth=1
	movapd	%xmm7, %xmm2
	addsd	.LCPI3_4(%rip), %xmm2
	movapd	%xmm7, %xmm0
	addsd	.LCPI3_5(%rip), %xmm0
	testl	%edx, %edx
	jle	.LBB3_17
# BB#15:                                # %.thread136
                                        #   in Loop: Header=BB3_7 Depth=1
	movslq	%edx, %rdx
	movswl	-2(%r12,%rdx,2), %esi
	jmp	.LBB3_16
.LBB3_17:                               #   in Loop: Header=BB3_7 Depth=1
	movslq	%edx, %rdi
	addq	%rdi, %rdi
	movswl	fill_buffer_resample.inbuf_old+8(%rdi,%rbx,2), %esi
	cmpl	$-3, %edx
	jg	.LBB3_16
# BB#18:                                #   in Loop: Header=BB3_7 Depth=1
	leaq	fill_buffer_resample.inbuf_old+14(%rdi,%rbx,2), %rdx
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_16:                               #   in Loop: Header=BB3_7 Depth=1
	movslq	%r14d, %rdx
	leaq	(%r12,%rdx,2), %rdx
.LBB3_19:                               #   in Loop: Header=BB3_7 Depth=1
	negl	%esi
	cvtsi2sdl	%esi, %xmm4
	mulsd	%xmm7, %xmm4
	mulsd	%xmm1, %xmm4
	mulsd	%xmm0, %xmm4
	movsd	.LCPI3_6(%rip), %xmm6   # xmm6 = mem[0],zero
	divsd	%xmm6, %xmm4
	movsd	.LCPI3_0(%rip), %xmm3   # xmm3 = mem[0],zero
	movapd	%xmm3, %xmm5
	addsd	%xmm5, %xmm4
	cwtl
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm2, %xmm3
	mulsd	%xmm1, %xmm3
	mulsd	%xmm0, %xmm3
	mulsd	%xmm5, %xmm3
	addsd	%xmm4, %xmm3
	movswl	%cx, %eax
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%eax, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm7, %xmm4
	mulsd	%xmm0, %xmm4
	mulsd	%xmm5, %xmm4
	subsd	%xmm4, %xmm3
	movswl	(%rdx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm2, %xmm0
	mulsd	%xmm7, %xmm0
	mulsd	%xmm1, %xmm0
	divsd	%xmm6, %xmm0
	addsd	%xmm3, %xmm0
	callq	floor
	cvttsd2si	%xmm0, %ecx
	movw	$32767, %ax             # imm = 0x7FFF
	cmpl	$32767, %ecx            # imm = 0x7FFF
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	jg	.LBB3_21
# BB#20:                                #   in Loop: Header=BB3_7 Depth=1
	cmpl	$-32767, %ecx           # imm = 0x8001
	movw	$-32767, %ax            # imm = 0x8001
	cmovlew	%ax, %cx
	movw	%cx, %ax
.LBB3_21:                               # %.critedge
                                        #   in Loop: Header=BB3_7 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
	movw	%ax, (%rcx,%rbp,2)
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB3_7
	jmp	.LBB3_23
.LBB3_5:
	movl	$2, %r14d
	xorl	%ebp, %ebp
	jmp	.LBB3_24
.LBB3_22:                               # %.._crit_edge.loopexit_crit_edge
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB3_23:                               # %._crit_edge.loopexit
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
.LBB3_24:                               # %._crit_edge
	cmpl	%r15d, %r14d
	cmovgl	%r15d, %r14d
	movl	%r14d, (%r13)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r14d, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ebp, %xmm1
	mulss	4(%rsp), %xmm1          # 4-byte Folded Reload
	subss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	addsd	%xmm0, %xmm2
	movsd	%xmm2, fill_buffer_resample.itime(,%rbx,8)
	leal	-5(%r14), %eax
	cltq
	movzwl	(%r12,%rax,2), %eax
	addq	%rbx, %rbx
	movw	%ax, fill_buffer_resample.inbuf_old(%rbx,%rbx,4)
	leal	-4(%r14), %eax
	cltq
	movzwl	(%r12,%rax,2), %eax
	movw	%ax, fill_buffer_resample.inbuf_old+2(%rbx,%rbx,4)
	leal	-3(%r14), %eax
	cltq
	movzwl	(%r12,%rax,2), %eax
	movw	%ax, fill_buffer_resample.inbuf_old+4(%rbx,%rbx,4)
	leal	-2(%r14), %eax
	cltq
	movzwl	(%r12,%rax,2), %eax
	movw	%ax, fill_buffer_resample.inbuf_old+6(%rbx,%rbx,4)
	decl	%r14d
	movslq	%r14d, %rax
	movzwl	(%r12,%rax,2), %eax
	movw	%ax, fill_buffer_resample.inbuf_old+8(%rbx,%rbx,4)
	movl	%ebp, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	fill_buffer_resample, .Lfunc_end3-fill_buffer_resample
	.cfi_endproc

	.globl	fill_buffer
	.p2align	4, 0x90
	.type	fill_buffer,@function
fill_buffer:                            # @fill_buffer
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 16
.Lcfi60:
	.cfi_offset %rbx, -16
	cmpl	%r8d, %edx
	cmovlel	%edx, %r8d
	movslq	%r8d, %rbx
	leaq	(%rbx,%rbx), %rdx
	movq	%rsi, %rdi
	movq	%rcx, %rsi
	callq	memcpy
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	fill_buffer, .Lfunc_end4-fill_buffer
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1065353216              # float 1
	.text
	.globl	lame_encode_buffer
	.p2align	4, 0x90
	.type	lame_encode_buffer,@function
lame_encode_buffer:                     # @lame_encode_buffer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi67:
	.cfi_def_cfa_offset 176
.Lcfi68:
	.cfi_offset %rbx, -56
.Lcfi69:
	.cfi_offset %r12, -48
.Lcfi70:
	.cfi_offset %r13, -40
.Lcfi71:
	.cfi_offset %r14, -32
.Lcfi72:
	.cfi_offset %r15, -24
.Lcfi73:
	.cfi_offset %rbp, -16
	movl	%r9d, 52(%rsp)          # 4-byte Spill
	movq	%r8, %r15
	movl	%ecx, %ebx
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	%r12, 80(%rsp)
	movq	%r14, 88(%rsp)
	movl	$752, %eax              # imm = 0x2F0
	addl	188(%r13), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movq	168(%r13), %rax
	testq	%rax, %rax
	jne	.LBB5_3
# BB#1:
	testb	$1, lame_encode_buffer.frame_buffered(%rip)
	jne	.LBB5_3
# BB#2:
	movl	$mfbuf, %edi
	xorl	%esi, %esi
	movl	$12224, %edx            # imm = 0x2FC0
	callq	memset
	movb	$1, lame_encode_buffer.frame_buffered(%rip)
	movl	$1088, mf_samples_to_encode(%rip) # imm = 0x440
	movl	$752, mf_size(%rip)     # imm = 0x2F0
	movq	168(%r13), %rax
.LBB5_3:
	cmpq	$1, %rax
	jne	.LBB5_5
# BB#4:
	movb	$0, lame_encode_buffer.frame_buffered(%rip)
.LBB5_5:
	cmpl	$2, 8(%r13)
	jne	.LBB5_23
# BB#6:
	cmpl	$1, 204(%r13)
	jne	.LBB5_23
# BB#7:
	testl	%ebx, %ebx
	jle	.LBB5_23
# BB#8:                                 # %.lr.ph95.preheader
	movl	%ebx, %ecx
	movl	%ecx, %eax
	cmpl	$8, %ecx
	jae	.LBB5_10
# BB#9:
	xorl	%ecx, %ecx
	jmp	.LBB5_18
.LBB5_10:                               # %min.iters.checked
	movl	%ecx, %edx
	andl	$7, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB5_14
# BB#11:                                # %vector.memcheck
	leaq	(%r14,%rax,2), %rsi
	cmpq	%r12, %rsi
	jbe	.LBB5_15
# BB#12:                                # %vector.memcheck
	leaq	(%r12,%rax,2), %rsi
	cmpq	%r14, %rsi
	jbe	.LBB5_15
.LBB5_14:
	xorl	%ecx, %ecx
	jmp	.LBB5_18
.LBB5_15:                               # %vector.body.preheader
	xorps	%xmm0, %xmm0
	movq	%rcx, %rsi
	movq	%r14, %rdi
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB5_16:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %xmm1           # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	8(%rbp), %xmm2          # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	movq	(%rdi), %xmm3           # xmm3 = mem[0],zero
	punpcklwd	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm3
	movq	8(%rdi), %xmm4          # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	paddd	%xmm2, %xmm4
	paddd	%xmm1, %xmm3
	movdqa	%xmm3, %xmm1
	psrld	$31, %xmm1
	paddd	%xmm3, %xmm1
	psrad	$1, %xmm1
	movdqa	%xmm4, %xmm2
	psrld	$31, %xmm2
	paddd	%xmm4, %xmm2
	psrad	$1, %xmm2
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm2, %xmm1
	movdqu	%xmm1, (%rbp)
	movups	%xmm0, (%rdi)
	addq	$16, %rbp
	addq	$16, %rdi
	addq	$-8, %rsi
	jne	.LBB5_16
# BB#17:                                # %middle.block
	testl	%edx, %edx
	je	.LBB5_23
.LBB5_18:                               # %.lr.ph95.preheader148
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	testb	$1, %sil
	movq	%rcx, %rsi
	je	.LBB5_20
# BB#19:                                # %.lr.ph95.prol
	movswl	(%r12,%rcx,2), %esi
	movswl	(%r14,%rcx,2), %edi
	addl	%esi, %edi
	movl	%edi, %esi
	shrl	$31, %esi
	addl	%edi, %esi
	shrl	%esi
	movw	%si, (%r12,%rcx,2)
	movw	$0, (%r14,%rcx,2)
	leaq	1(%rcx), %rsi
.LBB5_20:                               # %.lr.ph95.prol.loopexit
	cmpq	%rcx, %rdx
	je	.LBB5_23
# BB#21:                                # %.lr.ph95.preheader148.new
	subq	%rsi, %rax
	leaq	2(%r12,%rsi,2), %rcx
	leaq	2(%r14,%rsi,2), %rdx
	.p2align	4, 0x90
.LBB5_22:                               # %.lr.ph95
                                        # =>This Inner Loop Header: Depth=1
	movswl	-2(%rcx), %esi
	movswl	-2(%rdx), %edi
	addl	%esi, %edi
	movl	%edi, %esi
	shrl	$31, %esi
	addl	%edi, %esi
	shrl	%esi
	movw	%si, -2(%rcx)
	movw	$0, -2(%rdx)
	movswl	(%rcx), %esi
	movswl	(%rdx), %edi
	addl	%esi, %edi
	movl	%edi, %esi
	shrl	$31, %esi
	addl	%edi, %esi
	shrl	%esi
	movw	%si, (%rcx)
	movw	$0, (%rdx)
	addq	$4, %rcx
	addq	$4, %rdx
	addq	$-2, %rax
	jne	.LBB5_22
.LBB5_23:                               # %.preheader78
	movl	%ebx, %r12d
	testl	%r12d, %r12d
	jle	.LBB5_55
# BB#24:                                # %.lr.ph92
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movl	204(%r13), %ecx
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r13, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_25:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_27 Depth 2
                                        #     Child Loop BB5_36 Depth 2
                                        #       Child Loop BB5_44 Depth 3
                                        #       Child Loop BB5_48 Depth 3
                                        #       Child Loop BB5_51 Depth 3
	movl	$0, 20(%rsp)
	testl	%ecx, %ecx
	movl	$0, %r14d
	movl	$0, %eax
	jle	.LBB5_31
# BB#26:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB5_25 Depth=1
	movl	$mfbuf, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_27:                               # %.lr.ph
                                        #   Parent Loop BB5_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	216(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movslq	mf_size(%rip), %rax
	leaq	(%rbp,%rax,2), %rsi
	movl	188(%r13), %r14d
	movq	80(%rsp,%rbx,8), %r15
	ucomiss	.LCPI5_0(%rip), %xmm0
	jne	.LBB5_28
	jnp	.LBB5_29
.LBB5_28:                               #   in Loop: Header=BB5_27 Depth=2
	movl	%ebx, (%rsp)
	movq	%r13, %rdi
	movl	%r14d, %edx
	movq	%r15, %rcx
	movl	%r12d, %r8d
	leaq	20(%rsp), %r9
	callq	fill_buffer_resample
	movl	20(%rsp), %r14d
	jmp	.LBB5_30
	.p2align	4, 0x90
.LBB5_29:                               #   in Loop: Header=BB5_27 Depth=2
	cmpl	%r12d, %r14d
	cmovgl	%r12d, %r14d
	movl	%r12d, %r13d
	movslq	%r14d, %r12
	leaq	(%r12,%r12), %rdx
	movq	%rsi, %rdi
	movq	%r15, %rsi
	callq	memcpy
	movl	%r12d, 20(%rsp)
	movl	%r13d, %r12d
	movq	72(%rsp), %r13          # 8-byte Reload
	movl	%r14d, %eax
.LBB5_30:                               #   in Loop: Header=BB5_27 Depth=2
	movslq	%r14d, %rcx
	leaq	(%r15,%rcx,2), %rcx
	movq	%rcx, 80(%rsp,%rbx,8)
	incq	%rbx
	movslq	204(%r13), %rcx
	addq	$6112, %rbp             # imm = 0x17E0
	cmpq	%rcx, %rbx
	jl	.LBB5_27
.LBB5_31:                               # %._crit_edge
                                        #   in Loop: Header=BB5_25 Depth=1
	movl	mf_size(%rip), %edx
	addl	%eax, %edx
	movl	%edx, mf_size(%rip)
	addl	%eax, mf_samples_to_encode(%rip)
	cmpl	56(%rsp), %edx          # 4-byte Folded Reload
	jl	.LBB5_54
# BB#32:                                #   in Loop: Header=BB5_25 Depth=1
	movl	$mfbuf, %esi
	movl	$mfbuf+6112, %edx
	movq	%r13, %rdi
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r8
	movl	52(%rsp), %r9d          # 4-byte Reload
	callq	lame_encode_frame
	cmpl	$-1, %eax
	je	.LBB5_56
# BB#33:                                #   in Loop: Header=BB5_25 Depth=1
	movslq	%eax, %rcx
	addq	%rcx, %rbx
	movq	24(%rsp), %rcx          # 8-byte Reload
	addl	%eax, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movslq	188(%r13), %rax
	movl	mf_size(%rip), %r11d
	subl	%eax, %r11d
	movl	%r11d, mf_size(%rip)
	subl	%eax, mf_samples_to_encode(%rip)
	movslq	204(%r13), %rcx
	testq	%rcx, %rcx
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	jle	.LBB5_54
# BB#34:                                #   in Loop: Header=BB5_25 Depth=1
	testl	%r11d, %r11d
	jle	.LBB5_54
# BB#35:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB5_25 Depth=1
	movl	%r12d, 48(%rsp)         # 4-byte Spill
	movl	%r11d, %edx
	leaq	(%rax,%rdx), %rsi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	leaq	-1(%rdx), %rsi
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movl	%r11d, %esi
	andl	$15, %esi
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	subq	%rsi, %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movl	$mfbuf+6, %r13d
	movl	$mfbuf+4, %edx
	movl	$mfbuf, %r12d
	movl	$mfbuf+16, %r9d
	xorl	%r8d, %r8d
	movl	%r11d, 60(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB5_36:                               # %.preheader.us
                                        #   Parent Loop BB5_25 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_44 Depth 3
                                        #       Child Loop BB5_48 Depth 3
                                        #       Child Loop BB5_51 Depth 3
	cmpl	$16, %r11d
	jae	.LBB5_38
# BB#37:                                #   in Loop: Header=BB5_36 Depth=2
	xorl	%r10d, %r10d
	jmp	.LBB5_46
	.p2align	4, 0x90
.LBB5_38:                               # %min.iters.checked119
                                        #   in Loop: Header=BB5_36 Depth=2
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB5_42
# BB#39:                                # %vector.memcheck136
                                        #   in Loop: Header=BB5_36 Depth=2
	imulq	$6112, %r8, %rsi        # imm = 0x17E0
	leaq	mfbuf(%rsi), %rbp
	movq	104(%rsp), %rdi         # 8-byte Reload
	leaq	mfbuf(%rsi,%rdi,2), %rbx
	cmpq	%rbx, %rbp
	jae	.LBB5_43
# BB#40:                                # %vector.memcheck136
                                        #   in Loop: Header=BB5_36 Depth=2
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	mfbuf(%rsi,%rdi,2), %rbp
	leaq	mfbuf(%rsi,%rax,2), %rsi
	cmpq	%rbp, %rsi
	jae	.LBB5_43
.LBB5_42:                               #   in Loop: Header=BB5_36 Depth=2
	xorl	%r10d, %r10d
	jmp	.LBB5_46
.LBB5_43:                               # %vector.body115.preheader
                                        #   in Loop: Header=BB5_36 Depth=2
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%r9, %rbp
	.p2align	4, 0x90
.LBB5_44:                               # %vector.body115
                                        #   Parent Loop BB5_25 Depth=1
                                        #     Parent Loop BB5_36 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-16(%rbp,%rax,2), %xmm0
	movdqu	(%rbp,%rax,2), %xmm1
	movaps	%xmm0, -16(%rbp)
	movdqa	%xmm1, (%rbp)
	addq	$32, %rbp
	addq	$-16, %rsi
	jne	.LBB5_44
# BB#45:                                # %middle.block116
                                        #   in Loop: Header=BB5_36 Depth=2
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	movq	32(%rsp), %r10          # 8-byte Reload
	je	.LBB5_52
	.p2align	4, 0x90
.LBB5_46:                               # %scalar.ph117.preheader
                                        #   in Loop: Header=BB5_36 Depth=2
	movq	40(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	subl	%r10d, %esi
	movq	112(%rsp), %rbx         # 8-byte Reload
	subq	%r10, %rbx
	andq	$3, %rsi
	je	.LBB5_49
# BB#47:                                # %scalar.ph117.prol.preheader
                                        #   in Loop: Header=BB5_36 Depth=2
	leaq	(%r12,%r10,2), %r11
	leaq	(%rax,%r10), %rbp
	leaq	(%r12,%rbp,2), %r15
	negq	%rsi
	.p2align	4, 0x90
.LBB5_48:                               # %scalar.ph117.prol
                                        #   Parent Loop BB5_25 Depth=1
                                        #     Parent Loop BB5_36 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	(%r15), %ebp
	movw	%bp, (%r11)
	incq	%r10
	addq	$2, %r11
	addq	$2, %r15
	incq	%rsi
	jne	.LBB5_48
.LBB5_49:                               # %scalar.ph117.prol.loopexit
                                        #   in Loop: Header=BB5_36 Depth=2
	cmpq	$3, %rbx
	movl	60(%rsp), %r11d         # 4-byte Reload
	jb	.LBB5_52
# BB#50:                                # %scalar.ph117.preheader.new
                                        #   in Loop: Header=BB5_36 Depth=2
	movq	40(%rsp), %rsi          # 8-byte Reload
	subq	%r10, %rsi
	leaq	(%rdx,%r10,2), %rbx
	addq	%rax, %r10
	leaq	(%r13,%r10,2), %rbp
	.p2align	4, 0x90
.LBB5_51:                               # %scalar.ph117
                                        #   Parent Loop BB5_25 Depth=1
                                        #     Parent Loop BB5_36 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	-6(%rbp), %edi
	movw	%di, -4(%rbx)
	movzwl	-4(%rbp), %edi
	movw	%di, -2(%rbx)
	movzwl	-2(%rbp), %edi
	movw	%di, (%rbx)
	movzwl	(%rbp), %edi
	movw	%di, 2(%rbx)
	addq	$8, %rbx
	addq	$8, %rbp
	addq	$-4, %rsi
	jne	.LBB5_51
.LBB5_52:                               # %._crit_edge86.us
                                        #   in Loop: Header=BB5_36 Depth=2
	incq	%r8
	addq	$6112, %r9              # imm = 0x17E0
	addq	$6112, %r12             # imm = 0x17E0
	addq	$6112, %rdx             # imm = 0x17E0
	addq	$6112, %r13             # imm = 0x17E0
	cmpq	%rcx, %r8
	jl	.LBB5_36
# BB#53:                                #   in Loop: Header=BB5_25 Depth=1
	movl	48(%rsp), %r12d         # 4-byte Reload
	movq	72(%rsp), %r13          # 8-byte Reload
.LBB5_54:                               # %.thread
                                        #   in Loop: Header=BB5_25 Depth=1
	subl	%r14d, %r12d
	testl	%r12d, %r12d
	jg	.LBB5_25
	jmp	.LBB5_58
.LBB5_55:
	xorl	%eax, %eax
	jmp	.LBB5_57
.LBB5_56:
	movl	$-1, %eax
.LBB5_57:                               # %.loopexit
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB5_58:                               # %.loopexit
	movq	24(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	lame_encode_buffer, .Lfunc_end5-lame_encode_buffer
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1065353216              # float 1
	.text
	.globl	lame_encode_buffer_interleaved
	.p2align	4, 0x90
	.type	lame_encode_buffer_interleaved,@function
lame_encode_buffer_interleaved:         # @lame_encode_buffer_interleaved
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi77:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi78:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi80:
	.cfi_def_cfa_offset 144
.Lcfi81:
	.cfi_offset %rbx, -56
.Lcfi82:
	.cfi_offset %r12, -48
.Lcfi83:
	.cfi_offset %r13, -40
.Lcfi84:
	.cfi_offset %r14, -32
.Lcfi85:
	.cfi_offset %r15, -24
.Lcfi86:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	movq	%rcx, %r14
	movl	%edx, %r12d
	movq	%rsi, %rbx
	movq	%rdi, %r9
	cmpl	$1, 8(%r9)
	jne	.LBB6_2
# BB#1:
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%rbx, %rsi
	movl	%r12d, %ecx
	movq	%r14, %r8
	movl	%r15d, %r9d
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	lame_encode_buffer      # TAILCALL
.LBB6_2:
	movd	216(%r9), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI6_0(%rip), %xmm0
	jne	.LBB6_3
	jnp	.LBB6_8
.LBB6_3:
	movq	%r9, %rbp
	movq	%r14, (%rsp)            # 8-byte Spill
	movslq	%r12d, %r13
	addq	%r13, %r13
	movq	%r13, %rdi
	callq	malloc
	movq	%rax, %r14
	movq	%r13, %rdi
	callq	malloc
	movq	%rax, %r13
	testq	%r14, %r14
	movl	$-1, %eax
	je	.LBB6_75
# BB#4:
	testq	%r13, %r13
	je	.LBB6_75
# BB#5:                                 # %.preheader
	testl	%r12d, %r12d
	movq	%rbp, %rdi
	jle	.LBB6_29
# BB#6:                                 # %.lr.ph.preheader
	movl	%r12d, %eax
	cmpl	$8, %r12d
	jb	.LBB6_26
# BB#18:                                # %min.iters.checked225
	movl	%r12d, %r8d
	andl	$7, %r8d
	movq	%rax, %r9
	subq	%r8, %r9
	je	.LBB6_26
# BB#19:                                # %vector.body221.preheader
	movq	%r9, %rsi
	movq	%r13, %rcx
	movq	%r14, %rbp
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB6_20:                               # %vector.body221
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rdx), %xmm0
	movdqu	16(%rdx), %xmm1
	pshuflw	$232, %xmm1, %xmm2      # xmm2 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm0, %xmm3      # xmm3 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpcklqdq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	pshuflw	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshuflw	$177, %xmm1, %xmm1      # xmm1 = xmm1[1,0,3,2,4,5,6,7]
	pshuflw	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$177, %xmm0, %xmm0      # xmm0 = xmm0[1,0,3,2,4,5,6,7]
	punpcklqdq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movdqu	%xmm3, (%rbp)
	movdqu	%xmm0, (%rcx)
	addq	$32, %rdx
	addq	$16, %rbp
	addq	$16, %rcx
	addq	$-8, %rsi
	jne	.LBB6_20
# BB#21:                                # %middle.block222
	testl	%r8d, %r8d
	jne	.LBB6_27
	jmp	.LBB6_29
.LBB6_8:
	movl	$752, %r13d             # imm = 0x2F0
	addl	188(%r9), %r13d
	movq	168(%r9), %rax
	testq	%rax, %rax
	jne	.LBB6_11
# BB#9:
	testb	$1, lame_encode_buffer_interleaved.frame_buffered(%rip)
	jne	.LBB6_11
# BB#10:
	movl	$mfbuf, %edi
	xorl	%esi, %esi
	movl	$12224, %edx            # imm = 0x2FC0
	movq	%r9, %rbp
	callq	memset
	movq	%rbp, %r9
	movb	$1, lame_encode_buffer_interleaved.frame_buffered(%rip)
	movl	$1088, mf_samples_to_encode(%rip) # imm = 0x440
	movl	$752, mf_size(%rip)     # imm = 0x2F0
	movq	168(%r9), %rax
.LBB6_11:
	cmpq	$1, %rax
	jne	.LBB6_13
# BB#12:
	movb	$0, lame_encode_buffer_interleaved.frame_buffered(%rip)
.LBB6_13:
	cmpl	$2, 8(%r9)
	jne	.LBB6_33
# BB#14:
	cmpl	$1, 204(%r9)
	jne	.LBB6_33
# BB#15:
	testl	%r12d, %r12d
	jle	.LBB6_33
# BB#16:                                # %.lr.ph135.preheader
	movl	%r12d, %eax
	cmpl	$8, %r12d
	jb	.LBB6_30
# BB#22:                                # %min.iters.checked
	movl	%r12d, %edx
	andl	$7, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB6_30
# BB#23:                                # %vector.body.preheader
	pxor	%xmm0, %xmm0
	movq	%rcx, %rsi
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB6_24:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rdi), %xmm1
	movdqu	16(%rdi), %xmm2
	pshuflw	$232, %xmm2, %xmm3      # xmm3 = xmm2[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshuflw	$232, %xmm1, %xmm4      # xmm4 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpcklqdq	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0]
	punpcklwd	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1],xmm3[2],xmm4[2],xmm3[3],xmm4[3]
	psrad	$16, %xmm3
	punpckhwd	%xmm4, %xmm4    # xmm4 = xmm4[4,4,5,5,6,6,7,7]
	psrad	$16, %xmm4
	pshuflw	$231, %xmm2, %xmm2      # xmm2 = xmm2[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$177, %xmm2, %xmm2      # xmm2 = xmm2[1,0,3,2,4,5,6,7]
	pshuflw	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshuflw	$177, %xmm1, %xmm1      # xmm1 = xmm1[1,0,3,2,4,5,6,7]
	punpcklqdq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	punpcklwd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1],xmm2[2],xmm1[2],xmm2[3],xmm1[3]
	psrad	$16, %xmm2
	punpckhwd	%xmm1, %xmm1    # xmm1 = xmm1[4,4,5,5,6,6,7,7]
	psrad	$16, %xmm1
	paddd	%xmm4, %xmm1
	paddd	%xmm3, %xmm2
	movdqa	%xmm2, %xmm3
	psrld	$31, %xmm3
	paddd	%xmm2, %xmm3
	psrad	$1, %xmm3
	movdqa	%xmm1, %xmm2
	psrld	$31, %xmm2
	paddd	%xmm1, %xmm2
	psrad	$1, %xmm2
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	pslld	$16, %xmm3
	psrad	$16, %xmm3
	packssdw	%xmm2, %xmm3
	movdqa	%xmm3, %xmm1
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	punpckhwd	%xmm0, %xmm3    # xmm3 = xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	movdqu	%xmm3, 16(%rdi)
	movdqu	%xmm1, (%rdi)
	addq	$32, %rdi
	addq	$-8, %rsi
	jne	.LBB6_24
# BB#25:                                # %middle.block
	testl	%edx, %edx
	jne	.LBB6_31
	jmp	.LBB6_33
.LBB6_26:
	xorl	%r9d, %r9d
.LBB6_27:                               # %.lr.ph.preheader240
	leaq	(%r14,%r9,2), %rdx
	leaq	(%r13,%r9,2), %rsi
	leaq	2(%rbx,%r9,4), %rbp
	subq	%r9, %rax
	.p2align	4, 0x90
.LBB6_28:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-2(%rbp), %ecx
	movw	%cx, (%rdx)
	movzwl	(%rbp), %ecx
	movw	%cx, (%rsi)
	addq	$2, %rdx
	addq	$2, %rsi
	addq	$4, %rbp
	decq	%rax
	jne	.LBB6_28
.LBB6_29:                               # %._crit_edge
	movq	%r14, %rsi
	movq	%r13, %rdx
	movl	%r12d, %ecx
	movq	(%rsp), %r8             # 8-byte Reload
	movl	%r15d, %r9d
	callq	lame_encode_buffer
	movl	%eax, %ebx
	movq	%r14, %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movl	%ebx, %eax
	jmp	.LBB6_76
.LBB6_30:
	xorl	%ecx, %ecx
.LBB6_31:                               # %.lr.ph135.preheader242
	leaq	2(%rbx,%rcx,4), %rdx
	subq	%rcx, %rax
	.p2align	4, 0x90
.LBB6_32:                               # %.lr.ph135
                                        # =>This Inner Loop Header: Depth=1
	movswl	-2(%rdx), %ecx
	movswl	(%rdx), %esi
	addl	%ecx, %esi
	movl	%esi, %ecx
	shrl	$31, %ecx
	addl	%esi, %ecx
	shrl	%ecx
	movw	%cx, -2(%rdx)
	movw	$0, (%rdx)
	addq	$4, %rdx
	decq	%rax
	jne	.LBB6_32
.LBB6_33:                               # %.thread.outer.preheader
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%r13d, 44(%rsp)         # 4-byte Spill
	testl	%r12d, %r12d
	jg	.LBB6_36
	jmp	.LBB6_74
.LBB6_44:                               # %vector.body189.preheader
                                        #   in Loop: Header=BB6_36 Depth=1
	leaq	-8(%rsi), %rcx
	movq	%rcx, %rdi
	shrq	$3, %rdi
	btl	$3, %ecx
	jb	.LBB6_46
# BB#45:                                # %vector.body189.prol
                                        #   in Loop: Header=BB6_36 Depth=1
	movdqu	(%rbx), %xmm0
	movdqu	16(%rbx), %xmm1
	pshuflw	$232, %xmm1, %xmm2      # xmm2 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm0, %xmm3      # xmm3 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpcklqdq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	pshuflw	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshuflw	$177, %xmm1, %xmm1      # xmm1 = xmm1[1,0,3,2,4,5,6,7]
	pshuflw	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$177, %xmm0, %xmm0      # xmm0 = xmm0[1,0,3,2,4,5,6,7]
	punpcklqdq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movdqu	%xmm3, mfbuf(%r8,%r8)
	movdqu	%xmm0, mfbuf+6112(%r8,%r8)
	movl	$8, %ecx
	testq	%rdi, %rdi
	jne	.LBB6_47
	jmp	.LBB6_49
.LBB6_46:                               #   in Loop: Header=BB6_36 Depth=1
	xorl	%ecx, %ecx
	testq	%rdi, %rdi
	je	.LBB6_49
.LBB6_47:                               # %vector.body189.preheader.new
                                        #   in Loop: Header=BB6_36 Depth=1
	movq	%rsi, %rdi
	subq	%rcx, %rdi
	leaq	32(%rbx,%rcx,4), %rbp
	addq	%r8, %rcx
	leaq	mfbuf+6128(%rcx,%rcx), %rcx
	.p2align	4, 0x90
.LBB6_48:                               # %vector.body189
                                        #   Parent Loop BB6_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-32(%rbp), %xmm0
	movdqu	-16(%rbp), %xmm1
	pshuflw	$232, %xmm1, %xmm2      # xmm2 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm0, %xmm3      # xmm3 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpcklqdq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	pshuflw	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshuflw	$177, %xmm1, %xmm1      # xmm1 = xmm1[1,0,3,2,4,5,6,7]
	pshuflw	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$177, %xmm0, %xmm0      # xmm0 = xmm0[1,0,3,2,4,5,6,7]
	punpcklqdq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movdqu	%xmm3, -6128(%rcx)
	movdqu	%xmm0, -16(%rcx)
	movdqu	(%rbp), %xmm0
	movdqu	16(%rbp), %xmm1
	pshuflw	$232, %xmm1, %xmm2      # xmm2 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm0, %xmm3      # xmm3 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpcklqdq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	pshuflw	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshuflw	$177, %xmm1, %xmm1      # xmm1 = xmm1[1,0,3,2,4,5,6,7]
	pshuflw	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$177, %xmm0, %xmm0      # xmm0 = xmm0[1,0,3,2,4,5,6,7]
	punpcklqdq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movdqu	%xmm3, -6112(%rcx)
	movdqu	%xmm0, (%rcx)
	addq	$64, %rbp
	addq	$32, %rcx
	addq	$-16, %rdi
	jne	.LBB6_48
.LBB6_49:                               # %middle.block190
                                        #   in Loop: Header=BB6_36 Depth=1
	cmpq	%rsi, %rdx
	je	.LBB6_52
	jmp	.LBB6_50
.LBB6_34:                               #   in Loop: Header=BB6_36 Depth=1
	movl	36(%rsp), %r15d         # 4-byte Reload
	movq	(%rsp), %r14            # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	.p2align	4, 0x90
.LBB6_35:                               # %.thread
                                        #   in Loop: Header=BB6_36 Depth=1
	testl	%r12d, %r12d
	jle	.LBB6_74
.LBB6_36:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_48 Depth 2
                                        #     Child Loop BB6_51 Depth 2
                                        #     Child Loop BB6_57 Depth 2
                                        #       Child Loop BB6_65 Depth 3
                                        #       Child Loop BB6_69 Depth 3
                                        #       Child Loop BB6_72 Depth 3
	movl	188(%r9), %eax
	cmpl	%r12d, %eax
	cmovgl	%r12d, %eax
	movslq	mf_size(%rip), %r8
	testl	%eax, %eax
	jle	.LBB6_52
# BB#37:                                # %.lr.ph128
                                        #   in Loop: Header=BB6_36 Depth=1
	movslq	%eax, %rdx
	cmpl	$8, %eax
	jae	.LBB6_39
# BB#38:                                #   in Loop: Header=BB6_36 Depth=1
	xorl	%esi, %esi
	jmp	.LBB6_50
	.p2align	4, 0x90
.LBB6_39:                               # %min.iters.checked193
                                        #   in Loop: Header=BB6_36 Depth=1
	movq	%rdx, %rsi
	andq	$-8, %rsi
	je	.LBB6_43
# BB#40:                                # %vector.memcheck209
                                        #   in Loop: Header=BB6_36 Depth=1
	leaq	mfbuf(%r8,%r8), %rcx
	leaq	(%rbx,%rdx,4), %rdi
	cmpq	%rdi, %rcx
	jae	.LBB6_44
# BB#41:                                # %vector.memcheck209
                                        #   in Loop: Header=BB6_36 Depth=1
	leaq	(%r8,%rdx), %rcx
	leaq	mfbuf+6112(%rcx,%rcx), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB6_44
.LBB6_43:                               #   in Loop: Header=BB6_36 Depth=1
	xorl	%esi, %esi
.LBB6_50:                               # %scalar.ph191.preheader
                                        #   in Loop: Header=BB6_36 Depth=1
	leaq	(%rsi,%r8), %rcx
	leaq	mfbuf(%rcx,%rcx), %rcx
	.p2align	4, 0x90
.LBB6_51:                               # %scalar.ph191
                                        #   Parent Loop BB6_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rbx,%rsi,4), %edi
	movw	%di, (%rcx)
	movzwl	2(%rbx,%rsi,4), %edi
	movw	%di, 6112(%rcx)
	incq	%rsi
	addq	$2, %rcx
	cmpq	%rdx, %rsi
	jl	.LBB6_51
.LBB6_52:                               # %._crit_edge129
                                        #   in Loop: Header=BB6_36 Depth=1
	leal	(%rax,%rax), %ecx
	movslq	%ecx, %rcx
	leaq	(%rbx,%rcx,2), %rbx
	subl	%eax, %r12d
	addl	%eax, %r8d
	movl	%r8d, mf_size(%rip)
	addl	%eax, mf_samples_to_encode(%rip)
	cmpl	%r13d, %r8d
	jl	.LBB6_35
# BB#53:                                #   in Loop: Header=BB6_36 Depth=1
	movl	$mfbuf, %esi
	movl	$mfbuf+6112, %edx
	movq	%r9, %rdi
	movq	%r14, %r8
	movq	%r9, %rbp
	movl	%r15d, %r9d
	callq	lame_encode_frame
	cmpl	$-1, %eax
	je	.LBB6_77
# BB#54:                                #   in Loop: Header=BB6_36 Depth=1
	movslq	%eax, %rcx
	addq	%rcx, %r14
	movq	8(%rsp), %rcx           # 8-byte Reload
	addl	%eax, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movslq	188(%rbp), %rax
	movl	mf_size(%rip), %r11d
	subl	%eax, %r11d
	movl	%r11d, mf_size(%rip)
	subl	%eax, mf_samples_to_encode(%rip)
	movslq	204(%rbp), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	testq	%rcx, %rcx
	movq	%rbp, %r9
	jle	.LBB6_35
# BB#55:                                #   in Loop: Header=BB6_36 Depth=1
	testl	%r11d, %r11d
	jle	.LBB6_35
# BB#56:                                # %.preheader120.us.preheader
                                        #   in Loop: Header=BB6_36 Depth=1
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%r14, (%rsp)            # 8-byte Spill
	movl	%r15d, 36(%rsp)         # 4-byte Spill
	movl	%r11d, %ecx
	leaq	(%rax,%rcx), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	leaq	-1(%rcx), %rdx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movl	%r11d, %edx
	andl	$15, %edx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	subq	%rdx, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	$mfbuf+6, %r14d
	movl	$mfbuf+4, %edx
	movl	$mfbuf, %r15d
	movl	$mfbuf+16, %r9d
	xorl	%r8d, %r8d
	movl	%r11d, 40(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB6_57:                               # %.preheader120.us
                                        #   Parent Loop BB6_36 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_65 Depth 3
                                        #       Child Loop BB6_69 Depth 3
                                        #       Child Loop BB6_72 Depth 3
	cmpl	$16, %r11d
	jae	.LBB6_59
# BB#58:                                #   in Loop: Header=BB6_57 Depth=2
	xorl	%r10d, %r10d
	jmp	.LBB6_67
	.p2align	4, 0x90
.LBB6_59:                               # %min.iters.checked167
                                        #   in Loop: Header=BB6_57 Depth=2
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB6_63
# BB#60:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_57 Depth=2
	imulq	$6112, %r8, %rcx        # imm = 0x17E0
	leaq	mfbuf(%rcx), %rsi
	movq	64(%rsp), %rdi          # 8-byte Reload
	leaq	mfbuf(%rcx,%rdi,2), %rbp
	cmpq	%rbp, %rsi
	jae	.LBB6_64
# BB#61:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_57 Depth=2
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	mfbuf(%rcx,%rsi,2), %rsi
	leaq	mfbuf(%rcx,%rax,2), %rcx
	cmpq	%rsi, %rcx
	jae	.LBB6_64
.LBB6_63:                               #   in Loop: Header=BB6_57 Depth=2
	xorl	%r10d, %r10d
	jmp	.LBB6_67
.LBB6_64:                               # %vector.body163.preheader
                                        #   in Loop: Header=BB6_57 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB6_65:                               # %vector.body163
                                        #   Parent Loop BB6_36 Depth=1
                                        #     Parent Loop BB6_57 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	-16(%rsi,%rax,2), %xmm0
	movdqu	(%rsi,%rax,2), %xmm1
	movdqa	%xmm0, -16(%rsi)
	movdqa	%xmm1, (%rsi)
	addq	$32, %rsi
	addq	$-16, %rcx
	jne	.LBB6_65
# BB#66:                                # %middle.block164
                                        #   in Loop: Header=BB6_57 Depth=2
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	je	.LBB6_73
	.p2align	4, 0x90
.LBB6_67:                               # %scalar.ph165.preheader
                                        #   in Loop: Header=BB6_57 Depth=2
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	subl	%r10d, %esi
	movq	72(%rsp), %rbp          # 8-byte Reload
	subq	%r10, %rbp
	andq	$3, %rsi
	je	.LBB6_70
# BB#68:                                # %scalar.ph165.prol.preheader
                                        #   in Loop: Header=BB6_57 Depth=2
	leaq	(%r15,%r10,2), %r11
	leaq	(%rax,%r10), %rcx
	leaq	(%r15,%rcx,2), %r13
	negq	%rsi
	.p2align	4, 0x90
.LBB6_69:                               # %scalar.ph165.prol
                                        #   Parent Loop BB6_36 Depth=1
                                        #     Parent Loop BB6_57 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	(%r13), %ecx
	movw	%cx, (%r11)
	incq	%r10
	addq	$2, %r11
	addq	$2, %r13
	incq	%rsi
	jne	.LBB6_69
.LBB6_70:                               # %scalar.ph165.prol.loopexit
                                        #   in Loop: Header=BB6_57 Depth=2
	cmpq	$3, %rbp
	movl	44(%rsp), %r13d         # 4-byte Reload
	movl	40(%rsp), %r11d         # 4-byte Reload
	jb	.LBB6_73
# BB#71:                                # %scalar.ph165.preheader.new
                                        #   in Loop: Header=BB6_57 Depth=2
	movq	24(%rsp), %rsi          # 8-byte Reload
	subq	%r10, %rsi
	leaq	(%rdx,%r10,2), %rbp
	addq	%rax, %r10
	leaq	(%r14,%r10,2), %rcx
	.p2align	4, 0x90
.LBB6_72:                               # %scalar.ph165
                                        #   Parent Loop BB6_36 Depth=1
                                        #     Parent Loop BB6_57 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	-6(%rcx), %edi
	movw	%di, -4(%rbp)
	movzwl	-4(%rcx), %edi
	movw	%di, -2(%rbp)
	movzwl	-2(%rcx), %edi
	movw	%di, (%rbp)
	movzwl	(%rcx), %edi
	movw	%di, 2(%rbp)
	addq	$8, %rbp
	addq	$8, %rcx
	addq	$-4, %rsi
	jne	.LBB6_72
.LBB6_73:                               # %._crit_edge132.us
                                        #   in Loop: Header=BB6_57 Depth=2
	incq	%r8
	addq	$6112, %r9              # imm = 0x17E0
	addq	$6112, %r15             # imm = 0x17E0
	addq	$6112, %rdx             # imm = 0x17E0
	addq	$6112, %r14             # imm = 0x17E0
	cmpq	80(%rsp), %r8           # 8-byte Folded Reload
	jl	.LBB6_57
	jmp	.LBB6_34
.LBB6_74:
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB6_75:                               # %.loopexit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
.LBB6_76:                               # %.loopexit
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_77:
	movl	$-1, %eax
	jmp	.LBB6_75
.Lfunc_end6:
	.size	lame_encode_buffer_interleaved, .Lfunc_end6-lame_encode_buffer_interleaved
	.cfi_endproc

	.globl	lame_encode
	.p2align	4, 0x90
	.type	lame_encode,@function
lame_encode:                            # @lame_encode
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 16
.Lcfi88:
	.cfi_offset %rbx, -16
	movl	%ecx, %r9d
	movq	%rdx, %rax
	movl	mf_samples_to_encode(%rip), %ebx
	movl	200(%rdi), %ecx
	shll	$6, %ecx
	leal	(%rcx,%rcx,8), %ecx
	leaq	2304(%rsi), %rdx
	movq	%rax, %r8
	callq	lame_encode_buffer
	movl	%ebx, mf_samples_to_encode(%rip)
	popq	%rbx
	retq
.Lfunc_end7:
	.size	lame_encode, .Lfunc_end7-lame_encode
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
.LCPI8_1:
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.text
	.globl	lame_init
	.p2align	4, 0x90
	.type	lame_init,@function
lame_init:                              # @lame_init
	.cfi_startproc
# BB#0:
	movl	$0, 156(%rdi)
	movl	$0, 144(%rdi)
	movl	$0, 148(%rdi)
	movl	$1, 24(%rdi)
	movl	$0, 152(%rdi)
	movq	$0, 168(%rdi)
	movl	$0, 20(%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 72(%rdi)
	movl	$5, 28(%rdi)
	movl	$0, 120(%rdi)
	movl	$0, 256(%rdi)
	movaps	.LCPI8_0(%rip), %xmm1   # xmm1 = [0,0,4294967295,4294967295]
	movups	%xmm1, 104(%rdi)
	movups	%xmm0, 232(%rdi)
	movl	$32, 248(%rdi)
	movl	$-1, 252(%rdi)
	movl	$0, 160(%rdi)
	movl	$1065353216, 216(%rdi)  # imm = 0x3F800000
	movl	$2, 64(%rdi)
	movl	$0, 196(%rdi)
	movl	$0, 124(%rdi)
	movl	$0, 32(%rdi)
	movq	$0, 176(%rdi)
	movaps	.LCPI8_1(%rip), %xmm1   # xmm1 = [0,4,0,0]
	movups	%xmm1, 88(%rdi)
	movl	$1, 208(%rdi)
	movl	$13, 212(%rdi)
	movl	$1, 192(%rdi)
	movl	$1, 36(%rdi)
	movups	%xmm0, 40(%rdi)
	movl	$1, 56(%rdi)
	movl	$0, 68(%rdi)
	movl	$0, 60(%rdi)
	movl	$0, 164(%rdi)
	movl	$44100, 12(%rdi)        # imm = 0xAC44
	movl	$0, 16(%rdi)
	movl	$2, 8(%rdi)
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, (%rdi)
	movups	%xmm0, 128(%rdi)
	movl	$0, id3tag(%rip)
	retq
.Lfunc_end8:
	.size	lame_init, .Lfunc_end8-lame_init
	.cfi_endproc

	.globl	lame_encode_finish
	.p2align	4, 0x90
	.type	lame_encode_finish,@function
lame_encode_finish:                     # @lame_encode_finish
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi92:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi93:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 56
	subq	$4616, %rsp             # imm = 0x1208
.Lcfi95:
	.cfi_def_cfa_offset 4672
.Lcfi96:
	.cfi_offset %rbx, -56
.Lcfi97:
	.cfi_offset %r12, -48
.Lcfi98:
	.cfi_offset %r13, -40
.Lcfi99:
	.cfi_offset %r14, -32
.Lcfi100:
	.cfi_offset %r15, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movq	%rsp, %rdi
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$4608, %edx             # imm = 0x1200
	callq	memset
	movl	mf_samples_to_encode(%rip), %r15d
	testl	%r15d, %r15d
	jle	.LBB9_7
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	movq	%rsp, %r12
	testl	%r14d, %r14d
	je	.LBB9_4
	.p2align	4, 0x90
.LBB9_2:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%r14d, %r9d
	subl	%ebp, %r9d
	movl	200(%r13), %eax
	shll	$6, %eax
	leal	(%rax,%rax,8), %ecx
	movq	%r13, %rdi
	movq	%r12, %rsi
	leaq	2304(%rsp), %rdx
	movq	%rbx, %r8
	callq	lame_encode_buffer
	movl	%r15d, mf_samples_to_encode(%rip)
	cmpl	$-1, %eax
	je	.LBB9_3
# BB#6:                                 #   in Loop: Header=BB9_2 Depth=1
	movslq	%eax, %rcx
	addq	%rcx, %rbx
	addl	%eax, %ebp
	subl	188(%r13), %r15d
	movl	%r15d, mf_samples_to_encode(%rip)
	testl	%r15d, %r15d
	jg	.LBB9_2
	jmp	.LBB9_7
	.p2align	4, 0x90
.LBB9_4:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	200(%r13), %eax
	shll	$6, %eax
	leal	(%rax,%rax,8), %ecx
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	movq	%r12, %rsi
	leaq	2304(%rsp), %rdx
	movq	%rbx, %r8
	callq	lame_encode_buffer
	movl	%r15d, mf_samples_to_encode(%rip)
	cmpl	$-1, %eax
	je	.LBB9_3
# BB#5:                                 #   in Loop: Header=BB9_4 Depth=1
	movslq	%eax, %rcx
	addq	%rcx, %rbx
	addl	%eax, %ebp
	subl	188(%r13), %r15d
	movl	%r15d, mf_samples_to_encode(%rip)
	testl	%r15d, %r15d
	jg	.LBB9_4
.LBB9_7:                                # %._crit_edge
	movq	168(%r13), %rsi
	decq	%rsi
	movq	%rsi, 168(%r13)
	cmpl	$0, 20(%r13)
	jne	.LBB9_10
# BB#8:
	cmpl	$0, 32(%r13)
	je	.LBB9_9
.LBB9_10:
	callq	III_FlushBitstream
	movl	%r14d, %esi
	subl	%ebp, %esi
	testl	%r14d, %r14d
	cmovel	%r14d, %esi
	movl	$bs, %edx
	movq	%rbx, %rdi
	callq	copy_buffer
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB9_3
# BB#11:
	addl	%ebp, %ebx
	movl	$bs, %edi
	callq	desalloc_buffer
	jmp	.LBB9_12
.LBB9_3:                                # %.us-lcssa.us
	movl	$bs, %edi
	callq	desalloc_buffer
	movl	$-1, %ebx
.LBB9_12:
	movl	%ebx, %eax
	addq	$4616, %rsp             # imm = 0x1208
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_9:
	movl	16(%r13), %edi
	movq	176(%r13), %rdx
	movl	188(%r13), %ecx
	callq	timestatus
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stderr(%rip), %rdi
	callq	fflush
	jmp	.LBB9_10
.Lfunc_end9:
	.size	lame_encode_finish, .Lfunc_end9-lame_encode_finish
	.cfi_endproc

	.globl	lame_mp3_tags
	.p2align	4, 0x90
	.type	lame_mp3_tags,@function
lame_mp3_tags:                          # @lame_mp3_tags
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 16
.Lcfi103:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpl	$0, 24(%rbx)
	je	.LBB10_2
# BB#1:
	imull	$100, 92(%rbx), %eax
	cltq
	imulq	$954437177, %rax, %rsi  # imm = 0x38E38E39
	movq	%rsi, %rax
	shrq	$63, %rax
	sarq	$33, %rsi
	addl	%eax, %esi
	movq	136(%rbx), %rdi
	movl	$1, %edx
	subl	192(%rbx), %edx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	PutVbrTag
.LBB10_2:
	cmpl	$0, id3tag(%rip)
	je	.LBB10_3
# BB#4:
	movl	$id3tag, %edi
	callq	id3_buildtag
	movq	136(%rbx), %rdi
	movl	$id3tag, %esi
	popq	%rbx
	jmp	id3_writetag            # TAILCALL
.LBB10_3:
	popq	%rbx
	retq
.Lfunc_end10:
	.size	lame_mp3_tags, .Lfunc_end10-lame_mp3_tags
	.cfi_endproc

	.globl	lame_version
	.p2align	4, 0x90
	.type	lame_version,@function
lame_version:                           # @lame_version
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 16
.Lcfi105:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	callq	get_lame_version
	movl	$20, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	popq	%rbx
	jmp	strncpy                 # TAILCALL
.Lfunc_end11:
	.size	lame_version, .Lfunc_end11-lame_version
	.cfi_endproc

	.type	bs,@object              # @bs
	.local	bs
	.comm	bs,56,8
	.type	l3_side,@object         # @l3_side
	.local	l3_side
	.comm	l3_side,528,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Warning: highpass filter disabled.  highpass frequency to small\n"
	.size	.L.str, 65

	.type	lame_print_config.mode_names,@object # @lame_print_config.mode_names
	.section	.rodata,"a",@progbits
	.p2align	4
lame_print_config.mode_names:
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	.L.str.3
	.quad	.L.str.4
	.size	lame_print_config.mode_names, 32

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"stereo"
	.size	.L.str.1, 7

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"j-stereo"
	.size	.L.str.2, 9

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"dual-ch"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"single-ch"
	.size	.L.str.4, 10

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Autoconverting from stereo to mono. Setting encoding to mono mode.\n"
	.size	.L.str.5, 68

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Resampling:  input=%ikHz  output=%ikHz\n"
	.size	.L.str.6, 40

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Using polyphase highpass filter, transition band: %.0f Hz -  %.0f Hz\n"
	.size	.L.str.7, 70

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Using polyphase lowpass filter,  transition band:  %.0f Hz - %.0f Hz\n"
	.size	.L.str.8, 70

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Analyzing %s \n"
	.size	.L.str.9, 15

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Encoding %s to %s\n"
	.size	.L.str.10, 19

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"stdin"
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"stdout"
	.size	.L.str.13, 7

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Encoding as %.1fkHz VBR(q=%i) %s MPEG%i LayerIII  qval=%i\n"
	.size	.L.str.14, 59

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Encoding as %.1f kHz %d kbps %s MPEG%i LayerIII (%4.1fx)  qval=%i\n"
	.size	.L.str.15, 67

	.type	lame_encode_frame.frameBits,@object # @lame_encode_frame.frameBits
	.local	lame_encode_frame.frameBits
	.comm	lame_encode_frame.frameBits,8,8
	.type	lame_encode_frame.bitsPerSlot,@object # @lame_encode_frame.bitsPerSlot
	.local	lame_encode_frame.bitsPerSlot
	.comm	lame_encode_frame.bitsPerSlot,1,8
	.type	lame_encode_frame.frac_SpF,@object # @lame_encode_frame.frac_SpF
	.local	lame_encode_frame.frac_SpF
	.comm	lame_encode_frame.frac_SpF,8,8
	.type	lame_encode_frame.slot_lag,@object # @lame_encode_frame.slot_lag
	.local	lame_encode_frame.slot_lag
	.comm	lame_encode_frame.slot_lag,8,8
	.type	lame_encode_frame.sentBits,@object # @lame_encode_frame.sentBits
	.local	lame_encode_frame.sentBits
	.comm	lame_encode_frame.sentBits,8,8
	.type	lame_encode_frame.ms_ratio,@object # @lame_encode_frame.ms_ratio
	.local	lame_encode_frame.ms_ratio
	.comm	lame_encode_frame.ms_ratio,16,16
	.type	lame_encode_frame.ms_ener_ratio,@object # @lame_encode_frame.ms_ener_ratio
	.local	lame_encode_frame.ms_ener_ratio
	.comm	lame_encode_frame.ms_ener_ratio,16,16
	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Sent %ld bits = %ld slots plus %ld\n"
	.size	.L.str.16, 36

	.type	fill_buffer_resample.itime,@object # @fill_buffer_resample.itime
	.local	fill_buffer_resample.itime
	.comm	fill_buffer_resample.itime,16,16
	.type	fill_buffer_resample.inbuf_old,@object # @fill_buffer_resample.inbuf_old
	.local	fill_buffer_resample.inbuf_old
	.comm	fill_buffer_resample.inbuf_old,20,16
	.type	fill_buffer_resample.init,@object # @fill_buffer_resample.init
	.local	fill_buffer_resample.init
	.comm	fill_buffer_resample.init,8,4
	.type	lame_encode_buffer.frame_buffered,@object # @lame_encode_buffer.frame_buffered
	.local	lame_encode_buffer.frame_buffered
	.comm	lame_encode_buffer.frame_buffered,1,4
	.type	mfbuf,@object           # @mfbuf
	.local	mfbuf
	.comm	mfbuf,12224,16
	.type	mf_samples_to_encode,@object # @mf_samples_to_encode
	.local	mf_samples_to_encode
	.comm	mf_samples_to_encode,4,4
	.type	mf_size,@object         # @mf_size
	.local	mf_size
	.comm	mf_size,4,4
	.type	lame_encode_buffer_interleaved.frame_buffered,@object # @lame_encode_buffer_interleaved.frame_buffered
	.local	lame_encode_buffer_interleaved.frame_buffered
	.comm	lame_encode_buffer_interleaved.frame_buffered,1,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
