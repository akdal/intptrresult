	.text
	.file	"MethodProps.bc"
	.globl	_Z19SetMethodPropertiesRK7CMethodPKyP8IUnknown
	.p2align	4, 0x90
	.type	_Z19SetMethodPropertiesRK7CMethodPKyP8IUnknown,@function
_Z19SetMethodPropertiesRK7CMethodPKyP8IUnknown: # @_Z19SetMethodPropertiesRK7CMethodPKyP8IUnknown
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi2:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
.Lcfi3:
	.cfi_offset %rbx, -56
.Lcfi4:
	.cfi_offset %r12, -48
.Lcfi5:
	.cfi_offset %r13, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
	movq	%rdx, %rbx
	movq	%rdi, %r15
	movl	$1024, %r13d            # imm = 0x400
	testq	%rsi, %rsi
	je	.LBB0_1
# BB#2:
	movq	(%r15), %rcx
	xorl	%eax, %eax
	cmpq	$196865, %rcx           # imm = 0x30101
	je	.LBB0_4
# BB#3:
	cmpq	$33, %rcx
	movl	$0, %r14d
	jne	.LBB0_9
.LBB0_4:                                # %.preheader122
	movq	(%rsi), %rcx
	movl	$1024, %r13d            # imm = 0x400
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movl	%r13d, %edx
	movb	$1, %r14b
	cmpq	%rcx, %rdx
	jae	.LBB0_9
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=1
	movl	%r13d, %edx
	shrl	%edx
	addl	%edx, %r13d
	cmpq	%rcx, %r13
	jae	.LBB0_9
# BB#7:                                 #   in Loop: Header=BB0_5 Depth=1
	cmpl	$-1073741825, %r13d     # imm = 0xBFFFFFFF
	cmoval	%eax, %edx
	addl	%r13d, %edx
	cmpq	%rcx, %rdx
	jae	.LBB0_8
# BB#87:                                #   in Loop: Header=BB0_5 Depth=1
	movl	%edx, %r13d
	shrl	%r13d
	addl	%r13d, %edx
	cmpq	%rcx, %rdx
	jae	.LBB0_8
# BB#88:                                #   in Loop: Header=BB0_5 Depth=1
	xorl	%r14d, %r14d
	cmpl	$-1073741825, %edx      # imm = 0xBFFFFFFF
	cmoval	%r14d, %r13d
	addl	%edx, %r13d
	cmpl	$-1073741825, %edx      # imm = 0xBFFFFFFF
	jbe	.LBB0_5
	jmp	.LBB0_9
.LBB0_1:
	xorl	%r14d, %r14d
	jmp	.LBB0_9
.LBB0_8:
	movl	%edx, %r13d
.LBB0_9:                                # %.thread
	movslq	20(%r15), %rax
	movq	%rax, -56(%rbp)         # 8-byte Spill
	movq	$0, -72(%rbp)
	movq	(%rbx), %rax
.Ltmp0:
	leaq	-72(%rbp), %rdx
	movl	$IID_ICompressSetCoderProperties, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
.Ltmp1:
# BB#10:
	cmpq	$0, -72(%rbp)
	je	.LBB0_11
# BB#13:
	movq	%rbx, -120(%rbp)        # 8-byte Spill
	movl	$16, %ecx
	movq	-56(%rbp), %rbx         # 8-byte Reload
	movq	%rbx, %rax
	mulq	%rcx
	pushfq
	popq	%rcx
	addq	$8, %rax
	movq	$-1, %rdi
	cmovbq	%rdi, %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, -96(%rbp)
	movq	$4, -80(%rbp)
	movq	$_ZTV13CRecordVectorIjE+16, -104(%rbp)
	pushq	%rcx
	popfq
	cmovnoq	%rax, %rdi
.Ltmp3:
	callq	_Znam
	movq	%rax, %r12
.Ltmp4:
# BB#14:
	movq	%rbx, (%r12)
	leaq	8(%r12), %rax
	movq	%rax, -48(%rbp)         # 8-byte Spill
	testl	%ebx, %ebx
	je	.LBB0_32
# BB#15:
	movq	-56(%rbp), %rdi         # 8-byte Reload
	movq	%rdi, %rax
	shlq	$4, %rax
	leaq	-16(%rax), %rdx
	movl	%edx, %esi
	shrl	$4, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB0_16
# BB#17:                                # %.prol.preheader
	negq	%rsi
	movq	-48(%rbp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_18:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rcx)
	addq	$16, %rcx
	incq	%rsi
	jne	.LBB0_18
	jmp	.LBB0_19
.LBB0_11:
	cmpl	$0, -56(%rbp)           # 4-byte Folded Reload
	je	.LBB0_73
# BB#12:
	movl	$-2147024809, %r14d     # imm = 0x80070057
	movl	$1, %ebx
	jmp	.LBB0_74
.LBB0_16:
	movq	-48(%rbp), %rcx         # 8-byte Reload
.LBB0_19:                               # %.prol.loopexit
	cmpq	$112, %rdx
	jb	.LBB0_22
# BB#20:                                # %.new
	addq	-48(%rbp), %rax         # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB0_21:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 32(%rcx)
	movl	$0, 48(%rcx)
	movl	$0, 64(%rcx)
	movl	$0, 80(%rcx)
	movl	$0, 96(%rcx)
	movl	$0, 112(%rcx)
	subq	$-128, %rcx
	cmpq	%rax, %rcx
	jne	.LBB0_21
.LBB0_22:                               # %.preheader121
	testl	%edi, %edi
	jle	.LBB0_32
# BB#23:                                # %.lr.ph
	testb	%r14b, %r14b
	movq	%r15, -112(%rbp)        # 8-byte Spill
	je	.LBB0_44
# BB#24:                                # %.lr.ph.split.us.preheader
	xorl	%ecx, %ecx
	movq	-48(%rbp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_25:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rax
	movq	%rcx, -128(%rbp)        # 8-byte Spill
	movq	(%rax,%rcx,8), %rbx
	movl	(%rbx), %r15d
.Ltmp6:
	leaq	-104(%rbp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp7:
# BB#26:                                #   in Loop: Header=BB0_25 Depth=1
	movq	-88(%rbp), %rax
	movslq	-92(%rbp), %rcx
	movl	%r15d, (%rax,%rcx,4)
	incl	-92(%rbp)
	leaq	8(%rbx), %rsi
.Ltmp9:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp10:
# BB#27:                                #   in Loop: Header=BB0_25 Depth=1
	cmpl	$1, (%rbx)
	jne	.LBB0_31
# BB#28:                                #   in Loop: Header=BB0_25 Depth=1
	movzwl	(%r14), %eax
	cmpl	$19, %eax
	jne	.LBB0_31
# BB#29:                                #   in Loop: Header=BB0_25 Depth=1
	cmpl	8(%r14), %r13d
	jae	.LBB0_31
# BB#30:                                #   in Loop: Header=BB0_25 Depth=1
	movl	%r13d, 8(%r14)
	.p2align	4, 0x90
.LBB0_31:                               #   in Loop: Header=BB0_25 Depth=1
	movq	-128(%rbp), %rcx        # 8-byte Reload
	incq	%rcx
	addq	$16, %r14
	cmpq	-56(%rbp), %rcx         # 8-byte Folded Reload
	movq	-112(%rbp), %r15        # 8-byte Reload
	jl	.LBB0_25
	jmp	.LBB0_32
.LBB0_44:                               # %.lr.ph.split.preheader
	xorl	%ebx, %ebx
	movq	-48(%rbp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_45:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rax
	movq	(%rax,%rbx,8), %r14
	movl	(%r14), %r15d
.Ltmp12:
	leaq	-104(%rbp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp13:
# BB#46:                                #   in Loop: Header=BB0_45 Depth=1
	movq	-88(%rbp), %rax
	movslq	-92(%rbp), %rcx
	movl	%r15d, (%rax,%rcx,4)
	incl	-92(%rbp)
	addq	$8, %r14
.Ltmp15:
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp16:
# BB#47:                                #   in Loop: Header=BB0_45 Depth=1
	incq	%rbx
	addq	$16, %r13
	cmpq	-56(%rbp), %rbx         # 8-byte Folded Reload
	movq	-112(%rbp), %r15        # 8-byte Reload
	jl	.LBB0_45
.LBB0_32:                               # %._crit_edge
	movq	$0, -64(%rbp)
	movq	-120(%rbp), %rdi        # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp18:
	leaq	-64(%rbp), %rdx
	movl	$IID_ICompressSetCoderProperties, %esi
	callq	*(%rax)
.Ltmp19:
# BB#33:
	movq	-64(%rbp), %rdi
	movq	(%rdi), %rax
	movq	-88(%rbp), %rsi
.Ltmp20:
	movq	-48(%rbp), %rdx         # 8-byte Reload
	movq	-56(%rbp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	*40(%rax)
	movl	%eax, %r14d
.Ltmp21:
# BB#34:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_36
# BB#35:
	movq	(%rdi), %rax
.Ltmp25:
	callq	*16(%rax)
.Ltmp26:
.LBB0_36:                               # %_ZN9CMyComPtrI27ICompressSetCoderPropertiesED2Ev.exit108
	movq	(%r12), %rbx
	shlq	$4, %rbx
	je	.LBB0_39
	.p2align	4, 0x90
.LBB0_37:                               # %.preheader119
                                        # =>This Inner Loop Header: Depth=1
	leaq	-8(%r12,%rbx), %rdi
.Ltmp39:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp40:
# BB#38:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit100
                                        #   in Loop: Header=BB0_37 Depth=1
	addq	$-16, %rbx
	jne	.LBB0_37
.LBB0_39:                               # %.loopexit120
	movq	%r12, %rdi
	callq	_ZdaPv
.Ltmp47:
	leaq	-104(%rbp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp48:
# BB#40:
	xorl	%ebx, %ebx
	testl	%r14d, %r14d
	setne	%al
	je	.LBB0_73
# BB#41:
	movb	%al, %bl
	jmp	.LBB0_74
.LBB0_73:
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
.LBB0_74:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_76
# BB#75:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB0_76:                               # %_ZN9CMyComPtrI27ICompressSetCoderPropertiesED2Ev.exit97
	testl	%ebx, %ebx
	cmovel	%ebx, %r14d
	movl	%r14d, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_52:
.Ltmp27:
	jmp	.LBB0_53
.LBB0_70:
.Ltmp49:
	jmp	.LBB0_78
.LBB0_71:
.Ltmp5:
	movq	%rax, %r14
	jmp	.LBB0_72
.LBB0_50:
.Ltmp22:
	movq	%rax, %r14
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_54
# BB#51:
	movq	(%rdi), %rax
.Ltmp23:
	callq	*16(%rax)
.Ltmp24:
	jmp	.LBB0_54
.LBB0_77:
.Ltmp2:
.LBB0_78:
	movq	%rax, %r14
	jmp	.LBB0_79
.LBB0_49:                               # %.us-lcssa127
.Ltmp17:
	jmp	.LBB0_53
.LBB0_48:                               # %.us-lcssa
.Ltmp14:
	jmp	.LBB0_53
.LBB0_43:                               # %.us-lcssa127.us
.Ltmp11:
	jmp	.LBB0_53
.LBB0_42:                               # %.us-lcssa.us
.Ltmp8:
.LBB0_53:
	movq	%rax, %r14
.LBB0_54:
	movq	%r14, %rdi
	callq	__cxa_begin_catch
	movq	(%r12), %rbx
	shlq	$4, %rbx
	je	.LBB0_57
	.p2align	4, 0x90
.LBB0_55:                               # %.preheader112
                                        # =>This Inner Loop Header: Depth=1
	leaq	-8(%r12,%rbx), %rdi
.Ltmp28:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp29:
# BB#56:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit104
                                        #   in Loop: Header=BB0_55 Depth=1
	addq	$-16, %rbx
	jne	.LBB0_55
.LBB0_57:                               # %.loopexit113
	movq	%r12, %rdi
	callq	_ZdaPv
.Ltmp34:
	callq	__cxa_rethrow
.Ltmp35:
# BB#86:
.LBB0_63:
.Ltmp36:
	movq	%rax, %r14
	jmp	.LBB0_64
.LBB0_65:
.Ltmp41:
	movq	%rax, %r14
	cmpq	$16, %rbx
	jne	.LBB0_67
	jmp	.LBB0_69
	.p2align	4, 0x90
.LBB0_68:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
                                        #   in Loop: Header=BB0_67 Depth=1
	addq	$-16, %rbx
	cmpq	$16, %rbx
	je	.LBB0_69
.LBB0_67:                               # %.preheader114
                                        # =>This Inner Loop Header: Depth=1
	leaq	-24(%r12,%rbx), %rdi
.Ltmp42:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp43:
	jmp	.LBB0_68
.LBB0_69:                               # %.loopexit118
	movq	%r12, %rdi
	callq	_ZdaPv
	jmp	.LBB0_72
.LBB0_83:                               # %.loopexit.split-lp.loopexit
.Ltmp44:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_58:
.Ltmp30:
	movq	%rax, %r14
	cmpq	$16, %rbx
	jne	.LBB0_60
	jmp	.LBB0_62
	.p2align	4, 0x90
.LBB0_61:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit102
                                        #   in Loop: Header=BB0_60 Depth=1
	addq	$-16, %rbx
	cmpq	$16, %rbx
	je	.LBB0_62
.LBB0_60:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leaq	-24(%r12,%rbx), %rdi
.Ltmp31:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp32:
	jmp	.LBB0_61
.LBB0_62:                               # %.loopexit111
	movq	%r12, %rdi
	callq	_ZdaPv
.LBB0_64:
.Ltmp37:
	callq	__cxa_end_catch
.Ltmp38:
.LBB0_72:
.Ltmp45:
	leaq	-104(%rbp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp46:
.LBB0_79:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_81
# BB#80:
	movq	(%rdi), %rax
.Ltmp50:
	callq	*16(%rax)
.Ltmp51:
.LBB0_81:                               # %_ZN9CMyComPtrI27ICompressSetCoderPropertiesED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_84:                               # %.loopexit.split-lp.loopexit.split-lp
.Ltmp52:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_82:                               # %.loopexit
.Ltmp33:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_Z19SetMethodPropertiesRK7CMethodPKyP8IUnknown, .Lfunc_end0-_Z19SetMethodPropertiesRK7CMethodPKyP8IUnknown
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\200\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\367\001"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	1                       #   On action: 1
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp21-.Ltmp18         #   Call between .Ltmp18 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	1                       #   On action: 1
	.long	.Ltmp25-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin0   #     jumps to .Ltmp27
	.byte	1                       #   On action: 1
	.long	.Ltmp39-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin0   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin0   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp23-.Ltmp48         #   Call between .Ltmp48 and .Ltmp23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp52-.Lfunc_begin0   #     jumps to .Ltmp52
	.byte	1                       #   On action: 1
	.long	.Ltmp24-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp28-.Ltmp24         #   Call between .Ltmp24 and .Ltmp28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin0   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin0   #     jumps to .Ltmp44
	.byte	1                       #   On action: 1
	.long	.Ltmp31-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin0   #     jumps to .Ltmp33
	.byte	1                       #   On action: 1
	.long	.Ltmp37-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp51-.Ltmp37         #   Call between .Ltmp37 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin0   #     jumps to .Ltmp52
	.byte	1                       #   On action: 1
	.long	.Ltmp51-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Lfunc_end0-.Ltmp51     #   Call between .Ltmp51 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.text._ZN13CRecordVectorIjED0Ev,"axG",@progbits,_ZN13CRecordVectorIjED0Ev,comdat
	.weak	_ZN13CRecordVectorIjED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjED0Ev,@function
_ZN13CRecordVectorIjED0Ev:              # @_ZN13CRecordVectorIjED0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp53:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp54:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB2_2:
.Ltmp55:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN13CRecordVectorIjED0Ev, .Lfunc_end2-_ZN13CRecordVectorIjED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp53-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin1   #     jumps to .Ltmp55
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end2-.Ltmp54     #   Call between .Ltmp54 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV13CRecordVectorIjE,@object # @_ZTV13CRecordVectorIjE
	.section	.rodata._ZTV13CRecordVectorIjE,"aG",@progbits,_ZTV13CRecordVectorIjE,comdat
	.weak	_ZTV13CRecordVectorIjE
	.p2align	3
_ZTV13CRecordVectorIjE:
	.quad	0
	.quad	_ZTI13CRecordVectorIjE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIjED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIjE, 40

	.type	_ZTS13CRecordVectorIjE,@object # @_ZTS13CRecordVectorIjE
	.section	.rodata._ZTS13CRecordVectorIjE,"aG",@progbits,_ZTS13CRecordVectorIjE,comdat
	.weak	_ZTS13CRecordVectorIjE
	.p2align	4
_ZTS13CRecordVectorIjE:
	.asciz	"13CRecordVectorIjE"
	.size	_ZTS13CRecordVectorIjE, 19

	.type	_ZTI13CRecordVectorIjE,@object # @_ZTI13CRecordVectorIjE
	.section	.rodata._ZTI13CRecordVectorIjE,"aG",@progbits,_ZTI13CRecordVectorIjE,comdat
	.weak	_ZTI13CRecordVectorIjE
	.p2align	4
_ZTI13CRecordVectorIjE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIjE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIjE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
