	.text
	.file	"adi.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4607182418800017408     # double 1
.LCPI6_1:
	.quad	4562146422526312448     # double 9.765625E-4
.LCPI6_2:
	.quad	4611686018427387904     # double 2
.LCPI6_3:
	.quad	4613937818241073152     # double 3
.LCPI6_5:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_4:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 80
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_30
# BB#1:
	movq	8(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_30
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_30
# BB#3:                                 # %polybench_alloc_data.exit
	movq	8(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_30
# BB#4:                                 # %polybench_alloc_data.exit39
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_30
# BB#5:                                 # %polybench_alloc_data.exit39
	movq	8(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_30
# BB#6:                                 # %polybench_alloc_data.exit41
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_30
# BB#7:                                 # %polybench_alloc_data.exit41
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_30
# BB#8:                                 # %polybench_alloc_data.exit43
	xorl	%eax, %eax
	movsd	.LCPI6_1(%rip), %xmm2   # xmm2 = mem[0],zero
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_9:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_10 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movq	$-1024, %rdx            # imm = 0xFC00
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_10:                               #   Parent Loop BB6_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1025(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	.LCPI6_0(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	movsd	%xmm1, (%r14,%rsi)
	leal	1026(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	.LCPI6_2(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	movsd	%xmm1, (%r12,%rsi)
	leal	1027(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	.LCPI6_3(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	movsd	%xmm1, (%rbx,%rsi)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB6_10
# BB#11:                                #   in Loop: Header=BB6_9 Depth=1
	incq	%rcx
	addq	$8192, %rax             # imm = 0x2000
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB6_9
# BB#12:                                # %init_array.exit
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	kernel_adi
	movsd	.LCPI6_3(%rip), %xmm5   # xmm5 = mem[0],zero
	movsd	.LCPI6_2(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_0(%rip), %xmm2   # xmm2 = mem[0],zero
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_13:                               # %.preheader.i45
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_14 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movq	$-1024, %rdx            # imm = 0xFC00
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_14:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1025(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	movsd	%xmm1, (%r15,%rsi)
	leal	1026(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm4, %xmm1
	mulsd	%xmm3, %xmm1
	movsd	%xmm1, (%r12,%rsi)
	leal	1027(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm5, %xmm1
	mulsd	%xmm3, %xmm1
	movsd	%xmm1, (%rbx,%rsi)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB6_14
# BB#15:                                #   in Loop: Header=BB6_13 Depth=1
	incq	%rcx
	addq	$8192, %rax             # imm = 0x2000
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB6_13
# BB#16:                                # %init_array.exit51
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	kernel_adi
	xorl	%edx, %edx
	movl	$1, %eax
	movapd	.LCPI6_4(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_5(%rip), %xmm3   # xmm3 = mem[0],zero
.LBB6_17:                               # %.preheader.i52
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_18 Depth 2
	movq	%rax, %rsi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_18:                               #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%r14,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r15,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_19
# BB#21:                                #   in Loop: Header=BB6_18 Depth=2
	movsd	(%r14,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r15,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_20
# BB#22:                                #   in Loop: Header=BB6_18 Depth=2
	addq	$2, %rsi
	leaq	2(%rcx), %rdi
	incq	%rcx
	cmpq	$1024, %rcx             # imm = 0x400
	movq	%rdi, %rcx
	jl	.LBB6_18
# BB#23:                                #   in Loop: Header=BB6_17 Depth=1
	incq	%rdx
	addq	$1024, %rax             # imm = 0x400
	cmpq	$1024, %rdx             # imm = 0x400
	jl	.LBB6_17
# BB#24:                                # %check_FP.exit
	movl	$16385, %edi            # imm = 0x4001
	callq	malloc
	movq	%rax, %r13
	movb	$0, 16384(%r13)
	xorl	%eax, %eax
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB6_25:                               # %.preheader.i56
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_26 Depth 2
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbp, %rsi
	movl	$15, %ecx
	.p2align	4, 0x90
.LBB6_26:                               #   Parent Loop BB6_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rdx
	movl	%edx, %eax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -15(%r13,%rcx)
	movb	%al, -14(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$8, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -13(%r13,%rcx)
	movb	%al, -12(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$16, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -11(%r13,%rcx)
	movb	%al, -10(%r13,%rcx)
	movl	%edx, %eax
	shrl	$24, %eax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -9(%r13,%rcx)
	movb	%al, -8(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$32, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -7(%r13,%rcx)
	movb	%al, -6(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$40, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -5(%r13,%rcx)
	movb	%al, -4(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$48, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -3(%r13,%rcx)
	movb	%al, -2(%r13,%rcx)
	shrq	$56, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, -1(%r13,%rcx)
	movb	%dl, (%r13,%rcx)
	addq	$16, %rcx
	addq	$8, %rsi
	cmpq	$16399, %rcx            # imm = 0x400F
	jne	.LBB6_26
# BB#27:                                #   in Loop: Header=BB6_25 Depth=1
	movq	stderr(%rip), %rsi
	movq	%r13, %rdi
	callq	fputs
	movq	16(%rsp), %rax          # 8-byte Reload
	incq	%rax
	addq	$8192, %rbp             # imm = 0x2000
	cmpq	$1024, %rax             # imm = 0x400
	jne	.LBB6_25
# BB#28:                                # %print_array.exit
	movq	%r13, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_29
.LBB6_19:                               # %check_FP.exit.threadsplit
	decq	%rcx
.LBB6_20:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_5(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r8d
	movl	%ecx, %r9d
	callq	fprintf
	movl	$1, %eax
.LBB6_29:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_30:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.p2align	4, 0x90
	.type	kernel_adi,@function
kernel_adi:                             # @kernel_adi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %rsi
	leaq	8380416(%rsi), %rax
	leaq	8388608(%rsi), %rcx
	leaq	8380416(%rdx), %rbp
	leaq	8388608(%rdx), %rbx
	cmpq	%rbx, %rax
	sbbb	%al, %al
	cmpq	%rcx, %rbp
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, -121(%rsp)         # 1-byte Spill
	leaq	8(%rsi), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	leaq	8(%r12), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	leaq	8(%rdx), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	leaq	8176(%rsi), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	leaq	8168(%r12), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	leaq	8168(%rdx), %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	leaq	8192(%rsi), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	leaq	8192(%r12), %rax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	leaq	8192(%rdx), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	leaq	8372224(%rsi), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	leaq	8364032(%r12), %rax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	leaq	8372224(%rdx), %rax
	movq	%rax, -112(%rsp)        # 8-byte Spill
	xorl	%r13d, %r13d
	movq	%r12, -8(%rsp)          # 8-byte Spill
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB7_1:                                # %.preheader9
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_2 Depth 2
                                        #       Child Loop BB7_7 Depth 3
                                        #       Child Loop BB7_5 Depth 3
                                        #     Child Loop BB7_10 Depth 2
                                        #     Child Loop BB7_12 Depth 2
                                        #       Child Loop BB7_13 Depth 3
                                        #     Child Loop BB7_16 Depth 2
                                        #       Child Loop BB7_20 Depth 3
                                        #       Child Loop BB7_22 Depth 3
                                        #     Child Loop BB7_26 Depth 2
                                        #     Child Loop BB7_28 Depth 2
                                        #     Child Loop BB7_30 Depth 2
                                        #       Child Loop BB7_33 Depth 3
                                        #       Child Loop BB7_35 Depth 3
	movq	-40(%rsp), %r14         # 8-byte Reload
	movq	-32(%rsp), %r11         # 8-byte Reload
	movq	-24(%rsp), %r15         # 8-byte Reload
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB7_2:                                # %.lver.check
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_7 Depth 3
                                        #       Child Loop BB7_5 Depth 3
	movq	%r10, %rax
	shlq	$13, %rax
	leaq	(%rsi,%rax), %rcx
	leaq	8192(%rdx,%rax), %rbx
	movsd	(%rdx,%rax), %xmm0      # xmm0 = mem[0],zero
	cmpq	%rbx, %rcx
	jae	.LBB7_6
# BB#3:                                 # %.lver.check
                                        #   in Loop: Header=BB7_2 Depth=2
	leaq	8192(%rsi,%rax), %rcx
	leaq	(%rdx,%rax), %rbx
	cmpq	%rcx, %rbx
	jae	.LBB7_6
# BB#4:                                 # %.ph.lver.orig.preheader
                                        #   in Loop: Header=BB7_2 Depth=2
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_5:                                # %.ph.lver.orig
                                        #   Parent Loop BB7_1 Depth=1
                                        #     Parent Loop BB7_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%r15,%rax,8), %xmm1    # xmm1 = mem[0],zero
	movsd	-8(%r15,%rax,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	(%r11,%rax,8), %xmm2
	divsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	movsd	%xmm1, (%r15,%rax,8)
	movsd	(%r14,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r11,%rax,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	divsd	-8(%r14,%rax,8), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%r14,%rax,8)
	incq	%rax
	cmpq	$1023, %rax             # imm = 0x3FF
	jne	.LBB7_5
	jmp	.LBB7_8
	.p2align	4, 0x90
.LBB7_6:                                # %.ph
                                        #   in Loop: Header=BB7_2 Depth=2
	movsd	(%rsi,%rax), %xmm2      # xmm2 = mem[0],zero
	movsd	(%rdx,%rax), %xmm1      # xmm1 = mem[0],zero
	movl	$1023, %eax             # imm = 0x3FF
	movq	%r14, %rcx
	movq	%r11, %rbx
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB7_7:                                #   Parent Loop BB7_1 Depth=1
                                        #     Parent Loop BB7_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rbp), %xmm3           # xmm3 = mem[0],zero
	mulsd	(%rbx), %xmm2
	divsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm3
	movsd	%xmm3, (%rbp)
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rbx), %xmm2           # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm2
	divsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm0
	movsd	%xmm0, (%rcx)
	addq	$8, %rbp
	addq	$8, %rbx
	addq	$8, %rcx
	decq	%rax
	movapd	%xmm0, %xmm1
	movapd	%xmm3, %xmm2
	jne	.LBB7_7
.LBB7_8:                                #   in Loop: Header=BB7_2 Depth=2
	incq	%r10
	addq	$8192, %r15             # imm = 0x2000
	addq	$8192, %r11             # imm = 0x2000
	addq	$8192, %r14             # imm = 0x2000
	cmpq	$1024, %r10             # imm = 0x400
	jne	.LBB7_2
# BB#9:                                 # %.preheader8.preheader
                                        #   in Loop: Header=BB7_1 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_10:                               # %.preheader8
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	8184(%rsi,%rax), %xmm0  # xmm0 = mem[0],zero
	divsd	8184(%rdx,%rax), %xmm0
	movsd	%xmm0, 8184(%rsi,%rax)
	movsd	16376(%rsi,%rax), %xmm0 # xmm0 = mem[0],zero
	divsd	16376(%rdx,%rax), %xmm0
	movsd	%xmm0, 16376(%rsi,%rax)
	addq	$16384, %rax            # imm = 0x4000
	cmpq	$8388608, %rax          # imm = 0x800000
	jne	.LBB7_10
# BB#11:                                # %.preheader2.preheader
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	-64(%rsp), %r10         # 8-byte Reload
	movq	-56(%rsp), %r11         # 8-byte Reload
	movq	-48(%rsp), %r14         # 8-byte Reload
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB7_12:                               # %.preheader2
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_13 Depth 3
	movl	$1022, %eax             # imm = 0x3FE
	movq	%r10, %rcx
	movq	%r11, %rbp
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB7_13:                               #   Parent Loop BB7_1 Depth=1
                                        #     Parent Loop BB7_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	mulsd	(%rbp), %xmm0
	subsd	%xmm0, %xmm1
	divsd	(%rcx), %xmm1
	movsd	%xmm1, (%rbx)
	addq	$-8, %rbx
	addq	$-8, %rbp
	addq	$-8, %rcx
	decq	%rax
	jne	.LBB7_13
# BB#14:                                #   in Loop: Header=BB7_12 Depth=2
	incq	%r15
	addq	$8192, %r14             # imm = 0x2000
	addq	$8192, %r11             # imm = 0x2000
	addq	$8192, %r10             # imm = 0x2000
	cmpq	$1024, %r15             # imm = 0x400
	jne	.LBB7_12
# BB#15:                                # %.preheader1.preheader
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	%r13, -120(%rsp)        # 8-byte Spill
	movq	-88(%rsp), %r10         # 8-byte Reload
	movq	-80(%rsp), %r11         # 8-byte Reload
	movq	-72(%rsp), %r14         # 8-byte Reload
	xorl	%r15d, %r15d
	movl	$1, %edi
	.p2align	4, 0x90
.LBB7_16:                               # %.preheader1
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_20 Depth 3
                                        #       Child Loop BB7_22 Depth 3
	movq	%r15, %rax
	shlq	$13, %rax
	leaq	(%rsi,%rax), %rcx
	leaq	16384(%rsi,%rax), %rbx
	leaq	(%rdx,%rax), %rbp
	leaq	16384(%rdx,%rax), %r8
	leaq	8192(%r12,%rax), %r13
	leaq	16384(%r12,%rax), %rax
	cmpq	%r8, %rcx
	sbbb	%r9b, %r9b
	cmpq	%rbx, %rbp
	sbbb	%r12b, %r12b
	andb	%r9b, %r12b
	cmpq	%rax, %rcx
	sbbb	%cl, %cl
	cmpq	%rbx, %r13
	sbbb	%bl, %bl
	cmpq	%rax, %rbp
	sbbb	%al, %al
	cmpq	%r8, %r13
	sbbb	%bpl, %bpl
	testb	$1, %r12b
	jne	.LBB7_21
# BB#17:                                # %.preheader1
                                        #   in Loop: Header=BB7_16 Depth=2
	andb	%bl, %cl
	andb	$1, %cl
	jne	.LBB7_21
# BB#18:                                # %.preheader1
                                        #   in Loop: Header=BB7_16 Depth=2
	andb	%bpl, %al
	andb	$1, %al
	jne	.LBB7_21
# BB#19:                                # %vector.body94.preheader
                                        #   in Loop: Header=BB7_16 Depth=2
	movl	$1024, %eax             # imm = 0x400
	movq	%r10, %rcx
	movq	%r11, %rbx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB7_20:                               # %vector.body94
                                        #   Parent Loop BB7_1 Depth=1
                                        #     Parent Loop BB7_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	(%rbp), %xmm0
	movupd	-8192(%rbp), %xmm1
	movupd	(%rbx), %xmm2
	mulpd	%xmm1, %xmm2
	movupd	-8192(%rcx), %xmm1
	divpd	%xmm1, %xmm2
	subpd	%xmm2, %xmm0
	movupd	%xmm0, (%rbp)
	movupd	(%rcx), %xmm0
	movupd	(%rbx), %xmm1
	mulpd	%xmm1, %xmm1
	movupd	-8192(%rcx), %xmm2
	divpd	%xmm2, %xmm1
	subpd	%xmm1, %xmm0
	movupd	%xmm0, (%rcx)
	addq	$16, %rbp
	addq	$16, %rbx
	addq	$16, %rcx
	addq	$-2, %rax
	jne	.LBB7_20
	jmp	.LBB7_23
	.p2align	4, 0x90
.LBB7_21:                               # %scalar.ph96.preheader
                                        #   in Loop: Header=BB7_16 Depth=2
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_22:                               # %scalar.ph96
                                        #   Parent Loop BB7_1 Depth=1
                                        #     Parent Loop BB7_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%r14,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movsd	-8192(%r14,%rax,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	(%r11,%rax,8), %xmm1
	divsd	-8192(%r10,%rax,8), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%r14,%rax,8)
	movsd	(%r10,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r11,%rax,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	divsd	-8192(%r10,%rax,8), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%r10,%rax,8)
	incq	%rax
	cmpq	$1024, %rax             # imm = 0x400
	jne	.LBB7_22
.LBB7_23:                               # %middle.block95
                                        #   in Loop: Header=BB7_16 Depth=2
	incq	%rdi
	incq	%r15
	addq	$8192, %r14             # imm = 0x2000
	addq	$8192, %r11             # imm = 0x2000
	addq	$8192, %r10             # imm = 0x2000
	cmpq	$1024, %rdi             # imm = 0x400
	movq	-8(%rsp), %r12          # 8-byte Reload
	movq	-16(%rsp), %rdx         # 8-byte Reload
	jne	.LBB7_16
# BB#24:                                # %vector.memcheck83
                                        #   in Loop: Header=BB7_1 Depth=1
	cmpb	$0, -121(%rsp)          # 1-byte Folded Reload
	je	.LBB7_25
# BB#27:                                # %.preheader5.preheader
                                        #   in Loop: Header=BB7_1 Depth=1
	xorl	%eax, %eax
	movq	-120(%rsp), %r13        # 8-byte Reload
	.p2align	4, 0x90
.LBB7_28:                               # %.preheader5
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	8380416(%rsi,%rax,8), %xmm0 # xmm0 = mem[0],zero
	divsd	8380416(%rdx,%rax,8), %xmm0
	movsd	%xmm0, 8380416(%rsi,%rax,8)
	movsd	8380424(%rsi,%rax,8), %xmm0 # xmm0 = mem[0],zero
	divsd	8380424(%rdx,%rax,8), %xmm0
	movsd	%xmm0, 8380424(%rsi,%rax,8)
	addq	$2, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jne	.LBB7_28
	jmp	.LBB7_29
	.p2align	4, 0x90
.LBB7_25:                               # %vector.body66.preheader
                                        #   in Loop: Header=BB7_1 Depth=1
	xorl	%eax, %eax
	movq	-120(%rsp), %r13        # 8-byte Reload
	.p2align	4, 0x90
.LBB7_26:                               # %vector.body66
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	8380416(%rsi,%rax,8), %xmm0
	movupd	8380416(%rdx,%rax,8), %xmm1
	divpd	%xmm1, %xmm0
	movupd	%xmm0, 8380416(%rsi,%rax,8)
	movupd	8380432(%rsi,%rax,8), %xmm0
	movupd	8380432(%rdx,%rax,8), %xmm1
	divpd	%xmm1, %xmm0
	movupd	%xmm0, 8380432(%rsi,%rax,8)
	addq	$4, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jne	.LBB7_26
.LBB7_29:                               # %.preheader.preheader
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	-112(%rsp), %r11        # 8-byte Reload
	movq	-104(%rsp), %r14        # 8-byte Reload
	movq	-96(%rsp), %r15         # 8-byte Reload
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB7_30:                               # %.preheader
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_33 Depth 3
                                        #       Child Loop BB7_35 Depth 3
	movl	$1021, %eax             # imm = 0x3FD
	subq	%r10, %rax
	shlq	$13, %rax
	leaq	(%rsi,%rax), %r8
	movl	$1023, %edi             # imm = 0x3FF
	subq	%r10, %rdi
	shlq	$13, %rdi
	leaq	(%rsi,%rdi), %rbp
	addq	%r12, %rax
	movl	$1022, %ebx             # imm = 0x3FE
	subq	%r10, %rbx
	shlq	$13, %rbx
	leaq	(%r12,%rbx), %rcx
	addq	%rdx, %rbx
	addq	%rdx, %rdi
	cmpq	%rcx, %r8
	sbbb	%r9b, %r9b
	cmpq	%rbp, %rax
	sbbb	%cl, %cl
	andb	%r9b, %cl
	cmpq	%rdi, %r8
	sbbb	%al, %al
	cmpq	%rbp, %rbx
	sbbb	%bl, %bl
	testb	$1, %cl
	jne	.LBB7_34
# BB#31:                                # %.preheader
                                        #   in Loop: Header=BB7_30 Depth=2
	andb	%bl, %al
	andb	$1, %al
	jne	.LBB7_34
# BB#32:                                # %vector.body.preheader
                                        #   in Loop: Header=BB7_30 Depth=2
	movl	$1024, %eax             # imm = 0x400
	movq	%r11, %rcx
	movq	%r14, %rbx
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB7_33:                               # %vector.body
                                        #   Parent Loop BB7_1 Depth=1
                                        #     Parent Loop BB7_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	(%rbp), %xmm0
	movupd	-8192(%rbp), %xmm1
	movupd	(%rbx), %xmm2
	mulpd	%xmm1, %xmm2
	subpd	%xmm2, %xmm0
	movupd	(%rcx), %xmm1
	divpd	%xmm1, %xmm0
	movupd	%xmm0, (%rbp)
	addq	$16, %rbp
	addq	$16, %rbx
	addq	$16, %rcx
	addq	$-2, %rax
	jne	.LBB7_33
	jmp	.LBB7_36
	.p2align	4, 0x90
.LBB7_34:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB7_30 Depth=2
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_35:                               # %scalar.ph
                                        #   Parent Loop BB7_1 Depth=1
                                        #     Parent Loop BB7_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%r15,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movsd	-8192(%r15,%rax,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	(%r14,%rax,8), %xmm1
	subsd	%xmm1, %xmm0
	divsd	(%r11,%rax,8), %xmm0
	movsd	%xmm0, (%r15,%rax,8)
	incq	%rax
	cmpq	$1024, %rax             # imm = 0x400
	jne	.LBB7_35
.LBB7_36:                               # %middle.block
                                        #   in Loop: Header=BB7_30 Depth=2
	incq	%r10
	addq	$-8192, %r15            # imm = 0xE000
	addq	$-8192, %r14            # imm = 0xE000
	addq	$-8192, %r11            # imm = 0xE000
	cmpq	$1022, %r10             # imm = 0x3FE
	jne	.LBB7_30
# BB#37:                                #   in Loop: Header=BB7_1 Depth=1
	incl	%r13d
	cmpl	$50, %r13d
	jne	.LBB7_1
# BB#38:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	kernel_adi, .Lfunc_end7-kernel_adi
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d][%d] = %lf and B[%d][%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 76


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
