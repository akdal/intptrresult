	.text
	.file	"btTriangleShapeEx.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	3296329728              # float -1000
.LCPI0_1:
	.long	872415232               # float 1.1920929E-7
	.text
	.globl	_ZN20GIM_TRIANGLE_CONTACT12merge_pointsERK9btVector4fPK9btVector3i
	.p2align	4, 0x90
	.type	_ZN20GIM_TRIANGLE_CONTACT12merge_pointsERK9btVector4fPK9btVector3i,@function
_ZN20GIM_TRIANGLE_CONTACT12merge_pointsERK9btVector4fPK9btVector3i: # @_ZN20GIM_TRIANGLE_CONTACT12merge_pointsERK9btVector4fPK9btVector3i
	.cfi_startproc
# BB#0:
	movl	$3296329728, %eax       # imm = 0xC47A0000
	movq	%rax, (%rdi)
	testl	%ecx, %ecx
	jle	.LBB0_8
# BB#1:                                 # %.lr.ph23
	movl	%ecx, %r8d
	leaq	8(%rdx), %rcx
	movss	.LCPI0_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	xorl	%r9d, %r9d
	xorps	%xmm1, %xmm1
	movss	.LCPI0_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movss	-8(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	-4(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	(%rsi), %xmm4
	mulss	4(%rsi), %xmm5
	addss	%xmm4, %xmm5
	movss	(%rcx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	mulss	8(%rsi), %xmm6
	addss	%xmm5, %xmm6
	subss	12(%rsi), %xmm6
	movaps	%xmm0, %xmm4
	subss	%xmm6, %xmm4
	ucomiss	%xmm1, %xmm4
	jb	.LBB0_11
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	ucomiss	%xmm3, %xmm4
	jbe	.LBB0_9
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	movss	%xmm4, (%rdi)
	movl	%eax, -72(%rsp)
	movl	$1, 4(%rdi)
	movl	$1, %r9d
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_2 Depth=1
	addss	%xmm2, %xmm4
	ucomiss	%xmm3, %xmm4
	jb	.LBB0_11
# BB#10:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%r9d, %r9
	movl	%eax, -72(%rsp,%r9,4)
	incl	%r9d
	movl	%r9d, 4(%rdi)
	.p2align	4, 0x90
.LBB0_11:                               #   in Loop: Header=BB0_2 Depth=1
	movaps	%xmm3, %xmm4
.LBB0_12:                               #   in Loop: Header=BB0_2 Depth=1
	incq	%rax
	addq	$16, %rcx
	cmpq	%rax, %r8
	movaps	%xmm4, %xmm3
	jne	.LBB0_2
# BB#5:                                 # %.preheader
	testl	%r9d, %r9d
	jle	.LBB0_8
# BB#6:                                 # %.lr.ph.preheader
	leaq	24(%rdi), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	-72(%rsp,%rcx,4), %rsi
	shlq	$4, %rsi
	movups	(%rdx,%rsi), %xmm0
	movups	%xmm0, (%rax)
	incq	%rcx
	movslq	4(%rdi), %rsi
	addq	$16, %rax
	cmpq	%rsi, %rcx
	jl	.LBB0_7
.LBB0_8:                                # %._crit_edge
	retq
.Lfunc_end0:
	.size	_ZN20GIM_TRIANGLE_CONTACT12merge_pointsERK9btVector4fPK9btVector3i, .Lfunc_end0-_ZN20GIM_TRIANGLE_CONTACT12merge_pointsERK9btVector4fPK9btVector3i
	.cfi_endproc

	.globl	_ZN19btPrimitiveTriangle25overlap_test_conservativeERKS_
	.p2align	4, 0x90
	.type	_ZN19btPrimitiveTriangle25overlap_test_conservativeERKS_,@function
_ZN19btPrimitiveTriangle25overlap_test_conservativeERKS_: # @_ZN19btPrimitiveTriangle25overlap_test_conservativeERKS_
	.cfi_startproc
# BB#0:
	movss	64(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	64(%rsi), %xmm0
	movss	48(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	52(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movss	4(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm1, %xmm5
	movss	56(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm6
	addss	%xmm5, %xmm6
	movss	60(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm6
	subss	%xmm0, %xmm6
	xorps	%xmm8, %xmm8
	ucomiss	%xmm8, %xmm6
	jbe	.LBB1_4
# BB#1:
	movss	16(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	movss	20(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm7
	addss	%xmm6, %xmm7
	movss	24(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm6
	addss	%xmm7, %xmm6
	subss	%xmm5, %xmm6
	subss	%xmm0, %xmm6
	ucomiss	%xmm8, %xmm6
	jbe	.LBB1_4
# BB#2:
	mulss	32(%rsi), %xmm3
	mulss	36(%rsi), %xmm2
	addss	%xmm3, %xmm2
	mulss	40(%rsi), %xmm4
	addss	%xmm2, %xmm4
	subss	%xmm5, %xmm4
	subss	%xmm0, %xmm4
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm4
	jbe	.LBB1_4
# BB#3:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB1_4:
	movss	48(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	52(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	movss	4(%rdi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	addss	%xmm2, %xmm6
	movss	56(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm6, %xmm5
	movss	60(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm5
	subss	%xmm0, %xmm5
	movss	16(%rdi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm7
	movss	20(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	%xmm7, %xmm1
	movss	24(%rdi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm7
	addss	%xmm1, %xmm7
	subss	%xmm6, %xmm7
	subss	%xmm0, %xmm7
	mulss	32(%rdi), %xmm4
	mulss	36(%rdi), %xmm3
	addss	%xmm4, %xmm3
	mulss	40(%rdi), %xmm2
	addss	%xmm3, %xmm2
	subss	%xmm6, %xmm2
	subss	%xmm0, %xmm2
	ucomiss	%xmm8, %xmm5
	setbe	%al
	ucomiss	%xmm8, %xmm7
	setbe	%cl
	orb	%al, %cl
	ucomiss	%xmm8, %xmm2
	setbe	%al
	orb	%cl, %al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end1:
	.size	_ZN19btPrimitiveTriangle25overlap_test_conservativeERKS_, .Lfunc_end1-_ZN19btPrimitiveTriangle25overlap_test_conservativeERKS_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN19btPrimitiveTriangle13clip_triangleERS_P9btVector3
	.p2align	4, 0x90
	.type	_ZN19btPrimitiveTriangle13clip_triangleERS_P9btVector3,@function
_ZN19btPrimitiveTriangle13clip_triangleERS_P9btVector3: # @_ZN19btPrimitiveTriangle13clip_triangleERS_P9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$544, %rsp              # imm = 0x220
.Lcfi3:
	.cfi_def_cfa_offset 576
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movss	16(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	(%rbx), %xmm6
	subss	4(%rbx), %xmm0
	movss	24(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	8(%rbx), %xmm4
	movss	56(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm5
	mulss	%xmm1, %xmm5
	movss	52(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm3
	mulss	%xmm2, %xmm3
	subss	%xmm3, %xmm5
	movss	48(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm4
	mulss	%xmm2, %xmm6
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm6
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm4, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB2_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm4, 12(%rsp)         # 4-byte Spill
	movss	%xmm5, 8(%rsp)          # 4-byte Spill
	movss	%xmm6, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
.LBB2_2:                                # %.split
	movss	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm5
	mulss	%xmm1, %xmm4
	mulss	%xmm6, %xmm1
	movss	16(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	movss	20(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	addss	%xmm0, %xmm2
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	addss	%xmm2, %xmm0
	movss	%xmm5, 16(%rsp)
	movss	%xmm4, 20(%rsp)
	movss	%xmm1, 24(%rsp)
	movss	%xmm0, 28(%rsp)
	leaq	16(%rsp), %rdi
	leaq	288(%rsp), %r8
	leaq	16(%rbp), %rdx
	leaq	32(%rbp), %rcx
	movq	%rbp, %rsi
	callq	_Z22bt_plane_clip_triangleRK9btVector4RK9btVector3S4_S4_PS2_
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB2_10
# BB#3:
	movss	32(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	36(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	16(%rbx), %xmm6
	subss	20(%rbx), %xmm0
	movss	40(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	24(%rbx), %xmm4
	movss	56(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm5
	mulss	%xmm1, %xmm5
	movss	52(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm3
	mulss	%xmm2, %xmm3
	subss	%xmm3, %xmm5
	movss	48(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm4
	mulss	%xmm2, %xmm6
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm6
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm6, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB2_5
# BB#4:                                 # %call.sqrt12
	movss	%xmm4, 12(%rsp)         # 4-byte Spill
	movss	%xmm5, 8(%rsp)          # 4-byte Spill
	movss	%xmm6, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB2_5:                                # %.split11
	movss	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm5
	mulss	%xmm0, %xmm4
	mulss	%xmm6, %xmm0
	movss	32(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	movss	36(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	addss	%xmm1, %xmm2
	movss	40(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	%xmm5, 16(%rsp)
	movss	%xmm4, 20(%rsp)
	movss	%xmm0, 24(%rsp)
	movss	%xmm1, 28(%rsp)
	leaq	16(%rsp), %rdi
	leaq	288(%rsp), %rsi
	leaq	32(%rsp), %rcx
	movl	%ebp, %edx
	callq	_Z21bt_plane_clip_polygonRK9btVector4PK9btVector3iPS2_
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB2_10
# BB#6:
	movss	(%rbx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	32(%rbx), %xmm6
	subss	36(%rbx), %xmm0
	movss	8(%rbx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	subss	40(%rbx), %xmm4
	movss	56(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm5
	mulss	%xmm1, %xmm5
	movss	52(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm3
	mulss	%xmm2, %xmm3
	subss	%xmm3, %xmm5
	movss	48(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm4
	mulss	%xmm2, %xmm6
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm6
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm6, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB2_8
# BB#7:                                 # %call.sqrt14
	movss	%xmm4, 12(%rsp)         # 4-byte Spill
	movss	%xmm5, 8(%rsp)          # 4-byte Spill
	movss	%xmm6, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB2_8:                                # %.split13
	movss	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	divss	%xmm1, %xmm2
	mulss	%xmm2, %xmm5
	mulss	%xmm2, %xmm4
	mulss	%xmm6, %xmm2
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm5, 16(%rsp)
	movss	%xmm4, 20(%rsp)
	movss	%xmm2, 24(%rsp)
	movss	%xmm0, 28(%rsp)
	leaq	16(%rsp), %rdi
	leaq	32(%rsp), %rsi
	movl	%ebp, %edx
	movq	%r14, %rcx
	callq	_Z21bt_plane_clip_polygonRK9btVector4PK9btVector3iPS2_
	jmp	.LBB2_11
.LBB2_10:
	xorl	%eax, %eax
.LBB2_11:
	addq	$544, %rsp              # imm = 0x220
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN19btPrimitiveTriangle13clip_triangleERS_P9btVector3, .Lfunc_end2-_ZN19btPrimitiveTriangle13clip_triangleERS_P9btVector3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	872415232               # float 1.1920929E-7
.LCPI3_2:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._Z22bt_plane_clip_triangleRK9btVector4RK9btVector3S4_S4_PS2_,"axG",@progbits,_Z22bt_plane_clip_triangleRK9btVector4RK9btVector3S4_S4_PS2_,comdat
	.weak	_Z22bt_plane_clip_triangleRK9btVector4RK9btVector3S4_S4_PS2_
	.p2align	4, 0x90
	.type	_Z22bt_plane_clip_triangleRK9btVector4RK9btVector3S4_S4_PS2_,@function
_Z22bt_plane_clip_triangleRK9btVector4RK9btVector3S4_S4_PS2_: # @_Z22bt_plane_clip_triangleRK9btVector4RK9btVector3S4_S4_PS2_
	.cfi_startproc
# BB#0:
	movss	(%rsi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	(%rdi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm5, %xmm0
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	addss	%xmm0, %xmm3
	movss	8(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	addss	%xmm3, %xmm0
	movss	12(%rdi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm0
	xorl	%eax, %eax
	ucomiss	.LCPI3_0(%rip), %xmm0
	ja	.LBB3_2
# BB#1:
	movups	(%rsi), %xmm1
	movups	%xmm1, (%r8)
	movss	(%rdi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movl	$1, %eax
.LBB3_2:
	movss	.LCPI3_0(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	ucomiss	%xmm8, %xmm0
	seta	%r9b
	movq	(%rdx), %xmm4           # xmm4 = mem[0],zero
	pshufd	$229, %xmm4, %xmm3      # xmm3 = xmm4[1,1,2,3]
	mulss	%xmm4, %xmm5
	mulss	%xmm6, %xmm3
	addss	%xmm5, %xmm3
	movss	8(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	addss	%xmm3, %xmm1
	subss	%xmm7, %xmm1
	ucomiss	%xmm8, %xmm1
	seta	%r10b
	cmpb	%r10b, %r9b
	je	.LBB3_4
# BB#3:
	movaps	.LCPI3_1(%rip), %xmm3   # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm3
	movaps	%xmm1, %xmm6
	subss	%xmm0, %xmm6
	divss	%xmm6, %xmm3
	movq	%rax, %r9
	shlq	$4, %r9
	movss	.LCPI3_2(%rip), %xmm6   # xmm6 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm6
	movaps	%xmm6, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movss	4(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm9    # xmm9 = xmm9[0],xmm2[0],xmm9[1],xmm2[1]
	mulps	%xmm7, %xmm9
	mulss	8(%rsi), %xmm6
	mulss	%xmm3, %xmm5
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm9, %xmm4
	addss	%xmm6, %xmm5
	xorps	%xmm2, %xmm2
	movss	%xmm5, %xmm2            # xmm2 = xmm5[0],xmm2[1,2,3]
	movlps	%xmm4, (%r8,%r9)
	movlps	%xmm2, 8(%r8,%r9)
	incl	%eax
.LBB3_4:
	ucomiss	.LCPI3_0(%rip), %xmm1
	ja	.LBB3_6
# BB#5:
	cltq
	movq	%rax, %r9
	shlq	$4, %r9
	movups	(%rdx), %xmm2
	movups	%xmm2, (%r8,%r9)
	incl	%eax
.LBB3_6:                                # %_Z29bt_plane_clip_polygon_collectRK9btVector3S1_ffPS_Ri.exit47
	ucomiss	%xmm8, %xmm1
	seta	%r9b
	movq	(%rcx), %xmm4           # xmm4 = mem[0],zero
	pshufd	$229, %xmm4, %xmm3      # xmm3 = xmm4[1,1,2,3]
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	mulss	4(%rdi), %xmm3
	addss	%xmm2, %xmm3
	movss	8(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	addss	%xmm3, %xmm2
	subss	12(%rdi), %xmm2
	ucomiss	%xmm8, %xmm2
	seta	%dil
	cmpb	%dil, %r9b
	je	.LBB3_8
# BB#7:
	movaps	%xmm2, %xmm3
	subss	%xmm1, %xmm3
	xorps	.LCPI3_1(%rip), %xmm1
	divss	%xmm3, %xmm1
	cltq
	movq	%rax, %rdi
	shlq	$4, %rdi
	movss	.LCPI3_2(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm3
	movsd	(%rdx), %xmm6           # xmm6 = mem[0],zero
	movaps	%xmm3, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm6, %xmm7
	mulss	8(%rdx), %xmm3
	mulss	%xmm1, %xmm5
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm4
	addps	%xmm7, %xmm4
	addss	%xmm3, %xmm5
	xorps	%xmm1, %xmm1
	movss	%xmm5, %xmm1            # xmm1 = xmm5[0],xmm1[1,2,3]
	movlps	%xmm4, (%r8,%rdi)
	movlps	%xmm1, 8(%r8,%rdi)
	incl	%eax
.LBB3_8:
	ucomiss	.LCPI3_0(%rip), %xmm2
	jbe	.LBB3_9
# BB#10:                                # %_Z29bt_plane_clip_polygon_collectRK9btVector3S1_ffPS_Ri.exit39
	ucomiss	.LCPI3_0(%rip), %xmm0
	jbe	.LBB3_11
	jmp	.LBB3_13
.LBB3_9:
	cltq
	movq	%rax, %rdx
	shlq	$4, %rdx
	incl	%eax
	ucomiss	.LCPI3_0(%rip), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm1, (%r8,%rdx)
	jbe	.LBB3_12
.LBB3_11:
	cltq
	movq	%rax, %rdx
	shlq	$4, %rdx
	incl	%eax
	ucomiss	.LCPI3_0(%rip), %xmm0
	subss	%xmm2, %xmm0
	xorps	.LCPI3_1(%rip), %xmm2
	divss	%xmm0, %xmm2
	movss	.LCPI3_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	mulss	8(%rcx), %xmm0
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm1, %xmm4
	mulss	8(%rsi), %xmm2
	addps	%xmm3, %xmm4
	addss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm4, (%r8,%rdx)
	movlps	%xmm0, 8(%r8,%rdx)
	ja	.LBB3_13
.LBB3_12:                               # %.thread66
	cltq
	movq	%rax, %rcx
	shlq	$4, %rcx
	movups	(%rsi), %xmm0
	movups	%xmm0, (%r8,%rcx)
	incl	%eax
.LBB3_13:                               # %_Z29bt_plane_clip_polygon_collectRK9btVector3S1_ffPS_Ri.exit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end3:
	.size	_Z22bt_plane_clip_triangleRK9btVector4RK9btVector3S4_S4_PS2_, .Lfunc_end3-_Z22bt_plane_clip_triangleRK9btVector4RK9btVector3S4_S4_PS2_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	872415232               # float 1.1920929E-7
.LCPI4_2:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._Z21bt_plane_clip_polygonRK9btVector4PK9btVector3iPS2_,"axG",@progbits,_Z21bt_plane_clip_polygonRK9btVector4PK9btVector3iPS2_,comdat
	.weak	_Z21bt_plane_clip_polygonRK9btVector4PK9btVector3iPS2_
	.p2align	4, 0x90
	.type	_Z21bt_plane_clip_polygonRK9btVector4PK9btVector3iPS2_,@function
_Z21bt_plane_clip_polygonRK9btVector4PK9btVector3iPS2_: # @_Z21bt_plane_clip_polygonRK9btVector4PK9btVector3iPS2_
	.cfi_startproc
# BB#0:
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi), %xmm0
	mulss	4(%rdi), %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rsi), %xmm10         # xmm10 = mem[0],zero,zero,zero
	mulss	8(%rdi), %xmm10
	addss	%xmm1, %xmm10
	subss	12(%rdi), %xmm10
	xorl	%eax, %eax
	ucomiss	.LCPI4_0(%rip), %xmm10
	ja	.LBB4_2
# BB#1:
	movups	(%rsi), %xmm1
	movups	%xmm1, (%rcx)
	movl	$1, %eax
.LBB4_2:                                # %.preheader
	cmpl	$2, %edx
	jl	.LBB4_3
# BB#9:                                 # %.lr.ph.preheader
	movl	%edx, %r8d
	leaq	16(%rsi), %r9
	decq	%r8
	movss	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	.LCPI4_1(%rip), %xmm8   # xmm8 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movss	.LCPI4_2(%rip), %xmm9   # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm5
	.p2align	4, 0x90
.LBB4_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r9), %xmm6            # xmm6 = mem[0],zero
	movss	(%rdi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	movss	8(%r9), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movsd	4(%rdi), %xmm4          # xmm4 = mem[0],zero
	pshufd	$229, %xmm6, %xmm0      # xmm0 = xmm6[1,1,2,3]
	movaps	%xmm7, %xmm1
	shufps	$0, %xmm0, %xmm1        # xmm1 = xmm1[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm1      # xmm1 = xmm1[2,0],xmm0[2,3]
	mulps	%xmm4, %xmm1
	addss	%xmm1, %xmm3
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	addss	%xmm3, %xmm1
	subss	12(%rdi), %xmm1
	ucomiss	%xmm2, %xmm5
	seta	%r10b
	ucomiss	%xmm2, %xmm1
	seta	%r11b
	cmpb	%r11b, %r10b
	je	.LBB4_12
# BB#11:                                #   in Loop: Header=BB4_10 Depth=1
	movaps	%xmm1, %xmm0
	subss	%xmm5, %xmm0
	xorps	%xmm8, %xmm5
	divss	%xmm0, %xmm5
	cltq
	movq	%rax, %r10
	shlq	$4, %r10
	movaps	%xmm9, %xmm0
	subss	%xmm5, %xmm0
	movsd	-16(%r9), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	mulss	-8(%r9), %xmm0
	mulss	%xmm5, %xmm7
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm5, %xmm6
	addps	%xmm4, %xmm6
	addss	%xmm0, %xmm7
	xorps	%xmm0, %xmm0
	movss	%xmm7, %xmm0            # xmm0 = xmm7[0],xmm0[1,2,3]
	movlps	%xmm6, (%rcx,%r10)
	movlps	%xmm0, 8(%rcx,%r10)
	incl	%eax
.LBB4_12:                               #   in Loop: Header=BB4_10 Depth=1
	ucomiss	%xmm2, %xmm1
	ja	.LBB4_14
# BB#13:                                #   in Loop: Header=BB4_10 Depth=1
	cltq
	movq	%rax, %r10
	shlq	$4, %r10
	movups	(%r9), %xmm0
	movups	%xmm0, (%rcx,%r10)
	incl	%eax
.LBB4_14:                               # %_Z29bt_plane_clip_polygon_collectRK9btVector3S1_ffPS_Ri.exit
                                        #   in Loop: Header=BB4_10 Depth=1
	addq	$16, %r9
	decq	%r8
	movaps	%xmm1, %xmm5
	jne	.LBB4_10
	jmp	.LBB4_4
.LBB4_3:
	movaps	%xmm10, %xmm1
.LBB4_4:                                # %._crit_edge
	movss	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm10
	seta	%dil
	ucomiss	%xmm2, %xmm1
	seta	%r8b
	cmpb	%r8b, %dil
	je	.LBB4_6
# BB#5:
	decl	%edx
	movslq	%edx, %rdx
	movaps	%xmm10, %xmm2
	subss	%xmm1, %xmm2
	xorps	.LCPI4_1(%rip), %xmm1
	divss	%xmm2, %xmm1
	cltq
	movq	%rax, %rdi
	shlq	$4, %rdi
	movss	.LCPI4_2(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	shlq	$4, %rdx
	movsd	(%rsi,%rdx), %xmm3      # xmm3 = mem[0],zero
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	mulss	8(%rsi,%rdx), %xmm2
	movsd	(%rsi), %xmm3           # xmm3 = mem[0],zero
	movaps	%xmm1, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	mulss	8(%rsi), %xmm1
	addps	%xmm4, %xmm5
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm5, (%rcx,%rdi)
	movlps	%xmm2, 8(%rcx,%rdi)
	incl	%eax
.LBB4_6:
	ucomiss	.LCPI4_0(%rip), %xmm10
	ja	.LBB4_8
# BB#7:
	cltq
	movq	%rax, %rdx
	shlq	$4, %rdx
	movups	(%rsi), %xmm0
	movups	%xmm0, (%rcx,%rdx)
	incl	%eax
.LBB4_8:                                # %_Z29bt_plane_clip_polygon_collectRK9btVector3S1_ffPS_Ri.exit40
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end4:
	.size	_Z21bt_plane_clip_polygonRK9btVector4PK9btVector3iPS2_, .Lfunc_end4-_Z21bt_plane_clip_polygonRK9btVector4PK9btVector3iPS2_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	3296329728              # float -1000
.LCPI5_1:
	.long	872415232               # float 1.1920929E-7
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN19btPrimitiveTriangle35find_triangle_collision_clip_methodERS_R20GIM_TRIANGLE_CONTACT
	.p2align	4, 0x90
	.type	_ZN19btPrimitiveTriangle35find_triangle_collision_clip_methodERS_R20GIM_TRIANGLE_CONTACT,@function
_ZN19btPrimitiveTriangle35find_triangle_collision_clip_methodERS_R20GIM_TRIANGLE_CONTACT: # @_ZN19btPrimitiveTriangle35find_triangle_collision_clip_methodERS_R20GIM_TRIANGLE_CONTACT
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 48
	subq	$896, %rsp              # imm = 0x380
.Lcfi12:
	.cfi_def_cfa_offset 944
.Lcfi13:
	.cfi_offset %rbx, -48
.Lcfi14:
	.cfi_offset %r12, -40
.Lcfi15:
	.cfi_offset %r13, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movss	64(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	64(%r15), %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movups	48(%r12), %xmm0
	movups	%xmm0, 88(%rsp)
	leaq	640(%rsp), %rdx
	callq	_ZN19btPrimitiveTriangle13clip_triangleERS_P9btVector3
	testl	%eax, %eax
	je	.LBB5_36
# BB#1:
	movl	$3296329728, %ecx       # imm = 0xC47A0000
	movq	%rcx, 80(%rsp)
	testl	%eax, %eax
	jle	.LBB5_36
# BB#2:                                 # %.lr.ph23.i24
	leaq	88(%rsp), %r13
	movl	%eax, %eax
	movss	88(%rsp), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	92(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	100(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	leaq	648(%rsp), %rcx
	xorl	%ebx, %ebx
	movss	.LCPI5_0(%rip), %xmm9   # xmm9 = mem[0],zero,zero,zero
	xorps	%xmm8, %xmm8
	movaps	%xmm9, %xmm5
	xorl	%edx, %edx
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB5_3
	.p2align	4, 0x90
.LBB5_5:                                #   in Loop: Header=BB5_3 Depth=1
	addss	.LCPI5_1(%rip), %xmm6
	ucomiss	%xmm5, %xmm6
	jb	.LBB5_7
# BB#6:                                 #   in Loop: Header=BB5_3 Depth=1
	movslq	%ebx, %rbx
	movl	%edx, 16(%rsp,%rbx,4)
	incl	%ebx
	jmp	.LBB5_7
	.p2align	4, 0x90
.LBB5_3:                                # =>This Inner Loop Header: Depth=1
	movss	-8(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm6
	movss	-4(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	addss	%xmm6, %xmm7
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	addss	%xmm7, %xmm4
	subss	%xmm3, %xmm4
	movaps	%xmm0, %xmm6
	subss	%xmm4, %xmm6
	ucomiss	%xmm8, %xmm6
	jae	.LBB5_4
.LBB5_7:                                #   in Loop: Header=BB5_3 Depth=1
	movaps	%xmm5, %xmm6
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_4:                                #   in Loop: Header=BB5_3 Depth=1
	ucomiss	%xmm5, %xmm6
	jbe	.LBB5_5
# BB#8:                                 #   in Loop: Header=BB5_3 Depth=1
	movl	%edx, 16(%rsp)
	movl	$1, %ebx
	movaps	%xmm6, %xmm9
.LBB5_9:                                #   in Loop: Header=BB5_3 Depth=1
	incq	%rdx
	addq	$16, %rcx
	cmpq	%rdx, %rax
	movaps	%xmm6, %xmm5
	jne	.LBB5_3
# BB#10:                                # %.preheader.i25
	movl	%ebx, 84(%rsp)
	movss	%xmm9, 80(%rsp)
	testl	%ebx, %ebx
	jle	.LBB5_13
# BB#11:                                # %.lr.ph.i31.preheader
	movslq	%ebx, %rax
	leaq	104(%rsp), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_12:                               # %.lr.ph.i31
                                        # =>This Inner Loop Header: Depth=1
	movslq	16(%rsp,%rdx,4), %rsi
	shlq	$4, %rsi
	movups	640(%rsp,%rsi), %xmm0
	movups	%xmm0, (%rcx)
	incq	%rdx
	addq	$16, %rcx
	cmpq	%rax, %rdx
	jl	.LBB5_12
.LBB5_13:                               # %_ZN20GIM_TRIANGLE_CONTACT12merge_pointsERK9btVector4fPK9btVector3i.exit32
	testl	%ebx, %ebx
	je	.LBB5_36
# BB#14:
	movss	%xmm9, 12(%rsp)         # 4-byte Spill
	movss	88(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm0
	movss	%xmm0, 88(%rsp)
	movss	92(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm0
	movss	%xmm0, 92(%rsp)
	movss	96(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm0
	movss	%xmm0, 96(%rsp)
	movups	48(%r15), %xmm0
	movups	%xmm0, 368(%rsp)
	leaq	640(%rsp), %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_ZN19btPrimitiveTriangle13clip_triangleERS_P9btVector3
	testl	%eax, %eax
	je	.LBB5_36
# BB#15:
	movl	$3296329728, %ecx       # imm = 0xC47A0000
	movq	%rcx, 360(%rsp)
	testl	%eax, %eax
	jle	.LBB5_36
# BB#16:                                # %.lr.ph23.i
	leaq	368(%rsp), %r8
	movl	%eax, %edx
	movss	368(%rsp), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movss	372(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	376(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	380(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	leaq	648(%rsp), %rsi
	xorl	%eax, %eax
	movss	.LCPI5_0(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	xorps	%xmm9, %xmm9
	movaps	%xmm8, %xmm6
	xorl	%edi, %edi
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI5_1(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB5_17:                               # =>This Inner Loop Header: Depth=1
	movss	-8(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm7
	movss	-4(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	addss	%xmm7, %xmm0
	movss	(%rsi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm0, %xmm5
	subss	%xmm4, %xmm5
	movaps	%xmm1, %xmm7
	subss	%xmm5, %xmm7
	ucomiss	%xmm9, %xmm7
	jb	.LBB5_22
# BB#18:                                #   in Loop: Header=BB5_17 Depth=1
	ucomiss	%xmm6, %xmm7
	jbe	.LBB5_20
# BB#19:                                #   in Loop: Header=BB5_17 Depth=1
	movl	%edi, 16(%rsp)
	movl	$1, %eax
	movaps	%xmm7, %xmm8
	jmp	.LBB5_23
	.p2align	4, 0x90
.LBB5_20:                               #   in Loop: Header=BB5_17 Depth=1
	addss	%xmm11, %xmm7
	ucomiss	%xmm6, %xmm7
	jb	.LBB5_22
# BB#21:                                #   in Loop: Header=BB5_17 Depth=1
	cltq
	movl	%edi, 16(%rsp,%rax,4)
	incl	%eax
	.p2align	4, 0x90
.LBB5_22:                               #   in Loop: Header=BB5_17 Depth=1
	movaps	%xmm6, %xmm7
.LBB5_23:                               #   in Loop: Header=BB5_17 Depth=1
	incq	%rdi
	addq	$16, %rsi
	cmpq	%rdi, %rdx
	movaps	%xmm7, %xmm6
	jne	.LBB5_17
# BB#24:                                # %.preheader.i
	movl	%eax, 364(%rsp)
	movss	%xmm8, 360(%rsp)
	testl	%eax, %eax
	jle	.LBB5_27
# BB#25:                                # %.lr.ph.i22.preheader
	movslq	%eax, %rdx
	leaq	384(%rsp), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_26:                               # %.lr.ph.i22
                                        # =>This Inner Loop Header: Depth=1
	movslq	16(%rsp,%rdi,4), %rcx
	shlq	$4, %rcx
	movups	640(%rsp,%rcx), %xmm0
	movups	%xmm0, (%rsi)
	incq	%rdi
	addq	$16, %rsi
	cmpq	%rdx, %rdi
	jl	.LBB5_26
.LBB5_27:                               # %_ZN20GIM_TRIANGLE_CONTACT12merge_pointsERK9btVector4fPK9btVector3i.exit
	testl	%eax, %eax
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	je	.LBB5_36
# BB#28:
	ucomiss	%xmm8, %xmm0
	jbe	.LBB5_37
# BB#29:                                # %.lr.ph.preheader.i15
	movss	%xmm8, (%r14)
	movups	(%r8), %xmm0
	movups	%xmm0, 8(%r14)
	movl	%eax, 4(%r14)
	movslq	%eax, %rcx
	leal	-1(%rax), %edx
	andl	$3, %eax
	je	.LBB5_32
# BB#30:                                # %.lr.ph.i18.prol.preheader
	movq	%rcx, %rdi
	shlq	$4, %rdi
	leaq	8(%r14,%rdi), %rsi
	leaq	368(%rsp,%rdi), %rdi
	negl	%eax
	.p2align	4, 0x90
.LBB5_31:                               # %.lr.ph.i18.prol
                                        # =>This Inner Loop Header: Depth=1
	decq	%rcx
	movups	(%rdi), %xmm0
	movups	%xmm0, (%rsi)
	addq	$-16, %rsi
	addq	$-16, %rdi
	incl	%eax
	jne	.LBB5_31
.LBB5_32:                               # %.lr.ph.i18.prol.loopexit
	movb	$1, %al
	cmpl	$3, %edx
	jb	.LBB5_43
# BB#33:                                # %.lr.ph.preheader.i15.new
	movl	%ecx, %edx
	negl	%edx
	shlq	$4, %rcx
	addq	%rcx, %r14
	leaq	360(%rsp,%rcx), %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_34:                               # %.lr.ph.i18
                                        # =>This Inner Loop Header: Depth=1
	movups	8(%rcx,%rsi), %xmm0
	movups	%xmm0, 8(%r14,%rsi)
	movups	-8(%rcx,%rsi), %xmm0
	movups	%xmm0, -8(%r14,%rsi)
	movups	-24(%rcx,%rsi), %xmm0
	movups	%xmm0, -24(%r14,%rsi)
	movups	-40(%rcx,%rsi), %xmm0
	movups	%xmm0, -40(%r14,%rsi)
	addq	$-64, %rsi
	addl	$4, %edx
	jne	.LBB5_34
	jmp	.LBB5_43
.LBB5_36:
	xorl	%eax, %eax
.LBB5_43:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$896, %rsp              # imm = 0x380
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB5_37:                               # %.lr.ph.preheader.i
	movss	%xmm0, (%r14)
	movups	(%r13), %xmm0
	movups	%xmm0, 8(%r14)
	movl	%ebx, 4(%r14)
	movslq	%ebx, %rcx
	leal	-1(%rbx), %edx
	andl	$3, %ebx
	je	.LBB5_40
# BB#38:                                # %.lr.ph.i.prol.preheader
	movq	%rcx, %rsi
	shlq	$4, %rsi
	leaq	8(%r14,%rsi), %rax
	leaq	88(%rsp,%rsi), %rsi
	negl	%ebx
	.p2align	4, 0x90
.LBB5_39:                               # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	decq	%rcx
	movups	(%rsi), %xmm0
	movups	%xmm0, (%rax)
	addq	$-16, %rax
	addq	$-16, %rsi
	incl	%ebx
	jne	.LBB5_39
.LBB5_40:                               # %.lr.ph.i.prol.loopexit
	movb	$1, %al
	cmpl	$3, %edx
	jb	.LBB5_43
# BB#41:                                # %.lr.ph.preheader.i.new
	movl	%ecx, %edx
	negl	%edx
	shlq	$4, %rcx
	addq	%rcx, %r14
	leaq	80(%rsp,%rcx), %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_42:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movups	8(%rcx,%rsi), %xmm0
	movups	%xmm0, 8(%r14,%rsi)
	movups	-8(%rcx,%rsi), %xmm0
	movups	%xmm0, -8(%r14,%rsi)
	movups	-24(%rcx,%rsi), %xmm0
	movups	%xmm0, -24(%r14,%rsi)
	movups	-40(%rcx,%rsi), %xmm0
	movups	%xmm0, -40(%r14,%rsi)
	addq	$-64, %rsi
	addl	$4, %edx
	jne	.LBB5_42
	jmp	.LBB5_43
.Lfunc_end5:
	.size	_ZN19btPrimitiveTriangle35find_triangle_collision_clip_methodERS_R20GIM_TRIANGLE_CONTACT, .Lfunc_end5-_ZN19btPrimitiveTriangle35find_triangle_collision_clip_methodERS_R20GIM_TRIANGLE_CONTACT
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN17btTriangleShapeEx25overlap_test_conservativeERKS_
	.p2align	4, 0x90
	.type	_ZN17btTriangleShapeEx25overlap_test_conservativeERKS_,@function
_ZN17btTriangleShapeEx25overlap_test_conservativeERKS_: # @_ZN17btTriangleShapeEx25overlap_test_conservativeERKS_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 64
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*88(%rax)
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, %xmm4
	movss	80(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	68(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	84(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm3
	movss	88(%r14), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	72(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm13
	movss	96(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm5
	movss	100(%r14), %xmm7        # xmm7 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm7
	movss	104(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm1
	movaps	%xmm13, %xmm2
	mulss	%xmm5, %xmm13
	mulss	%xmm3, %xmm5
	movaps	%xmm3, %xmm11
	mulss	%xmm1, %xmm11
	mulss	%xmm7, %xmm2
	subss	%xmm2, %xmm11
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm13
	mulss	%xmm0, %xmm7
	subss	%xmm5, %xmm7
	movaps	%xmm11, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm13, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm7, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB6_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm13, 20(%rsp)        # 4-byte Spill
	movss	%xmm11, 16(%rsp)        # 4-byte Spill
	movss	%xmm4, 12(%rsp)         # 4-byte Spill
	movss	%xmm7, 8(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	8(%rsp), %xmm7          # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
.LBB6_2:                                # %.split
	movss	24(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm8
	movss	.LCPI6_0(%rip), %xmm9   # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm4
	divss	%xmm0, %xmm4
	mulss	%xmm4, %xmm11
	mulss	%xmm4, %xmm13
	mulss	%xmm7, %xmm4
	movss	64(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	movss	68(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm1
	addss	%xmm0, %xmm1
	movss	72(%r14), %xmm14        # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm14
	addss	%xmm1, %xmm14
	movss	80(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	84(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm3
	movss	88(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm10
	movss	96(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm5
	movss	100(%rbx), %xmm15       # xmm15 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm15
	movss	104(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm1
	movaps	%xmm10, %xmm2
	mulss	%xmm5, %xmm10
	mulss	%xmm3, %xmm5
	movaps	%xmm3, %xmm12
	mulss	%xmm1, %xmm12
	mulss	%xmm15, %xmm2
	subss	%xmm2, %xmm12
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm10
	mulss	%xmm0, %xmm15
	subss	%xmm5, %xmm15
	movaps	%xmm12, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm10, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm15, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB6_4
# BB#3:                                 # %call.sqrt61
	movaps	%xmm1, %xmm0
	movss	%xmm8, 24(%rsp)         # 4-byte Spill
	movss	%xmm13, 20(%rsp)        # 4-byte Spill
	movss	%xmm10, 12(%rsp)        # 4-byte Spill
	movss	%xmm11, 16(%rsp)        # 4-byte Spill
	movss	%xmm12, 8(%rsp)         # 4-byte Spill
	movss	%xmm4, 36(%rsp)         # 4-byte Spill
	movss	%xmm14, 32(%rsp)        # 4-byte Spill
	movss	%xmm15, 28(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	28(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm14        # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movss	36(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm12         # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movss	.LCPI6_0(%rip), %xmm9   # xmm9 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
.LBB6_4:                                # %.split.split
	movss	64(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm2
	mulss	%xmm7, %xmm2
	movaps	%xmm13, %xmm5
	mulss	%xmm3, %xmm5
	addss	%xmm2, %xmm5
	movaps	%xmm4, %xmm6
	mulss	%xmm1, %xmm6
	addss	%xmm5, %xmm6
	subss	%xmm14, %xmm6
	subss	%xmm8, %xmm6
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm6
	jbe	.LBB6_8
# BB#5:                                 # %.split.split
	movss	80(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm5
	movss	84(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm6
	addss	%xmm5, %xmm6
	movss	88(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	addss	%xmm6, %xmm5
	subss	%xmm14, %xmm5
	subss	%xmm8, %xmm5
	ucomiss	%xmm2, %xmm5
	jbe	.LBB6_8
# BB#6:                                 # %.split.split
	mulss	96(%rbx), %xmm11
	mulss	100(%rbx), %xmm13
	addss	%xmm11, %xmm13
	mulss	104(%rbx), %xmm4
	addss	%xmm13, %xmm4
	subss	%xmm14, %xmm4
	subss	%xmm8, %xmm4
	xorps	%xmm5, %xmm5
	ucomiss	%xmm5, %xmm4
	jbe	.LBB6_8
# BB#7:
	xorl	%eax, %eax
	jmp	.LBB6_9
.LBB6_8:
	divss	%xmm0, %xmm9
	mulss	%xmm9, %xmm12
	mulss	%xmm9, %xmm10
	mulss	%xmm15, %xmm9
	mulss	%xmm12, %xmm7
	mulss	%xmm10, %xmm3
	addss	%xmm7, %xmm3
	mulss	%xmm9, %xmm1
	addss	%xmm3, %xmm1
	movss	64(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	movss	68(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm3
	addss	%xmm0, %xmm3
	movss	72(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm0
	addss	%xmm3, %xmm0
	subss	%xmm1, %xmm0
	subss	%xmm8, %xmm0
	movss	80(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm3
	movss	84(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm3, %xmm4
	movss	88(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	addss	%xmm4, %xmm3
	subss	%xmm1, %xmm3
	subss	%xmm8, %xmm3
	mulss	96(%r14), %xmm12
	mulss	100(%r14), %xmm10
	addss	%xmm12, %xmm10
	mulss	104(%r14), %xmm9
	addss	%xmm10, %xmm9
	subss	%xmm1, %xmm9
	subss	%xmm8, %xmm9
	ucomiss	%xmm2, %xmm0
	setbe	%al
	ucomiss	%xmm2, %xmm3
	setbe	%cl
	orb	%al, %cl
	ucomiss	%xmm2, %xmm9
	setbe	%al
	orb	%cl, %al
.LBB6_9:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	_ZN17btTriangleShapeEx25overlap_test_conservativeERKS_, .Lfunc_end6-_ZN17btTriangleShapeEx25overlap_test_conservativeERKS_
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
