	.text
	.file	"zmisc.bc"
	.globl	zbind
	.p2align	4, 0x90
	.type	zbind,@function
zbind:                                  # @zbind
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 64
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r13, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movb	8(%r14), %cl
	shrb	$2, %cl
	cmpb	$10, %cl
	je	.LBB0_2
# BB#1:
	movl	$-20, %eax
	testb	%cl, %cl
	jne	.LBB0_20
.LBB0_2:
	leaq	16(%r14), %r12
	movups	(%r14), %xmm0
	movups	%xmm0, 16(%r14)
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_5 Depth 2
	movw	10(%r12), %ax
	testw	%ax, %ax
	je	.LBB0_18
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	leaq	10(%r12), %r13
	.p2align	4, 0x90
.LBB0_5:                                #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rbx
	leaq	16(%rbx), %rcx
	movq	%rcx, (%r12)
	decl	%eax
	movw	%ax, (%r13)
	movzwl	8(%rbx), %ecx
	movl	%ecx, %edx
	shrb	$2, %dl
	je	.LBB0_14
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=2
	cmpb	$10, %dl
	je	.LBB0_15
# BB#7:                                 #   in Loop: Header=BB0_5 Depth=2
	cmpb	$7, %dl
	jne	.LBB0_17
# BB#8:                                 #   in Loop: Header=BB0_5 Depth=2
	testb	$1, %cl
	je	.LBB0_17
# BB#9:                                 #   in Loop: Header=BB0_5 Depth=2
	movq	dsp(%rip), %rsi
	movl	$dstack, %edi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB0_13
# BB#10:                                #   in Loop: Header=BB0_5 Depth=2
	movq	8(%rsp), %rax
	movzwl	8(%rax), %esi
	movl	%esi, %edx
	andl	$252, %edx
	cmpl	$36, %edx
	setne	%cl
	cmpl	$61, %edx
	sbbb	%dl, %dl
	testb	$1, %sil
	je	.LBB0_13
# BB#11:                                #   in Loop: Header=BB0_5 Depth=2
	andb	%dl, %cl
	jne	.LBB0_13
# BB#12:                                #   in Loop: Header=BB0_5 Depth=2
	movups	(%rax), %xmm0
	movups	%xmm0, (%rbx)
.LBB0_13:                               #   in Loop: Header=BB0_5 Depth=2
	movw	(%r13), %ax
	testw	%ax, %ax
	jne	.LBB0_5
	jmp	.LBB0_18
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_5 Depth=2
	testb	$1, %ch
	je	.LBB0_17
.LBB0_15:                               #   in Loop: Header=BB0_5 Depth=2
	testb	$1, %cl
	je	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_5 Depth=2
	cmpq	ostop(%rip), %r12
	jb	.LBB0_21
	.p2align	4, 0x90
.LBB0_17:                               # %.backedge
                                        #   in Loop: Header=BB0_5 Depth=2
	testw	%ax, %ax
	jne	.LBB0_5
	jmp	.LBB0_18
.LBB0_21:                               # %.outer
                                        #   in Loop: Header=BB0_5 Depth=2
	andl	$65279, %ecx            # imm = 0xFEFF
	movw	%cx, 8(%rbx)
	movups	(%rbx), %xmm0
	movups	%xmm0, 16(%r12)
	leaq	26(%r12), %r13
	movw	26(%r12), %ax
	leaq	16(%r12), %r12
	testw	%ax, %ax
	jne	.LBB0_5
	.p2align	4, 0x90
.LBB0_18:                               # %.outer._crit_edge
                                        #   in Loop: Header=BB0_3 Depth=1
	addq	$-16, %r12
	cmpq	%r14, %r12
	ja	.LBB0_3
# BB#19:
	xorl	%eax, %eax
.LBB0_20:                               # %.loopexit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	zbind, .Lfunc_end0-zbind
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4654048002422341632     # double 1440
.LCPI1_1:
	.quad	4678479150791524352     # double 6.0E+4
	.text
	.globl	zcurrenttime
	.p2align	4, 0x90
	.type	zcurrenttime,@function
zcurrenttime:                           # @zcurrenttime
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%ebp, %ebp
	movq	%rsp, %rdi
	xorl	%eax, %eax
	callq	gs_get_clock
	leaq	16(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB1_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %ebp
	jmp	.LBB1_3
.LBB1_2:
	cvtsi2sdq	(%rsp), %xmm0
	mulsd	.LCPI1_0(%rip), %xmm0
	cvtsi2sdq	8(%rsp), %xmm1
	divsd	.LCPI1_1(%rip), %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 16(%rbx)
	movw	$44, 24(%rbx)
.LBB1_3:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	zcurrenttime, .Lfunc_end1-zcurrenttime
	.cfi_endproc

	.globl	zgetenv
	.p2align	4, 0x90
	.type	zgetenv,@function
zgetenv:                                # @zgetenv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$52, %edx
	jne	.LBB2_11
# BB#1:
	movl	$-7, %eax
	testb	$2, %ch
	je	.LBB2_11
# BB#2:
	movl	$.L.str, %esi
	movq	%rbx, %rdi
	callq	ref_to_string
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB2_3
# BB#4:
	movq	%r15, %rdi
	callq	getenv
	movq	%rax, %r14
	movzwl	10(%rbx), %esi
	incl	%esi
	movl	$1, %edx
	movl	$.L.str, %ecx
	movq	%r15, %rdi
	callq	alloc_free
	testq	%r14, %r14
	je	.LBB2_5
# BB#6:
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	string_to_ref
	testl	%eax, %eax
	js	.LBB2_11
# BB#7:
	leaq	16(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB2_9
# BB#8:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB2_11
.LBB2_3:
	movl	$-25, %eax
	jmp	.LBB2_11
.LBB2_5:
	movw	$0, (%rbx)
	movw	$4, 8(%rbx)
	jmp	.LBB2_10
.LBB2_9:
	movw	$1, 16(%rbx)
	movw	$4, 24(%rbx)
.LBB2_10:
	xorl	%eax, %eax
.LBB2_11:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	zgetenv, .Lfunc_end2-zgetenv
	.cfi_endproc

	.globl	zsetdebug
	.p2align	4, 0x90
	.type	zsetdebug,@function
zsetdebug:                              # @zsetdebug
	.cfi_startproc
# BB#0:
	movzwl	-8(%rdi), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$52, %edx
	jne	.LBB3_4
# BB#1:
	movl	$-7, %eax
	testb	$2, %ch
	je	.LBB3_4
# BB#2:
	movzwl	8(%rdi), %eax
	andl	$252, %eax
	cmpl	$4, %eax
	movl	$-20, %eax
	jne	.LBB3_4
# BB#3:
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
.LBB3_4:
	retq
.Lfunc_end3:
	.size	zsetdebug, .Lfunc_end3-zsetdebug
	.cfi_endproc

	.globl	ztype1encrypt
	.p2align	4, 0x90
	.type	ztype1encrypt,@function
ztype1encrypt:                          # @ztype1encrypt
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movzwl	-24(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB4_10
# BB#1:
	movq	-32(%rbx), %rcx
	movw	%cx, 6(%rsp)
	movzwl	%cx, %edx
	cmpq	%rdx, %rcx
	jne	.LBB4_2
# BB#3:
	movzwl	-8(%rbx), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	cmpl	$52, %edx
	jne	.LBB4_10
# BB#4:
	testb	$2, %ch
	jne	.LBB4_6
# BB#5:
	movl	$-7, %eax
	jmp	.LBB4_10
.LBB4_2:
	movl	$-15, %eax
.LBB4_10:                               # %type1crypt.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_6:
	movzwl	8(%rbx), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	cmpl	$52, %edx
	jne	.LBB4_10
# BB#7:
	testb	$1, %ch
	movl	$-7, %eax
	je	.LBB4_10
# BB#8:
	movzwl	-6(%rbx), %edx
	cmpw	%dx, 10(%rbx)
	movl	$-15, %eax
	jb	.LBB4_10
# BB#9:
	leaq	-16(%rbx), %r14
	movq	-16(%rbx), %rsi
	movq	(%rbx), %rdi
	leaq	6(%rsp), %rcx
	callq	gs_type1_encrypt
	movzwl	6(%rsp), %eax
	movq	%rax, -32(%rbx)
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
	movzwl	10(%rbx), %eax
	movw	%ax, -6(%rbx)
	orb	$-128, -7(%rbx)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
	jmp	.LBB4_10
.Lfunc_end4:
	.size	ztype1encrypt, .Lfunc_end4-ztype1encrypt
	.cfi_endproc

	.globl	type1crypt
	.p2align	4, 0x90
	.type	type1crypt,@function
type1crypt:                             # @type1crypt
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -24
.Lcfi31:
	.cfi_offset %r14, -16
	movq	%rsi, %r8
	movq	%rdi, %rbx
	movzwl	-24(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB5_10
# BB#1:
	movq	-32(%rbx), %rcx
	movw	%cx, 6(%rsp)
	movzwl	%cx, %edx
	cmpq	%rdx, %rcx
	jne	.LBB5_2
# BB#3:
	movzwl	-8(%rbx), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	cmpl	$52, %edx
	jne	.LBB5_10
# BB#4:
	testb	$2, %ch
	jne	.LBB5_6
# BB#5:
	movl	$-7, %eax
	jmp	.LBB5_10
.LBB5_2:
	movl	$-15, %eax
.LBB5_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_6:
	movzwl	8(%rbx), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	cmpl	$52, %edx
	jne	.LBB5_10
# BB#7:
	testb	$1, %ch
	movl	$-7, %eax
	je	.LBB5_10
# BB#8:
	movzwl	-6(%rbx), %edx
	cmpw	%dx, 10(%rbx)
	movl	$-15, %eax
	jb	.LBB5_10
# BB#9:
	leaq	-16(%rbx), %r14
	movq	-16(%rbx), %rsi
	movq	(%rbx), %rdi
	leaq	6(%rsp), %rcx
	callq	*%r8
	movzwl	6(%rsp), %eax
	movq	%rax, -32(%rbx)
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
	movzwl	10(%rbx), %eax
	movw	%ax, -6(%rbx)
	orb	$-128, -7(%rbx)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
	jmp	.LBB5_10
.Lfunc_end5:
	.size	type1crypt, .Lfunc_end5-type1crypt
	.cfi_endproc

	.globl	ztype1decrypt
	.p2align	4, 0x90
	.type	ztype1decrypt,@function
ztype1decrypt:                          # @ztype1decrypt
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -24
.Lcfi36:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movzwl	-24(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB6_10
# BB#1:
	movq	-32(%rbx), %rcx
	movw	%cx, 6(%rsp)
	movzwl	%cx, %edx
	cmpq	%rdx, %rcx
	jne	.LBB6_2
# BB#3:
	movzwl	-8(%rbx), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	cmpl	$52, %edx
	jne	.LBB6_10
# BB#4:
	testb	$2, %ch
	jne	.LBB6_6
# BB#5:
	movl	$-7, %eax
	jmp	.LBB6_10
.LBB6_2:
	movl	$-15, %eax
.LBB6_10:                               # %type1crypt.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB6_6:
	movzwl	8(%rbx), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	cmpl	$52, %edx
	jne	.LBB6_10
# BB#7:
	testb	$1, %ch
	movl	$-7, %eax
	je	.LBB6_10
# BB#8:
	movzwl	-6(%rbx), %edx
	cmpw	%dx, 10(%rbx)
	movl	$-15, %eax
	jb	.LBB6_10
# BB#9:
	leaq	-16(%rbx), %r14
	movq	-16(%rbx), %rsi
	movq	(%rbx), %rdi
	leaq	6(%rsp), %rcx
	callq	gs_type1_decrypt
	movzwl	6(%rsp), %eax
	movq	%rax, -32(%rbx)
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
	movzwl	10(%rbx), %eax
	movw	%ax, -6(%rbx)
	orb	$-128, -7(%rbx)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
	jmp	.LBB6_10
.Lfunc_end6:
	.size	ztype1decrypt, .Lfunc_end6-ztype1decrypt
	.cfi_endproc

	.globl	zusertime
	.p2align	4, 0x90
	.type	zusertime,@function
zusertime:                              # @zusertime
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 48
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%ebp, %ebp
	movq	%rsp, %rdi
	xorl	%eax, %eax
	callq	gs_get_clock
	leaq	16(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB7_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %ebp
	jmp	.LBB7_3
.LBB7_2:
	imulq	$86400000, (%rsp), %rax # imm = 0x5265C00
	addq	8(%rsp), %rax
	movq	%rax, 16(%rbx)
	movw	$20, 24(%rbx)
.LBB7_3:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end7:
	.size	zusertime, .Lfunc_end7-zusertime
	.cfi_endproc

	.globl	zmisc_op_init
	.p2align	4, 0x90
	.type	zmisc_op_init,@function
zmisc_op_init:                          # @zmisc_op_init
	.cfi_startproc
# BB#0:
	movl	$zmisc_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end8:
	.size	zmisc_op_init, .Lfunc_end8-zmisc_op_init
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"getenv name"
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"getenv value"
	.size	.L.str.1, 13

	.type	zmisc_op_init.my_defs,@object # @zmisc_op_init.my_defs
	.data
	.p2align	4
zmisc_op_init.my_defs:
	.quad	.L.str.2
	.quad	zbind
	.quad	.L.str.3
	.quad	zcurrenttime
	.quad	.L.str.4
	.quad	zgetenv
	.quad	.L.str.5
	.quad	zsetdebug
	.quad	.L.str.6
	.quad	ztype1encrypt
	.quad	.L.str.7
	.quad	ztype1decrypt
	.quad	.L.str.8
	.quad	zusertime
	.zero	16
	.size	zmisc_op_init.my_defs, 128

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"1bind"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"0currenttime"
	.size	.L.str.3, 13

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"1getenv"
	.size	.L.str.4, 8

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"2setdebug"
	.size	.L.str.5, 10

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"3type1encrypt"
	.size	.L.str.6, 14

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"3type1decrypt"
	.size	.L.str.7, 14

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"0usertime"
	.size	.L.str.8, 10


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
