	.text
	.file	"output.bc"
	.globl	testEndian
	.p2align	4, 0x90
	.type	testEndian,@function
testEndian:                             # @testEndian
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	testEndian, .Lfunc_end0-testEndian
	.cfi_endproc

	.globl	img2buf
	.p2align	4, 0x90
	.type	img2buf,@function
img2buf:                                # @img2buf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movl	%r8d, %ebx
	movl	%ecx, %r14d
	movl	%edx, %r15d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	224(%rsp), %ebp
	movl	216(%rsp), %r11d
	movl	208(%rsp), %edx
	movl	%r15d, %edi
	movq	%r9, %r12
	subl	%r9d, %edi
	subl	%edx, %edi
	cmpl	$3, %ebx
	movl	%edi, 16(%rsp)          # 4-byte Spill
	jb	.LBB1_12
# BB#1:                                 # %.thread
	movl	%r14d, %eax
	subl	%r11d, %eax
	subl	%ebp, %eax
	movl	16(%rsp), %ecx          # 4-byte Reload
	imull	%ebx, %ecx
	imull	%eax, %ecx
	movslq	%ecx, %rdx
	movq	%rsi, %r13
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	memset
	movl	208(%rsp), %edx
	movl	216(%rsp), %r11d
	movl	16(%rsp), %edi          # 4-byte Reload
	movq	%r13, %rsi
	movl	$2, %eax
	jmp	.LBB1_2
.LBB1_12:
	cmpl	$1, %ebx
	movl	%ebx, %eax
	jne	.LBB1_2
# BB#13:
	movl	%edx, %eax
	orl	%r12d, %eax
	orl	%r11d, %eax
	orl	%ebp, %eax
	movl	%ebx, %eax
	jne	.LBB1_2
# BB#14:                                # %.preheader154
	testl	%r14d, %r14d
	jle	.LBB1_26
# BB#15:                                # %.preheader154
	testl	%r15d, %r15d
	jle	.LBB1_26
# BB#16:                                # %.preheader153.us.preheader
	leal	-1(%r15), %r10d
	incq	%r10
	movl	%r15d, %r8d
	movl	%r14d, %r9d
	leaq	-1(%r8), %r11
	movl	%r8d, %r14d
	andl	$3, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_17:                               # %.preheader153.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_20 Depth 2
                                        #     Child Loop BB1_24 Depth 2
	xorl	%ebx, %ebx
	testq	%r14, %r14
	je	.LBB1_18
# BB#19:                                # %.prol.preheader185
                                        #   in Loop: Header=BB1_17 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_20:                               #   Parent Loop BB1_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rbp,8), %rax
	movzbl	(%rax,%rbx,2), %eax
	movb	%al, (%rsi,%rbx)
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB1_20
# BB#21:                                # %.prol.loopexit186.unr-lcssa
                                        #   in Loop: Header=BB1_17 Depth=1
	movq	%rsi, %r12
	leaq	(%rsi,%rbx), %rcx
	cmpq	$3, %r11
	jae	.LBB1_23
	jmp	.LBB1_25
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_17 Depth=1
	movq	%rsi, %r12
	movq	%rsi, %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpq	$3, %r11
	jb	.LBB1_25
.LBB1_23:                               # %.preheader153.us.new
                                        #   in Loop: Header=BB1_17 Depth=1
	movq	%r8, %r15
	subq	%rbx, %r15
	addq	%rbx, %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_24:                               #   Parent Loop BB1_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rbp,8), %rsi
	addq	%rbx, %rsi
	movzbl	(%rsi,%rax,2), %edx
	movb	%dl, (%rcx,%rax)
	movq	(%rdi,%rbp,8), %rdx
	addq	%rbx, %rdx
	movzbl	2(%rdx,%rax,2), %edx
	movb	%dl, 1(%rcx,%rax)
	movq	(%rdi,%rbp,8), %rdx
	addq	%rbx, %rdx
	movzbl	4(%rdx,%rax,2), %edx
	movb	%dl, 2(%rcx,%rax)
	movq	(%rdi,%rbp,8), %rdx
	addq	%rbx, %rdx
	movzbl	6(%rdx,%rax,2), %edx
	movb	%dl, 3(%rcx,%rax)
	addq	$4, %rax
	cmpq	%rax, %r15
	jne	.LBB1_24
.LBB1_25:                               # %._crit_edge162.us
                                        #   in Loop: Header=BB1_17 Depth=1
	movq	%r12, %rsi
	addq	%r10, %rsi
	incq	%rbp
	cmpq	%r9, %rbp
	jne	.LBB1_17
	jmp	.LBB1_26
.LBB1_2:                                # %.preheader
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	subl	%ebp, %r14d
	cmpl	%r11d, %r14d
	movq	%r12, %rbp
	jle	.LBB1_26
# BB#3:                                 # %.lr.ph158
	subl	%edx, %r15d
	movl	%r15d, %edx
	subl	%ebp, %edx
	jle	.LBB1_26
# BB#4:                                 # %.lr.ph158.split.us.preheader
	movslq	%eax, %r10
	movslq	%ebp, %r9
	movslq	%r11d, %rbx
	movslq	%r15d, %r8
	movslq	%r14d, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%r8, 72(%rsp)           # 8-byte Spill
	leaq	-1(%r8), %rax
	subq	%r9, %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	andl	$3, %edx
	movq	%r9, 56(%rsp)           # 8-byte Spill
	leaq	(%r9,%r9), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	%r8d, %eax
	imull	%edi, %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	negq	%rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	negl	%ebp
	leal	(,%r8,4), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movq	%r10, 24(%rsp)          # 8-byte Spill
	movq	40(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph158.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_7 Depth 2
                                        #     Child Loop BB1_10 Depth 2
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	cmpq	$0, 112(%rsp)           # 8-byte Folded Reload
	movq	%rbx, %rdx
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	je	.LBB1_8
# BB#6:                                 # %.prol.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	80(%rsp), %r15          # 8-byte Reload
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebx
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	56(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_7:                                #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebx, %rbx
	leaq	(%rsi,%rbx), %rdi
	movq	%rsi, %r12
	movq	(%rcx,%rdx,8), %rsi
	addq	%rbp, %rsi
	movq	%r10, %rdx
	callq	memcpy
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	movq	%r12, %rsi
	incq	%r14
	addq	$2, %rbp
	addl	%r13d, %ebx
	incq	%r15
	jne	.LBB1_7
.LBB1_8:                                # %.prol.loopexit
                                        #   in Loop: Header=BB1_5 Depth=1
	cmpq	$3, 96(%rsp)            # 8-byte Folded Reload
	movq	%rsi, %rbx
	movl	48(%rsp), %ebp          # 4-byte Reload
	jb	.LBB1_11
# BB#9:                                 # %.lr.ph158.split.us.new
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	72(%rsp), %r12          # 8-byte Reload
	subq	%r14, %r12
	leaq	6(%r14,%r14), %r15
	movq	64(%rsp), %rsi          # 8-byte Reload
	leal	3(%rsi,%r14), %edi
	movq	40(%rsp), %rax          # 8-byte Reload
	imull	%eax, %edi
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	leal	2(%rsi,%r14), %edi
	imull	%eax, %edi
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	movl	%r14d, %edi
	addl	%esi, %edi
	imull	%eax, %edi
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	leal	1(%rsi,%r14), %esi
	imull	%eax, %esi
	movq	%rsi, 120(%rsp)         # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_10:                               #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	128(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%rbx, %rdi
	movq	(%rcx,%rdx,8), %rax
	leaq	-6(%rax,%r15), %rsi
	movq	%r10, %rdx
	callq	memcpy
	movq	120(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%rbx, %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	leaq	-4(%rax,%r15), %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	136(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%rbx, %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	leaq	-2(%rax,%r15), %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	144(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%rbx, %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rsi
	addq	%r15, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	addq	$8, %r15
	addl	%ebp, %r14d
	addq	$-4, %r12
	jne	.LBB1_10
.LBB1_11:                               # %._crit_edge.us
                                        #   in Loop: Header=BB1_5 Depth=1
	incq	%rdx
	movl	20(%rsp), %eax          # 4-byte Reload
	addl	52(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	64(%rsp), %rbp          # 8-byte Reload
	addl	16(%rsp), %ebp          # 4-byte Folded Reload
	cmpq	104(%rsp), %rdx         # 8-byte Folded Reload
	movq	%rbx, %rsi
	movq	%rdx, %rbx
	jne	.LBB1_5
.LBB1_26:                               # %.loopexit
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	img2buf, .Lfunc_end1-img2buf
	.cfi_endproc

	.globl	write_picture
	.p2align	4, 0x90
	.type	write_picture,@function
write_picture:                          # @write_picture
	.cfi_startproc
# BB#0:
	jmp	write_out_picture       # TAILCALL
.Lfunc_end2:
	.size	write_picture, .Lfunc_end2-write_picture
	.cfi_endproc

	.globl	write_out_picture
	.p2align	4, 0x90
	.type	write_out_picture,@function
write_out_picture:                      # @write_out_picture
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 304
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	cmpl	$0, 316856(%rdi)
	je	.LBB3_2
# BB#1:
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_2:
	movq	img(%rip), %rax
	movl	5872(%rax), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	movl	%eax, 164(%rsp)         # 4-byte Spill
	addl	%eax, %ecx
	sarl	$3, %ecx
	movq	active_sps(%rip), %rax
	movl	2160(%rax), %eax
	movl	%eax, 96(%rsp)          # 4-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 317052(%rax)
	movl	%esi, 28(%rsp)          # 4-byte Spill
	je	.LBB3_4
# BB#3:
	movq	40(%rsp), %rdx          # 8-byte Reload
	movslq	317044(%rdx), %rax
	movl	.Lwrite_out_picture.SubWidthC(,%rax,4), %r13d
	movl	317056(%rdx), %r15d
	imull	%r13d, %r15d
	imull	317060(%rdx), %r13d
	movl	$2, %r12d
	subl	317048(%rdx), %r12d
	imull	.Lwrite_out_picture.SubHeightC(,%rax,4), %r12d
	movl	317064(%rdx), %r14d
	imull	%r12d, %r14d
	imull	317068(%rdx), %r12d
	jmp	.LBB3_5
.LBB3_4:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
.LBB3_5:
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	316864(%rdx), %eax
	imull	%ecx, %eax
	imull	316868(%rdx), %eax
	movslq	%eax, %rdi
	movq	%rcx, %rbx
	callq	malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB3_7
# BB#6:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB3_7:
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	movq	%rbx, %r11
	movq	%r12, %rbx
	movq	%r13, %r12
	movl	28(%rsp), %r8d          # 4-byte Reload
	movq	%r14, %r9
	jne	.LBB3_38
# BB#8:
	movq	40(%rsp), %rdx          # 8-byte Reload
	movslq	317056(%rdx), %r15
	movl	317060(%rdx), %r12d
	movslq	317048(%rdx), %rax
	movl	$2, %ecx
	subq	%rax, %rcx
	movslq	317064(%rdx), %r9
	imulq	%rcx, %r9
	movl	317068(%rdx), %ebx
	imull	%ecx, %ebx
	movq	316928(%rdx), %rax
	movq	8(%rax), %rcx
	movl	316872(%rdx), %eax
	movl	316876(%rdx), %esi
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%eax, %edi
	subl	%r15d, %edi
	subl	%r12d, %edi
	cmpl	$3, %r11d
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	%edi, 136(%rsp)         # 4-byte Spill
	jb	.LBB3_21
# BB#9:                                 # %.thread.i
	leal	(%rbx,%r9), %eax
	movl	%esi, %ecx
	subl	%eax, %ecx
	movl	%edi, %eax
	imull	%r11d, %eax
	imull	%ecx, %eax
	movslq	%eax, %rdx
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movq	%r11, %r14
	movl	%r8d, %r13d
	movl	%r12d, 128(%rsp)        # 4-byte Spill
	movq	%r9, %r12
	callq	memset
	movq	%r12, %r9
	movl	128(%rsp), %r12d        # 4-byte Reload
	movl	136(%rsp), %edi         # 4-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	%r13d, %r8d
	movq	%r14, %r11
	movl	$2, %r10d
.LBB3_10:                               # %.preheader.i
	movl	%esi, %ecx
	subl	%ebx, %ecx
	cmpl	%r9d, %ecx
	jle	.LBB3_34
# BB#11:                                # %.lr.ph158.i
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %eax
	subl	%r12d, %eax
	cmpl	%r15d, %eax
	jle	.LBB3_34
# BB#12:                                # %.lr.ph158.split.us.preheader.i
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	movl	%r12d, 128(%rsp)        # 4-byte Spill
	movslq	%r10d, %rdx
	movslq	%eax, %rsi
	movslq	%ecx, %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	subl	%r15d, %eax
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	leaq	-1(%rsi), %rcx
	subq	%r15, %rcx
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	andl	$3, %eax
	leaq	(%r15,%r15), %rcx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movl	%r11d, %ecx
	imull	%edi, %ecx
	movl	%ecx, 60(%rsp)          # 4-byte Spill
	movq	%rax, %rcx
	negq	%rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movl	%r15d, %ecx
	negl	%ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leal	(,%r11,4), %ecx
	movl	%ecx, 176(%rsp)         # 4-byte Spill
	movl	$0, 80(%rsp)            # 4-byte Folded Spill
	movq	%r9, 152(%rsp)          # 8-byte Spill
	movq	%r9, %rsi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%r15, 112(%rsp)         # 8-byte Spill
	movq	%rax, 200(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_13:                               # %.lr.ph158.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_15 Depth 2
                                        #     Child Loop BB3_18 Depth 2
	testq	%rax, %rax
	movq	120(%rsp), %r12         # 8-byte Reload
	movq	%rbp, %r13
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rsi, (%rsp)            # 8-byte Spill
	je	.LBB3_16
# BB#14:                                # %.prol.preheader413
                                        #   in Loop: Header=BB3_13 Depth=1
	movq	104(%rsp), %r14         # 8-byte Reload
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	movq	168(%rsp), %rbx         # 8-byte Reload
	movq	112(%rsp), %r15         # 8-byte Reload
	.p2align	4, 0x90
.LBB3_15:                               #   Parent Loop BB3_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rbp
	leaq	(%r13,%rbp), %rdi
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rcx,%rax,8), %rsi
	addq	%rbx, %rsi
	callq	memcpy
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	incq	%r15
	addq	$2, %rbx
	addl	%r12d, %ebp
	incq	%r14
	jne	.LBB3_15
.LBB3_16:                               # %.prol.loopexit414
                                        #   in Loop: Header=BB3_13 Depth=1
	cmpq	$3, 184(%rsp)           # 8-byte Folded Reload
	movq	%r13, %r12
	movl	176(%rsp), %ebp         # 4-byte Reload
	jb	.LBB3_19
# BB#17:                                # %.lr.ph158.split.us.i.new
                                        #   in Loop: Header=BB3_13 Depth=1
	movq	64(%rsp), %r13          # 8-byte Reload
	subq	%r15, %r13
	leaq	6(%r15,%r15), %r14
	movq	72(%rsp), %rdi          # 8-byte Reload
	leal	3(%rdi,%r15), %ebx
	movq	120(%rsp), %rax         # 8-byte Reload
	imull	%eax, %ebx
	movq	%rbx, 224(%rsp)         # 8-byte Spill
	leal	2(%rdi,%r15), %ebx
	imull	%eax, %ebx
	movq	%rbx, 216(%rsp)         # 8-byte Spill
	leal	1(%rdi,%r15), %ebx
	imull	%eax, %ebx
	movq	%rbx, 208(%rsp)         # 8-byte Spill
	addl	%edi, %r15d
	imull	%eax, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_18:                               #   Parent Loop BB3_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r15,%rbx), %eax
	movslq	%eax, %rdi
	addq	%r12, %rdi
	movq	(%rcx,%rsi,8), %rax
	leaq	-6(%rax,%r14), %rsi
	callq	memcpy
	movq	208(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rbx), %eax
	movslq	%eax, %rdi
	addq	%r12, %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	leaq	-4(%rax,%r14), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	216(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rbx), %eax
	movslq	%eax, %rdi
	addq	%r12, %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	leaq	-2(%rax,%r14), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	224(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rbx), %eax
	movslq	%eax, %rdi
	addq	%r12, %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rax,%rcx,8), %rsi
	addq	%r14, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	addq	$8, %r14
	addl	%ebp, %ebx
	addq	$-4, %r13
	jne	.LBB3_18
.LBB3_19:                               # %._crit_edge.us.i
                                        #   in Loop: Header=BB3_13 Depth=1
	incq	%rsi
	movl	80(%rsp), %eax          # 4-byte Reload
	addl	60(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 80(%rsp)          # 4-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	addl	136(%rsp), %eax         # 4-byte Folded Reload
	movq	%rax, 72(%rsp)          # 8-byte Spill
	cmpq	192(%rsp), %rsi         # 8-byte Folded Reload
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	%r12, %rbp
	jne	.LBB3_13
# BB#20:
	movl	28(%rsp), %r8d          # 4-byte Reload
	movq	120(%rsp), %r11         # 8-byte Reload
	movl	128(%rsp), %r12d        # 4-byte Reload
	movq	152(%rsp), %r9          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	144(%rsp), %rbx         # 8-byte Reload
	jmp	.LBB3_34
.LBB3_21:
	movl	164(%rsp), %eax         # 4-byte Reload
	andl	$-8, %eax
	cmpl	$8, %eax
	movl	%r11d, %r10d
	jne	.LBB3_10
# BB#22:
	movl	%r12d, %eax
	orl	%r15d, %eax
	orl	%r9d, %eax
	orl	%ebx, %eax
	movl	%r11d, %r10d
	jne	.LBB3_10
# BB#23:                                # %.preheader154.i
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_34
# BB#24:                                # %.preheader154.i
	testl	%esi, %esi
	jle	.LBB3_34
# BB#25:                                # %.preheader153.us.preheader.i
	movq	%r9, 152(%rsp)          # 8-byte Spill
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	movl	%r8d, %r14d
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r9d
	decl	%r9d
	incq	%r9
	leaq	-1(%rax), %r8
	movl	%eax, %r10d
	andl	$3, %r10d
	xorl	%esi, %esi
	movq	%rbp, %r13
	.p2align	4, 0x90
.LBB3_26:                               # %.preheader153.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_27 Depth 2
                                        #     Child Loop BB3_31 Depth 2
	xorl	%ebp, %ebp
	testq	%r10, %r10
	je	.LBB3_29
	.p2align	4, 0x90
.LBB3_27:                               #   Parent Loop BB3_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rsi,8), %rax
	movzbl	(%rax,%rbp,2), %eax
	movb	%al, (%r13,%rbp)
	incq	%rbp
	cmpq	%rbp, %r10
	jne	.LBB3_27
# BB#28:                                # %.prol.loopexit419.unr-lcssa
                                        #   in Loop: Header=BB3_26 Depth=1
	leaq	(%r13,%rbp), %rdi
	cmpq	$3, %r8
	jae	.LBB3_30
	jmp	.LBB3_32
	.p2align	4, 0x90
.LBB3_29:                               #   in Loop: Header=BB3_26 Depth=1
	movq	%r13, %rdi
	cmpq	$3, %r8
	jb	.LBB3_32
.LBB3_30:                               # %.preheader153.us.i.new
                                        #   in Loop: Header=BB3_26 Depth=1
	movq	32(%rsp), %rbx          # 8-byte Reload
	subq	%rbp, %rbx
	addq	%rbp, %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_31:                               #   Parent Loop BB3_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	(%rdx,%rax,2), %edx
	movb	%dl, (%rdi,%rax)
	movq	(%rcx,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	2(%rdx,%rax,2), %edx
	movb	%dl, 1(%rdi,%rax)
	movq	(%rcx,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	4(%rdx,%rax,2), %edx
	movb	%dl, 2(%rdi,%rax)
	movq	(%rcx,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	6(%rdx,%rax,2), %edx
	movb	%dl, 3(%rdi,%rax)
	addq	$4, %rax
	cmpq	%rax, %rbx
	jne	.LBB3_31
.LBB3_32:                               # %._crit_edge162.us.i
                                        #   in Loop: Header=BB3_26 Depth=1
	addq	%r9, %r13
	incq	%rsi
	cmpq	48(%rsp), %rsi          # 8-byte Folded Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	jne	.LBB3_26
# BB#33:                                # %img2buf.exit.loopexit357
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	316872(%rax), %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	316876(%rax), %esi
	movl	%r14d, %r8d
	movq	144(%rsp), %rbx         # 8-byte Reload
	movq	152(%rsp), %r9          # 8-byte Reload
.LBB3_34:                               # %img2buf.exit
	addl	%r9d, %ebx
	subl	%ebx, %esi
	addl	%r15d, %r12d
	movq	32(%rsp), %rax          # 8-byte Reload
	subl	%r12d, %eax
	movq	%r11, %r13
	imull	%r11d, %esi
	imull	%eax, %esi
	movslq	%esi, %rdx
	movl	%r8d, %r14d
	movl	%r8d, %edi
	movq	%rbp, %rsi
	callq	write
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 317052(%rax)
	je	.LBB3_36
# BB#35:
	movq	40(%rsp), %rcx          # 8-byte Reload
	movslq	317044(%rcx), %rax
	movl	.Lwrite_out_picture.SubWidthC(,%rax,4), %r12d
	movl	317056(%rcx), %r15d
	imull	%r12d, %r15d
	imull	317060(%rcx), %r12d
	movl	$2, %ebx
	subl	317048(%rcx), %ebx
	imull	.Lwrite_out_picture.SubHeightC(,%rax,4), %ebx
	movl	317064(%rcx), %r9d
	imull	%ebx, %r9d
	imull	317068(%rcx), %ebx
	jmp	.LBB3_37
.LBB3_36:
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
.LBB3_37:
	movq	%r13, %r11
	movl	%r14d, %r8d
.LBB3_38:
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	316920(%rax), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	316864(%rax), %ecx
	movl	316868(%rax), %edi
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	%ecx, %esi
	subl	%r15d, %esi
	subl	%r12d, %esi
	cmpl	$3, %r11d
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movl	%esi, 136(%rsp)         # 4-byte Spill
	movq	%r15, 128(%rsp)         # 8-byte Spill
	jb	.LBB3_40
# BB#39:                                # %.thread.i282
	leal	(%r9,%rbx), %eax
	movl	%edi, %ecx
	subl	%eax, %ecx
	movl	%esi, %eax
	imull	%r11d, %eax
	imull	%ecx, %eax
	movslq	%eax, %rdx
	movl	%r8d, %r15d
	movq	%r12, %r13
	movq	%rbx, %r12
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movq	%r11, %r14
	movq	%r9, %rbx
	callq	memset
	movl	136(%rsp), %esi         # 4-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %r9
	movq	%r12, %rbx
	movq	%r13, %r12
	movl	%r15d, %r8d
	movq	128(%rsp), %r15         # 8-byte Reload
	movq	%r14, %r11
	movl	$2, %r10d
	jmp	.LBB3_54
.LBB3_40:
	movl	164(%rsp), %eax         # 4-byte Reload
	andl	$-8, %eax
	cmpl	$8, %eax
	movl	%r11d, %r10d
	jne	.LBB3_54
# BB#41:
	movl	%r9d, %eax
	orl	%ebx, %eax
	orl	%r12d, %eax
	orl	%r15d, %eax
	movl	%r11d, %r10d
	jne	.LBB3_54
# BB#42:                                # %.preheader154.i285
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_65
# BB#43:                                # %.preheader154.i285
	testl	%edi, %edi
	jle	.LBB3_65
# BB#44:                                # %.preheader153.us.preheader.i288
	movl	%r8d, %r13d
	movq	%r9, 144(%rsp)          # 8-byte Spill
	movq	%rbx, 152(%rsp)         # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r9d
	decl	%r9d
	incq	%r9
	leaq	-1(%rax), %r8
	movl	%eax, %r10d
	andl	$3, %r10d
	xorl	%esi, %esi
	movq	%rbp, %rbx
	movq	8(%rsp), %r14           # 8-byte Reload
	.p2align	4, 0x90
.LBB3_45:                               # %.preheader153.us.i291
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_47 Depth 2
                                        #     Child Loop BB3_51 Depth 2
	testq	%r10, %r10
	movq	8(%rsp), %rcx           # 8-byte Reload
	je	.LBB3_49
# BB#46:                                # %.prol.preheader408
                                        #   in Loop: Header=BB3_45 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_47:                               #   Parent Loop BB3_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rsi,8), %rax
	movzbl	(%rax,%rbp,2), %eax
	movb	%al, (%rbx,%rbp)
	incq	%rbp
	cmpq	%rbp, %r10
	jne	.LBB3_47
# BB#48:                                # %.prol.loopexit409.unr-lcssa
                                        #   in Loop: Header=BB3_45 Depth=1
	leaq	(%rbx,%rbp), %rdi
	cmpq	$3, %r8
	jae	.LBB3_50
	jmp	.LBB3_52
	.p2align	4, 0x90
.LBB3_49:                               #   in Loop: Header=BB3_45 Depth=1
	xorl	%ebp, %ebp
	movq	%rbx, %rdi
	cmpq	$3, %r8
	jb	.LBB3_52
.LBB3_50:                               # %.preheader153.us.i291.new
                                        #   in Loop: Header=BB3_45 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rbp, %rcx
	addq	%rbp, %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_51:                               #   Parent Loop BB3_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	(%rdx,%rax,2), %edx
	movb	%dl, (%rdi,%rax)
	movq	(%r14,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	2(%rdx,%rax,2), %edx
	movb	%dl, 1(%rdi,%rax)
	movq	(%r14,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	4(%rdx,%rax,2), %edx
	movb	%dl, 2(%rdi,%rax)
	movq	(%r14,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	6(%rdx,%rax,2), %edx
	movb	%dl, 3(%rdi,%rax)
	addq	$4, %rax
	cmpq	%rax, %rcx
	jne	.LBB3_51
.LBB3_52:                               # %._crit_edge162.us.i299
                                        #   in Loop: Header=BB3_45 Depth=1
	addq	%r9, %rbx
	incq	%rsi
	cmpq	48(%rsp), %rsi          # 8-byte Folded Reload
	jne	.LBB3_45
# BB#53:                                # %img2buf.exit314.loopexit356
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	316864(%rax), %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	316868(%rax), %edi
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	152(%rsp), %rbx         # 8-byte Reload
	movq	144(%rsp), %r9          # 8-byte Reload
	movl	%r13d, %r8d
	jmp	.LBB3_65
.LBB3_54:                               # %.preheader.i301
	movl	%edi, %ecx
	subl	%ebx, %ecx
	cmpl	%r9d, %ecx
	jle	.LBB3_65
# BB#55:                                # %.lr.ph158.i302
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %r14d
	subl	%r12d, %r14d
	movl	%r14d, %eax
	subl	%r15d, %eax
	jle	.LBB3_65
# BB#56:                                # %.lr.ph158.split.us.preheader.i305
	movq	%r12, 240(%rsp)         # 8-byte Spill
	movq	%rbx, 152(%rsp)         # 8-byte Spill
	movslq	%r10d, %rdx
	movslq	%r15d, %rdi
	movq	%r9, 144(%rsp)          # 8-byte Spill
	movslq	%r9d, %rbx
	movslq	%r14d, %r8
	movslq	%ecx, %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	movq	%r8, 64(%rsp)           # 8-byte Spill
	leaq	-1(%r8), %rcx
	subq	%rdi, %rcx
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	andl	$3, %eax
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rdi), %rcx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movl	%r11d, %ecx
	imull	%esi, %ecx
	movl	%ecx, 60(%rsp)          # 4-byte Spill
	movq	%rax, %rcx
	negq	%rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
                                        # kill: %R15D<def> %R15D<kill> %R15<kill> %R15<def>
	negl	%r15d
	movq	%r15, 72(%rsp)          # 8-byte Spill
	leal	(,%r11,4), %ecx
	movl	%ecx, 176(%rsp)         # 4-byte Spill
	movl	$0, 80(%rsp)            # 4-byte Folded Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rax, 200(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_57:                               # %.lr.ph158.split.us.i307
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_59 Depth 2
                                        #     Child Loop BB3_62 Depth 2
	testq	%rax, %rax
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	%rbp, %r13
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rbx, %rsi
	movq	%rsi, (%rsp)            # 8-byte Spill
	je	.LBB3_60
# BB#58:                                # %.prol.preheader403
                                        #   in Loop: Header=BB3_57 Depth=1
	movq	104(%rsp), %r14         # 8-byte Reload
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	movq	168(%rsp), %rbx         # 8-byte Reload
	movq	112(%rsp), %r15         # 8-byte Reload
	.p2align	4, 0x90
.LBB3_59:                               #   Parent Loop BB3_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rbp
	leaq	(%r13,%rbp), %rdi
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rcx,%rax,8), %rsi
	addq	%rbx, %rsi
	movq	%r11, %r12
	callq	memcpy
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %r11
	incq	%r15
	addq	$2, %rbx
	addl	%r11d, %ebp
	incq	%r14
	jne	.LBB3_59
.LBB3_60:                               # %.prol.loopexit404
                                        #   in Loop: Header=BB3_57 Depth=1
	cmpq	$3, 184(%rsp)           # 8-byte Folded Reload
	movl	176(%rsp), %ebp         # 4-byte Reload
	jb	.LBB3_63
# BB#61:                                # %.lr.ph158.split.us.i307.new
                                        #   in Loop: Header=BB3_57 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
	subq	%r15, %r12
	leaq	6(%r15,%r15), %rbx
	movq	72(%rsp), %rdi          # 8-byte Reload
	leal	3(%rdi,%r15), %eax
	movq	120(%rsp), %r9          # 8-byte Reload
	imull	%r9d, %eax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	leal	2(%rdi,%r15), %eax
	imull	%r9d, %eax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	leal	1(%rdi,%r15), %eax
	imull	%r9d, %eax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	addl	%edi, %r15d
	imull	%r9d, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_62:                               #   Parent Loop BB3_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r15,%r14), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	movq	(%rcx,%rsi,8), %rax
	leaq	-6(%rax,%rbx), %rsi
	callq	memcpy
	movq	208(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	leaq	-4(%rax,%rbx), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	216(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	leaq	-2(%rax,%rbx), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	224(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rax,%rcx,8), %rsi
	addq	%rbx, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	addq	$8, %rbx
	addl	%ebp, %r14d
	addq	$-4, %r12
	jne	.LBB3_62
.LBB3_63:                               # %._crit_edge.us.i313
                                        #   in Loop: Header=BB3_57 Depth=1
	incq	%rsi
	movl	80(%rsp), %eax          # 4-byte Reload
	addl	60(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 80(%rsp)          # 4-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	addl	136(%rsp), %eax         # 4-byte Folded Reload
	movq	%rax, 72(%rsp)          # 8-byte Spill
	cmpq	192(%rsp), %rsi         # 8-byte Folded Reload
	movq	120(%rsp), %r11         # 8-byte Reload
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	%rsi, %rbx
	movq	%r13, %rbp
	jne	.LBB3_57
# BB#64:
	movl	28(%rsp), %r8d          # 4-byte Reload
	movq	152(%rsp), %rbx         # 8-byte Reload
	movq	240(%rsp), %r12         # 8-byte Reload
	movq	128(%rsp), %r15         # 8-byte Reload
	movq	144(%rsp), %r9          # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
.LBB3_65:                               # %img2buf.exit314
	leal	(%r9,%rbx), %r13d
	subl	%r13d, %edi
	leal	(%r15,%r12), %r14d
	movq	32(%rsp), %rax          # 8-byte Reload
	subl	%r14d, %eax
	imull	%r11d, %edi
	imull	%eax, %edi
	movslq	%edi, %rdx
	movl	%r8d, %edi
	movq	%rbx, %r15
	movq	%rbp, %rsi
	movq	%r9, %rbx
	callq	write
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 317044(%rax)
	je	.LBB3_79
# BB#66:
	movq	40(%rsp), %rdx          # 8-byte Reload
	movslq	317056(%rdx), %r13
	movl	317060(%rdx), %r14d
	movslq	317048(%rdx), %rax
	movl	$2, %ecx
	subq	%rax, %rcx
	movslq	317064(%rdx), %r15
	imulq	%rcx, %r15
	movl	317068(%rdx), %r9d
	imull	%ecx, %r9d
	movq	316928(%rdx), %rax
	movq	(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	316872(%rdx), %eax
	movl	316876(%rdx), %esi
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%eax, %ebx
	subl	%r13d, %ebx
	subl	%r14d, %ebx
	movq	120(%rsp), %r8          # 8-byte Reload
	cmpl	$3, %r8d
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	jb	.LBB3_99
# BB#67:                                # %.thread.i249
	leal	(%r9,%r15), %eax
	movl	%esi, %edx
	subl	%eax, %edx
	movl	%ebx, %eax
	imull	%r8d, %eax
	imull	%edx, %eax
	movslq	%eax, %rdx
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movq	%r8, %r12
	callq	memset
	movq	128(%rsp), %r9          # 8-byte Reload
	movq	%r12, %r8
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	$2, %edx
	movl	28(%rsp), %ecx          # 4-byte Reload
.LBB3_68:                               # %.preheader.i268
	movl	%esi, %eax
	subl	%r9d, %eax
	cmpl	%r15d, %eax
	jle	.LBB3_113
# BB#69:                                # %.lr.ph158.i269
	movq	32(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	subl	%r14d, %edi
	cmpl	%r13d, %edi
	jle	.LBB3_113
# BB#70:                                # %.lr.ph158.split.us.preheader.i272
	movq	%r14, 152(%rsp)         # 8-byte Spill
	movslq	%edx, %rdx
	movslq	%edi, %rsi
	cltq
	movq	%rax, 192(%rsp)         # 8-byte Spill
	subl	%r13d, %edi
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	leaq	-1(%rsi), %rax
	subq	%r13, %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	andl	$3, %edi
	leaq	(%r13,%r13), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movl	%r8d, %eax
	imull	%ebx, %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movq	%rdi, %rax
	negq	%rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	%r13d, %eax
	negl	%eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leal	(,%r8,4), %eax
	movl	%eax, 176(%rsp)         # 4-byte Spill
	movl	$0, 72(%rsp)            # 4-byte Folded Spill
	movq	%r15, 144(%rsp)         # 8-byte Spill
	movq	%r15, %rax
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%r13, 80(%rsp)          # 8-byte Spill
	movl	%ebx, 136(%rsp)         # 4-byte Spill
	movq	%rdi, 200(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_71:                               # %.lr.ph158.split.us.i274
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_73 Depth 2
                                        #     Child Loop BB3_76 Depth 2
	testq	%rdi, %rdi
	movq	%rbp, %r15
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, %rsi
	movq	%rsi, (%rsp)            # 8-byte Spill
	je	.LBB3_74
# BB#72:                                # %.prol.preheader393
                                        #   in Loop: Header=BB3_71 Depth=1
	movq	104(%rsp), %r14         # 8-byte Reload
	movl	72(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	movq	168(%rsp), %rbx         # 8-byte Reload
	movq	80(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_73:                               #   Parent Loop BB3_71 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rbp
	leaq	(%r15,%rbp), %rdi
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rcx,%rax,8), %rsi
	addq	%rbx, %rsi
	movq	%r8, %r12
	callq	memcpy
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %r8
	incq	%r13
	addq	$2, %rbx
	addl	%r8d, %ebp
	incq	%r14
	jne	.LBB3_73
.LBB3_74:                               # %.prol.loopexit394
                                        #   in Loop: Header=BB3_71 Depth=1
	cmpq	$3, 184(%rsp)           # 8-byte Folded Reload
	movl	176(%rsp), %ebp         # 4-byte Reload
	jb	.LBB3_77
# BB#75:                                # %.lr.ph158.split.us.i274.new
                                        #   in Loop: Header=BB3_71 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
	subq	%r13, %r12
	leaq	6(%r13,%r13), %rbx
	movq	112(%rsp), %rdi         # 8-byte Reload
	leal	3(%rdi,%r13), %eax
	movq	120(%rsp), %r8          # 8-byte Reload
	imull	%r8d, %eax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	leal	2(%rdi,%r13), %eax
	imull	%r8d, %eax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	leal	1(%rdi,%r13), %eax
	imull	%r8d, %eax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	addl	%edi, %r13d
	imull	%r8d, %r13d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_76:                               #   Parent Loop BB3_71 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r13,%r14), %eax
	movslq	%eax, %rdi
	addq	%r15, %rdi
	movq	(%rcx,%rsi,8), %rax
	leaq	-6(%rax,%rbx), %rsi
	callq	memcpy
	movq	208(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%r15, %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	leaq	-4(%rax,%rbx), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	216(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%r15, %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	leaq	-2(%rax,%rbx), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	224(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%r15, %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rax,%rcx,8), %rsi
	addq	%rbx, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	addq	$8, %rbx
	addl	%ebp, %r14d
	addq	$-4, %r12
	jne	.LBB3_76
.LBB3_77:                               # %._crit_edge.us.i280
                                        #   in Loop: Header=BB3_71 Depth=1
	incq	%rsi
	movl	72(%rsp), %eax          # 4-byte Reload
	addl	60(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	136(%rsp), %ebx         # 4-byte Reload
	movq	112(%rsp), %rax         # 8-byte Reload
	addl	%ebx, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	cmpq	192(%rsp), %rsi         # 8-byte Folded Reload
	movq	120(%rsp), %r8          # 8-byte Reload
	movq	80(%rsp), %r13          # 8-byte Reload
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	%r15, %rbp
	movq	%rsi, %rax
	jne	.LBB3_71
# BB#78:
	movl	28(%rsp), %ecx          # 4-byte Reload
	movq	152(%rsp), %r14         # 8-byte Reload
	movq	144(%rsp), %r15         # 8-byte Reload
	movq	128(%rsp), %r9          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB3_113
.LBB3_79:
	movl	%r13d, 96(%rsp)         # 4-byte Spill
	movl	%r14d, 64(%rsp)         # 4-byte Spill
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	movq	%r12, 240(%rsp)         # 8-byte Spill
	movq	%r15, 152(%rsp)         # 8-byte Spill
	movq	input(%rip), %rax
	cmpl	$0, 3012(%rax)
	je	.LBB3_170
# BB#80:
	movq	img(%rip), %rax
	movl	5876(%rax), %ecx
	decl	%ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	movq	40(%rsp), %rbp          # 8-byte Reload
	leaq	316928(%rbp), %r15
	movl	316864(%rbp), %eax
	movl	316868(%rbp), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movl	$1, %esi
	movq	%r15, %rdi
	callq	get_mem3Dpel
	movl	316864(%rbp), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movl	316868(%rbp), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	movl	%eax, 236(%rsp)         # 4-byte Spill
	cmpl	$2, %eax
	jl	.LBB3_97
# BB#81:
	cmpl	$2, 48(%rsp)            # 4-byte Folded Reload
	jl	.LBB3_97
# BB#82:                                # %.preheader.us.preheader
	movslq	(%rsp), %r11            # 4-byte Folded Reload
	movq	(%r15), %rax
	movq	(%rax), %r14
	movl	48(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	movslq	%eax, %rdx
	testq	%rdx, %rdx
	movl	$1, %r12d
	cmovgq	%rdx, %r12
	movabsq	$9223372036854775792, %rcx # imm = 0x7FFFFFFFFFFFFFF0
	andq	%r12, %rcx
	leaq	-16(%rcx), %r8
	movl	%r8d, %r10d
	shrl	$4, %r10d
	incl	%r10d
	movd	%ebx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	andl	$7, %r10d
	movq	%r10, %r9
	negq	%r9
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_83:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_88 Depth 2
                                        #     Child Loop BB3_93 Depth 2
                                        #     Child Loop BB3_95 Depth 2
	cmpq	$16, %r12
	movq	(%r14,%rbp,8), %rax
	jae	.LBB3_85
# BB#84:                                #   in Loop: Header=BB3_83 Depth=1
	xorl	%esi, %esi
	jmp	.LBB3_95
	.p2align	4, 0x90
.LBB3_85:                               # %min.iters.checked
                                        #   in Loop: Header=BB3_83 Depth=1
	testq	%rcx, %rcx
	je	.LBB3_89
# BB#86:                                # %vector.ph
                                        #   in Loop: Header=BB3_83 Depth=1
	testq	%r10, %r10
	je	.LBB3_90
# BB#87:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB3_83 Depth=1
	movq	%r9, %rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_88:                               # %vector.body.prol
                                        #   Parent Loop BB3_83 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, (%rax,%rsi,2)
	movdqu	%xmm0, 16(%rax,%rsi,2)
	addq	$16, %rsi
	incq	%rdi
	jne	.LBB3_88
	jmp	.LBB3_91
.LBB3_89:                               #   in Loop: Header=BB3_83 Depth=1
	xorl	%esi, %esi
	jmp	.LBB3_95
.LBB3_90:                               #   in Loop: Header=BB3_83 Depth=1
	xorl	%esi, %esi
.LBB3_91:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB3_83 Depth=1
	cmpq	$112, %r8
	jb	.LBB3_94
# BB#92:                                # %vector.ph.new
                                        #   in Loop: Header=BB3_83 Depth=1
	movq	%rcx, %rdi
	subq	%rsi, %rdi
	leaq	240(%rax,%rsi,2), %rsi
	.p2align	4, 0x90
.LBB3_93:                               # %vector.body
                                        #   Parent Loop BB3_83 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -240(%rsi)
	movdqu	%xmm0, -224(%rsi)
	movdqu	%xmm0, -208(%rsi)
	movdqu	%xmm0, -192(%rsi)
	movdqu	%xmm0, -176(%rsi)
	movdqu	%xmm0, -160(%rsi)
	movdqu	%xmm0, -144(%rsi)
	movdqu	%xmm0, -128(%rsi)
	movdqu	%xmm0, -112(%rsi)
	movdqu	%xmm0, -96(%rsi)
	movdqu	%xmm0, -80(%rsi)
	movdqu	%xmm0, -64(%rsi)
	movdqu	%xmm0, -48(%rsi)
	movdqu	%xmm0, -32(%rsi)
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	addq	$256, %rsi              # imm = 0x100
	addq	$-128, %rdi
	jne	.LBB3_93
.LBB3_94:                               # %middle.block
                                        #   in Loop: Header=BB3_83 Depth=1
	cmpq	%rcx, %r12
	movq	%rcx, %rsi
	je	.LBB3_96
	.p2align	4, 0x90
.LBB3_95:                               # %scalar.ph
                                        #   Parent Loop BB3_83 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%bx, (%rax,%rsi,2)
	incq	%rsi
	cmpq	%rdx, %rsi
	jl	.LBB3_95
.LBB3_96:                               # %._crit_edge.us
                                        #   in Loop: Header=BB3_83 Depth=1
	incq	%rbp
	cmpq	%r11, %rbp
	jl	.LBB3_83
.LBB3_97:                               # %._crit_edge334
	movq	(%r15), %rax
	movq	(%rax), %rdi
	movl	48(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r15d
	shrl	$31, %r15d
	addl	%eax, %r15d
	sarl	%r15d
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	%eax, %esi
	shrl	$31, %esi
	addl	%eax, %esi
	sarl	%esi
	movq	240(%rsp), %rax         # 8-byte Reload
	movl	%eax, %r14d
	shrl	$31, %r14d
	addl	%eax, %r14d
	sarl	%r14d
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	%eax, %ebp
	shrl	$31, %ebp
	addl	%eax, %ebp
	sarl	%ebp
	movq	152(%rsp), %rax         # 8-byte Reload
	movl	%eax, %ebx
	shrl	$31, %ebx
	addl	%eax, %ebx
	sarl	%ebx
	movl	%r15d, %r8d
	subl	%esi, %r8d
	subl	%r14d, %r8d
	movq	120(%rsp), %r12         # 8-byte Reload
	cmpl	$3, %r12d
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	%r8d, 136(%rsp)         # 4-byte Spill
	jb	.LBB3_144
# BB#98:                                # %.thread.i183
	leal	(%rbp,%rbx), %eax
	movl	(%rsp), %ecx            # 4-byte Reload
	subl	%eax, %ecx
	imull	%r12d, %ecx
	imull	%r8d, %ecx
	movq	%rsi, %r13
	movslq	%ecx, %rdx
	xorl	%esi, %esi
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	memset
	movl	136(%rsp), %r8d         # 4-byte Reload
	movq	%r13, %rsi
	movl	$2, %eax
	jmp	.LBB3_157
.LBB3_99:
	movl	164(%rsp), %eax         # 4-byte Reload
	andl	$-8, %eax
	cmpl	$8, %eax
	movl	%r8d, %edx
	movl	28(%rsp), %ecx          # 4-byte Reload
	jne	.LBB3_68
# BB#100:
	movl	%r14d, %eax
	orl	%r13d, %eax
	orl	%r15d, %eax
	orl	%r9d, %eax
	movl	%r8d, %edx
	jne	.LBB3_68
# BB#101:                               # %.preheader154.i252
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_113
# BB#102:                               # %.preheader154.i252
	testl	%esi, %esi
	jle	.LBB3_113
# BB#103:                               # %.preheader153.us.preheader.i255
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r9d
	decl	%r9d
	incq	%r9
	leaq	-1(%rax), %rcx
	movl	%eax, %r10d
	andl	$3, %r10d
	xorl	%esi, %esi
	movq	%rbp, %r11
	movq	8(%rsp), %r12           # 8-byte Reload
	.p2align	4, 0x90
.LBB3_104:                              # %.preheader153.us.i258
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_106 Depth 2
                                        #     Child Loop BB3_110 Depth 2
	testq	%r10, %r10
	movq	8(%rsp), %rdx           # 8-byte Reload
	je	.LBB3_108
# BB#105:                               # %.prol.preheader398
                                        #   in Loop: Header=BB3_104 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_106:                              #   Parent Loop BB3_104 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rsi,8), %rax
	movzbl	(%rax,%rbp,2), %eax
	movb	%al, (%r11,%rbp)
	incq	%rbp
	cmpq	%rbp, %r10
	jne	.LBB3_106
# BB#107:                               # %.prol.loopexit399.unr-lcssa
                                        #   in Loop: Header=BB3_104 Depth=1
	leaq	(%r11,%rbp), %rdi
	cmpq	$3, %rcx
	jae	.LBB3_109
	jmp	.LBB3_111
	.p2align	4, 0x90
.LBB3_108:                              #   in Loop: Header=BB3_104 Depth=1
	xorl	%ebp, %ebp
	movq	%r11, %rdi
	cmpq	$3, %rcx
	jb	.LBB3_111
.LBB3_109:                              # %.preheader153.us.i258.new
                                        #   in Loop: Header=BB3_104 Depth=1
	movq	32(%rsp), %rbx          # 8-byte Reload
	subq	%rbp, %rbx
	addq	%rbp, %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_110:                              #   Parent Loop BB3_104 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	(%rdx,%rax,2), %edx
	movb	%dl, (%rdi,%rax)
	movq	(%r12,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	2(%rdx,%rax,2), %edx
	movb	%dl, 1(%rdi,%rax)
	movq	(%r12,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	4(%rdx,%rax,2), %edx
	movb	%dl, 2(%rdi,%rax)
	movq	(%r12,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	6(%rdx,%rax,2), %edx
	movb	%dl, 3(%rdi,%rax)
	addq	$4, %rax
	cmpq	%rax, %rbx
	jne	.LBB3_110
.LBB3_111:                              # %._crit_edge162.us.i266
                                        #   in Loop: Header=BB3_104 Depth=1
	addq	%r9, %r11
	incq	%rsi
	cmpq	48(%rsp), %rsi          # 8-byte Folded Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	jne	.LBB3_104
# BB#112:                               # %img2buf.exit281.loopexit355
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	316872(%rax), %edx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movl	316876(%rax), %esi
	movq	128(%rsp), %r9          # 8-byte Reload
	movl	28(%rsp), %ecx          # 4-byte Reload
.LBB3_113:                              # %img2buf.exit281
	leal	(%r9,%r15), %eax
	movl	%eax, 168(%rsp)         # 4-byte Spill
	subl	%eax, %esi
	leal	(%r14,%r13), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movq	32(%rsp), %rdx          # 8-byte Reload
	subl	%eax, %edx
	imull	%r8d, %esi
	imull	%edx, %esi
	movslq	%esi, %rdx
	movl	%ecx, %edi
	movq	%rbp, %rsi
	movq	%r8, %r12
	callq	write
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	je	.LBB3_170
# BB#114:
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	316928(%rcx), %rax
	movq	8(%rax), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	316872(%rcx), %eax
	movl	316876(%rcx), %ecx
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	%eax, %ebx
	subl	%r13d, %ebx
	subl	%r14d, %ebx
	cmpl	$3, %r12d
	movq	%r12, %r11
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	jb	.LBB3_130
# BB#115:                               # %.thread.i216
	movl	%ecx, %eax
	subl	168(%rsp), %eax         # 4-byte Folded Reload
	movl	%ebx, %ecx
	imull	%r11d, %ecx
	imull	%eax, %ecx
	movslq	%ecx, %rdx
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movq	%r11, %r12
	callq	memset
	movq	%r12, %r11
	movl	$2, %eax
	movq	128(%rsp), %rdx         # 8-byte Reload
	movl	28(%rsp), %edi          # 4-byte Reload
.LBB3_116:                              # %.preheader.i235
	movq	104(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%edx, %ecx
	cmpl	%r15d, %ecx
	jle	.LBB3_127
# BB#117:                               # %.lr.ph158.i236
	movq	96(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %esi
	subl	%r14d, %esi
	cmpl	%r13d, %esi
	jle	.LBB3_127
# BB#118:                               # %.lr.ph158.split.us.preheader.i239
	movslq	%eax, %rdx
	movslq	%r15d, %rdi
	movslq	%esi, %r8
	movslq	%ecx, %rcx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	subl	%r13d, %esi
	movq	%r8, 32(%rsp)           # 8-byte Spill
	leaq	-1(%r8), %rcx
	subq	%r13, %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	andl	$3, %esi
	leaq	(%r13,%r13), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movl	%r11d, %ecx
	imull	%ebx, %ecx
	movl	%ecx, 184(%rsp)         # 4-byte Spill
	movq	%rsi, %rcx
	negq	%rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%r13, 80(%rsp)          # 8-byte Spill
	movl	%r13d, %eax
	negl	%eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leal	(,%r11,4), %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movl	$0, 72(%rsp)            # 4-byte Folded Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%ebx, 112(%rsp)         # 4-byte Spill
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_119:                              # %.lr.ph158.split.us.i241
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_121 Depth 2
                                        #     Child Loop BB3_124 Depth 2
	testq	%rsi, %rsi
	movq	80(%rsp), %r15          # 8-byte Reload
	movq	%rbp, %r13
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rdi, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	je	.LBB3_122
# BB#120:                               # %.prol.preheader383
                                        #   in Loop: Header=BB3_119 Depth=1
	movq	48(%rsp), %r14          # 8-byte Reload
	movl	72(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	movq	176(%rsp), %rbx         # 8-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_121:                              #   Parent Loop BB3_119 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rbp
	leaq	(%r13,%rbp), %rdi
	movq	(%rsi,%rcx,8), %rsi
	addq	%rbx, %rsi
	movq	%r11, %r12
	callq	memcpy
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %r11
	incq	%r15
	addq	$2, %rbx
	addl	%r11d, %ebp
	incq	%r14
	jne	.LBB3_121
.LBB3_122:                              # %.prol.loopexit384
                                        #   in Loop: Header=BB3_119 Depth=1
	cmpq	$3, 192(%rsp)           # 8-byte Folded Reload
	movl	60(%rsp), %ebp          # 4-byte Reload
	jb	.LBB3_125
# BB#123:                               # %.lr.ph158.split.us.i241.new
                                        #   in Loop: Header=BB3_119 Depth=1
	movq	32(%rsp), %r12          # 8-byte Reload
	subq	%r15, %r12
	leaq	6(%r15,%r15), %rbx
	movq	40(%rsp), %rdi          # 8-byte Reload
	leal	3(%rdi,%r15), %eax
	movq	120(%rsp), %r9          # 8-byte Reload
	imull	%r9d, %eax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	leal	2(%rdi,%r15), %eax
	imull	%r9d, %eax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	leal	1(%rdi,%r15), %eax
	imull	%r9d, %eax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	addl	%edi, %r15d
	imull	%r9d, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_124:                              #   Parent Loop BB3_119 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r15,%r14), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	movq	(%rsi,%rcx,8), %rax
	leaq	-6(%rax,%rbx), %rsi
	callq	memcpy
	movq	208(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	leaq	-4(%rax,%rbx), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	216(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	leaq	-2(%rax,%rbx), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	224(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movslq	%eax, %rdi
	addq	%r13, %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rcx,%rax,8), %rsi
	addq	%rbx, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	addq	$8, %rbx
	addl	%ebp, %r14d
	addq	$-4, %r12
	jne	.LBB3_124
.LBB3_125:                              # %._crit_edge.us.i247
                                        #   in Loop: Header=BB3_119 Depth=1
	incq	%rcx
	movl	72(%rsp), %eax          # 4-byte Reload
	addl	184(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	112(%rsp), %ebx         # 4-byte Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	addl	%ebx, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpq	200(%rsp), %rcx         # 8-byte Folded Reload
	movq	120(%rsp), %r11         # 8-byte Reload
	movq	136(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdi
	movq	%r13, %rbp
	jne	.LBB3_119
# BB#126:
	movl	28(%rsp), %edi          # 4-byte Reload
.LBB3_127:
	movl	64(%rsp), %r14d         # 4-byte Reload
	movq	96(%rsp), %r15          # 8-byte Reload
.LBB3_128:                              # %img2buf.exit248
	movq	104(%rsp), %r13         # 8-byte Reload
.LBB3_129:                              # %img2buf.exit248
	subl	168(%rsp), %r13d        # 4-byte Folded Reload
	subl	%r14d, %r15d
	imull	%r11d, %r13d
	imull	%r15d, %r13d
	movslq	%r13d, %rdx
	movq	%rbp, %rsi
	callq	write
	jmp	.LBB3_170
.LBB3_130:
	movl	164(%rsp), %eax         # 4-byte Reload
	andl	$-8, %eax
	cmpl	$8, %eax
	movl	%r11d, %eax
	movq	128(%rsp), %rdx         # 8-byte Reload
	movl	28(%rsp), %edi          # 4-byte Reload
	jne	.LBB3_116
# BB#131:
	movl	%r14d, %eax
	orl	%r13d, %eax
	orl	%r15d, %eax
	orl	%edx, %eax
	movl	%r11d, %eax
	jne	.LBB3_116
# BB#132:                               # %.preheader154.i219
	movq	96(%rsp), %r15          # 8-byte Reload
	testl	%r15d, %r15d
	jle	.LBB3_171
# BB#133:                               # %.preheader154.i219
	movq	104(%rsp), %r13         # 8-byte Reload
	testl	%r13d, %r13d
	jle	.LBB3_172
# BB#134:                               # %.preheader153.us.preheader.i222
	movl	%r15d, %r9d
	decl	%r9d
	incq	%r9
	leaq	-1(%r15), %r8
	movl	%r15d, %r10d
	andl	$3, %r10d
	xorl	%esi, %esi
	movq	%rbp, %rbx
	movl	64(%rsp), %r14d         # 4-byte Reload
	movq	(%rsp), %r12            # 8-byte Reload
	.p2align	4, 0x90
.LBB3_135:                              # %.preheader153.us.i225
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_137 Depth 2
                                        #     Child Loop BB3_141 Depth 2
	testq	%r10, %r10
	movq	(%rsp), %rcx            # 8-byte Reload
	je	.LBB3_139
# BB#136:                               # %.prol.preheader388
                                        #   in Loop: Header=BB3_135 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_137:                              #   Parent Loop BB3_135 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rsi,8), %rax
	movzbl	(%rax,%rbp,2), %eax
	movb	%al, (%rbx,%rbp)
	incq	%rbp
	cmpq	%rbp, %r10
	jne	.LBB3_137
# BB#138:                               # %.prol.loopexit389.unr-lcssa
                                        #   in Loop: Header=BB3_135 Depth=1
	leaq	(%rbx,%rbp), %rdi
	cmpq	$3, %r8
	jae	.LBB3_140
	jmp	.LBB3_142
	.p2align	4, 0x90
.LBB3_139:                              #   in Loop: Header=BB3_135 Depth=1
	xorl	%ebp, %ebp
	movq	%rbx, %rdi
	cmpq	$3, %r8
	jb	.LBB3_142
.LBB3_140:                              # %.preheader153.us.i225.new
                                        #   in Loop: Header=BB3_135 Depth=1
	movq	%r15, %rcx
	subq	%rbp, %rcx
	addq	%rbp, %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_141:                              #   Parent Loop BB3_135 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	(%rdx,%rax,2), %edx
	movb	%dl, (%rdi,%rax)
	movq	(%r12,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	2(%rdx,%rax,2), %edx
	movb	%dl, 1(%rdi,%rax)
	movq	(%r12,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	4(%rdx,%rax,2), %edx
	movb	%dl, 2(%rdi,%rax)
	movq	(%r12,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzbl	6(%rdx,%rax,2), %edx
	movb	%dl, 3(%rdi,%rax)
	addq	$4, %rax
	cmpq	%rax, %rcx
	jne	.LBB3_141
.LBB3_142:                              # %._crit_edge162.us.i233
                                        #   in Loop: Header=BB3_135 Depth=1
	addq	%r9, %rbx
	incq	%rsi
	cmpq	%r13, %rsi
	movq	88(%rsp), %rbp          # 8-byte Reload
	jne	.LBB3_135
# BB#143:                               # %img2buf.exit248.loopexit354
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	316872(%rax), %r15d
	movl	316876(%rax), %r13d
	movl	28(%rsp), %edi          # 4-byte Reload
	jmp	.LBB3_129
.LBB3_144:
	movl	164(%rsp), %eax         # 4-byte Reload
	andl	$-8, %eax
	cmpl	$8, %eax
	movl	%r12d, %eax
	jne	.LBB3_157
# BB#145:
	movl	%ebp, %eax
	orl	%ebx, %eax
	orl	%r14d, %eax
	orl	%esi, %eax
	movl	%r12d, %eax
	jne	.LBB3_157
# BB#146:                               # %.preheader154.i186
	movl	236(%rsp), %ecx         # 4-byte Reload
	cmpl	$2, %ecx
	movq	40(%rsp), %r13          # 8-byte Reload
	movl	64(%rsp), %r14d         # 4-byte Reload
	movl	96(%rsp), %eax          # 4-byte Reload
	jl	.LBB3_173
# BB#147:                               # %.preheader154.i186
	cmpl	$2, 48(%rsp)            # 4-byte Folded Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	jl	.LBB3_169
# BB#148:                               # %.preheader153.us.preheader.i189
	leal	-1(%r15), %r10d
	incq	%r10
	movl	%r15d, %r8d
	movl	(%rsp), %r9d            # 4-byte Reload
	leaq	-1(%r8), %r11
	movl	%r8d, %r14d
	andl	$3, %r14d
	movq	%rbp, %rax
	xorl	%ebp, %ebp
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB3_149:                              # %.preheader153.us.i192
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_150 Depth 2
                                        #     Child Loop BB3_154 Depth 2
	xorl	%ecx, %ecx
	testq	%r14, %r14
	je	.LBB3_152
	.p2align	4, 0x90
.LBB3_150:                              #   Parent Loop BB3_149 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rbp,8), %rax
	movzbl	(%rax,%rcx,2), %eax
	movb	%al, (%r15,%rcx)
	incq	%rcx
	cmpq	%rcx, %r14
	jne	.LBB3_150
# BB#151:                               # %.prol.loopexit376.unr-lcssa
                                        #   in Loop: Header=BB3_149 Depth=1
	leaq	(%r15,%rcx), %rbx
	cmpq	$3, %r11
	jae	.LBB3_153
	jmp	.LBB3_155
.LBB3_152:                              #   in Loop: Header=BB3_149 Depth=1
	movq	%r15, %rbx
	cmpq	$3, %r11
	jb	.LBB3_155
.LBB3_153:                              # %.preheader153.us.i192.new
                                        #   in Loop: Header=BB3_149 Depth=1
	movq	%r8, %rax
	subq	%rcx, %rax
	addq	%rcx, %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_154:                              #   Parent Loop BB3_149 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%rbp,8), %rdi
	addq	%rcx, %rdi
	movzbl	(%rdi,%rsi,2), %edx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movb	%dl, (%rbx,%rsi)
	movq	(%rdi,%rbp,8), %rdx
	addq	%rcx, %rdx
	movzbl	2(%rdx,%rsi,2), %edx
	movb	%dl, 1(%rbx,%rsi)
	movq	(%rdi,%rbp,8), %rdx
	addq	%rcx, %rdx
	movzbl	4(%rdx,%rsi,2), %edx
	movb	%dl, 2(%rbx,%rsi)
	movq	(%rdi,%rbp,8), %rdx
	addq	%rcx, %rdx
	movzbl	6(%rdx,%rsi,2), %edx
	movb	%dl, 3(%rbx,%rsi)
	addq	$4, %rsi
	cmpq	%rsi, %rax
	jne	.LBB3_154
.LBB3_155:                              # %._crit_edge162.us.i200
                                        #   in Loop: Header=BB3_149 Depth=1
	addq	%r10, %r15
	incq	%rbp
	cmpq	%r9, %rbp
	jne	.LBB3_149
# BB#156:                               # %img2buf.exit215.loopexit352
	movq	40(%rsp), %r13          # 8-byte Reload
	movl	316864(%r13), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movl	316868(%r13), %ecx
	movq	88(%rsp), %rbp          # 8-byte Reload
	movl	64(%rsp), %r14d         # 4-byte Reload
	movl	96(%rsp), %eax          # 4-byte Reload
	jmp	.LBB3_169
.LBB3_157:                              # %.preheader.i202
	movl	(%rsp), %r13d           # 4-byte Reload
	subl	%ebx, %r13d
	movl	%r13d, %ecx
	cmpl	%ebp, %r13d
	jle	.LBB3_168
# BB#158:                               # %.lr.ph158.i203
	subl	%r14d, %r15d
	movl	%r15d, %edi
	subl	%esi, %edi
	jle	.LBB3_168
# BB#159:                               # %.lr.ph158.split.us.preheader.i206
	movslq	%eax, %rdx
	movslq	%esi, %rax
	movslq	%ebp, %rbp
	movslq	%r15d, %rbx
	movslq	%ecx, %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	leaq	-1(%rbx), %rcx
	subq	%rax, %rcx
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	andl	$3, %edi
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	(%rax,%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%r12d, %eax
	imull	%r8d, %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movq	%rdi, %rax
	negq	%rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	negl	%esi
	leal	(,%r12,4), %eax
	movl	%eax, 176(%rsp)         # 4-byte Spill
	movl	$0, 80(%rsp)            # 4-byte Folded Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rdi, 200(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_160:                              # %.lr.ph158.split.us.i208
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_162 Depth 2
                                        #     Child Loop BB3_165 Depth 2
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	testq	%rdi, %rdi
	movq	112(%rsp), %r12         # 8-byte Reload
	movq	120(%rsp), %r13         # 8-byte Reload
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, %r8
	movq	%r8, (%rsp)             # 8-byte Spill
	movq	%rcx, %r14
	je	.LBB3_163
# BB#161:                               # %.prol.preheader
                                        #   in Loop: Header=BB3_160 Depth=1
	movq	168(%rsp), %r15         # 8-byte Reload
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	112(%rsp), %r12         # 8-byte Reload
	.p2align	4, 0x90
.LBB3_162:                              #   Parent Loop BB3_160 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rbp
	leaq	(%rsi,%rbp), %rdi
	movq	(%rcx,%r8,8), %rsi
	addq	%rbx, %rsi
	callq	memcpy
	movq	%r14, %rcx
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	(%rsp), %r8             # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	incq	%r12
	addq	$2, %rbx
	addl	%r13d, %ebp
	incq	%r15
	jne	.LBB3_162
.LBB3_163:                              # %.prol.loopexit
                                        #   in Loop: Header=BB3_160 Depth=1
	cmpq	$3, 184(%rsp)           # 8-byte Folded Reload
	movl	176(%rsp), %ebx         # 4-byte Reload
	jb	.LBB3_166
# BB#164:                               # %.lr.ph158.split.us.i208.new
                                        #   in Loop: Header=BB3_160 Depth=1
	movq	104(%rsp), %r13         # 8-byte Reload
	subq	%r12, %r13
	leaq	6(%r12,%r12), %rbp
	movq	72(%rsp), %rdi          # 8-byte Reload
	leal	3(%rdi,%r12), %eax
	movq	120(%rsp), %r9          # 8-byte Reload
	imull	%r9d, %eax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	leal	2(%rdi,%r12), %eax
	imull	%r9d, %eax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	leal	1(%rdi,%r12), %eax
	imull	%r9d, %eax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	addl	%edi, %r12d
	imull	%r9d, %r12d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_165:                              #   Parent Loop BB3_160 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r12,%r15), %eax
	movslq	%eax, %rdi
	addq	%rsi, %rdi
	movq	(%rcx,%r8,8), %rax
	leaq	-6(%rax,%rbp), %rsi
	callq	memcpy
	movq	208(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r15), %eax
	movslq	%eax, %rdi
	addq	88(%rsp), %rdi          # 8-byte Folded Reload
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%r14,%rax,8), %rax
	leaq	-4(%rax,%rbp), %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	memcpy
	movq	216(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r15), %eax
	movslq	%eax, %rdi
	addq	88(%rsp), %rdi          # 8-byte Folded Reload
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%r14,%rax,8), %rax
	leaq	-2(%rax,%rbp), %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	memcpy
	movq	224(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r15), %eax
	movslq	%eax, %rdi
	addq	88(%rsp), %rdi          # 8-byte Folded Reload
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%r14,%rax,8), %rsi
	addq	%rbp, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	memcpy
	movq	%r14, %rcx
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	(%rsp), %r8             # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	addq	$8, %rbp
	addl	%ebx, %r15d
	addq	$-4, %r13
	jne	.LBB3_165
.LBB3_166:                              # %._crit_edge.us.i214
                                        #   in Loop: Header=BB3_160 Depth=1
	incq	%r8
	movl	80(%rsp), %eax          # 4-byte Reload
	addl	60(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 80(%rsp)          # 4-byte Spill
	movq	72(%rsp), %rsi          # 8-byte Reload
	addl	136(%rsp), %esi         # 4-byte Folded Reload
	cmpq	192(%rsp), %r8          # 8-byte Folded Reload
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	%r8, %rbp
	jne	.LBB3_160
# BB#167:
	movq	120(%rsp), %r12         # 8-byte Reload
.LBB3_168:
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	movl	64(%rsp), %r14d         # 4-byte Reload
	movl	96(%rsp), %eax          # 4-byte Reload
	movl	236(%rsp), %ecx         # 4-byte Reload
.LBB3_169:                              # %img2buf.exit215
	subl	%eax, %ecx
	imull	%r12d, %ecx
	movl	%eax, %r15d
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	movl	48(%rsp), %ecx          # 4-byte Reload
	subl	%r14d, %ecx
	imull	%eax, %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	movslq	%eax, %rdx
	movl	28(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %edi
	movq	%rbp, %rsi
	callq	write
	movl	316864(%r13), %eax
	movl	316868(%r13), %ecx
	subl	%r15d, %ecx
	imull	%r12d, %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	subl	%r14d, %eax
	imull	%edx, %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rdx
	movl	%ebx, %edi
	movq	%rbp, %rsi
	callq	write
	movq	316928(%r13), %rdi
	movl	$1, %esi
	callq	free_mem3Dpel
	movq	$0, 316928(%r13)
.LBB3_170:
	movq	%rbp, %rdi
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB3_171:
	movl	64(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB3_128
.LBB3_172:
	movl	64(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB3_129
.LBB3_173:
	movq	88(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB3_169
.Lfunc_end3:
	.size	write_out_picture, .Lfunc_end3-write_out_picture
	.cfi_endproc

	.globl	init_out_buffer
	.p2align	4, 0x90
	.type	init_out_buffer,@function
init_out_buffer:                        # @init_out_buffer
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 16
	callq	alloc_frame_store
	movq	%rax, out_buffer(%rip)
	popq	%rax
	retq
.Lfunc_end4:
	.size	init_out_buffer, .Lfunc_end4-init_out_buffer
	.cfi_endproc

	.globl	uninit_out_buffer
	.p2align	4, 0x90
	.type	uninit_out_buffer,@function
uninit_out_buffer:                      # @uninit_out_buffer
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 16
	movq	out_buffer(%rip), %rdi
	callq	free_frame_store
	movq	$0, out_buffer(%rip)
	popq	%rax
	retq
.Lfunc_end5:
	.size	uninit_out_buffer, .Lfunc_end5-uninit_out_buffer
	.cfi_endproc

	.globl	clear_picture
	.p2align	4, 0x90
	.type	clear_picture,@function
clear_picture:                          # @clear_picture
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 40
.Lcfi32:
	.cfi_offset %rbx, -40
.Lcfi33:
	.cfi_offset %r12, -32
.Lcfi34:
	.cfi_offset %r14, -24
.Lcfi35:
	.cfi_offset %r15, -16
	movslq	316868(%rdi), %r14
	testq	%r14, %r14
	jle	.LBB6_17
# BB#1:                                 # %.preheader35.lr.ph
	movslq	316864(%rdi), %rcx
	testq	%rcx, %rcx
	jle	.LBB6_17
# BB#2:                                 # %.preheader35.us.preheader
	movq	img(%rip), %rax
	movl	5892(%rax), %edx
	movq	316920(%rdi), %r10
	leaq	-16(%rcx), %r8
	movl	%r8d, %r9d
	shrl	$4, %r9d
	incl	%r9d
	movq	%rcx, %r15
	andq	$-16, %r15
	movd	%edx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	andl	$7, %r9d
	movq	%r9, %r11
	negq	%r11
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB6_3:                                # %.preheader35.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_10 Depth 2
                                        #     Child Loop BB6_13 Depth 2
                                        #     Child Loop BB6_15 Depth 2
	cmpl	$16, %ecx
	movq	(%r10,%r12,8), %rax
	jae	.LBB6_5
# BB#4:                                 #   in Loop: Header=BB6_3 Depth=1
	xorl	%esi, %esi
	jmp	.LBB6_15
	.p2align	4, 0x90
.LBB6_5:                                # %min.iters.checked
                                        #   in Loop: Header=BB6_3 Depth=1
	testq	%r15, %r15
	je	.LBB6_6
# BB#7:                                 # %vector.ph
                                        #   in Loop: Header=BB6_3 Depth=1
	testq	%r9, %r9
	je	.LBB6_8
# BB#9:                                 # %vector.body.prol.preheader
                                        #   in Loop: Header=BB6_3 Depth=1
	movq	%r11, %rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_10:                               # %vector.body.prol
                                        #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, (%rax,%rbx,2)
	movdqu	%xmm0, 16(%rax,%rbx,2)
	addq	$16, %rbx
	incq	%rsi
	jne	.LBB6_10
	jmp	.LBB6_11
.LBB6_6:                                #   in Loop: Header=BB6_3 Depth=1
	xorl	%esi, %esi
	jmp	.LBB6_15
.LBB6_8:                                #   in Loop: Header=BB6_3 Depth=1
	xorl	%ebx, %ebx
.LBB6_11:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB6_3 Depth=1
	cmpq	$112, %r8
	jb	.LBB6_14
# BB#12:                                # %vector.ph.new
                                        #   in Loop: Header=BB6_3 Depth=1
	movq	%r15, %rsi
	subq	%rbx, %rsi
	leaq	240(%rax,%rbx,2), %rbx
	.p2align	4, 0x90
.LBB6_13:                               # %vector.body
                                        #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -240(%rbx)
	movdqu	%xmm0, -224(%rbx)
	movdqu	%xmm0, -208(%rbx)
	movdqu	%xmm0, -192(%rbx)
	movdqu	%xmm0, -176(%rbx)
	movdqu	%xmm0, -160(%rbx)
	movdqu	%xmm0, -144(%rbx)
	movdqu	%xmm0, -128(%rbx)
	movdqu	%xmm0, -112(%rbx)
	movdqu	%xmm0, -96(%rbx)
	movdqu	%xmm0, -80(%rbx)
	movdqu	%xmm0, -64(%rbx)
	movdqu	%xmm0, -48(%rbx)
	movdqu	%xmm0, -32(%rbx)
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	addq	$256, %rbx              # imm = 0x100
	addq	$-128, %rsi
	jne	.LBB6_13
.LBB6_14:                               # %middle.block
                                        #   in Loop: Header=BB6_3 Depth=1
	cmpq	%r15, %rcx
	movq	%r15, %rsi
	je	.LBB6_16
	.p2align	4, 0x90
.LBB6_15:                               # %scalar.ph
                                        #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%dx, (%rax,%rsi,2)
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB6_15
.LBB6_16:                               # %._crit_edge45.us
                                        #   in Loop: Header=BB6_3 Depth=1
	incq	%r12
	cmpq	%r14, %r12
	jl	.LBB6_3
.LBB6_17:                               # %.preheader34
	movslq	316876(%rdi), %r10
	testq	%r10, %r10
	jle	.LBB6_51
# BB#18:                                # %.preheader33.lr.ph
	movslq	316872(%rdi), %rcx
	testq	%rcx, %rcx
	jle	.LBB6_35
# BB#19:                                # %.preheader33.us.preheader
	movq	img(%rip), %rax
	movl	5896(%rax), %edx
	movq	316928(%rdi), %rax
	movq	(%rax), %r14
	leaq	-16(%rcx), %r8
	movl	%r8d, %r9d
	shrl	$4, %r9d
	incl	%r9d
	movq	%rcx, %r15
	andq	$-16, %r15
	movd	%edx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	andl	$7, %r9d
	movq	%r9, %r11
	negq	%r11
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB6_20:                               # %.preheader33.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_27 Depth 2
                                        #     Child Loop BB6_30 Depth 2
                                        #     Child Loop BB6_32 Depth 2
	cmpl	$16, %ecx
	movq	(%r14,%r12,8), %rsi
	jae	.LBB6_22
# BB#21:                                #   in Loop: Header=BB6_20 Depth=1
	xorl	%eax, %eax
	jmp	.LBB6_32
	.p2align	4, 0x90
.LBB6_22:                               # %min.iters.checked75
                                        #   in Loop: Header=BB6_20 Depth=1
	testq	%r15, %r15
	je	.LBB6_23
# BB#24:                                # %vector.ph79
                                        #   in Loop: Header=BB6_20 Depth=1
	testq	%r9, %r9
	je	.LBB6_25
# BB#26:                                # %vector.body71.prol.preheader
                                        #   in Loop: Header=BB6_20 Depth=1
	movq	%r11, %rax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_27:                               # %vector.body71.prol
                                        #   Parent Loop BB6_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, (%rsi,%rbx,2)
	movdqu	%xmm0, 16(%rsi,%rbx,2)
	addq	$16, %rbx
	incq	%rax
	jne	.LBB6_27
	jmp	.LBB6_28
.LBB6_23:                               #   in Loop: Header=BB6_20 Depth=1
	xorl	%eax, %eax
	jmp	.LBB6_32
.LBB6_25:                               #   in Loop: Header=BB6_20 Depth=1
	xorl	%ebx, %ebx
.LBB6_28:                               # %vector.body71.prol.loopexit
                                        #   in Loop: Header=BB6_20 Depth=1
	cmpq	$112, %r8
	jb	.LBB6_31
# BB#29:                                # %vector.ph79.new
                                        #   in Loop: Header=BB6_20 Depth=1
	movq	%r15, %rax
	subq	%rbx, %rax
	leaq	240(%rsi,%rbx,2), %rbx
	.p2align	4, 0x90
.LBB6_30:                               # %vector.body71
                                        #   Parent Loop BB6_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -240(%rbx)
	movdqu	%xmm0, -224(%rbx)
	movdqu	%xmm0, -208(%rbx)
	movdqu	%xmm0, -192(%rbx)
	movdqu	%xmm0, -176(%rbx)
	movdqu	%xmm0, -160(%rbx)
	movdqu	%xmm0, -144(%rbx)
	movdqu	%xmm0, -128(%rbx)
	movdqu	%xmm0, -112(%rbx)
	movdqu	%xmm0, -96(%rbx)
	movdqu	%xmm0, -80(%rbx)
	movdqu	%xmm0, -64(%rbx)
	movdqu	%xmm0, -48(%rbx)
	movdqu	%xmm0, -32(%rbx)
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	addq	$256, %rbx              # imm = 0x100
	addq	$-128, %rax
	jne	.LBB6_30
.LBB6_31:                               # %middle.block72
                                        #   in Loop: Header=BB6_20 Depth=1
	cmpq	%r15, %rcx
	movq	%r15, %rax
	je	.LBB6_33
	.p2align	4, 0x90
.LBB6_32:                               # %scalar.ph73
                                        #   Parent Loop BB6_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%dx, (%rsi,%rax,2)
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB6_32
.LBB6_33:                               # %._crit_edge41.us
                                        #   in Loop: Header=BB6_20 Depth=1
	incq	%r12
	cmpq	%r10, %r12
	jl	.LBB6_20
# BB#34:                                # %.preheader32
	testl	%r10d, %r10d
	jle	.LBB6_51
.LBB6_35:                               # %.preheader.lr.ph
	movslq	316872(%rdi), %rcx
	testq	%rcx, %rcx
	jle	.LBB6_51
# BB#36:                                # %.preheader.us.preheader
	movq	img(%rip), %rax
	movl	5896(%rax), %edx
	movq	316928(%rdi), %rax
	movq	8(%rax), %r14
	leaq	-16(%rcx), %r8
	movl	%r8d, %r11d
	shrl	$4, %r11d
	incl	%r11d
	movq	%rcx, %r15
	andq	$-16, %r15
	movd	%edx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	andl	$7, %r11d
	movq	%r11, %r9
	negq	%r9
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_37:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_44 Depth 2
                                        #     Child Loop BB6_47 Depth 2
                                        #     Child Loop BB6_49 Depth 2
	cmpl	$16, %ecx
	movq	(%r14,%rbx,8), %rdi
	jae	.LBB6_39
# BB#38:                                #   in Loop: Header=BB6_37 Depth=1
	xorl	%eax, %eax
	jmp	.LBB6_49
	.p2align	4, 0x90
.LBB6_39:                               # %min.iters.checked94
                                        #   in Loop: Header=BB6_37 Depth=1
	testq	%r15, %r15
	je	.LBB6_40
# BB#41:                                # %vector.ph98
                                        #   in Loop: Header=BB6_37 Depth=1
	testq	%r11, %r11
	je	.LBB6_42
# BB#43:                                # %vector.body90.prol.preheader
                                        #   in Loop: Header=BB6_37 Depth=1
	movq	%r9, %rax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_44:                               # %vector.body90.prol
                                        #   Parent Loop BB6_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, (%rdi,%rsi,2)
	movdqu	%xmm0, 16(%rdi,%rsi,2)
	addq	$16, %rsi
	incq	%rax
	jne	.LBB6_44
	jmp	.LBB6_45
.LBB6_40:                               #   in Loop: Header=BB6_37 Depth=1
	xorl	%eax, %eax
	jmp	.LBB6_49
.LBB6_42:                               #   in Loop: Header=BB6_37 Depth=1
	xorl	%esi, %esi
.LBB6_45:                               # %vector.body90.prol.loopexit
                                        #   in Loop: Header=BB6_37 Depth=1
	cmpq	$112, %r8
	jb	.LBB6_48
# BB#46:                                # %vector.ph98.new
                                        #   in Loop: Header=BB6_37 Depth=1
	movq	%r15, %rax
	subq	%rsi, %rax
	leaq	240(%rdi,%rsi,2), %rsi
	.p2align	4, 0x90
.LBB6_47:                               # %vector.body90
                                        #   Parent Loop BB6_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -240(%rsi)
	movdqu	%xmm0, -224(%rsi)
	movdqu	%xmm0, -208(%rsi)
	movdqu	%xmm0, -192(%rsi)
	movdqu	%xmm0, -176(%rsi)
	movdqu	%xmm0, -160(%rsi)
	movdqu	%xmm0, -144(%rsi)
	movdqu	%xmm0, -128(%rsi)
	movdqu	%xmm0, -112(%rsi)
	movdqu	%xmm0, -96(%rsi)
	movdqu	%xmm0, -80(%rsi)
	movdqu	%xmm0, -64(%rsi)
	movdqu	%xmm0, -48(%rsi)
	movdqu	%xmm0, -32(%rsi)
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	addq	$256, %rsi              # imm = 0x100
	addq	$-128, %rax
	jne	.LBB6_47
.LBB6_48:                               # %middle.block91
                                        #   in Loop: Header=BB6_37 Depth=1
	cmpq	%r15, %rcx
	movq	%r15, %rax
	je	.LBB6_50
	.p2align	4, 0x90
.LBB6_49:                               # %scalar.ph92
                                        #   Parent Loop BB6_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%dx, (%rdi,%rax,2)
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB6_49
.LBB6_50:                               # %._crit_edge.us
                                        #   in Loop: Header=BB6_37 Depth=1
	incq	%rbx
	cmpq	%r10, %rbx
	jl	.LBB6_37
.LBB6_51:                               # %._crit_edge38
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	clear_picture, .Lfunc_end6-clear_picture
	.cfi_endproc

	.globl	write_unpaired_field
	.p2align	4, 0x90
	.type	write_unpaired_field,@function
write_unpaired_field:                   # @write_unpaired_field
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 32
.Lcfi39:
	.cfi_offset %rbx, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	testb	$1, %al
	je	.LBB7_2
# BB#1:
	movq	56(%rbx), %rbp
	movl	316864(%rbp), %esi
	movl	316868(%rbp), %edx
	addl	%edx, %edx
	movl	316872(%rbp), %ecx
	movl	316876(%rbp), %r8d
	addl	%r8d, %r8d
	movl	$2, %edi
	callq	alloc_storable_picture
	movq	%rax, 64(%rbx)
	movl	317044(%rbp), %ecx
	movl	%ecx, 317044(%rax)
	movq	%rax, %rdi
	callq	clear_picture
	movq	%rbx, %rdi
	callq	dpb_combine_field_yuv
	movq	48(%rbx), %rdi
	movl	%r14d, %esi
	callq	write_out_picture
	movl	(%rbx), %eax
.LBB7_2:
	testb	$2, %al
	je	.LBB7_6
# BB#3:
	movq	64(%rbx), %rbp
	movl	316864(%rbp), %esi
	movl	316868(%rbp), %edx
	addl	%edx, %edx
	movl	316872(%rbp), %ecx
	movl	316876(%rbp), %r8d
	addl	%r8d, %r8d
	movl	$1, %edi
	callq	alloc_storable_picture
	movq	%rax, 56(%rbx)
	movl	317044(%rbp), %ecx
	movl	%ecx, 317044(%rax)
	movq	%rax, %rdi
	callq	clear_picture
	movq	56(%rbx), %rax
	movq	64(%rbx), %rcx
	movl	317052(%rcx), %edx
	movl	%edx, 317052(%rax)
	testl	%edx, %edx
	je	.LBB7_5
# BB#4:
	movups	317056(%rcx), %xmm0
	movups	%xmm0, 317056(%rax)
.LBB7_5:
	movq	%rbx, %rdi
	callq	dpb_combine_field_yuv
	movq	48(%rbx), %rdi
	movl	%r14d, %esi
	callq	write_out_picture
.LBB7_6:
	movl	$3, (%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end7:
	.size	write_unpaired_field, .Lfunc_end7-write_unpaired_field
	.cfi_endproc

	.globl	flush_direct_output
	.p2align	4, 0x90
	.type	flush_direct_output,@function
flush_direct_output:                    # @flush_direct_output
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 16
	movl	%edi, %eax
	movq	out_buffer(%rip), %rdi
	movl	%eax, %esi
	callq	write_unpaired_field
	movq	out_buffer(%rip), %rax
	movq	48(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 48(%rax)
	movq	56(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 56(%rax)
	movq	64(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 64(%rax)
	movl	$0, (%rax)
	popq	%rax
	retq
.Lfunc_end8:
	.size	flush_direct_output, .Lfunc_end8-flush_direct_output
	.cfi_endproc

	.globl	write_stored_frame
	.p2align	4, 0x90
	.type	write_stored_frame,@function
write_stored_frame:                     # @write_stored_frame
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 32
.Lcfi46:
	.cfi_offset %rbx, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	out_buffer(%rip), %rdi
	callq	write_unpaired_field
	movq	out_buffer(%rip), %rax
	movq	48(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 48(%rax)
	movq	56(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 56(%rax)
	movq	64(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 64(%rax)
	movl	$0, (%rax)
	cmpl	$2, (%rbx)
	jg	.LBB9_2
# BB#1:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	write_unpaired_field
	jmp	.LBB9_7
.LBB9_2:
	cmpl	$0, 24(%rbx)
	je	.LBB9_4
# BB#3:                                 # %.thread
	movl	$1, recovery_flag(%rip)
.LBB9_6:
	movq	48(%rbx), %rdi
	movl	%ebp, %esi
	callq	write_out_picture
.LBB9_7:
	movl	$1, 36(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB9_4:
	cmpl	$0, non_conforming_stream(%rip)
	je	.LBB9_6
# BB#5:
	movl	recovery_flag(%rip), %eax
	testl	%eax, %eax
	je	.LBB9_7
	jmp	.LBB9_6
.Lfunc_end9:
	.size	write_stored_frame, .Lfunc_end9-write_stored_frame
	.cfi_endproc

	.globl	direct_output
	.p2align	4, 0x90
	.type	direct_output,@function
direct_output:                          # @direct_output
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 32
.Lcfi51:
	.cfi_offset %rbx, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB10_6
# BB#1:
	testl	%eax, %eax
	jne	.LBB10_9
# BB#2:
	movq	out_buffer(%rip), %rdi
	movl	%ebp, %esi
	callq	write_unpaired_field
	movq	out_buffer(%rip), %rax
	movq	48(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 48(%rax)
	movq	56(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 56(%rax)
	movq	64(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 64(%rax)
	movl	$0, (%rax)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	write_out_picture
	movl	p_ref(%rip), %edx
	cmpl	$-1, %edx
	je	.LBB10_5
# BB#3:
	movq	input(%rip), %rax
	cmpl	$0, 3016(%rax)
	jne	.LBB10_5
# BB#4:
	movq	snr(%rip), %rdi
	movq	%rbx, %rsi
	callq	find_snr
.LBB10_5:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	free_storable_picture   # TAILCALL
.LBB10_6:
	movq	out_buffer(%rip), %rdi
	movl	(%rdi), %ecx
	movl	$1, %eax
	testb	$1, %cl
	je	.LBB10_8
# BB#7:
	movl	%ebp, %esi
	callq	write_unpaired_field
	movq	out_buffer(%rip), %rax
	movq	48(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 48(%rax)
	movq	56(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 56(%rax)
	movq	64(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rdi
	movq	$0, 64(%rdi)
	movl	$0, (%rdi)
	movl	(%rbx), %eax
	xorl	%ecx, %ecx
.LBB10_8:
	movq	%rbx, 56(%rdi)
	orl	$1, %ecx
	movl	%ecx, (%rdi)
.LBB10_9:
	movq	out_buffer(%rip), %rdi
	movl	(%rdi), %ecx
	cmpl	$2, %eax
	jne	.LBB10_13
# BB#10:
	testb	$2, %cl
	je	.LBB10_12
# BB#11:
	movl	%ebp, %esi
	callq	write_unpaired_field
	movq	out_buffer(%rip), %rax
	movq	48(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 48(%rax)
	movq	56(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 56(%rax)
	movq	64(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rdi
	movq	$0, 64(%rdi)
	movl	$0, (%rdi)
	xorl	%ecx, %ecx
.LBB10_12:
	movq	%rbx, 64(%rdi)
	orl	$2, %ecx
	movl	%ecx, (%rdi)
.LBB10_13:                              # %._crit_edge
	cmpl	$3, %ecx
	jne	.LBB10_18
# BB#14:
	callq	dpb_combine_field_yuv
	movq	out_buffer(%rip), %rax
	movq	48(%rax), %rdi
	movl	%ebp, %esi
	callq	write_out_picture
	movl	p_ref(%rip), %edx
	cmpl	$-1, %edx
	je	.LBB10_17
# BB#15:
	movq	input(%rip), %rax
	cmpl	$0, 3016(%rax)
	jne	.LBB10_17
# BB#16:
	movq	snr(%rip), %rdi
	movq	out_buffer(%rip), %rax
	movq	48(%rax), %rsi
	callq	find_snr
.LBB10_17:
	movq	out_buffer(%rip), %rax
	movq	48(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 48(%rax)
	movq	56(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 56(%rax)
	movq	64(%rax), %rdi
	callq	free_storable_picture
	movq	out_buffer(%rip), %rax
	movq	$0, 64(%rax)
	movl	$0, (%rax)
.LBB10_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end10:
	.size	direct_output, .Lfunc_end10-direct_output
	.cfi_endproc

	.type	pending_output,@object  # @pending_output
	.bss
	.globl	pending_output
	.p2align	3
pending_output:
	.quad	0
	.size	pending_output, 8

	.type	pending_output_state,@object # @pending_output_state
	.globl	pending_output_state
	.p2align	2
pending_output_state:
	.long	0                       # 0x0
	.size	pending_output_state, 4

	.type	recovery_flag,@object   # @recovery_flag
	.globl	recovery_flag
	.p2align	2
recovery_flag:
	.long	0                       # 0x0
	.size	recovery_flag, 4

	.type	.Lwrite_out_picture.SubWidthC,@object # @write_out_picture.SubWidthC
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.Lwrite_out_picture.SubWidthC:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.size	.Lwrite_out_picture.SubWidthC, 16

	.type	.Lwrite_out_picture.SubHeightC,@object # @write_out_picture.SubHeightC
	.p2align	4
.Lwrite_out_picture.SubHeightC:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.size	.Lwrite_out_picture.SubHeightC, 16

	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"write_out_picture: buf"
	.size	.L.str.1, 23

	.type	out_buffer,@object      # @out_buffer
	.comm	out_buffer,8,8
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
