	.text
	.file	"direction.bc"
	.globl	_ZN9direction9as_stringEv
	.p2align	4, 0x90
	.type	_ZN9direction9as_stringEv,@function
_ZN9direction9as_stringEv:              # @_ZN9direction9as_stringEv
	.cfi_startproc
# BB#0:
	movslq	(%rdi), %rax
	movq	.L_ZZN9direction9as_stringEvE4dirs(,%rax,8), %rax
	retq
.Lfunc_end0:
	.size	_ZN9direction9as_stringEv, .Lfunc_end0-_ZN9direction9as_stringEv
	.cfi_endproc

	.globl	_Zeq9directionS_
	.p2align	4, 0x90
	.type	_Zeq9directionS_,@function
_Zeq9directionS_:                       # @_Zeq9directionS_
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	%esi, %edi
	sete	%al
	retq
.Lfunc_end1:
	.size	_Zeq9directionS_, .Lfunc_end1-_Zeq9directionS_
	.cfi_endproc

	.globl	_Zne9directionS_
	.p2align	4, 0x90
	.type	_Zne9directionS_,@function
_Zne9directionS_:                       # @_Zne9directionS_
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	%esi, %edi
	setne	%al
	retq
.Lfunc_end2:
	.size	_Zne9directionS_, .Lfunc_end2-_Zne9directionS_
	.cfi_endproc

	.globl	_Zle9directionS_
	.p2align	4, 0x90
	.type	_Zle9directionS_,@function
_Zle9directionS_:                       # @_Zle9directionS_
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	%esi, %edi
	setle	%al
	retq
.Lfunc_end3:
	.size	_Zle9directionS_, .Lfunc_end3-_Zle9directionS_
	.cfi_endproc

	.globl	_ZlsRSo9direction
	.p2align	4, 0x90
	.type	_ZlsRSo9direction,@function
_ZlsRSo9direction:                      # @_ZlsRSo9direction
	.cfi_startproc
# BB#0:                                 # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movslq	%esi, %rax
	movq	.L_ZZN9direction9as_stringEvE4dirs(,%rax,8), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_ZlsRSo9direction, .Lfunc_end4-_ZlsRSo9direction
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_direction.ii,@function
_GLOBAL__sub_I_direction.ii:            # @_GLOBAL__sub_I_direction.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$0, N(%rip)
	movl	$1, NE(%rip)
	movl	$2, E(%rip)
	movl	$3, SE(%rip)
	movl	$4, S(%rip)
	movl	$5, SW(%rip)
	movl	$6, W(%rip)
	movl	$7, NW(%rip)
	movl	$8, NO_DIRECTION(%rip)
	popq	%rax
	retq
.Lfunc_end5:
	.size	_GLOBAL__sub_I_direction.ii, .Lfunc_end5-_GLOBAL__sub_I_direction.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"N"
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"NE"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"E"
	.size	.L.str.2, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"SE"
	.size	.L.str.3, 3

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"S"
	.size	.L.str.4, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"SW"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"W"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"NW"
	.size	.L.str.7, 3

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"No direction"
	.size	.L.str.8, 13

	.type	.L_ZZN9direction9as_stringEvE4dirs,@object # @_ZZN9direction9as_stringEvE4dirs
	.section	.rodata,"a",@progbits
	.p2align	4
.L_ZZN9direction9as_stringEvE4dirs:
	.quad	.L.str
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	.L.str.7
	.quad	.L.str.8
	.size	.L_ZZN9direction9as_stringEvE4dirs, 72

	.type	N,@object               # @N
	.bss
	.globl	N
	.p2align	2
N:
	.zero	4
	.size	N, 4

	.type	NE,@object              # @NE
	.globl	NE
	.p2align	2
NE:
	.zero	4
	.size	NE, 4

	.type	E,@object               # @E
	.globl	E
	.p2align	2
E:
	.zero	4
	.size	E, 4

	.type	SE,@object              # @SE
	.globl	SE
	.p2align	2
SE:
	.zero	4
	.size	SE, 4

	.type	S,@object               # @S
	.globl	S
	.p2align	2
S:
	.zero	4
	.size	S, 4

	.type	SW,@object              # @SW
	.globl	SW
	.p2align	2
SW:
	.zero	4
	.size	SW, 4

	.type	W,@object               # @W
	.globl	W
	.p2align	2
W:
	.zero	4
	.size	W, 4

	.type	NW,@object              # @NW
	.globl	NW
	.p2align	2
NW:
	.zero	4
	.size	NW, 4

	.type	NO_DIRECTION,@object    # @NO_DIRECTION
	.globl	NO_DIRECTION
	.p2align	2
NO_DIRECTION:
	.zero	4
	.size	NO_DIRECTION, 4

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_direction.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
