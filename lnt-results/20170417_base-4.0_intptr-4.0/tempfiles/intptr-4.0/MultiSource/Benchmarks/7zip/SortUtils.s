	.text
	.file	"SortUtils.bc"
	.globl	_Z13SortFileNamesRK13CObjectVectorI11CStringBaseIwEER13CRecordVectorIiE
	.p2align	4, 0x90
	.type	_Z13SortFileNamesRK13CObjectVectorI11CStringBaseIwEER13CRecordVectorIiE,@function
_Z13SortFileNamesRK13CObjectVectorI11CStringBaseIwEER13CRecordVectorIiE: # @_Z13SortFileNamesRK13CObjectVectorI11CStringBaseIwEER13CRecordVectorIiE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	12(%rbx), %r15d
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r15d, %r15d
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	jle	.LBB0_1
# BB#24:                                # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_25:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r14), %rax
	movslq	12(%r14), %rcx
	movl	%ebp, (%rax,%rcx,4)
	movl	12(%r14), %ebx
	incl	%ebx
	movl	%ebx, 12(%r14)
	incl	%ebp
	cmpl	%ebp, %r15d
	jne	.LBB0_25
	jmp	.LBB0_2
.LBB0_1:                                # %.._crit_edge_crit_edge
	movl	12(%r14), %ebx
.LBB0_2:                                # %._crit_edge
	cmpl	$2, %ebx
	jl	.LBB0_23
# BB#3:
	movq	16(%r14), %rbp
	movl	%ebx, %eax
	shrl	%eax
	.p2align	4, 0x90
.LBB0_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
	movl	-4(%rbp,%rax,4), %ecx
	leal	(%rax,%rax), %r12d
	cmpl	%ebx, %r12d
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	jle	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	movl	%eax, %r14d
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph.i29.i.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movslq	%ecx, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%eax, %r15d
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph.i29.i
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebx, %r12d
	jge	.LBB0_8
# BB#9:                                 #   in Loop: Header=BB0_7 Depth=2
	movslq	%r12d, %rax
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	16(%r13), %rcx
	movslq	(%rbp,%rax,4), %rdx
	movq	(%rcx,%rdx,8), %rdi
	movslq	-4(%rbp,%rax,4), %rax
	movq	(%rcx,%rax,8), %rsi
	callq	_Z16CompareFileNamesRK11CStringBaseIwES2_
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setg	%r14b
	orl	%r12d, %r14d
	jmp	.LBB0_10
	.p2align	4, 0x90
.LBB0_8:                                #   in Loop: Header=BB0_7 Depth=2
	movl	%r12d, %r14d
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB0_10:                               #   in Loop: Header=BB0_7 Depth=2
	movslq	%r14d, %r12
	movq	16(%r13), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rdi
	movslq	-4(%rbp,%r12,4), %rcx
	movq	(%rax,%rcx,8), %rsi
	callq	_Z16CompareFileNamesRK11CStringBaseIwES2_
	testl	%eax, %eax
	jns	.LBB0_11
# BB#12:                                #   in Loop: Header=BB0_7 Depth=2
	movl	-4(%rbp,%r12,4), %eax
	movslq	%r15d, %rcx
	movl	%eax, -4(%rbp,%rcx,4)
	addl	%r12d, %r12d
	cmpl	%ebx, %r12d
	movl	%r14d, %r15d
	jle	.LBB0_7
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_11:                               #   in Loop: Header=BB0_4 Depth=1
	movl	%r15d, %r14d
.LBB0_13:                               # %_ZN13CRecordVectorIiE11SortRefDownEPiiiPFiPKiS3_PvES4_.exit36.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movslq	%r14d, %rax
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, -4(%rbp,%rax,4)
	movq	32(%rsp), %rax          # 8-byte Reload
	decq	%rax
	testl	%eax, %eax
	jne	.LBB0_4
# BB#14:                                # %.preheader.i
	movslq	%ebx, %r15
	.p2align	4, 0x90
.LBB0_15:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_17 Depth 2
	movl	-4(%rbp,%r15,4), %ecx
	movl	(%rbp), %eax
	movl	%eax, -4(%rbp,%r15,4)
	movl	%ecx, (%rbp)
	cmpq	$3, %r15
	jl	.LBB0_26
# BB#16:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB0_15 Depth=1
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movslq	%ecx, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-1(%r15), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %r14d
	movl	$2, %r13d
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph.i.i
                                        #   Parent Loop BB0_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%r13d, %r12
	cmpq	16(%rsp), %r12          # 8-byte Folded Reload
	movl	%r13d, %ebx
	jge	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_17 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movslq	(%rbp,%r12,4), %rcx
	movq	(%rax,%rcx,8), %rdi
	movslq	-4(%rbp,%r12,4), %rcx
	movq	(%rax,%rcx,8), %rsi
	callq	_Z16CompareFileNamesRK11CStringBaseIwES2_
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setg	%cl
	orl	%ecx, %r12d
	movl	%r12d, %ebx
.LBB0_19:                               #   in Loop: Header=BB0_17 Depth=2
	movslq	%ebx, %r13
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rdi
	movslq	-4(%rbp,%r13,4), %rcx
	movq	(%rax,%rcx,8), %rsi
	callq	_Z16CompareFileNamesRK11CStringBaseIwES2_
	testl	%eax, %eax
	jns	.LBB0_20
# BB#21:                                #   in Loop: Header=BB0_17 Depth=2
	movl	-4(%rbp,%r13,4), %eax
	movslq	%r14d, %rcx
	movl	%eax, -4(%rbp,%rcx,4)
	addl	%r13d, %r13d
	movslq	%r13d, %rax
	cmpq	%r15, %rax
	movl	%ebx, %r14d
	jl	.LBB0_17
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_20:                               #   in Loop: Header=BB0_15 Depth=1
	movl	%r14d, %ebx
.LBB0_22:                               # %_ZN13CRecordVectorIiE11SortRefDownEPiiiPFiPKiS3_PvES4_.exit.i
                                        #   in Loop: Header=BB0_15 Depth=1
	movslq	%ebx, %rax
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, -4(%rbp,%rax,4)
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$1, %rax
	movq	%rax, %r15
	jg	.LBB0_15
	jmp	.LBB0_23
.LBB0_26:                               # %_ZN13CRecordVectorIiE11SortRefDownEPiiiPFiPKiS3_PvES4_.exit.thread.i
	movl	%ecx, (%rbp)
.LBB0_23:                               # %_ZN13CRecordVectorIiE4SortEPFiPKiS2_PvES3_.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_Z13SortFileNamesRK13CObjectVectorI11CStringBaseIwEER13CRecordVectorIiE, .Lfunc_end0-_Z13SortFileNamesRK13CObjectVectorI11CStringBaseIwEER13CRecordVectorIiE
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
