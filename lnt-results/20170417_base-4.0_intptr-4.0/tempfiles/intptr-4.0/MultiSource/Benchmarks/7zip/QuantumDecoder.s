	.text
	.file	"QuantumDecoder.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.short	64                      # 0x40
	.short	63                      # 0x3f
	.short	62                      # 0x3e
	.short	61                      # 0x3d
	.short	60                      # 0x3c
	.short	59                      # 0x3b
	.short	58                      # 0x3a
	.short	57                      # 0x39
.LCPI0_1:
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	0                       # 0x0
	.byte	2                       # 0x2
	.byte	0                       # 0x0
	.byte	3                       # 0x3
	.byte	0                       # 0x0
	.byte	4                       # 0x4
	.byte	0                       # 0x0
	.byte	5                       # 0x5
	.byte	0                       # 0x0
	.byte	6                       # 0x6
	.byte	0                       # 0x0
	.byte	7                       # 0x7
	.byte	0                       # 0x0
.LCPI0_2:
	.short	56                      # 0x38
	.short	55                      # 0x37
	.short	54                      # 0x36
	.short	53                      # 0x35
	.short	52                      # 0x34
	.short	51                      # 0x33
	.short	50                      # 0x32
	.short	49                      # 0x31
.LCPI0_3:
	.byte	8                       # 0x8
	.byte	0                       # 0x0
	.byte	9                       # 0x9
	.byte	0                       # 0x0
	.byte	10                      # 0xa
	.byte	0                       # 0x0
	.byte	11                      # 0xb
	.byte	0                       # 0x0
	.byte	12                      # 0xc
	.byte	0                       # 0x0
	.byte	13                      # 0xd
	.byte	0                       # 0x0
	.byte	14                      # 0xe
	.byte	0                       # 0x0
	.byte	15                      # 0xf
	.byte	0                       # 0x0
.LCPI0_4:
	.short	48                      # 0x30
	.short	47                      # 0x2f
	.short	46                      # 0x2e
	.short	45                      # 0x2d
	.short	44                      # 0x2c
	.short	43                      # 0x2b
	.short	42                      # 0x2a
	.short	41                      # 0x29
.LCPI0_5:
	.byte	16                      # 0x10
	.byte	0                       # 0x0
	.byte	17                      # 0x11
	.byte	0                       # 0x0
	.byte	18                      # 0x12
	.byte	0                       # 0x0
	.byte	19                      # 0x13
	.byte	0                       # 0x0
	.byte	20                      # 0x14
	.byte	0                       # 0x0
	.byte	21                      # 0x15
	.byte	0                       # 0x0
	.byte	22                      # 0x16
	.byte	0                       # 0x0
	.byte	23                      # 0x17
	.byte	0                       # 0x0
.LCPI0_6:
	.short	40                      # 0x28
	.short	39                      # 0x27
	.short	38                      # 0x26
	.short	37                      # 0x25
	.short	36                      # 0x24
	.short	35                      # 0x23
	.short	34                      # 0x22
	.short	33                      # 0x21
.LCPI0_7:
	.byte	24                      # 0x18
	.byte	0                       # 0x0
	.byte	25                      # 0x19
	.byte	0                       # 0x0
	.byte	26                      # 0x1a
	.byte	0                       # 0x0
	.byte	27                      # 0x1b
	.byte	0                       # 0x0
	.byte	28                      # 0x1c
	.byte	0                       # 0x0
	.byte	29                      # 0x1d
	.byte	0                       # 0x0
	.byte	30                      # 0x1e
	.byte	0                       # 0x0
	.byte	31                      # 0x1f
	.byte	0                       # 0x0
.LCPI0_8:
	.short	32                      # 0x20
	.short	31                      # 0x1f
	.short	30                      # 0x1e
	.short	29                      # 0x1d
	.short	28                      # 0x1c
	.short	27                      # 0x1b
	.short	26                      # 0x1a
	.short	25                      # 0x19
.LCPI0_9:
	.byte	32                      # 0x20
	.byte	0                       # 0x0
	.byte	33                      # 0x21
	.byte	0                       # 0x0
	.byte	34                      # 0x22
	.byte	0                       # 0x0
	.byte	35                      # 0x23
	.byte	0                       # 0x0
	.byte	36                      # 0x24
	.byte	0                       # 0x0
	.byte	37                      # 0x25
	.byte	0                       # 0x0
	.byte	38                      # 0x26
	.byte	0                       # 0x0
	.byte	39                      # 0x27
	.byte	0                       # 0x0
.LCPI0_10:
	.short	24                      # 0x18
	.short	23                      # 0x17
	.short	22                      # 0x16
	.short	21                      # 0x15
	.short	20                      # 0x14
	.short	19                      # 0x13
	.short	18                      # 0x12
	.short	17                      # 0x11
.LCPI0_11:
	.byte	40                      # 0x28
	.byte	0                       # 0x0
	.byte	41                      # 0x29
	.byte	0                       # 0x0
	.byte	42                      # 0x2a
	.byte	0                       # 0x0
	.byte	43                      # 0x2b
	.byte	0                       # 0x0
	.byte	44                      # 0x2c
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	46                      # 0x2e
	.byte	0                       # 0x0
	.byte	47                      # 0x2f
	.byte	0                       # 0x0
.LCPI0_12:
	.short	16                      # 0x10
	.short	15                      # 0xf
	.short	14                      # 0xe
	.short	13                      # 0xd
	.short	12                      # 0xc
	.short	11                      # 0xb
	.short	10                      # 0xa
	.short	9                       # 0x9
.LCPI0_13:
	.byte	48                      # 0x30
	.byte	0                       # 0x0
	.byte	49                      # 0x31
	.byte	0                       # 0x0
	.byte	50                      # 0x32
	.byte	0                       # 0x0
	.byte	51                      # 0x33
	.byte	0                       # 0x0
	.byte	52                      # 0x34
	.byte	0                       # 0x0
	.byte	53                      # 0x35
	.byte	0                       # 0x0
	.byte	54                      # 0x36
	.byte	0                       # 0x0
	.byte	55                      # 0x37
	.byte	0                       # 0x0
.LCPI0_14:
	.short	8                       # 0x8
	.short	7                       # 0x7
	.short	6                       # 0x6
	.short	5                       # 0x5
	.short	4                       # 0x4
	.short	3                       # 0x3
	.short	2                       # 0x2
	.short	1                       # 0x1
.LCPI0_15:
	.byte	56                      # 0x38
	.byte	0                       # 0x0
	.byte	57                      # 0x39
	.byte	0                       # 0x0
	.byte	58                      # 0x3a
	.byte	0                       # 0x0
	.byte	59                      # 0x3b
	.byte	0                       # 0x0
	.byte	60                      # 0x3c
	.byte	0                       # 0x0
	.byte	61                      # 0x3d
	.byte	0                       # 0x0
	.byte	62                      # 0x3e
	.byte	0                       # 0x0
	.byte	63                      # 0x3f
	.byte	0                       # 0x0
.LCPI0_16:
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
.LCPI0_17:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI0_18:
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
.LCPI0_19:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI0_20:
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
.LCPI0_21:
	.short	20                      # 0x14
	.short	19                      # 0x13
	.short	18                      # 0x12
	.short	17                      # 0x11
	.short	16                      # 0x10
	.short	15                      # 0xf
	.short	14                      # 0xe
	.short	13                      # 0xd
.LCPI0_22:
	.short	12                      # 0xc
	.short	11                      # 0xb
	.short	10                      # 0xa
	.short	9                       # 0x9
	.short	8                       # 0x8
	.short	7                       # 0x7
	.short	6                       # 0x6
	.short	5                       # 0x5
	.text
	.globl	_ZN9NCompress8NQuantum8CDecoder4InitEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum8CDecoder4InitEv,@function
_ZN9NCompress8NQuantum8CDecoder4InitEv: # @_ZN9NCompress8NQuantum8CDecoder4InitEv
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	movl	$7, 184(%rdi)
	movl	$4, 188(%rdi)
	movw	$7, 192(%rdi)
	movb	$0, 322(%rdi)
	movw	$6, 194(%rdi)
	movb	$1, 323(%rdi)
	movw	$5, 196(%rdi)
	movb	$2, 324(%rdi)
	movw	$4, 198(%rdi)
	movb	$3, 325(%rdi)
	movw	$3, 200(%rdi)
	movb	$4, 326(%rdi)
	movw	$2, 202(%rdi)
	movb	$5, 327(%rdi)
	movw	$1, 204(%rdi)
	movb	$6, 328(%rdi)
	movw	$0, 206(%rdi)
	movabsq	$17179869248, %rax      # imm = 0x400000040
	movq	%rax, 388(%rdi)
	leaq	396(%rdi), %rcx
	leaq	590(%rdi), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB0_4
# BB#1:                                 # %.lr.ph.i
	leaq	524(%rdi), %rcx
	leaq	526(%rdi), %rdx
	cmpq	%rcx, %rdx
	jae	.LBB0_4
# BB#2:                                 # %.lr.ph.i26.preheader
	movw	$61, %cx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph.i26
                                        # =>This Inner Loop Header: Depth=1
	leal	3(%rcx), %esi
	movw	%si, 396(%rdi,%rdx,2)
	movb	%dl, 526(%rdi,%rdx)
	leal	2(%rcx), %esi
	movw	%si, 398(%rdi,%rdx,2)
	leal	1(%rdx), %esi
	movb	%sil, 527(%rdi,%rdx)
	leal	1(%rcx), %esi
	movw	%si, 400(%rdi,%rdx,2)
	leaq	2(%rdx), %rsi
	movb	%sil, 528(%rdi,%rdx)
	movw	%cx, 402(%rdi,%rdx,2)
	incl	%esi
	movb	%sil, 529(%rdi,%rdx)
	leal	-4(%rcx), %ecx
	addq	$4, %rdx
	cmpq	$64, %rdx
	jne	.LBB0_3
	jmp	.LBB0_5
.LBB0_4:                                # %vector.body
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [64,63,62,61,60,59,58,57]
	movups	%xmm0, 396(%rdi)
	movdqa	.LCPI0_1(%rip), %xmm0   # xmm0 = [0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 526(%rdi)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [56,55,54,53,52,51,50,49]
	movups	%xmm0, 412(%rdi)
	movdqa	.LCPI0_3(%rip), %xmm0   # xmm0 = [8,0,9,0,10,0,11,0,12,0,13,0,14,0,15,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 534(%rdi)
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [48,47,46,45,44,43,42,41]
	movups	%xmm0, 428(%rdi)
	movdqa	.LCPI0_5(%rip), %xmm0   # xmm0 = [16,0,17,0,18,0,19,0,20,0,21,0,22,0,23,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 542(%rdi)
	movaps	.LCPI0_6(%rip), %xmm0   # xmm0 = [40,39,38,37,36,35,34,33]
	movups	%xmm0, 444(%rdi)
	movdqa	.LCPI0_7(%rip), %xmm0   # xmm0 = [24,0,25,0,26,0,27,0,28,0,29,0,30,0,31,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 550(%rdi)
	movaps	.LCPI0_8(%rip), %xmm0   # xmm0 = [32,31,30,29,28,27,26,25]
	movups	%xmm0, 460(%rdi)
	movdqa	.LCPI0_9(%rip), %xmm0   # xmm0 = [32,0,33,0,34,0,35,0,36,0,37,0,38,0,39,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 558(%rdi)
	movaps	.LCPI0_10(%rip), %xmm0  # xmm0 = [24,23,22,21,20,19,18,17]
	movups	%xmm0, 476(%rdi)
	movdqa	.LCPI0_11(%rip), %xmm0  # xmm0 = [40,0,41,0,42,0,43,0,44,0,45,0,46,0,47,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 566(%rdi)
	movaps	.LCPI0_12(%rip), %xmm0  # xmm0 = [16,15,14,13,12,11,10,9]
	movups	%xmm0, 492(%rdi)
	movdqa	.LCPI0_13(%rip), %xmm0  # xmm0 = [48,0,49,0,50,0,51,0,52,0,53,0,54,0,55,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 574(%rdi)
	movaps	.LCPI0_14(%rip), %xmm0  # xmm0 = [8,7,6,5,4,3,2,1]
	movups	%xmm0, 508(%rdi)
	movdqa	.LCPI0_15(%rip), %xmm0  # xmm0 = [56,0,57,0,58,0,59,0,60,0,61,0,62,0,63,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 582(%rdi)
.LBB0_5:                                # %_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder4InitEj.exit27
	movw	$0, 524(%rdi)
	movq	%rax, 592(%rdi)
	leaq	600(%rdi), %rcx
	leaq	794(%rdi), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB0_9
# BB#6:                                 # %_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder4InitEj.exit27
	leaq	728(%rdi), %rcx
	leaq	730(%rdi), %rdx
	cmpq	%rcx, %rdx
	jae	.LBB0_9
# BB#7:                                 # %.lr.ph.i26.1.preheader
	movw	$61, %cx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph.i26.1
                                        # =>This Inner Loop Header: Depth=1
	leal	3(%rcx), %esi
	movw	%si, 600(%rdi,%rdx,2)
	movb	%dl, 730(%rdi,%rdx)
	leal	2(%rcx), %esi
	movw	%si, 602(%rdi,%rdx,2)
	leal	1(%rdx), %esi
	movb	%sil, 731(%rdi,%rdx)
	leal	1(%rcx), %esi
	movw	%si, 604(%rdi,%rdx,2)
	leaq	2(%rdx), %rsi
	movb	%sil, 732(%rdi,%rdx)
	movw	%cx, 606(%rdi,%rdx,2)
	incl	%esi
	movb	%sil, 733(%rdi,%rdx)
	leal	-4(%rcx), %ecx
	addq	$4, %rdx
	cmpq	$64, %rdx
	jne	.LBB0_8
	jmp	.LBB0_10
.LBB0_9:                                # %vector.body39
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [64,63,62,61,60,59,58,57]
	movups	%xmm0, 600(%rdi)
	movdqa	.LCPI0_1(%rip), %xmm0   # xmm0 = [0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 730(%rdi)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [56,55,54,53,52,51,50,49]
	movups	%xmm0, 616(%rdi)
	movdqa	.LCPI0_3(%rip), %xmm0   # xmm0 = [8,0,9,0,10,0,11,0,12,0,13,0,14,0,15,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 738(%rdi)
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [48,47,46,45,44,43,42,41]
	movups	%xmm0, 632(%rdi)
	movdqa	.LCPI0_5(%rip), %xmm0   # xmm0 = [16,0,17,0,18,0,19,0,20,0,21,0,22,0,23,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 746(%rdi)
	movaps	.LCPI0_6(%rip), %xmm0   # xmm0 = [40,39,38,37,36,35,34,33]
	movups	%xmm0, 648(%rdi)
	movdqa	.LCPI0_7(%rip), %xmm0   # xmm0 = [24,0,25,0,26,0,27,0,28,0,29,0,30,0,31,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 754(%rdi)
	movaps	.LCPI0_8(%rip), %xmm0   # xmm0 = [32,31,30,29,28,27,26,25]
	movups	%xmm0, 664(%rdi)
	movdqa	.LCPI0_9(%rip), %xmm0   # xmm0 = [32,0,33,0,34,0,35,0,36,0,37,0,38,0,39,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 762(%rdi)
	movaps	.LCPI0_10(%rip), %xmm0  # xmm0 = [24,23,22,21,20,19,18,17]
	movups	%xmm0, 680(%rdi)
	movdqa	.LCPI0_11(%rip), %xmm0  # xmm0 = [40,0,41,0,42,0,43,0,44,0,45,0,46,0,47,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 770(%rdi)
	movaps	.LCPI0_12(%rip), %xmm0  # xmm0 = [16,15,14,13,12,11,10,9]
	movups	%xmm0, 696(%rdi)
	movdqa	.LCPI0_13(%rip), %xmm0  # xmm0 = [48,0,49,0,50,0,51,0,52,0,53,0,54,0,55,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 778(%rdi)
	movaps	.LCPI0_14(%rip), %xmm0  # xmm0 = [8,7,6,5,4,3,2,1]
	movups	%xmm0, 712(%rdi)
	movdqa	.LCPI0_15(%rip), %xmm0  # xmm0 = [56,0,57,0,58,0,59,0,60,0,61,0,62,0,63,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 786(%rdi)
.LBB0_10:                               # %_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder4InitEj.exit27.1
	movw	$0, 728(%rdi)
	movq	%rax, 796(%rdi)
	leaq	804(%rdi), %rcx
	leaq	998(%rdi), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB0_14
# BB#11:                                # %_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder4InitEj.exit27.1
	leaq	932(%rdi), %rcx
	leaq	934(%rdi), %rdx
	cmpq	%rcx, %rdx
	jae	.LBB0_14
# BB#12:                                # %.lr.ph.i26.2.preheader
	movw	$61, %cx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph.i26.2
                                        # =>This Inner Loop Header: Depth=1
	leal	3(%rcx), %esi
	movw	%si, 804(%rdi,%rdx,2)
	movb	%dl, 934(%rdi,%rdx)
	leal	2(%rcx), %esi
	movw	%si, 806(%rdi,%rdx,2)
	leal	1(%rdx), %esi
	movb	%sil, 935(%rdi,%rdx)
	leal	1(%rcx), %esi
	movw	%si, 808(%rdi,%rdx,2)
	leaq	2(%rdx), %rsi
	movb	%sil, 936(%rdi,%rdx)
	movw	%cx, 810(%rdi,%rdx,2)
	incl	%esi
	movb	%sil, 937(%rdi,%rdx)
	leal	-4(%rcx), %ecx
	addq	$4, %rdx
	cmpq	$64, %rdx
	jne	.LBB0_13
	jmp	.LBB0_15
.LBB0_14:                               # %vector.body66
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [64,63,62,61,60,59,58,57]
	movups	%xmm0, 804(%rdi)
	movdqa	.LCPI0_1(%rip), %xmm0   # xmm0 = [0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 934(%rdi)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [56,55,54,53,52,51,50,49]
	movups	%xmm0, 820(%rdi)
	movdqa	.LCPI0_3(%rip), %xmm0   # xmm0 = [8,0,9,0,10,0,11,0,12,0,13,0,14,0,15,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 942(%rdi)
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [48,47,46,45,44,43,42,41]
	movups	%xmm0, 836(%rdi)
	movdqa	.LCPI0_5(%rip), %xmm0   # xmm0 = [16,0,17,0,18,0,19,0,20,0,21,0,22,0,23,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 950(%rdi)
	movaps	.LCPI0_6(%rip), %xmm0   # xmm0 = [40,39,38,37,36,35,34,33]
	movups	%xmm0, 852(%rdi)
	movdqa	.LCPI0_7(%rip), %xmm0   # xmm0 = [24,0,25,0,26,0,27,0,28,0,29,0,30,0,31,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 958(%rdi)
	movaps	.LCPI0_8(%rip), %xmm0   # xmm0 = [32,31,30,29,28,27,26,25]
	movups	%xmm0, 868(%rdi)
	movdqa	.LCPI0_9(%rip), %xmm0   # xmm0 = [32,0,33,0,34,0,35,0,36,0,37,0,38,0,39,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 966(%rdi)
	movaps	.LCPI0_10(%rip), %xmm0  # xmm0 = [24,23,22,21,20,19,18,17]
	movups	%xmm0, 884(%rdi)
	movdqa	.LCPI0_11(%rip), %xmm0  # xmm0 = [40,0,41,0,42,0,43,0,44,0,45,0,46,0,47,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 974(%rdi)
	movaps	.LCPI0_12(%rip), %xmm0  # xmm0 = [16,15,14,13,12,11,10,9]
	movups	%xmm0, 900(%rdi)
	movdqa	.LCPI0_13(%rip), %xmm0  # xmm0 = [48,0,49,0,50,0,51,0,52,0,53,0,54,0,55,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 982(%rdi)
	movaps	.LCPI0_14(%rip), %xmm0  # xmm0 = [8,7,6,5,4,3,2,1]
	movups	%xmm0, 916(%rdi)
	movdqa	.LCPI0_15(%rip), %xmm0  # xmm0 = [56,0,57,0,58,0,59,0,60,0,61,0,62,0,63,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 990(%rdi)
.LBB0_15:                               # %_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder4InitEj.exit27.2
	movw	$0, 932(%rdi)
	movq	%rax, 1000(%rdi)
	leaq	1008(%rdi), %rax
	leaq	1202(%rdi), %rcx
	cmpq	%rcx, %rax
	jae	.LBB0_19
# BB#16:                                # %_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder4InitEj.exit27.2
	leaq	1136(%rdi), %rax
	leaq	1138(%rdi), %rcx
	cmpq	%rax, %rcx
	jae	.LBB0_19
# BB#17:                                # %.lr.ph.i26.3.preheader
	movw	$61, %ax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_18:                               # %.lr.ph.i26.3
                                        # =>This Inner Loop Header: Depth=1
	leal	3(%rax), %edx
	movw	%dx, 1008(%rdi,%rcx,2)
	movb	%cl, 1138(%rdi,%rcx)
	leal	2(%rax), %edx
	movw	%dx, 1010(%rdi,%rcx,2)
	leal	1(%rcx), %edx
	movb	%dl, 1139(%rdi,%rcx)
	leal	1(%rax), %edx
	movw	%dx, 1012(%rdi,%rcx,2)
	leaq	2(%rcx), %rdx
	movb	%dl, 1140(%rdi,%rcx)
	movw	%ax, 1014(%rdi,%rcx,2)
	incl	%edx
	movb	%dl, 1141(%rdi,%rcx)
	leal	-4(%rax), %eax
	addq	$4, %rcx
	cmpq	$64, %rcx
	jne	.LBB0_18
	jmp	.LBB0_20
.LBB0_19:                               # %vector.body93
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [64,63,62,61,60,59,58,57]
	movups	%xmm0, 1008(%rdi)
	movdqa	.LCPI0_1(%rip), %xmm0   # xmm0 = [0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 1138(%rdi)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [56,55,54,53,52,51,50,49]
	movups	%xmm0, 1024(%rdi)
	movdqa	.LCPI0_3(%rip), %xmm0   # xmm0 = [8,0,9,0,10,0,11,0,12,0,13,0,14,0,15,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 1146(%rdi)
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [48,47,46,45,44,43,42,41]
	movups	%xmm0, 1040(%rdi)
	movdqa	.LCPI0_5(%rip), %xmm0   # xmm0 = [16,0,17,0,18,0,19,0,20,0,21,0,22,0,23,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 1154(%rdi)
	movaps	.LCPI0_6(%rip), %xmm0   # xmm0 = [40,39,38,37,36,35,34,33]
	movups	%xmm0, 1056(%rdi)
	movdqa	.LCPI0_7(%rip), %xmm0   # xmm0 = [24,0,25,0,26,0,27,0,28,0,29,0,30,0,31,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 1162(%rdi)
	movaps	.LCPI0_8(%rip), %xmm0   # xmm0 = [32,31,30,29,28,27,26,25]
	movups	%xmm0, 1072(%rdi)
	movdqa	.LCPI0_9(%rip), %xmm0   # xmm0 = [32,0,33,0,34,0,35,0,36,0,37,0,38,0,39,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 1170(%rdi)
	movaps	.LCPI0_10(%rip), %xmm0  # xmm0 = [24,23,22,21,20,19,18,17]
	movups	%xmm0, 1088(%rdi)
	movdqa	.LCPI0_11(%rip), %xmm0  # xmm0 = [40,0,41,0,42,0,43,0,44,0,45,0,46,0,47,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 1178(%rdi)
	movaps	.LCPI0_12(%rip), %xmm0  # xmm0 = [16,15,14,13,12,11,10,9]
	movups	%xmm0, 1104(%rdi)
	movdqa	.LCPI0_13(%rip), %xmm0  # xmm0 = [48,0,49,0,50,0,51,0,52,0,53,0,54,0,55,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 1186(%rdi)
	movaps	.LCPI0_14(%rip), %xmm0  # xmm0 = [8,7,6,5,4,3,2,1]
	movups	%xmm0, 1120(%rdi)
	movdqa	.LCPI0_15(%rip), %xmm0  # xmm0 = [56,0,57,0,58,0,59,0,60,0,61,0,62,0,63,0]
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 1194(%rdi)
.LBB0_20:                               # %_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder4InitEj.exit27.3
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
.Lcfi2:
	.cfi_offset %rbx, -24
.Lcfi3:
	.cfi_offset %rbp, -16
	movw	$0, 1136(%rdi)
	movl	176(%rdi), %eax
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %r8d
	cmovnel	%ecx, %r8d
	cmpl	$24, %r8d
	movl	$24, %eax
	cmovbl	%r8d, %eax
	movl	%eax, 1204(%rdi)
	movl	$4, 1208(%rdi)
	testl	%eax, %eax
	je	.LBB0_23
# BB#21:                                # %.lr.ph.preheader.i
	movl	%eax, %r9d
	cmpl	$8, %eax
	jae	.LBB0_24
# BB#22:
	xorl	%edx, %edx
	jmp	.LBB0_32
.LBB0_23:
	xorl	%r9d, %r9d
	jmp	.LBB0_38
.LBB0_24:                               # %min.iters.checked123
	movl	%eax, %r10d
	andl	$7, %r10d
	movq	%r9, %rdx
	subq	%r10, %rdx
	je	.LBB0_28
# BB#25:                                # %vector.memcheck133
	leaq	1212(%rdi), %rcx
	leaq	1342(%rdi,%r9), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB0_29
# BB#26:                                # %vector.memcheck133
	leal	(%rax,%rax), %ecx
	leaq	1212(%rdi,%rcx), %rcx
	leaq	1342(%rdi), %rsi
	cmpq	%rcx, %rsi
	jae	.LBB0_29
.LBB0_28:
	xorl	%edx, %edx
	jmp	.LBB0_32
.LBB0_29:                               # %vector.ph134
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm4        # xmm4 = xmm0[0,0,0,0]
	movdqa	.LCPI0_16(%rip), %xmm1  # xmm1 = [4,5,6,7]
	movdqa	.LCPI0_17(%rip), %xmm2  # xmm2 = [0,1,2,3]
	movdqa	.LCPI0_1(%rip), %xmm3   # xmm3 = [0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,0]
	xorl	%eax, %eax
	movdqa	.LCPI0_18(%rip), %xmm8  # xmm8 = [255,255,255,255,255,255,255,255]
	movdqa	.LCPI0_19(%rip), %xmm5  # xmm5 = [8,8,8,8]
	movdqa	.LCPI0_20(%rip), %xmm6  # xmm6 = [8,8,8,8,8,8,8,8]
	.p2align	4, 0x90
.LBB0_30:                               # %vector.body120
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm7
	psubd	%xmm1, %xmm7
	movdqa	%xmm4, %xmm0
	psubd	%xmm2, %xmm0
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	pslld	$16, %xmm7
	psrad	$16, %xmm7
	packssdw	%xmm7, %xmm0
	movdqu	%xmm0, 1212(%rdi,%rax,2)
	movdqa	%xmm3, %xmm0
	pand	%xmm8, %xmm0
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 1342(%rdi,%rax)
	addq	$8, %rax
	paddd	%xmm5, %xmm2
	paddd	%xmm5, %xmm1
	paddw	%xmm6, %xmm3
	cmpq	%rax, %rdx
	jne	.LBB0_30
# BB#31:                                # %middle.block121
	testl	%r10d, %r10d
	je	.LBB0_38
.LBB0_32:                               # %.lr.ph.i19.preheader
	movl	%r9d, %ecx
	subl	%edx, %ecx
	leaq	-1(%r9), %r10
	subq	%rdx, %r10
	andq	$3, %rcx
	je	.LBB0_35
# BB#33:                                # %.lr.ph.i19.prol.preheader
	movl	%r8d, %eax
	notl	%eax
	cmpl	$-25, %eax
	movl	$-25, %esi
	cmoval	%eax, %esi
	notl	%esi
	subl	%edx, %esi
	negq	%rcx
	.p2align	4, 0x90
.LBB0_34:                               # %.lr.ph.i19.prol
                                        # =>This Inner Loop Header: Depth=1
	movw	%si, 1212(%rdi,%rdx,2)
	movb	%dl, 1342(%rdi,%rdx)
	incq	%rdx
	decl	%esi
	incq	%rcx
	jne	.LBB0_34
.LBB0_35:                               # %.lr.ph.i19.prol.loopexit
	cmpq	$3, %r10
	jb	.LBB0_38
# BB#36:                                # %.lr.ph.i19.preheader.new
	movl	%r8d, %ecx
	notl	%ecx
	cmpl	$-25, %ecx
	movl	$-25, %eax
	cmoval	%ecx, %eax
	notl	%eax
	leal	3(%rdx), %ecx
	movl	%eax, %r10d
	subl	%ecx, %r10d
	leal	2(%rdx), %ecx
	movl	%eax, %r11d
	subl	%ecx, %r11d
	leal	1(%rdx), %ecx
	movl	%eax, %esi
	subl	%ecx, %esi
	subl	%edx, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_37:                               # %.lr.ph.i19
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rcx), %ebx
	movw	%bx, 1212(%rdi,%rdx,2)
	movb	%dl, 1342(%rdi,%rdx)
	leal	(%rsi,%rcx), %ebx
	movw	%bx, 1214(%rdi,%rdx,2)
	leal	1(%rdx), %ebx
	movb	%bl, 1343(%rdi,%rdx)
	leal	(%r11,%rcx), %ebx
	movw	%bx, 1216(%rdi,%rdx,2)
	leaq	2(%rdx), %rbx
	movb	%bl, 1344(%rdi,%rdx)
	leal	(%r10,%rcx), %ebp
	movw	%bp, 1218(%rdi,%rdx,2)
	incl	%ebx
	movb	%bl, 1345(%rdi,%rdx)
	addl	$-4, %ecx
	addq	$4, %rdx
	cmpq	%r9, %rdx
	jne	.LBB0_37
.LBB0_38:                               # %_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder4InitEj.exit20
	movw	$0, 1212(%rdi,%r9,2)
	cmpl	$36, %r8d
	movl	$36, %eax
	cmovbl	%r8d, %eax
	movl	%eax, 1408(%rdi)
	movl	$4, 1412(%rdi)
	testl	%eax, %eax
	je	.LBB0_41
# BB#39:                                # %.lr.ph.preheader.i.1
	movl	%eax, %r9d
	cmpl	$8, %eax
	jae	.LBB0_42
# BB#40:
	xorl	%edx, %edx
	jmp	.LBB0_50
.LBB0_41:
	xorl	%r9d, %r9d
	jmp	.LBB0_56
.LBB0_42:                               # %min.iters.checked149
	movl	%eax, %esi
	andl	$7, %esi
	movq	%r9, %rdx
	subq	%rsi, %rdx
	je	.LBB0_46
# BB#43:                                # %vector.memcheck163
	leaq	1416(%rdi), %rcx
	leaq	1546(%rdi,%r9), %rbp
	cmpq	%rbp, %rcx
	jae	.LBB0_47
# BB#44:                                # %vector.memcheck163
	leal	(%rax,%rax), %ecx
	leaq	1416(%rdi,%rcx), %rcx
	leaq	1546(%rdi), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB0_47
.LBB0_46:
	xorl	%edx, %edx
	jmp	.LBB0_50
.LBB0_47:                               # %vector.ph164
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm4        # xmm4 = xmm0[0,0,0,0]
	movdqa	.LCPI0_16(%rip), %xmm1  # xmm1 = [4,5,6,7]
	movdqa	.LCPI0_17(%rip), %xmm2  # xmm2 = [0,1,2,3]
	movdqa	.LCPI0_1(%rip), %xmm3   # xmm3 = [0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,0]
	xorl	%eax, %eax
	movdqa	.LCPI0_18(%rip), %xmm8  # xmm8 = [255,255,255,255,255,255,255,255]
	movdqa	.LCPI0_19(%rip), %xmm5  # xmm5 = [8,8,8,8]
	movdqa	.LCPI0_20(%rip), %xmm6  # xmm6 = [8,8,8,8,8,8,8,8]
	.p2align	4, 0x90
.LBB0_48:                               # %vector.body145
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm7
	psubd	%xmm1, %xmm7
	movdqa	%xmm4, %xmm0
	psubd	%xmm2, %xmm0
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	pslld	$16, %xmm7
	psrad	$16, %xmm7
	packssdw	%xmm7, %xmm0
	movdqu	%xmm0, 1416(%rdi,%rax,2)
	movdqa	%xmm3, %xmm0
	pand	%xmm8, %xmm0
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 1546(%rdi,%rax)
	addq	$8, %rax
	paddd	%xmm5, %xmm2
	paddd	%xmm5, %xmm1
	paddw	%xmm6, %xmm3
	cmpq	%rax, %rdx
	jne	.LBB0_48
# BB#49:                                # %middle.block146
	testl	%esi, %esi
	je	.LBB0_56
.LBB0_50:                               # %.lr.ph.i19.1.preheader
	movl	%r9d, %ecx
	subl	%edx, %ecx
	leaq	-1(%r9), %rax
	subq	%rdx, %rax
	andq	$3, %rcx
	je	.LBB0_53
# BB#51:                                # %.lr.ph.i19.1.prol.preheader
	movl	%r8d, %ebp
	notl	%ebp
	cmpl	$-37, %ebp
	movl	$-37, %esi
	cmoval	%ebp, %esi
	notl	%esi
	subl	%edx, %esi
	negq	%rcx
	.p2align	4, 0x90
.LBB0_52:                               # %.lr.ph.i19.1.prol
                                        # =>This Inner Loop Header: Depth=1
	movw	%si, 1416(%rdi,%rdx,2)
	movb	%dl, 1546(%rdi,%rdx)
	incq	%rdx
	decl	%esi
	incq	%rcx
	jne	.LBB0_52
.LBB0_53:                               # %.lr.ph.i19.1.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB0_56
# BB#54:                                # %.lr.ph.i19.1.preheader.new
	movl	%r8d, %ecx
	notl	%ecx
	cmpl	$-37, %ecx
	movl	$-37, %eax
	cmoval	%ecx, %eax
	notl	%eax
	leal	3(%rdx), %ecx
	movl	%eax, %r10d
	subl	%ecx, %r10d
	leal	2(%rdx), %ecx
	movl	%eax, %r11d
	subl	%ecx, %r11d
	leal	1(%rdx), %ecx
	movl	%eax, %esi
	subl	%ecx, %esi
	subl	%edx, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_55:                               # %.lr.ph.i19.1
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rcx), %ebx
	movw	%bx, 1416(%rdi,%rdx,2)
	movb	%dl, 1546(%rdi,%rdx)
	leal	(%rsi,%rcx), %ebx
	movw	%bx, 1418(%rdi,%rdx,2)
	leal	1(%rdx), %ebx
	movb	%bl, 1547(%rdi,%rdx)
	leal	(%r11,%rcx), %ebx
	movw	%bx, 1420(%rdi,%rdx,2)
	leaq	2(%rdx), %rbx
	movb	%bl, 1548(%rdi,%rdx)
	leal	(%r10,%rcx), %ebp
	movw	%bp, 1422(%rdi,%rdx,2)
	incl	%ebx
	movb	%bl, 1549(%rdi,%rdx)
	addl	$-4, %ecx
	addq	$4, %rdx
	cmpq	%r9, %rdx
	jne	.LBB0_55
.LBB0_56:                               # %_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder4InitEj.exit20.1
	movw	$0, 1416(%rdi,%r9,2)
	cmpl	$42, %r8d
	movl	$42, %eax
	cmovbl	%r8d, %eax
	movl	%eax, 1612(%rdi)
	movl	$4, 1616(%rdi)
	testl	%eax, %eax
	je	.LBB0_59
# BB#57:                                # %.lr.ph.preheader.i.2
	movl	%eax, %r9d
	cmpl	$8, %eax
	jae	.LBB0_60
# BB#58:
	xorl	%edx, %edx
	jmp	.LBB0_68
.LBB0_59:
	xorl	%r9d, %r9d
	jmp	.LBB0_74
.LBB0_60:                               # %min.iters.checked181
	movl	%eax, %esi
	andl	$7, %esi
	movq	%r9, %rdx
	subq	%rsi, %rdx
	je	.LBB0_64
# BB#61:                                # %vector.memcheck195
	leaq	1620(%rdi), %rcx
	leaq	1750(%rdi,%r9), %rbp
	cmpq	%rbp, %rcx
	jae	.LBB0_65
# BB#62:                                # %vector.memcheck195
	leal	(%rax,%rax), %ecx
	leaq	1620(%rdi,%rcx), %rcx
	leaq	1750(%rdi), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB0_65
.LBB0_64:
	xorl	%edx, %edx
	jmp	.LBB0_68
.LBB0_65:                               # %vector.ph196
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm4        # xmm4 = xmm0[0,0,0,0]
	movdqa	.LCPI0_16(%rip), %xmm1  # xmm1 = [4,5,6,7]
	movdqa	.LCPI0_17(%rip), %xmm2  # xmm2 = [0,1,2,3]
	movdqa	.LCPI0_1(%rip), %xmm3   # xmm3 = [0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,0]
	xorl	%eax, %eax
	movdqa	.LCPI0_18(%rip), %xmm8  # xmm8 = [255,255,255,255,255,255,255,255]
	movdqa	.LCPI0_19(%rip), %xmm5  # xmm5 = [8,8,8,8]
	movdqa	.LCPI0_20(%rip), %xmm6  # xmm6 = [8,8,8,8,8,8,8,8]
	.p2align	4, 0x90
.LBB0_66:                               # %vector.body177
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm4, %xmm7
	psubd	%xmm1, %xmm7
	movdqa	%xmm4, %xmm0
	psubd	%xmm2, %xmm0
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	pslld	$16, %xmm7
	psrad	$16, %xmm7
	packssdw	%xmm7, %xmm0
	movdqu	%xmm0, 1620(%rdi,%rax,2)
	movdqa	%xmm3, %xmm0
	pand	%xmm8, %xmm0
	packuswb	%xmm0, %xmm0
	movq	%xmm0, 1750(%rdi,%rax)
	addq	$8, %rax
	paddd	%xmm5, %xmm2
	paddd	%xmm5, %xmm1
	paddw	%xmm6, %xmm3
	cmpq	%rax, %rdx
	jne	.LBB0_66
# BB#67:                                # %middle.block178
	testl	%esi, %esi
	je	.LBB0_74
.LBB0_68:                               # %.lr.ph.i19.2.preheader
	movl	%r9d, %ecx
	subl	%edx, %ecx
	leaq	-1(%r9), %rax
	subq	%rdx, %rax
	andq	$3, %rcx
	je	.LBB0_71
# BB#69:                                # %.lr.ph.i19.2.prol.preheader
	movl	%r8d, %ebp
	notl	%ebp
	cmpl	$-43, %ebp
	movl	$-43, %esi
	cmoval	%ebp, %esi
	notl	%esi
	subl	%edx, %esi
	negq	%rcx
	.p2align	4, 0x90
.LBB0_70:                               # %.lr.ph.i19.2.prol
                                        # =>This Inner Loop Header: Depth=1
	movw	%si, 1620(%rdi,%rdx,2)
	movb	%dl, 1750(%rdi,%rdx)
	incq	%rdx
	decl	%esi
	incq	%rcx
	jne	.LBB0_70
.LBB0_71:                               # %.lr.ph.i19.2.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB0_74
# BB#72:                                # %.lr.ph.i19.2.preheader.new
	notl	%r8d
	cmpl	$-43, %r8d
	movl	$-43, %eax
	cmoval	%r8d, %eax
	notl	%eax
	leal	3(%rdx), %ecx
	movl	%eax, %r8d
	subl	%ecx, %r8d
	leal	2(%rdx), %ecx
	movl	%eax, %r10d
	subl	%ecx, %r10d
	leal	1(%rdx), %ecx
	movl	%eax, %esi
	subl	%ecx, %esi
	subl	%edx, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_73:                               # %.lr.ph.i19.2
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rcx), %ebp
	movw	%bp, 1620(%rdi,%rdx,2)
	movb	%dl, 1750(%rdi,%rdx)
	leal	(%rsi,%rcx), %ebp
	movw	%bp, 1622(%rdi,%rdx,2)
	leal	1(%rdx), %ebp
	movb	%bpl, 1751(%rdi,%rdx)
	leal	(%r10,%rcx), %ebp
	movw	%bp, 1624(%rdi,%rdx,2)
	leaq	2(%rdx), %rbp
	movb	%bpl, 1752(%rdi,%rdx)
	leal	(%r8,%rcx), %ebx
	movw	%bx, 1626(%rdi,%rdx,2)
	incl	%ebp
	movb	%bpl, 1753(%rdi,%rdx)
	addl	$-4, %ecx
	addq	$4, %rdx
	cmpq	%r9, %rdx
	jne	.LBB0_73
.LBB0_74:                               # %_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder4InitEj.exit20.2
	movw	$0, 1620(%rdi,%r9,2)
	movl	$27, 1816(%rdi)
	movl	$4, 1820(%rdi)
	movw	$27, 1824(%rdi)
	movb	$0, 1954(%rdi)
	movw	$26, 1826(%rdi)
	movb	$1, 1955(%rdi)
	movw	$25, 1828(%rdi)
	movb	$2, 1956(%rdi)
	movw	$24, 1830(%rdi)
	movb	$3, 1957(%rdi)
	movw	$23, 1832(%rdi)
	movb	$4, 1958(%rdi)
	movw	$22, 1834(%rdi)
	movb	$5, 1959(%rdi)
	movw	$21, 1836(%rdi)
	movb	$6, 1960(%rdi)
	movb	$7, 1961(%rdi)
	movb	$8, 1962(%rdi)
	movb	$9, 1963(%rdi)
	movb	$10, 1964(%rdi)
	movb	$11, 1965(%rdi)
	movb	$12, 1966(%rdi)
	movb	$13, 1967(%rdi)
	movaps	.LCPI0_21(%rip), %xmm0  # xmm0 = [20,19,18,17,16,15,14,13]
	movups	%xmm0, 1838(%rdi)
	movb	$14, 1968(%rdi)
	movb	$15, 1969(%rdi)
	movb	$16, 1970(%rdi)
	movb	$17, 1971(%rdi)
	movb	$18, 1972(%rdi)
	movb	$19, 1973(%rdi)
	movb	$20, 1974(%rdi)
	movb	$21, 1975(%rdi)
	movaps	.LCPI0_22(%rip), %xmm0  # xmm0 = [12,11,10,9,8,7,6,5]
	movups	%xmm0, 1854(%rdi)
	movb	$22, 1976(%rdi)
	movw	$4, 1870(%rdi)
	movb	$23, 1977(%rdi)
	movw	$3, 1872(%rdi)
	movb	$24, 1978(%rdi)
	movw	$2, 1874(%rdi)
	movb	$25, 1979(%rdi)
	movw	$1, 1876(%rdi)
	movb	$26, 1980(%rdi)
	movw	$0, 1878(%rdi)
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN9NCompress8NQuantum8CDecoder4InitEv, .Lfunc_end0-_ZN9NCompress8NQuantum8CDecoder4InitEv
	.cfi_endproc

	.globl	_ZN9NCompress8NQuantum8CDecoder8CodeSpecEj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum8CDecoder8CodeSpecEj,@function
_ZN9NCompress8NQuantum8CDecoder8CodeSpecEj: # @_ZN9NCompress8NQuantum8CDecoder8CodeSpecEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi7:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 112
.Lcfi11:
	.cfi_offset %rbx, -56
.Lcfi12:
	.cfi_offset %r12, -48
.Lcfi13:
	.cfi_offset %r13, -40
.Lcfi14:
	.cfi_offset %r14, -32
.Lcfi15:
	.cfi_offset %r15, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r12
	movl	168(%r12), %eax
	cmpl	$-2, %eax
	jne	.LBB1_13
# BB#1:
	cmpb	$0, 180(%r12)
	jne	.LBB1_4
# BB#2:
	leaq	32(%r12), %rdi
	movb	176(%r12), %cl
	movl	$1, %esi
	shll	%cl, %esi
	callq	_ZN10COutBuffer6CreateEj
	movl	$-2147024882, %ecx      # imm = 0x8007000E
	testb	%al, %al
	je	.LBB1_90
# BB#3:
	movq	%r12, %rdi
	callq	_ZN9NCompress8NQuantum8CDecoder4InitEv
.LBB1_4:
	leaq	112(%r12), %r14
	movl	$1048576, %esi          # imm = 0x100000
	movq	%r14, %rdi
	callq	_ZN9CInBuffer6CreateEj
	movl	$-2147024882, %ecx      # imm = 0x8007000E
	testb	%al, %al
	je	.LBB1_90
# BB#5:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer4InitEv
	movl	$65536, 104(%r12)       # imm = 0x10000
	movabsq	$281474976710656, %rax  # imm = 0x1000000000000
	movq	%rax, 88(%r12)
	xorl	%ecx, %ecx
	movl	$65536, %eax            # imm = 0x10000
	movl	$-16, %ebp
	.p2align	4, 0x90
.LBB1_6:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %r15d
	addl	%r15d, %r15d
	cmpl	$65536, %eax            # imm = 0x10000
	jb	.LBB1_11
# BB#7:                                 #   in Loop: Header=BB1_6 Depth=1
	movq	112(%r12), %rax
	cmpq	120(%r12), %rax
	jae	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_6 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB1_10
	.p2align	4, 0x90
.LBB1_9:                                #   in Loop: Header=BB1_6 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB1_10:                               # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i
                                        #   in Loop: Header=BB1_6 Depth=1
	movzbl	%al, %eax
	orl	$256, %eax              # imm = 0x100
	movl	%eax, 104(%r12)
.LBB1_11:                               # %_ZN9NCompress8NQuantum17CStreamBitDecoder7ReadBitEv.exit.i.i
                                        #   in Loop: Header=BB1_6 Depth=1
	movl	%eax, %ecx
	shrl	$7, %ecx
	andl	$1, %ecx
	addl	%eax, %eax
	movl	%eax, 104(%r12)
	orl	%r15d, %ecx
	incl	%ebp
	jne	.LBB1_6
# BB#12:                                # %_ZN9NCompress8NQuantum11NRangeCoder8CDecoder4InitEv.exit
	movl	%ecx, 96(%r12)
	movl	$0, 168(%r12)
	xorl	%eax, %eax
.LBB1_13:
	testl	%ebx, %ebx
	je	.LBB1_22
# BB#14:                                # %.preheader
	testl	%eax, %eax
	jle	.LBB1_23
# BB#15:                                # %.lr.ph90
	leaq	32(%r12), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_16:                               # =>This Inner Loop Header: Depth=1
	decl	%eax
	movl	%eax, 168(%r12)
	movl	40(%r12), %eax
	movl	%eax, %ecx
	subl	172(%r12), %ecx
	leal	-1(%rcx), %edx
	movl	52(%r12), %esi
	cmpl	%esi, %edx
	cmovbl	%ebp, %esi
	leal	-1(%rsi,%rcx), %ecx
	movq	32(%r12), %rdx
	movzbl	(%rdx,%rcx), %ecx
	leal	1(%rax), %esi
	movl	%esi, 40(%r12)
	movb	%cl, (%rdx,%rax)
	movl	40(%r12), %eax
	cmpl	44(%r12), %eax
	jne	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_16 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB1_18:                               # %_ZN12CLzOutWindow7PutByteEh.exit
                                        #   in Loop: Header=BB1_16 Depth=1
	leal	-1(%rbx), %r15d
	cmpl	$1, %ebx
	je	.LBB1_20
# BB#19:                                # %_ZN12CLzOutWindow7PutByteEh.exit
                                        #   in Loop: Header=BB1_16 Depth=1
	movl	168(%r12), %eax
	testl	%eax, %eax
	movl	%r15d, %ebx
	jg	.LBB1_16
.LBB1_20:                               # %_ZN12CLzOutWindow9CopyBlockEjj.exit.thread82.preheader
	testl	%r15d, %r15d
	jne	.LBB1_24
# BB#21:                                # %_ZN12CLzOutWindow9CopyBlockEjj.exit.thread82.preheader..loopexit84_crit_edge
	addq	$156, %r12
	movq	%r12, %rbx
	jmp	.LBB1_89
.LBB1_22:
	xorl	%ecx, %ecx
	jmp	.LBB1_90
.LBB1_23:
	movl	%ebx, %r15d
.LBB1_24:                               # %.lr.ph
	leaq	88(%r12), %rbp
	leaq	156(%r12), %rbx
	leaq	112(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	184(%r12), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	32(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	1816(%r12), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	jmp	.LBB1_86
	.p2align	4, 0x90
.LBB1_25:                               #   in Loop: Header=BB1_86 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	callq	_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder6DecodeEPNS1_8CDecoderE
	movl	%eax, %r14d
	cmpl	$3, %r14d
	ja	.LBB1_29
# BB#26:                                #   in Loop: Header=BB1_86 Depth=1
	movl	%r14d, %eax
	shll	$6, %r14d
	imulq	$204, %rax, %rax
	leaq	388(%r12,%rax), %rdi
	movq	%rbp, %rsi
	callq	_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder6DecodeEPNS1_8CDecoderE
	addl	%r14d, %eax
	movq	32(%r12), %rcx
	movl	40(%r12), %edx
	leal	1(%rdx), %esi
	movl	%esi, 40(%r12)
	movb	%al, (%rcx,%rdx)
	movl	40(%r12), %eax
	cmpl	44(%r12), %eax
	jne	.LBB1_28
# BB#27:                                #   in Loop: Header=BB1_86 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB1_28:                               # %_ZN12CLzOutWindow7PutByteEh.exit70
                                        #   in Loop: Header=BB1_86 Depth=1
	decl	%r15d
	jmp	.LBB1_66
	.p2align	4, 0x90
.LBB1_29:                               #   in Loop: Header=BB1_86 Depth=1
	leal	-4(%r14), %ebx
	leal	-1(%r14), %r13d
	cmpl	$2, %ebx
	jne	.LBB1_42
# BB#30:                                #   in Loop: Header=BB1_86 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	callq	_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder6DecodeEPNS1_8CDecoderE
	cmpl	$6, %eax
	jb	.LBB1_40
# BB#31:                                #   in Loop: Header=BB1_86 Depth=1
	addl	$-2, %eax
	movl	%eax, %ebp
	shrl	$2, %ebp
	movl	%eax, %edx
	andl	$3, %edx
	orl	$4, %edx
	movl	%ebp, %ecx
	shll	%cl, %edx
	leal	-3(%r14,%rdx), %r13d
	cmpl	$23, %eax
	ja	.LBB1_41
# BB#32:                                #   in Loop: Header=BB1_86 Depth=1
	movl	104(%r12), %eax
	negl	%ebp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_33:                               #   Parent Loop BB1_86 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %r14d
	addl	%r14d, %r14d
	cmpl	$65536, %eax            # imm = 0x10000
	jb	.LBB1_38
# BB#34:                                #   in Loop: Header=BB1_33 Depth=2
	movq	112(%r12), %rax
	cmpq	120(%r12), %rax
	jae	.LBB1_36
# BB#35:                                #   in Loop: Header=BB1_33 Depth=2
	leaq	1(%rax), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rcx, (%rdx)
	movzbl	(%rax), %eax
	jmp	.LBB1_37
	.p2align	4, 0x90
.LBB1_36:                               #   in Loop: Header=BB1_33 Depth=2
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB1_37:                               # %_ZN9CInBuffer8ReadByteEv.exit.i.i
                                        #   in Loop: Header=BB1_33 Depth=2
	movzbl	%al, %eax
	orl	$256, %eax              # imm = 0x100
	movl	%eax, 104(%r12)
.LBB1_38:                               # %_ZN9NCompress8NQuantum17CStreamBitDecoder7ReadBitEv.exit.i
                                        #   in Loop: Header=BB1_33 Depth=2
	movl	%eax, %ecx
	shrl	$7, %ecx
	andl	$1, %ecx
	addl	%eax, %eax
	movl	%eax, 104(%r12)
	orl	%r14d, %ecx
	incl	%ebp
	jne	.LBB1_33
# BB#39:                                # %_ZN9NCompress8NQuantum17CStreamBitDecoder8ReadBitsEi.exit
                                        #   in Loop: Header=BB1_86 Depth=1
	addl	%ecx, %r13d
.LBB1_41:                               #   in Loop: Header=BB1_86 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB1_42
.LBB1_40:                               #   in Loop: Header=BB1_86 Depth=1
	addl	%eax, %r13d
.LBB1_42:                               #   in Loop: Header=BB1_86 Depth=1
	movl	%ebx, %eax
	imulq	$204, %rax, %rax
	leaq	1204(%r12,%rax), %rdi
	movq	%rbp, %rsi
	callq	_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder6DecodeEPNS1_8CDecoderE
	movl	%eax, %r14d
	cmpl	$4, %r14d
	movl	%r13d, 4(%rsp)          # 4-byte Spill
	jb	.LBB1_51
# BB#43:                                #   in Loop: Header=BB1_86 Depth=1
	movl	%r14d, %r13d
	shrl	%r13d
	andl	$1, %r14d
	orl	$2, %r14d
	movl	104(%r12), %eax
	movl	$1, %ebp
	subl	%r13d, %ebp
	decl	%r13d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_44:                               #   Parent Loop BB1_86 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %ebx
	addl	%ebx, %ebx
	cmpl	$65536, %eax            # imm = 0x10000
	jb	.LBB1_49
# BB#45:                                #   in Loop: Header=BB1_44 Depth=2
	movq	112(%r12), %rax
	cmpq	120(%r12), %rax
	jae	.LBB1_47
# BB#46:                                #   in Loop: Header=BB1_44 Depth=2
	leaq	1(%rax), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rcx, (%rdx)
	movzbl	(%rax), %eax
	jmp	.LBB1_48
	.p2align	4, 0x90
.LBB1_47:                               #   in Loop: Header=BB1_44 Depth=2
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB1_48:                               # %_ZN9CInBuffer8ReadByteEv.exit.i.i77
                                        #   in Loop: Header=BB1_44 Depth=2
	movzbl	%al, %eax
	orl	$256, %eax              # imm = 0x100
	movl	%eax, 104(%r12)
.LBB1_49:                               # %_ZN9NCompress8NQuantum17CStreamBitDecoder7ReadBitEv.exit.i78
                                        #   in Loop: Header=BB1_44 Depth=2
	movl	%eax, %edx
	shrl	$7, %edx
	andl	$1, %edx
	addl	%eax, %eax
	movl	%eax, 104(%r12)
	orl	%ebx, %edx
	incl	%ebp
	jne	.LBB1_44
# BB#50:                                # %_ZN9NCompress8NQuantum17CStreamBitDecoder8ReadBitsEi.exit79
                                        #   in Loop: Header=BB1_86 Depth=1
	movl	%r13d, %ecx
	shll	%cl, %r14d
	addl	%edx, %r14d
	movl	4(%rsp), %r13d          # 4-byte Reload
.LBB1_51:                               #   in Loop: Header=BB1_86 Depth=1
	cmpl	%r15d, %r13d
	movl	%r13d, %r8d
	cmoval	%r15d, %r8d
	movl	40(%r12), %edx
	movl	%edx, %ebp
	subl	%r14d, %ebp
	decl	%ebp
	cmpl	%r14d, %edx
	ja	.LBB1_55
# BB#52:                                #   in Loop: Header=BB1_86 Depth=1
	cmpb	$0, 80(%r12)
	je	.LBB1_92
# BB#53:                                #   in Loop: Header=BB1_86 Depth=1
	movl	52(%r12), %eax
	cmpl	%r14d, %eax
	jbe	.LBB1_92
# BB#54:                                #   in Loop: Header=BB1_86 Depth=1
	addl	%ebp, %eax
	movl	%eax, %ebp
.LBB1_55:                               #   in Loop: Header=BB1_86 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	44(%r12), %eax
	subl	%edx, %eax
	cmpl	%r8d, %eax
	movl	%r8d, (%rsp)            # 4-byte Spill
	jbe	.LBB1_59
# BB#56:                                #   in Loop: Header=BB1_86 Depth=1
	movl	52(%r12), %eax
	subl	%ebp, %eax
	cmpl	%r8d, %eax
	jbe	.LBB1_59
# BB#57:                                #   in Loop: Header=BB1_86 Depth=1
	movq	32(%r12), %r11
	movl	%ebp, %r9d
	leaq	(%r11,%r9), %rax
	leaq	(%r11,%rdx), %rcx
	movl	%edx, %esi
	addl	%r8d, %esi
	movl	%esi, 40(%r12)
	cmpl	%r15d, %r13d
                                        # kill: %R13D<def> %R13D<kill> %R13<def>
	cmoval	%r15d, %r13d
	decl	%r13d
	incq	%r13
	cmpq	$32, %r13
	jb	.LBB1_71
# BB#67:                                # %min.iters.checked
                                        #   in Loop: Header=BB1_86 Depth=1
	movq	%r13, %r8
	movabsq	$8589934560, %rsi       # imm = 0x1FFFFFFE0
	andq	%rsi, %r8
	je	.LBB1_70
# BB#68:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_86 Depth=1
	movl	4(%rsp), %esi           # 4-byte Reload
	cmpl	%r15d, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	cmoval	%r15d, %esi
	decl	%esi
	leaq	(%r9,%rsi), %rdi
	leaq	1(%r11,%rdi), %rdi
	cmpq	%rdi, %rcx
	jae	.LBB1_77
# BB#69:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_86 Depth=1
	addq	%rdx, %rsi
	leaq	1(%r11,%rsi), %rsi
	cmpq	%rsi, %rax
	jae	.LBB1_77
.LBB1_70:                               #   in Loop: Header=BB1_86 Depth=1
	movl	(%rsp), %r8d            # 4-byte Reload
.LBB1_71:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB1_86 Depth=1
	movl	%r8d, %edx
.LBB1_72:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB1_86 Depth=1
	leal	-1(%rdx), %esi
	movl	%edx, %edi
	andl	$7, %edi
	je	.LBB1_75
# BB#73:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB1_86 Depth=1
	negl	%edi
	.p2align	4, 0x90
.LBB1_74:                               # %scalar.ph.prol
                                        #   Parent Loop BB1_86 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ebx
	incq	%rax
	movb	%bl, (%rcx)
	incq	%rcx
	decl	%edx
	incl	%edi
	jne	.LBB1_74
.LBB1_75:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB1_86 Depth=1
	cmpl	$7, %esi
	jb	.LBB1_64
	.p2align	4, 0x90
.LBB1_76:                               # %scalar.ph
                                        #   Parent Loop BB1_86 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ebx
	movb	%bl, (%rcx)
	movzbl	1(%rax), %ebx
	movb	%bl, 1(%rcx)
	movzbl	2(%rax), %ebx
	movb	%bl, 2(%rcx)
	movzbl	3(%rax), %ebx
	movb	%bl, 3(%rcx)
	movzbl	4(%rax), %ebx
	movb	%bl, 4(%rcx)
	movzbl	5(%rax), %ebx
	movb	%bl, 5(%rcx)
	movzbl	6(%rax), %ebx
	movb	%bl, 6(%rcx)
	movzbl	7(%rax), %ebx
	movb	%bl, 7(%rcx)
	addq	$8, %rax
	addq	$8, %rcx
	addl	$-8, %edx
	jne	.LBB1_76
	jmp	.LBB1_64
	.p2align	4, 0x90
.LBB1_59:                               # %.preheader.i.preheader
                                        #   in Loop: Header=BB1_86 Depth=1
	movl	%r15d, %eax
	notl	%eax
	notl	%r13d
	cmpl	%r13d, %eax
	cmoval	%eax, %r13d
	addl	$2, %r13d
	jmp	.LBB1_61
	.p2align	4, 0x90
.LBB1_60:                               # %._crit_edge.i
                                        #   in Loop: Header=BB1_61 Depth=2
	incl	%ebp
	movl	40(%r12), %edx
	incl	%r13d
.LBB1_61:                               # %.preheader.i
                                        #   Parent Loop BB1_86 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	52(%r12), %ebp
	movl	$0, %eax
	cmovel	%eax, %ebp
	movq	32(%r12), %rax
	movzbl	(%rax,%rbp), %ecx
	leal	1(%rdx), %esi
	movl	%esi, 40(%r12)
	movl	%edx, %edx
	movb	%cl, (%rax,%rdx)
	movl	40(%r12), %eax
	cmpl	44(%r12), %eax
	jne	.LBB1_63
# BB#62:                                #   in Loop: Header=BB1_61 Depth=2
	movq	%rbx, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB1_63:                               #   in Loop: Header=BB1_61 Depth=2
	testl	%r13d, %r13d
	jne	.LBB1_60
.LBB1_64:                               # %.loopexit
                                        #   in Loop: Header=BB1_86 Depth=1
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
	subl	%ecx, %eax
	jne	.LBB1_88
# BB#65:                                #   in Loop: Header=BB1_86 Depth=1
	subl	%ecx, %r15d
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB1_66:                               # %_ZN12CLzOutWindow9CopyBlockEjj.exit.thread82.backedge
                                        #   in Loop: Header=BB1_86 Depth=1
	testl	%r15d, %r15d
	jne	.LBB1_86
	jmp	.LBB1_89
.LBB1_77:                               # %vector.body.preheader
                                        #   in Loop: Header=BB1_86 Depth=1
	leaq	-32(%r8), %rbx
	movl	%ebx, %ebp
	shrl	$5, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB1_80
# BB#78:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB1_86 Depth=1
	leaq	16(%r11,%rdx), %rsi
	leaq	16(%r11,%r9), %r10
	negq	%rbp
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_79:                               # %vector.body.prol
                                        #   Parent Loop BB1_86 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%r10,%rdi), %xmm0
	movups	(%r10,%rdi), %xmm1
	movups	%xmm0, -16(%rsi,%rdi)
	movups	%xmm1, (%rsi,%rdi)
	addq	$32, %rdi
	incq	%rbp
	jne	.LBB1_79
	jmp	.LBB1_81
.LBB1_80:                               #   in Loop: Header=BB1_86 Depth=1
	xorl	%edi, %edi
.LBB1_81:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB1_86 Depth=1
	cmpq	$96, %rbx
	jb	.LBB1_84
# BB#82:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB1_86 Depth=1
	movq	%r8, %rbp
	subq	%rdi, %rbp
	leaq	(%rdx,%rdi), %rdx
	leaq	112(%r11,%rdx), %rdx
	addq	%rdi, %r9
	leaq	112(%r11,%r9), %rdi
	.p2align	4, 0x90
.LBB1_83:                               # %vector.body
                                        #   Parent Loop BB1_86 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rdi
	addq	$-128, %rbp
	jne	.LBB1_83
.LBB1_84:                               # %middle.block
                                        #   in Loop: Header=BB1_86 Depth=1
	cmpq	%r8, %r13
	je	.LBB1_64
# BB#85:                                #   in Loop: Header=BB1_86 Depth=1
	movl	(%rsp), %esi            # 4-byte Reload
	movl	%esi, %edx
	subl	%r8d, %edx
	addq	%r8, %rax
	addq	%r8, %rcx
	jmp	.LBB1_72
	.p2align	4, 0x90
.LBB1_86:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_33 Depth 2
                                        #     Child Loop BB1_44 Depth 2
                                        #     Child Loop BB1_61 Depth 2
                                        #     Child Loop BB1_79 Depth 2
                                        #     Child Loop BB1_83 Depth 2
                                        #     Child Loop BB1_74 Depth 2
                                        #     Child Loop BB1_76 Depth 2
	cmpb	$0, (%rbx)
	je	.LBB1_25
# BB#87:
	movl	$1, %ecx
	jmp	.LBB1_90
.LBB1_88:                               # %_ZN12CLzOutWindow9CopyBlockEjj.exit
	movl	%eax, 168(%r12)
	movl	%r14d, 172(%r12)
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB1_89:                               # %.loopexit84
	movzbl	(%rbx), %ecx
.LBB1_90:                               # %_ZN12CLzOutWindow9CopyBlockEjj.exit.thread
	movl	%ecx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_92:
	movl	$1, %ecx
	jmp	.LBB1_90
.Lfunc_end1:
	.size	_ZN9NCompress8NQuantum8CDecoder8CodeSpecEj, .Lfunc_end1-_ZN9NCompress8NQuantum8CDecoder8CodeSpecEj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
	.section	.text._ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder6DecodeEPNS1_8CDecoderE,"axG",@progbits,_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder6DecodeEPNS1_8CDecoderE,comdat
	.weak	_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder6DecodeEPNS1_8CDecoderE
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder6DecodeEPNS1_8CDecoderE,@function
_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder6DecodeEPNS1_8CDecoderE: # @_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder6DecodeEPNS1_8CDecoderE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 48
.Lcfi22:
	.cfi_offset %rbx, -48
.Lcfi23:
	.cfi_offset %r12, -40
.Lcfi24:
	.cfi_offset %r13, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movzwl	8(%r14), %ecx
	movl	8(%rsi), %eax
	incl	%eax
	imull	%ecx, %eax
	decl	%eax
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	divl	4(%rsi)
	movl	$-1, %r15d
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	leal	2(%r15), %edx
	movzwl	8(%r14,%rdx,2), %ebx
	incl	%r12d
	incl	%r15d
	cmpl	%eax, %ebx
	ja	.LBB2_1
# BB#2:
	movl	%r15d, %r13d
	movzwl	8(%r14,%r13,2), %edx
	movq	%rsi, %rdi
	movl	%ebx, %esi
	callq	_ZN9NCompress8NQuantum11NRangeCoder8CDecoder6DecodeEjjj
	movzbl	138(%r14,%r13), %eax
	leal	1(%r15), %ecx
	cmpl	$16, %ecx
	jb	.LBB2_11
# BB#3:                                 # %min.iters.checked
	movl	%ecx, %edx
	andl	$-16, %edx
	je	.LBB2_11
# BB#4:                                 # %vector.body.preheader
	leal	-16(%rdx), %edi
	movl	%edi, %esi
	shrl	$4, %esi
	xorl	%ebx, %ebx
	btl	$4, %edi
	jb	.LBB2_6
# BB#5:                                 # %vector.body.prol
	movdqu	-6(%r14,%r13,2), %xmm0
	movdqu	-22(%r14,%r13,2), %xmm1
	movdqa	.LCPI2_0(%rip), %xmm2   # xmm2 = [8,8,8,8,8,8,8,8]
	paddw	%xmm2, %xmm0
	paddw	%xmm2, %xmm1
	movdqu	%xmm0, -6(%r14,%r13,2)
	movdqu	%xmm1, -22(%r14,%r13,2)
	movl	$16, %ebx
.LBB2_6:                                # %vector.body.prol.loopexit
	testl	%esi, %esi
	je	.LBB2_9
# BB#7:                                 # %vector.body.preheader.new
	movl	%edx, %esi
	subl	%ebx, %esi
	movl	%r15d, %edi
	subl	%ebx, %edi
	movdqa	.LCPI2_0(%rip), %xmm0   # xmm0 = [8,8,8,8,8,8,8,8]
	.p2align	4, 0x90
.LBB2_8:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, %ebx
	movdqu	-6(%r14,%rbx,2), %xmm1
	movdqu	-22(%r14,%rbx,2), %xmm2
	paddw	%xmm0, %xmm1
	paddw	%xmm0, %xmm2
	movdqu	%xmm1, -6(%r14,%rbx,2)
	movdqu	%xmm2, -22(%r14,%rbx,2)
	leal	-16(%rdi), %ebx
	movdqu	-6(%r14,%rbx,2), %xmm1
	movdqu	-22(%r14,%rbx,2), %xmm2
	paddw	%xmm0, %xmm1
	paddw	%xmm0, %xmm2
	movdqu	%xmm1, -6(%r14,%rbx,2)
	movdqu	%xmm2, -22(%r14,%rbx,2)
	addl	$-32, %edi
	addl	$-32, %esi
	jne	.LBB2_8
.LBB2_9:                                # %middle.block
	cmpl	%edx, %ecx
	je	.LBB2_13
# BB#10:
	notl	%r12d
	orl	$15, %r12d
	leal	1(%r12,%r15), %r15d
.LBB2_11:                               # %scalar.ph.preheader
	movl	%r15d, %ecx
	.p2align	4, 0x90
.LBB2_12:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	addw	$8, 8(%r14,%rcx,2)
	decq	%rcx
	cmpl	$-1, %ecx
	jne	.LBB2_12
.LBB2_13:                               # %.loopexit83
	movzwl	8(%r14), %edx
	cmpl	$3801, %edx             # imm = 0xED9
	jb	.LBB2_42
# BB#14:
	decl	4(%r14)
	je	.LBB2_15
# BB#36:
	movl	(%r14), %esi
	movw	8(%r14,%rsi,2), %dx
	testb	$1, %sil
	jne	.LBB2_38
# BB#37:
	movl	%esi, %ecx
	cmpl	$1, %esi
	jne	.LBB2_40
	jmp	.LBB2_42
.LBB2_15:
	movl	$50, 4(%r14)
	movl	(%r14), %ecx
	testq	%rcx, %rcx
	je	.LBB2_16
# BB#18:                                # %.lr.ph61
	testb	$1, %cl
	jne	.LBB2_20
# BB#19:
	xorl	%esi, %esi
	cmpl	$1, %ecx
	jne	.LBB2_29
	jmp	.LBB2_22
.LBB2_38:
	leal	-1(%rsi), %ecx
	movzwl	8(%r14,%rcx,2), %edi
	shrl	%edi
	cmpw	%dx, %di
	leal	1(%rdx), %edx
	cmovaw	%di, %dx
	movw	%dx, 8(%r14,%rcx,2)
                                        # kill: %DX<def> %DX<kill> %EDX<kill> %RDX<def>
	cmpl	$1, %esi
	je	.LBB2_42
.LBB2_40:                               # %.new
	decl	%ecx
	.p2align	4, 0x90
.LBB2_41:                               # =>This Inner Loop Header: Depth=1
	movl	%ecx, %esi
	movzwl	8(%r14,%rsi,2), %edi
	shrl	%edi
	leal	1(%rdx), %ebx
	cmpw	%dx, %di
	cmovaw	%di, %bx
	movw	%bx, 8(%r14,%rsi,2)
	leal	-1(%rcx), %esi
	movzwl	8(%r14,%rsi,2), %edi
	shrl	%edi
	leal	1(%rbx), %edx
	cmpw	%bx, %di
	cmovaw	%di, %dx
	movw	%dx, 8(%r14,%rsi,2)
	addl	$-2, %ecx
	cmpl	$-1, %ecx
	jne	.LBB2_41
	jmp	.LBB2_42
.LBB2_16:
	xorl	%r9d, %r9d
	jmp	.LBB2_17
.LBB2_20:
	movzwl	10(%r14), %edi
	incl	%edx
	subl	%edi, %edx
	shrl	%edx
	movw	%dx, 8(%r14)
	movl	$1, %esi
	movw	%di, %dx
	cmpl	$1, %ecx
	je	.LBB2_22
	.p2align	4, 0x90
.LBB2_29:                               # =>This Inner Loop Header: Depth=1
	movzwl	%dx, %edx
	movzwl	10(%r14,%rsi,2), %edi
	incl	%edx
	subl	%edi, %edx
	shrl	%edx
	movw	%dx, 8(%r14,%rsi,2)
	movzwl	12(%r14,%rsi,2), %edx
	incl	%edi
	subl	%edx, %edi
	shrl	%edi
	movw	%di, 10(%r14,%rsi,2)
	leaq	2(%rsi), %rsi
	cmpq	%rcx, %rsi
	jb	.LBB2_29
.LBB2_22:                               # %.preheader54
	movl	(%r14), %r9d
	cmpl	$1, %r9d
	jne	.LBB2_17
# BB#23:
	xorl	%edx, %edx
	jmp	.LBB2_24
.LBB2_17:                               # %.lr.ph59
	movl	%r9d, %r8d
	decl	%r9d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB2_31:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_33 Depth 2
	movq	%r10, %rsi
	leaq	1(%rsi), %r10
	cmpq	%r8, %r10
	jae	.LBB2_30
# BB#32:                                # %.lr.ph
                                        #   in Loop: Header=BB2_31 Depth=1
	movl	(%r14), %r11d
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB2_33:                               #   Parent Loop BB2_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	8(%r14,%rsi,2), %ecx
	movzwl	10(%r14,%rdi,2), %edx
	cmpw	%dx, %cx
	jae	.LBB2_35
# BB#34:                                #   in Loop: Header=BB2_33 Depth=2
	movzbl	138(%r14,%rsi), %ebx
	movw	%dx, 8(%r14,%rsi,2)
	movzbl	139(%r14,%rdi), %edx
	movb	%dl, 138(%r14,%rsi)
	movw	%cx, 10(%r14,%rdi,2)
	movb	%bl, 139(%r14,%rdi)
.LBB2_35:                               #   in Loop: Header=BB2_33 Depth=2
	incq	%rdi
	leal	1(%rdi), %ecx
	cmpl	%r11d, %ecx
	jb	.LBB2_33
.LBB2_30:                               # %.loopexit53
                                        #   in Loop: Header=BB2_31 Depth=1
	cmpq	%r9, %r10
	jb	.LBB2_31
# BB#28:                                # %.preheader.preheader.loopexit
	movl	%r10d, %edx
.LBB2_24:                               # %.preheader.preheader
	testb	$1, %dl
	movq	%rdx, %rcx
	jne	.LBB2_26
# BB#25:                                # %.preheader.prol
	leal	1(%rdx), %ecx
	movzwl	8(%r14,%rcx,2), %ecx
	addw	%cx, 8(%r14,%rdx,2)
	leaq	-1(%rdx), %rcx
.LBB2_26:                               # %.preheader.prol.loopexit
	testq	%rdx, %rdx
	je	.LBB2_42
	.p2align	4, 0x90
.LBB2_27:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rcx), %edx
	movzwl	8(%r14,%rdx,2), %edx
	addw	%dx, 8(%r14,%rcx,2)
	movl	%ecx, %edx
	movzwl	8(%r14,%rdx,2), %edx
	addw	%dx, 6(%r14,%rcx,2)
	addq	$-2, %rcx
	cmpq	$-1, %rcx
	jne	.LBB2_27
.LBB2_42:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder6DecodeEPNS1_8CDecoderE, .Lfunc_end2-_ZN9NCompress8NQuantum11NRangeCoder13CModelDecoder6DecodeEPNS1_8CDecoderE
	.cfi_endproc

	.text
	.globl	_ZN9NCompress8NQuantum8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress8NQuantum8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress8NQuantum8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 80
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, %rbx
	movq	%rdx, %r13
	movq	%rdi, %r15
	testq	%rbx, %rbx
	je	.LBB3_1
# BB#2:
	movq	(%rbx), %rbp
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*48(%rax)
	leaq	32(%r15), %r14
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	*64(%rax)
.Ltmp0:
	movq	%r14, %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
	movq	%rax, %rbx
.Ltmp1:
# BB#3:                                 # %.thread.preheader
	testq	%r12, %r12
	je	.LBB3_4
	.p2align	4, 0x90
.LBB3_12:                               # %.thread.outer
                                        # =>This Inner Loop Header: Depth=1
.Ltmp3:
	movq	%r14, %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
.Ltmp4:
# BB#13:                                #   in Loop: Header=BB3_12 Depth=1
	movq	%rbx, %rsi
	subq	%rax, %rsi
	addq	%rbp, %rsi
	cmpq	$262144, %rsi           # imm = 0x40000
	movl	$262144, %eax           # imm = 0x40000
	cmovael	%eax, %esi
	testl	%esi, %esi
	je	.LBB3_23
# BB#14:                                #   in Loop: Header=BB3_12 Depth=1
.Ltmp6:
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN9NCompress8NQuantum8CDecoder8CodeSpecEj
	movl	%eax, %r13d
.Ltmp7:
# BB#15:                                #   in Loop: Header=BB3_12 Depth=1
	testl	%r13d, %r13d
	jne	.LBB3_9
# BB#16:                                # %.us-lcssa93
                                        #   in Loop: Header=BB3_12 Depth=1
	movq	112(%r15), %rax
	addq	144(%r15), %rax
	subq	128(%r15), %rax
	movq	%rax, 16(%rsp)
.Ltmp9:
	movq	%r14, %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
.Ltmp10:
# BB#17:                                #   in Loop: Header=BB3_12 Depth=1
	subq	%rbx, %rax
	movq	%rax, 8(%rsp)
	movq	(%r12), %rax
.Ltmp12:
	movq	%r12, %rdi
	leaq	16(%rsp), %rsi
	leaq	8(%rsp), %rdx
	callq	*40(%rax)
	movl	%eax, %r13d
.Ltmp13:
# BB#18:                                #   in Loop: Header=BB3_12 Depth=1
	testl	%r13d, %r13d
	je	.LBB3_12
	jmp	.LBB3_9
.LBB3_1:
	movl	$-2147024809, %r13d     # imm = 0x80070057
	jmp	.LBB3_27
.LBB3_4:                                # %.thread.us.us.preheader
	movl	$262144, %r12d          # imm = 0x40000
	.p2align	4, 0x90
.LBB3_5:                                # %.thread.us.us
                                        # =>This Inner Loop Header: Depth=1
.Ltmp15:
	movq	%r14, %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
.Ltmp16:
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=1
	movq	%rbx, %rsi
	subq	%rax, %rsi
	addq	%rbp, %rsi
	cmpq	$262144, %rsi           # imm = 0x40000
	cmovael	%r12d, %esi
	testl	%esi, %esi
	je	.LBB3_23
# BB#7:                                 #   in Loop: Header=BB3_5 Depth=1
.Ltmp18:
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN9NCompress8NQuantum8CDecoder8CodeSpecEj
	movl	%eax, %r13d
.Ltmp19:
# BB#8:                                 #   in Loop: Header=BB3_5 Depth=1
	testl	%r13d, %r13d
	je	.LBB3_5
.LBB3_9:                                # %_ZN9NCompress8NQuantum8CDecoder5FlushEv.exit
	movq	%r14, %rdi
	callq	_ZN10COutBuffer5FlushEv
	jmp	.LBB3_24
.LBB3_23:                               # %.thread75
.Ltmp23:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer5FlushEv
	movl	%eax, %r13d
.Ltmp24:
.LBB3_24:                               # %._crit_edge.i64
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB3_26
# BB#25:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 56(%r15)
.LBB3_26:                               # %_ZN9NCompress8NQuantum8CDecoder15CDecoderFlusherD2Ev.exit66
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*56(%rax)
.LBB3_27:
	movl	%r13d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_28:
.Ltmp25:
	movq	%rax, %rbx
	jmp	.LBB3_31
.LBB3_29:
.Ltmp2:
	jmp	.LBB3_30
.LBB3_11:                               # %.us-lcssa91.us.us-lcssa.us
.Ltmp20:
	jmp	.LBB3_30
.LBB3_10:                               # %.us-lcssa.us.us-lcssa.us
.Ltmp17:
	jmp	.LBB3_30
.LBB3_22:                               # %.us-lcssa97
.Ltmp14:
	jmp	.LBB3_30
.LBB3_21:                               # %.us-lcssa95
.Ltmp11:
	jmp	.LBB3_30
.LBB3_20:                               # %.us-lcssa91.us-lcssa
.Ltmp8:
	jmp	.LBB3_30
.LBB3_19:                               # %.us-lcssa.us-lcssa
.Ltmp5:
.LBB3_30:
	movq	%rax, %rbx
.Ltmp21:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer5FlushEv
.Ltmp22:
.LBB3_31:                               # %._crit_edge.i
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB3_34
# BB#32:
	movq	(%rdi), %rax
.Ltmp26:
	callq	*16(%rax)
.Ltmp27:
# BB#33:                                # %.noexc60
	movq	$0, 56(%r15)
.LBB3_34:                               # %_ZN9NCompress8NQuantum8CDecoder14ReleaseStreamsEv.exit.i
	movq	(%r15), %rax
.Ltmp28:
	movq	%r15, %rdi
	callq	*56(%rax)
.Ltmp29:
# BB#35:                                # %_ZN9NCompress8NQuantum8CDecoder15CDecoderFlusherD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB3_36:
.Ltmp30:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN9NCompress8NQuantum8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end3-_ZN9NCompress8NQuantum8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\262\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\251\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp23-.Ltmp19         #   Call between .Ltmp19 and .Ltmp23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp21-.Ltmp24         #   Call between .Ltmp24 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp29-.Ltmp21         #   Call between .Ltmp21 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	1                       #   On action: 1
	.long	.Ltmp29-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Lfunc_end3-.Ltmp29     #   Call between .Ltmp29 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end4:
	.size	__clang_call_terminate, .Lfunc_end4-__clang_call_terminate

	.text
	.globl	_ZN9NCompress8NQuantum8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress8NQuantum8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress8NQuantum8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 16
.Lcfi41:
	.cfi_offset %rbx, -16
.Ltmp31:
	callq	_ZN9NCompress8NQuantum8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	movl	%eax, %ebx
.Ltmp32:
.LBB5_6:
	movl	%ebx, %eax
	popq	%rbx
	retq
.LBB5_1:
.Ltmp33:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	cmpl	$3, %ebx
	jne	.LBB5_3
# BB#2:
	callq	__cxa_begin_catch
	jmp	.LBB5_4
.LBB5_3:
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB5_5
.LBB5_4:
	movl	(%rax), %ebx
	callq	__cxa_end_catch
	jmp	.LBB5_6
.LBB5_5:
	callq	__cxa_end_catch
	movl	$1, %ebx
	jmp	.LBB5_6
.Lfunc_end5:
	.size	_ZN9NCompress8NQuantum8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end5-_ZN9NCompress8NQuantum8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\256\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp31-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin1   #     jumps to .Ltmp33
	.byte	5                       #   On action: 3
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end5-.Ltmp32     #   Call between .Ltmp32 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
	.byte	3                       # >> Action Record 3 <<
                                        #   Catch TypeInfo 3
	.byte	125                     #   Continue to action 2
                                        # >> Catch TypeInfos <<
	.long	_ZTI18CInBufferException # TypeInfo 3
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress8NQuantum8CDecoder11SetInStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum8CDecoder11SetInStreamEP19ISequentialInStream,@function
_ZN9NCompress8NQuantum8CDecoder11SetInStreamEP19ISequentialInStream: # @_ZN9NCompress8NQuantum8CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 16
	addq	$112, %rdi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end6:
	.size	_ZN9NCompress8NQuantum8CDecoder11SetInStreamEP19ISequentialInStream, .Lfunc_end6-_ZN9NCompress8NQuantum8CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZThn8_N9NCompress8NQuantum8CDecoder11SetInStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NQuantum8CDecoder11SetInStreamEP19ISequentialInStream,@function
_ZThn8_N9NCompress8NQuantum8CDecoder11SetInStreamEP19ISequentialInStream: # @_ZThn8_N9NCompress8NQuantum8CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 16
	addq	$104, %rdi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end7:
	.size	_ZThn8_N9NCompress8NQuantum8CDecoder11SetInStreamEP19ISequentialInStream, .Lfunc_end7-_ZThn8_N9NCompress8NQuantum8CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN9NCompress8NQuantum8CDecoder15ReleaseInStreamEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum8CDecoder15ReleaseInStreamEv,@function
_ZN9NCompress8NQuantum8CDecoder15ReleaseInStreamEv: # @_ZN9NCompress8NQuantum8CDecoder15ReleaseInStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 16
.Lcfi45:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 136(%rbx)
.LBB8_2:                                # %_ZN9NCompress8NQuantum11NRangeCoder8CDecoder13ReleaseStreamEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end8:
	.size	_ZN9NCompress8NQuantum8CDecoder15ReleaseInStreamEv, .Lfunc_end8-_ZN9NCompress8NQuantum8CDecoder15ReleaseInStreamEv
	.cfi_endproc

	.globl	_ZThn8_N9NCompress8NQuantum8CDecoder15ReleaseInStreamEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NQuantum8CDecoder15ReleaseInStreamEv,@function
_ZThn8_N9NCompress8NQuantum8CDecoder15ReleaseInStreamEv: # @_ZThn8_N9NCompress8NQuantum8CDecoder15ReleaseInStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 16
.Lcfi47:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 128(%rbx)
.LBB9_2:                                # %_ZN9NCompress8NQuantum8CDecoder15ReleaseInStreamEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	_ZThn8_N9NCompress8NQuantum8CDecoder15ReleaseInStreamEv, .Lfunc_end9-_ZThn8_N9NCompress8NQuantum8CDecoder15ReleaseInStreamEv
	.cfi_endproc

	.globl	_ZN9NCompress8NQuantum8CDecoder16SetOutStreamSizeEPKy
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum8CDecoder16SetOutStreamSizeEPKy,@function
_ZN9NCompress8NQuantum8CDecoder16SetOutStreamSizeEPKy: # @_ZN9NCompress8NQuantum8CDecoder16SetOutStreamSizeEPKy
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 16
	testq	%rsi, %rsi
	je	.LBB10_1
# BB#2:
	movl	$-2, 168(%rdi)
	movzbl	180(%rdi), %esi
	addq	$32, %rdi
	callq	_ZN12CLzOutWindow4InitEb
	xorl	%eax, %eax
	popq	%rcx
	retq
.LBB10_1:
	movl	$-2147467259, %eax      # imm = 0x80004005
	popq	%rcx
	retq
.Lfunc_end10:
	.size	_ZN9NCompress8NQuantum8CDecoder16SetOutStreamSizeEPKy, .Lfunc_end10-_ZN9NCompress8NQuantum8CDecoder16SetOutStreamSizeEPKy
	.cfi_endproc

	.globl	_ZThn16_N9NCompress8NQuantum8CDecoder16SetOutStreamSizeEPKy
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NQuantum8CDecoder16SetOutStreamSizeEPKy,@function
_ZThn16_N9NCompress8NQuantum8CDecoder16SetOutStreamSizeEPKy: # @_ZThn16_N9NCompress8NQuantum8CDecoder16SetOutStreamSizeEPKy
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 16
	testq	%rsi, %rsi
	je	.LBB11_1
# BB#2:
	movl	$-2, 152(%rdi)
	movzbl	164(%rdi), %esi
	addq	$16, %rdi
	callq	_ZN12CLzOutWindow4InitEb
	xorl	%eax, %eax
	popq	%rcx
	retq
.LBB11_1:
	movl	$-2147467259, %eax      # imm = 0x80004005
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZThn16_N9NCompress8NQuantum8CDecoder16SetOutStreamSizeEPKy, .Lfunc_end11-_ZThn16_N9NCompress8NQuantum8CDecoder16SetOutStreamSizeEPKy
	.cfi_endproc

	.section	.text._ZN9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB12_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB12_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB12_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB12_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB12_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB12_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB12_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB12_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB12_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB12_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB12_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB12_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB12_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB12_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB12_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB12_16
.LBB12_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICompressSetInStream(%rip), %cl
	jne	.LBB12_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetInStream+1(%rip), %al
	jne	.LBB12_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetInStream+2(%rip), %al
	jne	.LBB12_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetInStream+3(%rip), %al
	jne	.LBB12_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetInStream+4(%rip), %al
	jne	.LBB12_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetInStream+5(%rip), %al
	jne	.LBB12_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetInStream+6(%rip), %al
	jne	.LBB12_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetInStream+7(%rip), %al
	jne	.LBB12_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetInStream+8(%rip), %al
	jne	.LBB12_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetInStream+9(%rip), %al
	jne	.LBB12_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetInStream+10(%rip), %al
	jne	.LBB12_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetInStream+11(%rip), %al
	jne	.LBB12_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetInStream+12(%rip), %al
	jne	.LBB12_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetInStream+13(%rip), %al
	jne	.LBB12_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetInStream+14(%rip), %al
	jne	.LBB12_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit8
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetInStream+15(%rip), %al
	jne	.LBB12_33
.LBB12_16:
	leaq	8(%rdi), %rax
	jmp	.LBB12_50
.LBB12_33:                              # %_ZeqRK4GUIDS1_.exit8.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressSetOutStreamSize(%rip), %cl
	jne	.LBB12_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+1(%rip), %cl
	jne	.LBB12_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+2(%rip), %cl
	jne	.LBB12_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+3(%rip), %cl
	jne	.LBB12_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+4(%rip), %cl
	jne	.LBB12_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+5(%rip), %cl
	jne	.LBB12_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+6(%rip), %cl
	jne	.LBB12_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+7(%rip), %cl
	jne	.LBB12_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+8(%rip), %cl
	jne	.LBB12_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+9(%rip), %cl
	jne	.LBB12_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+10(%rip), %cl
	jne	.LBB12_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+11(%rip), %cl
	jne	.LBB12_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+12(%rip), %cl
	jne	.LBB12_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+13(%rip), %cl
	jne	.LBB12_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+14(%rip), %cl
	jne	.LBB12_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+15(%rip), %cl
	jne	.LBB12_51
# BB#49:
	leaq	16(%rdi), %rax
.LBB12_50:                              # %_ZeqRK4GUIDS1_.exit10.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB12_51:                              # %_ZeqRK4GUIDS1_.exit10.thread
	popq	%rcx
	retq
.Lfunc_end12:
	.size	_ZN9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end12-_ZN9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress8NQuantum8CDecoder6AddRefEv,"axG",@progbits,_ZN9NCompress8NQuantum8CDecoder6AddRefEv,comdat
	.weak	_ZN9NCompress8NQuantum8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum8CDecoder6AddRefEv,@function
_ZN9NCompress8NQuantum8CDecoder6AddRefEv: # @_ZN9NCompress8NQuantum8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end13:
	.size	_ZN9NCompress8NQuantum8CDecoder6AddRefEv, .Lfunc_end13-_ZN9NCompress8NQuantum8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NQuantum8CDecoder7ReleaseEv,"axG",@progbits,_ZN9NCompress8NQuantum8CDecoder7ReleaseEv,comdat
	.weak	_ZN9NCompress8NQuantum8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum8CDecoder7ReleaseEv,@function
_ZN9NCompress8NQuantum8CDecoder7ReleaseEv: # @_ZN9NCompress8NQuantum8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB14_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB14_2:
	popq	%rcx
	retq
.Lfunc_end14:
	.size	_ZN9NCompress8NQuantum8CDecoder7ReleaseEv, .Lfunc_end14-_ZN9NCompress8NQuantum8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NQuantum8CDecoderD2Ev,"axG",@progbits,_ZN9NCompress8NQuantum8CDecoderD2Ev,comdat
	.weak	_ZN9NCompress8NQuantum8CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum8CDecoderD2Ev,@function
_ZN9NCompress8NQuantum8CDecoderD2Ev:    # @_ZN9NCompress8NQuantum8CDecoderD2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 32
.Lcfi55:
	.cfi_offset %rbx, -24
.Lcfi56:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress8NQuantum8CDecoderE+104, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NQuantum8CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN9NCompress8NQuantum8CDecoderE+176, 16(%rbx)
	leaq	112(%rbx), %rdi
.Ltmp34:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp35:
# BB#1:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp40:
	callq	*16(%rax)
.Ltmp41:
.LBB15_3:                               # %_ZN9NCompress8NQuantum11NRangeCoder8CDecoderD2Ev.exit
	leaq	32(%rbx), %rdi
.Ltmp52:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp53:
# BB#4:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp58:
	callq	*16(%rax)
.Ltmp59:
.LBB15_6:                               # %_ZN10COutBufferD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB15_20:
.Ltmp60:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB15_13:
.Ltmp42:
	movq	%rax, %r14
	jmp	.LBB15_14
.LBB15_10:
.Ltmp54:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_21
# BB#11:
	movq	(%rdi), %rax
.Ltmp55:
	callq	*16(%rax)
.Ltmp56:
	jmp	.LBB15_21
.LBB15_12:
.Ltmp57:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_7:
.Ltmp36:
	movq	%rax, %r14
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_14
# BB#8:
	movq	(%rdi), %rax
.Ltmp37:
	callq	*16(%rax)
.Ltmp38:
.LBB15_14:                              # %.body
	leaq	32(%rbx), %rdi
.Ltmp43:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp44:
# BB#15:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_21
# BB#16:
	movq	(%rdi), %rax
.Ltmp49:
	callq	*16(%rax)
.Ltmp50:
.LBB15_21:                              # %_ZN10COutBufferD2Ev.exit12
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB15_9:
.Ltmp39:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_22:
.Ltmp51:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB15_17:
.Ltmp45:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_23
# BB#18:
	movq	(%rdi), %rax
.Ltmp46:
	callq	*16(%rax)
.Ltmp47:
.LBB15_23:                              # %.body10
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB15_19:
.Ltmp48:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN9NCompress8NQuantum8CDecoderD2Ev, .Lfunc_end15-_ZN9NCompress8NQuantum8CDecoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp34-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin2   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin2   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin2   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp55-.Ltmp59         #   Call between .Ltmp59 and .Ltmp55
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin2   #     jumps to .Ltmp57
	.byte	1                       #   On action: 1
	.long	.Ltmp37-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin2   #     jumps to .Ltmp39
	.byte	1                       #   On action: 1
	.long	.Ltmp43-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin2   #     jumps to .Ltmp45
	.byte	1                       #   On action: 1
	.long	.Ltmp49-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin2   #     jumps to .Ltmp51
	.byte	1                       #   On action: 1
	.long	.Ltmp50-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp46-.Ltmp50         #   Call between .Ltmp50 and .Ltmp46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin2   # >> Call Site 11 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin2   #     jumps to .Ltmp48
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress8NQuantum8CDecoderD0Ev,"axG",@progbits,_ZN9NCompress8NQuantum8CDecoderD0Ev,comdat
	.weak	_ZN9NCompress8NQuantum8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum8CDecoderD0Ev,@function
_ZN9NCompress8NQuantum8CDecoderD0Ev:    # @_ZN9NCompress8NQuantum8CDecoderD0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi59:
	.cfi_def_cfa_offset 32
.Lcfi60:
	.cfi_offset %rbx, -24
.Lcfi61:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp61:
	callq	_ZN9NCompress8NQuantum8CDecoderD2Ev
.Ltmp62:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB16_2:
.Ltmp63:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end16:
	.size	_ZN9NCompress8NQuantum8CDecoderD0Ev, .Lfunc_end16-_ZN9NCompress8NQuantum8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp61-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin3   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end16-.Ltmp62    #   Call between .Ltmp62 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end17:
	.size	_ZThn8_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end17-_ZThn8_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NQuantum8CDecoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress8NQuantum8CDecoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress8NQuantum8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NQuantum8CDecoder6AddRefEv,@function
_ZThn8_N9NCompress8NQuantum8CDecoder6AddRefEv: # @_ZThn8_N9NCompress8NQuantum8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end18:
	.size	_ZThn8_N9NCompress8NQuantum8CDecoder6AddRefEv, .Lfunc_end18-_ZThn8_N9NCompress8NQuantum8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NQuantum8CDecoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress8NQuantum8CDecoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress8NQuantum8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NQuantum8CDecoder7ReleaseEv,@function
_ZThn8_N9NCompress8NQuantum8CDecoder7ReleaseEv: # @_ZThn8_N9NCompress8NQuantum8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB19_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB19_2:                               # %_ZN9NCompress8NQuantum8CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end19:
	.size	_ZThn8_N9NCompress8NQuantum8CDecoder7ReleaseEv, .Lfunc_end19-_ZThn8_N9NCompress8NQuantum8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NQuantum8CDecoderD1Ev,"axG",@progbits,_ZThn8_N9NCompress8NQuantum8CDecoderD1Ev,comdat
	.weak	_ZThn8_N9NCompress8NQuantum8CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NQuantum8CDecoderD1Ev,@function
_ZThn8_N9NCompress8NQuantum8CDecoderD1Ev: # @_ZThn8_N9NCompress8NQuantum8CDecoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress8NQuantum8CDecoderD2Ev # TAILCALL
.Lfunc_end20:
	.size	_ZThn8_N9NCompress8NQuantum8CDecoderD1Ev, .Lfunc_end20-_ZThn8_N9NCompress8NQuantum8CDecoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NQuantum8CDecoderD0Ev,"axG",@progbits,_ZThn8_N9NCompress8NQuantum8CDecoderD0Ev,comdat
	.weak	_ZThn8_N9NCompress8NQuantum8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NQuantum8CDecoderD0Ev,@function
_ZThn8_N9NCompress8NQuantum8CDecoderD0Ev: # @_ZThn8_N9NCompress8NQuantum8CDecoderD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 32
.Lcfi66:
	.cfi_offset %rbx, -24
.Lcfi67:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp64:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NQuantum8CDecoderD2Ev
.Ltmp65:
# BB#1:                                 # %_ZN9NCompress8NQuantum8CDecoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB21_2:
.Ltmp66:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZThn8_N9NCompress8NQuantum8CDecoderD0Ev, .Lfunc_end21-_ZThn8_N9NCompress8NQuantum8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp64-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin4   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end21-.Ltmp65    #   Call between .Ltmp65 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn16_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end22:
	.size	_ZThn16_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end22-_ZThn16_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NQuantum8CDecoder6AddRefEv,"axG",@progbits,_ZThn16_N9NCompress8NQuantum8CDecoder6AddRefEv,comdat
	.weak	_ZThn16_N9NCompress8NQuantum8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NQuantum8CDecoder6AddRefEv,@function
_ZThn16_N9NCompress8NQuantum8CDecoder6AddRefEv: # @_ZThn16_N9NCompress8NQuantum8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end23:
	.size	_ZThn16_N9NCompress8NQuantum8CDecoder6AddRefEv, .Lfunc_end23-_ZThn16_N9NCompress8NQuantum8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NQuantum8CDecoder7ReleaseEv,"axG",@progbits,_ZThn16_N9NCompress8NQuantum8CDecoder7ReleaseEv,comdat
	.weak	_ZThn16_N9NCompress8NQuantum8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NQuantum8CDecoder7ReleaseEv,@function
_ZThn16_N9NCompress8NQuantum8CDecoder7ReleaseEv: # @_ZThn16_N9NCompress8NQuantum8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB24_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB24_2:                               # %_ZN9NCompress8NQuantum8CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZThn16_N9NCompress8NQuantum8CDecoder7ReleaseEv, .Lfunc_end24-_ZThn16_N9NCompress8NQuantum8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NQuantum8CDecoderD1Ev,"axG",@progbits,_ZThn16_N9NCompress8NQuantum8CDecoderD1Ev,comdat
	.weak	_ZThn16_N9NCompress8NQuantum8CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NQuantum8CDecoderD1Ev,@function
_ZThn16_N9NCompress8NQuantum8CDecoderD1Ev: # @_ZThn16_N9NCompress8NQuantum8CDecoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress8NQuantum8CDecoderD2Ev # TAILCALL
.Lfunc_end25:
	.size	_ZThn16_N9NCompress8NQuantum8CDecoderD1Ev, .Lfunc_end25-_ZThn16_N9NCompress8NQuantum8CDecoderD1Ev
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NQuantum8CDecoderD0Ev,"axG",@progbits,_ZThn16_N9NCompress8NQuantum8CDecoderD0Ev,comdat
	.weak	_ZThn16_N9NCompress8NQuantum8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NQuantum8CDecoderD0Ev,@function
_ZThn16_N9NCompress8NQuantum8CDecoderD0Ev: # @_ZThn16_N9NCompress8NQuantum8CDecoderD0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi71:
	.cfi_def_cfa_offset 32
.Lcfi72:
	.cfi_offset %rbx, -24
.Lcfi73:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp67:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NQuantum8CDecoderD2Ev
.Ltmp68:
# BB#1:                                 # %_ZN9NCompress8NQuantum8CDecoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB26_2:
.Ltmp69:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end26:
	.size	_ZThn16_N9NCompress8NQuantum8CDecoderD0Ev, .Lfunc_end26-_ZThn16_N9NCompress8NQuantum8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp67-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin5   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end26-.Ltmp68    #   Call between .Ltmp68 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN9NCompress8NQuantum11NRangeCoder8CDecoder6DecodeEjjj,"axG",@progbits,_ZN9NCompress8NQuantum11NRangeCoder8CDecoder6DecodeEjjj,comdat
	.weak	_ZN9NCompress8NQuantum11NRangeCoder8CDecoder6DecodeEjjj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum11NRangeCoder8CDecoder6DecodeEjjj,@function
_ZN9NCompress8NQuantum11NRangeCoder8CDecoder6DecodeEjjj: # @_ZN9NCompress8NQuantum11NRangeCoder8CDecoder6DecodeEjjj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 48
.Lcfi79:
	.cfi_offset %rbx, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movl	%edx, %eax
	movq	%rdi, %r15
	movl	(%r15), %edi
	movl	4(%r15), %r8d
	imull	%r8d, %eax
	xorl	%edx, %edx
	divl	%ecx
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-1(%rdi,%rax), %ebx
	imull	%esi, %r8d
	xorl	%edx, %edx
	movl	%r8d, %eax
	divl	%ecx
	movl	8(%r15), %ebp
	subl	%eax, %ebp
	movl	%ebp, 8(%r15)
	addl	%eax, %edi
	movl	%edi, (%r15)
	leaq	24(%r15), %r14
	jmp	.LBB27_1
	.p2align	4, 0x90
.LBB27_10:                              # %_ZN9NCompress8NQuantum17CStreamBitDecoder7ReadBitEv.exit
                                        #   in Loop: Header=BB27_1 Depth=1
	leal	1(%rbx,%rbx), %ebx
	movl	%eax, %ecx
	shrl	$7, %ecx
	andl	$1, %ecx
	addl	%eax, %eax
	movl	%eax, 16(%r15)
	orl	%ecx, %ebp
	movl	%ebp, 8(%r15)
.LBB27_1:                               # =>This Inner Loop Header: Depth=1
	movl	%edi, %eax
	xorw	%bx, %ax
	jns	.LBB27_5
# BB#2:                                 #   in Loop: Header=BB27_1 Depth=1
	testb	$64, %bh
	jne	.LBB27_11
# BB#3:                                 #   in Loop: Header=BB27_1 Depth=1
	movl	%edi, %eax
	andl	$16384, %eax            # imm = 0x4000
	je	.LBB27_11
# BB#4:                                 #   in Loop: Header=BB27_1 Depth=1
	andl	$16383, %edi            # imm = 0x3FFF
	movl	%edi, (%r15)
	orl	$16384, %ebx            # imm = 0x4000
.LBB27_5:                               #   in Loop: Header=BB27_1 Depth=1
	addl	%edi, %edi
	movzwl	%di, %edi
	movl	%edi, (%r15)
	andl	$32767, %ebx            # imm = 0x7FFF
	addl	%ebp, %ebp
	movl	16(%r15), %eax
	cmpl	$65536, %eax            # imm = 0x10000
	jb	.LBB27_10
# BB#6:                                 #   in Loop: Header=BB27_1 Depth=1
	movq	24(%r15), %rax
	cmpq	32(%r15), %rax
	jae	.LBB27_7
# BB#8:                                 #   in Loop: Header=BB27_1 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB27_9
	.p2align	4, 0x90
.LBB27_7:                               #   in Loop: Header=BB27_1 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	(%r15), %edi
.LBB27_9:                               # %_ZN9CInBuffer8ReadByteEv.exit.i
                                        #   in Loop: Header=BB27_1 Depth=1
	movzbl	%al, %eax
	orl	$256, %eax              # imm = 0x100
	movl	%eax, 16(%r15)
	jmp	.LBB27_10
.LBB27_11:
	incl	%ebx
	subl	%edi, %ebx
	movl	%ebx, 4(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	_ZN9NCompress8NQuantum11NRangeCoder8CDecoder6DecodeEjjj, .Lfunc_end27-_ZN9NCompress8NQuantum11NRangeCoder8CDecoder6DecodeEjjj
	.cfi_endproc

	.type	_ZTS18CInBufferException,@object # @_ZTS18CInBufferException
	.section	.rodata._ZTS18CInBufferException,"aG",@progbits,_ZTS18CInBufferException,comdat
	.weak	_ZTS18CInBufferException
	.p2align	4
_ZTS18CInBufferException:
	.asciz	"18CInBufferException"
	.size	_ZTS18CInBufferException, 21

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	_ZTI18CInBufferException,@object # @_ZTI18CInBufferException
	.section	.rodata._ZTI18CInBufferException,"aG",@progbits,_ZTI18CInBufferException,comdat
	.weak	_ZTI18CInBufferException
	.p2align	4
_ZTI18CInBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18CInBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI18CInBufferException, 24

	.type	_ZTS19COutBufferException,@object # @_ZTS19COutBufferException
	.section	.rodata._ZTS19COutBufferException,"aG",@progbits,_ZTS19COutBufferException,comdat
	.weak	_ZTS19COutBufferException
	.p2align	4
_ZTS19COutBufferException:
	.asciz	"19COutBufferException"
	.size	_ZTS19COutBufferException, 22

	.type	_ZTI19COutBufferException,@object # @_ZTI19COutBufferException
	.section	.rodata._ZTI19COutBufferException,"aG",@progbits,_ZTI19COutBufferException,comdat
	.weak	_ZTI19COutBufferException
	.p2align	4
_ZTI19COutBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19COutBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI19COutBufferException, 24

	.type	_ZTVN9NCompress8NQuantum8CDecoderE,@object # @_ZTVN9NCompress8NQuantum8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress8NQuantum8CDecoderE
	.p2align	3
_ZTVN9NCompress8NQuantum8CDecoderE:
	.quad	0
	.quad	_ZTIN9NCompress8NQuantum8CDecoderE
	.quad	_ZN9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress8NQuantum8CDecoder6AddRefEv
	.quad	_ZN9NCompress8NQuantum8CDecoder7ReleaseEv
	.quad	_ZN9NCompress8NQuantum8CDecoderD2Ev
	.quad	_ZN9NCompress8NQuantum8CDecoderD0Ev
	.quad	_ZN9NCompress8NQuantum8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.quad	_ZN9NCompress8NQuantum8CDecoder11SetInStreamEP19ISequentialInStream
	.quad	_ZN9NCompress8NQuantum8CDecoder15ReleaseInStreamEv
	.quad	_ZN9NCompress8NQuantum8CDecoder16SetOutStreamSizeEPKy
	.quad	-8
	.quad	_ZTIN9NCompress8NQuantum8CDecoderE
	.quad	_ZThn8_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress8NQuantum8CDecoder6AddRefEv
	.quad	_ZThn8_N9NCompress8NQuantum8CDecoder7ReleaseEv
	.quad	_ZThn8_N9NCompress8NQuantum8CDecoderD1Ev
	.quad	_ZThn8_N9NCompress8NQuantum8CDecoderD0Ev
	.quad	_ZThn8_N9NCompress8NQuantum8CDecoder11SetInStreamEP19ISequentialInStream
	.quad	_ZThn8_N9NCompress8NQuantum8CDecoder15ReleaseInStreamEv
	.quad	-16
	.quad	_ZTIN9NCompress8NQuantum8CDecoderE
	.quad	_ZThn16_N9NCompress8NQuantum8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N9NCompress8NQuantum8CDecoder6AddRefEv
	.quad	_ZThn16_N9NCompress8NQuantum8CDecoder7ReleaseEv
	.quad	_ZThn16_N9NCompress8NQuantum8CDecoderD1Ev
	.quad	_ZThn16_N9NCompress8NQuantum8CDecoderD0Ev
	.quad	_ZThn16_N9NCompress8NQuantum8CDecoder16SetOutStreamSizeEPKy
	.size	_ZTVN9NCompress8NQuantum8CDecoderE, 224

	.type	_ZTSN9NCompress8NQuantum8CDecoderE,@object # @_ZTSN9NCompress8NQuantum8CDecoderE
	.globl	_ZTSN9NCompress8NQuantum8CDecoderE
	.p2align	4
_ZTSN9NCompress8NQuantum8CDecoderE:
	.asciz	"N9NCompress8NQuantum8CDecoderE"
	.size	_ZTSN9NCompress8NQuantum8CDecoderE, 31

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS20ICompressSetInStream,@object # @_ZTS20ICompressSetInStream
	.section	.rodata._ZTS20ICompressSetInStream,"aG",@progbits,_ZTS20ICompressSetInStream,comdat
	.weak	_ZTS20ICompressSetInStream
	.p2align	4
_ZTS20ICompressSetInStream:
	.asciz	"20ICompressSetInStream"
	.size	_ZTS20ICompressSetInStream, 23

	.type	_ZTI20ICompressSetInStream,@object # @_ZTI20ICompressSetInStream
	.section	.rodata._ZTI20ICompressSetInStream,"aG",@progbits,_ZTI20ICompressSetInStream,comdat
	.weak	_ZTI20ICompressSetInStream
	.p2align	4
_ZTI20ICompressSetInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ICompressSetInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ICompressSetInStream, 24

	.type	_ZTS25ICompressSetOutStreamSize,@object # @_ZTS25ICompressSetOutStreamSize
	.section	.rodata._ZTS25ICompressSetOutStreamSize,"aG",@progbits,_ZTS25ICompressSetOutStreamSize,comdat
	.weak	_ZTS25ICompressSetOutStreamSize
	.p2align	4
_ZTS25ICompressSetOutStreamSize:
	.asciz	"25ICompressSetOutStreamSize"
	.size	_ZTS25ICompressSetOutStreamSize, 28

	.type	_ZTI25ICompressSetOutStreamSize,@object # @_ZTI25ICompressSetOutStreamSize
	.section	.rodata._ZTI25ICompressSetOutStreamSize,"aG",@progbits,_ZTI25ICompressSetOutStreamSize,comdat
	.weak	_ZTI25ICompressSetOutStreamSize
	.p2align	4
_ZTI25ICompressSetOutStreamSize:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS25ICompressSetOutStreamSize
	.quad	_ZTI8IUnknown
	.size	_ZTI25ICompressSetOutStreamSize, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress8NQuantum8CDecoderE,@object # @_ZTIN9NCompress8NQuantum8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress8NQuantum8CDecoderE
	.p2align	4
_ZTIN9NCompress8NQuantum8CDecoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress8NQuantum8CDecoderE
	.long	1                       # 0x1
	.long	4                       # 0x4
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI20ICompressSetInStream
	.quad	2050                    # 0x802
	.quad	_ZTI25ICompressSetOutStreamSize
	.quad	4098                    # 0x1002
	.quad	_ZTI13CMyUnknownImp
	.quad	6146                    # 0x1802
	.size	_ZTIN9NCompress8NQuantum8CDecoderE, 88


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
