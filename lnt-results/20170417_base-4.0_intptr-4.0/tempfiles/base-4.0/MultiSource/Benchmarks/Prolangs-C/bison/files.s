	.text
	.file	"files.bc"
	.globl	stringappend
	.p2align	4, 0x90
	.type	stringappend,@function
stringappend:                           # @stringappend
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %r14d
	movq	%rdi, %r15
	cmpb	$0, (%rbx)
	je	.LBB0_3
# BB#1:                                 # %.lr.ph30.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph30
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 1(%rbx,%rax)
	leaq	1(%rax), %rax
	jne	.LBB0_2
	jmp	.LBB0_4
.LBB0_3:
	xorl	%eax, %eax
.LBB0_4:                                # %._crit_edge
	leal	1(%r14,%rax), %edi
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	callq	mallocate
	testl	%r14d, %r14d
	movq	%rax, %rcx
	jle	.LBB0_28
# BB#5:                                 # %.lr.ph.preheader
	leal	-1(%r14), %edx
	leaq	1(%rdx), %r8
	cmpq	$32, %r8
	jae	.LBB0_7
# BB#6:
	movq	%rax, %rdi
	jmp	.LBB0_21
.LBB0_7:                                # %min.iters.checked
	movabsq	$8589934560, %rsi       # imm = 0x1FFFFFFE0
	andq	%r8, %rsi
	je	.LBB0_11
# BB#8:                                 # %vector.memcheck
	leaq	1(%r15,%rdx), %rcx
	cmpq	%rcx, %rax
	jae	.LBB0_12
# BB#9:                                 # %vector.memcheck
	leaq	1(%rax,%rdx), %rcx
	cmpq	%r15, %rcx
	jbe	.LBB0_12
.LBB0_11:
	movq	%rax, %rdi
.LBB0_21:                               # %.lr.ph.preheader44
	movl	%r14d, %esi
	subl	%ebp, %esi
	subl	%ebp, %edx
	andl	$7, %esi
	je	.LBB0_24
# BB#22:                                # %.lr.ph.prol.preheader
	negl	%esi
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r15), %ecx
	incq	%r15
	movb	%cl, (%rdi)
	incq	%rdi
	incl	%ebp
	incl	%esi
	jne	.LBB0_23
.LBB0_24:                               # %.lr.ph.prol.loopexit
	cmpl	$7, %edx
	jb	.LBB0_27
# BB#25:                                # %.lr.ph.preheader44.new
	subl	%ebp, %r14d
	.p2align	4, 0x90
.LBB0_26:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r15), %ecx
	movb	%cl, (%rdi)
	movzbl	1(%r15), %ecx
	movb	%cl, 1(%rdi)
	movzbl	2(%r15), %ecx
	movb	%cl, 2(%rdi)
	movzbl	3(%r15), %ecx
	movb	%cl, 3(%rdi)
	movzbl	4(%r15), %ecx
	movb	%cl, 4(%rdi)
	movzbl	5(%r15), %ecx
	movb	%cl, 5(%rdi)
	movzbl	6(%r15), %ecx
	movb	%cl, 6(%rdi)
	movzbl	7(%r15), %ecx
	movb	%cl, 7(%rdi)
	addq	$8, %r15
	addq	$8, %rdi
	addl	$-8, %r14d
	jne	.LBB0_26
.LBB0_27:                               # %.preheader.preheader.loopexit
	movq	%rax, %rcx
	addq	%r8, %rcx
	.p2align	4, 0x90
.LBB0_28:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %edx
	incq	%rbx
	movb	%dl, (%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB0_28
# BB#29:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_12:                               # %vector.body.preheader
	leaq	-32(%rsi), %rbp
	movl	%ebp, %edi
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB0_15
# BB#13:                                # %vector.body.prol.preheader
	negq	%rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_14:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r15,%rcx), %xmm0
	movups	16(%r15,%rcx), %xmm1
	movups	%xmm0, (%rax,%rcx)
	movups	%xmm1, 16(%rax,%rcx)
	addq	$32, %rcx
	incq	%rdi
	jne	.LBB0_14
	jmp	.LBB0_16
.LBB0_15:
	xorl	%ecx, %ecx
.LBB0_16:                               # %vector.body.prol.loopexit
	cmpq	$96, %rbp
	jb	.LBB0_19
# BB#17:                                # %vector.body.preheader.new
	movq	%rsi, %rdi
	subq	%rcx, %rdi
	leaq	112(%rax,%rcx), %rbp
	leaq	112(%r15,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_18:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rbp
	subq	$-128, %rcx
	addq	$-128, %rdi
	jne	.LBB0_18
.LBB0_19:                               # %middle.block
	cmpq	%rsi, %r8
	je	.LBB0_27
# BB#20:
	addq	%rsi, %r15
	movq	%rsi, %rdi
	addq	%rax, %rdi
	movl	%esi, %ebp
	jmp	.LBB0_21
.Lfunc_end0:
	.size	stringappend, .Lfunc_end0-stringappend
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.byte	47                      # 0x2f
	.byte	116                     # 0x74
	.byte	109                     # 0x6d
	.byte	112                     # 0x70
	.byte	47                      # 0x2f
	.byte	98                      # 0x62
	.byte	46                      # 0x2e
	.byte	97                      # 0x61
	.byte	99                      # 0x63
	.byte	116                     # 0x74
	.byte	46                      # 0x2e
	.byte	88                      # 0x58
	.byte	88                      # 0x58
	.byte	88                      # 0x58
	.byte	88                      # 0x58
	.byte	88                      # 0x58
.LCPI1_1:
	.byte	47                      # 0x2f
	.byte	116                     # 0x74
	.byte	109                     # 0x6d
	.byte	112                     # 0x70
	.byte	47                      # 0x2f
	.byte	98                      # 0x62
	.byte	46                      # 0x2e
	.byte	97                      # 0x61
	.byte	116                     # 0x74
	.byte	116                     # 0x74
	.byte	114                     # 0x72
	.byte	115                     # 0x73
	.byte	46                      # 0x2e
	.byte	88                      # 0x58
	.byte	88                      # 0x58
	.byte	88                      # 0x58
.LCPI1_2:
	.byte	47                      # 0x2f
	.byte	116                     # 0x74
	.byte	109                     # 0x6d
	.byte	112                     # 0x70
	.byte	47                      # 0x2f
	.byte	98                      # 0x62
	.byte	46                      # 0x2e
	.byte	116                     # 0x74
	.byte	97                      # 0x61
	.byte	98                      # 0x62
	.byte	46                      # 0x2e
	.byte	88                      # 0x58
	.byte	88                      # 0x58
	.byte	88                      # 0x58
	.byte	88                      # 0x58
	.byte	88                      # 0x58
	.text
	.globl	openfiles
	.p2align	4, 0x90
	.type	openfiles,@function
openfiles:                              # @openfiles
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 64
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movabsq	$8589934560, %r15       # imm = 0x1FFFFFFE0
	movq	spec_outfile(%rip), %r14
	testq	%r14, %r14
	je	.LBB1_6
# BB#1:
	movq	%r14, %rdi
	callq	strlen
	movslq	%eax, %rdx
	movzbl	-2(%r14,%rdx), %esi
	movl	$46, %ecx
	subl	%esi, %ecx
	jne	.LBB1_4
# BB#2:
	movzbl	-1(%r14,%rdx), %esi
	movl	$99, %ecx
	subl	%esi, %ecx
	jne	.LBB1_4
# BB#3:
	movzbl	(%r14,%rdx), %ecx
	negl	%ecx
.LBB1_4:
	leal	-2(%rax), %r13d
	testl	%ecx, %ecx
	cmovnel	%eax, %r13d
	movslq	%r13d, %rax
	leaq	-4(%r14,%rax), %rbx
	movl	$.L.str.2, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_19
# BB#5:
	movl	$.L.str.3, %esi
	movq	%rbx, %rdi
	callq	strcmp
	leal	-4(%r13), %r12d
	testl	%eax, %eax
	cmovnel	%r13d, %r12d
	jmp	.LBB1_42
.LBB1_6:
	cmpl	$0, fixed_outfiles(%rip)
	movl	$.L.str.4, %ebx
	cmoveq	infile(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_9
# BB#7:
	movl	$1, %edi
	callq	exit
	.p2align	4, 0x90
.LBB1_8:                                #   in Loop: Header=BB1_9 Depth=1
	movq	%rax, %rbx
.LBB1_9:                                # %.preheader.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_11 Depth 2
	leaq	1(%rbx), %rax
	jmp	.LBB1_11
	.p2align	4, 0x90
.LBB1_10:                               # %.preheader._crit_edge
                                        #   in Loop: Header=BB1_11 Depth=2
	incq	%rax
.LBB1_11:                               # %.preheader
                                        #   Parent Loop BB1_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rax), %ecx
	testb	%cl, %cl
	je	.LBB1_13
# BB#12:                                # %.preheader
                                        #   in Loop: Header=BB1_11 Depth=2
	cmpb	$47, %cl
	jne	.LBB1_10
	jmp	.LBB1_8
.LBB1_13:
	movq	%rbx, %rdi
	callq	strlen
	movslq	%eax, %rdx
	movzbl	-2(%rbx,%rdx), %esi
	movl	$46, %ecx
	subl	%esi, %ecx
	jne	.LBB1_16
# BB#14:
	movzbl	-1(%rbx,%rdx), %esi
	movl	$121, %ecx
	subl	%esi, %ecx
	jne	.LBB1_16
# BB#15:
	movzbl	(%rbx,%rdx), %ecx
	negl	%ecx
.LBB1_16:                               # %._crit_edge.i
	leal	-2(%rax), %r12d
	testl	%ecx, %ecx
	cmovnel	%eax, %r12d
	leal	5(%r12), %edi
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %r14
	testl	%r12d, %r12d
	jle	.LBB1_41
# BB#17:                                # %.lr.ph.preheader.i
	leal	-1(%r12), %ecx
	movl	%ecx, %r8d
	leaq	1(%r8), %rsi
	cmpq	$32, %rsi
	jae	.LBB1_20
# BB#18:
	movq	%r14, %rsi
	jmp	.LBB1_34
.LBB1_19:
	leal	-4(%r13), %r12d
	jmp	.LBB1_42
.LBB1_20:                               # %min.iters.checked
	movq	%rsi, %rdx
	andq	%r15, %rdx
	je	.LBB1_24
# BB#21:                                # %vector.memcheck
	leaq	1(%rbx,%rcx), %rax
	cmpq	%rax, %r14
	jae	.LBB1_25
# BB#22:                                # %vector.memcheck
	leaq	1(%r14,%rcx), %rax
	cmpq	%rax, %rbx
	jae	.LBB1_25
.LBB1_24:
	movq	%r14, %rsi
.LBB1_34:                               # %.lr.ph.i.preheader
	movl	%r12d, %edx
	subl	%ebp, %edx
	subl	%ebp, %ecx
	andl	$7, %edx
	je	.LBB1_37
# BB#35:                                # %.lr.ph.i.prol.preheader
	negl	%edx
	.p2align	4, 0x90
.LBB1_36:                               # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %eax
	incq	%rbx
	movb	%al, (%rsi)
	incq	%rsi
	incl	%ebp
	incl	%edx
	jne	.LBB1_36
.LBB1_37:                               # %.lr.ph.i.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB1_40
# BB#38:                                # %.lr.ph.i.preheader.new
	movl	%r12d, %ecx
	subl	%ebp, %ecx
	.p2align	4, 0x90
.LBB1_39:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %eax
	movb	%al, (%rsi)
	movzbl	1(%rbx), %eax
	movb	%al, 1(%rsi)
	movzbl	2(%rbx), %eax
	movb	%al, 2(%rsi)
	movzbl	3(%rbx), %eax
	movb	%al, 3(%rsi)
	movzbl	4(%rbx), %eax
	movb	%al, 4(%rsi)
	movzbl	5(%rbx), %eax
	movb	%al, 5(%rsi)
	movzbl	6(%rbx), %eax
	movb	%al, 6(%rsi)
	movzbl	7(%rbx), %eax
	movb	%al, 7(%rsi)
	addq	$8, %rbx
	addq	$8, %rsi
	addl	$-8, %ecx
	jne	.LBB1_39
.LBB1_40:                               # %.preheader.preheader.loopexit.i
	leaq	1(%r14,%r8), %rax
.LBB1_41:                               # %.preheader.i.preheader
	movl	$1650553902, (%rax)     # imm = 0x6261742E
	movb	$0, 4(%rax)
	leal	4(%r12), %r13d
.LBB1_42:
	movq	infile(%rip), %rbx
	movl	$.L.str.6, %esi
	movq	%rbx, %rdi
	callq	fopen
	testq	%rax, %rax
	je	.LBB1_162
# BB#43:                                # %tryopen.exit
	movq	%rax, finput(%rip)
	cmpl	$0, verboseflag(%rip)
	je	.LBB1_61
# BB#44:                                # %.lr.ph30.i186.preheader
	leal	8(%r12), %edi
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	callq	mallocate
	testl	%r12d, %r12d
	movq	%rax, %rcx
	jle	.LBB1_60
# BB#45:                                # %.lr.ph.preheader.i188
	leal	-1(%r12), %ebp
	movl	%ebp, %r8d
	leaq	1(%r8), %rsi
	cmpq	$32, %rsi
	jb	.LBB1_52
# BB#46:                                # %min.iters.checked338
	movq	%rsi, %rdx
	andq	%r15, %rdx
	je	.LBB1_52
# BB#47:                                # %vector.memcheck349
	leaq	1(%r14,%rbp), %rcx
	cmpq	%rcx, %rax
	jae	.LBB1_49
# BB#48:                                # %vector.memcheck349
	leaq	1(%rax,%rbp), %rcx
	cmpq	%rcx, %r14
	movq	%r14, %rdi
	movq	%rax, %rcx
	jb	.LBB1_53
.LBB1_49:                               # %vector.body334.preheader
	leaq	-32(%rdx), %rcx
	movl	%ecx, %edi
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_144
# BB#50:                                # %vector.body334.prol.preheader
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_51:                               # %vector.body334.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r14,%rbx), %xmm0
	movups	16(%r14,%rbx), %xmm1
	movups	%xmm0, (%rax,%rbx)
	movups	%xmm1, 16(%rax,%rbx)
	addq	$32, %rbx
	incq	%rdi
	jne	.LBB1_51
	jmp	.LBB1_145
.LBB1_52:
	movq	%r14, %rdi
	movq	%rax, %rcx
.LBB1_53:                               # %.lr.ph.i193.preheader
	movl	%r12d, %esi
	subl	%ebx, %esi
	subl	%ebx, %ebp
	andl	$7, %esi
	je	.LBB1_56
# BB#54:                                # %.lr.ph.i193.prol.preheader
	negl	%esi
	.p2align	4, 0x90
.LBB1_55:                               # %.lr.ph.i193.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %edx
	incq	%rdi
	movb	%dl, (%rcx)
	incq	%rcx
	incl	%ebx
	incl	%esi
	jne	.LBB1_55
.LBB1_56:                               # %.lr.ph.i193.prol.loopexit
	cmpl	$7, %ebp
	jb	.LBB1_59
# BB#57:                                # %.lr.ph.i193.preheader.new
	movl	%r12d, %edx
	subl	%ebx, %edx
	.p2align	4, 0x90
.LBB1_58:                               # %.lr.ph.i193
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %ebx
	movb	%bl, (%rcx)
	movzbl	1(%rdi), %ebx
	movb	%bl, 1(%rcx)
	movzbl	2(%rdi), %ebx
	movb	%bl, 2(%rcx)
	movzbl	3(%rdi), %ebx
	movb	%bl, 3(%rcx)
	movzbl	4(%rdi), %ebx
	movb	%bl, 4(%rcx)
	movzbl	5(%rdi), %ebx
	movb	%bl, 5(%rcx)
	movzbl	6(%rdi), %ebx
	movb	%bl, 6(%rcx)
	movzbl	7(%rdi), %ebx
	movb	%bl, 7(%rcx)
	addq	$8, %rdi
	addq	$8, %rcx
	addl	$-8, %edx
	jne	.LBB1_58
.LBB1_59:                               # %.preheader.preheader.loopexit.i195
	leaq	1(%rax,%r8), %rcx
.LBB1_60:                               # %.preheader.i198.preheader
	movb	$46, (%rcx)
	movb	$111, 1(%rcx)
	movb	$117, 2(%rcx)
	movb	$116, 3(%rcx)
	movb	$112, 4(%rcx)
	movb	$117, 5(%rcx)
	movb	$116, 6(%rcx)
	movb	$0, 7(%rcx)
	movq	%rax, outfile(%rip)
	movq	stdout(%rip), %rax
	movq	%rax, foutput(%rip)
.LBB1_61:
	cmpl	$0, definesflag(%rip)
	je	.LBB1_79
# BB#62:                                # %.lr.ph30.i202.preheader
	leal	3(%r13), %edi
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	callq	mallocate
	testl	%r13d, %r13d
	movq	%rax, %rcx
	jle	.LBB1_78
# BB#63:                                # %.lr.ph.preheader.i204
	leal	-1(%r13), %ebp
	movl	%ebp, %r8d
	leaq	1(%r8), %rsi
	cmpq	$32, %rsi
	jb	.LBB1_70
# BB#64:                                # %min.iters.checked376
	movq	%rsi, %rdx
	andq	%r15, %rdx
	je	.LBB1_70
# BB#65:                                # %vector.memcheck387
	leaq	1(%r14,%rbp), %rcx
	cmpq	%rcx, %rax
	jae	.LBB1_67
# BB#66:                                # %vector.memcheck387
	leaq	1(%rax,%rbp), %rcx
	cmpq	%rcx, %r14
	movq	%r14, %rdi
	movq	%rax, %rcx
	jb	.LBB1_71
.LBB1_67:                               # %vector.body372.preheader
	leaq	-32(%rdx), %rcx
	movl	%ecx, %edi
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_150
# BB#68:                                # %vector.body372.prol.preheader
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_69:                               # %vector.body372.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r14,%rbx), %xmm0
	movups	16(%r14,%rbx), %xmm1
	movups	%xmm0, (%rax,%rbx)
	movups	%xmm1, 16(%rax,%rbx)
	addq	$32, %rbx
	incq	%rdi
	jne	.LBB1_69
	jmp	.LBB1_151
.LBB1_70:
	movq	%r14, %rdi
	movq	%rax, %rcx
.LBB1_71:                               # %.lr.ph.i209.preheader
	movl	%r13d, %esi
	subl	%ebx, %esi
	subl	%ebx, %ebp
	andl	$7, %esi
	je	.LBB1_74
# BB#72:                                # %.lr.ph.i209.prol.preheader
	negl	%esi
	.p2align	4, 0x90
.LBB1_73:                               # %.lr.ph.i209.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %edx
	incq	%rdi
	movb	%dl, (%rcx)
	incq	%rcx
	incl	%ebx
	incl	%esi
	jne	.LBB1_73
.LBB1_74:                               # %.lr.ph.i209.prol.loopexit
	cmpl	$7, %ebp
	jb	.LBB1_77
# BB#75:                                # %.lr.ph.i209.preheader.new
	movl	%r13d, %edx
	subl	%ebx, %edx
	.p2align	4, 0x90
.LBB1_76:                               # %.lr.ph.i209
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %ebx
	movb	%bl, (%rcx)
	movzbl	1(%rdi), %ebx
	movb	%bl, 1(%rcx)
	movzbl	2(%rdi), %ebx
	movb	%bl, 2(%rcx)
	movzbl	3(%rdi), %ebx
	movb	%bl, 3(%rcx)
	movzbl	4(%rdi), %ebx
	movb	%bl, 4(%rcx)
	movzbl	5(%rdi), %ebx
	movb	%bl, 5(%rcx)
	movzbl	6(%rdi), %ebx
	movb	%bl, 6(%rcx)
	movzbl	7(%rdi), %ebx
	movb	%bl, 7(%rcx)
	addq	$8, %rdi
	addq	$8, %rcx
	addl	$-8, %edx
	jne	.LBB1_76
.LBB1_77:                               # %.preheader.preheader.loopexit.i211
	leaq	1(%rax,%r8), %rcx
.LBB1_78:                               # %.preheader.i214.preheader
	movb	$46, (%rcx)
	movb	$104, 1(%rcx)
	movb	$0, 2(%rcx)
	movq	%rax, defsfile(%rip)
	movq	stdout(%rip), %rax
	movq	%rax, fdefines(%rip)
.LBB1_79:                               # %.lr.ph30.i218.preheader
	movl	$18, %edi
	xorl	%eax, %eax
	callq	mallocate
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [47,116,109,112,47,98,46,97,99,116,46,88,88,88,88,88]
	movups	%xmm0, (%rax)
	movw	$88, 16(%rax)
	movq	%rax, %rdi
	callq	mktemp
	movq	%rax, actfile(%rip)
	movq	stdout(%rip), %rax
	movq	%rax, faction(%rip)
	movl	$20, %edi
	xorl	%eax, %eax
	callq	mallocate
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [47,116,109,112,47,98,46,97,116,116,114,115,46,88,88,88]
	movups	%xmm0, (%rax)
	movl	$5789784, 16(%rax)      # imm = 0x585858
	movq	%rax, %rdi
	callq	mktemp
	movq	%rax, tmpattrsfile(%rip)
	movq	stdout(%rip), %rax
	movq	%rax, fattrs(%rip)
	movl	$18, %edi
	xorl	%eax, %eax
	callq	mallocate
	movaps	.LCPI1_2(%rip), %xmm0   # xmm0 = [47,116,109,112,47,98,46,116,97,98,46,88,88,88,88,88]
	movups	%xmm0, (%rax)
	movw	$88, 16(%rax)
	movq	%rax, %rdi
	callq	mktemp
	movq	%rax, tmptabfile(%rip)
	movq	stdout(%rip), %rax
	movq	%rax, ftable(%rip)
	movq	spec_outfile(%rip), %rax
	testq	%rax, %rax
	jne	.LBB1_97
# BB#80:                                # %.lr.ph30.i266.preheader
	leal	3(%r13), %edi
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	callq	mallocate
	testl	%r13d, %r13d
	movq	%rax, %rcx
	jle	.LBB1_96
# BB#81:                                # %.lr.ph.preheader.i268
	leal	-1(%r13), %ebp
	movl	%ebp, %r8d
	leaq	1(%r8), %rsi
	cmpq	$32, %rsi
	jb	.LBB1_88
# BB#82:                                # %min.iters.checked414
	movq	%rsi, %rdx
	andq	%r15, %rdx
	je	.LBB1_88
# BB#83:                                # %vector.memcheck425
	leaq	1(%r14,%rbp), %rcx
	cmpq	%rcx, %rax
	jae	.LBB1_85
# BB#84:                                # %vector.memcheck425
	leaq	1(%rax,%rbp), %rcx
	cmpq	%rcx, %r14
	movq	%r14, %rdi
	movq	%rax, %rcx
	jb	.LBB1_89
.LBB1_85:                               # %vector.body410.preheader
	leaq	-32(%rdx), %rcx
	movl	%ecx, %edi
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_156
# BB#86:                                # %vector.body410.prol.preheader
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_87:                               # %vector.body410.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r14,%rbx), %xmm0
	movups	16(%r14,%rbx), %xmm1
	movups	%xmm0, (%rax,%rbx)
	movups	%xmm1, 16(%rax,%rbx)
	addq	$32, %rbx
	incq	%rdi
	jne	.LBB1_87
	jmp	.LBB1_157
.LBB1_88:
	movq	%r14, %rdi
	movq	%rax, %rcx
.LBB1_89:                               # %.lr.ph.i273.preheader
	movl	%r13d, %esi
	subl	%ebx, %esi
	subl	%ebx, %ebp
	andl	$7, %esi
	je	.LBB1_92
# BB#90:                                # %.lr.ph.i273.prol.preheader
	negl	%esi
	.p2align	4, 0x90
.LBB1_91:                               # %.lr.ph.i273.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %edx
	incq	%rdi
	movb	%dl, (%rcx)
	incq	%rcx
	incl	%ebx
	incl	%esi
	jne	.LBB1_91
.LBB1_92:                               # %.lr.ph.i273.prol.loopexit
	cmpl	$7, %ebp
	jb	.LBB1_95
# BB#93:                                # %.lr.ph.i273.preheader.new
	subl	%ebx, %r13d
	.p2align	4, 0x90
.LBB1_94:                               # %.lr.ph.i273
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %edx
	movb	%dl, (%rcx)
	movzbl	1(%rdi), %edx
	movb	%dl, 1(%rcx)
	movzbl	2(%rdi), %edx
	movb	%dl, 2(%rcx)
	movzbl	3(%rdi), %edx
	movb	%dl, 3(%rcx)
	movzbl	4(%rdi), %edx
	movb	%dl, 4(%rcx)
	movzbl	5(%rdi), %edx
	movb	%dl, 5(%rcx)
	movzbl	6(%rdi), %edx
	movb	%dl, 6(%rcx)
	movzbl	7(%rdi), %edx
	movb	%dl, 7(%rcx)
	addq	$8, %rdi
	addq	$8, %rcx
	addl	$-8, %r13d
	jne	.LBB1_94
.LBB1_95:                               # %.preheader.preheader.loopexit.i275
	leaq	1(%rax,%r8), %rcx
.LBB1_96:                               # %.preheader.i278.preheader
	movw	$25390, (%rcx)          # imm = 0x632E
	movb	$0, 2(%rcx)
.LBB1_97:                               # %stringappend.exit279
	movq	%rax, tabfile(%rip)
	leal	9(%r12), %r13d
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movl	%r13d, %edi
	callq	mallocate
	testl	%r12d, %r12d
	movq	%rax, %rcx
	jle	.LBB1_113
# BB#98:                                # %.lr.ph.preheader.i284
	leal	-1(%r12), %edx
	movl	%edx, %r8d
	leaq	1(%r8), %rsi
	cmpq	$32, %rsi
	jb	.LBB1_105
# BB#99:                                # %min.iters.checked452
	movq	%rsi, %r9
	andq	%r15, %r9
	je	.LBB1_105
# BB#100:                               # %vector.memcheck463
	leaq	1(%r14,%rdx), %rcx
	cmpq	%rcx, %rax
	jae	.LBB1_102
# BB#101:                               # %vector.memcheck463
	leaq	1(%rax,%rdx), %rcx
	cmpq	%rcx, %r14
	movq	%r14, %rdi
	movq	%rax, %rcx
	jb	.LBB1_106
.LBB1_102:                              # %vector.body448.preheader
	leaq	-32(%r9), %rcx
	movl	%ecx, %edi
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_124
# BB#103:                               # %vector.body448.prol.preheader
	negq	%rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_104:                              # %vector.body448.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r14,%rbp), %xmm0
	movups	16(%r14,%rbp), %xmm1
	movups	%xmm0, (%rax,%rbp)
	movups	%xmm1, 16(%rax,%rbp)
	addq	$32, %rbp
	incq	%rdi
	jne	.LBB1_104
	jmp	.LBB1_125
.LBB1_105:
	movq	%r14, %rdi
	movq	%rax, %rcx
.LBB1_106:                              # %.lr.ph.i289.preheader
	movl	%r12d, %esi
	subl	%ebp, %esi
	subl	%ebp, %edx
	andl	$7, %esi
	je	.LBB1_109
# BB#107:                               # %.lr.ph.i289.prol.preheader
	negl	%esi
	.p2align	4, 0x90
.LBB1_108:                              # %.lr.ph.i289.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %ebx
	incq	%rdi
	movb	%bl, (%rcx)
	incq	%rcx
	incl	%ebp
	incl	%esi
	jne	.LBB1_108
.LBB1_109:                              # %.lr.ph.i289.prol.loopexit
	cmpl	$7, %edx
	jb	.LBB1_112
# BB#110:                               # %.lr.ph.i289.preheader.new
	movl	%r12d, %edx
	subl	%ebp, %edx
	.p2align	4, 0x90
.LBB1_111:                              # %.lr.ph.i289
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %ebx
	movb	%bl, (%rcx)
	movzbl	1(%rdi), %ebx
	movb	%bl, 1(%rcx)
	movzbl	2(%rdi), %ebx
	movb	%bl, 2(%rcx)
	movzbl	3(%rdi), %ebx
	movb	%bl, 3(%rcx)
	movzbl	4(%rdi), %ebx
	movb	%bl, 4(%rcx)
	movzbl	5(%rdi), %ebx
	movb	%bl, 5(%rcx)
	movzbl	6(%rdi), %ebx
	movb	%bl, 6(%rcx)
	movzbl	7(%rdi), %ebx
	movb	%bl, 7(%rcx)
	addq	$8, %rdi
	addq	$8, %rcx
	addl	$-8, %edx
	jne	.LBB1_111
.LBB1_112:                              # %.preheader.preheader.loopexit.i291
	leaq	1(%rax,%r8), %rcx
.LBB1_113:                              # %.preheader.i294.preheader
	movb	$46, (%rcx)
	movb	$115, 1(%rcx)
	movb	$116, 2(%rcx)
	movb	$121, 3(%rcx)
	movb	$112, 4(%rcx)
	movb	$101, 5(%rcx)
	movb	$46, 6(%rcx)
	movb	$104, 7(%rcx)
	movb	$0, 8(%rcx)
	movq	%rax, attrsfile(%rip)
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movl	%r13d, %edi
	callq	mallocate
	testl	%r12d, %r12d
	movq	%rax, %rcx
	jle	.LBB1_143
# BB#114:                               # %.lr.ph.preheader.i300
	leal	-1(%r12), %edx
	movl	%edx, %ecx
	leaq	1(%rcx), %rsi
	cmpq	$32, %rsi
	jae	.LBB1_116
# BB#115:
	movq	%rax, %rsi
	jmp	.LBB1_136
.LBB1_116:                              # %min.iters.checked490
	andq	%rsi, %r15
	je	.LBB1_120
# BB#117:                               # %vector.memcheck501
	leaq	1(%r14,%rdx), %rdi
	cmpq	%rdi, %rax
	jae	.LBB1_121
# BB#118:                               # %vector.memcheck501
	leaq	1(%rax,%rdx), %rdi
	cmpq	%rdi, %r14
	jae	.LBB1_121
.LBB1_120:
	movq	%rax, %rsi
.LBB1_136:                              # %.lr.ph.i305.preheader
	movl	%r12d, %edi
	subl	%ebp, %edi
	subl	%ebp, %edx
	andl	$7, %edi
	je	.LBB1_139
# BB#137:                               # %.lr.ph.i305.prol.preheader
	negl	%edi
	.p2align	4, 0x90
.LBB1_138:                              # %.lr.ph.i305.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14), %ebx
	incq	%r14
	movb	%bl, (%rsi)
	incq	%rsi
	incl	%ebp
	incl	%edi
	jne	.LBB1_138
.LBB1_139:                              # %.lr.ph.i305.prol.loopexit
	cmpl	$7, %edx
	jb	.LBB1_142
# BB#140:                               # %.lr.ph.i305.preheader.new
	subl	%ebp, %r12d
	.p2align	4, 0x90
.LBB1_141:                              # %.lr.ph.i305
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14), %edx
	movb	%dl, (%rsi)
	movzbl	1(%r14), %edx
	movb	%dl, 1(%rsi)
	movzbl	2(%r14), %edx
	movb	%dl, 2(%rsi)
	movzbl	3(%r14), %edx
	movb	%dl, 3(%rsi)
	movzbl	4(%r14), %edx
	movb	%dl, 4(%rsi)
	movzbl	5(%r14), %edx
	movb	%dl, 5(%rsi)
	movzbl	6(%r14), %edx
	movb	%dl, 6(%rsi)
	movzbl	7(%r14), %edx
	movb	%dl, 7(%rsi)
	addq	$8, %r14
	addq	$8, %rsi
	addl	$-8, %r12d
	jne	.LBB1_141
.LBB1_142:                              # %.preheader.preheader.loopexit.i307
	leaq	1(%rax,%rcx), %rcx
.LBB1_143:                              # %.preheader.i310.preheader
	movb	$46, (%rcx)
	movb	$103, 1(%rcx)
	movb	$117, 2(%rcx)
	movb	$97, 3(%rcx)
	movb	$114, 4(%rcx)
	movb	$100, 5(%rcx)
	movb	$46, 6(%rcx)
	movb	$99, 7(%rcx)
	movb	$0, 8(%rcx)
	movq	%rax, guardfile(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_121:                              # %vector.body486.preheader
	leaq	-32(%r15), %rbp
	movl	%ebp, %edi
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_130
# BB#122:                               # %vector.body486.prol.preheader
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_123:                              # %vector.body486.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r14,%rbx), %xmm0
	movups	16(%r14,%rbx), %xmm1
	movups	%xmm0, (%rax,%rbx)
	movups	%xmm1, 16(%rax,%rbx)
	addq	$32, %rbx
	incq	%rdi
	jne	.LBB1_123
	jmp	.LBB1_131
.LBB1_25:                               # %vector.body.preheader
	leaq	-32(%rdx), %rbp
	movl	%ebp, %edi
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_28
# BB#26:                                # %vector.body.prol.preheader
	negq	%rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_27:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbx,%rax), %xmm0
	movups	16(%rbx,%rax), %xmm1
	movups	%xmm0, (%r14,%rax)
	movups	%xmm1, 16(%r14,%rax)
	addq	$32, %rax
	incq	%rdi
	jne	.LBB1_27
	jmp	.LBB1_29
.LBB1_124:
	xorl	%ebp, %ebp
.LBB1_125:                              # %vector.body448.prol.loopexit
	cmpq	$96, %rcx
	jb	.LBB1_128
# BB#126:                               # %vector.body448.preheader.new
	movq	%r9, %rcx
	subq	%rbp, %rcx
	leaq	112(%rax,%rbp), %rdi
	leaq	112(%r14,%rbp), %rbp
	.p2align	4, 0x90
.LBB1_127:                              # %vector.body448
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	subq	$-128, %rdi
	subq	$-128, %rbp
	addq	$-128, %rcx
	jne	.LBB1_127
.LBB1_128:                              # %middle.block449
	cmpq	%r9, %rsi
	je	.LBB1_112
# BB#129:
	leaq	(%r14,%r9), %rdi
	movq	%r9, %rcx
	addq	%rax, %rcx
	movl	%r9d, %ebp
	jmp	.LBB1_106
.LBB1_130:
	xorl	%ebx, %ebx
.LBB1_131:                              # %vector.body486.prol.loopexit
	cmpq	$96, %rbp
	jb	.LBB1_134
# BB#132:                               # %vector.body486.preheader.new
	movq	%r15, %rdi
	subq	%rbx, %rdi
	leaq	112(%rax,%rbx), %rbp
	leaq	112(%r14,%rbx), %rbx
	.p2align	4, 0x90
.LBB1_133:                              # %vector.body486
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-128, %rdi
	jne	.LBB1_133
.LBB1_134:                              # %middle.block487
	cmpq	%r15, %rsi
	je	.LBB1_142
# BB#135:
	addq	%r15, %r14
	movq	%r15, %rsi
	addq	%rax, %rsi
	movl	%r15d, %ebp
	jmp	.LBB1_136
.LBB1_144:
	xorl	%ebx, %ebx
.LBB1_145:                              # %vector.body334.prol.loopexit
	cmpq	$96, %rcx
	jb	.LBB1_148
# BB#146:                               # %vector.body334.preheader.new
	movq	%rdx, %rcx
	subq	%rbx, %rcx
	leaq	112(%rax,%rbx), %rdi
	leaq	112(%r14,%rbx), %rbx
	.p2align	4, 0x90
.LBB1_147:                              # %vector.body334
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	subq	$-128, %rdi
	subq	$-128, %rbx
	addq	$-128, %rcx
	jne	.LBB1_147
.LBB1_148:                              # %middle.block335
	cmpq	%rdx, %rsi
	je	.LBB1_59
# BB#149:
	leaq	(%r14,%rdx), %rdi
	movq	%rdx, %rcx
	addq	%rax, %rcx
	movl	%edx, %ebx
	jmp	.LBB1_53
.LBB1_150:
	xorl	%ebx, %ebx
.LBB1_151:                              # %vector.body372.prol.loopexit
	cmpq	$96, %rcx
	jb	.LBB1_154
# BB#152:                               # %vector.body372.preheader.new
	movq	%rdx, %rcx
	subq	%rbx, %rcx
	leaq	112(%rax,%rbx), %rdi
	leaq	112(%r14,%rbx), %rbx
	.p2align	4, 0x90
.LBB1_153:                              # %vector.body372
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	subq	$-128, %rdi
	subq	$-128, %rbx
	addq	$-128, %rcx
	jne	.LBB1_153
.LBB1_154:                              # %middle.block373
	cmpq	%rdx, %rsi
	je	.LBB1_77
# BB#155:
	leaq	(%r14,%rdx), %rdi
	movq	%rdx, %rcx
	addq	%rax, %rcx
	movl	%edx, %ebx
	jmp	.LBB1_71
.LBB1_156:
	xorl	%ebx, %ebx
.LBB1_157:                              # %vector.body410.prol.loopexit
	cmpq	$96, %rcx
	jb	.LBB1_160
# BB#158:                               # %vector.body410.preheader.new
	movq	%rdx, %rcx
	subq	%rbx, %rcx
	leaq	112(%rax,%rbx), %rdi
	leaq	112(%r14,%rbx), %rbx
	.p2align	4, 0x90
.LBB1_159:                              # %vector.body410
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	subq	$-128, %rdi
	subq	$-128, %rbx
	addq	$-128, %rcx
	jne	.LBB1_159
.LBB1_160:                              # %middle.block411
	cmpq	%rdx, %rsi
	je	.LBB1_95
# BB#161:
	leaq	(%r14,%rdx), %rdi
	movq	%rdx, %rcx
	addq	%rax, %rcx
	movl	%edx, %ebx
	jmp	.LBB1_89
.LBB1_28:
	xorl	%eax, %eax
.LBB1_29:                               # %vector.body.prol.loopexit
	cmpq	$96, %rbp
	jb	.LBB1_32
# BB#30:                                # %vector.body.preheader.new
	movq	%rdx, %rdi
	subq	%rax, %rdi
	leaq	112(%r14,%rax), %rbp
	leaq	112(%rbx,%rax), %rax
	.p2align	4, 0x90
.LBB1_31:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rax), %xmm0
	movups	-96(%rax), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rax), %xmm0
	movups	-64(%rax), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rax), %xmm0
	movups	-32(%rax), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rax), %xmm0
	movups	(%rax), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rbp
	subq	$-128, %rax
	addq	$-128, %rdi
	jne	.LBB1_31
.LBB1_32:                               # %middle.block
	cmpq	%rdx, %rsi
	je	.LBB1_40
# BB#33:
	addq	%rdx, %rbx
	movq	%rdx, %rsi
	addq	%r14, %rsi
	movl	%edx, %ebp
	jmp	.LBB1_34
.LBB1_162:
	movq	stderr(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, %rdi
	callq	perror
	movl	$2, %edi
	callq	exit
.Lfunc_end1:
	.size	openfiles, .Lfunc_end1-openfiles
	.cfi_endproc

	.globl	open_extra_files
	.p2align	4, 0x90
	.type	open_extra_files,@function
open_extra_files:                       # @open_extra_files
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movq	fparser(%rip), %rdi
	callq	fclose
	movl	$.L.str.14, %edi
	callq	getenv
	testq	%rax, %rax
	movl	$.L.str.15, %ebx
	cmovneq	%rax, %rbx
	movl	$.L.str.6, %esi
	movq	%rbx, %rdi
	callq	fopen
	testq	%rax, %rax
	je	.LBB2_1
# BB#3:                                 # %tryopen.exit
	movq	%rax, fparser(%rip)
	movq	attrsfile(%rip), %r14
	movl	$.L.str.16, %esi
	movq	%r14, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_4
# BB#5:                                 # %tryopen.exit9
	movq	fattrs(%rip), %rdi
	callq	rewind
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph
                                        #   in Loop: Header=BB2_7 Depth=1
	movl	%eax, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
.LBB2_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	fattrs(%rip), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB2_6
# BB#8:                                 # %._crit_edge
	movq	%rbx, fattrs(%rip)
	movq	guardfile(%rip), %rbx
	movl	$.L.str.16, %esi
	movq	%rbx, %rdi
	callq	fopen
	testq	%rax, %rax
	je	.LBB2_1
# BB#9:                                 # %tryopen.exit10
	movq	%rax, fguard(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_1:
	movq	stderr(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, %rdi
	jmp	.LBB2_2
.LBB2_4:
	movq	stderr(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r14, %rdi
.LBB2_2:
	callq	perror
	movl	$2, %edi
	callq	exit
.Lfunc_end2:
	.size	open_extra_files, .Lfunc_end2-open_extra_files
	.cfi_endproc

	.globl	tryopen
	.p2align	4, 0x90
	.type	tryopen,@function
tryopen:                                # @tryopen
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 16
.Lcfi28:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	fopen
	testq	%rax, %rax
	je	.LBB3_2
# BB#1:
	popq	%rbx
	retq
.LBB3_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, %rdi
	callq	perror
	movl	$2, %edi
	callq	exit
.Lfunc_end3:
	.size	tryopen, .Lfunc_end3-tryopen
	.cfi_endproc

	.globl	done
	.p2align	4, 0x90
	.type	done,@function
done:                                   # @done
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 16
	callq	exit
.Lfunc_end4:
	.size	done, .Lfunc_end4-done
	.cfi_endproc

	.type	finput,@object          # @finput
	.bss
	.globl	finput
	.p2align	3
finput:
	.quad	0
	.size	finput, 8

	.type	foutput,@object         # @foutput
	.globl	foutput
	.p2align	3
foutput:
	.quad	0
	.size	foutput, 8

	.type	fdefines,@object        # @fdefines
	.globl	fdefines
	.p2align	3
fdefines:
	.quad	0
	.size	fdefines, 8

	.type	ftable,@object          # @ftable
	.globl	ftable
	.p2align	3
ftable:
	.quad	0
	.size	ftable, 8

	.type	fattrs,@object          # @fattrs
	.globl	fattrs
	.p2align	3
fattrs:
	.quad	0
	.size	fattrs, 8

	.type	fguard,@object          # @fguard
	.globl	fguard
	.p2align	3
fguard:
	.quad	0
	.size	fguard, 8

	.type	faction,@object         # @faction
	.globl	faction
	.p2align	3
faction:
	.quad	0
	.size	faction, 8

	.type	fparser,@object         # @fparser
	.globl	fparser
	.p2align	3
fparser:
	.quad	0
	.size	fparser, 8

	.type	fixed_outfiles,@object  # @fixed_outfiles
	.globl	fixed_outfiles
	.p2align	2
fixed_outfiles:
	.long	0                       # 0x0
	.size	fixed_outfiles, 4

	.type	spec_outfile,@object    # @spec_outfile
	.comm	spec_outfile,8,8
	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	".tab"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"_tab"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"y.y"
	.size	.L.str.4, 4

	.type	infile,@object          # @infile
	.comm	infile,8,8
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"r"
	.size	.L.str.6, 2

	.type	outfile,@object         # @outfile
	.comm	outfile,8,8
	.type	defsfile,@object        # @defsfile
	.comm	defsfile,8,8
	.type	actfile,@object         # @actfile
	.comm	actfile,8,8
	.type	tmpattrsfile,@object    # @tmpattrsfile
	.comm	tmpattrsfile,8,8
	.type	tmptabfile,@object      # @tmptabfile
	.comm	tmptabfile,8,8
	.type	tabfile,@object         # @tabfile
	.comm	tabfile,8,8
	.type	attrsfile,@object       # @attrsfile
	.comm	attrsfile,8,8
	.type	guardfile,@object       # @guardfile
	.comm	guardfile,8,8
	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"BISON_HAIRY"
	.size	.L.str.14, 12

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"parse.y.in"
	.size	.L.str.15, 11

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"w"
	.size	.L.str.16, 2

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"bison: "
	.size	.L.str.17, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
