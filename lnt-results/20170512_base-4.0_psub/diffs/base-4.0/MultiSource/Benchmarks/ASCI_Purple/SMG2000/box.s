	.text
	.file	"box.bc"
	.globl	hypre_BoxCreate
	.p2align	4, 0x90
	.type	hypre_BoxCreate,@function
hypre_BoxCreate:                        # @hypre_BoxCreate
	.cfi_startproc
# BB#0:
	movl	$24, %edi
	jmp	hypre_MAlloc            # TAILCALL
.Lfunc_end0:
	.size	hypre_BoxCreate, .Lfunc_end0-hypre_BoxCreate
	.cfi_endproc

	.globl	hypre_BoxSetExtents
	.p2align	4, 0x90
	.type	hypre_BoxSetExtents,@function
hypre_BoxSetExtents:                    # @hypre_BoxSetExtents
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 4(%rdi)
	movl	8(%rsi), %eax
	movl	%eax, 8(%rdi)
	movl	(%rdx), %eax
	movl	%eax, 12(%rdi)
	movl	4(%rdx), %eax
	movl	%eax, 16(%rdi)
	movl	8(%rdx), %eax
	movl	%eax, 20(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	hypre_BoxSetExtents, .Lfunc_end1-hypre_BoxSetExtents
	.cfi_endproc

	.globl	hypre_BoxArrayCreate
	.p2align	4, 0x90
	.type	hypre_BoxArrayCreate,@function
hypre_BoxArrayCreate:                   # @hypre_BoxArrayCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	$16, %edi
	callq	hypre_MAlloc
	movq	%rax, %rbx
	movl	$24, %esi
	movl	%ebp, %edi
	callq	hypre_CAlloc
	movq	%rax, (%rbx)
	movl	%ebp, 8(%rbx)
	movl	%ebp, 12(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end2:
	.size	hypre_BoxArrayCreate, .Lfunc_end2-hypre_BoxArrayCreate
	.cfi_endproc

	.globl	hypre_BoxArraySetSize
	.p2align	4, 0x90
	.type	hypre_BoxArraySetSize,@function
hypre_BoxArraySetSize:                  # @hypre_BoxArraySetSize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	cmpl	%r14d, 12(%rbx)
	jge	.LBB3_2
# BB#1:
	leal	10(%r14), %ebp
	movq	(%rbx), %rdi
	leal	80(,%r14,8), %eax
	leal	(%rax,%rax,2), %esi
	callq	hypre_ReAlloc
	movq	%rax, (%rbx)
	movl	%ebp, 12(%rbx)
.LBB3_2:
	movl	%r14d, 8(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	hypre_BoxArraySetSize, .Lfunc_end3-hypre_BoxArraySetSize
	.cfi_endproc

	.globl	hypre_BoxArrayArrayCreate
	.p2align	4, 0x90
	.type	hypre_BoxArrayArrayCreate,@function
hypre_BoxArrayArrayCreate:              # @hypre_BoxArrayArrayCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -48
.Lcfi17:
	.cfi_offset %r12, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movl	$1, %edi
	movl	$16, %esi
	callq	hypre_CAlloc
	movq	%rax, %r15
	movl	$8, %esi
	movl	%r14d, %edi
	callq	hypre_CAlloc
	movq	%rax, (%r15)
	testl	%r14d, %r14d
	jle	.LBB4_3
# BB#1:                                 # %.lr.ph.preheader
	movl	%r14d, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	hypre_MAlloc
	movq	%rax, %rbp
	xorl	%edi, %edi
	movl	$24, %esi
	callq	hypre_CAlloc
	movq	%rax, (%rbp)
	movq	$0, 8(%rbp)
	movq	(%r15), %rax
	movq	%rbp, (%rax,%rbx,8)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB4_2
.LBB4_3:                                # %._crit_edge
	movl	%r14d, 8(%r15)
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	hypre_BoxArrayArrayCreate, .Lfunc_end4-hypre_BoxArrayArrayCreate
	.cfi_endproc

	.globl	hypre_BoxDestroy
	.p2align	4, 0x90
	.type	hypre_BoxDestroy,@function
hypre_BoxDestroy:                       # @hypre_BoxDestroy
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#1:
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 16
	callq	hypre_Free
	addq	$8, %rsp
.LBB5_2:
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	hypre_BoxDestroy, .Lfunc_end5-hypre_BoxDestroy
	.cfi_endproc

	.globl	hypre_BoxArrayDestroy
	.p2align	4, 0x90
	.type	hypre_BoxArrayDestroy,@function
hypre_BoxArrayDestroy:                  # @hypre_BoxArrayDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
.Lcfi23:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB6_2
# BB#1:
	movq	(%rbx), %rdi
	callq	hypre_Free
	movq	$0, (%rbx)
	movq	%rbx, %rdi
	callq	hypre_Free
.LBB6_2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	hypre_BoxArrayDestroy, .Lfunc_end6-hypre_BoxArrayDestroy
	.cfi_endproc

	.globl	hypre_BoxArrayArrayDestroy
	.p2align	4, 0x90
	.type	hypre_BoxArrayArrayDestroy,@function
hypre_BoxArrayArrayDestroy:             # @hypre_BoxArrayArrayDestroy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB7_7
# BB#1:                                 # %.preheader
	movl	8(%r14), %eax
	movq	(%r14), %rdi
	testl	%eax, %eax
	jle	.LBB7_6
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %r15
	testq	%r15, %r15
	je	.LBB7_5
# BB#4:                                 #   in Loop: Header=BB7_3 Depth=1
	movq	(%r15), %rdi
	callq	hypre_Free
	movq	$0, (%r15)
	movq	%r15, %rdi
	callq	hypre_Free
	movl	8(%r14), %eax
	movq	(%r14), %rdi
.LBB7_5:                                # %hypre_BoxArrayDestroy.exit
                                        #   in Loop: Header=BB7_3 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB7_3
.LBB7_6:                                # %._crit_edge
	callq	hypre_Free
	movq	$0, (%r14)
	movq	%r14, %rdi
	callq	hypre_Free
.LBB7_7:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	hypre_BoxArrayArrayDestroy, .Lfunc_end7-hypre_BoxArrayArrayDestroy
	.cfi_endproc

	.globl	hypre_BoxDuplicate
	.p2align	4, 0x90
	.type	hypre_BoxDuplicate,@function
hypre_BoxDuplicate:                     # @hypre_BoxDuplicate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 16
.Lcfi31:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$24, %edi
	callq	hypre_MAlloc
	movl	(%rbx), %ecx
	movl	%ecx, (%rax)
	movl	4(%rbx), %ecx
	movl	%ecx, 4(%rax)
	movl	8(%rbx), %ecx
	movl	%ecx, 8(%rax)
	movl	12(%rbx), %ecx
	movl	%ecx, 12(%rax)
	movl	16(%rbx), %ecx
	movl	%ecx, 16(%rax)
	movl	20(%rbx), %ecx
	movl	%ecx, 20(%rax)
	popq	%rbx
	retq
.Lfunc_end8:
	.size	hypre_BoxDuplicate, .Lfunc_end8-hypre_BoxDuplicate
	.cfi_endproc

	.globl	hypre_BoxArrayDuplicate
	.p2align	4, 0x90
	.type	hypre_BoxArrayDuplicate,@function
hypre_BoxArrayDuplicate:                # @hypre_BoxArrayDuplicate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	8(%rbx), %ebp
	movl	$16, %edi
	callq	hypre_MAlloc
	movq	%rax, %r14
	movl	$24, %esi
	movl	%ebp, %edi
	callq	hypre_CAlloc
	movq	%rax, (%r14)
	movl	%ebp, 8(%r14)
	movl	%ebp, 12(%r14)
	cmpl	$0, 8(%rbx)
	jle	.LBB9_3
# BB#1:                                 # %.lr.ph
	movq	(%rbx), %rcx
	addq	$20, %rcx
	addq	$20, %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rax)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rax)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rax)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rax)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rax)
	movl	(%rcx), %esi
	movl	%esi, (%rax)
	incq	%rdx
	movslq	8(%rbx), %rsi
	addq	$24, %rcx
	addq	$24, %rax
	cmpq	%rsi, %rdx
	jl	.LBB9_2
.LBB9_3:                                # %._crit_edge
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end9:
	.size	hypre_BoxArrayDuplicate, .Lfunc_end9-hypre_BoxArrayDuplicate
	.cfi_endproc

	.globl	hypre_BoxArrayArrayDuplicate
	.p2align	4, 0x90
	.type	hypre_BoxArrayArrayDuplicate,@function
hypre_BoxArrayArrayDuplicate:           # @hypre_BoxArrayArrayDuplicate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 96
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	8(%r15), %r14d
	movl	$1, %edi
	movl	$16, %esi
	callq	hypre_CAlloc
	movq	%rax, %r12
	movl	$8, %esi
	movl	%r14d, %edi
	callq	hypre_CAlloc
	movq	%rax, (%r12)
	testl	%r14d, %r14d
	jle	.LBB10_3
# BB#1:                                 # %.lr.ph.preheader.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	hypre_MAlloc
	movq	%rax, %rbx
	xorl	%edi, %edi
	movl	$24, %esi
	callq	hypre_CAlloc
	movq	%rax, (%rbx)
	movq	$0, 8(%rbx)
	movq	(%r12), %rax
	movq	%rbx, (%rax,%rbp,8)
	incq	%rbp
	cmpq	%rbp, %r14
	jne	.LBB10_2
.LBB10_3:                               # %hypre_BoxArrayArrayCreate.exit
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movl	%r14d, 8(%r12)
	testl	%r14d, %r14d
	je	.LBB10_12
# BB#4:
	jle	.LBB10_12
# BB#5:                                 # %.lr.ph.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rsi
	movq	(%r15), %rdi
	xorl	%ebx, %ebx
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB10_6:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_10 Depth 2
	movq	(%rdi,%rbx,8), %r13
	movq	(%rsi,%rbx,8), %r15
	movslq	8(%r15), %r14
	movl	8(%r13), %ebp
	leal	(%rbp,%r14), %r12d
	cmpl	%r12d, 12(%r15)
	jge	.LBB10_8
# BB#7:                                 #   in Loop: Header=BB10_6 Depth=1
	leal	10(%r12), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	(%r15), %rdi
	leal	80(,%r12,8), %eax
	leal	(%rax,%rax,2), %esi
	callq	hypre_ReAlloc
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rax, (%r15)
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 12(%r15)
.LBB10_8:                               # %hypre_BoxArraySetSize.exit.i
                                        #   in Loop: Header=BB10_6 Depth=1
	movl	%r12d, 8(%r15)
	testl	%ebp, %ebp
	jle	.LBB10_11
# BB#9:                                 # %.lr.ph.i22
                                        #   in Loop: Header=BB10_6 Depth=1
	movq	(%r13), %rax
	movq	(%r15), %rcx
	leaq	(%r14,%r14,2), %rdx
	leaq	20(%rcx,%rdx,8), %rcx
	addq	$20, %rax
	.p2align	4, 0x90
.LBB10_10:                              #   Parent Loop BB10_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-20(%rax), %edx
	movl	%edx, -20(%rcx)
	movl	-16(%rax), %edx
	movl	%edx, -16(%rcx)
	movl	-12(%rax), %edx
	movl	%edx, -12(%rcx)
	movl	-8(%rax), %edx
	movl	%edx, -8(%rcx)
	movl	-4(%rax), %edx
	movl	%edx, -4(%rcx)
	movl	(%rax), %edx
	movl	%edx, (%rcx)
	addq	$24, %rcx
	addq	$24, %rax
	decq	%rbp
	jne	.LBB10_10
.LBB10_11:                              # %hypre_AppendBoxArray.exit
                                        #   in Loop: Header=BB10_6 Depth=1
	incq	%rbx
	movq	32(%rsp), %r14          # 8-byte Reload
	cmpq	%r14, %rbx
	jne	.LBB10_6
.LBB10_12:                              # %.loopexit
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	hypre_BoxArrayArrayDuplicate, .Lfunc_end10-hypre_BoxArrayArrayDuplicate
	.cfi_endproc

	.globl	hypre_AppendBoxArray
	.p2align	4, 0x90
	.type	hypre_AppendBoxArray,@function
hypre_AppendBoxArray:                   # @hypre_AppendBoxArray
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 64
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movslq	8(%r15), %r12
	movl	8(%r14), %ebx
	leal	(%rbx,%r12), %r13d
	cmpl	%r13d, 12(%r15)
	jge	.LBB11_2
# BB#1:
	leal	10(%r13), %ebp
	movq	(%r15), %rdi
	leal	80(,%r13,8), %eax
	leal	(%rax,%rax,2), %esi
	callq	hypre_ReAlloc
	movq	%rax, (%r15)
	movl	%ebp, 12(%r15)
.LBB11_2:                               # %hypre_BoxArraySetSize.exit
	movl	%r13d, 8(%r15)
	testl	%ebx, %ebx
	jle	.LBB11_5
# BB#3:                                 # %.lr.ph
	movq	(%r14), %rax
	movq	(%r15), %rcx
	addq	$20, %rax
	leaq	(%r12,%r12,2), %rdx
	leaq	20(%rcx,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB11_4:                               # =>This Inner Loop Header: Depth=1
	movl	-20(%rax), %edx
	movl	%edx, -20(%rcx)
	movl	-16(%rax), %edx
	movl	%edx, -16(%rcx)
	movl	-12(%rax), %edx
	movl	%edx, -12(%rcx)
	movl	-8(%rax), %edx
	movl	%edx, -8(%rcx)
	movl	-4(%rax), %edx
	movl	%edx, -4(%rcx)
	movl	(%rax), %edx
	movl	%edx, (%rcx)
	addq	$24, %rax
	addq	$24, %rcx
	decq	%rbx
	jne	.LBB11_4
.LBB11_5:                               # %._crit_edge
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	hypre_AppendBoxArray, .Lfunc_end11-hypre_AppendBoxArray
	.cfi_endproc

	.globl	hypre_AppendBox
	.p2align	4, 0x90
	.type	hypre_AppendBox,@function
hypre_AppendBox:                        # @hypre_AppendBox
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 48
.Lcfi69:
	.cfi_offset %rbx, -48
.Lcfi70:
	.cfi_offset %r12, -40
.Lcfi71:
	.cfi_offset %r14, -32
.Lcfi72:
	.cfi_offset %r15, -24
.Lcfi73:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movslq	8(%rbx), %r15
	leal	1(%r15), %ebp
	cmpl	%r15d, 12(%rbx)
	jle	.LBB12_2
# BB#1:                                 # %.hypre_BoxArraySetSize.exit_crit_edge
	movq	(%rbx), %rax
	jmp	.LBB12_3
.LBB12_2:
	leal	11(%r15), %r12d
	movq	(%rbx), %rdi
	leal	88(,%r15,8), %eax
	leal	(%rax,%rax,2), %esi
	callq	hypre_ReAlloc
	movq	%rax, (%rbx)
	movl	%r12d, 12(%rbx)
.LBB12_3:                               # %hypre_BoxArraySetSize.exit
	movl	%ebp, 8(%rbx)
	movl	(%r14), %ecx
	leaq	(%r15,%r15,2), %rdx
	movl	%ecx, (%rax,%rdx,8)
	movl	4(%r14), %ecx
	movl	%ecx, 4(%rax,%rdx,8)
	movl	8(%r14), %ecx
	movl	%ecx, 8(%rax,%rdx,8)
	movl	12(%r14), %ecx
	movl	%ecx, 12(%rax,%rdx,8)
	movl	16(%r14), %ecx
	movl	%ecx, 16(%rax,%rdx,8)
	movl	20(%r14), %ecx
	movl	%ecx, 20(%rax,%rdx,8)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	hypre_AppendBox, .Lfunc_end12-hypre_AppendBox
	.cfi_endproc

	.globl	hypre_DeleteBox
	.p2align	4, 0x90
	.type	hypre_DeleteBox,@function
hypre_DeleteBox:                        # @hypre_DeleteBox
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %edx
	decl	%edx
	cmpl	%esi, %edx
	jle	.LBB13_3
# BB#1:                                 # %.lr.ph
	movq	(%rdi), %rcx
	movslq	%esi, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	44(%rcx,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	incq	%rax
	movups	-20(%rcx), %xmm0
	movups	%xmm0, -44(%rcx)
	movl	-4(%rcx), %edx
	movl	%edx, -28(%rcx)
	movl	(%rcx), %edx
	movl	%edx, -24(%rcx)
	movslq	8(%rdi), %rdx
	decq	%rdx
	addq	$24, %rcx
	cmpq	%rdx, %rax
	jl	.LBB13_2
.LBB13_3:                               # %._crit_edge
	movl	%edx, 8(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end13:
	.size	hypre_DeleteBox, .Lfunc_end13-hypre_DeleteBox
	.cfi_endproc

	.globl	hypre_BoxGetSize
	.p2align	4, 0x90
	.type	hypre_BoxGetSize,@function
hypre_BoxGetSize:                       # @hypre_BoxGetSize
	.cfi_startproc
# BB#0:
	movl	12(%rdi), %eax
	movl	(%rdi), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	xorl	%r8d, %r8d
	cmpl	%ecx, %eax
	cmovsl	%r8d, %edx
	movl	%edx, (%rsi)
	movl	16(%rdi), %eax
	movl	4(%rdi), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	cmovsl	%r8d, %edx
	movl	%edx, 4(%rsi)
	movl	20(%rdi), %eax
	movl	8(%rdi), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	cmovsl	%r8d, %edx
	movl	%edx, 8(%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end14:
	.size	hypre_BoxGetSize, .Lfunc_end14-hypre_BoxGetSize
	.cfi_endproc

	.globl	hypre_BoxGetStrideSize
	.p2align	4, 0x90
	.type	hypre_BoxGetStrideSize,@function
hypre_BoxGetStrideSize:                 # @hypre_BoxGetStrideSize
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
	movl	12(%rdi), %r9d
	movl	(%rdi), %edx
	movl	%r9d, %eax
	subl	%edx, %eax
	incl	%eax
	xorl	%ecx, %ecx
	cmpl	%edx, %r9d
	cmovsl	%ecx, %eax
	testl	%eax, %eax
	jle	.LBB15_2
# BB#1:
	decl	%eax
	cltd
	idivl	(%rsi)
	incl	%eax
.LBB15_2:
	movl	%eax, (%r8)
	movl	16(%rdi), %r9d
	movl	4(%rdi), %edx
	movl	%r9d, %eax
	subl	%edx, %eax
	incl	%eax
	cmpl	%edx, %r9d
	cmovnsl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.LBB15_4
# BB#3:
	decl	%ecx
	movl	%ecx, %eax
	cltd
	idivl	4(%rsi)
	movl	%eax, %ecx
	incl	%ecx
.LBB15_4:
	movl	%ecx, 4(%r8)
	movl	20(%rdi), %ecx
	movl	8(%rdi), %edx
	movl	%ecx, %edi
	subl	%edx, %edi
	incl	%edi
	xorl	%eax, %eax
	cmpl	%edx, %ecx
	cmovnsl	%edi, %eax
	testl	%eax, %eax
	jle	.LBB15_6
# BB#5:
	decl	%eax
	cltd
	idivl	8(%rsi)
	incl	%eax
.LBB15_6:
	movl	%eax, 8(%r8)
	xorl	%eax, %eax
	retq
.Lfunc_end15:
	.size	hypre_BoxGetStrideSize, .Lfunc_end15-hypre_BoxGetStrideSize
	.cfi_endproc

	.globl	hypre_IModPeriod
	.p2align	4, 0x90
	.type	hypre_IModPeriod,@function
hypre_IModPeriod:                       # @hypre_IModPeriod
	.cfi_startproc
# BB#0:
	movl	%edi, %ecx
	testl	%esi, %esi
	je	.LBB16_6
# BB#1:
	cmpl	%esi, %ecx
	jge	.LBB16_2
# BB#3:
	testl	%ecx, %ecx
	jns	.LBB16_6
# BB#4:
	movl	%ecx, %eax
	negl	%eax
	cltd
	idivl	%esi
	incl	%eax
	imull	%esi, %eax
	addl	%ecx, %eax
	jmp	.LBB16_5
.LBB16_2:
	movl	%ecx, %eax
.LBB16_5:
	cltd
	idivl	%esi
	movl	%edx, %ecx
.LBB16_6:
	movl	%ecx, %eax
	retq
.Lfunc_end16:
	.size	hypre_IModPeriod, .Lfunc_end16-hypre_IModPeriod
	.cfi_endproc

	.globl	hypre_IModPeriodX
	.p2align	4, 0x90
	.type	hypre_IModPeriodX,@function
hypre_IModPeriodX:                      # @hypre_IModPeriodX
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	movl	(%rsi), %esi
	testl	%esi, %esi
	je	.LBB17_6
# BB#1:
	cmpl	%esi, %ecx
	jge	.LBB17_2
# BB#3:
	testl	%ecx, %ecx
	jns	.LBB17_6
# BB#4:
	movl	%ecx, %eax
	negl	%eax
	cltd
	idivl	%esi
	incl	%eax
	imull	%esi, %eax
	addl	%ecx, %eax
	jmp	.LBB17_5
.LBB17_2:
	movl	%ecx, %eax
.LBB17_5:                               # %hypre_IModPeriod.exit
	cltd
	idivl	%esi
	movl	%edx, %ecx
.LBB17_6:                               # %hypre_IModPeriod.exit
	movl	%ecx, %eax
	retq
.Lfunc_end17:
	.size	hypre_IModPeriodX, .Lfunc_end17-hypre_IModPeriodX
	.cfi_endproc

	.globl	hypre_IModPeriodY
	.p2align	4, 0x90
	.type	hypre_IModPeriodY,@function
hypre_IModPeriodY:                      # @hypre_IModPeriodY
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %ecx
	movl	4(%rsi), %esi
	testl	%esi, %esi
	je	.LBB18_6
# BB#1:
	cmpl	%esi, %ecx
	jge	.LBB18_2
# BB#3:
	testl	%ecx, %ecx
	jns	.LBB18_6
# BB#4:
	movl	%ecx, %eax
	negl	%eax
	cltd
	idivl	%esi
	incl	%eax
	imull	%esi, %eax
	addl	%ecx, %eax
	jmp	.LBB18_5
.LBB18_2:
	movl	%ecx, %eax
.LBB18_5:                               # %hypre_IModPeriod.exit
	cltd
	idivl	%esi
	movl	%edx, %ecx
.LBB18_6:                               # %hypre_IModPeriod.exit
	movl	%ecx, %eax
	retq
.Lfunc_end18:
	.size	hypre_IModPeriodY, .Lfunc_end18-hypre_IModPeriodY
	.cfi_endproc

	.globl	hypre_IModPeriodZ
	.p2align	4, 0x90
	.type	hypre_IModPeriodZ,@function
hypre_IModPeriodZ:                      # @hypre_IModPeriodZ
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %ecx
	movl	8(%rsi), %esi
	testl	%esi, %esi
	je	.LBB19_6
# BB#1:
	cmpl	%esi, %ecx
	jge	.LBB19_2
# BB#3:
	testl	%ecx, %ecx
	jns	.LBB19_6
# BB#4:
	movl	%ecx, %eax
	negl	%eax
	cltd
	idivl	%esi
	incl	%eax
	imull	%esi, %eax
	addl	%ecx, %eax
	jmp	.LBB19_5
.LBB19_2:
	movl	%ecx, %eax
.LBB19_5:                               # %hypre_IModPeriod.exit
	cltd
	idivl	%esi
	movl	%edx, %ecx
.LBB19_6:                               # %hypre_IModPeriod.exit
	movl	%ecx, %eax
	retq
.Lfunc_end19:
	.size	hypre_IModPeriodZ, .Lfunc_end19-hypre_IModPeriodZ
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
