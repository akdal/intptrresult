	.text
	.file	"dbisect.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
.LCPI0_2:
	.quad	4841369599423283200     # double 4503599627370496
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_1:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	sturm
	.p2align	4, 0x90
	.type	sturm,@function
sturm:                                  # @sturm
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	jle	.LBB0_1
# BB#2:                                 # %.lr.ph.preheader
	movl	%edi, %r8d
	xorl	%eax, %eax
	movsd	.LCPI0_0(%rip), %xmm5   # xmm5 = mem[0],zero
	xorps	%xmm1, %xmm1
	movapd	.LCPI0_1(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI0_2(%rip), %xmm3   # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rsi), %xmm4           # xmm4 = mem[0],zero
	subsd	%xmm0, %xmm4
	ucomisd	%xmm1, %xmm5
	jne	.LBB0_4
	jnp	.LBB0_5
.LBB0_4:                                #   in Loop: Header=BB0_3 Depth=1
	movsd	(%rcx), %xmm6           # xmm6 = mem[0],zero
	divsd	%xmm5, %xmm6
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_5:                                #   in Loop: Header=BB0_3 Depth=1
	movsd	(%rdx), %xmm6           # xmm6 = mem[0],zero
	andpd	%xmm2, %xmm6
	mulsd	%xmm3, %xmm6
.LBB0_6:                                #   in Loop: Header=BB0_3 Depth=1
	subsd	%xmm6, %xmm4
	xorl	%edi, %edi
	ucomisd	%xmm4, %xmm1
	seta	%dil
	addl	%edi, %eax
	addq	$8, %rsi
	addq	$8, %rcx
	addq	$8, %rdx
	decq	%r8
	movapd	%xmm4, %xmm5
	jne	.LBB0_3
# BB#7:                                 # %._crit_edge
	retq
.LBB0_1:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	sturm, .Lfunc_end0-sturm
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
.LCPI1_2:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_1:
	.quad	4607227454796291113     # double 1.01
.LCPI1_3:
	.quad	4372995238176751616     # double 2.2204460492503131E-16
.LCPI1_4:
	.quad	4602678819172646912     # double 0.5
.LCPI1_5:
	.quad	4619567317775286272     # double 7
.LCPI1_6:
	.quad	4377498837804122112     # double 4.4408920985006262E-16
.LCPI1_7:
	.quad	4607182418800017408     # double 1
.LCPI1_8:
	.quad	4841369599423283200     # double 4503599627370496
	.text
	.globl	dbisect
	.p2align	4, 0x90
	.type	dbisect,@function
dbisect:                                # @dbisect
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	160(%rsp), %r12
	movq	152(%rsp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rax
	movq	$0, (%r15)
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	$0, (%rdx)
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movslq	%ecx, %rcx
	movsd	-8(%r14,%rcx,8), %xmm3  # xmm3 = mem[0],zero
	movsd	-8(%r15,%rcx,8), %xmm1  # xmm1 = mem[0],zero
	andpd	.LCPI1_0(%rip), %xmm1
	mulsd	.LCPI1_1(%rip), %xmm1
	movapd	%xmm3, %xmm2
	subsd	%xmm1, %xmm2
	addsd	%xmm3, %xmm1
	movl	%r8d, %ebx
	addl	$-2, %ecx
	js	.LBB1_1
# BB#2:                                 # %.lr.ph166.preheader
	movslq	%ecx, %rcx
	incq	%rcx
	movapd	.LCPI1_0(%rip), %xmm3   # xmm3 = [nan,nan]
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph166
                                        # =>This Inner Loop Header: Depth=1
	movsd	-8(%r15,%rcx,8), %xmm5  # xmm5 = mem[0],zero
	andpd	%xmm3, %xmm5
	movsd	(%r15,%rcx,8), %xmm6    # xmm6 = mem[0],zero
	andpd	%xmm3, %xmm6
	addsd	%xmm5, %xmm6
	mulsd	%xmm4, %xmm6
	movsd	-8(%r14,%rcx,8), %xmm5  # xmm5 = mem[0],zero
	movapd	%xmm5, %xmm7
	addsd	%xmm6, %xmm7
	maxsd	%xmm1, %xmm7
	subsd	%xmm6, %xmm5
	minsd	%xmm2, %xmm5
	decq	%rcx
	movapd	%xmm7, %xmm1
	movapd	%xmm5, %xmm2
	jg	.LBB1_3
	jmp	.LBB1_4
.LBB1_1:
	movapd	%xmm2, %xmm5
	movapd	%xmm1, %xmm7
.LBB1_4:                                # %._crit_edge167
	movapd	%xmm5, %xmm1
	addsd	%xmm7, %xmm1
	movapd	.LCPI1_2(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00]
	movapd	%xmm5, (%rsp)           # 16-byte Spill
	xorpd	%xmm5, %xmm2
	xorpd	%xmm3, %xmm3
	movapd	%xmm0, %xmm4
	cmpnlesd	%xmm3, %xmm4
	cmpltsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm1
	movapd	%xmm7, 48(%rsp)         # 16-byte Spill
	andpd	%xmm7, %xmm1
	andnpd	%xmm2, %xmm3
	orpd	%xmm1, %xmm3
	mulsd	.LCPI1_3(%rip), %xmm3
	movapd	%xmm4, %xmm1
	andnpd	%xmm3, %xmm1
	andpd	%xmm0, %xmm4
	orpd	%xmm1, %xmm4
	movsd	.LCPI1_4(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm4, 64(%rsp)         # 16-byte Spill
	mulsd	%xmm4, %xmm0
	mulsd	.LCPI1_5(%rip), %xmm3
	addsd	%xmm0, %xmm3
	movsd	%xmm3, (%rax)
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	1(%rax), %eax
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	testq	%rax, %rax
	je	.LBB1_31
# BB#5:                                 # %.preheader140
	movl	%ebx, %esi
	cmpl	%esi, %r13d
	jge	.LBB1_7
# BB#6:                                 # %._crit_edge161.thread
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	$0, (%rcx)
	jmp	.LBB1_52
.LBB1_7:                                # %.lr.ph160.preheader
	movslq	%r13d, %rdx
	movslq	%esi, %r10
	cmpq	%rdx, %r10
	movq	%r10, %rcx
	cmovgq	%rdx, %rcx
	notq	%rcx
	leaq	2(%rdx,%rcx), %r11
	cmpq	$3, %r11
	ja	.LBB1_22
# BB#8:
	movaps	(%rsp), %xmm13          # 16-byte Reload
	movapd	64(%rsp), %xmm14        # 16-byte Reload
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	jmp	.LBB1_9
.LBB1_22:                               # %min.iters.checked
	movq	%r11, %r8
	andq	$-4, %r8
	movq	%r11, %r9
	andq	$-4, %r9
	movaps	(%rsp), %xmm13          # 16-byte Reload
	movapd	64(%rsp), %xmm14        # 16-byte Reload
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	je	.LBB1_9
# BB#23:                                # %vector.ph
	leaq	-4(%r9), %rcx
	movq	%rcx, %rbp
	shrq	$2, %rbp
	btl	$2, %ecx
	jb	.LBB1_24
# BB#25:                                # %vector.body.prol
	movaps	%xmm3, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movups	%xmm0, -8(%r12,%rdx,8)
	movups	%xmm0, -24(%r12,%rdx,8)
	movaps	%xmm13, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movups	%xmm0, -8(%rax,%rdx,8)
	movups	%xmm0, -24(%rax,%rdx,8)
	movl	$4, %ecx
	testq	%rbp, %rbp
	jne	.LBB1_27
	jmp	.LBB1_29
.LBB1_24:
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	je	.LBB1_29
.LBB1_27:                               # %vector.ph.new
	movaps	%xmm3, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movaps	%xmm13, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movq	%r9, %rbx
	negq	%rbx
	negq	%rcx
	leaq	-8(%r12,%rdx,8), %rbp
	leaq	-8(%rax,%rdx,8), %rdi
	.p2align	4, 0x90
.LBB1_28:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, (%rbp,%rcx,8)
	movups	%xmm0, -16(%rbp,%rcx,8)
	movups	%xmm1, (%rdi,%rcx,8)
	movups	%xmm1, -16(%rdi,%rcx,8)
	movups	%xmm0, -32(%rbp,%rcx,8)
	movups	%xmm0, -48(%rbp,%rcx,8)
	movups	%xmm1, -32(%rdi,%rcx,8)
	movups	%xmm1, -48(%rdi,%rcx,8)
	addq	$-8, %rcx
	cmpq	%rcx, %rbx
	jne	.LBB1_28
.LBB1_29:                               # %middle.block
	cmpq	%r9, %r11
	je	.LBB1_11
# BB#30:
	subq	%r8, %rdx
.LBB1_9:                                # %.lr.ph160.preheader196
	incq	%rdx
	.p2align	4, 0x90
.LBB1_10:                               # %.lr.ph160
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm3, -8(%r12,%rdx,8)
	movsd	%xmm13, -8(%rax,%rdx,8)
	decq	%rdx
	cmpq	%r10, %rdx
	jg	.LBB1_10
.LBB1_11:                               # %._crit_edge161
	movq	%r12, (%rsp)            # 8-byte Spill
	movl	%esi, %r12d
	cmpl	%esi, %r13d
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	$0, (%rcx)
	jl	.LBB1_52
# BB#12:                                # %.preheader.lr.ph
	movl	32(%rsp), %r9d          # 4-byte Reload
	movslq	%r13d, %r11
	leaq	1(%r11), %r8
	xorl	%edi, %edi
	movsd	.LCPI1_4(%rip), %xmm10  # xmm10 = mem[0],zero
	movapd	.LCPI1_0(%rip), %xmm11  # xmm11 = [nan,nan]
	movsd	.LCPI1_6(%rip), %xmm9   # xmm9 = mem[0],zero
	movsd	.LCPI1_7(%rip), %xmm8   # xmm8 = mem[0],zero
	xorpd	%xmm4, %xmm4
	movapd	.LCPI1_0(%rip), %xmm12  # xmm12 = [nan,nan]
	movsd	.LCPI1_8(%rip), %xmm6   # xmm6 = mem[0],zero
	.p2align	4, 0x90
.LBB1_13:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_14 Depth 2
                                        #     Child Loop BB1_42 Depth 2
                                        #     Child Loop BB1_19 Depth 2
                                        #       Child Loop BB1_20 Depth 3
	cmpq	%r10, %r11
	movq	%r8, %rcx
	movapd	%xmm13, %xmm2
	jl	.LBB1_17
	.p2align	4, 0x90
.LBB1_14:                               # %.lr.ph
                                        #   Parent Loop BB1_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rax,%rcx,8), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm13, %xmm2
	ja	.LBB1_17
# BB#15:                                #   in Loop: Header=BB1_14 Depth=2
	decq	%rcx
	cmpq	%r10, %rcx
	jg	.LBB1_14
# BB#16:                                #   in Loop: Header=BB1_13 Depth=1
	movapd	%xmm13, %xmm2
.LBB1_17:                               # %._crit_edge
                                        #   in Loop: Header=BB1_13 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	movsd	(%rcx,%r11,8), %xmm7    # xmm7 = mem[0],zero
	minsd	%xmm3, %xmm7
	movapd	%xmm7, %xmm0
	subsd	%xmm2, %xmm0
	movapd	%xmm2, %xmm1
	andpd	%xmm11, %xmm1
	movapd	%xmm7, %xmm3
	andpd	%xmm11, %xmm3
	addsd	%xmm1, %xmm3
	mulsd	%xmm9, %xmm3
	addsd	%xmm14, %xmm3
	ucomisd	%xmm3, %xmm0
	jbe	.LBB1_51
# BB#18:                                # %.lr.ph150
                                        #   in Loop: Header=BB1_13 Depth=1
	movapd	%xmm2, %xmm3
	addsd	%xmm7, %xmm3
	mulsd	%xmm10, %xmm3
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_42
	.p2align	4, 0x90
.LBB1_19:                               # %.lr.ph150.split.us
                                        #   Parent Loop BB1_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_20 Depth 3
	xorl	%ebx, %ebx
	movq	%r14, %rbp
	movq	%r15, %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%r9, %rsi
	movapd	%xmm8, %xmm1
	.p2align	4, 0x90
.LBB1_20:                               # %.lr.ph.i.us
                                        #   Parent Loop BB1_13 Depth=1
                                        #     Parent Loop BB1_19 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	subsd	%xmm3, %xmm0
	ucomisd	%xmm4, %xmm1
	jne	.LBB1_32
	jnp	.LBB1_21
.LBB1_32:                               #   in Loop: Header=BB1_20 Depth=3
	movsd	(%rcx), %xmm5           # xmm5 = mem[0],zero
	divsd	%xmm1, %xmm5
	jmp	.LBB1_33
	.p2align	4, 0x90
.LBB1_21:                               #   in Loop: Header=BB1_20 Depth=3
	movsd	(%rdx), %xmm5           # xmm5 = mem[0],zero
	andpd	%xmm12, %xmm5
	mulsd	%xmm6, %xmm5
.LBB1_33:                               #   in Loop: Header=BB1_20 Depth=3
	subsd	%xmm5, %xmm0
	xorl	%r13d, %r13d
	ucomisd	%xmm0, %xmm4
	seta	%r13b
	addl	%r13d, %ebx
	addq	$8, %rcx
	addq	$8, %rdx
	addq	$8, %rbp
	decq	%rsi
	movapd	%xmm0, %xmm1
	jne	.LBB1_20
# BB#34:                                # %sturm.exit.loopexit.us
                                        #   in Loop: Header=BB1_19 Depth=2
	movslq	%ebx, %rcx
	cmpq	%r11, %rcx
	jge	.LBB1_41
# BB#35:                                #   in Loop: Header=BB1_19 Depth=2
	cmpl	%r12d, %ebx
	jge	.LBB1_36
# BB#38:                                #   in Loop: Header=BB1_19 Depth=2
	movsd	%xmm3, (%rax,%r10,8)
	jmp	.LBB1_39
	.p2align	4, 0x90
.LBB1_41:                               #   in Loop: Header=BB1_19 Depth=2
	movapd	%xmm3, %xmm7
	jmp	.LBB1_40
	.p2align	4, 0x90
.LBB1_36:                               #   in Loop: Header=BB1_19 Depth=2
	movsd	%xmm3, 8(%rax,%rcx,8)
	movq	(%rsp), %rdx            # 8-byte Reload
	movsd	(%rdx,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	ucomisd	%xmm3, %xmm0
	jbe	.LBB1_39
# BB#37:                                #   in Loop: Header=BB1_19 Depth=2
	movsd	%xmm3, (%rdx,%rcx,8)
.LBB1_39:                               #   in Loop: Header=BB1_19 Depth=2
	movapd	%xmm3, %xmm2
.LBB1_40:                               #   in Loop: Header=BB1_19 Depth=2
	incl	%edi
	movapd	%xmm2, %xmm3
	addsd	%xmm7, %xmm3
	mulsd	%xmm10, %xmm3
	movapd	%xmm7, %xmm0
	subsd	%xmm2, %xmm0
	movapd	%xmm2, %xmm1
	andpd	%xmm11, %xmm1
	movapd	%xmm7, %xmm5
	andpd	%xmm11, %xmm5
	addsd	%xmm1, %xmm5
	mulsd	%xmm9, %xmm5
	addsd	%xmm14, %xmm5
	ucomisd	%xmm5, %xmm0
	ja	.LBB1_19
	jmp	.LBB1_50
	.p2align	4, 0x90
.LBB1_42:                               # %.lr.ph150.split
                                        #   Parent Loop BB1_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%r11, %r11
	jle	.LBB1_43
# BB#44:                                #   in Loop: Header=BB1_42 Depth=2
	testl	%r12d, %r12d
	jle	.LBB1_46
# BB#45:                                #   in Loop: Header=BB1_42 Depth=2
	movsd	%xmm3, (%rax,%r10,8)
	jmp	.LBB1_48
	.p2align	4, 0x90
.LBB1_43:                               #   in Loop: Header=BB1_42 Depth=2
	movapd	%xmm3, %xmm7
	jmp	.LBB1_49
	.p2align	4, 0x90
.LBB1_46:                               #   in Loop: Header=BB1_42 Depth=2
	movsd	%xmm3, 8(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm3, %xmm0
	jbe	.LBB1_48
# BB#47:                                #   in Loop: Header=BB1_42 Depth=2
	movsd	%xmm3, (%rcx)
	.p2align	4, 0x90
.LBB1_48:                               #   in Loop: Header=BB1_42 Depth=2
	movapd	%xmm3, %xmm2
.LBB1_49:                               #   in Loop: Header=BB1_42 Depth=2
	incl	%edi
	movapd	%xmm2, %xmm3
	addsd	%xmm7, %xmm3
	mulsd	%xmm10, %xmm3
	movapd	%xmm7, %xmm0
	subsd	%xmm2, %xmm0
	movapd	%xmm2, %xmm1
	andpd	%xmm11, %xmm1
	movapd	%xmm7, %xmm5
	andpd	%xmm11, %xmm5
	addsd	%xmm1, %xmm5
	mulsd	%xmm9, %xmm5
	addsd	%xmm14, %xmm5
	ucomisd	%xmm5, %xmm0
	ja	.LBB1_42
.LBB1_50:                               # %._crit_edge151
                                        #   in Loop: Header=BB1_13 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%edi, (%rcx)
.LBB1_51:                               #   in Loop: Header=BB1_13 Depth=1
	addsd	%xmm7, %xmm2
	mulsd	%xmm10, %xmm2
	movq	(%rsp), %rcx            # 8-byte Reload
	movsd	%xmm2, (%rcx,%r11,8)
	decq	%r8
	cmpq	%r10, %r11
	leaq	-1(%r11), %r11
	movapd	%xmm7, %xmm3
	jg	.LBB1_13
.LBB1_52:                               # %._crit_edge158
	movq	%rax, %rdi
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB1_31:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$39, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	dbisect, .Lfunc_end1-dbisect
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"bisect: Couldn't allocate memory for wu"
	.size	.L.str, 40


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
