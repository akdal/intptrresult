	.text
	.file	"misc.bc"
	.globl	randwc
	.p2align	4, 0x90
	.type	randwc,@function
randwc:                                 # @randwc
	.cfi_startproc
# BB#0:
	movl	randnum.randw.0(%rip), %ecx
	imull	$529562, randnum.randw.1(%rip), %eax # imm = 0x8149A
	imull	$254754, %ecx, %edx     # imm = 0x3E322
	addl	%eax, %edx
	movslq	%edx, %rax
	imulq	$-2042183015, %rax, %rdx # imm = 0x8646C299
	shrq	$32, %rdx
	addl	%eax, %edx
	movl	%edx, %esi
	shrl	$31, %esi
	sarl	$19, %edx
	addl	%esi, %edx
	imull	$999563, %edx, %edx     # imm = 0xF408B
	subl	%edx, %eax
	movl	%ecx, randnum.randw.1(%rip)
	movl	%eax, randnum.randw.0(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	%edi
	movl	%edx, %eax
	retq
.Lfunc_end0:
	.size	randwc, .Lfunc_end0-randwc
	.cfi_endproc

	.globl	randnum
	.p2align	4, 0x90
	.type	randnum,@function
randnum:                                # @randnum
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	je	.LBB1_1
# BB#2:
	movl	$13, randnum.randw.0(%rip)
	movl	$117, randnum.randw.1(%rip)
	movl	$13, %ecx
	movl	$61958754, %eax         # imm = 0x3B16A62
	jmp	.LBB1_3
.LBB1_1:                                # %._crit_edge
	movl	randnum.randw.0(%rip), %ecx
	imull	$529562, randnum.randw.1(%rip), %eax # imm = 0x8149A
.LBB1_3:
	imull	$254754, %ecx, %edx     # imm = 0x3E322
	addl	%eax, %edx
	movslq	%edx, %rax
	imulq	$-2042183015, %rax, %rdx # imm = 0x8646C299
	shrq	$32, %rdx
	addl	%eax, %edx
	movl	%edx, %esi
	shrl	$31, %esi
	sarl	$19, %edx
	addl	%esi, %edx
	imull	$999563, %edx, %edx     # imm = 0xF408B
	subl	%edx, %eax
	movl	%ecx, randnum.randw.1(%rip)
	movl	%eax, randnum.randw.0(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end1:
	.size	randnum, .Lfunc_end1-randnum
	.cfi_endproc

	.globl	abs_randwc
	.p2align	4, 0x90
	.type	abs_randwc,@function
abs_randwc:                             # @abs_randwc
	.cfi_startproc
# BB#0:
	movl	randnum.randw.0(%rip), %ecx
	imull	$529562, randnum.randw.1(%rip), %eax # imm = 0x8149A
	imull	$254754, %ecx, %edx     # imm = 0x3E322
	addl	%eax, %edx
	movslq	%edx, %rax
	imulq	$-2042183015, %rax, %rdx # imm = 0x8646C299
	shrq	$32, %rdx
	addl	%eax, %edx
	movl	%edx, %esi
	shrl	$31, %esi
	sarl	$19, %edx
	addl	%esi, %edx
	imull	$999563, %edx, %edx     # imm = 0xF408B
	subl	%edx, %eax
	movl	%ecx, randnum.randw.1(%rip)
	movl	%eax, randnum.randw.0(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	%edi
	movl	%edx, %eax
	negl	%eax
	cmovll	%edx, %eax
	retq
.Lfunc_end2:
	.size	abs_randwc, .Lfunc_end2-abs_randwc
	.cfi_endproc

	.type	randnum.randw.0,@object # @randnum.randw.0
	.data
	.p2align	2
randnum.randw.0:
	.long	13                      # 0xd
	.size	randnum.randw.0, 4

	.type	randnum.randw.1,@object # @randnum.randw.1
	.p2align	2
randnum.randw.1:
	.long	117                     # 0x75
	.size	randnum.randw.1, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
