	.text
	.file	"decoder.bc"
	.globl	decode_one_b8block
	.p2align	4, 0x90
	.type	decode_one_b8block,@function
decode_one_b8block:                     # @decode_one_b8block
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1256, %rsp             # imm = 0x4E8
.Lcfi6:
	.cfi_def_cfa_offset 1312
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r14d
	movq	img(%rip), %r13
	movl	(%r13), %eax
	decl	%eax
	subl	start_frame_no_in_this_IGOP(%rip), %eax
	cltd
	idivl	28(%r13)
	movl	%r14d, %r9d
	shrl	$31, %r9d
	addl	%r14d, %r9d
	movl	%r9d, %eax
	andl	$-2, %eax
	subl	%eax, %r14d
	leal	(,%r14,8), %r15d
	movl	%r14d, %ebx
	leal	8(,%rbx,8), %r11d
	shll	$2, %r9d
	andl	$-8, %r9d
	leal	8(%r9), %ebp
	movl	20(%r13), %eax
	cmpl	$2, %eax
	jne	.LBB0_4
# BB#1:                                 # %.preheader190
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	decs(%rip), %rcx
	movq	8(%rcx), %rcx
	movslq	%edi, %rsi
	movq	(%rcx,%rsi,8), %rcx
	movslq	%r9d, %r10
	movslq	180(%r13), %r9
	movslq	%ebp, %rdx
	movslq	%r15d, %rdi
	movslq	176(%r13), %rbp
	movslq	%r11d, %rsi
	leaq	(%r9,%r10), %r8
	movq	%r10, %r11
	orq	$1, %r11
	leaq	1(%r9,%r11), %rbx
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r10, %r12
	orq	$3, %r12
	leaq	(%r9,%r12), %rbx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	leaq	1(%r9,%r12), %rbx
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	leaq	2(%r9,%r12), %r15
	leaq	3(%r9,%r12), %r12
	orq	$7, %r10
	addq	%r9, %r10
	leaq	(%r9,%r11), %r13
	addq	%rbp, %rbp
	movq	(%rax,%r8,8), %r14
	addq	%rbp, %r14
	movq	(%rcx,%r8,8), %r8
	addq	%rbp, %r8
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rdx, %r11
	movzwl	(%r14,%rdi,2), %r9d
	movw	%r9w, (%r8,%rdi,2)
	jge	.LBB0_3
# BB#38:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%rsi, %r9
	movq	(%rax,%r13,8), %rsi
	addq	%rbp, %rsi
	movzwl	(%rsi,%rdi,2), %esi
	movq	%rdx, %rbx
	movq	(%rcx,%r13,8), %rdx
	addq	%rbp, %rdx
	movw	%si, (%rdx,%rdi,2)
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzwl	(%rdx,%rdi,2), %edx
	movq	(%rcx,%rsi,8), %rsi
	addq	%rbp, %rsi
	movw	%dx, (%rsi,%rdi,2)
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rax,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzwl	(%rdx,%rdi,2), %edx
	movq	(%rcx,%rsi,8), %rsi
	addq	%rbp, %rsi
	movw	%dx, (%rsi,%rdi,2)
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzwl	(%rdx,%rdi,2), %edx
	movq	(%rcx,%rsi,8), %rsi
	addq	%rbp, %rsi
	movw	%dx, (%rsi,%rdi,2)
	movq	(%rax,%r15,8), %rdx
	addq	%rbp, %rdx
	movzwl	(%rdx,%rdi,2), %edx
	movq	(%rcx,%r15,8), %rsi
	addq	%rbp, %rsi
	movw	%dx, (%rsi,%rdi,2)
	movq	(%rax,%r12,8), %rdx
	addq	%rbp, %rdx
	movzwl	(%rdx,%rdi,2), %edx
	movq	(%rcx,%r12,8), %rsi
	addq	%rbp, %rsi
	movw	%dx, (%rsi,%rdi,2)
	movq	(%rax,%r10,8), %rdx
	addq	%rbp, %rdx
	movzwl	(%rdx,%rdi,2), %edx
	movq	(%rcx,%r10,8), %rsi
	addq	%rbp, %rsi
	movw	%dx, (%rsi,%rdi,2)
	movq	%r9, %rsi
	movq	%rbx, %rdx
.LBB0_3:                                #   in Loop: Header=BB0_2 Depth=1
	incq	%rdi
	cmpq	%rsi, %rdi
	jl	.LBB0_2
	jmp	.LBB0_37
.LBB0_4:
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	movl	%r11d, 24(%rsp)         # 4-byte Spill
	addl	%r14d, %r14d
	leal	2(%rbx,%rbx), %ebp
	movl	%r9d, %r12d
	sarl	$2, %r12d
	leal	2(%r12), %ebx
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	testl	%esi, %esi
	movl	%edx, 32(%rsp)          # 4-byte Spill
	movl	%esi, 48(%rsp)          # 4-byte Spill
	movl	%ebp, 40(%rsp)          # 4-byte Spill
	je	.LBB0_5
.LBB0_14:
	leal	-1(%rcx), %eax
	cmpl	$7, %eax
	movl	%edi, 64(%rsp)          # 4-byte Spill
	movl	%r8d, 56(%rsp)          # 4-byte Spill
	movl	%eax, 76(%rsp)          # 4-byte Spill
	jae	.LBB0_15
# BB#41:                                # %.preheader202
	movl	%r15d, 72(%rsp)         # 4-byte Spill
	movq	14384(%r13), %rax
	movslq	%r8d, %r11
	movslq	%ecx, %rcx
	movslq	%r14d, %rbx
	movslq	%ebp, %rdx
	movslq	%r12d, %r10
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%rbx,8), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%r11,8), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movswl	(%rsi), %ebp
	movq	%r10, %rdi
	shlq	$4, %rdi
	leaq	96(%rsp,%rdi), %r8
	movl	%ebp, (%r8,%rbx,4)
	movslq	8(%rsp), %rbp           # 4-byte Folded Reload
	movswl	2(%rsi), %esi
	movl	%esi, 64(%r8,%rbx,4)
	movq	%rbx, %rsi
	orq	$1, %rsi
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	cmpq	%rdx, %rsi
	jge	.LBB0_43
# BB#42:
	movq	(%rax,%rsi,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%rcx,8), %rax
	movswl	(%rax), %edx
	movl	%edx, (%r8,%rsi,4)
	movswl	2(%rax), %eax
	movl	%eax, 64(%r8,%rsi,4)
.LBB0_43:
	orq	$1, %r10
	cmpq	%rbp, %r10
	movl	72(%rsp), %r15d         # 4-byte Reload
	jge	.LBB0_17
# BB#44:                                # %.preheader201.1
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%rbx,8), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movswl	(%rdx), %ebp
	shlq	$4, %r10
	leaq	96(%rsp,%r10), %rdi
	movl	%ebp, (%rdi,%rbx,4)
	movswl	2(%rdx), %edx
	movl	%edx, 64(%rdi,%rbx,4)
	cmpq	88(%rsp), %rsi          # 8-byte Folded Reload
	jge	.LBB0_17
# BB#45:
	movq	(%rax,%rsi,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%rcx,8), %rax
	movswl	(%rax), %ecx
	movl	%ecx, (%rdi,%rsi,4)
	movswl	2(%rax), %eax
	movl	%eax, 64(%rdi,%rsi,4)
	jmp	.LBB0_17
.LBB0_15:                               # %.preheader203.preheader
	movslq	%r14d, %rdx
	movslq	%ebp, %rcx
	movslq	%r12d, %rdi
	movslq	8(%rsp), %rbp           # 4-byte Folded Reload
	movq	%rdi, %rsi
	shlq	$4, %rsi
	leaq	96(%rsp,%rsi), %rbx
	movl	$0, 64(%rbx,%rdx,4)
	movl	$0, (%rbx,%rdx,4)
	movq	%rdx, %rsi
	orq	$1, %rsi
	cmpq	%rcx, %rsi
	jge	.LBB0_16
# BB#46:
	movl	$0, 64(%rbx,%rsi,4)
	movl	$0, (%rbx,%rsi,4)
.LBB0_16:
	orq	$1, %rdi
	cmpq	%rbp, %rdi
	jge	.LBB0_17
# BB#47:                                # %.preheader203.1
	shlq	$4, %rdi
	leaq	96(%rsp,%rdi), %rdi
	movl	$0, 64(%rdi,%rdx,4)
	movl	$0, (%rdi,%rdx,4)
	cmpq	%rcx, %rsi
	jge	.LBB0_17
# BB#48:
	movl	$0, 64(%rdi,%rsi,4)
	movl	$0, (%rdi,%rsi,4)
.LBB0_17:                               # %.preheader199
	movq	decs(%rip), %rax
	movq	(%rax), %rax
	movslq	%r9d, %r11
	movslq	20(%rsp), %rbx          # 4-byte Folded Reload
	movl	%r15d, %r10d
	movslq	%r15d, %rbp
	movslq	24(%rsp), %r15          # 4-byte Folded Reload
	movq	(%rax,%r11,8), %r8
	movq	%r11, %rcx
	orq	$1, %rcx
	movq	%r11, %rsi
	orq	$3, %rsi
	orq	$7, %r11
	movq	%r11, %rdi
	shlq	$6, %rdi
	leaq	224(%rsp,%rdi), %rdi
	.p2align	4, 0x90
.LBB0_18:                               # %.preheader198
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rbx, %rcx
	movl	(%r8,%rbp,4), %edx
	movl	%edx, -448(%rdi,%rbp,4)
	jge	.LBB0_19
# BB#40:                                #   in Loop: Header=BB0_18 Depth=1
	movq	(%rax,%rcx,8), %rdx
	movl	(%rdx,%rbp,4), %edx
	movl	%edx, -384(%rdi,%rbp,4)
	movq	8(%rax,%rcx,8), %rdx
	movl	(%rdx,%rbp,4), %edx
	movl	%edx, -320(%rdi,%rbp,4)
	movq	(%rax,%rsi,8), %rdx
	movl	(%rdx,%rbp,4), %edx
	movl	%edx, -256(%rdi,%rbp,4)
	movq	8(%rax,%rsi,8), %rdx
	movl	(%rdx,%rbp,4), %edx
	movl	%edx, -192(%rdi,%rbp,4)
	movq	16(%rax,%rsi,8), %rdx
	movl	(%rdx,%rbp,4), %edx
	movl	%edx, -128(%rdi,%rbp,4)
	movq	24(%rax,%rsi,8), %rdx
	movl	(%rdx,%rbp,4), %edx
	movl	%edx, -64(%rdi,%rbp,4)
	movq	(%rax,%r11,8), %rdx
	movl	(%rdx,%rbp,4), %edx
	movl	%edx, (%rdi,%rbp,4)
.LBB0_19:                               #   in Loop: Header=BB0_18 Depth=1
	incq	%rbp
	cmpq	%r15, %rbp
	jl	.LBB0_18
# BB#20:
	movl	%r10d, %esi
	movl	64(%rsp), %edi          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	48(%rsp), %eax          # 4-byte Reload
	movl	40(%rsp), %ecx          # 4-byte Reload
	movl	76(%rsp), %edx          # 4-byte Reload
	cmpl	$7, %edx
	jae	.LBB0_22
.LBB0_29:                               # %.preheader193
	movl	$-2, %eax
	subl	%r8d, %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movslq	%edi, %r15
	movslq	%r14d, %rdx
	movslq	%ecx, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	%r12d, %rax
	movslq	8(%rsp), %rcx           # 4-byte Folded Reload
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rax, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	shlq	$4, %rax
	leaq	96(%rsp,%rax), %rax
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	leaq	64(%rax,%rdx,4), %rdx
	shll	$2, %r12d
	movslq	%r12d, %rax
	shlq	$6, %rax
	movslq	%esi, %rcx
	leaq	224(%rsp,%rax), %rax
	leaq	204(%rax,%rcx,4), %r14
	.p2align	4, 0x90
.LBB0_30:                               # %.preheader192
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_31 Depth 2
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rdx, %r12
	movq	64(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_31:                               #   Parent Loop BB0_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	img(%rip), %rcx
	movl	168(%rcx), %ebp
	addl	%r13d, %ebp
	movl	172(%rcx), %ebx
	addl	8(%rsp), %ebx           # 4-byte Folded Reload
	cmpl	$1, 20(%rcx)
	jne	.LBB0_32
# BB#33:                                #   in Loop: Header=BB0_31 Depth=2
	movq	enc_picture(%rip), %rax
	cmpq	enc_frame_picture(%rip), %rax
	movl	32(%rsp), %edx          # 4-byte Reload
	je	.LBB0_35
# BB#34:                                #   in Loop: Header=BB0_31 Depth=2
	movl	(%rcx), %eax
	addl	20(%rsp), %eax          # 4-byte Folded Reload
	subl	start_frame_no_in_this_IGOP(%rip), %eax
	cltd
	idivl	28(%rcx)
	jmp	.LBB0_35
	.p2align	4, 0x90
.LBB0_32:                               #   in Loop: Header=BB0_31 Depth=2
	movl	32(%rsp), %edx          # 4-byte Reload
.LBB0_35:                               # %.preheader191
                                        #   in Loop: Header=BB0_31 Depth=2
	movq	decs(%rip), %rax
	movq	16(%rax), %rcx
	movq	32(%rax), %r9
	movq	(%rcx,%r15,8), %rax
	movl	%edx, 32(%rsp)          # 4-byte Spill
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rdi
	movl	-64(%r12), %ecx
	movl	(%r12), %r8d
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	Get_Reference_Block
	movq	decs(%rip), %rax
	movq	8(%rax), %rdx
	movq	32(%rax), %rcx
	movq	(%rdx,%r15,8), %rdx
	shll	$2, %ebx
	shll	$2, %ebp
	movslq	%ebp, %rax
	movslq	%ebx, %rsi
	movq	(%rcx), %rdi
	movq	(%rdx,%rsi,8), %rbp
	movzwl	(%rdi), %ebx
	addl	-204(%r14), %ebx
	movw	%bx, (%rbp,%rax,2)
	movzwl	2(%rdi), %ebx
	addl	-200(%r14), %ebx
	movw	%bx, 2(%rbp,%rax,2)
	movzwl	4(%rdi), %ebx
	addl	-196(%r14), %ebx
	movw	%bx, 4(%rbp,%rax,2)
	movzwl	6(%rdi), %edi
	addl	-192(%r14), %edi
	movw	%di, 6(%rbp,%rax,2)
	movq	8(%rcx), %rdi
	movq	8(%rdx,%rsi,8), %rbp
	movzwl	(%rdi), %ebx
	addl	-140(%r14), %ebx
	movw	%bx, (%rbp,%rax,2)
	movzwl	2(%rdi), %ebx
	addl	-136(%r14), %ebx
	movw	%bx, 2(%rbp,%rax,2)
	movzwl	4(%rdi), %ebx
	addl	-132(%r14), %ebx
	movw	%bx, 4(%rbp,%rax,2)
	movzwl	6(%rdi), %edi
	addl	-128(%r14), %edi
	movw	%di, 6(%rbp,%rax,2)
	movq	16(%rcx), %rdi
	movq	16(%rdx,%rsi,8), %rbp
	movzwl	(%rdi), %ebx
	addl	-76(%r14), %ebx
	movw	%bx, (%rbp,%rax,2)
	movzwl	2(%rdi), %ebx
	addl	-72(%r14), %ebx
	movw	%bx, 2(%rbp,%rax,2)
	movzwl	4(%rdi), %ebx
	addl	-68(%r14), %ebx
	movw	%bx, 4(%rbp,%rax,2)
	movzwl	6(%rdi), %edi
	addl	-64(%r14), %edi
	movw	%di, 6(%rbp,%rax,2)
	movq	24(%rcx), %rcx
	movq	24(%rdx,%rsi,8), %rdx
	movzwl	(%rcx), %esi
	addl	-12(%r14), %esi
	movw	%si, (%rdx,%rax,2)
	movzwl	2(%rcx), %esi
	addl	-8(%r14), %esi
	movw	%si, 2(%rdx,%rax,2)
	movzwl	4(%rcx), %esi
	addl	-4(%r14), %esi
	movw	%si, 4(%rdx,%rax,2)
	movzwl	6(%rcx), %ecx
	addl	(%r14), %ecx
	movw	%cx, 6(%rdx,%rax,2)
	incq	%r13
	addq	$4, %r12
	addq	$16, %r14
	cmpq	24(%rsp), %r13          # 8-byte Folded Reload
	jl	.LBB0_31
# BB#36:                                #   in Loop: Header=BB0_30 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	addq	$16, %rdx
	movq	48(%rsp), %r14          # 8-byte Reload
	addq	$256, %r14              # imm = 0x100
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	56(%rsp), %rcx          # 8-byte Folded Reload
	jl	.LBB0_30
	jmp	.LBB0_37
.LBB0_5:
	testl	%eax, %eax
	je	.LBB0_8
# BB#6:
	cmpl	$1, %eax
	jne	.LBB0_14
# BB#7:
	cmpl	$0, 15360(%r13)
	jle	.LBB0_14
.LBB0_8:                                # %.preheader209.preheader
	movslq	%r9d, %rbx
	movslq	20(%rsp), %r10          # 4-byte Folded Reload
	movslq	%r15d, %rax
	movslq	24(%rsp), %rdx          # 4-byte Folded Reload
	movq	%rbx, %rbp
	orq	$1, %rbp
	orq	$7, %rbx
	shlq	$6, %rbx
	leaq	224(%rsp,%rbx), %rbx
	.p2align	4, 0x90
.LBB0_9:                                # %.preheader209
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r10, %rbp
	movl	$0, -448(%rbx,%rax,4)
	jge	.LBB0_10
# BB#52:                                #   in Loop: Header=BB0_9 Depth=1
	movl	$0, -384(%rbx,%rax,4)
	movl	$0, -320(%rbx,%rax,4)
	movl	$0, -256(%rbx,%rax,4)
	movl	$0, -192(%rbx,%rax,4)
	movl	$0, -128(%rbx,%rax,4)
	movl	$0, -64(%rbx,%rax,4)
	movl	$0, (%rbx,%rax,4)
.LBB0_10:                               #   in Loop: Header=BB0_9 Depth=1
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB0_9
# BB#11:                                # %.preheader206.preheader
	movslq	%r14d, %rdx
	movslq	40(%rsp), %r11          # 4-byte Folded Reload
	movslq	%r12d, %rbp
	movslq	8(%rsp), %rbx           # 4-byte Folded Reload
	movq	%rbp, %rsi
	shlq	$4, %rsi
	leaq	96(%rsp,%rsi), %rsi
	movl	$0, 64(%rsi,%rdx,4)
	movl	$0, (%rsi,%rdx,4)
	movq	%rdx, %rax
	orq	$1, %rax
	cmpq	%r11, %rax
	jge	.LBB0_12
# BB#49:
	movl	$0, 64(%rsi,%rax,4)
	movl	$0, (%rsi,%rax,4)
.LBB0_12:
	orq	$1, %rbp
	cmpq	%rbx, %rbp
	jge	.LBB0_13
# BB#50:                                # %.preheader206.1
	shlq	$4, %rbp
	leaq	96(%rsp,%rbp), %rsi
	movl	$0, 64(%rsi,%rdx,4)
	movl	$0, (%rsi,%rdx,4)
	cmpq	%r11, %rax
	jge	.LBB0_13
# BB#51:
	movl	$0, 64(%rsi,%rax,4)
	movl	$0, (%rsi,%rax,4)
.LBB0_13:                               # %.loopexit200.loopexit234
	decl	%ecx
	movl	%ecx, %edx
	movl	%r15d, %esi
	movl	48(%rsp), %eax          # 4-byte Reload
	movl	40(%rsp), %ecx          # 4-byte Reload
	cmpl	$7, %edx
	jb	.LBB0_29
.LBB0_22:
	testl	%eax, %eax
	je	.LBB0_26
.LBB0_23:                               # %.preheader196
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	decs(%rip), %rcx
	movq	8(%rcx), %rcx
	movslq	%edi, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	%r9d, %r10
	movslq	180(%r13), %r9
	movslq	20(%rsp), %rdx          # 4-byte Folded Reload
	movslq	%esi, %rdi
	movslq	176(%r13), %rbp
	movslq	24(%rsp), %rsi          # 4-byte Folded Reload
	leaq	(%r9,%r10), %r8
	movq	%r10, %r11
	orq	$1, %r11
	leaq	1(%r9,%r11), %rbx
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r10, %r12
	orq	$3, %r12
	leaq	(%r9,%r12), %rbx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	leaq	1(%r9,%r12), %rbx
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	leaq	2(%r9,%r12), %r15
	leaq	3(%r9,%r12), %r12
	orq	$7, %r10
	addq	%r9, %r10
	leaq	(%r9,%r11), %r13
	addq	%rbp, %rbp
	movq	(%rax,%r8,8), %r14
	addq	%rbp, %r14
	movq	(%rcx,%r8,8), %r8
	addq	%rbp, %r8
	.p2align	4, 0x90
.LBB0_24:                               # %.preheader195
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rdx, %r11
	movzwl	(%r14,%rdi,2), %r9d
	movw	%r9w, (%r8,%rdi,2)
	jge	.LBB0_25
# BB#39:                                #   in Loop: Header=BB0_24 Depth=1
	movq	%rsi, %r9
	movq	(%rax,%r13,8), %rsi
	addq	%rbp, %rsi
	movzwl	(%rsi,%rdi,2), %esi
	movq	%rdx, %rbx
	movq	(%rcx,%r13,8), %rdx
	addq	%rbp, %rdx
	movw	%si, (%rdx,%rdi,2)
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzwl	(%rdx,%rdi,2), %edx
	movq	(%rcx,%rsi,8), %rsi
	addq	%rbp, %rsi
	movw	%dx, (%rsi,%rdi,2)
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rax,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzwl	(%rdx,%rdi,2), %edx
	movq	(%rcx,%rsi,8), %rsi
	addq	%rbp, %rsi
	movw	%dx, (%rsi,%rdi,2)
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rdx
	addq	%rbp, %rdx
	movzwl	(%rdx,%rdi,2), %edx
	movq	(%rcx,%rsi,8), %rsi
	addq	%rbp, %rsi
	movw	%dx, (%rsi,%rdi,2)
	movq	(%rax,%r15,8), %rdx
	addq	%rbp, %rdx
	movzwl	(%rdx,%rdi,2), %edx
	movq	(%rcx,%r15,8), %rsi
	addq	%rbp, %rsi
	movw	%dx, (%rsi,%rdi,2)
	movq	(%rax,%r12,8), %rdx
	addq	%rbp, %rdx
	movzwl	(%rdx,%rdi,2), %edx
	movq	(%rcx,%r12,8), %rsi
	addq	%rbp, %rsi
	movw	%dx, (%rsi,%rdi,2)
	movq	(%rax,%r10,8), %rdx
	addq	%rbp, %rdx
	movzwl	(%rdx,%rdi,2), %edx
	movq	(%rcx,%r10,8), %rsi
	addq	%rbp, %rsi
	movw	%dx, (%rsi,%rdi,2)
	movq	%r9, %rsi
	movq	%rbx, %rdx
.LBB0_25:                               #   in Loop: Header=BB0_24 Depth=1
	incq	%rdi
	cmpq	%rsi, %rdi
	jl	.LBB0_24
.LBB0_37:                               # %.loopexit
	addq	$1256, %rsp             # imm = 0x4E8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_26:
	movl	20(%r13), %eax
	testl	%eax, %eax
	je	.LBB0_29
# BB#27:
	cmpl	$1, %eax
	jne	.LBB0_23
# BB#28:
	cmpl	$0, 15360(%r13)
	jg	.LBB0_29
	jmp	.LBB0_23
.Lfunc_end0:
	.size	decode_one_b8block, .Lfunc_end0-decode_one_b8block
	.cfi_endproc

	.globl	Get_Reference_Block
	.p2align	4, 0x90
	.type	Get_Reference_Block,@function
Get_Reference_Block:                    # @Get_Reference_Block
	.cfi_startproc
# BB#0:                                 # %.preheader
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 96
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	%ecx, %r15d
	movl	%edx, %ebp
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	shll	$4, %esi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	leal	(%rsi,%r8), %r13d
	shll	$4, %ebp
	leal	(%rbp,%r15), %edx
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%r13d, %esi
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movq	(%rbx), %r14
	movw	%ax, (%r14)
	leal	4(%rbp,%r15), %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movq	%r12, %rdi
	movl	%r13d, %esi
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movw	%ax, 2(%r14)
	leal	8(%rbp,%r15), %edx
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movq	%r12, %rdi
	movl	%r13d, %esi
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movw	%ax, 4(%r14)
	leal	12(%rbp,%r15), %edx
	movl	%edx, (%rsp)            # 4-byte Spill
	movq	%r12, %rdi
	movl	%r13d, %esi
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movw	%ax, 6(%r14)
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	leal	4(%r13,%r15), %ebp
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	4(%rsp), %edx           # 4-byte Reload
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movq	%rbx, %r14
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	8(%r14), %rbx
	movw	%ax, (%rbx)
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movw	%ax, 2(%rbx)
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	8(%rsp), %edx           # 4-byte Reload
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movw	%ax, 4(%rbx)
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	(%rsp), %edx            # 4-byte Reload
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movw	%ax, 6(%rbx)
	leal	8(%r13,%r15), %ebp
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	4(%rsp), %r15d          # 4-byte Reload
	movl	%r15d, %edx
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movq	16(%r14), %rbx
	movw	%ax, (%rbx)
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	12(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, %edx
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movw	%ax, 2(%rbx)
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	8(%rsp), %r13d          # 4-byte Reload
	movl	%r13d, %edx
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movw	%ax, 4(%rbx)
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	(%rsp), %edx            # 4-byte Reload
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movw	%ax, 6(%rbx)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	leal	12(%rcx,%rax), %ebp
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	%r15d, %edx
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	24(%rcx), %rbx
	movw	%ax, (%rbx)
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	%r14d, %edx
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movw	%ax, 2(%rbx)
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	%r13d, %edx
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movw	%ax, 4(%rbx)
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	(%rsp), %edx            # 4-byte Reload
	callq	Get_Reference_Pixel
	movzbl	%al, %eax
	movw	%ax, 6(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	Get_Reference_Block, .Lfunc_end1-Get_Reference_Block
	.cfi_endproc

	.globl	decode_one_mb
	.p2align	4, 0x90
	.type	decode_one_mb,@function
decode_one_mb:                          # @decode_one_mb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	72(%rbx), %esi
	movl	376(%rbx), %ecx
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	(%rax), %rax
	movq	img(%rip), %rdx
	movslq	172(%rdx), %rdi
	movq	(%rax,%rdi,8), %rax
	movslq	168(%rdx), %rdx
	movsbl	(%rax,%rdx), %r8d
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	decode_one_b8block
	movl	72(%rbx), %esi
	movl	380(%rbx), %ecx
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	(%rax), %rax
	movq	img(%rip), %rdx
	movslq	172(%rdx), %rdi
	movq	(%rax,%rdi,8), %rax
	movslq	168(%rdx), %rdx
	movsbl	2(%rax,%rdx), %r8d
	movl	$1, %edx
	movl	%ebp, %edi
	callq	decode_one_b8block
	movl	72(%rbx), %esi
	movl	384(%rbx), %ecx
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	(%rax), %rax
	movq	img(%rip), %rdx
	movslq	172(%rdx), %rdi
	movq	16(%rax,%rdi,8), %rax
	movslq	168(%rdx), %rdx
	movsbl	(%rax,%rdx), %r8d
	movl	$2, %edx
	movl	%ebp, %edi
	callq	decode_one_b8block
	movl	72(%rbx), %esi
	movl	388(%rbx), %ecx
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	(%rax), %rax
	movq	img(%rip), %rdx
	movslq	172(%rdx), %rdi
	movq	16(%rax,%rdi,8), %rax
	movslq	168(%rdx), %rdx
	movsbl	2(%rax,%rdx), %r8d
	movl	$3, %edx
	movl	%ebp, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	decode_one_b8block      # TAILCALL
.Lfunc_end2:
	.size	decode_one_mb, .Lfunc_end2-decode_one_mb
	.cfi_endproc

	.globl	Get_Reference_Pixel
	.p2align	4, 0x90
	.type	Get_Reference_Pixel,@function
Get_Reference_Pixel:                    # @Get_Reference_Pixel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
.Lcfi37:
	.cfi_offset %rbx, -56
.Lcfi38:
	.cfi_offset %r12, -48
.Lcfi39:
	.cfi_offset %r13, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movl	%edx, %r10d
	andl	$3, %r10d
	movl	%esi, %ebp
	andl	$3, %ebp
	movl	%edx, %r15d
	subl	%r10d, %r15d
	movl	%r15d, %r11d
	sarl	$31, %r11d
	shrl	$30, %r11d
	addl	%r15d, %r11d
	sarl	$2, %r11d
	movl	%esi, %r13d
	subl	%ebp, %r13d
	movl	%r13d, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%r13d, %ecx
	sarl	$2, %ecx
	movq	img(%rip), %r14
	movl	52(%r14), %ebx
	movl	68(%r14), %r9d
	decl	%ebx
	decl	%r9d
	movl	%edx, %eax
	orl	%esi, %eax
	testb	$3, %al
	je	.LBB3_9
# BB#1:
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	testl	%ebp, %ebp
	je	.LBB3_10
# BB#2:
	andb	$3, %dl
	je	.LBB3_13
# BB#3:
	cmpb	$2, %dl
	jne	.LBB3_16
# BB#4:                                 # %.preheader189.preheader
	movl	%esi, -16(%rsp)         # 4-byte Spill
	movq	%r14, -8(%rsp)          # 8-byte Spill
	leal	-2(%r11), %eax
	xorl	%edx, %edx
	cmpl	$11, %r15d
	cmovlel	%edx, %eax
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	movslq	%eax, %r12
	movslq	%ecx, %r13
	leal	-1(%r11), %eax
	cmpl	$7, %r15d
	cmovlel	%edx, %eax
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	movslq	%eax, %r10
	testl	%r11d, %r11d
	movl	$0, %eax
	cmovnsl	%r11d, %eax
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	movslq	%eax, %r8
	leal	1(%r11), %ecx
	cmpl	$-4, %r15d
	cmovlel	%edx, %ecx
	cmpl	%ebx, %ecx
	cmovgl	%ebx, %ecx
	movslq	%ecx, %r14
	leal	2(%r11), %ecx
	cmpl	$-8, %r15d
	cmovlel	%edx, %ecx
	cmpl	%ebx, %ecx
	cmovgl	%ebx, %ecx
	movslq	%ecx, %rdx
	addl	$3, %r11d
	cmpl	$-12, %r15d
	movl	$0, %r15d
	cmovlel	%r15d, %r11d
	cmpl	%ebx, %r11d
	cmovgl	%ebx, %r11d
	movslq	%r11d, %rcx
	addq	$-2, %r13
	xorl	%ebx, %ebx
	movq	-24(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB3_5:                                # %.preheader189
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%r13,%rbx), %rax
	testq	%rax, %rax
	cmovlel	%r15d, %eax
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	cltq
	movq	(%rdi,%rax,8), %rax
	movzwl	(%rax,%r12,2), %esi
	movzwl	(%rax,%r10,2), %ebp
	imull	$-5, %ebp, %ebp
	addl	%esi, %ebp
	movzwl	(%rax,%r8,2), %esi
	leal	(%rsi,%rsi,4), %esi
	leal	(%rbp,%rsi,4), %esi
	movzwl	(%rax,%r14,2), %ebp
	leal	(%rbp,%rbp,4), %ebp
	leal	(%rsi,%rbp,4), %esi
	movzwl	(%rax,%rdx,2), %ebp
	imull	$-5, %ebp, %ebp
	addl	%esi, %ebp
	movzwl	(%rax,%rcx,2), %eax
	addl	%ebp, %eax
	movl	%eax, -56(%rsp,%rbx,4)
	incq	%rbx
	cmpq	$6, %rbx
	jne	.LBB3_5
# BB#6:                                 # %.preheader188.preheader
	imull	$-5, -52(%rsp), %eax
	addl	-56(%rsp), %eax
	movl	-48(%rsp), %edx
	movl	-44(%rsp), %edi
	leal	(%rdx,%rdx,4), %ecx
	leal	(%rax,%rcx,4), %eax
	leal	(%rdi,%rdi,4), %ecx
	leal	(%rax,%rcx,4), %eax
	imull	$-5, -40(%rsp), %esi
	addl	%eax, %esi
	movl	-36(%rsp), %eax
	leal	(%rax,%rsi), %ebp
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movl	15520(%rcx), %ecx
	leal	512(%rax,%rsi), %eax
	sarl	$31, %eax
	shrl	$22, %eax
	leal	512(%rax,%rbp), %eax
	xorl	%ebp, %ebp
	sarl	$10, %eax
	cmovsl	%ebp, %eax
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	movl	-16(%rsp), %esi         # 4-byte Reload
	andb	$3, %sil
	cmpb	$3, %sil
	je	.LBB3_22
# BB#7:                                 # %.preheader188.preheader
	cmpb	$1, %sil
	jne	.LBB3_31
# BB#8:
	leal	16(%rdx), %esi
	sarl	$31, %esi
	shrl	$27, %esi
	leal	16(%rdx,%rsi), %edx
	xorl	%esi, %esi
	sarl	$5, %edx
	cmovsl	%esi, %edx
	cmpl	%ecx, %edx
	cmovgl	%ecx, %edx
	addl	%eax, %edx
	jmp	.LBB3_29
.LBB3_9:
	xorl	%eax, %eax
	testl	%ecx, %ecx
	cmovsl	%eax, %ecx
	cmpl	%r9d, %ecx
	cmovgl	%r9d, %ecx
	movslq	%ecx, %rcx
	movq	(%rdi,%rcx,8), %rcx
	testl	%r11d, %r11d
	cmovnsl	%r11d, %eax
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	cltq
	movzwl	(%rcx,%rax,2), %eax
	jmp	.LBB3_31
.LBB3_10:
	xorl	%esi, %esi
	testl	%ecx, %ecx
	cmovsl	%esi, %ecx
	cmpl	%r9d, %ecx
	cmovgl	%r9d, %ecx
	movslq	%ecx, %rax
	movq	-24(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %r8
	leal	-2(%r11), %eax
	cmpl	$11, %r15d
	cmovlel	%esi, %eax
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	cltq
	movzwl	(%r8,%rax,2), %eax
	leal	-1(%r11), %edi
	cmpl	$7, %r15d
	cmovlel	%esi, %edi
	cmpl	%ebx, %edi
	cmovgl	%ebx, %edi
	movslq	%edi, %rdi
	movzwl	(%r8,%rdi,2), %edi
	imull	$-5, %edi, %edi
	addl	%eax, %edi
	testl	%r11d, %r11d
	movl	$0, %eax
	cmovnsl	%r11d, %eax
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	cltq
	movzwl	(%r8,%rax,2), %eax
	leal	(%rax,%rax,4), %eax
	leal	(%rdi,%rax,4), %eax
	leal	1(%r11), %r9d
	cmpl	$-4, %r15d
	movl	$0, %ebp
	cmovgl	%r9d, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movslq	%ebp, %rbp
	movzwl	(%r8,%rbp,2), %ebp
	leal	(%rbp,%rbp,4), %ebp
	leal	(%rax,%rbp,4), %eax
	leal	2(%r11), %ebp
	cmpl	$-8, %r15d
	cmovlel	%esi, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movslq	%ebp, %rbp
	movzwl	(%r8,%rbp,2), %ebp
	imull	$-5, %ebp, %ebp
	addl	%eax, %ebp
	leal	3(%r11), %eax
	cmpl	$-12, %r15d
	cmovlel	%esi, %eax
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	cltq
	movzwl	(%r8,%rax,2), %eax
	leal	(%rax,%rbp), %ecx
	movl	15520(%r14), %edi
	leal	16(%rax,%rbp), %eax
	sarl	$31, %eax
	shrl	$27, %eax
	leal	16(%rax,%rcx), %eax
	sarl	$5, %eax
	cmovsl	%esi, %eax
	cmpl	%edi, %eax
	cmovgl	%edi, %eax
	andb	$3, %dl
	cmpb	$3, %dl
	je	.LBB3_19
# BB#11:
	cmpb	$1, %dl
	jne	.LBB3_31
# BB#12:
	xorl	%edx, %edx
	testl	%r11d, %r11d
	cmovnsl	%r11d, %edx
	cmpl	%ebx, %edx
	cmovgl	%ebx, %edx
	movslq	%edx, %rdx
	jmp	.LBB3_20
.LBB3_13:
	xorl	%ebp, %ebp
	testl	%r11d, %r11d
	cmovsl	%ebp, %r11d
	cmpl	%ebx, %r11d
	cmovgl	%ebx, %r11d
	movslq	%r11d, %r10
	leal	-2(%rcx), %eax
	cmpl	$11, %r13d
	cmovlel	%ebp, %eax
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	cltq
	movq	-24(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi,%rax,8), %rax
	movzwl	(%rax,%r10,2), %eax
	leal	-1(%rcx), %edx
	cmpl	$7, %r13d
	cmovlel	%ebp, %edx
	cmpl	%r9d, %edx
	cmovgl	%r9d, %edx
	movslq	%edx, %rdx
	movq	(%rdi,%rdx,8), %rdx
	movzwl	(%rdx,%r10,2), %edx
	imull	$-5, %edx, %edx
	addl	%eax, %edx
	testl	%ecx, %ecx
	movl	$0, %eax
	cmovnsl	%ecx, %eax
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	cltq
	movq	(%rdi,%rax,8), %rax
	movzwl	(%rax,%r10,2), %eax
	leal	(%rax,%rax,4), %eax
	leal	(%rdx,%rax,4), %eax
	leal	1(%rcx), %r8d
	cmpl	$-4, %r13d
	movl	$0, %edx
	cmovgl	%r8d, %edx
	cmpl	%r9d, %edx
	cmovgl	%r9d, %edx
	movslq	%edx, %rdx
	movq	(%rdi,%rdx,8), %rdx
	movzwl	(%rdx,%r10,2), %edx
	leal	(%rdx,%rdx,4), %edx
	leal	(%rax,%rdx,4), %eax
	leal	2(%rcx), %edx
	cmpl	$-8, %r13d
	cmovlel	%ebp, %edx
	cmpl	%r9d, %edx
	cmovgl	%r9d, %edx
	movslq	%edx, %rdx
	movq	(%rdi,%rdx,8), %rdx
	movzwl	(%rdx,%r10,2), %edx
	imull	$-5, %edx, %edx
	addl	%eax, %edx
	leal	3(%rcx), %eax
	cmpl	$-12, %r13d
	cmovlel	%ebp, %eax
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	cltq
	movq	(%rdi,%rax,8), %rax
	movzwl	(%rax,%r10,2), %eax
	leal	(%rax,%rdx), %r11d
	movl	15520(%r14), %ebx
	leal	16(%rax,%rdx), %eax
	sarl	$31, %eax
	shrl	$27, %eax
	leal	16(%rax,%r11), %eax
	sarl	$5, %eax
	cmovsl	%ebp, %eax
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	andb	$3, %sil
	cmpb	$3, %sil
	je	.LBB3_23
# BB#14:
	cmpb	$1, %sil
	jne	.LBB3_31
# BB#15:
	xorl	%edx, %edx
	testl	%ecx, %ecx
	cmovnsl	%ecx, %edx
	cmpl	%r9d, %edx
	cmovgl	%r9d, %edx
	movslq	%edx, %rcx
	jmp	.LBB3_24
.LBB3_16:
	cmpl	$2, %ebp
	jne	.LBB3_21
# BB#17:                                # %.preheader187
	leal	-2(%rcx), %eax
	xorl	%edx, %edx
	cmpl	$11, %r13d
	cmovlel	%edx, %eax
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	cltq
	movslq	%r11d, %rbp
	movq	%r14, -8(%rsp)          # 8-byte Spill
	movq	-24(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi,%rax,8), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	leal	-1(%rcx), %eax
	cmpl	$7, %r13d
	cmovlel	%edx, %eax
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	cltq
	movq	(%rdi,%rax,8), %r15
	testl	%ecx, %ecx
	movl	$0, %eax
	cmovnsl	%ecx, %eax
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	cltq
	movq	(%rdi,%rax,8), %r12
	leal	1(%rcx), %eax
	cmpl	$-4, %r13d
	cmovlel	%edx, %eax
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	cltq
	movq	(%rdi,%rax,8), %r14
	leal	2(%rcx), %eax
	cmpl	$-8, %r13d
	cmovlel	%edx, %eax
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	cltq
	movq	(%rdi,%rax,8), %r8
	addl	$3, %ecx
	cmpl	$-12, %r13d
	cmovlel	%edx, %ecx
	cmpl	%r9d, %ecx
	cmovgl	%r9d, %ecx
	movslq	%ecx, %rcx
	movq	(%rdi,%rcx,8), %r9
	leaq	-2(%rbp), %rsi
	testq	%rsi, %rsi
	cmovlel	%edx, %esi
	cmpl	%ebx, %esi
	cmovgl	%ebx, %esi
	movslq	%esi, %rsi
	movq	-16(%rsp), %rcx         # 8-byte Reload
	movzwl	(%rcx,%rsi,2), %edi
	movzwl	(%r15,%rsi,2), %eax
	imull	$-5, %eax, %eax
	addl	%edi, %eax
	movzwl	(%r12,%rsi,2), %edi
	leal	(%rdi,%rdi,4), %edi
	leal	(%rax,%rdi,4), %eax
	movzwl	(%r14,%rsi,2), %edi
	leal	(%rdi,%rdi,4), %edi
	leal	(%rax,%rdi,4), %eax
	movzwl	(%r8,%rsi,2), %edi
	imull	$-5, %edi, %edi
	addl	%eax, %edi
	movzwl	(%r9,%rsi,2), %eax
	addl	%edi, %eax
	movl	%eax, -56(%rsp)
	leaq	-1(%rbp), %rax
	testq	%rax, %rax
	cmovlel	%edx, %eax
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	cltq
	movzwl	(%rcx,%rax,2), %esi
	movzwl	(%r15,%rax,2), %edi
	imull	$-5, %edi, %edi
	addl	%esi, %edi
	movzwl	(%r12,%rax,2), %esi
	leal	(%rsi,%rsi,4), %esi
	leal	(%rdi,%rsi,4), %esi
	movzwl	(%r14,%rax,2), %edi
	leal	(%rdi,%rdi,4), %edi
	leal	(%rsi,%rdi,4), %esi
	movzwl	(%r8,%rax,2), %edi
	imull	$-5, %edi, %edi
	addl	%esi, %edi
	movzwl	(%r9,%rax,2), %eax
	addl	%edi, %eax
	movl	%eax, -52(%rsp)
	testq	%rbp, %rbp
	cmovlel	%edx, %r11d
	cmpl	%ebx, %r11d
	cmovgl	%ebx, %r11d
	movslq	%r11d, %rax
	movzwl	(%rcx,%rax,2), %esi
	movzwl	(%r15,%rax,2), %edi
	imull	$-5, %edi, %edi
	addl	%esi, %edi
	movzwl	(%r12,%rax,2), %esi
	leal	(%rsi,%rsi,4), %esi
	leal	(%rdi,%rsi,4), %esi
	movzwl	(%r14,%rax,2), %edi
	leal	(%rdi,%rdi,4), %edi
	leal	(%rsi,%rdi,4), %esi
	movzwl	(%r8,%rax,2), %edi
	imull	$-5, %edi, %edi
	addl	%esi, %edi
	movzwl	(%r9,%rax,2), %eax
	addl	%edi, %eax
	movl	%eax, -48(%rsp)
	leaq	1(%rbp), %rax
	testq	%rax, %rax
	cmovlel	%edx, %eax
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	cltq
	movzwl	(%rcx,%rax,2), %esi
	movzwl	(%r15,%rax,2), %edi
	imull	$-5, %edi, %edi
	addl	%esi, %edi
	movzwl	(%r12,%rax,2), %esi
	leal	(%rsi,%rsi,4), %esi
	leal	(%rdi,%rsi,4), %esi
	movzwl	(%r14,%rax,2), %edi
	leal	(%rdi,%rdi,4), %edi
	leal	(%rsi,%rdi,4), %esi
	movzwl	(%r8,%rax,2), %edi
	imull	$-5, %edi, %edi
	addl	%esi, %edi
	movzwl	(%r9,%rax,2), %eax
	addl	%edi, %eax
	movl	%eax, -44(%rsp)
	leaq	2(%rbp), %rax
	testq	%rax, %rax
	cmovlel	%edx, %eax
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	cltq
	movzwl	(%rcx,%rax,2), %esi
	movzwl	(%r15,%rax,2), %edi
	imull	$-5, %edi, %edi
	addl	%esi, %edi
	movzwl	(%r12,%rax,2), %esi
	leal	(%rsi,%rsi,4), %esi
	leal	(%rdi,%rsi,4), %esi
	movzwl	(%r14,%rax,2), %edi
	leal	(%rdi,%rdi,4), %edi
	leal	(%rsi,%rdi,4), %esi
	movzwl	(%r8,%rax,2), %edi
	imull	$-5, %edi, %edi
	addl	%esi, %edi
	movzwl	(%r9,%rax,2), %eax
	addl	%edi, %eax
	movl	%eax, -40(%rsp)
	addq	$3, %rbp
	testq	%rbp, %rbp
	cmovlel	%edx, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movslq	%ebp, %rax
	movzwl	(%rcx,%rax,2), %esi
	movzwl	(%r15,%rax,2), %edi
	imull	$-5, %edi, %edi
	addl	%esi, %edi
	movzwl	(%r12,%rax,2), %esi
	leal	(%rsi,%rsi,4), %esi
	leal	(%rdi,%rsi,4), %esi
	movzwl	(%r14,%rax,2), %edi
	leal	(%rdi,%rdi,4), %edi
	leal	(%rsi,%rdi,4), %esi
	movzwl	(%r8,%rax,2), %edi
	imull	$-5, %edi, %edi
	addl	%esi, %edi
	movzwl	(%r9,%rax,2), %eax
	addl	%edi, %eax
	movl	%eax, -36(%rsp)
	imull	$-5, -52(%rsp), %ecx
	addl	-56(%rsp), %ecx
	movl	-48(%rsp), %esi
	movl	-44(%rsp), %ebp
	leal	(%rsi,%rsi,4), %edi
	leal	(%rcx,%rdi,4), %ecx
	leal	(%rbp,%rbp,4), %edi
	leal	(%rcx,%rdi,4), %ecx
	imull	$-5, -40(%rsp), %edi
	addl	%ecx, %edi
	leal	(%rax,%rdi), %ebx
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movl	15520(%rcx), %ecx
	leal	512(%rax,%rdi), %eax
	sarl	$31, %eax
	shrl	$22, %eax
	leal	512(%rax,%rbx), %edi
	sarl	$10, %edi
	cmovsl	%edx, %edi
	cmpl	%ecx, %edi
	cmovgl	%ecx, %edi
	cmpl	$1, %r10d
	jne	.LBB3_27
# BB#18:
	leal	16(%rsi), %eax
	sarl	$31, %eax
	shrl	$27, %eax
	leal	16(%rsi,%rax), %eax
	jmp	.LBB3_28
.LBB3_19:
	testl	%r9d, %r9d
	cmovnsl	%r9d, %esi
	cmpl	%ebx, %esi
	cmovgl	%ebx, %esi
	movslq	%esi, %rdx
.LBB3_20:
	movzwl	(%r8,%rdx,2), %ecx
	jmp	.LBB3_25
.LBB3_21:
	xorl	%eax, %eax
	cmpl	$1, %ebp
	setne	%al
	xorl	%r12d, %r12d
	addl	%ecx, %eax
	cmovsl	%r12d, %eax
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	cltq
	movq	-24(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%rax,8), %rax
	leal	-2(%r11), %edi
	cmpl	$11, %r15d
	cmovlel	%r12d, %edi
	cmpl	%ebx, %edi
	cmovgl	%ebx, %edi
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %edi
	leal	-1(%r11), %ebp
	cmpl	$7, %r15d
	cmovlel	%r12d, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movslq	%ebp, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	imull	$-5, %ebp, %ebp
	addl	%edi, %ebp
	testl	%r11d, %r11d
	movl	$0, %edi
	cmovnsl	%r11d, %edi
	cmpl	%ebx, %edi
	cmovgl	%ebx, %edi
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %edi
	leal	(%rdi,%rdi,4), %edi
	leal	(%rbp,%rdi,4), %edi
	leal	1(%r11), %ebp
	cmpl	$-4, %r15d
	cmovlel	%r12d, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movslq	%ebp, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	leal	(%rbp,%rbp,4), %ebp
	leal	(%rdi,%rbp,4), %edi
	leal	2(%r11), %ebp
	cmpl	$-8, %r15d
	cmovlel	%r12d, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movslq	%ebp, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	imull	$-5, %ebp, %ebp
	addl	%edi, %ebp
	leal	3(%r11), %edi
	cmpl	$-12, %r15d
	cmovlel	%r12d, %edi
	cmpl	%ebx, %edi
	cmovgl	%ebx, %edi
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	leal	(%rax,%rbp), %edi
	movl	15520(%r14), %r8d
	leal	16(%rax,%rbp), %eax
	sarl	$31, %eax
	shrl	$27, %eax
	leal	16(%rax,%rdi), %ebp
	sarl	$5, %ebp
	cmovsl	%r12d, %ebp
	xorl	%eax, %eax
	cmpl	$1, %r10d
	setne	%al
	addl	%r11d, %eax
	cmovsl	%r12d, %eax
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	cltq
	leal	-2(%rcx), %edi
	cmpl	$11, %r13d
	cmovlel	%r12d, %edi
	cmpl	%r9d, %edi
	cmovgl	%r9d, %edi
	movslq	%edi, %rdi
	movq	(%rdx,%rdi,8), %rdi
	movzwl	(%rdi,%rax,2), %edi
	leal	-1(%rcx), %ebx
	cmpl	$7, %r13d
	cmovlel	%r12d, %ebx
	cmpl	%r9d, %ebx
	cmovgl	%r9d, %ebx
	movslq	%ebx, %rbx
	movq	(%rdx,%rbx,8), %rbx
	movzwl	(%rbx,%rax,2), %ebx
	imull	$-5, %ebx, %ebx
	addl	%edi, %ebx
	testl	%ecx, %ecx
	movl	$0, %edi
	cmovnsl	%ecx, %edi
	cmpl	%r9d, %edi
	cmovgl	%r9d, %edi
	movslq	%edi, %rdi
	movq	(%rdx,%rdi,8), %rdi
	movzwl	(%rdi,%rax,2), %edi
	leal	(%rdi,%rdi,4), %edi
	leal	(%rbx,%rdi,4), %edi
	leal	1(%rcx), %ebx
	cmpl	$-4, %r13d
	cmovlel	%r12d, %ebx
	cmpl	%r9d, %ebx
	cmovgl	%r9d, %ebx
	movslq	%ebx, %rbx
	movq	(%rdx,%rbx,8), %rbx
	movzwl	(%rbx,%rax,2), %ebx
	leal	(%rbx,%rbx,4), %ebx
	leal	(%rdi,%rbx,4), %edi
	leal	2(%rcx), %ebx
	cmpl	$-8, %r13d
	cmovlel	%r12d, %ebx
	cmpl	%r9d, %ebx
	cmovgl	%r9d, %ebx
	movslq	%ebx, %rbx
	movq	(%rdx,%rbx,8), %rbx
	movzwl	(%rbx,%rax,2), %ebx
	imull	$-5, %ebx, %ebx
	addl	%edi, %ebx
	addl	$3, %ecx
	cmpl	$-12, %r13d
	cmovlel	%r12d, %ecx
	cmpl	%r9d, %ecx
	cmovgl	%r9d, %ecx
	movslq	%ecx, %rcx
	movq	(%rdx,%rcx,8), %rcx
	movzwl	(%rcx,%rax,2), %eax
	leal	(%rax,%rbx), %ecx
	cmpl	%r8d, %ebp
	cmovgl	%r8d, %ebp
	movq	img(%rip), %rsi
	movl	15520(%rsi), %esi
	leal	16(%rax,%rbx), %eax
	sarl	$31, %eax
	shrl	$27, %eax
	leal	16(%rax,%rcx), %ecx
	sarl	$5, %ecx
	cmovsl	%r12d, %ecx
	cmpl	%esi, %ecx
	cmovgl	%esi, %ecx
	addl	%ebp, %ecx
	jmp	.LBB3_26
.LBB3_22:
	leal	16(%rdi), %edx
	sarl	$31, %edx
	shrl	$27, %edx
	leal	16(%rdi,%rdx), %edx
	sarl	$5, %edx
	cmovnsl	%edx, %ebp
	cmpl	%ecx, %ebp
	cmovgl	%ecx, %ebp
	addl	%eax, %ebp
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	jmp	.LBB3_30
.LBB3_23:
	testl	%r8d, %r8d
	cmovnsl	%r8d, %ebp
	cmpl	%r9d, %ebp
	cmovgl	%r9d, %ebp
	movslq	%ebp, %rcx
.LBB3_24:
	movq	(%rdi,%rcx,8), %rcx
	movzwl	(%rcx,%r10,2), %ecx
.LBB3_25:
	addl	%eax, %ecx
.LBB3_26:
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	jmp	.LBB3_30
.LBB3_27:
	leal	16(%rbp), %eax
	sarl	$31, %eax
	shrl	$27, %eax
	leal	16(%rbp,%rax), %eax
.LBB3_28:
	sarl	$5, %eax
	cmovnsl	%eax, %edx
	cmpl	%ecx, %edx
	cmovgl	%ecx, %edx
	addl	%edi, %edx
.LBB3_29:
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
.LBB3_30:
	sarl	%eax
.LBB3_31:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	Get_Reference_Pixel, .Lfunc_end3-Get_Reference_Pixel
	.cfi_endproc

	.globl	UpdateDecoders
	.p2align	4, 0x90
	.type	UpdateDecoders,@function
UpdateDecoders:                         # @UpdateDecoders
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 80
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	input(%rip), %rax
	cmpl	$0, 4728(%rax)
	jle	.LBB4_15
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
                                        #       Child Loop BB4_6 Depth 3
                                        #     Child Loop BB4_13 Depth 2
	movq	decs(%rip), %rax
	movq	40(%rax), %rdi
	callq	Build_Status_Map
	movq	decs(%rip), %rax
	movq	24(%rax), %rcx
	movq	(%rcx,%r13,8), %r14
	movq	img(%rip), %rcx
	movl	68(%rcx), %edx
	cmpl	$16, %edx
	jl	.LBB4_11
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	52(%rcx), %ebp
	cmpl	$16, %ebp
	jl	.LBB4_11
# BB#4:                                 # %.preheader.us.preheader.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	16(%rax), %rcx
	movq	40(%rax), %r12
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	(%rcx,%r13,8), %r15
	shrl	$4, %ebp
	shrl	$4, %edx
	xorl	%r13d, %r13d
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_5:                                # %.preheader.us.i
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_6 Depth 3
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_6:                                #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r12,%r13,8), %rax
	cmpb	$0, (%rax,%rbx)
	je	.LBB4_8
# BB#7:                                 #   in Loop: Header=BB4_6 Depth=3
	movq	%r14, %rdi
	movl	%r13d, %esi
	movl	%ebx, %edx
	movq	%r15, %rcx
	movq	%r12, %r8
	callq	Conceal_Error
.LBB4_8:                                #   in Loop: Header=BB4_6 Depth=3
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB4_6
# BB#9:                                 # %._crit_edge.us.i
                                        #   in Loop: Header=BB4_5 Depth=2
	incq	%r13
	movq	16(%rsp), %rdx          # 8-byte Reload
	cmpq	%rdx, %r13
	jl	.LBB4_5
# BB#10:                                # %Error_Concealment.exit.loopexit
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	decs(%rip), %rax
	movq	24(%rax), %rcx
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	(%rcx,%r13,8), %r14
	movq	img(%rip), %rcx
	movl	68(%rcx), %edx
.LBB4_11:                               # %Error_Concealment.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	testl	%edx, %edx
	jle	.LBB4_14
# BB#12:                                # %.lr.ph.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	16(%rax), %rax
	movq	(%rax,%r13,8), %r15
	movl	(%rcx), %eax
	subl	start_frame_no_in_this_IGOP(%rip), %eax
	cltd
	idivl	15240(%rcx)
	movslq	%edx, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_13:                               #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15,%rbp,8), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	(%r14,%rbx,8), %rsi
	movslq	52(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%rbx
	movq	img(%rip), %rcx
	movslq	68(%rcx), %rax
	cmpq	%rax, %rbx
	jl	.LBB4_13
.LBB4_14:                               # %DecOneForthPix.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	incq	%r13
	movq	input(%rip), %rax
	movslq	4728(%rax), %rax
	cmpq	%rax, %r13
	jl	.LBB4_2
.LBB4_15:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	UpdateDecoders, .Lfunc_end4-UpdateDecoders
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4746794007244308480     # double 2147483647
.LCPI5_1:
	.quad	4636737291354636288     # double 100
	.text
	.globl	Build_Status_Map
	.p2align	4, 0x90
	.type	Build_Status_Map,@function
Build_Status_Map:                       # @Build_Status_Map
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi62:
	.cfi_def_cfa_offset 96
.Lcfi63:
	.cfi_offset %rbx, -56
.Lcfi64:
	.cfi_offset %r12, -48
.Lcfi65:
	.cfi_offset %r13, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	img(%rip), %rcx
	movl	68(%rcx), %eax
	cmpl	$16, %eax
	jl	.LBB5_14
# BB#1:
	movl	52(%rcx), %ecx
	cmpl	$16, %ecx
	jl	.LBB5_14
# BB#2:                                 # %.preheader.us.preheader
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$28, %edx
	addl	%ecx, %edx
	sarl	$4, %edx
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$28, %ecx
	addl	%eax, %ecx
	sarl	$4, %ecx
	movslq	%edx, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movslq	%ecx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$-1, %ebp
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_4 Depth 2
	movslq	%edx, %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	imulq	$536, %rdx, %r13        # imm = 0x218
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB5_4:                                #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	input(%rip), %rax
	cmpl	$0, 264(%rax)
	je	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=2
	movq	img(%rip), %rax
	movq	14224(%rax), %rax
	cmpl	%ebp, (%rax,%r13)
	je	.LBB5_7
.LBB5_6:                                #   in Loop: Header=BB5_4 Depth=2
	callq	rand
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm1, %xmm0
	movsd	.LCPI5_1(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movq	input(%rip), %rax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	4724(%rax), %xmm1
	xorl	%ebx, %ebx
	ucomisd	%xmm0, %xmm1
	seta	%bl
	leal	(%rbx,%rbx,2), %r12d
	callq	rand
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	.LCPI5_0(%rip), %xmm0
	mulsd	.LCPI5_1(%rip), %xmm0
	movq	input(%rip), %rax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	4720(%rax), %xmm1
	ucomisd	%xmm0, %xmm1
	leal	2(%rbx,%rbx,2), %r14d
	cmovbel	%r12d, %r14d
	callq	rand
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	.LCPI5_0(%rip), %xmm0
	mulsd	.LCPI5_1(%rip), %xmm0
	movq	input(%rip), %rax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	4716(%rax), %xmm1
	incl	%ebp
	ucomisd	%xmm0, %xmm1
	movl	$1, %eax
	ja	.LBB5_9
.LBB5_7:                                #   in Loop: Header=BB5_4 Depth=2
	testl	%r14d, %r14d
	je	.LBB5_15
# BB#8:                                 #   in Loop: Header=BB5_4 Depth=2
	movl	%r14d, %eax
.LBB5_9:                                # %.thread.us
                                        #   in Loop: Header=BB5_4 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movb	%al, (%rcx,%r15)
	movq	input(%rip), %rcx
	cmpl	$0, 4016(%rcx)
	jne	.LBB5_12
# BB#10:                                #   in Loop: Header=BB5_4 Depth=2
	movb	$1, %cl
	jmp	.LBB5_11
	.p2align	4, 0x90
.LBB5_15:                               #   in Loop: Header=BB5_4 Depth=2
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.LBB5_11:                               # %.sink.split.us
                                        #   in Loop: Header=BB5_4 Depth=2
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	(%rdx,%rsi,8), %rdx
	movb	%cl, (%rdx,%r15)
.LBB5_12:                               #   in Loop: Header=BB5_4 Depth=2
	movl	%eax, %r14d
	incq	%r15
	addq	$536, %r13              # imm = 0x218
	cmpq	32(%rsp), %r15          # 8-byte Folded Reload
	jl	.LBB5_4
# BB#13:                                # %._crit_edge.us
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	%r15, %rax
	movq	%rax, %rdx
	movq	%rcx, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	jl	.LBB5_3
.LBB5_14:                               # %._crit_edge63
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	Build_Status_Map, .Lfunc_end5-Build_Status_Map
	.cfi_endproc

	.globl	Error_Concealment
	.p2align	4, 0x90
	.type	Error_Concealment,@function
Error_Concealment:                      # @Error_Concealment
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi73:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi75:
	.cfi_def_cfa_offset 64
.Lcfi76:
	.cfi_offset %rbx, -56
.Lcfi77:
	.cfi_offset %r12, -48
.Lcfi78:
	.cfi_offset %r13, -40
.Lcfi79:
	.cfi_offset %r14, -32
.Lcfi80:
	.cfi_offset %r15, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	img(%rip), %rcx
	movl	68(%rcx), %eax
	cmpl	$16, %eax
	jl	.LBB6_8
# BB#1:
	movl	52(%rcx), %ecx
	cmpl	$16, %ecx
	jl	.LBB6_8
# BB#2:                                 # %.preheader.us.preheader
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$28, %edx
	addl	%ecx, %edx
	sarl	$4, %edx
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$28, %ecx
	addl	%eax, %ecx
	sarl	$4, %ecx
	movslq	%edx, %rbp
	movslq	%ecx, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB6_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_4 Depth 2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_4:                                #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%r13,8), %rax
	cmpb	$0, (%rax,%rbx)
	je	.LBB6_6
# BB#5:                                 #   in Loop: Header=BB6_4 Depth=2
	movq	%r15, %rdi
	movl	%r13d, %esi
	movl	%ebx, %edx
	movq	%r14, %rcx
	movq	%r12, %r8
	callq	Conceal_Error
.LBB6_6:                                #   in Loop: Header=BB6_4 Depth=2
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB6_4
# BB#7:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB6_3 Depth=1
	incq	%r13
	cmpq	(%rsp), %r13            # 8-byte Folded Reload
	jl	.LBB6_3
.LBB6_8:                                # %._crit_edge23
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	Error_Concealment, .Lfunc_end6-Error_Concealment
	.cfi_endproc

	.globl	DecOneForthPix
	.p2align	4, 0x90
	.type	DecOneForthPix,@function
DecOneForthPix:                         # @DecOneForthPix
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi86:
	.cfi_def_cfa_offset 48
.Lcfi87:
	.cfi_offset %rbx, -40
.Lcfi88:
	.cfi_offset %r12, -32
.Lcfi89:
	.cfi_offset %r14, -24
.Lcfi90:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	img(%rip), %rcx
	cmpl	$0, 68(%rcx)
	jle	.LBB7_3
# BB#1:                                 # %.lr.ph
	movl	(%rcx), %eax
	subl	start_frame_no_in_this_IGOP(%rip), %eax
	cltd
	idivl	15240(%rcx)
	movslq	%edx, %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14,%r12,8), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	(%r15,%rbx,8), %rsi
	movslq	52(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%rbx
	movq	img(%rip), %rcx
	movslq	68(%rcx), %rax
	cmpq	%rax, %rbx
	jl	.LBB7_2
.LBB7_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	DecOneForthPix, .Lfunc_end7-DecOneForthPix
	.cfi_endproc

	.globl	compute_residue_b8block
	.p2align	4, 0x90
	.type	compute_residue_b8block,@function
compute_residue_b8block:                # @compute_residue_b8block
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 56
.Lcfi97:
	.cfi_offset %rbx, -56
.Lcfi98:
	.cfi_offset %r12, -48
.Lcfi99:
	.cfi_offset %r13, -40
.Lcfi100:
	.cfi_offset %r14, -32
.Lcfi101:
	.cfi_offset %r15, -24
.Lcfi102:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	%edi, %ebx
	shrl	$31, %ebx
	addl	%edi, %ebx
	movl	%ebx, %eax
	andl	$536870910, %eax        # imm = 0x1FFFFFFE
	subl	%eax, %edi
	leal	(,%rdi,8), %r14d
	leal	8(,%rdi,8), %edi
	shll	$2, %ebx
	andl	$-8, %ebx
	leal	8(%rbx), %ebp
	testl	%esi, %esi
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	img(%rip), %rcx
	js	.LBB8_1
# BB#4:                                 # %.preheader46
	movslq	%esi, %r15
	movq	decs(%rip), %rdx
	movq	(%rdx), %r9
	movslq	%ebx, %r8
	movslq	%ebp, %rdx
	movq	%rdx, -8(%rsp)          # 8-byte Spill
	movslq	%r14d, %r10
	movslq	%edi, %rdx
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	movq	%r8, %r12
	orq	$1, %r12
	leal	1(%r12), %edi
	movq	%r8, %r11
	orq	$3, %r11
	leal	1(%r11), %edx
	movl	%edx, -64(%rsp)         # 4-byte Spill
	leal	2(%r11), %ebp
	movq	%r8, %rdx
	orq	$7, %rdx
	leaq	(,%r10,4), %rbx
	shlq	$9, %r15
	movq	%r8, %rsi
	shlq	$5, %rsi
	addq	%r15, %rsi
	movq	(%r9,%r8,8), %r15
	addq	%rbx, %r15
	leaq	(%rsi,%r10,2), %rsi
	leaq	5040(%rcx,%rsi), %rsi
	movl	%r14d, %r13d
	movslq	%edi, %rdi
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	movslq	-64(%rsp), %rdi         # 4-byte Folded Reload
	movq	%rdi, -32(%rsp)         # 8-byte Spill
	movslq	%ebp, %rdi
	movq	%rdi, -40(%rsp)         # 8-byte Spill
	leal	3(%r11), %ebp
	movslq	%ebp, %rdi
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	movq	%rdx, %rdi
	movq	%rdi, -64(%rsp)         # 8-byte Spill
	movslq	%edx, %rdx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB8_5:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movslq	180(%rcx), %rdi
	addq	%r8, %rdi
	movq	(%rax,%rdi,8), %rdi
	leal	(%r13,%rbp), %edx
	addl	176(%rcx), %edx
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	movzwl	-224(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	cmpq	-8(%rsp), %r12          # 8-byte Folded Reload
	movl	%edx, (%r15,%rbp,4)
	jge	.LBB8_6
# BB#8:                                 #   in Loop: Header=BB8_5 Depth=1
	movslq	180(%rcx), %rdx
	movslq	%r12d, %rdi
	addq	%rdx, %rdi
	movq	(%rax,%rdi,8), %rdx
	leal	(%r13,%rbp), %r14d
	movl	176(%rcx), %edi
	addl	%r14d, %edi
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movzwl	-192(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	movq	(%r9,%r12,8), %rdi
	addq	%rbx, %rdi
	movl	%edx, (%rdi,%rbp,4)
	movslq	180(%rcx), %rdx
	addq	-24(%rsp), %rdx         # 8-byte Folded Reload
	movq	(%rax,%rdx,8), %rdx
	movl	176(%rcx), %edi
	addl	%r14d, %edi
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movzwl	-160(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	movq	8(%r9,%r12,8), %rdi
	addq	%rbx, %rdi
	movl	%edx, (%rdi,%rbp,4)
	movslq	180(%rcx), %rdx
	movslq	%r11d, %rdi
	addq	%rdx, %rdi
	movq	(%rax,%rdi,8), %rdx
	movl	176(%rcx), %edi
	addl	%r14d, %edi
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movzwl	-128(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	movq	(%r9,%r11,8), %rdi
	addq	%rbx, %rdi
	movl	%edx, (%rdi,%rbp,4)
	movslq	180(%rcx), %rdx
	addq	-32(%rsp), %rdx         # 8-byte Folded Reload
	movq	(%rax,%rdx,8), %rdx
	movl	176(%rcx), %edi
	addl	%r14d, %edi
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movzwl	-96(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	movq	8(%r9,%r11,8), %rdi
	addq	%rbx, %rdi
	movl	%edx, (%rdi,%rbp,4)
	movslq	180(%rcx), %rdx
	addq	-40(%rsp), %rdx         # 8-byte Folded Reload
	movq	(%rax,%rdx,8), %rdx
	movl	176(%rcx), %edi
	addl	%r14d, %edi
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movzwl	-64(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	movq	16(%r9,%r11,8), %rdi
	addq	%rbx, %rdi
	movl	%edx, (%rdi,%rbp,4)
	movslq	180(%rcx), %rdx
	addq	-48(%rsp), %rdx         # 8-byte Folded Reload
	movq	(%rax,%rdx,8), %rdx
	movl	176(%rcx), %edi
	addl	%r14d, %edi
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movzwl	-32(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	movq	24(%r9,%r11,8), %rdi
	addq	%rbx, %rdi
	movl	%edx, (%rdi,%rbp,4)
	movslq	180(%rcx), %rdx
	addq	-56(%rsp), %rdx         # 8-byte Folded Reload
	movq	(%rax,%rdx,8), %rdx
	addl	176(%rcx), %r14d
	movslq	%r14d, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movzwl	(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	movq	-64(%rsp), %rdi         # 8-byte Reload
	movq	(%r9,%rdi,8), %rdi
	addq	%rbx, %rdi
	movl	%edx, (%rdi,%rbp,4)
.LBB8_6:                                #   in Loop: Header=BB8_5 Depth=1
	leaq	1(%r10,%rbp), %rdx
	incq	%rbp
	cmpq	-16(%rsp), %rdx         # 8-byte Folded Reload
	jl	.LBB8_5
	jmp	.LBB8_7
.LBB8_1:                                # %.preheader48
	movq	decs(%rip), %rdx
	movq	(%rdx), %r9
	movslq	%ebx, %r8
	movslq	%ebp, %rdx
	movq	%rdx, -8(%rsp)          # 8-byte Spill
	movslq	%r14d, %r10
	movslq	%edi, %rdx
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	movq	%r8, %r12
	orq	$1, %r12
	leal	1(%r12), %edi
	movq	%r8, %r11
	orq	$3, %r11
	leal	1(%r11), %r13d
	leal	2(%r11), %r14d
	leal	3(%r11), %ebp
	movq	%r8, %rdx
	orq	$7, %rdx
	leaq	(,%r10,4), %rbx
	movq	(%r9,%r8,8), %r15
	addq	%rbx, %r15
	movq	%r8, %rsi
	shlq	$5, %rsi
	leaq	(%rsi,%r10,2), %rsi
	leaq	12848(%rcx,%rsi), %rsi
	movslq	%edi, %rdi
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	movslq	%r13d, %rdi
	movq	%rdi, -32(%rsp)         # 8-byte Spill
	movslq	%r14d, %rdi
	movq	%rdi, -40(%rsp)         # 8-byte Spill
	movslq	%ebp, %rdi
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	movl	%r10d, %r13d
	xorl	%ebp, %ebp
	movq	%rdx, %rdi
	movq	%rdi, -64(%rsp)         # 8-byte Spill
	movslq	%edx, %rdx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB8_2:                                # %.preheader47
                                        # =>This Inner Loop Header: Depth=1
	movslq	180(%rcx), %rdi
	addq	%r8, %rdi
	movq	(%rax,%rdi,8), %rdi
	leal	(%r13,%rbp), %edx
	addl	176(%rcx), %edx
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	movzwl	-224(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	cmpq	-8(%rsp), %r12          # 8-byte Folded Reload
	movl	%edx, (%r15,%rbp,4)
	jge	.LBB8_3
# BB#9:                                 #   in Loop: Header=BB8_2 Depth=1
	movslq	180(%rcx), %rdx
	movslq	%r12d, %rdi
	addq	%rdx, %rdi
	movq	(%rax,%rdi,8), %rdx
	leal	(%r13,%rbp), %r14d
	movl	176(%rcx), %edi
	addl	%r14d, %edi
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movzwl	-192(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	movq	(%r9,%r12,8), %rdi
	addq	%rbx, %rdi
	movl	%edx, (%rdi,%rbp,4)
	movslq	180(%rcx), %rdx
	addq	-24(%rsp), %rdx         # 8-byte Folded Reload
	movq	(%rax,%rdx,8), %rdx
	movl	176(%rcx), %edi
	addl	%r14d, %edi
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movzwl	-160(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	movq	8(%r9,%r12,8), %rdi
	addq	%rbx, %rdi
	movl	%edx, (%rdi,%rbp,4)
	movslq	180(%rcx), %rdx
	movslq	%r11d, %rdi
	addq	%rdx, %rdi
	movq	(%rax,%rdi,8), %rdx
	movl	176(%rcx), %edi
	addl	%r14d, %edi
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movzwl	-128(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	movq	(%r9,%r11,8), %rdi
	addq	%rbx, %rdi
	movl	%edx, (%rdi,%rbp,4)
	movslq	180(%rcx), %rdx
	addq	-32(%rsp), %rdx         # 8-byte Folded Reload
	movq	(%rax,%rdx,8), %rdx
	movl	176(%rcx), %edi
	addl	%r14d, %edi
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movzwl	-96(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	movq	8(%r9,%r11,8), %rdi
	addq	%rbx, %rdi
	movl	%edx, (%rdi,%rbp,4)
	movslq	180(%rcx), %rdx
	addq	-40(%rsp), %rdx         # 8-byte Folded Reload
	movq	(%rax,%rdx,8), %rdx
	movl	176(%rcx), %edi
	addl	%r14d, %edi
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movzwl	-64(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	movq	16(%r9,%r11,8), %rdi
	addq	%rbx, %rdi
	movl	%edx, (%rdi,%rbp,4)
	movslq	180(%rcx), %rdx
	addq	-48(%rsp), %rdx         # 8-byte Folded Reload
	movq	(%rax,%rdx,8), %rdx
	movl	176(%rcx), %edi
	addl	%r14d, %edi
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movzwl	-32(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	movq	24(%r9,%r11,8), %rdi
	addq	%rbx, %rdi
	movl	%edx, (%rdi,%rbp,4)
	movslq	180(%rcx), %rdx
	addq	-56(%rsp), %rdx         # 8-byte Folded Reload
	movq	(%rax,%rdx,8), %rdx
	addl	176(%rcx), %r14d
	movslq	%r14d, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movzwl	(%rsi,%rbp,2), %edi
	subl	%edi, %edx
	movq	-64(%rsp), %rdi         # 8-byte Reload
	movq	(%r9,%rdi,8), %rdi
	addq	%rbx, %rdi
	movl	%edx, (%rdi,%rbp,4)
.LBB8_3:                                #   in Loop: Header=BB8_2 Depth=1
	leaq	1(%r10,%rbp), %rdx
	incq	%rbp
	cmpq	-16(%rsp), %rdx         # 8-byte Folded Reload
	jl	.LBB8_2
.LBB8_7:                                # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	compute_residue_b8block, .Lfunc_end8-compute_residue_b8block
	.cfi_endproc

	.globl	compute_residue_mb
	.p2align	4, 0x90
	.type	compute_residue_mb,@function
compute_residue_mb:                     # @compute_residue_mb
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 16
.Lcfi104:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	xorl	%edi, %edi
	movl	%ebx, %esi
	callq	compute_residue_b8block
	movl	$1, %edi
	movl	%ebx, %esi
	callq	compute_residue_b8block
	movl	$2, %edi
	movl	%ebx, %esi
	callq	compute_residue_b8block
	movl	$3, %edi
	movl	%ebx, %esi
	popq	%rbx
	jmp	compute_residue_b8block # TAILCALL
.Lfunc_end9:
	.size	compute_residue_mb, .Lfunc_end9-compute_residue_mb
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.short	127                     # 0x7f
	.short	127                     # 0x7f
	.short	127                     # 0x7f
	.short	127                     # 0x7f
	.short	127                     # 0x7f
	.short	127                     # 0x7f
	.short	127                     # 0x7f
	.short	127                     # 0x7f
	.text
	.globl	Conceal_Error
	.p2align	4, 0x90
	.type	Conceal_Error,@function
Conceal_Error:                          # @Conceal_Error
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi108:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi109:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi111:
	.cfi_def_cfa_offset 272
.Lcfi112:
	.cfi_offset %rbx, -56
.Lcfi113:
	.cfi_offset %r12, -48
.Lcfi114:
	.cfi_offset %r13, -40
.Lcfi115:
	.cfi_offset %r14, -32
.Lcfi116:
	.cfi_offset %r15, -24
.Lcfi117:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movl	%esi, %r15d
	movq	%rdi, %r12
	movq	img(%rip), %r14
	movl	(%r14), %eax
	decl	%eax
	subl	start_frame_no_in_this_IGOP(%rip), %eax
	cltd
	idivl	28(%r14)
	movq	decs(%rip), %rax
	movq	48(%rax), %rbp
	movslq	%r15d, %rsi
	movq	(%rbp,%rsi,8), %rbx
	movslq	%r13d, %rbp
	movb	(%rbx,%rbp), %al
	movl	%eax, %ebx
	decb	%bl
	cmpb	$3, %bl
	jb	.LBB10_6
# BB#1:
	cmpb	$8, %al
	je	.LBB10_6
# BB#2:
	testb	%al, %al
	jne	.LBB10_9
# BB#3:
	movl	20(%r14), %ebx
	testl	%ebx, %ebx
	je	.LBB10_34
# BB#4:
	cmpl	$1, %ebx
	jne	.LBB10_9
# BB#5:
	cmpl	$0, 15360(%r14)
	setg	%r9b
	jmp	.LBB10_10
.LBB10_6:
	movl	20(%r14), %ebx
	testl	%ebx, %ebx
	je	.LBB10_11
# BB#7:
	cmpl	$1, %ebx
	jne	.LBB10_9
# BB#8:
	cmpl	$0, 15360(%r14)
	setg	%dil
	jmp	.LBB10_12
.LBB10_9:                               # %.fold.split262
	xorl	%r9d, %r9d
.LBB10_10:                              # %.thread
	xorl	%edi, %edi
	jmp	.LBB10_13
.LBB10_11:
	movb	$1, %dil
.LBB10_12:                              # %.thread
	xorl	%r9d, %r9d
.LBB10_13:                              # %.thread
	movq	(%r8,%rsi,8), %rax
	movb	(%rax,%rbp), %bl
	decb	%bl
	cmpb	$4, %bl
	ja	.LBB10_27
# BB#14:                                # %.thread
	movl	%r15d, %r10d
	shll	$4, %r10d
	movl	%r13d, %r8d
	shll	$4, %r8d
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %rax
	movq	(%rax), %r11
	movzbl	%bl, %eax
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	jmpq	*.LJTI10_0(,%rax,8)
.LBB10_15:
	cmpl	$2, 20(%r14)
	je	.LBB10_19
# BB#16:                                # %.preheader266
	movslq	%edx, %rax
	movslq	%r8d, %rdx
	movslq	%r10d, %rbp
	movq	%rdx, %rsi
	orq	$1, %rsi
	movq	%rdx, %rdi
	orq	$2, %rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%rdx, %rdi
	orq	$3, %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%rdx, %rdi
	orq	$4, %rdi
	movq	%rdi, (%rsp)            # 8-byte Spill
	movq	%rdx, %rdi
	orq	$5, %rdi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %rdi
	orq	$6, %rdi
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %rdi
	orq	$7, %rdi
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	%rdx, %rdi
	orq	$8, %rdi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	%rdx, %r13
	orq	$9, %r13
	leaq	(,%rbp,8), %rdi
	addq	(%rcx,%rax,8), %rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	%rsi, %rcx
	movq	%rdx, %rsi
	orq	$10, %rsi
	movq	%rdx, %r11
	orq	$11, %r11
	leaq	(%r12,%rbp,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rdx, %rbp
	orq	$12, %rbp
	movq	%rdx, %rbx
	orq	$13, %rbx
	movq	%rdx, %r8
	orq	$14, %r8
	movq	%rdx, %r12
	orq	$15, %r12
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB10_17:                              # %.preheader265
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r9,8), %r10
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r9,8), %r14
	movzwl	(%r10,%rdx,2), %r15d
	movw	%r15w, (%r14,%rdx,2)
	movzwl	(%r10,%rcx,2), %eax
	movw	%ax, (%r14,%rcx,2)
	movq	32(%rsp), %rdi          # 8-byte Reload
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movq	24(%rsp), %rdi          # 8-byte Reload
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movq	(%rsp), %rdi            # 8-byte Reload
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movq	64(%rsp), %rdi          # 8-byte Reload
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movq	56(%rsp), %rdi          # 8-byte Reload
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movzwl	(%r10,%r13,2), %eax
	movw	%ax, (%r14,%r13,2)
	movzwl	(%r10,%rsi,2), %eax
	movw	%ax, (%r14,%rsi,2)
	movzwl	(%r10,%r11,2), %eax
	movw	%ax, (%r14,%r11,2)
	movzwl	(%r10,%rbp,2), %eax
	movw	%ax, (%r14,%rbp,2)
	movzwl	(%r10,%rbx,2), %eax
	movw	%ax, (%r14,%rbx,2)
	movzwl	(%r10,%r8,2), %eax
	movw	%ax, (%r14,%r8,2)
	movzwl	(%r10,%r12,2), %eax
	movw	%ax, (%r14,%r12,2)
	incq	%r9
	cmpq	$16, %r9
	jne	.LBB10_17
	jmp	.LBB10_27
.LBB10_18:
	cmpl	$2, 20(%r14)
	jne	.LBB10_28
.LBB10_19:                              # %.preheader294
	movslq	%r8d, %rcx
	movslq	%r10d, %rdx
	movq	%rcx, %rax
	orq	$8, %rax
	movq	(%r12,%rdx,8), %rsi
	movdqa	.LCPI10_0(%rip), %xmm0  # xmm0 = [127,127,127,127,127,127,127,127]
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$1, %rsi
	movq	(%r12,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$2, %rsi
	movq	(%r12,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$3, %rsi
	movq	(%r12,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$4, %rsi
	movq	(%r12,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$5, %rsi
	movq	(%r12,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$6, %rsi
	movq	(%r12,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$7, %rsi
	movq	(%r12,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$8, %rsi
	movq	(%r12,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$9, %rsi
	movq	(%r12,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$10, %rsi
	movq	(%r12,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$11, %rsi
	movq	(%r12,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$12, %rsi
	movq	(%r12,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$13, %rsi
	movq	(%r12,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$14, %rsi
	movq	(%r12,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	orq	$15, %rdx
	movq	(%r12,%rdx,8), %rdx
	jmp	.LBB10_26
.LBB10_20:
	cmpl	$2, 20(%r14)
	je	.LBB10_27
# BB#21:                                # %.preheader292
	movl	%edx, (%rsp)            # 4-byte Spill
	movl	%edi, 8(%rsp)           # 4-byte Spill
	shll	$2, %r15d
	leal	4(,%r13,4), %eax
	movslq	%r15d, %rsi
	movslq	%eax, %rdx
	leal	5(,%r13,4), %eax
	movslq	%eax, %r14
	leal	6(,%r13,4), %eax
	movl	%r9d, 16(%rsp)          # 4-byte Spill
	movslq	%eax, %r9
	leal	7(,%r13,4), %eax
	movslq	%eax, %rbp
	movq	(%r11,%rsi,8), %rax
	movq	(%rax,%rdx,8), %rcx
	movq	(%rax,%r14,8), %rdi
	movq	(%rax,%r9,8), %rbx
	movq	(%rax,%rbp,8), %rax
	pinsrw	$0, (%rcx), %xmm0
	pinsrw	$1, (%rdi), %xmm0
	pinsrw	$2, (%rbx), %xmm0
	pinsrw	$3, (%rax), %xmm0
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	pinsrw	$0, 2(%rcx), %xmm1
	pinsrw	$1, 2(%rdi), %xmm1
	pinsrw	$2, 2(%rbx), %xmm1
	movdqa	%xmm0, 80(%rsp)
	pinsrw	$3, 2(%rax), %xmm1
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	psrad	$16, %xmm0
	movdqa	%xmm0, 144(%rsp)
	movq	8(%r11,%rsi,8), %rax
	movq	(%rax,%rdx,8), %rcx
	movq	(%rax,%r14,8), %rdi
	movq	(%rax,%r9,8), %rbx
	movq	(%rax,%rbp,8), %rax
	pinsrw	$0, (%rcx), %xmm0
	pinsrw	$1, (%rdi), %xmm0
	pinsrw	$2, (%rbx), %xmm0
	pinsrw	$3, (%rax), %xmm0
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	pinsrw	$0, 2(%rcx), %xmm1
	pinsrw	$1, 2(%rdi), %xmm1
	pinsrw	$2, 2(%rbx), %xmm1
	movdqa	%xmm0, 96(%rsp)
	pinsrw	$3, 2(%rax), %xmm1
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	psrad	$16, %xmm0
	movdqa	%xmm0, 160(%rsp)
	movq	16(%r11,%rsi,8), %rax
	movq	(%rax,%rdx,8), %rcx
	movq	(%rax,%r14,8), %rdi
	movq	(%rax,%r9,8), %rbx
	movq	(%rax,%rbp,8), %rax
	pinsrw	$0, (%rcx), %xmm0
	pinsrw	$1, (%rdi), %xmm0
	pinsrw	$2, (%rbx), %xmm0
	pinsrw	$3, (%rax), %xmm0
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	pinsrw	$0, 2(%rcx), %xmm1
	pinsrw	$1, 2(%rdi), %xmm1
	pinsrw	$2, 2(%rbx), %xmm1
	movdqa	%xmm0, 112(%rsp)
	pinsrw	$3, 2(%rax), %xmm1
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	psrad	$16, %xmm0
	movdqa	%xmm0, 176(%rsp)
	movq	24(%r11,%rsi,8), %rax
	movq	(%rax,%rdx,8), %rcx
	movq	(%rax,%r14,8), %rdi
	movq	(%rax,%r9,8), %rbx
	movq	(%rax,%rbp,8), %rax
	pinsrw	$0, (%rcx), %xmm0
	pinsrw	$1, (%rdi), %xmm0
	pinsrw	$2, (%rbx), %xmm0
	pinsrw	$3, (%rax), %xmm0
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	pinsrw	$0, 2(%rcx), %xmm1
	pinsrw	$1, 2(%rdi), %xmm1
	pinsrw	$2, 2(%rbx), %xmm1
	movdqa	%xmm0, 128(%rsp)
	pinsrw	$3, 2(%rax), %xmm1
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	psrad	$16, %xmm0
	movdqa	%xmm0, 192(%rsp)
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB10_35
# BB#22:                                # %.preheader285
	movslq	(%rsp), %rax            # 4-byte Folded Reload
	movslq	%r8d, %r13
	movslq	%r10d, %rbp
	movq	%r13, %rcx
	orq	$1, %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%r13, %rcx
	orq	$2, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r13, %rcx
	orq	$3, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r13, %rcx
	orq	$4, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%r13, %rcx
	orq	$5, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%r13, %rcx
	orq	$6, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%r13, %rcx
	orq	$7, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%r13, %rdx
	orq	$8, %rdx
	movq	%r13, %rcx
	orq	$9, %rcx
	leaq	(,%rbp,8), %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	addq	(%rsi,%rax,8), %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%r13, %rsi
	orq	$10, %rsi
	movq	%r13, %r11
	orq	$11, %r11
	leaq	(%r12,%rbp,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%r13, %rbp
	orq	$12, %rbp
	movq	%r13, %rbx
	orq	$13, %rbx
	movq	%r13, %r8
	orq	$14, %r8
	movq	%r13, %r12
	orq	$15, %r12
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB10_23:                              # %.preheader284
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r9,8), %r10
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r9,8), %r14
	movzwl	(%r10,%r13,2), %r15d
	movw	%r15w, (%r14,%r13,2)
	movq	(%rsp), %rdi            # 8-byte Reload
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movq	32(%rsp), %rdi          # 8-byte Reload
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movq	64(%rsp), %rdi          # 8-byte Reload
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movq	56(%rsp), %rdi          # 8-byte Reload
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movq	48(%rsp), %rdi          # 8-byte Reload
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movzwl	(%r10,%rcx,2), %eax
	movw	%ax, (%r14,%rcx,2)
	movzwl	(%r10,%rsi,2), %eax
	movw	%ax, (%r14,%rsi,2)
	movzwl	(%r10,%r11,2), %eax
	movw	%ax, (%r14,%r11,2)
	movzwl	(%r10,%rbp,2), %eax
	movw	%ax, (%r14,%rbp,2)
	movzwl	(%r10,%rbx,2), %eax
	movw	%ax, (%r14,%rbx,2)
	movzwl	(%r10,%r8,2), %eax
	movw	%ax, (%r14,%r8,2)
	movzwl	(%r10,%r12,2), %eax
	movw	%ax, (%r14,%r12,2)
	incq	%r9
	cmpq	$16, %r9
	jne	.LBB10_23
	jmp	.LBB10_27
.LBB10_24:                              # %.preheader282
	movl	%r9d, 16(%rsp)          # 4-byte Spill
	movl	%edi, 8(%rsp)           # 4-byte Spill
	movl	%edx, (%rsp)            # 4-byte Spill
	shll	$2, %r15d
	leal	4(,%r13,4), %ecx
	movslq	%r15d, %rsi
	movslq	%ecx, %rax
	leal	5(,%r13,4), %ecx
	movslq	%ecx, %rdi
	leal	6(,%r13,4), %ecx
	movslq	%ecx, %r12
	leal	7(,%r13,4), %ecx
	movslq	%ecx, %rbp
	movq	(%r11,%rsi,8), %rcx
	movq	(%rcx,%rax,8), %rbx
	movq	(%rcx,%rdi,8), %r9
	movq	(%rcx,%r12,8), %rdx
	movq	(%rcx,%rbp,8), %rcx
	pinsrw	$0, (%rbx), %xmm0
	pinsrw	$1, (%r9), %xmm0
	pinsrw	$2, (%rdx), %xmm0
	pinsrw	$3, (%rcx), %xmm0
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	pinsrw	$0, 2(%rbx), %xmm1
	pinsrw	$1, 2(%r9), %xmm1
	pinsrw	$2, 2(%rdx), %xmm1
	movdqa	%xmm0, 80(%rsp)
	pinsrw	$3, 2(%rcx), %xmm1
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	psrad	$16, %xmm0
	movdqa	%xmm0, 144(%rsp)
	movq	8(%r11,%rsi,8), %rcx
	movq	%rax, %rbx
	movq	(%rcx,%rbx,8), %rdx
	movq	(%rcx,%rdi,8), %rax
	movq	(%rcx,%r12,8), %r9
	movq	(%rcx,%rbp,8), %rcx
	pinsrw	$0, (%rdx), %xmm0
	pinsrw	$1, (%rax), %xmm0
	pinsrw	$2, (%r9), %xmm0
	pinsrw	$3, (%rcx), %xmm0
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	pinsrw	$0, 2(%rdx), %xmm1
	pinsrw	$1, 2(%rax), %xmm1
	pinsrw	$2, 2(%r9), %xmm1
	movdqa	%xmm0, 96(%rsp)
	pinsrw	$3, 2(%rcx), %xmm1
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	psrad	$16, %xmm0
	movdqa	%xmm0, 160(%rsp)
	movq	16(%r11,%rsi,8), %rax
	movq	(%rax,%rbx,8), %rcx
	movq	%rbx, %r9
	movq	(%rax,%rdi,8), %rdx
	movq	(%rax,%r12,8), %rbx
	movq	(%rax,%rbp,8), %rax
	pinsrw	$0, (%rcx), %xmm0
	pinsrw	$1, (%rdx), %xmm0
	pinsrw	$2, (%rbx), %xmm0
	pinsrw	$3, (%rax), %xmm0
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	pinsrw	$0, 2(%rcx), %xmm1
	pinsrw	$1, 2(%rdx), %xmm1
	pinsrw	$2, 2(%rbx), %xmm1
	movdqa	%xmm0, 112(%rsp)
	pinsrw	$3, 2(%rax), %xmm1
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	psrad	$16, %xmm0
	movdqa	%xmm0, 176(%rsp)
	movq	24(%r11,%rsi,8), %rax
	movq	(%rax,%r9,8), %rcx
	movq	(%rax,%rdi,8), %rdx
	movq	(%rax,%r12,8), %rbx
	movq	(%rax,%rbp,8), %rax
	pinsrw	$0, (%rcx), %xmm0
	pinsrw	$1, (%rdx), %xmm0
	pinsrw	$2, (%rbx), %xmm0
	pinsrw	$3, (%rax), %xmm0
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	pinsrw	$0, 2(%rcx), %xmm1
	pinsrw	$1, 2(%rdx), %xmm1
	pinsrw	$2, 2(%rbx), %xmm1
	movdqa	%xmm0, 128(%rsp)
	pinsrw	$3, 2(%rax), %xmm1
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	psrad	$16, %xmm0
	movdqa	%xmm0, 192(%rsp)
	cmpl	$2, 20(%r14)
	jne	.LBB10_31
# BB#25:                                # %.preheader268
	movslq	%r8d, %rcx
	movslq	%r10d, %rdx
	movq	%rcx, %rax
	orq	$8, %rax
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi,%rdx,8), %rsi
	movdqa	.LCPI10_0(%rip), %xmm0  # xmm0 = [127,127,127,127,127,127,127,127]
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$1, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$2, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$3, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$4, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$5, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$6, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$7, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$8, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$9, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$10, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$11, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$12, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$13, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	movq	%rdx, %rsi
	orq	$14, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movdqu	%xmm0, (%rsi,%rcx,2)
	movdqu	%xmm0, (%rsi,%rax,2)
	orq	$15, %rdx
	movq	(%rdi,%rdx,8), %rdx
.LBB10_26:                              # %.loopexit
	movdqu	%xmm0, (%rdx,%rcx,2)
	movdqu	%xmm0, (%rdx,%rax,2)
.LBB10_27:                              # %.loopexit
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_28:
	testb	%dil, %dil
	jne	.LBB10_27
# BB#29:                                # %.preheader298
	movslq	%edx, %rax
	movslq	%r8d, %rdi
	movslq	%r10d, %rbp
	movq	%rdi, %rsi
	orq	$1, %rsi
	movq	%rdi, %rdx
	orq	$2, %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %rdx
	orq	$3, %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %rdx
	orq	$4, %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rdi, %rdx
	orq	$5, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %rdx
	orq	$6, %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %rdx
	orq	$7, %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rdi, %rdx
	orq	$8, %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	orq	$9, %r13
	leaq	(,%rbp,8), %rdx
	addq	(%rcx,%rax,8), %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rsi, %rcx
	movq	%rdi, %rsi
	orq	$10, %rsi
	movq	%rdi, %r11
	orq	$11, %r11
	leaq	(%r12,%rbp,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	orq	$12, %rbp
	movq	%rdi, %rbx
	orq	$13, %rbx
	movq	%rdi, %r8
	orq	$14, %r8
	movq	%rdi, %r12
	orq	$15, %r12
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB10_30:                              # %.preheader297
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r9,8), %r10
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r9,8), %r14
	movzwl	(%r10,%rdi,2), %r15d
	movw	%r15w, (%r14,%rdi,2)
	movzwl	(%r10,%rcx,2), %eax
	movw	%ax, (%r14,%rcx,2)
	movq	32(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	24(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	(%rsp), %rdx            # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	8(%rsp), %rdx           # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	64(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	56(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movzwl	(%r10,%r13,2), %eax
	movw	%ax, (%r14,%r13,2)
	movzwl	(%r10,%rsi,2), %eax
	movw	%ax, (%r14,%rsi,2)
	movzwl	(%r10,%r11,2), %eax
	movw	%ax, (%r14,%r11,2)
	movzwl	(%r10,%rbp,2), %eax
	movw	%ax, (%r14,%rbp,2)
	movzwl	(%r10,%rbx,2), %eax
	movw	%ax, (%r14,%rbx,2)
	movzwl	(%r10,%r8,2), %eax
	movw	%ax, (%r14,%r8,2)
	movzwl	(%r10,%r12,2), %eax
	movw	%ax, (%r14,%r12,2)
	incq	%r9
	cmpq	$16, %r9
	jne	.LBB10_30
	jmp	.LBB10_27
.LBB10_31:
	movslq	(%rsp), %r14            # 4-byte Folded Reload
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	je	.LBB10_40
# BB#32:                                # %.preheader272
	movslq	%r8d, %r11
	movslq	%r10d, %rbp
	movq	%r11, %rcx
	orq	$1, %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%r11, %rcx
	orq	$2, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r11, %rcx
	orq	$3, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%r11, %rcx
	orq	$4, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%r11, %rcx
	orq	$5, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%r11, %rcx
	orq	$6, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%r11, %rcx
	orq	$7, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%r11, %r13
	orq	$8, %r13
	movq	%r11, %rcx
	orq	$9, %rcx
	leaq	(,%rbp,8), %rdx
	movq	24(%rsp), %rsi          # 8-byte Reload
	addq	(%rsi,%r14,8), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%r11, %rsi
	orq	$10, %rsi
	movq	%r11, %rdi
	orq	$11, %rdi
	leaq	(%rax,%rbp,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r11, %rbp
	orq	$12, %rbp
	movq	%r11, %rbx
	orq	$13, %rbx
	movq	%r11, %r8
	orq	$14, %r8
	movq	%r11, %r12
	orq	$15, %r12
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB10_33:                              # %.preheader271
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r9,8), %r10
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r9,8), %r14
	movzwl	(%r10,%r11,2), %r15d
	movw	%r15w, (%r14,%r11,2)
	movq	(%rsp), %rdx            # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	8(%rsp), %rdx           # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	64(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	56(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	48(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	40(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movzwl	(%r10,%r13,2), %eax
	movw	%ax, (%r14,%r13,2)
	movzwl	(%r10,%rcx,2), %eax
	movw	%ax, (%r14,%rcx,2)
	movzwl	(%r10,%rsi,2), %eax
	movw	%ax, (%r14,%rsi,2)
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movzwl	(%r10,%rbp,2), %eax
	movw	%ax, (%r14,%rbp,2)
	movzwl	(%r10,%rbx,2), %eax
	movw	%ax, (%r14,%rbx,2)
	movzwl	(%r10,%r8,2), %eax
	movw	%ax, (%r14,%r8,2)
	movzwl	(%r10,%r12,2), %eax
	movw	%ax, (%r14,%r12,2)
	incq	%r9
	cmpq	$16, %r9
	jne	.LBB10_33
	jmp	.LBB10_27
.LBB10_34:
	movb	$1, %r9b
	jmp	.LBB10_10
.LBB10_35:
	cmpb	$0, 8(%rsp)             # 1-byte Folded Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	movl	(%rsp), %eax            # 4-byte Reload
	je	.LBB10_27
# BB#36:
	shll	$2, %r13d
	movslq	%eax, %rcx
	movslq	%r13d, %r12
	movl	%r12d, %edx
	movl	%r12d, %eax
	orl	$3, %eax
	movslq	%eax, %r14
	orl	$3, %r15d
	movslq	%r15d, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	decq	%r12
	leaq	144(%rsp), %rdi
	movslq	%r8d, %rax
	leaq	6(%rax,%rax), %r15
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB10_37:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_38 Depth 2
	leal	(,%rsi,4), %eax
	movslq	%eax, %rbp
	leaq	1(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	2(%rbp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	3(%rbp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB10_38:                              # %.preheader287
                                        #   Parent Loop BB10_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r9,%rcx,8), %rdi
	movl	-64(%rbx), %ecx
	movl	(%rbx), %r8d
	movq	decs(%rip), %rax
	movq	32(%rax), %r9
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r13), %edx
	movq	%rsi, %rbp
	callq	Get_Reference_Block
	movq	%rbp, %rsi
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	decs(%rip), %rax
	movq	32(%rax), %rax
	movq	(%rax), %rcx
	movzwl	(%rcx), %edx
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	(%rdi,%rbp,8), %rbp
	addq	%r15, %rbp
	movw	%dx, -6(%rbp,%r13,8)
	movzwl	2(%rcx), %edx
	movw	%dx, -4(%rbp,%r13,8)
	movzwl	4(%rcx), %edx
	movw	%dx, -2(%rbp,%r13,8)
	movzwl	6(%rcx), %ecx
	movw	%cx, (%rbp,%r13,8)
	movq	8(%rax), %rcx
	movzwl	(%rcx), %edx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rdi,%rbp,8), %rbp
	addq	%r15, %rbp
	movw	%dx, -6(%rbp,%r13,8)
	movzwl	2(%rcx), %edx
	movw	%dx, -4(%rbp,%r13,8)
	movzwl	4(%rcx), %edx
	movw	%dx, -2(%rbp,%r13,8)
	movzwl	6(%rcx), %ecx
	movw	%cx, (%rbp,%r13,8)
	movq	16(%rax), %rcx
	movzwl	(%rcx), %edx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%rdi,%rbp,8), %rbp
	addq	%r15, %rbp
	movw	%dx, -6(%rbp,%r13,8)
	movzwl	2(%rcx), %edx
	movw	%dx, -4(%rbp,%r13,8)
	movzwl	4(%rcx), %edx
	movw	%dx, -2(%rbp,%r13,8)
	movzwl	6(%rcx), %ecx
	movw	%cx, (%rbp,%r13,8)
	movq	24(%rax), %rax
	movzwl	(%rax), %ecx
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	(%rdi,%rdx,8), %rdx
	addq	%r15, %rdx
	movw	%cx, -6(%rdx,%r13,8)
	movzwl	2(%rax), %ecx
	movw	%cx, -4(%rdx,%r13,8)
	movzwl	4(%rax), %ecx
	movw	%cx, -2(%rdx,%r13,8)
	movq	56(%rsp), %rcx          # 8-byte Reload
	movzwl	6(%rax), %eax
	movw	%ax, (%rdx,%r13,8)
	leaq	1(%r12,%r13), %rax
	incq	%r13
	addq	$4, %rbx
	cmpq	%r14, %rax
	jl	.LBB10_38
# BB#39:                                #   in Loop: Header=BB10_37 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	addq	$16, %rdi
	cmpq	72(%rsp), %rsi          # 8-byte Folded Reload
	leaq	1(%rsi), %rsi
	jl	.LBB10_37
	jmp	.LBB10_27
.LBB10_40:
	cmpb	$0, 8(%rsp)             # 1-byte Folded Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	je	.LBB10_45
# BB#41:
	shll	$2, %r13d
	movslq	%r13d, %rcx
	movl	%ecx, %edx
	movl	%ecx, %eax
	orl	$3, %eax
	movslq	%eax, %r12
	orl	$3, %r15d
	movslq	%r15d, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	decq	%rcx
	leaq	144(%rsp), %rbp
	movslq	%r8d, %rax
	leaq	6(%rax,%rax), %r15
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB10_42:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_43 Depth 2
	leal	(,%rsi,4), %eax
	movslq	%eax, %rbx
	leaq	1(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	2(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbx, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	3(%rbx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%rbp, %rbx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB10_43:                              # %.preheader274
                                        #   Parent Loop BB10_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r9,%r14,8), %rdi
	movl	-64(%rbx), %ecx
	movl	(%rbx), %r8d
	movq	decs(%rip), %rax
	movq	32(%rax), %r9
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r13), %edx
	movq	%rsi, %rbp
	callq	Get_Reference_Block
	movq	%rbp, %rsi
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	decs(%rip), %rax
	movq	32(%rax), %rax
	movq	(%rax), %rcx
	movzwl	(%rcx), %edx
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	(%rdi,%rbp,8), %rbp
	addq	%r15, %rbp
	movw	%dx, -6(%rbp,%r13,8)
	movzwl	2(%rcx), %edx
	movw	%dx, -4(%rbp,%r13,8)
	movzwl	4(%rcx), %edx
	movw	%dx, -2(%rbp,%r13,8)
	movzwl	6(%rcx), %ecx
	movw	%cx, (%rbp,%r13,8)
	movq	8(%rax), %rcx
	movzwl	(%rcx), %edx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rdi,%rbp,8), %rbp
	addq	%r15, %rbp
	movw	%dx, -6(%rbp,%r13,8)
	movzwl	2(%rcx), %edx
	movw	%dx, -4(%rbp,%r13,8)
	movzwl	4(%rcx), %edx
	movw	%dx, -2(%rbp,%r13,8)
	movzwl	6(%rcx), %ecx
	movw	%cx, (%rbp,%r13,8)
	movq	16(%rax), %rcx
	movzwl	(%rcx), %edx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%rdi,%rbp,8), %rbp
	addq	%r15, %rbp
	movw	%dx, -6(%rbp,%r13,8)
	movzwl	2(%rcx), %edx
	movw	%dx, -4(%rbp,%r13,8)
	movzwl	4(%rcx), %edx
	movw	%dx, -2(%rbp,%r13,8)
	movzwl	6(%rcx), %ecx
	movw	%cx, (%rbp,%r13,8)
	movq	24(%rax), %rax
	movzwl	(%rax), %ecx
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	(%rdi,%rdx,8), %rdx
	addq	%r15, %rdx
	movw	%cx, -6(%rdx,%r13,8)
	movzwl	2(%rax), %ecx
	movw	%cx, -4(%rdx,%r13,8)
	movzwl	4(%rax), %ecx
	movw	%cx, -2(%rdx,%r13,8)
	movq	56(%rsp), %rcx          # 8-byte Reload
	movzwl	6(%rax), %eax
	movw	%ax, (%rdx,%r13,8)
	leaq	1(%rcx,%r13), %rax
	incq	%r13
	addq	$4, %rbx
	cmpq	%r12, %rax
	jl	.LBB10_43
# BB#44:                                #   in Loop: Header=BB10_42 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	addq	$16, %rbp
	cmpq	72(%rsp), %rsi          # 8-byte Folded Reload
	leaq	1(%rsi), %rsi
	jl	.LBB10_42
	jmp	.LBB10_27
.LBB10_45:                              # %.preheader277
	movslq	%r8d, %r11
	movslq	%r10d, %rbx
	movq	%r11, %rcx
	orq	$1, %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%r11, %rcx
	orq	$2, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r11, %rcx
	orq	$3, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%r11, %rcx
	orq	$4, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%r11, %rcx
	orq	$5, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%r11, %rcx
	orq	$6, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%r11, %rcx
	orq	$7, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%r11, %r13
	orq	$8, %r13
	movq	%r11, %rcx
	orq	$9, %rcx
	leaq	(,%rbx,8), %rdx
	addq	(%r9,%r14,8), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%r11, %rsi
	orq	$10, %rsi
	movq	%r11, %rdi
	orq	$11, %rdi
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r11, %rbp
	orq	$12, %rbp
	movq	%r11, %rbx
	orq	$13, %rbx
	movq	%r11, %r8
	orq	$14, %r8
	movq	%r11, %r12
	orq	$15, %r12
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB10_46:                              # %.preheader276
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r9,8), %r10
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r9,8), %r14
	movzwl	(%r10,%r11,2), %r15d
	movw	%r15w, (%r14,%r11,2)
	movq	(%rsp), %rdx            # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	8(%rsp), %rdx           # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	64(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	56(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	48(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movq	40(%rsp), %rdx          # 8-byte Reload
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r14,%rdx,2)
	movzwl	(%r10,%r13,2), %eax
	movw	%ax, (%r14,%r13,2)
	movzwl	(%r10,%rcx,2), %eax
	movw	%ax, (%r14,%rcx,2)
	movzwl	(%r10,%rsi,2), %eax
	movw	%ax, (%r14,%rsi,2)
	movzwl	(%r10,%rdi,2), %eax
	movw	%ax, (%r14,%rdi,2)
	movzwl	(%r10,%rbp,2), %eax
	movw	%ax, (%r14,%rbp,2)
	movzwl	(%r10,%rbx,2), %eax
	movw	%ax, (%r14,%rbx,2)
	movzwl	(%r10,%r8,2), %eax
	movw	%ax, (%r14,%r8,2)
	movzwl	(%r10,%r12,2), %eax
	movw	%ax, (%r14,%r12,2)
	incq	%r9
	cmpq	$16, %r9
	jne	.LBB10_46
	jmp	.LBB10_27
.Lfunc_end10:
	.size	Conceal_Error, .Lfunc_end10-Conceal_Error
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI10_0:
	.quad	.LBB10_15
	.quad	.LBB10_18
	.quad	.LBB10_20
	.quad	.LBB10_27
	.quad	.LBB10_24

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	height_pad,@object      # @height_pad
	.comm	height_pad,4,4
	.type	width_pad,@object       # @width_pad
	.comm	width_pad,4,4
	.type	height_pad_cr,@object   # @height_pad_cr
	.comm	height_pad_cr,4,4
	.type	width_pad_cr,@object    # @width_pad_cr
	.comm	width_pad_cr,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
