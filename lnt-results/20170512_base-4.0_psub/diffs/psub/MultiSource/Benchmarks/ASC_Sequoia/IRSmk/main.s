	.text
	.file	"main.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$304, %rsp              # imm = 0x130
.Lcfi5:
	.cfi_def_cfa_offset 352
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$.Lstr, %edi
	callq	puts
	cmpl	$2, %ebp
	jne	.LBB0_1
# BB#2:
	movq	8(%rbx), %rdi
	callq	readInput
	movslq	i_ub(%rip), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, %r12
	movslq	x_size(%rip), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, %rbp
	leaq	88(%rsp), %r14
	movq	%r14, %rdi
	callq	allocMem
	leaq	8(%rsp), %r15
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	movq	%r12, %rcx
	callq	init
	movl	$5000, %ebx             # imm = 0x1388
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	movq	%r12, %rcx
	callq	rmatmult3
	decl	%ebx
	jne	.LBB0_3
# BB#4:
	movl	$.Lstr.1, %edi
	callq	puts
	cmpl	$0, i_ub(%rip)
	jle	.LBB0_5
# BB#6:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%eax, %rbp
	movsd	(%r12,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.3, %edi
	movb	$1, %al
	movl	%ebp, %esi
	callq	printf
	movslq	i_ub(%rip), %rcx
	imulq	$1717986919, %rcx, %rax # imm = 0x66666667
	movq	%rax, %rdx
	shrq	$63, %rdx
	sarq	$33, %rax
	addl	%edx, %eax
	addl	%ebp, %eax
	cmpl	%ecx, %eax
	jl	.LBB0_7
	jmp	.LBB0_8
.LBB0_1:
	movq	(%rbx), %rsi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$1, %r14d
	jmp	.LBB0_8
.LBB0_5:
	xorl	%r14d, %r14d
.LBB0_8:                                # %.loopexit
	movl	%r14d, %eax
	addq	$304, %rsp              # imm = 0x130
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"Usage: %s <input>\n"
	.size	.L.str.1, 19

	.type	i_ub,@object            # @i_ub
	.comm	i_ub,4,4
	.type	x_size,@object          # @x_size
	.comm	x_size,4,4
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"i = %10d      b[i] = %e \n"
	.size	.L.str.3, 26

	.type	kmin,@object            # @kmin
	.comm	kmin,4,4
	.type	kmax,@object            # @kmax
	.comm	kmax,4,4
	.type	jmin,@object            # @jmin
	.comm	jmin,4,4
	.type	jmax,@object            # @jmax
	.comm	jmax,4,4
	.type	imin,@object            # @imin
	.comm	imin,4,4
	.type	imax,@object            # @imax
	.comm	imax,4,4
	.type	kp,@object              # @kp
	.comm	kp,4,4
	.type	jp,@object              # @jp
	.comm	jp,4,4
	.type	i_lb,@object            # @i_lb
	.comm	i_lb,4,4
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"\nSequoia Benchmark Version 1.0\n"
	.size	.Lstr, 32

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.1:
	.asciz	"***** results "
	.size	.Lstr.1, 15


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
