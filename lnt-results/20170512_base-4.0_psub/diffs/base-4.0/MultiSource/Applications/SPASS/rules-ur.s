	.text
	.file	"rules-ur.bc"
	.globl	inf_URResolution
	.p2align	4, 0x90
	.type	inf_URResolution,@function
inf_URResolution:                       # @inf_URResolution
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movl	68(%rdi), %eax
	addl	64(%rdi), %eax
	addl	72(%rdi), %eax
	cmpl	$1, %eax
	jne	.LBB0_1
# BB#2:
	movq	56(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB0_4
# BB#3:
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB0_4:                                # %clause_LiteralAtom.exit88
	callq	term_Copy
	movq	%rax, %r13
	xorl	%ebx, %ebx
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movq	%r15, 56(%rsp)          # 8-byte Spill
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	jmp	.LBB0_5
	.p2align	4, 0x90
.LBB0_21:                               # %._crit_edge
                                        #   in Loop: Header=BB0_5 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_24
# BB#22:                                # %._crit_edge
                                        #   in Loop: Header=BB0_5 Depth=1
	movl	(%r13), %eax
	cmpl	%eax, fol_EQUALITY(%rip)
	jne	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_5 Depth=1
	movq	16(%r13), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rax)
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	%rdx, 8(%rax)
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
.LBB0_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
                                        #       Child Loop BB0_8 Depth 3
                                        #       Child Loop BB0_13 Depth 3
                                        #       Child Loop BB0_18 Depth 3
	movq	(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	inf_GetURPartnerLits
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_21
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_5 Depth=1
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_8 Depth 3
                                        #       Child Loop BB0_13 Depth 3
                                        #       Child Loop BB0_18 Depth 3
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	8(%r14), %rbp
	movq	16(%rbp), %rcx
	movq	56(%rcx), %rax
	movl	$-1, %r15d
	.p2align	4, 0x90
.LBB0_8:                                #   Parent Loop BB0_5 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%r15d
	cmpq	%rbp, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB0_8
# BB#9:                                 # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB0_7 Depth=2
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	52(%rcx), %ebx
	movl	%ebx, %edi
	callq	term_StartMaxRenaming
	movq	%r13, %rdi
	callq	term_Rename
	movq	%r13, %rdi
	callq	term_MaxVar
	movl	%eax, %r12d
	cmpl	%r12d, %ebx
	cmovgel	%ebx, %r12d
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	24(%rbp), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB0_11
# BB#10:                                #   in Loop: Header=BB0_7 Depth=2
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB0_11:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB0_7 Depth=2
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%r13, %rcx
	callq	unify_UnifyNoOC
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	32(%rsp), %rsi
	leaq	80(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB0_14
# BB#12:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_7 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph.i
                                        #   Parent Loop BB0_5 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB0_13
.LBB0_14:                               # %cont_Reset.exit
                                        #   in Loop: Header=BB0_7 Depth=2
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	80(%rsp), %rdi
	callq	subst_Delete
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%rbx, 8(%r13)
	movq	$0, (%r13)
	movq	32(%rsp), %rcx
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movq	%r13, %rdx
	movl	%r12d, %r8d
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %r9
	movq	56(%rsp), %r15          # 8-byte Reload
	pushq	%r15
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movq	72(%rsp), %r12          # 8-byte Reload
	pushq	%r12
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	inf_NonUnitURResolution
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_15
# BB#16:                                #   in Loop: Header=BB0_7 Depth=2
	movq	24(%rsp), %rdx          # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB0_20
# BB#17:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB0_7 Depth=2
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB0_18:                               # %.preheader.i
                                        #   Parent Loop BB0_5 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_18
# BB#19:                                #   in Loop: Header=BB0_7 Depth=2
	movq	%rdx, (%rax)
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_15:                               #   in Loop: Header=BB0_7 Depth=2
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB0_20:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB0_7 Depth=2
	movl	$list_PairFree, %esi
	movq	%r13, %rdi
	callq	list_DeleteWithElement
	movq	32(%rsp), %rdi
	callq	subst_Delete
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rbx, %rcx
	movq	%rax, %r14
	movq	40(%rsp), %r13          # 8-byte Reload
	jne	.LBB0_7
	jmp	.LBB0_21
.LBB0_1:
	movl	52(%rdi), %r8d
	movl	$-1, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbp, %r9
	pushq	%r15
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	inf_NonUnitURResolution
	addq	$104, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB0_25
.LBB0_24:
	movq	%r13, %rdi
	callq	term_Delete
	movq	%rbx, %rax
	addq	$88, %rsp
.LBB0_25:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	inf_URResolution, .Lfunc_end0-inf_URResolution
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_NonUnitURResolution,@function
inf_NonUnitURResolution:                # @inf_NonUnitURResolution
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 112
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movl	%esi, %r13d
	movq	%rdi, %r14
	movl	64(%r14), %ecx
	movl	68(%r14), %esi
	movl	72(%r14), %edx
	leal	(%rcx,%rsi), %eax
	addl	%edx, %eax
	decl	%eax
	js	.LBB1_1
# BB#2:                                 # %.lr.ph.i39
	movl	%r13d, %r15d
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	cmpq	%rbx, %r15
	je	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%rbp, (%rax)
	movl	64(%r14), %ecx
	movl	68(%r14), %esi
	movl	72(%r14), %edx
	movq	%rax, %rbp
.LBB1_5:                                #   in Loop: Header=BB1_3 Depth=1
	leal	(%rdx,%rsi), %eax
	leal	-1(%rcx,%rax), %eax
	cltq
	cmpq	%rax, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB1_3
	jmp	.LBB1_6
.LBB1_1:
	xorl	%ebp, %ebp
.LBB1_6:                                # %clause_GetLiteralListExcept.exit
	addl	%ecx, %esi
	addl	%edx, %esi
	movl	%esi, %eax
	decl	%eax
	js	.LBB1_7
# BB#8:                                 # %.lr.ph
	movl	%r13d, %eax
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_14 Depth 2
	cmpq	%rax, %r15
	je	.LBB1_17
# BB#10:                                #   in Loop: Header=BB1_9 Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%r15,8), %rsi
	movq	%rbp, %rdi
	callq	list_PointerDeleteOneElement
	movq	%rax, %r12
	subq	$8, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	movq	%r14, %rdi
	movl	%r15d, %esi
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %rcx
	movq	48(%rsp), %r8           # 8-byte Reload
	movl	20(%rsp), %r9d          # 4-byte Reload
	pushq	128(%rsp)
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	72(%rsp)                # 8-byte Folded Reload
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	callq	inf_SearchURResolvents
	addq	$32, %rsp
.Lcfi36:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB1_11
# BB#12:                                #   in Loop: Header=BB1_9 Depth=1
	testq	%rbx, %rbx
	je	.LBB1_16
# BB#13:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB1_9 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB1_14:                               # %.preheader.i
                                        #   Parent Loop BB1_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_14
# BB#15:                                #   in Loop: Header=BB1_9 Depth=1
	movq	%rbx, (%rax)
	jmp	.LBB1_16
.LBB1_11:                               #   in Loop: Header=BB1_9 Depth=1
	movq	%rbx, %r13
.LBB1_16:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB1_9 Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%r15,8), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	%r12, (%rbp)
	movq	%r13, %rbx
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB1_17:                               #   in Loop: Header=BB1_9 Depth=1
	incq	%r15
	cmpq	%rsi, %r15
	jne	.LBB1_9
	jmp	.LBB1_18
.LBB1_7:
	xorl	%ebx, %ebx
.LBB1_18:                               # %._crit_edge
	testq	%rbp, %rbp
	je	.LBB1_20
	.p2align	4, 0x90
.LBB1_19:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB1_19
.LBB1_20:                               # %list_Delete.exit
	movq	%rbx, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	inf_NonUnitURResolution, .Lfunc_end1-inf_NonUnitURResolution
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_GetURPartnerLits,@function
inf_GetURPartnerLits:                   # @inf_GetURPartnerLits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 64
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %rax
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%rcx, %rsi
	movq	%rax, %rcx
	callq	st_GetUnifier
	movq	%rax, %r12
	xorl	%r13d, %r13d
	testq	%r12, %r12
	jne	.LBB2_1
	jmp	.LBB2_19
	.p2align	4, 0x90
.LBB2_2:                                #   in Loop: Header=BB2_1 Depth=1
	callq	sharing_NAtomDataList
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_18
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_1 Depth=1
	testl	%r14d, %r14d
	je	.LBB2_11
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph.split.us
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rbp
	movq	16(%rbp), %rcx
	movq	24(%rbp), %rsi
	movl	68(%rcx), %eax
	addl	64(%rcx), %eax
	addl	72(%rcx), %eax
	movq	24(%r15), %rcx
	movl	(%rcx), %edx
	movl	fol_NOT(%rip), %ecx
	movl	(%rsi), %esi
	cmpl	%edx, %ecx
	jne	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=2
	cmpl	%edx, %esi
	movl	%edx, %esi
	jne	.LBB2_8
.LBB2_6:                                # %clause_LiteralsAreComplementary.exit.us
                                        #   in Loop: Header=BB2_4 Depth=2
	cmpl	%edx, %ecx
	je	.LBB2_10
# BB#7:                                 # %clause_LiteralsAreComplementary.exit.us
                                        #   in Loop: Header=BB2_4 Depth=2
	cmpl	%esi, %ecx
	jne	.LBB2_10
.LBB2_8:                                # %clause_LiteralsAreComplementary.exit.thread.us
                                        #   in Loop: Header=BB2_4 Depth=2
	cmpl	$1, %eax
	jne	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_4 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, %r13
.LBB2_10:                               #   in Loop: Header=BB2_4 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_4
	jmp	.LBB2_18
	.p2align	4, 0x90
.LBB2_11:                               # %.lr.ph.split.split.us
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rbp
	movq	16(%rbp), %rcx
	movq	24(%rbp), %rsi
	movl	68(%rcx), %eax
	addl	64(%rcx), %eax
	addl	72(%rcx), %eax
	movq	24(%r15), %rcx
	movl	(%rcx), %edx
	movl	fol_NOT(%rip), %ecx
	movl	(%rsi), %esi
	cmpl	%edx, %ecx
	jne	.LBB2_13
# BB#12:                                #   in Loop: Header=BB2_11 Depth=2
	cmpl	%edx, %esi
	movl	%edx, %esi
	jne	.LBB2_15
.LBB2_13:                               # %clause_LiteralsAreComplementary.exit.us23
                                        #   in Loop: Header=BB2_11 Depth=2
	cmpl	%edx, %ecx
	je	.LBB2_17
# BB#14:                                # %clause_LiteralsAreComplementary.exit.us23
                                        #   in Loop: Header=BB2_11 Depth=2
	cmpl	%esi, %ecx
	jne	.LBB2_17
.LBB2_15:                               # %clause_LiteralsAreComplementary.exit.thread.us26
                                        #   in Loop: Header=BB2_11 Depth=2
	cmpl	$1, %eax
	je	.LBB2_17
# BB#16:                                #   in Loop: Header=BB2_11 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, %r13
.LBB2_17:                               #   in Loop: Header=BB2_11 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_11
	jmp	.LBB2_18
	.p2align	4, 0x90
.LBB2_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
                                        #     Child Loop BB2_11 Depth 2
	movq	8(%r12), %rdi
	cmpl	$0, (%rdi)
	jle	.LBB2_2
.LBB2_18:                               # %.loopexit
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB2_1
.LBB2_19:                               # %._crit_edge
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	inf_GetURPartnerLits, .Lfunc_end2-inf_GetURPartnerLits
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_SearchURResolvents,@function
inf_SearchURResolvents:                 # @inf_SearchURResolvents
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi56:
	.cfi_def_cfa_offset 144
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movq	%r8, %r13
	movq	%rdx, %r12
	movl	%esi, %r15d
	movq	%rdi, %r14
	testq	%rcx, %rcx
	je	.LBB3_1
# BB#43:
	movq	%rcx, %rdi
	callq	list_Copy
	movl	$clause_HyperLiteralIsBetter, %ecx
	movq	%rax, %rdi
	movq	%r13, %rsi
	movl	%ebp, %edx
	callq	clause_MoveBestLiteralToFront
	movq	(%rax), %rbx
	movq	8(%rax), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	24(%rsi), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movl	%ebp, 52(%rsp)          # 4-byte Spill
	jne	.LBB3_45
# BB#44:
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB3_45:                               # %clause_LiteralAtom.exit
	callq	term_Copy
	movq	%r13, 80(%rsp)          # 8-byte Spill
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rsi
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movl	%r15d, 48(%rsp)         # 4-byte Spill
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	jmp	.LBB3_46
	.p2align	4, 0x90
.LBB3_47:                               #   in Loop: Header=BB3_46 Depth=1
	movq	(%rsp), %r14            # 8-byte Reload
.LBB3_61:                               # %._crit_edge
                                        #   in Loop: Header=BB3_46 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	jne	.LBB3_64
# BB#62:                                # %._crit_edge
                                        #   in Loop: Header=BB3_46 Depth=1
	movl	(%rsi), %eax
	cmpl	%eax, fol_EQUALITY(%rip)
	jne	.LBB3_64
# BB#63:                                #   in Loop: Header=BB3_46 Depth=1
	movq	16(%rsi), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rax)
	movq	16(%rsi), %rax
	movq	(%rax), %rax
	movq	%rdx, 8(%rax)
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
	movq	%r14, (%rsp)            # 8-byte Spill
.LBB3_46:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_48 Depth 2
                                        #       Child Loop BB3_53 Depth 3
                                        #       Child Loop BB3_58 Depth 3
	movq	144(%rsp), %rax
	movq	(%rax), %rcx
	movl	$1, %edx
	movq	%rsi, %rdi
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	inf_GetURPartnerLits
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB3_47
	.p2align	4, 0x90
.LBB3_48:                               # %.lr.ph
                                        #   Parent Loop BB3_46 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_53 Depth 3
                                        #       Child Loop BB3_58 Depth 3
	movq	8(%r13), %r15
	movq	24(%r15), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB3_50
# BB#49:                                #   in Loop: Header=BB3_48 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB3_50:                               # %clause_LiteralAtom.exit86
                                        #   in Loop: Header=BB3_48 Depth=2
	callq	term_Copy
	movq	%rax, %rbp
	movl	52(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %edi
	callq	term_StartMaxRenaming
	movq	%rbp, %rdi
	callq	term_Rename
	movq	%rbp, %rdi
	callq	term_MaxVar
	movl	%eax, %r14d
	cmpl	%ebx, %r14d
	cmovll	%ebx, %r14d
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rcx
	callq	unify_UnifyNoOC
	testl	%eax, %eax
	je	.LBB3_67
# BB#51:                                #   in Loop: Header=BB3_48 Depth=2
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	24(%rsp), %rsi
	leaq	40(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB3_54
# BB#52:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB3_48 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB3_53:                               # %.lr.ph.i88
                                        #   Parent Loop BB3_46 Depth=1
                                        #     Parent Loop BB3_48 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB3_53
.LBB3_54:                               # %cont_Reset.exit
                                        #   in Loop: Header=BB3_48 Depth=2
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	40(%rsp), %rdi
	callq	subst_Delete
	movq	%rbp, %rdi
	callq	term_Delete
	movq	24(%rsp), %rbp
	movq	%rbp, 40(%rsp)
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	subst_Copy
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	subst_Compose
	movq	%rax, 24(%rsp)
	movq	40(%rsp), %rdi
	callq	subst_Delete
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rbx)
	movq	%r15, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbp)
	movq	24(%rsp), %r8
	subq	$8, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	56(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %esi
	movq	%rbp, %rdx
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	movl	%r14d, %r9d
	pushq	168(%rsp)
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	callq	inf_SearchURResolvents
	addq	$32, %rsp
.Lcfi67:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB3_55
# BB#56:                                #   in Loop: Header=BB3_48 Depth=2
	movq	(%rsp), %rdx            # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB3_60
# BB#57:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB3_48 Depth=2
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB3_58:                               # %.preheader.i
                                        #   Parent Loop BB3_46 Depth=1
                                        #     Parent Loop BB3_48 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB3_58
# BB#59:                                #   in Loop: Header=BB3_48 Depth=2
	movq	%rdx, (%rax)
	jmp	.LBB3_60
	.p2align	4, 0x90
.LBB3_55:                               #   in Loop: Header=BB3_48 Depth=2
	movq	(%rsp), %r14            # 8-byte Reload
.LBB3_60:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB3_48 Depth=2
	movq	8(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	(%rbp), %r12
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbp, (%rax)
	movq	24(%rsp), %rdi
	callq	subst_Delete
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	movq	%r14, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	jne	.LBB3_48
	jmp	.LBB3_61
.LBB3_64:
	movq	%rsi, %rdi
	callq	term_Delete
	testq	%rbx, %rbx
	je	.LBB3_66
	.p2align	4, 0x90
.LBB3_65:                               # %.lr.ph.i90
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB3_65
	jmp	.LBB3_66
.LBB3_1:
	movq	56(%r14), %rax
	movslq	%r15d, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB3_3
# BB#2:
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB3_3:                                # %clause_GetLiteralAtom.exit.i
	callq	term_Copy
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	$0, (%rbx)
	movl	64(%r14), %eax
	decl	%eax
	cmpl	%r15d, %eax
	jge	.LBB3_4
# BB#5:
	addl	68(%r14), %eax
	xorl	%edi, %edi
	cmpl	%r15d, %eax
	jge	.LBB3_6
# BB#7:
	xorl	%esi, %esi
	movq	%rbx, %rdx
	jmp	.LBB3_8
.LBB3_4:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	jmp	.LBB3_8
.LBB3_6:
	xorl	%edx, %edx
	movq	%rbx, %rsi
.LBB3_8:
	movq	152(%rsp), %rcx
	movq	160(%rsp), %r8
	callq	clause_Create
	movq	%rax, %r13
	testq	%rbx, %rbx
	je	.LBB3_10
	.p2align	4, 0x90
.LBB3_9:                                # %.lr.ph.i82.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB3_9
.LBB3_10:                               # %list_Delete.exit83.i
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%r14, 8(%r15)
	movq	$0, (%r15)
	movl	8(%r14), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	%r12, %rax
	testq	%rax, %rax
	je	.LBB3_17
# BB#11:                                # %.lr.ph.i
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movabsq	$4294967296, %r14       # imm = 0x100000000
	.p2align	4, 0x90
.LBB3_12:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_13 Depth 2
                                        #     Child Loop BB3_15 Depth 2
	movq	%r15, %r12
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rax), %rax
	movq	(%rax), %rbp
	movq	16(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	%r12, (%r15)
	movl	8(%rbx), %esi
	movl	(%rsp), %edi            # 4-byte Reload
	callq	misc_Max
	movl	%eax, (%rsp)            # 4-byte Spill
	movslq	(%rbx), %rbx
	movq	32(%r13), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, 32(%r13)
	movq	16(%rbp), %rax
	movq	56(%rax), %rax
	movabsq	$-4294967296, %r12      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB3_13:                               #   Parent Loop BB3_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%r14, %r12
	cmpq	%rbp, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB3_13
# BB#14:                                # %clause_LiteralGetIndex.exit78.i
                                        #   in Loop: Header=BB3_12 Depth=1
	sarq	$32, %r12
	movq	40(%r13), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 40(%r13)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	movq	8(%rax), %rbp
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	(%rax), %rbx
	movq	32(%r13), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, 32(%r13)
	movq	16(%rbp), %rax
	movq	56(%rax), %rax
	movabsq	$-4294967296, %rbx      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB3_15:                               #   Parent Loop BB3_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%r14, %rbx
	cmpq	%rbp, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB3_15
# BB#16:                                # %clause_LiteralGetIndex.exit.i
                                        #   in Loop: Header=BB3_12 Depth=1
	sarq	$32, %rbx
	movq	40(%r13), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 40(%r13)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB3_12
.LBB3_17:                               # %._crit_edge.i
	movl	$12, 76(%r13)
	movl	(%rsp), %eax            # 4-byte Reload
	incl	%eax
	movl	%eax, 8(%r13)
	movl	24(%r13), %ecx
	testq	%r15, %r15
	je	.LBB3_34
# BB#18:                                # %.lr.ph60.i.i
	movl	%ecx, %ebx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB3_19:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rdx
	testb	$8, 48(%rdx)
	je	.LBB3_21
# BB#20:                                #   in Loop: Header=BB3_19 Depth=1
	orb	$8, 48(%r13)
.LBB3_21:                               #   in Loop: Header=BB3_19 Depth=1
	movl	12(%rdx), %esi
	cmpl	12(%r13), %esi
	movq	%r13, %rsi
	cmovaq	%rdx, %rsi
	movl	12(%rsi), %esi
	movl	%esi, 12(%r13)
	movl	24(%rdx), %edx
	cmpl	%edx, %ebx
	cmovbl	%edx, %ebx
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB3_19
# BB#22:                                # %._crit_edge61.i.i
	cmpl	%ecx, %ebx
	jbe	.LBB3_34
# BB#23:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB3_31
# BB#24:
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB3_25
# BB#30:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB3_31
.LBB3_34:                               # %.preheader51.i.i
	testl	%ecx, %ecx
	jne	.LBB3_32
	jmp	.LBB3_35
.LBB3_25:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rdx, %rbp
	movq	%r8, (%rbp)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB3_27
# BB#26:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB3_27:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB3_29
# BB#28:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB3_29:
	addq	$-16, %rdi
	callq	free
.LBB3_31:                               # %.preheader51.thread.i.i
	leal	(,%rbx,8), %edi
	callq	memory_Malloc
	movq	%rax, 16(%r13)
	movl	%ebx, 24(%r13)
.LBB3_32:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_33:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r13), %rcx
	movl	%eax, %edx
	movq	$0, (%rcx,%rdx,8)
	incl	%eax
	cmpl	24(%r13), %eax
	jb	.LBB3_33
.LBB3_35:                               # %.preheader.i.i
	testq	%r15, %r15
	je	.LBB3_42
# BB#36:                                # %.lr.ph54.i.i
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB3_38:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_40 Depth 2
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	cmpl	$0, 24(%rcx)
	je	.LBB3_37
# BB#39:                                # %.lr.ph.i69.i
                                        #   in Loop: Header=BB3_38 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_40:                               #   Parent Loop BB3_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %rsi
	movl	%edx, %edi
	movq	16(%rcx), %rbp
	movq	(%rbp,%rdi,8), %rbp
	orq	%rbp, (%rsi,%rdi,8)
	incl	%edx
	cmpl	24(%rcx), %edx
	jb	.LBB3_40
.LBB3_37:                               # %.loopexit.i.i
                                        #   in Loop: Header=BB3_38 Depth=1
	testq	%rax, %rax
	jne	.LBB3_38
	.p2align	4, 0x90
.LBB3_41:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB3_41
.LBB3_42:                               # %inf_CreateURUnitResolvent.exit
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r13, 8(%r14)
	movq	$0, (%r14)
.LBB3_66:
	movq	%r14, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_67:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$214, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end3:
	.size	inf_SearchURResolvents, .Lfunc_end3-inf_SearchURResolvents
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_HyperLiteralIsBetter,@function
clause_HyperLiteralIsBetter:            # @clause_HyperLiteralIsBetter
	.cfi_startproc
# BB#0:
	cmpl	%esi, %ecx
	sbbl	%eax, %eax
	andl	$1, %eax
	retq
.Lfunc_end4:
	.size	clause_HyperLiteralIsBetter, .Lfunc_end4-clause_HyperLiteralIsBetter
	.cfi_endproc

	.p2align	4, 0x90
	.type	list_PairFree,@function
list_PairFree:                          # @list_PairFree
	.cfi_startproc
# BB#0:
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdi)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rdi, (%rax)
	retq
.Lfunc_end5:
	.size	list_PairFree, .Lfunc_end5-list_PairFree
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str, 31

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/SPASS/rules-ur.c"
	.size	.L.str.1, 80

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n In inf_SearchURResolvents: Unification failed."
	.size	.L.str.2, 49

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.3, 133

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n\n"
	.size	.L.str.4, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
