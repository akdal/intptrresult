	.text
	.file	"main.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	8                       # 0x8
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	1                       # 0x1
.LCPI0_1:
	.long	128                     # 0x80
	.long	64                      # 0x40
	.long	32                      # 0x20
	.long	16                      # 0x10
.LCPI0_2:
	.long	2048                    # 0x800
	.long	1024                    # 0x400
	.long	512                     # 0x200
	.long	256                     # 0x100
.LCPI0_3:
	.long	32768                   # 0x8000
	.long	16384                   # 0x4000
	.long	8192                    # 0x2000
	.long	4096                    # 0x1000
.LCPI0_4:
	.long	524288                  # 0x80000
	.long	262144                  # 0x40000
	.long	131072                  # 0x20000
	.long	65536                   # 0x10000
.LCPI0_5:
	.long	8388608                 # 0x800000
	.long	4194304                 # 0x400000
	.long	2097152                 # 0x200000
	.long	1048576                 # 0x100000
.LCPI0_6:
	.long	134217728               # 0x8000000
	.long	67108864                # 0x4000000
	.long	33554432                # 0x2000000
	.long	16777216                # 0x1000000
.LCPI0_7:
	.long	2147483648              # 0x80000000
	.long	1073741824              # 0x40000000
	.long	536870912               # 0x20000000
	.long	268435456               # 0x10000000
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:                                 # %initial_value.exit247
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi6:
	.cfi_def_cfa_offset 336
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movl	%edi, %r13d
	movl	$0, NOUPPER(%rip)
	movl	$0, NOPROMPT(%rip)
	movl	$0, BESTMATCH(%rip)
	movl	$0, FNAME(%rip)
	movl	$0, REGEX(%rip)
	movl	$0, JUMP(%rip)
	movl	$0, SGREP(%rip)
	movl	$0, WHOLELINE(%rip)
	movl	$0, LINENUM(%rip)
	movl	$0, COUNT(%rip)
	movl	$0, OUTTAIL(%rip)
	movl	$0, TRUNCATE(%rip)
	movl	$0, AND(%rip)
	movl	$0, INVERSE(%rip)
	movl	$0, EATFIRST(%rip)
	movl	$1, FIRSTOUTPUT(%rip)
	movl	$1, NOMATCH(%rip)
	movl	$1, FIRST_IN_RE(%rip)
	movl	$1, S(%rip)
	movl	$1, DD(%rip)
	movl	$1, I(%rip)
	movl	$1, TAIL(%rip)
	movl	$1, HEAD(%rip)
	movl	$2, D_length(%rip)
	movl	$0, num_of_matched(%rip)
	movl	$0, SIMPLEPATTERN(%rip)
	movl	$0, PSIZE(%rip)
	movl	$0, Num_Pat(%rip)
	movl	$0, SILENT(%rip)
	movl	$0, RE_ERR(%rip)
	movl	$0, DELIMITER(%rip)
	movl	$0, WORDBOUND(%rip)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [8,4,2,1]
	movups	%xmm0, Bit+116(%rip)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [128,64,32,16]
	movups	%xmm0, Bit+100(%rip)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [2048,1024,512,256]
	movups	%xmm0, Bit+84(%rip)
	movaps	.LCPI0_3(%rip), %xmm0   # xmm0 = [32768,16384,8192,4096]
	movups	%xmm0, Bit+68(%rip)
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [524288,262144,131072,65536]
	movups	%xmm0, Bit+52(%rip)
	movaps	.LCPI0_5(%rip), %xmm0   # xmm0 = [8388608,4194304,2097152,1048576]
	movups	%xmm0, Bit+36(%rip)
	movaps	.LCPI0_6(%rip), %xmm0   # xmm0 = [134217728,67108864,33554432,16777216]
	movups	%xmm0, Bit+20(%rip)
	movaps	.LCPI0_7(%rip), %xmm0   # xmm0 = [2147483648,1073741824,536870912,268435456]
	movups	%xmm0, Bit+4(%rip)
	movl	$Mask, %edi
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	callq	memset
	movw	$112, Progname+4(%rip)
	movl	$1701996385, Progname(%rip) # imm = 0x65726761
	cmpl	$1, %r13d
	jle	.LBB0_154
# BB#1:                                 # %.lr.ph217
	movb	$0, 16(%rsp)
	leal	-1(%r13), %r14d
                                        # implicit-def: %EAX
	movl	%eax, 4(%rsp)           # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB0_43
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph702.preheader
                                        #   in Loop: Header=BB0_43 Depth=1
	addq	$16, %r12
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph702
                                        #   Parent Loop BB0_43 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	1(%rbp), %r15d
	movb	%r15b, 3(%rsp)
	leal	-66(%r15), %eax
	cmpl	$55, %eax
	ja	.LBB0_12
# BB#4:                                 # %.lr.ph702
                                        #   in Loop: Header=BB0_3 Depth=2
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_5:                                #   in Loop: Header=BB0_3 Depth=2
	cmpl	$2, %r13d
	je	.LBB0_150
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=2
	leal	-2(%r13), %ebx
	movl	$1, CONSTANT(%rip)
	movq	(%r12), %rsi
	leaq	16(%rsp), %rdi
	callq	strcat
	cmpl	$2, %ebx
	jl	.LBB0_45
# BB#7:                                 #   in Loop: Header=BB0_3 Depth=2
	movq	8(%r12), %rax
	cmpb	$45, (%rax)
	je	.LBB0_152
# BB#8:                                 # %.backedge142.thread
                                        #   in Loop: Header=BB0_3 Depth=2
	movl	%r13d, %r14d
	addl	$-3, %r14d
	movq	8(%r12), %rbp
	addq	$16, %r12
	cmpb	$45, (%rbp)
	movl	%ebx, %r13d
	je	.LBB0_3
	jmp	.LBB0_46
.LBB0_9:                                #   in Loop: Header=BB0_43 Depth=1
	movl	$0, I(%rip)
	jmp	.LBB0_38
.LBB0_10:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, OUTTAIL(%rip)
	jmp	.LBB0_38
.LBB0_11:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, BESTMATCH(%rip)
	jmp	.LBB0_38
.LBB0_12:                               #   in Loop: Header=BB0_43 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%r15d, %rcx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB0_169
# BB#13:                                #   in Loop: Header=BB0_43 Depth=1
	incq	%rbp
	movl	$1, APPROX(%rip)
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strtol
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpl	$8, %eax
	jle	.LBB0_38
	jmp	.LBB0_170
.LBB0_14:                               #   in Loop: Header=BB0_43 Depth=1
	addq	$2, %rbp
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strtol
	movl	%eax, DD(%rip)
	movl	$1, JUMP(%rip)
	jmp	.LBB0_38
.LBB0_15:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, FILEOUT(%rip)
.LBB0_16:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, COUNT(%rip)
	jmp	.LBB0_38
.LBB0_17:                               #   in Loop: Header=BB0_43 Depth=1
	addq	$2, %rbp
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strtol
	movl	%eax, I(%rip)
	movl	$1, JUMP(%rip)
	jmp	.LBB0_38
.LBB0_18:                               #   in Loop: Header=BB0_43 Depth=1
	addq	$2, %rbp
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strtol
	movl	%eax, S(%rip)
	movl	$1, JUMP(%rip)
	jmp	.LBB0_38
.LBB0_19:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, DELIMITER(%rip)
	cmpl	$2, %r13d
	jle	.LBB0_154
# BB#20:                                #   in Loop: Header=BB0_43 Depth=1
	cmpb	$0, 2(%rbp)
	je	.LBB0_40
# BB#21:                                #   in Loop: Header=BB0_43 Depth=1
	addq	$2, %rbp
	movq	%rbp, %rdi
	callq	strlen
	movl	%eax, D_length(%rip)
	cmpl	$17, %eax
	jge	.LBB0_173
# BB#22:                                #   in Loop: Header=BB0_43 Depth=1
	addq	$-8, %r12
	movb	$60, D_pattern(%rip)
	movl	$D_pattern+1, %edi
	movq	%rbp, %rsi
	callq	strcpy
	jmp	.LBB0_42
.LBB0_23:                               #   in Loop: Header=BB0_43 Depth=1
	cmpl	$2, %r13d
	je	.LBB0_171
# BB#24:                                #   in Loop: Header=BB0_43 Depth=1
	addl	$-2, %r13d
	movq	(%r12), %rsi
	cmpb	$45, (%rsi)
	jne	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_43 Depth=1
	movb	$92, 16(%rsp)
.LBB0_26:                               #   in Loop: Header=BB0_43 Depth=1
	leaq	16(%rsp), %rdi
	callq	strcat
	movl	%r13d, %r14d
	jmp	.LBB0_39
.LBB0_27:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, PAT_FILE(%rip)
	movq	(%r12), %rdi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, 4(%rsp)           # 4-byte Spill
	testl	%eax, %eax
	js	.LBB0_172
# BB#28:                                #   in Loop: Header=BB0_43 Depth=1
	addl	$-2, %r13d
	movl	%r13d, %r14d
	jmp	.LBB0_39
.LBB0_29:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, NOFILENAME(%rip)
	jmp	.LBB0_38
.LBB0_30:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, NOUPPER(%rip)
	jmp	.LBB0_38
.LBB0_31:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, LINENUM(%rip)
	jmp	.LBB0_38
.LBB0_32:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, SILENT(%rip)
	jmp	.LBB0_38
.LBB0_33:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, INVERSE(%rip)
	jmp	.LBB0_38
.LBB0_34:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, WORDBOUND(%rip)
	cmpl	$0, WHOLELINE(%rip)
	je	.LBB0_38
	jmp	.LBB0_168
.LBB0_35:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, NOPROMPT(%rip)
	jmp	.LBB0_38
.LBB0_36:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, FILENAMEONLY(%rip)
	jmp	.LBB0_38
.LBB0_37:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$1, WHOLELINE(%rip)
	cmpl	$0, WORDBOUND(%rip)
	jne	.LBB0_168
	.p2align	4, 0x90
.LBB0_38:                               # %.backedge142.loopexit
                                        #   in Loop: Header=BB0_43 Depth=1
	addq	$-8, %r12
.LBB0_39:                               # %.backedge142
                                        #   in Loop: Header=BB0_43 Depth=1
	movl	%r14d, %r13d
	leal	-1(%r13), %r14d
	cmpl	$1, %r13d
	jg	.LBB0_43
	jmp	.LBB0_47
.LBB0_40:                               #   in Loop: Header=BB0_43 Depth=1
	movq	(%r12), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movl	%eax, D_length(%rip)
	cmpl	$17, %eax
	jge	.LBB0_173
# BB#41:                                #   in Loop: Header=BB0_43 Depth=1
	movb	$60, D_pattern(%rip)
	movl	$D_pattern+1, %edi
	movq	%rbp, %rsi
	callq	strcpy
	addl	$-2, %r13d
	movl	%r13d, %r14d
.LBB0_42:                               #   in Loop: Header=BB0_43 Depth=1
	movl	$D_pattern, %edi
	callq	strlen
	movl	$2112318, D_pattern(%rax) # imm = 0x203B3E
	incl	D_length(%rip)
	jmp	.LBB0_39
	.p2align	4, 0x90
.LBB0_43:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_3 Depth 2
	movq	8(%r12), %rbp
	cmpb	$45, (%rbp)
	je	.LBB0_2
# BB#44:
	addq	$8, %r12
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB0_48
	jmp	.LBB0_49
.LBB0_45:
	xorl	%r14d, %r14d
	movl	$1, %r13d
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB0_48
	jmp	.LBB0_49
.LBB0_46:                               # %.backedge142.thread..critedge.loopexit_crit_edge
	addq	$-8, %r12
	leal	-1(%rbx), %r14d
	movl	%ebx, %r13d
.LBB0_47:                               # %.critedge
	cmpl	$0, FILENAMEONLY(%rip)
	je	.LBB0_49
.LBB0_48:                               # %.critedge
	movl	NOFILENAME(%rip), %eax
	testl	%eax, %eax
	jne	.LBB0_141
.LBB0_49:
	cmpl	$0, COUNT(%rip)
	je	.LBB0_53
.LBB0_50:
	movl	NOFILENAME(%rip), %eax
	orl	FILENAMEONLY(%rip), %eax
	je	.LBB0_53
# BB#51:
	movl	$0, FILENAMEONLY(%rip)
	cmpl	$0, FILEOUT(%rip)
	jne	.LBB0_53
# BB#52:
	movl	$0, NOFILENAME(%rip)
.LBB0_53:
	cmpl	$0, PAT_FILE(%rip)
	jne	.LBB0_57
# BB#54:
	movb	16(%rsp), %al
	testb	%al, %al
	jne	.LBB0_57
# BB#55:
	testl	%r14d, %r14d
	je	.LBB0_159
# BB#56:
	movq	(%r12), %rsi
	leaq	16(%rsp), %rdi
	callq	strcpy
	addl	$-2, %r13d
	addq	$8, %r12
	movl	%r13d, %r14d
.LBB0_57:
	movl	$0, Numfiles(%rip)
	testl	%r14d, %r14d
	je	.LBB0_65
# BB#58:
	movslq	%r14d, %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, Textfiles(%rip)
	testq	%rax, %rax
	jne	.LBB0_60
	jmp	.LBB0_149
.LBB0_59:                               #   in Loop: Header=BB0_60 Depth=1
	movq	stderr(%rip), %rdi
	movq	(%r12), %rcx
	movl	$.L.str.12, %esi
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB0_63
	.p2align	4, 0x90
.LBB0_60:                               # %.lr.ph211
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdi
	callq	check_file
	cmpl	$-3, %eax
	je	.LBB0_59
# BB#61:                                #   in Loop: Header=BB0_60 Depth=1
	movq	(%r12), %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	Textfiles(%rip), %rdx
	movslq	Numfiles(%rip), %rcx
	movq	%rax, (%rdx,%rcx,8)
	testq	%rax, %rax
	je	.LBB0_149
# BB#62:                                #   in Loop: Header=BB0_60 Depth=1
	movq	Textfiles(%rip), %rax
	leal	1(%rcx), %edx
	movl	%edx, Numfiles(%rip)
	movq	(%rax,%rcx,8), %rdi
	movq	(%r12), %rsi
	callq	strcpy
.LBB0_63:                               # %.backedge
                                        #   in Loop: Header=BB0_60 Depth=1
	decl	%r14d
	addq	$8, %r12
	testl	%r14d, %r14d
	jne	.LBB0_60
# BB#64:
	movl	$3, %ebp
	jmp	.LBB0_66
.LBB0_65:
	xorl	%ebp, %ebp
.LBB0_66:                               # %.loopexit
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	%r14d, %esi
	callq	checksg
	leaq	144(%rsp), %rdi
	movq	%rbx, %rsi
	callq	strcpy
	cmpl	$0, SGREP(%rip)
	je	.LBB0_68
# BB#67:
	leaq	144(%rsp), %rdi
	callq	strlen
	movq	%rax, %r13
	cmpl	$0, PAT_FILE(%rip)
	jne	.LBB0_69
	jmp	.LBB0_70
.LBB0_68:
	leaq	16(%rsp), %rbx
	movl	$D_pattern, %edi
	movq	%rbx, %rsi
	callq	preprocess
	movl	$old_D_pat, %edi
	movl	$D_pattern, %esi
	callq	strcpy
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	maskgen
	movl	%eax, %r13d
	cmpl	$0, PAT_FILE(%rip)
	je	.LBB0_70
.LBB0_69:
	movl	4(%rsp), %edi           # 4-byte Reload
	callq	prepf
.LBB0_70:
	cmpl	$2, Numfiles(%rip)
	jl	.LBB0_72
# BB#71:
	movl	$1, FNAME(%rip)
.LBB0_72:
	cmpl	$0, NOFILENAME(%rip)
	je	.LBB0_74
# BB#73:
	movl	$0, FNAME(%rip)
.LBB0_74:
	movl	$0, num_of_matched(%rip)
	callq	compat
	testl	%ebp, %ebp
	je	.LBB0_155
# BB#75:                                # %.preheader140
	cmpl	$0, Numfiles(%rip)
	jle	.LBB0_102
# BB#76:                                # %.lr.ph208.preheader
	leaq	16(%rsp), %r15
	leaq	144(%rsp), %r12
	xorl	%ebx, %ebx
	jmp	.LBB0_78
.LBB0_77:                               #   in Loop: Header=BB0_78 Depth=1
	movq	stderr(%rip), %rdi
	movq	Textfiles(%rip), %rax
	movq	(%rax,%rbx,8), %rcx
	movl	$.L.str.15, %esi
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB0_101
	.p2align	4, 0x90
.LBB0_78:                               # %.lr.ph208
                                        # =>This Inner Loop Header: Depth=1
	movq	Textfiles(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	movl	$CurrentFileName, %edi
	callq	strcpy
	movq	Textfiles(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jle	.LBB0_77
# BB#79:                                #   in Loop: Header=BB0_78 Depth=1
	cmpl	$0, PAT_FILE(%rip)
	je	.LBB0_81
# BB#80:                                #   in Loop: Header=BB0_78 Depth=1
	movl	%ebp, %edi
	callq	mgrep
	jmp	.LBB0_84
	.p2align	4, 0x90
.LBB0_81:                               #   in Loop: Header=BB0_78 Depth=1
	cmpl	$0, SGREP(%rip)
	je	.LBB0_83
# BB#82:                                #   in Loop: Header=BB0_78 Depth=1
	movq	%r12, %rdi
	callq	strlen
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	%ebp, %edx
	movl	%r14d, %ecx
	callq	sgrep
	jmp	.LBB0_84
.LBB0_83:                               #   in Loop: Header=BB0_78 Depth=1
	movl	$old_D_pat, %edi
	movq	%r15, %rsi
	movl	%ebp, %edx
	movl	%r13d, %ecx
	movl	%r14d, %r8d
	callq	bitap
	.p2align	4, 0x90
.LBB0_84:                               #   in Loop: Header=BB0_78 Depth=1
	movl	num_of_matched(%rip), %edx
	testl	%edx, %edx
	je	.LBB0_86
# BB#85:                                #   in Loop: Header=BB0_78 Depth=1
	movl	$0, NOMATCH(%rip)
.LBB0_86:                               #   in Loop: Header=BB0_78 Depth=1
	cmpl	$0, COUNT(%rip)
	je	.LBB0_98
# BB#87:                                #   in Loop: Header=BB0_78 Depth=1
	movl	FILEOUT(%rip), %eax
	testl	%eax, %eax
	jne	.LBB0_98
# BB#88:                                #   in Loop: Header=BB0_78 Depth=1
	movl	FNAME(%rip), %eax
	cmpl	$0, INVERSE(%rip)
	je	.LBB0_92
# BB#89:                                #   in Loop: Header=BB0_78 Depth=1
	movl	PAT_FILE(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB0_92
# BB#90:                                #   in Loop: Header=BB0_78 Depth=1
	movl	total_line(%rip), %ecx
	subl	%edx, %ecx
	testl	%eax, %eax
	je	.LBB0_96
# BB#91:                                #   in Loop: Header=BB0_78 Depth=1
	movl	$.L.str.16, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	jmp	.LBB0_94
	.p2align	4, 0x90
.LBB0_92:                               #   in Loop: Header=BB0_78 Depth=1
	testl	%eax, %eax
	je	.LBB0_95
# BB#93:                                #   in Loop: Header=BB0_78 Depth=1
	movl	$.L.str.16, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
.LBB0_94:                               #   in Loop: Header=BB0_78 Depth=1
	callq	printf
	cmpl	$0, FILEOUT(%rip)
	jne	.LBB0_99
	jmp	.LBB0_101
.LBB0_95:                               #   in Loop: Header=BB0_78 Depth=1
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	movl	%edx, %esi
	jmp	.LBB0_97
.LBB0_96:                               #   in Loop: Header=BB0_78 Depth=1
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
.LBB0_97:                               #   in Loop: Header=BB0_78 Depth=1
	callq	printf
	.p2align	4, 0x90
.LBB0_98:                               #   in Loop: Header=BB0_78 Depth=1
	cmpl	$0, FILEOUT(%rip)
	je	.LBB0_101
.LBB0_99:                               #   in Loop: Header=BB0_78 Depth=1
	movl	num_of_matched(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_101
# BB#100:                               #   in Loop: Header=BB0_78 Depth=1
	movl	$CurrentFileName, %edi
	callq	file_out
.LBB0_101:                              #   in Loop: Header=BB0_78 Depth=1
	incq	%rbx
	movl	%ebp, %edi
	callq	close
	movl	$0, num_of_matched(%rip)
	movslq	Numfiles(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_78
.LBB0_102:                              # %._crit_edge209
	cmpl	$0, NOMATCH(%rip)
	je	.LBB0_134
# BB#103:                               # %._crit_edge209
	movl	BESTMATCH(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_134
# BB#104:
	movl	WHOLELINE(%rip), %eax
	orl	WORDBOUND(%rip), %eax
	orl	LINENUM(%rip), %eax
	orl	INVERSE(%rip), %eax
	je	.LBB0_106
# BB#105:
	movl	$0, SGREP(%rip)
	leaq	16(%rsp), %rbx
	movl	$D_pattern, %edi
	movq	%rbx, %rsi
	callq	preprocess
	movl	$old_D_pat, %edi
	movl	$D_pattern, %esi
	callq	strcpy
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	maskgen
	movl	%eax, %r13d
.LBB0_106:
	movl	$1, COUNT(%rip)
	movl	num_of_matched(%rip), %eax
	xorl	%r12d, %r12d
	cmpl	$2, %r13d
	jl	.LBB0_122
# BB#107:
	testl	%eax, %eax
	jne	.LBB0_122
# BB#108:                               # %.preheader139.preheader
	movl	$1, %edx
	movl	Numfiles(%rip), %ecx
	leaq	16(%rsp), %r14
	leaq	144(%rsp), %r15
	.p2align	4, 0x90
.LBB0_109:                              # %.preheader139
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_111 Depth 2
	movl	%edx, %r12d
	testl	%ecx, %ecx
	movl	$0, %eax
	jle	.LBB0_119
# BB#110:                               # %.lr.ph202.preheader
                                        #   in Loop: Header=BB0_109 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_111:                              # %.lr.ph202
                                        #   Parent Loop BB0_109 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	Textfiles(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	movl	$CurrentFileName, %edi
	callq	strcpy
	movq	Textfiles(%rip), %rax
	movq	(%rax,%rbp,8), %rdi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, %ebx
	testl	%ebx, %ebx
	jle	.LBB0_117
# BB#112:                               #   in Loop: Header=BB0_111 Depth=2
	cmpl	$0, PAT_FILE(%rip)
	je	.LBB0_114
# BB#113:                               #   in Loop: Header=BB0_111 Depth=2
	movl	%ebx, %edi
	callq	mgrep
	jmp	.LBB0_117
	.p2align	4, 0x90
.LBB0_114:                              #   in Loop: Header=BB0_111 Depth=2
	cmpl	$0, SGREP(%rip)
	je	.LBB0_116
# BB#115:                               #   in Loop: Header=BB0_111 Depth=2
	movq	%r15, %rdi
	callq	strlen
	movq	%r15, %rdi
	movl	%eax, %esi
	movl	%ebx, %edx
	movl	%r12d, %ecx
	callq	sgrep
	jmp	.LBB0_117
.LBB0_116:                              #   in Loop: Header=BB0_111 Depth=2
	movl	$old_D_pat, %edi
	movq	%r14, %rsi
	movl	%ebx, %edx
	movl	%r13d, %ecx
	movl	%r12d, %r8d
	callq	bitap
	.p2align	4, 0x90
.LBB0_117:                              #   in Loop: Header=BB0_111 Depth=2
	incq	%rbp
	movl	%ebx, %edi
	callq	close
	movslq	Numfiles(%rip), %rcx
	cmpq	%rcx, %rbp
	jl	.LBB0_111
# BB#118:                               # %._crit_edge203.loopexit
                                        #   in Loop: Header=BB0_109 Depth=1
	movl	num_of_matched(%rip), %eax
.LBB0_119:                              # %._crit_edge203
                                        #   in Loop: Header=BB0_109 Depth=1
	leal	1(%r12), %edx
	cmpl	%r13d, %edx
	jge	.LBB0_122
# BB#120:                               # %._crit_edge203
                                        #   in Loop: Header=BB0_109 Depth=1
	cmpl	$8, %edx
	jg	.LBB0_122
# BB#121:                               # %._crit_edge203
                                        #   in Loop: Header=BB0_109 Depth=1
	testl	%eax, %eax
	je	.LBB0_109
.LBB0_122:                              # %.critedge22
	testl	%eax, %eax
	jle	.LBB0_134
# BB#123:
	movl	$0, COUNT(%rip)
	cmpl	$0, NOPROMPT(%rip)
	je	.LBB0_142
.LBB0_124:                              # %.preheader
	cmpl	$0, Numfiles(%rip)
	jle	.LBB0_133
# BB#125:                               # %.lr.ph.preheader
	leaq	16(%rsp), %r14
	leaq	144(%rsp), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_126:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	Textfiles(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	movl	$CurrentFileName, %edi
	callq	strcpy
	movq	Textfiles(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jle	.LBB0_132
# BB#127:                               #   in Loop: Header=BB0_126 Depth=1
	cmpl	$0, PAT_FILE(%rip)
	je	.LBB0_129
# BB#128:                               #   in Loop: Header=BB0_126 Depth=1
	movl	%ebp, %edi
	callq	mgrep
	jmp	.LBB0_132
	.p2align	4, 0x90
.LBB0_129:                              #   in Loop: Header=BB0_126 Depth=1
	cmpl	$0, SGREP(%rip)
	je	.LBB0_131
# BB#130:                               #   in Loop: Header=BB0_126 Depth=1
	movq	%r15, %rdi
	callq	strlen
	movq	%r15, %rdi
	movl	%eax, %esi
	movl	%ebp, %edx
	movl	%r12d, %ecx
	callq	sgrep
	jmp	.LBB0_132
.LBB0_131:                              #   in Loop: Header=BB0_126 Depth=1
	movl	$old_D_pat, %edi
	movq	%r14, %rsi
	movl	%ebp, %edx
	movl	%r13d, %ecx
	movl	%r12d, %r8d
	callq	bitap
	.p2align	4, 0x90
.LBB0_132:                              #   in Loop: Header=BB0_126 Depth=1
	incq	%rbx
	movl	%ebp, %edi
	callq	close
	movslq	Numfiles(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_126
.LBB0_133:                              # %._crit_edge
	movl	$0, NOMATCH(%rip)
.LBB0_134:
	cmpl	$0, EATFIRST(%rip)
	je	.LBB0_136
.LBB0_135:
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$0, EATFIRST(%rip)
.LBB0_136:
	cmpl	$0, num_of_matched(%rip)
	jne	.LBB0_139
# BB#137:
	cmpl	$0, NOMATCH(%rip)
	jne	.LBB0_140
# BB#138:
	xorl	%edi, %edi
	callq	exit
.LBB0_139:                              # %.thread
	movl	$0, NOMATCH(%rip)
	xorl	%edi, %edi
	callq	exit
.LBB0_140:
	movl	$1, %edi
	callq	exit
.LBB0_141:
	movq	stderr(%rip), %rdi
	movl	$.L.str.10, %esi
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	cmpl	$0, COUNT(%rip)
	jne	.LBB0_50
	jmp	.LBB0_53
.LBB0_142:
	movq	stderr(%rip), %rcx
	cmpl	$1, %r12d
	jne	.LBB0_144
# BB#143:
	movl	$.L.str.17, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB0_145
.LBB0_144:
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r12d, %edx
	callq	fprintf
.LBB0_145:
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	num_of_matched(%rip), %edx
	movq	stderr(%rip), %rcx
	cmpl	$1, %edx
	jne	.LBB0_147
# BB#146:
	movl	$.L.str.19, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB0_148
.LBB0_147:
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
.LBB0_148:
	leaq	3(%rsp), %rsi
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	scanf
	cmpb	$121, 3(%rsp)
	je	.LBB0_124
	jmp	.LBB0_134
.LBB0_149:
	movq	stderr(%rip), %rdi
	movl	$.L.str.11, %esi
	jmp	.LBB0_153
.LBB0_150:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
.LBB0_151:
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	callq	usage
.LBB0_152:
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
.LBB0_153:
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$2, %edi
	callq	exit
.LBB0_154:
	callq	usage
.LBB0_155:
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB0_160
# BB#156:
	cmpl	$0, PAT_FILE(%rip)
	jne	.LBB0_161
# BB#157:
	cmpl	$0, SGREP(%rip)
	jne	.LBB0_162
# BB#158:
	leaq	16(%rsp), %rsi
	movl	$old_D_pat, %edi
	xorl	%edx, %edx
	movl	%r13d, %ecx
	movq	8(%rsp), %r8            # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	bitap
	cmpl	$0, COUNT(%rip)
	je	.LBB0_134
	jmp	.LBB0_163
.LBB0_159:
	callq	usage
.LBB0_160:
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	jmp	.LBB0_153
.LBB0_161:
	xorl	%edi, %edi
	callq	mgrep
	cmpl	$0, COUNT(%rip)
	je	.LBB0_134
	jmp	.LBB0_163
.LBB0_162:
	leaq	144(%rsp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	%eax, %esi
	movq	8(%rsp), %rcx           # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	sgrep
	cmpl	$0, COUNT(%rip)
	je	.LBB0_134
.LBB0_163:
	cmpl	$0, INVERSE(%rip)
	je	.LBB0_166
# BB#164:
	movl	PAT_FILE(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_166
# BB#165:
	movl	total_line(%rip), %esi
	subl	num_of_matched(%rip), %esi
	jmp	.LBB0_167
.LBB0_166:
	movl	num_of_matched(%rip), %esi
.LBB0_167:
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, EATFIRST(%rip)
	jne	.LBB0_135
	jmp	.LBB0_136
.LBB0_168:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	jmp	.LBB0_153
.LBB0_169:
	movq	stderr(%rip), %rdi
	movl	$.L.str.9, %esi
	movl	$Progname, %edx
	xorl	%eax, %eax
	movl	%r15d, %ecx
	callq	fprintf
	callq	usage
.LBB0_170:
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$Progname, %edx
	movl	$8, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movl	$2, %edi
	callq	exit
.LBB0_171:
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	jmp	.LBB0_151
.LBB0_172:
	movq	stderr(%rip), %rdi
	movq	(%r12), %rcx
	movl	$.L.str.5, %esi
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$2, %edi
	callq	exit
.LBB0_173:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	jmp	.LBB0_153
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_11
	.quad	.LBB0_12
	.quad	.LBB0_14
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_15
	.quad	.LBB0_12
	.quad	.LBB0_17
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_38
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_18
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_16
	.quad	.LBB0_19
	.quad	.LBB0_23
	.quad	.LBB0_27
	.quad	.LBB0_12
	.quad	.LBB0_29
	.quad	.LBB0_30
	.quad	.LBB0_12
	.quad	.LBB0_5
	.quad	.LBB0_36
	.quad	.LBB0_12
	.quad	.LBB0_31
	.quad	.LBB0_12
	.quad	.LBB0_9
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_32
	.quad	.LBB0_10
	.quad	.LBB0_12
	.quad	.LBB0_33
	.quad	.LBB0_34
	.quad	.LBB0_37
	.quad	.LBB0_35

	.text
	.globl	initial_value
	.p2align	4, 0x90
	.type	initial_value,@function
initial_value:                          # @initial_value
	.cfi_startproc
# BB#0:
	movl	$0, NOUPPER(%rip)
	movl	$0, NOPROMPT(%rip)
	movl	$0, BESTMATCH(%rip)
	movl	$0, FNAME(%rip)
	movl	$0, REGEX(%rip)
	movl	$0, JUMP(%rip)
	movl	$0, SGREP(%rip)
	movl	$0, WHOLELINE(%rip)
	movl	$0, LINENUM(%rip)
	movl	$0, COUNT(%rip)
	movl	$0, OUTTAIL(%rip)
	movl	$0, TRUNCATE(%rip)
	movl	$0, AND(%rip)
	movl	$0, INVERSE(%rip)
	movl	$0, EATFIRST(%rip)
	movl	$1, FIRSTOUTPUT(%rip)
	movl	$1, NOMATCH(%rip)
	movl	$1, FIRST_IN_RE(%rip)
	movl	$1, S(%rip)
	movl	$1, DD(%rip)
	movl	$1, I(%rip)
	movl	$1, TAIL(%rip)
	movl	$1, HEAD(%rip)
	movl	$2, D_length(%rip)
	movl	$0, num_of_matched(%rip)
	movl	$0, SIMPLEPATTERN(%rip)
	movl	$0, PSIZE(%rip)
	movl	$0, Num_Pat(%rip)
	movl	$0, SILENT(%rip)
	movl	$0, RE_ERR(%rip)
	movl	$0, DELIMITER(%rip)
	movl	$0, WORDBOUND(%rip)
	movl	$1, Bit+128(%rip)
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	$2, Bit+124(%rip)
	movl	$4, Bit+120(%rip)
	movl	$8, Bit+116(%rip)
	movl	$16, Bit+112(%rip)
	movl	$32, Bit+108(%rip)
	movl	$64, Bit+104(%rip)
	movl	$128, Bit+100(%rip)
	movl	$256, Bit+96(%rip)      # imm = 0x100
	movl	$512, Bit+92(%rip)      # imm = 0x200
	movl	$1024, Bit+88(%rip)     # imm = 0x400
	movl	$2048, Bit+84(%rip)     # imm = 0x800
	movl	$4096, Bit+80(%rip)     # imm = 0x1000
	movl	$8192, Bit+76(%rip)     # imm = 0x2000
	movl	$16384, Bit+72(%rip)    # imm = 0x4000
	movl	$32768, Bit+68(%rip)    # imm = 0x8000
	movl	$65536, Bit+64(%rip)    # imm = 0x10000
	movl	$131072, Bit+60(%rip)   # imm = 0x20000
	movl	$262144, Bit+56(%rip)   # imm = 0x40000
	movl	$524288, Bit+52(%rip)   # imm = 0x80000
	movl	$1048576, Bit+48(%rip)  # imm = 0x100000
	movl	$2097152, Bit+44(%rip)  # imm = 0x200000
	movl	$4194304, Bit+40(%rip)  # imm = 0x400000
	movl	$8388608, Bit+36(%rip)  # imm = 0x800000
	movl	$16777216, Bit+32(%rip) # imm = 0x1000000
	movl	$33554432, Bit+28(%rip) # imm = 0x2000000
	movl	$67108864, Bit+24(%rip) # imm = 0x4000000
	movl	$134217728, Bit+20(%rip) # imm = 0x8000000
	movl	$268435456, Bit+16(%rip) # imm = 0x10000000
	movl	$536870912, Bit+12(%rip) # imm = 0x20000000
	movabsq	$4611686020574871552, %rax # imm = 0x4000000080000000
	movq	%rax, Bit+4(%rip)
	movl	$Mask, %edi
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	callq	memset
	popq	%rax
	retq
.Lfunc_end1:
	.size	initial_value, .Lfunc_end1-initial_value
	.cfi_endproc

	.globl	usage
	.p2align	4, 0x90
	.type	usage,@function
usage:                                  # @usage
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	movl	$.L.str.27, %esi
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$10, %edi
	callq	putchar
	movq	stderr(%rip), %rcx
	movl	$.L.str.28, %edi
	movl	$36, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.29, %edi
	movl	$39, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$41, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.31, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.32, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.33, %edi
	movl	$45, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.34, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.35, %edi
	movl	$44, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.36, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.37, %edi
	movl	$70, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.38, %edi
	movl	$61, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.39, %edi
	movl	$42, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$10, %edi
	callq	putchar
	movl	$2, %edi
	callq	exit
.Lfunc_end2:
	.size	usage, .Lfunc_end2-usage
	.cfi_endproc

	.globl	checksg
	.p2align	4, 0x90
	.type	checksg,@function
checksg:                                # @checksg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	callq	strlen
	cmpl	$0, PAT_FILE(%rip)
	jne	.LBB3_2
# BB#1:
	cmpl	%r14d, %eax
	jle	.LBB3_83
.LBB3_2:
	movl	$1, SIMPLEPATTERN(%rip)
	testl	%eax, %eax
	jle	.LBB3_3
# BB#6:                                 # %.lr.ph45
	testl	%r14d, %r14d
	movl	%eax, %ecx
	jle	.LBB3_7
# BB#18:                                # %.lr.ph45.split.us.preheader
	leaq	-1(%rcx), %rsi
	movq	%rax, %rbp
	andq	$3, %rbp
	je	.LBB3_19
# BB#20:                                # %.lr.ph45.split.us.prol.preheader
	movl	$1, %edx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_21:                               # %.lr.ph45.split.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r15,%rdi), %ebx
	addb	$-35, %bl
	cmpb	$89, %bl
	ja	.LBB3_24
# BB#22:                                # %.lr.ph45.split.us.prol
                                        #   in Loop: Header=BB3_21 Depth=1
	movzbl	%bl, %ebx
	jmpq	*.LJTI3_5(,%rbx,8)
.LBB3_23:                               # %.sink.split.prol
                                        #   in Loop: Header=BB3_21 Depth=1
	movl	$0, SIMPLEPATTERN(%rip)
	xorl	%edx, %edx
.LBB3_24:                               #   in Loop: Header=BB3_21 Depth=1
	incq	%rdi
	cmpq	%rdi, %rbp
	jne	.LBB3_21
	jmp	.LBB3_25
.LBB3_3:
	movl	$1, %edx
	cmpl	$0, CONSTANT(%rip)
	jne	.LBB3_5
	jmp	.LBB3_35
.LBB3_7:                                # %.lr.ph45.split.split.preheader
	leaq	-1(%rcx), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB3_8
# BB#9:                                 # %.lr.ph45.split.split.prol.preheader
	movl	$1, %edx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_10:                               # %.lr.ph45.split.split.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r15,%rdi), %ebx
	addb	$-35, %bl
	cmpb	$89, %bl
	ja	.LBB3_13
# BB#11:                                # %.lr.ph45.split.split.prol
                                        #   in Loop: Header=BB3_10 Depth=1
	movzbl	%bl, %ebp
	jmpq	*.LJTI3_0(,%rbp,8)
.LBB3_12:                               # %.sink.split47.prol
                                        #   in Loop: Header=BB3_10 Depth=1
	movl	$0, SIMPLEPATTERN(%rip)
	xorl	%edx, %edx
.LBB3_13:                               #   in Loop: Header=BB3_10 Depth=1
	incq	%rdi
	cmpq	%rdi, %rsi
	jne	.LBB3_10
	jmp	.LBB3_14
.LBB3_19:
	xorl	%edi, %edi
	movl	$1, %edx
.LBB3_25:                               # %.lr.ph45.split.us.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB3_4
# BB#26:                                # %.lr.ph45.split.us.preheader.new
	subq	%rdi, %rcx
	leaq	3(%r15,%rdi), %rsi
	.p2align	4, 0x90
.LBB3_27:                               # %.lr.ph45.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rsi), %ebx
	addb	$-35, %bl
	cmpb	$89, %bl
	ja	.LBB3_30
# BB#28:                                # %.lr.ph45.split.us
                                        #   in Loop: Header=BB3_27 Depth=1
	movzbl	%bl, %edi
	jmpq	*.LJTI3_6(,%rdi,8)
.LBB3_29:                               # %.sink.split
                                        #   in Loop: Header=BB3_27 Depth=1
	movl	$0, SIMPLEPATTERN(%rip)
	xorl	%edx, %edx
.LBB3_30:                               # %.lr.ph45.split.us.172
                                        #   in Loop: Header=BB3_27 Depth=1
	movzbl	-2(%rsi), %ebx
	addb	$-35, %bl
	cmpb	$89, %bl
	ja	.LBB3_68
# BB#31:                                # %.lr.ph45.split.us.172
                                        #   in Loop: Header=BB3_27 Depth=1
	movzbl	%bl, %edi
	jmpq	*.LJTI3_7(,%rdi,8)
.LBB3_67:                               # %.sink.split.1
                                        #   in Loop: Header=BB3_27 Depth=1
	movl	$0, SIMPLEPATTERN(%rip)
	xorl	%edx, %edx
.LBB3_68:                               # %.lr.ph45.split.us.273
                                        #   in Loop: Header=BB3_27 Depth=1
	movzbl	-1(%rsi), %ebx
	addb	$-35, %bl
	cmpb	$89, %bl
	ja	.LBB3_71
# BB#69:                                # %.lr.ph45.split.us.273
                                        #   in Loop: Header=BB3_27 Depth=1
	movzbl	%bl, %edi
	jmpq	*.LJTI3_8(,%rdi,8)
.LBB3_70:                               # %.sink.split.2
                                        #   in Loop: Header=BB3_27 Depth=1
	movl	$0, SIMPLEPATTERN(%rip)
	xorl	%edx, %edx
.LBB3_71:                               # %.lr.ph45.split.us.374
                                        #   in Loop: Header=BB3_27 Depth=1
	movzbl	(%rsi), %ebx
	addb	$-35, %bl
	cmpb	$89, %bl
	ja	.LBB3_74
# BB#72:                                # %.lr.ph45.split.us.374
                                        #   in Loop: Header=BB3_27 Depth=1
	movzbl	%bl, %edi
	jmpq	*.LJTI3_9(,%rdi,8)
.LBB3_73:                               # %.sink.split.3
                                        #   in Loop: Header=BB3_27 Depth=1
	movl	$0, SIMPLEPATTERN(%rip)
	xorl	%edx, %edx
.LBB3_74:                               #   in Loop: Header=BB3_27 Depth=1
	addq	$4, %rsi
	addq	$-4, %rcx
	jne	.LBB3_27
	jmp	.LBB3_4
.LBB3_8:
	xorl	%edi, %edi
	movl	$1, %edx
.LBB3_14:                               # %.lr.ph45.split.split.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB3_4
# BB#15:                                # %.lr.ph45.split.split.preheader.new
	subq	%rdi, %rcx
	leaq	3(%r15,%rdi), %rsi
	.p2align	4, 0x90
.LBB3_16:                               # %.lr.ph45.split.split
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rsi), %ebx
	addb	$-35, %bl
	cmpb	$89, %bl
	ja	.LBB3_33
# BB#17:                                # %.lr.ph45.split.split
                                        #   in Loop: Header=BB3_16 Depth=1
	movzbl	%bl, %edi
	jmpq	*.LJTI3_1(,%rdi,8)
.LBB3_32:                               # %.sink.split47
                                        #   in Loop: Header=BB3_16 Depth=1
	movl	$0, SIMPLEPATTERN(%rip)
	xorl	%edx, %edx
.LBB3_33:                               # %.lr.ph45.split.split.178
                                        #   in Loop: Header=BB3_16 Depth=1
	movzbl	-2(%rsi), %ebx
	addb	$-35, %bl
	cmpb	$89, %bl
	ja	.LBB3_76
# BB#34:                                # %.lr.ph45.split.split.178
                                        #   in Loop: Header=BB3_16 Depth=1
	movzbl	%bl, %edi
	jmpq	*.LJTI3_2(,%rdi,8)
.LBB3_75:                               # %.sink.split47.1
                                        #   in Loop: Header=BB3_16 Depth=1
	movl	$0, SIMPLEPATTERN(%rip)
	xorl	%edx, %edx
.LBB3_76:                               # %.lr.ph45.split.split.279
                                        #   in Loop: Header=BB3_16 Depth=1
	movzbl	-1(%rsi), %ebx
	addb	$-35, %bl
	cmpb	$89, %bl
	ja	.LBB3_79
# BB#77:                                # %.lr.ph45.split.split.279
                                        #   in Loop: Header=BB3_16 Depth=1
	movzbl	%bl, %edi
	jmpq	*.LJTI3_3(,%rdi,8)
.LBB3_78:                               # %.sink.split47.2
                                        #   in Loop: Header=BB3_16 Depth=1
	movl	$0, SIMPLEPATTERN(%rip)
	xorl	%edx, %edx
.LBB3_79:                               # %.lr.ph45.split.split.380
                                        #   in Loop: Header=BB3_16 Depth=1
	movzbl	(%rsi), %ebx
	addb	$-35, %bl
	cmpb	$89, %bl
	ja	.LBB3_82
# BB#80:                                # %.lr.ph45.split.split.380
                                        #   in Loop: Header=BB3_16 Depth=1
	movzbl	%bl, %edi
	jmpq	*.LJTI3_4(,%rdi,8)
.LBB3_81:                               # %.sink.split47.3
                                        #   in Loop: Header=BB3_16 Depth=1
	movl	$0, SIMPLEPATTERN(%rip)
	xorl	%edx, %edx
.LBB3_82:                               #   in Loop: Header=BB3_16 Depth=1
	addq	$4, %rsi
	addq	$-4, %rcx
	jne	.LBB3_16
.LBB3_4:                                # %._crit_edge
	cmpl	$0, CONSTANT(%rip)
	je	.LBB3_35
.LBB3_5:                                # %.thread
	movl	$1, SIMPLEPATTERN(%rip)
	jmp	.LBB3_36
.LBB3_35:
	testl	%edx, %edx
	je	.LBB3_66
.LBB3_36:
	cmpl	$0, NOUPPER(%rip)
	sete	%sil
	testl	%r14d, %r14d
	sete	%cl
	cmpl	$1, JUMP(%rip)
	setne	%dl
	cmpl	$0, I(%rip)
	setne	%bl
	movl	DELIMITER(%rip), %edi
	orl	LINENUM(%rip), %edi
	orl	INVERSE(%rip), %edi
	jne	.LBB3_66
# BB#37:
	orb	%sil, %cl
	andb	%bl, %dl
	andb	%cl, %dl
	je	.LBB3_66
# BB#38:
	testl	%r14d, %r14d
	jle	.LBB3_40
# BB#39:
	movl	WORDBOUND(%rip), %ecx
	testl	%ecx, %ecx
	jne	.LBB3_66
.LBB3_40:
	testl	%r14d, %r14d
	setle	%cl
	cmpl	$0, WHOLELINE(%rip)
	sete	%dl
	cmpl	$0, SILENT(%rip)
	jne	.LBB3_66
# BB#41:
	orb	%dl, %cl
	je	.LBB3_66
# BB#42:
	movl	$1, SGREP(%rip)
	cmpl	$16, %eax
	jl	.LBB3_44
# BB#43:
	movl	$1, DNA(%rip)
.LBB3_44:                               # %.preheader
	testl	%eax, %eax
	jle	.LBB3_66
# BB#45:                                # %.lr.ph.preheader
	movl	%eax, %ecx
	leaq	-1(%rcx), %rdx
	xorl	%esi, %esi
	andq	$3, %rax
	je	.LBB3_51
# BB#46:                                # %.lr.ph.prol.preheader
	movl	$524357, %edi           # imm = 0x80045
	jmp	.LBB3_47
	.p2align	4, 0x90
.LBB3_49:                               #   in Loop: Header=BB3_47 Depth=1
	movl	$0, DNA(%rip)
	jmp	.LBB3_50
	.p2align	4, 0x90
.LBB3_47:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r15,%rsi), %ebx
	addb	$-97, %bl
	cmpb	$19, %bl
	ja	.LBB3_49
# BB#48:                                # %.lr.ph.prol
                                        #   in Loop: Header=BB3_47 Depth=1
	movzbl	%bl, %ebp
	btq	%rbp, %rdi
	jae	.LBB3_49
.LBB3_50:                               #   in Loop: Header=BB3_47 Depth=1
	incq	%rsi
	cmpq	%rsi, %rax
	jne	.LBB3_47
.LBB3_51:                               # %.lr.ph.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB3_66
# BB#52:                                # %.lr.ph.preheader.new
	subq	%rsi, %rcx
	leaq	3(%r15,%rsi), %rax
	movl	$524357, %edx           # imm = 0x80045
	jmp	.LBB3_53
	.p2align	4, 0x90
.LBB3_55:                               #   in Loop: Header=BB3_53 Depth=1
	movl	$0, DNA(%rip)
	jmp	.LBB3_56
	.p2align	4, 0x90
.LBB3_58:                               #   in Loop: Header=BB3_53 Depth=1
	movl	$0, DNA(%rip)
	jmp	.LBB3_59
	.p2align	4, 0x90
.LBB3_61:                               #   in Loop: Header=BB3_53 Depth=1
	movl	$0, DNA(%rip)
	jmp	.LBB3_62
	.p2align	4, 0x90
.LBB3_64:                               #   in Loop: Header=BB3_53 Depth=1
	movl	$0, DNA(%rip)
	jmp	.LBB3_65
	.p2align	4, 0x90
.LBB3_53:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rax), %ebx
	addb	$-97, %bl
	cmpb	$19, %bl
	ja	.LBB3_55
# BB#54:                                # %.lr.ph
                                        #   in Loop: Header=BB3_53 Depth=1
	movzbl	%bl, %esi
	btq	%rsi, %rdx
	jae	.LBB3_55
.LBB3_56:                               # %.lr.ph.166
                                        #   in Loop: Header=BB3_53 Depth=1
	movzbl	-2(%rax), %ebx
	addb	$-97, %bl
	cmpb	$19, %bl
	ja	.LBB3_58
# BB#57:                                # %.lr.ph.166
                                        #   in Loop: Header=BB3_53 Depth=1
	movzbl	%bl, %esi
	btq	%rsi, %rdx
	jae	.LBB3_58
.LBB3_59:                               # %.lr.ph.267
                                        #   in Loop: Header=BB3_53 Depth=1
	movzbl	-1(%rax), %ebx
	addb	$-97, %bl
	cmpb	$19, %bl
	ja	.LBB3_61
# BB#60:                                # %.lr.ph.267
                                        #   in Loop: Header=BB3_53 Depth=1
	movzbl	%bl, %esi
	btq	%rsi, %rdx
	jae	.LBB3_61
.LBB3_62:                               # %.lr.ph.368
                                        #   in Loop: Header=BB3_53 Depth=1
	movzbl	(%rax), %ebx
	addb	$-97, %bl
	cmpb	$19, %bl
	ja	.LBB3_64
# BB#63:                                # %.lr.ph.368
                                        #   in Loop: Header=BB3_53 Depth=1
	movzbl	%bl, %esi
	btq	%rsi, %rdx
	jae	.LBB3_64
.LBB3_65:                               #   in Loop: Header=BB3_53 Depth=1
	addq	$4, %rax
	addq	$-4, %rcx
	jne	.LBB3_53
.LBB3_66:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_83:
	movq	stderr(%rip), %rdi
	movl	$.L.str.40, %esi
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$2, %edi
	callq	exit
.Lfunc_end3:
	.size	checksg, .Lfunc_end3-checksg
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_12
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_13
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_13
	.quad	.LBB3_12
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_12
.LJTI3_1:
	.quad	.LBB3_32
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_32
	.quad	.LBB3_32
	.quad	.LBB3_32
	.quad	.LBB3_33
	.quad	.LBB3_32
	.quad	.LBB3_32
	.quad	.LBB3_32
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_32
	.quad	.LBB3_32
	.quad	.LBB3_33
	.quad	.LBB3_32
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_32
	.quad	.LBB3_32
	.quad	.LBB3_32
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_33
	.quad	.LBB3_32
.LJTI3_2:
	.quad	.LBB3_75
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_75
	.quad	.LBB3_75
	.quad	.LBB3_75
	.quad	.LBB3_76
	.quad	.LBB3_75
	.quad	.LBB3_75
	.quad	.LBB3_75
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_75
	.quad	.LBB3_75
	.quad	.LBB3_76
	.quad	.LBB3_75
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_75
	.quad	.LBB3_75
	.quad	.LBB3_75
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_76
	.quad	.LBB3_75
.LJTI3_3:
	.quad	.LBB3_78
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_78
	.quad	.LBB3_78
	.quad	.LBB3_78
	.quad	.LBB3_79
	.quad	.LBB3_78
	.quad	.LBB3_78
	.quad	.LBB3_78
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_78
	.quad	.LBB3_78
	.quad	.LBB3_79
	.quad	.LBB3_78
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_78
	.quad	.LBB3_78
	.quad	.LBB3_78
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_79
	.quad	.LBB3_78
.LJTI3_4:
	.quad	.LBB3_81
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_81
	.quad	.LBB3_81
	.quad	.LBB3_81
	.quad	.LBB3_82
	.quad	.LBB3_81
	.quad	.LBB3_81
	.quad	.LBB3_81
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_81
	.quad	.LBB3_81
	.quad	.LBB3_82
	.quad	.LBB3_81
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_81
	.quad	.LBB3_81
	.quad	.LBB3_81
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_82
	.quad	.LBB3_81
.LJTI3_5:
	.quad	.LBB3_23
	.quad	.LBB3_23
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_23
	.quad	.LBB3_23
	.quad	.LBB3_23
	.quad	.LBB3_24
	.quad	.LBB3_23
	.quad	.LBB3_23
	.quad	.LBB3_23
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_23
	.quad	.LBB3_23
	.quad	.LBB3_24
	.quad	.LBB3_23
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_23
	.quad	.LBB3_23
	.quad	.LBB3_23
	.quad	.LBB3_23
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_24
	.quad	.LBB3_23
.LJTI3_6:
	.quad	.LBB3_29
	.quad	.LBB3_29
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_29
	.quad	.LBB3_29
	.quad	.LBB3_29
	.quad	.LBB3_30
	.quad	.LBB3_29
	.quad	.LBB3_29
	.quad	.LBB3_29
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_29
	.quad	.LBB3_29
	.quad	.LBB3_30
	.quad	.LBB3_29
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_29
	.quad	.LBB3_29
	.quad	.LBB3_29
	.quad	.LBB3_29
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_30
	.quad	.LBB3_29
.LJTI3_7:
	.quad	.LBB3_67
	.quad	.LBB3_67
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_67
	.quad	.LBB3_67
	.quad	.LBB3_67
	.quad	.LBB3_68
	.quad	.LBB3_67
	.quad	.LBB3_67
	.quad	.LBB3_67
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_67
	.quad	.LBB3_67
	.quad	.LBB3_68
	.quad	.LBB3_67
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_67
	.quad	.LBB3_67
	.quad	.LBB3_67
	.quad	.LBB3_67
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_68
	.quad	.LBB3_67
.LJTI3_8:
	.quad	.LBB3_70
	.quad	.LBB3_70
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_70
	.quad	.LBB3_70
	.quad	.LBB3_70
	.quad	.LBB3_71
	.quad	.LBB3_70
	.quad	.LBB3_70
	.quad	.LBB3_70
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_70
	.quad	.LBB3_70
	.quad	.LBB3_71
	.quad	.LBB3_70
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_70
	.quad	.LBB3_70
	.quad	.LBB3_70
	.quad	.LBB3_70
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_71
	.quad	.LBB3_70
.LJTI3_9:
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_74
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_74
	.quad	.LBB3_73
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_74
	.quad	.LBB3_73

	.text
	.globl	file_out
	.p2align	4, 0x90
	.type	file_out,@function
file_out:                               # @file_out
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 32
	subq	$4112, %rsp             # imm = 0x1010
.Lcfi27:
	.cfi_def_cfa_offset 4144
.Lcfi28:
	.cfi_offset %rbx, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, FNAME(%rip)
	je	.LBB4_6
# BB#1:
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	testl	%ebx, %ebx
	jle	.LBB4_3
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph24
                                        # =>This Inner Loop Header: Depth=1
	movl	$58, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	decl	%ebx
	jne	.LBB4_2
.LBB4_3:                                # %._crit_edge25
	movl	$10, %edi
	callq	_IO_putc
	movl	$CurrentFileName, %edi
	callq	puts
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	stdout(%rip), %rsi
	testl	%ebx, %ebx
	jle	.LBB4_5
	.p2align	4, 0x90
.LBB4_4:                                # %.lr.ph20
                                        # =>This Inner Loop Header: Depth=1
	movl	$58, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	decl	%ebx
	jne	.LBB4_4
.LBB4_5:                                # %._crit_edge21
	movl	$10, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB4_6:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	open
	movl	%eax, %ebp
	movq	%rsp, %rsi
	movl	$4096, %edx             # imm = 0x1000
	movl	%ebp, %edi
	callq	read
	testl	%eax, %eax
	jle	.LBB4_9
# BB#7:                                 # %.lr.ph.preheader
	movq	%rsp, %rbx
	.p2align	4, 0x90
.LBB4_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%eax, %rdx
	movl	$1, %edi
	movq	%rbx, %rsi
	callq	write
	movl	$4096, %edx             # imm = 0x1000
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	read
	testl	%eax, %eax
	jg	.LBB4_8
.LBB4_9:                                # %._crit_edge
	addq	$4112, %rsp             # imm = 0x1010
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end4:
	.size	file_out, .Lfunc_end4-file_out
	.cfi_endproc

	.globl	compute_next
	.p2align	4, 0x90
	.type	compute_next,@function
compute_next:                           # @compute_next
	.cfi_startproc
# BB#0:                                 # %.preheader114
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 240
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r13
	movl	%edi, %r14d
	movl	$32, %eax
	subl	%r14d, %eax
	movslq	%eax, %r12
	movl	Bit(,%r12,4), %r8d
	movl	$0, Bit(,%r12,4)
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 160(%rsp)
	movdqa	%xmm0, 144(%rsp)
	movdqa	%xmm0, 128(%rsp)
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	movdqa	%xmm0, 80(%rsp)
	movdqa	%xmm0, 64(%rsp)
	movdqa	%xmm0, 48(%rsp)
	cmpl	$2, %r14d
	jl	.LBB5_73
# BB#1:                                 # %.preheader.preheader
	movl	%r14d, %r9d
	movl	$1, %esi
	movl	$table+132, %edx
	.p2align	4, 0x90
.LBB5_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_4 Depth 2
	movq	%rsi, %rcx
	shlq	$7, %rcx
	movl	table(%rcx), %ecx
	testl	%ecx, %ecx
	jle	.LBB5_7
# BB#3:                                 # %.lr.ph133
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	48(%rsp,%rsi,4), %ebp
	movl	$1, %ebx
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB5_4:                                #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	%eax, %ecx
	movslq	%ecx, %rcx
	orl	Bit(,%rcx,4), %ebp
	cmpq	$9, %rbx
	jg	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=2
	movl	(%rdi), %ecx
	addq	$4, %rdi
	incq	%rbx
	testl	%ecx, %ecx
	jg	.LBB5_4
.LBB5_6:                                # %._crit_edge134
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	%ebp, 48(%rsp,%rsi,4)
.LBB5_7:                                #   in Loop: Header=BB5_2 Depth=1
	incq	%rsi
	subq	$-128, %rdx
	cmpq	%r9, %rsi
	jne	.LBB5_2
# BB#8:                                 # %._crit_edge137
	movl	%r8d, Bit(,%r12,4)
	cmpl	$15, %r14d
	jle	.LBB5_9
# BB#37:
	cmpl	$31, %r14d
	jge	.LBB5_38
.LBB5_39:
	movl	%r14d, %edx
	andl	$1, %edx
	addl	%r14d, %edx
	movl	$1, %eax
	cmpl	$2, %edx
	jl	.LBB5_45
# BB#40:                                # %.lr.ph.i110.preheader
	movl	%edx, %ecx
	shrl	$31, %ecx
	addl	%edx, %ecx
	sarl	%ecx
	leal	-1(%rcx), %edx
	movl	%ecx, %edi
	xorl	%esi, %esi
	movl	$1, %eax
	andl	$7, %edi
	je	.LBB5_42
	.p2align	4, 0x90
.LBB5_41:                               # %.lr.ph.i110.prol
                                        # =>This Inner Loop Header: Depth=1
	addl	%eax, %eax
	incl	%esi
	cmpl	%esi, %edi
	jne	.LBB5_41
.LBB5_42:                               # %.lr.ph.i110.prol.loopexit
	cmpl	$7, %edx
	jb	.LBB5_45
# BB#43:                                # %.lr.ph.i110.preheader.new
	subl	%esi, %ecx
	.p2align	4, 0x90
.LBB5_44:                               # %.lr.ph.i110
                                        # =>This Inner Loop Header: Depth=1
	shll	$8, %eax
	addl	$-8, %ecx
	jne	.LBB5_44
.LBB5_45:                               # %exponen.exit112
	leal	(%rax,%rax), %edx
	cmpl	%edx, %eax
	jge	.LBB5_54
# BB#46:                                # %.lr.ph131
	movl	%r14d, %ecx
	shrl	$31, %ecx
	addl	%r14d, %ecx
	sarl	%ecx
	cmpl	%r14d, %ecx
	jge	.LBB5_47
# BB#55:                                # %.lr.ph131.split.us.preheader
	movslq	%r14d, %rsi
	movslq	%ecx, %rcx
	movslq	%eax, %rdi
	movslq	%edx, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %rax
	subq	%rcx, %rax
	leaq	-1(%rsi), %r11
	movq	%rax, %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%eax, %r9d
	andl	$1, %r9d
	leaq	(,%rcx,4), %rax
	negq	%rax
	leaq	48(%rsp,%rax), %r10
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	leaq	48(%rsp,%rsi,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB5_56:                               # %.lr.ph131.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_62 Depth 2
                                        #     Child Loop BB5_69 Depth 2
	movl	Bit(,%r12,4), %ebp
	shrl	%ebp
	testq	%r9, %r9
	movl	%ebp, (%r13,%rdx,4)
	jne	.LBB5_58
# BB#57:                                #   in Loop: Header=BB5_56 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%edx, %r8d
	cmpq	%rcx, %r11
	jne	.LBB5_62
	jmp	.LBB5_67
	.p2align	4, 0x90
.LBB5_58:                               #   in Loop: Header=BB5_56 Depth=1
	testl	Bit+128(%rip), %edx
	je	.LBB5_60
# BB#59:                                #   in Loop: Header=BB5_56 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	orl	48(%rsp,%rax,4), %ebp
	movl	%ebp, (%r13,%rdx,4)
.LBB5_60:                               #   in Loop: Header=BB5_56 Depth=1
	movl	%edx, %r8d
	sarl	%r8d
	movq	%r11, %rdi
	cmpq	%rcx, %r11
	je	.LBB5_67
	.p2align	4, 0x90
.LBB5_62:                               #   Parent Loop BB5_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	Bit+128(%rip), %r8d
	je	.LBB5_64
# BB#63:                                #   in Loop: Header=BB5_62 Depth=2
	orl	(%r10,%rdi,4), %ebp
	movl	%ebp, (%r13,%rdx,4)
.LBB5_64:                               #   in Loop: Header=BB5_62 Depth=2
	movl	%r8d, %eax
	sarl	%eax
	testl	Bit+128(%rip), %eax
	je	.LBB5_66
# BB#65:                                #   in Loop: Header=BB5_62 Depth=2
	orl	-4(%r10,%rdi,4), %ebp
	movl	%ebp, (%r13,%rdx,4)
.LBB5_66:                               #   in Loop: Header=BB5_62 Depth=2
	sarl	$2, %r8d
	addq	$-2, %rdi
	cmpq	%rcx, %rdi
	jg	.LBB5_62
.LBB5_67:                               # %._crit_edge124.us
                                        #   in Loop: Header=BB5_56 Depth=1
	movq	%rdx, %rdi
	subq	40(%rsp), %rdi          # 8-byte Folded Reload
	movl	$0, (%r15,%rdi,4)
	cmpl	$2, %r14d
	jl	.LBB5_72
# BB#68:                                # %.lr.ph128.us.preheader
                                        #   in Loop: Header=BB5_56 Depth=1
	xorl	%ebp, %ebp
	movq	16(%rsp), %rax          # 8-byte Reload
	xorl	%esi, %esi
	movl	%edi, %ebx
	.p2align	4, 0x90
.LBB5_69:                               # %.lr.ph128.us
                                        #   Parent Loop BB5_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	Bit+128(%rip), %ebx
	je	.LBB5_71
# BB#70:                                #   in Loop: Header=BB5_69 Depth=2
	orl	(%rax), %ebp
	movl	%ebp, (%r15,%rdi,4)
.LBB5_71:                               #   in Loop: Header=BB5_69 Depth=2
	sarl	%ebx
	incq	%rsi
	addq	$-4, %rax
	cmpq	%rcx, %rsi
	jl	.LBB5_69
.LBB5_72:                               # %._crit_edge129.us
                                        #   in Loop: Header=BB5_56 Depth=1
	incq	%rdx
	cmpq	32(%rsp), %rdx          # 8-byte Folded Reload
	jne	.LBB5_56
	jmp	.LBB5_54
.LBB5_73:                               # %._crit_edge137.thread
	movl	%r8d, Bit(,%r12,4)
.LBB5_9:
	testl	%r14d, %r14d
	jle	.LBB5_10
# BB#11:                                # %.lr.ph.i.preheader
	leal	-1(%r14), %eax
	movl	%r14d, %esi
	xorl	%edx, %edx
	movl	$1, %ecx
	andl	$7, %esi
	je	.LBB5_13
	.p2align	4, 0x90
.LBB5_12:                               # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	addl	%ecx, %ecx
	incl	%edx
	cmpl	%edx, %esi
	jne	.LBB5_12
.LBB5_13:                               # %.lr.ph.i.prol.loopexit
	cmpl	$7, %eax
	jb	.LBB5_16
# BB#14:                                # %.lr.ph.i.preheader.new
	movl	%r14d, %eax
	subl	%edx, %eax
	.p2align	4, 0x90
.LBB5_15:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	shll	$8, %ecx
	addl	$-8, %eax
	jne	.LBB5_15
	jmp	.LBB5_16
.LBB5_10:
	movl	$1, %ecx
.LBB5_16:                               # %exponen.exit
	leal	(%rcx,%rcx), %eax
	cmpl	%eax, %ecx
	jge	.LBB5_54
# BB#17:                                # %.lr.ph119
	movl	%ecx, %ebp
	sarl	%ebp
	testl	%r14d, %r14d
	jle	.LBB5_18
# BB#31:                                # %.lr.ph119.split.us.preheader
	movslq	%r14d, %rdx
	movslq	%ecx, %rcx
	movslq	%eax, %rsi
	incq	%rdx
	.p2align	4, 0x90
.LBB5_32:                               # %.lr.ph119.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_33 Depth 2
	movl	%ebp, (%r13,%rcx,4)
	movq	%rdx, %rdi
	movl	%ebp, %eax
	movl	%ecx, %ebx
	.p2align	4, 0x90
.LBB5_33:                               #   Parent Loop BB5_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	Bit+128(%rip), %ebx
	je	.LBB5_35
# BB#34:                                #   in Loop: Header=BB5_33 Depth=2
	orl	44(%rsp,%rdi,4), %eax
	movl	%eax, (%r13,%rcx,4)
.LBB5_35:                               #   in Loop: Header=BB5_33 Depth=2
	sarl	%ebx
	decq	%rdi
	cmpq	$1, %rdi
	jg	.LBB5_33
# BB#36:                                # %._crit_edge.us
                                        #   in Loop: Header=BB5_32 Depth=1
	incq	%rcx
	cmpq	%rsi, %rcx
	jne	.LBB5_32
	jmp	.LBB5_54
.LBB5_18:                               # %.lr.ph119.split.preheader
	movslq	%ecx, %rdx
	movslq	%eax, %rcx
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpq	$7, %rsi
	jbe	.LBB5_19
# BB#21:                                # %min.iters.checked
	movq	%rsi, %r9
	andq	$-8, %r9
	movq	%rsi, %r10
	andq	$-8, %r10
	je	.LBB5_19
# BB#22:                                # %vector.ph
	movd	%ebp, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	-8(%r10), %r8
	movl	%r8d, %ebx
	shrl	$3, %ebx
	incl	%ebx
	andq	$3, %rbx
	je	.LBB5_23
# BB#24:                                # %vector.body.prol.preheader
	leaq	16(%r13,%rdx,4), %rax
	negq	%rbx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_25:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rax,%rdi,4)
	movdqu	%xmm0, (%rax,%rdi,4)
	addq	$8, %rdi
	incq	%rbx
	jne	.LBB5_25
	jmp	.LBB5_26
.LBB5_47:                               # %.lr.ph131.split.preheader
	movslq	%ecx, %rcx
	movslq	%r14d, %rsi
	movslq	%eax, %r10
	movslq	%edx, %r9
	leaq	48(%rsp,%rsi,4), %r8
	movq	%r10, %rbp
	.p2align	4, 0x90
.LBB5_48:                               # %.lr.ph131.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_50 Depth 2
	movl	Bit(,%r12,4), %eax
	shrl	%eax
	movl	%eax, (%r13,%rbp,4)
	movq	%rbp, %rdi
	subq	%r10, %rdi
	movl	$0, (%r15,%rdi,4)
	cmpl	$2, %r14d
	jl	.LBB5_53
# BB#49:                                # %.lr.ph128.preheader
                                        #   in Loop: Header=BB5_48 Depth=1
	xorl	%esi, %esi
	movq	%r8, %rdx
	xorl	%ebx, %ebx
	movl	%edi, %eax
	.p2align	4, 0x90
.LBB5_50:                               # %.lr.ph128
                                        #   Parent Loop BB5_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	Bit+128(%rip), %eax
	je	.LBB5_52
# BB#51:                                #   in Loop: Header=BB5_50 Depth=2
	orl	(%rdx), %esi
	movl	%esi, (%r15,%rdi,4)
.LBB5_52:                               #   in Loop: Header=BB5_50 Depth=2
	sarl	%eax
	incq	%rbx
	addq	$-4, %rdx
	cmpq	%rcx, %rbx
	jl	.LBB5_50
.LBB5_53:                               # %._crit_edge129
                                        #   in Loop: Header=BB5_48 Depth=1
	incq	%rbp
	cmpq	%r9, %rbp
	jne	.LBB5_48
	jmp	.LBB5_54
.LBB5_38:
	movq	stderr(%rip), %rdi
	movl	$.L.str.23, %esi
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB5_39
.LBB5_23:
	xorl	%edi, %edi
.LBB5_26:                               # %vector.body.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB5_29
# BB#27:                                # %vector.ph.new
	movq	%r10, %rax
	subq	%rdi, %rax
	addq	%rdx, %rdi
	leaq	112(%r13,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB5_28:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -112(%rdi)
	movdqu	%xmm0, -96(%rdi)
	movdqu	%xmm0, -80(%rdi)
	movdqu	%xmm0, -64(%rdi)
	movdqu	%xmm0, -48(%rdi)
	movdqu	%xmm0, -32(%rdi)
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	subq	$-128, %rdi
	addq	$-32, %rax
	jne	.LBB5_28
.LBB5_29:                               # %middle.block
	cmpq	%r10, %rsi
	je	.LBB5_54
# BB#30:
	addq	%r9, %rdx
.LBB5_19:                               # %.lr.ph119.split.preheader186
	leaq	(%r13,%rdx,4), %rax
	subq	%rdx, %rcx
	.p2align	4, 0x90
.LBB5_20:                               # %.lr.ph119.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, (%rax)
	addq	$4, %rax
	decq	%rcx
	jne	.LBB5_20
.LBB5_54:                               # %.loopexit
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	compute_next, .Lfunc_end5-compute_next
	.cfi_endproc

	.globl	exponen
	.p2align	4, 0x90
	.type	exponen,@function
exponen:                                # @exponen
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB6_1
# BB#2:                                 # %.lr.ph.preheader
	leal	-1(%rdi), %ecx
	movl	%edi, %esi
	xorl	%edx, %edx
	movl	$1, %eax
	andl	$7, %esi
	je	.LBB6_4
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	addl	%eax, %eax
	incl	%edx
	cmpl	%edx, %esi
	jne	.LBB6_3
.LBB6_4:                                # %.lr.ph.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB6_7
# BB#5:                                 # %.lr.ph.preheader.new
	subl	%edx, %edi
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	shll	$8, %eax
	addl	$-8, %edi
	jne	.LBB6_6
.LBB6_7:                                # %._crit_edge
	retq
.LBB6_1:
	movl	$1, %eax
	retq
.Lfunc_end6:
	.size	exponen, .Lfunc_end6-exponen
	.cfi_endproc

	.globl	re1
	.p2align	4, 0x90
	.type	re1,@function
re1:                                    # @re1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	subq	$578328, %rsp           # imm = 0x8D318
.Lcfi50:
	.cfi_def_cfa_offset 578384
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	%edi, 112(%rsp)         # 4-byte Spill
	cmpl	$31, %esi
	jge	.LBB7_159
# BB#1:
	movl	NO_ERR_MASK(%rip), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	$32, %r14d
	movl	%esi, %ecx
	shrl	$31, %ecx
	addl	%esi, %ecx
	sarl	%ecx
	leal	1(%rsi), %eax
	xorl	%r15d, %r15d
	cmpl	$3, %eax
	movl	$0, %ebp
	jb	.LBB7_8
# BB#2:                                 # %.lr.ph595.preheader
	cmpl	$1, %ecx
	movl	$1, %r8d
	cmoval	%ecx, %r8d
	xorl	%r15d, %r15d
	cmpl	$8, %r8d
	jb	.LBB7_6
# BB#3:                                 # %min.iters.checked
	movl	%r8d, %ebp
	andl	$-8, %ebp
	je	.LBB7_6
# BB#4:                                 # %vector.scevcheck
	cmpl	$1, %ecx
	movl	$1, %eax
	cmoval	%ecx, %eax
	movl	$33, %edx
	subl	%eax, %edx
	movl	$32, %eax
	cmpl	$32, %edx
	jbe	.LBB7_152
# BB#5:
	xorl	%ebp, %ebp
	jmp	.LBB7_7
.LBB7_6:
	movl	$32, %eax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_7:                                # %.lr.ph595
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %edx
	orl	Bit(,%rdx,4), %r15d
	decl	%eax
	incl	%ebp
	cmpl	%ecx, %ebp
	jb	.LBB7_7
.LBB7_8:                                # %._crit_edge596
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	subl	%esi, %r14d
	cmpl	$0, FIRST_IN_RE(%rip)
	movl	%ecx, (%rsp)            # 4-byte Spill
	je	.LBB7_10
# BB#9:
	leaq	314320(%rsp), %rax
	leaq	50320(%rsp), %rdx
	movl	%esi, %edi
	movq	%rax, %rsi
	callq	compute_next
	movl	(%rsp), %ecx            # 4-byte Reload
.LBB7_10:
	movl	$0, FIRST_IN_RE(%rip)
	movslq	%r14d, %rax
	movl	Bit(,%rax,4), %edi
	movl	%edi, Init(%rip)
	cmpl	$0, HEAD(%rip)
	je	.LBB7_12
# BB#11:
	orl	Bit+4(,%rax,4), %edi
	movl	%edi, Init(%rip)
.LBB7_12:                               # %.preheader545
	movl	28(%rsp), %r14d         # 4-byte Reload
	testl	%r12d, %r12d
	je	.LBB7_16
# BB#13:                                # %.lr.ph589.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB7_14:                               # %.lr.ph589
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, %edx
	shrl	%cl, %edx
	movl	314320(%rsp,%rdx,4), %esi
	orl	%edi, %esi
	andl	%r15d, %edi
	orl	50320(%rsp,%rdi,4), %esi
	movl	%eax, %edx
	movl	%esi, Init(,%rdx,4)
	incl	%eax
	cmpl	%r12d, %eax
	movl	%esi, %edi
	jbe	.LBB7_14
# BB#15:                                # %._crit_edge590.loopexit
	movl	Init(%rip), %edi
.LBB7_16:                               # %._crit_edge590
	movl	%edi, %eax
	orl	$1, %eax
	movl	%eax, Init1(%rip)
	movl	%edi, 32(%rsp)
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	movl	%edi, 64(%rsp)
	testl	%r12d, %r12d
	je	.LBB7_19
# BB#17:                                # %._crit_edge622.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB7_18:                               # %._crit_edge622
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ecx
	movl	Init(,%rcx,4), %edx
	movl	%edx, 32(%rsp,%rcx,4)
	movl	%edx, 64(%rsp,%rcx,4)
	incl	%eax
	cmpl	%r12d, %eax
	jbe	.LBB7_18
.LBB7_19:                               # %._crit_edge645
	leaq	1152(%rsp), %rsi
	movl	$49152, %edx            # imm = 0xC000
	movl	112(%rsp), %edi         # 4-byte Reload
	callq	read
	movq	%rax, %r8
	testl	%r12d, %r12d
	je	.LBB7_95
# BB#20:                                # %.preheader543
	testl	%r8d, %r8d
	jle	.LBB7_151
# BB#21:                                # %.lr.ph586
	movslq	%r12d, %r10
	movl	$1, %eax
	movq	%r10, 120(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB7_22:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_34 Depth 2
                                        #       Child Loop BB7_44 Depth 3
                                        #       Child Loop BB7_50 Depth 3
                                        #       Child Loop BB7_52 Depth 3
                                        #       Child Loop BB7_54 Depth 3
                                        #       Child Loop BB7_75 Depth 3
                                        #       Child Loop BB7_81 Depth 3
                                        #       Child Loop BB7_83 Depth 3
                                        #       Child Loop BB7_85 Depth 3
	leal	1024(%r8), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	cmpl	$49151, %r8d            # imm = 0xBFFF
	jg	.LBB7_25
# BB#23:                                #   in Loop: Header=BB7_22 Depth=1
	leal	1023(%r8), %ecx
	cmpb	$10, 128(%rsp,%rcx)
	je	.LBB7_25
# BB#24:                                #   in Loop: Header=BB7_22 Depth=1
	movl	4(%rsp), %ecx           # 4-byte Reload
	movb	$10, 128(%rsp,%rcx)
.LBB7_25:                               #   in Loop: Header=BB7_22 Depth=1
	movq	%r8, 96(%rsp)           # 8-byte Spill
	testl	%eax, %eax
	movl	(%rsp), %r11d           # 4-byte Reload
	je	.LBB7_27
# BB#26:                                #   in Loop: Header=BB7_22 Depth=1
	movb	$10, 1151(%rsp)
	movl	$1023, %r9d             # imm = 0x3FF
	cmpl	4(%rsp), %r9d           # 4-byte Folded Reload
	jb	.LBB7_28
	jmp	.LBB7_94
	.p2align	4, 0x90
.LBB7_27:                               #   in Loop: Header=BB7_22 Depth=1
	movl	$1024, %r9d             # imm = 0x400
	cmpl	4(%rsp), %r9d           # 4-byte Folded Reload
	jae	.LBB7_94
.LBB7_28:                               # %.lr.ph581.preheader
                                        #   in Loop: Header=BB7_22 Depth=1
	leal	1(%r9), %eax
	movl	%eax, 116(%rsp)         # 4-byte Spill
	jmp	.LBB7_34
.LBB7_29:                               #   in Loop: Header=BB7_34 Depth=2
	movl	%eax, 32(%rsp)
	movl	68(%rsp), %esi
	movl	%esi, %edi
	andl	%r8d, %edi
	movl	%edx, %eax
	movl	%r11d, %ecx
	shrl	%cl, %eax
	movl	314320(%rsp,%rax,4), %eax
	orl	%edx, %eax
	andl	%r15d, %edx
	orl	50320(%rsp,%rdx,4), %eax
	movl	28(%rsp), %r14d         # 4-byte Reload
	andl	%r14d, %eax
	orl	%eax, %edi
	cmpl	$1, %r12d
	movl	%edi, 36(%rsp)
	je	.LBB7_93
# BB#30:                                #   in Loop: Header=BB7_34 Depth=2
	movl	72(%rsp), %edx
	movl	%r8d, %edi
	andl	%edx, %edi
	orl	%esi, %eax
	movl	%eax, %ebp
	movl	%r11d, %ecx
	shrl	%cl, %ebp
	orl	314320(%rsp,%rbp,4), %esi
	andl	%r15d, %eax
	orl	50320(%rsp,%rax,4), %esi
	andl	%r14d, %esi
	orl	%esi, %edi
	cmpl	$2, %r12d
	movl	%edi, 40(%rsp)
	je	.LBB7_93
# BB#31:                                #   in Loop: Header=BB7_34 Depth=2
	movl	76(%rsp), %edi
	movl	%r8d, %eax
	andl	%edi, %eax
	orl	%edx, %esi
	movl	%esi, %ebp
	movl	%r11d, %ecx
	shrl	%cl, %ebp
	orl	314320(%rsp,%rbp,4), %edx
	andl	%r15d, %esi
	orl	50320(%rsp,%rsi,4), %edx
	andl	%r14d, %edx
	orl	%edx, %eax
	cmpl	$3, %r12d
	movl	%eax, 44(%rsp)
	je	.LBB7_93
# BB#32:                                #   in Loop: Header=BB7_34 Depth=2
	andl	80(%rsp), %r8d
	orl	%edi, %edx
	movl	%edx, %eax
	movl	%r11d, %ecx
	shrl	%cl, %eax
	orl	314320(%rsp,%rax,4), %edi
	andl	%r15d, %edx
	orl	50320(%rsp,%rdx,4), %edi
	andl	%r14d, %edi
	orl	%r8d, %edi
	movl	%edi, 48(%rsp)
	jmp	.LBB7_93
	.p2align	4, 0x90
.LBB7_34:                               # %.lr.ph581
                                        #   Parent Loop BB7_22 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_44 Depth 3
                                        #       Child Loop BB7_50 Depth 3
                                        #       Child Loop BB7_52 Depth 3
                                        #       Child Loop BB7_54 Depth 3
                                        #       Child Loop BB7_75 Depth 3
                                        #       Child Loop BB7_81 Depth 3
                                        #       Child Loop BB7_83 Depth 3
                                        #       Child Loop BB7_85 Depth 3
	movl	%r9d, %eax
	movzbl	128(%rsp,%rax), %eax
	cmpq	$10, %rax
	movl	Mask(,%rax,4), %r13d
	movq	%r9, 8(%rsp)            # 8-byte Spill
	jne	.LBB7_56
# BB#35:                                #   in Loop: Header=BB7_34 Depth=2
	movl	32(%rsp,%r10,4), %eax
	movl	Init1(%rip), %edx
	andl	%eax, %edx
	movl	%eax, %esi
	movl	%r11d, %ecx
	shrl	%cl, %esi
	andl	%r15d, %eax
	movl	50320(%rsp,%rax,4), %eax
	orl	314320(%rsp,%rsi,4), %eax
	andl	%r13d, %eax
	orl	%edx, %eax
	movl	%eax, 64(%rsp,%r10,4)
	cmpl	$0, TAIL(%rip)
	je	.LBB7_37
# BB#36:                                #   in Loop: Header=BB7_34 Depth=2
	movl	%eax, %edx
	movl	%r11d, %ecx
	shrl	%cl, %edx
	movl	%eax, %ecx
	andl	%r15d, %ecx
	orl	314320(%rsp,%rdx,4), %eax
	orl	50320(%rsp,%rcx,4), %eax
	movl	%eax, 64(%rsp,%r10,4)
.LBB7_37:                               #   in Loop: Header=BB7_34 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	incl	%ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	andl	$1, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB7_51
# BB#38:                                #   in Loop: Header=BB7_34 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB7_150
# BB#39:                                #   in Loop: Header=BB7_34 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	4(%rsp), %eax           # 4-byte Folded Reload
	jge	.LBB7_51
# BB#40:                                #   in Loop: Header=BB7_34 Depth=2
	incl	num_of_matched(%rip)
	cmpl	$0, COUNT(%rip)
	jne	.LBB7_51
# BB#41:                                #   in Loop: Header=BB7_34 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movslq	%ecx, %rbp
	cmpl	$0, FNAME(%rip)
	je	.LBB7_43
# BB#42:                                #   in Loop: Header=BB7_34 Depth=2
	movl	$.L.str.25, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	callq	printf
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	(%rsp), %r11d           # 4-byte Reload
.LBB7_43:                               # %.preheader.preheader.i515
                                        #   in Loop: Header=BB7_34 Depth=2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB7_44:                               # %.preheader.i518
                                        #   Parent Loop BB7_22 Depth=1
                                        #     Parent Loop BB7_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	127(%rsp,%rbx), %eax
	cmpq	$2, %rbx
	leaq	-1(%rbx), %rbx
	jl	.LBB7_46
# BB#45:                                # %.preheader.i518
                                        #   in Loop: Header=BB7_44 Depth=3
	cmpb	$10, %al
	jne	.LBB7_44
.LBB7_46:                               #   in Loop: Header=BB7_34 Depth=2
	cmpl	$0, LINENUM(%rip)
	je	.LBB7_48
# BB#47:                                #   in Loop: Header=BB7_34 Depth=2
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	(%rsp), %r11d           # 4-byte Reload
	movb	128(%rsp,%rbx), %al
.LBB7_48:                               #   in Loop: Header=BB7_34 Depth=2
	cmpb	$10, %al
	movl	$1023, %eax             # imm = 0x3FF
	cmovel	%ebx, %eax
	cmpl	%ecx, %eax
	jge	.LBB7_51
# BB#49:                                # %.lr.ph.preheader.i521
                                        #   in Loop: Header=BB7_34 Depth=2
	cltq
	leaq	129(%rsp), %rcx
	leaq	(%rcx,%rax), %rbx
	subq	%rax, %rbp
	.p2align	4, 0x90
.LBB7_50:                               # %.lr.ph.i525
                                        #   Parent Loop BB7_22 Depth=1
                                        #     Parent Loop BB7_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbx), %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	movl	(%rsp), %r11d           # 4-byte Reload
	incq	%rbx
	decq	%rbp
	jne	.LBB7_50
	.p2align	4, 0x90
.LBB7_51:                               # %r_output.exit526.preheader
                                        #   in Loop: Header=BB7_34 Depth=2
	xorl	%eax, %eax
	movl	Init(%rip), %ecx
	.p2align	4, 0x90
.LBB7_52:                               # %r_output.exit526
                                        #   Parent Loop BB7_22 Depth=1
                                        #     Parent Loop BB7_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, %edx
	movl	%ecx, 32(%rsp,%rdx,4)
	incl	%eax
	cmpl	%r12d, %eax
	jbe	.LBB7_52
# BB#53:                                # %.lr.ph572
                                        #   in Loop: Header=BB7_34 Depth=2
	movl	Init1(%rip), %r8d
	movl	32(%rsp), %edx
	movl	%edx, %eax
	andl	%r8d, %eax
	movl	%edx, %edi
	movl	%r11d, %ecx
	shrl	%cl, %edi
	movl	%edx, %ecx
	andl	%r15d, %ecx
	movl	50320(%rsp,%rcx,4), %esi
	orl	314320(%rsp,%rdi,4), %esi
	andl	%r13d, %esi
	orl	%eax, %esi
	movl	%esi, 64(%rsp)
	movl	$1, %edi
	.p2align	4, 0x90
.LBB7_54:                               #   Parent Loop BB7_22 Depth=1
                                        #     Parent Loop BB7_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edi, %r9d
	movl	32(%rsp,%r9,4), %ebx
	movl	%r8d, %ebp
	andl	%ebx, %ebp
	orl	%edx, %esi
	movl	%ebx, %r10d
	movl	%r11d, %ecx
	shrl	%cl, %r10d
	movl	%esi, %eax
	shrl	%cl, %eax
	orl	314320(%rsp,%rax,4), %edx
	andl	%r15d, %esi
	orl	50320(%rsp,%rsi,4), %edx
	andl	%r14d, %edx
	orl	%ebp, %edx
	movl	%edx, %esi
	movl	%ebx, %edx
	movl	%ebx, %eax
	andl	%r15d, %eax
	movl	50320(%rsp,%rax,4), %eax
	orl	314320(%rsp,%r10,4), %eax
	andl	%r13d, %eax
	orl	%eax, %esi
	movl	%esi, 64(%rsp,%r9,4)
	incl	%edi
	cmpl	%r12d, %edi
	jbe	.LBB7_54
# BB#55:                                #   in Loop: Header=BB7_34 Depth=2
	movq	120(%rsp), %r10         # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	jmp	.LBB7_65
	.p2align	4, 0x90
.LBB7_56:                               #   in Loop: Header=BB7_34 Depth=2
	movl	32(%rsp), %edx
	movl	Init1(%rip), %r8d
	movl	%r8d, %eax
	andl	%edx, %eax
	testl	%r13d, %r13d
	je	.LBB7_61
# BB#57:                                #   in Loop: Header=BB7_34 Depth=2
	movl	%edx, %edi
	movl	%r11d, %ecx
	shrl	%cl, %edi
	movl	%edx, %ecx
	andl	%r15d, %ecx
	movl	50320(%rsp,%rcx,4), %esi
	orl	314320(%rsp,%rdi,4), %esi
	andl	%r13d, %esi
	orl	%esi, %eax
	movl	%eax, 64(%rsp)
	movl	36(%rsp), %ebx
	movl	%r8d, %eax
	andl	%ebx, %eax
	orl	%edx, %esi
	movl	%ebx, %edi
	movl	%r11d, %ecx
	shrl	%cl, %edi
	movl	%ebx, %ecx
	andl	%r15d, %ecx
	movl	50320(%rsp,%rcx,4), %ebp
	orl	314320(%rsp,%rdi,4), %ebp
	andl	%r13d, %ebp
	movl	%esi, %edi
	movl	%r11d, %ecx
	shrl	%cl, %edi
	orl	314320(%rsp,%rdi,4), %edx
	andl	%r15d, %esi
	orl	50320(%rsp,%rsi,4), %edx
	andl	%r14d, %edx
	orl	%ebp, %edx
	orl	%edx, %eax
	cmpl	$1, %r12d
	movl	%eax, 68(%rsp)
	je	.LBB7_65
# BB#58:                                #   in Loop: Header=BB7_34 Depth=2
	movl	40(%rsp), %esi
	movl	%r8d, %ebp
	andl	%esi, %ebp
	orl	%ebx, %edx
	movl	%esi, %eax
	movl	%r11d, %ecx
	shrl	%cl, %eax
	movl	%esi, %ecx
	andl	%r15d, %ecx
	movl	50320(%rsp,%rcx,4), %edi
	orl	314320(%rsp,%rax,4), %edi
	andl	%r13d, %edi
	movl	%edx, %eax
	movl	%r11d, %ecx
	shrl	%cl, %eax
	orl	314320(%rsp,%rax,4), %ebx
	andl	%r15d, %edx
	orl	50320(%rsp,%rdx,4), %ebx
	andl	%r14d, %ebx
	orl	%edi, %ebx
	orl	%ebx, %ebp
	cmpl	$2, %r12d
	movl	%ebp, 72(%rsp)
	je	.LBB7_65
# BB#59:                                #   in Loop: Header=BB7_34 Depth=2
	movl	44(%rsp), %edx
	movl	%r8d, %ebp
	andl	%edx, %ebp
	orl	%esi, %ebx
	movl	%edx, %eax
	movl	%r11d, %ecx
	shrl	%cl, %eax
	movl	%edx, %ecx
	andl	%r15d, %ecx
	movl	50320(%rsp,%rcx,4), %edi
	orl	314320(%rsp,%rax,4), %edi
	andl	%r13d, %edi
	movl	%ebx, %eax
	movl	%r11d, %ecx
	shrl	%cl, %eax
	orl	314320(%rsp,%rax,4), %esi
	andl	%r15d, %ebx
	orl	50320(%rsp,%rbx,4), %esi
	andl	%r14d, %esi
	orl	%edi, %esi
	orl	%esi, %ebp
	cmpl	$3, %r12d
	movl	%ebp, 76(%rsp)
	je	.LBB7_65
# BB#60:                                #   in Loop: Header=BB7_34 Depth=2
	movl	48(%rsp), %eax
	movl	%r8d, %edi
	andl	%eax, %edi
	orl	%edx, %esi
	movl	%eax, %ebp
	movl	%r11d, %ecx
	shrl	%cl, %ebp
	andl	%r15d, %eax
	movl	50320(%rsp,%rax,4), %eax
	orl	314320(%rsp,%rbp,4), %eax
	andl	%r13d, %eax
	movl	%esi, %ebp
	shrl	%cl, %ebp
	orl	314320(%rsp,%rbp,4), %edx
	andl	%r15d, %esi
	orl	50320(%rsp,%rsi,4), %edx
	andl	%r14d, %edx
	orl	%edi, %eax
	orl	%edx, %eax
	movl	%eax, 80(%rsp)
	jmp	.LBB7_65
.LBB7_61:                               #   in Loop: Header=BB7_34 Depth=2
	movl	%eax, 64(%rsp)
	movl	36(%rsp), %esi
	movl	%esi, %edi
	andl	%r8d, %edi
	movl	%edx, %eax
	movl	%r11d, %ecx
	shrl	%cl, %eax
	movl	314320(%rsp,%rax,4), %eax
	orl	%edx, %eax
	andl	%r15d, %edx
	orl	50320(%rsp,%rdx,4), %eax
	andl	%r14d, %eax
	orl	%eax, %edi
	cmpl	$1, %r12d
	movl	%edi, 68(%rsp)
	je	.LBB7_65
# BB#62:                                #   in Loop: Header=BB7_34 Depth=2
	movl	40(%rsp), %edx
	movl	%r8d, %edi
	andl	%edx, %edi
	orl	%esi, %eax
	movl	%eax, %ebp
	movl	%r11d, %ecx
	shrl	%cl, %ebp
	orl	314320(%rsp,%rbp,4), %esi
	andl	%r15d, %eax
	orl	50320(%rsp,%rax,4), %esi
	andl	%r14d, %esi
	orl	%esi, %edi
	cmpl	$2, %r12d
	movl	%edi, 72(%rsp)
	je	.LBB7_65
# BB#63:                                #   in Loop: Header=BB7_34 Depth=2
	movl	44(%rsp), %edi
	movl	%r8d, %eax
	andl	%edi, %eax
	orl	%edx, %esi
	movl	%esi, %ebp
	movl	%r11d, %ecx
	shrl	%cl, %ebp
	orl	314320(%rsp,%rbp,4), %edx
	andl	%r15d, %esi
	orl	50320(%rsp,%rsi,4), %edx
	andl	%r14d, %edx
	orl	%edx, %eax
	cmpl	$3, %r12d
	movl	%eax, 76(%rsp)
	je	.LBB7_65
# BB#64:                                #   in Loop: Header=BB7_34 Depth=2
	movl	48(%rsp), %eax
	andl	%r8d, %eax
	orl	%edi, %edx
	movl	%edx, %esi
	movl	%r11d, %ecx
	shrl	%cl, %esi
	orl	314320(%rsp,%rsi,4), %edi
	andl	%r15d, %edx
	orl	50320(%rsp,%rdx,4), %edi
	andl	%r14d, %edi
	orl	%eax, %edi
	movl	%edi, 80(%rsp)
	.p2align	4, 0x90
.LBB7_65:                               # %.loopexit541
                                        #   in Loop: Header=BB7_34 Depth=2
	leal	1(%r9), %ebp
	movzbl	128(%rsp,%rbp), %eax
	cmpq	$10, %rax
	movl	Mask(,%rax,4), %r14d
	jne	.LBB7_87
# BB#66:                                #   in Loop: Header=BB7_34 Depth=2
	movl	64(%rsp,%r10,4), %eax
	andl	%eax, %r8d
	movl	%eax, %edx
	movl	%r11d, %ecx
	shrl	%cl, %edx
	andl	%r15d, %eax
	movl	50320(%rsp,%rax,4), %eax
	orl	314320(%rsp,%rdx,4), %eax
	andl	%r14d, %eax
	orl	%r8d, %eax
	movl	%eax, 32(%rsp,%r10,4)
	cmpl	$0, TAIL(%rip)
	je	.LBB7_68
# BB#67:                                #   in Loop: Header=BB7_34 Depth=2
	movl	%eax, %edx
	movl	%r11d, %ecx
	shrl	%cl, %edx
	movl	%eax, %ecx
	andl	%r15d, %ecx
	orl	314320(%rsp,%rdx,4), %eax
	orl	50320(%rsp,%rcx,4), %eax
	movl	%eax, 32(%rsp,%r10,4)
.LBB7_68:                               #   in Loop: Header=BB7_34 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	incl	%ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	andl	$1, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB7_82
# BB#69:                                #   in Loop: Header=BB7_34 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB7_150
# BB#70:                                #   in Loop: Header=BB7_34 Depth=2
	cmpl	4(%rsp), %ebp           # 4-byte Folded Reload
	jge	.LBB7_82
# BB#71:                                #   in Loop: Header=BB7_34 Depth=2
	incl	num_of_matched(%rip)
	cmpl	$0, COUNT(%rip)
	jne	.LBB7_82
# BB#72:                                #   in Loop: Header=BB7_34 Depth=2
	cmpl	$0, FNAME(%rip)
	je	.LBB7_74
# BB#73:                                #   in Loop: Header=BB7_34 Depth=2
	movl	$.L.str.25, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	callq	printf
	movl	(%rsp), %r11d           # 4-byte Reload
.LBB7_74:                               # %.preheader.preheader.i527
                                        #   in Loop: Header=BB7_34 Depth=2
	movslq	116(%rsp), %r13         # 4-byte Folded Reload
	movslq	%ebp, %rbx
	.p2align	4, 0x90
.LBB7_75:                               # %.preheader.i530
                                        #   Parent Loop BB7_22 Depth=1
                                        #     Parent Loop BB7_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	127(%rsp,%rbx), %eax
	cmpq	$2, %rbx
	leaq	-1(%rbx), %rbx
	jl	.LBB7_77
# BB#76:                                # %.preheader.i530
                                        #   in Loop: Header=BB7_75 Depth=3
	cmpb	$10, %al
	jne	.LBB7_75
.LBB7_77:                               #   in Loop: Header=BB7_34 Depth=2
	cmpl	$0, LINENUM(%rip)
	je	.LBB7_79
# BB#78:                                #   in Loop: Header=BB7_34 Depth=2
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movl	(%rsp), %r11d           # 4-byte Reload
	movb	128(%rsp,%rbx), %al
.LBB7_79:                               #   in Loop: Header=BB7_34 Depth=2
	cmpb	$10, %al
	movl	$1023, %eax             # imm = 0x3FF
	cmovel	%ebx, %eax
	cmpl	%ebp, %eax
	jge	.LBB7_82
# BB#80:                                # %.lr.ph.preheader.i533
                                        #   in Loop: Header=BB7_34 Depth=2
	cltq
	leaq	129(%rsp), %rcx
	leaq	(%rcx,%rax), %rbx
	subq	%rax, %r13
	.p2align	4, 0x90
.LBB7_81:                               # %.lr.ph.i537
                                        #   Parent Loop BB7_22 Depth=1
                                        #     Parent Loop BB7_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbx), %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	movl	(%rsp), %r11d           # 4-byte Reload
	incq	%rbx
	decq	%r13
	jne	.LBB7_81
	.p2align	4, 0x90
.LBB7_82:                               # %r_output.exit538.preheader
                                        #   in Loop: Header=BB7_34 Depth=2
	xorl	%eax, %eax
	movq	104(%rsp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB7_83:                               # %r_output.exit538
                                        #   Parent Loop BB7_22 Depth=1
                                        #     Parent Loop BB7_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, %ecx
	movl	%edx, 64(%rsp,%rcx,4)
	incl	%eax
	cmpl	%r12d, %eax
	jbe	.LBB7_83
# BB#84:                                # %.lr.ph576
                                        #   in Loop: Header=BB7_34 Depth=2
	movl	Init1(%rip), %r8d
	movl	64(%rsp), %edx
	movl	%edx, %esi
	andl	%r8d, %esi
	movl	%edx, %ebp
	movl	%r11d, %ecx
	shrl	%cl, %ebp
	movl	%edx, %ecx
	andl	%r15d, %ecx
	movl	50320(%rsp,%rcx,4), %edi
	orl	314320(%rsp,%rbp,4), %edi
	andl	%r14d, %edi
	orl	%esi, %edi
	movl	%edi, 32(%rsp)
	movl	$1, %esi
	movl	28(%rsp), %r13d         # 4-byte Reload
	.p2align	4, 0x90
.LBB7_85:                               #   Parent Loop BB7_22 Depth=1
                                        #     Parent Loop BB7_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%esi, %r9d
	movl	64(%rsp,%r9,4), %ebx
	movl	%r8d, %r10d
	andl	%ebx, %r10d
	orl	%edx, %edi
	movl	%ebx, %ebp
	movl	%r11d, %ecx
	shrl	%cl, %ebp
	movl	%ebx, %ecx
	andl	%r15d, %ecx
	movl	50320(%rsp,%rcx,4), %eax
	orl	314320(%rsp,%rbp,4), %eax
	andl	%r14d, %eax
	movl	%edi, %ebp
	movl	%r11d, %ecx
	shrl	%cl, %ebp
	orl	314320(%rsp,%rbp,4), %edx
	andl	%r15d, %edi
	orl	50320(%rsp,%rdi,4), %edx
	andl	%r13d, %edx
	orl	%r10d, %edx
	orl	%eax, %edx
	movl	%edx, 32(%rsp,%r9,4)
	incl	%esi
	cmpl	%r12d, %esi
	movl	%edx, %edi
	movl	%ebx, %edx
	jbe	.LBB7_85
# BB#86:                                #   in Loop: Header=BB7_34 Depth=2
	movl	%r13d, %r14d
	movq	120(%rsp), %r10         # 8-byte Reload
	jmp	.LBB7_92
	.p2align	4, 0x90
.LBB7_87:                               #   in Loop: Header=BB7_34 Depth=2
	movl	64(%rsp), %edx
	movl	%r8d, %eax
	andl	%edx, %eax
	testl	%r14d, %r14d
	je	.LBB7_29
# BB#88:                                #   in Loop: Header=BB7_34 Depth=2
	movl	%edx, %edi
	movl	%r11d, %ecx
	shrl	%cl, %edi
	movl	%edx, %ecx
	andl	%r15d, %ecx
	movl	50320(%rsp,%rcx,4), %esi
	orl	314320(%rsp,%rdi,4), %esi
	andl	%r14d, %esi
	orl	%esi, %eax
	movl	%eax, 32(%rsp)
	movl	68(%rsp), %ebx
	movl	%r8d, %eax
	andl	%ebx, %eax
	orl	%edx, %esi
	movl	%ebx, %edi
	movl	%r11d, %ecx
	shrl	%cl, %edi
	movl	%ebx, %ecx
	andl	%r15d, %ecx
	movl	50320(%rsp,%rcx,4), %ebp
	orl	314320(%rsp,%rdi,4), %ebp
	andl	%r14d, %ebp
	movl	%esi, %edi
	movl	%r11d, %ecx
	shrl	%cl, %edi
	orl	314320(%rsp,%rdi,4), %edx
	andl	%r15d, %esi
	orl	50320(%rsp,%rsi,4), %edx
	movl	28(%rsp), %r9d          # 4-byte Reload
	andl	%r9d, %edx
	orl	%ebp, %edx
	orl	%edx, %eax
	cmpl	$1, %r12d
	movl	%eax, 36(%rsp)
	je	.LBB7_91
# BB#89:                                #   in Loop: Header=BB7_34 Depth=2
	movl	72(%rsp), %esi
	movl	%r8d, %ebp
	andl	%esi, %ebp
	orl	%ebx, %edx
	movl	%esi, %eax
	movl	%r11d, %ecx
	shrl	%cl, %eax
	movl	%esi, %ecx
	andl	%r15d, %ecx
	movl	50320(%rsp,%rcx,4), %edi
	orl	314320(%rsp,%rax,4), %edi
	andl	%r14d, %edi
	movl	%edx, %eax
	movl	%r11d, %ecx
	shrl	%cl, %eax
	orl	314320(%rsp,%rax,4), %ebx
	andl	%r15d, %edx
	orl	50320(%rsp,%rdx,4), %ebx
	andl	%r9d, %ebx
	orl	%edi, %ebx
	orl	%ebx, %ebp
	cmpl	$2, %r12d
	movl	%ebp, 40(%rsp)
	je	.LBB7_91
# BB#90:                                #   in Loop: Header=BB7_34 Depth=2
	movl	76(%rsp), %edx
	movl	%r8d, %ebp
	andl	%edx, %ebp
	orl	%esi, %ebx
	movl	%edx, %eax
	movl	%r11d, %ecx
	shrl	%cl, %eax
	movl	%edx, %ecx
	andl	%r15d, %ecx
	movl	50320(%rsp,%rcx,4), %edi
	orl	314320(%rsp,%rax,4), %edi
	andl	%r14d, %edi
	movl	%ebx, %eax
	movl	%r11d, %ecx
	shrl	%cl, %eax
	orl	314320(%rsp,%rax,4), %esi
	andl	%r15d, %ebx
	orl	50320(%rsp,%rbx,4), %esi
	andl	%r9d, %esi
	orl	%edi, %esi
	orl	%esi, %ebp
	cmpl	$3, %r12d
	movl	%ebp, 44(%rsp)
	je	.LBB7_91
# BB#33:                                #   in Loop: Header=BB7_34 Depth=2
	movl	80(%rsp), %eax
	andl	%eax, %r8d
	orl	%edx, %esi
	movl	%eax, %edi
	movl	%r11d, %ecx
	shrl	%cl, %edi
	andl	%r15d, %eax
	movl	50320(%rsp,%rax,4), %eax
	orl	314320(%rsp,%rdi,4), %eax
	andl	%r14d, %eax
	movl	%esi, %edi
	shrl	%cl, %edi
	orl	314320(%rsp,%rdi,4), %edx
	andl	%r15d, %esi
	orl	50320(%rsp,%rsi,4), %edx
	andl	%r9d, %edx
	orl	%r8d, %eax
	orl	%edx, %eax
	movl	%eax, 48(%rsp)
	.p2align	4, 0x90
.LBB7_91:                               #   in Loop: Header=BB7_34 Depth=2
	movl	%r9d, %r14d
.LBB7_92:                               # %.loopexit540
                                        #   in Loop: Header=BB7_34 Depth=2
	movq	8(%rsp), %r9            # 8-byte Reload
.LBB7_93:                               # %.loopexit540
                                        #   in Loop: Header=BB7_34 Depth=2
	addl	$2, %r9d
	addl	$2, 116(%rsp)           # 4-byte Folded Spill
	cmpl	4(%rsp), %r9d           # 4-byte Folded Reload
	jb	.LBB7_34
.LBB7_94:                               # %._crit_edge582
                                        #   in Loop: Header=BB7_22 Depth=1
	movslq	96(%rsp), %rax          # 4-byte Folded Reload
	leaq	128(%rsp,%rax), %rsi
	movl	$1024, %edx             # imm = 0x400
	leaq	128(%rsp), %rdi
	callq	strncpy
	movl	$49152, %edx            # imm = 0xC000
	movl	112(%rsp), %edi         # 4-byte Reload
	leaq	1152(%rsp), %rsi
	callq	read
	movq	120(%rsp), %r10         # 8-byte Reload
	movq	%rax, %r8
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.LBB7_22
	jmp	.LBB7_151
.LBB7_95:                               # %.preheader539
	testl	%r8d, %r8d
	movq	104(%rsp), %rbp         # 8-byte Reload
	jle	.LBB7_151
# BB#96:                                # %.lr.ph568
	movl	$1, %eax
	movl	%ebp, %ebx
	.p2align	4, 0x90
.LBB7_97:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_104 Depth 2
                                        #       Child Loop BB7_114 Depth 3
                                        #       Child Loop BB7_120 Depth 3
                                        #       Child Loop BB7_136 Depth 3
                                        #       Child Loop BB7_142 Depth 3
	leal	1024(%r8), %r9d
	cmpl	$49151, %r8d            # imm = 0xBFFF
	jg	.LBB7_100
# BB#98:                                #   in Loop: Header=BB7_97 Depth=1
	leal	1023(%r8), %ecx
	cmpb	$10, 128(%rsp,%rcx)
	je	.LBB7_100
# BB#99:                                #   in Loop: Header=BB7_97 Depth=1
	movl	%r9d, %ecx
	movb	$10, 128(%rsp,%rcx)
.LBB7_100:                              #   in Loop: Header=BB7_97 Depth=1
	testl	%eax, %eax
	movl	(%rsp), %ecx            # 4-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	je	.LBB7_102
# BB#101:                               #   in Loop: Header=BB7_97 Depth=1
	movb	$10, 1151(%rsp)
	movl	$1023, %r12d            # imm = 0x3FF
	cmpl	%r9d, %r12d
	jb	.LBB7_103
	jmp	.LBB7_149
	.p2align	4, 0x90
.LBB7_102:                              #   in Loop: Header=BB7_97 Depth=1
	movl	$1024, %r12d            # imm = 0x400
	cmpl	%r9d, %r12d
	jae	.LBB7_149
.LBB7_103:                              # %.lr.ph.preheader
                                        #   in Loop: Header=BB7_97 Depth=1
	leal	1(%r12), %r13d
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB7_104:                              # %.lr.ph
                                        #   Parent Loop BB7_97 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_114 Depth 3
                                        #       Child Loop BB7_120 Depth 3
                                        #       Child Loop BB7_136 Depth 3
                                        #       Child Loop BB7_142 Depth 3
	movl	%r12d, %eax
	movzbl	128(%rsp,%rax), %eax
	cmpq	$10, %rax
	movl	Mask(,%rax,4), %r14d
	jne	.LBB7_122
# BB#105:                               #   in Loop: Header=BB7_104 Depth=2
	movl	Init1(%rip), %edx
	andl	%ebx, %edx
	movl	%ebx, %edi
	shrl	%cl, %edi
	andl	%r15d, %ebx
	movl	50320(%rsp,%rbx,4), %eax
	orl	314320(%rsp,%rdi,4), %eax
	andl	%r14d, %eax
	orl	%edx, %eax
	cmpl	$0, TAIL(%rip)
	je	.LBB7_107
# BB#106:                               #   in Loop: Header=BB7_104 Depth=2
	movl	%eax, %edx
	shrl	%cl, %edx
	movl	%eax, %edi
	andl	%r15d, %edi
	orl	314320(%rsp,%rdx,4), %eax
	orl	50320(%rsp,%rdi,4), %eax
.LBB7_107:                              #   in Loop: Header=BB7_104 Depth=2
	incl	%esi
	andl	$1, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB7_124
# BB#108:                               #   in Loop: Header=BB7_104 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB7_150
# BB#109:                               #   in Loop: Header=BB7_104 Depth=2
	cmpl	%r9d, %r12d
	jge	.LBB7_124
# BB#110:                               #   in Loop: Header=BB7_104 Depth=2
	incl	num_of_matched(%rip)
	cmpl	$0, COUNT(%rip)
	jne	.LBB7_124
# BB#111:                               #   in Loop: Header=BB7_104 Depth=2
	movslq	%r12d, %rbp
	cmpl	$0, FNAME(%rip)
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	je	.LBB7_113
# BB#112:                               #   in Loop: Header=BB7_104 Depth=2
	movl	$.L.str.25, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	callq	printf
	movl	8(%rsp), %r9d           # 4-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
.LBB7_113:                              # %.preheader.preheader.i
                                        #   in Loop: Header=BB7_104 Depth=2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB7_114:                              # %.preheader.i
                                        #   Parent Loop BB7_97 Depth=1
                                        #     Parent Loop BB7_104 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	127(%rsp,%rbx), %eax
	cmpq	$2, %rbx
	leaq	-1(%rbx), %rbx
	jl	.LBB7_116
# BB#115:                               # %.preheader.i
                                        #   in Loop: Header=BB7_114 Depth=3
	cmpb	$10, %al
	jne	.LBB7_114
.LBB7_116:                              #   in Loop: Header=BB7_104 Depth=2
	cmpl	$0, LINENUM(%rip)
	je	.LBB7_118
# BB#117:                               #   in Loop: Header=BB7_104 Depth=2
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movl	8(%rsp), %r9d           # 4-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
	movb	128(%rsp,%rbx), %al
.LBB7_118:                              #   in Loop: Header=BB7_104 Depth=2
	cmpb	$10, %al
	movl	$1023, %eax             # imm = 0x3FF
	cmovel	%ebx, %eax
	cmpl	%r12d, %eax
	jge	.LBB7_123
# BB#119:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB7_104 Depth=2
	cltq
	leaq	129(%rsp), %rcx
	leaq	(%rcx,%rax), %rbx
	subq	%rax, %rbp
	.p2align	4, 0x90
.LBB7_120:                              # %.lr.ph.i
                                        #   Parent Loop BB7_97 Depth=1
                                        #     Parent Loop BB7_104 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbx), %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	incq	%rbx
	decq	%rbp
	jne	.LBB7_120
# BB#121:                               #   in Loop: Header=BB7_104 Depth=2
	movq	104(%rsp), %rbp         # 8-byte Reload
	movl	%ebp, %ebx
	movl	%ebp, %edx
	movl	(%rsp), %ecx            # 4-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
	jmp	.LBB7_125
	.p2align	4, 0x90
.LBB7_122:                              #   in Loop: Header=BB7_104 Depth=2
	movl	Init1(%rip), %edx
	andl	%ebx, %edx
	testl	%r14d, %r14d
	movl	%edx, %eax
	jne	.LBB7_125
	jmp	.LBB7_126
.LBB7_123:                              #   in Loop: Header=BB7_104 Depth=2
	movq	104(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB7_124:                              #   in Loop: Header=BB7_104 Depth=2
	movl	%ebp, %ebx
	movl	%ebp, %edx
.LBB7_125:                              # %.sink.split
                                        #   in Loop: Header=BB7_104 Depth=2
	movl	%ebx, %edi
	shrl	%cl, %edi
	andl	%r15d, %ebx
	movl	50320(%rsp,%rbx,4), %eax
	orl	314320(%rsp,%rdi,4), %eax
	andl	%r14d, %eax
	orl	%edx, %eax
.LBB7_126:                              #   in Loop: Header=BB7_104 Depth=2
	leal	1(%r12), %r14d
	movl	%r14d, %edx
	movzbl	128(%rsp,%rdx), %edx
	cmpq	$10, %rdx
	movl	Mask(,%rdx,4), %r10d
	jne	.LBB7_144
# BB#127:                               #   in Loop: Header=BB7_104 Depth=2
	movl	Init1(%rip), %edx
	andl	%eax, %edx
	movl	%eax, %edi
	shrl	%cl, %edi
	andl	%r15d, %eax
	movl	50320(%rsp,%rax,4), %eax
	orl	314320(%rsp,%rdi,4), %eax
	andl	%r10d, %eax
	orl	%edx, %eax
	cmpl	$0, TAIL(%rip)
	je	.LBB7_129
# BB#128:                               #   in Loop: Header=BB7_104 Depth=2
	movl	%eax, %edx
	shrl	%cl, %edx
	movl	%eax, %edi
	andl	%r15d, %edi
	orl	314320(%rsp,%rdx,4), %eax
	orl	50320(%rsp,%rdi,4), %eax
.LBB7_129:                              #   in Loop: Header=BB7_104 Depth=2
	incl	%esi
	andl	$1, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB7_146
# BB#130:                               #   in Loop: Header=BB7_104 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB7_150
# BB#131:                               #   in Loop: Header=BB7_104 Depth=2
	cmpl	%r9d, %r14d
	jge	.LBB7_146
# BB#132:                               #   in Loop: Header=BB7_104 Depth=2
	incl	num_of_matched(%rip)
	cmpl	$0, COUNT(%rip)
	jne	.LBB7_146
# BB#133:                               #   in Loop: Header=BB7_104 Depth=2
	cmpl	$0, FNAME(%rip)
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%r10d, 4(%rsp)          # 4-byte Spill
	je	.LBB7_135
# BB#134:                               #   in Loop: Header=BB7_104 Depth=2
	movl	$.L.str.25, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	callq	printf
	movl	4(%rsp), %r10d          # 4-byte Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
.LBB7_135:                              # %.preheader.preheader.i503
                                        #   in Loop: Header=BB7_104 Depth=2
	movslq	%r13d, %rbp
	movslq	%r14d, %rbx
	.p2align	4, 0x90
.LBB7_136:                              # %.preheader.i506
                                        #   Parent Loop BB7_97 Depth=1
                                        #     Parent Loop BB7_104 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	127(%rsp,%rbx), %eax
	cmpq	$2, %rbx
	leaq	-1(%rbx), %rbx
	jl	.LBB7_138
# BB#137:                               # %.preheader.i506
                                        #   in Loop: Header=BB7_136 Depth=3
	cmpb	$10, %al
	jne	.LBB7_136
.LBB7_138:                              #   in Loop: Header=BB7_104 Depth=2
	cmpl	$0, LINENUM(%rip)
	je	.LBB7_140
# BB#139:                               #   in Loop: Header=BB7_104 Depth=2
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movl	4(%rsp), %r10d          # 4-byte Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
	movb	128(%rsp,%rbx), %al
.LBB7_140:                              #   in Loop: Header=BB7_104 Depth=2
	cmpb	$10, %al
	movl	$1023, %eax             # imm = 0x3FF
	cmovel	%ebx, %eax
	cmpl	%r14d, %eax
	jge	.LBB7_145
# BB#141:                               # %.lr.ph.preheader.i509
                                        #   in Loop: Header=BB7_104 Depth=2
	cltq
	leaq	129(%rsp), %rcx
	leaq	(%rcx,%rax), %rbx
	subq	%rax, %rbp
	.p2align	4, 0x90
.LBB7_142:                              # %.lr.ph.i513
                                        #   Parent Loop BB7_97 Depth=1
                                        #     Parent Loop BB7_104 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbx), %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	incq	%rbx
	decq	%rbp
	jne	.LBB7_142
# BB#143:                               #   in Loop: Header=BB7_104 Depth=2
	movq	104(%rsp), %rbp         # 8-byte Reload
	movl	%ebp, %eax
	movl	%ebp, %edx
	movl	(%rsp), %ecx            # 4-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
	movl	4(%rsp), %r10d          # 4-byte Reload
	jmp	.LBB7_147
	.p2align	4, 0x90
.LBB7_144:                              #   in Loop: Header=BB7_104 Depth=2
	movl	Init1(%rip), %edx
	andl	%eax, %edx
	testl	%r10d, %r10d
	movl	%edx, %ebx
	jne	.LBB7_147
	jmp	.LBB7_148
.LBB7_145:                              #   in Loop: Header=BB7_104 Depth=2
	movq	104(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB7_146:                              #   in Loop: Header=BB7_104 Depth=2
	movl	%ebp, %eax
	movl	%ebp, %edx
.LBB7_147:                              # %.backedge.sink.split
                                        #   in Loop: Header=BB7_104 Depth=2
	movl	%eax, %edi
	shrl	%cl, %edi
	andl	%r15d, %eax
	movl	50320(%rsp,%rax,4), %ebx
	orl	314320(%rsp,%rdi,4), %ebx
	andl	%r10d, %ebx
	orl	%edx, %ebx
.LBB7_148:                              # %.backedge
                                        #   in Loop: Header=BB7_104 Depth=2
	addl	$2, %r12d
	addl	$2, %r13d
	cmpl	%r9d, %r12d
	jb	.LBB7_104
.LBB7_149:                              # %._crit_edge
                                        #   in Loop: Header=BB7_97 Depth=1
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movslq	%r8d, %rax
	leaq	128(%rsp,%rax), %rsi
	movl	$1024, %edx             # imm = 0x400
	leaq	128(%rsp), %rdi
	callq	strncpy
	movl	$49152, %edx            # imm = 0xC000
	movl	112(%rsp), %edi         # 4-byte Reload
	leaq	1152(%rsp), %rsi
	callq	read
	movq	104(%rsp), %rbp         # 8-byte Reload
	movq	%rax, %r8
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.LBB7_97
	jmp	.LBB7_151
.LBB7_150:
	incl	num_of_matched(%rip)
	movl	$CurrentFileName, %edi
	callq	puts
.LBB7_151:                              # %.loopexit
	addq	$578328, %rsp           # imm = 0x8D318
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_152:                              # %vector.body.preheader
	leal	-8(%rbp), %edx
	movl	%edx, %edi
	shrl	$3, %edi
	btl	$3, %edx
	jb	.LBB7_154
# BB#153:                               # %vector.body.prol
	movdqu	Bit+116(%rip), %xmm0
	pshufd	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movdqu	Bit+100(%rip), %xmm1
	pshufd	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0]
	movl	$8, %edx
	testl	%edi, %edi
	jne	.LBB7_155
	jmp	.LBB7_157
.LBB7_154:
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	pxor	%xmm1, %xmm1
	testl	%edi, %edi
	je	.LBB7_157
.LBB7_155:                              # %vector.body.preheader.new
	movl	%r8d, %edi
	andl	$-8, %edi
	negl	%edi
	negl	%edx
	.p2align	4, 0x90
.LBB7_156:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leal	32(%rdx), %ebx
	movdqu	Bit-12(,%rbx,4), %xmm2
	pshufd	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	movdqu	Bit-28(,%rbx,4), %xmm3
	pshufd	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0]
	por	%xmm0, %xmm2
	por	%xmm1, %xmm3
	leal	24(%rdx), %ebx
	movdqu	Bit-12(,%rbx,4), %xmm0
	pshufd	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movdqu	Bit-28(,%rbx,4), %xmm1
	pshufd	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0]
	por	%xmm2, %xmm0
	por	%xmm3, %xmm1
	addl	$-16, %edx
	cmpl	%edx, %edi
	jne	.LBB7_156
.LBB7_157:                              # %middle.block
	por	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	por	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	por	%xmm1, %xmm0
	movd	%xmm0, %r15d
	cmpl	%ebp, %r8d
	je	.LBB7_8
# BB#158:
	subl	%ebp, %eax
	jmp	.LBB7_7
.LBB7_159:
	movq	stderr(%rip), %rdi
	movl	$.L.str.23, %esi
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$2, %edi
	callq	exit
.Lfunc_end7:
	.size	re1, .Lfunc_end7-re1
	.cfi_endproc

	.globl	r_output
	.p2align	4, 0x90
	.type	r_output,@function
r_output:                               # @r_output
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi63:
	.cfi_def_cfa_offset 64
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movl	%esi, %ebx
	movq	%rdi, %r14
	cmpl	%edx, %ebx
	jge	.LBB8_12
# BB#1:
	incl	num_of_matched(%rip)
	cmpl	$0, COUNT(%rip)
	jne	.LBB8_12
# BB#2:
	movl	%ebx, %r15d
	cmpl	$0, FNAME(%rip)
	je	.LBB8_4
# BB#3:
	movl	$.L.str.25, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	callq	printf
.LBB8_4:                                # %.preheader.preheader
	movslq	%ebx, %rbx
	leaq	(%r14,%rbx), %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_5:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rbx,%rbp), %rcx
	movzbl	-1(%r13,%rbp), %eax
	decq	%rbp
	cmpq	$2, %rcx
	jl	.LBB8_7
# BB#6:                                 # %.preheader
                                        #   in Loop: Header=BB8_5 Depth=1
	cmpb	$10, %al
	jne	.LBB8_5
.LBB8_7:
	movq	%r15, (%rsp)            # 8-byte Spill
	addq	%rbp, %r15
	cmpl	$0, LINENUM(%rip)
	je	.LBB8_9
# BB#8:
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	printf
	movb	(%r13,%rbp), %al
.LBB8_9:
	cmpb	$10, %al
	movl	$1023, %eax             # imm = 0x3FF
	cmovel	%r15d, %eax
	cmpl	(%rsp), %eax            # 4-byte Folded Reload
	jge	.LBB8_12
# BB#10:                                # %.lr.ph.preheader
	cltq
	subq	%rax, %rbx
	leaq	1(%r14,%rax), %rbp
	.p2align	4, 0x90
.LBB8_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	incq	%rbp
	decq	%rbx
	jne	.LBB8_11
.LBB8_12:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	r_output, .Lfunc_end8-r_output
	.cfi_endproc

	.globl	re
	.p2align	4, 0x90
	.type	re,@function
re:                                     # @re
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$1144, %rsp             # imm = 0x478
.Lcfi76:
	.cfi_def_cfa_offset 1200
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %eax
	movl	%edi, 88(%rsp)          # 4-byte Spill
	movl	$32, %ebx
	subl	%eax, %ebx
	cmpl	$0, FIRST_IN_RE(%rip)
	je	.LBB9_2
# BB#1:
	movl	$Next, %esi
	movl	$Next1, %edx
	movl	%eax, %edi
	callq	compute_next
	movl	$0, FIRST_IN_RE(%rip)
.LBB9_2:                                # %.preheader354.preheader
	leaq	112(%rsp), %rdi
	movl	$Mask, %esi
	movl	$1024, %edx             # imm = 0x400
	callq	memcpy
	movl	NO_ERR_MASK(%rip), %r12d
	movslq	%ebx, %rax
	movl	Bit(,%rax,4), %ebx
	movl	%ebx, Init(%rip)
	cmpl	$0, HEAD(%rip)
	je	.LBB9_4
# BB#3:
	orl	Bit+4(,%rax,4), %ebx
	movl	%ebx, Init(%rip)
.LBB9_4:
	testl	%ebp, %ebp
	movl	%ebx, %eax
	je	.LBB9_8
# BB#5:                                 # %.lr.ph404.preheader
	movl	$1, %eax
	movl	%ebx, %ecx
	.p2align	4, 0x90
.LBB9_6:                                # %.lr.ph404
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	orl	Next(,%rdx,4), %ecx
	movl	%eax, %edx
	movl	%ecx, Init(,%rdx,4)
	incl	%eax
	cmpl	%ebp, %eax
	jbe	.LBB9_6
# BB#7:                                 # %._crit_edge405.loopexit
	movl	Init(%rip), %eax
.LBB9_8:                                # %._crit_edge405
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB9_9:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	movl	%eax, 32(%rsp,%rdx,4)
	movl	%eax, 64(%rsp,%rdx,4)
	incl	%ecx
	cmpl	%ebp, %ecx
	jbe	.LBB9_9
# BB#10:
	movl	%ebx, %eax
	orl	$1, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	$buffer+1024, %esi
	movl	$49152, %edx            # imm = 0xC000
	movl	88(%rsp), %edi          # 4-byte Reload
	callq	read
	testl	%ebp, %ebp
	je	.LBB9_18
# BB#11:                                # %.preheader352
	testl	%eax, %eax
	jle	.LBB9_124
# BB#12:                                # %.lr.ph400
	movslq	%ebp, %r8
	movl	$1, %edx
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	%r12d, 92(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB9_13:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_72 Depth 2
                                        #       Child Loop BB9_86 Depth 3
                                        #       Child Loop BB9_92 Depth 3
                                        #       Child Loop BB9_94 Depth 3
                                        #       Child Loop BB9_96 Depth 3
                                        #       Child Loop BB9_111 Depth 3
                                        #       Child Loop BB9_117 Depth 3
                                        #       Child Loop BB9_119 Depth 3
                                        #       Child Loop BB9_121 Depth 3
	leal	1024(%rax), %r10d
	movq	%rax, 104(%rsp)         # 8-byte Spill
	cmpl	$49151, %eax            # imm = 0xBFFF
	jg	.LBB9_16
# BB#14:                                #   in Loop: Header=BB9_13 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	1023(%rax), %ecx
	cmpb	$10, buffer(%rcx)
	je	.LBB9_16
# BB#15:                                #   in Loop: Header=BB9_13 Depth=1
	movl	%r10d, %ecx
	movb	$10, buffer(%rcx)
.LBB9_16:                               #   in Loop: Header=BB9_13 Depth=1
	testl	%edx, %edx
	je	.LBB9_17
# BB#69:                                #   in Loop: Header=BB9_13 Depth=1
	movb	$10, buffer+1023(%rip)
	movl	$1023, %r9d             # imm = 0x3FF
	cmpl	%r10d, %r9d
	jb	.LBB9_71
	jmp	.LBB9_123
	.p2align	4, 0x90
.LBB9_17:                               #   in Loop: Header=BB9_13 Depth=1
	movl	$1024, %r9d             # imm = 0x400
	cmpl	%r10d, %r9d
	jae	.LBB9_123
.LBB9_71:                               # %.lr.ph395.preheader
                                        #   in Loop: Header=BB9_13 Depth=1
	leal	1(%r9), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	%r10d, 24(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB9_72:                               # %.lr.ph395
                                        #   Parent Loop BB9_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_86 Depth 3
                                        #       Child Loop BB9_92 Depth 3
                                        #       Child Loop BB9_94 Depth 3
                                        #       Child Loop BB9_96 Depth 3
                                        #       Child Loop BB9_111 Depth 3
                                        #       Child Loop BB9_117 Depth 3
                                        #       Child Loop BB9_119 Depth 3
                                        #       Child Loop BB9_121 Depth 3
	movl	%r9d, %eax
	movzbl	buffer(%rax), %eax
	cmpq	$10, %rax
	movl	112(%rsp,%rax,4), %r15d
	jne	.LBB9_73
# BB#77:                                #   in Loop: Header=BB9_72 Depth=2
	movl	32(%rsp,%r8,4), %ecx
	movl	Next(,%rcx,4), %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	andl	4(%rsp), %ecx           # 4-byte Folded Reload
	andl	%r15d, %eax
	orl	%ecx, %eax
	movl	%eax, 64(%rsp,%r8,4)
	cmpl	$0, TAIL(%rip)
	je	.LBB9_79
# BB#78:                                #   in Loop: Header=BB9_72 Depth=2
	movl	%eax, %ecx
	orl	Next(,%rcx,4), %eax
	movl	%eax, 64(%rsp,%r8,4)
.LBB9_79:                               #   in Loop: Header=BB9_72 Depth=2
	incl	8(%rsp)                 # 4-byte Folded Spill
	andl	$1, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB9_93
# BB#80:                                #   in Loop: Header=BB9_72 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB9_33
# BB#81:                                #   in Loop: Header=BB9_72 Depth=2
	cmpl	24(%rsp), %r9d          # 4-byte Folded Reload
	jge	.LBB9_93
# BB#82:                                #   in Loop: Header=BB9_72 Depth=2
	incl	num_of_matched(%rip)
	cmpl	$0, COUNT(%rip)
	jne	.LBB9_93
# BB#83:                                #   in Loop: Header=BB9_72 Depth=2
	movslq	%r9d, %r13
	cmpl	$0, FNAME(%rip)
	je	.LBB9_85
# BB#84:                                #   in Loop: Header=BB9_72 Depth=2
	movl	$.L.str.25, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	movq	%r9, %rbx
	callq	printf
	movq	%rbx, %r9
.LBB9_85:                               # %.preheader.preheader.i336
                                        #   in Loop: Header=BB9_72 Depth=2
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB9_86:                               # %.preheader.i339
                                        #   Parent Loop BB9_13 Depth=1
                                        #     Parent Loop BB9_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	buffer-1(%rbx), %eax
	cmpq	$2, %rbx
	leaq	-1(%rbx), %rbx
	jl	.LBB9_88
# BB#87:                                # %.preheader.i339
                                        #   in Loop: Header=BB9_86 Depth=3
	cmpb	$10, %al
	jne	.LBB9_86
.LBB9_88:                               #   in Loop: Header=BB9_72 Depth=2
	cmpl	$0, LINENUM(%rip)
	je	.LBB9_90
# BB#89:                                #   in Loop: Header=BB9_72 Depth=2
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movl	8(%rsp), %esi           # 4-byte Reload
	movq	%r13, 96(%rsp)          # 8-byte Spill
	movq	%r9, %r14
	callq	printf
	movq	%r14, %r9
	movq	96(%rsp), %r13          # 8-byte Reload
	movb	buffer(%rbx), %al
.LBB9_90:                               #   in Loop: Header=BB9_72 Depth=2
	cmpb	$10, %al
	movl	$1023, %eax             # imm = 0x3FF
	cmovel	%ebx, %eax
	cmpl	%r9d, %eax
	jge	.LBB9_93
# BB#91:                                # %.lr.ph.preheader.i342
                                        #   in Loop: Header=BB9_72 Depth=2
	movslq	%eax, %rbx
	.p2align	4, 0x90
.LBB9_92:                               # %.lr.ph.i346
                                        #   Parent Loop BB9_13 Depth=1
                                        #     Parent Loop BB9_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	buffer+1(%rbx), %edi
	incq	%rbx
	movq	stdout(%rip), %rsi
	movq	%r9, %r14
	callq	_IO_putc
	movq	%r14, %r9
	cmpq	%rbx, %r13
	jne	.LBB9_92
	.p2align	4, 0x90
.LBB9_93:                               # %r_output.exit347.preheader
                                        #   in Loop: Header=BB9_72 Depth=2
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_94:                               # %r_output.exit347
                                        #   Parent Loop BB9_13 Depth=1
                                        #     Parent Loop BB9_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, %ecx
	movl	Init(,%rcx,4), %edx
	movl	%edx, 32(%rsp,%rcx,4)
	movl	%edx, 64(%rsp,%rcx,4)
	incl	%eax
	cmpl	%ebp, %eax
	jbe	.LBB9_94
# BB#95:                                # %.lr.ph386.preheader
                                        #   in Loop: Header=BB9_72 Depth=2
	movl	32(%rsp), %eax
	movl	%eax, %ecx
	andl	4(%rsp), %ecx           # 4-byte Folded Reload
	movl	Next(,%rax,4), %edx
	andl	%r15d, %edx
	orl	%ecx, %edx
	movl	%edx, 64(%rsp)
	movl	$1, %ecx
	movq	16(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB9_96:                               # %.lr.ph386
                                        #   Parent Loop BB9_13 Depth=1
                                        #     Parent Loop BB9_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ecx, %esi
	movl	32(%rsp,%rsi,4), %edi
	movl	%edi, %ebx
	andl	4(%rsp), %ebx           # 4-byte Folded Reload
	orl	%eax, %edx
	orl	Next(,%rdx,4), %eax
	andl	%r12d, %eax
	movl	Next(,%rdi,4), %edx
	andl	%r15d, %edx
	orl	%ebx, %eax
	orl	%edx, %eax
	movl	%eax, 64(%rsp,%rsi,4)
	incl	%ecx
	cmpl	%ebp, %ecx
	movl	%eax, %edx
	movl	%edi, %eax
	jbe	.LBB9_96
	jmp	.LBB9_97
	.p2align	4, 0x90
.LBB9_73:                               #   in Loop: Header=BB9_72 Depth=2
	movl	32(%rsp), %ecx
	movl	%ecx, %eax
	movl	4(%rsp), %edi           # 4-byte Reload
	andl	%edi, %eax
	movl	Next(,%rcx,4), %edx
	andl	%r15d, %edx
	orl	%edx, %eax
	movl	%eax, 64(%rsp)
	movl	36(%rsp), %eax
	movl	%eax, %esi
	andl	%edi, %esi
	orl	%ecx, %edx
	movl	Next(,%rdx,4), %edi
	orl	%ecx, %edi
	andl	%r12d, %edi
	movl	Next(,%rax,4), %edx
	andl	%r15d, %edx
	orl	%edi, %edx
	orl	%edx, %esi
	cmpl	$1, %ebp
	movl	%esi, 68(%rsp)
	je	.LBB9_97
# BB#74:                                #   in Loop: Header=BB9_72 Depth=2
	movl	40(%rsp), %ecx
	movl	%ecx, %esi
	andl	4(%rsp), %esi           # 4-byte Folded Reload
	orl	%eax, %edx
	movl	Next(,%rdx,4), %edi
	orl	%eax, %edi
	andl	%r12d, %edi
	movl	Next(,%rcx,4), %edx
	andl	%r15d, %edx
	orl	%edi, %edx
	orl	%edx, %esi
	cmpl	$2, %ebp
	movl	%esi, 72(%rsp)
	je	.LBB9_97
# BB#75:                                #   in Loop: Header=BB9_72 Depth=2
	movl	44(%rsp), %eax
	movl	%eax, %esi
	andl	4(%rsp), %esi           # 4-byte Folded Reload
	orl	%ecx, %edx
	movl	Next(,%rdx,4), %edx
	orl	%ecx, %edx
	andl	%r12d, %edx
	movl	Next(,%rax,4), %ecx
	andl	%r15d, %ecx
	orl	%edx, %ecx
	orl	%ecx, %esi
	cmpl	$3, %ebp
	movl	%esi, 76(%rsp)
	je	.LBB9_97
# BB#76:                                #   in Loop: Header=BB9_72 Depth=2
	movl	48(%rsp), %edx
	andl	Next(,%rdx,4), %r15d
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	andl	4(%rsp), %edx           # 4-byte Folded Reload
	orl	%eax, %ecx
	movl	Next(,%rcx,4), %ecx
	orl	%eax, %ecx
	andl	%r12d, %ecx
	orl	%edx, %r15d
	orl	%ecx, %r15d
	movl	%r15d, 80(%rsp)
	.p2align	4, 0x90
.LBB9_97:                               #   in Loop: Header=BB9_72 Depth=2
	movl	24(%rsp), %r10d         # 4-byte Reload
	leal	1(%r9), %ebx
	movl	%ebx, %eax
	movzbl	buffer(%rax), %eax
	cmpq	$10, %rax
	movl	112(%rsp,%rax,4), %r14d
	jne	.LBB9_98
# BB#102:                               #   in Loop: Header=BB9_72 Depth=2
	movl	64(%rsp,%r8,4), %ecx
	movl	Next(,%rcx,4), %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	andl	4(%rsp), %ecx           # 4-byte Folded Reload
	andl	%r14d, %eax
	orl	%ecx, %eax
	movl	%eax, 32(%rsp,%r8,4)
	cmpl	$0, TAIL(%rip)
	je	.LBB9_104
# BB#103:                               #   in Loop: Header=BB9_72 Depth=2
	movl	%eax, %ecx
	orl	Next(,%rcx,4), %eax
	movl	%eax, 32(%rsp,%r8,4)
.LBB9_104:                              #   in Loop: Header=BB9_72 Depth=2
	incl	8(%rsp)                 # 4-byte Folded Spill
	andl	$1, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB9_118
# BB#105:                               #   in Loop: Header=BB9_72 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB9_33
# BB#106:                               #   in Loop: Header=BB9_72 Depth=2
	cmpl	%r10d, %ebx
	jge	.LBB9_118
# BB#107:                               #   in Loop: Header=BB9_72 Depth=2
	incl	num_of_matched(%rip)
	cmpl	$0, COUNT(%rip)
	jne	.LBB9_118
# BB#108:                               #   in Loop: Header=BB9_72 Depth=2
	cmpl	$0, FNAME(%rip)
	je	.LBB9_110
# BB#109:                               #   in Loop: Header=BB9_72 Depth=2
	movl	$.L.str.25, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	movq	%r9, %r15
	callq	printf
	movq	%r15, %r9
	movq	16(%rsp), %r8           # 8-byte Reload
.LBB9_110:                              # %.preheader.preheader.i
                                        #   in Loop: Header=BB9_72 Depth=2
	movslq	12(%rsp), %r13          # 4-byte Folded Reload
	movslq	%ebx, %r15
	.p2align	4, 0x90
.LBB9_111:                              # %.preheader.i
                                        #   Parent Loop BB9_13 Depth=1
                                        #     Parent Loop BB9_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	buffer-1(%r15), %eax
	cmpq	$2, %r15
	leaq	-1(%r15), %r15
	jl	.LBB9_113
# BB#112:                               # %.preheader.i
                                        #   in Loop: Header=BB9_111 Depth=3
	cmpb	$10, %al
	jne	.LBB9_111
.LBB9_113:                              #   in Loop: Header=BB9_72 Depth=2
	cmpl	$0, LINENUM(%rip)
	je	.LBB9_115
# BB#114:                               #   in Loop: Header=BB9_72 Depth=2
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movl	8(%rsp), %esi           # 4-byte Reload
	movq	%r9, 96(%rsp)           # 8-byte Spill
	callq	printf
	movq	96(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movb	buffer(%r15), %al
.LBB9_115:                              #   in Loop: Header=BB9_72 Depth=2
	cmpb	$10, %al
	movl	$1023, %eax             # imm = 0x3FF
	cmovel	%r15d, %eax
	cmpl	%ebx, %eax
	movl	24(%rsp), %r10d         # 4-byte Reload
	jge	.LBB9_118
# BB#116:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB9_72 Depth=2
	movslq	%eax, %rbx
	.p2align	4, 0x90
.LBB9_117:                              # %.lr.ph.i
                                        #   Parent Loop BB9_13 Depth=1
                                        #     Parent Loop BB9_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	buffer+1(%rbx), %edi
	incq	%rbx
	movq	stdout(%rip), %rsi
	movq	%r9, %r15
	movq	%r13, %r12
	movl	%r10d, %r13d
	callq	_IO_putc
	movl	%r13d, %r10d
	movq	%r12, %r13
	movl	92(%rsp), %r12d         # 4-byte Reload
	movq	%r15, %r9
	movq	16(%rsp), %r8           # 8-byte Reload
	cmpq	%rbx, %r13
	jne	.LBB9_117
	.p2align	4, 0x90
.LBB9_118:                              # %r_output.exit.preheader
                                        #   in Loop: Header=BB9_72 Depth=2
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_119:                              # %r_output.exit
                                        #   Parent Loop BB9_13 Depth=1
                                        #     Parent Loop BB9_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, %ecx
	movl	Init(,%rcx,4), %edx
	movl	%edx, 32(%rsp,%rcx,4)
	movl	%edx, 64(%rsp,%rcx,4)
	incl	%eax
	cmpl	%ebp, %eax
	jbe	.LBB9_119
# BB#120:                               # %.lr.ph390.preheader
                                        #   in Loop: Header=BB9_72 Depth=2
	movl	64(%rsp), %eax
	movl	%eax, %ecx
	andl	4(%rsp), %ecx           # 4-byte Folded Reload
	movl	Next(,%rax,4), %edx
	andl	%r14d, %edx
	orl	%ecx, %edx
	movl	%edx, 32(%rsp)
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB9_121:                              # %.lr.ph390
                                        #   Parent Loop BB9_13 Depth=1
                                        #     Parent Loop BB9_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ecx, %esi
	movl	64(%rsp,%rsi,4), %edi
	movl	%edi, %ebx
	andl	4(%rsp), %ebx           # 4-byte Folded Reload
	orl	%eax, %edx
	orl	Next(,%rdx,4), %eax
	andl	%r12d, %eax
	movl	Next(,%rdi,4), %edx
	andl	%r14d, %edx
	orl	%ebx, %eax
	orl	%edx, %eax
	movl	%eax, 32(%rsp,%rsi,4)
	incl	%ecx
	cmpl	%ebp, %ecx
	movl	%eax, %edx
	movl	%edi, %eax
	jbe	.LBB9_121
	jmp	.LBB9_122
	.p2align	4, 0x90
.LBB9_98:                               #   in Loop: Header=BB9_72 Depth=2
	movl	64(%rsp), %ecx
	movl	%ecx, %eax
	movl	4(%rsp), %edx           # 4-byte Reload
	andl	%edx, %eax
	movl	Next(,%rcx,4), %esi
	andl	%r14d, %esi
	orl	%esi, %eax
	movl	%eax, 32(%rsp)
	movl	68(%rsp), %eax
	movl	%eax, %edi
	andl	%edx, %edi
	movl	Next(,%rax,4), %edx
	andl	%r14d, %edx
	orl	%ecx, %esi
	movl	Next(,%rsi,4), %esi
	orl	%ecx, %esi
	andl	%r12d, %esi
	orl	%edi, %edx
	orl	%esi, %edx
	cmpl	$1, %ebp
	movl	%edx, 36(%rsp)
	je	.LBB9_122
# BB#99:                                #   in Loop: Header=BB9_72 Depth=2
	movl	72(%rsp), %ecx
	movl	%ecx, %edi
	andl	4(%rsp), %edi           # 4-byte Folded Reload
	movl	Next(,%rcx,4), %esi
	andl	%r14d, %esi
	orl	%eax, %edx
	movl	Next(,%rdx,4), %edx
	orl	%eax, %edx
	andl	%r12d, %edx
	orl	%edi, %esi
	orl	%edx, %esi
	cmpl	$2, %ebp
	movl	%esi, 40(%rsp)
	je	.LBB9_122
# BB#100:                               #   in Loop: Header=BB9_72 Depth=2
	movl	76(%rsp), %eax
	movl	%eax, %edi
	andl	4(%rsp), %edi           # 4-byte Folded Reload
	movl	Next(,%rax,4), %edx
	andl	%r14d, %edx
	orl	%ecx, %esi
	movl	Next(,%rsi,4), %esi
	orl	%ecx, %esi
	andl	%r12d, %esi
	orl	%edi, %edx
	orl	%esi, %edx
	cmpl	$3, %ebp
	movl	%edx, 44(%rsp)
	je	.LBB9_122
# BB#101:                               #   in Loop: Header=BB9_72 Depth=2
	movl	80(%rsp), %ecx
	andl	Next(,%rcx,4), %r14d
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	andl	4(%rsp), %ecx           # 4-byte Folded Reload
	orl	%eax, %edx
	movl	Next(,%rdx,4), %edx
	orl	%eax, %edx
	andl	%r12d, %edx
	orl	%ecx, %r14d
	orl	%edx, %r14d
	movl	%r14d, 48(%rsp)
	.p2align	4, 0x90
.LBB9_122:                              # %.loopexit349
                                        #   in Loop: Header=BB9_72 Depth=2
	addl	$2, %r9d
	addl	$2, 12(%rsp)            # 4-byte Folded Spill
	cmpl	%r10d, %r9d
	jb	.LBB9_72
.LBB9_123:                              # %._crit_edge396
                                        #   in Loop: Header=BB9_13 Depth=1
	movslq	104(%rsp), %rax         # 4-byte Folded Reload
	leaq	buffer(%rax), %rsi
	movl	$buffer, %edi
	movl	$1024, %edx             # imm = 0x400
	callq	strncpy
	movl	$buffer+1024, %esi
	movl	$49152, %edx            # imm = 0xC000
	movl	88(%rsp), %edi          # 4-byte Reload
	callq	read
	movq	16(%rsp), %r8           # 8-byte Reload
	xorl	%edx, %edx
	testl	%eax, %eax
	jg	.LBB9_13
	jmp	.LBB9_124
.LBB9_18:                               # %.preheader348
	movl	%ebx, %r15d
	testl	%eax, %eax
	jle	.LBB9_124
# BB#19:                                # %.lr.ph382
	movl	%r15d, %edi
	movl	%edi, %ecx
	leaq	Next(,%rcx,4), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	$1, %edx
	movl	$1024, %ebp             # imm = 0x400
	xorl	%esi, %esi
	movl	%edi, %r14d
	movl	%edi, 8(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB9_20:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_28 Depth 2
                                        #       Child Loop BB9_39 Depth 3
                                        #       Child Loop BB9_45 Depth 3
                                        #       Child Loop BB9_59 Depth 3
                                        #       Child Loop BB9_65 Depth 3
	leal	1024(%rax), %edi
	cmpl	$49151, %eax            # imm = 0xBFFF
	jg	.LBB9_23
# BB#21:                                #   in Loop: Header=BB9_20 Depth=1
	addl	$1023, %eax             # imm = 0x3FF
	cmpb	$10, buffer(%rax)
	je	.LBB9_23
# BB#22:                                #   in Loop: Header=BB9_20 Depth=1
	movl	%edi, %ecx
	movb	$10, buffer(%rcx)
.LBB9_23:                               #   in Loop: Header=BB9_20 Depth=1
	testl	%edx, %edx
	movl	%ebp, %ebx
	movl	%r15d, %r8d
	je	.LBB9_24
# BB#25:                                #   in Loop: Header=BB9_20 Depth=1
	movb	$10, buffer+1023(%rip)
	movl	$1023, %r15d            # imm = 0x3FF
	cmpl	%edi, %r15d
	jb	.LBB9_27
	jmp	.LBB9_68
	.p2align	4, 0x90
.LBB9_24:                               #   in Loop: Header=BB9_20 Depth=1
	movl	$1024, %r15d            # imm = 0x400
	cmpl	%edi, %r15d
	jae	.LBB9_68
.LBB9_27:                               # %.lr.ph.preheader
                                        #   in Loop: Header=BB9_20 Depth=1
	leal	1(%r15), %edx
	movl	%edi, 12(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB9_28:                               # %.lr.ph
                                        #   Parent Loop BB9_20 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_39 Depth 3
                                        #       Child Loop BB9_45 Depth 3
                                        #       Child Loop BB9_59 Depth 3
                                        #       Child Loop BB9_65 Depth 3
	movl	%r15d, %eax
	movzbl	buffer(%rax), %ecx
	movl	112(%rsp,%rcx,4), %ebp
	movl	%r14d, %eax
	andl	4(%rsp), %r14d          # 4-byte Folded Reload
	movl	Next(,%rax,4), %eax
	andl	%ebp, %eax
	orl	%r14d, %eax
	cmpq	$10, %rcx
	jne	.LBB9_47
# BB#29:                                #   in Loop: Header=BB9_28 Depth=2
	cmpl	$0, TAIL(%rip)
	je	.LBB9_31
# BB#30:                                #   in Loop: Header=BB9_28 Depth=2
	movl	%eax, %ecx
	orl	Next(,%rcx,4), %eax
.LBB9_31:                               #   in Loop: Header=BB9_28 Depth=2
	incl	%esi
	andl	$1, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB9_46
# BB#32:                                #   in Loop: Header=BB9_28 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB9_33
# BB#34:                                #   in Loop: Header=BB9_28 Depth=2
	cmpl	12(%rsp), %r15d         # 4-byte Folded Reload
	jge	.LBB9_46
# BB#35:                                #   in Loop: Header=BB9_28 Depth=2
	incl	num_of_matched(%rip)
	cmpl	$0, COUNT(%rip)
	jne	.LBB9_46
# BB#36:                                #   in Loop: Header=BB9_28 Depth=2
	cmpl	$0, FNAME(%rip)
	movl	%esi, 16(%rsp)          # 4-byte Spill
	je	.LBB9_38
# BB#37:                                #   in Loop: Header=BB9_28 Depth=2
	movl	$.L.str.25, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	movl	%edx, %ebx
	callq	printf
	movl	%ebx, %edx
	movl	16(%rsp), %esi          # 4-byte Reload
.LBB9_38:                               # %.preheader.preheader.i312
                                        #   in Loop: Header=BB9_28 Depth=2
	movslq	%r15d, %r14
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB9_39:                               # %.preheader.i315
                                        #   Parent Loop BB9_20 Depth=1
                                        #     Parent Loop BB9_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	buffer-1(%rbx), %eax
	cmpq	$2, %rbx
	leaq	-1(%rbx), %rbx
	jl	.LBB9_41
# BB#40:                                # %.preheader.i315
                                        #   in Loop: Header=BB9_39 Depth=3
	cmpb	$10, %al
	jne	.LBB9_39
.LBB9_41:                               #   in Loop: Header=BB9_28 Depth=2
	cmpl	$0, LINENUM(%rip)
	je	.LBB9_43
# BB#42:                                #   in Loop: Header=BB9_28 Depth=2
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	%edx, %r12d
	callq	printf
	movl	%r12d, %edx
	movl	16(%rsp), %esi          # 4-byte Reload
	movb	buffer(%rbx), %al
.LBB9_43:                               #   in Loop: Header=BB9_28 Depth=2
	cmpb	$10, %al
	movl	$1023, %eax             # imm = 0x3FF
	cmovel	%ebx, %eax
	cmpl	%r15d, %eax
	jge	.LBB9_46
# BB#44:                                # %.lr.ph.preheader.i318
                                        #   in Loop: Header=BB9_28 Depth=2
	movslq	%eax, %rbx
	.p2align	4, 0x90
.LBB9_45:                               # %.lr.ph.i322
                                        #   Parent Loop BB9_20 Depth=1
                                        #     Parent Loop BB9_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	buffer+1(%rbx), %edi
	incq	%rbx
	movq	stdout(%rip), %rsi
	movl	%edx, %r12d
	callq	_IO_putc
	movl	%r12d, %edx
	movl	16(%rsp), %esi          # 4-byte Reload
	cmpq	%rbx, %r14
	jne	.LBB9_45
	.p2align	4, 0x90
.LBB9_46:                               # %r_output.exit323
                                        #   in Loop: Header=BB9_28 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	andl	(%rax), %ebp
	movl	8(%rsp), %r8d           # 4-byte Reload
	orl	%r8d, %ebp
	movl	%ebp, %eax
	movl	%r15d, %ebx
	movl	12(%rsp), %edi          # 4-byte Reload
.LBB9_47:                               #   in Loop: Header=BB9_28 Depth=2
	leal	1(%r15), %r12d
	movl	%r12d, %ecx
	movzbl	buffer(%rcx), %ecx
	cmpq	$10, %rcx
	movl	112(%rsp,%rcx,4), %r14d
	movl	%eax, %ecx
	jne	.LBB9_48
# BB#49:                                #   in Loop: Header=BB9_28 Depth=2
	andl	4(%rsp), %eax           # 4-byte Folded Reload
	movl	Next(,%rcx,4), %ecx
	andl	%r14d, %ecx
	orl	%eax, %ecx
	cmpl	$0, TAIL(%rip)
	je	.LBB9_51
# BB#50:                                #   in Loop: Header=BB9_28 Depth=2
	movl	%ecx, %eax
	orl	Next(,%rax,4), %ecx
.LBB9_51:                               #   in Loop: Header=BB9_28 Depth=2
	incl	%esi
	andl	$1, %ecx
	cmpl	INVERSE(%rip), %ecx
	je	.LBB9_52
# BB#53:                                #   in Loop: Header=BB9_28 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB9_33
# BB#54:                                #   in Loop: Header=BB9_28 Depth=2
	cmpl	%edi, %r12d
	jge	.LBB9_52
# BB#55:                                #   in Loop: Header=BB9_28 Depth=2
	incl	num_of_matched(%rip)
	cmpl	$0, COUNT(%rip)
	jne	.LBB9_52
# BB#56:                                #   in Loop: Header=BB9_28 Depth=2
	cmpl	$0, FNAME(%rip)
	movl	%esi, 16(%rsp)          # 4-byte Spill
	je	.LBB9_58
# BB#57:                                #   in Loop: Header=BB9_28 Depth=2
	movl	$.L.str.25, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	movl	%edx, %ebp
	movl	%r8d, %ebx
	callq	printf
	movl	%ebx, %r8d
	movl	%ebp, %edx
	movl	12(%rsp), %edi          # 4-byte Reload
	movl	16(%rsp), %esi          # 4-byte Reload
.LBB9_58:                               # %.preheader.preheader.i324
                                        #   in Loop: Header=BB9_28 Depth=2
	movslq	%edx, %r13
	movslq	%r12d, %rbp
	.p2align	4, 0x90
.LBB9_59:                               # %.preheader.i327
                                        #   Parent Loop BB9_20 Depth=1
                                        #     Parent Loop BB9_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	buffer-1(%rbp), %eax
	cmpq	$2, %rbp
	leaq	-1(%rbp), %rbp
	jl	.LBB9_61
# BB#60:                                # %.preheader.i327
                                        #   in Loop: Header=BB9_59 Depth=3
	cmpb	$10, %al
	jne	.LBB9_59
.LBB9_61:                               #   in Loop: Header=BB9_28 Depth=2
	cmpl	$0, LINENUM(%rip)
	je	.LBB9_63
# BB#62:                                #   in Loop: Header=BB9_28 Depth=2
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	%edx, 92(%rsp)          # 4-byte Spill
	movl	%r8d, %ebx
	callq	printf
	movl	%ebx, %r8d
	movl	92(%rsp), %edx          # 4-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	movl	16(%rsp), %esi          # 4-byte Reload
	movb	buffer(%rbp), %al
.LBB9_63:                               #   in Loop: Header=BB9_28 Depth=2
	cmpb	$10, %al
	movl	$1023, %eax             # imm = 0x3FF
	cmovel	%ebp, %eax
	cmpl	%r12d, %eax
	jge	.LBB9_52
# BB#64:                                # %.lr.ph.preheader.i330
                                        #   in Loop: Header=BB9_28 Depth=2
	movl	%edx, %ebp
	movslq	%eax, %rbx
	.p2align	4, 0x90
.LBB9_65:                               # %.lr.ph.i334
                                        #   Parent Loop BB9_20 Depth=1
                                        #     Parent Loop BB9_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	buffer+1(%rbx), %edi
	incq	%rbx
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	cmpq	%rbx, %r13
	jne	.LBB9_65
# BB#66:                                #   in Loop: Header=BB9_28 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	8(%rsp), %r8d           # 4-byte Reload
	movl	%r8d, %eax
	movl	%r12d, %ebx
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	movl	%ebp, %edx
	jmp	.LBB9_67
	.p2align	4, 0x90
.LBB9_48:                               #   in Loop: Header=BB9_28 Depth=2
	andl	4(%rsp), %eax           # 4-byte Folded Reload
	leaq	Next(,%rcx,4), %rcx
	jmp	.LBB9_67
	.p2align	4, 0x90
.LBB9_52:                               #   in Loop: Header=BB9_28 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%r8d, %eax
	movl	%r12d, %ebx
.LBB9_67:                               # %.backedge
                                        #   in Loop: Header=BB9_28 Depth=2
	addl	$2, %r15d
	andl	(%rcx), %r14d
	orl	%eax, %r14d
	addl	$2, %edx
	cmpl	%edi, %r15d
	jb	.LBB9_28
.LBB9_68:                               # %._crit_edge
                                        #   in Loop: Header=BB9_20 Depth=1
	subl	%ebx, %edi
	cmpl	$1025, %edi             # imm = 0x401
	movl	$1024, %eax             # imm = 0x400
	cmovgel	%eax, %edi
	movslq	%edi, %rbp
	movl	$buffer+1024, %edi
	subq	%rbp, %rdi
	movslq	%ebx, %rax
	movl	%esi, %ebx
	leaq	buffer(%rax), %rsi
	movq	%rbp, %rdx
	callq	strncpy
	movl	$1024, %eax             # imm = 0x400
	subl	%ebp, %eax
	movl	%eax, %ebp
	movl	8(%rsp), %r15d          # 4-byte Reload
	movl	$buffer+1024, %esi
	movl	$49152, %edx            # imm = 0xC000
	movl	88(%rsp), %edi          # 4-byte Reload
	callq	read
	movl	%ebx, %esi
	xorl	%edx, %edx
	testl	%eax, %eax
	jg	.LBB9_20
	jmp	.LBB9_124
.LBB9_33:
	incl	num_of_matched(%rip)
	movl	$CurrentFileName, %edi
	callq	puts
.LBB9_124:                              # %.loopexit
	addq	$1144, %rsp             # imm = 0x478
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	re, .Lfunc_end9-re
	.cfi_endproc

	.globl	output
	.p2align	4, 0x90
	.type	output,@function
output:                                 # @output
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi87:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 64
.Lcfi90:
	.cfi_offset %rbx, -56
.Lcfi91:
	.cfi_offset %r12, -48
.Lcfi92:
	.cfi_offset %r13, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	cmpl	%r15d, %ebp
	jg	.LBB10_23
# BB#1:
	movl	num_of_matched(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, num_of_matched(%rip)
	movl	SILENT(%rip), %eax
	orl	COUNT(%rip), %eax
	jne	.LBB10_23
# BB#2:
	cmpl	$0, OUTTAIL(%rip)
	je	.LBB10_4
# BB#3:
	movl	D_length(%rip), %eax
	addl	%eax, %ebp
	addl	%eax, %r15d
.LBB10_4:
	xorl	%r12d, %r12d
	cmpl	$0, DELIMITER(%rip)
	setne	7(%rsp)                 # 1-byte Folded Spill
	cmpl	$0, FIRSTOUTPUT(%rip)
	je	.LBB10_8
# BB#5:
	movslq	%ebp, %rax
	cmpb	$10, (%rbx,%rax)
	jne	.LBB10_7
# BB#6:
	incl	%ebp
	movl	$1, EATFIRST(%rip)
.LBB10_7:
	movl	$0, FIRSTOUTPUT(%rip)
.LBB10_8:
	cmpl	$0, TRUNCATE(%rip)
	jne	.LBB10_9
.LBB10_10:                              # %.preheader
	movslq	%ebp, %rbp
	leaq	(%rbx,%rbp), %r13
	cmpl	%r15d, %ebp
	jg	.LBB10_16
# BB#11:                                # %.preheader
	cmpb	$10, (%rbx,%rbp)
	jne	.LBB10_16
# BB#12:                                # %.lr.ph31.preheader
	movslq	%r15d, %r12
	.p2align	4, 0x90
.LBB10_13:                              # %.lr.ph31
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	leaq	1(%rbp), %r13
	cmpq	%r12, %rbp
	jge	.LBB10_15
# BB#14:                                # %.lr.ph31
                                        #   in Loop: Header=BB10_13 Depth=1
	cmpb	$10, 1(%rbx,%rbp)
	movq	%r13, %rbp
	je	.LBB10_13
.LBB10_15:                              # %.critedge.loopexit
	addq	%rbx, %r13
	xorl	%r12d, %r12d
.LBB10_16:                              # %.critedge
	cmpl	$1, FNAME(%rip)
	jne	.LBB10_18
# BB#17:
	movl	$.L.str.25, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	callq	printf
.LBB10_18:
	cmpl	$0, LINENUM(%rip)
	je	.LBB10_20
# BB#19:
	movb	7(%rsp), %al            # 1-byte Reload
	movb	%al, %r12b
	leal	-1(%r14,%r12), %esi
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	printf
.LBB10_20:
	movslq	%r15d, %rax
	addq	%rax, %rbx
	cmpq	%rbx, %r13
	ja	.LBB10_23
	.p2align	4, 0x90
.LBB10_21:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r13), %edi
	incq	%r13
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	cmpq	%rbx, %r13
	jbe	.LBB10_21
.LBB10_23:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_9:
	movq	stderr(%rip), %rdi
	movl	$.L.str.41, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	jmp	.LBB10_10
.Lfunc_end10:
	.size	output, .Lfunc_end10-output
	.cfi_endproc

	.type	DNA,@object             # @DNA
	.bss
	.globl	DNA
	.p2align	2
DNA:
	.long	0                       # 0x0
	.size	DNA, 4

	.type	APPROX,@object          # @APPROX
	.globl	APPROX
	.p2align	2
APPROX:
	.long	0                       # 0x0
	.size	APPROX, 4

	.type	PAT_FILE,@object        # @PAT_FILE
	.globl	PAT_FILE
	.p2align	2
PAT_FILE:
	.long	0                       # 0x0
	.size	PAT_FILE, 4

	.type	CONSTANT,@object        # @CONSTANT
	.globl	CONSTANT
	.p2align	2
CONSTANT:
	.long	0                       # 0x0
	.size	CONSTANT, 4

	.type	total_line,@object      # @total_line
	.globl	total_line
	.p2align	2
total_line:
	.long	0                       # 0x0
	.size	total_line, 4

	.type	old_D_pat,@object       # @old_D_pat
	.data
	.globl	old_D_pat
	.p2align	4
old_D_pat:
	.asciz	"\n\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.size	old_D_pat, 16

	.type	D_pattern,@object       # @D_pattern
	.globl	D_pattern
	.p2align	4
D_pattern:
	.asciz	"\n; \000\000\000\000\000\000\000\000\000\000\000\000"
	.size	D_pattern, 16

	.type	NOFILENAME,@object      # @NOFILENAME
	.bss
	.globl	NOFILENAME
	.p2align	2
NOFILENAME:
	.long	0                       # 0x0
	.size	NOFILENAME, 4

	.type	FILENAMEONLY,@object    # @FILENAMEONLY
	.globl	FILENAMEONLY
	.p2align	2
FILENAMEONLY:
	.long	0                       # 0x0
	.size	FILENAMEONLY, 4

	.type	Numfiles,@object        # @Numfiles
	.globl	Numfiles
	.p2align	2
Numfiles:
	.long	0                       # 0x0
	.size	Numfiles, 4

	.type	Progname,@object        # @Progname
	.comm	Progname,256,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"agrep"
	.size	.L.str, 6

	.type	COUNT,@object           # @COUNT
	.comm	COUNT,4,4
	.type	SILENT,@object          # @SILENT
	.comm	SILENT,4,4
	.type	I,@object               # @I
	.comm	I,4,4
	.type	WHOLELINE,@object       # @WHOLELINE
	.comm	WHOLELINE,4,4
	.type	WORDBOUND,@object       # @WORDBOUND
	.comm	WORDBOUND,4,4
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%s: illegal option combination\n"
	.size	.L.str.1, 32

	.type	DELIMITER,@object       # @DELIMITER
	.comm	DELIMITER,4,4
	.type	D_length,@object        # @D_length
	.comm	D_length,4,4
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s: delimiter pattern too long\n"
	.size	.L.str.2, 32

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%s: the pattern should immediately follow the -e option\n"
	.size	.L.str.4, 57

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%s: Can't open pattern file %s\n"
	.size	.L.str.5, 32

	.type	NOUPPER,@object         # @NOUPPER
	.comm	NOUPPER,4,4
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%s: the pattern should immediately follow the -k option\n"
	.size	.L.str.6, 57

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%s: -k should be the last option in the command\n"
	.size	.L.str.7, 49

	.type	LINENUM,@object         # @LINENUM
	.comm	LINENUM,4,4
	.type	INVERSE,@object         # @INVERSE
	.comm	INVERSE,4,4
	.type	OUTTAIL,@object         # @OUTTAIL
	.comm	OUTTAIL,4,4
	.type	BESTMATCH,@object       # @BESTMATCH
	.comm	BESTMATCH,4,4
	.type	NOPROMPT,@object        # @NOPROMPT
	.comm	NOPROMPT,4,4
	.type	JUMP,@object            # @JUMP
	.comm	JUMP,4,4
	.type	S,@object               # @S
	.comm	S,4,4
	.type	DD,@object              # @DD
	.comm	DD,4,4
	.type	FILEOUT,@object         # @FILEOUT
	.comm	FILEOUT,4,4
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%s: the maximum number of errors is %d \n"
	.size	.L.str.8, 41

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%s: illegal option  -%c\n"
	.size	.L.str.9, 25

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%s: -h and -l options are mutually exclusive\n"
	.size	.L.str.10, 46

	.type	Textfiles,@object       # @Textfiles
	.comm	Textfiles,8,8
	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%s: malloc failure (you probably don't have enough memory)\n"
	.size	.L.str.11, 60

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"%s: %s: no such file or directory\n"
	.size	.L.str.12, 35

	.type	SGREP,@object           # @SGREP
	.comm	SGREP,4,4
	.type	FNAME,@object           # @FNAME
	.comm	FNAME,4,4
	.type	num_of_matched,@object  # @num_of_matched
	.comm	num_of_matched,4,4
	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"%s: -l option is not compatible with standard input\n"
	.size	.L.str.13, 53

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%d\n"
	.size	.L.str.14, 4

	.type	CurrentFileName,@object # @CurrentFileName
	.comm	CurrentFileName,256,16
	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"%s: can't open file %s\n"
	.size	.L.str.15, 24

	.type	NOMATCH,@object         # @NOMATCH
	.comm	NOMATCH,4,4
	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%s: %d\n"
	.size	.L.str.16, 8

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"best match has 1 error, "
	.size	.L.str.17, 25

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"best match has %d errors, "
	.size	.L.str.18, 27

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"there is 1 match, output it? (y/n)"
	.size	.L.str.19, 35

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"there are %d matches, output them? (y/n)"
	.size	.L.str.20, 41

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%c"
	.size	.L.str.21, 3

	.type	EATFIRST,@object        # @EATFIRST
	.comm	EATFIRST,4,4
	.type	REGEX,@object           # @REGEX
	.comm	REGEX,4,4
	.type	TRUNCATE,@object        # @TRUNCATE
	.comm	TRUNCATE,4,4
	.type	AND,@object             # @AND
	.comm	AND,4,4
	.type	FIRSTOUTPUT,@object     # @FIRSTOUTPUT
	.comm	FIRSTOUTPUT,4,4
	.type	FIRST_IN_RE,@object     # @FIRST_IN_RE
	.comm	FIRST_IN_RE,4,4
	.type	TAIL,@object            # @TAIL
	.comm	TAIL,4,4
	.type	HEAD,@object            # @HEAD
	.comm	HEAD,4,4
	.type	SIMPLEPATTERN,@object   # @SIMPLEPATTERN
	.comm	SIMPLEPATTERN,4,4
	.type	PSIZE,@object           # @PSIZE
	.comm	PSIZE,4,4
	.type	Num_Pat,@object         # @Num_Pat
	.comm	Num_Pat,4,4
	.type	RE_ERR,@object          # @RE_ERR
	.comm	RE_ERR,4,4
	.type	Bit,@object             # @Bit
	.comm	Bit,132,16
	.type	Mask,@object            # @Mask
	.comm	Mask,1024,16
	.type	table,@object           # @table
	.comm	table,4096,16
	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"%s: regular expression too long\n"
	.size	.L.str.23, 33

	.type	NO_ERR_MASK,@object     # @NO_ERR_MASK
	.comm	NO_ERR_MASK,4,4
	.type	Init,@object            # @Init
	.comm	Init,32,16
	.type	Init1,@object           # @Init1
	.comm	Init1,4,4
	.type	Next,@object            # @Next
	.comm	Next,264000,16
	.type	Next1,@object           # @Next1
	.comm	Next1,264000,16
	.type	buffer,@object          # @buffer
	.comm	buffer,50177,16
	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"%s: "
	.size	.L.str.25, 5

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"%d: "
	.size	.L.str.26, 5

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"usage: %s [-#cdehiklnpstvwxBDGIS] [-f patternfile] pattern [files]\n"
	.size	.L.str.27, 68

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"summary of frequently used options:\n"
	.size	.L.str.28, 37

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"-#: find matches with at most # errors\n"
	.size	.L.str.29, 40

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"-c: output the number of matched records\n"
	.size	.L.str.30, 42

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"-d: define record delimiter\n"
	.size	.L.str.31, 29

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"-h: do not output file names\n"
	.size	.L.str.32, 30

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"-i: case-insensitive search, e.g., 'a' = 'A'\n"
	.size	.L.str.33, 46

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"-l: output the names of files that contain a match\n"
	.size	.L.str.34, 52

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"-n: output record prefixed by record number\n"
	.size	.L.str.35, 45

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"-v: output those records containing no matches\n"
	.size	.L.str.36, 48

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"-w: pattern has to match as a word, e.g., 'win' will not match 'wind'\n"
	.size	.L.str.37, 71

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"-B: best match mode. find the closest matches to the pattern\n"
	.size	.L.str.38, 62

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"-G: output the files that contain a match\n"
	.size	.L.str.39, 43

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"%s: size of pattern must be greater than number of errors\n"
	.size	.L.str.40, 59

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"WARNING!!!  some lines have been truncated in output record #%d\n"
	.size	.L.str.41, 65

	.type	wildmask,@object        # @wildmask
	.comm	wildmask,4,4
	.type	endposition,@object     # @endposition
	.comm	endposition,4,4
	.type	D_endpos,@object        # @D_endpos
	.comm	D_endpos,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
