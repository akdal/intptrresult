	.text
	.file	"initMetricsData.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	-2147483647             # 0xffffffff80000001
	.quad	2147483647              # 0x7fffffff
.LCPI0_1:
	.quad	-4039728866278637763    # double -3.4028234699999998E+38
	.quad	-4039728866278637763    # double -3.4028234699999998E+38
	.text
	.globl	initMetricsData
	.p2align	4, 0x90
	.type	initMetricsData,@function
initMetricsData:                        # @initMetricsData
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	initTime
	callq	getTime
	movq	%rax, (%rbx)
	movq	%rax, 8(%rbx)
	movq	%rax, 16(%rbx)
	movq	$-2147483647, 24(%rbx)  # imm = 0x80000001
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movq	$0, 48(%rbx)
	movaps	.LCPI0_0(%rip), %xmm1   # xmm1 = [18446744071562067969,2147483647]
	movups	%xmm1, 56(%rbx)
	movaps	.LCPI0_1(%rip), %xmm2   # xmm2 = [-3.402823e+38,-3.402823e+38]
	movups	%xmm2, 72(%rbx)
	movq	$-2147483647, 88(%rbx)  # imm = 0x80000001
	movups	%xmm0, 96(%rbx)
	movq	$0, 112(%rbx)
	movups	%xmm1, 120(%rbx)
	movups	%xmm2, 136(%rbx)
	movq	$-2147483647, 152(%rbx) # imm = 0x80000001
	movups	%xmm0, 160(%rbx)
	movq	$0, 176(%rbx)
	movups	%xmm1, 184(%rbx)
	movups	%xmm2, 200(%rbx)
	movl	$5, 216(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	initMetricsData, .Lfunc_end0-initMetricsData
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
