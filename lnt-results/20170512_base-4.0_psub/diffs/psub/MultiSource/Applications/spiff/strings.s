	.text
	.file	"strings.bc"
	.globl	S_wordcpy
	.p2align	4, 0x90
	.type	S_wordcpy,@function
S_wordcpy:                              # @S_wordcpy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movb	(%rbx), %bpl
	testb	%bpl, %bpl
	je	.LBB0_4
# BB#1:                                 # %.lr.ph
	callq	__ctype_b_loc
	incq	%rbx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movsbq	%bpl, %rdx
	movzwl	(%rcx,%rdx,2), %ecx
	andl	$24576, %ecx            # imm = 0x6000
	cmpl	$16384, %ecx            # imm = 0x4000
	jne	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movb	%bpl, (%r14)
	incq	%r14
	movzbl	(%rbx), %ebp
	incq	%rbx
	testb	%bpl, %bpl
	jne	.LBB0_2
.LBB0_4:                                # %.critedge
	movb	$0, (%r14)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	S_wordcpy, .Lfunc_end0-S_wordcpy
	.cfi_endproc

	.globl	S_skipword
	.p2align	4, 0x90
	.type	S_skipword,@function
S_skipword:                             # @S_skipword
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	(%r14), %rbx
	movb	(%rbx), %bpl
	testb	%bpl, %bpl
	je	.LBB1_4
# BB#1:                                 # %.lr.ph
	callq	__ctype_b_loc
	incq	%rbx
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movsbq	%bpl, %rdx
	movzwl	(%rcx,%rdx,2), %ecx
	andl	$24576, %ecx            # imm = 0x6000
	cmpl	$16384, %ecx            # imm = 0x4000
	jne	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	%rbx, (%r14)
	movzbl	(%rbx), %ebp
	incq	%rbx
	testb	%bpl, %bpl
	jne	.LBB1_2
.LBB1_4:                                # %.critedge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	S_skipword, .Lfunc_end1-S_skipword
	.cfi_endproc

	.globl	S_skipspace
	.p2align	4, 0x90
	.type	S_skipspace,@function
S_skipspace:                            # @S_skipspace
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	(%r14), %rbx
	movb	(%rbx), %bpl
	testb	%bpl, %bpl
	je	.LBB2_4
# BB#1:                                 # %.lr.ph
	callq	__ctype_b_loc
	incq	%rbx
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movsbq	%bpl, %rdx
	testb	$32, 1(%rcx,%rdx,2)
	je	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	%rbx, (%r14)
	movzbl	(%rbx), %ebp
	incq	%rbx
	testb	%bpl, %bpl
	jne	.LBB2_2
.LBB2_4:                                # %.critedge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	S_skipspace, .Lfunc_end2-S_skipspace
	.cfi_endproc

	.globl	S_nextword
	.p2align	4, 0x90
	.type	S_nextword,@function
S_nextword:                             # @S_nextword
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	(%r14), %rbx
	movb	(%rbx), %bpl
	testb	%bpl, %bpl
	je	.LBB3_7
# BB#1:                                 # %.lr.ph.i
	callq	__ctype_b_loc
	incq	%rbx
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movsbq	%bpl, %rdx
	movzwl	(%rcx,%rdx,2), %ecx
	andl	$24576, %ecx            # imm = 0x6000
	cmpl	$16384, %ecx            # imm = 0x4000
	jne	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	%rbx, (%r14)
	movzbl	(%rbx), %ebp
	incq	%rbx
	testb	%bpl, %bpl
	jne	.LBB3_2
	jmp	.LBB3_7
.LBB3_4:                                # %.lr.ph.i2
	callq	__ctype_b_loc
	.p2align	4, 0x90
.LBB3_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movsbq	%bpl, %rdx
	testb	$32, 1(%rcx,%rdx,2)
	je	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=1
	movq	%rbx, (%r14)
	movzbl	(%rbx), %ebp
	incq	%rbx
	testb	%bpl, %bpl
	jne	.LBB3_5
.LBB3_7:                                # %S_skipspace.exit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	S_nextword, .Lfunc_end3-S_nextword
	.cfi_endproc

	.globl	S_wordcmp
	.p2align	4, 0x90
	.type	S_wordcmp,@function
S_wordcmp:                              # @S_wordcmp
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rbx, %rdi
	callq	strlen
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	strncmp                 # TAILCALL
.Lfunc_end4:
	.size	S_wordcmp, .Lfunc_end4-S_wordcmp
	.cfi_endproc

	.globl	S_trimzeros
	.p2align	4, 0x90
	.type	S_trimzeros,@function
S_trimzeros:                            # @S_trimzeros
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
.Lcfi30:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	strlen
	cmpq	$2, %rax
	jl	.LBB5_3
	.p2align	4, 0x90
.LBB5_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$48, -1(%rbx,%rax)
	jne	.LBB5_3
# BB#2:                                 #   in Loop: Header=BB5_1 Depth=1
	movb	$0, -1(%rbx,%rax)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB5_1
.LBB5_3:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end5:
	.size	S_trimzeros, .Lfunc_end5-S_trimzeros
	.cfi_endproc

	.globl	S_savestr
	.p2align	4, 0x90
	.type	S_savestr,@function
S_savestr:                              # @S_savestr
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -24
.Lcfi35:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rsi
	callq	S_allocstr
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	strcpy                  # TAILCALL
.Lfunc_end6:
	.size	S_savestr, .Lfunc_end6-S_savestr
	.cfi_endproc

	.globl	S_savenstr
	.p2align	4, 0x90
	.type	S_savenstr,@function
S_savenstr:                             # @S_savenstr
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 32
.Lcfi39:
	.cfi_offset %rbx, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	leal	1(%rbx), %eax
	movslq	%eax, %rdi
	xorl	%eax, %eax
	callq	_Z_myalloc
	movq	%rax, (%r15)
	movslq	%ebx, %rbx
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	strncpy
	movq	(%r15), %rax
	movb	$0, (%rax,%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	S_savenstr, .Lfunc_end7-S_savenstr
	.cfi_endproc

	.globl	S_allocstr
	.p2align	4, 0x90
	.type	S_allocstr,@function
S_allocstr:                             # @S_allocstr
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 16
.Lcfi43:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	incl	%esi
	movslq	%esi, %rdi
	xorl	%eax, %eax
	callq	_Z_myalloc
	movq	%rax, (%rbx)
	popq	%rbx
	retq
.Lfunc_end8:
	.size	S_allocstr, .Lfunc_end8-S_allocstr
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
