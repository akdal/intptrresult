	.text
	.file	"z07.bc"
	.globl	SplitIsDefinite
	.p2align	4, 0x90
	.type	SplitIsDefinite,@function
SplitIsDefinite:                        # @SplitIsDefinite
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpb	$9, 32(%rbx)
	je	.LBB0_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_2:
	movq	8(%rbx), %rcx
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %eax
	testb	%al, %al
	je	.LBB0_3
# BB#4:
	movq	(%rbx), %rdx
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rdx), %rdx
	movzbl	32(%rdx), %ecx
	testb	%cl, %cl
	je	.LBB0_5
# BB#6:
	addb	$-9, %al
	cmpb	$91, %al
	sbbb	%al, %al
	addb	$-9, %cl
	cmpb	$91, %cl
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	SplitIsDefinite, .Lfunc_end0-SplitIsDefinite
	.cfi_endproc

	.globl	DisposeObject
	.p2align	4, 0x90
	.type	DisposeObject,@function
DisposeObject:                          # @DisposeObject
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	24(%r14), %rax
	.p2align	4, 0x90
.LBB1_1:                                # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_14 Depth 2
                                        #     Child Loop BB1_17 Depth 2
                                        #     Child Loop BB1_37 Depth 2
                                        #     Child Loop BB1_40 Depth 2
	cmpq	%r14, %rax
	je	.LBB1_3
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	movb	32(%r14), %cl
	cmpb	$9, %cl
	movq	8(%r14), %rax
	jne	.LBB1_4
# BB#7:                                 #   in Loop: Header=BB1_1 Depth=1
	cmpq	%r14, %rax
	jne	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%r14), %rax
.LBB1_9:                                #   in Loop: Header=BB1_1 Depth=1
	movq	(%r14), %rcx
	cmpq	%rax, %rcx
	jne	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.18, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r14), %rcx
	movq	8(%r14), %rax
.LBB1_11:                               #   in Loop: Header=BB1_1 Depth=1
	cmpq	8(%rax), %rcx
	je	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.19, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_13:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%r14), %rax
	addq	$16, %rax
	movl	$1, %edi
	jmp	.LBB1_14
	.p2align	4, 0x90
.LBB1_67:                               #   in Loop: Header=BB1_14 Depth=2
	addq	$16, %rax
	incl	%edi
.LBB1_14:                               #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB1_67
# BB#15:                                #   in Loop: Header=BB1_1 Depth=1
	cmpb	$16, %cl
	jne	.LBB1_29
# BB#16:                                # %.preheader12.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	$1, %edx
	movq	%rax, %r15
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB1_17:                               # %.preheader12
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rsi), %rsi
	movq	8(%r15), %r15
	cmpq	%rax, %r15
	setne	%cl
	cmpq	%rax, %rsi
	setne	%bl
	cmpl	%edi, %edx
	jge	.LBB1_19
# BB#18:                                # %.preheader12
                                        #   in Loop: Header=BB1_17 Depth=2
	andb	%cl, %bl
	incl	%edx
	testb	%bl, %bl
	jne	.LBB1_17
.LBB1_19:                               # %.critedge.i
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpq	%rax, %rsi
	je	.LBB1_21
# BB#20:                                # %.critedge.i
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpq	%rax, %r15
	jne	.LBB1_22
.LBB1_21:                               #   in Loop: Header=BB1_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_22:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%r15, xx_link(%rip)
	movq	%r15, zz_hold(%rip)
	movq	24(%r15), %rax
	cmpq	%r15, %rax
	je	.LBB1_23
# BB#24:                                #   in Loop: Header=BB1_1 Depth=1
	movq	%rax, zz_res(%rip)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r15), %rcx
	movq	%rax, 24(%rcx)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	jmp	.LBB1_25
.LBB1_23:                               #   in Loop: Header=BB1_1 Depth=1
	xorl	%eax, %eax
.LBB1_25:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%rax, xx_tmp(%rip)
	movq	%r15, zz_hold(%rip)
	movq	8(%r15), %rax
	cmpq	%r15, %rax
	je	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_1 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%r15), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %r15
.LBB1_27:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%r15, zz_hold(%rip)
	movzbl	32(%r15), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r15), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r15)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_29
# BB#28:                                #   in Loop: Header=BB1_1 Depth=1
	callq	DisposeObject
	.p2align	4, 0x90
.LBB1_29:                               # %.loopexit13
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	8(%r14), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_30
# BB#31:                                #   in Loop: Header=BB1_1 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_32
	.p2align	4, 0x90
.LBB1_30:                               #   in Loop: Header=BB1_1 Depth=1
	xorl	%ecx, %ecx
.LBB1_32:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_34
# BB#33:                                #   in Loop: Header=BB1_1 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_34:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_36
# BB#35:                                #   in Loop: Header=BB1_1 Depth=1
	callq	DisposeObject
.LBB1_36:                               #   in Loop: Header=BB1_1 Depth=1
	movq	(%r14), %rax
	addq	$16, %rax
	movl	$1, %edi
	jmp	.LBB1_37
	.p2align	4, 0x90
.LBB1_68:                               #   in Loop: Header=BB1_37 Depth=2
	addq	$16, %rax
	incl	%edi
.LBB1_37:                               #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB1_68
# BB#38:                                #   in Loop: Header=BB1_1 Depth=1
	cmpb	$15, %cl
	jne	.LBB1_52
# BB#39:                                # %.preheader11.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	$1, %edx
	movq	%rax, %r15
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB1_40:                               # %.preheader11
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rsi), %rsi
	movq	8(%r15), %r15
	cmpq	%rax, %r15
	setne	%cl
	cmpq	%rax, %rsi
	setne	%bl
	cmpl	%edi, %edx
	jge	.LBB1_42
# BB#41:                                # %.preheader11
                                        #   in Loop: Header=BB1_40 Depth=2
	andb	%cl, %bl
	incl	%edx
	testb	%bl, %bl
	jne	.LBB1_40
.LBB1_42:                               # %.critedge4.i
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpq	%rax, %rsi
	je	.LBB1_44
# BB#43:                                # %.critedge4.i
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpq	%rax, %r15
	jne	.LBB1_45
.LBB1_44:                               #   in Loop: Header=BB1_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.21, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_45:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%r15, xx_link(%rip)
	movq	%r15, zz_hold(%rip)
	movq	24(%r15), %rax
	cmpq	%r15, %rax
	je	.LBB1_46
# BB#47:                                #   in Loop: Header=BB1_1 Depth=1
	movq	%rax, zz_res(%rip)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r15), %rcx
	movq	%rax, 24(%rcx)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	jmp	.LBB1_48
.LBB1_46:                               #   in Loop: Header=BB1_1 Depth=1
	xorl	%eax, %eax
.LBB1_48:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%rax, xx_tmp(%rip)
	movq	%r15, zz_hold(%rip)
	movq	8(%r15), %rax
	cmpq	%r15, %rax
	je	.LBB1_50
# BB#49:                                #   in Loop: Header=BB1_1 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%r15), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %r15
.LBB1_50:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%r15, zz_hold(%rip)
	movzbl	32(%r15), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r15), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r15)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_52
# BB#51:                                #   in Loop: Header=BB1_1 Depth=1
	callq	DisposeObject
	.p2align	4, 0x90
.LBB1_52:                               # %.loopexit
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	(%r14), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_53
# BB#54:                                #   in Loop: Header=BB1_1 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_55
	.p2align	4, 0x90
.LBB1_53:                               #   in Loop: Header=BB1_1 Depth=1
	xorl	%ecx, %ecx
.LBB1_55:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_57
# BB#56:                                #   in Loop: Header=BB1_1 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_57:                               #   in Loop: Header=BB1_1 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rax
	cmpq	%rax, 24(%rax)
	movq	%rax, %r14
	je	.LBB1_1
	jmp	.LBB1_66
.LBB1_4:                                # %.preheader
	cmpq	%r14, %rax
	je	.LBB1_65
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_6
# BB#58:                                #   in Loop: Header=BB1_5 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_59
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_5 Depth=1
	xorl	%ecx, %ecx
.LBB1_59:                               #   in Loop: Header=BB1_5 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_61
# BB#60:                                #   in Loop: Header=BB1_5 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_61:                               #   in Loop: Header=BB1_5 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_63
# BB#62:                                #   in Loop: Header=BB1_5 Depth=1
	callq	DisposeObject
.LBB1_63:                               # %.backedge
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	8(%r14), %rax
	cmpq	%r14, %rax
	jne	.LBB1_5
# BB#64:                                # %._crit_edge.loopexit
	movb	32(%r14), %cl
.LBB1_65:                               # %._crit_edge
	movq	%r14, zz_hold(%rip)
	movzbl	%cl, %eax
	addb	$-11, %cl
	leaq	33(%r14), %rdx
	cmpb	$2, %cl
	leaq	zz_lengths(%rax), %rax
	cmovbq	%rdx, %rax
	movzbl	(%rax), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r14)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
.LBB1_66:                               # %DisposeSplitObject.exit
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	DisposeObject, .Lfunc_end1-DisposeObject
	.cfi_endproc

	.globl	MakeWord
	.p2align	4, 0x90
	.type	MakeWord,@function
MakeWord:                               # @MakeWord
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 48
.Lcfi13:
	.cfi_offset %rbx, -40
.Lcfi14:
	.cfi_offset %r14, -32
.Lcfi15:
	.cfi_offset %r15, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movl	%edi, %r15d
	movq	%r14, %rdi
	callq	strlen
	shlq	$32, %rax
	movabsq	$292057776128, %rdi     # imm = 0x4400000000
	addq	%rax, %rdi
	sarq	$32, %rdi
	shrq	$3, %rdi
	incq	%rdi
	movl	%edi, zz_size(%rip)
	movslq	%edi, %rax
	cmpq	$265, %rax              # imm = 0x109
	jb	.LBB2_2
# BB#1:
	movl	$1, %edi
	movl	$1, %esi
	movl	$.L.str.3, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movq	zz_hold(%rip), %rbp
	jmp	.LBB2_5
.LBB2_2:
	movq	zz_free(,%rax,8), %rbp
	testq	%rbp, %rbp
	je	.LBB2_3
# BB#4:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movslq	%edi, %rcx
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB2_5
.LBB2_3:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%rbx, %rsi
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB2_5:
	movb	zz_size(%rip), %al
	movb	%al, 33(%rbp)
	movb	%r15b, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	leaq	64(%rbp), %rdi
	movq	%r14, %rsi
	callq	strcpy
	movzwl	2(%rbx), %eax
	movw	%ax, 34(%rbp)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	4(%rbx), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbp), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbp)
	andl	4(%rbx), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	MakeWord, .Lfunc_end2-MakeWord
	.cfi_endproc

	.globl	MakeWordTwo
	.p2align	4, 0x90
	.type	MakeWordTwo,@function
MakeWordTwo:                            # @MakeWordTwo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 64
.Lcfi24:
	.cfi_offset %rbx, -56
.Lcfi25:
	.cfi_offset %r12, -48
.Lcfi26:
	.cfi_offset %r13, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movl	%edi, %r13d
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r12
	movq	%r14, %rdi
	callq	strlen
	addl	%r12d, %eax
	shlq	$32, %rax
	movabsq	$292057776128, %rdi     # imm = 0x4400000000
	addq	%rax, %rdi
	sarq	$32, %rdi
	shrq	$3, %rdi
	incq	%rdi
	movl	%edi, zz_size(%rip)
	movslq	%edi, %rax
	cmpq	$265, %rax              # imm = 0x109
	jb	.LBB3_2
# BB#1:
	movl	$1, %edi
	movl	$1, %esi
	movl	$.L.str.3, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movq	zz_hold(%rip), %rbp
	jmp	.LBB3_5
.LBB3_2:
	movq	zz_free(,%rax,8), %rbp
	testq	%rbp, %rbp
	je	.LBB3_3
# BB#4:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movslq	%edi, %rcx
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB3_5
.LBB3_3:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%rbx, %rsi
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB3_5:
	movb	zz_size(%rip), %al
	movb	%al, 33(%rbp)
	movb	%r13b, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	leaq	64(%rbp), %rdi
	movq	%r15, %rsi
	callq	strcpy
	movslq	%r12d, %rax
	leaq	64(%rbp,%rax), %rdi
	movq	%r14, %rsi
	callq	strcpy
	movzwl	2(%rbx), %eax
	movw	%ax, 34(%rbp)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	4(%rbx), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbp), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbp)
	andl	4(%rbx), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	MakeWordTwo, .Lfunc_end3-MakeWordTwo
	.cfi_endproc

	.globl	MakeWordThree
	.p2align	4, 0x90
	.type	MakeWordThree,@function
MakeWordThree:                          # @MakeWordThree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 64
.Lcfi37:
	.cfi_offset %rbx, -56
.Lcfi38:
	.cfi_offset %r12, -48
.Lcfi39:
	.cfi_offset %r13, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	callq	strlen
	movq	%rax, %r13
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	strlen
	addl	%r13d, %ebx
	leal	68(%rax,%rbx), %eax
	movslq	%eax, %rdi
	shrq	$3, %rdi
	incq	%rdi
	movl	%edi, zz_size(%rip)
	movslq	%edi, %rax
	cmpq	$265, %rax              # imm = 0x109
	jb	.LBB4_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$1, %esi
	movl	$.L.str.3, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	zz_hold(%rip), %rbp
	jmp	.LBB4_5
.LBB4_2:
	movq	zz_free(,%rax,8), %rbp
	testq	%rbp, %rbp
	je	.LBB4_3
# BB#4:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movslq	%edi, %rcx
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB4_5
.LBB4_3:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB4_5:
	movb	zz_size(%rip), %al
	movb	%al, 33(%rbp)
	movb	$11, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	leaq	64(%rbp), %rdi
	movq	%r12, %rsi
	callq	strcpy
	movslq	%r13d, %rax
	leaq	64(%rbp,%rax), %rdi
	movq	%r15, %rsi
	callq	strcpy
	movslq	%ebx, %rax
	leaq	64(%rbp,%rax), %rdi
	movq	%r14, %rsi
	callq	strcpy
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	MakeWordThree, .Lfunc_end4-MakeWordThree
	.cfi_endproc

	.globl	CopyObject
	.p2align	4, 0x90
	.type	CopyObject,@function
CopyObject:                             # @CopyObject
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 64
.Lcfi49:
	.cfi_offset %rbx, -48
.Lcfi50:
	.cfi_offset %r12, -40
.Lcfi51:
	.cfi_offset %r13, -32
.Lcfi52:
	.cfi_offset %r14, -24
.Lcfi53:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movb	32(%r15), %al
	movzbl	%al, %edi
	movl	%edi, %ecx
	decb	%cl
	cmpb	$98, %cl
	ja	.LBB5_110
# BB#1:
	movzbl	%cl, %ecx
	movq	%r15, %r13
	jmpq	*.LJTI5_0(,%rcx,8)
.LBB5_22:
	movzbl	zz_lengths(%rdi), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r13
	testq	%r13, %r13
	je	.LBB5_23
# BB#24:
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_25
.LBB5_23:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r13
	movq	%r13, zz_hold(%rip)
	movb	32(%r15), %al
.LBB5_25:
	movb	%al, 32(%r13)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%r13, 8(%r13)
	movq	%r13, (%r13)
	movq	8(%r15), %rbx
	cmpq	%r15, %rbx
	je	.LBB5_108
# BB#26:                                # %.preheader228.lr.ph
	testq	%r13, %r13
	je	.LBB5_27
	.p2align	4, 0x90
.LBB5_36:                               # %.preheader228
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_37 Depth 2
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB5_37:                               #   Parent Loop BB5_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB5_37
# BB#38:                                #   in Loop: Header=BB5_36 Depth=1
	movq	%r14, %rsi
	callq	CopyObject
	movq	%rax, %r12
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_39
# BB#40:                                #   in Loop: Header=BB5_36 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_41
	.p2align	4, 0x90
.LBB5_39:                               #   in Loop: Header=BB5_36 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_41:                               #   in Loop: Header=BB5_36 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_43
# BB#42:                                #   in Loop: Header=BB5_36 Depth=1
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_43:                               #   in Loop: Header=BB5_36 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB5_45
# BB#44:                                #   in Loop: Header=BB5_36 Depth=1
	testq	%rax, %rax
	je	.LBB5_45
# BB#46:                                #   in Loop: Header=BB5_36 Depth=1
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_45:                               # %.backedge
                                        #   in Loop: Header=BB5_36 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB5_36
	jmp	.LBB5_108
	.p2align	4, 0x90
.LBB5_27:                               # %.preheader228.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_28 Depth 2
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB5_28:                               #   Parent Loop BB5_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB5_28
# BB#29:                                #   in Loop: Header=BB5_27 Depth=1
	movq	%r14, %rsi
	callq	CopyObject
	movq	%rax, %r12
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_31
# BB#30:                                #   in Loop: Header=BB5_27 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_32
	.p2align	4, 0x90
.LBB5_31:                               #   in Loop: Header=BB5_27 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_32:                               #   in Loop: Header=BB5_27 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB5_35
# BB#33:                                #   in Loop: Header=BB5_27 Depth=1
	testq	%rax, %rax
	je	.LBB5_35
# BB#34:                                #   in Loop: Header=BB5_27 Depth=1
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_35:                               # %.backedge.us
                                        #   in Loop: Header=BB5_27 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB5_27
.LBB5_108:                              # %.loopexit
	leaq	34(%r15), %rax
	addq	$36, %r15
	cmpq	%r14, no_fpos(%rip)
	leaq	2(%r14), %rcx
	leaq	4(%r14), %rdx
	cmoveq	%rax, %rcx
	cmovneq	%rdx, %r15
	movzwl	(%rcx), %eax
	movw	%ax, 34(%r13)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	(%r15), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%r13), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%r13)
	andl	(%r15), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r13)
	movq	%r13, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB5_2:
	leaq	64(%r15), %r12
	movq	%r12, %rdi
	callq	strlen
	shlq	$32, %rax
	movabsq	$292057776128, %rdi     # imm = 0x4400000000
	addq	%rax, %rdi
	sarq	$32, %rdi
	shrq	$3, %rdi
	incq	%rdi
	movl	%edi, zz_size(%rip)
	movslq	%edi, %rax
	cmpq	$265, %rax              # imm = 0x109
	jb	.LBB5_4
# BB#3:
	movl	$1, %edi
	movl	$1, %esi
	movl	$.L.str.3, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
	movq	zz_hold(%rip), %r13
	jmp	.LBB5_7
.LBB5_8:
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r13
	testq	%r13, %r13
	je	.LBB5_9
# BB#10:
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB5_11
.LBB5_79:
	movzbl	zz_lengths+2(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r13
	testq	%r13, %r13
	je	.LBB5_80
# BB#81:
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB5_82
.LBB5_72:
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r13
	testq	%r13, %r13
	je	.LBB5_73
# BB#74:
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB5_75
.LBB5_47:
	movzbl	zz_lengths+57(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r13
	testq	%r13, %r13
	je	.LBB5_48
# BB#49:
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movq	%rax, zz_free(,%rdi,8)
	movb	$57, %al
	jmp	.LBB5_50
.LBB5_4:
	movq	zz_free(,%rax,8), %r13
	testq	%r13, %r13
	je	.LBB5_5
# BB#6:
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movslq	%edi, %rcx
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB5_7
.LBB5_9:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r13
	movq	%r13, zz_hold(%rip)
.LBB5_11:
	movb	$1, 32(%r13)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%r13, 8(%r13)
	movq	%r13, (%r13)
	movzwl	44(%r15), %eax
	andl	$256, %eax              # imm = 0x100
	movzwl	44(%r13), %ecx
	andl	$65279, %ecx            # imm = 0xFEFF
	orl	%eax, %ecx
	movw	%cx, 44(%r13)
	movzwl	44(%r15), %eax
	andl	$512, %eax              # imm = 0x200
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	%eax, %ecx
	movw	%cx, 44(%r13)
	movb	41(%r15), %al
	movb	%al, 41(%r13)
	movb	42(%r15), %al
	movb	%al, 42(%r13)
	movq	8(%r15), %rdi
	cmpq	%r15, %rdi
	je	.LBB5_108
	.p2align	4, 0x90
.LBB5_12:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB5_12
	jmp	.LBB5_13
.LBB5_80:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r13
	movq	%r13, zz_hold(%rip)
.LBB5_82:
	movb	$2, 32(%r13)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%r13, 8(%r13)
	movq	%r13, (%r13)
	movq	8(%r15), %r12
	cmpq	%r15, %r12
	je	.LBB5_107
# BB#83:                                # %.preheader232.lr.ph
	testq	%r13, %r13
	je	.LBB5_84
.LBB5_95:                               # %.preheader232
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_96 Depth 2
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB5_96:                               #   Parent Loop BB5_95 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB5_96
# BB#97:                                #   in Loop: Header=BB5_95 Depth=1
	cmpb	$2, %al
	jne	.LBB5_99
# BB#98:                                #   in Loop: Header=BB5_95 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_99:                               # %.loopexit233
                                        #   in Loop: Header=BB5_95 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	CopyObject
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_100
# BB#101:                               #   in Loop: Header=BB5_95 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_102
.LBB5_100:                              #   in Loop: Header=BB5_95 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_102:                              #   in Loop: Header=BB5_95 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_104
# BB#103:                               #   in Loop: Header=BB5_95 Depth=1
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_104:                              #   in Loop: Header=BB5_95 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB5_106
# BB#105:                               #   in Loop: Header=BB5_95 Depth=1
	testq	%rax, %rax
	je	.LBB5_106
# BB#109:                               #   in Loop: Header=BB5_95 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_106:                              # %.backedge234
                                        #   in Loop: Header=BB5_95 Depth=1
	movq	8(%r12), %r12
	cmpq	%r15, %r12
	jne	.LBB5_95
	jmp	.LBB5_107
.LBB5_84:                               # %.preheader232.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_85 Depth 2
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB5_85:                               #   Parent Loop BB5_84 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB5_85
# BB#86:                                #   in Loop: Header=BB5_84 Depth=1
	cmpb	$2, %al
	jne	.LBB5_88
# BB#87:                                #   in Loop: Header=BB5_84 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_88:                               # %.loopexit233.us
                                        #   in Loop: Header=BB5_84 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	CopyObject
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_90
# BB#89:                                #   in Loop: Header=BB5_84 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_91
.LBB5_90:                               #   in Loop: Header=BB5_84 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_91:                               #   in Loop: Header=BB5_84 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB5_94
# BB#92:                                #   in Loop: Header=BB5_84 Depth=1
	testq	%rax, %rax
	je	.LBB5_94
# BB#93:                                #   in Loop: Header=BB5_84 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_94:                               # %.backedge234.us
                                        #   in Loop: Header=BB5_84 Depth=1
	movq	8(%r12), %r12
	cmpq	%r15, %r12
	jne	.LBB5_84
.LBB5_107:                              # %._crit_edge
	movq	80(%r15), %rax
	movq	%rax, 80(%r13)
	movzwl	64(%r15), %ecx
	andl	$128, %ecx
	movzwl	64(%r13), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 64(%r13)
	movzwl	64(%r15), %ecx
	andl	$256, %ecx              # imm = 0x100
	movl	%eax, %edx
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	64(%r15), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	64(%r15), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	64(%r15), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	66(%r15), %ecx
	movw	%cx, 66(%r13)
	movb	68(%r15), %cl
	andb	$3, %cl
	movb	68(%r13), %dl
	andb	$-4, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	68(%r15), %cl
	andb	$12, %cl
	andb	$-13, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	68(%r15), %cl
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	64(%r15), %cl
	andb	$8, %cl
	andb	$-9, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	68(%r15), %edx
	andl	$128, %edx
	movzwl	68(%r13), %ecx
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	68(%r15), %edx
	andl	$256, %edx              # imm = 0x100
	andl	$-257, %ecx             # imm = 0xFEFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	68(%r15), %edx
	andl	$512, %edx              # imm = 0x200
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	68(%r15), %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$-7169, %ecx            # imm = 0xE3FF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	68(%r15), %edx
	andl	$57344, %edx            # imm = 0xE000
	andl	$8191, %ecx             # imm = 0x1FFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	70(%r15), %ecx
	movw	%cx, 70(%r13)
	movl	$4095, %edx             # imm = 0xFFF
	andl	76(%r15), %edx
	movl	$-4096, %ecx            # imm = 0xF000
	andl	76(%r13), %ecx
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	76(%r15), %edx
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$12582912, %edx         # imm = 0xC00000
	andl	76(%r15), %edx
	andl	$-12582913, %ecx        # imm = 0xFF3FFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1056964608, %edx       # imm = 0x3F000000
	andl	76(%r15), %edx
	andl	$-1056964609, %ecx      # imm = 0xC0FFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	76(%r15), %edx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1073741824, %edx       # imm = 0x40000000
	andl	76(%r15), %edx
	andl	$-1073741825, %ecx      # imm = 0xBFFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movb	64(%r15), %cl
	andb	$1, %cl
	andb	$-2, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	64(%r15), %cl
	andb	$2, %cl
	andb	$-3, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	64(%r15), %cl
	andb	$4, %cl
	andb	$-5, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	64(%r15), %cl
	andb	$112, %cl
	andb	$-113, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	72(%r15), %eax
	movw	%ax, 72(%r13)
	movzwl	74(%r15), %eax
	movw	%ax, 74(%r13)
	jmp	.LBB5_108
.LBB5_73:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r13
	movq	%r13, zz_hold(%rip)
.LBB5_75:
	movb	$10, 32(%r13)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%r13, 8(%r13)
	movq	%r13, (%r13)
	movq	80(%r15), %rax
	movq	%rax, 80(%r13)
	movq	8(%r15), %rax
	cmpq	%r15, %rax
	jne	.LBB5_77
# BB#76:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%r15), %rax
.LBB5_77:
	addq	$16, %rax
	.p2align	4, 0x90
.LBB5_78:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB5_78
.LBB5_13:
	movq	%r14, %rsi
	callq	CopyObject
	movq	%rax, %r12
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_14
# BB#15:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_16
.LBB5_14:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_16:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB5_19
# BB#17:
	testq	%rax, %rax
	je	.LBB5_19
# BB#18:
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_19:
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB5_108
# BB#20:
	testq	%rax, %rax
	je	.LBB5_108
# BB#21:
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB5_108
.LBB5_48:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r13
	movq	%r13, zz_hold(%rip)
	movb	32(%r15), %al
.LBB5_50:
	movb	%al, 32(%r13)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%r13, 8(%r13)
	movq	%r13, (%r13)
	movq	8(%r15), %r12
	cmpq	%r15, %r12
	je	.LBB5_108
# BB#51:                                # %.preheader229.lr.ph
	testq	%r13, %r13
	je	.LBB5_52
.LBB5_61:                               # %.preheader229
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_62 Depth 2
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB5_62:                               #   Parent Loop BB5_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB5_62
# BB#63:                                #   in Loop: Header=BB5_61 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_64
# BB#65:                                #   in Loop: Header=BB5_61 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_66
.LBB5_64:                               #   in Loop: Header=BB5_61 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_66:                               #   in Loop: Header=BB5_61 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_68
# BB#67:                                #   in Loop: Header=BB5_61 Depth=1
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_68:                               #   in Loop: Header=BB5_61 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB5_70
# BB#69:                                #   in Loop: Header=BB5_61 Depth=1
	testq	%rax, %rax
	je	.LBB5_70
# BB#71:                                #   in Loop: Header=BB5_61 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_70:                               # %.backedge231
                                        #   in Loop: Header=BB5_61 Depth=1
	movq	8(%r12), %r12
	cmpq	%r15, %r12
	jne	.LBB5_61
	jmp	.LBB5_108
.LBB5_52:                               # %.preheader229.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_53 Depth 2
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB5_53:                               #   Parent Loop BB5_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB5_53
# BB#54:                                #   in Loop: Header=BB5_52 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_56
# BB#55:                                #   in Loop: Header=BB5_52 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_57
.LBB5_56:                               #   in Loop: Header=BB5_52 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_57:                               #   in Loop: Header=BB5_52 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB5_60
# BB#58:                                #   in Loop: Header=BB5_52 Depth=1
	testq	%rax, %rax
	je	.LBB5_60
# BB#59:                                #   in Loop: Header=BB5_52 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_60:                               # %.backedge231.us
                                        #   in Loop: Header=BB5_52 Depth=1
	movq	8(%r12), %r12
	cmpq	%r15, %r12
	jne	.LBB5_52
	jmp	.LBB5_108
.LBB5_5:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r14, %rsi
	callq	GetMemory
	movq	%rax, %r13
	movq	%r13, zz_hold(%rip)
.LBB5_7:
	movb	zz_size(%rip), %al
	movb	%al, 33(%r13)
	movb	32(%r15), %al
	movb	%al, 32(%r13)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%r13, 8(%r13)
	movq	%r13, (%r13)
	movq	%r13, %rdi
	addq	$64, %rdi
	movq	%r12, %rsi
	callq	strcpy
	jmp	.LBB5_108
.LBB5_110:
	movq	no_fpos(%rip), %rbx
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	Image
	movq	%rax, (%rsp)
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.6, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	subq	$8, %rsp
.Lfunc_end5:
	.size	CopyObject, .Lfunc_end5-CopyObject
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_8
	.quad	.LBB5_79
	.quad	.LBB5_110
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_110
	.quad	.LBB5_110
	.quad	.LBB5_72
	.quad	.LBB5_2
	.quad	.LBB5_2
	.quad	.LBB5_110
	.quad	.LBB5_110
	.quad	.LBB5_110
	.quad	.LBB5_110
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_47
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_108
	.quad	.LBB5_110
	.quad	.LBB5_110
	.quad	.LBB5_110
	.quad	.LBB5_110
	.quad	.LBB5_110
	.quad	.LBB5_110
	.quad	.LBB5_110
	.quad	.LBB5_110
	.quad	.LBB5_110
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22
	.quad	.LBB5_22

	.text
	.globl	InsertObject
	.p2align	4, 0x90
	.type	InsertObject,@function
InsertObject:                           # @InsertObject
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi60:
	.cfi_def_cfa_offset 80
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	movl	$-1048576, %ebp         # imm = 0xFFF00000
	movl	$1073741824, %r14d      # imm = 0x40000000
	jmp	.LBB6_1
	.p2align	4, 0x90
.LBB6_24:                               #   in Loop: Header=BB6_1 Depth=1
	movzwl	(%r12), %ecx
	andl	$128, %ecx
	movzwl	64(%rbx), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 64(%rbx)
	movzwl	(%r12), %ecx
	andl	$256, %ecx              # imm = 0x100
	movl	%eax, %edx
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 64(%rbx)
	movzwl	(%r12), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 64(%rbx)
	movzwl	(%r12), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%ecx, %edx
	movw	%dx, 64(%rbx)
	movzwl	(%r12), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%ecx, %edx
	movw	%dx, 64(%rbx)
	movzwl	2(%r12), %ecx
	movw	%cx, 66(%rbx)
	movzbl	4(%r12), %ecx
	andb	$3, %cl
	movzbl	68(%rbx), %edx
	andb	$-4, %dl
	orb	%cl, %dl
	movb	%dl, 68(%rbx)
	movzbl	4(%r12), %ecx
	andb	$12, %cl
	andb	$-13, %dl
	orb	%cl, %dl
	movb	%dl, 68(%rbx)
	movzbl	4(%r12), %ecx
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 68(%rbx)
	movzbl	(%r12), %ecx
	andb	$8, %cl
	andb	$-9, %al
	orb	%cl, %al
	movb	%al, 64(%rbx)
	movzwl	4(%r12), %edx
	andl	$128, %edx
	movzwl	68(%rbx), %ecx
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%edx, %ecx
	movw	%cx, 68(%rbx)
	movzwl	4(%r12), %edx
	andl	$256, %edx              # imm = 0x100
	andl	$-257, %ecx             # imm = 0xFEFF
	orl	%edx, %ecx
	movw	%cx, 68(%rbx)
	movzwl	4(%r12), %edx
	andl	$512, %edx              # imm = 0x200
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	%edx, %ecx
	movw	%cx, 68(%rbx)
	movzwl	4(%r12), %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$-7169, %ecx            # imm = 0xE3FF
	orl	%edx, %ecx
	movw	%cx, 68(%rbx)
	movzwl	4(%r12), %edx
	andl	$57344, %edx            # imm = 0xE000
	andl	$8191, %ecx             # imm = 0x1FFF
	orl	%edx, %ecx
	movw	%cx, 68(%rbx)
	movzwl	6(%r12), %ecx
	movw	%cx, 70(%rbx)
	movl	12(%r12), %edx
	movl	$4095, %ecx             # imm = 0xFFF
	andl	%ecx, %edx
	movl	76(%rbx), %ecx
	movl	$-4096, %esi            # imm = 0xF000
	andl	%esi, %ecx
	orl	%edx, %ecx
	movl	%ecx, 76(%rbx)
	movl	12(%r12), %edx
	movl	$4190208, %esi          # imm = 0x3FF000
	andl	%esi, %edx
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rbx)
	movl	12(%r12), %edx
	movl	$12582912, %esi         # imm = 0xC00000
	andl	%esi, %edx
	andl	$-12582913, %ecx        # imm = 0xFF3FFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rbx)
	movl	12(%r12), %edx
	movl	$1056964608, %esi       # imm = 0x3F000000
	andl	%esi, %edx
	andl	$-1056964609, %ecx      # imm = 0xC0FFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rbx)
	movl	12(%r12), %edx
	movl	$-2147483648, %esi      # imm = 0x80000000
	andl	%esi, %edx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rbx)
	movl	12(%r12), %edx
	andl	%r14d, %edx
	andl	$-1073741825, %ecx      # imm = 0xBFFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rbx)
	movzbl	(%r12), %ecx
	andb	$1, %cl
	andb	$-2, %al
	orb	%cl, %al
	movb	%al, 64(%rbx)
	movzbl	(%r12), %ecx
	andb	$2, %cl
	andb	$-3, %al
	orb	%cl, %al
	movb	%al, 64(%rbx)
	movzbl	(%r12), %ecx
	andb	$4, %cl
	andb	$-5, %al
	orb	%cl, %al
	movb	%al, 64(%rbx)
	movzbl	(%r12), %ecx
	andb	$112, %cl
	andb	$-113, %al
	orb	%cl, %al
	movb	%al, 64(%rbx)
	movzwl	8(%r12), %eax
	movw	%ax, 72(%rbx)
	movzwl	10(%r12), %eax
	movw	%ax, 74(%rbx)
	movzbl	(%r12), %eax
	shrb	$2, %al
	movzwl	42(%rbx), %ecx
	andb	$1, %al
	movzbl	%al, %eax
	shll	$11, %eax
	andl	$63487, %ecx            # imm = 0xF7FF
	orl	%eax, %ecx
	movw	%cx, 42(%rbx)
	movq	%rbx, %r15
.LBB6_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movzbl	32(%r15), %edi
	movl	%edi, %eax
	addb	$-2, %al
	cmpb	$97, %al
	ja	.LBB6_50
# BB#2:                                 # %tailrecurse
                                        #   in Loop: Header=BB6_1 Depth=1
	movzbl	%al, %eax
	jmpq	*.LJTI6_0(,%rax,8)
.LBB6_9:                                #   in Loop: Header=BB6_1 Depth=1
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB6_10
# BB#11:                                #   in Loop: Header=BB6_1 Depth=1
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB6_12
	.p2align	4, 0x90
.LBB6_10:                               #   in Loop: Header=BB6_1 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB6_12:                               #   in Loop: Header=BB6_1 Depth=1
	movb	$17, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	34(%r15), %eax
	movw	%ax, 34(%rbx)
	movl	36(%r15), %eax
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %eax
	movl	36(%rbx), %ecx
	andl	%ebp, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movl	36(%r15), %ecx
	andl	%ebp, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movq	%r15, zz_hold(%rip)
	movq	24(%r15), %rax
	cmpq	%r15, %rax
	je	.LBB6_13
# BB#14:                                #   in Loop: Header=BB6_1 Depth=1
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r15), %rcx
	movq	%rax, 24(%rcx)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%r13b
	je	.LBB6_17
# BB#15:                                #   in Loop: Header=BB6_1 Depth=1
	testq	%rax, %rax
	je	.LBB6_17
# BB#16:                                #   in Loop: Header=BB6_1 Depth=1
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB6_17
	.p2align	4, 0x90
.LBB6_13:                               # %.thread
                                        #   in Loop: Header=BB6_1 Depth=1
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%r13b
.LBB6_17:                               #   in Loop: Header=BB6_1 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_18
# BB#19:                                #   in Loop: Header=BB6_1 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_20
	.p2align	4, 0x90
.LBB6_18:                               #   in Loop: Header=BB6_1 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_20:                               #   in Loop: Header=BB6_1 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	sete	%cl
	orb	%cl, %r13b
	jne	.LBB6_22
# BB#21:                                #   in Loop: Header=BB6_1 Depth=1
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_22:                               #   in Loop: Header=BB6_1 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB6_24
# BB#23:                                #   in Loop: Header=BB6_1 Depth=1
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB6_24
.LBB6_25:
	movq	(%r15), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB6_26:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB6_26
# BB#27:
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	InsertObject
.LBB6_51:                               # %.critedge
	movq	%r15, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_3:                                # %.preheader159
	movq	8(%r15), %rbx
	cmpq	%r15, %rbx
	jne	.LBB6_5
	jmp	.LBB6_51
	.p2align	4, 0x90
.LBB6_8:                                #   in Loop: Header=BB6_5 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	InsertObject
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	je	.LBB6_51
.LBB6_5:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_7 Depth 2
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, (%rax)
	je	.LBB6_51
# BB#6:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB6_5 Depth=1
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB6_7:                                # %.preheader
                                        #   Parent Loop BB6_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB6_7
	jmp	.LBB6_8
.LBB6_50:
	movq	no_fpos(%rip), %rbx
	callq	Image
	movq	%rax, (%rsp)
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.6, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	jmp	.LBB6_51
.LBB6_28:
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB6_29
# BB#30:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB6_31
.LBB6_29:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB6_31:
	movb	$1, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	44(%rbx), %eax
	andl	$127, %eax
	orl	$9728, %eax             # imm = 0x2600
	movw	%ax, 44(%rbx)
	movw	$0, 46(%rbx)
	movw	$0, 41(%rbx)
	movl	$-1610612737, %eax      # imm = 0x9FFFFFFF
	andl	40(%rbx), %eax
	orl	$536870912, %eax        # imm = 0x20000000
	movl	%eax, 40(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_32
# BB#33:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_34
.LBB6_32:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_34:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r15), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB6_37
# BB#35:
	testq	%rcx, %rcx
	je	.LBB6_37
# BB#36:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_37:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB6_40
# BB#38:
	testq	%rax, %rax
	je	.LBB6_40
# BB#39:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB6_40:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_41
# BB#42:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_43
.LBB6_41:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_43:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r15), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB6_46
# BB#44:
	testq	%rcx, %rcx
	je	.LBB6_46
# BB#45:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_46:
	movq	%rax, zz_res(%rip)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB6_49
# BB#47:
	testq	%rcx, %rcx
	je	.LBB6_49
# BB#48:
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB6_49:
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rax
	movl	$-1610612737, %ecx      # imm = 0x9FFFFFFF
	andl	40(%rax), %ecx
	orl	$536870912, %ecx        # imm = 0x20000000
	movl	%ecx, 40(%rax)
	movq	$0, (%rdx)
	jmp	.LBB6_51
.Lfunc_end6:
	.size	InsertObject, .Lfunc_end6-InsertObject
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_51
	.quad	.LBB6_50
	.quad	.LBB6_51
	.quad	.LBB6_51
	.quad	.LBB6_51
	.quad	.LBB6_51
	.quad	.LBB6_51
	.quad	.LBB6_3
	.quad	.LBB6_50
	.quad	.LBB6_9
	.quad	.LBB6_9
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_3
	.quad	.LBB6_3
	.quad	.LBB6_28
	.quad	.LBB6_3
	.quad	.LBB6_3
	.quad	.LBB6_51
	.quad	.LBB6_51
	.quad	.LBB6_51
	.quad	.LBB6_51
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_51
	.quad	.LBB6_51
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_50
	.quad	.LBB6_51
	.quad	.LBB6_51
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25
	.quad	.LBB6_25

	.text
	.globl	Meld
	.p2align	4, 0x90
	.type	Meld,@function
Meld:                                   # @Meld
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 56
	subq	$3128, %rsp             # imm = 0xC38
.Lcfi73:
	.cfi_def_cfa_offset 3184
.Lcfi74:
	.cfi_offset %rbx, -56
.Lcfi75:
	.cfi_offset %r12, -48
.Lcfi76:
	.cfi_offset %r13, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpb	$17, 32(%r12)
	je	.LBB7_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.9, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_2:
	cmpb	$17, 32(%r15)
	je	.LBB7_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.10, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_4:
	movq	$0, 304(%rsp)
	movq	8(%r12), %rbx
	movl	$1, %r14d
	cmpq	%r12, %rbx
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r12, 24(%rsp)          # 8-byte Spill
	je	.LBB7_42
# BB#5:                                 # %.preheader402.lr.ph.preheader
	leaq	32(%r12), %r8
	.p2align	4, 0x90
.LBB7_6:                                # %.preheader402
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_7 Depth 2
                                        #     Child Loop BB7_12 Depth 2
                                        #     Child Loop BB7_14 Depth 2
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB7_7:                                #   Parent Loop BB7_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB7_7
# BB#8:                                 #   in Loop: Header=BB7_6 Depth=1
	cmpb	$9, %al
	je	.LBB7_11
# BB#9:                                 #   in Loop: Header=BB7_6 Depth=1
	cmpb	$1, %al
	je	.LBB7_17
# BB#10:                                #   in Loop: Header=BB7_6 Depth=1
	addb	$-9, %al
	cmpb	$91, %al
	jae	.LBB7_17
	jmp	.LBB7_19
	.p2align	4, 0x90
.LBB7_11:                               #   in Loop: Header=BB7_6 Depth=1
	movq	8(%rbp), %rcx
	.p2align	4, 0x90
.LBB7_12:                               #   Parent Loop BB7_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %eax
	testb	%al, %al
	je	.LBB7_12
# BB#13:                                #   in Loop: Header=BB7_6 Depth=1
	movq	(%rbp), %rdx
	.p2align	4, 0x90
.LBB7_14:                               #   Parent Loop BB7_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdx), %rdx
	movzbl	32(%rdx), %ecx
	testb	%cl, %cl
	je	.LBB7_14
# BB#15:                                # %SplitIsDefinite.exit
                                        #   in Loop: Header=BB7_6 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB7_17
# BB#16:                                # %SplitIsDefinite.exit
                                        #   in Loop: Header=BB7_6 Depth=1
	addb	$-9, %cl
	cmpb	$91, %cl
	jb	.LBB7_19
	.p2align	4, 0x90
.LBB7_17:                               # %.critedge.outer
                                        #   in Loop: Header=BB7_6 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.LBB7_6
# BB#18:
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB7_42
.LBB7_19:                               # %.preheader400
	cmpq	%r12, %rbx
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	je	.LBB7_42
# BB#20:                                # %.lr.ph540
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r14d, %r14d
.LBB7_21:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_25 Depth 2
                                        #       Child Loop BB7_26 Depth 3
                                        #       Child Loop BB7_31 Depth 3
                                        #       Child Loop BB7_33 Depth 3
	movq	8(%rsp), %r13           # 8-byte Reload
	cmpq	$32, %r13
	jl	.LBB7_23
# BB#22:                                #   in Loop: Header=BB7_21 Depth=1
	movl	$31, (%rsp)
	movl	$7, %edi
	movl	$1, %esi
	movl	$.L.str.11, %edx
	movl	$1, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	movq	%r8, %r12
	callq	Error
	movq	%r12, %r8
.LBB7_23:                               #   in Loop: Header=BB7_21 Depth=1
	movq	%rbp, 304(%rsp,%r13,8)
	movq	%r14, 560(%rsp,%r13,8)
	incq	%r13
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	8(%rbx), %rbx
	movq	24(%rsp), %r12          # 8-byte Reload
	cmpq	%r12, %rbx
	je	.LBB7_41
# BB#24:                                # %.preheader398.lr.ph.preheader
                                        #   in Loop: Header=BB7_21 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_25:                               # %.preheader398
                                        #   Parent Loop BB7_21 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_26 Depth 3
                                        #       Child Loop BB7_31 Depth 3
                                        #       Child Loop BB7_33 Depth 3
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB7_26:                               #   Parent Loop BB7_21 Depth=1
                                        #     Parent Loop BB7_25 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB7_26
# BB#27:                                #   in Loop: Header=BB7_25 Depth=2
	cmpb	$9, %al
	je	.LBB7_30
# BB#28:                                #   in Loop: Header=BB7_25 Depth=2
	cmpb	$1, %al
	jne	.LBB7_36
# BB#29:                                # %.critedge1.outer
                                        #   in Loop: Header=BB7_25 Depth=2
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	movq	%rbp, %r14
	jne	.LBB7_25
	jmp	.LBB7_41
	.p2align	4, 0x90
.LBB7_30:                               #   in Loop: Header=BB7_25 Depth=2
	movq	8(%rbp), %rcx
	.p2align	4, 0x90
.LBB7_31:                               #   Parent Loop BB7_21 Depth=1
                                        #     Parent Loop BB7_25 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %eax
	testb	%al, %al
	je	.LBB7_31
# BB#32:                                #   in Loop: Header=BB7_25 Depth=2
	movq	(%rbp), %rdx
	.p2align	4, 0x90
.LBB7_33:                               #   Parent Loop BB7_21 Depth=1
                                        #     Parent Loop BB7_25 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdx), %rdx
	movzbl	32(%rdx), %ecx
	testb	%cl, %cl
	je	.LBB7_33
# BB#34:                                # %SplitIsDefinite.exit355
                                        #   in Loop: Header=BB7_25 Depth=2
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB7_37
# BB#35:                                # %SplitIsDefinite.exit355
                                        #   in Loop: Header=BB7_25 Depth=2
	addb	$-9, %cl
	cmpb	$91, %cl
	jae	.LBB7_37
	jmp	.LBB7_38
	.p2align	4, 0x90
.LBB7_36:                               #   in Loop: Header=BB7_25 Depth=2
	addb	$-9, %al
	cmpb	$91, %al
	jb	.LBB7_38
.LBB7_37:                               # %.critedge1.backedge
                                        #   in Loop: Header=BB7_25 Depth=2
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.LBB7_25
	jmp	.LBB7_41
.LBB7_38:                               #   in Loop: Header=BB7_21 Depth=1
	testq	%r14, %r14
	jne	.LBB7_40
# BB#39:                                #   in Loop: Header=BB7_21 Depth=1
	movq	%r8, %r14
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%r14, %r8
	xorl	%r14d, %r14d
.LBB7_40:                               # %.backedge401
                                        #   in Loop: Header=BB7_21 Depth=1
	cmpq	%r12, %rbx
	jne	.LBB7_21
.LBB7_41:                               # %._crit_edge541.loopexit
	movl	$1, %r14d
.LBB7_42:                               # %._crit_edge541
	movq	$0, 48(%rsp)
	movq	8(%r15), %rbx
	cmpq	%r15, %rbx
	je	.LBB7_58
# BB#43:                                # %.preheader397.lr.ph.preheader
	leaq	32(%r15), %r8
	xorl	%r13d, %r13d
	movq	8(%rsp), %r12           # 8-byte Reload
	.p2align	4, 0x90
.LBB7_44:                               # %.preheader397
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_45 Depth 2
                                        #     Child Loop BB7_50 Depth 2
                                        #     Child Loop BB7_52 Depth 2
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB7_45:                               #   Parent Loop BB7_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB7_45
# BB#46:                                #   in Loop: Header=BB7_44 Depth=1
	cmpb	$9, %al
	je	.LBB7_49
# BB#47:                                #   in Loop: Header=BB7_44 Depth=1
	cmpb	$1, %al
	jne	.LBB7_55
# BB#48:                                # %.critedge2.outer
                                        #   in Loop: Header=BB7_44 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB7_44
	jmp	.LBB7_81
	.p2align	4, 0x90
.LBB7_49:                               #   in Loop: Header=BB7_44 Depth=1
	movq	8(%rbp), %rcx
	.p2align	4, 0x90
.LBB7_50:                               #   Parent Loop BB7_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %eax
	testb	%al, %al
	je	.LBB7_50
# BB#51:                                #   in Loop: Header=BB7_44 Depth=1
	movq	(%rbp), %rdx
	.p2align	4, 0x90
.LBB7_52:                               #   Parent Loop BB7_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdx), %rdx
	movzbl	32(%rdx), %ecx
	testb	%cl, %cl
	je	.LBB7_52
# BB#53:                                # %SplitIsDefinite.exit364
                                        #   in Loop: Header=BB7_44 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB7_56
# BB#54:                                # %SplitIsDefinite.exit364
                                        #   in Loop: Header=BB7_44 Depth=1
	addb	$-9, %cl
	cmpb	$91, %cl
	jae	.LBB7_56
	jmp	.LBB7_59
	.p2align	4, 0x90
.LBB7_55:                               #   in Loop: Header=BB7_44 Depth=1
	addb	$-9, %al
	cmpb	$91, %al
	jb	.LBB7_59
.LBB7_56:                               # %.critedge2.backedge
                                        #   in Loop: Header=BB7_44 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB7_44
# BB#57:
	movl	$1, %r14d
	jmp	.LBB7_84
.LBB7_58:
	xorl	%r13d, %r13d
	jmp	.LBB7_83
.LBB7_59:                               # %.preheader395
	cmpq	%r15, %rbx
	je	.LBB7_188
# BB#60:                                # %.lr.ph498
	movl	$1, %r14d
	xorl	%r12d, %r12d
.LBB7_61:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_65 Depth 2
                                        #       Child Loop BB7_66 Depth 3
                                        #       Child Loop BB7_71 Depth 3
                                        #       Child Loop BB7_73 Depth 3
	cmpq	$32, %r14
	jl	.LBB7_63
# BB#62:                                #   in Loop: Header=BB7_61 Depth=1
	movl	$31, (%rsp)
	movl	$7, %edi
	movl	$1, %esi
	movl	$.L.str.11, %edx
	movl	$1, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	movq	%r8, %r13
	callq	Error
	movq	%r13, %r8
.LBB7_63:                               #   in Loop: Header=BB7_61 Depth=1
	movq	%rbp, 48(%rsp,%r14,8)
	movq	%r12, 816(%rsp,%r14,8)
	incq	%r14
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	je	.LBB7_189
# BB#64:                                # %.preheader394.lr.ph.preheader
                                        #   in Loop: Header=BB7_61 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB7_65:                               # %.preheader394
                                        #   Parent Loop BB7_61 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_66 Depth 3
                                        #       Child Loop BB7_71 Depth 3
                                        #       Child Loop BB7_73 Depth 3
	movq	%rbx, %r13
	.p2align	4, 0x90
.LBB7_66:                               #   Parent Loop BB7_61 Depth=1
                                        #     Parent Loop BB7_65 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r13), %r13
	movzbl	32(%r13), %eax
	testb	%al, %al
	je	.LBB7_66
# BB#67:                                #   in Loop: Header=BB7_65 Depth=2
	cmpb	$9, %al
	je	.LBB7_70
# BB#68:                                #   in Loop: Header=BB7_65 Depth=2
	cmpb	$1, %al
	jne	.LBB7_76
# BB#69:                                # %.critedge3.outer
                                        #   in Loop: Header=BB7_65 Depth=2
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	movq	%r13, %r12
	jne	.LBB7_65
	jmp	.LBB7_83
	.p2align	4, 0x90
.LBB7_70:                               #   in Loop: Header=BB7_65 Depth=2
	movq	8(%r13), %rcx
	.p2align	4, 0x90
.LBB7_71:                               #   Parent Loop BB7_61 Depth=1
                                        #     Parent Loop BB7_65 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %eax
	testb	%al, %al
	je	.LBB7_71
# BB#72:                                #   in Loop: Header=BB7_65 Depth=2
	movq	(%r13), %rdx
	.p2align	4, 0x90
.LBB7_73:                               #   Parent Loop BB7_61 Depth=1
                                        #     Parent Loop BB7_65 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdx), %rdx
	movzbl	32(%rdx), %ecx
	testb	%cl, %cl
	je	.LBB7_73
# BB#74:                                # %SplitIsDefinite.exit373
                                        #   in Loop: Header=BB7_65 Depth=2
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB7_77
# BB#75:                                # %SplitIsDefinite.exit373
                                        #   in Loop: Header=BB7_65 Depth=2
	addb	$-9, %cl
	cmpb	$91, %cl
	jae	.LBB7_77
	jmp	.LBB7_78
	.p2align	4, 0x90
.LBB7_76:                               #   in Loop: Header=BB7_65 Depth=2
	addb	$-9, %al
	cmpb	$91, %al
	jb	.LBB7_78
.LBB7_77:                               # %.critedge3.backedge
                                        #   in Loop: Header=BB7_65 Depth=2
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB7_65
	jmp	.LBB7_82
.LBB7_78:                               #   in Loop: Header=BB7_61 Depth=1
	testq	%r12, %r12
	jne	.LBB7_80
# BB#79:                                #   in Loop: Header=BB7_61 Depth=1
	movq	%r8, %rbp
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%rbp, %r8
	xorl	%r12d, %r12d
.LBB7_80:                               # %.backedge396
                                        #   in Loop: Header=BB7_61 Depth=1
	cmpq	%r15, %rbx
	movq	%r13, %rbp
	movq	%r12, %r13
	jne	.LBB7_61
	jmp	.LBB7_83
.LBB7_81:
	movl	$1, %r14d
	jmp	.LBB7_84
.LBB7_82:
	movq	%r12, %r13
.LBB7_83:                               # %._crit_edge499.loopexit
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB7_84:                               # %._crit_edge499
	movb	$0, 2096(%rsp)
	movb	$0, 1072(%rsp)
	cmpl	$2, %r12d
	jl	.LBB7_92
# BB#85:                                # %.lr.ph477.preheader
	movl	%r12d, %eax
	leal	3(%r12), %esi
	leaq	-2(%rax), %rdx
	andq	$3, %rsi
	je	.LBB7_89
# BB#86:                                # %.lr.ph477.prol.preheader
	leaq	1104(%rsp), %rdi
	leaq	2128(%rsp), %rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_87:                               # %.lr.ph477.prol
                                        # =>This Inner Loop Header: Depth=1
	movb	$0, (%rbp)
	movb	$1, (%rdi)
	incq	%rcx
	addq	$32, %rdi
	addq	$32, %rbp
	cmpq	%rcx, %rsi
	jne	.LBB7_87
# BB#88:                                # %.lr.ph477.prol.loopexit.unr-lcssa
	incq	%rcx
	cmpq	$3, %rdx
	jae	.LBB7_90
	jmp	.LBB7_92
.LBB7_89:
	movl	$1, %ecx
	cmpq	$3, %rdx
	jb	.LBB7_92
.LBB7_90:                               # %.lr.ph477.preheader.new
	subq	%rcx, %rax
	shlq	$5, %rcx
	leaq	1168(%rsp,%rcx), %rdx
	leaq	2192(%rsp,%rcx), %rcx
	.p2align	4, 0x90
.LBB7_91:                               # %.lr.ph477
                                        # =>This Inner Loop Header: Depth=1
	movb	$0, -96(%rcx)
	movb	$1, -96(%rdx)
	movb	$0, -64(%rcx)
	movb	$1, -64(%rdx)
	movb	$0, -32(%rcx)
	movb	$1, -32(%rdx)
	movb	$0, (%rcx)
	movb	$1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rcx
	addq	$-4, %rax
	jne	.LBB7_91
.LBB7_92:                               # %.preheader393
	cmpl	$2, %r14d
	jl	.LBB7_94
# BB#93:                                # %.lr.ph474.preheader
	leaq	2097(%rsp), %rdi
	leal	-2(%r14), %ebx
	incq	%rbx
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	leaq	1073(%rsp), %rdi
	movl	$2, %esi
	movq	%rbx, %rdx
	callq	memset
.LBB7_94:                               # %.preheader392
	movq	%r14, 16(%rsp)          # 8-byte Spill
	cmpl	$2, %r12d
	jl	.LBB7_114
# BB#95:                                # %.preheader392
	cmpl	$2, 16(%rsp)            # 4-byte Folded Reload
	jl	.LBB7_114
# BB#96:                                # %.preheader.us.preheader
	movl	8(%rsp), %eax           # 4-byte Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	16(%rsp), %r12d         # 4-byte Reload
	leaq	2129(%rsp), %rbp
	leaq	1105(%rsp), %r14
	decq	%r12
	movl	$1, %eax
	.p2align	4, 0x90
.LBB7_97:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_107 Depth 2
                                        #     Child Loop BB7_100 Depth 2
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	304(%rsp,%rax,8), %rbx
	movb	32(%rbx), %r8b
	movl	%r8d, %eax
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB7_106
# BB#98:                                # %.lr.ph456.split.us.us.preheader
                                        #   in Loop: Header=BB7_97 Depth=1
	addq	$64, %rbx
	xorl	%r15d, %r15d
	jmp	.LBB7_100
.LBB7_99:                               #   in Loop: Header=BB7_100 Depth=2
	movzbl	-33(%rbp,%r15), %eax
	incb	%al
	movb	$3, %cl
	jmp	.LBB7_105
	.p2align	4, 0x90
.LBB7_100:                              # %.lr.ph456.split.us.us
                                        #   Parent Loop BB7_97 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%rsp,%r15,8), %rsi
	movzbl	32(%rsi), %eax
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB7_102
# BB#101:                               #   in Loop: Header=BB7_100 Depth=2
	addq	$64, %rsi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB7_99
.LBB7_102:                              # %.thread.us.us
                                        #   in Loop: Header=BB7_100 Depth=2
	movzbl	-32(%rbp,%r15), %eax
	movzbl	-1(%rbp,%r15), %ecx
	cmpb	%cl, %al
	jge	.LBB7_104
# BB#103:                               # %.thread.us.us
                                        #   in Loop: Header=BB7_100 Depth=2
	movl	%ecx, %eax
.LBB7_104:                              # %.thread.us.us
                                        #   in Loop: Header=BB7_100 Depth=2
	setle	%cl
	incb	%cl
.LBB7_105:                              #   in Loop: Header=BB7_100 Depth=2
	movb	%al, (%rbp,%r15)
	movb	%cl, (%r14,%r15)
	incq	%r15
	cmpq	%r15, %r12
	jne	.LBB7_100
	jmp	.LBB7_113
	.p2align	4, 0x90
.LBB7_106:                              # %.lr.ph456..lr.ph456.split_crit_edge.us.preheader
                                        #   in Loop: Header=BB7_97 Depth=1
	movq	%r12, %rcx
	movq	%r14, %rdx
	leaq	56(%rsp), %rsi
	movq	%rbp, %rdi
	.p2align	4, 0x90
.LBB7_107:                              # %.lr.ph456..lr.ph456.split_crit_edge.us
                                        #   Parent Loop BB7_97 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rax
	cmpb	32(%rax), %r8b
	jne	.LBB7_109
# BB#108:                               #   in Loop: Header=BB7_107 Depth=2
	movzbl	-33(%rdi), %ebx
	incb	%bl
	movb	$3, %al
	jmp	.LBB7_112
	.p2align	4, 0x90
.LBB7_109:                              #   in Loop: Header=BB7_107 Depth=2
	movzbl	-32(%rdi), %ebx
	movzbl	-1(%rdi), %eax
	cmpb	%al, %bl
	jge	.LBB7_111
# BB#110:                               #   in Loop: Header=BB7_107 Depth=2
	movl	%eax, %ebx
.LBB7_111:                              #   in Loop: Header=BB7_107 Depth=2
	setle	%al
	incb	%al
.LBB7_112:                              #   in Loop: Header=BB7_107 Depth=2
	movb	%bl, (%rdi)
	movb	%al, (%rdx)
	incq	%rdi
	addq	$8, %rsi
	incq	%rdx
	decq	%rcx
	jne	.LBB7_107
.LBB7_113:                              # %._crit_edge457.us
                                        #   in Loop: Header=BB7_97 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	incq	%rax
	addq	$32, %rbp
	addq	$32, %r14
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB7_97
.LBB7_114:                              # %._crit_edge459
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB7_116
# BB#115:
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB7_117
.LBB7_116:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB7_117:
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movb	$17, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	movq	24(%rsp), %rdi          # 8-byte Reload
	movzwl	64(%rdi), %ecx
	andl	$128, %ecx
	movzwl	64(%r15), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 64(%r15)
	movzwl	64(%rdi), %ecx
	andl	$256, %ecx              # imm = 0x100
	movl	%eax, %edx
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 64(%r15)
	movzwl	64(%rdi), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 64(%r15)
	movzwl	64(%rdi), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%ecx, %edx
	movw	%dx, 64(%r15)
	movzwl	64(%rdi), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%ecx, %edx
	movw	%dx, 64(%r15)
	movzwl	66(%rdi), %ecx
	movw	%cx, 66(%r15)
	movb	68(%rdi), %cl
	andb	$3, %cl
	movb	68(%r15), %dl
	andb	$-4, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r15)
	movb	68(%rdi), %cl
	andb	$12, %cl
	andb	$-13, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r15)
	movb	68(%rdi), %cl
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r15)
	movb	64(%rdi), %cl
	andb	$8, %cl
	andb	$-9, %al
	orb	%cl, %al
	movb	%al, 64(%r15)
	movzwl	68(%rdi), %edx
	andl	$128, %edx
	movzwl	68(%r15), %ecx
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%edx, %ecx
	movw	%cx, 68(%r15)
	movzwl	68(%rdi), %edx
	andl	$256, %edx              # imm = 0x100
	andl	$-257, %ecx             # imm = 0xFEFF
	orl	%edx, %ecx
	movw	%cx, 68(%r15)
	movzwl	68(%rdi), %edx
	andl	$512, %edx              # imm = 0x200
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	%edx, %ecx
	movw	%cx, 68(%r15)
	movzwl	68(%rdi), %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$-7169, %ecx            # imm = 0xE3FF
	orl	%edx, %ecx
	movw	%cx, 68(%r15)
	movzwl	68(%rdi), %edx
	andl	$57344, %edx            # imm = 0xE000
	andl	$8191, %ecx             # imm = 0x1FFF
	orl	%edx, %ecx
	movw	%cx, 68(%r15)
	movzwl	70(%rdi), %ecx
	movw	%cx, 70(%r15)
	movl	$4095, %edx             # imm = 0xFFF
	andl	76(%rdi), %edx
	movl	$-4096, %ecx            # imm = 0xF000
	andl	76(%r15), %ecx
	orl	%edx, %ecx
	movl	%ecx, 76(%r15)
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	76(%rdi), %edx
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r15)
	movl	$12582912, %edx         # imm = 0xC00000
	andl	76(%rdi), %edx
	andl	$-12582913, %ecx        # imm = 0xFF3FFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r15)
	movl	$1056964608, %edx       # imm = 0x3F000000
	andl	76(%rdi), %edx
	andl	$-1056964609, %ecx      # imm = 0xC0FFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r15)
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	76(%rdi), %edx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r15)
	movl	$1073741824, %edx       # imm = 0x40000000
	andl	76(%rdi), %edx
	andl	$-1073741825, %ecx      # imm = 0xBFFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r15)
	movb	64(%rdi), %cl
	andb	$1, %cl
	andb	$-2, %al
	orb	%cl, %al
	movb	%al, 64(%r15)
	movb	64(%rdi), %cl
	andb	$2, %cl
	andb	$-3, %al
	orb	%cl, %al
	movb	%al, 64(%r15)
	movb	64(%rdi), %cl
	andb	$4, %cl
	andb	$-5, %al
	orb	%cl, %al
	movb	%al, 64(%r15)
	movb	64(%rdi), %cl
	andb	$112, %cl
	andb	$-113, %al
	orb	%cl, %al
	movb	%al, 64(%r15)
	movzwl	72(%rdi), %eax
	movw	%ax, 72(%r15)
	movzwl	74(%rdi), %eax
	movw	%ax, 74(%r15)
	movslq	%esi, %rbx
	decq	%rbx
	movslq	%r14d, %rbp
	movq	%rbx, %rax
	shlq	$5, %rax
	leaq	1072(%rsp,%rax), %rax
	movb	-1(%rbp,%rax), %al
	testb	%al, %al
	je	.LBB7_187
# BB#118:                               # %.lr.ph
	movq	8(%rsp), %rcx           # 8-byte Reload
	decl	%ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	decl	%r14d
	decq	%rbp
	.p2align	4, 0x90
.LBB7_119:                              # =>This Inner Loop Header: Depth=1
	movsbl	%al, %eax
	cmpl	$1, %eax
	je	.LBB7_124
# BB#120:                               #   in Loop: Header=BB7_119 Depth=1
	cmpl	$2, %eax
	je	.LBB7_126
# BB#121:                               #   in Loop: Header=BB7_119 Depth=1
	cmpl	$3, %eax
	jne	.LBB7_128
# BB#122:                               #   in Loop: Header=BB7_119 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB7_129
# BB#123:                               #   in Loop: Header=BB7_119 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB7_130
	.p2align	4, 0x90
.LBB7_124:                              #   in Loop: Header=BB7_119 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB7_137
# BB#125:                               #   in Loop: Header=BB7_119 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB7_138
	.p2align	4, 0x90
.LBB7_126:                              #   in Loop: Header=BB7_119 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB7_145
# BB#127:                               #   in Loop: Header=BB7_119 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB7_146
	.p2align	4, 0x90
.LBB7_128:                              #   in Loop: Header=BB7_119 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB7_154
.LBB7_129:                              #   in Loop: Header=BB7_119 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB7_130:                              #   in Loop: Header=BB7_119 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r15), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB7_133
# BB#131:                               #   in Loop: Header=BB7_119 Depth=1
	testq	%rcx, %rcx
	je	.LBB7_133
# BB#132:                               #   in Loop: Header=BB7_119 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB7_133:                              #   in Loop: Header=BB7_119 Depth=1
	movq	%rax, zz_res(%rip)
	movq	304(%rsp,%rbx,8), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB7_136
# BB#134:                               #   in Loop: Header=BB7_119 Depth=1
	testq	%rcx, %rcx
	je	.LBB7_136
# BB#135:                               #   in Loop: Header=BB7_119 Depth=1
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB7_136:                              #   in Loop: Header=BB7_119 Depth=1
	movq	560(%rsp,%rbx,8), %r13
	decl	%edi
	jmp	.LBB7_153
.LBB7_137:                              #   in Loop: Header=BB7_119 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB7_138:                              #   in Loop: Header=BB7_119 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r15), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB7_141
# BB#139:                               #   in Loop: Header=BB7_119 Depth=1
	testq	%rcx, %rcx
	je	.LBB7_141
# BB#140:                               #   in Loop: Header=BB7_119 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB7_141:                              #   in Loop: Header=BB7_119 Depth=1
	movq	%rax, zz_res(%rip)
	movq	304(%rsp,%rbx,8), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB7_144
# BB#142:                               #   in Loop: Header=BB7_119 Depth=1
	testq	%rcx, %rcx
	je	.LBB7_144
# BB#143:                               #   in Loop: Header=BB7_119 Depth=1
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB7_144:                              #   in Loop: Header=BB7_119 Depth=1
	movq	560(%rsp,%rbx,8), %r13
	decl	%edi
	jmp	.LBB7_154
.LBB7_145:                              #   in Loop: Header=BB7_119 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB7_146:                              #   in Loop: Header=BB7_119 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r15), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB7_149
# BB#147:                               #   in Loop: Header=BB7_119 Depth=1
	testq	%rcx, %rcx
	je	.LBB7_149
# BB#148:                               #   in Loop: Header=BB7_119 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB7_149:                              #   in Loop: Header=BB7_119 Depth=1
	movq	%rax, zz_res(%rip)
	movq	48(%rsp,%rbp,8), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB7_152
# BB#150:                               #   in Loop: Header=BB7_119 Depth=1
	testq	%rcx, %rcx
	je	.LBB7_152
# BB#151:                               #   in Loop: Header=BB7_119 Depth=1
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB7_152:                              #   in Loop: Header=BB7_119 Depth=1
	movq	816(%rsp,%rbp,8), %r13
.LBB7_153:                              #   in Loop: Header=BB7_119 Depth=1
	decl	%r14d
.LBB7_154:                              #   in Loop: Header=BB7_119 Depth=1
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movslq	%edi, %rbx
	movslq	%r14d, %rbp
	movq	%rbx, %rax
	shlq	$5, %rax
	leaq	1072(%rsp,%rax), %r12
	cmpb	$0, (%rbp,%r12)
	je	.LBB7_187
# BB#155:                               #   in Loop: Header=BB7_119 Depth=1
	addq	%rbp, %r12
	testq	%r13, %r13
	je	.LBB7_160
# BB#156:                               #   in Loop: Header=BB7_119 Depth=1
	movq	24(%r13), %rax
	cmpq	16(%r13), %rax
	je	.LBB7_158
# BB#157:                               #   in Loop: Header=BB7_119 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_158:                              #   in Loop: Header=BB7_119 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB7_162
# BB#159:                               #   in Loop: Header=BB7_119 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB7_163
	.p2align	4, 0x90
.LBB7_160:                              #   in Loop: Header=BB7_119 Depth=1
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r13
	testq	%r13, %r13
	movq	%r14, 16(%rsp)          # 8-byte Spill
	je	.LBB7_167
# BB#161:                               #   in Loop: Header=BB7_119 Depth=1
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB7_168
	.p2align	4, 0x90
.LBB7_162:                              #   in Loop: Header=BB7_119 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB7_163:                              #   in Loop: Header=BB7_119 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r15), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB7_166
# BB#164:                               #   in Loop: Header=BB7_119 Depth=1
	testq	%rcx, %rcx
	je	.LBB7_166
# BB#165:                               #   in Loop: Header=BB7_119 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB7_166:                              #   in Loop: Header=BB7_119 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%rax, %rax
	jne	.LBB7_185
	jmp	.LBB7_186
.LBB7_167:                              #   in Loop: Header=BB7_119 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r13
	movq	%r13, zz_hold(%rip)
.LBB7_168:                              #   in Loop: Header=BB7_119 Depth=1
	leaq	32(%r13), %rdx
	movb	$1, 32(%r13)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%r13, 8(%r13)
	movq	%r13, (%r13)
	movw	$1, 41(%r13)
	movq	no_fpos(%rip), %rax
	movzwl	2(%rax), %ecx
	movw	%cx, 34(%r13)
	movl	4(%rax), %ecx
	movl	$1048575, %esi          # imm = 0xFFFFF
	andl	%esi, %ecx
	movl	36(%r13), %esi
	movl	$-1048576, %edi         # imm = 0xFFF00000
	andl	%edi, %esi
	orl	%ecx, %esi
	movl	%esi, 36(%r13)
	movl	4(%rax), %eax
	andl	%edi, %eax
	orl	%ecx, %eax
	movl	%eax, 36(%r13)
	movzwl	44(%r13), %eax
	andl	$127, %eax
	orl	$9728, %eax             # imm = 0x2600
	movw	%ax, 44(%r13)
	movzwl	70(%r15), %eax
	movw	%ax, 46(%r13)
	movl	$11, %edi
	movl	$.L.str.14, %esi
	callq	MakeWord
	movq	%rax, %r14
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB7_170
# BB#169:                               #   in Loop: Header=BB7_119 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB7_171
.LBB7_170:                              #   in Loop: Header=BB7_119 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB7_171:                              #   in Loop: Header=BB7_119 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB7_174
# BB#172:                               #   in Loop: Header=BB7_119 Depth=1
	testq	%rax, %rax
	je	.LBB7_174
# BB#173:                               #   in Loop: Header=BB7_119 Depth=1
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB7_174:                              #   in Loop: Header=BB7_119 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB7_177
# BB#175:                               #   in Loop: Header=BB7_119 Depth=1
	testq	%rax, %rax
	je	.LBB7_177
# BB#176:                               #   in Loop: Header=BB7_119 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB7_177:                              #   in Loop: Header=BB7_119 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB7_179
# BB#178:                               #   in Loop: Header=BB7_119 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB7_180
.LBB7_179:                              #   in Loop: Header=BB7_119 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB7_180:                              #   in Loop: Header=BB7_119 Depth=1
	movq	16(%rsp), %r14          # 8-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r15), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB7_183
# BB#181:                               #   in Loop: Header=BB7_119 Depth=1
	testq	%rcx, %rcx
	je	.LBB7_183
# BB#182:                               #   in Loop: Header=BB7_119 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB7_183:                              #   in Loop: Header=BB7_119 Depth=1
	testq	%r13, %r13
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	je	.LBB7_186
# BB#184:                               #   in Loop: Header=BB7_119 Depth=1
	testq	%rax, %rax
	je	.LBB7_186
.LBB7_185:                              #   in Loop: Header=BB7_119 Depth=1
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB7_186:                              # %.backedge
                                        #   in Loop: Header=BB7_119 Depth=1
	movzbl	(%r12), %eax
	testb	%al, %al
	jne	.LBB7_119
.LBB7_187:                              # %._crit_edge
	movq	%r15, %rax
	addq	$3128, %rsp             # imm = 0xC38
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_188:
	xorl	%r13d, %r13d
	movl	$1, %r14d
	jmp	.LBB7_84
.LBB7_189:
	xorl	%r13d, %r13d
	jmp	.LBB7_83
.Lfunc_end7:
	.size	Meld, .Lfunc_end7-Meld
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SplitIsDefinite: x not a SPLIT!"
	.size	.L.str.1, 32

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"DisposeObject: x has a parent!"
	.size	.L.str.2, 31

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"word is too long"
	.size	.L.str.3, 17

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"CopyObject: PAR child!"
	.size	.L.str.4, 23

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"CopyObject: CLOSURE!"
	.size	.L.str.5, 21

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"assert failed in %s %s"
	.size	.L.str.6, 23

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"CopyObject:"
	.size	.L.str.7, 12

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"InsertObject:"
	.size	.L.str.8, 14

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Meld: type(x) != ACAT"
	.size	.L.str.9, 22

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Meld: type(y) != ACAT"
	.size	.L.str.10, 22

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%s: maximum paragraph length (%d) exceeded"
	.size	.L.str.11, 43

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"@Meld"
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"NextDefiniteWithGap: g == nilobj!"
	.size	.L.str.13, 34

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"1s"
	.size	.L.str.14, 3

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Meld: g!"
	.size	.L.str.15, 9

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"DisposeSplitObject: x has no children!"
	.size	.L.str.17, 39

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"DisposeSplitObject: x has one child!"
	.size	.L.str.18, 37

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"DisposeSplitObject: children!"
	.size	.L.str.19, 30

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"DisposeSplitObject: link (a)!"
	.size	.L.str.20, 30

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"DisposeSplitObject: link (b)!"
	.size	.L.str.21, 30


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
