	.text
	.file	"TarHandlerOut.bc"
	.globl	_ZN8NArchive4NTar8CHandler15GetFileTimeTypeEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler15GetFileTimeTypeEPj,@function
_ZN8NArchive4NTar8CHandler15GetFileTimeTypeEPj: # @_ZN8NArchive4NTar8CHandler15GetFileTimeTypeEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	_ZN8NArchive4NTar8CHandler15GetFileTimeTypeEPj, .Lfunc_end0-_ZN8NArchive4NTar8CHandler15GetFileTimeTypeEPj
	.cfi_endproc

	.globl	_ZThn24_N8NArchive4NTar8CHandler15GetFileTimeTypeEPj
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive4NTar8CHandler15GetFileTimeTypeEPj,@function
_ZThn24_N8NArchive4NTar8CHandler15GetFileTimeTypeEPj: # @_ZThn24_N8NArchive4NTar8CHandler15GetFileTimeTypeEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	_ZThn24_N8NArchive4NTar8CHandler15GetFileTimeTypeEPj, .Lfunc_end1-_ZThn24_N8NArchive4NTar8CHandler15GetFileTimeTypeEPj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.zero	16
	.text
	.globl	_ZN8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback,@function
_ZN8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback: # @_ZN8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 304
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %rax
	movl	%edx, %ebx
	movq	%rdi, %r14
	movq	72(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	movl	$-2147467263, %r12d     # imm = 0x80004001
	cmpl	$0, 256(%r14)
	jne	.LBB2_167
.LBB2_2:
	movl	$-2147467263, %r12d     # imm = 0x80004001
	cmpq	$0, 80(%r14)
	jne	.LBB2_167
# BB#3:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 176(%rsp)
	movq	$8, 192(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NTar11CUpdateItemEE+16, 168(%rsp)
	testl	%ebx, %ebx
	je	.LBB2_120
# BB#4:                                 # %.lr.ph
	movq	%rsi, 240(%rsp)         # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
                                        # implicit-def: %R15D
	movl	%ebx, 156(%rsp)         # 4-byte Spill
	jmp	.LBB2_99
	.p2align	4, 0x90
.LBB2_5:                                #   in Loop: Header=BB2_99 Depth=1
	incl	%r13d
	movl	156(%rsp), %ebx         # 4-byte Reload
	cmpl	%ebx, %r13d
	movl	%r12d, %r15d
	xorps	%xmm0, %xmm0
	jb	.LBB2_99
	jmp	.LBB2_119
.LBB2_6:                                #   in Loop: Header=BB2_99 Depth=1
	cmpw	$0, 40(%rsp)
	setne	130(%rsp)
.LBB2_7:                                #   in Loop: Header=BB2_99 Depth=1
	xorl	%ebp, %ebp
	movl	%r15d, %r12d
.LBB2_8:                                #   in Loop: Header=BB2_99 Depth=1
.Ltmp16:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp17:
# BB#9:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit154
                                        #   in Loop: Header=BB2_99 Depth=1
	movl	$1, %ebx
	testl	%ebp, %ebp
	jne	.LBB2_112
# BB#10:                                #   in Loop: Header=BB2_99 Depth=1
	movl	$0, 136(%rsp)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp19:
	movl	$53, %edx
	movl	%r13d, %esi
	leaq	136(%rsp), %rbx
	movq	%rbx, %rcx
	callq	*64(%rax)
	movl	%eax, %r15d
.Ltmp20:
# BB#11:                                #   in Loop: Header=BB2_99 Depth=1
	movl	$1, %ebp
	testl	%r15d, %r15d
	jne	.LBB2_17
# BB#12:                                #   in Loop: Header=BB2_99 Depth=1
	movzwl	136(%rsp), %eax
	cmpl	$19, %eax
	je	.LBB2_15
# BB#13:                                #   in Loop: Header=BB2_99 Depth=1
	movl	$-2147024809, %r15d     # imm = 0x80070057
	testw	%ax, %ax
	jne	.LBB2_17
# BB#14:                                #   in Loop: Header=BB2_99 Depth=1
	cmpb	$0, 130(%rsp)
	movl	$33279, %eax            # imm = 0x81FF
	movl	$16895, %ecx            # imm = 0x41FF
	cmovnel	%ecx, %eax
	jmp	.LBB2_16
.LBB2_15:                               #   in Loop: Header=BB2_99 Depth=1
	movl	144(%rsp), %eax
.LBB2_16:                               #   in Loop: Header=BB2_99 Depth=1
	movl	%eax, 68(%rsp)
	xorl	%ebp, %ebp
	movl	%r12d, %r15d
.LBB2_17:                               #   in Loop: Header=BB2_99 Depth=1
.Ltmp24:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp25:
# BB#18:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit158
                                        #   in Loop: Header=BB2_99 Depth=1
	movl	$1, %ebx
	testl	%ebp, %ebp
	je	.LBB2_20
# BB#19:                                #   in Loop: Header=BB2_99 Depth=1
	movl	%r15d, %r12d
	jmp	.LBB2_112
.LBB2_20:                               #   in Loop: Header=BB2_99 Depth=1
	movl	$0, 16(%rsp)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp27:
	movl	$12, %edx
	movl	%r13d, %esi
	leaq	16(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r12d
.Ltmp28:
# BB#21:                                #   in Loop: Header=BB2_99 Depth=1
	testl	%r12d, %r12d
	cmovnel	%r12d, %r15d
	movl	$1, %ebp
	jne	.LBB2_28
# BB#22:                                #   in Loop: Header=BB2_99 Depth=1
	movzwl	16(%rsp), %eax
	testw	%ax, %ax
	je	.LBB2_26
# BB#23:                                #   in Loop: Header=BB2_99 Depth=1
	movl	$-2147024809, %r12d     # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$64, %eax
	jne	.LBB2_28
# BB#24:                                #   in Loop: Header=BB2_99 Depth=1
.Ltmp29:
	leaq	24(%rsp), %rdi
	leaq	64(%rsp), %rsi
	callq	_ZN8NWindows5NTime18FileTimeToUnixTimeERK9_FILETIMERj
.Ltmp30:
# BB#25:                                #   in Loop: Header=BB2_99 Depth=1
	xorl	%ebp, %ebp
	testb	%al, %al
	jne	.LBB2_27
.LBB2_26:                               # %.sink.split
                                        #   in Loop: Header=BB2_99 Depth=1
	movl	$0, 64(%rsp)
	xorl	%ebp, %ebp
.LBB2_27:                               #   in Loop: Header=BB2_99 Depth=1
	movl	%r15d, %r12d
.LBB2_28:                               #   in Loop: Header=BB2_99 Depth=1
.Ltmp34:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp35:
# BB#29:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit166
                                        #   in Loop: Header=BB2_99 Depth=1
	movl	$1, %ebx
	testl	%ebp, %ebp
	jne	.LBB2_112
# BB#30:                                #   in Loop: Header=BB2_99 Depth=1
	movl	$0, 216(%rsp)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp37:
	movl	$3, %edx
	movl	%r13d, %esi
	leaq	216(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r15d
.Ltmp38:
# BB#31:                                #   in Loop: Header=BB2_99 Depth=1
	testl	%r15d, %r15d
	cmovnel	%r15d, %r12d
	movl	$1, %ebp
	je	.LBB2_35
.LBB2_32:                               #   in Loop: Header=BB2_99 Depth=1
.Ltmp56:
	leaq	216(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp57:
# BB#33:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit179
                                        #   in Loop: Header=BB2_99 Depth=1
	movl	$1, %ebx
	testl	%ebp, %ebp
	je	.LBB2_46
# BB#34:                                #   in Loop: Header=BB2_99 Depth=1
	movl	%r15d, %r12d
	jmp	.LBB2_112
.LBB2_35:                               #   in Loop: Header=BB2_99 Depth=1
	movzwl	216(%rsp), %eax
	testw	%ax, %ax
	je	.LBB2_88
# BB#36:                                #   in Loop: Header=BB2_99 Depth=1
	movl	$-2147024809, %r15d     # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$8, %eax
	jne	.LBB2_32
# BB#37:                                #   in Loop: Header=BB2_99 Depth=1
	movq	224(%rsp), %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$-1, %ebp
	movq	%rbx, %rax
.LBB2_38:                               #   Parent Loop BB2_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB2_38
# BB#39:                                # %_Z11MyStringLenIwEiPKT_.exit.i
                                        #   in Loop: Header=BB2_99 Depth=1
	leal	1(%rbp), %r15d
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp39:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp40:
# BB#40:                                # %.noexc167
                                        #   in Loop: Header=BB2_99 Depth=1
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	%r15d, 28(%rsp)
.LBB2_41:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   Parent Loop BB2_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB2_41
# BB#42:                                #   in Loop: Header=BB2_99 Depth=1
	movl	%ebp, 24(%rsp)
.Ltmp42:
	leaq	136(%rsp), %rbx
	movq	%rbx, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZN8NArchive9NItemName13MakeLegalNameERK11CStringBaseIwE
.Ltmp43:
# BB#43:                                #   in Loop: Header=BB2_99 Depth=1
.Ltmp45:
	movl	$1, %edx
	leaq	32(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp46:
# BB#44:                                #   in Loop: Header=BB2_99 Depth=1
	movl	$0, 88(%rsp)
	movq	80(%rsp), %rax
	movb	$0, (%rax)
	movslq	40(%rsp), %rax
	leaq	1(%rax), %rcx
	movl	92(%rsp), %ebp
	cmpl	%ebp, %ecx
	jne	.LBB2_63
# BB#45:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
                                        #   in Loop: Header=BB2_99 Depth=1
	movq	80(%rsp), %r15
	jmp	.LBB2_80
.LBB2_46:                               #   in Loop: Header=BB2_99 Depth=1
.Ltmp59:
	movl	$25, %edx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%r13d, %esi
	leaq	96(%rsp), %rcx
	callq	_ZN8NArchive4NTarL13GetPropStringEP22IArchiveUpdateCallbackjjR11CStringBaseIcE
	movl	%eax, %r12d
.Ltmp60:
# BB#47:                                #   in Loop: Header=BB2_99 Depth=1
	testl	%r12d, %r12d
	jne	.LBB2_112
# BB#48:                                #   in Loop: Header=BB2_99 Depth=1
.Ltmp61:
	movl	$26, %edx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%r13d, %esi
	leaq	112(%rsp), %rcx
	callq	_ZN8NArchive4NTarL13GetPropStringEP22IArchiveUpdateCallbackjjR11CStringBaseIcE
	movl	%eax, %r12d
.Ltmp62:
# BB#49:                                #   in Loop: Header=BB2_99 Depth=1
	testl	%r12d, %r12d
	jne	.LBB2_112
# BB#50:                                # %._crit_edge415
                                        #   in Loop: Header=BB2_99 Depth=1
	movl	52(%rsp), %eax
.LBB2_51:                               #   in Loop: Header=BB2_99 Depth=1
	testl	%eax, %eax
	je	.LBB2_58
# BB#52:                                #   in Loop: Header=BB2_99 Depth=1
	movl	$0, 200(%rsp)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp63:
	movl	$7, %edx
	movl	%r13d, %esi
	leaq	200(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r12d
.Ltmp64:
# BB#53:                                #   in Loop: Header=BB2_99 Depth=1
	movl	$1, %ebp
	testl	%r12d, %r12d
	jne	.LBB2_56
# BB#54:                                #   in Loop: Header=BB2_99 Depth=1
	movl	$-2147024809, %r12d     # imm = 0x80070057
	movzwl	200(%rsp), %eax
	cmpl	$21, %eax
	jne	.LBB2_56
# BB#55:                                #   in Loop: Header=BB2_99 Depth=1
	movq	208(%rsp), %rax
	movq	%rax, 72(%rsp)
	xorl	%ebp, %ebp
	movl	%r15d, %r12d
.LBB2_56:                               #   in Loop: Header=BB2_99 Depth=1
.Ltmp68:
	leaq	200(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp69:
# BB#57:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit185
                                        #   in Loop: Header=BB2_99 Depth=1
	movl	$1, %ebx
	testl	%ebp, %ebp
	jne	.LBB2_112
	jmp	.LBB2_59
.LBB2_58:                               #   in Loop: Header=BB2_99 Depth=1
	movl	%r15d, %r12d
.LBB2_59:                               #   in Loop: Header=BB2_99 Depth=1
.Ltmp71:
	movl	$80, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp72:
# BB#60:                                # %.noexc186
                                        #   in Loop: Header=BB2_99 Depth=1
.Ltmp73:
	movq	%rbp, %rdi
	leaq	56(%rsp), %rsi
	callq	_ZN8NArchive4NTar11CUpdateItemC2ERKS1_
.Ltmp74:
# BB#61:                                #   in Loop: Header=BB2_99 Depth=1
.Ltmp76:
	leaq	168(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp77:
# BB#62:                                # %_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEE3AddERKS2_.exit
                                        #   in Loop: Header=BB2_99 Depth=1
	movq	184(%rsp), %rax
	movslq	180(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 180(%rsp)
	xorl	%ebx, %ebx
	jmp	.LBB2_112
.LBB2_63:                               #   in Loop: Header=BB2_99 Depth=1
	cmpl	$-1, %eax
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	movq	%rcx, %rdi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp48:
	callq	_Znam
	movq	%rax, %r15
.Ltmp49:
# BB#64:                                # %.noexc173
                                        #   in Loop: Header=BB2_99 Depth=1
	testl	%ebp, %ebp
	jle	.LBB2_79
# BB#65:                                # %.preheader.i.i
                                        #   in Loop: Header=BB2_99 Depth=1
	movslq	88(%rsp), %rax
	testq	%rax, %rax
	movq	80(%rsp), %rdi
	jle	.LBB2_77
# BB#66:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB2_99 Depth=1
	cmpl	$31, %eax
	jbe	.LBB2_70
# BB#67:                                # %min.iters.checked
                                        #   in Loop: Header=BB2_99 Depth=1
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB2_70
# BB#68:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_99 Depth=1
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r15
	jae	.LBB2_91
# BB#69:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_99 Depth=1
	leaq	(%r15,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB2_91
.LBB2_70:                               #   in Loop: Header=BB2_99 Depth=1
	xorl	%ecx, %ecx
.LBB2_71:                               # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB2_99 Depth=1
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB2_74
# BB#72:                                # %.lr.ph.i.i.prol.preheader
                                        #   in Loop: Header=BB2_99 Depth=1
	negq	%rsi
.LBB2_73:                               # %.lr.ph.i.i.prol
                                        #   Parent Loop BB2_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r15,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB2_73
.LBB2_74:                               # %.lr.ph.i.i.prol.loopexit
                                        #   in Loop: Header=BB2_99 Depth=1
	cmpq	$7, %rdx
	jb	.LBB2_78
# BB#75:                                # %.lr.ph.i.i.preheader.new
                                        #   in Loop: Header=BB2_99 Depth=1
	subq	%rcx, %rax
	leaq	7(%r15,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB2_76:                               # %.lr.ph.i.i
                                        #   Parent Loop BB2_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB2_76
	jmp	.LBB2_78
.LBB2_77:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB2_99 Depth=1
	testq	%rdi, %rdi
	je	.LBB2_79
.LBB2_78:                               # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB2_99 Depth=1
	callq	_ZdaPv
.LBB2_79:                               # %._crit_edge17.i.i
                                        #   in Loop: Header=BB2_99 Depth=1
	movq	%r15, 80(%rsp)
	movslq	88(%rsp), %rax
	movb	$0, (%r15,%rax)
	movq	232(%rsp), %rax         # 8-byte Reload
	movl	%eax, 92(%rsp)
.LBB2_80:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        #   in Loop: Header=BB2_99 Depth=1
	movq	32(%rsp), %rax
.LBB2_81:                               #   Parent Loop BB2_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%r15)
	incq	%r15
	testb	%cl, %cl
	jne	.LBB2_81
# BB#82:                                #   in Loop: Header=BB2_99 Depth=1
	movl	40(%rsp), %eax
	movl	%eax, 88(%rsp)
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_84
# BB#83:                                #   in Loop: Header=BB2_99 Depth=1
	callq	_ZdaPv
.LBB2_84:                               # %_ZN11CStringBaseIcED2Ev.exit
                                        #   in Loop: Header=BB2_99 Depth=1
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_86
# BB#85:                                #   in Loop: Header=BB2_99 Depth=1
	callq	_ZdaPv
.LBB2_86:                               # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB2_99 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_88
# BB#87:                                #   in Loop: Header=BB2_99 Depth=1
	callq	_ZdaPv
.LBB2_88:                               #   in Loop: Header=BB2_99 Depth=1
	xorl	%ebp, %ebp
	cmpb	$0, 130(%rsp)
	je	.LBB2_90
# BB#89:                                #   in Loop: Header=BB2_99 Depth=1
.Ltmp51:
	movl	$47, %esi
	leaq	80(%rsp), %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp52:
	movl	%r12d, %r15d
	jmp	.LBB2_32
.LBB2_90:                               #   in Loop: Header=BB2_99 Depth=1
	movl	%r12d, %r15d
	jmp	.LBB2_32
.LBB2_91:                               # %vector.body.preheader
                                        #   in Loop: Header=BB2_99 Depth=1
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_94
# BB#92:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB2_99 Depth=1
	negq	%rsi
	xorl	%ebp, %ebp
.LBB2_93:                               # %vector.body.prol
                                        #   Parent Loop BB2_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%r15,%rbp)
	movups	%xmm1, 16(%r15,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB2_93
	jmp	.LBB2_95
.LBB2_94:                               #   in Loop: Header=BB2_99 Depth=1
	xorl	%ebp, %ebp
.LBB2_95:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB2_99 Depth=1
	cmpq	$96, %rdx
	jb	.LBB2_98
# BB#96:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB2_99 Depth=1
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r15,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB2_97:                               # %vector.body
                                        #   Parent Loop BB2_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB2_97
.LBB2_98:                               # %middle.block
                                        #   in Loop: Header=BB2_99 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB2_71
	jmp	.LBB2_78
	.p2align	4, 0x90
.LBB2_99:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_38 Depth 2
                                        #     Child Loop BB2_41 Depth 2
                                        #     Child Loop BB2_93 Depth 2
                                        #     Child Loop BB2_97 Depth 2
                                        #     Child Loop BB2_73 Depth 2
                                        #     Child Loop BB2_76 Depth 2
                                        #     Child Loop BB2_81 Depth 2
	leaq	80(%rsp), %rax
	movups	%xmm0, (%rax)
.Ltmp0:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp1:
# BB#100:                               # %.noexc
                                        #   in Loop: Header=BB2_99 Depth=1
	movq	%rbp, 80(%rsp)
	movb	$0, (%rbp)
	movl	$4, 92(%rsp)
	leaq	96(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
.Ltmp3:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp4:
# BB#101:                               #   in Loop: Header=BB2_99 Depth=1
	movq	%rbx, 96(%rsp)
	movb	$0, (%rbx)
	movl	$4, 108(%rsp)
	leaq	112(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
.Ltmp6:
	movl	$4, %edi
	callq	_Znam
.Ltmp7:
# BB#102:                               #   in Loop: Header=BB2_99 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	movq	%rax, 112(%rsp)
	movb	$0, (%rax)
	movl	$4, 124(%rsp)
	movl	$1, %ebx
	je	.LBB2_111
# BB#103:                               #   in Loop: Header=BB2_99 Depth=1
	movq	(%rdi), %rax
.Ltmp9:
	movl	%r13d, %esi
	leaq	52(%rsp), %rdx
	leaq	164(%rsp), %rcx
	leaq	160(%rsp), %r8
	callq	*56(%rax)
	movl	%eax, %r12d
.Ltmp10:
# BB#104:                               #   in Loop: Header=BB2_99 Depth=1
	testl	%r12d, %r12d
	jne	.LBB2_112
# BB#105:                               #   in Loop: Header=BB2_99 Depth=1
	movl	164(%rsp), %ecx
	cmpl	$0, %ecx
	setne	129(%rsp)
	movl	52(%rsp), %eax
	testl	%eax, %eax
	setne	128(%rsp)
	cmpl	$0, %ecx
	movl	160(%rsp), %ecx
	movl	%ecx, 56(%rsp)
	movl	%r13d, 60(%rsp)
	je	.LBB2_51
# BB#106:                               #   in Loop: Header=BB2_99 Depth=1
	movl	$0, 32(%rsp)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp11:
	movl	$6, %edx
	movl	%r13d, %esi
	leaq	32(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r12d
.Ltmp12:
# BB#107:                               #   in Loop: Header=BB2_99 Depth=1
	movl	$1, %ebp
	testl	%r12d, %r12d
	jne	.LBB2_8
# BB#108:                               #   in Loop: Header=BB2_99 Depth=1
	movzwl	32(%rsp), %eax
	cmpl	$11, %eax
	je	.LBB2_6
# BB#109:                               #   in Loop: Header=BB2_99 Depth=1
	movl	$-2147024809, %r12d     # imm = 0x80070057
	testw	%ax, %ax
	jne	.LBB2_8
# BB#110:                               #   in Loop: Header=BB2_99 Depth=1
	movb	$0, 130(%rsp)
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_111:                              #   in Loop: Header=BB2_99 Depth=1
	movl	$-2147467259, %r12d     # imm = 0x80004005
.LBB2_112:                              #   in Loop: Header=BB2_99 Depth=1
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_114
# BB#113:                               #   in Loop: Header=BB2_99 Depth=1
	callq	_ZdaPv
.LBB2_114:                              # %_ZN11CStringBaseIcED2Ev.exit.i190
                                        #   in Loop: Header=BB2_99 Depth=1
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_116
# BB#115:                               #   in Loop: Header=BB2_99 Depth=1
	callq	_ZdaPv
.LBB2_116:                              # %_ZN11CStringBaseIcED2Ev.exit1.i
                                        #   in Loop: Header=BB2_99 Depth=1
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_118
# BB#117:                               #   in Loop: Header=BB2_99 Depth=1
	callq	_ZdaPv
.LBB2_118:                              # %_ZN8NArchive4NTar11CUpdateItemD2Ev.exit
                                        #   in Loop: Header=BB2_99 Depth=1
	testl	%ebx, %ebx
	je	.LBB2_5
	jmp	.LBB2_121
.LBB2_119:                              # %._crit_edge.loopexit
	movq	72(%r14), %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	240(%rsp), %rsi         # 8-byte Reload
.LBB2_120:                              # %._crit_edge
	addq	$40, %r14
.Ltmp79:
	leaq	168(%rsp), %rbp
	movq	%r14, %rdx
	movq	%rbp, %rcx
	movq	%rax, %r8
	callq	_ZN8NArchive4NTar13UpdateArchiveEP9IInStreamP20ISequentialOutStreamRK13CObjectVectorINS0_7CItemExEERKS5_INS0_11CUpdateItemEEP22IArchiveUpdateCallback
	movl	%eax, %r12d
.Ltmp80:
.LBB2_121:                              # %._crit_edge..loopexit_crit_edge
	leaq	168(%rsp), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NTar11CUpdateItemEE+16, 168(%rsp)
.Ltmp91:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp92:
# BB#122:
.Ltmp97:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp98:
	jmp	.LBB2_167
.LBB2_123:
.Ltmp81:
	movq	%rdx, %r14
	movq	%rax, %r15
	jmp	.LBB2_163
.LBB2_124:
.Ltmp99:
	movq	%rdx, %r14
	movq	%rax, %r15
	jmp	.LBB2_165
.LBB2_125:
.Ltmp93:
	movq	%rdx, %r14
	movq	%rax, %r15
.Ltmp94:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp95:
	jmp	.LBB2_165
.LBB2_126:
.Ltmp96:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_127:
.Ltmp50:
	movq	%rdx, %r14
	movq	%rax, %r15
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_130
# BB#128:
	callq	_ZdaPv
	jmp	.LBB2_130
.LBB2_129:
.Ltmp47:
	movq	%rdx, %r14
	movq	%rax, %r15
.LBB2_130:                              # %_ZN11CStringBaseIcED2Ev.exit175
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_133
# BB#131:
	callq	_ZdaPv
	jmp	.LBB2_133
.LBB2_132:
.Ltmp44:
	movq	%rdx, %r14
	movq	%rax, %r15
.LBB2_133:                              # %_ZN11CStringBaseIwED2Ev.exit176
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_139
# BB#134:
	callq	_ZdaPv
	jmp	.LBB2_139
.LBB2_135:
.Ltmp41:
	jmp	.LBB2_138
.LBB2_136:
.Ltmp58:
	jmp	.LBB2_150
.LBB2_137:
.Ltmp53:
.LBB2_138:
	movq	%rdx, %r14
	movq	%rax, %r15
.LBB2_139:
.Ltmp54:
	leaq	216(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp55:
	jmp	.LBB2_151
.LBB2_140:
.Ltmp36:
	jmp	.LBB2_150
.LBB2_141:
.Ltmp31:
	movq	%rdx, %r14
	movq	%rax, %r15
.Ltmp32:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp33:
	jmp	.LBB2_151
.LBB2_142:
.Ltmp26:
	jmp	.LBB2_150
.LBB2_143:
.Ltmp75:
	movq	%rdx, %r14
	movq	%rax, %r15
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB2_151
.LBB2_144:
.Ltmp21:
	movq	%rdx, %r14
	movq	%rax, %r15
.Ltmp22:
	leaq	136(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp23:
	jmp	.LBB2_151
.LBB2_145:
.Ltmp70:
	jmp	.LBB2_150
.LBB2_146:
.Ltmp65:
	movq	%rdx, %r14
	movq	%rax, %r15
.Ltmp66:
	leaq	200(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp67:
	jmp	.LBB2_151
.LBB2_147:
.Ltmp18:
	jmp	.LBB2_150
.LBB2_148:
.Ltmp13:
	movq	%rdx, %r14
	movq	%rax, %r15
.Ltmp14:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp15:
	jmp	.LBB2_151
.LBB2_149:
.Ltmp78:
.LBB2_150:
	movq	%rdx, %r14
	movq	%rax, %r15
.LBB2_151:
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_153
# BB#152:
	callq	_ZdaPv
.LBB2_153:                              # %_ZN11CStringBaseIcED2Ev.exit.i191
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_155
# BB#154:
	callq	_ZdaPv
.LBB2_155:                              # %_ZN11CStringBaseIcED2Ev.exit1.i192
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_162
# BB#156:
	callq	_ZdaPv
	jmp	.LBB2_162
.LBB2_158:                              # %_ZN11CStringBaseIcED2Ev.exit.i
.Ltmp8:
	movq	%rdx, %r14
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	80(%rsp), %rbp
	testq	%rbp, %rbp
	jne	.LBB2_160
	jmp	.LBB2_162
.LBB2_159:                              # %_ZN11CStringBaseIcED2Ev.exit.thread.i
.Ltmp5:
	movq	%rdx, %r14
	movq	%rax, %r15
.LBB2_160:
	movq	%rbp, %rdi
	callq	_ZdaPv
	jmp	.LBB2_162
.LBB2_161:
.Ltmp2:
	movq	%rdx, %r14
	movq	%rax, %r15
.LBB2_162:                              # %.body150
	leaq	168(%rsp), %rbp
.LBB2_163:
	movq	$_ZTV13CObjectVectorIN8NArchive4NTar11CUpdateItemEE+16, 168(%rsp)
.Ltmp82:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp83:
# BB#164:
.Ltmp88:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp89:
.LBB2_165:
	movq	%r15, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB2_168
# BB#166:
	callq	__cxa_end_catch
	movl	$-2147024882, %r12d     # imm = 0x8007000E
.LBB2_167:
	movl	%r12d, %eax
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_168:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp100:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp101:
# BB#169:
.LBB2_170:
.Ltmp102:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB2_171:
.Ltmp84:
	movq	%rax, %rbx
.Ltmp85:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp86:
	jmp	.LBB2_174
.LBB2_172:
.Ltmp87:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_173:
.Ltmp90:
	movq	%rax, %rbx
.LBB2_174:                              # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback, .Lfunc_end2-_ZN8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\311\203\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\272\003"              # Call site table length
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 1 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin0   #     jumps to .Ltmp18
	.byte	3                       #   On action: 2
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 2 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin0   #     jumps to .Ltmp21
	.byte	3                       #   On action: 2
	.long	.Ltmp24-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin0   #     jumps to .Ltmp26
	.byte	3                       #   On action: 2
	.long	.Ltmp27-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp30-.Ltmp27         #   Call between .Ltmp27 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin0   #     jumps to .Ltmp31
	.byte	3                       #   On action: 2
	.long	.Ltmp34-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin0   #     jumps to .Ltmp36
	.byte	3                       #   On action: 2
	.long	.Ltmp37-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp53-.Lfunc_begin0   #     jumps to .Ltmp53
	.byte	3                       #   On action: 2
	.long	.Ltmp56-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin0   #     jumps to .Ltmp58
	.byte	3                       #   On action: 2
	.long	.Ltmp39-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin0   #     jumps to .Ltmp41
	.byte	3                       #   On action: 2
	.long	.Ltmp42-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin0   #     jumps to .Ltmp44
	.byte	3                       #   On action: 2
	.long	.Ltmp45-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin0   #     jumps to .Ltmp47
	.byte	3                       #   On action: 2
	.long	.Ltmp59-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp62-.Ltmp59         #   Call between .Ltmp59 and .Ltmp62
	.long	.Ltmp78-.Lfunc_begin0   #     jumps to .Ltmp78
	.byte	3                       #   On action: 2
	.long	.Ltmp63-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin0   #     jumps to .Ltmp65
	.byte	3                       #   On action: 2
	.long	.Ltmp68-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin0   #     jumps to .Ltmp70
	.byte	3                       #   On action: 2
	.long	.Ltmp71-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp78-.Lfunc_begin0   #     jumps to .Ltmp78
	.byte	3                       #   On action: 2
	.long	.Ltmp73-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin0   #     jumps to .Ltmp75
	.byte	3                       #   On action: 2
	.long	.Ltmp76-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin0   #     jumps to .Ltmp78
	.byte	3                       #   On action: 2
	.long	.Ltmp48-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin0   #     jumps to .Ltmp50
	.byte	3                       #   On action: 2
	.long	.Ltmp51-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin0   #     jumps to .Ltmp53
	.byte	3                       #   On action: 2
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 19 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	3                       #   On action: 2
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 20 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	3                       #   On action: 2
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 21 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	3                       #   On action: 2
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 22 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp78-.Lfunc_begin0   #     jumps to .Ltmp78
	.byte	3                       #   On action: 2
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	3                       #   On action: 2
	.long	.Ltmp79-.Lfunc_begin0   # >> Call Site 24 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin0   #     jumps to .Ltmp81
	.byte	3                       #   On action: 2
	.long	.Ltmp91-.Lfunc_begin0   # >> Call Site 25 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin0   #     jumps to .Ltmp93
	.byte	3                       #   On action: 2
	.long	.Ltmp97-.Lfunc_begin0   # >> Call Site 26 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin0   #     jumps to .Ltmp99
	.byte	3                       #   On action: 2
	.long	.Ltmp94-.Lfunc_begin0   # >> Call Site 27 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin0   #     jumps to .Ltmp96
	.byte	1                       #   On action: 1
	.long	.Ltmp54-.Lfunc_begin0   # >> Call Site 28 <<
	.long	.Ltmp15-.Ltmp54         #   Call between .Ltmp54 and .Ltmp15
	.long	.Ltmp90-.Lfunc_begin0   #     jumps to .Ltmp90
	.byte	1                       #   On action: 1
	.long	.Ltmp82-.Lfunc_begin0   # >> Call Site 29 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin0   #     jumps to .Ltmp84
	.byte	1                       #   On action: 1
	.long	.Ltmp88-.Lfunc_begin0   # >> Call Site 30 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin0   #     jumps to .Ltmp90
	.byte	1                       #   On action: 1
	.long	.Ltmp89-.Lfunc_begin0   # >> Call Site 31 <<
	.long	.Ltmp100-.Ltmp89        #   Call between .Ltmp89 and .Ltmp100
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp100-.Lfunc_begin0  # >> Call Site 32 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin0  #     jumps to .Ltmp102
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin0  # >> Call Site 33 <<
	.long	.Ltmp85-.Ltmp101        #   Call between .Ltmp101 and .Ltmp85
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin0   # >> Call Site 34 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin0   #     jumps to .Ltmp87
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.section	.text._ZN11CStringBaseIcEpLEc,"axG",@progbits,_ZN11CStringBaseIcEpLEc,comdat
	.weak	_ZN11CStringBaseIcEpLEc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEc,@function
_ZN11CStringBaseIcEpLEc:                # @_ZN11CStringBaseIcEpLEc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB4_28
# BB#1:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB4_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB4_3:                                # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rsi,%rbx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB4_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB4_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB4_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB4_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB4_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB4_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB4_17
.LBB4_7:
	xorl	%ecx, %ecx
.LBB4_8:                                # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB4_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB4_10:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB4_10
.LBB4_11:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB4_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB4_13:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB4_13
	jmp	.LBB4_26
.LBB4_25:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB4_27
.LBB4_26:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB4_27:
	movq	%r12, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%r13)
.LBB4_28:                               # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movb	%r14b, (%rax,%rcx)
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movb	$0, 1(%rax,%rcx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_17:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB4_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r12,%rbx)
	movups	%xmm1, 16(%r12,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB4_20
	jmp	.LBB4_21
.LBB4_18:
	xorl	%ebx, %ebx
.LBB4_21:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB4_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB4_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB4_23
.LBB4_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB4_8
	jmp	.LBB4_26
.Lfunc_end4:
	.size	_ZN11CStringBaseIcEpLEc, .Lfunc_end4-_ZN11CStringBaseIcEpLEc
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTarL13GetPropStringEP22IArchiveUpdateCallbackjjR11CStringBaseIcE,@function
_ZN8NArchive4NTarL13GetPropStringEP22IArchiveUpdateCallbackjjR11CStringBaseIcE: # @_ZN8NArchive4NTarL13GetPropStringEP22IArchiveUpdateCallbackjjR11CStringBaseIcE
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 96
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	$0, 40(%rsp)
	movq	(%rdi), %rax
.Ltmp103:
	leaq	40(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %ebx
.Ltmp104:
# BB#1:
	testl	%ebx, %ebx
	jne	.LBB5_46
# BB#2:
	movzwl	40(%rsp), %eax
	testw	%ax, %ax
	je	.LBB5_45
# BB#3:
	movl	$-2147024809, %ebx      # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$8, %eax
	jne	.LBB5_46
# BB#4:
	movq	48(%rsp), %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %r15d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB5_5:                                # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB5_5
# BB#6:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%r15), %ebp
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp106:
	callq	_Znam
.Ltmp107:
# BB#7:                                 # %.noexc
	movq	%rax, (%rsp)
	movl	$0, (%rax)
	movl	%ebp, 12(%rsp)
	.p2align	4, 0x90
.LBB5_8:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB5_8
# BB#9:
	movl	%r15d, 8(%rsp)
.Ltmp109:
	leaq	24(%rsp), %rbx
	movq	%rsp, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp110:
# BB#10:
	cmpq	%r14, %rbx
	je	.LBB5_41
# BB#11:
	movl	$0, 8(%r14)
	movq	(%r14), %rax
	movb	$0, (%rax)
	movslq	32(%rsp), %rax
	leaq	1(%rax), %r15
	movl	12(%r14), %ebp
	cmpl	%ebp, %r15d
	jne	.LBB5_13
# BB#12:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	(%r14), %rbx
	jmp	.LBB5_38
.LBB5_13:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r15, %rdi
	cmovlq	%rax, %rdi
.Ltmp112:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp113:
# BB#14:                                # %.noexc20
	testl	%ebp, %ebp
	jle	.LBB5_37
# BB#15:                                # %.preheader.i.i
	movslq	8(%r14), %rbp
	testq	%rbp, %rbp
	movq	(%r14), %rdi
	jle	.LBB5_35
# BB#16:                                # %.lr.ph.preheader.i.i
	cmpl	$31, %ebp
	jbe	.LBB5_17
# BB#24:                                # %min.iters.checked
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB5_17
# BB#25:                                # %vector.memcheck
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB5_27
# BB#26:                                # %vector.memcheck
	leaq	(%rbx,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB5_27
.LBB5_17:
	xorl	%ecx, %ecx
.LBB5_18:                               # %.lr.ph.i.i.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rax
	subq	%rcx, %rax
	andq	$7, %rsi
	je	.LBB5_21
# BB#19:                                # %.lr.ph.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB5_20:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB5_20
.LBB5_21:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB5_36
# BB#22:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rbp
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB5_23:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB5_23
	jmp	.LBB5_36
.LBB5_35:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB5_37
.LBB5_36:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB5_37:                               # %._crit_edge17.i.i
	movq	%rbx, (%r14)
	movslq	8(%r14), %rax
	movb	$0, (%rbx,%rax)
	movl	%r15d, 12(%r14)
.LBB5_38:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	24(%rsp), %rax
	.p2align	4, 0x90
.LBB5_39:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB5_39
# BB#40:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i
	movl	32(%rsp), %eax
	movl	%eax, 8(%r14)
.LBB5_41:                               # %_ZN11CStringBaseIcEaSERKS0_.exit
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_43
# BB#42:
	callq	_ZdaPv
.LBB5_43:                               # %_ZN11CStringBaseIcED2Ev.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_45
# BB#44:
	callq	_ZdaPv
.LBB5_45:
	xorl	%ebx, %ebx
.LBB5_46:
	leaq	40(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movl	%ebx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_27:                               # %vector.body.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_28
# BB#29:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
.LBB5_30:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB5_30
	jmp	.LBB5_31
.LBB5_28:
	xorl	%edx, %edx
.LBB5_31:                               # %vector.body.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB5_34
# BB#32:                                # %vector.body.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbx,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB5_33:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB5_33
.LBB5_34:                               # %middle.block
	cmpq	%rcx, %rbp
	jne	.LBB5_18
	jmp	.LBB5_36
.LBB5_49:
.Ltmp114:
	movq	%rax, %rbx
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_51
# BB#50:
	callq	_ZdaPv
	jmp	.LBB5_51
.LBB5_48:
.Ltmp111:
	movq	%rax, %rbx
.LBB5_51:                               # %_ZN11CStringBaseIcED2Ev.exit21
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_55
# BB#52:
	callq	_ZdaPv
	jmp	.LBB5_55
.LBB5_47:
.Ltmp108:
	jmp	.LBB5_54
.LBB5_53:
.Ltmp105:
.LBB5_54:
	movq	%rax, %rbx
.LBB5_55:
.Ltmp115:
	leaq	40(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp116:
# BB#56:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB5_57:
.Ltmp117:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN8NArchive4NTarL13GetPropStringEP22IArchiveUpdateCallbackjjR11CStringBaseIcE, .Lfunc_end5-_ZN8NArchive4NTarL13GetPropStringEP22IArchiveUpdateCallbackjjR11CStringBaseIcE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp103-.Lfunc_begin1  # >> Call Site 1 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin1  #     jumps to .Ltmp105
	.byte	0                       #   On action: cleanup
	.long	.Ltmp106-.Lfunc_begin1  # >> Call Site 2 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin1  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin1  # >> Call Site 3 <<
	.long	.Ltmp110-.Ltmp109       #   Call between .Ltmp109 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin1  #     jumps to .Ltmp111
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin1  # >> Call Site 4 <<
	.long	.Ltmp113-.Ltmp112       #   Call between .Ltmp112 and .Ltmp113
	.long	.Ltmp114-.Lfunc_begin1  #     jumps to .Ltmp114
	.byte	0                       #   On action: cleanup
	.long	.Ltmp113-.Lfunc_begin1  # >> Call Site 5 <<
	.long	.Ltmp115-.Ltmp113       #   Call between .Ltmp113 and .Ltmp115
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp115-.Lfunc_begin1  # >> Call Site 6 <<
	.long	.Ltmp116-.Ltmp115       #   Call between .Ltmp115 and .Ltmp116
	.long	.Ltmp117-.Lfunc_begin1  #     jumps to .Ltmp117
	.byte	1                       #   On action: 1
	.long	.Ltmp116-.Lfunc_begin1  # >> Call Site 7 <<
	.long	.Lfunc_end5-.Ltmp116    #   Call between .Ltmp116 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NTar11CUpdateItemEE+16, (%rbx)
.Ltmp118:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp119:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB6_2:
.Ltmp120:
	movq	%rax, %r14
.Ltmp121:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp122:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_4:
.Ltmp123:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED2Ev, .Lfunc_end6-_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp118-.Lfunc_begin2  # >> Call Site 1 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin2  #     jumps to .Ltmp120
	.byte	0                       #   On action: cleanup
	.long	.Ltmp119-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Ltmp121-.Ltmp119       #   Call between .Ltmp119 and .Ltmp121
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp121-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin2  #     jumps to .Ltmp123
	.byte	1                       #   On action: 1
	.long	.Ltmp122-.Lfunc_begin2  # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp122    #   Call between .Ltmp122 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn24_N8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback,@function
_ZThn24_N8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback: # @_ZThn24_N8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback # TAILCALL
.Lfunc_end7:
	.size	_ZThn24_N8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback, .Lfunc_end7-_ZThn24_N8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -24
.Lcfi44:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NTar11CUpdateItemEE+16, (%rbx)
.Ltmp124:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp125:
# BB#1:
.Ltmp130:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp131:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB8_5:
.Ltmp132:
	movq	%rax, %r14
	jmp	.LBB8_6
.LBB8_3:
.Ltmp126:
	movq	%rax, %r14
.Ltmp127:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp128:
.LBB8_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_4:
.Ltmp129:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED0Ev, .Lfunc_end8-_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp124-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp126-.Lfunc_begin3  #     jumps to .Ltmp126
	.byte	0                       #   On action: cleanup
	.long	.Ltmp130-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin3  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp129-.Lfunc_begin3  #     jumps to .Ltmp129
	.byte	1                       #   On action: 1
	.long	.Ltmp128-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp128    #   Call between .Ltmp128 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 64
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB9_11
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB9_10
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_5
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=1
	callq	_ZdaPv
.LBB9_5:                                # %_ZN11CStringBaseIcED2Ev.exit.i
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_7
# BB#6:                                 #   in Loop: Header=BB9_2 Depth=1
	callq	_ZdaPv
.LBB9_7:                                # %_ZN11CStringBaseIcED2Ev.exit1.i
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_9
# BB#8:                                 #   in Loop: Header=BB9_2 Depth=1
	callq	_ZdaPv
.LBB9_9:                                # %_ZN8NArchive4NTar11CUpdateItemD2Ev.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB9_10:                               #   in Loop: Header=BB9_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB9_2
.LBB9_11:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end9:
	.size	_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEE6DeleteEii, .Lfunc_end9-_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN8NArchive4NTar11CUpdateItemC2ERKS1_,"axG",@progbits,_ZN8NArchive4NTar11CUpdateItemC2ERKS1_,comdat
	.weak	_ZN8NArchive4NTar11CUpdateItemC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar11CUpdateItemC2ERKS1_,@function
_ZN8NArchive4NTar11CUpdateItemC2ERKS1_: # @_ZN8NArchive4NTar11CUpdateItemC2ERKS1_
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 48
.Lcfi63:
	.cfi_offset %rbx, -48
.Lcfi64:
	.cfi_offset %r12, -40
.Lcfi65:
	.cfi_offset %r13, -32
.Lcfi66:
	.cfi_offset %r14, -24
.Lcfi67:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	16(%r14), %rax
	movq	%rax, 16(%rbx)
	movups	(%r14), %xmm0
	movups	%xmm0, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movslq	32(%r14), %rax
	leaq	1(%rax), %r15
	testl	%r15d, %r15d
	je	.LBB10_1
# BB#2:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r15, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, 24(%rbx)
	movb	$0, (%rax)
	movl	%r15d, 36(%rbx)
	jmp	.LBB10_3
.LBB10_1:
	xorl	%eax, %eax
.LBB10_3:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	leaq	24(%rbx), %r15
	movq	24(%r14), %rcx
	.p2align	4, 0x90
.LBB10_4:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB10_4
# BB#5:                                 # %_ZN11CStringBaseIcEC2ERKS0_.exit
	movl	32(%r14), %eax
	movl	%eax, 32(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rbx)
	movslq	48(%r14), %rax
	leaq	1(%rax), %r12
	testl	%r12d, %r12d
	je	.LBB10_6
# BB#7:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
.Ltmp133:
	callq	_Znam
.Ltmp134:
# BB#8:                                 # %.noexc
	movq	%rax, 40(%rbx)
	movb	$0, (%rax)
	movl	%r12d, 52(%rbx)
	jmp	.LBB10_9
.LBB10_6:
	xorl	%eax, %eax
.LBB10_9:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i8
	leaq	40(%rbx), %r12
	movq	40(%r14), %rcx
	.p2align	4, 0x90
.LBB10_10:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB10_10
# BB#11:
	movl	48(%r14), %eax
	movl	%eax, 48(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%rbx)
	movslq	64(%r14), %rax
	leaq	1(%rax), %r13
	testl	%r13d, %r13d
	je	.LBB10_12
# BB#13:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r13, %rdi
	cmovlq	%rax, %rdi
.Ltmp136:
	callq	_Znam
.Ltmp137:
# BB#14:                                # %.noexc15
	movq	%rax, 56(%rbx)
	movb	$0, (%rax)
	movl	%r13d, 68(%rbx)
	jmp	.LBB10_15
.LBB10_12:
	xorl	%eax, %eax
.LBB10_15:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i12
	movq	56(%r14), %rcx
	.p2align	4, 0x90
.LBB10_16:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB10_16
# BB#17:
	movl	64(%r14), %eax
	movl	%eax, 64(%rbx)
	movb	74(%r14), %al
	movb	%al, 74(%rbx)
	movzwl	72(%r14), %eax
	movw	%ax, 72(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB10_19:
.Ltmp138:
	movq	%rax, %rbx
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_21
# BB#20:
	callq	_ZdaPv
	jmp	.LBB10_21
.LBB10_18:
.Ltmp135:
	movq	%rax, %rbx
.LBB10_21:                              # %_ZN11CStringBaseIcED2Ev.exit
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB10_23
# BB#22:
	callq	_ZdaPv
.LBB10_23:                              # %_ZN11CStringBaseIcED2Ev.exit17
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN8NArchive4NTar11CUpdateItemC2ERKS1_, .Lfunc_end10-_ZN8NArchive4NTar11CUpdateItemC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp133-.Lfunc_begin4  #   Call between .Lfunc_begin4 and .Ltmp133
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin4  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp136-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp137-.Ltmp136       #   Call between .Ltmp136 and .Ltmp137
	.long	.Ltmp138-.Lfunc_begin4  #     jumps to .Ltmp138
	.byte	0                       #   On action: cleanup
	.long	.Ltmp137-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Lfunc_end10-.Ltmp137   #   Call between .Ltmp137 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV13CObjectVectorIN8NArchive4NTar11CUpdateItemEE,@object # @_ZTV13CObjectVectorIN8NArchive4NTar11CUpdateItemEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NTar11CUpdateItemEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NTar11CUpdateItemEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NTar11CUpdateItemEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NTar11CUpdateItemEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NTar11CUpdateItemEE
	.quad	_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NTar11CUpdateItemEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NTar11CUpdateItemEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NTar11CUpdateItemEE,@object # @_ZTS13CObjectVectorIN8NArchive4NTar11CUpdateItemEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NTar11CUpdateItemEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NTar11CUpdateItemEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NTar11CUpdateItemEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NTar11CUpdateItemEE:
	.asciz	"13CObjectVectorIN8NArchive4NTar11CUpdateItemEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NTar11CUpdateItemEE, 47

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN8NArchive4NTar11CUpdateItemEE,@object # @_ZTI13CObjectVectorIN8NArchive4NTar11CUpdateItemEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NTar11CUpdateItemEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NTar11CUpdateItemEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NTar11CUpdateItemEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NTar11CUpdateItemEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NTar11CUpdateItemEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NTar11CUpdateItemEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
