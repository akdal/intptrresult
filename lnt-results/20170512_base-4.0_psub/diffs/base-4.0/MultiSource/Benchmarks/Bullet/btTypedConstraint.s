	.text
	.file	"btTypedConstraint.bc"
	.section	.text._ZN11btRigidBodyD2Ev,"axG",@progbits,_ZN11btRigidBodyD2Ev,comdat
	.weak	_ZN11btRigidBodyD2Ev
	.p2align	4, 0x90
	.type	_ZN11btRigidBodyD2Ev,@function
_ZN11btRigidBodyD2Ev:                   # @_ZN11btRigidBodyD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV11btRigidBody+16, (%rbx)
	movq	536(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#1:
	cmpb	$0, 544(%rbx)
	je	.LBB0_3
# BB#2:
.Ltmp0:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp1:
.LBB0_3:                                # %.noexc
	movq	$0, 536(%rbx)
.LBB0_4:
	movb	$1, 544(%rbx)
	movq	$0, 536(%rbx)
	movq	$0, 524(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17btCollisionObjectD2Ev # TAILCALL
.LBB0_5:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp4:
# BB#6:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_7:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN11btRigidBodyD2Ev, .Lfunc_end0-_ZN11btRigidBodyD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN17btTypedConstraintC2E21btTypedConstraintType
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraintC2E21btTypedConstraintType,@function
_ZN17btTypedConstraintC2E21btTypedConstraintType: # @_ZN17btTypedConstraintC2E21btTypedConstraintType
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 32
	movl	%esi, 8(%rdi)
	movq	$_ZTV17btTypedConstraint+16, (%rdi)
	movl	$-1, 12(%rdi)
	movl	$-1, 16(%rdi)
	movb	$0, 20(%rdi)
	movl	$_ZL7s_fixed, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, 24(%rdi)
	movabsq	$4510805388492275712, %rax # imm = 0x3E99999A00000000
	movq	%rax, 40(%rdi)
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, (%rsp)
	movq	%rsp, %rsi
	movl	$_ZL7s_fixed, %edi
	pxor	%xmm0, %xmm0
	callq	_ZN11btRigidBody12setMassPropsEfRK9btVector3
	addq	$24, %rsp
	retq
.Lfunc_end1:
	.size	_ZN17btTypedConstraintC2E21btTypedConstraintType, .Lfunc_end1-_ZN17btTypedConstraintC2E21btTypedConstraintType
	.cfi_endproc

	.globl	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody,@function
_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody: # @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 32
	movl	%esi, 8(%rdi)
	movq	$_ZTV17btTypedConstraint+16, (%rdi)
	movl	$-1, 12(%rdi)
	movl	$-1, 16(%rdi)
	movb	$0, 20(%rdi)
	movq	%rdx, 24(%rdi)
	movq	$_ZL7s_fixed, 32(%rdi)
	movabsq	$4510805388492275712, %rax # imm = 0x3E99999A00000000
	movq	%rax, 40(%rdi)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rsi
	movl	$_ZL7s_fixed, %edi
	xorps	%xmm0, %xmm0
	callq	_ZN11btRigidBody12setMassPropsEfRK9btVector3
	addq	$24, %rsp
	retq
.Lfunc_end2:
	.size	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody, .Lfunc_end2-_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody
	.cfi_endproc

	.globl	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_,@function
_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_: # @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 32
	movl	%esi, 8(%rdi)
	movq	$_ZTV17btTypedConstraint+16, (%rdi)
	movl	$-1, 12(%rdi)
	movl	$-1, 16(%rdi)
	movb	$0, 20(%rdi)
	movq	%rdx, 24(%rdi)
	movq	%rcx, 32(%rdi)
	movabsq	$4510805388492275712, %rax # imm = 0x3E99999A00000000
	movq	%rax, 40(%rdi)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rsi
	movl	$_ZL7s_fixed, %edi
	xorps	%xmm0, %xmm0
	callq	_ZN11btRigidBody12setMassPropsEfRK9btVector3
	addq	$24, %rsp
	retq
.Lfunc_end3:
	.size	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_, .Lfunc_end3-_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN17btTypedConstraint14getMotorFactorEfffff
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraint14getMotorFactorEfffff,@function
_ZN17btTypedConstraint14getMotorFactorEfffff: # @_ZN17btTypedConstraint14getMotorFactorEfffff
	.cfi_startproc
# BB#0:
	ucomiss	%xmm2, %xmm1
	jbe	.LBB4_2
# BB#1:
	movss	.LCPI4_0(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	retq
.LBB4_2:
	xorps	%xmm5, %xmm5
	jne	.LBB4_3
	jp	.LBB4_3
.LBB4_14:
	movaps	%xmm5, %xmm0
	retq
.LBB4_3:
	divss	%xmm4, %xmm3
	xorps	%xmm5, %xmm5
	ucomiss	%xmm3, %xmm5
	jbe	.LBB4_8
# BB#4:
	ucomiss	%xmm1, %xmm0
	jb	.LBB4_7
# BB#5:
	movaps	%xmm1, %xmm2
	subss	%xmm3, %xmm2
	ucomiss	%xmm0, %xmm2
	jbe	.LBB4_7
# BB#6:
	subss	%xmm0, %xmm1
	divss	%xmm3, %xmm1
	movaps	%xmm1, %xmm5
	movaps	%xmm5, %xmm0
	retq
.LBB4_8:
	ucomiss	%xmm5, %xmm3
	jbe	.LBB4_14
# BB#9:
	ucomiss	%xmm0, %xmm2
	jb	.LBB4_12
# BB#10:
	movaps	%xmm2, %xmm1
	subss	%xmm3, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB4_12
# BB#11:
	subss	%xmm0, %xmm2
	divss	%xmm3, %xmm2
	jmp	.LBB4_13
.LBB4_7:
	cmpltss	%xmm1, %xmm0
	movss	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	andnps	%xmm1, %xmm0
	retq
.LBB4_12:
	cmpltss	%xmm0, %xmm2
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	andnps	%xmm0, %xmm2
.LBB4_13:
	movaps	%xmm2, %xmm5
	movaps	%xmm5, %xmm0
	retq
.Lfunc_end4:
	.size	_ZN17btTypedConstraint14getMotorFactorEfffff, .Lfunc_end4-_ZN17btTypedConstraint14getMotorFactorEfffff
	.cfi_endproc

	.section	.text._ZN17btTypedConstraintD2Ev,"axG",@progbits,_ZN17btTypedConstraintD2Ev,comdat
	.weak	_ZN17btTypedConstraintD2Ev
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraintD2Ev,@function
_ZN17btTypedConstraintD2Ev:             # @_ZN17btTypedConstraintD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end5:
	.size	_ZN17btTypedConstraintD2Ev, .Lfunc_end5-_ZN17btTypedConstraintD2Ev
	.cfi_endproc

	.section	.text._ZN17btTypedConstraintD0Ev,"axG",@progbits,_ZN17btTypedConstraintD0Ev,comdat
	.weak	_ZN17btTypedConstraintD0Ev
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraintD0Ev,@function
_ZN17btTypedConstraintD0Ev:             # @_ZN17btTypedConstraintD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end6:
	.size	_ZN17btTypedConstraintD0Ev, .Lfunc_end6-_ZN17btTypedConstraintD0Ev
	.cfi_endproc

	.section	.text._ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,"axG",@progbits,_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,comdat
	.weak	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,@function
_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif: # @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end7:
	.size	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif, .Lfunc_end7-_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end8:
	.size	__clang_call_terminate, .Lfunc_end8-__clang_call_terminate

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_btTypedConstraint.ii,@function
_GLOBAL__sub_I_btTypedConstraint.ii:    # @_GLOBAL__sub_I_btTypedConstraint.ii
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 32
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rcx
	movl	$_ZL7s_fixed, %edi
	xorps	%xmm0, %xmm0
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	_ZN11btRigidBodyC1EfP13btMotionStateP16btCollisionShapeRK9btVector3
	movl	$_ZN11btRigidBodyD2Ev, %edi
	movl	$_ZL7s_fixed, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	addq	$24, %rsp
	retq
.Lfunc_end9:
	.size	_GLOBAL__sub_I_btTypedConstraint.ii, .Lfunc_end9-_GLOBAL__sub_I_btTypedConstraint.ii
	.cfi_endproc

	.type	_ZL7s_fixed,@object     # @_ZL7s_fixed
	.local	_ZL7s_fixed
	.comm	_ZL7s_fixed,568,8
	.type	_ZTV17btTypedConstraint,@object # @_ZTV17btTypedConstraint
	.section	.rodata._ZTV17btTypedConstraint,"aG",@progbits,_ZTV17btTypedConstraint,comdat
	.weak	_ZTV17btTypedConstraint
	.p2align	3
_ZTV17btTypedConstraint:
	.quad	0
	.quad	_ZTI17btTypedConstraint
	.quad	_ZN17btTypedConstraintD2Ev
	.quad	_ZN17btTypedConstraintD0Ev
	.quad	__cxa_pure_virtual
	.quad	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.size	_ZTV17btTypedConstraint, 72

	.type	_ZTS17btTypedConstraint,@object # @_ZTS17btTypedConstraint
	.section	.rodata._ZTS17btTypedConstraint,"aG",@progbits,_ZTS17btTypedConstraint,comdat
	.weak	_ZTS17btTypedConstraint
	.p2align	4
_ZTS17btTypedConstraint:
	.asciz	"17btTypedConstraint"
	.size	_ZTS17btTypedConstraint, 20

	.type	_ZTS13btTypedObject,@object # @_ZTS13btTypedObject
	.section	.rodata._ZTS13btTypedObject,"aG",@progbits,_ZTS13btTypedObject,comdat
	.weak	_ZTS13btTypedObject
_ZTS13btTypedObject:
	.asciz	"13btTypedObject"
	.size	_ZTS13btTypedObject, 16

	.type	_ZTI13btTypedObject,@object # @_ZTI13btTypedObject
	.section	.rodata._ZTI13btTypedObject,"aG",@progbits,_ZTI13btTypedObject,comdat
	.weak	_ZTI13btTypedObject
	.p2align	3
_ZTI13btTypedObject:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13btTypedObject
	.size	_ZTI13btTypedObject, 16

	.type	_ZTI17btTypedConstraint,@object # @_ZTI17btTypedConstraint
	.section	.rodata._ZTI17btTypedConstraint,"aG",@progbits,_ZTI17btTypedConstraint,comdat
	.weak	_ZTI17btTypedConstraint
	.p2align	4
_ZTI17btTypedConstraint:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS17btTypedConstraint
	.long	0                       # 0x0
	.long	1                       # 0x1
	.quad	_ZTI13btTypedObject
	.quad	2050                    # 0x802
	.size	_ZTI17btTypedConstraint, 40

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_btTypedConstraint.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
