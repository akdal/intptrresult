	.text
	.file	"iinit.bc"
	.globl	initial_enter_name
	.p2align	4, 0x90
	.type	initial_enter_name,@function
initial_enter_name:                     # @initial_enter_name
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 48
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	leaq	8(%rsp), %r14
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	name_enter
	movl	$dstack, %edi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	dict_put
	testl	%eax, %eax
	jne	.LBB0_2
# BB#1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_2:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$43, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	initial_enter_name, .Lfunc_end0-initial_enter_name
	.cfi_endproc

	.globl	initial_enter_op
	.p2align	4, 0x90
	.type	initial_enter_op,@function
initial_enter_op:                       # @initial_enter_op
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 64
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	%rsi, (%rsp)
	movw	$37, 8(%rsp)
	movsbl	(%rbx), %eax
	addl	$-48, %eax
	movw	%ax, 10(%rsp)
	movq	%rsp, %r14
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	interp_fix_op
	incq	%rbx
	leaq	16(%rsp), %r15
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	name_enter
	movl	$dstack, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	dict_put
	testl	%eax, %eax
	jne	.LBB1_2
# BB#1:                                 # %initial_enter_name.exit
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB1_2:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$43, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	initial_enter_op, .Lfunc_end1-initial_enter_op
	.cfi_endproc

	.globl	obj_init
	.p2align	4, 0x90
	.type	obj_init,@function
obj_init:                               # @obj_init
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
	subq	$48, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 64
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	$0, 16(%rsp)
	movw	$24, 24(%rsp)
	movq	$0, (%rsp)
	movw	$32, 8(%rsp)
	movl	$401, %edi              # imm = 0x191
	movl	$dstack, %esi
	callq	dict_create
	movups	dstack(%rip), %xmm0
	movups	%xmm0, dstack+16(%rip)
	leaq	32(%rsp), %rbx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	name_enter
	leaq	16(%rsp), %rdx
	movl	$dstack, %edi
	movq	%rbx, %rsi
	callq	dict_put
	testl	%eax, %eax
	jne	.LBB2_3
# BB#1:                                 # %initial_enter_name.exit
	leaq	32(%rsp), %rbx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	name_enter
	movq	%rsp, %rdx
	movl	$dstack, %edi
	movq	%rbx, %rsi
	callq	dict_put
	testl	%eax, %eax
	jne	.LBB2_3
# BB#2:                                 # %initial_enter_name.exit1
	movl	$.L.str.5, %edi
	movl	$name_errordict, %esi
	xorl	%eax, %eax
	callq	name_enter
	movl	$.L.str.6, %edi
	movl	$name_ErrorNames, %esi
	xorl	%eax, %eax
	callq	name_enter
	addq	$48, %rsp
	popq	%rbx
	retq
.LBB2_3:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$43, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	obj_init, .Lfunc_end2-obj_init
	.cfi_endproc

	.globl	op_init
	.p2align	4, 0x90
	.type	op_init,@function
op_init:                                # @op_init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 16
	xorl	%eax, %eax
	callq	zarith_op_init
	xorl	%eax, %eax
	callq	zarray_op_init
	xorl	%eax, %eax
	callq	zcontrol_op_init
	xorl	%eax, %eax
	callq	zdict_op_init
	xorl	%eax, %eax
	callq	zfile_op_init
	xorl	%eax, %eax
	callq	zgeneric_op_init
	xorl	%eax, %eax
	callq	zmath_op_init
	xorl	%eax, %eax
	callq	zmisc_op_init
	xorl	%eax, %eax
	callq	zpacked_op_init
	xorl	%eax, %eax
	callq	zrelbit_op_init
	xorl	%eax, %eax
	callq	zstack_op_init
	xorl	%eax, %eax
	callq	zstring_op_init
	xorl	%eax, %eax
	callq	ztype_op_init
	xorl	%eax, %eax
	callq	zvmem_op_init
	xorl	%eax, %eax
	callq	zchar_op_init
	xorl	%eax, %eax
	callq	zcolor_op_init
	xorl	%eax, %eax
	callq	zdevice_op_init
	xorl	%eax, %eax
	callq	zfont_op_init
	xorl	%eax, %eax
	callq	zgstate_op_init
	xorl	%eax, %eax
	callq	zht_op_init
	xorl	%eax, %eax
	callq	zmatrix_op_init
	xorl	%eax, %eax
	callq	zpaint_op_init
	xorl	%eax, %eax
	callq	zpath_op_init
	xorl	%eax, %eax
	popq	%rcx
	jmp	zpath2_op_init          # TAILCALL
.Lfunc_end3:
	.size	op_init, .Lfunc_end3-op_init
	.cfi_endproc

	.globl	z_op_init
	.p2align	4, 0x90
	.type	z_op_init,@function
z_op_init:                              # @z_op_init
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 80
.Lcfi21:
	.cfi_offset %rbx, -40
.Lcfi22:
	.cfi_offset %r12, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB4_4
# BB#1:                                 # %.lr.ph
	addq	$16, %r12
	leaq	8(%rsp), %r14
	leaq	24(%rsp), %r15
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	-8(%r12), %rax
	movq	%rax, 8(%rsp)
	movw	$37, 16(%rsp)
	movsbl	(%rbx), %eax
	addl	$-48, %eax
	movw	%ax, 18(%rsp)
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	interp_fix_op
	incq	%rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	name_enter
	movl	$dstack, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	dict_put
	testl	%eax, %eax
	jne	.LBB4_5
# BB#3:                                 # %initial_enter_op.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%r12), %rbx
	addq	$16, %r12
	testq	%rbx, %rbx
	jne	.LBB4_2
.LBB4_4:                                # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB4_5:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$43, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	z_op_init, .Lfunc_end4-z_op_init
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s(%d): "
	.size	.L.str, 9

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Benchmarks/MallocBench/gs/iinit.c"
	.size	.L.str.1, 84

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"dict_put failed!\n"
	.size	.L.str.2, 18

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"mark"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"null"
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"errordict"
	.size	.L.str.5, 10

	.type	name_errordict,@object  # @name_errordict
	.comm	name_errordict,16,8
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"ErrorNames"
	.size	.L.str.6, 11

	.type	name_ErrorNames,@object # @name_ErrorNames
	.comm	name_ErrorNames,16,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
