	.text
	.file	"ary3.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI0_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI0_2:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
.LCPI0_3:
	.quad	8                       # 0x8
	.quad	8                       # 0x8
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	$1500000, %r12d         # imm = 0x16E360
	cmpl	$2, %edi
	jne	.LBB0_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r12
.LBB0_2:
	movslq	%r12d, %r15
	movl	$4, %esi
	movq	%r15, %rdi
	callq	calloc
	movq	%rax, %r14
	movl	$4, %esi
	movq	%r15, %rdi
	callq	calloc
	movq	%rax, %rbx
	testl	%r15d, %r15d
	jle	.LBB0_26
# BB#3:                                 # %.lr.ph39.preheader
	movl	%r12d, %eax
	cmpl	$7, %r12d
	jbe	.LBB0_8
# BB#4:                                 # %min.iters.checked
	movl	%r12d, %edx
	andl	$7, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB0_8
# BB#5:                                 # %vector.body.preheader
	leaq	16(%r14), %rsi
	movl	$1, %edi
	movd	%rdi, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI0_0(%rip), %xmm1   # xmm1 = [2,3]
	movdqa	.LCPI0_1(%rip), %xmm2   # xmm2 = [1,1,1,1]
	movdqa	.LCPI0_2(%rip), %xmm3   # xmm3 = [5,5,5,5]
	movdqa	.LCPI0_3(%rip), %xmm4   # xmm4 = [8,8]
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB0_6:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm5
	shufps	$136, %xmm1, %xmm5      # xmm5 = xmm5[0,2],xmm1[0,2]
	movaps	%xmm5, %xmm6
	paddd	%xmm2, %xmm6
	paddd	%xmm3, %xmm5
	movdqu	%xmm6, -16(%rsi)
	movdqu	%xmm5, (%rsi)
	paddq	%xmm4, %xmm0
	paddq	%xmm4, %xmm1
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB0_6
# BB#7:                                 # %middle.block
	testl	%edx, %edx
	jne	.LBB0_9
	jmp	.LBB0_11
.LBB0_8:
	xorl	%ecx, %ecx
.LBB0_9:                                # %.lr.ph39.preheader68
	leaq	(%r14,%rcx,4), %rdx
	subq	%rcx, %rax
	incl	%ecx
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph39
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, (%rdx)
	addq	$4, %rdx
	incl	%ecx
	decq	%rax
	jne	.LBB0_10
.LBB0_11:                               # %.preheader33
	testl	%r12d, %r12d
	jle	.LBB0_26
# BB#12:                                # %.preheader.us.preheader
	movq	%r15, %rax
	notq	%rax
	cmpq	$-3, %rax
	movq	$-2, %rcx
	cmovgq	%rax, %rcx
	leaq	2(%rcx,%r15), %r12
	leaq	-6(%rcx,%r15), %r10
	shrq	$3, %r10
	movq	%r12, %r11
	andq	$-8, %r11
	movq	%r15, %r8
	subq	%r11, %r8
	movl	%r10d, %r9d
	andl	$1, %r9d
	movq	%r11, %rdx
	negq	%rdx
	leaq	-16(%rbx,%r15,4), %rdi
	leaq	-16(%r14,%r15,4), %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_13:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_19 Depth 2
                                        #     Child Loop BB0_22 Depth 2
	cmpq	$8, %r12
	movq	%r15, %rax
	jb	.LBB0_21
# BB#14:                                # %min.iters.checked50
                                        #   in Loop: Header=BB0_13 Depth=1
	testq	%r11, %r11
	movq	%r15, %rax
	je	.LBB0_21
# BB#15:                                # %vector.body46.preheader
                                        #   in Loop: Header=BB0_13 Depth=1
	testq	%r9, %r9
	jne	.LBB0_17
# BB#16:                                # %vector.body46.prol
                                        #   in Loop: Header=BB0_13 Depth=1
	movdqu	-16(%r14,%r15,4), %xmm0
	movdqu	-32(%r14,%r15,4), %xmm1
	movdqu	-16(%rbx,%r15,4), %xmm2
	movdqu	-32(%rbx,%r15,4), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rbx,%r15,4)
	movdqu	%xmm3, -32(%rbx,%r15,4)
	movl	$8, %eax
	testq	%r10, %r10
	jne	.LBB0_18
	jmp	.LBB0_20
.LBB0_17:                               #   in Loop: Header=BB0_13 Depth=1
	xorl	%eax, %eax
	testq	%r10, %r10
	je	.LBB0_20
.LBB0_18:                               # %vector.body46.preheader.new
                                        #   in Loop: Header=BB0_13 Depth=1
	negq	%rax
	.p2align	4, 0x90
.LBB0_19:                               # %vector.body46
                                        #   Parent Loop BB0_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rcx,%rax,4), %xmm0
	movdqu	-16(%rcx,%rax,4), %xmm1
	movdqu	(%rdi,%rax,4), %xmm2
	movdqu	-16(%rdi,%rax,4), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, (%rdi,%rax,4)
	movdqu	%xmm3, -16(%rdi,%rax,4)
	movdqu	-32(%rcx,%rax,4), %xmm0
	movdqu	-48(%rcx,%rax,4), %xmm1
	movdqu	-32(%rdi,%rax,4), %xmm2
	movdqu	-48(%rdi,%rax,4), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -32(%rdi,%rax,4)
	movdqu	%xmm3, -48(%rdi,%rax,4)
	addq	$-16, %rax
	cmpq	%rax, %rdx
	jne	.LBB0_19
.LBB0_20:                               # %middle.block47
                                        #   in Loop: Header=BB0_13 Depth=1
	cmpq	%r11, %r12
	movq	%r8, %rax
	je	.LBB0_23
	.p2align	4, 0x90
.LBB0_21:                               # %scalar.ph48.preheader
                                        #   in Loop: Header=BB0_13 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB0_22:                               # %scalar.ph48
                                        #   Parent Loop BB0_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-8(%r14,%rax,4), %ebp
	addl	%ebp, -8(%rbx,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB0_22
.LBB0_23:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_13 Depth=1
	incl	%esi
	cmpl	$1000, %esi             # imm = 0x3E8
	jne	.LBB0_13
# BB#24:                                # %.us-lcssa.us.loopexit
	movl	(%rbx), %esi
	jmp	.LBB0_27
.LBB0_26:
	xorl	%esi, %esi
.LBB0_27:                               # %.us-lcssa.us
	movl	-4(%rbx,%r15,4), %edx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movq	%r14, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d %d\n"
	.size	.L.str, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
