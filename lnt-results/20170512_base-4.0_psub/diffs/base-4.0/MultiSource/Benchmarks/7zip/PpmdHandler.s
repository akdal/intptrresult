	.text
	.file	"PpmdHandler.bc"
	.globl	_ZN8NArchive5NPpmd5CItem10ReadHeaderEP19ISequentialInStreamRj
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd5CItem10ReadHeaderEP19ISequentialInStreamRj,@function
_ZN8NArchive5NPpmd5CItem10ReadHeaderEP19ISequentialInStreamRj: # @_ZN8NArchive5NPpmd5CItem10ReadHeaderEP19ISequentialInStreamRj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	leaq	16(%rsp), %rsi
	movl	$16, %edx
	movq	%r15, %rdi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	testl	%eax, %eax
	jne	.LBB0_34
# BB#1:
	movl	$1, %eax
	cmpl	$-2069057649, 16(%rsp)  # imm = 0x84ACAF8F
	jne	.LBB0_34
# BB#2:
	movl	20(%rsp), %ecx
	movl	%ecx, (%rbx)
	movl	28(%rsp), %ecx
	movl	%ecx, 4(%rbx)
	movzwl	24(%rsp), %ecx
	movl	%ecx, %edx
	andl	$15, %edx
	incl	%edx
	movl	%edx, 24(%rbx)
	movl	%ecx, %edx
	shrl	$4, %edx
	movzbl	%dl, %edx
	incl	%edx
	movl	%edx, 28(%rbx)
	movl	%ecx, %edx
	shrl	$12, %edx
	movl	%edx, 32(%rbx)
	movzwl	26(%rsp), %edx
	movl	%edx, %esi
	shrl	$14, %esi
	movl	%esi, 36(%rbx)
	cmpl	$3, %esi
	je	.LBB0_34
# BB#3:
	movl	%edx, %ebp
	andl	$16383, %ebp            # imm = 0x3FFF
	testw	%cx, %cx
	cmovnsl	%edx, %ebp
	cmpl	$512, %ebp              # imm = 0x200
	ja	.LBB0_34
# BB#4:
	leal	1(%rbp), %eax
	movl	20(%rbx), %r13d
	cmpl	%eax, %r13d
	jg	.LBB0_31
# BB#5:
	leal	2(%rbp), %eax
	cmpl	%r13d, %eax
	je	.LBB0_31
# BB#6:
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	%eax, %edi
	callq	_Znam
	movq	%rax, %r12
	testl	%r13d, %r13d
	jle	.LBB0_7
# BB#8:                                 # %.preheader.i.i
	leaq	16(%rbx), %r13
	movslq	16(%rbx), %r9
	testq	%r9, %r9
	movq	8(%rbx), %rdi
	jle	.LBB0_28
# BB#9:                                 # %.lr.ph.preheader.i.i
	cmpl	$31, %r9d
	jbe	.LBB0_10
# BB#17:                                # %min.iters.checked
	movq	%r9, %rcx
	andq	$-32, %rcx
	je	.LBB0_10
# BB#18:                                # %vector.memcheck
	leaq	(%rdi,%r9), %rdx
	cmpq	%rdx, %r12
	jae	.LBB0_20
# BB#19:                                # %vector.memcheck
	leaq	(%r12,%r9), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_20
.LBB0_10:
	xorl	%ecx, %ecx
.LBB0_11:                               # %.lr.ph.i.i.preheader
	movl	%r9d, %esi
	subl	%ecx, %esi
	leaq	-1(%r9), %r8
	subq	%rcx, %r8
	andq	$7, %rsi
	je	.LBB0_14
# BB#12:                                # %.lr.ph.i.i.prol.preheader
	negq	%rsi
.LBB0_13:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r12,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_13
.LBB0_14:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB0_29
# BB#15:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %r9
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB0_16:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r9
	jne	.LBB0_16
	jmp	.LBB0_29
.LBB0_7:                                # %._crit_edge17.i.i
	leaq	16(%rbx), %r13
	jmp	.LBB0_30
.LBB0_28:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB0_30
.LBB0_29:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB0_30:
	movq	%r12, 8(%rbx)
	movslq	(%r13), %rax
	movb	$0, (%r12,%rax)
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 20(%rbx)
.LBB0_31:                               # %_ZN11CStringBaseIcE9GetBufferEi.exit
	movq	8(%rbx), %r12
	movl	%ebp, %r13d
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	movb	$0, (%r12,%r13)
	addl	$16, %ebp
	movl	%ebp, (%r14)
	movq	8(%rbx), %rbp
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	movl	$-1, %edx
	movabsq	$4294967296, %rdi       # imm = 0x100000000
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB0_32:                               # =>This Inner Loop Header: Depth=1
	addq	%rdi, %rsi
	incl	%edx
	cmpb	$0, (%rcx)
	leaq	1(%rcx), %rcx
	jne	.LBB0_32
# BB#33:                                # %_ZN11CStringBaseIcE13ReleaseBufferEv.exit
	sarq	$32, %rsi
	movb	$0, (%rbp,%rsi)
	movl	%edx, 16(%rbx)
.LBB0_34:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_20:                               # %vector.body.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_21
# BB#22:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
.LBB0_23:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%r12,%rdx)
	movups	%xmm1, 16(%r12,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB0_23
	jmp	.LBB0_24
.LBB0_21:
	xorl	%edx, %edx
.LBB0_24:                               # %vector.body.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB0_27
# BB#25:                                # %vector.body.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%r12,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB0_26:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB0_26
.LBB0_27:                               # %middle.block
	cmpq	%rcx, %r9
	jne	.LBB0_11
	jmp	.LBB0_29
.Lfunc_end0:
	.size	_ZN8NArchive5NPpmd5CItem10ReadHeaderEP19ISequentialInStreamRj, .Lfunc_end0-_ZN8NArchive5NPpmd5CItem10ReadHeaderEP19ISequentialInStreamRj
	.cfi_endproc

	.globl	_ZN8NArchive5NPpmd8CHandler21GetNumberOfPropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandler21GetNumberOfPropertiesEPj,@function
_ZN8NArchive5NPpmd8CHandler21GetNumberOfPropertiesEPj: # @_ZN8NArchive5NPpmd8CHandler21GetNumberOfPropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$4, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	_ZN8NArchive5NPpmd8CHandler21GetNumberOfPropertiesEPj, .Lfunc_end1-_ZN8NArchive5NPpmd8CHandler21GetNumberOfPropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive5NPpmd8CHandler15GetPropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandler15GetPropertyInfoEjPPwPjPt,@function
_ZN8NArchive5NPpmd8CHandler15GetPropertyInfoEjPPwPjPt: # @_ZN8NArchive5NPpmd8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$3, %esi
	ja	.LBB2_2
# BB#1:
	movl	%esi, %eax
	shlq	$4, %rax
	movl	_ZN8NArchive5NPpmd6kPropsE+8(%rax), %esi
	movl	%esi, (%rcx)
	movzwl	_ZN8NArchive5NPpmd6kPropsE+12(%rax), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB2_2:
	retq
.Lfunc_end2:
	.size	_ZN8NArchive5NPpmd8CHandler15GetPropertyInfoEjPPwPjPt, .Lfunc_end2-_ZN8NArchive5NPpmd8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive5NPpmd8CHandler28GetNumberOfArchivePropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandler28GetNumberOfArchivePropertiesEPj,@function
_ZN8NArchive5NPpmd8CHandler28GetNumberOfArchivePropertiesEPj: # @_ZN8NArchive5NPpmd8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$0, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	_ZN8NArchive5NPpmd8CHandler28GetNumberOfArchivePropertiesEPj, .Lfunc_end3-_ZN8NArchive5NPpmd8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive5NPpmd8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandler22GetArchivePropertyInfoEjPPwPjPt,@function
_ZN8NArchive5NPpmd8CHandler22GetArchivePropertyInfoEjPPwPjPt: # @_ZN8NArchive5NPpmd8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147467263, %eax      # imm = 0x80004001
	retq
.Lfunc_end4:
	.size	_ZN8NArchive5NPpmd8CHandler22GetArchivePropertyInfoEjPPwPjPt, .Lfunc_end4-_ZN8NArchive5NPpmd8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive5NPpmd8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandler18GetArchivePropertyEjP14tagPROPVARIANT,@function
_ZN8NArchive5NPpmd8CHandler18GetArchivePropertyEjP14tagPROPVARIANT: # @_ZN8NArchive5NPpmd8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -16
	movq	%rdx, %rbx
	movl	$0, (%rsp)
	cmpl	$44, %esi
	jne	.LBB5_3
# BB#1:
	cmpb	$0, 80(%rdi)
	je	.LBB5_3
# BB#2:
	movq	72(%rdi), %rsi
.Ltmp0:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp1:
.LBB5_3:
.Ltmp2:
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp3:
# BB#4:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB5_5:
.Ltmp4:
	movq	%rax, %rbx
.Ltmp5:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp6:
# BB#6:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB5_7:
.Ltmp7:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN8NArchive5NPpmd8CHandler18GetArchivePropertyEjP14tagPROPVARIANT, .Lfunc_end5-_ZN8NArchive5NPpmd8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp5-.Ltmp3           #   Call between .Ltmp3 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end6:
	.size	__clang_call_terminate, .Lfunc_end6-__clang_call_terminate

	.text
	.globl	_ZN8NArchive5NPpmd8CHandler16GetNumberOfItemsEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandler16GetNumberOfItemsEPj,@function
_ZN8NArchive5NPpmd8CHandler16GetNumberOfItemsEPj: # @_ZN8NArchive5NPpmd8CHandler16GetNumberOfItemsEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	_ZN8NArchive5NPpmd8CHandler16GetNumberOfItemsEPj, .Lfunc_end7-_ZN8NArchive5NPpmd8CHandler16GetNumberOfItemsEPj
	.cfi_endproc

	.globl	_ZN8NArchive5NPpmd8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandler11GetPropertyEjjP14tagPROPVARIANT,@function
_ZN8NArchive5NPpmd8CHandler11GetPropertyEjjP14tagPROPVARIANT: # @_ZN8NArchive5NPpmd8CHandler11GetPropertyEjjP14tagPROPVARIANT
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 160
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %r12
	movl	$0, 32(%rsp)
	addl	$-3, %edx
	cmpl	$19, %edx
	ja	.LBB8_214
# BB#1:
	jmpq	*.LJTI8_0(,%rdx,8)
.LBB8_2:
	addq	$32, %r12
.Ltmp44:
	leaq	88(%rsp), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp45:
# BB#3:
	movq	88(%rsp), %rsi
.Ltmp46:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp47:
# BB#4:
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_214
# BB#5:
	callq	_ZdaPv
	jmp	.LBB8_214
.LBB8_12:
	movl	24(%r12), %esi
.Ltmp37:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp38:
	jmp	.LBB8_214
.LBB8_13:
	cmpb	$0, 80(%r12)
	je	.LBB8_214
# BB#14:
	movq	72(%r12), %rsi
.Ltmp35:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp36:
	jmp	.LBB8_214
.LBB8_9:
	movl	28(%r12), %edi
.Ltmp39:
	leaq	48(%rsp), %rsi
	callq	_ZN8NWindows5NTime17DosTimeToFileTimeEjR9_FILETIME
.Ltmp40:
# BB#10:
	testb	%al, %al
	je	.LBB8_214
# BB#11:
.Ltmp41:
	leaq	32(%rsp), %rdi
	leaq	48(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp42:
	jmp	.LBB8_214
.LBB8_15:                               # %_Z11MyStringLenIcEiPKT_.exit.i
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp8:
	movl	$5, %edi
	callq	_Znam
.Ltmp9:
# BB#16:                                # %.noexc
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movq	%rax, (%rsp)
	movl	$5, 12(%rsp)
	movb	$80, (%rax)
	movb	$80, 1(%rax)
	movb	$77, 2(%rax)
	movb	$100, 3(%rax)
	movb	$0, 4(%rax)
	movl	$4, 8(%rsp)
	movl	56(%r12), %eax
	addl	$65, %eax
.Ltmp10:
	movsbl	%al, %esi
	movq	%rsp, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp11:
# BB#17:                                # %_Z11MyStringLenIcEiPKT_.exit.i.i
	movl	48(%r12), %r15d
	movl	8(%rsp), %ebx
	movl	12(%rsp), %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	cmpl	$2, %eax
	jg	.LBB8_46
# BB#18:
	leal	-1(%rax), %ecx
	cmpl	$8, %r13d
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %r13d
	jl	.LBB8_20
# BB#19:                                # %select.true.sink
	movl	%r13d, %edx
	shrl	$31, %edx
	addl	%r13d, %edx
	sarl	%edx
.LBB8_20:                               # %select.end
	addl	%edx, %ecx
	movl	$3, %esi
	subl	%eax, %esi
	cmpl	$2, %ecx
	cmovgel	%edx, %esi
	leal	1(%rsi,%r13), %ebp
	cmpl	%r13d, %ebp
	je	.LBB8_46
# BB#21:
	addl	%r13d, %esi
	movslq	%ebp, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp12:
	callq	_Znam
	movq	%rax, %r14
.Ltmp13:
# BB#22:                                # %.noexc106
	testl	%r13d, %r13d
	jle	.LBB8_45
# BB#23:                                # %.preheader.i.i95
	movq	(%rsp), %rdi
	testl	%ebx, %ebx
	jle	.LBB8_43
# BB#24:                                # %.lr.ph.preheader.i.i96
	movslq	%ebx, %rax
	cmpl	$31, %ebx
	jbe	.LBB8_25
# BB#32:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB8_25
# BB#33:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r14
	jae	.LBB8_35
# BB#34:                                # %vector.memcheck
	leaq	(%r14,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB8_35
.LBB8_25:
	xorl	%ecx, %ecx
.LBB8_26:                               # %.lr.ph.i.i101.preheader
	subl	%ecx, %ebx
	leaq	-1(%rax), %rsi
	subq	%rcx, %rsi
	andq	$7, %rbx
	je	.LBB8_29
# BB#27:                                # %.lr.ph.i.i101.prol.preheader
	negq	%rbx
	.p2align	4, 0x90
.LBB8_28:                               # %.lr.ph.i.i101.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r14,%rcx)
	incq	%rcx
	incq	%rbx
	jne	.LBB8_28
.LBB8_29:                               # %.lr.ph.i.i101.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB8_44
# BB#30:                                # %.lr.ph.i.i101.preheader.new
	subq	%rcx, %rax
	leaq	7(%r14,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB8_31:                               # %.lr.ph.i.i101
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB8_31
	jmp	.LBB8_44
.LBB8_43:                               # %._crit_edge.i.i97
	testq	%rdi, %rdi
	je	.LBB8_45
.LBB8_44:                               # %._crit_edge.thread.i.i103
	callq	_ZdaPv
	movl	8(%rsp), %ebx
.LBB8_45:                               # %._crit_edge17.i.i94
	movq	%r14, (%rsp)
	movslq	%ebx, %rax
	movb	$0, (%r14,%rax)
	movl	%ebp, 12(%rsp)
.LBB8_46:                               # %.noexc14
	movq	(%rsp), %rax
	movslq	%ebx, %rcx
	movb	$58, (%rax,%rcx)
	movb	$111, 1(%rax,%rcx)
	movb	$0, 2(%rax,%rcx)
	addl	$2, 8(%rsp)
.Ltmp14:
	leaq	48(%rsp), %rbp
	movl	%r15d, %edi
	movq	%rbp, %rsi
	callq	_Z21ConvertUInt32ToStringjPc
.Ltmp15:
# BB#47:                                # %.noexc15.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_48:                               # %.noexc15
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	leaq	1(%rbx), %rax
	cmpb	$0, 48(%rsp,%rbx)
	jne	.LBB8_48
# BB#49:                                # %_Z11MyStringLenIcEiPKT_.exit.i5.i
	movl	8(%rsp), %r13d
	movl	12(%rsp), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	cmpl	%ebx, %eax
	jg	.LBB8_78
# BB#50:
	decl	%eax
	cmpl	$8, %r14d
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %r14d
	jl	.LBB8_52
# BB#51:                                # %select.true.sink764
	movl	%r14d, %ecx
	shrl	$31, %ecx
	addl	%r14d, %ecx
	sarl	%ecx
.LBB8_52:                               # %select.end763
	movl	%ebx, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%ebx, %eax
	cmovgel	%ecx, %edx
	leal	1(%rdx,%r14), %eax
	cmpl	%r14d, %eax
	je	.LBB8_78
# BB#53:
	addl	%r14d, %edx
	movl	%eax, 28(%rsp)          # 4-byte Spill
	cltq
	cmpl	$-1, %edx
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp16:
	callq	_Znam
	movq	%rax, %r15
.Ltmp17:
# BB#54:                                # %.noexc20
	testl	%r14d, %r14d
	jle	.LBB8_77
# BB#55:                                # %.preheader.i.i
	movq	(%rsp), %rdi
	testl	%r13d, %r13d
	jle	.LBB8_75
# BB#56:                                # %.lr.ph.preheader.i.i
	movslq	%r13d, %r9
	cmpl	$31, %r13d
	jbe	.LBB8_57
# BB#64:                                # %min.iters.checked154
	movq	%r9, %rcx
	andq	$-32, %rcx
	je	.LBB8_57
# BB#65:                                # %vector.memcheck165
	leaq	(%rdi,%r9), %rdx
	cmpq	%rdx, %r15
	jae	.LBB8_67
# BB#66:                                # %vector.memcheck165
	leaq	(%r15,%r9), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB8_67
.LBB8_57:
	xorl	%ecx, %ecx
.LBB8_58:                               # %.lr.ph.i.i.preheader
	subl	%ecx, %r13d
	leaq	-1(%r9), %rsi
	subq	%rcx, %rsi
	andq	$7, %r13
	je	.LBB8_61
# BB#59:                                # %.lr.ph.i.i.prol.preheader
	negq	%r13
	.p2align	4, 0x90
.LBB8_60:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r15,%rcx)
	incq	%rcx
	incq	%r13
	jne	.LBB8_60
.LBB8_61:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB8_76
# BB#62:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %r9
	leaq	7(%r15,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB8_63:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r9
	jne	.LBB8_63
	jmp	.LBB8_76
.LBB8_75:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB8_77
.LBB8_76:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%rsp), %r13d
.LBB8_77:                               # %._crit_edge17.i.i
	movq	%r15, (%rsp)
	movslq	%r13d, %rax
	movb	$0, (%r15,%rax)
	movl	28(%rsp), %eax          # 4-byte Reload
	movl	%eax, 12(%rsp)
.LBB8_78:                               # %.noexc16
	movslq	%r13d, %rax
	addq	(%rsp), %rax
	.p2align	4, 0x90
.LBB8_79:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %ecx
	incq	%rbp
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB8_79
# BB#80:                                # %_Z11MyStringLenIcEiPKT_.exit.i.i23
	movl	8(%rsp), %r13d
	leal	(%r13,%rbx), %r15d
	movl	%r15d, 8(%rsp)
	movl	52(%r12), %edi
	movl	12(%rsp), %r14d
	movl	%r14d, %eax
	subl	%r15d, %eax
	cmpl	$4, %eax
	jg	.LBB8_109
# BB#81:
	leal	-1(%rax), %ecx
	cmpl	$8, %r14d
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %r14d
	jl	.LBB8_83
# BB#82:                                # %select.true.sink794
	movl	%r14d, %edx
	shrl	$31, %edx
	addl	%r14d, %edx
	sarl	%edx
.LBB8_83:                               # %select.end793
	addl	%edx, %ecx
	movl	$5, %esi
	subl	%eax, %esi
	cmpl	$4, %ecx
	cmovgel	%edx, %esi
	leal	1(%rsi,%r14), %eax
	cmpl	%r14d, %eax
	je	.LBB8_109
# BB#84:
	movl	%edi, 28(%rsp)          # 4-byte Spill
	addl	%r14d, %esi
	movl	%eax, 76(%rsp)          # 4-byte Spill
	cltq
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp18:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp19:
# BB#85:                                # %.noexc124
	testl	%r14d, %r14d
	jle	.LBB8_108
# BB#86:                                # %.preheader.i.i113
	movq	(%rsp), %rdi
	testl	%r15d, %r15d
	jle	.LBB8_106
# BB#87:                                # %.lr.ph.preheader.i.i114
	movslq	%r15d, %r9
	cmpl	$31, %r15d
	jbe	.LBB8_88
# BB#95:                                # %min.iters.checked181
	movq	%r9, %rax
	andq	$-32, %rax
	je	.LBB8_88
# BB#96:                                # %vector.memcheck192
	leaq	(%rdi,%r9), %rcx
	cmpq	%rcx, %rbp
	jae	.LBB8_98
# BB#97:                                # %vector.memcheck192
	leaq	(%rbp,%r9), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB8_98
.LBB8_88:
	xorl	%eax, %eax
.LBB8_89:                               # %.lr.ph.i.i119.preheader
	subl	%eax, %r15d
	decq	%r9
	subq	%rax, %r9
	testb	$7, %r15b
	je	.LBB8_92
# BB#90:                                # %.lr.ph.i.i119.prol.preheader
	leal	(%rbx,%r13), %ecx
	subl	%eax, %ecx
	andl	$7, %ecx
	negq	%rcx
	.p2align	4, 0x90
.LBB8_91:                               # %.lr.ph.i.i119.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rax), %edx
	movb	%dl, (%rbp,%rax)
	incq	%rax
	incq	%rcx
	jne	.LBB8_91
.LBB8_92:                               # %.lr.ph.i.i119.prol.loopexit
	cmpq	$7, %r9
	jb	.LBB8_107
# BB#93:                                # %.lr.ph.i.i119.preheader.new
	addl	%r13d, %ebx
	movslq	%ebx, %rcx
	subq	%rax, %rcx
	leaq	7(%rbp,%rax), %rdx
	leaq	7(%rdi,%rax), %rax
	.p2align	4, 0x90
.LBB8_94:                               # %.lr.ph.i.i119
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rax), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rax), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rax), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rax), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rax), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rax), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rax), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rax), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rax
	addq	$-8, %rcx
	jne	.LBB8_94
	jmp	.LBB8_107
.LBB8_106:                              # %._crit_edge.i.i115
	testq	%rdi, %rdi
	je	.LBB8_108
.LBB8_107:                              # %._crit_edge.thread.i.i121
	callq	_ZdaPv
	movl	8(%rsp), %r15d
.LBB8_108:                              # %._crit_edge17.i.i112
	movq	%rbp, (%rsp)
	movslq	%r15d, %rax
	movb	$0, (%rbp,%rax)
	movl	76(%rsp), %eax          # 4-byte Reload
	movl	%eax, 12(%rsp)
	movl	28(%rsp), %edi          # 4-byte Reload
.LBB8_109:                              # %.noexc32
	movq	(%rsp), %rax
	movslq	%r15d, %rcx
	movb	$58, (%rax,%rcx)
	movb	$109, 1(%rax,%rcx)
	movb	$101, 2(%rax,%rcx)
	movb	$109, 3(%rax,%rcx)
	movb	$0, 4(%rax,%rcx)
	addl	$4, 8(%rsp)
.Ltmp20:
	leaq	48(%rsp), %rbx
	movq	%rbx, %rsi
	callq	_Z21ConvertUInt32ToStringjPc
.Ltmp21:
# BB#110:                               # %.noexc33.preheader
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB8_111:                              # %.noexc33
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpb	$0, (%rbx)
	leaq	1(%rbx), %rbx
	jne	.LBB8_111
# BB#112:                               # %_Z11MyStringLenIcEiPKT_.exit.i5.i29
	movl	8(%rsp), %ebx
	movl	12(%rsp), %r15d
	movl	%r15d, %eax
	subl	%ebx, %eax
	cmpl	%ebp, %eax
	jg	.LBB8_141
# BB#113:
	decl	%eax
	cmpl	$8, %r15d
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %r15d
	jl	.LBB8_115
# BB#114:                               # %select.true.sink828
	movl	%r15d, %ecx
	shrl	$31, %ecx
	addl	%r15d, %ecx
	sarl	%ecx
.LBB8_115:                              # %select.end827
	addl	%ecx, %eax
	movl	%ebx, %edx
	subl	%r15d, %edx
	cmpl	%ebp, %eax
	leal	1(%rdx,%rbp), %eax
	cmovgel	%ecx, %eax
	leal	1(%rax,%r15), %r14d
	cmpl	%r15d, %r14d
	je	.LBB8_141
# BB#116:
	addl	%r15d, %eax
	movslq	%r14d, %rcx
	cmpl	$-1, %eax
	movq	$-1, %rdi
	cmovgeq	%rcx, %rdi
.Ltmp22:
	callq	_Znam
	movq	%rax, %r13
.Ltmp23:
# BB#117:                               # %.noexc52
	testl	%r15d, %r15d
	jle	.LBB8_140
# BB#118:                               # %.preheader.i.i41
	movq	(%rsp), %rdi
	testl	%ebx, %ebx
	jle	.LBB8_138
# BB#119:                               # %.lr.ph.preheader.i.i42
	movslq	%ebx, %rax
	cmpl	$31, %ebx
	jbe	.LBB8_120
# BB#127:                               # %min.iters.checked208
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB8_120
# BB#128:                               # %vector.memcheck219
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r13
	jae	.LBB8_130
# BB#129:                               # %vector.memcheck219
	leaq	(%r13,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB8_130
.LBB8_120:
	xorl	%ecx, %ecx
.LBB8_121:                              # %.lr.ph.i.i47.preheader
	subl	%ecx, %ebx
	leaq	-1(%rax), %rsi
	subq	%rcx, %rsi
	andq	$7, %rbx
	je	.LBB8_124
# BB#122:                               # %.lr.ph.i.i47.prol.preheader
	negq	%rbx
	.p2align	4, 0x90
.LBB8_123:                              # %.lr.ph.i.i47.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r13,%rcx)
	incq	%rcx
	incq	%rbx
	jne	.LBB8_123
.LBB8_124:                              # %.lr.ph.i.i47.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB8_139
# BB#125:                               # %.lr.ph.i.i47.preheader.new
	subq	%rcx, %rax
	leaq	7(%r13,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB8_126:                              # %.lr.ph.i.i47
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB8_126
	jmp	.LBB8_139
.LBB8_138:                              # %._crit_edge.i.i43
	testq	%rdi, %rdi
	je	.LBB8_140
.LBB8_139:                              # %._crit_edge.thread.i.i49
	callq	_ZdaPv
	movl	8(%rsp), %ebx
.LBB8_140:                              # %._crit_edge17.i.i40
	movq	%r13, (%rsp)
	movslq	%ebx, %rax
	movb	$0, (%r13,%rax)
	movl	%r14d, 12(%rsp)
.LBB8_141:                              # %.noexc34
	leaq	48(%rsp), %rax
	movslq	%ebx, %rcx
	addq	(%rsp), %rcx
	.p2align	4, 0x90
.LBB8_142:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	incq	%rax
	movb	%dl, (%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB8_142
# BB#143:
	addl	%ebp, 8(%rsp)
.Ltmp24:
	movq	%rsp, %rdi
	movl	$109, %esi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp25:
# BB#144:
	cmpl	$8, 56(%r12)
	jb	.LBB8_210
# BB#145:
	movl	60(%r12), %r12d
	testl	%r12d, %r12d
	je	.LBB8_210
# BB#146:                               # %.preheader.preheader
	movl	8(%rsp), %ebx
	movl	12(%rsp), %ebp
	movl	%ebp, %eax
	subl	%ebx, %eax
	cmpl	$2, %eax
	jg	.LBB8_175
# BB#147:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebp
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebp
	jl	.LBB8_149
# BB#148:                               # %select.true.sink864
	movl	%ebp, %edx
	shrl	$31, %edx
	addl	%ebp, %edx
	sarl	%edx
.LBB8_149:                              # %select.end863
	addl	%edx, %ecx
	movl	$3, %esi
	subl	%eax, %esi
	cmpl	$2, %ecx
	cmovgel	%edx, %esi
	leal	1(%rsi,%rbp), %r15d
	cmpl	%ebp, %r15d
	je	.LBB8_175
# BB#150:
	addl	%ebp, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp26:
	callq	_Znam
	movq	%rax, %r14
.Ltmp27:
# BB#151:                               # %.noexc142
	testl	%ebp, %ebp
	jle	.LBB8_174
# BB#152:                               # %.preheader.i.i131
	movq	(%rsp), %rdi
	testl	%ebx, %ebx
	jle	.LBB8_172
# BB#153:                               # %.lr.ph.preheader.i.i132
	movslq	%ebx, %rax
	cmpl	$31, %ebx
	jbe	.LBB8_154
# BB#161:                               # %min.iters.checked235
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB8_154
# BB#162:                               # %vector.memcheck246
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r14
	jae	.LBB8_164
# BB#163:                               # %vector.memcheck246
	leaq	(%r14,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB8_164
.LBB8_154:
	xorl	%ecx, %ecx
.LBB8_155:                              # %.lr.ph.i.i137.preheader
	subl	%ecx, %ebx
	leaq	-1(%rax), %rsi
	subq	%rcx, %rsi
	andq	$7, %rbx
	je	.LBB8_158
# BB#156:                               # %.lr.ph.i.i137.prol.preheader
	negq	%rbx
.LBB8_157:                              # %.lr.ph.i.i137.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r14,%rcx)
	incq	%rcx
	incq	%rbx
	jne	.LBB8_157
.LBB8_158:                              # %.lr.ph.i.i137.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB8_173
# BB#159:                               # %.lr.ph.i.i137.preheader.new
	subq	%rcx, %rax
	leaq	7(%r14,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB8_160:                              # %.lr.ph.i.i137
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB8_160
	jmp	.LBB8_173
.LBB8_172:                              # %._crit_edge.i.i133
	testq	%rdi, %rdi
	je	.LBB8_174
.LBB8_173:                              # %._crit_edge.thread.i.i139
	callq	_ZdaPv
	movl	8(%rsp), %ebx
.LBB8_174:                              # %._crit_edge17.i.i130
	movq	%r14, (%rsp)
	movslq	%ebx, %rax
	movb	$0, (%r14,%rax)
	movl	%r15d, 12(%rsp)
.LBB8_175:                              # %.noexc65
	movq	(%rsp), %rax
	movslq	%ebx, %rcx
	movb	$58, (%rax,%rcx)
	movb	$114, 1(%rax,%rcx)
	movb	$0, 2(%rax,%rcx)
	addl	$2, 8(%rsp)
.Ltmp28:
	leaq	48(%rsp), %rbp
	movl	%r12d, %edi
	movq	%rbp, %rsi
	callq	_Z21ConvertUInt32ToStringjPc
.Ltmp29:
# BB#176:                               # %.noexc66.preheader
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB8_177:                              # %.noexc66
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebx
	cmpb	$0, (%rbp)
	leaq	1(%rbp), %rbp
	jne	.LBB8_177
# BB#178:                               # %_Z11MyStringLenIcEiPKT_.exit.i5.i62
	movl	8(%rsp), %r13d
	movl	12(%rsp), %ebp
	movl	%ebp, %eax
	subl	%r13d, %eax
	cmpl	%ebx, %eax
	jg	.LBB8_207
# BB#179:
	decl	%eax
	cmpl	$8, %ebp
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebp
	jl	.LBB8_181
# BB#180:                               # %select.true.sink897
	movl	%ebp, %ecx
	shrl	$31, %ecx
	addl	%ebp, %ecx
	sarl	%ecx
.LBB8_181:                              # %select.end896
	addl	%ecx, %eax
	movl	%r13d, %edx
	subl	%ebp, %edx
	cmpl	%ebx, %eax
	leal	1(%rdx,%rbx), %eax
	cmovgel	%ecx, %eax
	leal	1(%rax,%rbp), %r15d
	cmpl	%ebp, %r15d
	je	.LBB8_207
# BB#182:
	addl	%ebp, %eax
	movslq	%r15d, %rcx
	cmpl	$-1, %eax
	movq	$-1, %rdi
	cmovgeq	%rcx, %rdi
.Ltmp30:
	callq	_Znam
	movq	%rax, %r12
.Ltmp31:
# BB#183:                               # %.noexc85
	testl	%ebp, %ebp
	jle	.LBB8_206
# BB#184:                               # %.preheader.i.i74
	movq	(%rsp), %rdi
	testl	%r13d, %r13d
	jle	.LBB8_204
# BB#185:                               # %.lr.ph.preheader.i.i75
	movslq	%r13d, %r8
	cmpl	$31, %r13d
	jbe	.LBB8_186
# BB#193:                               # %min.iters.checked262
	movq	%r8, %rcx
	andq	$-32, %rcx
	je	.LBB8_186
# BB#194:                               # %vector.memcheck273
	leaq	(%rdi,%r8), %rdx
	cmpq	%rdx, %r12
	jae	.LBB8_196
# BB#195:                               # %vector.memcheck273
	leaq	(%r12,%r8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB8_196
.LBB8_186:
	xorl	%ecx, %ecx
.LBB8_187:                              # %.lr.ph.i.i80.preheader
	subl	%ecx, %r13d
	leaq	-1(%r8), %rsi
	subq	%rcx, %rsi
	andq	$7, %r13
	je	.LBB8_190
# BB#188:                               # %.lr.ph.i.i80.prol.preheader
	negq	%r13
.LBB8_189:                              # %.lr.ph.i.i80.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r12,%rcx)
	incq	%rcx
	incq	%r13
	jne	.LBB8_189
.LBB8_190:                              # %.lr.ph.i.i80.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB8_205
# BB#191:                               # %.lr.ph.i.i80.preheader.new
	subq	%rcx, %r8
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB8_192:                              # %.lr.ph.i.i80
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r8
	jne	.LBB8_192
	jmp	.LBB8_205
.LBB8_204:                              # %._crit_edge.i.i76
	testq	%rdi, %rdi
	je	.LBB8_206
.LBB8_205:                              # %._crit_edge.thread.i.i82
	callq	_ZdaPv
	movl	8(%rsp), %r13d
.LBB8_206:                              # %._crit_edge17.i.i73
	movq	%r12, (%rsp)
	movslq	%r13d, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%rsp)
.LBB8_207:                              # %.noexc67
	leaq	48(%rsp), %rax
	movslq	%r13d, %rcx
	addq	(%rsp), %rcx
	.p2align	4, 0x90
.LBB8_208:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	incq	%rax
	movb	%dl, (%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB8_208
# BB#209:                               # %_ZN8NArchive5NPpmdL12UIntToStringER11CStringBaseIcEPKcj.exit68
	addl	%ebx, 8(%rsp)
.LBB8_210:
	movq	(%rsp), %rsi
.Ltmp32:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKc
.Ltmp33:
# BB#211:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_213
# BB#212:
	callq	_ZdaPv
.LBB8_213:                              # %_ZN11CStringBaseIcED2Ev.exit87
	movq	80(%rsp), %rbx          # 8-byte Reload
.LBB8_214:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp49:
	leaq	32(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp50:
# BB#215:
.Ltmp55:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp56:
# BB#216:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit89
	xorl	%eax, %eax
.LBB8_225:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_35:                               # %vector.body.preheader
	movl	%ebp, %r8d
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB8_36
# BB#37:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB8_38:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%r14,%rbp)
	movups	%xmm1, 16(%r14,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB8_38
	jmp	.LBB8_39
.LBB8_67:                               # %vector.body150.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB8_68
# BB#69:                                # %vector.body150.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
.LBB8_70:                               # %vector.body150.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%r15,%rdx)
	movups	%xmm1, 16(%r15,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB8_70
	jmp	.LBB8_71
.LBB8_98:                               # %vector.body177.preheader
	leaq	-32(%rax), %r8
	movl	%r8d, %ecx
	shrl	$5, %ecx
	incl	%ecx
	testb	$3, %cl
	je	.LBB8_99
# BB#100:                               # %vector.body177.prol.preheader
	leal	(%rbx,%r13), %edx
	andl	$96, %edx
	addl	$-32, %edx
	shrl	$5, %edx
	incl	%edx
	andl	$3, %edx
	negq	%rdx
	xorl	%esi, %esi
.LBB8_101:                              # %vector.body177.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rsi), %xmm0
	movups	16(%rdi,%rsi), %xmm1
	movups	%xmm0, (%rbp,%rsi)
	movups	%xmm1, 16(%rbp,%rsi)
	addq	$32, %rsi
	incq	%rdx
	jne	.LBB8_101
	jmp	.LBB8_102
.LBB8_130:                              # %vector.body204.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB8_131
# BB#132:                               # %vector.body204.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
.LBB8_133:                              # %vector.body204.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%r13,%rdx)
	movups	%xmm1, 16(%r13,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB8_133
	jmp	.LBB8_134
.LBB8_36:
	xorl	%ebp, %ebp
.LBB8_39:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB8_42
# BB#40:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r14,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB8_41:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB8_41
.LBB8_42:                               # %middle.block
	cmpq	%rcx, %rax
	movl	%r8d, %ebp
	jne	.LBB8_26
	jmp	.LBB8_44
.LBB8_68:
	xorl	%edx, %edx
.LBB8_71:                               # %vector.body150.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB8_74
# BB#72:                                # %vector.body150.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%r15,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB8_73:                               # %vector.body150
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB8_73
.LBB8_74:                               # %middle.block151
	cmpq	%rcx, %r9
	jne	.LBB8_58
	jmp	.LBB8_76
.LBB8_99:
	xorl	%esi, %esi
.LBB8_102:                              # %vector.body177.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB8_105
# BB#103:                               # %vector.body177.preheader.new
	leal	(%rbx,%r13), %ecx
	movslq	%ecx, %rdx
	andq	$-32, %rdx
	subq	%rsi, %rdx
	leaq	112(%rbp,%rsi), %rcx
	leaq	112(%rdi,%rsi), %rsi
.LBB8_104:                              # %vector.body177
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rcx
	subq	$-128, %rsi
	addq	$-128, %rdx
	jne	.LBB8_104
.LBB8_105:                              # %middle.block178
	cmpq	%rax, %r9
	jne	.LBB8_89
	jmp	.LBB8_107
.LBB8_131:
	xorl	%edx, %edx
.LBB8_134:                              # %vector.body204.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB8_137
# BB#135:                               # %vector.body204.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%r13,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB8_136:                              # %vector.body204
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB8_136
.LBB8_137:                              # %middle.block205
	cmpq	%rcx, %rax
	jne	.LBB8_121
	jmp	.LBB8_139
.LBB8_164:                              # %vector.body231.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB8_165
# BB#166:                               # %vector.body231.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB8_167:                              # %vector.body231.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%r14,%rbp)
	movups	%xmm1, 16(%r14,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB8_167
	jmp	.LBB8_168
.LBB8_196:                              # %vector.body258.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB8_197
# BB#198:                               # %vector.body258.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB8_199:                              # %vector.body258.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%r12,%rbp)
	movups	%xmm1, 16(%r12,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB8_199
	jmp	.LBB8_200
.LBB8_165:
	xorl	%ebp, %ebp
.LBB8_168:                              # %vector.body231.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB8_171
# BB#169:                               # %vector.body231.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r14,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB8_170:                              # %vector.body231
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB8_170
.LBB8_171:                              # %middle.block232
	cmpq	%rcx, %rax
	jne	.LBB8_155
	jmp	.LBB8_173
.LBB8_197:
	xorl	%ebp, %ebp
.LBB8_200:                              # %vector.body258.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB8_203
# BB#201:                               # %vector.body258.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r12,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB8_202:                              # %vector.body258
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB8_202
.LBB8_203:                              # %middle.block259
	cmpq	%rcx, %r8
	jne	.LBB8_187
	jmp	.LBB8_205
.LBB8_8:
.Ltmp48:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB8_219
	jmp	.LBB8_220
.LBB8_228:
.Ltmp43:
	jmp	.LBB8_7
.LBB8_229:
.Ltmp57:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	jmp	.LBB8_221
.LBB8_217:
.Ltmp34:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_220
.LBB8_219:
	callq	_ZdaPv
	jmp	.LBB8_220
.LBB8_6:
.Ltmp51:
.LBB8_7:                                # %_ZN11CStringBaseIwED2Ev.exit13
	movq	%rdx, %rbx
	movq	%rax, %rbp
.LBB8_220:                              # %_ZN11CStringBaseIwED2Ev.exit13
.Ltmp52:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp53:
.LBB8_221:                              # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbp, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbp
	cmpl	$2, %ebx
	je	.LBB8_222
# BB#224:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB8_225
.LBB8_222:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbp, (%rax)
.Ltmp58:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp59:
# BB#227:
.LBB8_223:
.Ltmp60:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB8_226:
.Ltmp54:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN8NArchive5NPpmd8CHandler11GetPropertyEjjP14tagPROPVARIANT, .Lfunc_end8-_ZN8NArchive5NPpmd8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_2
	.quad	.LBB8_214
	.quad	.LBB8_214
	.quad	.LBB8_214
	.quad	.LBB8_214
	.quad	.LBB8_13
	.quad	.LBB8_12
	.quad	.LBB8_214
	.quad	.LBB8_214
	.quad	.LBB8_9
	.quad	.LBB8_214
	.quad	.LBB8_214
	.quad	.LBB8_214
	.quad	.LBB8_214
	.quad	.LBB8_214
	.quad	.LBB8_214
	.quad	.LBB8_214
	.quad	.LBB8_214
	.quad	.LBB8_214
	.quad	.LBB8_15
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\253\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Ltmp44-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp51-.Lfunc_begin1   #     jumps to .Ltmp51
	.byte	3                       #   On action: 2
	.long	.Ltmp46-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin1   #     jumps to .Ltmp48
	.byte	3                       #   On action: 2
	.long	.Ltmp37-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp36-.Ltmp37         #   Call between .Ltmp37 and .Ltmp36
	.long	.Ltmp51-.Lfunc_begin1   #     jumps to .Ltmp51
	.byte	3                       #   On action: 2
	.long	.Ltmp39-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp42-.Ltmp39         #   Call between .Ltmp39 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin1   #     jumps to .Ltmp43
	.byte	3                       #   On action: 2
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 5 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp51-.Lfunc_begin1   #     jumps to .Ltmp51
	.byte	3                       #   On action: 2
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp33-.Ltmp10         #   Call between .Ltmp10 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin1   #     jumps to .Ltmp34
	.byte	3                       #   On action: 2
	.long	.Ltmp49-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin1   #     jumps to .Ltmp51
	.byte	3                       #   On action: 2
	.long	.Ltmp55-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin1   #     jumps to .Ltmp57
	.byte	3                       #   On action: 2
	.long	.Ltmp52-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin1   #     jumps to .Ltmp54
	.byte	1                       #   On action: 1
	.long	.Ltmp53-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp58-.Ltmp53         #   Call between .Ltmp53 and .Ltmp58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin1   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Lfunc_end8-.Ltmp59     #   Call between .Ltmp59 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11CStringBaseIcEpLEc,"axG",@progbits,_ZN11CStringBaseIcEpLEc,comdat
	.weak	_ZN11CStringBaseIcEpLEc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEc,@function
_ZN11CStringBaseIcEpLEc:                # @_ZN11CStringBaseIcEpLEc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 64
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB9_28
# BB#1:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB9_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB9_3:                                # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rsi,%rbx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB9_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB9_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB9_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB9_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB9_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB9_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB9_17
.LBB9_7:
	xorl	%ecx, %ecx
.LBB9_8:                                # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB9_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB9_10:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB9_10
.LBB9_11:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB9_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB9_13:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB9_13
	jmp	.LBB9_26
.LBB9_25:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB9_27
.LBB9_26:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB9_27:
	movq	%r12, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%r13)
.LBB9_28:                               # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movb	%r14b, (%rax,%rcx)
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movb	$0, 1(%rax,%rcx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_17:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB9_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r12,%rbx)
	movups	%xmm1, 16(%r12,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB9_20
	jmp	.LBB9_21
.LBB9_18:
	xorl	%ebx, %ebx
.LBB9_21:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB9_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB9_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB9_23
.LBB9_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB9_8
	jmp	.LBB9_26
.Lfunc_end9:
	.size	_ZN11CStringBaseIcEpLEc, .Lfunc_end9-_ZN11CStringBaseIcEpLEc
	.cfi_endproc

	.text
	.globl	_ZN8NArchive5NPpmd8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback,@function
_ZN8NArchive5NPpmd8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback: # @_ZN8NArchive5NPpmd8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	120(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end10:
	.size	_ZN8NArchive5NPpmd8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback, .Lfunc_end10-_ZN8NArchive5NPpmd8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.cfi_endproc

	.globl	_ZN8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream,@function
_ZN8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream: # @_ZN8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
.Ltmp61:
	callq	*48(%rax)
.Ltmp62:
# BB#1:
	leaq	24(%rbx), %rdi
	leaq	64(%rbx), %rdx
.Ltmp63:
	movq	%r14, %rsi
	callq	_ZN8NArchive5NPpmd5CItem10ReadHeaderEP19ISequentialInStreamRj
	movl	%eax, %ebp
.Ltmp64:
# BB#2:
	testl	%ebp, %ebp
	je	.LBB11_3
.LBB11_11:                              # %.thread
	movq	(%rbx), %rax
.Ltmp68:
	movq	%rbx, %rdi
	callq	*48(%rax)
.Ltmp69:
	jmp	.LBB11_14
.LBB11_3:
	testq	%r14, %r14
	je	.LBB11_5
# BB#4:
	movq	(%r14), %rax
.Ltmp70:
	movq	%r14, %rdi
	callq	*8(%rax)
.Ltmp71:
.LBB11_5:                               # %.noexc
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp72:
	callq	*16(%rax)
.Ltmp73:
.LBB11_7:                               # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	%r14, 88(%rbx)
	xorl	%ebp, %ebp
.LBB11_14:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB11_10:
.Ltmp65:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movl	$1, %ebp
.Ltmp66:
	callq	__cxa_end_catch
.Ltmp67:
	jmp	.LBB11_11
.LBB11_8:
.Ltmp74:
	movq	%rdx, %rbp
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %ebp
	je	.LBB11_9
# BB#13:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB11_14
.LBB11_9:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp75:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp76:
# BB#15:
.LBB11_12:
.Ltmp77:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream, .Lfunc_end11-_ZN8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	105                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp61-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp64-.Ltmp61         #   Call between .Ltmp61 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin2   #     jumps to .Ltmp65
	.byte	1                       #   On action: 1
	.long	.Ltmp68-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp73-.Ltmp68         #   Call between .Ltmp68 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin2   #     jumps to .Ltmp74
	.byte	3                       #   On action: 2
	.long	.Ltmp73-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp66-.Ltmp73         #   Call between .Ltmp73 and .Ltmp66
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp74-.Lfunc_begin2   #     jumps to .Ltmp74
	.byte	3                       #   On action: 2
	.long	.Ltmp67-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp75-.Ltmp67         #   Call between .Ltmp67 and .Ltmp75
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp76-.Ltmp75         #   Call between .Ltmp75 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin2   #     jumps to .Ltmp77
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Lfunc_end11-.Ltmp76    #   Call between .Ltmp76 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn8_N8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream,@function
_ZThn8_N8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream: # @_ZThn8_N8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream # TAILCALL
.Lfunc_end12:
	.size	_ZThn8_N8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream, .Lfunc_end12-_ZThn8_N8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN8NArchive5NPpmd8CHandler5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandler5CloseEv,@function
_ZN8NArchive5NPpmd8CHandler5CloseEv:    # @_ZN8NArchive5NPpmd8CHandler5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 16
.Lcfi49:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	$0, 80(%rbx)
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 88(%rbx)
.LBB13_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamE7ReleaseEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end13:
	.size	_ZN8NArchive5NPpmd8CHandler5CloseEv, .Lfunc_end13-_ZN8NArchive5NPpmd8CHandler5CloseEv
	.cfi_endproc

	.globl	_ZN8NArchive5NPpmd13CRangeDecoderC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd13CRangeDecoderC2Ev,@function
_ZN8NArchive5NPpmd13CRangeDecoderC2Ev:  # @_ZN8NArchive5NPpmd13CRangeDecoderC2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZN8NArchive5NPpmdL18Range_GetThresholdEPvj, (%rdi)
	movq	$_ZN8NArchive5NPpmdL12Range_DecodeEPvjj, 8(%rdi)
	movq	$_ZN8NArchive5NPpmdL15Range_DecodeBitEPvj, 16(%rdi)
	retq
.Lfunc_end14:
	.size	_ZN8NArchive5NPpmd13CRangeDecoderC2Ev, .Lfunc_end14-_ZN8NArchive5NPpmd13CRangeDecoderC2Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmdL18Range_GetThresholdEPvj,@function
_ZN8NArchive5NPpmdL18Range_GetThresholdEPvj: # @_ZN8NArchive5NPpmdL18Range_GetThresholdEPvj
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	movl	28(%rdi), %ecx
	xorl	%edx, %edx
	divl	%esi
	movl	%eax, %esi
	movl	%esi, 24(%rdi)
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	retq
.Lfunc_end15:
	.size	_ZN8NArchive5NPpmdL18Range_GetThresholdEPvj, .Lfunc_end15-_ZN8NArchive5NPpmdL18Range_GetThresholdEPvj
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmdL12Range_DecodeEPvjj,@function
_ZN8NArchive5NPpmdL12Range_DecodeEPvjj: # @_ZN8NArchive5NPpmdL12Range_DecodeEPvjj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 32
.Lcfi53:
	.cfi_offset %rbx, -24
.Lcfi54:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %rbx
	movl	24(%rbx), %eax
	imull	%eax, %esi
	movl	32(%rbx), %ecx
	addl	%esi, %ecx
	movl	%ecx, 32(%rbx)
	movl	28(%rbx), %ebp
	subl	%esi, %ebp
	movl	%ebp, 28(%rbx)
	imull	%eax, %edx
	movl	%edx, 24(%rbx)
	jmp	.LBB16_1
	.p2align	4, 0x90
.LBB16_7:                               # %_ZN14CByteInBufWrap8ReadByteEv.exit.i
                                        #   in Loop: Header=BB16_1 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 28(%rbx)
	shll	$8, %edx
	movl	%edx, 24(%rbx)
	shll	$8, %ecx
	movl	%ecx, 32(%rbx)
.LBB16_1:                               # =>This Inner Loop Header: Depth=1
	leal	(%rcx,%rdx), %eax
	xorl	%ecx, %eax
	cmpl	$16777216, %eax         # imm = 0x1000000
	jb	.LBB16_4
# BB#2:                                 #   in Loop: Header=BB16_1 Depth=1
	cmpl	$32767, %edx            # imm = 0x7FFF
	ja	.LBB16_8
# BB#3:                                 #   in Loop: Header=BB16_1 Depth=1
	movl	%ecx, %edx
	negl	%edx
	andl	$32767, %edx            # imm = 0x7FFF
	movl	%edx, 24(%rbx)
.LBB16_4:                               # %.critedge.i
                                        #   in Loop: Header=BB16_1 Depth=1
	shll	$8, %ebp
	movq	40(%rbx), %rdi
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	je	.LBB16_6
# BB#5:                                 #   in Loop: Header=BB16_1 Depth=1
	leaq	1(%rax), %rsi
	movq	%rsi, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB16_7
	.p2align	4, 0x90
.LBB16_6:                               #   in Loop: Header=BB16_1 Depth=1
	callq	_ZN14CByteInBufWrap20ReadByteFromNewBlockEv
	movl	24(%rbx), %edx
	movl	32(%rbx), %ecx
	jmp	.LBB16_7
.LBB16_8:                               # %_ZN8NArchive5NPpmd13CRangeDecoder9NormalizeEv.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end16:
	.size	_ZN8NArchive5NPpmdL12Range_DecodeEPvjj, .Lfunc_end16-_ZN8NArchive5NPpmdL12Range_DecodeEPvjj
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmdL15Range_DecodeBitEPvj,@function
_ZN8NArchive5NPpmdL15Range_DecodeBitEPvj: # @_ZN8NArchive5NPpmdL15Range_DecodeBitEPvj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 32
.Lcfi58:
	.cfi_offset %rbx, -24
.Lcfi59:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %rbx
	movl	24(%rbx), %ecx
	movl	28(%rbx), %ebp
	shrl	$14, %ecx
	movl	%ecx, 24(%rbx)
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	%ecx
	cmpl	%esi, %eax
	jae	.LBB17_11
# BB#1:
	movl	32(%rbx), %edx
	imull	%esi, %ecx
	movl	%ecx, 24(%rbx)
	jmp	.LBB17_2
	.p2align	4, 0x90
.LBB17_10:                              # %_ZN14CByteInBufWrap8ReadByteEv.exit.i.i
                                        #   in Loop: Header=BB17_2 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 28(%rbx)
	shll	$8, %ecx
	movl	%ecx, 24(%rbx)
	shll	$8, %edx
	movl	%edx, 32(%rbx)
.LBB17_2:                               # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rcx), %eax
	xorl	%edx, %eax
	cmpl	$16777216, %eax         # imm = 0x1000000
	jb	.LBB17_7
# BB#3:                                 #   in Loop: Header=BB17_2 Depth=1
	cmpl	$32767, %ecx            # imm = 0x7FFF
	ja	.LBB17_4
# BB#6:                                 #   in Loop: Header=BB17_2 Depth=1
	movl	%edx, %ecx
	negl	%ecx
	andl	$32767, %ecx            # imm = 0x7FFF
	movl	%ecx, 24(%rbx)
.LBB17_7:                               # %.critedge.i.i
                                        #   in Loop: Header=BB17_2 Depth=1
	shll	$8, %ebp
	movq	40(%rbx), %rdi
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	je	.LBB17_9
# BB#8:                                 #   in Loop: Header=BB17_2 Depth=1
	leaq	1(%rax), %rsi
	movq	%rsi, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB17_10
	.p2align	4, 0x90
.LBB17_9:                               #   in Loop: Header=BB17_2 Depth=1
	callq	_ZN14CByteInBufWrap20ReadByteFromNewBlockEv
	movl	24(%rbx), %ecx
	movl	32(%rbx), %edx
	jmp	.LBB17_10
.LBB17_11:
	movl	$16384, %edx            # imm = 0x4000
	subl	%esi, %edx
	imull	%ecx, %esi
	subl	%esi, %ebp
	addl	32(%rbx), %esi
	movl	%esi, 32(%rbx)
	movl	%ebp, 28(%rbx)
	imull	%ecx, %edx
	movl	%edx, 24(%rbx)
	jmp	.LBB17_12
	.p2align	4, 0x90
.LBB17_19:                              # %_ZN14CByteInBufWrap8ReadByteEv.exit.i.i12
                                        #   in Loop: Header=BB17_12 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 28(%rbx)
	shll	$8, %edx
	movl	%edx, 24(%rbx)
	shll	$8, %esi
	movl	%esi, 32(%rbx)
.LBB17_12:                              # =>This Inner Loop Header: Depth=1
	leal	(%rsi,%rdx), %eax
	xorl	%esi, %eax
	cmpl	$16777216, %eax         # imm = 0x1000000
	jb	.LBB17_16
# BB#13:                                #   in Loop: Header=BB17_12 Depth=1
	cmpl	$32767, %edx            # imm = 0x7FFF
	ja	.LBB17_14
# BB#15:                                #   in Loop: Header=BB17_12 Depth=1
	movl	%esi, %edx
	negl	%edx
	andl	$32767, %edx            # imm = 0x7FFF
	movl	%edx, 24(%rbx)
.LBB17_16:                              # %.critedge.i.i8
                                        #   in Loop: Header=BB17_12 Depth=1
	shll	$8, %ebp
	movq	40(%rbx), %rdi
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	je	.LBB17_18
# BB#17:                                #   in Loop: Header=BB17_12 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB17_19
	.p2align	4, 0x90
.LBB17_18:                              #   in Loop: Header=BB17_12 Depth=1
	callq	_ZN14CByteInBufWrap20ReadByteFromNewBlockEv
	movl	24(%rbx), %edx
	movl	32(%rbx), %esi
	jmp	.LBB17_19
.LBB17_4:
	xorl	%eax, %eax
	jmp	.LBB17_5
.LBB17_14:
	movl	$1, %eax
.LBB17_5:                               # %_ZN8NArchive5NPpmdL12Range_DecodeEPvjj.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end17:
	.size	_ZN8NArchive5NPpmdL15Range_DecodeBitEPvj, .Lfunc_end17-_ZN8NArchive5NPpmdL15Range_DecodeBitEPvj
	.cfi_endproc

	.globl	_ZN8NArchive5NPpmd8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandler7ExtractEPKjjiP23IArchiveExtractCallback,@function
_ZN8NArchive5NPpmd8CHandler7ExtractEPKjjiP23IArchiveExtractCallback: # @_ZN8NArchive5NPpmd8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi64:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 56
	subq	$26728, %rsp            # imm = 0x6868
.Lcfi66:
	.cfi_def_cfa_offset 26784
.Lcfi67:
	.cfi_offset %rbx, -56
.Lcfi68:
	.cfi_offset %r12, -48
.Lcfi69:
	.cfi_offset %r13, -40
.Lcfi70:
	.cfi_offset %r14, -32
.Lcfi71:
	.cfi_offset %r15, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movl	%ecx, %ebx
	movq	%rdi, %r12
	cmpl	$-1, %edx
	je	.LBB18_7
# BB#1:
	testl	%edx, %edx
	je	.LBB18_2
# BB#3:
	cmpl	$1, %edx
	jne	.LBB18_5
# BB#4:
	cmpl	$0, (%rsi)
	je	.LBB18_7
.LBB18_5:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	jmp	.LBB18_6
.LBB18_7:
	movq	$0, 88(%rsp)
	movq	(%r15), %rax
	leaq	88(%rsp), %rsi
	movq	%r15, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB18_6
# BB#8:
	movq	$0, (%rsp)
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	setne	%r14b
	movq	(%r15), %rax
.Ltmp78:
	movq	%rsp, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%r14d, %ecx
	callq	*56(%rax)
	movl	%eax, %ebp
.Ltmp79:
# BB#9:
	testl	%ebp, %ebp
	jne	.LBB18_35
# BB#10:
	testl	%ebx, %ebx
	sete	%al
	cmpq	$0, (%rsp)
	jne	.LBB18_12
# BB#11:
	xorl	%ebp, %ebp
	testb	%al, %al
	jne	.LBB18_6
.LBB18_12:
	movq	(%r15), %rax
.Ltmp80:
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	*64(%rax)
.Ltmp81:
# BB#13:
.Ltmp82:
	leaq	24(%rsp), %rdi
	callq	_ZN14CByteInBufWrapC1Ev
.Ltmp83:
# BB#14:
.Ltmp85:
	leaq	24(%rsp), %r13
	movl	$1048576, %esi          # imm = 0x100000
	movq	%r13, %rdi
	callq	_ZN14CByteInBufWrap5AllocEj
.Ltmp86:
# BB#15:
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	testb	%al, %al
	je	.LBB18_34
# BB#16:
	movq	88(%r12), %rax
	movq	%rax, 64(%rsp)
	xorl	%ebx, %ebx
.Ltmp88:
	movl	$1048576, %edi          # imm = 0x100000
	callq	MidAlloc
.Ltmp89:
# BB#17:
	testq	%rax, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	je	.LBB18_18
# BB#19:
.Ltmp90:
	movq	%rax, %rbx
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp91:
# BB#20:
.Ltmp93:
	movq	%rbp, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp94:
# BB#21:
	movq	(%rbp), %rax
.Ltmp96:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp97:
# BB#22:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit
.Ltmp99:
	movl	$1, %edx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp100:
# BB#23:
	movl	56(%r12), %eax
	movq	$_ZN8NArchive5NPpmdL18Range_GetThresholdEPvj, 104(%rsp)
	movq	$_ZN8NArchive5NPpmdL12Range_DecodeEPvjj, 112(%rsp)
	movq	$_ZN8NArchive5NPpmdL15Range_DecodeBitEPvj, 120(%rsp)
	movl	%eax, 96(%rsp)
	leaq	152(%rsp), %rdi
.Ltmp101:
	callq	Ppmd7_Construct
.Ltmp102:
# BB#24:                                # %.noexc148
	leaq	19336(%rsp), %rdi
.Ltmp103:
	callq	Ppmd8_Construct
.Ltmp104:
# BB#25:                                # %_ZN8NArchive5NPpmd8CPpmdCppC2Ej.exit
	movl	52(%r12), %esi
	shll	$20, %esi
	cmpl	$7, 96(%rsp)
	jne	.LBB18_27
# BB#26:
.Ltmp108:
	movl	$_ZN8NArchive5NPpmdL10g_BigAllocE, %edx
	leaq	152(%rsp), %rdi
	callq	Ppmd7_Alloc
.Ltmp109:
	jmp	.LBB18_28
.LBB18_2:
	xorl	%ebp, %ebp
	jmp	.LBB18_6
.LBB18_18:
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB18_33
.LBB18_27:
.Ltmp106:
	movl	$_ZN8NArchive5NPpmdL10g_BigAllocE, %edx
	leaq	19336(%rsp), %rdi
	callq	Ppmd8_Alloc
.Ltmp107:
.LBB18_28:
	testl	%eax, %eax
	je	.LBB18_29
# BB#39:
	movl	56(%r12), %eax
	cmpl	$7, %eax
	je	.LBB18_42
# BB#40:
	movl	$1, %r14d
	cmpl	$8, %eax
	jne	.LBB18_49
# BB#41:                                # %_ZNK8NArchive5NPpmd5CItem11IsSupportedEv.exit
	cmpl	$1, 60(%r12)
	ja	.LBB18_49
.LBB18_42:                              # %_ZNK8NArchive5NPpmd5CItem11IsSupportedEv.exit.thread
	movl	48(%r12), %esi
	cmpl	$7, 96(%rsp)
	jne	.LBB18_44
# BB#43:
.Ltmp113:
	leaq	152(%rsp), %rdi
	callq	Ppmd7_Init
.Ltmp114:
	jmp	.LBB18_45
.LBB18_29:
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB18_30
.LBB18_44:
	movl	60(%r12), %edx
.Ltmp111:
	leaq	19336(%rsp), %rdi
	callq	Ppmd8_Init
.Ltmp112:
.LBB18_45:                              # %_ZN8NArchive5NPpmd8CPpmdCpp4InitEjj.exit
	movq	48(%rsp), %rax
	movq	%rax, 32(%rsp)
	movq	%rax, 40(%rsp)
	movq	$0, 72(%rsp)
	movb	$0, 80(%rsp)
	movl	$0, 84(%rsp)
	cmpl	$7, 96(%rsp)
	jne	.LBB18_53
# BB#46:
	leaq	104(%rsp), %rdi
	movq	%r13, 144(%rsp)
.Ltmp117:
	callq	_ZN8NArchive5NPpmd13CRangeDecoder4InitEv
.Ltmp118:
# BB#47:                                # %_ZN8NArchive5NPpmd8CPpmdCpp6InitRcEP14CByteInBufWrap.exit
	cmpb	$0, 80(%rsp)
	sete	%cl
	andb	%cl, %al
	movl	$2, %r14d
	cmpb	$1, %al
	je	.LBB18_56
	jmp	.LBB18_48
.LBB18_53:
	movq	%r13, 19456(%rsp)
.Ltmp115:
	leaq	19336(%rsp), %rdi
	callq	Ppmd8_RangeDec_Init
.Ltmp116:
# BB#54:                                # %.noexc156
	movl	$2, %r14d
	testl	%eax, %eax
	je	.LBB18_48
# BB#55:                                # %.noexc156
	movb	80(%rsp), %al
	testb	%al, %al
	jne	.LBB18_48
.LBB18_56:
	movl	84(%rsp), %ebp
	testl	%ebp, %ebp
	jne	.LBB18_30
# BB#57:                                # %.thread183.preheader
	xorl	%ebx, %ebx
.LBB18_58:                              # %.thread183
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_62 Depth 2
                                        #     Child Loop BB18_76 Depth 2
	movq	32(%rsp), %rax
	addq	72(%rsp), %rax
	subq	48(%rsp), %rax
	movq	%rax, 72(%r12)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rax, 48(%rdi)
	movq	%rbx, 56(%rdi)
.Ltmp120:
	callq	_ZN14CLocalProgress6SetCurEv
	movl	%eax, %ebp
.Ltmp121:
# BB#59:                                #   in Loop: Header=BB18_58 Depth=1
	testl	%ebp, %ebp
	jne	.LBB18_30
# BB#60:                                #   in Loop: Header=BB18_58 Depth=1
	cmpl	$7, 96(%rsp)
	jne	.LBB18_61
# BB#75:                                # %.preheader.preheader
                                        #   in Loop: Header=BB18_58 Depth=1
	xorl	%r14d, %r14d
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB18_76:                              # %.preheader
                                        #   Parent Loop BB18_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp126:
	leaq	152(%rsp), %rdi
	leaq	104(%rsp), %rsi
	callq	Ppmd7_DecodeSymbol
	movl	%eax, %r13d
.Ltmp127:
# BB#77:                                #   in Loop: Header=BB18_76 Depth=2
	testl	%r13d, %r13d
	js	.LBB18_66
# BB#78:                                #   in Loop: Header=BB18_76 Depth=2
	movzbl	80(%rsp), %eax
	testb	%al, %al
	jne	.LBB18_66
# BB#79:                                #   in Loop: Header=BB18_76 Depth=2
	movb	%r13b, (%rbp,%r14)
	incq	%r14
	cmpq	$1048576, %r14          # imm = 0x100000
	jb	.LBB18_76
	jmp	.LBB18_66
.LBB18_61:                              # %.preheader204.preheader
                                        #   in Loop: Header=BB18_58 Depth=1
	xorl	%r14d, %r14d
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB18_62:                              # %.preheader204
                                        #   Parent Loop BB18_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp123:
	leaq	19336(%rsp), %rdi
	callq	Ppmd8_DecodeSymbol
	movl	%eax, %r13d
.Ltmp124:
# BB#63:                                #   in Loop: Header=BB18_62 Depth=2
	testl	%r13d, %r13d
	js	.LBB18_66
# BB#64:                                #   in Loop: Header=BB18_62 Depth=2
	movzbl	80(%rsp), %eax
	testb	%al, %al
	jne	.LBB18_66
# BB#65:                                #   in Loop: Header=BB18_62 Depth=2
	movb	%r13b, (%rbp,%r14)
	incq	%r14
	cmpq	$1048576, %r14          # imm = 0x100000
	jb	.LBB18_62
.LBB18_66:                              # %.loopexit
                                        #   in Loop: Header=BB18_58 Depth=1
	movl	64(%r12), %eax
	addq	72(%rsp), %rax
	addq	32(%rsp), %rax
	subq	48(%rsp), %rax
	movq	%rax, 72(%r12)
	movb	$1, 80(%r12)
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_69
# BB#67:                                #   in Loop: Header=BB18_58 Depth=1
.Ltmp129:
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r14, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, %ebp
.Ltmp130:
# BB#68:                                #   in Loop: Header=BB18_58 Depth=1
	testl	%ebp, %ebp
	jne	.LBB18_30
.LBB18_69:                              #   in Loop: Header=BB18_58 Depth=1
	addq	%r14, %rbx
	testl	%r13d, %r13d
	jns	.LBB18_58
# BB#70:
	movl	$2, %r14d
	cmpl	$-1, %r13d
	jne	.LBB18_48
# BB#71:
	cmpl	$7, 96(%rsp)
	leaq	132(%rsp), %rax
	leaq	19444(%rsp), %rcx
	cmoveq	%rax, %rcx
	xorl	%r14d, %r14d
	cmpl	$0, (%rcx)
	setne	%r14b
	addl	%r14d, %r14d
.LBB18_48:                              # %.thread
	movl	84(%rsp), %ebp
	testl	%ebp, %ebp
	jne	.LBB18_30
.LBB18_49:                              # %_ZNK8NArchive5NPpmd5CItem11IsSupportedEv.exit.thread182
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_52
# BB#50:
	movq	(%rdi), %rax
.Ltmp132:
	callq	*16(%rax)
.Ltmp133:
# BB#51:                                # %.noexc163
	movq	$0, (%rsp)
.LBB18_52:                              # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
	movq	(%r15), %rax
.Ltmp134:
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	*72(%rax)
	movl	%eax, %ebp
.Ltmp135:
.LBB18_30:                              # %.thread193
.Ltmp141:
	movl	$_ZN8NArchive5NPpmdL10g_BigAllocE, %esi
	leaq	152(%rsp), %rdi
	callq	Ppmd7_Free
.Ltmp142:
# BB#31:                                # %.noexc164
.Ltmp143:
	movl	$_ZN8NArchive5NPpmdL10g_BigAllocE, %esi
	leaq	19336(%rsp), %rdi
	callq	Ppmd8_Free
.Ltmp144:
# BB#32:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp148:
	callq	*16(%rax)
.Ltmp149:
.LBB18_33:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit167
.Ltmp153:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	MidFree
.Ltmp154:
.LBB18_34:                              # %_ZN8NArchive5NPpmd4CBufD2Ev.exit159
.Ltmp158:
	leaq	24(%rsp), %rdi
	callq	_ZN14CByteInBufWrap4FreeEv
.Ltmp159:
.LBB18_35:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_6
# BB#36:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB18_6:
	movl	%ebp, %eax
	addq	$26728, %rsp            # imm = 0x6868
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_74:                              # %.loopexit.split-lp211
.Ltmp119:
	jmp	.LBB18_85
.LBB18_82:                              # %.loopexit.split-lp.loopexit.split-lp
.Ltmp131:
	jmp	.LBB18_85
.LBB18_73:                              # %.loopexit210
.Ltmp122:
	jmp	.LBB18_85
.LBB18_72:
.Ltmp136:
	jmp	.LBB18_85
.LBB18_87:
.Ltmp150:
	movq	%rax, %r14
	jmp	.LBB18_93
.LBB18_84:
.Ltmp110:
	jmp	.LBB18_85
.LBB18_38:
.Ltmp98:
	movq	%rax, %r14
	jmp	.LBB18_93
.LBB18_37:
.Ltmp95:
	movq	%rax, %r14
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB18_93
.LBB18_91:
.Ltmp155:
	jmp	.LBB18_96
.LBB18_83:
.Ltmp145:
	jmp	.LBB18_89
.LBB18_92:
.Ltmp92:
	movq	%rax, %r14
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	jmp	.LBB18_93
.LBB18_88:
.Ltmp105:
.LBB18_89:
	movq	%rax, %r14
	jmp	.LBB18_90
.LBB18_94:
.Ltmp160:
	jmp	.LBB18_99
.LBB18_95:
.Ltmp87:
.LBB18_96:                              # %_ZN8NArchive5NPpmd4CBufD2Ev.exit
	movq	%rax, %r14
	jmp	.LBB18_97
.LBB18_81:                              # %.loopexit.split-lp.loopexit
.Ltmp125:
	jmp	.LBB18_85
.LBB18_80:                              # %.loopexit203
.Ltmp128:
.LBB18_85:                              # %.loopexit.split-lp
	movq	%rax, %r14
.Ltmp137:
	movl	$_ZN8NArchive5NPpmdL10g_BigAllocE, %esi
	leaq	152(%rsp), %rdi
	callq	Ppmd7_Free
.Ltmp138:
# BB#86:                                # %.noexc168
.Ltmp139:
	movl	$_ZN8NArchive5NPpmdL10g_BigAllocE, %esi
	leaq	19336(%rsp), %rdi
	callq	Ppmd8_Free
.Ltmp140:
.LBB18_90:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp146:
	callq	*16(%rax)
.Ltmp147:
.LBB18_93:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
.Ltmp151:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	MidFree
.Ltmp152:
.LBB18_97:                              # %_ZN8NArchive5NPpmd4CBufD2Ev.exit
.Ltmp156:
	leaq	24(%rsp), %rdi
	callq	_ZN14CByteInBufWrap4FreeEv
.Ltmp157:
	jmp	.LBB18_100
.LBB18_98:
.Ltmp84:
.LBB18_99:
	movq	%rax, %r14
.LBB18_100:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_102
# BB#101:
	movq	(%rdi), %rax
.Ltmp161:
	callq	*16(%rax)
.Ltmp162:
.LBB18_102:                             # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB18_103:
.Ltmp163:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN8NArchive5NPpmd8CHandler7ExtractEPKjjiP23IArchiveExtractCallback, .Lfunc_end18-_ZN8NArchive5NPpmd8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\247\202"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\236\002"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp78-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp78
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp83-.Ltmp78         #   Call between .Ltmp78 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin3   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin3   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp91-.Ltmp88         #   Call between .Ltmp88 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin3   #     jumps to .Ltmp92
	.byte	0                       #   On action: cleanup
	.long	.Ltmp93-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp94-.Ltmp93         #   Call between .Ltmp93 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin3   #     jumps to .Ltmp95
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp97-.Ltmp96         #   Call between .Ltmp96 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin3   #     jumps to .Ltmp98
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp104-.Ltmp99        #   Call between .Ltmp99 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin3  #     jumps to .Ltmp105
	.byte	0                       #   On action: cleanup
	.long	.Ltmp108-.Lfunc_begin3  # >> Call Site 8 <<
	.long	.Ltmp107-.Ltmp108       #   Call between .Ltmp108 and .Ltmp107
	.long	.Ltmp110-.Lfunc_begin3  #     jumps to .Ltmp110
	.byte	0                       #   On action: cleanup
	.long	.Ltmp113-.Lfunc_begin3  # >> Call Site 9 <<
	.long	.Ltmp112-.Ltmp113       #   Call between .Ltmp113 and .Ltmp112
	.long	.Ltmp136-.Lfunc_begin3  #     jumps to .Ltmp136
	.byte	0                       #   On action: cleanup
	.long	.Ltmp117-.Lfunc_begin3  # >> Call Site 10 <<
	.long	.Ltmp116-.Ltmp117       #   Call between .Ltmp117 and .Ltmp116
	.long	.Ltmp119-.Lfunc_begin3  #     jumps to .Ltmp119
	.byte	0                       #   On action: cleanup
	.long	.Ltmp120-.Lfunc_begin3  # >> Call Site 11 <<
	.long	.Ltmp121-.Ltmp120       #   Call between .Ltmp120 and .Ltmp121
	.long	.Ltmp122-.Lfunc_begin3  #     jumps to .Ltmp122
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin3  # >> Call Site 12 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp128-.Lfunc_begin3  #     jumps to .Ltmp128
	.byte	0                       #   On action: cleanup
	.long	.Ltmp123-.Lfunc_begin3  # >> Call Site 13 <<
	.long	.Ltmp124-.Ltmp123       #   Call between .Ltmp123 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin3  #     jumps to .Ltmp125
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin3  # >> Call Site 14 <<
	.long	.Ltmp130-.Ltmp129       #   Call between .Ltmp129 and .Ltmp130
	.long	.Ltmp131-.Lfunc_begin3  #     jumps to .Ltmp131
	.byte	0                       #   On action: cleanup
	.long	.Ltmp132-.Lfunc_begin3  # >> Call Site 15 <<
	.long	.Ltmp135-.Ltmp132       #   Call between .Ltmp132 and .Ltmp135
	.long	.Ltmp136-.Lfunc_begin3  #     jumps to .Ltmp136
	.byte	0                       #   On action: cleanup
	.long	.Ltmp141-.Lfunc_begin3  # >> Call Site 16 <<
	.long	.Ltmp144-.Ltmp141       #   Call between .Ltmp141 and .Ltmp144
	.long	.Ltmp145-.Lfunc_begin3  #     jumps to .Ltmp145
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin3  # >> Call Site 17 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin3  #     jumps to .Ltmp150
	.byte	0                       #   On action: cleanup
	.long	.Ltmp153-.Lfunc_begin3  # >> Call Site 18 <<
	.long	.Ltmp154-.Ltmp153       #   Call between .Ltmp153 and .Ltmp154
	.long	.Ltmp155-.Lfunc_begin3  #     jumps to .Ltmp155
	.byte	0                       #   On action: cleanup
	.long	.Ltmp158-.Lfunc_begin3  # >> Call Site 19 <<
	.long	.Ltmp159-.Ltmp158       #   Call between .Ltmp158 and .Ltmp159
	.long	.Ltmp160-.Lfunc_begin3  #     jumps to .Ltmp160
	.byte	0                       #   On action: cleanup
	.long	.Ltmp159-.Lfunc_begin3  # >> Call Site 20 <<
	.long	.Ltmp137-.Ltmp159       #   Call between .Ltmp159 and .Ltmp137
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp137-.Lfunc_begin3  # >> Call Site 21 <<
	.long	.Ltmp162-.Ltmp137       #   Call between .Ltmp137 and .Ltmp162
	.long	.Ltmp163-.Lfunc_begin3  #     jumps to .Ltmp163
	.byte	1                       #   On action: 1
	.long	.Ltmp162-.Lfunc_begin3  # >> Call Site 22 <<
	.long	.Lfunc_end18-.Ltmp162   #   Call between .Ltmp162 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 32
.Lcfi76:
	.cfi_offset %rbx, -32
.Lcfi77:
	.cfi_offset %r14, -24
.Lcfi78:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB19_1
# BB#2:
	movl	$IID_IInArchive, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB19_3
.LBB19_1:
	movq	%rbx, (%r14)
.LBB19_6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB19_7:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB19_3:
	movl	$IID_IArchiveOpenSeq, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB19_4
# BB#5:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	jmp	.LBB19_6
.LBB19_4:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB19_7
.Lfunc_end19:
	.size	_ZN8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end19-_ZN8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive5NPpmd8CHandler6AddRefEv,"axG",@progbits,_ZN8NArchive5NPpmd8CHandler6AddRefEv,comdat
	.weak	_ZN8NArchive5NPpmd8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandler6AddRefEv,@function
_ZN8NArchive5NPpmd8CHandler6AddRefEv:   # @_ZN8NArchive5NPpmd8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end20:
	.size	_ZN8NArchive5NPpmd8CHandler6AddRefEv, .Lfunc_end20-_ZN8NArchive5NPpmd8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive5NPpmd8CHandler7ReleaseEv,"axG",@progbits,_ZN8NArchive5NPpmd8CHandler7ReleaseEv,comdat
	.weak	_ZN8NArchive5NPpmd8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandler7ReleaseEv,@function
_ZN8NArchive5NPpmd8CHandler7ReleaseEv:  # @_ZN8NArchive5NPpmd8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB21_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB21_2:
	popq	%rcx
	retq
.Lfunc_end21:
	.size	_ZN8NArchive5NPpmd8CHandler7ReleaseEv, .Lfunc_end21-_ZN8NArchive5NPpmd8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive5NPpmd8CHandlerD2Ev,"axG",@progbits,_ZN8NArchive5NPpmd8CHandlerD2Ev,comdat
	.weak	_ZN8NArchive5NPpmd8CHandlerD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandlerD2Ev,@function
_ZN8NArchive5NPpmd8CHandlerD2Ev:        # @_ZN8NArchive5NPpmd8CHandlerD2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi82:
	.cfi_def_cfa_offset 32
.Lcfi83:
	.cfi_offset %rbx, -24
.Lcfi84:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive5NPpmd8CHandlerE+160, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive5NPpmd8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp164:
	callq	*16(%rax)
.Ltmp165:
.LBB22_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	32(%rbx), %rdi
	addq	$8, %rsp
	testq	%rdi, %rdi
	je	.LBB22_3
# BB#7:
	popq	%rbx
	popq	%r14
	jmp	_ZdaPv                  # TAILCALL
.LBB22_3:                               # %_ZN8NArchive5NPpmd5CItemD2Ev.exit
	popq	%rbx
	popq	%r14
	retq
.LBB22_4:
.Ltmp166:
	movq	%rax, %r14
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_6
# BB#5:
	callq	_ZdaPv
.LBB22_6:                               # %_ZN8NArchive5NPpmd5CItemD2Ev.exit4
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end22:
	.size	_ZN8NArchive5NPpmd8CHandlerD2Ev, .Lfunc_end22-_ZN8NArchive5NPpmd8CHandlerD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp164-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp165-.Ltmp164       #   Call between .Ltmp164 and .Ltmp165
	.long	.Ltmp166-.Lfunc_begin4  #     jumps to .Ltmp166
	.byte	0                       #   On action: cleanup
	.long	.Ltmp165-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Lfunc_end22-.Ltmp165   #   Call between .Ltmp165 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive5NPpmd8CHandlerD0Ev,"axG",@progbits,_ZN8NArchive5NPpmd8CHandlerD0Ev,comdat
	.weak	_ZN8NArchive5NPpmd8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd8CHandlerD0Ev,@function
_ZN8NArchive5NPpmd8CHandlerD0Ev:        # @_ZN8NArchive5NPpmd8CHandlerD0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi87:
	.cfi_def_cfa_offset 32
.Lcfi88:
	.cfi_offset %rbx, -24
.Lcfi89:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive5NPpmd8CHandlerE+160, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive5NPpmd8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp167:
	callq	*16(%rax)
.Ltmp168:
.LBB23_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_4
# BB#3:
	callq	_ZdaPv
.LBB23_4:                               # %_ZN8NArchive5NPpmd8CHandlerD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB23_5:
.Ltmp169:
	movq	%rax, %r14
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_7
# BB#6:
	callq	_ZdaPv
.LBB23_7:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end23:
	.size	_ZN8NArchive5NPpmd8CHandlerD0Ev, .Lfunc_end23-_ZN8NArchive5NPpmd8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp167-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp168-.Ltmp167       #   Call between .Ltmp167 and .Ltmp168
	.long	.Ltmp169-.Lfunc_begin5  #     jumps to .Ltmp169
	.byte	0                       #   On action: cleanup
	.long	.Ltmp168-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Lfunc_end23-.Ltmp168   #   Call between .Ltmp168 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi90:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB24_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB24_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB24_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB24_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB24_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB24_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB24_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB24_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB24_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB24_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB24_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB24_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB24_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB24_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB24_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB24_16
.LBB24_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_IInArchive(%rip), %cl
	jne	.LBB24_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_IInArchive+1(%rip), %al
	jne	.LBB24_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_IInArchive+2(%rip), %al
	jne	.LBB24_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_IInArchive+3(%rip), %al
	jne	.LBB24_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_IInArchive+4(%rip), %al
	jne	.LBB24_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_IInArchive+5(%rip), %al
	jne	.LBB24_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_IInArchive+6(%rip), %al
	jne	.LBB24_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_IInArchive+7(%rip), %al
	jne	.LBB24_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_IInArchive+8(%rip), %al
	jne	.LBB24_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_IInArchive+9(%rip), %al
	jne	.LBB24_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_IInArchive+10(%rip), %al
	jne	.LBB24_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_IInArchive+11(%rip), %al
	jne	.LBB24_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_IInArchive+12(%rip), %al
	jne	.LBB24_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_IInArchive+13(%rip), %al
	jne	.LBB24_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_IInArchive+14(%rip), %al
	jne	.LBB24_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %al
	cmpb	IID_IInArchive+15(%rip), %al
	jne	.LBB24_33
.LBB24_16:
	movq	%rdi, (%rdx)
	jmp	.LBB24_50
.LBB24_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IArchiveOpenSeq(%rip), %cl
	jne	.LBB24_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+1(%rip), %cl
	jne	.LBB24_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+2(%rip), %cl
	jne	.LBB24_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+3(%rip), %cl
	jne	.LBB24_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+4(%rip), %cl
	jne	.LBB24_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+5(%rip), %cl
	jne	.LBB24_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+6(%rip), %cl
	jne	.LBB24_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+7(%rip), %cl
	jne	.LBB24_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+8(%rip), %cl
	jne	.LBB24_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+9(%rip), %cl
	jne	.LBB24_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+10(%rip), %cl
	jne	.LBB24_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+11(%rip), %cl
	jne	.LBB24_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+12(%rip), %cl
	jne	.LBB24_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+13(%rip), %cl
	jne	.LBB24_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+14(%rip), %cl
	jne	.LBB24_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+15(%rip), %cl
	jne	.LBB24_51
# BB#49:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
.LBB24_50:                              # %_ZN8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv.exit
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB24_51:                              # %_ZN8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZThn8_N8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end24-_ZThn8_N8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive5NPpmd8CHandler6AddRefEv,"axG",@progbits,_ZThn8_N8NArchive5NPpmd8CHandler6AddRefEv,comdat
	.weak	_ZThn8_N8NArchive5NPpmd8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive5NPpmd8CHandler6AddRefEv,@function
_ZThn8_N8NArchive5NPpmd8CHandler6AddRefEv: # @_ZThn8_N8NArchive5NPpmd8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end25:
	.size	_ZThn8_N8NArchive5NPpmd8CHandler6AddRefEv, .Lfunc_end25-_ZThn8_N8NArchive5NPpmd8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive5NPpmd8CHandler7ReleaseEv,"axG",@progbits,_ZThn8_N8NArchive5NPpmd8CHandler7ReleaseEv,comdat
	.weak	_ZThn8_N8NArchive5NPpmd8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive5NPpmd8CHandler7ReleaseEv,@function
_ZThn8_N8NArchive5NPpmd8CHandler7ReleaseEv: # @_ZThn8_N8NArchive5NPpmd8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB26_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB26_2:                               # %_ZN8NArchive5NPpmd8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end26:
	.size	_ZThn8_N8NArchive5NPpmd8CHandler7ReleaseEv, .Lfunc_end26-_ZThn8_N8NArchive5NPpmd8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive5NPpmd8CHandlerD1Ev,"axG",@progbits,_ZThn8_N8NArchive5NPpmd8CHandlerD1Ev,comdat
	.weak	_ZThn8_N8NArchive5NPpmd8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive5NPpmd8CHandlerD1Ev,@function
_ZThn8_N8NArchive5NPpmd8CHandlerD1Ev:   # @_ZThn8_N8NArchive5NPpmd8CHandlerD1Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 32
.Lcfi95:
	.cfi_offset %rbx, -24
.Lcfi96:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive5NPpmd8CHandlerE+160, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive5NPpmd8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rbx)
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp170:
	callq	*16(%rax)
.Ltmp171:
.LBB27_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i
	movq	24(%rbx), %rdi
	addq	$8, %rsp
	testq	%rdi, %rdi
	je	.LBB27_7
# BB#3:
	popq	%rbx
	popq	%r14
	jmp	_ZdaPv                  # TAILCALL
.LBB27_7:                               # %_ZN8NArchive5NPpmd8CHandlerD2Ev.exit
	popq	%rbx
	popq	%r14
	retq
.LBB27_4:
.Ltmp172:
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_6
# BB#5:
	callq	_ZdaPv
.LBB27_6:                               # %_ZN8NArchive5NPpmd5CItemD2Ev.exit4.i
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end27:
	.size	_ZThn8_N8NArchive5NPpmd8CHandlerD1Ev, .Lfunc_end27-_ZThn8_N8NArchive5NPpmd8CHandlerD1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp170-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp171-.Ltmp170       #   Call between .Ltmp170 and .Ltmp171
	.long	.Ltmp172-.Lfunc_begin6  #     jumps to .Ltmp172
	.byte	0                       #   On action: cleanup
	.long	.Ltmp171-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Lfunc_end27-.Ltmp171   #   Call between .Ltmp171 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N8NArchive5NPpmd8CHandlerD0Ev,"axG",@progbits,_ZThn8_N8NArchive5NPpmd8CHandlerD0Ev,comdat
	.weak	_ZThn8_N8NArchive5NPpmd8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive5NPpmd8CHandlerD0Ev,@function
_ZThn8_N8NArchive5NPpmd8CHandlerD0Ev:   # @_ZThn8_N8NArchive5NPpmd8CHandlerD0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 32
.Lcfi100:
	.cfi_offset %rbx, -24
.Lcfi101:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	movl	$_ZTVN8NArchive5NPpmd8CHandlerE+160, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN8NArchive5NPpmd8CHandlerE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rax)
	movq	80(%rax), %rdi
	leaq	-8(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB28_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp173:
	callq	*16(%rax)
.Ltmp174:
.LBB28_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i.i
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_4
# BB#3:
	callq	_ZdaPv
.LBB28_4:                               # %_ZN8NArchive5NPpmd8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB28_5:
.Ltmp175:
	movq	%rax, %r14
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_7
# BB#6:
	callq	_ZdaPv
.LBB28_7:                               # %.body.i
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end28:
	.size	_ZThn8_N8NArchive5NPpmd8CHandlerD0Ev, .Lfunc_end28-_ZThn8_N8NArchive5NPpmd8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp173-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp174-.Ltmp173       #   Call between .Ltmp173 and .Ltmp174
	.long	.Ltmp175-.Lfunc_begin7  #     jumps to .Ltmp175
	.byte	0                       #   On action: cleanup
	.long	.Ltmp174-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Lfunc_end28-.Ltmp174   #   Call between .Ltmp174 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmdL10SzBigAllocEPvm,@function
_ZN8NArchive5NPpmdL10SzBigAllocEPvm:    # @_ZN8NArchive5NPpmdL10SzBigAllocEPvm
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	BigAlloc                # TAILCALL
.Lfunc_end29:
	.size	_ZN8NArchive5NPpmdL10SzBigAllocEPvm, .Lfunc_end29-_ZN8NArchive5NPpmdL10SzBigAllocEPvm
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmdL9SzBigFreeEPvS1_,@function
_ZN8NArchive5NPpmdL9SzBigFreeEPvS1_:    # @_ZN8NArchive5NPpmdL9SzBigFreeEPvS1_
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	BigFree                 # TAILCALL
.Lfunc_end30:
	.size	_ZN8NArchive5NPpmdL9SzBigFreeEPvS1_, .Lfunc_end30-_ZN8NArchive5NPpmdL9SzBigFreeEPvS1_
	.cfi_endproc

	.section	.text._ZN8NArchive5NPpmd13CRangeDecoder4InitEv,"axG",@progbits,_ZN8NArchive5NPpmd13CRangeDecoder4InitEv,comdat
	.weak	_ZN8NArchive5NPpmd13CRangeDecoder4InitEv
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmd13CRangeDecoder4InitEv,@function
_ZN8NArchive5NPpmd13CRangeDecoder4InitEv: # @_ZN8NArchive5NPpmd13CRangeDecoder4InitEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 32
.Lcfi105:
	.cfi_offset %rbx, -32
.Lcfi106:
	.cfi_offset %r14, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$0, 32(%r14)
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 24(%r14)
	movq	40(%r14), %rdi
	movq	8(%rdi), %rax
	movq	16(%rdi), %rcx
	cmpq	%rcx, %rax
	je	.LBB31_2
# BB#1:
	leaq	1(%rax), %rsi
	movq	%rsi, 8(%rdi)
	movb	(%rax), %al
	jmp	.LBB31_3
.LBB31_2:
	callq	_ZN14CByteInBufWrap20ReadByteFromNewBlockEv
	movq	40(%r14), %rdi
	movq	8(%rdi), %rsi
	movq	16(%rdi), %rcx
.LBB31_3:                               # %_ZN14CByteInBufWrap8ReadByteEv.exit
	movzbl	%al, %ebx
	movl	%ebx, 28(%r14)
	shll	$8, %ebx
	cmpq	%rcx, %rsi
	je	.LBB31_5
# BB#4:
	leaq	1(%rsi), %rdx
	movq	%rdx, 8(%rdi)
	movb	(%rsi), %al
	jmp	.LBB31_6
.LBB31_5:
	callq	_ZN14CByteInBufWrap20ReadByteFromNewBlockEv
	movq	40(%r14), %rdi
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rcx
.LBB31_6:                               # %_ZN14CByteInBufWrap8ReadByteEv.exit.1
	movzbl	%al, %ebp
	orl	%ebx, %ebp
	movl	%ebp, 28(%r14)
	shll	$8, %ebp
	cmpq	%rcx, %rdx
	je	.LBB31_8
# BB#7:
	leaq	1(%rdx), %rsi
	movq	%rsi, 8(%rdi)
	movb	(%rdx), %al
	jmp	.LBB31_9
.LBB31_8:
	callq	_ZN14CByteInBufWrap20ReadByteFromNewBlockEv
	movq	40(%r14), %rdi
	movq	8(%rdi), %rsi
	movq	16(%rdi), %rcx
.LBB31_9:                               # %_ZN14CByteInBufWrap8ReadByteEv.exit.2
	movzbl	%al, %ebx
	orl	%ebp, %ebx
	movl	%ebx, 28(%r14)
	shll	$8, %ebx
	cmpq	%rcx, %rsi
	je	.LBB31_11
# BB#10:
	leaq	1(%rsi), %rax
	movq	%rax, 8(%rdi)
	movb	(%rsi), %al
	jmp	.LBB31_12
.LBB31_11:
	callq	_ZN14CByteInBufWrap20ReadByteFromNewBlockEv
.LBB31_12:                              # %_ZN14CByteInBufWrap8ReadByteEv.exit.3
	movzbl	%al, %eax
	orl	%ebx, %eax
	movl	%eax, 28(%r14)
	cmpl	$-1, %eax
	setne	%al
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end31:
	.size	_ZN8NArchive5NPpmd13CRangeDecoder4InitEv, .Lfunc_end31-_ZN8NArchive5NPpmd13CRangeDecoder4InitEv
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive5NPpmdL9CreateArcEv,@function
_ZN8NArchive5NPpmdL9CreateArcEv:        # @_ZN8NArchive5NPpmdL9CreateArcEv
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi110:
	.cfi_def_cfa_offset 32
.Lcfi111:
	.cfi_offset %rbx, -24
.Lcfi112:
	.cfi_offset %r14, -16
	movl	$96, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	$0, 16(%rbx)
	movl	$_ZTVN8NArchive5NPpmd8CHandlerE+160, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive5NPpmd8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 32(%rbx)
.Ltmp176:
	movl	$4, %edi
	callq	_Znam
.Ltmp177:
# BB#1:
	movq	%rax, 32(%rbx)
	movb	$0, (%rax)
	movl	$4, 44(%rbx)
	movq	$0, 88(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB32_2:
.Ltmp178:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end32:
	.size	_ZN8NArchive5NPpmdL9CreateArcEv, .Lfunc_end32-_ZN8NArchive5NPpmdL9CreateArcEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin8-.Lfunc_begin8 # >> Call Site 1 <<
	.long	.Ltmp176-.Lfunc_begin8  #   Call between .Lfunc_begin8 and .Ltmp176
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp176-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp177-.Ltmp176       #   Call between .Ltmp176 and .Ltmp177
	.long	.Ltmp178-.Lfunc_begin8  #     jumps to .Ltmp178
	.byte	0                       #   On action: cleanup
	.long	.Ltmp177-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Lfunc_end32-.Ltmp177   #   Call between .Ltmp177 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB33_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB33_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB33_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB33_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB33_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB33_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB33_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB33_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB33_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB33_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB33_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB33_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB33_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB33_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB33_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB33_16:
	xorl	%eax, %eax
	retq
.Lfunc_end33:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end33-_ZeqRK4GUIDS1_
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_PpmdHandler.ii,@function
_GLOBAL__sub_I_PpmdHandler.ii:          # @_GLOBAL__sub_I_PpmdHandler.ii
	.cfi_startproc
# BB#0:
	movl	$_ZN8NArchive5NPpmdL9g_ArcInfoE, %edi
	jmp	_Z11RegisterArcPK8CArcInfo # TAILCALL
.Lfunc_end34:
	.size	_GLOBAL__sub_I_PpmdHandler.ii, .Lfunc_end34-_GLOBAL__sub_I_PpmdHandler.ii
	.cfi_endproc

	.type	_ZN8NArchive5NPpmd6kPropsE,@object # @_ZN8NArchive5NPpmd6kPropsE
	.data
	.globl	_ZN8NArchive5NPpmd6kPropsE
	.p2align	4
_ZN8NArchive5NPpmd6kPropsE:
	.quad	0
	.long	3                       # 0x3
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	12                      # 0xc
	.short	64                      # 0x40
	.zero	2
	.quad	0
	.long	9                       # 0x9
	.short	19                      # 0x13
	.zero	2
	.quad	0
	.long	22                      # 0x16
	.short	8                       # 0x8
	.zero	2
	.size	_ZN8NArchive5NPpmd6kPropsE, 64

	.type	_ZTVN8NArchive5NPpmd8CHandlerE,@object # @_ZTVN8NArchive5NPpmd8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive5NPpmd8CHandlerE
	.p2align	3
_ZTVN8NArchive5NPpmd8CHandlerE:
	.quad	0
	.quad	_ZTIN8NArchive5NPpmd8CHandlerE
	.quad	_ZN8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive5NPpmd8CHandler6AddRefEv
	.quad	_ZN8NArchive5NPpmd8CHandler7ReleaseEv
	.quad	_ZN8NArchive5NPpmd8CHandlerD2Ev
	.quad	_ZN8NArchive5NPpmd8CHandlerD0Ev
	.quad	_ZN8NArchive5NPpmd8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.quad	_ZN8NArchive5NPpmd8CHandler5CloseEv
	.quad	_ZN8NArchive5NPpmd8CHandler16GetNumberOfItemsEPj
	.quad	_ZN8NArchive5NPpmd8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.quad	_ZN8NArchive5NPpmd8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.quad	_ZN8NArchive5NPpmd8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.quad	_ZN8NArchive5NPpmd8CHandler21GetNumberOfPropertiesEPj
	.quad	_ZN8NArchive5NPpmd8CHandler15GetPropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive5NPpmd8CHandler28GetNumberOfArchivePropertiesEPj
	.quad	_ZN8NArchive5NPpmd8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream
	.quad	-8
	.quad	_ZTIN8NArchive5NPpmd8CHandlerE
	.quad	_ZThn8_N8NArchive5NPpmd8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N8NArchive5NPpmd8CHandler6AddRefEv
	.quad	_ZThn8_N8NArchive5NPpmd8CHandler7ReleaseEv
	.quad	_ZThn8_N8NArchive5NPpmd8CHandlerD1Ev
	.quad	_ZThn8_N8NArchive5NPpmd8CHandlerD0Ev
	.quad	_ZThn8_N8NArchive5NPpmd8CHandler7OpenSeqEP19ISequentialInStream
	.size	_ZTVN8NArchive5NPpmd8CHandlerE, 208

	.type	_ZTSN8NArchive5NPpmd8CHandlerE,@object # @_ZTSN8NArchive5NPpmd8CHandlerE
	.globl	_ZTSN8NArchive5NPpmd8CHandlerE
	.p2align	4
_ZTSN8NArchive5NPpmd8CHandlerE:
	.asciz	"N8NArchive5NPpmd8CHandlerE"
	.size	_ZTSN8NArchive5NPpmd8CHandlerE, 27

	.type	_ZTS10IInArchive,@object # @_ZTS10IInArchive
	.section	.rodata._ZTS10IInArchive,"aG",@progbits,_ZTS10IInArchive,comdat
	.weak	_ZTS10IInArchive
_ZTS10IInArchive:
	.asciz	"10IInArchive"
	.size	_ZTS10IInArchive, 13

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI10IInArchive,@object # @_ZTI10IInArchive
	.section	.rodata._ZTI10IInArchive,"aG",@progbits,_ZTI10IInArchive,comdat
	.weak	_ZTI10IInArchive
	.p2align	4
_ZTI10IInArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IInArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI10IInArchive, 24

	.type	_ZTS15IArchiveOpenSeq,@object # @_ZTS15IArchiveOpenSeq
	.section	.rodata._ZTS15IArchiveOpenSeq,"aG",@progbits,_ZTS15IArchiveOpenSeq,comdat
	.weak	_ZTS15IArchiveOpenSeq
	.p2align	4
_ZTS15IArchiveOpenSeq:
	.asciz	"15IArchiveOpenSeq"
	.size	_ZTS15IArchiveOpenSeq, 18

	.type	_ZTI15IArchiveOpenSeq,@object # @_ZTI15IArchiveOpenSeq
	.section	.rodata._ZTI15IArchiveOpenSeq,"aG",@progbits,_ZTI15IArchiveOpenSeq,comdat
	.weak	_ZTI15IArchiveOpenSeq
	.p2align	4
_ZTI15IArchiveOpenSeq:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15IArchiveOpenSeq
	.quad	_ZTI8IUnknown
	.size	_ZTI15IArchiveOpenSeq, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive5NPpmd8CHandlerE,@object # @_ZTIN8NArchive5NPpmd8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive5NPpmd8CHandlerE
	.p2align	4
_ZTIN8NArchive5NPpmd8CHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive5NPpmd8CHandlerE
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI10IInArchive
	.quad	2                       # 0x2
	.quad	_ZTI15IArchiveOpenSeq
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTIN8NArchive5NPpmd8CHandlerE, 72

	.type	_ZN8NArchive5NPpmdL10g_BigAllocE,@object # @_ZN8NArchive5NPpmdL10g_BigAllocE
	.data
	.p2align	3
_ZN8NArchive5NPpmdL10g_BigAllocE:
	.quad	_ZN8NArchive5NPpmdL10SzBigAllocEPvm
	.quad	_ZN8NArchive5NPpmdL9SzBigFreeEPvS1_
	.size	_ZN8NArchive5NPpmdL10g_BigAllocE, 16

	.type	_ZN8NArchive5NPpmdL9g_ArcInfoE,@object # @_ZN8NArchive5NPpmdL9g_ArcInfoE
	.p2align	3
_ZN8NArchive5NPpmdL9g_ArcInfoE:
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	0
	.byte	13                      # 0xd
	.asciz	"\217\257\254\204\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.zero	3
	.long	4                       # 0x4
	.byte	0                       # 0x0
	.zero	3
	.quad	_ZN8NArchive5NPpmdL9CreateArcEv
	.quad	0
	.size	_ZN8NArchive5NPpmdL9g_ArcInfoE, 80

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.4:
	.long	80                      # 0x50
	.long	112                     # 0x70
	.long	109                     # 0x6d
	.long	100                     # 0x64
	.long	0                       # 0x0
	.size	.L.str.4, 20

	.type	.L.str.5,@object        # @.str.5
	.p2align	2
.L.str.5:
	.long	112                     # 0x70
	.long	109                     # 0x6d
	.long	100                     # 0x64
	.long	0                       # 0x0
	.size	.L.str.5, 16

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_PpmdHandler.ii

	.globl	_ZN8NArchive5NPpmd13CRangeDecoderC1Ev
	.type	_ZN8NArchive5NPpmd13CRangeDecoderC1Ev,@function
_ZN8NArchive5NPpmd13CRangeDecoderC1Ev = _ZN8NArchive5NPpmd13CRangeDecoderC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
