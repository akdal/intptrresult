	.text
	.file	"struct_io.bc"
	.globl	hypre_PrintBoxArrayData
	.p2align	4, 0x90
	.type	hypre_PrintBoxArrayData,@function
hypre_PrintBoxArrayData:                # @hypre_PrintBoxArrayData
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 192
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	cmpl	$0, 8(%rsi)
	jle	.LBB0_17
# BB#1:                                 # %.lr.ph271
	movl	12(%rsp), %eax          # 4-byte Reload
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_5 Depth 2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_10 Depth 4
                                        #           Child Loop BB0_11 Depth 5
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leaq	(,%rcx,8), %rax
	leaq	(%rax,%rax,2), %rbx
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %r13
	movl	20(%r13,%rbx), %eax
	movl	8(%r13,%rbx), %ecx
	movl	%eax, %r15d
	subl	%ecx, %r15d
	incl	%r15d
	cmpl	%ecx, %eax
	movl	12(%r13,%rbx), %eax
	movl	16(%r13,%rbx), %ecx
	movl	4(%r13,%rbx), %edx
	movl	$0, %r14d
	cmovsl	%r14d, %r15d
	movl	%ecx, %r12d
	subl	%edx, %r12d
	incl	%r12d
	cmpl	%edx, %ecx
	movl	(%r13,%rbx), %ecx
	cmovsl	%r14d, %r12d
	movl	%eax, %ebp
	subl	%ecx, %ebp
	incl	%ebp
	cmpl	%ecx, %eax
	movq	(%rsi), %rax
	cmovsl	%r14d, %ebp
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	(%rax,%rbx), %rdi
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	leaq	84(%rsp), %rsi
	callq	hypre_BoxGetSize
	movl	(%r13,%rbx), %ecx
	movl	4(%r13,%rbx), %r11d
	movl	12(%r13,%rbx), %edx
	movl	%edx, %edi
	subl	%ecx, %edi
	incl	%edi
	cmpl	%ecx, %edx
	movl	16(%r13,%rbx), %esi
	cmovsl	%r14d, %edi
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movl	%esi, %eax
	subl	%r11d, %eax
	incl	%eax
	cmpl	%r11d, %esi
	cmovsl	%r14d, %eax
	imull	%r12d, %ebp
	imull	%r15d, %ebp
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	movl	84(%rsp), %esi
	movl	88(%rsp), %edi
	movl	92(%rsp), %ebp
	cmpl	%esi, %edi
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	cmovgel	%edi, %esi
	cmpl	%esi, %ebp
	movl	%ebp, 24(%rsp)          # 4-byte Spill
	cmovgel	%ebp, %esi
	testl	%esi, %esi
	jle	.LBB0_16
# BB#3:                                 # %.lr.ph264
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_16
# BB#4:                                 # %.preheader233.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	8(%r13,%rbx), %r8
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	4(%rdx,%rbx), %rbx
	movl	28(%rsp), %r10d         # 4-byte Reload
	movl	%r10d, %edi
	movl	16(%rsp), %r9d          # 4-byte Reload
	subl	%r9d, %edi
	movl	%eax, %ebp
	movq	40(%rsp), %rdx          # 8-byte Reload
	subl	%edx, %ebp
	imull	%r10d, %ebp
	movl	%ebp, 60(%rsp)          # 4-byte Spill
	leal	-1(%rdx), %ebp
	imull	%edi, %ebp
	addl	%r10d, %ebp
	subl	%r9d, %ebp
	movl	%ebp, 56(%rsp)          # 4-byte Spill
	movslq	20(%rsp), %r15          # 4-byte Folded Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	(%rdi), %edi
	subl	%ecx, %edi
	movl	(%rbx), %ecx
	subl	%r11d, %ecx
	movl	4(%rbx), %ebp
	subl	(%r8), %ebp
	imull	%eax, %ebp
	addl	%ecx, %ebp
	imull	%r10d, %ebp
	addl	%edi, %ebp
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	shlq	$3, %r15
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_5:                                # %.preheader233
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_10 Depth 4
                                        #           Child Loop BB0_11 Depth 5
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_15
# BB#6:                                 # %.preheader232.lr.ph
                                        #   in Loop: Header=BB0_5 Depth=2
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_14
# BB#7:                                 # %.preheader232.us.preheader
                                        #   in Loop: Header=BB0_5 Depth=2
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_8:                                # %.preheader232.us
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_5 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_10 Depth 4
                                        #           Child Loop BB0_11 Depth 5
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_13
# BB#9:                                 # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB0_8 Depth=3
	movslq	8(%rsp), %rax           # 4-byte Folded Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %r14
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_10:                               # %.preheader.us.us
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_5 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_11 Depth 5
	movq	%r14, 112(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	movl	%edx, 48(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB0_11:                               #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_5 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        #         Parent Loop BB0_10 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ecx
	addl	%edx, %ecx
	movl	(%rbx), %r8d
	addl	%r13d, %r8d
	movl	4(%rbx), %r9d
	addl	%r12d, %r9d
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	movl	%ebp, (%rsp)
	movl	$.L.str, %esi
	movb	$1, %al
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movl	48(%rsp), %edx          # 4-byte Reload
	incq	%rbp
	addq	%r15, %r14
	cmpq	%rbp, 120(%rsp)         # 8-byte Folded Reload
	jne	.LBB0_11
# BB#12:                                # %._crit_edge.us.us
                                        #   in Loop: Header=BB0_10 Depth=4
	incl	%edx
	movq	112(%rsp), %r14         # 8-byte Reload
	addq	$8, %r14
	cmpl	16(%rsp), %edx          # 4-byte Folded Reload
	jne	.LBB0_10
.LBB0_13:                               # %._crit_edge239.us
                                        #   in Loop: Header=BB0_8 Depth=3
	movl	8(%rsp), %eax           # 4-byte Reload
	addl	28(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 8(%rsp)           # 4-byte Spill
	incl	%r13d
	cmpl	40(%rsp), %r13d         # 4-byte Folded Reload
	jne	.LBB0_8
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_14:                               # %.preheader232.preheader
                                        #   in Loop: Header=BB0_5 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
	addl	56(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 8(%rsp)           # 4-byte Spill
.LBB0_15:                               # %._crit_edge244
                                        #   in Loop: Header=BB0_5 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
	addl	60(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 8(%rsp)           # 4-byte Spill
	incl	%r12d
	cmpl	24(%rsp), %r12d         # 4-byte Folded Reload
	jne	.LBB0_5
.LBB0_16:                               # %._crit_edge265
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	20(%rsp), %eax          # 4-byte Reload
	imull	12(%rsp), %eax          # 4-byte Folded Reload
	cltq
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movq	96(%rsp), %rsi          # 8-byte Reload
	movslq	8(%rsi), %rax
	cmpq	%rax, %rcx
	jl	.LBB0_2
.LBB0_17:                               # %._crit_edge272
	xorl	%eax, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_PrintBoxArrayData, .Lfunc_end0-hypre_PrintBoxArrayData
	.cfi_endproc

	.globl	hypre_ReadBoxArrayData
	.p2align	4, 0x90
	.type	hypre_ReadBoxArrayData,@function
hypre_ReadBoxArrayData:                 # @hypre_ReadBoxArrayData
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 192
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	%rdi, %rbx
	cmpl	$0, 8(%rsi)
	jle	.LBB1_17
# BB#1:                                 # %.lr.ph256
	movl	12(%rsp), %eax          # 4-byte Reload
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leaq	132(%rsp), %r13
	xorl	%ecx, %ecx
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
                                        #       Child Loop BB1_8 Depth 3
                                        #         Child Loop BB1_10 Depth 4
                                        #           Child Loop BB1_11 Depth 5
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	(,%rcx,8), %rax
	leaq	(%rax,%rax,2), %r12
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %r14
	movl	20(%r14,%r12), %eax
	movl	8(%r14,%r12), %ecx
	movl	%eax, %edi
	subl	%ecx, %edi
	incl	%edi
	cmpl	%ecx, %eax
	movl	12(%r14,%r12), %eax
	movl	16(%r14,%r12), %ecx
	movl	4(%r14,%r12), %edx
	movl	$0, %ebp
	cmovsl	%ebp, %edi
	movl	%edi, 56(%rsp)          # 4-byte Spill
	movl	%ecx, %r15d
	subl	%edx, %r15d
	incl	%r15d
	cmpl	%edx, %ecx
	movl	(%r14,%r12), %ecx
	cmovsl	%ebp, %r15d
	xorl	%edx, %edx
	movl	%eax, %ebp
	subl	%ecx, %ebp
	incl	%ebp
	cmpl	%ecx, %eax
	movq	(%rsi), %rax
	cmovsl	%edx, %ebp
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	(%rax,%r12), %rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	leaq	92(%rsp), %rsi
	callq	hypre_BoxGetSize
	movl	(%r14,%r12), %ecx
	movl	4(%r14,%r12), %r11d
	movl	12(%r14,%r12), %edx
	movl	%edx, %edi
	subl	%ecx, %edi
	incl	%edi
	cmpl	%ecx, %edx
	movl	16(%r14,%r12), %esi
	movl	$0, %eax
	cmovsl	%eax, %edi
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movl	%esi, %edx
	subl	%r11d, %edx
	incl	%edx
	cmpl	%r11d, %esi
	cmovsl	%eax, %edx
	imull	%r15d, %ebp
	imull	56(%rsp), %ebp          # 4-byte Folded Reload
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	movl	92(%rsp), %esi
	movl	96(%rsp), %edi
	movl	100(%rsp), %ebp
	cmpl	%esi, %edi
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	cmovgel	%edi, %esi
	cmpl	%esi, %ebp
	movl	%ebp, 24(%rsp)          # 4-byte Spill
	cmovgel	%ebp, %esi
	testl	%esi, %esi
	jle	.LBB1_16
# BB#3:                                 # %.lr.ph250
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_16
# BB#4:                                 # %.preheader225.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	leaq	8(%r14,%r12), %r8
	movl	28(%rsp), %r10d         # 4-byte Reload
	movl	%r10d, %edi
	movl	16(%rsp), %r9d          # 4-byte Reload
	subl	%r9d, %edi
	movl	%edx, %esi
	movq	40(%rsp), %rbp          # 8-byte Reload
	subl	%ebp, %esi
	imull	%r10d, %esi
	movl	%esi, 72(%rsp)          # 4-byte Spill
	leal	-1(%rbp), %esi
	imull	%edi, %esi
	addl	%r10d, %esi
	subl	%r9d, %esi
	movl	%esi, 68(%rsp)          # 4-byte Spill
	movslq	20(%rsp), %r14          # 4-byte Folded Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	(%rsi), %edi
	subl	%ecx, %edi
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rcx,2), %rcx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	4(%rsi,%rcx,8), %ebp
	subl	%r11d, %ebp
	movl	8(%rsi,%rcx,8), %eax
	subl	(%r8), %eax
	imull	%edx, %eax
	addl	%ebp, %eax
	imull	%r10d, %eax
	addl	%edi, %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	shlq	$3, %r14
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_5:                                # %.preheader225
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_8 Depth 3
                                        #         Child Loop BB1_10 Depth 4
                                        #           Child Loop BB1_11 Depth 5
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	movl	%eax, 76(%rsp)          # 4-byte Spill
	jle	.LBB1_15
# BB#6:                                 # %.preheader224.lr.ph
                                        #   in Loop: Header=BB1_5 Depth=2
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_14
# BB#7:                                 # %.preheader224.us.preheader
                                        #   in Loop: Header=BB1_5 Depth=2
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_8:                                # %.preheader224.us
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_5 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_10 Depth 4
                                        #           Child Loop BB1_11 Depth 5
	movl	%eax, 48(%rsp)          # 4-byte Spill
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_13
# BB#9:                                 # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB1_8 Depth=3
	movslq	(%rsp), %rax            # 4-byte Folded Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rbp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_10:                               # %.preheader.us.us
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_5 Depth=2
                                        #       Parent Loop BB1_8 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB1_11 Depth 5
	movq	120(%rsp), %r12         # 8-byte Reload
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_11:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_5 Depth=2
                                        #       Parent Loop BB1_8 Depth=3
                                        #         Parent Loop BB1_10 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	$.L.str.1, %esi
	movl	$0, %eax
	movq	%rbx, %rdi
	movq	%r13, %rdx
	movq	%r13, %rcx
	movq	%r13, %r8
	movq	%r13, %r9
	pushq	%rbp
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	callq	fscanf
	addq	$16, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset -16
	addq	%r14, %rbp
	decq	%r12
	jne	.LBB1_11
# BB#12:                                # %._crit_edge.us.us
                                        #   in Loop: Header=BB1_10 Depth=4
	incl	%r15d
	movq	56(%rsp), %rbp          # 8-byte Reload
	addq	$8, %rbp
	cmpl	16(%rsp), %r15d         # 4-byte Folded Reload
	jne	.LBB1_10
.LBB1_13:                               # %._crit_edge230.us
                                        #   in Loop: Header=BB1_8 Depth=3
	movl	(%rsp), %eax            # 4-byte Reload
	addl	28(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	48(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	40(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB1_8
	jmp	.LBB1_15
	.p2align	4, 0x90
.LBB1_14:                               # %.preheader224.preheader
                                        #   in Loop: Header=BB1_5 Depth=2
	movl	(%rsp), %eax            # 4-byte Reload
	addl	68(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, (%rsp)            # 4-byte Spill
.LBB1_15:                               # %._crit_edge233
                                        #   in Loop: Header=BB1_5 Depth=2
	movl	(%rsp), %eax            # 4-byte Reload
	addl	72(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	76(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	24(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB1_5
.LBB1_16:                               # %._crit_edge251
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	20(%rsp), %eax          # 4-byte Reload
	imull	12(%rsp), %eax          # 4-byte Folded Reload
	cltq
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movq	104(%rsp), %rsi         # 8-byte Reload
	movslq	8(%rsi), %rax
	cmpq	%rax, %rcx
	jl	.LBB1_2
.LBB1_17:                               # %._crit_edge257
	xorl	%eax, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_ReadBoxArrayData, .Lfunc_end1-hypre_ReadBoxArrayData
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d: (%d, %d, %d; %d) %e\n"
	.size	.L.str, 25

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%d: (%d, %d, %d; %d) %le\n"
	.size	.L.str.1, 26


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
