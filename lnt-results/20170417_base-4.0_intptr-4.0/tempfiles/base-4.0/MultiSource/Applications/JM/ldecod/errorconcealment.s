	.text
	.file	"errorconcealment.bc"
	.globl	set_ec_flag
	.p2align	4, 0x90
	.type	set_ec_flag,@function
set_ec_flag:                            # @set_ec_flag
	.cfi_startproc
# BB#0:
	cmpl	$17, %edi
	ja	.LBB0_20
# BB#1:
	movl	%edi, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_2:
	movl	$1, ec_flag(%rip)
.LBB0_3:
	movl	$1, ec_flag+4(%rip)
.LBB0_4:
	movl	$1, ec_flag+8(%rip)
.LBB0_5:
	movl	$1, ec_flag+12(%rip)
.LBB0_6:                                # %.thread.sink.split
	movl	$1, ec_flag+20(%rip)
.LBB0_7:                                # %.thread
	movl	$1, ec_flag+44(%rip)
.LBB0_8:
	movl	$1, ec_flag+48(%rip)
.LBB0_9:
	movl	$1, ec_flag+52(%rip)
.LBB0_10:
	movl	$1, ec_flag+56(%rip)
.LBB0_11:
	movl	$1, ec_flag+60(%rip)
	movl	$1, %eax
	retq
.LBB0_12:                               # %.thread2
	movl	$1, ec_flag+16(%rip)
.LBB0_13:
	movl	$1, ec_flag+24(%rip)
.LBB0_14:
	movl	$1, ec_flag+28(%rip)
.LBB0_15:
	movl	$1, ec_flag+32(%rip)
.LBB0_16:
	movl	$1, ec_flag+36(%rip)
.LBB0_17:
	movl	$1, ec_flag+40(%rip)
	movl	$1, %eax
	retq
.LBB0_18:
	movl	$1, ec_flag+64(%rip)
	movl	$1, %eax
	retq
.LBB0_19:
	movl	$1, ec_flag+68(%rip)
.LBB0_20:
	movl	$1, %eax
	retq
.Lfunc_end0:
	.size	set_ec_flag, .Lfunc_end0-set_ec_flag
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_2
	.quad	.LBB0_3
	.quad	.LBB0_4
	.quad	.LBB0_5
	.quad	.LBB0_12
	.quad	.LBB0_6
	.quad	.LBB0_13
	.quad	.LBB0_14
	.quad	.LBB0_15
	.quad	.LBB0_16
	.quad	.LBB0_17
	.quad	.LBB0_7
	.quad	.LBB0_8
	.quad	.LBB0_9
	.quad	.LBB0_10
	.quad	.LBB0_11
	.quad	.LBB0_18
	.quad	.LBB0_19

	.text
	.globl	reset_ec_flags
	.p2align	4, 0x90
	.type	reset_ec_flags,@function
reset_ec_flags:                         # @reset_ec_flags
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, ec_flag+64(%rip)
	movaps	%xmm0, ec_flag+48(%rip)
	movaps	%xmm0, ec_flag+32(%rip)
	movaps	%xmm0, ec_flag+16(%rip)
	movaps	%xmm0, ec_flag(%rip)
	retq
.Lfunc_end1:
	.size	reset_ec_flags, .Lfunc_end1-reset_ec_flags
	.cfi_endproc

	.globl	get_concealed_element
	.p2align	4, 0x90
	.type	get_concealed_element,@function
get_concealed_element:                  # @get_concealed_element
	.cfi_startproc
# BB#0:
	movslq	(%rdi), %rcx
	cmpl	$0, ec_flag(,%rcx,4)
	je	.LBB2_1
# BB#2:
	movl	$1, %eax
	cmpl	$17, %ecx
	ja	.LBB2_4
# BB#3:                                 # %switch.lookup
	movl	.Lswitch.table(,%rcx,4), %ecx
	movl	%ecx, 12(%rdi)
	movl	$0, 16(%rdi)
.LBB2_4:
	retq
.LBB2_1:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	get_concealed_element, .Lfunc_end2-get_concealed_element
	.cfi_endproc

	.type	ec_flag,@object         # @ec_flag
	.local	ec_flag
	.comm	ec_flag,80,16
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.long	31                      # 0x1f
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.size	.Lswitch.table, 72


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
