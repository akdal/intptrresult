	.text
	.file	"btConvexShape.bc"
	.globl	_ZN13btConvexShapeC2Ev
	.p2align	4, 0x90
	.type	_ZN13btConvexShapeC2Ev,@function
_ZN13btConvexShapeC2Ev:                 # @_ZN13btConvexShapeC2Ev
	.cfi_startproc
# BB#0:
	movl	$35, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$_ZTV13btConvexShape+16, (%rdi)
	retq
.Lfunc_end0:
	.size	_ZN13btConvexShapeC2Ev, .Lfunc_end0-_ZN13btConvexShapeC2Ev
	.cfi_endproc

	.globl	_ZN13btConvexShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN13btConvexShapeD2Ev,@function
_ZN13btConvexShapeD2Ev:                 # @_ZN13btConvexShapeD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN13btConvexShapeD2Ev, .Lfunc_end1-_ZN13btConvexShapeD2Ev
	.cfi_endproc

	.globl	_ZN13btConvexShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN13btConvexShapeD0Ev,@function
_ZN13btConvexShapeD0Ev:                 # @_ZN13btConvexShapeD0Ev
	.cfi_startproc
# BB#0:
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.Lfunc_end2:
	.size	_ZN13btConvexShapeD0Ev, .Lfunc_end2-_ZN13btConvexShapeD0Ev
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	3713928043              # float -9.99999984E+17
.LCPI3_1:
	.long	1065353216              # float 1
.LCPI3_2:
	.long	953267991               # float 9.99999974E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3,@function
_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3: # @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$112, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 144
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	8(%rbx), %eax
	cmpq	$13, %rax
	ja	.LBB3_4
# BB#1:
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_2:
	movss	40(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	44(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	.LCPI3_3(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm0
	cmpless	(%rsi), %xmm0
	movaps	%xmm0, %xmm4
	andps	%xmm2, %xmm0
	movaps	%xmm2, %xmm5
	xorps	%xmm1, %xmm5
	xorps	%xmm2, %xmm2
	andnps	%xmm5, %xmm4
	orps	%xmm4, %xmm0
	xorps	%xmm4, %xmm4
	cmpless	4(%rsi), %xmm4
	movaps	%xmm4, %xmm5
	andps	%xmm3, %xmm4
	xorps	%xmm1, %xmm3
	andnps	%xmm3, %xmm5
	orps	%xmm5, %xmm4
	movss	48(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm1
	cmpless	8(%rsi), %xmm2
	movaps	%xmm2, %xmm5
	andnps	%xmm1, %xmm5
	andps	%xmm3, %xmm2
	orps	%xmm5, %xmm2
	unpcklps	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	jmp	.LBB3_32
.LBB3_3:
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	80(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	84(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm3, %xmm6
	addps	%xmm5, %xmm6
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	88(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	addps	%xmm6, %xmm5
	mulss	96(%rbx), %xmm2
	mulss	100(%rbx), %xmm1
	addss	%xmm2, %xmm1
	mulss	104(%rbx), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm5, (%rsp)
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm1, 4(%rsp)
	movss	%xmm0, 8(%rsp)
	movl	$0, 12(%rsp)
	xorl	%eax, %eax
	ucomiss	%xmm5, %xmm1
	seta	%al
	ucomiss	(%rsp,%rax,4), %xmm0
	movl	$2, %ecx
	cmovbeq	%rax, %rcx
	shlq	$4, %rcx
	movsd	64(%rbx,%rcx), %xmm0    # xmm0 = mem[0],zero
	movss	72(%rbx,%rcx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	jmp	.LBB3_35
.LBB3_4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*104(%rax)
	jmp	.LBB3_35
.LBB3_5:
	movq	120(%rbx), %rax
	movl	108(%rbx), %ecx
	movq	24(%rbx), %xmm0         # xmm0 = mem[0],zero
	movss	32(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	testl	%ecx, %ecx
	jle	.LBB3_30
# BB#6:                                 # %.lr.ph.preheader.i
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	mulss	4(%rsi), %xmm1
	movss	8(%rsi), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	leaq	8(%rax), %rdx
	movss	.LCPI3_0(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movl	$-1, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movss	-8(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	movss	-4(%rdx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	addss	%xmm6, %xmm7
	movss	(%rdx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm6
	addss	%xmm7, %xmm6
	ucomiss	%xmm5, %xmm6
	maxss	%xmm5, %xmm6
	cmoval	%edi, %esi
	incq	%rdi
	addq	$16, %rdx
	cmpq	%rdi, %rcx
	movaps	%xmm6, %xmm5
	jne	.LBB3_7
	jmp	.LBB3_11
.LBB3_8:
	movq	104(%rbx), %rax
	movl	112(%rbx), %ecx
	movq	24(%rbx), %xmm0         # xmm0 = mem[0],zero
	movss	32(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	testl	%ecx, %ecx
	jle	.LBB3_30
# BB#9:                                 # %.lr.ph.preheader.i164
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	mulss	4(%rsi), %xmm1
	movss	8(%rsi), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	leaq	8(%rax), %rdx
	movss	.LCPI3_0(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movl	$-1, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_10:                               # %.lr.ph.i181
                                        # =>This Inner Loop Header: Depth=1
	movss	-8(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	movss	-4(%rdx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	addss	%xmm6, %xmm7
	movss	(%rdx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm6
	addss	%xmm7, %xmm6
	ucomiss	%xmm5, %xmm6
	maxss	%xmm5, %xmm6
	cmoval	%edi, %esi
	incq	%rdi
	addq	$16, %rdx
	cmpq	%rdi, %rcx
	movaps	%xmm6, %xmm5
	jne	.LBB3_10
.LBB3_11:                               # %._crit_edge.loopexit.i166
	movslq	%esi, %rcx
	jmp	.LBB3_31
.LBB3_12:
	movq	(%rsi), %xmm12          # xmm12 = mem[0],zero
	pshufd	$229, %xmm12, %xmm1     # xmm1 = xmm12[1,1,2,3]
	movss	8(%rsi), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movslq	64(%rbx), %r14
	movss	40(%rbx,%r14,4), %xmm13 # xmm13 = mem[0],zero,zero,zero
	leal	2(%r14), %eax
	cltq
	imulq	$1431655766, %rax, %rcx # imm = 0x55555556
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$32, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx,2), %ecx
	subl	%ecx, %eax
	cltq
	movss	40(%rbx,%rax,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movdqa	%xmm12, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm11, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI3_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI3_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB3_16
# BB#13:
	xorps	%xmm11, %xmm11
	movaps	%xmm2, %xmm12
	jmp	.LBB3_19
.LBB3_14:
	movups	40(%rbx), %xmm0
	movaps	%xmm0, (%rsp)
	movl	(%rsi), %eax
	movl	%eax, 16(%rsp)
	movl	4(%rsi), %ecx
	movl	%ecx, 20(%rsp)
	movl	8(%rsi), %edx
	movl	%edx, 24(%rsp)
	movl	$0, 28(%rsp)
	movslq	64(%rbx), %rax
	cmpq	$2, %rax
	ja	.LBB3_24
# BB#15:                                # %switch.lookup
	movq	.Lswitch.table(,%rax,8), %r14
	movl	%eax, %r15d
	movq	.Lswitch.table.1(,%rax,8), %rbx
	movd	16(%rsp,%rbx,4), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movd	16(%rsp,%r14,4), %xmm6  # xmm6 = mem[0],zero,zero,zero
	jmp	.LBB3_25
.LBB3_30:
	movq	$-1, %rcx
.LBB3_31:                               # %_ZL17convexHullSupportRK9btVector3PS0_iS1_.exit182
	shlq	$4, %rcx
	movsd	(%rax,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulps	%xmm1, %xmm0
	mulss	8(%rax,%rcx), %xmm2
.LBB3_32:
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	jmp	.LBB3_35
.LBB3_16:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB3_18
# BB#17:                                # %call.sqrt474
	movss	%xmm11, 96(%rsp)        # 4-byte Spill
	movdqa	%xmm12, 80(%rsp)        # 16-byte Spill
	movaps	%xmm13, 64(%rsp)        # 16-byte Spill
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	callq	sqrtf
	movss	.LCPI3_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	movaps	64(%rsp), %xmm13        # 16-byte Reload
	movdqa	80(%rsp), %xmm12        # 16-byte Reload
	movss	96(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB3_18:                               # %.split
	divss	%xmm1, %xmm2
	mulss	%xmm2, %xmm11
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm2, %xmm12
.LBB3_19:
	xorps	%xmm8, %xmm8
	movaps	%xmm8, (%rsp)
	movss	%xmm13, (%rsp,%r14,4)
	movsd	24(%rbx), %xmm2         # xmm2 = mem[0],zero
	mulps	%xmm12, %xmm2
	movss	32(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	movaps	%xmm3, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	mulss	%xmm3, %xmm1
	movaps	(%rsp), %xmm3
	addps	%xmm0, %xmm3
	movss	8(%rsp), %xmm10         # xmm10 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm10
	movss	56(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm9
	mulss	%xmm4, %xmm9
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm12, %xmm4
	subps	%xmm4, %xmm3
	subss	%xmm9, %xmm10
	movaps	%xmm3, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movaps	%xmm12, %xmm6
	mulss	%xmm3, %xmm6
	movaps	%xmm12, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	mulss	%xmm2, %xmm5
	addss	%xmm6, %xmm5
	movaps	%xmm11, %xmm7
	mulss	%xmm10, %xmm7
	addss	%xmm5, %xmm7
	movss	.LCPI3_0(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	ucomiss	%xmm5, %xmm7
	ja	.LBB3_21
# BB#20:
	xorps	%xmm3, %xmm3
.LBB3_21:
	movaps	%xmm7, %xmm6
	maxss	%xmm5, %xmm6
	xorps	.LCPI3_3(%rip), %xmm13
	movaps	%xmm8, (%rsp)
	movss	%xmm13, (%rsp,%r14,4)
	addps	(%rsp), %xmm0
	addss	8(%rsp), %xmm1
	subps	%xmm4, %xmm0
	subss	%xmm9, %xmm1
	movaps	%xmm0, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	mulss	%xmm0, %xmm12
	mulss	%xmm4, %xmm2
	addss	%xmm12, %xmm2
	mulss	%xmm1, %xmm11
	addss	%xmm2, %xmm11
	ucomiss	%xmm6, %xmm11
	ja	.LBB3_23
# BB#22:
	movaps	%xmm3, %xmm0
.LBB3_23:
	cmpltss	%xmm7, %xmm5
	andps	%xmm10, %xmm5
	cmpltss	%xmm11, %xmm6
	andps	%xmm6, %xmm1
	andnps	%xmm5, %xmm6
	orps	%xmm1, %xmm6
	xorps	%xmm1, %xmm1
	movss	%xmm6, %xmm1            # xmm1 = xmm6[0],xmm1[1,2,3]
	jmp	.LBB3_35
.LBB3_24:
	xorl	%r15d, %r15d
	movd	%ecx, %xmm4
	movd	%edx, %xmm6
	movl	$1, %ebx
	movl	$2, %r14d
.LBB3_25:                               # %._crit_edge
	movss	(%rsp,%rbx,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	movss	(%rsp,%rax,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movdqa	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	movdqa	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB3_27
# BB#26:                                # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm3, 96(%rsp)         # 16-byte Spill
	movd	%xmm4, 80(%rsp)         # 4-byte Folded Spill
	movss	%xmm5, 64(%rsp)         # 4-byte Spill
	movd	%xmm6, 48(%rsp)         # 4-byte Folded Spill
	callq	sqrtf
	movd	48(%rsp), %xmm6         # 4-byte Folded Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	64(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movd	80(%rsp), %xmm4         # 4-byte Folded Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	96(%rsp), %xmm3         # 16-byte Reload
.LBB3_27:                               # %._crit_edge.split
	movss	16(%rsp,%r15,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB3_28
	jnp	.LBB3_33
.LBB3_28:
	divss	%xmm0, %xmm5
	mulss	%xmm5, %xmm4
	xorps	%xmm0, %xmm0
	cmpltss	%xmm0, %xmm2
	movaps	%xmm2, %xmm0
	andnps	%xmm3, %xmm0
	xorps	.LCPI3_3(%rip), %xmm3
	andps	%xmm2, %xmm3
	orps	%xmm0, %xmm3
	movaps	%xmm5, %xmm1
	mulss	%xmm6, %xmm1
	movaps	%xmm4, %xmm5
	jmp	.LBB3_34
.LBB3_33:
	cmpltss	%xmm1, %xmm2
	movaps	%xmm2, %xmm0
	andnps	%xmm3, %xmm0
	xorps	.LCPI3_3(%rip), %xmm3
	andps	%xmm2, %xmm3
	orps	%xmm0, %xmm3
.LBB3_34:
	movss	%xmm5, 32(%rsp,%rbx,4)
	movss	%xmm3, 32(%rsp,%r15,4)
	movss	%xmm1, 32(%rsp,%r14,4)
	movsd	32(%rsp), %xmm0         # xmm0 = mem[0],zero
	movss	40(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
.LBB3_35:
	addq	$112, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3, .Lfunc_end3-_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_2
	.quad	.LBB3_3
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_5
	.quad	.LBB3_8
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_35
	.quad	.LBB3_4
	.quad	.LBB3_12
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_14

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	679477248               # float 1.42108547E-14
.LCPI4_1:
	.long	3212836864              # float -1
.LCPI4_2:
	.long	1065353216              # float 1
	.text
	.globl	_ZNK13btConvexShape31localGetSupportVertexNonVirtualERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK13btConvexShape31localGetSupportVertexNonVirtualERK9btVector3,@function
_ZNK13btConvexShape31localGetSupportVertexNonVirtualERK9btVector3: # @_ZNK13btConvexShape31localGetSupportVertexNonVirtualERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
	subq	$64, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 80
.Lcfi9:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movups	(%rsi), %xmm0
	movaps	%xmm0, (%rsp)
	movss	(%rsp), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rsp), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB4_2
# BB#1:
	movabsq	$-4647714812233515008, %rax # imm = 0xBF800000BF800000
	movq	%rax, (%rsp)
	movl	$3212836864, %eax       # imm = 0xBF800000
	movq	%rax, 8(%rsp)
	movss	.LCPI4_1(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm3
	movaps	%xmm4, %xmm5
.LBB4_2:
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm3, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB4_4
# BB#3:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm3, 48(%rsp)         # 4-byte Spill
	movss	%xmm4, 32(%rsp)         # 4-byte Spill
	movss	%xmm5, 28(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	28(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	48(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB4_4:                                # %.split
	movss	.LCPI4_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm5
	movss	%xmm5, (%rsp)
	mulss	%xmm1, %xmm3
	movss	%xmm3, 4(%rsp)
	mulss	%xmm4, %xmm1
	movss	%xmm1, 8(%rsp)
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3
	movaps	%xmm0, %xmm4
	movaps	%xmm1, %xmm3
	movl	8(%rbx), %eax
	cmpq	$13, %rax
	ja	.LBB4_8
# BB#5:                                 # %.split
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_7:
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
.LBB4_9:                                # %_ZNK13btConvexShape19getMarginNonVirtualEv.exit
	movss	(%rsp), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	mulss	8(%rsp), %xmm0
	addss	%xmm4, %xmm2
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	addss	%xmm1, %xmm4
	addss	%xmm0, %xmm3
	unpcklps	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movaps	%xmm2, %xmm0
	addq	$64, %rsp
	popq	%rbx
	retq
.LBB4_8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	movaps	%xmm4, 32(%rsp)         # 16-byte Spill
	callq	*88(%rax)
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	jmp	.LBB4_9
.LBB4_6:
	movss	40(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	24(%rbx), %xmm0
	jmp	.LBB4_9
.Lfunc_end4:
	.size	_ZNK13btConvexShape31localGetSupportVertexNonVirtualERK9btVector3, .Lfunc_end4-_ZNK13btConvexShape31localGetSupportVertexNonVirtualERK9btVector3
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_7
	.quad	.LBB4_7
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_7
	.quad	.LBB4_7
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_6
	.quad	.LBB4_8
	.quad	.LBB4_7
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_7

	.text
	.globl	_ZNK13btConvexShape19getMarginNonVirtualEv
	.p2align	4, 0x90
	.type	_ZNK13btConvexShape19getMarginNonVirtualEv,@function
_ZNK13btConvexShape19getMarginNonVirtualEv: # @_ZNK13btConvexShape19getMarginNonVirtualEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	cmpq	$13, %rax
	ja	.LBB5_3
# BB#1:
	jmpq	*.LJTI5_0(,%rax,8)
.LBB5_2:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.LBB5_3:
	movq	(%rdi), %rax
	jmpq	*88(%rax)               # TAILCALL
.LBB5_4:
	movss	40(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	24(%rdi), %xmm0
	retq
.Lfunc_end5:
	.size	_ZNK13btConvexShape19getMarginNonVirtualEv, .Lfunc_end5-_ZNK13btConvexShape19getMarginNonVirtualEv
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_2
	.quad	.LBB5_2
	.quad	.LBB5_3
	.quad	.LBB5_3
	.quad	.LBB5_2
	.quad	.LBB5_2
	.quad	.LBB5_3
	.quad	.LBB5_3
	.quad	.LBB5_4
	.quad	.LBB5_3
	.quad	.LBB5_2
	.quad	.LBB5_3
	.quad	.LBB5_3
	.quad	.LBB5_2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI6_2:
	.zero	16
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_1:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZNK13btConvexShape17getAabbNonVirtualERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK13btConvexShape17getAabbNonVirtualERK11btTransformR9btVector3S4_,@function
_ZNK13btConvexShape17getAabbNonVirtualERK11btTransformR9btVector3S4_: # @_ZNK13btConvexShape17getAabbNonVirtualERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 112
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	8(%r12), %eax
	cmpq	$13, %rax
	ja	.LBB6_10
# BB#1:
	jmpq	*.LJTI6_0(,%rax,8)
.LBB6_2:                                # %_ZNK13btConvexShape19getMarginNonVirtualEv.exit119
	movss	56(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	40(%r12), %xmm10        # xmm10 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm10
	movss	44(%r12), %xmm11        # xmm11 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm11
	addss	48(%r12), %xmm0
	movss	16(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movaps	.LCPI6_0(%rip), %xmm2   # xmm2 = [nan,nan,nan,nan]
	andps	%xmm2, %xmm4
	movss	20(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	andps	%xmm2, %xmm3
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	andps	%xmm2, %xmm6
	movss	32(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm7
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm1
	movss	40(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm5
	movsd	48(%rbx), %xmm9         # xmm9 = mem[0],zero
	movss	56(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm7
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm4, %xmm10
	mulss	%xmm11, %xmm1
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm3, %xmm11
	addps	%xmm10, %xmm11
	mulss	%xmm0, %xmm5
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm6, %xmm0
	addps	%xmm11, %xmm0
	addss	%xmm7, %xmm1
	addss	%xmm1, %xmm5
	movaps	%xmm9, %xmm1
	subps	%xmm0, %xmm1
	movaps	%xmm8, %xmm2
	subss	%xmm5, %xmm2
	xorps	%xmm3, %xmm3
	xorps	%xmm4, %xmm4
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
	movlps	%xmm1, (%r15)
	movlps	%xmm4, 8(%r15)
	addps	%xmm9, %xmm0
	addss	%xmm8, %xmm5
	movss	%xmm5, %xmm3            # xmm3 = xmm5[0],xmm3[1,2,3]
	movlps	%xmm0, (%r14)
	movlps	%xmm3, 8(%r14)
	jmp	.LBB6_12
.LBB6_6:
	andl	$-2, %eax
	cmpl	$4, %eax
	jne	.LBB6_8
# BB#7:
	movss	56(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB6_9
.LBB6_3:                                # %_ZNK13btConvexShape19getMarginNonVirtualEv.exit147
	movss	56(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	xorl	%r13d, %r13d
	leaq	40(%rsp), %rbp
	.p2align	4, 0x90
.LBB6_4:                                # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$1065353216, (%rsp,%r13,4) # imm = 0x3F800000
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movsd	(%rbx), %xmm3           # xmm3 = mem[0],zero
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	movsd	16(%rbx), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm1, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm4, %xmm5
	movsd	32(%rbx), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm5, %xmm4
	mulss	8(%rbx), %xmm0
	mulss	24(%rbx), %xmm1
	addss	%xmm0, %xmm1
	mulss	40(%rbx), %xmm2
	addss	%xmm1, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm4, 24(%rsp)
	movlps	%xmm0, 32(%rsp)
	movq	%r12, %rdi
	leaq	24(%rsp), %rsi
	callq	_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3
	movaps	%xmm0, %xmm2
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm2, %xmm5
	movss	20(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0],xmm6[1],xmm2[1]
	mulps	%xmm0, %xmm6
	addps	%xmm5, %xmm6
	movss	40(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	24(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm1, %xmm5
	addps	%xmm6, %xmm5
	movsd	48(%rbx), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm5, %xmm1
	movss	36(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm3, %xmm4
	addss	%xmm4, %xmm2
	addss	56(%rbx), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, 24(%rsp)
	movlps	%xmm0, 32(%rsp)
	movss	24(%rsp,%r13,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	20(%rsp), %xmm0         # 4-byte Folded Reload
	movss	%xmm0, (%r14,%r13,4)
	movl	$-1082130432, (%rsp,%r13,4) # imm = 0xBF800000
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movsd	(%rbx), %xmm3           # xmm3 = mem[0],zero
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	movsd	16(%rbx), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm1, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm4, %xmm5
	movsd	32(%rbx), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm5, %xmm4
	mulss	8(%rbx), %xmm0
	mulss	24(%rbx), %xmm1
	addss	%xmm0, %xmm1
	mulss	40(%rbx), %xmm2
	addss	%xmm1, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm4, 40(%rsp)
	movlps	%xmm0, 48(%rsp)
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3
	movaps	%xmm0, %xmm2
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	movss	20(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm0, %xmm6
	addps	%xmm2, %xmm6
	movss	40(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	24(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm1, %xmm5
	addps	%xmm6, %xmm5
	movsd	48(%rbx), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm5, %xmm1
	movss	36(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm3, %xmm4
	addss	%xmm4, %xmm2
	addss	56(%rbx), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, 24(%rsp)
	movlps	%xmm0, 32(%rsp)
	movss	24(%rsp,%r13,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	subss	20(%rsp), %xmm0         # 4-byte Folded Reload
	movss	%xmm0, (%r15,%r13,4)
	incq	%r13
	cmpq	$3, %r13
	jne	.LBB6_4
	jmp	.LBB6_12
.LBB6_10:
	movq	(%r12), %rax
	movq	16(%rax), %rax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB6_11:                               # %_ZNK13btConvexShape19getMarginNonVirtualEv.exit
	movss	40(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm0, %xmm1
	movsd	48(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm1, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	subps	%xmm2, %xmm0
	movss	56(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm3
	xorps	%xmm4, %xmm4
	xorps	%xmm5, %xmm5
	movss	%xmm3, %xmm5            # xmm5 = xmm3[0],xmm5[1,2,3]
	movlps	%xmm0, (%r15)
	movlps	%xmm5, 8(%r15)
	movsd	48(%rbx), %xmm0         # xmm0 = mem[0],zero
	addps	%xmm2, %xmm0
	addss	56(%rbx), %xmm1
	movss	%xmm1, %xmm4            # xmm4 = xmm1[0],xmm4[1,2,3]
	movlps	%xmm0, (%r14)
	movlps	%xmm4, 8(%r14)
	jmp	.LBB6_12
.LBB6_5:                                # %_ZNK13btConvexShape19getMarginNonVirtualEv.exit159
	movslq	64(%r12), %rax
	leal	2(%rax), %ecx
	movslq	%ecx, %rcx
	imulq	$1431655766, %rcx, %rdx # imm = 0x55555556
	movq	%rdx, %rsi
	shrq	$63, %rsi
	shrq	$32, %rdx
	addl	%esi, %edx
	leal	(%rdx,%rdx,2), %edx
	subl	%edx, %ecx
	movslq	%ecx, %rcx
	movl	40(%r12,%rcx,4), %ecx
	movl	%ecx, (%rsp)
	movl	%ecx, 4(%rsp)
	movl	%ecx, 8(%rsp)
	movl	$0, 12(%rsp)
	movd	%ecx, %xmm0
	addss	40(%r12,%rax,4), %xmm0
	movss	%xmm0, (%rsp,%rax,4)
	movss	56(%r12), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	(%rsp), %xmm2           # xmm2 = mem[0],zero,zero,zero
	addss	%xmm10, %xmm2
	movss	%xmm2, (%rsp)
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm10, %xmm1
	movss	%xmm1, 4(%rsp)
	addss	8(%rsp), %xmm10
	movss	16(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	movaps	.LCPI6_0(%rip), %xmm8   # xmm8 = [nan,nan,nan,nan]
	andps	%xmm8, %xmm0
	movss	20(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	andps	%xmm8, %xmm5
	movss	24(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	andps	%xmm8, %xmm4
	movss	32(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	andps	%xmm8, %xmm6
	movss	36(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	andps	%xmm8, %xmm7
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	andps	%xmm8, %xmm3
	movsd	48(%rbx), %xmm9         # xmm9 = mem[0],zero
	movss	56(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm0, %xmm2
	mulss	%xmm1, %xmm7
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm5, %xmm1
	addps	%xmm2, %xmm1
	mulss	%xmm10, %xmm3
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm4, %xmm10
	addps	%xmm1, %xmm10
	addss	%xmm6, %xmm7
	addss	%xmm7, %xmm3
	movaps	%xmm9, %xmm0
	subps	%xmm10, %xmm0
	movaps	%xmm8, %xmm1
	subss	%xmm3, %xmm1
	xorps	%xmm2, %xmm2
	xorps	%xmm4, %xmm4
	movss	%xmm1, %xmm4            # xmm4 = xmm1[0],xmm4[1,2,3]
	movlps	%xmm0, (%r15)
	movlps	%xmm4, 8(%r15)
	addps	%xmm9, %xmm10
	addss	%xmm8, %xmm3
	movss	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1,2,3]
	movlps	%xmm10, (%r14)
	movlps	%xmm2, 8(%r14)
	jmp	.LBB6_12
.LBB6_8:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*88(%rax)
.LBB6_9:                                # %_ZNK13btConvexShape19getMarginNonVirtualEv.exit127
	movss	80(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	64(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	68(%r12), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm11
	subss	%xmm2, %xmm11
	movss	84(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm12
	subss	%xmm8, %xmm12
	movss	88(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm13
	subss	%xmm7, %xmm13
	movss	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm11
	mulss	%xmm3, %xmm12
	mulss	%xmm3, %xmm13
	addss	%xmm0, %xmm11
	addss	%xmm0, %xmm12
	addss	%xmm0, %xmm13
	addss	%xmm2, %xmm5
	addss	%xmm8, %xmm6
	addss	%xmm7, %xmm4
	mulss	%xmm3, %xmm5
	mulss	%xmm3, %xmm6
	mulss	%xmm3, %xmm4
	movss	16(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm8    # xmm8 = xmm8[0],xmm0[0],xmm8[1],xmm0[1]
	movaps	.LCPI6_0(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	movaps	%xmm5, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm8, %xmm2
	andps	%xmm0, %xmm8
	movss	20(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm9    # xmm9 = xmm9[0],xmm3[0],xmm9[1],xmm3[1]
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm9, %xmm3
	andps	%xmm0, %xmm9
	movss	24(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm10         # xmm10 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm10   # xmm10 = xmm10[0],xmm7[0],xmm10[1],xmm7[1]
	movaps	%xmm4, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm10, %xmm1
	andps	%xmm0, %xmm10
	addps	%xmm2, %xmm3
	movss	32(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	andps	%xmm0, %xmm2
	addps	%xmm3, %xmm1
	movsd	48(%rbx), %xmm7         # xmm7 = mem[0],zero
	addps	%xmm1, %xmm7
	movss	36(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	andps	%xmm0, %xmm3
	addss	%xmm5, %xmm6
	movss	40(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm0
	mulss	%xmm1, %xmm4
	addss	%xmm6, %xmm4
	mulss	%xmm11, %xmm2
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm8, %xmm11
	mulss	%xmm12, %xmm3
	shufps	$224, %xmm12, %xmm12    # xmm12 = xmm12[0,0,2,3]
	mulps	%xmm9, %xmm12
	addps	%xmm11, %xmm12
	mulss	%xmm13, %xmm0
	shufps	$224, %xmm13, %xmm13    # xmm13 = xmm13[0,0,2,3]
	mulps	%xmm10, %xmm13
	addps	%xmm12, %xmm13
	addss	56(%rbx), %xmm4
	addss	%xmm2, %xmm3
	addss	%xmm3, %xmm0
	movaps	%xmm4, %xmm1
	subss	%xmm0, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movaps	%xmm7, %xmm1
	subps	%xmm13, %xmm1
	movlps	%xmm1, (%r15)
	movlps	%xmm2, 8(%r15)
	addps	%xmm7, %xmm13
	addss	%xmm4, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm13, (%r14)
	movlps	%xmm1, 8(%r14)
.LBB6_12:                               # %.loopexit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZNK13btConvexShape17getAabbNonVirtualERK11btTransformR9btVector3S4_, .Lfunc_end6-_ZNK13btConvexShape17getAabbNonVirtualERK11btTransformR9btVector3S4_
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_2
	.quad	.LBB6_3
	.quad	.LBB6_10
	.quad	.LBB6_10
	.quad	.LBB6_6
	.quad	.LBB6_6
	.quad	.LBB6_10
	.quad	.LBB6_10
	.quad	.LBB6_11
	.quad	.LBB6_10
	.quad	.LBB6_5
	.quad	.LBB6_10
	.quad	.LBB6_10
	.quad	.LBB6_2

	.type	_ZTV13btConvexShape,@object # @_ZTV13btConvexShape
	.globl	_ZTV13btConvexShape
	.p2align	3
_ZTV13btConvexShape:
	.quad	0
	.quad	_ZTI13btConvexShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN13btConvexShapeD0Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.size	_ZTV13btConvexShape, 160

	.type	_ZTS13btConvexShape,@object # @_ZTS13btConvexShape
	.globl	_ZTS13btConvexShape
_ZTS13btConvexShape:
	.asciz	"13btConvexShape"
	.size	_ZTS13btConvexShape, 16

	.type	_ZTI13btConvexShape,@object # @_ZTI13btConvexShape
	.globl	_ZTI13btConvexShape
	.p2align	4
_ZTI13btConvexShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13btConvexShape
	.quad	_ZTI16btCollisionShape
	.size	_ZTI13btConvexShape, 24

	.type	.Lswitch.table,@object  # @switch.table
	.p2align	4
.Lswitch.table:
	.quad	2                       # 0x2
	.quad	2                       # 0x2
	.quad	1                       # 0x1
	.size	.Lswitch.table, 24

	.type	.Lswitch.table.1,@object # @switch.table.1
	.p2align	4
.Lswitch.table.1:
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.size	.Lswitch.table.1, 24


	.globl	_ZN13btConvexShapeD1Ev
	.type	_ZN13btConvexShapeD1Ev,@function
_ZN13btConvexShapeD1Ev = _ZN13btConvexShapeD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
