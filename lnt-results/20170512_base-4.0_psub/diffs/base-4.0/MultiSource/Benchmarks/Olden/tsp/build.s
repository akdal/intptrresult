	.text
	.file	"build.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4602678819172646912     # double 0.5
.LCPI0_1:
	.quad	-4534036246697902950    # double -325507.58283800783
.LCPI0_2:
	.quad	4684832224889240730     # double 162754.79141900392
.LCPI0_3:
	.quad	4607182418800017408     # double 1
.LCPI0_5:
	.quad	-4620693217682128896    # double -0.5
.LCPI0_6:
	.quad	4622945017495814144     # double 12
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_4:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	build_tree
	.p2align	4, 0x90
	.type	build_tree,@function
build_tree:                             # @build_tree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	movl	%ecx, %ebx
	movl	%edx, %r15d
	movl	%esi, %ebp
	movl	%edi, %r14d
	testl	%r14d, %r14d
	je	.LBB0_1
# BB#2:
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	movsd	%xmm2, 40(%rsp)         # 8-byte Spill
	movsd	%xmm3, 16(%rsp)         # 8-byte Spill
	movl	$56, %edi
	callq	malloc
	movq	%rax, %r12
	callq	drand48
	xorl	%r13d, %r13d
	testl	%ebp, %ebp
	sete	%r13b
	je	.LBB0_7
# BB#3:
	ucomisd	.LCPI0_0(%rip), %xmm0
	movq	%r15, 48(%rsp)          # 8-byte Spill
	jbe	.LBB0_5
# BB#4:
	addsd	.LCPI0_5(%rip), %xmm0
	mulsd	.LCPI0_1(%rip), %xmm0
	divsd	.LCPI0_2(%rip), %xmm0
	addsd	.LCPI0_3(%rip), %xmm0
	callq	log
	jmp	.LBB0_6
.LBB0_1:
	xorl	%r12d, %r12d
	jmp	.LBB0_12
.LBB0_7:
	movl	%r14d, %ebp
	movq	%r15, %r14
	ucomisd	.LCPI0_0(%rip), %xmm0
	jbe	.LBB0_9
# BB#8:
	addsd	.LCPI0_5(%rip), %xmm0
	mulsd	.LCPI0_1(%rip), %xmm0
	divsd	.LCPI0_2(%rip), %xmm0
	addsd	.LCPI0_3(%rip), %xmm0
	callq	log
	jmp	.LBB0_10
.LBB0_5:
	mulsd	.LCPI0_1(%rip), %xmm0
	divsd	.LCPI0_2(%rip), %xmm0
	addsd	.LCPI0_3(%rip), %xmm0
	callq	log
	xorpd	.LCPI0_4(%rip), %xmm0
.LBB0_6:                                # %median.exit
	divsd	.LCPI0_6(%rip), %xmm0
	addsd	.LCPI0_3(%rip), %xmm0
	movsd	24(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	32(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	subsd	%xmm3, %xmm2
	mulsd	%xmm0, %xmm2
	mulsd	.LCPI0_0(%rip), %xmm2
	addsd	%xmm3, %xmm2
	movapd	%xmm2, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movl	%r14d, %ebp
	shrl	$31, %ebp
	addl	%r14d, %ebp
	sarl	%ebp
	movl	%r14d, %r15d
	movl	%ebx, %r14d
	shrl	$31, %r14d
	addl	%ebx, %r14d
	sarl	%r14d
	movq	48(%rsp), %rbx          # 8-byte Reload
	leal	(%r14,%rbx), %edx
	movl	%ebp, %edi
	movl	%r13d, %esi
	movl	%r14d, %ecx
	movapd	%xmm3, %xmm0
	movsd	40(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	16(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	callq	build_tree
	movq	%rax, 24(%r12)
	movl	%ebp, %edi
	movl	%r13d, %esi
	movl	%ebx, %edx
	movl	%r14d, %ecx
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	40(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	16(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	callq	build_tree
	movq	%rax, 32(%r12)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%r12)
	callq	drand48
	movsd	40(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movl	$16, %eax
	jmp	.LBB0_11
.LBB0_9:
	mulsd	.LCPI0_1(%rip), %xmm0
	divsd	.LCPI0_2(%rip), %xmm0
	addsd	.LCPI0_3(%rip), %xmm0
	callq	log
	xorpd	.LCPI0_4(%rip), %xmm0
.LBB0_10:                               # %median.exit70
	divsd	.LCPI0_6(%rip), %xmm0
	addsd	.LCPI0_3(%rip), %xmm0
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	40(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	subsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	.LCPI0_0(%rip), %xmm1
	addsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm3
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	movl	%ebp, %r15d
	shrl	$31, %ebp
	addl	%r15d, %ebp
	sarl	%ebp
	movl	%ebx, %eax
	shrl	$31, %ebx
	addl	%eax, %ebx
	sarl	%ebx
	leal	(%rbx,%r14), %edx
	movl	%ebp, %edi
	movl	%r13d, %esi
	movl	%ebx, %ecx
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	build_tree
	movq	%rax, 24(%r12)
	movl	%ebp, %edi
	movl	%r13d, %esi
	movl	%r14d, %edx
	movl	%ebx, %ecx
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	16(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	callq	build_tree
	movq	%rax, 32(%r12)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%r12)
	callq	drand48
	movsd	32(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movl	$8, %eax
.LBB0_11:
	mulsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%r12,%rax)
	movl	%r15d, (%r12)
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 40(%r12)
.LBB0_12:
	movq	%r12, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	build_tree, .Lfunc_end0-build_tree
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
