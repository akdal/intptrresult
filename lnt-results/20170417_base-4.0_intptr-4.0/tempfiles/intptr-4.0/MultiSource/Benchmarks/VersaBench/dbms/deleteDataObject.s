	.text
	.file	"deleteDataObject.bc"
	.globl	deleteDataObject
	.p2align	4, 0x90
	.type	deleteDataObject,@function
deleteDataObject:                       # @deleteDataObject
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	(%r14), %eax
	decl	%eax
	cmpl	$3, %eax
	jae	.LBB0_1
# BB#2:                                 # %.fold.split
	cltq
	movq	.Lswitch.table(,%rax,8), %r15
	movq	8(%r14), %rdi
	movl	$8, %ebx
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, %rdi
	callq	free
	movq	8(%r14), %rdi
.LBB0_5:                                #   in Loop: Header=BB0_3 Depth=1
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB0_3
	jmp	.LBB0_6
.LBB0_1:                                # %.fold.split.thread
	movq	8(%r14), %rdi
.LBB0_6:                                # %._crit_edge
	callq	free
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end0:
	.size	deleteDataObject, .Lfunc_end0-deleteDataObject
	.cfi_endproc

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	18                      # 0x12
	.quad	25                      # 0x19
	.quad	51                      # 0x33
	.size	.Lswitch.table, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
