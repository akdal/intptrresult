	.text
	.file	"timing.bc"
	.globl	hypre_InitializeTiming
	.p2align	4, 0x90
	.type	hypre_InitializeTiming,@function
hypre_InitializeTiming:                 # @hypre_InitializeTiming
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	hypre_global_timing(%rip), %r12
	testq	%r12, %r12
	jne	.LBB0_2
# BB#1:
	movl	$1, %edi
	movl	$80, %esi
	callq	hypre_CAlloc
	movq	%rax, %r12
	movq	%r12, hypre_global_timing(%rip)
.LBB0_2:                                # %.preheader
	cmpl	$0, 52(%r12)
	jle	.LBB0_7
# BB#3:                                 # %.lr.ph90
	movq	40(%r12), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rbx,4), %ebp
	testl	%ebp, %ebp
	jle	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	movq	24(%r12), %rax
	movq	(%rax,%rbx,8), %rsi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_19
.LBB0_6:                                #   in Loop: Header=BB0_4 Depth=1
	incq	%rbx
	movslq	52(%r12), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_4
.LBB0_7:                                # %.thread.preheader
	cmpl	$0, 52(%r12)
	jle	.LBB0_8
# BB#9:                                 # %.lr.ph86
	movq	40(%r12), %rax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_10:                               # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rax,%rbx,4)
	je	.LBB0_12
# BB#11:                                # %.thread
                                        #   in Loop: Header=BB0_10 Depth=1
	incq	%rbx
	movslq	52(%r12), %rcx
	cmpq	%rcx, %rbx
	jl	.LBB0_10
	jmp	.LBB0_12
.LBB0_8:
	xorl	%ebx, %ebx
.LBB0_12:                               # %._crit_edge87
	cmpl	52(%r12), %ebx
	jne	.LBB0_17
# BB#13:
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	(%r12), %r15
	movq	8(%r12), %r14
	movq	16(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%r12), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	32(%r12), %r13
	movq	40(%r12), %rbp
	leal	1(%rbx), %r12d
	movl	$8, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	movq	hypre_global_timing(%rip), %rcx
	movq	%rax, (%rcx)
	movl	$8, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	movq	hypre_global_timing(%rip), %rcx
	movq	%rax, 8(%rcx)
	movl	$8, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	movq	hypre_global_timing(%rip), %rcx
	movq	%rax, 16(%rcx)
	movl	$8, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	movq	hypre_global_timing(%rip), %rcx
	movq	%rax, 24(%rcx)
	movl	$4, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	movq	hypre_global_timing(%rip), %rcx
	movq	%rax, 32(%rcx)
	movl	$4, %esi
	movl	%r12d, %edi
	movq	%r13, %r12
	movq	%rbp, %r13
	callq	hypre_CAlloc
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	hypre_global_timing(%rip), %rcx
	movq	%rax, 40(%rcx)
	incl	52(%rcx)
	testl	%ebx, %ebx
	jle	.LBB0_16
# BB#14:                                # %.lr.ph.preheader
	movl	%ebx, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rdx,8), %rsi
	movq	(%rcx), %rdi
	movq	%rsi, (%rdi,%rdx,8)
	movq	(%r14,%rdx,8), %rsi
	movq	8(%rcx), %rdi
	movq	%rsi, (%rdi,%rdx,8)
	movq	(%r8,%rdx,8), %rsi
	movq	16(%rcx), %rdi
	movq	%rsi, (%rdi,%rdx,8)
	movq	(%rbp,%rdx,8), %rsi
	movq	24(%rcx), %rcx
	movq	%rsi, (%rcx,%rdx,8)
	movl	(%r12,%rdx,4), %esi
	movq	hypre_global_timing(%rip), %rcx
	movq	32(%rcx), %rdi
	movl	%esi, (%rdi,%rdx,4)
	movl	(%r13,%rdx,4), %esi
	movq	40(%rcx), %rdi
	movl	%esi, (%rdi,%rdx,4)
	incq	%rdx
	cmpq	%rdx, %rax
	jne	.LBB0_15
.LBB0_16:                               # %._crit_edge
	movq	%r15, %rdi
	movq	%rbp, %r15
	movq	%r8, %rbp
	callq	hypre_Free
	movq	%r14, %rdi
	callq	hypre_Free
	movq	%rbp, %rdi
	callq	hypre_Free
	movq	%r15, %rdi
	callq	hypre_Free
	movq	%r12, %rdi
	callq	hypre_Free
	movq	%r13, %rdi
	callq	hypre_Free
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB0_17:
	movl	$80, %edi
	movl	$1, %esi
	callq	hypre_CAlloc
	movq	hypre_global_timing(%rip), %rcx
	movq	24(%rcx), %rcx
	movslq	%ebx, %rbp
	movq	%rax, (%rcx,%rbp,8)
	movq	hypre_global_timing(%rip), %rax
	movq	24(%rax), %rax
	movq	(%rax,%rbp,8), %rdi
	movl	$79, %edx
	movq	%r15, %rsi
	callq	strncpy
	movq	hypre_global_timing(%rip), %rax
	movq	32(%rax), %rcx
	movl	$0, (%rcx,%rbp,4)
	movq	40(%rax), %rcx
	movl	$1, (%rcx,%rbp,4)
	incl	48(%rax)
	jmp	.LBB0_18
.LBB0_19:
	incl	%ebp
	movl	%ebp, (%r14,%rbx,4)
.LBB0_18:
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_InitializeTiming, .Lfunc_end0-hypre_InitializeTiming
	.cfi_endproc

	.globl	hypre_FinalizeTiming
	.p2align	4, 0x90
	.type	hypre_FinalizeTiming,@function
hypre_FinalizeTiming:                   # @hypre_FinalizeTiming
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movl	%edi, %eax
	movq	hypre_global_timing(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_11
# BB#1:
	cmpl	%eax, 52(%rdi)
	jle	.LBB1_6
# BB#2:
	movq	40(%rdi), %rcx
	movslq	%eax, %rbx
	movl	(%rcx,%rbx,4), %eax
	testl	%eax, %eax
	jle	.LBB1_4
# BB#3:
	decl	%eax
	movl	%eax, (%rcx,%rbx,4)
.LBB1_4:
	testl	%eax, %eax
	jne	.LBB1_6
# BB#5:
	movq	24(%rdi), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_Free
	movq	hypre_global_timing(%rip), %rax
	movq	24(%rax), %rax
	movq	$0, (%rax,%rbx,8)
	movq	hypre_global_timing(%rip), %rdi
	decl	48(%rdi)
.LBB1_6:
	cmpl	$0, 48(%rdi)
	jne	.LBB1_11
# BB#7:                                 # %.preheader
	cmpl	$0, 52(%rdi)
	jle	.LBB1_10
# BB#8:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdi
	callq	hypre_Free
	movq	hypre_global_timing(%rip), %rax
	movq	$0, (%rax)
	movq	hypre_global_timing(%rip), %rax
	movq	8(%rax), %rdi
	callq	hypre_Free
	movq	hypre_global_timing(%rip), %rax
	movq	$0, 8(%rax)
	movq	16(%rax), %rdi
	callq	hypre_Free
	movq	hypre_global_timing(%rip), %rax
	movq	$0, 16(%rax)
	movq	24(%rax), %rdi
	callq	hypre_Free
	movq	hypre_global_timing(%rip), %rax
	movq	$0, 24(%rax)
	movq	32(%rax), %rdi
	callq	hypre_Free
	movq	hypre_global_timing(%rip), %rax
	movq	$0, 32(%rax)
	movq	40(%rax), %rdi
	callq	hypre_Free
	movq	hypre_global_timing(%rip), %rdi
	movq	$0, 40(%rdi)
	incl	%ebx
	cmpl	52(%rdi), %ebx
	jl	.LBB1_9
.LBB1_10:                               # %._crit_edge
	callq	hypre_Free
	movq	$0, hypre_global_timing(%rip)
.LBB1_11:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	hypre_FinalizeTiming, .Lfunc_end1-hypre_FinalizeTiming
	.cfi_endproc

	.globl	hypre_IncFLOPCount
	.p2align	4, 0x90
	.type	hypre_IncFLOPCount,@function
hypre_IncFLOPCount:                     # @hypre_IncFLOPCount
	.cfi_startproc
# BB#0:
	movq	hypre_global_timing(%rip), %rax
	testq	%rax, %rax
	je	.LBB2_2
# BB#1:
	cvtsi2sdl	%edi, %xmm0
	addsd	72(%rax), %xmm0
	movsd	%xmm0, 72(%rax)
.LBB2_2:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	hypre_IncFLOPCount, .Lfunc_end2-hypre_IncFLOPCount
	.cfi_endproc

	.globl	hypre_BeginTiming
	.p2align	4, 0x90
	.type	hypre_BeginTiming,@function
hypre_BeginTiming:                      # @hypre_BeginTiming
	.cfi_startproc
# BB#0:
	movq	hypre_global_timing(%rip), %rax
	testq	%rax, %rax
	je	.LBB3_4
# BB#1:
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 16
.Lcfi16:
	.cfi_offset %rbx, -16
	movq	32(%rax), %rax
	movslq	%edi, %rbx
	movl	(%rax,%rbx,4), %ecx
	testl	%ecx, %ecx
	jne	.LBB3_3
# BB#2:
	callq	time_getWallclockSeconds
	movq	hypre_global_timing(%rip), %rax
	addsd	56(%rax), %xmm0
	movsd	%xmm0, 56(%rax)
	callq	time_getCPUSeconds
	movq	hypre_global_timing(%rip), %rax
	addsd	64(%rax), %xmm0
	movsd	%xmm0, 64(%rax)
	movq	(%rax), %rcx
	movsd	(%rcx,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	subsd	56(%rax), %xmm0
	movsd	%xmm0, (%rcx,%rbx,8)
	movq	8(%rax), %rcx
	movsd	(%rcx,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	subsd	64(%rax), %xmm0
	movsd	%xmm0, (%rcx,%rbx,8)
	movq	16(%rax), %rcx
	movsd	(%rcx,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	subsd	72(%rax), %xmm0
	movsd	%xmm0, (%rcx,%rbx,8)
	callq	time_getWallclockSeconds
	movq	hypre_global_timing(%rip), %rax
	movsd	56(%rax), %xmm1         # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 56(%rax)
	callq	time_getCPUSeconds
	movq	hypre_global_timing(%rip), %rax
	movsd	64(%rax), %xmm1         # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 64(%rax)
	movq	32(%rax), %rax
	movl	(%rax,%rbx,4), %ecx
.LBB3_3:
	incl	%ecx
	movl	%ecx, (%rax,%rbx,4)
	popq	%rbx
.LBB3_4:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	hypre_BeginTiming, .Lfunc_end3-hypre_BeginTiming
	.cfi_endproc

	.globl	hypre_EndTiming
	.p2align	4, 0x90
	.type	hypre_EndTiming,@function
hypre_EndTiming:                        # @hypre_EndTiming
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 16
.Lcfi18:
	.cfi_offset %rbx, -16
	movq	hypre_global_timing(%rip), %rax
	testq	%rax, %rax
	je	.LBB4_3
# BB#1:
	movq	32(%rax), %rax
	movslq	%edi, %rbx
	decl	(%rax,%rbx,4)
	jne	.LBB4_3
# BB#2:
	callq	time_getWallclockSeconds
	movq	hypre_global_timing(%rip), %rax
	addsd	56(%rax), %xmm0
	movsd	%xmm0, 56(%rax)
	callq	time_getCPUSeconds
	movq	hypre_global_timing(%rip), %rax
	addsd	64(%rax), %xmm0
	movsd	%xmm0, 64(%rax)
	movsd	56(%rax), %xmm0         # xmm0 = mem[0],zero
	movq	(%rax), %rcx
	addsd	(%rcx,%rbx,8), %xmm0
	movsd	%xmm0, (%rcx,%rbx,8)
	movsd	64(%rax), %xmm0         # xmm0 = mem[0],zero
	movq	8(%rax), %rcx
	addsd	(%rcx,%rbx,8), %xmm0
	movsd	%xmm0, (%rcx,%rbx,8)
	movsd	72(%rax), %xmm0         # xmm0 = mem[0],zero
	movq	16(%rax), %rax
	addsd	(%rax,%rbx,8), %xmm0
	movsd	%xmm0, (%rax,%rbx,8)
	callq	time_getWallclockSeconds
	movq	hypre_global_timing(%rip), %rax
	movsd	56(%rax), %xmm1         # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 56(%rax)
	callq	time_getCPUSeconds
	movq	hypre_global_timing(%rip), %rax
	movsd	64(%rax), %xmm1         # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 64(%rax)
.LBB4_3:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	hypre_EndTiming, .Lfunc_end4-hypre_EndTiming
	.cfi_endproc

	.globl	hypre_ClearTiming
	.p2align	4, 0x90
	.type	hypre_ClearTiming,@function
hypre_ClearTiming:                      # @hypre_ClearTiming
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	hypre_global_timing(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB5_17
# BB#1:                                 # %.preheader
	movslq	52(%rcx), %rax
	testq	%rax, %rax
	jle	.LBB5_17
# BB#2:                                 # %.lr.ph
	movq	(%rcx), %r9
	movq	8(%rcx), %r10
	movq	16(%rcx), %r11
	cmpl	$4, %eax
	jae	.LBB5_4
# BB#3:
	xorl	%edi, %edi
	jmp	.LBB5_16
.LBB5_4:                                # %min.iters.checked
	movq	%rax, %r8
	andq	$-4, %r8
	je	.LBB5_5
# BB#6:                                 # %vector.memcheck
	leaq	(%r9,%rax,8), %rcx
	leaq	(%r10,%rax,8), %rsi
	leaq	(%r11,%rax,8), %rdi
	cmpq	%rsi, %r9
	sbbb	%dl, %dl
	cmpq	%rcx, %r10
	sbbb	%bl, %bl
	andb	%dl, %bl
	cmpq	%rdi, %r9
	sbbb	%dl, %dl
	cmpq	%rcx, %r11
	sbbb	%bpl, %bpl
	cmpq	%rdi, %r10
	sbbb	%cl, %cl
	cmpq	%rsi, %r11
	sbbb	%sil, %sil
	xorl	%edi, %edi
	testb	$1, %bl
	jne	.LBB5_16
# BB#7:                                 # %vector.memcheck
	andb	%bpl, %dl
	andb	$1, %dl
	jne	.LBB5_16
# BB#8:                                 # %vector.memcheck
	andb	%sil, %cl
	andb	$1, %cl
	jne	.LBB5_16
# BB#9:                                 # %vector.body.preheader
	leaq	-4(%r8), %rdx
	movq	%rdx, %rcx
	shrq	$2, %rcx
	btl	$2, %edx
	jb	.LBB5_10
# BB#11:                                # %vector.body.prol
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r9)
	movups	%xmm0, 16(%r9)
	movups	%xmm0, (%r10)
	movups	%xmm0, 16(%r10)
	movups	%xmm0, (%r11)
	movups	%xmm0, 16(%r11)
	movl	$4, %esi
	testq	%rcx, %rcx
	jne	.LBB5_13
	jmp	.LBB5_15
.LBB5_5:
	xorl	%edi, %edi
	jmp	.LBB5_16
.LBB5_10:
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.LBB5_15
.LBB5_13:                               # %vector.body.preheader.new
	movq	%r8, %rdi
	subq	%rsi, %rdi
	leaq	48(%r9,%rsi,8), %rcx
	leaq	48(%r10,%rsi,8), %rdx
	leaq	48(%r11,%rsi,8), %rsi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB5_14:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -48(%rcx)
	movups	%xmm0, -32(%rcx)
	movups	%xmm0, -48(%rdx)
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -48(%rsi)
	movups	%xmm0, -32(%rsi)
	movups	%xmm0, -16(%rcx)
	movups	%xmm0, (%rcx)
	movups	%xmm0, -16(%rdx)
	movups	%xmm0, (%rdx)
	movups	%xmm0, -16(%rsi)
	movups	%xmm0, (%rsi)
	addq	$64, %rcx
	addq	$64, %rdx
	addq	$64, %rsi
	addq	$-8, %rdi
	jne	.LBB5_14
.LBB5_15:                               # %middle.block
	cmpq	%r8, %rax
	movq	%r8, %rdi
	je	.LBB5_17
	.p2align	4, 0x90
.LBB5_16:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, (%r9,%rdi,8)
	movq	$0, (%r10,%rdi,8)
	movq	$0, (%r11,%rdi,8)
	incq	%rdi
	cmpq	%rax, %rdi
	jl	.LBB5_16
.LBB5_17:                               # %.loopexit
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end5:
	.size	hypre_ClearTiming, .Lfunc_end5-hypre_ClearTiming
	.cfi_endproc

	.globl	hypre_PrintTiming
	.p2align	4, 0x90
	.type	hypre_PrintTiming,@function
hypre_PrintTiming:                      # @hypre_PrintTiming
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 96
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	cmpq	$0, hypre_global_timing(%rip)
	je	.LBB6_9
# BB#1:
	leaq	4(%rsp), %rsi
	movl	%ebx, %edi
	callq	hypre_MPI_Comm_rank
	cmpl	$0, 4(%rsp)
	jne	.LBB6_3
# BB#2:
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
	movl	$.Lstr.1, %edi
	callq	puts
.LBB6_3:                                # %.preheader
	movq	hypre_global_timing(%rip), %rax
	cmpl	$0, 52(%rax)
	jle	.LBB6_9
# BB#4:                                 # %.lr.ph
	leaq	32(%rsp), %r14
	leaq	16(%rsp), %r15
	leaq	24(%rsp), %r12
	leaq	8(%rsp), %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	movq	40(%rax), %rcx
	cmpl	$0, (%rcx,%rbp,4)
	jle	.LBB6_8
# BB#6:                                 #   in Loop: Header=BB6_5 Depth=1
	movq	(%rax), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	%rcx, 32(%rsp)
	movq	8(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	%rax, 24(%rsp)
	movl	$1, %edx
	xorl	%ecx, %ecx
	movl	$2, %r8d
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	%ebx, %r9d
	callq	hypre_MPI_Allreduce
	movl	$1, %edx
	xorl	%ecx, %ecx
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	%r13, %rsi
	movl	%ebx, %r9d
	callq	hypre_MPI_Allreduce
	cmpl	$0, 4(%rsp)
	jne	.LBB6_8
# BB#7:                                 #   in Loop: Header=BB6_5 Depth=1
	movq	hypre_global_timing(%rip), %rax
	movq	24(%rax), %rax
	movq	(%rax,%rbp,8), %rsi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	16(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str.2, %edi
	movb	$1, %al
	callq	printf
	movsd	8(%rsp), %xmm0          # xmm0 = mem[0],zero
	movl	$.L.str.3, %edi
	movb	$1, %al
	callq	printf
.LBB6_8:                                #   in Loop: Header=BB6_5 Depth=1
	incq	%rbp
	movq	hypre_global_timing(%rip), %rax
	movslq	52(%rax), %rcx
	cmpq	%rcx, %rbp
	jl	.LBB6_5
.LBB6_9:                                # %.loopexit
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	hypre_PrintTiming, .Lfunc_end6-hypre_PrintTiming
	.cfi_endproc

	.type	hypre_global_timing,@object # @hypre_global_timing
	.bss
	.globl	hypre_global_timing
	.p2align	3
hypre_global_timing:
	.quad	0
	.size	hypre_global_timing, 8

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"%s:\n"
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"  wall clock time = %f seconds\n"
	.size	.L.str.2, 32

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"  cpu clock time  = %f seconds\n"
	.size	.L.str.3, 32

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.1:
	.asciz	"============================================="
	.size	.Lstr.1, 46


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
