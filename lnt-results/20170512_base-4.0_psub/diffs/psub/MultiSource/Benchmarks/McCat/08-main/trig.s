	.text
	.file	"trig.bc"
	.globl	MultMatrixMatrix
	.p2align	4, 0x90
	.type	MultMatrixMatrix,@function
MultMatrixMatrix:                       # @MultMatrixMatrix
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_1:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, (%rdx,%rax)
	movsd	(%rdi,%rax), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%rsi), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdx,%rax)
	movsd	8(%rdi,%rax), %xmm2     # xmm2 = mem[0],zero
	mulsd	32(%rsi), %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, (%rdx,%rax)
	movsd	16(%rdi,%rax), %xmm1    # xmm1 = mem[0],zero
	mulsd	64(%rsi), %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, (%rdx,%rax)
	movsd	24(%rdi,%rax), %xmm2    # xmm2 = mem[0],zero
	mulsd	96(%rsi), %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, (%rdx,%rax)
	movq	$0, 8(%rdx,%rax)
	movsd	(%rdi,%rax), %xmm1      # xmm1 = mem[0],zero
	mulsd	8(%rsi), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rdx,%rax)
	movsd	8(%rdi,%rax), %xmm2     # xmm2 = mem[0],zero
	mulsd	40(%rsi), %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 8(%rdx,%rax)
	movsd	16(%rdi,%rax), %xmm1    # xmm1 = mem[0],zero
	mulsd	72(%rsi), %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, 8(%rdx,%rax)
	movsd	24(%rdi,%rax), %xmm2    # xmm2 = mem[0],zero
	mulsd	104(%rsi), %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 8(%rdx,%rax)
	movq	$0, 16(%rdx,%rax)
	movsd	(%rdi,%rax), %xmm1      # xmm1 = mem[0],zero
	mulsd	16(%rsi), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 16(%rdx,%rax)
	movsd	8(%rdi,%rax), %xmm2     # xmm2 = mem[0],zero
	mulsd	48(%rsi), %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 16(%rdx,%rax)
	movsd	16(%rdi,%rax), %xmm1    # xmm1 = mem[0],zero
	mulsd	80(%rsi), %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, 16(%rdx,%rax)
	movsd	24(%rdi,%rax), %xmm2    # xmm2 = mem[0],zero
	mulsd	112(%rsi), %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 16(%rdx,%rax)
	movq	$0, 24(%rdx,%rax)
	movsd	(%rdi,%rax), %xmm1      # xmm1 = mem[0],zero
	mulsd	24(%rsi), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 24(%rdx,%rax)
	movsd	8(%rdi,%rax), %xmm2     # xmm2 = mem[0],zero
	mulsd	56(%rsi), %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 24(%rdx,%rax)
	movsd	16(%rdi,%rax), %xmm1    # xmm1 = mem[0],zero
	mulsd	88(%rsi), %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, 24(%rdx,%rax)
	movsd	24(%rdi,%rax), %xmm2    # xmm2 = mem[0],zero
	mulsd	120(%rsi), %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 24(%rdx,%rax)
	addq	$32, %rax
	cmpq	$128, %rax
	jne	.LBB0_1
# BB#2:
	retq
.Lfunc_end0:
	.size	MultMatrixMatrix, .Lfunc_end0-MultMatrixMatrix
	.cfi_endproc

	.globl	MultMatrixHPoint
	.p2align	4, 0x90
	.type	MultMatrixHPoint,@function
MultMatrixHPoint:                       # @MultMatrixHPoint
	.cfi_startproc
# BB#0:
	movsd	8(%rsp), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rsp), %xmm0         # xmm0 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movsd	(%rsi), %xmm2           # xmm2 = mem[0],zero
	movsd	8(%rsi), %xmm3          # xmm3 = mem[0],zero
	movhpd	32(%rsi), %xmm2         # xmm2 = xmm2[0],mem[0]
	mulpd	%xmm1, %xmm2
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movhpd	40(%rsi), %xmm3         # xmm3 = xmm3[0],mem[0]
	mulpd	%xmm0, %xmm3
	addpd	%xmm2, %xmm3
	movsd	24(%rsp), %xmm2         # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movsd	16(%rsi), %xmm5         # xmm5 = mem[0],zero
	movhpd	48(%rsi), %xmm5         # xmm5 = xmm5[0],mem[0]
	mulpd	%xmm2, %xmm5
	addpd	%xmm3, %xmm5
	movsd	32(%rsp), %xmm3         # xmm3 = mem[0],zero
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	movsd	24(%rsi), %xmm4         # xmm4 = mem[0],zero
	movhpd	56(%rsi), %xmm4         # xmm4 = xmm4[0],mem[0]
	mulpd	%xmm3, %xmm4
	addpd	%xmm5, %xmm4
	movsd	64(%rsi), %xmm5         # xmm5 = mem[0],zero
	movhpd	96(%rsi), %xmm5         # xmm5 = xmm5[0],mem[0]
	mulpd	%xmm1, %xmm5
	movsd	72(%rsi), %xmm1         # xmm1 = mem[0],zero
	movhpd	104(%rsi), %xmm1        # xmm1 = xmm1[0],mem[0]
	mulpd	%xmm0, %xmm1
	addpd	%xmm5, %xmm1
	movsd	80(%rsi), %xmm0         # xmm0 = mem[0],zero
	movhpd	112(%rsi), %xmm0        # xmm0 = xmm0[0],mem[0]
	mulpd	%xmm2, %xmm0
	addpd	%xmm1, %xmm0
	movsd	88(%rsi), %xmm1         # xmm1 = mem[0],zero
	movhpd	120(%rsi), %xmm1        # xmm1 = xmm1[0],mem[0]
	mulpd	%xmm3, %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm4, (%rdi)
	movupd	%xmm1, 16(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	MultMatrixHPoint, .Lfunc_end1-MultMatrixHPoint
	.cfi_endproc

	.globl	CopyMatrix
	.p2align	4, 0x90
	.type	CopyMatrix,@function
CopyMatrix:                             # @CopyMatrix
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB2_1
# BB#2:                                 # %.preheader
	movl	$128, %edi
	callq	malloc
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movups	32(%rbx), %xmm0
	movups	48(%rbx), %xmm1
	movups	%xmm1, 48(%rax)
	movups	%xmm0, 32(%rax)
	movups	64(%rbx), %xmm0
	movups	80(%rbx), %xmm1
	movups	%xmm1, 80(%rax)
	movups	%xmm0, 64(%rax)
	movups	96(%rbx), %xmm0
	movups	112(%rbx), %xmm1
	movups	%xmm1, 112(%rax)
	movups	%xmm0, 96(%rax)
	popq	%rbx
	retq
.LBB2_1:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	CopyMatrix, .Lfunc_end2-CopyMatrix
	.cfi_endproc

	.globl	IdentMatrix
	.p2align	4, 0x90
	.type	IdentMatrix,@function
IdentMatrix:                            # @IdentMatrix
	.cfi_startproc
# BB#0:
	subq	$72, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 80
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movl	$128, %edi
	callq	malloc
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	movq	%rcx, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rax)
	movups	%xmm0, 8(%rax)
	xorps	%xmm0, %xmm0
	movq	%rcx, 40(%rax)
	movups	%xmm0, 48(%rax)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 64(%rax)
	movq	%rcx, 80(%rax)
	movq	$0, 88(%rax)
	movq	24(%rsp), %rdx
	movq	%rdx, 112(%rax)
	movups	8(%rsp), %xmm0
	movups	%xmm0, 96(%rax)
	movq	%rcx, 120(%rax)
	addq	$72, %rsp
	retq
.Lfunc_end3:
	.size	IdentMatrix, .Lfunc_end3-IdentMatrix
	.cfi_endproc

	.globl	TranslateMatrix
	.p2align	4, 0x90
	.type	TranslateMatrix,@function
TranslateMatrix:                        # @TranslateMatrix
	.cfi_startproc
# BB#0:
	subq	$104, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 112
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 80(%rsp)
	movapd	%xmm0, 64(%rsp)
	movapd	%xmm0, 48(%rsp)
	movapd	%xmm0, 32(%rsp)
	movl	$128, %edi
	callq	malloc
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	movq	%rcx, (%rax)
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 24(%rax)
	movupd	%xmm0, 8(%rax)
	movq	%rcx, 40(%rax)
	movq	$0, 48(%rax)
	movaps	80(%rsp), %xmm0
	movups	%xmm0, 64(%rax)
	movq	%rcx, 80(%rax)
	movq	56(%rsp), %rdx
	movq	%rdx, 112(%rax)
	movups	40(%rsp), %xmm0
	movups	%xmm0, 96(%rax)
	movq	%rcx, 120(%rax)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 24(%rax)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 56(%rax)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 88(%rax)
	addq	$104, %rsp
	retq
.Lfunc_end4:
	.size	TranslateMatrix, .Lfunc_end4-TranslateMatrix
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4580687790476533380     # double 0.017453292519944444
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_1:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	RotateMatrix
	.p2align	4, 0x90
	.type	RotateMatrix,@function
RotateMatrix:                           # @RotateMatrix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi7:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 192
.Lcfi11:
	.cfi_offset %rbx, -56
.Lcfi12:
	.cfi_offset %r12, -48
.Lcfi13:
	.cfi_offset %r13, -40
.Lcfi14:
	.cfi_offset %r14, -32
.Lcfi15:
	.cfi_offset %r15, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movsd	%xmm2, 80(%rsp)         # 8-byte Spill
	movsd	%xmm1, 64(%rsp)         # 8-byte Spill
	mulsd	.LCPI5_0(%rip), %xmm0
	movsd	%xmm0, 96(%rsp)         # 8-byte Spill
	callq	cos
	movsd	%xmm0, 128(%rsp)        # 8-byte Spill
	movsd	96(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movsd	64(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	callq	cos
	movsd	%xmm0, 120(%rsp)        # 8-byte Spill
	movsd	64(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	movsd	80(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI5_0(%rip), %xmm0
	movsd	%xmm0, 80(%rsp)         # 8-byte Spill
	callq	cos
	movsd	%xmm0, 112(%rsp)        # 8-byte Spill
	movsd	80(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movl	$128, %edi
	callq	malloc
	movq	%rax, %r14
	movabsq	$4607182418800017408, %rbp # imm = 0x3FF0000000000000
	movq	%rbp, (%r14)
	xorpd	%xmm1, %xmm1
	movupd	%xmm1, 24(%r14)
	movupd	%xmm1, 8(%r14)
	movq	%rbp, 40(%r14)
	movupd	%xmm1, 48(%r14)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 64(%r14)
	movq	%rbp, 80(%r14)
	movq	$0, 88(%r14)
	movq	24(%rsp), %rax
	movq	%rax, 112(%r14)
	movups	8(%rsp), %xmm0
	movups	%xmm0, 96(%r14)
	movq	%rbp, 120(%r14)
	movapd	%xmm1, 48(%rsp)
	movapd	%xmm1, 32(%rsp)
	movapd	%xmm1, 16(%rsp)
	movapd	%xmm1, (%rsp)
	movl	$128, %edi
	callq	malloc
	movq	%rax, %r15
	movq	%rbp, (%r15)
	xorpd	%xmm1, %xmm1
	movupd	%xmm1, 24(%r15)
	movupd	%xmm1, 8(%r15)
	movq	%rbp, 40(%r15)
	movupd	%xmm1, 48(%r15)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 64(%r15)
	movq	%rbp, 80(%r15)
	movq	$0, 88(%r15)
	movq	24(%rsp), %rax
	movq	%rax, 112(%r15)
	movups	8(%rsp), %xmm0
	movups	%xmm0, 96(%r15)
	movq	%rbp, 120(%r15)
	movapd	%xmm1, 48(%rsp)
	movapd	%xmm1, 32(%rsp)
	movapd	%xmm1, 16(%rsp)
	movapd	%xmm1, (%rsp)
	movl	$128, %edi
	callq	malloc
	movq	%rax, %r12
	movq	%rbp, (%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r12)
	movq	$0, 32(%r12)
	movq	%rbp, 40(%r12)
	movups	%xmm0, 48(%r12)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 64(%r12)
	movq	%rbp, 80(%r12)
	movq	$0, 88(%r12)
	movq	24(%rsp), %rax
	movq	%rax, 112(%r12)
	movups	8(%rsp), %xmm0
	movups	%xmm0, 96(%r12)
	movq	%rbp, 120(%r12)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movl	$128, %edi
	callq	malloc
	movq	%rax, %r13
	movq	%rbp, (%r13)
	xorpd	%xmm1, %xmm1
	movupd	%xmm1, 24(%r13)
	movupd	%xmm1, 8(%r13)
	movq	%rbp, 40(%r13)
	movupd	%xmm1, 48(%r13)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 64(%r13)
	movq	%rbp, 80(%r13)
	movq	$0, 88(%r13)
	movq	24(%rsp), %rax
	movq	%rax, 112(%r13)
	movups	8(%rsp), %xmm0
	movups	%xmm0, 96(%r13)
	movq	%rbp, 120(%r13)
	movapd	%xmm1, 48(%rsp)
	movapd	%xmm1, 32(%rsp)
	movapd	%xmm1, 16(%rsp)
	movapd	%xmm1, (%rsp)
	movl	$128, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbp, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movups	%xmm0, 8(%rbx)
	xorps	%xmm0, %xmm0
	movq	%rbp, 40(%rbx)
	movups	%xmm0, 48(%rbx)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rbp, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	24(%rsp), %rax
	movq	%rax, 112(%rbx)
	movups	8(%rsp), %xmm0
	movups	%xmm0, 96(%rbx)
	movq	%rbp, 120(%rbx)
	movsd	128(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	%xmm2, 40(%r14)
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00]
	movapd	96(%rsp), %xmm3         # 16-byte Reload
	movapd	%xmm3, %xmm1
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, 48(%r14)
	movsd	%xmm3, 72(%r14)
	movsd	%xmm2, 80(%r14)
	movsd	120(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	%xmm1, (%r15)
	movapd	64(%rsp), %xmm2         # 16-byte Reload
	movsd	%xmm2, 16(%r15)
	xorpd	%xmm0, %xmm2
	movlpd	%xmm2, 64(%r15)
	movsd	%xmm1, 80(%r15)
	movsd	112(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	%xmm1, (%r12)
	movapd	80(%rsp), %xmm2         # 16-byte Reload
	xorpd	%xmm2, %xmm0
	movlpd	%xmm0, 8(%r12)
	movsd	%xmm2, 32(%r12)
	movsd	%xmm1, 40(%r12)
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	MultMatrixMatrix
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	MultMatrixMatrix
	movq	%rbx, %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	RotateMatrix, .Lfunc_end5-RotateMatrix
	.cfi_endproc

	.globl	ScaleMatrix
	.p2align	4, 0x90
	.type	ScaleMatrix,@function
ScaleMatrix:                            # @ScaleMatrix
	.cfi_startproc
# BB#0:
	subq	$104, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 112
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 80(%rsp)
	movapd	%xmm0, 64(%rsp)
	movapd	%xmm0, 48(%rsp)
	movapd	%xmm0, 32(%rsp)
	movl	$128, %edi
	callq	malloc
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 24(%rax)
	movupd	%xmm0, 8(%rax)
	movupd	%xmm0, 48(%rax)
	movaps	80(%rsp), %xmm0
	movups	%xmm0, 64(%rax)
	movq	$0, 88(%rax)
	movq	56(%rsp), %rcx
	movq	%rcx, 112(%rax)
	movups	40(%rsp), %xmm0
	movups	%xmm0, 96(%rax)
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	movq	%rcx, 120(%rax)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, (%rax)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 40(%rax)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 80(%rax)
	addq	$104, %rsp
	retq
.Lfunc_end6:
	.size	ScaleMatrix, .Lfunc_end6-ScaleMatrix
	.cfi_endproc

	.globl	RotatePoint
	.p2align	4, 0x90
	.type	RotatePoint,@function
RotatePoint:                            # @RotatePoint
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
	subq	$112, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 128
.Lcfi20:
	.cfi_offset %rbx, -16
	movsd	%xmm2, 72(%rsp)         # 8-byte Spill
	movsd	%xmm1, 64(%rsp)         # 8-byte Spill
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movq	%rdi, %rbx
	movaps	128(%rsp), %xmm0
	movaps	144(%rsp), %xmm1
	movaps	160(%rsp), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	80(%rsp), %rdi
	callq	PointToHPoint
	movsd	56(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	64(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	72(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	callq	RotateMatrix
	movaps	128(%rsp), %xmm0
	movaps	144(%rsp), %xmm1
	movaps	160(%rsp), %xmm2
	movups	%xmm2, 32(%rbx)
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	%rbx, %rax
	addq	$112, %rsp
	popq	%rbx
	retq
.Lfunc_end7:
	.size	RotatePoint, .Lfunc_end7-RotatePoint
	.cfi_endproc

	.globl	PrintMatrix
	.p2align	4, 0x90
	.type	PrintMatrix,@function
PrintMatrix:                            # @PrintMatrix
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 16
.Lcfi22:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rbx), %xmm2         # xmm2 = mem[0],zero
	movsd	24(%rbx), %xmm3         # xmm3 = mem[0],zero
	movl	$.L.str, %edi
	movb	$4, %al
	callq	printf
	movsd	32(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	40(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	48(%rbx), %xmm2         # xmm2 = mem[0],zero
	movsd	56(%rbx), %xmm3         # xmm3 = mem[0],zero
	movl	$.L.str.1, %edi
	movb	$4, %al
	callq	printf
	movsd	64(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	72(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	80(%rbx), %xmm2         # xmm2 = mem[0],zero
	movsd	88(%rbx), %xmm3         # xmm3 = mem[0],zero
	movl	$.L.str.1, %edi
	movb	$4, %al
	callq	printf
	movsd	96(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	104(%rbx), %xmm1        # xmm1 = mem[0],zero
	movsd	112(%rbx), %xmm2        # xmm2 = mem[0],zero
	movsd	120(%rbx), %xmm3        # xmm3 = mem[0],zero
	movl	$.L.str.2, %edi
	movb	$4, %al
	popq	%rbx
	jmp	printf                  # TAILCALL
.Lfunc_end8:
	.size	PrintMatrix, .Lfunc_end8-PrintMatrix
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" [[ %.2f, %.2f, %.2f, %.2f] \n"
	.size	.L.str, 30

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"  [ %.2f, %.2f, %.2f, %.2f] \n"
	.size	.L.str.1, 30

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"  [ %.2f, %.2f, %.2f, %.2f]]\n"
	.size	.L.str.2, 30


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
