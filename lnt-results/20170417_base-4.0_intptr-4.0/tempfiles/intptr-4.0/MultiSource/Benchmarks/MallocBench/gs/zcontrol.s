	.text
	.file	"zcontrol.bc"
	.globl	zexec
	.p2align	4, 0x90
	.type	zexec,@function
zexec:                                  # @zexec
	.cfi_startproc
# BB#0:
	movl	$-17, %eax
	cmpq	%rdi, osp_nargs(%rip)
	ja	.LBB0_3
# BB#1:
	movq	esp(%rip), %rcx
	addq	$16, %rcx
	movl	$-5, %eax
	cmpq	estop(%rip), %rcx
	ja	.LBB0_3
# BB#2:
	movq	%rcx, esp(%rip)
	movups	(%rdi), %xmm0
	movups	%xmm0, (%rcx)
	addq	$-16, osp(%rip)
	movl	$1, %eax
.LBB0_3:
	retq
.Lfunc_end0:
	.size	zexec, .Lfunc_end0-zexec
	.cfi_endproc

	.globl	zif
	.p2align	4, 0x90
	.type	zif,@function
zif:                                    # @zif
	.cfi_startproc
# BB#0:
	movzwl	-8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$4, %ecx
	jne	.LBB1_5
# BB#1:
	cmpw	$0, -16(%rdi)
	je	.LBB1_4
# BB#2:
	movq	esp(%rip), %rcx
	addq	$16, %rcx
	movl	$-5, %eax
	cmpq	estop(%rip), %rcx
	ja	.LBB1_5
# BB#3:
	movq	%rcx, esp(%rip)
	movups	(%rdi), %xmm0
	movups	%xmm0, (%rcx)
.LBB1_4:
	addq	$-32, osp(%rip)
	movl	$1, %eax
.LBB1_5:
	retq
.Lfunc_end1:
	.size	zif, .Lfunc_end1-zif
	.cfi_endproc

	.globl	zifelse
	.p2align	4, 0x90
	.type	zifelse,@function
zifelse:                                # @zifelse
	.cfi_startproc
# BB#0:
	movzwl	-24(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$4, %ecx
	jne	.LBB2_6
# BB#1:
	movq	esp(%rip), %rcx
	addq	$16, %rcx
	movl	$-5, %eax
	cmpq	estop(%rip), %rcx
	ja	.LBB2_6
# BB#2:
	movq	%rcx, esp(%rip)
	cmpw	$0, -32(%rdi)
	je	.LBB2_4
# BB#3:
	movups	-16(%rdi), %xmm0
	jmp	.LBB2_5
.LBB2_4:
	movups	(%rdi), %xmm0
.LBB2_5:
	movups	%xmm0, (%rcx)
	addq	$-48, osp(%rip)
	movl	$1, %eax
.LBB2_6:
	retq
.Lfunc_end2:
	.size	zifelse, .Lfunc_end2-zifelse
	.cfi_endproc

	.globl	zfor
	.p2align	4, 0x90
	.type	zfor,@function
zfor:                                   # @zfor
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	leaq	-16(%r14), %rbx
	movl	$3, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	num_params
	testl	%eax, %eax
	js	.LBB3_1
# BB#2:
	movq	esp(%rip), %rdx
	leaq	112(%rdx), %rsi
	movl	$-5, %ecx
	cmpq	estop(%rip), %rsi
	ja	.LBB3_25
# BB#3:
	movw	$2, 16(%rdx)
	movw	$33, 24(%rdx)
	leaq	32(%rdx), %rcx
	movq	%rcx, esp(%rip)
	movups	-48(%r14), %xmm0
	movups	%xmm0, 32(%rdx)
	leaq	48(%rdx), %rcx
	movq	%rcx, esp(%rip)
	movups	-32(%r14), %xmm0
	movups	%xmm0, 48(%rdx)
	leaq	64(%rdx), %rcx
	movq	%rcx, esp(%rip)
	movups	(%rbx), %xmm0
	movups	%xmm0, 64(%rdx)
	leaq	80(%rdx), %r8
	movq	%r8, esp(%rip)
	movups	(%r14), %xmm0
	movups	%xmm0, 80(%rdx)
	addq	$-64, osp(%rip)
	leaq	-48(%r14), %rdi
	addq	$-64, %r14
	cmpl	$7, %eax
	jne	.LBB3_11
# BB#4:
	movq	32(%rdx), %rax
	movq	48(%rdx), %rbx
	movq	64(%rdx), %rcx
	testq	%rbx, %rbx
	js	.LBB3_8
# BB#5:
	cmpq	%rcx, %rax
	jg	.LBB3_9
	jmp	.LBB3_6
.LBB3_1:
	movl	%eax, %ecx
	jmp	.LBB3_25
.LBB3_11:
	testb	$1, %al
	je	.LBB3_13
# BB#12:
	xorps	%xmm0, %xmm0
	cvtsi2ssq	32(%rdx), %xmm0
	movss	%xmm0, 32(%rdx)
	movw	$44, 40(%rdx)
.LBB3_13:
	testb	$2, %al
	je	.LBB3_15
# BB#14:
	xorps	%xmm0, %xmm0
	cvtsi2ssq	48(%rdx), %xmm0
	movss	%xmm0, 48(%rdx)
	movw	$44, 56(%rdx)
.LBB3_15:
	testb	$4, %al
	jne	.LBB3_17
# BB#16:                                # %._crit_edge
	movss	64(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB3_18
.LBB3_17:
	cvtsi2ssq	64(%rdx), %xmm2
	movss	%xmm2, 64(%rdx)
	movw	$44, 72(%rdx)
.LBB3_18:
	movss	32(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	48(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm1
	jae	.LBB3_19
# BB#20:
	ucomiss	%xmm0, %xmm2
	jbe	.LBB3_21
	jmp	.LBB3_9
.LBB3_19:
	ucomiss	%xmm2, %xmm0
	ja	.LBB3_9
.LBB3_21:
	movq	%rdi, osp(%rip)
	cmpq	ostop(%rip), %rdi
	ja	.LBB3_7
# BB#22:
	movups	32(%rdx), %xmm2
	movups	%xmm2, (%rdi)
	addss	%xmm1, %xmm0
	movss	%xmm0, 32(%rdx)
	movq	$for_real_continue, 96(%rdx)
	jmp	.LBB3_23
.LBB3_8:
	cmpq	%rcx, %rax
	jge	.LBB3_6
.LBB3_9:
	movq	%rdx, esp(%rip)
	jmp	.LBB3_24
.LBB3_6:
	movq	%rdi, osp(%rip)
	cmpq	ostop(%rip), %rdi
	jbe	.LBB3_10
.LBB3_7:
	movq	%r14, osp(%rip)
	movl	$-16, %ecx
	jmp	.LBB3_25
.LBB3_10:
	movups	32(%rdx), %xmm0
	movups	%xmm0, (%rdi)
	addq	%rax, %rbx
	movq	%rbx, 32(%rdx)
	movq	$for_int_continue, 96(%rdx)
.LBB3_23:                               # %for_int_continue.exit
	movw	$37, 104(%rdx)
	movw	$0, 106(%rdx)
	movq	%rsi, esp(%rip)
	movups	(%r8), %xmm0
	movups	%xmm0, 112(%rdx)
.LBB3_24:                               # %for_int_continue.exit
	movl	$1, %ecx
.LBB3_25:                               # %for_int_continue.exit
	movl	%ecx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	zfor, .Lfunc_end3-zfor
	.cfi_endproc

	.globl	for_int_continue
	.p2align	4, 0x90
	.type	for_int_continue,@function
for_int_continue:                       # @for_int_continue
	.cfi_startproc
# BB#0:
	movq	esp(%rip), %rax
	movq	-48(%rax), %rcx
	movq	-32(%rax), %rdx
	movq	-16(%rax), %rsi
	testq	%rdx, %rdx
	js	.LBB4_4
# BB#1:
	cmpq	%rsi, %rcx
	jg	.LBB4_5
	jmp	.LBB4_2
.LBB4_4:
	cmpq	%rsi, %rcx
	jge	.LBB4_2
.LBB4_5:
	addq	$-80, %rax
	movq	%rax, esp(%rip)
	movl	$1, %eax
	retq
.LBB4_2:
	leaq	16(%rdi), %rsi
	movq	%rsi, osp(%rip)
	cmpq	ostop(%rip), %rsi
	jbe	.LBB4_6
# BB#3:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB4_6:
	movups	-48(%rax), %xmm0
	movups	%xmm0, (%rsi)
	addq	%rcx, %rdx
	movq	%rdx, -48(%rax)
	movups	(%rax), %xmm0
	movaps	%xmm0, -24(%rsp)
	movq	$for_int_continue, 16(%rax)
	movw	$37, 24(%rax)
	movw	$0, 26(%rax)
	leaq	32(%rax), %rcx
	movq	%rcx, esp(%rip)
	movaps	-24(%rsp), %xmm0
	movups	%xmm0, 32(%rax)
	movl	$1, %eax
	retq
.Lfunc_end4:
	.size	for_int_continue, .Lfunc_end4-for_int_continue
	.cfi_endproc

	.globl	for_real_continue
	.p2align	4, 0x90
	.type	for_real_continue,@function
for_real_continue:                      # @for_real_continue
	.cfi_startproc
# BB#0:
	movq	esp(%rip), %rax
	movss	-48(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	-32(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm1
	movss	-16(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	jae	.LBB5_1
# BB#4:
	ucomiss	%xmm0, %xmm2
	ja	.LBB5_5
	jmp	.LBB5_2
.LBB5_1:
	ucomiss	%xmm2, %xmm0
	jbe	.LBB5_2
.LBB5_5:
	addq	$-80, %rax
	movq	%rax, esp(%rip)
	movl	$1, %eax
	retq
.LBB5_2:
	leaq	16(%rdi), %rcx
	movq	%rcx, osp(%rip)
	cmpq	ostop(%rip), %rcx
	jbe	.LBB5_6
# BB#3:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB5_6:
	movups	-48(%rax), %xmm2
	movups	%xmm2, (%rcx)
	addss	%xmm1, %xmm0
	movss	%xmm0, -48(%rax)
	movups	(%rax), %xmm0
	movaps	%xmm0, -24(%rsp)
	movq	$for_real_continue, 16(%rax)
	movw	$37, 24(%rax)
	movw	$0, 26(%rax)
	leaq	32(%rax), %rcx
	movq	%rcx, esp(%rip)
	movaps	-24(%rsp), %xmm0
	movups	%xmm0, 32(%rax)
	movl	$1, %eax
	retq
.Lfunc_end5:
	.size	for_real_continue, .Lfunc_end5-for_real_continue
	.cfi_endproc

	.globl	zrepeat
	.p2align	4, 0x90
	.type	zrepeat,@function
zrepeat:                                # @zrepeat
	.cfi_startproc
# BB#0:
	movzwl	-8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB6_8
# BB#1:
	leaq	-16(%rdi), %rsi
	cmpq	$0, (%rsi)
	js	.LBB6_2
# BB#3:
	movq	esp(%rip), %rcx
	leaq	80(%rcx), %rdx
	movl	$-5, %eax
	cmpq	estop(%rip), %rdx
	ja	.LBB6_8
# BB#4:
	movw	$2, 16(%rcx)
	movw	$33, 24(%rcx)
	leaq	32(%rcx), %rax
	movq	%rax, esp(%rip)
	movups	(%rsi), %xmm0
	movups	%xmm0, 32(%rcx)
	leaq	48(%rcx), %rax
	movq	%rax, esp(%rip)
	movups	(%rdi), %xmm0
	movups	%xmm0, 48(%rcx)
	addq	$-32, osp(%rip)
	movups	48(%rcx), %xmm0
	movaps	%xmm0, -24(%rsp)
	movq	32(%rcx), %rax
	leaq	-1(%rax), %rsi
	movq	%rsi, 32(%rcx)
	testq	%rax, %rax
	jle	.LBB6_6
# BB#5:
	movq	$repeat_continue, 64(%rcx)
	movw	$37, 72(%rcx)
	movw	$0, 74(%rcx)
	movq	%rdx, esp(%rip)
	movaps	-24(%rsp), %xmm0
	movups	%xmm0, 80(%rcx)
	jmp	.LBB6_7
.LBB6_2:
	movl	$-15, %eax
	retq
.LBB6_6:
	movq	%rcx, esp(%rip)
.LBB6_7:                                # %repeat_continue.exit
	movl	$1, %eax
.LBB6_8:
	retq
.Lfunc_end6:
	.size	zrepeat, .Lfunc_end6-zrepeat
	.cfi_endproc

	.globl	repeat_continue
	.p2align	4, 0x90
	.type	repeat_continue,@function
repeat_continue:                        # @repeat_continue
	.cfi_startproc
# BB#0:
	movq	esp(%rip), %rax
	movups	(%rax), %xmm0
	movaps	%xmm0, -24(%rsp)
	movq	-16(%rax), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -16(%rax)
	testq	%rcx, %rcx
	jle	.LBB7_2
# BB#1:
	movq	$repeat_continue, 16(%rax)
	movw	$37, 24(%rax)
	movw	$0, 26(%rax)
	leaq	32(%rax), %rcx
	movq	%rcx, esp(%rip)
	movaps	-24(%rsp), %xmm0
	movups	%xmm0, 32(%rax)
	movl	$1, %eax
	retq
.LBB7_2:
	addq	$-48, %rax
	movq	%rax, esp(%rip)
	movl	$1, %eax
	retq
.Lfunc_end7:
	.size	repeat_continue, .Lfunc_end7-repeat_continue
	.cfi_endproc

	.globl	zloop
	.p2align	4, 0x90
	.type	zloop,@function
zloop:                                  # @zloop
	.cfi_startproc
# BB#0:
	movl	$-17, %eax
	cmpq	%rdi, osp_nargs(%rip)
	ja	.LBB8_3
# BB#1:
	movq	esp(%rip), %rcx
	leaq	64(%rcx), %rdx
	movl	$-5, %eax
	cmpq	estop(%rip), %rdx
	ja	.LBB8_3
# BB#2:
	movw	$2, 16(%rcx)
	movw	$33, 24(%rcx)
	leaq	32(%rcx), %rax
	movq	%rax, esp(%rip)
	movups	(%rdi), %xmm0
	movups	%xmm0, 32(%rcx)
	addq	$-16, osp(%rip)
	movups	32(%rcx), %xmm0
	movaps	%xmm0, -24(%rsp)
	movq	$loop_continue, 48(%rcx)
	movw	$37, 56(%rcx)
	movw	$0, 58(%rcx)
	movq	%rdx, esp(%rip)
	movaps	-24(%rsp), %xmm0
	movups	%xmm0, 64(%rcx)
	movl	$1, %eax
.LBB8_3:
	retq
.Lfunc_end8:
	.size	zloop, .Lfunc_end8-zloop
	.cfi_endproc

	.globl	loop_continue
	.p2align	4, 0x90
	.type	loop_continue,@function
loop_continue:                          # @loop_continue
	.cfi_startproc
# BB#0:
	movq	esp(%rip), %rax
	movups	(%rax), %xmm0
	movaps	%xmm0, -24(%rsp)
	movq	$loop_continue, 16(%rax)
	movw	$37, 24(%rax)
	movw	$0, 26(%rax)
	leaq	32(%rax), %rcx
	movq	%rcx, esp(%rip)
	movaps	-24(%rsp), %xmm0
	movups	%xmm0, 32(%rax)
	movl	$1, %eax
	retq
.Lfunc_end9:
	.size	loop_continue, .Lfunc_end9-loop_continue
	.cfi_endproc

	.globl	zexit
	.p2align	4, 0x90
	.type	zexit,@function
zexit:                                  # @zexit
	.cfi_startproc
# BB#0:
	movq	esp(%rip), %rcx
	movl	$estack, %edx
	movl	$-8, %eax
	cmpq	%rdx, %rcx
	jb	.LBB10_7
# BB#1:                                 # %.lr.ph.preheader
	movl	$estack, %edx
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	8(%rcx), %esi
	andl	$252, %esi
	cmpl	$32, %esi
	jne	.LBB10_3
# BB#4:                                 #   in Loop: Header=BB10_2 Depth=1
	movzwl	(%rcx), %esi
	cmpl	$3, %esi
	je	.LBB10_7
# BB#5:                                 #   in Loop: Header=BB10_2 Depth=1
	movzwl	%si, %esi
	cmpl	$2, %esi
	je	.LBB10_6
.LBB10_3:                               # %.backedge
                                        #   in Loop: Header=BB10_2 Depth=1
	addq	$-16, %rcx
	cmpq	%rdx, %rcx
	jae	.LBB10_2
	jmp	.LBB10_7
.LBB10_6:
	addq	$-16, %rcx
	movq	%rcx, esp(%rip)
	movl	$1, %eax
.LBB10_7:                               # %.loopexit
	retq
.Lfunc_end10:
	.size	zexit, .Lfunc_end10-zexit
	.cfi_endproc

	.globl	zstop
	.p2align	4, 0x90
	.type	zstop,@function
zstop:                                  # @zstop
	.cfi_startproc
# BB#0:
	movq	esp(%rip), %rcx
	movl	$estack, %edx
	movl	$-8, %eax
	cmpq	%rdx, %rcx
	jb	.LBB11_8
# BB#1:                                 # %.lr.ph.preheader
	movl	$estack, %edx
	.p2align	4, 0x90
.LBB11_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	8(%rcx), %esi
	andl	$252, %esi
	cmpl	$32, %esi
	jne	.LBB11_7
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	movzwl	(%rcx), %esi
	cmpl	$3, %esi
	je	.LBB11_4
.LBB11_7:                               #   in Loop: Header=BB11_2 Depth=1
	addq	$-16, %rcx
	cmpq	%rdx, %rcx
	jae	.LBB11_2
.LBB11_8:                               # %.loopexit
	retq
.LBB11_4:
	addq	$-16, %rcx
	movq	%rcx, esp(%rip)
	leaq	16(%rdi), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB11_6
# BB#5:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB11_6:
	movw	$1, 16(%rdi)
	movw	$4, 24(%rdi)
	movl	$1, %eax
	retq
.Lfunc_end11:
	.size	zstop, .Lfunc_end11-zstop
	.cfi_endproc

	.globl	zstopped
	.p2align	4, 0x90
	.type	zstopped,@function
zstopped:                               # @zstopped
	.cfi_startproc
# BB#0:
	movl	$-17, %eax
	cmpq	%rdi, osp_nargs(%rip)
	ja	.LBB12_3
# BB#1:
	movq	esp(%rip), %rdx
	leaq	48(%rdx), %rcx
	movl	$-5, %eax
	cmpq	estop(%rip), %rcx
	ja	.LBB12_3
# BB#2:
	movw	$3, 16(%rdx)
	movw	$33, 24(%rdx)
	movw	$0, 32(%rdx)
	movw	$4, 40(%rdx)
	movq	%rcx, esp(%rip)
	movups	(%rdi), %xmm0
	movups	%xmm0, (%rcx)
	addq	$-16, osp(%rip)
	movl	$1, %eax
.LBB12_3:
	retq
.Lfunc_end12:
	.size	zstopped, .Lfunc_end12-zstopped
	.cfi_endproc

	.globl	zcountexecstack
	.p2align	4, 0x90
	.type	zcountexecstack,@function
zcountexecstack:                        # @zcountexecstack
	.cfi_startproc
# BB#0:
	leaq	16(%rdi), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB13_2
# BB#1:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB13_2:
	movq	esp(%rip), %rax
	movl	$estack, %ecx
	subq	%rcx, %rax
	sarq	$4, %rax
	incq	%rax
	movq	%rax, 16(%rdi)
	movw	$20, 24(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end13:
	.size	zcountexecstack, .Lfunc_end13-zcountexecstack
	.cfi_endproc

	.globl	zexecstack
	.p2align	4, 0x90
	.type	zexecstack,@function
zexecstack:                             # @zexecstack
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	movl	$-20, %eax
	testb	$-4, %cl
	jne	.LBB14_5
# BB#1:
	movl	$-7, %eax
	testb	$1, %ch
	je	.LBB14_5
# BB#2:
	movq	esp(%rip), %r8
	movl	$estack, %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	shrq	$4, %rsi
	incq	%rsi
	movzwl	10(%rdi), %edx
	movl	$-15, %eax
	cmpl	%edx, %esi
	jg	.LBB14_5
# BB#3:
	movl	$estack, %edx
	movl	$-5, %eax
	cmpq	%rdx, estop(%rip)
	jbe	.LBB14_5
# BB#4:
	movw	%si, 10(%rdi)
	orl	$32768, %ecx            # imm = 0x8000
	movw	%cx, 8(%rdi)
	leaq	16(%r8), %rax
	movq	%rax, esp(%rip)
	movq	$continue_execstack, 16(%r8)
	movl	$37, 24(%r8)
	movl	$1, %eax
.LBB14_5:
	retq
.Lfunc_end14:
	.size	zexecstack, .Lfunc_end14-zexecstack
	.cfi_endproc

	.globl	continue_execstack
	.p2align	4, 0x90
	.type	continue_execstack,@function
continue_execstack:                     # @continue_execstack
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movzwl	10(%rdi), %edx
	movq	(%rdi), %rdi
	movl	$estack, %esi
	xorl	%eax, %eax
	callq	refcpy
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end15:
	.size	continue_execstack, .Lfunc_end15-continue_execstack
	.cfi_endproc

	.globl	zquit
	.p2align	4, 0x90
	.type	zquit,@function
zquit:                                  # @zquit
	.cfi_startproc
# BB#0:
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	gs_exit                 # TAILCALL
.Lfunc_end16:
	.size	zquit, .Lfunc_end16-zquit
	.cfi_endproc

	.globl	zcontrol_op_init
	.p2align	4, 0x90
	.type	zcontrol_op_init,@function
zcontrol_op_init:                       # @zcontrol_op_init
	.cfi_startproc
# BB#0:
	movl	$zcontrol_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end17:
	.size	zcontrol_op_init, .Lfunc_end17-zcontrol_op_init
	.cfi_endproc

	.type	zcontrol_op_init.my_defs,@object # @zcontrol_op_init.my_defs
	.data
	.p2align	4
zcontrol_op_init.my_defs:
	.quad	.L.str
	.quad	zcountexecstack
	.quad	.L.str.1
	.quad	zexec
	.quad	.L.str.2
	.quad	zexecstack
	.quad	.L.str.3
	.quad	zexit
	.quad	.L.str.4
	.quad	zif
	.quad	.L.str.5
	.quad	zifelse
	.quad	.L.str.6
	.quad	zfor
	.quad	.L.str.7
	.quad	zloop
	.quad	.L.str.8
	.quad	zquit
	.quad	.L.str.9
	.quad	zrepeat
	.quad	.L.str.10
	.quad	zstop
	.quad	.L.str.11
	.quad	zstopped
	.zero	16
	.size	zcontrol_op_init.my_defs, 208

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"0countexecstack"
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"1exec"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"0execstack"
	.size	.L.str.2, 11

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"0exit"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"2if"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"3ifelse"
	.size	.L.str.5, 8

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"4for"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"1loop"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"0quit"
	.size	.L.str.8, 6

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"2repeat"
	.size	.L.str.9, 8

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"0stop"
	.size	.L.str.10, 6

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"1stopped"
	.size	.L.str.11, 9


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
