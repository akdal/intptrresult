	.text
	.file	"partitionEntries.bc"
	.globl	partitionEntries
	.p2align	4, 0x90
	.type	partitionEntries,@function
partitionEntries:                       # @partitionEntries
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 256
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movq	%rbp, (%r13)
	movq	40(%rbp), %rsi
	movq	%rsi, (%r14)
	movq	(%r13), %rdi
	addq	$8, %rdi
	addq	$8, %rsi
	leaq	128(%rsp), %rdx
	callq	keyUnion
	movups	128(%rsp), %xmm0
	movups	144(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	callq	volume
	movss	%xmm0, 100(%rsp)        # 4-byte Spill
	testq	%rbp, %rbp
	je	.LBB0_19
# BB#1:
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	leaq	168(%rsp), %r15
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_10 Depth 2
	movq	40(%rbx), %r12
	testq	%r12, %r12
	je	.LBB0_3
# BB#9:                                 # %.lr.ph201
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	8(%rbx), %rbp
	.p2align	4, 0x90
.LBB0_10:                               #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	8(%r12), %rsi
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	keyUnion
	movups	168(%rsp), %xmm0
	movups	184(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	callq	volume
	ucomiss	100(%rsp), %xmm0        # 4-byte Folded Reload
	jbe	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_10 Depth=2
	movq	%rbx, (%r13)
	movq	%r12, (%r14)
	movups	168(%rsp), %xmm1
	movups	184(%rsp), %xmm2
	movaps	%xmm2, 144(%rsp)
	movaps	%xmm1, 128(%rsp)
	movss	%xmm0, 100(%rsp)        # 4-byte Spill
.LBB0_12:                               #   in Loop: Header=BB0_10 Depth=2
	movq	40(%r12), %r12
	testq	%r12, %r12
	jne	.LBB0_10
# BB#13:                                # %._crit_edge202
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_2
.LBB0_3:                                # %.preheader
	movq	104(%rsp), %rsi         # 8-byte Reload
	testq	%rsi, %rsi
	movq	(%r13), %rax
	je	.LBB0_20
# BB#4:
	movq	120(%rsp), %r12         # 8-byte Reload
.LBB0_5:                                # %.lr.ph181.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
                                        #       Child Loop BB0_14 Depth 3
	xorl	%edx, %edx
	movq	%rsi, %rcx
.LBB0_6:                                # %.lr.ph181
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_14 Depth 3
	testq	%rdx, %rdx
	je	.LBB0_7
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph181.split
                                        #   Parent Loop BB0_5 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rax, %rcx
	je	.LBB0_16
# BB#15:                                #   in Loop: Header=BB0_14 Depth=3
	cmpq	(%r14), %rcx
	jne	.LBB0_21
.LBB0_16:                               #   in Loop: Header=BB0_14 Depth=3
	movq	40(%rcx), %rcx
	movq	%rcx, 40(%rdx)
	testq	%rcx, %rcx
	movq	(%r13), %rax
	jne	.LBB0_14
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph181.split.us
                                        #   in Loop: Header=BB0_6 Depth=2
	cmpq	%rax, %rcx
	je	.LBB0_8
# BB#17:                                #   in Loop: Header=BB0_6 Depth=2
	cmpq	(%r14), %rcx
	je	.LBB0_18
.LBB0_21:                               # %.outer113
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%rcx, %rdx
	movq	40(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_6
	jmp	.LBB0_22
.LBB0_8:                                #   in Loop: Header=BB0_5 Depth=1
	movq	%rax, %rcx
.LBB0_18:                               # %.outer113.loopexit.us-lcssa.us
                                        #   in Loop: Header=BB0_5 Depth=1
	movq	40(%rcx), %rsi
	testq	%rsi, %rsi
	jne	.LBB0_5
	jmp	.LBB0_20
.LBB0_22:                               # %.outer113._crit_edge
	movq	$0, 40(%rax)
	movq	(%r14), %rax
	movq	$0, 40(%rax)
	testq	%rsi, %rsi
	je	.LBB0_35
# BB#23:                                # %.lr.ph.lr.ph.preheader
	movq	(%r14), %r15
	movq	(%r13), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	$1, %ebp
	movl	$1, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
.LBB0_24:                               # %.lr.ph.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_31 Depth 2
                                        #     Child Loop BB0_26 Depth 2
	cmpq	%r12, 104(%rsp)         # 8-byte Folded Reload
	setge	%al
	jge	.LBB0_31
# BB#25:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB0_24 Depth=1
	movb	%al, 99(%rsp)           # 1-byte Spill
	.p2align	4, 0x90
.LBB0_26:                               # %.lr.ph.us
                                        #   Parent Loop BB0_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %rbx
	cmpq	%r12, %rbp
	jge	.LBB0_29
# BB#27:                                # %.us-lcssa.us.us
                                        #   in Loop: Header=BB0_26 Depth=2
	movq	(%r13), %rax
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movups	%xmm2, 80(%rsp)
	movups	%xmm1, 64(%rsp)
	movups	%xmm0, 48(%rsp)
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	callq	penalty
	movss	%xmm0, 100(%rsp)        # 4-byte Spill
	movq	(%r14), %rax
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movups	%xmm2, 80(%rsp)
	movups	%xmm1, 64(%rsp)
	movups	%xmm0, 48(%rsp)
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	callq	penalty
	ucomiss	100(%rsp), %xmm0        # 4-byte Folded Reload
	ja	.LBB0_28
# BB#36:                                # %.outer108.backedge.us
                                        #   in Loop: Header=BB0_26 Depth=2
	movq	%rbx, 40(%r15)
	movq	40(%rbx), %rsi
	movq	$0, 40(%rbx)
	incq	%rbp
	testq	%rsi, %rsi
	movq	%rbx, %r15
	jne	.LBB0_26
	jmp	.LBB0_35
	.p2align	4, 0x90
.LBB0_31:                               # %.lr.ph
                                        #   Parent Loop BB0_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %rbx
	cmpq	%r12, %rbp
	jge	.LBB0_32
# BB#34:                                # %.outer108.backedge
                                        #   in Loop: Header=BB0_31 Depth=2
	movq	%rbx, 40(%r15)
	movq	40(%rbx), %rsi
	movq	$0, 40(%rbx)
	incq	%rbp
	testq	%rsi, %rsi
	movq	%rbx, %r15
	jne	.LBB0_31
	jmp	.LBB0_35
.LBB0_32:                               # %.lr.ph.split.split
                                        #   in Loop: Header=BB0_24 Depth=1
	cmpq	%r12, 104(%rsp)         # 8-byte Folded Reload
	jl	.LBB0_28
	jmp	.LBB0_33
.LBB0_29:                               # %.lr.ph.split..lr.ph.split.split_crit_edge.us
                                        #   in Loop: Header=BB0_24 Depth=1
	setl	%al
	orb	99(%rsp), %al           # 1-byte Folded Reload
	cmpb	$1, %al
	je	.LBB0_30
.LBB0_28:                               # %.outer.backedge
                                        #   in Loop: Header=BB0_24 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rbx, 40(%rax)
	movq	40(%rbx), %rsi
	movq	$0, 40(%rbx)
	incq	104(%rsp)               # 8-byte Folded Spill
	testq	%rsi, %rsi
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	jne	.LBB0_24
	jmp	.LBB0_35
.LBB0_19:                               # %.preheader.thread
	movq	(%r13), %rax
.LBB0_20:                               # %.outer113._crit_edge.thread
	movq	$0, 40(%rax)
	movq	(%r14), %rax
	movq	$0, 40(%rax)
.LBB0_35:                               # %.outer108._crit_edge
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_33:                               # %.lr.ph.split.split.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rbx
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$partitionEntries.name, %edi
	movl	$1, %esi
	callq	errorMessage
	testq	%rbx, %rbx
	jne	.LBB0_33
	jmp	.LBB0_35
.LBB0_30:                               # %.lr.ph.split.split.split.us.us
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rbx
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$partitionEntries.name, %edi
	movl	$1, %esi
	callq	errorMessage
	testq	%rbx, %rbx
	jne	.LBB0_30
	jmp	.LBB0_35
.Lfunc_end0:
	.size	partitionEntries, .Lfunc_end0-partitionEntries
	.cfi_endproc

	.type	partitionEntries.name,@object # @partitionEntries.name
	.data
	.p2align	4
partitionEntries.name:
	.asciz	"partitionEntries"
	.size	partitionEntries.name, 17

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"too many entries to partition"
	.size	.L.str, 30


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
