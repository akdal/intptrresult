	.text
	.file	"pcmp.bc"
	.globl	pcmpz
	.p2align	4, 0x90
	.type	pcmpz,@function
pcmpz:                                  # @pcmpz
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB0_2
# BB#1:
	incw	(%rdi)
.LBB0_2:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	leaq	8(%rdi), %rax
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	cmpw	$0, (%rax)
	jne	.LBB0_4
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	addq	$2, %rax
	movzwl	4(%rdi), %ecx
	leaq	8(%rdi,%rcx,2), %rcx
	cmpq	%rcx, %rax
	jb	.LBB0_3
# BB#6:
	xorl	%ebx, %ebx
	decw	(%rdi)
	jne	.LBB0_9
	jmp	.LBB0_8
.LBB0_4:
	cmpb	$1, 6(%rdi)
	sbbl	%ebx, %ebx
	notl	%ebx
	orl	$1, %ebx
	decw	(%rdi)
	jne	.LBB0_9
.LBB0_8:
	xorl	%eax, %eax
	callq	pfree
.LBB0_9:
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	pcmpz, .Lfunc_end0-pcmpz
	.cfi_endproc

	.globl	pcmp
	.p2align	4, 0x90
	.type	pcmp,@function
pcmp:                                   # @pcmp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	incw	(%rdi)
.LBB1_2:
	testq	%rbx, %rbx
	je	.LBB1_4
# BB#3:
	incw	(%rbx)
.LBB1_4:
	movb	6(%rdi), %r8b
	cmpb	6(%rbx), %r8b
	jne	.LBB1_5
# BB#6:
	movzwl	4(%rdi), %eax
	movzwl	4(%rbx), %esi
	movl	%eax, %ecx
	subl	%esi, %ecx
	jne	.LBB1_12
# BB#7:
	leaq	8(%rbx), %rcx
	leaq	6(%rdi,%rax,2), %rdx
	leaq	6(%rbx,%rsi,2), %rsi
	.p2align	4, 0x90
.LBB1_8:                                # =>This Inner Loop Header: Depth=1
	movzwl	(%rdx), %ebp
	movzwl	(%rsi), %eax
	cmpq	%rcx, %rsi
	jbe	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_8 Depth=1
	addq	$-2, %rdx
	addq	$-2, %rsi
	cmpw	%ax, %bp
	je	.LBB1_8
.LBB1_10:
	movl	$1, %ecx
	cmpw	%ax, %bp
	ja	.LBB1_12
# BB#11:
	sbbl	%ecx, %ecx
.LBB1_12:
	movl	%ecx, %ebp
	negl	%ebp
	testb	%r8b, %r8b
	cmovel	%ecx, %ebp
	decw	(%rdi)
	jne	.LBB1_15
	jmp	.LBB1_14
.LBB1_5:
	cmpb	$1, %r8b
	sbbl	%ebp, %ebp
	notl	%ebp
	orl	$1, %ebp
	decw	(%rdi)
	jne	.LBB1_15
.LBB1_14:
	xorl	%eax, %eax
	callq	pfree
.LBB1_15:
	testq	%rbx, %rbx
	je	.LBB1_18
# BB#16:
	decw	(%rbx)
	jne	.LBB1_18
# BB#17:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pfree
.LBB1_18:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	pcmp, .Lfunc_end1-pcmp
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
