	.text
	.file	"rpos.bc"
	.globl	rpos_Equal
	.p2align	4, 0x90
	.type	rpos_Equal,@function
rpos_Equal:                             # @rpos_Equal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	(%rdi), %eax
	xorl	%r14d, %r14d
	cmpl	(%rbx), %eax
	jne	.LBB0_10
# BB#1:
	movq	16(%rdi), %rbp
	testq	%rbp, %rbp
	je	.LBB0_9
# BB#2:
	negl	%eax
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	testb	$16, 20(%rax)
	jne	.LBB0_3
# BB#6:                                 # %.lr.ph.preheader
	addq	$16, %rbx
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movq	8(%rbp), %rdi
	movq	8(%rbx), %rsi
	callq	rpos_Equal
	testl	%eax, %eax
	je	.LBB0_10
# BB#8:                                 #   in Loop: Header=BB0_7 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_7
.LBB0_9:
	movl	$1, %r14d
	jmp	.LBB0_10
.LBB0_3:
	movq	%rbp, %rdi
	callq	list_Copy
	movq	16(%rbx), %rsi
	movl	$rpos_Equal, %edx
	movq	%rax, %rdi
	callq	list_NMultisetDifference
	xorl	%ecx, %ecx
	testq	%rax, %rax
	sete	%dl
	je	.LBB0_4
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB0_5
	jmp	.LBB0_10
.LBB0_4:
	movb	%dl, %cl
	movl	%ecx, %r14d
.LBB0_10:                               # %list_Delete.exit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	rpos_Equal, .Lfunc_end0-rpos_Equal
	.cfi_endproc

	.globl	rpos_GreaterEqual
	.p2align	4, 0x90
	.type	rpos_GreaterEqual,@function
rpos_GreaterEqual:                      # @rpos_GreaterEqual
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 64
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	(%r15), %edx
	movl	(%r14), %esi
	testl	%edx, %edx
	jle	.LBB1_2
# BB#1:
	xorl	%eax, %eax
	cmpl	%esi, %edx
	sete	%al
	addl	%eax, %eax
	jmp	.LBB1_53
.LBB1_2:
	testl	%esi, %esi
	jle	.LBB1_4
# BB#3:
	movq	%r15, %rdi
	callq	term_ContainsSymbol
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	leal	(%rcx,%rcx,2), %ebp
.LBB1_52:                               # %rpos_MulGreaterEqual.exit
	movl	%ebp, %eax
.LBB1_53:                               # %rpos_MulGreaterEqual.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_4:
	cmpl	%esi, %edx
	jne	.LBB1_41
# BB#5:
	negl	%edx
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	20(%rax), %eax
	testb	$16, %al
	jne	.LBB1_6
# BB#17:
	testb	$8, %al
	movq	16(%r15), %rbx
	jne	.LBB1_18
# BB#19:
	movq	16(%r14), %r13
	testq	%rbx, %rbx
	jne	.LBB1_22
	jmp	.LBB1_21
.LBB1_41:
	movq	ord_PRECEDENCE(%rip), %rax
	negl	%edx
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %edx
	negl	%esi
	sarl	%cl, %esi
	movslq	%esi, %rcx
	cmpl	(%rax,%rcx,4), %edx
	jge	.LBB1_47
# BB#42:
	movq	16(%r14), %rbx
	movl	$3, %ebp
	testq	%rbx, %rbx
	jne	.LBB1_45
	jmp	.LBB1_52
	.p2align	4, 0x90
.LBB1_43:                               #   in Loop: Header=BB1_45 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB1_52
.LBB1_45:                               # %.lr.ph86
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	rpos_GreaterEqual
	cmpl	$3, %eax
	je	.LBB1_43
# BB#46:
	xorl	%ebp, %ebp
	jmp	.LBB1_52
.LBB1_6:
	movq	16(%r15), %rdi
	callq	list_Copy
	movq	16(%r14), %rsi
	movl	$rpos_Equal, %edx
	movq	%rax, %rdi
	callq	list_NMultisetDifference
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB1_7
# BB#8:
	movq	16(%r14), %rdi
	callq	list_Copy
	movq	16(%r15), %rsi
	movl	$rpos_Equal, %edx
	movq	%rax, %rdi
	callq	list_NMultisetDifference
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB1_9
	.p2align	4, 0x90
.LBB1_10:                               # %.preheader57.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_11 Depth 2
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB1_11:                               #   Parent Loop BB1_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movq	8(%rbp), %rsi
	callq	rpos_GreaterEqual
	movq	(%rbx), %rbx
	cmpl	$3, %eax
	setne	%cl
	testq	%rbx, %rbx
	je	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_11 Depth=2
	testb	%cl, %cl
	jne	.LBB1_11
.LBB1_13:                               # %..critedge.i39_crit_edge.us
                                        #   in Loop: Header=BB1_10 Depth=1
	movq	(%rbp), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbp, (%rdx)
	testq	%rcx, %rcx
	je	.LBB1_15
# BB#14:                                # %..critedge.i39_crit_edge.us
                                        #   in Loop: Header=BB1_10 Depth=1
	cmpl	$3, %eax
	movq	%rcx, %rbp
	je	.LBB1_10
.LBB1_15:                               # %._crit_edge81.loopexit98
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	sete	%cl
	leal	(%rcx,%rcx,2), %ebp
	jmp	.LBB1_16
.LBB1_47:
	movq	16(%r15), %rbx
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	jne	.LBB1_50
	jmp	.LBB1_52
	.p2align	4, 0x90
.LBB1_48:                               #   in Loop: Header=BB1_50 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB1_52
.LBB1_50:                               # %.lr.ph93
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	callq	rpos_GreaterEqual
	testl	%eax, %eax
	je	.LBB1_48
# BB#51:
	movl	$3, %ebp
	jmp	.LBB1_52
.LBB1_18:
	movq	%rbx, %rdi
	callq	list_Reverse
	movq	%rax, %rbx
	movq	16(%r14), %rdi
	callq	list_Reverse
	movq	%rax, %r13
	testq	%rbx, %rbx
	je	.LBB1_21
.LBB1_22:                               # %.lr.ph67.preheader
	movl	$2, 4(%rsp)             # 4-byte Folded Spill
	movq	%r13, %r12
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph67
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movq	8(%r12), %rsi
	callq	rpos_GreaterEqual
	cmpl	$2, %eax
	jne	.LBB1_24
# BB#27:                                #   in Loop: Header=BB1_23 Depth=1
	movq	(%rbp), %rbp
	movq	(%r12), %r12
	testq	%rbp, %rbp
	jne	.LBB1_23
	jmp	.LBB1_35
.LBB1_7:
	movl	$2, %ebp
	jmp	.LBB1_52
.LBB1_21:
	movl	$2, 4(%rsp)             # 4-byte Folded Spill
.LBB1_35:                               # %.critedge1.i
	xorl	%eax, %eax
	subl	(%r15), %eax
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	testb	$8, 20(%rax)
	jne	.LBB1_37
# BB#36:
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB1_52
.LBB1_37:
	testq	%rbx, %rbx
	je	.LBB1_39
	.p2align	4, 0x90
.LBB1_38:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB1_38
.LBB1_39:                               # %list_Delete.exit.i
	testq	%r13, %r13
	movl	4(%rsp), %ebp           # 4-byte Reload
	je	.LBB1_52
	.p2align	4, 0x90
.LBB1_40:                               # %.lr.ph.i57.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB1_40
	jmp	.LBB1_52
.LBB1_24:                               # %.lr.ph67
	cmpl	$3, %eax
	jne	.LBB1_25
.LBB1_28:                               # %.preheader55
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.LBB1_29
# BB#30:                                #   in Loop: Header=BB1_28 Depth=1
	movq	8(%r12), %rsi
	movq	%r15, %rdi
	callq	rpos_GreaterEqual
	cmpl	$3, %eax
	je	.LBB1_28
# BB#31:
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB1_35
.LBB1_9:
	movl	$3, %ebp
	.p2align	4, 0x90
.LBB1_16:                               # %.lr.ph.i.i43
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB1_16
	jmp	.LBB1_52
.LBB1_25:                               # %.preheader
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB1_26
# BB#32:                                # %.lr.ph.preheader
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	$3, %r12d
.LBB1_33:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movq	%r14, %rsi
	callq	rpos_GreaterEqual
	testl	%eax, %eax
	movl	4(%rsp), %eax           # 4-byte Reload
	cmovnel	%r12d, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	cmpl	$3, %eax
	je	.LBB1_35
# BB#34:                                # %.lr.ph
                                        #   in Loop: Header=BB1_33 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB1_33
	jmp	.LBB1_35
.LBB1_29:
	movl	$3, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB1_35
.LBB1_26:
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB1_35
.Lfunc_end1:
	.size	rpos_GreaterEqual, .Lfunc_end1-rpos_GreaterEqual
	.cfi_endproc

	.globl	rpos_Compare
	.p2align	4, 0x90
	.type	rpos_Compare,@function
rpos_Compare:                           # @rpos_Compare
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	rpos_GreaterEqual
	testl	%eax, %eax
	jne	.LBB2_2
# BB#1:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	rpos_GreaterEqual
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	sete	%cl
	movl	%ecx, %eax
.LBB2_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	rpos_Compare, .Lfunc_end2-rpos_Compare
	.cfi_endproc

	.globl	rpos_ContEqual
	.p2align	4, 0x90
	.type	rpos_ContEqual,@function
rpos_ContEqual:                         # @rpos_ContEqual
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 64
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rdi, %r13
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.LBB3_5
# BB#1:                                 # %.lr.ph.i.preheader
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %r13
	je	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	movq	8(%r13,%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB3_5
# BB#4:                                 # %.thread.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	16(%r13,%rcx), %r13
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	movq	%rdx, %rsi
	jg	.LBB3_2
.LBB3_5:                                # %cont_Deref.exit
	movl	(%r15), %eax
	testl	%eax, %eax
	jle	.LBB3_11
# BB#6:                                 # %.lr.ph.i33.preheader
	movq	cont_INSTANCECONTEXT(%rip), %rcx
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph.i33
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rcx, %r14
	je	.LBB3_10
# BB#8:                                 #   in Loop: Header=BB3_7 Depth=1
	cltq
	shlq	$5, %rax
	movq	8(%r14,%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB3_10
# BB#9:                                 # %.thread.i38
                                        #   in Loop: Header=BB3_7 Depth=1
	movq	16(%r14,%rax), %r14
	movl	(%rdx), %eax
	testl	%eax, %eax
	movq	%rdx, %r15
	jg	.LBB3_7
.LBB3_10:                               # %cont_Deref.exit40.loopexit
	movl	(%r15), %eax
.LBB3_11:                               # %cont_Deref.exit40
	xorl	%r12d, %r12d
	cmpl	%eax, (%rsi)
	jne	.LBB3_31
# BB#12:
	movq	16(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB3_30
# BB#13:
	negl	%eax
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	testb	$16, 20(%rax)
	jne	.LBB3_14
# BB#27:                                # %.lr.ph
	addq	$16, %r15
	.p2align	4, 0x90
.LBB3_28:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15), %r15
	movq	8(%rbp), %rsi
	movq	8(%r15), %rcx
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	rpos_ContEqual
	testl	%eax, %eax
	je	.LBB3_31
# BB#29:                                #   in Loop: Header=BB3_28 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_28
.LBB3_30:
	movl	$1, %r12d
	jmp	.LBB3_31
.LBB3_14:
	movq	%rbp, %rdi
	callq	list_Copy
	movq	%rax, %r12
	movq	16(%r15), %rbp
	testq	%rbp, %rbp
	je	.LBB3_23
# BB#15:                                # %.preheader.lr.ph
	testq	%r12, %r12
	je	.LBB3_16
	.p2align	4, 0x90
.LBB3_17:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_18 Depth 2
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB3_18:                               #   Parent Loop BB3_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB3_21
# BB#19:                                #   in Loop: Header=BB3_18 Depth=2
	movq	8(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	rpos_ContEqual
	testl	%eax, %eax
	jne	.LBB3_20
.LBB3_21:                               #   in Loop: Header=BB3_18 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_18
	jmp	.LBB3_22
	.p2align	4, 0x90
.LBB3_20:                               #   in Loop: Header=BB3_17 Depth=1
	movq	$0, 8(%rbx)
.LBB3_22:                               # %.loopexit
                                        #   in Loop: Header=BB3_17 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_17
	jmp	.LBB3_23
	.p2align	4, 0x90
.LBB3_16:                               # %.preheader.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_16
.LBB3_23:                               # %rpos_ContMultisetDifference.exit
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	list_PointerDeleteElement
	xorl	%r12d, %r12d
	testq	%rax, %rax
	sete	%cl
	je	.LBB3_24
	.p2align	4, 0x90
.LBB3_25:                               # %.lr.ph.i41
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB3_25
# BB#26:
	xorl	%r12d, %r12d
	jmp	.LBB3_31
.LBB3_24:
	movb	%cl, %r12b
.LBB3_31:                               # %list_Delete.exit
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	rpos_ContEqual, .Lfunc_end3-rpos_ContEqual
	.cfi_endproc

	.globl	rpos_ContGreaterEqual
	.p2align	4, 0x90
	.type	rpos_ContGreaterEqual,@function
rpos_ContGreaterEqual:                  # @rpos_ContGreaterEqual
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi43:
	.cfi_def_cfa_offset 80
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jle	.LBB4_5
# BB#1:                                 # %.lr.ph.i.preheader
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %r12
	je	.LBB4_5
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	movq	8(%r12,%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB4_5
# BB#4:                                 # %.thread.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	16(%r12,%rcx), %r12
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	movq	%rdx, %r15
	jg	.LBB4_2
.LBB4_5:                                # %cont_Deref.exit
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.LBB4_11
# BB#6:                                 # %.lr.ph.i43.preheader
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph.i43
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %r14
	je	.LBB4_10
# BB#8:                                 #   in Loop: Header=BB4_7 Depth=1
	movslq	%edx, %rdx
	shlq	$5, %rdx
	movq	8(%r14,%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB4_10
# BB#9:                                 # %.thread.i48
                                        #   in Loop: Header=BB4_7 Depth=1
	movq	16(%r14,%rdx), %r14
	movl	(%rcx), %edx
	testl	%edx, %edx
	movq	%rcx, %rbx
	jg	.LBB4_7
.LBB4_10:                               # %cont_Deref.exit50.loopexit
	movl	(%rbx), %edx
.LBB4_11:                               # %cont_Deref.exit50
	movl	(%r15), %esi
	testl	%esi, %esi
	jle	.LBB4_13
# BB#12:
	xorl	%eax, %eax
	cmpl	%edx, %esi
	sete	%al
	addl	%eax, %eax
	jmp	.LBB4_81
.LBB4_13:
	testl	%edx, %edx
	jle	.LBB4_15
# BB#14:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	cont_TermContainsSymbol
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	leal	(%rcx,%rcx,2), %r13d
.LBB4_80:                               # %rpos_ContMulGreaterEqual.exit
	movl	%r13d, %eax
.LBB4_81:                               # %rpos_ContMulGreaterEqual.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_15:
	cmpl	%edx, %esi
	jne	.LBB4_69
# BB#16:
	negl	%edx
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	20(%rax), %eax
	testb	$16, %al
	jne	.LBB4_17
# BB#47:
	testb	$8, %al
	movq	16(%r15), %rdi
	jne	.LBB4_48
# BB#49:
	movq	16(%rbx), %rbp
	jmp	.LBB4_50
.LBB4_69:
	movq	ord_PRECEDENCE(%rip), %rax
	negl	%esi
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %esi
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %esi
	negl	%edx
	sarl	%cl, %edx
	movslq	%edx, %rcx
	cmpl	(%rax,%rcx,4), %esi
	jge	.LBB4_75
# BB#70:
	movq	16(%rbx), %rbp
	movl	$3, %r13d
	testq	%rbp, %rbp
	jne	.LBB4_73
	jmp	.LBB4_80
	.p2align	4, 0x90
.LBB4_71:                               #   in Loop: Header=BB4_73 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB4_80
.LBB4_73:                               # %.lr.ph111
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	rpos_ContGreaterEqual
	cmpl	$3, %eax
	je	.LBB4_71
# BB#74:
	xorl	%r13d, %r13d
	jmp	.LBB4_80
.LBB4_17:
	movq	16(%r15), %rdi
	callq	list_Copy
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB4_26
# BB#18:                                # %.preheader.lr.ph.i.i
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB4_19
	.p2align	4, 0x90
.LBB4_20:                               # %.preheader.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_21 Depth 2
	movq	8(%rsp), %r13           # 8-byte Reload
	.p2align	4, 0x90
.LBB4_21:                               #   Parent Loop BB4_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB4_24
# BB#22:                                #   in Loop: Header=BB4_21 Depth=2
	movq	8(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	rpos_ContEqual
	testl	%eax, %eax
	jne	.LBB4_23
.LBB4_24:                               #   in Loop: Header=BB4_21 Depth=2
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB4_21
	jmp	.LBB4_25
.LBB4_23:                               #   in Loop: Header=BB4_20 Depth=1
	movq	$0, 8(%r13)
.LBB4_25:                               # %.loopexit.i.i
                                        #   in Loop: Header=BB4_20 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB4_20
	jmp	.LBB4_26
	.p2align	4, 0x90
.LBB4_19:                               # %.preheader.us.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB4_19
.LBB4_26:                               # %rpos_ContMultisetDifference.exit.i
	xorl	%esi, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	list_PointerDeleteElement
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB4_27
# BB#28:
	movq	16(%rbx), %rdi
	callq	list_Copy
	movq	%rax, %rbx
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.LBB4_37
# BB#29:                                # %.preheader.lr.ph.i36.i
	testq	%rbx, %rbx
	je	.LBB4_30
	.p2align	4, 0x90
.LBB4_31:                               # %.preheader.i43.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_32 Depth 2
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB4_32:                               #   Parent Loop BB4_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rsi
	testq	%rsi, %rsi
	je	.LBB4_35
# BB#33:                                #   in Loop: Header=BB4_32 Depth=2
	movq	8(%r15), %rcx
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	rpos_ContEqual
	testl	%eax, %eax
	jne	.LBB4_34
.LBB4_35:                               #   in Loop: Header=BB4_32 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB4_32
	jmp	.LBB4_36
.LBB4_34:                               #   in Loop: Header=BB4_31 Depth=1
	movq	$0, 8(%rbp)
.LBB4_36:                               # %.loopexit.i52.i
                                        #   in Loop: Header=BB4_31 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB4_31
	jmp	.LBB4_37
.LBB4_30:                               # %.preheader.us.i40.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB4_30
.LBB4_37:                               # %rpos_ContMultisetDifference.exit53.i
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	list_PointerDeleteElement
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB4_38
	.p2align	4, 0x90
.LBB4_39:                               # %.preheader78.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_40 Depth 2
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB4_40:                               #   Parent Loop BB4_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	movq	8(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	rpos_ContGreaterEqual
	movq	(%rbx), %rbx
	cmpl	$3, %eax
	setne	%cl
	testq	%rbx, %rbx
	je	.LBB4_42
# BB#41:                                #   in Loop: Header=BB4_40 Depth=2
	testb	%cl, %cl
	jne	.LBB4_40
.LBB4_42:                               # %..critedge.i54_crit_edge.us
                                        #   in Loop: Header=BB4_39 Depth=1
	movq	(%rbp), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbp, (%rdx)
	testq	%rcx, %rcx
	je	.LBB4_44
# BB#43:                                # %..critedge.i54_crit_edge.us
                                        #   in Loop: Header=BB4_39 Depth=1
	cmpl	$3, %eax
	movq	%rcx, %rbp
	je	.LBB4_39
.LBB4_44:                               # %._crit_edge106.loopexit123
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	sete	%cl
	leal	(%rcx,%rcx,2), %esi
	jmp	.LBB4_45
.LBB4_75:
	movq	16(%r15), %rbp
	xorl	%r13d, %r13d
	testq	%rbp, %rbp
	jne	.LBB4_78
	jmp	.LBB4_80
	.p2align	4, 0x90
.LBB4_76:                               #   in Loop: Header=BB4_78 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB4_80
.LBB4_78:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rbx, %rcx
	callq	rpos_ContGreaterEqual
	testl	%eax, %eax
	je	.LBB4_76
# BB#79:
	movl	$3, %r13d
	jmp	.LBB4_80
.LBB4_48:
	callq	list_Reverse
	movq	%rax, %rbp
	movq	16(%rbx), %rdi
	callq	list_Reverse
	movq	%rbp, %rdi
	movq	%rax, %rbp
.LBB4_50:
	movl	$2, %r13d
	testq	%rdi, %rdi
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	je	.LBB4_64
# BB#51:                                # %.lr.ph92.preheader
	movq	%rdi, %r13
	.p2align	4, 0x90
.LBB4_52:                               # %.lr.ph92
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %rsi
	movq	8(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	rpos_ContGreaterEqual
	cmpl	$2, %eax
	jne	.LBB4_53
# BB#58:                                #   in Loop: Header=BB4_52 Depth=1
	movq	(%r13), %r13
	movq	(%rbp), %rbp
	testq	%r13, %r13
	jne	.LBB4_52
# BB#59:
	movl	$2, %r13d
	jmp	.LBB4_64
.LBB4_27:
	movl	$2, %r13d
	jmp	.LBB4_80
.LBB4_53:                               # %.lr.ph92
	cmpl	$3, %eax
	jne	.LBB4_54
.LBB4_60:                               # %.preheader76
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB4_61
# BB#62:                                #   in Loop: Header=BB4_60 Depth=1
	movq	8(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	rpos_ContGreaterEqual
	cmpl	$3, %eax
	je	.LBB4_60
# BB#63:
	xorl	%r13d, %r13d
	jmp	.LBB4_64
.LBB4_38:
	movl	$3, %esi
	.p2align	4, 0x90
.LBB4_45:                               # %.lr.ph.i.i58
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB4_45
# BB#46:
	movl	%esi, %r13d
	jmp	.LBB4_80
.LBB4_54:                               # %.preheader
	movq	(%r13), %rbp
	xorl	%r13d, %r13d
	testq	%rbp, %rbp
	jne	.LBB4_56
	jmp	.LBB4_64
.LBB4_57:                               # %.lr.ph
                                        #   in Loop: Header=BB4_56 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB4_64
.LBB4_56:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rbx, %rcx
	callq	rpos_ContGreaterEqual
	testl	%eax, %eax
	movl	$3, %eax
	cmovnel	%eax, %r13d
	cmpl	$3, %r13d
	jne	.LBB4_57
	jmp	.LBB4_64
.LBB4_61:
	movl	$3, %r13d
.LBB4_64:                               # %.critedge1.i
	xorl	%eax, %eax
	subl	(%r15), %eax
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	testb	$8, 20(%rax)
	je	.LBB4_80
# BB#65:
	movq	16(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB4_67
	.p2align	4, 0x90
.LBB4_66:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB4_66
.LBB4_67:                               # %list_Delete.exit.i
	movq	8(%rsp), %rsi           # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB4_80
	.p2align	4, 0x90
.LBB4_68:                               # %.lr.ph.i63.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB4_68
	jmp	.LBB4_80
.Lfunc_end4:
	.size	rpos_ContGreaterEqual, .Lfunc_end4-rpos_ContGreaterEqual
	.cfi_endproc

	.globl	rpos_ContCompare
	.p2align	4, 0x90
	.type	rpos_ContCompare,@function
rpos_ContCompare:                       # @rpos_ContCompare
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 48
.Lcfi55:
	.cfi_offset %rbx, -40
.Lcfi56:
	.cfi_offset %r12, -32
.Lcfi57:
	.cfi_offset %r14, -24
.Lcfi58:
	.cfi_offset %r15, -16
	movq	%rcx, %r12
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jle	.LBB5_5
# BB#1:                                 # %.lr.ph.i.preheader
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %rbx
	je	.LBB5_5
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	movq	8(%rbx,%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB5_5
# BB#4:                                 # %.thread.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	16(%rbx,%rcx), %rbx
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	movq	%rdx, %r15
	jg	.LBB5_2
.LBB5_5:                                # %cont_Deref.exit
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.LBB5_10
# BB#6:                                 # %.lr.ph.i12.preheader
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB5_7:                                # %.lr.ph.i12
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %r14
	je	.LBB5_10
# BB#8:                                 #   in Loop: Header=BB5_7 Depth=1
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	movq	8(%r14,%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB5_10
# BB#9:                                 # %.thread.i17
                                        #   in Loop: Header=BB5_7 Depth=1
	movq	16(%r14,%rcx), %r14
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	movq	%rdx, %r12
	jg	.LBB5_7
.LBB5_10:                               # %cont_Deref.exit19
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r12, %rcx
	callq	rpos_ContGreaterEqual
	testl	%eax, %eax
	jne	.LBB5_12
# BB#11:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	rpos_ContGreaterEqual
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	sete	%cl
	movl	%ecx, %eax
.LBB5_12:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	rpos_ContCompare, .Lfunc_end5-rpos_ContCompare
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
