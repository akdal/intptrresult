	.text
	.file	"CoderMixer2MT.bc"
	.globl	_ZN11NCoderMixer7CCoder2C2Ejj
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer7CCoder2C2Ejj,@function
_ZN11NCoderMixer7CCoder2C2Ejj:          # @_ZN11NCoderMixer7CCoder2C2Ejj
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	240(%rbx), %r14
	movq	%r14, %rdi
	callq	_ZN11NCoderMixer11CCoderInfo2C2Ejj
	movl	$0, 8(%rbx)
	movl	$0, 112(%rbx)
	movl	$0, 224(%rbx)
	movq	$_ZTVN11NCoderMixer7CCoder2E+16, (%rbx)
	leaq	400(%rbx), %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 408(%rbx)
	movq	$8, 424(%rbx)
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, 400(%rbx)
	leaq	432(%rbx), %r12
	movups	%xmm0, 440(%rbx)
	movq	$8, 456(%rbx)
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, 432(%rbx)
	leaq	464(%rbx), %rbp
	movups	%xmm0, 472(%rbx)
	movq	$8, 488(%rbx)
	movq	$_ZTV13CRecordVectorIP19ISequentialInStreamE+16, 464(%rbx)
	leaq	496(%rbx), %r13
	movups	%xmm0, 504(%rbx)
	movq	$8, 520(%rbx)
	movq	$_ZTV13CRecordVectorIP20ISequentialOutStreamE+16, 496(%rbx)
	movl	256(%rbx), %esi
.Ltmp0:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp1:
# BB#1:
	movl	256(%rbx), %esi
.Ltmp2:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp3:
# BB#2:
	movl	260(%rbx), %esi
.Ltmp4:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp5:
# BB#3:
	movl	260(%rbx), %esi
.Ltmp6:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp7:
# BB#4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_5:
.Ltmp8:
	movq	%rax, (%rsp)            # 8-byte Spill
.Ltmp9:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp10:
# BB#6:
.Ltmp11:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp12:
# BB#7:
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, (%r12)
.Ltmp13:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp14:
# BB#8:
.Ltmp19:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp20:
# BB#9:                                 # %_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev.exit
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, (%r15)
.Ltmp21:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp22:
# BB#10:
.Ltmp27:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp28:
# BB#11:                                # %_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev.exit
.Ltmp29:
	movq	%rbx, %rdi
	callq	_ZN11CVirtThreadD2Ev
.Ltmp30:
# BB#12:
.Ltmp31:
	movq	%r14, %rdi
	callq	_ZN11NCoderMixer11CCoderInfo2D2Ev
.Ltmp32:
# BB#13:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_Unwind_Resume
.LBB0_16:
.Ltmp23:
	movq	%rax, %rbx
.Ltmp24:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp25:
	jmp	.LBB0_19
.LBB0_17:
.Ltmp26:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_14:
.Ltmp15:
	movq	%rax, %rbx
.Ltmp16:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp17:
	jmp	.LBB0_19
.LBB0_15:
.Ltmp18:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_18:
.Ltmp33:
	movq	%rax, %rbx
.LBB0_19:                               # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN11NCoderMixer7CCoder2C2Ejj, .Lfunc_end0-_ZN11NCoderMixer7CCoder2C2Ejj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp0           #   Call between .Ltmp0 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp12-.Ltmp9          #   Call between .Ltmp9 and .Ltmp12
	.long	.Ltmp33-.Lfunc_begin0   #     jumps to .Ltmp33
	.byte	1                       #   On action: 1
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	1                       #   On action: 1
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp33-.Lfunc_begin0   #     jumps to .Ltmp33
	.byte	1                       #   On action: 1
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin0   #     jumps to .Ltmp23
	.byte	1                       #   On action: 1
	.long	.Ltmp27-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp32-.Ltmp27         #   Call between .Ltmp27 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin0   #     jumps to .Ltmp33
	.byte	1                       #   On action: 1
	.long	.Ltmp32-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp24-.Ltmp32         #   Call between .Ltmp32 and .Ltmp24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin0   #     jumps to .Ltmp26
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin0   #     jumps to .Ltmp18
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.text._ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev,@function
_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev: # @_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, (%rbx)
.Ltmp34:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp35:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB2_2:
.Ltmp36:
	movq	%rax, %r14
.Ltmp37:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp38:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_4:
.Ltmp39:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev, .Lfunc_end2-_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp34-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin1   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp37-.Ltmp35         #   Call between .Ltmp35 and .Ltmp37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin1   #     jumps to .Ltmp39
	.byte	1                       #   On action: 1
	.long	.Ltmp38-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp38     #   Call between .Ltmp38 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev,@function
_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev: # @_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, (%rbx)
.Ltmp40:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp41:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB3_2:
.Ltmp42:
	movq	%rax, %r14
.Ltmp43:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp44:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_4:
.Ltmp45:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev, .Lfunc_end3-_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp40-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin2   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp43-.Ltmp41         #   Call between .Ltmp41 and .Ltmp43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin2   #     jumps to .Ltmp45
	.byte	1                       #   On action: 1
	.long	.Ltmp44-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp44     #   Call between .Ltmp44 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11NCoderMixer11CCoderInfo2D2Ev,"axG",@progbits,_ZN11NCoderMixer11CCoderInfo2D2Ev,comdat
	.weak	_ZN11NCoderMixer11CCoderInfo2D2Ev
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer11CCoderInfo2D2Ev,@function
_ZN11NCoderMixer11CCoderInfo2D2Ev:      # @_ZN11NCoderMixer11CCoderInfo2D2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	120(%rbx), %rdi
.Ltmp46:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp47:
# BB#1:
	leaq	88(%rbx), %rdi
.Ltmp51:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp52:
# BB#2:
	leaq	56(%rbx), %rdi
.Ltmp56:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp57:
# BB#3:
	leaq	24(%rbx), %rdi
.Ltmp61:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp62:
# BB#4:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp66:
	callq	*16(%rax)
.Ltmp67:
.LBB4_6:                                # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_7
# BB#21:
	movq	(%rdi), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*16(%rax)               # TAILCALL
.LBB4_7:                                # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_13:
.Ltmp68:
	movq	%rax, %r14
	jmp	.LBB4_17
.LBB4_14:
.Ltmp63:
	movq	%rax, %r14
	jmp	.LBB4_15
.LBB4_11:
.Ltmp58:
	movq	%rax, %r14
	jmp	.LBB4_12
.LBB4_9:
.Ltmp53:
	movq	%rax, %r14
	jmp	.LBB4_10
.LBB4_8:
.Ltmp48:
	movq	%rax, %r14
	leaq	88(%rbx), %rdi
.Ltmp49:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp50:
.LBB4_10:
	leaq	56(%rbx), %rdi
.Ltmp54:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp55:
.LBB4_12:
	leaq	24(%rbx), %rdi
.Ltmp59:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp60:
.LBB4_15:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_17
# BB#16:
	movq	(%rdi), %rax
.Ltmp64:
	callq	*16(%rax)
.Ltmp65:
.LBB4_17:                               # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit7
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_19
# BB#18:
	movq	(%rdi), %rax
.Ltmp69:
	callq	*16(%rax)
.Ltmp70:
.LBB4_19:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit9
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_20:
.Ltmp71:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN11NCoderMixer11CCoderInfo2D2Ev, .Lfunc_end4-_ZN11NCoderMixer11CCoderInfo2D2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp46-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin3   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin3   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin3   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin3   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin3   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp49-.Ltmp67         #   Call between .Ltmp67 and .Ltmp49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp70-.Ltmp49         #   Call between .Ltmp49 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin3   #     jumps to .Ltmp71
	.byte	1                       #   On action: 1
	.long	.Ltmp70-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Lfunc_end4-.Ltmp70     #   Call between .Ltmp70 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN11NCoderMixer7CCoder27ExecuteEv
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer7CCoder27ExecuteEv,@function
_ZN11NCoderMixer7CCoder27ExecuteEv:     # @_ZN11NCoderMixer7CCoder27ExecuteEv
	.cfi_startproc
# BB#0:
	xorl	%esi, %esi
	jmp	_ZN11NCoderMixer7CCoder24CodeEP21ICompressProgressInfo # TAILCALL
.Lfunc_end5:
	.size	_ZN11NCoderMixer7CCoder27ExecuteEv, .Lfunc_end5-_ZN11NCoderMixer7CCoder27ExecuteEv
	.cfi_endproc

	.globl	_ZN11NCoderMixer7CCoder24CodeEP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer7CCoder24CodeEP21ICompressProgressInfo,@function
_ZN11NCoderMixer7CCoder24CodeEP21ICompressProgressInfo: # @_ZN11NCoderMixer7CCoder24CodeEP21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 64
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	leaq	464(%r13), %r12
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	496(%r13), %r15
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	cmpl	$0, 256(%r13)
	je	.LBB6_5
# BB#1:                                 # %.lr.ph37
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	344(%r13), %rax
	movslq	%ebx, %rbx
	cmpq	$0, (%rax,%rbx,8)
	je	.LBB6_4
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	leaq	(,%rbx,8), %rcx
	addq	280(%r13), %rcx
	movq	%rcx, (%rax,%rbx,8)
.LBB6_4:                                #   in Loop: Header=BB6_2 Depth=1
	movq	416(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rbp
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	480(%r13), %rax
	movslq	476(%r13), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 476(%r13)
	incl	%ebx
	cmpl	256(%r13), %ebx
	jb	.LBB6_2
.LBB6_5:                                # %.preheader26
	cmpl	$0, 260(%r13)
	je	.LBB6_6
# BB#7:                                 # %.lr.ph33
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_8:                                # =>This Inner Loop Header: Depth=1
	movq	376(%r13), %rax
	movslq	%ebx, %rbx
	cmpq	$0, (%rax,%rbx,8)
	je	.LBB6_10
# BB#9:                                 #   in Loop: Header=BB6_8 Depth=1
	leaq	(,%rbx,8), %rcx
	addq	312(%r13), %rcx
	movq	%rcx, (%rax,%rbx,8)
.LBB6_10:                               #   in Loop: Header=BB6_8 Depth=1
	movq	448(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rbp
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	512(%r13), %rax
	movslq	508(%r13), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 508(%r13)
	incl	%ebx
	movl	260(%r13), %eax
	cmpl	%eax, %ebx
	jb	.LBB6_8
	jmp	.LBB6_11
.LBB6_6:
	xorl	%eax, %eax
.LBB6_11:                               # %._crit_edge34
	movq	240(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB6_13
# BB#12:
	movq	(%rdi), %rax
	movq	480(%r13), %rcx
	movq	(%rcx), %rsi
	movq	512(%r13), %rcx
	movq	(%rcx), %rdx
	movq	344(%r13), %rcx
	movq	376(%r13), %rbp
	movq	(%rcx), %rcx
	movq	(%rbp), %r8
	movq	%r14, %r9
	callq	*40(%rax)
	jmp	.LBB6_14
.LBB6_13:
	movq	248(%r13), %rdi
	movq	(%rdi), %rbp
	movq	480(%r13), %rsi
	movq	344(%r13), %rdx
	movl	256(%r13), %ecx
	movq	512(%r13), %r8
	movq	376(%r13), %r9
	pushq	%r14
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	callq	*40(%rbp)
	addq	$16, %rsp
.Lcfi43:
	.cfi_adjust_cfa_offset -16
.LBB6_14:
	movl	%eax, 392(%r13)
	movl	412(%r13), %eax
	testl	%eax, %eax
	jle	.LBB6_19
# BB#15:                                # %.lr.ph31
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_16:                               # =>This Inner Loop Header: Depth=1
	movq	416(%r13), %rcx
	movq	(%rcx,%rbx,8), %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB6_18
# BB#17:                                #   in Loop: Header=BB6_16 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, (%r14)
	movl	412(%r13), %eax
.LBB6_18:                               # %_ZN9CMyComPtrI19ISequentialInStreamE7ReleaseEv.exit
                                        #   in Loop: Header=BB6_16 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB6_16
.LBB6_19:                               # %.preheader
	movl	444(%r13), %eax
	testl	%eax, %eax
	jle	.LBB6_24
# BB#20:                                # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_21:                               # =>This Inner Loop Header: Depth=1
	movq	448(%r13), %rcx
	movq	(%rcx,%rbx,8), %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB6_23
# BB#22:                                #   in Loop: Header=BB6_21 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, (%r14)
	movl	444(%r13), %eax
.LBB6_23:                               # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
                                        #   in Loop: Header=BB6_21 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB6_21
.LBB6_24:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN11NCoderMixer7CCoder24CodeEP21ICompressProgressInfo, .Lfunc_end6-_ZN11NCoderMixer7CCoder24CodeEP21ICompressProgressInfo
	.cfi_endproc

	.globl	_ZN11NCoderMixer7CCoder212SetCoderInfoEPPKyS3_
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer7CCoder212SetCoderInfoEPPKyS3_,@function
_ZN11NCoderMixer7CCoder212SetCoderInfoEPPKyS3_: # @_ZN11NCoderMixer7CCoder212SetCoderInfoEPPKyS3_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -24
.Lcfi48:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	leaq	264(%rbx), %rax
	leaq	328(%rbx), %rdx
	movl	256(%rbx), %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej
	movl	260(%rbx), %ecx
	leaq	296(%rbx), %rsi
	leaq	360(%rbx), %rdx
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej # TAILCALL
.Lfunc_end7:
	.size	_ZN11NCoderMixer7CCoder212SetCoderInfoEPPKyS3_, .Lfunc_end7-_ZN11NCoderMixer7CCoder212SetCoderInfoEPPKyS3_
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej,@function
_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej: # @_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 64
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	testl	%ebp, %ebp
	je	.LBB8_6
# BB#1:                                 # %.lr.ph
	testq	%rbx, %rbx
	je	.LBB8_5
# BB#2:                                 # %.lr.ph.split.preheader
	movl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_3:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB8_4
# BB#7:                                 #   in Loop: Header=BB8_3 Depth=1
	movq	(%rax), %r13
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	leaq	(%rax,%rcx,8), %r12
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	jmp	.LBB8_8
	.p2align	4, 0x90
.LBB8_4:                                #   in Loop: Header=BB8_3 Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	$0, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	xorl	%r12d, %r12d
.LBB8_8:                                #   in Loop: Header=BB8_3 Depth=1
	movq	16(%r14), %rax
	movslq	12(%r14), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r14)
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB8_3
	jmp	.LBB8_6
	.p2align	4, 0x90
.LBB8_5:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	$0, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r14), %rax
	movslq	12(%r14), %rcx
	movq	$0, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r14)
	decl	%ebp
	jne	.LBB8_5
.LBB8_6:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej, .Lfunc_end8-_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej
	.cfi_endproc

	.globl	_ZN11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE,@function
_ZN11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE: # @_ZN11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 240
.Lcfi69:
	.cfi_offset %rbx, -56
.Lcfi70:
	.cfi_offset %r12, -48
.Lcfi71:
	.cfi_offset %r13, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	leaq	24(%r12), %rdi
	callq	_ZN11NCoderMixer9CBindInfoaSERKS0_
	leaq	152(%r12), %r15
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	cmpl	$0, 68(%r12)
	jle	.LBB9_1
# BB#3:                                 # %.lr.ph
	leaq	152(%rsp), %r13
	leaq	24(%rsp), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_4:                                # =>This Inner Loop Header: Depth=1
	movq	$0, 8(%rsp)
	movq	$_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE+16, (%rsp)
	movl	$0, 24(%rsp)
	movq	$0, 136(%rsp)
	movq	$_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE+16, 128(%rsp)
	movq	$0, 152(%rsp)
.Ltmp72:
	movl	$184, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp73:
# BB#5:                                 # %.noexc
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	$0, 8(%rbx)
	movzwl	16(%rsp), %eax
	movw	%ax, 16(%rbx)
	movq	$_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE+16, (%rbx)
	movq	96(%r14), %rax
	movq	%rax, 120(%rbx)
	movups	80(%r14), %xmm0
	movups	%xmm0, 104(%rbx)
	movups	64(%r14), %xmm0
	movups	%xmm0, 88(%rbx)
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	32(%r14), %xmm2
	movups	48(%r14), %xmm3
	movups	%xmm3, 72(%rbx)
	movups	%xmm2, 56(%rbx)
	movups	%xmm1, 40(%rbx)
	movups	%xmm0, 24(%rbx)
	movq	$0, 136(%rbx)
	movzwl	144(%rsp), %eax
	movw	%ax, 144(%rbx)
	movq	$_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE+16, 128(%rbx)
	movups	(%r13), %xmm0
	movups	16(%r13), %xmm1
	movups	%xmm1, 168(%rbx)
	movups	%xmm0, 152(%rbx)
.Ltmp74:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp75:
# BB#6:                                 #   in Loop: Header=BB9_4 Depth=1
	movq	168(%r12), %rax
	movslq	164(%r12), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 164(%r12)
	movq	152(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB9_10
# BB#7:                                 #   in Loop: Header=BB9_4 Depth=1
	cmpb	$0, 88(%rbx)
	je	.LBB9_9
# BB#8:                                 #   in Loop: Header=BB9_4 Depth=1
	movq	%rbx, %rdi
	callq	pthread_mutex_destroy
	leaq	40(%rbx), %rdi
	callq	pthread_cond_destroy
.LBB9_9:                                #   in Loop: Header=BB9_4 Depth=1
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB9_10:                               # %_ZN13CStreamBinderD2Ev.exit18
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	$0, 152(%rsp)
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 128(%rsp)
	movq	$0, 136(%rsp)
	movq	%r14, %rdi
	callq	Event_Close
	movslq	164(%r12), %rax
	movq	168(%r12), %rcx
	movq	-8(%rcx,%rax,8), %rdi
	callq	_ZN13CStreamBinder12CreateEventsEv
	testl	%eax, %eax
	jne	.LBB9_2
# BB#11:                                #   in Loop: Header=BB9_4 Depth=1
	incl	%ebp
	cmpl	68(%r12), %ebp
	jl	.LBB9_4
.LBB9_1:
	xorl	%eax, %eax
.LBB9_2:                                # %._crit_edge
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_12:
.Ltmp76:
	movq	%rax, %r15
	movq	152(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB9_16
# BB#13:
	cmpb	$0, 88(%rbx)
	je	.LBB9_15
# BB#14:
	movq	%rbx, %rdi
	callq	pthread_mutex_destroy
	leaq	40(%rbx), %rdi
	callq	pthread_cond_destroy
.LBB9_15:
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB9_16:
	movq	$0, 152(%rsp)
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 128(%rsp)
	movq	$0, 136(%rsp)
.Ltmp77:
	movq	%r14, %rdi
	callq	Event_Close
.Ltmp78:
# BB#17:
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB9_18:                               # %.body
.Ltmp79:
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, (%rsp)
	movq	$0, 8(%rsp)
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE, .Lfunc_end9-_ZN11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp72-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp72
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp75-.Ltmp72         #   Call between .Ltmp72 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin4   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp77-.Ltmp75         #   Call between .Ltmp75 and .Ltmp77
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin4   #     jumps to .Ltmp79
	.byte	1                       #   On action: 1
	.long	.Ltmp78-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Lfunc_end9-.Ltmp78     #   Call between .Ltmp78 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11NCoderMixer9CBindInfoaSERKS0_,"axG",@progbits,_ZN11NCoderMixer9CBindInfoaSERKS0_,comdat
	.weak	_ZN11NCoderMixer9CBindInfoaSERKS0_
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer9CBindInfoaSERKS0_,@function
_ZN11NCoderMixer9CBindInfoaSERKS0_:     # @_ZN11NCoderMixer9CBindInfoaSERKS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi79:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi81:
	.cfi_def_cfa_offset 64
.Lcfi82:
	.cfi_offset %rbx, -56
.Lcfi83:
	.cfi_offset %r12, -48
.Lcfi84:
	.cfi_offset %r13, -40
.Lcfi85:
	.cfi_offset %r14, -32
.Lcfi86:
	.cfi_offset %r15, -24
.Lcfi87:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	12(%r14), %r15d
	movl	12(%r13), %esi
	addl	%r15d, %esi
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r15d, %r15d
	jle	.LBB10_3
# BB#1:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r12
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r13), %rax
	movslq	12(%r13), %rcx
	movq	%r12, (%rax,%rcx,8)
	incl	12(%r13)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB10_2
.LBB10_3:                               # %_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEEaSERKS2_.exit
	leaq	32(%r13), %r15
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	44(%r14), %r12d
	movl	44(%r13), %esi
	addl	%r12d, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r12d, %r12d
	jle	.LBB10_6
# BB#4:                                 # %.lr.ph.i.i6
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_5:                               # =>This Inner Loop Header: Depth=1
	movq	48(%r14), %rax
	movq	(%rax,%rbx,8), %rbp
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	48(%r13), %rax
	movslq	44(%r13), %rcx
	movq	%rbp, (%rax,%rcx,8)
	incl	44(%r13)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB10_5
.LBB10_6:                               # %_ZN13CRecordVectorIN11NCoderMixer9CBindPairEEaSERKS2_.exit
	leaq	64(%r13), %r15
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	76(%r14), %r12d
	movl	76(%r13), %esi
	addl	%r12d, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r12d, %r12d
	jle	.LBB10_9
# BB#7:                                 # %.lr.ph.i.i14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_8:                               # =>This Inner Loop Header: Depth=1
	movq	80(%r14), %rax
	movl	(%rax,%rbx,4), %ebp
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	80(%r13), %rax
	movslq	76(%r13), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	76(%r13)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB10_8
.LBB10_9:                               # %_ZN13CRecordVectorIjEaSERKS0_.exit
	leaq	96(%r13), %r15
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	108(%r14), %r12d
	movl	108(%r13), %esi
	addl	%r12d, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r12d, %r12d
	jle	.LBB10_12
# BB#10:                                # %.lr.ph.i.i19
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_11:                              # =>This Inner Loop Header: Depth=1
	movq	112(%r14), %rax
	movl	(%rax,%rbx,4), %ebp
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	112(%r13), %rax
	movslq	108(%r13), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	108(%r13)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB10_11
.LBB10_12:                              # %_ZN13CRecordVectorIjEaSERKS0_.exit23
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN11NCoderMixer9CBindInfoaSERKS0_, .Lfunc_end10-_ZN11NCoderMixer9CBindInfoaSERKS0_
	.cfi_endproc

	.text
	.globl	_ZThn8_N11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE
	.p2align	4, 0x90
	.type	_ZThn8_N11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE,@function
_ZThn8_N11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE: # @_ZThn8_N11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE # TAILCALL
.Lfunc_end11:
	.size	_ZThn8_N11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE, .Lfunc_end11-_ZThn8_N11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE
	.cfi_endproc

	.globl	_ZN11NCoderMixer14CCoderMixer2MT14AddCoderCommonEv
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer14CCoderMixer2MT14AddCoderCommonEv,@function
_ZN11NCoderMixer14CCoderMixer2MT14AddCoderCommonEv: # @_ZN11NCoderMixer14CCoderMixer2MT14AddCoderCommonEv
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 24
	subq	$536, %rsp              # imm = 0x218
.Lcfi90:
	.cfi_def_cfa_offset 560
.Lcfi91:
	.cfi_offset %rbx, -24
.Lcfi92:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	40(%rbx), %rax
	movslq	204(%rbx), %rcx
	movl	(%rax,%rcx,8), %esi
	movl	4(%rax,%rcx,8), %edx
	leaq	8(%rsp), %rdi
	callq	_ZN11NCoderMixer7CCoder2C2Ejj
.Ltmp80:
	movl	$528, %edi              # imm = 0x210
	callq	_Znwm
	movq	%rax, %r14
.Ltmp81:
# BB#1:                                 # %.noexc
.Ltmp82:
	leaq	8(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZN11NCoderMixer7CCoder2C2ERKS0_
.Ltmp83:
# BB#2:
	leaq	192(%rbx), %rdi
.Ltmp85:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp86:
# BB#3:
	movq	208(%rbx), %rax
	movslq	204(%rbx), %rcx
	movq	%r14, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 204(%rbx)
	leaq	8(%rsp), %rdi
	callq	_ZN11NCoderMixer7CCoder2D2Ev
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r14
	retq
.LBB12_8:
.Ltmp84:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdlPv
	jmp	.LBB12_5
.LBB12_4:
.Ltmp87:
	movq	%rax, %rbx
.LBB12_5:                               # %.body
.Ltmp88:
	leaq	8(%rsp), %rdi
	callq	_ZN11NCoderMixer7CCoder2D2Ev
.Ltmp89:
# BB#6:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB12_7:
.Ltmp90:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN11NCoderMixer14CCoderMixer2MT14AddCoderCommonEv, .Lfunc_end12-_ZN11NCoderMixer14CCoderMixer2MT14AddCoderCommonEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp80-.Lfunc_begin5   #   Call between .Lfunc_begin5 and .Ltmp80
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp80-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp87-.Lfunc_begin5   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin5   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin5   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp86-.Lfunc_begin5   # >> Call Site 5 <<
	.long	.Ltmp88-.Ltmp86         #   Call between .Ltmp86 and .Ltmp88
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin5   # >> Call Site 6 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin5   #     jumps to .Ltmp90
	.byte	1                       #   On action: 1
	.long	.Ltmp89-.Lfunc_begin5   # >> Call Site 7 <<
	.long	.Lfunc_end12-.Ltmp89    #   Call between .Ltmp89 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11NCoderMixer7CCoder2D2Ev,"axG",@progbits,_ZN11NCoderMixer7CCoder2D2Ev,comdat
	.weak	_ZN11NCoderMixer7CCoder2D2Ev
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer7CCoder2D2Ev,@function
_ZN11NCoderMixer7CCoder2D2Ev:           # @_ZN11NCoderMixer7CCoder2D2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 32
.Lcfi96:
	.cfi_offset %rbx, -32
.Lcfi97:
	.cfi_offset %r14, -24
.Lcfi98:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTVN11NCoderMixer7CCoder2E+16, (%r15)
	leaq	496(%r15), %rdi
.Ltmp91:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp92:
# BB#1:
	leaq	464(%r15), %rdi
.Ltmp96:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp97:
# BB#2:
	leaq	432(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, 432(%r15)
.Ltmp107:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp108:
# BB#3:
.Ltmp113:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp114:
# BB#4:                                 # %_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev.exit
	leaq	400(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, 400(%r15)
.Ltmp124:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp125:
# BB#5:
.Ltmp130:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp131:
# BB#6:                                 # %_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev.exit
.Ltmp135:
	movq	%r15, %rdi
	callq	_ZN11CVirtThreadD2Ev
.Ltmp136:
# BB#7:
	addq	$240, %r15
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN11NCoderMixer11CCoderInfo2D2Ev # TAILCALL
.LBB13_25:
.Ltmp137:
	movq	%rax, %r14
	jmp	.LBB13_26
.LBB13_22:
.Ltmp132:
	movq	%rax, %r14
	jmp	.LBB13_19
.LBB13_10:
.Ltmp126:
	movq	%rax, %r14
.Ltmp127:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp128:
	jmp	.LBB13_19
.LBB13_11:
.Ltmp129:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_13:
.Ltmp115:
	movq	%rax, %r14
	jmp	.LBB13_17
.LBB13_8:
.Ltmp109:
	movq	%rax, %r14
.Ltmp110:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp111:
	jmp	.LBB13_17
.LBB13_9:
.Ltmp112:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_14:
.Ltmp98:
	movq	%rax, %r14
	jmp	.LBB13_15
.LBB13_12:
.Ltmp93:
	movq	%rax, %r14
	leaq	464(%r15), %rdi
.Ltmp94:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp95:
.LBB13_15:
	leaq	432(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, 432(%r15)
.Ltmp99:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp100:
# BB#16:
.Ltmp105:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp106:
.LBB13_17:                              # %_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev.exit10
	leaq	400(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, 400(%r15)
.Ltmp116:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp117:
# BB#18:
.Ltmp122:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp123:
.LBB13_19:                              # %_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev.exit13
.Ltmp133:
	movq	%r15, %rdi
	callq	_ZN11CVirtThreadD2Ev
.Ltmp134:
.LBB13_26:
	addq	$240, %r15
.Ltmp138:
	movq	%r15, %rdi
	callq	_ZN11NCoderMixer11CCoderInfo2D2Ev
.Ltmp139:
# BB#27:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB13_20:
.Ltmp101:
	movq	%rax, %r14
.Ltmp102:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp103:
	jmp	.LBB13_29
.LBB13_21:
.Ltmp104:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_23:
.Ltmp118:
	movq	%rax, %r14
.Ltmp119:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp120:
	jmp	.LBB13_29
.LBB13_24:
.Ltmp121:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_28:
.Ltmp140:
	movq	%rax, %r14
.LBB13_29:                              # %.body8
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZN11NCoderMixer7CCoder2D2Ev, .Lfunc_end13-_ZN11NCoderMixer7CCoder2D2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\363\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Ltmp91-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin6   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp97-.Ltmp96         #   Call between .Ltmp96 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin6   #     jumps to .Ltmp98
	.byte	0                       #   On action: cleanup
	.long	.Ltmp107-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp108-.Ltmp107       #   Call between .Ltmp107 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin6  #     jumps to .Ltmp109
	.byte	0                       #   On action: cleanup
	.long	.Ltmp113-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp114-.Ltmp113       #   Call between .Ltmp113 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin6  #     jumps to .Ltmp115
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp126-.Lfunc_begin6  #     jumps to .Ltmp126
	.byte	0                       #   On action: cleanup
	.long	.Ltmp130-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin6  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp135-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp136-.Ltmp135       #   Call between .Ltmp135 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin6  #     jumps to .Ltmp137
	.byte	0                       #   On action: cleanup
	.long	.Ltmp136-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp127-.Ltmp136       #   Call between .Ltmp136 and .Ltmp127
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp129-.Lfunc_begin6  #     jumps to .Ltmp129
	.byte	1                       #   On action: 1
	.long	.Ltmp110-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp111-.Ltmp110       #   Call between .Ltmp110 and .Ltmp111
	.long	.Ltmp112-.Lfunc_begin6  #     jumps to .Ltmp112
	.byte	1                       #   On action: 1
	.long	.Ltmp94-.Lfunc_begin6   # >> Call Site 11 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp140-.Lfunc_begin6  #     jumps to .Ltmp140
	.byte	1                       #   On action: 1
	.long	.Ltmp99-.Lfunc_begin6   # >> Call Site 12 <<
	.long	.Ltmp100-.Ltmp99        #   Call between .Ltmp99 and .Ltmp100
	.long	.Ltmp101-.Lfunc_begin6  #     jumps to .Ltmp101
	.byte	1                       #   On action: 1
	.long	.Ltmp105-.Lfunc_begin6  # >> Call Site 13 <<
	.long	.Ltmp106-.Ltmp105       #   Call between .Ltmp105 and .Ltmp106
	.long	.Ltmp140-.Lfunc_begin6  #     jumps to .Ltmp140
	.byte	1                       #   On action: 1
	.long	.Ltmp116-.Lfunc_begin6  # >> Call Site 14 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin6  #     jumps to .Ltmp118
	.byte	1                       #   On action: 1
	.long	.Ltmp122-.Lfunc_begin6  # >> Call Site 15 <<
	.long	.Ltmp139-.Ltmp122       #   Call between .Ltmp122 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin6  #     jumps to .Ltmp140
	.byte	1                       #   On action: 1
	.long	.Ltmp139-.Lfunc_begin6  # >> Call Site 16 <<
	.long	.Ltmp102-.Ltmp139       #   Call between .Ltmp139 and .Ltmp102
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp102-.Lfunc_begin6  # >> Call Site 17 <<
	.long	.Ltmp103-.Ltmp102       #   Call between .Ltmp102 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin6  #     jumps to .Ltmp104
	.byte	1                       #   On action: 1
	.long	.Ltmp119-.Lfunc_begin6  # >> Call Site 18 <<
	.long	.Ltmp120-.Ltmp119       #   Call between .Ltmp119 and .Ltmp120
	.long	.Ltmp121-.Lfunc_begin6  #     jumps to .Ltmp121
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN11NCoderMixer14CCoderMixer2MT8AddCoderEP14ICompressCoder
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer14CCoderMixer2MT8AddCoderEP14ICompressCoder,@function
_ZN11NCoderMixer14CCoderMixer2MT8AddCoderEP14ICompressCoder: # @_ZN11NCoderMixer14CCoderMixer2MT8AddCoderEP14ICompressCoder
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 32
.Lcfi102:
	.cfi_offset %rbx, -24
.Lcfi103:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN11NCoderMixer14CCoderMixer2MT14AddCoderCommonEv
	movslq	204(%rbx), %rax
	movq	208(%rbx), %rcx
	movq	-8(%rcx,%rax,8), %rbx
	testq	%r14, %r14
	je	.LBB14_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB14_2:
	movq	240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB14_4:                               # %_ZN9CMyComPtrI14ICompressCoderEaSEPS0_.exit
	movq	%r14, 240(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end14:
	.size	_ZN11NCoderMixer14CCoderMixer2MT8AddCoderEP14ICompressCoder, .Lfunc_end14-_ZN11NCoderMixer14CCoderMixer2MT8AddCoderEP14ICompressCoder
	.cfi_endproc

	.globl	_ZN11NCoderMixer14CCoderMixer2MT9AddCoder2EP15ICompressCoder2
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer14CCoderMixer2MT9AddCoder2EP15ICompressCoder2,@function
_ZN11NCoderMixer14CCoderMixer2MT9AddCoder2EP15ICompressCoder2: # @_ZN11NCoderMixer14CCoderMixer2MT9AddCoder2EP15ICompressCoder2
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -24
.Lcfi108:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN11NCoderMixer14CCoderMixer2MT14AddCoderCommonEv
	movslq	204(%rbx), %rax
	movq	208(%rbx), %rcx
	movq	-8(%rcx,%rax,8), %rbx
	testq	%r14, %r14
	je	.LBB15_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB15_2:
	movq	248(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB15_4:                               # %_ZN9CMyComPtrI15ICompressCoder2EaSEPS0_.exit
	movq	%r14, 248(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end15:
	.size	_ZN11NCoderMixer14CCoderMixer2MT9AddCoder2EP15ICompressCoder2, .Lfunc_end15-_ZN11NCoderMixer14CCoderMixer2MT9AddCoder2EP15ICompressCoder2
	.cfi_endproc

	.globl	_ZN11NCoderMixer14CCoderMixer2MT6ReInitEv
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer14CCoderMixer2MT6ReInitEv,@function
_ZN11NCoderMixer14CCoderMixer2MT6ReInitEv: # @_ZN11NCoderMixer14CCoderMixer2MT6ReInitEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi111:
	.cfi_def_cfa_offset 32
.Lcfi112:
	.cfi_offset %rbx, -24
.Lcfi113:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpl	$0, 164(%r14)
	jle	.LBB16_3
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_2:                               # =>This Inner Loop Header: Depth=1
	movq	168(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	_ZN13CStreamBinder6ReInitEv
	incq	%rbx
	movslq	164(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB16_2
.LBB16_3:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end16:
	.size	_ZN11NCoderMixer14CCoderMixer2MT6ReInitEv, .Lfunc_end16-_ZN11NCoderMixer14CCoderMixer2MT6ReInitEv
	.cfi_endproc

	.globl	_ZThn8_N11NCoderMixer14CCoderMixer2MT6ReInitEv
	.p2align	4, 0x90
	.type	_ZThn8_N11NCoderMixer14CCoderMixer2MT6ReInitEv,@function
_ZThn8_N11NCoderMixer14CCoderMixer2MT6ReInitEv: # @_ZThn8_N11NCoderMixer14CCoderMixer2MT6ReInitEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 32
.Lcfi117:
	.cfi_offset %rbx, -24
.Lcfi118:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpl	$0, 156(%r14)
	jle	.LBB17_3
# BB#1:                                 # %.lr.ph.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB17_2:                               # =>This Inner Loop Header: Depth=1
	movq	160(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	_ZN13CStreamBinder6ReInitEv
	incq	%rbx
	movslq	156(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB17_2
.LBB17_3:                               # %_ZN11NCoderMixer14CCoderMixer2MT6ReInitEv.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end17:
	.size	_ZThn8_N11NCoderMixer14CCoderMixer2MT6ReInitEv, .Lfunc_end17-_ZThn8_N11NCoderMixer14CCoderMixer2MT6ReInitEv
	.cfi_endproc

	.globl	_ZN11NCoderMixer14CCoderMixer2MT4InitEPP19ISequentialInStreamPP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer14CCoderMixer2MT4InitEPP19ISequentialInStreamPP20ISequentialOutStream,@function
_ZN11NCoderMixer14CCoderMixer2MT4InitEPP19ISequentialInStreamPP20ISequentialOutStream: # @_ZN11NCoderMixer14CCoderMixer2MT4InitEPP19ISequentialInStreamPP20ISequentialOutStream
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi122:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi125:
	.cfi_def_cfa_offset 96
.Lcfi126:
	.cfi_offset %rbx, -56
.Lcfi127:
	.cfi_offset %r12, -48
.Lcfi128:
	.cfi_offset %r13, -40
.Lcfi129:
	.cfi_offset %r14, -32
.Lcfi130:
	.cfi_offset %r15, -24
.Lcfi131:
	.cfi_offset %rbp, -16
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	cmpl	$0, 204(%r13)
	movq	%r13, 8(%rsp)           # 8-byte Spill
	jle	.LBB18_13
# BB#1:                                 # %.lr.ph171
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB18_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_4 Depth 2
                                        #     Child Loop BB18_9 Depth 2
	movq	%r13, %rax
	movq	40(%rax), %r13
	movq	208(%rax), %rax
	movq	(%rax,%r14,8), %r15
	leaq	400(%r15), %rbp
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	cmpl	$0, (%r13,%r14,8)
	je	.LBB18_7
# BB#3:                                 # %.lr.ph163
                                        #   in Loop: Header=BB18_2 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB18_4:                               #   Parent Loop BB18_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp141:
	movl	$8, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp142:
# BB#5:                                 # %_ZN9CMyComPtrI19ISequentialInStreamEC2ERKS1_.exit.i
                                        #   in Loop: Header=BB18_4 Depth=2
	movq	$0, (%rbx)
.Ltmp143:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp144:
# BB#6:                                 # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
                                        #   in Loop: Header=BB18_4 Depth=2
	movq	416(%r15), %rax
	movslq	412(%r15), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 412(%r15)
	incl	%r12d
	cmpl	(%r13,%r14,8), %r12d
	jb	.LBB18_4
.LBB18_7:                               # %._crit_edge164
                                        #   in Loop: Header=BB18_2 Depth=1
	leaq	432(%r15), %r12
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	cmpl	$0, 4(%r13,%r14,8)
	je	.LBB18_12
# BB#8:                                 # %.lr.ph167
                                        #   in Loop: Header=BB18_2 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB18_9:                               #   Parent Loop BB18_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp146:
	movl	$8, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp147:
# BB#10:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2ERKS1_.exit.i
                                        #   in Loop: Header=BB18_9 Depth=2
	movq	$0, (%rbx)
.Ltmp148:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp149:
# BB#11:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
                                        #   in Loop: Header=BB18_9 Depth=2
	movq	448(%r15), %rax
	movslq	444(%r15), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 444(%r15)
	incl	%ebp
	cmpl	4(%r13,%r14,8), %ebp
	jb	.LBB18_9
.LBB18_12:                              # %._crit_edge168
                                        #   in Loop: Header=BB18_2 Depth=1
	incq	%r14
	movq	8(%rsp), %r13           # 8-byte Reload
	movslq	204(%r13), %rax
	cmpq	%rax, %r14
	jl	.LBB18_2
.LBB18_13:                              # %.preheader134
	cmpl	$0, 68(%r13)
	jle	.LBB18_21
# BB#14:                                # %.lr.ph160
	xorl	%r15d, %r15d
	movq	8(%rsp), %r12           # 8-byte Reload
	.p2align	4, 0x90
.LBB18_15:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_17 Depth 2
                                        #     Child Loop BB18_28 Depth 2
	movl	36(%r13), %eax
	testl	%eax, %eax
	je	.LBB18_19
# BB#16:                                # %.lr.ph.i68
                                        #   in Loop: Header=BB18_15 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	72(%rcx), %rdx
	movl	(%rdx,%r15,8), %ebp
	movq	40(%rcx), %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB18_17:                              #   Parent Loop BB18_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %r13
	movl	%ebp, %edi
	subl	(%rcx,%r13,8), %edi
	jb	.LBB18_27
# BB#18:                                #   in Loop: Header=BB18_17 Depth=2
	incl	%esi
	cmpl	%eax, %esi
	movl	%edi, %ebp
	jb	.LBB18_17
	jmp	.LBB18_19
	.p2align	4, 0x90
.LBB18_27:                              # %.lr.ph.i71
                                        #   in Loop: Header=BB18_15 Depth=1
	movl	4(%rdx,%r15,8), %r14d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB18_28:                              #   Parent Loop BB18_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rbx
	movl	%r14d, %esi
	subl	4(%rcx,%rbx,8), %esi
	jb	.LBB18_30
# BB#29:                                #   in Loop: Header=BB18_28 Depth=2
	incl	%edx
	cmpl	%eax, %edx
	movl	%esi, %r14d
	jb	.LBB18_28
	jmp	.LBB18_19
	.p2align	4, 0x90
.LBB18_30:                              #   in Loop: Header=BB18_15 Depth=1
	movq	168(%r12), %rax
	movq	208(%r12), %rcx
	movq	(%rax,%r15,8), %rdi
	movq	(%rcx,%r13,8), %rax
	movq	416(%rax), %rax
	movslq	%ebp, %rdx
	movq	(%rax,%rdx,8), %rsi
	movq	(%rcx,%rbx,8), %rax
	movq	448(%rax), %rax
	movslq	%r14d, %rcx
	movq	(%rax,%rcx,8), %rdx
	callq	_ZN13CStreamBinder13CreateStreamsEPP19ISequentialInStreamPP20ISequentialOutStream
	movq	$0, 16(%rsp)
	movq	$0, (%rsp)
	movq	208(%r12), %rax
	movq	(%rax,%r13,8), %rax
	movq	240(%rax), %rdi
	testq	%rdi, %rdi
	jne	.LBB18_32
# BB#31:                                #   in Loop: Header=BB18_15 Depth=1
	movq	248(%rax), %rdi
.LBB18_32:                              #   in Loop: Header=BB18_15 Depth=1
	movq	(%rdi), %rax
.Ltmp151:
	movl	$IID_ICompressSetBufSize, %esi
	leaq	16(%rsp), %rdx
	callq	*(%rax)
.Ltmp152:
	movq	8(%rsp), %r13           # 8-byte Reload
# BB#33:                                #   in Loop: Header=BB18_15 Depth=1
	movq	208(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movq	240(%rax), %rdi
	testq	%rdi, %rdi
	jne	.LBB18_35
# BB#34:                                #   in Loop: Header=BB18_15 Depth=1
	movq	248(%rax), %rdi
.LBB18_35:                              #   in Loop: Header=BB18_15 Depth=1
	movq	(%rdi), %rax
.Ltmp153:
	movl	$IID_ICompressSetBufSize, %esi
	movq	%rsp, %rdx
	callq	*(%rax)
.Ltmp154:
# BB#36:                                # %_ZNK11NCoderMixer11CCoderInfo214QueryInterfaceERK4GUIDPPv.exit85
                                        #   in Loop: Header=BB18_15 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_40
# BB#37:                                #   in Loop: Header=BB18_15 Depth=1
	cmpq	$0, (%rsp)
	je	.LBB18_42
# BB#38:                                #   in Loop: Header=BB18_15 Depth=1
	movq	(%rdi), %rax
.Ltmp156:
	movl	$524288, %edx           # imm = 0x80000
	movl	%ebp, %esi
	callq	*40(%rax)
.Ltmp157:
# BB#39:                                #   in Loop: Header=BB18_15 Depth=1
	movq	(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp158:
	movl	$524288, %edx           # imm = 0x80000
	movl	%r14d, %esi
	callq	*48(%rax)
.Ltmp159:
.LBB18_40:                              #   in Loop: Header=BB18_15 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_42
# BB#41:                                #   in Loop: Header=BB18_15 Depth=1
	movq	(%rdi), %rax
.Ltmp163:
	callq	*16(%rax)
.Ltmp164:
.LBB18_42:                              # %_ZN9CMyComPtrI19ICompressSetBufSizeED2Ev.exit87
                                        #   in Loop: Header=BB18_15 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_44
# BB#43:                                #   in Loop: Header=BB18_15 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB18_44:                              # %_ZN9CMyComPtrI19ICompressSetBufSizeED2Ev.exit83
                                        #   in Loop: Header=BB18_15 Depth=1
	incq	%r15
	movslq	68(%r13), %rax
	cmpq	%rax, %r15
	jl	.LBB18_15
.LBB18_21:                              # %.preheader132
	cmpl	$0, 100(%r13)
	movq	24(%rsp), %r12          # 8-byte Reload
	jle	.LBB18_51
# BB#22:                                # %.lr.ph158
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB18_23:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_25 Depth 2
	movl	36(%r13), %eax
	testl	%eax, %eax
	je	.LBB18_19
# BB#24:                                # %.lr.ph.i75
                                        #   in Loop: Header=BB18_23 Depth=1
	movq	40(%r13), %rcx
	movq	104(%r13), %rdx
	movl	(%rdx,%rbx,4), %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB18_25:                              #   Parent Loop BB18_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rbp
	movl	%edx, %edi
	subl	(%rcx,%rbp,8), %edi
	jb	.LBB18_57
# BB#26:                                #   in Loop: Header=BB18_25 Depth=2
	incl	%esi
	cmpl	%eax, %esi
	movl	%edi, %edx
	jb	.LBB18_25
	jmp	.LBB18_19
	.p2align	4, 0x90
.LBB18_57:                              # %_ZNK11NCoderMixer9CBindInfo12FindInStreamEjRjS1_.exit78
                                        #   in Loop: Header=BB18_23 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %r15
	movq	208(%r13), %rax
	movq	(%rax,%rbp,8), %rax
	movq	416(%rax), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %r14
	testq	%r15, %r15
	je	.LBB18_59
# BB#58:                                #   in Loop: Header=BB18_23 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*8(%rax)
.LBB18_59:                              #   in Loop: Header=BB18_23 Depth=1
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB18_61
# BB#60:                                #   in Loop: Header=BB18_23 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB18_61:                              # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
                                        #   in Loop: Header=BB18_23 Depth=1
	movq	%r15, (%r14)
	incq	%rbx
	movslq	100(%r13), %rax
	cmpq	%rax, %rbx
	jl	.LBB18_23
.LBB18_51:                              # %.preheader
	cmpl	$0, 132(%r13)
	jle	.LBB18_67
# BB#52:                                # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB18_53:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_55 Depth 2
	movl	36(%r13), %eax
	testl	%eax, %eax
	je	.LBB18_19
# BB#54:                                # %.lr.ph.i
                                        #   in Loop: Header=BB18_53 Depth=1
	movq	40(%r13), %rcx
	movq	136(%r13), %rdx
	movl	(%rdx,%rbx,4), %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB18_55:                              #   Parent Loop BB18_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rbp
	movl	%edx, %edi
	subl	4(%rcx,%rbp,8), %edi
	jb	.LBB18_62
# BB#56:                                #   in Loop: Header=BB18_55 Depth=2
	incl	%esi
	cmpl	%eax, %esi
	movl	%edi, %edx
	jb	.LBB18_55
	jmp	.LBB18_19
	.p2align	4, 0x90
.LBB18_62:                              # %_ZNK11NCoderMixer9CBindInfo13FindOutStreamEjRjS1_.exit
                                        #   in Loop: Header=BB18_53 Depth=1
	movq	(%r12,%rbx,8), %r15
	movq	208(%r13), %rax
	movq	(%rax,%rbp,8), %rax
	movq	448(%rax), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %r14
	testq	%r15, %r15
	je	.LBB18_64
# BB#63:                                #   in Loop: Header=BB18_53 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*8(%rax)
.LBB18_64:                              #   in Loop: Header=BB18_53 Depth=1
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB18_66
# BB#65:                                #   in Loop: Header=BB18_53 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB18_66:                              # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit
                                        #   in Loop: Header=BB18_53 Depth=1
	movq	%r15, (%r14)
	incq	%rbx
	movslq	132(%r13), %rax
	cmpq	%rax, %rbx
	jl	.LBB18_53
.LBB18_67:                              # %._crit_edge
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_19:                              # %._crit_edge.i70
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.LBB18_45:
.Ltmp165:
	movq	%rax, %rbx
	jmp	.LBB18_49
.LBB18_72:
.Ltmp160:
	jmp	.LBB18_47
.LBB18_46:
.Ltmp155:
.LBB18_47:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_49
# BB#48:
	movq	(%rdi), %rax
.Ltmp161:
	callq	*16(%rax)
.Ltmp162:
.LBB18_49:                              # %_ZN9CMyComPtrI19ICompressSetBufSizeED2Ev.exit82
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_70
# BB#50:
	movq	(%rdi), %rax
.Ltmp166:
	callq	*16(%rax)
.Ltmp167:
	jmp	.LBB18_70
.LBB18_71:
.Ltmp168:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_20:
.Ltmp150:
	jmp	.LBB18_69
.LBB18_68:
.Ltmp145:
.LBB18_69:
	movq	%rax, %rbx
.LBB18_70:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end18:
	.size	_ZN11NCoderMixer14CCoderMixer2MT4InitEPP19ISequentialInStreamPP20ISequentialOutStream, .Lfunc_end18-_ZN11NCoderMixer14CCoderMixer2MT4InitEPP19ISequentialInStreamPP20ISequentialOutStream
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin7-.Lfunc_begin7 # >> Call Site 1 <<
	.long	.Ltmp141-.Lfunc_begin7  #   Call between .Lfunc_begin7 and .Ltmp141
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp141-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp144-.Ltmp141       #   Call between .Ltmp141 and .Ltmp144
	.long	.Ltmp145-.Lfunc_begin7  #     jumps to .Ltmp145
	.byte	0                       #   On action: cleanup
	.long	.Ltmp144-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp146-.Ltmp144       #   Call between .Ltmp144 and .Ltmp146
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp146-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp149-.Ltmp146       #   Call between .Ltmp146 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin7  #     jumps to .Ltmp150
	.byte	0                       #   On action: cleanup
	.long	.Ltmp149-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp151-.Ltmp149       #   Call between .Ltmp149 and .Ltmp151
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp154-.Ltmp151       #   Call between .Ltmp151 and .Ltmp154
	.long	.Ltmp155-.Lfunc_begin7  #     jumps to .Ltmp155
	.byte	0                       #   On action: cleanup
	.long	.Ltmp156-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp159-.Ltmp156       #   Call between .Ltmp156 and .Ltmp159
	.long	.Ltmp160-.Lfunc_begin7  #     jumps to .Ltmp160
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp164-.Ltmp163       #   Call between .Ltmp163 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin7  #     jumps to .Ltmp165
	.byte	0                       #   On action: cleanup
	.long	.Ltmp164-.Lfunc_begin7  # >> Call Site 9 <<
	.long	.Ltmp161-.Ltmp164       #   Call between .Ltmp164 and .Ltmp161
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp161-.Lfunc_begin7  # >> Call Site 10 <<
	.long	.Ltmp167-.Ltmp161       #   Call between .Ltmp161 and .Ltmp167
	.long	.Ltmp168-.Lfunc_begin7  #     jumps to .Ltmp168
	.byte	1                       #   On action: 1
	.long	.Ltmp167-.Lfunc_begin7  # >> Call Site 11 <<
	.long	.Lfunc_end18-.Ltmp167   #   Call between .Ltmp167 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN11NCoderMixer14CCoderMixer2MT13ReturnIfErrorEi
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer14CCoderMixer2MT13ReturnIfErrorEi,@function
_ZN11NCoderMixer14CCoderMixer2MT13ReturnIfErrorEi: # @_ZN11NCoderMixer14CCoderMixer2MT13ReturnIfErrorEi
	.cfi_startproc
# BB#0:
	movslq	204(%rdi), %rax
	testq	%rax, %rax
	jle	.LBB19_1
# BB#4:                                 # %.lr.ph
	movq	208(%rdi), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB19_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rdi
	cmpl	%esi, 392(%rdi)
	je	.LBB19_6
# BB#2:                                 #   in Loop: Header=BB19_5 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB19_5
# BB#3:
	xorl	%esi, %esi
	movl	%esi, %eax
	retq
.LBB19_1:
	xorl	%esi, %esi
	movl	%esi, %eax
	retq
.LBB19_6:                               # %._crit_edge
	movl	%esi, %eax
	retq
.Lfunc_end19:
	.size	_ZN11NCoderMixer14CCoderMixer2MT13ReturnIfErrorEi, .Lfunc_end19-_ZN11NCoderMixer14CCoderMixer2MT13ReturnIfErrorEi
	.cfi_endproc

	.globl	_ZN11NCoderMixer14CCoderMixer2MT4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS6_jP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer14CCoderMixer2MT4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS6_jP21ICompressProgressInfo,@function
_ZN11NCoderMixer14CCoderMixer2MT4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS6_jP21ICompressProgressInfo: # @_ZN11NCoderMixer14CCoderMixer2MT4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS6_jP21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi134:
	.cfi_def_cfa_offset 32
.Lcfi135:
	.cfi_offset %rbx, -24
.Lcfi136:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	%ecx, 100(%r14)
	jne	.LBB20_43
# BB#1:
	movl	32(%rsp), %ecx
	cmpl	%ecx, 132(%r14)
	jne	.LBB20_43
# BB#2:
	movq	%r14, %rdi
	movq	%r8, %rdx
	callq	_ZN11NCoderMixer14CCoderMixer2MT4InitEPP19ISequentialInStreamPP20ISequentialOutStream
	movl	204(%r14), %eax
	testl	%eax, %eax
	jle	.LBB20_13
# BB#3:                                 # %.lr.ph132
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_4:                               # =>This Inner Loop Header: Depth=1
	movl	184(%r14), %ecx
	cmpq	%rcx, %rbx
	je	.LBB20_7
# BB#5:                                 #   in Loop: Header=BB20_4 Depth=1
	movq	208(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	_ZN11CVirtThread6CreateEv
	testl	%eax, %eax
	jne	.LBB20_43
# BB#6:                                 # %._crit_edge150
                                        #   in Loop: Header=BB20_4 Depth=1
	movl	204(%r14), %eax
.LBB20_7:                               #   in Loop: Header=BB20_4 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB20_4
# BB#8:                                 # %.preheader
	testl	%eax, %eax
	movl	184(%r14), %ecx
	jle	.LBB20_14
# BB#9:                                 # %.lr.ph125
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_10:                              # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	cmpq	%rdx, %rbx
	je	.LBB20_12
# BB#11:                                #   in Loop: Header=BB20_10 Depth=1
	movq	208(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	_ZN11CVirtThread5StartEv
	movl	184(%r14), %ecx
	movl	204(%r14), %eax
.LBB20_12:                              #   in Loop: Header=BB20_10 Depth=1
	incq	%rbx
	movslq	%eax, %rdx
	cmpq	%rdx, %rbx
	jl	.LBB20_10
	jmp	.LBB20_14
.LBB20_13:                              # %.preheader.thread
	movl	184(%r14), %ecx
.LBB20_14:                              # %._crit_edge126
	movq	40(%rsp), %rsi
	movq	208(%r14), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rdi
	callq	_ZN11NCoderMixer7CCoder24CodeEP21ICompressProgressInfo
	movl	204(%r14), %eax
	testl	%eax, %eax
	jle	.LBB20_38
# BB#15:                                # %.lr.ph123.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_16:                              # %.lr.ph123
                                        # =>This Inner Loop Header: Depth=1
	movl	184(%r14), %ecx
	cmpq	%rcx, %rbx
	je	.LBB20_18
# BB#17:                                #   in Loop: Header=BB20_16 Depth=1
	movq	208(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	addq	$112, %rdi
	callq	Event_Wait
	movl	204(%r14), %eax
.LBB20_18:                              #   in Loop: Header=BB20_16 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB20_16
# BB#19:                                # %._crit_edge
	testl	%eax, %eax
	jle	.LBB20_38
# BB#20:                                # %.lr.ph.i85
	movq	208(%r14), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB20_21:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rdi
	cmpl	$-2147467260, 392(%rdi) # imm = 0x80004004
	je	.LBB20_39
# BB#22:                                #   in Loop: Header=BB20_21 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB20_21
# BB#23:                                # %.lr.ph.i81.preheader
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB20_24:                              # %.lr.ph.i81
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rdi
	cmpl	$-2147024882, 392(%rdi) # imm = 0x8007000E
	je	.LBB20_40
# BB#25:                                #   in Loop: Header=BB20_24 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB20_24
# BB#26:                                # %_ZN11NCoderMixer14CCoderMixer2MT13ReturnIfErrorEi.exit84.thread.preheader
	testl	%eax, %eax
	jle	.LBB20_41
# BB#27:                                # %.lr.ph121
	movq	208(%r14), %rdx
	xorl	%esi, %esi
.LBB20_28:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rax
	movl	392(%rax), %eax
	cmpl	$2, %eax
	jb	.LBB20_30
# BB#29:                                #   in Loop: Header=BB20_28 Depth=1
	cmpl	$-2147467259, %eax      # imm = 0x80004005
	jne	.LBB20_43
.LBB20_30:                              # %_ZN11NCoderMixer14CCoderMixer2MT13ReturnIfErrorEi.exit84.thread
                                        #   in Loop: Header=BB20_28 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB20_28
# BB#31:                                # %.lr.ph.i
	movq	208(%r14), %rax
	xorl	%edx, %edx
.LBB20_32:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rdx,8), %rsi
	cmpl	$1, 392(%rsi)
	je	.LBB20_42
# BB#33:                                #   in Loop: Header=BB20_32 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB20_32
# BB#34:                                # %.lr.ph
	movq	208(%r14), %rdx
	xorl	%esi, %esi
.LBB20_35:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rax
	movl	392(%rax), %eax
	testl	%eax, %eax
	jne	.LBB20_43
# BB#36:                                # %_ZN11NCoderMixer14CCoderMixer2MT13ReturnIfErrorEi.exit.thread
                                        #   in Loop: Header=BB20_35 Depth=1
	incq	%rsi
	xorl	%eax, %eax
	cmpq	%rcx, %rsi
	jl	.LBB20_35
	jmp	.LBB20_43
.LBB20_38:
	xorl	%eax, %eax
	jmp	.LBB20_43
.LBB20_39:
	movl	$-2147467260, %eax      # imm = 0x80004004
	jmp	.LBB20_43
.LBB20_40:
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB20_43
.LBB20_41:
	xorl	%eax, %eax
	jmp	.LBB20_43
.LBB20_42:
	movl	$1, %eax
.LBB20_43:                              # %_ZN11NCoderMixer14CCoderMixer2MT13ReturnIfErrorEi.exit88.thread92
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end20:
	.size	_ZN11NCoderMixer14CCoderMixer2MT4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS6_jP21ICompressProgressInfo, .Lfunc_end20-_ZN11NCoderMixer14CCoderMixer2MT4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS6_jP21ICompressProgressInfo
	.cfi_endproc

	.section	.text._ZN11NCoderMixer14CCoderMixer2MT14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN11NCoderMixer14CCoderMixer2MT14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN11NCoderMixer14CCoderMixer2MT14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer14CCoderMixer2MT14QueryInterfaceERK4GUIDPPv,@function
_ZN11NCoderMixer14CCoderMixer2MT14QueryInterfaceERK4GUIDPPv: # @_ZN11NCoderMixer14CCoderMixer2MT14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi137:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB21_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB21_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB21_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB21_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB21_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB21_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB21_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB21_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB21_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB21_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB21_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB21_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB21_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB21_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB21_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB21_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB21_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end21:
	.size	_ZN11NCoderMixer14CCoderMixer2MT14QueryInterfaceERK4GUIDPPv, .Lfunc_end21-_ZN11NCoderMixer14CCoderMixer2MT14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN11NCoderMixer14CCoderMixer2MT6AddRefEv,"axG",@progbits,_ZN11NCoderMixer14CCoderMixer2MT6AddRefEv,comdat
	.weak	_ZN11NCoderMixer14CCoderMixer2MT6AddRefEv
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer14CCoderMixer2MT6AddRefEv,@function
_ZN11NCoderMixer14CCoderMixer2MT6AddRefEv: # @_ZN11NCoderMixer14CCoderMixer2MT6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end22:
	.size	_ZN11NCoderMixer14CCoderMixer2MT6AddRefEv, .Lfunc_end22-_ZN11NCoderMixer14CCoderMixer2MT6AddRefEv
	.cfi_endproc

	.section	.text._ZN11NCoderMixer14CCoderMixer2MT7ReleaseEv,"axG",@progbits,_ZN11NCoderMixer14CCoderMixer2MT7ReleaseEv,comdat
	.weak	_ZN11NCoderMixer14CCoderMixer2MT7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer14CCoderMixer2MT7ReleaseEv,@function
_ZN11NCoderMixer14CCoderMixer2MT7ReleaseEv: # @_ZN11NCoderMixer14CCoderMixer2MT7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi138:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB23_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB23_2:
	popq	%rcx
	retq
.Lfunc_end23:
	.size	_ZN11NCoderMixer14CCoderMixer2MT7ReleaseEv, .Lfunc_end23-_ZN11NCoderMixer14CCoderMixer2MT7ReleaseEv
	.cfi_endproc

	.section	.text._ZN11NCoderMixer14CCoderMixer2MTD2Ev,"axG",@progbits,_ZN11NCoderMixer14CCoderMixer2MTD2Ev,comdat
	.weak	_ZN11NCoderMixer14CCoderMixer2MTD2Ev
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer14CCoderMixer2MTD2Ev,@function
_ZN11NCoderMixer14CCoderMixer2MTD2Ev:   # @_ZN11NCoderMixer14CCoderMixer2MTD2Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r15
.Lcfi139:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi140:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi141:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi143:
	.cfi_def_cfa_offset 48
.Lcfi144:
	.cfi_offset %rbx, -40
.Lcfi145:
	.cfi_offset %r12, -32
.Lcfi146:
	.cfi_offset %r14, -24
.Lcfi147:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movl	$_ZTVN11NCoderMixer14CCoderMixer2MTE+104, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN11NCoderMixer14CCoderMixer2MTE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r12)
	leaq	192(%r12), %rbx
	movq	$_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE+16, 192(%r12)
.Ltmp169:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp170:
# BB#1:
.Ltmp175:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp176:
# BB#2:                                 # %_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev.exit
	leaq	152(%r12), %rbx
	movq	$_ZTV13CObjectVectorI13CStreamBinderE+16, 152(%r12)
.Ltmp186:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp187:
# BB#3:
.Ltmp192:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp193:
# BB#4:                                 # %_ZN13CObjectVectorI13CStreamBinderED2Ev.exit
	leaq	24(%r12), %r15
	leaq	120(%r12), %rdi
.Ltmp214:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp215:
# BB#5:
	leaq	88(%r12), %rdi
.Ltmp219:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp220:
# BB#6:
	addq	$56, %r12
.Ltmp224:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp225:
# BB#7:
.Ltmp230:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp231:
# BB#8:                                 # %_ZN11NCoderMixer9CBindInfoD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB24_30:
.Ltmp232:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB24_14:
.Ltmp226:
	movq	%rax, %r14
	jmp	.LBB24_17
.LBB24_15:
.Ltmp221:
	movq	%rax, %r14
	jmp	.LBB24_16
.LBB24_13:
.Ltmp216:
	movq	%rax, %r14
	leaq	88(%r12), %rdi
.Ltmp217:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp218:
.LBB24_16:
	addq	$56, %r12
.Ltmp222:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp223:
.LBB24_17:
.Ltmp227:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp228:
	jmp	.LBB24_26
.LBB24_18:
.Ltmp229:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB24_29:
.Ltmp194:
	movq	%rax, %r14
	jmp	.LBB24_22
.LBB24_11:
.Ltmp188:
	movq	%rax, %r14
.Ltmp189:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp190:
	jmp	.LBB24_22
.LBB24_12:
.Ltmp191:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB24_19:
.Ltmp177:
	movq	%rax, %r14
	jmp	.LBB24_20
.LBB24_9:
.Ltmp171:
	movq	%rax, %r14
.Ltmp172:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp173:
.LBB24_20:                              # %.body
	leaq	152(%r12), %rbx
	movq	$_ZTV13CObjectVectorI13CStreamBinderE+16, 152(%r12)
.Ltmp178:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp179:
# BB#21:
.Ltmp184:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp185:
.LBB24_22:                              # %_ZN13CObjectVectorI13CStreamBinderED2Ev.exit10
	leaq	24(%r12), %r15
	leaq	120(%r12), %rdi
.Ltmp195:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp196:
# BB#23:
	leaq	88(%r12), %rdi
.Ltmp200:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp201:
# BB#24:
	addq	$56, %r12
.Ltmp205:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp206:
# BB#25:
.Ltmp211:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp212:
.LBB24_26:                              # %_ZN11NCoderMixer9CBindInfoD2Ev.exit17
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB24_10:
.Ltmp174:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB24_27:
.Ltmp180:
	movq	%rax, %r14
.Ltmp181:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp182:
	jmp	.LBB24_38
.LBB24_28:
.Ltmp183:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB24_32:
.Ltmp207:
	movq	%rax, %r14
	jmp	.LBB24_35
.LBB24_33:
.Ltmp202:
	movq	%rax, %r14
	jmp	.LBB24_34
.LBB24_31:
.Ltmp197:
	movq	%rax, %r14
	leaq	88(%r12), %rdi
.Ltmp198:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp199:
.LBB24_34:
	addq	$56, %r12
.Ltmp203:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp204:
.LBB24_35:
.Ltmp208:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp209:
	jmp	.LBB24_38
.LBB24_36:
.Ltmp210:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB24_37:
.Ltmp213:
	movq	%rax, %r14
.LBB24_38:                              # %.body8
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end24:
	.size	_ZN11NCoderMixer14CCoderMixer2MTD2Ev, .Lfunc_end24-_ZN11NCoderMixer14CCoderMixer2MTD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\232\202\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\221\002"              # Call site table length
	.long	.Ltmp169-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp170-.Ltmp169       #   Call between .Ltmp169 and .Ltmp170
	.long	.Ltmp171-.Lfunc_begin8  #     jumps to .Ltmp171
	.byte	0                       #   On action: cleanup
	.long	.Ltmp175-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp176-.Ltmp175       #   Call between .Ltmp175 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin8  #     jumps to .Ltmp177
	.byte	0                       #   On action: cleanup
	.long	.Ltmp186-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp187-.Ltmp186       #   Call between .Ltmp186 and .Ltmp187
	.long	.Ltmp188-.Lfunc_begin8  #     jumps to .Ltmp188
	.byte	0                       #   On action: cleanup
	.long	.Ltmp192-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp193-.Ltmp192       #   Call between .Ltmp192 and .Ltmp193
	.long	.Ltmp194-.Lfunc_begin8  #     jumps to .Ltmp194
	.byte	0                       #   On action: cleanup
	.long	.Ltmp214-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp215-.Ltmp214       #   Call between .Ltmp214 and .Ltmp215
	.long	.Ltmp216-.Lfunc_begin8  #     jumps to .Ltmp216
	.byte	0                       #   On action: cleanup
	.long	.Ltmp219-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp220-.Ltmp219       #   Call between .Ltmp219 and .Ltmp220
	.long	.Ltmp221-.Lfunc_begin8  #     jumps to .Ltmp221
	.byte	0                       #   On action: cleanup
	.long	.Ltmp224-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Ltmp225-.Ltmp224       #   Call between .Ltmp224 and .Ltmp225
	.long	.Ltmp226-.Lfunc_begin8  #     jumps to .Ltmp226
	.byte	0                       #   On action: cleanup
	.long	.Ltmp230-.Lfunc_begin8  # >> Call Site 8 <<
	.long	.Ltmp231-.Ltmp230       #   Call between .Ltmp230 and .Ltmp231
	.long	.Ltmp232-.Lfunc_begin8  #     jumps to .Ltmp232
	.byte	0                       #   On action: cleanup
	.long	.Ltmp231-.Lfunc_begin8  # >> Call Site 9 <<
	.long	.Ltmp217-.Ltmp231       #   Call between .Ltmp231 and .Ltmp217
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp217-.Lfunc_begin8  # >> Call Site 10 <<
	.long	.Ltmp228-.Ltmp217       #   Call between .Ltmp217 and .Ltmp228
	.long	.Ltmp229-.Lfunc_begin8  #     jumps to .Ltmp229
	.byte	1                       #   On action: 1
	.long	.Ltmp189-.Lfunc_begin8  # >> Call Site 11 <<
	.long	.Ltmp190-.Ltmp189       #   Call between .Ltmp189 and .Ltmp190
	.long	.Ltmp191-.Lfunc_begin8  #     jumps to .Ltmp191
	.byte	1                       #   On action: 1
	.long	.Ltmp172-.Lfunc_begin8  # >> Call Site 12 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	.Ltmp174-.Lfunc_begin8  #     jumps to .Ltmp174
	.byte	1                       #   On action: 1
	.long	.Ltmp178-.Lfunc_begin8  # >> Call Site 13 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp180-.Lfunc_begin8  #     jumps to .Ltmp180
	.byte	1                       #   On action: 1
	.long	.Ltmp184-.Lfunc_begin8  # >> Call Site 14 <<
	.long	.Ltmp185-.Ltmp184       #   Call between .Ltmp184 and .Ltmp185
	.long	.Ltmp213-.Lfunc_begin8  #     jumps to .Ltmp213
	.byte	1                       #   On action: 1
	.long	.Ltmp195-.Lfunc_begin8  # >> Call Site 15 <<
	.long	.Ltmp196-.Ltmp195       #   Call between .Ltmp195 and .Ltmp196
	.long	.Ltmp197-.Lfunc_begin8  #     jumps to .Ltmp197
	.byte	1                       #   On action: 1
	.long	.Ltmp200-.Lfunc_begin8  # >> Call Site 16 <<
	.long	.Ltmp201-.Ltmp200       #   Call between .Ltmp200 and .Ltmp201
	.long	.Ltmp202-.Lfunc_begin8  #     jumps to .Ltmp202
	.byte	1                       #   On action: 1
	.long	.Ltmp205-.Lfunc_begin8  # >> Call Site 17 <<
	.long	.Ltmp206-.Ltmp205       #   Call between .Ltmp205 and .Ltmp206
	.long	.Ltmp207-.Lfunc_begin8  #     jumps to .Ltmp207
	.byte	1                       #   On action: 1
	.long	.Ltmp211-.Lfunc_begin8  # >> Call Site 18 <<
	.long	.Ltmp212-.Ltmp211       #   Call between .Ltmp211 and .Ltmp212
	.long	.Ltmp213-.Lfunc_begin8  #     jumps to .Ltmp213
	.byte	1                       #   On action: 1
	.long	.Ltmp212-.Lfunc_begin8  # >> Call Site 19 <<
	.long	.Ltmp181-.Ltmp212       #   Call between .Ltmp212 and .Ltmp181
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp181-.Lfunc_begin8  # >> Call Site 20 <<
	.long	.Ltmp182-.Ltmp181       #   Call between .Ltmp181 and .Ltmp182
	.long	.Ltmp183-.Lfunc_begin8  #     jumps to .Ltmp183
	.byte	1                       #   On action: 1
	.long	.Ltmp198-.Lfunc_begin8  # >> Call Site 21 <<
	.long	.Ltmp209-.Ltmp198       #   Call between .Ltmp198 and .Ltmp209
	.long	.Ltmp210-.Lfunc_begin8  #     jumps to .Ltmp210
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11NCoderMixer14CCoderMixer2MTD0Ev,"axG",@progbits,_ZN11NCoderMixer14CCoderMixer2MTD0Ev,comdat
	.weak	_ZN11NCoderMixer14CCoderMixer2MTD0Ev
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer14CCoderMixer2MTD0Ev,@function
_ZN11NCoderMixer14CCoderMixer2MTD0Ev:   # @_ZN11NCoderMixer14CCoderMixer2MTD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi148:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi149:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi150:
	.cfi_def_cfa_offset 32
.Lcfi151:
	.cfi_offset %rbx, -24
.Lcfi152:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp233:
	callq	_ZN11NCoderMixer14CCoderMixer2MTD2Ev
.Ltmp234:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB25_2:
.Ltmp235:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end25:
	.size	_ZN11NCoderMixer14CCoderMixer2MTD0Ev, .Lfunc_end25-_ZN11NCoderMixer14CCoderMixer2MTD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp233-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp234-.Ltmp233       #   Call between .Ltmp233 and .Ltmp234
	.long	.Ltmp235-.Lfunc_begin9  #     jumps to .Ltmp235
	.byte	0                       #   On action: cleanup
	.long	.Ltmp234-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end25-.Ltmp234   #   Call between .Ltmp234 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_,"axG",@progbits,_ZN11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_,comdat
	.weak	_ZN11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_,@function
_ZN11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_: # @_ZN11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi153:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi154:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi155:
	.cfi_def_cfa_offset 32
.Lcfi156:
	.cfi_offset %rbx, -24
.Lcfi157:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	208(%rdi), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rbx
	leaq	264(%rbx), %rsi
	leaq	328(%rbx), %rax
	movl	256(%rbx), %ecx
	movq	%rdx, %rdi
	movq	%rax, %rdx
	callq	_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej
	movl	260(%rbx), %ecx
	leaq	296(%rbx), %rsi
	leaq	360(%rbx), %rdx
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej # TAILCALL
.Lfunc_end26:
	.size	_ZN11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_, .Lfunc_end26-_ZN11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_
	.cfi_endproc

	.section	.text._ZThn8_N11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_,"axG",@progbits,_ZThn8_N11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_,comdat
	.weak	_ZThn8_N11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_
	.p2align	4, 0x90
	.type	_ZThn8_N11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_,@function
_ZThn8_N11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_: # @_ZThn8_N11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi158:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi159:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi160:
	.cfi_def_cfa_offset 32
.Lcfi161:
	.cfi_offset %rbx, -24
.Lcfi162:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	200(%rdi), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rbx
	leaq	264(%rbx), %rsi
	leaq	328(%rbx), %rax
	movl	256(%rbx), %ecx
	movq	%rdx, %rdi
	movq	%rax, %rdx
	callq	_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej
	movl	260(%rbx), %ecx
	leaq	296(%rbx), %rsi
	leaq	360(%rbx), %rdx
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej # TAILCALL
.Lfunc_end27:
	.size	_ZThn8_N11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_, .Lfunc_end27-_ZThn8_N11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_
	.cfi_endproc

	.section	.text._ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,"axG",@progbits,_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,comdat
	.weak	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.p2align	4, 0x90
	.type	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,@function
_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv: # @_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.cfi_startproc
# BB#0:
	cmpb	$0, 17(%rdi)
	je	.LBB28_1
# BB#2:
	movb	$1, %al
	cmpb	$0, 16(%rdi)
	jne	.LBB28_4
# BB#3:
	movb	$0, 17(%rdi)
.LBB28_4:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB28_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end28:
	.size	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv, .Lfunc_end28-_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev,"axG",@progbits,_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev,comdat
	.weak	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev,@function
_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev: # @_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi163:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi164:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi165:
	.cfi_def_cfa_offset 32
.Lcfi166:
	.cfi_offset %rbx, -24
.Lcfi167:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE+16, (%rbx)
.Ltmp236:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp237:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB29_2:
.Ltmp238:
	movq	%rax, %r14
.Ltmp239:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp240:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB29_4:
.Ltmp241:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end29:
	.size	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev, .Lfunc_end29-_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp236-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp237-.Ltmp236       #   Call between .Ltmp236 and .Ltmp237
	.long	.Ltmp238-.Lfunc_begin10 #     jumps to .Ltmp238
	.byte	0                       #   On action: cleanup
	.long	.Ltmp237-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp239-.Ltmp237       #   Call between .Ltmp237 and .Ltmp239
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp239-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp240-.Ltmp239       #   Call between .Ltmp239 and .Ltmp240
	.long	.Ltmp241-.Lfunc_begin10 #     jumps to .Ltmp241
	.byte	1                       #   On action: 1
	.long	.Ltmp240-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Lfunc_end29-.Ltmp240   #   Call between .Ltmp240 and .Lfunc_end29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI13CStreamBinderED2Ev,"axG",@progbits,_ZN13CObjectVectorI13CStreamBinderED2Ev,comdat
	.weak	_ZN13CObjectVectorI13CStreamBinderED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI13CStreamBinderED2Ev,@function
_ZN13CObjectVectorI13CStreamBinderED2Ev: # @_ZN13CObjectVectorI13CStreamBinderED2Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi168:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi169:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi170:
	.cfi_def_cfa_offset 32
.Lcfi171:
	.cfi_offset %rbx, -24
.Lcfi172:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI13CStreamBinderE+16, (%rbx)
.Ltmp242:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp243:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB30_2:
.Ltmp244:
	movq	%rax, %r14
.Ltmp245:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp246:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB30_4:
.Ltmp247:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end30:
	.size	_ZN13CObjectVectorI13CStreamBinderED2Ev, .Lfunc_end30-_ZN13CObjectVectorI13CStreamBinderED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp242-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp243-.Ltmp242       #   Call between .Ltmp242 and .Ltmp243
	.long	.Ltmp244-.Lfunc_begin11 #     jumps to .Ltmp244
	.byte	0                       #   On action: cleanup
	.long	.Ltmp243-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp245-.Ltmp243       #   Call between .Ltmp243 and .Ltmp245
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp245-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp246-.Ltmp245       #   Call between .Ltmp245 and .Ltmp246
	.long	.Ltmp247-.Lfunc_begin11 #     jumps to .Ltmp247
	.byte	1                       #   On action: 1
	.long	.Ltmp246-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Lfunc_end30-.Ltmp246   #   Call between .Ltmp246 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev,"axG",@progbits,_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev,comdat
	.weak	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev,@function
_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev: # @_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi173:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi174:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi175:
	.cfi_def_cfa_offset 32
.Lcfi176:
	.cfi_offset %rbx, -24
.Lcfi177:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE+16, (%rbx)
.Ltmp248:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp249:
# BB#1:
.Ltmp254:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp255:
# BB#2:                                 # %_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB31_5:
.Ltmp256:
	movq	%rax, %r14
	jmp	.LBB31_6
.LBB31_3:
.Ltmp250:
	movq	%rax, %r14
.Ltmp251:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp252:
.LBB31_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB31_4:
.Ltmp253:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end31:
	.size	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev, .Lfunc_end31-_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table31:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp248-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp249-.Ltmp248       #   Call between .Ltmp248 and .Ltmp249
	.long	.Ltmp250-.Lfunc_begin12 #     jumps to .Ltmp250
	.byte	0                       #   On action: cleanup
	.long	.Ltmp254-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp255-.Ltmp254       #   Call between .Ltmp254 and .Ltmp255
	.long	.Ltmp256-.Lfunc_begin12 #     jumps to .Ltmp256
	.byte	0                       #   On action: cleanup
	.long	.Ltmp251-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp252-.Ltmp251       #   Call between .Ltmp251 and .Ltmp252
	.long	.Ltmp253-.Lfunc_begin12 #     jumps to .Ltmp253
	.byte	1                       #   On action: 1
	.long	.Ltmp252-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Lfunc_end31-.Ltmp252   #   Call between .Ltmp252 and .Lfunc_end31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii,@function
_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii: # @_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi178:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi179:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi180:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi181:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi182:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi183:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi184:
	.cfi_def_cfa_offset 64
.Lcfi185:
	.cfi_offset %rbx, -56
.Lcfi186:
	.cfi_offset %r12, -48
.Lcfi187:
	.cfi_offset %r13, -40
.Lcfi188:
	.cfi_offset %r14, -32
.Lcfi189:
	.cfi_offset %r15, -24
.Lcfi190:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB32_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB32_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB32_5
# BB#3:                                 #   in Loop: Header=BB32_2 Depth=1
.Ltmp257:
	movq	%rbp, %rdi
	callq	_ZN11NCoderMixer7CCoder2D2Ev
.Ltmp258:
# BB#4:                                 #   in Loop: Header=BB32_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB32_5:                               #   in Loop: Header=BB32_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB32_2
.LBB32_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB32_7:
.Ltmp259:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end32:
	.size	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii, .Lfunc_end32-_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp257-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp258-.Ltmp257       #   Call between .Ltmp257 and .Ltmp258
	.long	.Ltmp259-.Lfunc_begin13 #     jumps to .Ltmp259
	.byte	0                       #   On action: cleanup
	.long	.Ltmp258-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Lfunc_end32-.Ltmp258   #   Call between .Ltmp258 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI13CStreamBinderED0Ev,"axG",@progbits,_ZN13CObjectVectorI13CStreamBinderED0Ev,comdat
	.weak	_ZN13CObjectVectorI13CStreamBinderED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI13CStreamBinderED0Ev,@function
_ZN13CObjectVectorI13CStreamBinderED0Ev: # @_ZN13CObjectVectorI13CStreamBinderED0Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi191:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi192:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi193:
	.cfi_def_cfa_offset 32
.Lcfi194:
	.cfi_offset %rbx, -24
.Lcfi195:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI13CStreamBinderE+16, (%rbx)
.Ltmp260:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp261:
# BB#1:
.Ltmp266:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp267:
# BB#2:                                 # %_ZN13CObjectVectorI13CStreamBinderED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB33_5:
.Ltmp268:
	movq	%rax, %r14
	jmp	.LBB33_6
.LBB33_3:
.Ltmp262:
	movq	%rax, %r14
.Ltmp263:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp264:
.LBB33_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB33_4:
.Ltmp265:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end33:
	.size	_ZN13CObjectVectorI13CStreamBinderED0Ev, .Lfunc_end33-_ZN13CObjectVectorI13CStreamBinderED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp260-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp261-.Ltmp260       #   Call between .Ltmp260 and .Ltmp261
	.long	.Ltmp262-.Lfunc_begin14 #     jumps to .Ltmp262
	.byte	0                       #   On action: cleanup
	.long	.Ltmp266-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp267-.Ltmp266       #   Call between .Ltmp266 and .Ltmp267
	.long	.Ltmp268-.Lfunc_begin14 #     jumps to .Ltmp268
	.byte	0                       #   On action: cleanup
	.long	.Ltmp263-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp264-.Ltmp263       #   Call between .Ltmp263 and .Ltmp264
	.long	.Ltmp265-.Lfunc_begin14 #     jumps to .Ltmp265
	.byte	1                       #   On action: 1
	.long	.Ltmp264-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end33-.Ltmp264   #   Call between .Ltmp264 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI13CStreamBinderE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI13CStreamBinderE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI13CStreamBinderE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI13CStreamBinderE6DeleteEii,@function
_ZN13CObjectVectorI13CStreamBinderE6DeleteEii: # @_ZN13CObjectVectorI13CStreamBinderE6DeleteEii
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi196:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi197:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi198:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi199:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi200:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi201:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi202:
	.cfi_def_cfa_offset 80
.Lcfi203:
	.cfi_offset %rbx, -56
.Lcfi204:
	.cfi_offset %r12, -48
.Lcfi205:
	.cfi_offset %r13, -40
.Lcfi206:
	.cfi_offset %r14, -32
.Lcfi207:
	.cfi_offset %r15, -24
.Lcfi208:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %edi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	subl	%esi, %edi
	cmpl	%ecx, %eax
	cmovlel	%edx, %edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	testl	%edi, %edi
	jle	.LBB34_10
# BB#1:                                 # %.lr.ph
	movslq	16(%rsp), %r13          # 4-byte Folded Reload
	movslq	12(%rsp), %r14          # 4-byte Folded Reload
	shlq	$3, %r13
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB34_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%r13, %rax
	movq	(%rax,%r15,8), %rbp
	testq	%rbp, %rbp
	je	.LBB34_9
# BB#3:                                 #   in Loop: Header=BB34_2 Depth=1
	movq	152(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB34_7
# BB#4:                                 #   in Loop: Header=BB34_2 Depth=1
	cmpb	$0, 88(%rbx)
	je	.LBB34_6
# BB#5:                                 #   in Loop: Header=BB34_2 Depth=1
	movq	%rbx, %rdi
	callq	pthread_mutex_destroy
	leaq	40(%rbx), %rdi
	callq	pthread_cond_destroy
.LBB34_6:                               #   in Loop: Header=BB34_2 Depth=1
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB34_7:                               #   in Loop: Header=BB34_2 Depth=1
	movq	$0, 152(%rbp)
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 128(%rbp)
	movq	$0, 136(%rbp)
	leaq	24(%rbp), %rdi
.Ltmp269:
	callq	Event_Close
.Ltmp270:
# BB#8:                                 #   in Loop: Header=BB34_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB34_9:                               #   in Loop: Header=BB34_2 Depth=1
	incq	%r15
	cmpq	%r14, %r15
	jl	.LBB34_2
.LBB34_10:                              # %._crit_edge
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	12(%rsp), %edx          # 4-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB34_11:                              # %.body
.Ltmp271:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end34:
	.size	_ZN13CObjectVectorI13CStreamBinderE6DeleteEii, .Lfunc_end34-_ZN13CObjectVectorI13CStreamBinderE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp269-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp270-.Ltmp269       #   Call between .Ltmp269 and .Ltmp270
	.long	.Ltmp271-.Lfunc_begin15 #     jumps to .Ltmp271
	.byte	0                       #   On action: cleanup
	.long	.Ltmp270-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Lfunc_end34-.Ltmp270   #   Call between .Ltmp270 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev,@function
_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev: # @_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%r14
.Lcfi209:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi210:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi211:
	.cfi_def_cfa_offset 32
.Lcfi212:
	.cfi_offset %rbx, -24
.Lcfi213:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, (%rbx)
.Ltmp272:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp273:
# BB#1:
.Ltmp278:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp279:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB35_5:
.Ltmp280:
	movq	%rax, %r14
	jmp	.LBB35_6
.LBB35_3:
.Ltmp274:
	movq	%rax, %r14
.Ltmp275:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp276:
.LBB35_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB35_4:
.Ltmp277:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end35:
	.size	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev, .Lfunc_end35-_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp272-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp273-.Ltmp272       #   Call between .Ltmp272 and .Ltmp273
	.long	.Ltmp274-.Lfunc_begin16 #     jumps to .Ltmp274
	.byte	0                       #   On action: cleanup
	.long	.Ltmp278-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp279-.Ltmp278       #   Call between .Ltmp278 and .Ltmp279
	.long	.Ltmp280-.Lfunc_begin16 #     jumps to .Ltmp280
	.byte	0                       #   On action: cleanup
	.long	.Ltmp275-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Ltmp276-.Ltmp275       #   Call between .Ltmp275 and .Ltmp276
	.long	.Ltmp277-.Lfunc_begin16 #     jumps to .Ltmp277
	.byte	1                       #   On action: 1
	.long	.Ltmp276-.Lfunc_begin16 # >> Call Site 4 <<
	.long	.Lfunc_end35-.Ltmp276   #   Call between .Ltmp276 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii,@function
_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii: # @_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi214:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi215:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi216:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi217:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi218:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi219:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi220:
	.cfi_def_cfa_offset 64
.Lcfi221:
	.cfi_offset %rbx, -56
.Lcfi222:
	.cfi_offset %r12, -48
.Lcfi223:
	.cfi_offset %r13, -40
.Lcfi224:
	.cfi_offset %r14, -32
.Lcfi225:
	.cfi_offset %r15, -24
.Lcfi226:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB36_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB36_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB36_6
# BB#3:                                 #   in Loop: Header=BB36_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB36_5
# BB#4:                                 #   in Loop: Header=BB36_2 Depth=1
	movq	(%rdi), %rax
.Ltmp281:
	callq	*16(%rax)
.Ltmp282:
.LBB36_5:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
                                        #   in Loop: Header=BB36_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB36_6:                               #   in Loop: Header=BB36_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB36_2
.LBB36_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB36_8:
.Ltmp283:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end36:
	.size	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii, .Lfunc_end36-_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp281-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp282-.Ltmp281       #   Call between .Ltmp281 and .Ltmp282
	.long	.Ltmp283-.Lfunc_begin17 #     jumps to .Ltmp283
	.byte	0                       #   On action: cleanup
	.long	.Ltmp282-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Lfunc_end36-.Ltmp282   #   Call between .Ltmp282 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev,@function
_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev: # @_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi227:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi228:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi229:
	.cfi_def_cfa_offset 32
.Lcfi230:
	.cfi_offset %rbx, -24
.Lcfi231:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, (%rbx)
.Ltmp284:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp285:
# BB#1:
.Ltmp290:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp291:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB37_5:
.Ltmp292:
	movq	%rax, %r14
	jmp	.LBB37_6
.LBB37_3:
.Ltmp286:
	movq	%rax, %r14
.Ltmp287:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp288:
.LBB37_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB37_4:
.Ltmp289:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end37:
	.size	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev, .Lfunc_end37-_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table37:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp284-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp285-.Ltmp284       #   Call between .Ltmp284 and .Ltmp285
	.long	.Ltmp286-.Lfunc_begin18 #     jumps to .Ltmp286
	.byte	0                       #   On action: cleanup
	.long	.Ltmp290-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp291-.Ltmp290       #   Call between .Ltmp290 and .Ltmp291
	.long	.Ltmp292-.Lfunc_begin18 #     jumps to .Ltmp292
	.byte	0                       #   On action: cleanup
	.long	.Ltmp287-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Ltmp288-.Ltmp287       #   Call between .Ltmp287 and .Ltmp288
	.long	.Ltmp289-.Lfunc_begin18 #     jumps to .Ltmp289
	.byte	1                       #   On action: 1
	.long	.Ltmp288-.Lfunc_begin18 # >> Call Site 4 <<
	.long	.Lfunc_end37-.Ltmp288   #   Call between .Ltmp288 and .Lfunc_end37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii,@function
_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii: # @_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi232:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi233:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi234:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi235:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi236:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi237:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi238:
	.cfi_def_cfa_offset 64
.Lcfi239:
	.cfi_offset %rbx, -56
.Lcfi240:
	.cfi_offset %r12, -48
.Lcfi241:
	.cfi_offset %r13, -40
.Lcfi242:
	.cfi_offset %r14, -32
.Lcfi243:
	.cfi_offset %r15, -24
.Lcfi244:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB38_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB38_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB38_6
# BB#3:                                 #   in Loop: Header=BB38_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB38_5
# BB#4:                                 #   in Loop: Header=BB38_2 Depth=1
	movq	(%rdi), %rax
.Ltmp293:
	callq	*16(%rax)
.Ltmp294:
.LBB38_5:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
                                        #   in Loop: Header=BB38_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB38_6:                               #   in Loop: Header=BB38_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB38_2
.LBB38_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB38_8:
.Ltmp295:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end38:
	.size	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii, .Lfunc_end38-_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp293-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp294-.Ltmp293       #   Call between .Ltmp293 and .Ltmp294
	.long	.Ltmp295-.Lfunc_begin19 #     jumps to .Ltmp295
	.byte	0                       #   On action: cleanup
	.long	.Ltmp294-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Lfunc_end38-.Ltmp294   #   Call between .Ltmp294 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIP19ISequentialInStreamED0Ev,"axG",@progbits,_ZN13CRecordVectorIP19ISequentialInStreamED0Ev,comdat
	.weak	_ZN13CRecordVectorIP19ISequentialInStreamED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIP19ISequentialInStreamED0Ev,@function
_ZN13CRecordVectorIP19ISequentialInStreamED0Ev: # @_ZN13CRecordVectorIP19ISequentialInStreamED0Ev
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r14
.Lcfi245:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi246:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi247:
	.cfi_def_cfa_offset 32
.Lcfi248:
	.cfi_offset %rbx, -24
.Lcfi249:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp296:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp297:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB39_2:
.Ltmp298:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end39:
	.size	_ZN13CRecordVectorIP19ISequentialInStreamED0Ev, .Lfunc_end39-_ZN13CRecordVectorIP19ISequentialInStreamED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table39:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp296-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp297-.Ltmp296       #   Call between .Ltmp296 and .Ltmp297
	.long	.Ltmp298-.Lfunc_begin20 #     jumps to .Ltmp298
	.byte	0                       #   On action: cleanup
	.long	.Ltmp297-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Lfunc_end39-.Ltmp297   #   Call between .Ltmp297 and .Lfunc_end39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIP20ISequentialOutStreamED0Ev,"axG",@progbits,_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev,comdat
	.weak	_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev,@function
_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev: # @_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%r14
.Lcfi250:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi251:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi252:
	.cfi_def_cfa_offset 32
.Lcfi253:
	.cfi_offset %rbx, -24
.Lcfi254:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp299:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp300:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB40_2:
.Ltmp301:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end40:
	.size	_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev, .Lfunc_end40-_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp299-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp300-.Ltmp299       #   Call between .Ltmp299 and .Ltmp300
	.long	.Ltmp301-.Lfunc_begin21 #     jumps to .Ltmp301
	.byte	0                       #   On action: cleanup
	.long	.Ltmp300-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Lfunc_end40-.Ltmp300   #   Call between .Ltmp300 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11NCoderMixer7CCoder2C2ERKS0_,"axG",@progbits,_ZN11NCoderMixer7CCoder2C2ERKS0_,comdat
	.weak	_ZN11NCoderMixer7CCoder2C2ERKS0_
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer7CCoder2C2ERKS0_,@function
_ZN11NCoderMixer7CCoder2C2ERKS0_:       # @_ZN11NCoderMixer7CCoder2C2ERKS0_
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%rbp
.Lcfi255:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi256:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi257:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi258:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi259:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi260:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi261:
	.cfi_def_cfa_offset 64
.Lcfi262:
	.cfi_offset %rbx, -56
.Lcfi263:
	.cfi_offset %r12, -48
.Lcfi264:
	.cfi_offset %r13, -40
.Lcfi265:
	.cfi_offset %r14, -32
.Lcfi266:
	.cfi_offset %r15, -24
.Lcfi267:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	leaq	240(%rbp), %r14
	leaq	240(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZN11NCoderMixer11CCoderInfo2C2ERKS0_
	movq	$_ZTV11CVirtThread+16, (%rbp)
	leaq	8(%rbp), %rdi
	leaq	8(%rbx), %rsi
	movl	$225, %edx
	callq	memcpy
	movq	$_ZTVN11NCoderMixer7CCoder2E+16, (%rbp)
	movl	392(%rbx), %eax
	movl	%eax, 392(%rbp)
	leaq	400(%rbp), %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 408(%rbp)
	movq	$8, 424(%rbp)
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, 400(%rbp)
.Ltmp302:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp303:
# BB#1:                                 # %.noexc.i
	leaq	400(%rbx), %rsi
.Ltmp304:
	movq	%r15, %rdi
	callq	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEEpLERKS3_
.Ltmp305:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEEC2ERKS3_.exit
	leaq	432(%rbp), %r13
	xorps	%xmm0, %xmm0
	movups	%xmm0, 440(%rbp)
	movq	$8, 456(%rbp)
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, 432(%rbp)
.Ltmp310:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp311:
# BB#3:                                 # %.noexc.i12
	leaq	432(%rbx), %rsi
.Ltmp312:
	movq	%r13, %rdi
	callq	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEEpLERKS3_
.Ltmp313:
# BB#4:                                 # %_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEEC2ERKS3_.exit
	leaq	464(%rbp), %r12
	leaq	464(%rbx), %rsi
.Ltmp318:
	movq	%r12, %rdi
	callq	_ZN13CRecordVectorIP19ISequentialInStreamEC2ERKS2_
.Ltmp319:
# BB#5:
	leaq	496(%rbp), %rdi
	addq	$496, %rbx              # imm = 0x1F0
.Ltmp321:
	movq	%rbx, %rsi
	callq	_ZN13CRecordVectorIP20ISequentialOutStreamEC2ERKS2_
.Ltmp322:
# BB#6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB41_10:
.Ltmp323:
	movq	%rax, %rbx
.Ltmp324:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp325:
	jmp	.LBB41_11
.LBB41_9:
.Ltmp320:
	movq	%rax, %rbx
.LBB41_11:
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, (%r13)
.Ltmp326:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp327:
# BB#12:
.Ltmp332:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp333:
	jmp	.LBB41_13
.LBB41_18:
.Ltmp328:
	movq	%rax, %rbx
.Ltmp329:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp330:
	jmp	.LBB41_23
.LBB41_19:
.Ltmp331:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_8:
.Ltmp314:
	movq	%rax, %rbx
.Ltmp315:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp316:
.LBB41_13:                              # %_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev.exit
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, (%r15)
.Ltmp334:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp335:
# BB#14:
.Ltmp340:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp341:
	jmp	.LBB41_15
.LBB41_25:
.Ltmp317:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_20:
.Ltmp336:
	movq	%rax, %rbx
.Ltmp337:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp338:
	jmp	.LBB41_23
.LBB41_21:
.Ltmp339:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_7:
.Ltmp306:
	movq	%rax, %rbx
.Ltmp307:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp308:
.LBB41_15:                              # %_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev.exit
.Ltmp342:
	movq	%rbp, %rdi
	callq	_ZN11CVirtThreadD2Ev
.Ltmp343:
# BB#16:
.Ltmp344:
	movq	%r14, %rdi
	callq	_ZN11NCoderMixer11CCoderInfo2D2Ev
.Ltmp345:
# BB#17:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB41_24:
.Ltmp309:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_22:
.Ltmp346:
	movq	%rax, %rbx
.LBB41_23:                              # %.body15
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end41:
	.size	_ZN11NCoderMixer7CCoder2C2ERKS0_, .Lfunc_end41-_ZN11NCoderMixer7CCoder2C2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table41:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\331\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\320\001"              # Call site table length
	.long	.Lfunc_begin22-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp302-.Lfunc_begin22 #   Call between .Lfunc_begin22 and .Ltmp302
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp302-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Ltmp305-.Ltmp302       #   Call between .Ltmp302 and .Ltmp305
	.long	.Ltmp306-.Lfunc_begin22 #     jumps to .Ltmp306
	.byte	0                       #   On action: cleanup
	.long	.Ltmp310-.Lfunc_begin22 # >> Call Site 3 <<
	.long	.Ltmp313-.Ltmp310       #   Call between .Ltmp310 and .Ltmp313
	.long	.Ltmp314-.Lfunc_begin22 #     jumps to .Ltmp314
	.byte	0                       #   On action: cleanup
	.long	.Ltmp318-.Lfunc_begin22 # >> Call Site 4 <<
	.long	.Ltmp319-.Ltmp318       #   Call between .Ltmp318 and .Ltmp319
	.long	.Ltmp320-.Lfunc_begin22 #     jumps to .Ltmp320
	.byte	0                       #   On action: cleanup
	.long	.Ltmp321-.Lfunc_begin22 # >> Call Site 5 <<
	.long	.Ltmp322-.Ltmp321       #   Call between .Ltmp321 and .Ltmp322
	.long	.Ltmp323-.Lfunc_begin22 #     jumps to .Ltmp323
	.byte	0                       #   On action: cleanup
	.long	.Ltmp324-.Lfunc_begin22 # >> Call Site 6 <<
	.long	.Ltmp325-.Ltmp324       #   Call between .Ltmp324 and .Ltmp325
	.long	.Ltmp346-.Lfunc_begin22 #     jumps to .Ltmp346
	.byte	1                       #   On action: 1
	.long	.Ltmp326-.Lfunc_begin22 # >> Call Site 7 <<
	.long	.Ltmp327-.Ltmp326       #   Call between .Ltmp326 and .Ltmp327
	.long	.Ltmp328-.Lfunc_begin22 #     jumps to .Ltmp328
	.byte	1                       #   On action: 1
	.long	.Ltmp332-.Lfunc_begin22 # >> Call Site 8 <<
	.long	.Ltmp333-.Ltmp332       #   Call between .Ltmp332 and .Ltmp333
	.long	.Ltmp346-.Lfunc_begin22 #     jumps to .Ltmp346
	.byte	1                       #   On action: 1
	.long	.Ltmp329-.Lfunc_begin22 # >> Call Site 9 <<
	.long	.Ltmp330-.Ltmp329       #   Call between .Ltmp329 and .Ltmp330
	.long	.Ltmp331-.Lfunc_begin22 #     jumps to .Ltmp331
	.byte	1                       #   On action: 1
	.long	.Ltmp315-.Lfunc_begin22 # >> Call Site 10 <<
	.long	.Ltmp316-.Ltmp315       #   Call between .Ltmp315 and .Ltmp316
	.long	.Ltmp317-.Lfunc_begin22 #     jumps to .Ltmp317
	.byte	1                       #   On action: 1
	.long	.Ltmp334-.Lfunc_begin22 # >> Call Site 11 <<
	.long	.Ltmp335-.Ltmp334       #   Call between .Ltmp334 and .Ltmp335
	.long	.Ltmp336-.Lfunc_begin22 #     jumps to .Ltmp336
	.byte	1                       #   On action: 1
	.long	.Ltmp340-.Lfunc_begin22 # >> Call Site 12 <<
	.long	.Ltmp341-.Ltmp340       #   Call between .Ltmp340 and .Ltmp341
	.long	.Ltmp346-.Lfunc_begin22 #     jumps to .Ltmp346
	.byte	1                       #   On action: 1
	.long	.Ltmp337-.Lfunc_begin22 # >> Call Site 13 <<
	.long	.Ltmp338-.Ltmp337       #   Call between .Ltmp337 and .Ltmp338
	.long	.Ltmp339-.Lfunc_begin22 #     jumps to .Ltmp339
	.byte	1                       #   On action: 1
	.long	.Ltmp307-.Lfunc_begin22 # >> Call Site 14 <<
	.long	.Ltmp308-.Ltmp307       #   Call between .Ltmp307 and .Ltmp308
	.long	.Ltmp309-.Lfunc_begin22 #     jumps to .Ltmp309
	.byte	1                       #   On action: 1
	.long	.Ltmp342-.Lfunc_begin22 # >> Call Site 15 <<
	.long	.Ltmp345-.Ltmp342       #   Call between .Ltmp342 and .Ltmp345
	.long	.Ltmp346-.Lfunc_begin22 #     jumps to .Ltmp346
	.byte	1                       #   On action: 1
	.long	.Ltmp345-.Lfunc_begin22 # >> Call Site 16 <<
	.long	.Lfunc_end41-.Ltmp345   #   Call between .Ltmp345 and .Lfunc_end41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11NCoderMixer11CCoderInfo2C2ERKS0_,"axG",@progbits,_ZN11NCoderMixer11CCoderInfo2C2ERKS0_,comdat
	.weak	_ZN11NCoderMixer11CCoderInfo2C2ERKS0_
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer11CCoderInfo2C2ERKS0_,@function
_ZN11NCoderMixer11CCoderInfo2C2ERKS0_:  # @_ZN11NCoderMixer11CCoderInfo2C2ERKS0_
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:
	pushq	%rbp
.Lcfi268:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi269:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi270:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi271:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi272:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi273:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi274:
	.cfi_def_cfa_offset 64
.Lcfi275:
	.cfi_offset %rbx, -56
.Lcfi276:
	.cfi_offset %r12, -48
.Lcfi277:
	.cfi_offset %r13, -40
.Lcfi278:
	.cfi_offset %r14, -32
.Lcfi279:
	.cfi_offset %r15, -24
.Lcfi280:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	(%rbx), %rdi
	movq	%rdi, (%r12)
	testq	%rdi, %rdi
	je	.LBB42_2
# BB#1:
	movq	(%rdi), %rax
	callq	*8(%rax)
.LBB42_2:                               # %_ZN9CMyComPtrI14ICompressCoderEC2ERKS1_.exit
	movq	8(%rbx), %rdi
	movq	%rdi, 8(%r12)
	testq	%rdi, %rdi
	je	.LBB42_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp347:
	callq	*8(%rax)
.Ltmp348:
.LBB42_4:                               # %_ZN9CMyComPtrI15ICompressCoder2EC2ERKS1_.exit
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
	leaq	24(%r12), %r14
	leaq	24(%rbx), %rsi
.Ltmp350:
	movq	%r14, %rdi
	callq	_ZN13CRecordVectorIyEC2ERKS0_
.Ltmp351:
# BB#5:
	leaq	56(%r12), %r13
	leaq	56(%rbx), %rsi
.Ltmp353:
	movq	%r13, %rdi
	callq	_ZN13CRecordVectorIyEC2ERKS0_
.Ltmp354:
# BB#6:
	leaq	88(%r12), %rbp
	leaq	88(%rbx), %rsi
.Ltmp356:
	movq	%rbp, %rdi
	callq	_ZN13CRecordVectorIPKyEC2ERKS2_
.Ltmp357:
# BB#7:
	leaq	120(%r12), %rdi
	addq	$120, %rbx
.Ltmp359:
	movq	%rbx, %rsi
	callq	_ZN13CRecordVectorIPKyEC2ERKS2_
.Ltmp360:
# BB#8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB42_9:
.Ltmp349:
	movq	%rax, %r15
	jmp	.LBB42_18
.LBB42_13:
.Ltmp361:
	movq	%rax, %r15
.Ltmp362:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp363:
	jmp	.LBB42_14
.LBB42_12:
.Ltmp358:
	movq	%rax, %r15
.LBB42_14:
.Ltmp364:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp365:
	jmp	.LBB42_15
.LBB42_11:
.Ltmp355:
	movq	%rax, %r15
.LBB42_15:
.Ltmp366:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp367:
	jmp	.LBB42_16
.LBB42_10:
.Ltmp352:
	movq	%rax, %r15
.LBB42_16:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB42_18
# BB#17:
	movq	(%rdi), %rax
.Ltmp368:
	callq	*16(%rax)
.Ltmp369:
.LBB42_18:                              # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB42_20
# BB#19:
	movq	(%rdi), %rax
.Ltmp370:
	callq	*16(%rax)
.Ltmp371:
.LBB42_20:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB42_21:
.Ltmp372:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end42:
	.size	_ZN11NCoderMixer11CCoderInfo2C2ERKS0_, .Lfunc_end42-_ZN11NCoderMixer11CCoderInfo2C2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table42:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin23-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp347-.Lfunc_begin23 #   Call between .Lfunc_begin23 and .Ltmp347
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp347-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Ltmp348-.Ltmp347       #   Call between .Ltmp347 and .Ltmp348
	.long	.Ltmp349-.Lfunc_begin23 #     jumps to .Ltmp349
	.byte	0                       #   On action: cleanup
	.long	.Ltmp350-.Lfunc_begin23 # >> Call Site 3 <<
	.long	.Ltmp351-.Ltmp350       #   Call between .Ltmp350 and .Ltmp351
	.long	.Ltmp352-.Lfunc_begin23 #     jumps to .Ltmp352
	.byte	0                       #   On action: cleanup
	.long	.Ltmp353-.Lfunc_begin23 # >> Call Site 4 <<
	.long	.Ltmp354-.Ltmp353       #   Call between .Ltmp353 and .Ltmp354
	.long	.Ltmp355-.Lfunc_begin23 #     jumps to .Ltmp355
	.byte	0                       #   On action: cleanup
	.long	.Ltmp356-.Lfunc_begin23 # >> Call Site 5 <<
	.long	.Ltmp357-.Ltmp356       #   Call between .Ltmp356 and .Ltmp357
	.long	.Ltmp358-.Lfunc_begin23 #     jumps to .Ltmp358
	.byte	0                       #   On action: cleanup
	.long	.Ltmp359-.Lfunc_begin23 # >> Call Site 6 <<
	.long	.Ltmp360-.Ltmp359       #   Call between .Ltmp359 and .Ltmp360
	.long	.Ltmp361-.Lfunc_begin23 #     jumps to .Ltmp361
	.byte	0                       #   On action: cleanup
	.long	.Ltmp362-.Lfunc_begin23 # >> Call Site 7 <<
	.long	.Ltmp371-.Ltmp362       #   Call between .Ltmp362 and .Ltmp371
	.long	.Ltmp372-.Lfunc_begin23 #     jumps to .Ltmp372
	.byte	1                       #   On action: 1
	.long	.Ltmp371-.Lfunc_begin23 # >> Call Site 8 <<
	.long	.Lfunc_end42-.Ltmp371   #   Call between .Ltmp371 and .Lfunc_end42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIP19ISequentialInStreamEC2ERKS2_,"axG",@progbits,_ZN13CRecordVectorIP19ISequentialInStreamEC2ERKS2_,comdat
	.weak	_ZN13CRecordVectorIP19ISequentialInStreamEC2ERKS2_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIP19ISequentialInStreamEC2ERKS2_,@function
_ZN13CRecordVectorIP19ISequentialInStreamEC2ERKS2_: # @_ZN13CRecordVectorIP19ISequentialInStreamEC2ERKS2_
.Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception24
# BB#0:
	pushq	%r15
.Lcfi281:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi282:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi283:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi284:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi285:
	.cfi_def_cfa_offset 48
.Lcfi286:
	.cfi_offset %rbx, -48
.Lcfi287:
	.cfi_offset %r12, -40
.Lcfi288:
	.cfi_offset %r13, -32
.Lcfi289:
	.cfi_offset %r14, -24
.Lcfi290:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIP19ISequentialInStreamE+16, (%r12)
.Ltmp373:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp374:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp375:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp376:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB43_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB43_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp378:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp379:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB43_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB43_4
.LBB43_6:                               # %_ZN13CRecordVectorIP19ISequentialInStreamEaSERKS2_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB43_8:                               # %.loopexit.split-lp
.Ltmp377:
	jmp	.LBB43_9
.LBB43_7:                               # %.loopexit
.Ltmp380:
.LBB43_9:
	movq	%rax, %r14
.Ltmp381:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp382:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB43_11:
.Ltmp383:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end43:
	.size	_ZN13CRecordVectorIP19ISequentialInStreamEC2ERKS2_, .Lfunc_end43-_ZN13CRecordVectorIP19ISequentialInStreamEC2ERKS2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table43:
.Lexception24:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp373-.Lfunc_begin24 # >> Call Site 1 <<
	.long	.Ltmp376-.Ltmp373       #   Call between .Ltmp373 and .Ltmp376
	.long	.Ltmp377-.Lfunc_begin24 #     jumps to .Ltmp377
	.byte	0                       #   On action: cleanup
	.long	.Ltmp378-.Lfunc_begin24 # >> Call Site 2 <<
	.long	.Ltmp379-.Ltmp378       #   Call between .Ltmp378 and .Ltmp379
	.long	.Ltmp380-.Lfunc_begin24 #     jumps to .Ltmp380
	.byte	0                       #   On action: cleanup
	.long	.Ltmp381-.Lfunc_begin24 # >> Call Site 3 <<
	.long	.Ltmp382-.Ltmp381       #   Call between .Ltmp381 and .Ltmp382
	.long	.Ltmp383-.Lfunc_begin24 #     jumps to .Ltmp383
	.byte	1                       #   On action: 1
	.long	.Ltmp382-.Lfunc_begin24 # >> Call Site 4 <<
	.long	.Lfunc_end43-.Ltmp382   #   Call between .Ltmp382 and .Lfunc_end43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIP20ISequentialOutStreamEC2ERKS2_,"axG",@progbits,_ZN13CRecordVectorIP20ISequentialOutStreamEC2ERKS2_,comdat
	.weak	_ZN13CRecordVectorIP20ISequentialOutStreamEC2ERKS2_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIP20ISequentialOutStreamEC2ERKS2_,@function
_ZN13CRecordVectorIP20ISequentialOutStreamEC2ERKS2_: # @_ZN13CRecordVectorIP20ISequentialOutStreamEC2ERKS2_
.Lfunc_begin25:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception25
# BB#0:
	pushq	%r15
.Lcfi291:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi292:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi293:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi294:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi295:
	.cfi_def_cfa_offset 48
.Lcfi296:
	.cfi_offset %rbx, -48
.Lcfi297:
	.cfi_offset %r12, -40
.Lcfi298:
	.cfi_offset %r13, -32
.Lcfi299:
	.cfi_offset %r14, -24
.Lcfi300:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIP20ISequentialOutStreamE+16, (%r12)
.Ltmp384:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp385:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp386:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp387:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB44_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB44_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp389:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp390:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB44_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB44_4
.LBB44_6:                               # %_ZN13CRecordVectorIP20ISequentialOutStreamEaSERKS2_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB44_8:                               # %.loopexit.split-lp
.Ltmp388:
	jmp	.LBB44_9
.LBB44_7:                               # %.loopexit
.Ltmp391:
.LBB44_9:
	movq	%rax, %r14
.Ltmp392:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp393:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB44_11:
.Ltmp394:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end44:
	.size	_ZN13CRecordVectorIP20ISequentialOutStreamEC2ERKS2_, .Lfunc_end44-_ZN13CRecordVectorIP20ISequentialOutStreamEC2ERKS2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table44:
.Lexception25:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp384-.Lfunc_begin25 # >> Call Site 1 <<
	.long	.Ltmp387-.Ltmp384       #   Call between .Ltmp384 and .Ltmp387
	.long	.Ltmp388-.Lfunc_begin25 #     jumps to .Ltmp388
	.byte	0                       #   On action: cleanup
	.long	.Ltmp389-.Lfunc_begin25 # >> Call Site 2 <<
	.long	.Ltmp390-.Ltmp389       #   Call between .Ltmp389 and .Ltmp390
	.long	.Ltmp391-.Lfunc_begin25 #     jumps to .Ltmp391
	.byte	0                       #   On action: cleanup
	.long	.Ltmp392-.Lfunc_begin25 # >> Call Site 3 <<
	.long	.Ltmp393-.Ltmp392       #   Call between .Ltmp392 and .Ltmp393
	.long	.Ltmp394-.Lfunc_begin25 #     jumps to .Ltmp394
	.byte	1                       #   On action: 1
	.long	.Ltmp393-.Lfunc_begin25 # >> Call Site 4 <<
	.long	.Lfunc_end44-.Ltmp393   #   Call between .Ltmp393 and .Lfunc_end44
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIyEC2ERKS0_,"axG",@progbits,_ZN13CRecordVectorIyEC2ERKS0_,comdat
	.weak	_ZN13CRecordVectorIyEC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyEC2ERKS0_,@function
_ZN13CRecordVectorIyEC2ERKS0_:          # @_ZN13CRecordVectorIyEC2ERKS0_
.Lfunc_begin26:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception26
# BB#0:
	pushq	%r15
.Lcfi301:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi302:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi303:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi304:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi305:
	.cfi_def_cfa_offset 48
.Lcfi306:
	.cfi_offset %rbx, -48
.Lcfi307:
	.cfi_offset %r12, -40
.Lcfi308:
	.cfi_offset %r13, -32
.Lcfi309:
	.cfi_offset %r14, -24
.Lcfi310:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIyE+16, (%r12)
.Ltmp395:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp396:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp397:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp398:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB45_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB45_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp400:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp401:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB45_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB45_4
.LBB45_6:                               # %_ZN13CRecordVectorIyEaSERKS0_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB45_8:                               # %.loopexit.split-lp
.Ltmp399:
	jmp	.LBB45_9
.LBB45_7:                               # %.loopexit
.Ltmp402:
.LBB45_9:
	movq	%rax, %r14
.Ltmp403:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp404:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB45_11:
.Ltmp405:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end45:
	.size	_ZN13CRecordVectorIyEC2ERKS0_, .Lfunc_end45-_ZN13CRecordVectorIyEC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table45:
.Lexception26:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp395-.Lfunc_begin26 # >> Call Site 1 <<
	.long	.Ltmp398-.Ltmp395       #   Call between .Ltmp395 and .Ltmp398
	.long	.Ltmp399-.Lfunc_begin26 #     jumps to .Ltmp399
	.byte	0                       #   On action: cleanup
	.long	.Ltmp400-.Lfunc_begin26 # >> Call Site 2 <<
	.long	.Ltmp401-.Ltmp400       #   Call between .Ltmp400 and .Ltmp401
	.long	.Ltmp402-.Lfunc_begin26 #     jumps to .Ltmp402
	.byte	0                       #   On action: cleanup
	.long	.Ltmp403-.Lfunc_begin26 # >> Call Site 3 <<
	.long	.Ltmp404-.Ltmp403       #   Call between .Ltmp403 and .Ltmp404
	.long	.Ltmp405-.Lfunc_begin26 #     jumps to .Ltmp405
	.byte	1                       #   On action: 1
	.long	.Ltmp404-.Lfunc_begin26 # >> Call Site 4 <<
	.long	.Lfunc_end45-.Ltmp404   #   Call between .Ltmp404 and .Lfunc_end45
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIPKyEC2ERKS2_,"axG",@progbits,_ZN13CRecordVectorIPKyEC2ERKS2_,comdat
	.weak	_ZN13CRecordVectorIPKyEC2ERKS2_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIPKyEC2ERKS2_,@function
_ZN13CRecordVectorIPKyEC2ERKS2_:        # @_ZN13CRecordVectorIPKyEC2ERKS2_
.Lfunc_begin27:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception27
# BB#0:
	pushq	%r15
.Lcfi311:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi312:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi313:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi314:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi315:
	.cfi_def_cfa_offset 48
.Lcfi316:
	.cfi_offset %rbx, -48
.Lcfi317:
	.cfi_offset %r12, -40
.Lcfi318:
	.cfi_offset %r13, -32
.Lcfi319:
	.cfi_offset %r14, -24
.Lcfi320:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIPKyE+16, (%r12)
.Ltmp406:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp407:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp408:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp409:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB46_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB46_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp411:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp412:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB46_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB46_4
.LBB46_6:                               # %_ZN13CRecordVectorIPKyEaSERKS2_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB46_8:                               # %.loopexit.split-lp
.Ltmp410:
	jmp	.LBB46_9
.LBB46_7:                               # %.loopexit
.Ltmp413:
.LBB46_9:
	movq	%rax, %r14
.Ltmp414:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp415:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB46_11:
.Ltmp416:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end46:
	.size	_ZN13CRecordVectorIPKyEC2ERKS2_, .Lfunc_end46-_ZN13CRecordVectorIPKyEC2ERKS2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table46:
.Lexception27:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp406-.Lfunc_begin27 # >> Call Site 1 <<
	.long	.Ltmp409-.Ltmp406       #   Call between .Ltmp406 and .Ltmp409
	.long	.Ltmp410-.Lfunc_begin27 #     jumps to .Ltmp410
	.byte	0                       #   On action: cleanup
	.long	.Ltmp411-.Lfunc_begin27 # >> Call Site 2 <<
	.long	.Ltmp412-.Ltmp411       #   Call between .Ltmp411 and .Ltmp412
	.long	.Ltmp413-.Lfunc_begin27 #     jumps to .Ltmp413
	.byte	0                       #   On action: cleanup
	.long	.Ltmp414-.Lfunc_begin27 # >> Call Site 3 <<
	.long	.Ltmp415-.Ltmp414       #   Call between .Ltmp414 and .Ltmp415
	.long	.Ltmp416-.Lfunc_begin27 #     jumps to .Ltmp416
	.byte	1                       #   On action: 1
	.long	.Ltmp415-.Lfunc_begin27 # >> Call Site 4 <<
	.long	.Lfunc_end46-.Ltmp415   #   Call between .Ltmp415 and .Lfunc_end46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin28:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception28
# BB#0:
	pushq	%r14
.Lcfi321:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi322:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi323:
	.cfi_def_cfa_offset 32
.Lcfi324:
	.cfi_offset %rbx, -24
.Lcfi325:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp417:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp418:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB47_2:
.Ltmp419:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end47:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end47-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table47:
.Lexception28:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp417-.Lfunc_begin28 # >> Call Site 1 <<
	.long	.Ltmp418-.Ltmp417       #   Call between .Ltmp417 and .Ltmp418
	.long	.Ltmp419-.Lfunc_begin28 #     jumps to .Ltmp419
	.byte	0                       #   On action: cleanup
	.long	.Ltmp418-.Lfunc_begin28 # >> Call Site 2 <<
	.long	.Lfunc_end47-.Ltmp418   #   Call between .Ltmp418 and .Lfunc_end47
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIPKyED0Ev,"axG",@progbits,_ZN13CRecordVectorIPKyED0Ev,comdat
	.weak	_ZN13CRecordVectorIPKyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIPKyED0Ev,@function
_ZN13CRecordVectorIPKyED0Ev:            # @_ZN13CRecordVectorIPKyED0Ev
.Lfunc_begin29:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception29
# BB#0:
	pushq	%r14
.Lcfi326:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi327:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi328:
	.cfi_def_cfa_offset 32
.Lcfi329:
	.cfi_offset %rbx, -24
.Lcfi330:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp420:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp421:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB48_2:
.Ltmp422:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end48:
	.size	_ZN13CRecordVectorIPKyED0Ev, .Lfunc_end48-_ZN13CRecordVectorIPKyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table48:
.Lexception29:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp420-.Lfunc_begin29 # >> Call Site 1 <<
	.long	.Ltmp421-.Ltmp420       #   Call between .Ltmp420 and .Ltmp421
	.long	.Ltmp422-.Lfunc_begin29 #     jumps to .Ltmp422
	.byte	0                       #   On action: cleanup
	.long	.Ltmp421-.Lfunc_begin29 # >> Call Site 2 <<
	.long	.Lfunc_end48-.Ltmp421   #   Call between .Ltmp421 and .Lfunc_end48
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEEpLERKS3_,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEEpLERKS3_,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEEpLERKS3_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEEpLERKS3_,@function
_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEEpLERKS3_: # @_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEEpLERKS3_
.Lfunc_begin30:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception30
# BB#0:
	pushq	%rbp
.Lcfi331:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi332:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi333:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi334:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi335:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi336:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi337:
	.cfi_def_cfa_offset 64
.Lcfi338:
	.cfi_offset %rbx, -56
.Lcfi339:
	.cfi_offset %r12, -48
.Lcfi340:
	.cfi_offset %r13, -40
.Lcfi341:
	.cfi_offset %r14, -32
.Lcfi342:
	.cfi_offset %r15, -24
.Lcfi343:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	12(%r14), %r12
	movl	12(%r15), %esi
	addl	%r12d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testq	%r12, %r12
	jle	.LBB49_5
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB49_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %rbp
	movl	$8, %edi
	callq	_Znwm
	movq	%rax, %r13
	movq	(%rbp), %rdi
	movq	%rdi, (%r13)
	testq	%rdi, %rdi
	je	.LBB49_4
# BB#3:                                 #   in Loop: Header=BB49_2 Depth=1
	movq	(%rdi), %rax
.Ltmp423:
	callq	*8(%rax)
.Ltmp424:
.LBB49_4:                               # %_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE3AddERKS2_.exit
                                        #   in Loop: Header=BB49_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	incq	%rbx
	cmpq	%r12, %rbx
	jl	.LBB49_2
.LBB49_5:                               # %._crit_edge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB49_6:
.Ltmp425:
	movq	%rax, %r14
	movq	%r13, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end49:
	.size	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEEpLERKS3_, .Lfunc_end49-_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEEpLERKS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table49:
.Lexception30:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin30-.Lfunc_begin30 # >> Call Site 1 <<
	.long	.Ltmp423-.Lfunc_begin30 #   Call between .Lfunc_begin30 and .Ltmp423
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp423-.Lfunc_begin30 # >> Call Site 2 <<
	.long	.Ltmp424-.Ltmp423       #   Call between .Ltmp423 and .Ltmp424
	.long	.Ltmp425-.Lfunc_begin30 #     jumps to .Ltmp425
	.byte	0                       #   On action: cleanup
	.long	.Ltmp424-.Lfunc_begin30 # >> Call Site 3 <<
	.long	.Lfunc_end49-.Ltmp424   #   Call between .Ltmp424 and .Lfunc_end49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEEpLERKS3_,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEEpLERKS3_,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEEpLERKS3_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEEpLERKS3_,@function
_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEEpLERKS3_: # @_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEEpLERKS3_
.Lfunc_begin31:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception31
# BB#0:
	pushq	%rbp
.Lcfi344:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi345:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi346:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi347:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi348:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi349:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi350:
	.cfi_def_cfa_offset 64
.Lcfi351:
	.cfi_offset %rbx, -56
.Lcfi352:
	.cfi_offset %r12, -48
.Lcfi353:
	.cfi_offset %r13, -40
.Lcfi354:
	.cfi_offset %r14, -32
.Lcfi355:
	.cfi_offset %r15, -24
.Lcfi356:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	12(%r14), %r12
	movl	12(%r15), %esi
	addl	%r12d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testq	%r12, %r12
	jle	.LBB50_5
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB50_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %rbp
	movl	$8, %edi
	callq	_Znwm
	movq	%rax, %r13
	movq	(%rbp), %rdi
	movq	%rdi, (%r13)
	testq	%rdi, %rdi
	je	.LBB50_4
# BB#3:                                 #   in Loop: Header=BB50_2 Depth=1
	movq	(%rdi), %rax
.Ltmp426:
	callq	*8(%rax)
.Ltmp427:
.LBB50_4:                               # %_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE3AddERKS2_.exit
                                        #   in Loop: Header=BB50_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	incq	%rbx
	cmpq	%r12, %rbx
	jl	.LBB50_2
.LBB50_5:                               # %._crit_edge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB50_6:
.Ltmp428:
	movq	%rax, %r14
	movq	%r13, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end50:
	.size	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEEpLERKS3_, .Lfunc_end50-_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEEpLERKS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table50:
.Lexception31:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin31-.Lfunc_begin31 # >> Call Site 1 <<
	.long	.Ltmp426-.Lfunc_begin31 #   Call between .Lfunc_begin31 and .Ltmp426
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp426-.Lfunc_begin31 # >> Call Site 2 <<
	.long	.Ltmp427-.Ltmp426       #   Call between .Ltmp426 and .Ltmp427
	.long	.Ltmp428-.Lfunc_begin31 #     jumps to .Ltmp428
	.byte	0                       #   On action: cleanup
	.long	.Ltmp427-.Lfunc_begin31 # >> Call Site 3 <<
	.long	.Lfunc_end50-.Ltmp427   #   Call between .Ltmp427 and .Lfunc_end50
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTVN11NCoderMixer7CCoder2E,@object # @_ZTVN11NCoderMixer7CCoder2E
	.section	.rodata,"a",@progbits
	.globl	_ZTVN11NCoderMixer7CCoder2E
	.p2align	3
_ZTVN11NCoderMixer7CCoder2E:
	.quad	0
	.quad	_ZTIN11NCoderMixer7CCoder2E
	.quad	_ZN11NCoderMixer7CCoder27ExecuteEv
	.size	_ZTVN11NCoderMixer7CCoder2E, 24

	.type	_ZTVN11NCoderMixer14CCoderMixer2MTE,@object # @_ZTVN11NCoderMixer14CCoderMixer2MTE
	.globl	_ZTVN11NCoderMixer14CCoderMixer2MTE
	.p2align	3
_ZTVN11NCoderMixer14CCoderMixer2MTE:
	.quad	0
	.quad	_ZTIN11NCoderMixer14CCoderMixer2MTE
	.quad	_ZN11NCoderMixer14CCoderMixer2MT14QueryInterfaceERK4GUIDPPv
	.quad	_ZN11NCoderMixer14CCoderMixer2MT6AddRefEv
	.quad	_ZN11NCoderMixer14CCoderMixer2MT7ReleaseEv
	.quad	_ZN11NCoderMixer14CCoderMixer2MTD2Ev
	.quad	_ZN11NCoderMixer14CCoderMixer2MTD0Ev
	.quad	_ZN11NCoderMixer14CCoderMixer2MT4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS6_jP21ICompressProgressInfo
	.quad	_ZN11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE
	.quad	_ZN11NCoderMixer14CCoderMixer2MT6ReInitEv
	.quad	_ZN11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_
	.quad	-8
	.quad	_ZTIN11NCoderMixer14CCoderMixer2MTE
	.quad	_ZThn8_N11NCoderMixer14CCoderMixer2MT11SetBindInfoERKNS_9CBindInfoE
	.quad	_ZThn8_N11NCoderMixer14CCoderMixer2MT6ReInitEv
	.quad	_ZThn8_N11NCoderMixer14CCoderMixer2MT12SetCoderInfoEjPPKyS3_
	.size	_ZTVN11NCoderMixer14CCoderMixer2MTE, 128

	.type	_ZTSN11NCoderMixer14CCoderMixer2MTE,@object # @_ZTSN11NCoderMixer14CCoderMixer2MTE
	.globl	_ZTSN11NCoderMixer14CCoderMixer2MTE
	.p2align	4
_ZTSN11NCoderMixer14CCoderMixer2MTE:
	.asciz	"N11NCoderMixer14CCoderMixer2MTE"
	.size	_ZTSN11NCoderMixer14CCoderMixer2MTE, 32

	.type	_ZTS15ICompressCoder2,@object # @_ZTS15ICompressCoder2
	.section	.rodata._ZTS15ICompressCoder2,"aG",@progbits,_ZTS15ICompressCoder2,comdat
	.weak	_ZTS15ICompressCoder2
	.p2align	4
_ZTS15ICompressCoder2:
	.asciz	"15ICompressCoder2"
	.size	_ZTS15ICompressCoder2, 18

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI15ICompressCoder2,@object # @_ZTI15ICompressCoder2
	.section	.rodata._ZTI15ICompressCoder2,"aG",@progbits,_ZTI15ICompressCoder2,comdat
	.weak	_ZTI15ICompressCoder2
	.p2align	4
_ZTI15ICompressCoder2:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15ICompressCoder2
	.quad	_ZTI8IUnknown
	.size	_ZTI15ICompressCoder2, 24

	.type	_ZTSN11NCoderMixer12CCoderMixer2E,@object # @_ZTSN11NCoderMixer12CCoderMixer2E
	.section	.rodata._ZTSN11NCoderMixer12CCoderMixer2E,"aG",@progbits,_ZTSN11NCoderMixer12CCoderMixer2E,comdat
	.weak	_ZTSN11NCoderMixer12CCoderMixer2E
	.p2align	4
_ZTSN11NCoderMixer12CCoderMixer2E:
	.asciz	"N11NCoderMixer12CCoderMixer2E"
	.size	_ZTSN11NCoderMixer12CCoderMixer2E, 30

	.type	_ZTIN11NCoderMixer12CCoderMixer2E,@object # @_ZTIN11NCoderMixer12CCoderMixer2E
	.section	.rodata._ZTIN11NCoderMixer12CCoderMixer2E,"aG",@progbits,_ZTIN11NCoderMixer12CCoderMixer2E,comdat
	.weak	_ZTIN11NCoderMixer12CCoderMixer2E
	.p2align	3
_ZTIN11NCoderMixer12CCoderMixer2E:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN11NCoderMixer12CCoderMixer2E
	.size	_ZTIN11NCoderMixer12CCoderMixer2E, 16

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN11NCoderMixer14CCoderMixer2MTE,@object # @_ZTIN11NCoderMixer14CCoderMixer2MTE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN11NCoderMixer14CCoderMixer2MTE
	.p2align	4
_ZTIN11NCoderMixer14CCoderMixer2MTE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN11NCoderMixer14CCoderMixer2MTE
	.long	0                       # 0x0
	.long	3                       # 0x3
	.quad	_ZTI15ICompressCoder2
	.quad	2                       # 0x2
	.quad	_ZTIN11NCoderMixer12CCoderMixer2E
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTIN11NCoderMixer14CCoderMixer2MTE, 72

	.type	_ZTSN11NCoderMixer7CCoder2E,@object # @_ZTSN11NCoderMixer7CCoder2E
	.globl	_ZTSN11NCoderMixer7CCoder2E
	.p2align	4
_ZTSN11NCoderMixer7CCoder2E:
	.asciz	"N11NCoderMixer7CCoder2E"
	.size	_ZTSN11NCoderMixer7CCoder2E, 24

	.type	_ZTSN11NCoderMixer11CCoderInfo2E,@object # @_ZTSN11NCoderMixer11CCoderInfo2E
	.section	.rodata._ZTSN11NCoderMixer11CCoderInfo2E,"aG",@progbits,_ZTSN11NCoderMixer11CCoderInfo2E,comdat
	.weak	_ZTSN11NCoderMixer11CCoderInfo2E
	.p2align	4
_ZTSN11NCoderMixer11CCoderInfo2E:
	.asciz	"N11NCoderMixer11CCoderInfo2E"
	.size	_ZTSN11NCoderMixer11CCoderInfo2E, 29

	.type	_ZTIN11NCoderMixer11CCoderInfo2E,@object # @_ZTIN11NCoderMixer11CCoderInfo2E
	.section	.rodata._ZTIN11NCoderMixer11CCoderInfo2E,"aG",@progbits,_ZTIN11NCoderMixer11CCoderInfo2E,comdat
	.weak	_ZTIN11NCoderMixer11CCoderInfo2E
	.p2align	3
_ZTIN11NCoderMixer11CCoderInfo2E:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN11NCoderMixer11CCoderInfo2E
	.size	_ZTIN11NCoderMixer11CCoderInfo2E, 16

	.type	_ZTS11CVirtThread,@object # @_ZTS11CVirtThread
	.section	.rodata._ZTS11CVirtThread,"aG",@progbits,_ZTS11CVirtThread,comdat
	.weak	_ZTS11CVirtThread
_ZTS11CVirtThread:
	.asciz	"11CVirtThread"
	.size	_ZTS11CVirtThread, 14

	.type	_ZTI11CVirtThread,@object # @_ZTI11CVirtThread
	.section	.rodata._ZTI11CVirtThread,"aG",@progbits,_ZTI11CVirtThread,comdat
	.weak	_ZTI11CVirtThread
	.p2align	3
_ZTI11CVirtThread:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS11CVirtThread
	.size	_ZTI11CVirtThread, 16

	.type	_ZTIN11NCoderMixer7CCoder2E,@object # @_ZTIN11NCoderMixer7CCoder2E
	.section	.rodata,"a",@progbits
	.globl	_ZTIN11NCoderMixer7CCoder2E
	.p2align	4
_ZTIN11NCoderMixer7CCoder2E:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN11NCoderMixer7CCoder2E
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTIN11NCoderMixer11CCoderInfo2E
	.quad	61442                   # 0xf002
	.quad	_ZTI11CVirtThread
	.quad	2                       # 0x2
	.size	_ZTIN11NCoderMixer7CCoder2E, 56

	.type	_ZTV11CVirtThread,@object # @_ZTV11CVirtThread
	.section	.rodata._ZTV11CVirtThread,"aG",@progbits,_ZTV11CVirtThread,comdat
	.weak	_ZTV11CVirtThread
	.p2align	3
_ZTV11CVirtThread:
	.quad	0
	.quad	_ZTI11CVirtThread
	.quad	__cxa_pure_virtual
	.size	_ZTV11CVirtThread, 24

	.type	_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE,@object # @_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE
	.section	.rodata._ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE,"aG",@progbits,_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE,comdat
	.weak	_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE
	.p2align	3
_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE:
	.quad	0
	.quad	_ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE
	.quad	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.size	_ZTVN8NWindows16NSynchronization21CManualResetEventWFMOE, 24

	.type	_ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE,@object # @_ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE:
	.asciz	"N8NWindows16NSynchronization21CManualResetEventWFMOE"
	.size	_ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE, 53

	.type	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE:
	.asciz	"N8NWindows16NSynchronization14CBaseEventWFMOE"
	.size	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE, 46

	.type	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,@object # @_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE:
	.asciz	"N8NWindows16NSynchronization15CBaseHandleWFMOE"
	.size	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE, 47

	.type	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,@object # @_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.p2align	3
_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.size	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE, 16

	.type	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	4
_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.quad	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.size	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE, 24

	.type	_ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE,@object # @_ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE
	.p2align	4
_ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization21CManualResetEventWFMOE
	.quad	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.size	_ZTIN8NWindows16NSynchronization21CManualResetEventWFMOE, 24

	.type	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	3
_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE:
	.quad	0
	.quad	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.quad	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.size	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE, 24

	.type	_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE,@object # @_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE
	.section	.rodata._ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE,"aG",@progbits,_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE,comdat
	.weak	_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE
	.p2align	3
_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE
	.quad	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev
	.quad	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev
	.quad	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii
	.size	_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE, 40

	.type	_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE,@object # @_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE
	.section	.rodata._ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE,"aG",@progbits,_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE,comdat
	.weak	_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE
	.p2align	4
_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE:
	.asciz	"13CObjectVectorIN11NCoderMixer7CCoder2EE"
	.size	_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE, 41

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE,@object # @_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE
	.section	.rodata._ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE,"aG",@progbits,_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE,comdat
	.weak	_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE
	.p2align	4
_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE, 24

	.type	_ZTV13CObjectVectorI13CStreamBinderE,@object # @_ZTV13CObjectVectorI13CStreamBinderE
	.section	.rodata._ZTV13CObjectVectorI13CStreamBinderE,"aG",@progbits,_ZTV13CObjectVectorI13CStreamBinderE,comdat
	.weak	_ZTV13CObjectVectorI13CStreamBinderE
	.p2align	3
_ZTV13CObjectVectorI13CStreamBinderE:
	.quad	0
	.quad	_ZTI13CObjectVectorI13CStreamBinderE
	.quad	_ZN13CObjectVectorI13CStreamBinderED2Ev
	.quad	_ZN13CObjectVectorI13CStreamBinderED0Ev
	.quad	_ZN13CObjectVectorI13CStreamBinderE6DeleteEii
	.size	_ZTV13CObjectVectorI13CStreamBinderE, 40

	.type	_ZTS13CObjectVectorI13CStreamBinderE,@object # @_ZTS13CObjectVectorI13CStreamBinderE
	.section	.rodata._ZTS13CObjectVectorI13CStreamBinderE,"aG",@progbits,_ZTS13CObjectVectorI13CStreamBinderE,comdat
	.weak	_ZTS13CObjectVectorI13CStreamBinderE
	.p2align	4
_ZTS13CObjectVectorI13CStreamBinderE:
	.asciz	"13CObjectVectorI13CStreamBinderE"
	.size	_ZTS13CObjectVectorI13CStreamBinderE, 33

	.type	_ZTI13CObjectVectorI13CStreamBinderE,@object # @_ZTI13CObjectVectorI13CStreamBinderE
	.section	.rodata._ZTI13CObjectVectorI13CStreamBinderE,"aG",@progbits,_ZTI13CObjectVectorI13CStreamBinderE,comdat
	.weak	_ZTI13CObjectVectorI13CStreamBinderE
	.p2align	4
_ZTI13CObjectVectorI13CStreamBinderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI13CStreamBinderE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI13CStreamBinderE, 24

	.type	_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,@object # @_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.section	.rodata._ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,"aG",@progbits,_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,comdat
	.weak	_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.p2align	3
_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.quad	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii
	.size	_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE, 40

	.type	_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,@object # @_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.section	.rodata._ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,"aG",@progbits,_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,comdat
	.weak	_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.p2align	4
_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE:
	.asciz	"13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE"
	.size	_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE, 51

	.type	_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,@object # @_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.section	.rodata._ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,"aG",@progbits,_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,comdat
	.weak	_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.p2align	4
_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE, 24

	.type	_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,@object # @_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.section	.rodata._ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,"aG",@progbits,_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,comdat
	.weak	_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.p2align	3
_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.quad	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii
	.size	_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE, 40

	.type	_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,@object # @_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.section	.rodata._ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,"aG",@progbits,_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,comdat
	.weak	_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.p2align	4
_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE:
	.asciz	"13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE"
	.size	_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE, 52

	.type	_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,@object # @_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.section	.rodata._ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,"aG",@progbits,_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,comdat
	.weak	_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.p2align	4
_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE, 24

	.type	_ZTV13CRecordVectorIP19ISequentialInStreamE,@object # @_ZTV13CRecordVectorIP19ISequentialInStreamE
	.section	.rodata._ZTV13CRecordVectorIP19ISequentialInStreamE,"aG",@progbits,_ZTV13CRecordVectorIP19ISequentialInStreamE,comdat
	.weak	_ZTV13CRecordVectorIP19ISequentialInStreamE
	.p2align	3
_ZTV13CRecordVectorIP19ISequentialInStreamE:
	.quad	0
	.quad	_ZTI13CRecordVectorIP19ISequentialInStreamE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIP19ISequentialInStreamED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIP19ISequentialInStreamE, 40

	.type	_ZTS13CRecordVectorIP19ISequentialInStreamE,@object # @_ZTS13CRecordVectorIP19ISequentialInStreamE
	.section	.rodata._ZTS13CRecordVectorIP19ISequentialInStreamE,"aG",@progbits,_ZTS13CRecordVectorIP19ISequentialInStreamE,comdat
	.weak	_ZTS13CRecordVectorIP19ISequentialInStreamE
	.p2align	4
_ZTS13CRecordVectorIP19ISequentialInStreamE:
	.asciz	"13CRecordVectorIP19ISequentialInStreamE"
	.size	_ZTS13CRecordVectorIP19ISequentialInStreamE, 40

	.type	_ZTI13CRecordVectorIP19ISequentialInStreamE,@object # @_ZTI13CRecordVectorIP19ISequentialInStreamE
	.section	.rodata._ZTI13CRecordVectorIP19ISequentialInStreamE,"aG",@progbits,_ZTI13CRecordVectorIP19ISequentialInStreamE,comdat
	.weak	_ZTI13CRecordVectorIP19ISequentialInStreamE
	.p2align	4
_ZTI13CRecordVectorIP19ISequentialInStreamE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIP19ISequentialInStreamE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIP19ISequentialInStreamE, 24

	.type	_ZTV13CRecordVectorIP20ISequentialOutStreamE,@object # @_ZTV13CRecordVectorIP20ISequentialOutStreamE
	.section	.rodata._ZTV13CRecordVectorIP20ISequentialOutStreamE,"aG",@progbits,_ZTV13CRecordVectorIP20ISequentialOutStreamE,comdat
	.weak	_ZTV13CRecordVectorIP20ISequentialOutStreamE
	.p2align	3
_ZTV13CRecordVectorIP20ISequentialOutStreamE:
	.quad	0
	.quad	_ZTI13CRecordVectorIP20ISequentialOutStreamE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIP20ISequentialOutStreamE, 40

	.type	_ZTS13CRecordVectorIP20ISequentialOutStreamE,@object # @_ZTS13CRecordVectorIP20ISequentialOutStreamE
	.section	.rodata._ZTS13CRecordVectorIP20ISequentialOutStreamE,"aG",@progbits,_ZTS13CRecordVectorIP20ISequentialOutStreamE,comdat
	.weak	_ZTS13CRecordVectorIP20ISequentialOutStreamE
	.p2align	4
_ZTS13CRecordVectorIP20ISequentialOutStreamE:
	.asciz	"13CRecordVectorIP20ISequentialOutStreamE"
	.size	_ZTS13CRecordVectorIP20ISequentialOutStreamE, 41

	.type	_ZTI13CRecordVectorIP20ISequentialOutStreamE,@object # @_ZTI13CRecordVectorIP20ISequentialOutStreamE
	.section	.rodata._ZTI13CRecordVectorIP20ISequentialOutStreamE,"aG",@progbits,_ZTI13CRecordVectorIP20ISequentialOutStreamE,comdat
	.weak	_ZTI13CRecordVectorIP20ISequentialOutStreamE
	.p2align	4
_ZTI13CRecordVectorIP20ISequentialOutStreamE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIP20ISequentialOutStreamE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIP20ISequentialOutStreamE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24

	.type	_ZTV13CRecordVectorIPKyE,@object # @_ZTV13CRecordVectorIPKyE
	.section	.rodata._ZTV13CRecordVectorIPKyE,"aG",@progbits,_ZTV13CRecordVectorIPKyE,comdat
	.weak	_ZTV13CRecordVectorIPKyE
	.p2align	3
_ZTV13CRecordVectorIPKyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIPKyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIPKyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIPKyE, 40

	.type	_ZTS13CRecordVectorIPKyE,@object # @_ZTS13CRecordVectorIPKyE
	.section	.rodata._ZTS13CRecordVectorIPKyE,"aG",@progbits,_ZTS13CRecordVectorIPKyE,comdat
	.weak	_ZTS13CRecordVectorIPKyE
	.p2align	4
_ZTS13CRecordVectorIPKyE:
	.asciz	"13CRecordVectorIPKyE"
	.size	_ZTS13CRecordVectorIPKyE, 21

	.type	_ZTI13CRecordVectorIPKyE,@object # @_ZTI13CRecordVectorIPKyE
	.section	.rodata._ZTI13CRecordVectorIPKyE,"aG",@progbits,_ZTI13CRecordVectorIPKyE,comdat
	.weak	_ZTI13CRecordVectorIPKyE
	.p2align	4
_ZTI13CRecordVectorIPKyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPKyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPKyE, 24


	.globl	_ZN11NCoderMixer7CCoder2C1Ejj
	.type	_ZN11NCoderMixer7CCoder2C1Ejj,@function
_ZN11NCoderMixer7CCoder2C1Ejj = _ZN11NCoderMixer7CCoder2C2Ejj
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
