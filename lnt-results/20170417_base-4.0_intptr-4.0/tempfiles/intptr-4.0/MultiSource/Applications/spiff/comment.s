	.text
	.file	"comment.bc"
	.globl	W_addcom
	.p2align	4, 0x90
	.type	W_addcom,@function
W_addcom:                               # @W_addcom
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	%rbx, (%rsp)
	movb	(%rbx), %al
	cmpb	_W_bolchar(%rip), %al
	jne	.LBB0_5
# BB#1:
	movl	_W_nextbol(%rip), %eax
	cmpl	$20, %eax
	jl	.LBB0_3
# BB#2:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	movl	_W_nextbol(%rip), %eax
.LBB0_3:
	incq	%rbx
	movq	%rbx, (%rsp)
	cltq
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	leaq	_W_bols(%rax), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	S_wordcpy
	movq	%rsp, %rdi
	xorl	%eax, %eax
	callq	S_nextword
	movq	(%rsp), %rsi
	movb	(%rsi), %al
	movslq	_W_nextbol(%rip), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$4, %rcx
	leaq	_W_bols+16(%rcx), %rdi
	cmpb	_W_eolchar(%rip), %al
	jne	.LBB0_9
# BB#4:
	movw	$10, (%rdi)
	jmp	.LBB0_10
.LBB0_5:
	movl	_W_nextcom(%rip), %eax
	cmpl	$20, %eax
	jl	.LBB0_7
# BB#6:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	movl	_W_nextcom(%rip), %eax
.LBB0_7:
	cltq
	imulq	$52, %rax, %rax
	leaq	_W_coms(%rax), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	S_wordcpy
	movq	%rsp, %rdi
	xorl	%eax, %eax
	callq	S_nextword
	movq	(%rsp), %rsi
	movb	(%rsi), %al
	movslq	_W_nextbol(%rip), %rcx
	imulq	$52, %rcx, %rcx
	leaq	_W_coms+16(%rcx), %rdi
	cmpb	_W_eolchar(%rip), %al
	jne	.LBB0_12
# BB#8:
	movw	$10, (%rdi)
	jmp	.LBB0_13
.LBB0_9:
	xorl	%eax, %eax
	callq	S_wordcpy
.LBB0_10:
	movq	%rsp, %rdi
	xorl	%eax, %eax
	callq	S_nextword
	movslq	_W_nextbol(%rip), %rax
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	leaq	_W_bols+32(%rax), %rdi
	movq	(%rsp), %rsi
	xorl	%eax, %eax
	callq	S_wordcpy
	testl	%ebp, %ebp
	je	.LBB0_14
# BB#11:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	Z_complain
.LBB0_14:
	movl	$_W_nextbol, %ebx
	jmp	.LBB0_15
.LBB0_12:
	xorl	%eax, %eax
	callq	S_wordcpy
.LBB0_13:
	movq	%rsp, %rdi
	xorl	%eax, %eax
	callq	S_nextword
	movl	$_W_nextcom, %ebx
	movslq	_W_nextcom(%rip), %rax
	imulq	$52, %rax, %rax
	leaq	_W_coms+32(%rax), %rdi
	movq	(%rsp), %rsi
	xorl	%eax, %eax
	callq	S_wordcpy
	movslq	_W_nextcom(%rip), %rax
	imulq	$52, %rax, %rax
	movl	%ebp, _W_coms+48(%rax)
.LBB0_15:
	incl	(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	W_addcom, .Lfunc_end0-W_addcom
	.cfi_endproc

	.globl	W_clearcoms
	.p2align	4, 0x90
	.type	W_clearcoms,@function
W_clearcoms:                            # @W_clearcoms
	.cfi_startproc
# BB#0:
	movl	$0, _W_nextcom(%rip)
	movl	$0, _W_nextbol(%rip)
	retq
.Lfunc_end1:
	.size	W_clearcoms, .Lfunc_end1-W_clearcoms
	.cfi_endproc

	.globl	W_addlit
	.p2align	4, 0x90
	.type	W_addlit,@function
W_addlit:                               # @W_addlit
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 32
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	%rbx, 8(%rsp)
	movl	_W_nextlit(%rip), %eax
	cmpl	$20, %eax
	jl	.LBB2_2
# BB#1:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	movl	_W_nextlit(%rip), %eax
.LBB2_2:
	cltq
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	leaq	_W_lits(%rax), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	S_wordcpy
	leaq	8(%rsp), %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	S_nextword
	movslq	_W_nextlit(%rip), %rax
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	leaq	_W_lits+16(%rax), %rdi
	movq	8(%rsp), %rsi
	xorl	%eax, %eax
	callq	S_wordcpy
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	S_nextword
	movslq	_W_nextlit(%rip), %rax
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	leaq	_W_lits+32(%rax), %rdi
	movq	8(%rsp), %rsi
	xorl	%eax, %eax
	callq	S_wordcpy
	incl	_W_nextlit(%rip)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end2:
	.size	W_addlit, .Lfunc_end2-W_addlit
	.cfi_endproc

	.globl	W_clearlits
	.p2align	4, 0x90
	.type	W_clearlits,@function
W_clearlits:                            # @W_clearlits
	.cfi_startproc
# BB#0:
	movl	$0, _W_nextlit(%rip)
	retq
.Lfunc_end3:
	.size	W_clearlits, .Lfunc_end3-W_clearlits
	.cfi_endproc

	.globl	W_isbol
	.p2align	4, 0x90
	.type	W_isbol,@function
W_isbol:                                # @W_isbol
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 48
.Lcfi13:
	.cfi_offset %rbx, -40
.Lcfi14:
	.cfi_offset %r12, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpl	$0, _W_nextbol(%rip)
	jle	.LBB4_1
# BB#3:                                 # %.lr.ph.preheader
	movl	$_W_bols, %r12d
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB4_5
# BB#2:                                 #   in Loop: Header=BB4_4 Depth=1
	incq	%rbx
	movslq	_W_nextbol(%rip), %rax
	addq	$48, %r12
	cmpq	%rax, %rbx
	jl	.LBB4_4
	jmp	.LBB4_6
.LBB4_1:
	xorl	%r14d, %r14d
	jmp	.LBB4_6
.LBB4_5:
	movl	$bol_scratch, %r14d
	movl	$bol_scratch, %edi
	movq	%r12, %rsi
	callq	strcpy
	leaq	16(%r12), %rsi
	movl	$bol_scratch+16, %edi
	callq	strcpy
	addq	$32, %r12
	movl	$bol_scratch+32, %edi
	movq	%r12, %rsi
	callq	strcpy
.LBB4_6:                                # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	W_isbol, .Lfunc_end4-W_isbol
	.cfi_endproc

	.globl	W_is_bol
	.p2align	4, 0x90
	.type	W_is_bol,@function
W_is_bol:                               # @W_is_bol
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 48
.Lcfi22:
	.cfi_offset %rbx, -48
.Lcfi23:
	.cfi_offset %r12, -40
.Lcfi24:
	.cfi_offset %r13, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpl	$0, _W_nextbol(%rip)
	jle	.LBB5_7
# BB#1:                                 # %.lr.ph
	leaq	32(%r15), %r14
	movl	$_W_bols, %ebx
	leaq	16(%r15), %r12
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	jne	.LBB5_6
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	leaq	16(%rbx), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	S_wordcmp
	testl	%eax, %eax
	jne	.LBB5_6
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	leaq	32(%rbx), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB5_5
	.p2align	4, 0x90
.LBB5_6:                                #   in Loop: Header=BB5_2 Depth=1
	incq	%r13
	movslq	_W_nextbol(%rip), %rax
	addq	$48, %rbx
	cmpq	%rax, %r13
	jl	.LBB5_2
.LBB5_7:
	xorl	%eax, %eax
.LBB5_8:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB5_5:
	movl	$1, %eax
	jmp	.LBB5_8
.Lfunc_end5:
	.size	W_is_bol, .Lfunc_end5-W_is_bol
	.cfi_endproc

	.globl	W_islit
	.p2align	4, 0x90
	.type	W_islit,@function
W_islit:                                # @W_islit
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 48
.Lcfi32:
	.cfi_offset %rbx, -40
.Lcfi33:
	.cfi_offset %r12, -32
.Lcfi34:
	.cfi_offset %r14, -24
.Lcfi35:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpl	$0, _W_nextlit(%rip)
	jle	.LBB6_1
# BB#3:                                 # %.lr.ph.preheader
	movl	$_W_lits, %r12d
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB6_5
# BB#2:                                 #   in Loop: Header=BB6_4 Depth=1
	incq	%rbx
	movslq	_W_nextlit(%rip), %rax
	addq	$48, %r12
	cmpq	%rax, %rbx
	jl	.LBB6_4
	jmp	.LBB6_6
.LBB6_1:
	xorl	%r14d, %r14d
	jmp	.LBB6_6
.LBB6_5:
	movl	$lit_scratch, %r14d
	movl	$lit_scratch, %edi
	movq	%r12, %rsi
	callq	strcpy
	leaq	16(%r12), %rsi
	movl	$lit_scratch+16, %edi
	callq	strcpy
	addq	$32, %r12
	movl	$lit_scratch+32, %edi
	movq	%r12, %rsi
	callq	strcpy
.LBB6_6:                                # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	W_islit, .Lfunc_end6-W_islit
	.cfi_endproc

	.globl	W_is_lit
	.p2align	4, 0x90
	.type	W_is_lit,@function
W_is_lit:                               # @W_is_lit
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 48
.Lcfi41:
	.cfi_offset %rbx, -48
.Lcfi42:
	.cfi_offset %r12, -40
.Lcfi43:
	.cfi_offset %r13, -32
.Lcfi44:
	.cfi_offset %r14, -24
.Lcfi45:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpl	$0, _W_nextlit(%rip)
	jle	.LBB7_7
# BB#1:                                 # %.lr.ph
	leaq	32(%r15), %r14
	movl	$_W_lits, %ebx
	leaq	16(%r15), %r12
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	jne	.LBB7_6
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	leaq	16(%rbx), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	S_wordcmp
	testl	%eax, %eax
	jne	.LBB7_6
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=1
	leaq	32(%rbx), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB7_5
	.p2align	4, 0x90
.LBB7_6:                                #   in Loop: Header=BB7_2 Depth=1
	incq	%r13
	movslq	_W_nextlit(%rip), %rax
	addq	$48, %rbx
	cmpq	%rax, %r13
	jl	.LBB7_2
.LBB7_7:
	xorl	%eax, %eax
.LBB7_8:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB7_5:
	movl	$1, %eax
	jmp	.LBB7_8
.Lfunc_end7:
	.size	W_is_lit, .Lfunc_end7-W_is_lit
	.cfi_endproc

	.globl	W_iscom
	.p2align	4, 0x90
	.type	W_iscom,@function
W_iscom:                                # @W_iscom
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 48
.Lcfi51:
	.cfi_offset %rbx, -48
.Lcfi52:
	.cfi_offset %r12, -40
.Lcfi53:
	.cfi_offset %r13, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpl	$0, _W_nextcom(%rip)
	jle	.LBB8_1
# BB#3:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	movl	$_W_coms+32, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	-32(%r12), %r13
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB8_5
# BB#2:                                 #   in Loop: Header=BB8_4 Depth=1
	incq	%rbx
	movslq	_W_nextcom(%rip), %rax
	addq	$52, %r12
	cmpq	%rax, %rbx
	jl	.LBB8_4
	jmp	.LBB8_6
.LBB8_1:
	xorl	%r14d, %r14d
	jmp	.LBB8_6
.LBB8_5:
	movl	$com_scratch, %r14d
	movl	$com_scratch, %edi
	movq	%r13, %rsi
	callq	strcpy
	leaq	-16(%r12), %rsi
	movl	$com_scratch+16, %edi
	callq	strcpy
	movl	$com_scratch+32, %edi
	movq	%r12, %rsi
	callq	strcpy
	movl	16(%r12), %eax
	movl	%eax, com_scratch+48(%rip)
.LBB8_6:                                # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	W_iscom, .Lfunc_end8-W_iscom
	.cfi_endproc

	.globl	W_is_com
	.p2align	4, 0x90
	.type	W_is_com,@function
W_is_com:                               # @W_is_com
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 48
.Lcfi61:
	.cfi_offset %rbx, -48
.Lcfi62:
	.cfi_offset %r12, -40
.Lcfi63:
	.cfi_offset %r13, -32
.Lcfi64:
	.cfi_offset %r14, -24
.Lcfi65:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	cmpl	$0, _W_nextcom(%rip)
	jle	.LBB9_8
# BB#1:                                 # %.lr.ph
	leaq	16(%r12), %r14
	leaq	32(%r12), %r15
	movl	$_W_coms+32, %ebx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	leaq	-32(%rbx), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	S_wordcmp
	testl	%eax, %eax
	jne	.LBB9_7
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	leaq	-16(%rbx), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	S_wordcmp
	testl	%eax, %eax
	jne	.LBB9_7
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=1
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	jne	.LBB9_7
# BB#5:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	48(%r12), %eax
	cmpl	16(%rbx), %eax
	je	.LBB9_6
	.p2align	4, 0x90
.LBB9_7:                                #   in Loop: Header=BB9_2 Depth=1
	incq	%r13
	movslq	_W_nextcom(%rip), %rax
	addq	$52, %rbx
	cmpq	%rax, %r13
	jl	.LBB9_2
.LBB9_8:
	xorl	%eax, %eax
.LBB9_9:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB9_6:
	movl	$1, %eax
	jmp	.LBB9_9
.Lfunc_end9:
	.size	W_is_com, .Lfunc_end9-W_is_com
	.cfi_endproc

	.globl	W_is_nesting
	.p2align	4, 0x90
	.type	W_is_nesting,@function
W_is_nesting:                           # @W_is_nesting
	.cfi_startproc
# BB#0:
	movl	48(%rdi), %eax
	retq
.Lfunc_end10:
	.size	W_is_nesting, .Lfunc_end10-W_is_nesting
	.cfi_endproc

	.type	_W_bolchar,@object      # @_W_bolchar
	.data
	.globl	_W_bolchar
_W_bolchar:
	.byte	94                      # 0x5e
	.size	_W_bolchar, 1

	.type	_W_eolchar,@object      # @_W_eolchar
	.globl	_W_eolchar
_W_eolchar:
	.byte	36                      # 0x24
	.size	_W_eolchar, 1

	.type	_W_nextbol,@object      # @_W_nextbol
	.local	_W_nextbol
	.comm	_W_nextbol,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"too many beginning of line comment delimiter sets"
	.size	.L.str, 50

	.type	_W_bols,@object         # @_W_bols
	.comm	_W_bols,960,16
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"begining of line comment won't nest"
	.size	.L.str.2, 36

	.type	_W_nextcom,@object      # @_W_nextcom
	.local	_W_nextcom
	.comm	_W_nextcom,4,4
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"too many comment delimiter sets"
	.size	.L.str.3, 32

	.type	_W_coms,@object         # @_W_coms
	.comm	_W_coms,1040,16
	.type	_W_nextlit,@object      # @_W_nextlit
	.local	_W_nextlit
	.comm	_W_nextlit,4,4
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"too many literal delimiter sets"
	.size	.L.str.4, 32

	.type	_W_lits,@object         # @_W_lits
	.comm	_W_lits,960,16
	.type	bol_scratch,@object     # @bol_scratch
	.local	bol_scratch
	.comm	bol_scratch,48,1
	.type	lit_scratch,@object     # @lit_scratch
	.local	lit_scratch
	.comm	lit_scratch,48,1
	.type	com_scratch,@object     # @com_scratch
	.local	com_scratch
	.comm	com_scratch,52,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
