	.text
	.file	"findsavr.bc"
	.globl	findsaver
	.p2align	4, 0x90
	.type	findsaver,@function
findsaver:                              # @findsaver
	.cfi_startproc
# BB#0:                                 # %.preheader29
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movl	$-1, (%rdi)
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movl	$-1, (%rsi)
	movl	$-1, (%r12)
	xorl	%r13d, %r13d
	leaq	12(%rsp), %rbp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_2 Depth 2
	movq	%r13, %rbx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_2:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	p(%rbx), %eax
	cmpl	mymove(%rip), %eax
	jne	.LBB0_7
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=2
	cmpb	$1, l(%rbx)
	jne	.LBB0_7
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=2
	callq	initmark
	movl	$1, %r9d
	movl	%r14d, %edi
	movl	%r15d, %esi
	leaq	20(%rsp), %rdx
	leaq	16(%rsp), %rcx
	movq	%rbp, %r8
	callq	findnextmove
	testl	%eax, %eax
	je	.LBB0_7
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=2
	movl	12(%rsp), %eax
	cmpl	(%r12), %eax
	jle	.LBB0_7
# BB#6:                                 #   in Loop: Header=BB0_2 Depth=2
	movl	%eax, (%r12)
	movl	20(%rsp), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	16(%rsp), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_2 Depth=2
	incq	%r15
	incq	%rbx
	cmpq	$19, %r15
	jne	.LBB0_2
# BB#8:                                 #   in Loop: Header=BB0_1 Depth=1
	incq	%r14
	addq	$19, %r13
	cmpq	$19, %r14
	jne	.LBB0_1
# BB#9:                                 # %.preheader.1.preheader
	xorl	%r13d, %r13d
	leaq	12(%rsp), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_10:                               # %.preheader.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_11 Depth 2
	movq	%r13, %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_11:                               #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	p(%r15), %eax
	cmpl	mymove(%rip), %eax
	jne	.LBB0_16
# BB#12:                                #   in Loop: Header=BB0_11 Depth=2
	cmpb	$2, l(%r15)
	jne	.LBB0_16
# BB#13:                                #   in Loop: Header=BB0_11 Depth=2
	callq	initmark
	movl	$2, %r9d
	movl	%ebp, %edi
	movl	%ebx, %esi
	leaq	20(%rsp), %rdx
	leaq	16(%rsp), %rcx
	movq	%r14, %r8
	callq	findnextmove
	testl	%eax, %eax
	je	.LBB0_16
# BB#14:                                #   in Loop: Header=BB0_11 Depth=2
	movl	12(%rsp), %eax
	cmpl	(%r12), %eax
	jle	.LBB0_16
# BB#15:                                #   in Loop: Header=BB0_11 Depth=2
	movl	%eax, (%r12)
	movl	20(%rsp), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	16(%rsp), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	.p2align	4, 0x90
.LBB0_16:                               #   in Loop: Header=BB0_11 Depth=2
	incq	%rbx
	incq	%r15
	cmpq	$19, %rbx
	jne	.LBB0_11
# BB#17:                                #   in Loop: Header=BB0_10 Depth=1
	incq	%rbp
	addq	$19, %r13
	cmpq	$19, %rbp
	jne	.LBB0_10
# BB#18:                                # %.preheader.2.preheader
	xorl	%r13d, %r13d
	leaq	12(%rsp), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_19:                               # %.preheader.2
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_20 Depth 2
	movq	%r13, %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_20:                               #   Parent Loop BB0_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	p(%r15), %eax
	cmpl	mymove(%rip), %eax
	jne	.LBB0_25
# BB#21:                                #   in Loop: Header=BB0_20 Depth=2
	cmpb	$3, l(%r15)
	jne	.LBB0_25
# BB#22:                                #   in Loop: Header=BB0_20 Depth=2
	callq	initmark
	movl	$3, %r9d
	movl	%ebp, %edi
	movl	%ebx, %esi
	leaq	20(%rsp), %rdx
	leaq	16(%rsp), %rcx
	movq	%r14, %r8
	callq	findnextmove
	testl	%eax, %eax
	je	.LBB0_25
# BB#23:                                #   in Loop: Header=BB0_20 Depth=2
	movl	12(%rsp), %eax
	cmpl	(%r12), %eax
	jle	.LBB0_25
# BB#24:                                #   in Loop: Header=BB0_20 Depth=2
	movl	%eax, (%r12)
	movl	20(%rsp), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	16(%rsp), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	.p2align	4, 0x90
.LBB0_25:                               #   in Loop: Header=BB0_20 Depth=2
	incq	%rbx
	incq	%r15
	cmpq	$19, %rbx
	jne	.LBB0_20
# BB#26:                                #   in Loop: Header=BB0_19 Depth=1
	incq	%rbp
	addq	$19, %r13
	cmpq	$19, %rbp
	jne	.LBB0_19
# BB#27:
	xorl	%eax, %eax
	cmpl	$0, (%r12)
	setg	%al
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	findsaver, .Lfunc_end0-findsaver
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
