	.text
	.file	"Lzma2Decoder.bc"
	.globl	_ZN9NCompress6NLzma28CDecoderC2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CDecoderC2Ev,@function
_ZN9NCompress6NLzma28CDecoderC2Ev:      # @_ZN9NCompress6NLzma28CDecoderC2Ev
	.cfi_startproc
# BB#0:
	movl	$0, 48(%rdi)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rdi)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+256, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rdi)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+392, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+328, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 32(%rdi)
	movb	$0, 248(%rdi)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 56(%rdi)
	movdqu	%xmm0, 96(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN9NCompress6NLzma28CDecoderC2Ev, .Lfunc_end0-_ZN9NCompress6NLzma28CDecoderC2Ev
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN9NCompress6NLzma28CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CDecoderD2Ev,@function
_ZN9NCompress6NLzma28CDecoderD2Ev:      # @_ZN9NCompress6NLzma28CDecoderD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+256, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+392, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+328, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 32(%rbx)
	leaq	80(%rbx), %rdi
.Ltmp0:
	movl	$_ZN9NCompress6NLzma2L7g_AllocE, %esi
	callq	LzmaDec_Free
.Ltmp1:
# BB#1:
	movq	64(%rbx), %rdi
.Ltmp2:
	callq	MyFree
.Ltmp3:
# BB#2:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp8:
	callq	*16(%rax)
.Ltmp9:
.LBB2_4:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_7:
.Ltmp10:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_5:
.Ltmp4:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp5:
	callq	*16(%rax)
.Ltmp6:
.LBB2_8:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit9
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_9:
.Ltmp7:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN9NCompress6NLzma28CDecoderD2Ev, .Lfunc_end2-_ZN9NCompress6NLzma28CDecoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp5-.Ltmp9           #   Call between .Ltmp9 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end2-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn8_N9NCompress6NLzma28CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NLzma28CDecoderD1Ev,@function
_ZThn8_N9NCompress6NLzma28CDecoderD1Ev: # @_ZThn8_N9NCompress6NLzma28CDecoderD1Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+256, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 8(%rdi)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+392, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+328, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 24(%rdi)
	leaq	-8(%rdi), %rbx
	addq	$72, %rdi
.Ltmp11:
	movl	$_ZN9NCompress6NLzma2L7g_AllocE, %esi
	callq	LzmaDec_Free
.Ltmp12:
# BB#1:
	movq	64(%rbx), %rdi
.Ltmp13:
	callq	MyFree
.Ltmp14:
# BB#2:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp19:
	callq	*16(%rax)
.Ltmp20:
.LBB3_4:                                # %_ZN9NCompress6NLzma28CDecoderD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB3_7:
.Ltmp21:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_5:
.Ltmp15:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp16:
	callq	*16(%rax)
.Ltmp17:
.LBB3_8:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit9.i
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_9:
.Ltmp18:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZThn8_N9NCompress6NLzma28CDecoderD1Ev, .Lfunc_end3-_ZThn8_N9NCompress6NLzma28CDecoderD1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp14-.Ltmp11         #   Call between .Ltmp11 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin1   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin1   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp16-.Ltmp20         #   Call between .Ltmp20 and .Ltmp16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	1                       #   On action: 1
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end3-.Ltmp17     #   Call between .Ltmp17 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn16_N9NCompress6NLzma28CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NLzma28CDecoderD1Ev,@function
_ZThn16_N9NCompress6NLzma28CDecoderD1Ev: # @_ZThn16_N9NCompress6NLzma28CDecoderD1Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -16(%rdi)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+256, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rdi)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+392, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+328, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rdi)
	leaq	-16(%rdi), %rbx
	addq	$64, %rdi
.Ltmp22:
	movl	$_ZN9NCompress6NLzma2L7g_AllocE, %esi
	callq	LzmaDec_Free
.Ltmp23:
# BB#1:
	movq	64(%rbx), %rdi
.Ltmp24:
	callq	MyFree
.Ltmp25:
# BB#2:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp30:
	callq	*16(%rax)
.Ltmp31:
.LBB4_4:                                # %_ZN9NCompress6NLzma28CDecoderD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_7:
.Ltmp32:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_5:
.Ltmp26:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp27:
	callq	*16(%rax)
.Ltmp28:
.LBB4_8:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit9.i
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_9:
.Ltmp29:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZThn16_N9NCompress6NLzma28CDecoderD1Ev, .Lfunc_end4-_ZThn16_N9NCompress6NLzma28CDecoderD1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp25-.Ltmp22         #   Call between .Ltmp22 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin2   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin2   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp27-.Ltmp31         #   Call between .Ltmp31 and .Ltmp27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin2   #     jumps to .Ltmp29
	.byte	1                       #   On action: 1
	.long	.Ltmp28-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Lfunc_end4-.Ltmp28     #   Call between .Ltmp28 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn24_N9NCompress6NLzma28CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress6NLzma28CDecoderD1Ev,@function
_ZThn24_N9NCompress6NLzma28CDecoderD1Ev: # @_ZThn24_N9NCompress6NLzma28CDecoderD1Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -24(%rdi)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+256, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+392, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+328, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 8(%rdi)
	leaq	-24(%rdi), %rbx
	addq	$56, %rdi
.Ltmp33:
	movl	$_ZN9NCompress6NLzma2L7g_AllocE, %esi
	callq	LzmaDec_Free
.Ltmp34:
# BB#1:
	movq	64(%rbx), %rdi
.Ltmp35:
	callq	MyFree
.Ltmp36:
# BB#2:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp41:
	callq	*16(%rax)
.Ltmp42:
.LBB5_4:                                # %_ZN9NCompress6NLzma28CDecoderD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_7:
.Ltmp43:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_5:
.Ltmp37:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp38:
	callq	*16(%rax)
.Ltmp39:
.LBB5_8:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit9.i
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_9:
.Ltmp40:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZThn24_N9NCompress6NLzma28CDecoderD1Ev, .Lfunc_end5-_ZThn24_N9NCompress6NLzma28CDecoderD1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp33-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp36-.Ltmp33         #   Call between .Ltmp33 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin3   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin3   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp38-.Ltmp42         #   Call between .Ltmp42 and .Ltmp38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin3   #     jumps to .Ltmp40
	.byte	1                       #   On action: 1
	.long	.Ltmp39-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Lfunc_end5-.Ltmp39     #   Call between .Ltmp39 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn32_N9NCompress6NLzma28CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress6NLzma28CDecoderD1Ev,@function
_ZThn32_N9NCompress6NLzma28CDecoderD1Ev: # @_ZThn32_N9NCompress6NLzma28CDecoderD1Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %r14, -16
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -32(%rdi)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+256, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -16(%rdi)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+392, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+328, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rdi)
	leaq	-32(%rdi), %rbx
	addq	$48, %rdi
.Ltmp44:
	movl	$_ZN9NCompress6NLzma2L7g_AllocE, %esi
	callq	LzmaDec_Free
.Ltmp45:
# BB#1:
	movq	64(%rbx), %rdi
.Ltmp46:
	callq	MyFree
.Ltmp47:
# BB#2:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp52:
	callq	*16(%rax)
.Ltmp53:
.LBB6_4:                                # %_ZN9NCompress6NLzma28CDecoderD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB6_7:
.Ltmp54:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_5:
.Ltmp48:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp49:
	callq	*16(%rax)
.Ltmp50:
.LBB6_8:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit9.i
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_9:
.Ltmp51:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZThn32_N9NCompress6NLzma28CDecoderD1Ev, .Lfunc_end6-_ZThn32_N9NCompress6NLzma28CDecoderD1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp44-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp47-.Ltmp44         #   Call between .Ltmp44 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin4   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin4   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp49-.Ltmp53         #   Call between .Ltmp53 and .Ltmp49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin4   #     jumps to .Ltmp51
	.byte	1                       #   On action: 1
	.long	.Ltmp50-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Lfunc_end6-.Ltmp50     #   Call between .Ltmp50 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn40_N9NCompress6NLzma28CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn40_N9NCompress6NLzma28CDecoderD1Ev,@function
_ZThn40_N9NCompress6NLzma28CDecoderD1Ev: # @_ZThn40_N9NCompress6NLzma28CDecoderD1Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 32
.Lcfi28:
	.cfi_offset %rbx, -24
.Lcfi29:
	.cfi_offset %r14, -16
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -40(%rdi)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+256, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -24(%rdi)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+392, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+328, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	leaq	-40(%rdi), %rbx
	addq	$40, %rdi
.Ltmp55:
	movl	$_ZN9NCompress6NLzma2L7g_AllocE, %esi
	callq	LzmaDec_Free
.Ltmp56:
# BB#1:
	movq	64(%rbx), %rdi
.Ltmp57:
	callq	MyFree
.Ltmp58:
# BB#2:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp63:
	callq	*16(%rax)
.Ltmp64:
.LBB7_4:                                # %_ZN9NCompress6NLzma28CDecoderD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB7_7:
.Ltmp65:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_5:
.Ltmp59:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp60:
	callq	*16(%rax)
.Ltmp61:
.LBB7_8:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit9.i
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_9:
.Ltmp62:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZThn40_N9NCompress6NLzma28CDecoderD1Ev, .Lfunc_end7-_ZThn40_N9NCompress6NLzma28CDecoderD1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp55-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp58-.Ltmp55         #   Call between .Ltmp55 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin5   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin5   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp60-.Ltmp64         #   Call between .Ltmp64 and .Ltmp60
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin5   #     jumps to .Ltmp62
	.byte	1                       #   On action: 1
	.long	.Ltmp61-.Lfunc_begin5   # >> Call Site 5 <<
	.long	.Lfunc_end7-.Ltmp61     #   Call between .Ltmp61 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress6NLzma28CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CDecoderD0Ev,@function
_ZN9NCompress6NLzma28CDecoderD0Ev:      # @_ZN9NCompress6NLzma28CDecoderD0Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -24
.Lcfi34:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+256, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+392, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NLzma28CDecoderE+328, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 32(%rbx)
	leaq	80(%rbx), %rdi
.Ltmp66:
	movl	$_ZN9NCompress6NLzma2L7g_AllocE, %esi
	callq	LzmaDec_Free
.Ltmp67:
# BB#1:
	movq	64(%rbx), %rdi
.Ltmp68:
	callq	MyFree
.Ltmp69:
# BB#2:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp74:
	callq	*16(%rax)
.Ltmp75:
.LBB8_4:                                # %_ZN9NCompress6NLzma28CDecoderD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB8_8:
.Ltmp76:
	movq	%rax, %r14
	jmp	.LBB8_9
.LBB8_5:
.Ltmp70:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_9
# BB#6:
	movq	(%rdi), %rax
.Ltmp71:
	callq	*16(%rax)
.Ltmp72:
.LBB8_9:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_7:
.Ltmp73:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN9NCompress6NLzma28CDecoderD0Ev, .Lfunc_end8-_ZN9NCompress6NLzma28CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp66-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp69-.Ltmp66         #   Call between .Ltmp66 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin6   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin6   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin6   #     jumps to .Ltmp73
	.byte	1                       #   On action: 1
	.long	.Ltmp72-.Lfunc_begin6   # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp72     #   Call between .Ltmp72 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn8_N9NCompress6NLzma28CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NLzma28CDecoderD0Ev,@function
_ZThn8_N9NCompress6NLzma28CDecoderD0Ev: # @_ZThn8_N9NCompress6NLzma28CDecoderD0Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress6NLzma28CDecoderD0Ev # TAILCALL
.Lfunc_end9:
	.size	_ZThn8_N9NCompress6NLzma28CDecoderD0Ev, .Lfunc_end9-_ZThn8_N9NCompress6NLzma28CDecoderD0Ev
	.cfi_endproc

	.globl	_ZThn16_N9NCompress6NLzma28CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NLzma28CDecoderD0Ev,@function
_ZThn16_N9NCompress6NLzma28CDecoderD0Ev: # @_ZThn16_N9NCompress6NLzma28CDecoderD0Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress6NLzma28CDecoderD0Ev # TAILCALL
.Lfunc_end10:
	.size	_ZThn16_N9NCompress6NLzma28CDecoderD0Ev, .Lfunc_end10-_ZThn16_N9NCompress6NLzma28CDecoderD0Ev
	.cfi_endproc

	.globl	_ZThn24_N9NCompress6NLzma28CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress6NLzma28CDecoderD0Ev,@function
_ZThn24_N9NCompress6NLzma28CDecoderD0Ev: # @_ZThn24_N9NCompress6NLzma28CDecoderD0Ev
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN9NCompress6NLzma28CDecoderD0Ev # TAILCALL
.Lfunc_end11:
	.size	_ZThn24_N9NCompress6NLzma28CDecoderD0Ev, .Lfunc_end11-_ZThn24_N9NCompress6NLzma28CDecoderD0Ev
	.cfi_endproc

	.globl	_ZThn32_N9NCompress6NLzma28CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress6NLzma28CDecoderD0Ev,@function
_ZThn32_N9NCompress6NLzma28CDecoderD0Ev: # @_ZThn32_N9NCompress6NLzma28CDecoderD0Ev
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN9NCompress6NLzma28CDecoderD0Ev # TAILCALL
.Lfunc_end12:
	.size	_ZThn32_N9NCompress6NLzma28CDecoderD0Ev, .Lfunc_end12-_ZThn32_N9NCompress6NLzma28CDecoderD0Ev
	.cfi_endproc

	.globl	_ZThn40_N9NCompress6NLzma28CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn40_N9NCompress6NLzma28CDecoderD0Ev,@function
_ZThn40_N9NCompress6NLzma28CDecoderD0Ev: # @_ZThn40_N9NCompress6NLzma28CDecoderD0Ev
	.cfi_startproc
# BB#0:
	addq	$-40, %rdi
	jmp	_ZN9NCompress6NLzma28CDecoderD0Ev # TAILCALL
.Lfunc_end13:
	.size	_ZThn40_N9NCompress6NLzma28CDecoderD0Ev, .Lfunc_end13-_ZThn40_N9NCompress6NLzma28CDecoderD0Ev
	.cfi_endproc

	.globl	_ZN9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj,@function
_ZN9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj: # @_ZN9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 16
.Lcfi36:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$4, %eax
	cmpl	$1, %edx
	jne	.LBB14_8
# BB#1:
	leaq	80(%rbx), %rdi
	movzbl	(%rsi), %esi
	movl	$_ZN9NCompress6NLzma2L7g_AllocE, %edx
	callq	Lzma2Dec_Allocate
	movl	%eax, %ecx
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmpl	$5, %ecx
	ja	.LBB14_8
# BB#2:                                 # %_ZL13SResToHRESULTi.exit
	testl	%ecx, %ecx
	je	.LBB14_4
# BB#3:
	movslq	%ecx, %rax
	movl	.Lswitch.table(,%rax,4), %eax
	popq	%rbx
	retq
.LBB14_4:
	cmpq	$0, 64(%rbx)
	jne	.LBB14_7
# BB#5:
	movl	$1048576, %edi          # imm = 0x100000
	callq	MyAlloc
	movq	%rax, 64(%rbx)
	testq	%rax, %rax
	je	.LBB14_6
.LBB14_7:
	xorl	%eax, %eax
.LBB14_8:                               # %_ZL13SResToHRESULTi.exit.thread
	popq	%rbx
	retq
.LBB14_6:
	movl	$-2147024882, %eax      # imm = 0x8007000E
	popq	%rbx
	retq
.Lfunc_end14:
	.size	_ZN9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj, .Lfunc_end14-_ZN9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.globl	_ZThn8_N9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj,@function
_ZThn8_N9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj: # @_ZThn8_N9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 16
.Lcfi38:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$4, %eax
	cmpl	$1, %edx
	jne	.LBB15_8
# BB#1:
	addq	$-8, %rbx
	leaq	80(%rbx), %rdi
	movzbl	(%rsi), %esi
	movl	$_ZN9NCompress6NLzma2L7g_AllocE, %edx
	callq	Lzma2Dec_Allocate
	movl	%eax, %ecx
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmpl	$5, %ecx
	ja	.LBB15_8
# BB#2:                                 # %_ZL13SResToHRESULTi.exit.i
	testl	%ecx, %ecx
	je	.LBB15_4
# BB#3:
	movslq	%ecx, %rax
	movl	.Lswitch.table(,%rax,4), %eax
	popq	%rbx
	retq
.LBB15_4:
	cmpq	$0, 64(%rbx)
	jne	.LBB15_7
# BB#5:
	movl	$1048576, %edi          # imm = 0x100000
	callq	MyAlloc
	movq	%rax, 64(%rbx)
	testq	%rax, %rax
	je	.LBB15_6
.LBB15_7:
	xorl	%eax, %eax
.LBB15_8:                               # %_ZN9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj.exit
	popq	%rbx
	retq
.LBB15_6:
	movl	$-2147024882, %eax      # imm = 0x8007000E
	popq	%rbx
	retq
.Lfunc_end15:
	.size	_ZThn8_N9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj, .Lfunc_end15-_ZThn8_N9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.globl	_ZN9NCompress6NLzma28CDecoder24GetInStreamProcessedSizeEPy
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CDecoder24GetInStreamProcessedSizeEPy,@function
_ZN9NCompress6NLzma28CDecoder24GetInStreamProcessedSizeEPy: # @_ZN9NCompress6NLzma28CDecoder24GetInStreamProcessedSizeEPy
	.cfi_startproc
# BB#0:
	movq	264(%rdi), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end16:
	.size	_ZN9NCompress6NLzma28CDecoder24GetInStreamProcessedSizeEPy, .Lfunc_end16-_ZN9NCompress6NLzma28CDecoder24GetInStreamProcessedSizeEPy
	.cfi_endproc

	.globl	_ZThn16_N9NCompress6NLzma28CDecoder24GetInStreamProcessedSizeEPy
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NLzma28CDecoder24GetInStreamProcessedSizeEPy,@function
_ZThn16_N9NCompress6NLzma28CDecoder24GetInStreamProcessedSizeEPy: # @_ZThn16_N9NCompress6NLzma28CDecoder24GetInStreamProcessedSizeEPy
	.cfi_startproc
# BB#0:
	movq	248(%rdi), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end17:
	.size	_ZThn16_N9NCompress6NLzma28CDecoder24GetInStreamProcessedSizeEPy, .Lfunc_end17-_ZThn16_N9NCompress6NLzma28CDecoder24GetInStreamProcessedSizeEPy
	.cfi_endproc

	.globl	_ZN9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream,@function
_ZN9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream: # @_ZN9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 32
.Lcfi42:
	.cfi_offset %rbx, -24
.Lcfi43:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB18_2
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB18_2:
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB18_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB18_4:                               # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	%rbx, 56(%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	_ZN9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream, .Lfunc_end18-_ZN9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZThn24_N9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream,@function
_ZThn24_N9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream: # @_ZThn24_N9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -24
.Lcfi48:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB19_2
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB19_2:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB19_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB19_4:                               # %_ZN9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream.exit
	movq	%rbx, 32(%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end19:
	.size	_ZThn24_N9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream, .Lfunc_end19-_ZThn24_N9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN9NCompress6NLzma28CDecoder15ReleaseInStreamEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CDecoder15ReleaseInStreamEv,@function
_ZN9NCompress6NLzma28CDecoder15ReleaseInStreamEv: # @_ZN9NCompress6NLzma28CDecoder15ReleaseInStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 16
.Lcfi50:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB20_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 56(%rbx)
.LBB20_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamE7ReleaseEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end20:
	.size	_ZN9NCompress6NLzma28CDecoder15ReleaseInStreamEv, .Lfunc_end20-_ZN9NCompress6NLzma28CDecoder15ReleaseInStreamEv
	.cfi_endproc

	.globl	_ZThn24_N9NCompress6NLzma28CDecoder15ReleaseInStreamEv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress6NLzma28CDecoder15ReleaseInStreamEv,@function
_ZThn24_N9NCompress6NLzma28CDecoder15ReleaseInStreamEv: # @_ZThn24_N9NCompress6NLzma28CDecoder15ReleaseInStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 16
.Lcfi52:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 32(%rbx)
.LBB21_2:                               # %_ZN9NCompress6NLzma28CDecoder15ReleaseInStreamEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end21:
	.size	_ZThn24_N9NCompress6NLzma28CDecoder15ReleaseInStreamEv, .Lfunc_end21-_ZThn24_N9NCompress6NLzma28CDecoder15ReleaseInStreamEv
	.cfi_endproc

	.globl	_ZN9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy,@function
_ZN9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy: # @_ZN9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 16
.Lcfi54:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	setne	248(%rbx)
	je	.LBB22_2
# BB#1:
	movq	(%rsi), %rax
	movq	%rax, 256(%rbx)
.LBB22_2:
	leaq	80(%rbx), %rdi
	callq	Lzma2Dec_Init
	movl	$0, 76(%rbx)
	movl	$0, 72(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 264(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end22:
	.size	_ZN9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy, .Lfunc_end22-_ZN9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy
	.cfi_endproc

	.globl	_ZThn32_N9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy,@function
_ZThn32_N9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy: # @_ZThn32_N9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 16
.Lcfi56:
	.cfi_offset %rbx, -16
	testq	%rsi, %rsi
	setne	216(%rdi)
	leaq	-32(%rdi), %rbx
	je	.LBB23_2
# BB#1:
	movq	(%rsi), %rax
	movq	%rax, 256(%rbx)
.LBB23_2:                               # %_ZN9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy.exit
	leaq	80(%rbx), %rdi
	callq	Lzma2Dec_Init
	movl	$0, 76(%rbx)
	movl	$0, 72(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 264(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end23:
	.size	_ZThn32_N9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy, .Lfunc_end23-_ZThn32_N9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy
	.cfi_endproc

	.globl	_ZN9NCompress6NLzma28CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress6NLzma28CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress6NLzma28CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 128
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	cmpq	$0, 64(%rbp)
	je	.LBB24_19
# BB#1:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%r8, %rsi
	callq	*80(%rax)
	leaq	76(%rbp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	80(%rbp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	264(%rbp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	272(%rbp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 8(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB24_2:                               # =>This Inner Loop Header: Depth=1
	movl	72(%rbp), %eax
	cmpl	76(%rbp), %eax
	jne	.LBB24_4
# BB#3:                                 #   in Loop: Header=BB24_2 Depth=1
	movq	$0, 72(%rbp)
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	64(%rbp), %rsi
	movl	$1048576, %edx          # imm = 0x100000
	movq	32(%rsp), %rcx          # 8-byte Reload
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB24_24
.LBB24_4:                               #   in Loop: Header=BB24_2 Depth=1
	movq	128(%rbp), %r12
	movq	136(%rbp), %rsi
	subq	%r12, %rsi
	cmpq	$4194304, %rsi          # imm = 0x400000
	movl	$4194304, %eax          # imm = 0x400000
	cmovaeq	%rax, %rsi
	cmpb	$0, 248(%rbp)
	je	.LBB24_6
# BB#5:                                 #   in Loop: Header=BB24_2 Depth=1
	movq	256(%rbp), %rax
	subq	272(%rbp), %rax
	cmpq	%rsi, %rax
	cmovbq	%rax, %rsi
.LBB24_6:                               #   in Loop: Header=BB24_2 Depth=1
	movl	72(%rbp), %edx
	movl	76(%rbp), %eax
	subl	%edx, %eax
	movq	%rax, 24(%rsp)
	addq	%r12, %rsi
	addq	64(%rbp), %rdx
	xorl	%r14d, %r14d
	xorl	%r8d, %r8d
	movq	64(%rsp), %rdi          # 8-byte Reload
	leaq	24(%rsp), %rcx
	leaq	12(%rsp), %r9
	callq	Lzma2Dec_DecodeToDic
	movq	24(%rsp), %r13
	addl	%r13d, 72(%rbp)
	addq	%r13, 264(%rbp)
	movq	128(%rbp), %rdx
	movq	%rdx, %rcx
	subq	%r12, %rcx
	movq	272(%rbp), %rsi
	addq	%rcx, %rsi
	movq	%rsi, 272(%rbp)
	cmpb	$0, 248(%rbp)
	je	.LBB24_8
# BB#7:                                 #   in Loop: Header=BB24_2 Depth=1
	cmpq	256(%rbp), %rsi
	setae	%r14b
.LBB24_8:                               #   in Loop: Header=BB24_2 Depth=1
	testl	%eax, %eax
	jne	.LBB24_20
# BB#9:                                 #   in Loop: Header=BB24_2 Depth=1
	orq	%rcx, %r13
	sete	%cl
	movq	136(%rbp), %rax
	cmpq	%rax, %rdx
	sete	%bl
	orb	%cl, %bl
	orb	%r14b, %bl
	cmpb	$1, %bl
	jne	.LBB24_15
# BB#10:                                #   in Loop: Header=BB24_2 Depth=1
	movq	104(%rbp), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	movl	8(%rsp), %esi           # 4-byte Reload
	cmovnel	%eax, %esi
	setne	%dl
	testq	%r13, %r13
	setne	%cl
	orb	%r14b, %dl
	cmovnel	%eax, %esi
	movl	%esi, 8(%rsp)           # 4-byte Spill
	jne	.LBB24_12
# BB#11:                                #   in Loop: Header=BB24_2 Depth=1
	testb	%cl, %cl
	je	.LBB24_21
.LBB24_12:                              #   in Loop: Header=BB24_2 Depth=1
	testl	%eax, %eax
	jne	.LBB24_23
# BB#13:                                #   in Loop: Header=BB24_2 Depth=1
	xorb	$1, %r14b
	je	.LBB24_23
# BB#14:                                # %._crit_edge
                                        #   in Loop: Header=BB24_2 Depth=1
	movq	128(%rbp), %rdx
	movq	136(%rbp), %rax
.LBB24_15:                              #   in Loop: Header=BB24_2 Depth=1
	cmpq	%rax, %rdx
	jne	.LBB24_17
# BB#16:                                #   in Loop: Header=BB24_2 Depth=1
	movq	$0, 128(%rbp)
.LBB24_17:                              #   in Loop: Header=BB24_2 Depth=1
	testq	%r15, %r15
	je	.LBB24_2
# BB#18:                                #   in Loop: Header=BB24_2 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	*40(%rax)
	testl	%eax, %eax
	je	.LBB24_2
	jmp	.LBB24_24
.LBB24_19:
	movl	$1, %eax
	jmp	.LBB24_24
.LBB24_20:                              # %.thread
	movq	104(%rbp), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	$1, %eax
	jmp	.LBB24_24
.LBB24_21:
	xorl	%eax, %eax
	cmpl	$1, 12(%rsp)
	setne	%al
	jmp	.LBB24_24
.LBB24_23:
	movl	8(%rsp), %eax           # 4-byte Reload
.LBB24_24:                              # %.loopexit
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	_ZN9NCompress6NLzma28CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end24-_ZN9NCompress6NLzma28CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc

	.globl	_ZN9NCompress6NLzma28CDecoder4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CDecoder4ReadEPvjPj,@function
_ZN9NCompress6NLzma28CDecoder4ReadEPvjPj: # @_ZN9NCompress6NLzma28CDecoder4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 96
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%r15, %r15
	je	.LBB25_2
# BB#1:
	movl	$0, (%r15)
.LBB25_2:                               # %.preheader
	leaq	76(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	80(%rbx), %r12
	testq	%r15, %r15
	je	.LBB25_15
# BB#3:                                 # %.preheader.split.us.preheader
                                        # implicit-def: %R13D
	.p2align	4, 0x90
.LBB25_4:                               # %.preheader.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	72(%rbx), %eax
	movl	76(%rbx), %ecx
	cmpl	%ecx, %eax
	jne	.LBB25_7
# BB#5:                                 #   in Loop: Header=BB25_4 Depth=1
	movq	$0, 72(%rbx)
	movq	56(%rbx), %rdi
	movq	64(%rbx), %rsi
	movq	(%rdi), %rax
	movl	$1048576, %edx          # imm = 0x100000
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB25_29
# BB#6:                                 # %._crit_edge47
                                        #   in Loop: Header=BB25_4 Depth=1
	movl	72(%rbx), %eax
	movl	76(%rbx), %ecx
.LBB25_7:                               #   in Loop: Header=BB25_4 Depth=1
	subl	%eax, %ecx
	movq	%rcx, 16(%rsp)
	cmpb	$0, 248(%rbx)
	je	.LBB25_9
# BB#8:                                 #   in Loop: Header=BB25_4 Depth=1
	movq	256(%rbx), %rcx
	subq	272(%rbx), %rcx
	movl	%ebp, %edx
	cmpq	%rdx, %rcx
	cmovael	%ebp, %ecx
	movl	%ecx, %ebp
.LBB25_9:                               #   in Loop: Header=BB25_4 Depth=1
	movl	%ebp, %ecx
	movq	%rcx, 8(%rsp)
	movl	%eax, %ecx
	addq	64(%rbx), %rcx
	leaq	36(%rsp), %rax
	movq	%rax, (%rsp)
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	movq	%r14, %rsi
	leaq	8(%rsp), %rdx
	leaq	16(%rsp), %r8
	callq	Lzma2Dec_DecodeToBuf
	movq	16(%rsp), %rsi
	addl	%esi, 72(%rbx)
	movq	8(%rsp), %rcx
	movdqu	264(%rbx), %xmm0
	movd	%rsi, %xmm1
	movd	%rcx, %xmm2
	punpcklqdq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 264(%rbx)
	addl	%ecx, (%r15)
	movl	$1, %edx
	cmpl	$5, %eax
	ja	.LBB25_12
# BB#10:                                # %_ZL13SResToHRESULTi.exit.us
                                        #   in Loop: Header=BB25_4 Depth=1
	testl	%eax, %eax
	je	.LBB25_13
# BB#11:                                #   in Loop: Header=BB25_4 Depth=1
	cltq
	movl	.Lswitch.table(,%rax,4), %r13d
	testl	%edx, %edx
	je	.LBB25_14
	jmp	.LBB25_30
	.p2align	4, 0x90
.LBB25_12:                              #   in Loop: Header=BB25_4 Depth=1
	movl	$-2147467259, %r13d     # imm = 0x80004005
	testl	%edx, %edx
	je	.LBB25_14
	jmp	.LBB25_30
.LBB25_13:                              #   in Loop: Header=BB25_4 Depth=1
	xorl	%edx, %edx
	orq	%rcx, %rsi
	sete	%dl
	movl	$0, %eax
	cmovel	%eax, %r13d
	testl	%edx, %edx
	jne	.LBB25_30
	.p2align	4, 0x90
.LBB25_14:                              #   in Loop: Header=BB25_4 Depth=1
	subl	%ecx, %ebp
	addq	%rcx, %r14
	testl	%ebp, %ebp
	jne	.LBB25_4
	jmp	.LBB25_27
.LBB25_15:                              # %.preheader.split.preheader
	movl	72(%rbx), %ecx
	leaq	16(%rsp), %r15
                                        # implicit-def: %R13D
	.p2align	4, 0x90
.LBB25_16:                              # %.preheader.split
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpl	%eax, %ecx
	jne	.LBB25_19
# BB#17:                                #   in Loop: Header=BB25_16 Depth=1
	movq	$0, 72(%rbx)
	movq	56(%rbx), %rdi
	movq	64(%rbx), %rsi
	movq	(%rdi), %rax
	movl	$1048576, %edx          # imm = 0x100000
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB25_29
# BB#18:                                # %._crit_edge
                                        #   in Loop: Header=BB25_16 Depth=1
	movl	72(%rbx), %ecx
	movl	76(%rbx), %eax
.LBB25_19:                              #   in Loop: Header=BB25_16 Depth=1
	subl	%ecx, %eax
	movq	%rax, 16(%rsp)
	cmpb	$0, 248(%rbx)
	je	.LBB25_21
# BB#20:                                #   in Loop: Header=BB25_16 Depth=1
	movq	256(%rbx), %rax
	subq	272(%rbx), %rax
	movl	%ebp, %edx
	cmpq	%rdx, %rax
	cmovael	%ebp, %eax
	movl	%eax, %ebp
.LBB25_21:                              #   in Loop: Header=BB25_16 Depth=1
	movl	%ebp, %eax
	movq	%rax, 8(%rsp)
	movl	%ecx, %ecx
	addq	64(%rbx), %rcx
	leaq	36(%rsp), %rax
	movq	%rax, (%rsp)
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	movq	%r14, %rsi
	leaq	8(%rsp), %rdx
	movq	%r15, %r8
	callq	Lzma2Dec_DecodeToBuf
	movq	16(%rsp), %rdi
	movl	72(%rbx), %ecx
	addl	%edi, %ecx
	movl	%ecx, 72(%rbx)
	movq	8(%rsp), %rdx
	movdqu	264(%rbx), %xmm0
	movd	%rdi, %xmm1
	movd	%rdx, %xmm2
	punpcklqdq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 264(%rbx)
	movl	$1, %esi
	cmpl	$5, %eax
	ja	.LBB25_24
# BB#22:                                # %_ZL13SResToHRESULTi.exit
                                        #   in Loop: Header=BB25_16 Depth=1
	testl	%eax, %eax
	je	.LBB25_25
# BB#23:                                #   in Loop: Header=BB25_16 Depth=1
	cltq
	movl	.Lswitch.table(,%rax,4), %r13d
	testl	%esi, %esi
	je	.LBB25_26
	jmp	.LBB25_30
	.p2align	4, 0x90
.LBB25_24:                              #   in Loop: Header=BB25_16 Depth=1
	movl	$-2147467259, %r13d     # imm = 0x80004005
	testl	%esi, %esi
	je	.LBB25_26
	jmp	.LBB25_30
.LBB25_25:                              #   in Loop: Header=BB25_16 Depth=1
	xorl	%esi, %esi
	orq	%rdx, %rdi
	sete	%sil
	movl	$0, %eax
	cmovel	%eax, %r13d
	testl	%esi, %esi
	jne	.LBB25_30
	.p2align	4, 0x90
.LBB25_26:                              #   in Loop: Header=BB25_16 Depth=1
	subl	%edx, %ebp
	addq	%rdx, %r14
	testl	%ebp, %ebp
	jne	.LBB25_16
.LBB25_27:
	xorl	%r13d, %r13d
	jmp	.LBB25_30
.LBB25_29:
	movl	%eax, %r13d
.LBB25_30:                              # %.us-lcssa.us
	movl	%r13d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	_ZN9NCompress6NLzma28CDecoder4ReadEPvjPj, .Lfunc_end25-_ZN9NCompress6NLzma28CDecoder4ReadEPvjPj
	.cfi_endproc

	.globl	_ZThn40_N9NCompress6NLzma28CDecoder4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZThn40_N9NCompress6NLzma28CDecoder4ReadEPvjPj,@function
_ZThn40_N9NCompress6NLzma28CDecoder4ReadEPvjPj: # @_ZThn40_N9NCompress6NLzma28CDecoder4ReadEPvjPj
	.cfi_startproc
# BB#0:
	addq	$-40, %rdi
	jmp	_ZN9NCompress6NLzma28CDecoder4ReadEPvjPj # TAILCALL
.Lfunc_end26:
	.size	_ZThn40_N9NCompress6NLzma28CDecoder4ReadEPvjPj, .Lfunc_end26-_ZThn40_N9NCompress6NLzma28CDecoder4ReadEPvjPj
	.cfi_endproc

	.section	.text._ZN9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi83:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB27_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB27_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB27_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB27_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB27_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB27_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB27_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB27_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB27_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB27_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB27_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB27_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB27_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB27_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB27_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB27_16
.LBB27_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICompressSetDecoderProperties2(%rip), %cl
	jne	.LBB27_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+1(%rip), %al
	jne	.LBB27_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+2(%rip), %al
	jne	.LBB27_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+3(%rip), %al
	jne	.LBB27_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+4(%rip), %al
	jne	.LBB27_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+5(%rip), %al
	jne	.LBB27_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+6(%rip), %al
	jne	.LBB27_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+7(%rip), %al
	jne	.LBB27_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+8(%rip), %al
	jne	.LBB27_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+9(%rip), %al
	jne	.LBB27_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+10(%rip), %al
	jne	.LBB27_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+11(%rip), %al
	jne	.LBB27_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+12(%rip), %al
	jne	.LBB27_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+13(%rip), %al
	jne	.LBB27_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+14(%rip), %al
	jne	.LBB27_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+15(%rip), %al
	jne	.LBB27_33
.LBB27_16:
	leaq	8(%rdi), %rax
	jmp	.LBB27_101
.LBB27_33:                              # %_ZeqRK4GUIDS1_.exit14.thread
	cmpb	IID_ICompressGetInStreamProcessedSize(%rip), %cl
	jne	.LBB27_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+1(%rip), %al
	jne	.LBB27_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+2(%rip), %al
	jne	.LBB27_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+3(%rip), %al
	jne	.LBB27_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+4(%rip), %al
	jne	.LBB27_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+5(%rip), %al
	jne	.LBB27_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+6(%rip), %al
	jne	.LBB27_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+7(%rip), %al
	jne	.LBB27_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+8(%rip), %al
	jne	.LBB27_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+9(%rip), %al
	jne	.LBB27_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+10(%rip), %al
	jne	.LBB27_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+11(%rip), %al
	jne	.LBB27_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+12(%rip), %al
	jne	.LBB27_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+13(%rip), %al
	jne	.LBB27_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+14(%rip), %al
	jne	.LBB27_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit18
	movb	15(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+15(%rip), %al
	jne	.LBB27_50
# BB#49:
	leaq	16(%rdi), %rax
	jmp	.LBB27_101
.LBB27_50:                              # %_ZeqRK4GUIDS1_.exit18.thread
	cmpb	IID_ICompressSetInStream(%rip), %cl
	jne	.LBB27_67
# BB#51:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetInStream+1(%rip), %al
	jne	.LBB27_67
# BB#52:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetInStream+2(%rip), %al
	jne	.LBB27_67
# BB#53:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetInStream+3(%rip), %al
	jne	.LBB27_67
# BB#54:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetInStream+4(%rip), %al
	jne	.LBB27_67
# BB#55:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetInStream+5(%rip), %al
	jne	.LBB27_67
# BB#56:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetInStream+6(%rip), %al
	jne	.LBB27_67
# BB#57:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetInStream+7(%rip), %al
	jne	.LBB27_67
# BB#58:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetInStream+8(%rip), %al
	jne	.LBB27_67
# BB#59:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetInStream+9(%rip), %al
	jne	.LBB27_67
# BB#60:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetInStream+10(%rip), %al
	jne	.LBB27_67
# BB#61:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetInStream+11(%rip), %al
	jne	.LBB27_67
# BB#62:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetInStream+12(%rip), %al
	jne	.LBB27_67
# BB#63:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetInStream+13(%rip), %al
	jne	.LBB27_67
# BB#64:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetInStream+14(%rip), %al
	jne	.LBB27_67
# BB#65:                                # %_ZeqRK4GUIDS1_.exit22
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetInStream+15(%rip), %al
	jne	.LBB27_67
# BB#66:
	leaq	24(%rdi), %rax
	jmp	.LBB27_101
.LBB27_67:                              # %_ZeqRK4GUIDS1_.exit22.thread
	cmpb	IID_ICompressSetOutStreamSize(%rip), %cl
	jne	.LBB27_84
# BB#68:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+1(%rip), %al
	jne	.LBB27_84
# BB#69:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+2(%rip), %al
	jne	.LBB27_84
# BB#70:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+3(%rip), %al
	jne	.LBB27_84
# BB#71:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+4(%rip), %al
	jne	.LBB27_84
# BB#72:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+5(%rip), %al
	jne	.LBB27_84
# BB#73:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+6(%rip), %al
	jne	.LBB27_84
# BB#74:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+7(%rip), %al
	jne	.LBB27_84
# BB#75:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+8(%rip), %al
	jne	.LBB27_84
# BB#76:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+9(%rip), %al
	jne	.LBB27_84
# BB#77:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+10(%rip), %al
	jne	.LBB27_84
# BB#78:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+11(%rip), %al
	jne	.LBB27_84
# BB#79:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+12(%rip), %al
	jne	.LBB27_84
# BB#80:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+13(%rip), %al
	jne	.LBB27_84
# BB#81:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+14(%rip), %al
	jne	.LBB27_84
# BB#82:                                # %_ZeqRK4GUIDS1_.exit20
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+15(%rip), %al
	jne	.LBB27_84
# BB#83:
	leaq	32(%rdi), %rax
	jmp	.LBB27_101
.LBB27_84:                              # %_ZeqRK4GUIDS1_.exit20.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ISequentialInStream(%rip), %cl
	jne	.LBB27_102
# BB#85:
	movb	1(%rsi), %cl
	cmpb	IID_ISequentialInStream+1(%rip), %cl
	jne	.LBB27_102
# BB#86:
	movb	2(%rsi), %cl
	cmpb	IID_ISequentialInStream+2(%rip), %cl
	jne	.LBB27_102
# BB#87:
	movb	3(%rsi), %cl
	cmpb	IID_ISequentialInStream+3(%rip), %cl
	jne	.LBB27_102
# BB#88:
	movb	4(%rsi), %cl
	cmpb	IID_ISequentialInStream+4(%rip), %cl
	jne	.LBB27_102
# BB#89:
	movb	5(%rsi), %cl
	cmpb	IID_ISequentialInStream+5(%rip), %cl
	jne	.LBB27_102
# BB#90:
	movb	6(%rsi), %cl
	cmpb	IID_ISequentialInStream+6(%rip), %cl
	jne	.LBB27_102
# BB#91:
	movb	7(%rsi), %cl
	cmpb	IID_ISequentialInStream+7(%rip), %cl
	jne	.LBB27_102
# BB#92:
	movb	8(%rsi), %cl
	cmpb	IID_ISequentialInStream+8(%rip), %cl
	jne	.LBB27_102
# BB#93:
	movb	9(%rsi), %cl
	cmpb	IID_ISequentialInStream+9(%rip), %cl
	jne	.LBB27_102
# BB#94:
	movb	10(%rsi), %cl
	cmpb	IID_ISequentialInStream+10(%rip), %cl
	jne	.LBB27_102
# BB#95:
	movb	11(%rsi), %cl
	cmpb	IID_ISequentialInStream+11(%rip), %cl
	jne	.LBB27_102
# BB#96:
	movb	12(%rsi), %cl
	cmpb	IID_ISequentialInStream+12(%rip), %cl
	jne	.LBB27_102
# BB#97:
	movb	13(%rsi), %cl
	cmpb	IID_ISequentialInStream+13(%rip), %cl
	jne	.LBB27_102
# BB#98:
	movb	14(%rsi), %cl
	cmpb	IID_ISequentialInStream+14(%rip), %cl
	jne	.LBB27_102
# BB#99:                                # %_ZeqRK4GUIDS1_.exit16
	movb	15(%rsi), %cl
	cmpb	IID_ISequentialInStream+15(%rip), %cl
	jne	.LBB27_102
# BB#100:
	leaq	40(%rdi), %rax
.LBB27_101:                             # %_ZeqRK4GUIDS1_.exit16.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB27_102:                             # %_ZeqRK4GUIDS1_.exit16.thread
	popq	%rcx
	retq
.Lfunc_end27:
	.size	_ZN9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end27-_ZN9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress6NLzma28CDecoder6AddRefEv,"axG",@progbits,_ZN9NCompress6NLzma28CDecoder6AddRefEv,comdat
	.weak	_ZN9NCompress6NLzma28CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CDecoder6AddRefEv,@function
_ZN9NCompress6NLzma28CDecoder6AddRefEv: # @_ZN9NCompress6NLzma28CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	48(%rdi), %eax
	incl	%eax
	movl	%eax, 48(%rdi)
	retq
.Lfunc_end28:
	.size	_ZN9NCompress6NLzma28CDecoder6AddRefEv, .Lfunc_end28-_ZN9NCompress6NLzma28CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress6NLzma28CDecoder7ReleaseEv,"axG",@progbits,_ZN9NCompress6NLzma28CDecoder7ReleaseEv,comdat
	.weak	_ZN9NCompress6NLzma28CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma28CDecoder7ReleaseEv,@function
_ZN9NCompress6NLzma28CDecoder7ReleaseEv: # @_ZN9NCompress6NLzma28CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 16
	movl	48(%rdi), %eax
	decl	%eax
	movl	%eax, 48(%rdi)
	jne	.LBB29_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB29_2:
	popq	%rcx
	retq
.Lfunc_end29:
	.size	_ZN9NCompress6NLzma28CDecoder7ReleaseEv, .Lfunc_end29-_ZN9NCompress6NLzma28CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end30:
	.size	_ZThn8_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end30-_ZThn8_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NLzma28CDecoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress6NLzma28CDecoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress6NLzma28CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NLzma28CDecoder6AddRefEv,@function
_ZThn8_N9NCompress6NLzma28CDecoder6AddRefEv: # @_ZThn8_N9NCompress6NLzma28CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	40(%rdi), %eax
	incl	%eax
	movl	%eax, 40(%rdi)
	retq
.Lfunc_end31:
	.size	_ZThn8_N9NCompress6NLzma28CDecoder6AddRefEv, .Lfunc_end31-_ZThn8_N9NCompress6NLzma28CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NLzma28CDecoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress6NLzma28CDecoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress6NLzma28CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NLzma28CDecoder7ReleaseEv,@function
_ZThn8_N9NCompress6NLzma28CDecoder7ReleaseEv: # @_ZThn8_N9NCompress6NLzma28CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 16
	movl	40(%rdi), %eax
	decl	%eax
	movl	%eax, 40(%rdi)
	jne	.LBB32_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB32_2:                               # %_ZN9NCompress6NLzma28CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end32:
	.size	_ZThn8_N9NCompress6NLzma28CDecoder7ReleaseEv, .Lfunc_end32-_ZThn8_N9NCompress6NLzma28CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end33:
	.size	_ZThn16_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end33-_ZThn16_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress6NLzma28CDecoder6AddRefEv,"axG",@progbits,_ZThn16_N9NCompress6NLzma28CDecoder6AddRefEv,comdat
	.weak	_ZThn16_N9NCompress6NLzma28CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NLzma28CDecoder6AddRefEv,@function
_ZThn16_N9NCompress6NLzma28CDecoder6AddRefEv: # @_ZThn16_N9NCompress6NLzma28CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	32(%rdi), %eax
	incl	%eax
	movl	%eax, 32(%rdi)
	retq
.Lfunc_end34:
	.size	_ZThn16_N9NCompress6NLzma28CDecoder6AddRefEv, .Lfunc_end34-_ZThn16_N9NCompress6NLzma28CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress6NLzma28CDecoder7ReleaseEv,"axG",@progbits,_ZThn16_N9NCompress6NLzma28CDecoder7ReleaseEv,comdat
	.weak	_ZThn16_N9NCompress6NLzma28CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NLzma28CDecoder7ReleaseEv,@function
_ZThn16_N9NCompress6NLzma28CDecoder7ReleaseEv: # @_ZThn16_N9NCompress6NLzma28CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi86:
	.cfi_def_cfa_offset 16
	movl	32(%rdi), %eax
	decl	%eax
	movl	%eax, 32(%rdi)
	jne	.LBB35_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB35_2:                               # %_ZN9NCompress6NLzma28CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end35:
	.size	_ZThn16_N9NCompress6NLzma28CDecoder7ReleaseEv, .Lfunc_end35-_ZThn16_N9NCompress6NLzma28CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn24_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn24_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn24_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn24_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end36:
	.size	_ZThn24_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end36-_ZThn24_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress6NLzma28CDecoder6AddRefEv,"axG",@progbits,_ZThn24_N9NCompress6NLzma28CDecoder6AddRefEv,comdat
	.weak	_ZThn24_N9NCompress6NLzma28CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress6NLzma28CDecoder6AddRefEv,@function
_ZThn24_N9NCompress6NLzma28CDecoder6AddRefEv: # @_ZThn24_N9NCompress6NLzma28CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end37:
	.size	_ZThn24_N9NCompress6NLzma28CDecoder6AddRefEv, .Lfunc_end37-_ZThn24_N9NCompress6NLzma28CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress6NLzma28CDecoder7ReleaseEv,"axG",@progbits,_ZThn24_N9NCompress6NLzma28CDecoder7ReleaseEv,comdat
	.weak	_ZThn24_N9NCompress6NLzma28CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress6NLzma28CDecoder7ReleaseEv,@function
_ZThn24_N9NCompress6NLzma28CDecoder7ReleaseEv: # @_ZThn24_N9NCompress6NLzma28CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi87:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB38_2
# BB#1:
	addq	$-24, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB38_2:                               # %_ZN9NCompress6NLzma28CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end38:
	.size	_ZThn24_N9NCompress6NLzma28CDecoder7ReleaseEv, .Lfunc_end38-_ZThn24_N9NCompress6NLzma28CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn32_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn32_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn32_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn32_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end39:
	.size	_ZThn32_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end39-_ZThn32_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress6NLzma28CDecoder6AddRefEv,"axG",@progbits,_ZThn32_N9NCompress6NLzma28CDecoder6AddRefEv,comdat
	.weak	_ZThn32_N9NCompress6NLzma28CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress6NLzma28CDecoder6AddRefEv,@function
_ZThn32_N9NCompress6NLzma28CDecoder6AddRefEv: # @_ZThn32_N9NCompress6NLzma28CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end40:
	.size	_ZThn32_N9NCompress6NLzma28CDecoder6AddRefEv, .Lfunc_end40-_ZThn32_N9NCompress6NLzma28CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress6NLzma28CDecoder7ReleaseEv,"axG",@progbits,_ZThn32_N9NCompress6NLzma28CDecoder7ReleaseEv,comdat
	.weak	_ZThn32_N9NCompress6NLzma28CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress6NLzma28CDecoder7ReleaseEv,@function
_ZThn32_N9NCompress6NLzma28CDecoder7ReleaseEv: # @_ZThn32_N9NCompress6NLzma28CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB41_2
# BB#1:
	addq	$-32, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB41_2:                               # %_ZN9NCompress6NLzma28CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end41:
	.size	_ZThn32_N9NCompress6NLzma28CDecoder7ReleaseEv, .Lfunc_end41-_ZThn32_N9NCompress6NLzma28CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn40_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn40_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn40_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn40_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn40_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn40_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-40, %rdi
	jmp	_ZN9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end42:
	.size	_ZThn40_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end42-_ZThn40_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn40_N9NCompress6NLzma28CDecoder6AddRefEv,"axG",@progbits,_ZThn40_N9NCompress6NLzma28CDecoder6AddRefEv,comdat
	.weak	_ZThn40_N9NCompress6NLzma28CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn40_N9NCompress6NLzma28CDecoder6AddRefEv,@function
_ZThn40_N9NCompress6NLzma28CDecoder6AddRefEv: # @_ZThn40_N9NCompress6NLzma28CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end43:
	.size	_ZThn40_N9NCompress6NLzma28CDecoder6AddRefEv, .Lfunc_end43-_ZThn40_N9NCompress6NLzma28CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn40_N9NCompress6NLzma28CDecoder7ReleaseEv,"axG",@progbits,_ZThn40_N9NCompress6NLzma28CDecoder7ReleaseEv,comdat
	.weak	_ZThn40_N9NCompress6NLzma28CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn40_N9NCompress6NLzma28CDecoder7ReleaseEv,@function
_ZThn40_N9NCompress6NLzma28CDecoder7ReleaseEv: # @_ZThn40_N9NCompress6NLzma28CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB44_2
# BB#1:
	addq	$-40, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB44_2:                               # %_ZN9NCompress6NLzma28CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end44:
	.size	_ZThn40_N9NCompress6NLzma28CDecoder7ReleaseEv, .Lfunc_end44-_ZThn40_N9NCompress6NLzma28CDecoder7ReleaseEv
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma2L7SzAllocEPvm,@function
_ZN9NCompress6NLzma2L7SzAllocEPvm:      # @_ZN9NCompress6NLzma2L7SzAllocEPvm
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	MyAlloc                 # TAILCALL
.Lfunc_end45:
	.size	_ZN9NCompress6NLzma2L7SzAllocEPvm, .Lfunc_end45-_ZN9NCompress6NLzma2L7SzAllocEPvm
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress6NLzma2L6SzFreeEPvS1_,@function
_ZN9NCompress6NLzma2L6SzFreeEPvS1_:     # @_ZN9NCompress6NLzma2L6SzFreeEPvS1_
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	MyFree                  # TAILCALL
.Lfunc_end46:
	.size	_ZN9NCompress6NLzma2L6SzFreeEPvS1_, .Lfunc_end46-_ZN9NCompress6NLzma2L6SzFreeEPvS1_
	.cfi_endproc

	.type	_ZTVN9NCompress6NLzma28CDecoderE,@object # @_ZTVN9NCompress6NLzma28CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress6NLzma28CDecoderE
	.p2align	3
_ZTVN9NCompress6NLzma28CDecoderE:
	.quad	0
	.quad	_ZTIN9NCompress6NLzma28CDecoderE
	.quad	_ZN9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress6NLzma28CDecoder6AddRefEv
	.quad	_ZN9NCompress6NLzma28CDecoder7ReleaseEv
	.quad	_ZN9NCompress6NLzma28CDecoderD2Ev
	.quad	_ZN9NCompress6NLzma28CDecoderD0Ev
	.quad	_ZN9NCompress6NLzma28CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.quad	_ZN9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj
	.quad	_ZN9NCompress6NLzma28CDecoder24GetInStreamProcessedSizeEPy
	.quad	_ZN9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream
	.quad	_ZN9NCompress6NLzma28CDecoder15ReleaseInStreamEv
	.quad	_ZN9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy
	.quad	_ZN9NCompress6NLzma28CDecoder4ReadEPvjPj
	.quad	-8
	.quad	_ZTIN9NCompress6NLzma28CDecoderE
	.quad	_ZThn8_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress6NLzma28CDecoder6AddRefEv
	.quad	_ZThn8_N9NCompress6NLzma28CDecoder7ReleaseEv
	.quad	_ZThn8_N9NCompress6NLzma28CDecoderD1Ev
	.quad	_ZThn8_N9NCompress6NLzma28CDecoderD0Ev
	.quad	_ZThn8_N9NCompress6NLzma28CDecoder21SetDecoderProperties2EPKhj
	.quad	-16
	.quad	_ZTIN9NCompress6NLzma28CDecoderE
	.quad	_ZThn16_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N9NCompress6NLzma28CDecoder6AddRefEv
	.quad	_ZThn16_N9NCompress6NLzma28CDecoder7ReleaseEv
	.quad	_ZThn16_N9NCompress6NLzma28CDecoderD1Ev
	.quad	_ZThn16_N9NCompress6NLzma28CDecoderD0Ev
	.quad	_ZThn16_N9NCompress6NLzma28CDecoder24GetInStreamProcessedSizeEPy
	.quad	-24
	.quad	_ZTIN9NCompress6NLzma28CDecoderE
	.quad	_ZThn24_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn24_N9NCompress6NLzma28CDecoder6AddRefEv
	.quad	_ZThn24_N9NCompress6NLzma28CDecoder7ReleaseEv
	.quad	_ZThn24_N9NCompress6NLzma28CDecoderD1Ev
	.quad	_ZThn24_N9NCompress6NLzma28CDecoderD0Ev
	.quad	_ZThn24_N9NCompress6NLzma28CDecoder11SetInStreamEP19ISequentialInStream
	.quad	_ZThn24_N9NCompress6NLzma28CDecoder15ReleaseInStreamEv
	.quad	-32
	.quad	_ZTIN9NCompress6NLzma28CDecoderE
	.quad	_ZThn32_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn32_N9NCompress6NLzma28CDecoder6AddRefEv
	.quad	_ZThn32_N9NCompress6NLzma28CDecoder7ReleaseEv
	.quad	_ZThn32_N9NCompress6NLzma28CDecoderD1Ev
	.quad	_ZThn32_N9NCompress6NLzma28CDecoderD0Ev
	.quad	_ZThn32_N9NCompress6NLzma28CDecoder16SetOutStreamSizeEPKy
	.quad	-40
	.quad	_ZTIN9NCompress6NLzma28CDecoderE
	.quad	_ZThn40_N9NCompress6NLzma28CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn40_N9NCompress6NLzma28CDecoder6AddRefEv
	.quad	_ZThn40_N9NCompress6NLzma28CDecoder7ReleaseEv
	.quad	_ZThn40_N9NCompress6NLzma28CDecoderD1Ev
	.quad	_ZThn40_N9NCompress6NLzma28CDecoderD0Ev
	.quad	_ZThn40_N9NCompress6NLzma28CDecoder4ReadEPvjPj
	.size	_ZTVN9NCompress6NLzma28CDecoderE, 440

	.type	_ZN9NCompress6NLzma2L7g_AllocE,@object # @_ZN9NCompress6NLzma2L7g_AllocE
	.data
	.p2align	3
_ZN9NCompress6NLzma2L7g_AllocE:
	.quad	_ZN9NCompress6NLzma2L7SzAllocEPvm
	.quad	_ZN9NCompress6NLzma2L6SzFreeEPvS1_
	.size	_ZN9NCompress6NLzma2L7g_AllocE, 16

	.type	_ZTSN9NCompress6NLzma28CDecoderE,@object # @_ZTSN9NCompress6NLzma28CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTSN9NCompress6NLzma28CDecoderE
	.p2align	4
_ZTSN9NCompress6NLzma28CDecoderE:
	.asciz	"N9NCompress6NLzma28CDecoderE"
	.size	_ZTSN9NCompress6NLzma28CDecoderE, 29

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS30ICompressSetDecoderProperties2,@object # @_ZTS30ICompressSetDecoderProperties2
	.section	.rodata._ZTS30ICompressSetDecoderProperties2,"aG",@progbits,_ZTS30ICompressSetDecoderProperties2,comdat
	.weak	_ZTS30ICompressSetDecoderProperties2
	.p2align	4
_ZTS30ICompressSetDecoderProperties2:
	.asciz	"30ICompressSetDecoderProperties2"
	.size	_ZTS30ICompressSetDecoderProperties2, 33

	.type	_ZTI30ICompressSetDecoderProperties2,@object # @_ZTI30ICompressSetDecoderProperties2
	.section	.rodata._ZTI30ICompressSetDecoderProperties2,"aG",@progbits,_ZTI30ICompressSetDecoderProperties2,comdat
	.weak	_ZTI30ICompressSetDecoderProperties2
	.p2align	4
_ZTI30ICompressSetDecoderProperties2:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS30ICompressSetDecoderProperties2
	.quad	_ZTI8IUnknown
	.size	_ZTI30ICompressSetDecoderProperties2, 24

	.type	_ZTS33ICompressGetInStreamProcessedSize,@object # @_ZTS33ICompressGetInStreamProcessedSize
	.section	.rodata._ZTS33ICompressGetInStreamProcessedSize,"aG",@progbits,_ZTS33ICompressGetInStreamProcessedSize,comdat
	.weak	_ZTS33ICompressGetInStreamProcessedSize
	.p2align	4
_ZTS33ICompressGetInStreamProcessedSize:
	.asciz	"33ICompressGetInStreamProcessedSize"
	.size	_ZTS33ICompressGetInStreamProcessedSize, 36

	.type	_ZTI33ICompressGetInStreamProcessedSize,@object # @_ZTI33ICompressGetInStreamProcessedSize
	.section	.rodata._ZTI33ICompressGetInStreamProcessedSize,"aG",@progbits,_ZTI33ICompressGetInStreamProcessedSize,comdat
	.weak	_ZTI33ICompressGetInStreamProcessedSize
	.p2align	4
_ZTI33ICompressGetInStreamProcessedSize:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS33ICompressGetInStreamProcessedSize
	.quad	_ZTI8IUnknown
	.size	_ZTI33ICompressGetInStreamProcessedSize, 24

	.type	_ZTS20ICompressSetInStream,@object # @_ZTS20ICompressSetInStream
	.section	.rodata._ZTS20ICompressSetInStream,"aG",@progbits,_ZTS20ICompressSetInStream,comdat
	.weak	_ZTS20ICompressSetInStream
	.p2align	4
_ZTS20ICompressSetInStream:
	.asciz	"20ICompressSetInStream"
	.size	_ZTS20ICompressSetInStream, 23

	.type	_ZTI20ICompressSetInStream,@object # @_ZTI20ICompressSetInStream
	.section	.rodata._ZTI20ICompressSetInStream,"aG",@progbits,_ZTI20ICompressSetInStream,comdat
	.weak	_ZTI20ICompressSetInStream
	.p2align	4
_ZTI20ICompressSetInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ICompressSetInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ICompressSetInStream, 24

	.type	_ZTS25ICompressSetOutStreamSize,@object # @_ZTS25ICompressSetOutStreamSize
	.section	.rodata._ZTS25ICompressSetOutStreamSize,"aG",@progbits,_ZTS25ICompressSetOutStreamSize,comdat
	.weak	_ZTS25ICompressSetOutStreamSize
	.p2align	4
_ZTS25ICompressSetOutStreamSize:
	.asciz	"25ICompressSetOutStreamSize"
	.size	_ZTS25ICompressSetOutStreamSize, 28

	.type	_ZTI25ICompressSetOutStreamSize,@object # @_ZTI25ICompressSetOutStreamSize
	.section	.rodata._ZTI25ICompressSetOutStreamSize,"aG",@progbits,_ZTI25ICompressSetOutStreamSize,comdat
	.weak	_ZTI25ICompressSetOutStreamSize
	.p2align	4
_ZTI25ICompressSetOutStreamSize:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS25ICompressSetOutStreamSize
	.quad	_ZTI8IUnknown
	.size	_ZTI25ICompressSetOutStreamSize, 24

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress6NLzma28CDecoderE,@object # @_ZTIN9NCompress6NLzma28CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress6NLzma28CDecoderE
	.p2align	4
_ZTIN9NCompress6NLzma28CDecoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress6NLzma28CDecoderE
	.long	1                       # 0x1
	.long	7                       # 0x7
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI30ICompressSetDecoderProperties2
	.quad	2050                    # 0x802
	.quad	_ZTI33ICompressGetInStreamProcessedSize
	.quad	4098                    # 0x1002
	.quad	_ZTI20ICompressSetInStream
	.quad	6146                    # 0x1802
	.quad	_ZTI25ICompressSetOutStreamSize
	.quad	8194                    # 0x2002
	.quad	_ZTI19ISequentialInStream
	.quad	10242                   # 0x2802
	.quad	_ZTI13CMyUnknownImp
	.quad	12290                   # 0x3002
	.size	_ZTIN9NCompress6NLzma28CDecoderE, 136

	.type	.Lswitch.table,@object  # @switch.table
	.p2align	4
.Lswitch.table:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2147942414              # 0x8007000e
	.long	2147500037              # 0x80004005
	.long	2147500037              # 0x80004005
	.long	2147942487              # 0x80070057
	.size	.Lswitch.table, 24


	.globl	_ZN9NCompress6NLzma28CDecoderC1Ev
	.type	_ZN9NCompress6NLzma28CDecoderC1Ev,@function
_ZN9NCompress6NLzma28CDecoderC1Ev = _ZN9NCompress6NLzma28CDecoderC2Ev
	.globl	_ZN9NCompress6NLzma28CDecoderD1Ev
	.type	_ZN9NCompress6NLzma28CDecoderD1Ev,@function
_ZN9NCompress6NLzma28CDecoderD1Ev = _ZN9NCompress6NLzma28CDecoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
