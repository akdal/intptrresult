	.text
	.file	"WzAes.bc"
	.globl	_ZN7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj,@function
_ZN7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj: # @_ZN7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$99, %edx
	ja	.LBB0_11
# BB#1:
	movl	%edx, %r15d
	movq	56(%r13), %rbx
	cmpq	%r15, %rbx
	jne	.LBB0_3
# BB#2:                                 # %._ZN7CBufferIhE11SetCapacityEm.exit_crit_edge
	movq	64(%r13), %r12
	jmp	.LBB0_10
.LBB0_3:
	testl	%edx, %edx
	je	.LBB0_4
# BB#5:
	movq	%r15, %rdi
	callq	_Znam
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.LBB0_7
# BB#6:
	movq	64(%r13), %rsi
	cmpq	%r15, %rbx
	cmovaeq	%r15, %rbx
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	memmove
	jmp	.LBB0_7
.LBB0_4:
	xorl	%r12d, %r12d
.LBB0_7:
	movq	64(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB0_9
# BB#8:
	callq	_ZdaPv
.LBB0_9:
	movq	%r12, 64(%r13)
	movq	%r15, 56(%r13)
.LBB0_10:                               # %_ZN7CBufferIhE11SetCapacityEm.exit
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	memcpy
	xorl	%eax, %eax
.LBB0_11:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	_ZN7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj, .Lfunc_end0-_ZN7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj
	.cfi_endproc

	.globl	_ZThn8_N7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj,@function
_ZThn8_N7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj: # @_ZThn8_N7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -48
.Lcfi16:
	.cfi_offset %r12, -40
.Lcfi17:
	.cfi_offset %r13, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$99, %edx
	ja	.LBB1_11
# BB#1:
	movl	%edx, %r15d
	movq	48(%r13), %rbx
	cmpq	%r15, %rbx
	jne	.LBB1_3
# BB#2:                                 # %._ZN7CBufferIhE11SetCapacityEm.exit_crit_edge.i
	movq	56(%r13), %r12
	jmp	.LBB1_10
.LBB1_3:
	testl	%edx, %edx
	je	.LBB1_4
# BB#5:
	movq	%r15, %rdi
	callq	_Znam
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.LBB1_7
# BB#6:
	movq	56(%r13), %rsi
	cmpq	%r15, %rbx
	cmovaeq	%r15, %rbx
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	memmove
	jmp	.LBB1_7
.LBB1_4:
	xorl	%r12d, %r12d
.LBB1_7:
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB1_9
# BB#8:
	callq	_ZdaPv
.LBB1_9:
	movq	%r12, 56(%r13)
	movq	%r15, 48(%r13)
.LBB1_10:                               # %_ZN7CBufferIhE11SetCapacityEm.exit.i
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	memcpy
	xorl	%eax, %eax
.LBB1_11:                               # %_ZN7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZThn8_N7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj, .Lfunc_end1-_ZThn8_N7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj
	.cfi_endproc

	.globl	_ZN7NCrypto6NWzAes10CBaseCoder4InitEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes10CBaseCoder4InitEv,@function
_ZN7NCrypto6NWzAes10CBaseCoder4InitEv:  # @_ZN7NCrypto6NWzAes10CBaseCoder4InitEv
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 40
	subq	$440, %rsp              # imm = 0x1B8
.Lcfi24:
	.cfi_def_cfa_offset 480
.Lcfi25:
	.cfi_offset %rbx, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	24(%rbx), %ecx
	leal	(,%rcx,8), %eax
	andl	$24, %eax
	leal	8(%rax), %r15d
	leaq	16(%rax,%rax), %r14
	movl	%r15d, %eax
	shrl	%eax
	andl	$3, %ecx
	leal	4(,%rcx,4), %ecx
	shrl	$2, %ecx
	movzbl	28(%rbx), %edx
	shll	$24, %edx
	movzbl	29(%rbx), %esi
	shll	$16, %esi
	orl	%edx, %esi
	movzbl	30(%rbx), %edx
	shll	$8, %edx
	orl	%esi, %edx
	movzbl	31(%rbx), %esi
	orl	%edx, %esi
	movl	%esi, 176(%rsp)
	cmpl	$1, %ecx
	je	.LBB2_1
# BB#4:                                 # %.lr.ph.i.1
	movzbl	32(%rbx), %edx
	shll	$24, %edx
	movzbl	33(%rbx), %esi
	shll	$16, %esi
	orl	%edx, %esi
	movzbl	34(%rbx), %edx
	shll	$8, %edx
	orl	%esi, %edx
	movzbl	35(%rbx), %esi
	orl	%edx, %esi
	movl	%esi, 180(%rsp)
	cmpl	$2, %ecx
	je	.LBB2_1
# BB#5:                                 # %.lr.ph.i.2
	movzbl	36(%rbx), %edx
	shll	$24, %edx
	movzbl	37(%rbx), %esi
	shll	$16, %esi
	orl	%edx, %esi
	movzbl	38(%rbx), %edx
	shll	$8, %edx
	orl	%esi, %edx
	movzbl	39(%rbx), %esi
	orl	%edx, %esi
	movl	%esi, 184(%rsp)
	cmpl	$3, %ecx
	je	.LBB2_1
# BB#6:                                 # %.lr.ph.i.3
	movzbl	40(%rbx), %edx
	shll	$24, %edx
	movzbl	41(%rbx), %esi
	shll	$16, %esi
	orl	%edx, %esi
	movzbl	42(%rbx), %edx
	shll	$8, %edx
	orl	%esi, %edx
	movzbl	43(%rbx), %esi
	orl	%edx, %esi
	movl	%esi, 188(%rsp)
.LBB2_1:                                # %_ZN7NCrypto6NWzAesL16BytesToBeUInt32sEPKhPjj.exit.new
	movl	%r14d, %ebp
	orl	$2, %ebp
	orl	$1, %eax
	movq	56(%rbx), %rsi
	movq	64(%rbx), %rdi
	movq	%rax, (%rsp)
	leaq	176(%rsp), %rdx
	leaq	96(%rsp), %r9
	movl	$1000, %r8d             # imm = 0x3E8
	callq	_ZN7NCrypto5NSha112Pbkdf2Hmac32EPKhmPKjmjPjm
	xorl	%eax, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	andl	$-4, %ecx
	movl	96(%rsp,%rcx), %esi
	movl	%eax, %ecx
	notl	%ecx
	andb	$24, %cl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	movb	%sil, 16(%rsp,%rdx)
	leal	1(%rdx), %ecx
	andl	$-4, %ecx
	movl	96(%rsp,%rcx), %esi
	leal	8(%rax), %ecx
	notl	%ecx
	andb	$24, %cl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	movb	%sil, 17(%rsp,%rdx)
	addq	$2, %rdx
	addl	$16, %eax
	cmpq	%rdx, %rbp
	jne	.LBB2_2
# BB#3:
	leaq	72(%rbx), %rdi
	movl	%r15d, %edx
	leaq	16(%rsp,%rdx), %rsi
	callq	_ZN7NCrypto5NSha15CHmac6SetKeyEPKhm
	movzwl	16(%rsp,%r14), %eax
	movw	%ax, 44(%rbx)
	movl	288(%rbx), %eax
	xorps	%xmm0, %xmm0
	movups	%xmm0, 308(%rbx,%rax,4)
	movl	$16, 284(%rbx)
	leaq	324(%rbx,%rax,4), %rdi
	leaq	16(%rsp), %rsi
	movl	%r15d, %edx
	callq	Aes_SetKey_Enc
	xorl	%eax, %eax
	addq	$440, %rsp              # imm = 0x1B8
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN7NCrypto6NWzAes10CBaseCoder4InitEv, .Lfunc_end2-_ZN7NCrypto6NWzAes10CBaseCoder4InitEv
	.cfi_endproc

	.globl	_ZN7NCrypto6NWzAes12AesCtr2_InitEPNS0_8CAesCtr2E
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes12AesCtr2_InitEPNS0_8CAesCtr2E,@function
_ZN7NCrypto6NWzAes12AesCtr2_InitEPNS0_8CAesCtr2E: # @_ZN7NCrypto6NWzAes12AesCtr2_InitEPNS0_8CAesCtr2E
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %eax
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rdi,%rax,4)
	movl	$16, (%rdi)
	retq
.Lfunc_end3:
	.size	_ZN7NCrypto6NWzAes12AesCtr2_InitEPNS0_8CAesCtr2E, .Lfunc_end3-_ZN7NCrypto6NWzAes12AesCtr2_InitEPNS0_8CAesCtr2E
	.cfi_endproc

	.globl	_ZN7NCrypto6NWzAes8CEncoder11WriteHeaderEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CEncoder11WriteHeaderEP20ISequentialOutStream,@function
_ZN7NCrypto6NWzAes8CEncoder11WriteHeaderEP20ISequentialOutStream: # @_ZN7NCrypto6NWzAes8CEncoder11WriteHeaderEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 48
.Lcfi34:
	.cfi_offset %rbx, -40
.Lcfi35:
	.cfi_offset %r12, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	24(%rbx), %eax
	andl	$3, %eax
	leal	4(,%rax,4), %r15d
	leaq	28(%rbx), %r12
	movl	$g_RandomGenerator, %edi
	movq	%r12, %rsi
	movl	%r15d, %edx
	callq	_ZN16CRandomGenerator8GenerateEPhj
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*40(%rax)
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	je	.LBB4_2
# BB#1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB4_2:
	addq	$44, %rbx
	movl	$2, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_Z11WriteStreamP20ISequentialOutStreamPKvm # TAILCALL
.Lfunc_end4:
	.size	_ZN7NCrypto6NWzAes8CEncoder11WriteHeaderEP20ISequentialOutStream, .Lfunc_end4-_ZN7NCrypto6NWzAes8CEncoder11WriteHeaderEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN7NCrypto6NWzAes8CEncoder11WriteFooterEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CEncoder11WriteFooterEP20ISequentialOutStream,@function
_ZN7NCrypto6NWzAes8CEncoder11WriteFooterEP20ISequentialOutStream: # @_ZN7NCrypto6NWzAes8CEncoder11WriteFooterEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 48
.Lcfi41:
	.cfi_offset %rbx, -24
.Lcfi42:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	addq	$72, %rdi
	leaq	14(%rsp), %r14
	movl	$10, %edx
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha15CHmac5FinalEPhm
	movl	$10, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_ZN7NCrypto6NWzAes8CEncoder11WriteFooterEP20ISequentialOutStream, .Lfunc_end5-_ZN7NCrypto6NWzAes8CEncoder11WriteFooterEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj,@function
_ZN7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj: # @_ZN7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	cmpl	$1, %edx
	jne	.LBB6_2
# BB#1:
	movl	$3, 24(%rdi)
	movzbl	(%rsi), %eax
	leal	-1(%rax), %ecx
	xorl	%edx, %edx
	cmpl	$2, %ecx
	movl	$3, %ecx
	cmovbel	%eax, %ecx
	movl	%ecx, 24(%rdi)
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmovbel	%edx, %eax
	retq
.LBB6_2:                                # %_ZN7NCrypto6NWzAes10CBaseCoder10SetKeyModeEj.exit
	movl	$-2147024809, %eax      # imm = 0x80070057
	retq
.Lfunc_end6:
	.size	_ZN7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj, .Lfunc_end6-_ZN7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.globl	_ZThn592_N7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZThn592_N7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj,@function
_ZThn592_N7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj: # @_ZThn592_N7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$1, %edx
	jne	.LBB7_2
# BB#1:
	movl	$3, -568(%rdi)
	movzbl	(%rsi), %eax
	leal	-1(%rax), %ecx
	xorl	%edx, %edx
	cmpl	$2, %ecx
	movl	$3, %ecx
	cmovbel	%eax, %ecx
	movl	%ecx, -568(%rdi)
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmovbel	%edx, %eax
.LBB7_2:                                # %_ZN7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj.exit
	retq
.Lfunc_end7:
	.size	_ZThn592_N7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj, .Lfunc_end7-_ZThn592_N7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.globl	_ZN7NCrypto6NWzAes8CDecoder10ReadHeaderEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CDecoder10ReadHeaderEP19ISequentialInStream,@function
_ZN7NCrypto6NWzAes8CDecoder10ReadHeaderEP19ISequentialInStream: # @_ZN7NCrypto6NWzAes8CDecoder10ReadHeaderEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 64
.Lcfi47:
	.cfi_offset %rbx, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	24(%r14), %ebx
	shll	$2, %ebx
	andl	$12, %ebx
	leal	6(%rbx), %edx
	movq	%rsp, %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_Z15ReadStream_FAILP19ISequentialInStreamPvm
	testl	%eax, %eax
	jne	.LBB8_2
# BB#1:                                 # %.preheader27.preheader
	movl	%ebx, %r15d
	leaq	28(%r14), %rdi
	orl	$3, %ebx
	incl	%ebx
	movq	%rsp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movzwl	4(%rsp,%r15), %eax
	movw	%ax, 280(%r14)
	xorl	%eax, %eax
.LBB8_2:                                # %.loopexit
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_ZN7NCrypto6NWzAes8CDecoder10ReadHeaderEP19ISequentialInStream, .Lfunc_end8-_ZN7NCrypto6NWzAes8CDecoder10ReadHeaderEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN7NCrypto6NWzAes8CDecoder23CheckPasswordVerifyCodeEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CDecoder23CheckPasswordVerifyCodeEv,@function
_ZN7NCrypto6NWzAes8CDecoder23CheckPasswordVerifyCodeEv: # @_ZN7NCrypto6NWzAes8CDecoder23CheckPasswordVerifyCodeEv
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	movb	44(%rdi), %al
	cmpb	280(%rdi), %al
	jne	.LBB9_1
# BB#2:                                 # %.lr.ph.i.11
	movb	45(%rdi), %al
	cmpb	281(%rdi), %al
	sete	%al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB9_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end9:
	.size	_ZN7NCrypto6NWzAes8CDecoder23CheckPasswordVerifyCodeEv, .Lfunc_end9-_ZN7NCrypto6NWzAes8CDecoder23CheckPasswordVerifyCodeEv
	.cfi_endproc

	.globl	_ZN7NCrypto6NWzAes8CDecoder8CheckMacEP19ISequentialInStreamRb
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CDecoder8CheckMacEP19ISequentialInStreamRb,@function
_ZN7NCrypto6NWzAes8CDecoder8CheckMacEP19ISequentialInStreamRb: # @_ZN7NCrypto6NWzAes8CDecoder8CheckMacEP19ISequentialInStreamRb
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 48
.Lcfi53:
	.cfi_offset %rbx, -24
.Lcfi54:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movb	$0, (%r14)
	leaq	14(%rsp), %rax
	movl	$10, %edx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_Z15ReadStream_FAILP19ISequentialInStreamPvm
	testl	%eax, %eax
	jne	.LBB10_5
# BB#1:                                 # %.lr.ph.i
	addq	$72, %rbx
	leaq	4(%rsp), %rsi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	_ZN7NCrypto5NSha15CHmac5FinalEPhm
	movb	14(%rsp), %cl
	xorl	%eax, %eax
	cmpb	4(%rsp), %cl
	movl	$0, %ecx
	jne	.LBB10_4
# BB#2:                                 # %.lr.ph.i.19
	movb	15(%rsp), %cl
	cmpb	5(%rsp), %cl
	jne	.LBB10_3
# BB#6:                                 # %.lr.ph.i.210
	movb	16(%rsp), %cl
	cmpb	6(%rsp), %cl
	jne	.LBB10_3
# BB#7:                                 # %.lr.ph.i.311
	movb	17(%rsp), %cl
	cmpb	7(%rsp), %cl
	jne	.LBB10_3
# BB#8:                                 # %.lr.ph.i.412
	movb	18(%rsp), %cl
	cmpb	8(%rsp), %cl
	jne	.LBB10_3
# BB#9:                                 # %.lr.ph.i.513
	movb	19(%rsp), %cl
	cmpb	9(%rsp), %cl
	jne	.LBB10_3
# BB#10:                                # %.lr.ph.i.614
	movb	20(%rsp), %cl
	cmpb	10(%rsp), %cl
	jne	.LBB10_3
# BB#11:                                # %.lr.ph.i.715
	movb	21(%rsp), %cl
	cmpb	11(%rsp), %cl
	jne	.LBB10_3
# BB#12:                                # %.lr.ph.i.816
	movb	22(%rsp), %cl
	cmpb	12(%rsp), %cl
	jne	.LBB10_3
# BB#13:                                # %.lr.ph.i.917
	movb	23(%rsp), %cl
	cmpb	13(%rsp), %cl
	sete	%cl
	jmp	.LBB10_4
.LBB10_3:
	xorl	%ecx, %ecx
.LBB10_4:                               # %_ZN7NCrypto6NWzAesL13CompareArraysEPKhS2_j.exit
	movb	%cl, (%r14)
.LBB10_5:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	_ZN7NCrypto6NWzAes8CDecoder8CheckMacEP19ISequentialInStreamRb, .Lfunc_end10-_ZN7NCrypto6NWzAes8CDecoder8CheckMacEP19ISequentialInStreamRb
	.cfi_endproc

	.globl	_ZN7NCrypto6NWzAes8CAesCtr2C2Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CAesCtr2C2Ev,@function
_ZN7NCrypto6NWzAes8CAesCtr2C2Ev:        # @_ZN7NCrypto6NWzAes8CAesCtr2C2Ev
	.cfi_startproc
# BB#0:
	movl	$-8, %eax
	subl	%edi, %eax
	shrl	$2, %eax
	andl	$3, %eax
	movl	%eax, 4(%rdi)
	retq
.Lfunc_end11:
	.size	_ZN7NCrypto6NWzAes8CAesCtr2C2Ev, .Lfunc_end11-_ZN7NCrypto6NWzAes8CAesCtr2C2Ev
	.cfi_endproc

	.globl	_ZN7NCrypto6NWzAes12AesCtr2_CodeEPNS0_8CAesCtr2EPhm
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes12AesCtr2_CodeEPNS0_8CAesCtr2EPhm,@function
_ZN7NCrypto6NWzAes12AesCtr2_CodeEPNS0_8CAesCtr2EPhm: # @_ZN7NCrypto6NWzAes12AesCtr2_CodeEPNS0_8CAesCtr2EPhm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 64
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r14
	testq	%r15, %r15
	je	.LBB12_13
# BB#1:
	movl	(%r14), %ecx
	movl	4(%r14), %r8d
	leaq	8(%r14,%r8,4), %rbp
	movl	$16, %eax
	cmpl	$16, %ecx
	je	.LBB12_6
# BB#2:
	leaq	-1(%r15), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB12_3:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rcx,%rdx), %rdi
	movl	%edi, %esi
	movzbl	(%rbp,%rsi), %ebx
	xorb	%bl, (%r12,%rdx)
	leaq	1(%rdx), %rsi
	cmpl	$15, %edi
	je	.LBB12_5
# BB#4:                                 #   in Loop: Header=BB12_3 Depth=1
	cmpq	%rdx, %rax
	movq	%rsi, %rdx
	jne	.LBB12_3
.LBB12_5:                               # %.loopexit55.loopexit
	leal	(%rsi,%rcx), %eax
	subq	%rsi, %r15
	addq	%rsi, %r12
.LBB12_6:                               # %.loopexit55
	cmpq	$16, %r15
	jb	.LBB12_8
# BB#7:
	movq	%r15, %r13
	shrq	$4, %r13
	leaq	16(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r8, %rbx
	callq	*g_AesCtr_Code(%rip)
	movq	%rbx, %r8
	shlq	$4, %r13
	addq	%r13, %r12
	subq	%r13, %r15
	movl	$16, %eax
.LBB12_8:
	testq	%r15, %r15
	je	.LBB12_12
# BB#9:                                 # %.preheader.preheader
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r14,%r8,4)
	leaq	16(%rbp), %rdi
	movl	$1, %edx
	movq	%rbp, %rsi
	callq	*g_AesCtr_Code(%rip)
	decq	%r15
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB12_10:                              # =>This Inner Loop Header: Depth=1
	leaq	1(%rcx), %rax
	movzbl	(%rbp,%rcx), %edx
	xorb	%dl, (%r12,%rcx)
	cmpq	$15, %rcx
	je	.LBB12_12
# BB#11:                                #   in Loop: Header=BB12_10 Depth=1
	cmpq	%rcx, %r15
	movq	%rax, %rcx
	jne	.LBB12_10
.LBB12_12:                              # %.loopexit
	movl	%eax, (%r14)
.LBB12_13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN7NCrypto6NWzAes12AesCtr2_CodeEPNS0_8CAesCtr2EPhm, .Lfunc_end12-_ZN7NCrypto6NWzAes12AesCtr2_CodeEPNS0_8CAesCtr2EPhm
	.cfi_endproc

	.globl	_ZN7NCrypto6NWzAes8CEncoder6FilterEPhj
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CEncoder6FilterEPhj,@function
_ZN7NCrypto6NWzAes8CEncoder6FilterEPhj: # @_ZN7NCrypto6NWzAes8CEncoder6FilterEPhj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 80
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r13
	movq	%rdi, %r8
	movl	%ebx, %edx
	testl	%ebx, %ebx
	je	.LBB13_14
# BB#1:
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movl	284(%r8), %ecx
	movl	288(%r8), %r9d
	leaq	292(%r8,%r9,4), %r12
	movl	$16, %eax
	cmpl	$16, %ecx
	movq	%rdx, %r15
	movq	%r13, %rbx
	je	.LBB13_6
# BB#2:
	leaq	-1(%rdx), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_3:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rcx,%rbp), %rsi
	movl	%esi, %edi
	movzbl	(%r12,%rdi), %ebx
	xorb	%bl, (%r13,%rbp)
	leaq	1(%rbp), %rbx
	cmpl	$15, %esi
	je	.LBB13_5
# BB#4:                                 #   in Loop: Header=BB13_3 Depth=1
	cmpq	%rbp, %rax
	movq	%rbx, %rbp
	jne	.LBB13_3
.LBB13_5:                               # %.loopexit55.i.loopexit
	leal	(%rbx,%rcx), %eax
	movq	%rdx, %r15
	subq	%rbx, %r15
	addq	%r13, %rbx
.LBB13_6:                               # %.loopexit55.i
	cmpq	$16, %r15
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	jb	.LBB13_8
# BB#7:
	movq	%r15, %r14
	shrq	$4, %r14
	leaq	16(%r12), %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%r9, %rbp
	callq	*g_AesCtr_Code(%rip)
	movq	%rbp, %r9
	movq	8(%rsp), %r8            # 8-byte Reload
	shlq	$4, %r14
	addq	%r14, %rbx
	subq	%r14, %r15
	movl	$16, %eax
.LBB13_8:
	leaq	284(%r8), %r14
	testq	%r15, %r15
	je	.LBB13_13
# BB#9:                                 # %.preheader.preheader.i
	movq	%r8, %rbp
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r14,%r9,4)
	leaq	16(%r12), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	callq	*g_AesCtr_Code(%rip)
	decq	%r15
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB13_10:                              # =>This Inner Loop Header: Depth=1
	leaq	1(%rcx), %rax
	movzbl	(%r12,%rcx), %edx
	xorb	%dl, (%rbx,%rcx)
	cmpq	$15, %rcx
	je	.LBB13_12
# BB#11:                                #   in Loop: Header=BB13_10 Depth=1
	cmpq	%rcx, %r15
	movq	%rax, %rcx
	jne	.LBB13_10
.LBB13_12:                              # %.loopexit.loopexit.i.loopexit
	movq	%rbp, %r8
.LBB13_13:                              # %.loopexit.i
	movl	%eax, (%r14)
	movl	4(%rsp), %ebx           # 4-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB13_14:                              # %_ZN7NCrypto6NWzAes12AesCtr2_CodeEPNS0_8CAesCtr2EPhm.exit
	addq	$72, %r8
	movq	%r8, %rdi
	movq	%r13, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZN7NCrypto6NWzAes8CEncoder6FilterEPhj, .Lfunc_end13-_ZN7NCrypto6NWzAes8CEncoder6FilterEPhj
	.cfi_endproc

	.globl	_ZN7NCrypto6NWzAes8CDecoder6FilterEPhj
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CDecoder6FilterEPhj,@function
_ZN7NCrypto6NWzAes8CDecoder6FilterEPhj: # @_ZN7NCrypto6NWzAes8CDecoder6FilterEPhj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi85:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi87:
	.cfi_def_cfa_offset 64
.Lcfi88:
	.cfi_offset %rbx, -56
.Lcfi89:
	.cfi_offset %r12, -48
.Lcfi90:
	.cfi_offset %r13, -40
.Lcfi91:
	.cfi_offset %r14, -32
.Lcfi92:
	.cfi_offset %r15, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %rbp
	movq	%rdi, %r15
	movl	%ebx, %r12d
	leaq	72(%r15), %rdi
	movq	%r12, %rdx
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	testl	%ebx, %ebx
	je	.LBB14_13
# BB#1:
	movl	284(%r15), %ecx
	movl	288(%r15), %r9d
	leaq	292(%r15,%r9,4), %r14
	movl	$16, %eax
	cmpl	$16, %ecx
	je	.LBB14_6
# BB#2:
	movl	%ebx, %r8d
	leaq	-1(%r12), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB14_3:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rcx,%rdx), %rdi
	movl	%edi, %esi
	movzbl	(%r14,%rsi), %ebx
	xorb	%bl, (%rbp,%rdx)
	leaq	1(%rdx), %rsi
	cmpl	$15, %edi
	je	.LBB14_5
# BB#4:                                 #   in Loop: Header=BB14_3 Depth=1
	cmpq	%rdx, %rax
	movq	%rsi, %rdx
	jne	.LBB14_3
.LBB14_5:                               # %.loopexit55.i.loopexit
	leal	(%rsi,%rcx), %eax
	subq	%rsi, %r12
	addq	%rsi, %rbp
	movl	%r8d, %ebx
.LBB14_6:                               # %.loopexit55.i
	cmpq	$16, %r12
	jb	.LBB14_8
# BB#7:
	movq	%r12, %r13
	shrq	$4, %r13
	leaq	16(%r14), %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movq	%r9, (%rsp)             # 8-byte Spill
	callq	*g_AesCtr_Code(%rip)
	movq	(%rsp), %r9             # 8-byte Reload
	shlq	$4, %r13
	addq	%r13, %rbp
	subq	%r13, %r12
	movl	$16, %eax
.LBB14_8:
	addq	$284, %r15              # imm = 0x11C
	testq	%r12, %r12
	je	.LBB14_12
# BB#9:                                 # %.preheader.preheader.i
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r15,%r9,4)
	leaq	16(%r14), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	callq	*g_AesCtr_Code(%rip)
	decq	%r12
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB14_10:                              # =>This Inner Loop Header: Depth=1
	leaq	1(%rcx), %rax
	movzbl	(%r14,%rcx), %edx
	xorb	%dl, (%rbp,%rcx)
	cmpq	$15, %rcx
	je	.LBB14_12
# BB#11:                                #   in Loop: Header=BB14_10 Depth=1
	cmpq	%rcx, %r12
	movq	%rax, %rcx
	jne	.LBB14_10
.LBB14_12:                              # %.loopexit.i
	movl	%eax, (%r15)
.LBB14_13:                              # %_ZN7NCrypto6NWzAes12AesCtr2_CodeEPNS0_8CAesCtr2EPhm.exit
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZN7NCrypto6NWzAes8CDecoder6FilterEPhj, .Lfunc_end14-_ZN7NCrypto6NWzAes8CDecoder6FilterEPhj
	.cfi_endproc

	.section	.text._ZN7NCrypto6NWzAes10CBaseCoderD0Ev,"axG",@progbits,_ZN7NCrypto6NWzAes10CBaseCoderD0Ev,comdat
	.weak	_ZN7NCrypto6NWzAes10CBaseCoderD0Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes10CBaseCoderD0Ev,@function
_ZN7NCrypto6NWzAes10CBaseCoderD0Ev:     # @_ZN7NCrypto6NWzAes10CBaseCoderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 16
.Lcfi95:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTV7CBufferIhE+16, 48(%rbx)
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_2
# BB#1:
	callq	_ZdaPv
.LBB15_2:                               # %_ZN7NCrypto6NWzAes10CBaseCoderD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end15:
	.size	_ZN7NCrypto6NWzAes10CBaseCoderD0Ev, .Lfunc_end15-_ZN7NCrypto6NWzAes10CBaseCoderD0Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto6NWzAes10CBaseCoderD1Ev,"axG",@progbits,_ZThn8_N7NCrypto6NWzAes10CBaseCoderD1Ev,comdat
	.weak	_ZThn8_N7NCrypto6NWzAes10CBaseCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto6NWzAes10CBaseCoderD1Ev,@function
_ZThn8_N7NCrypto6NWzAes10CBaseCoderD1Ev: # @_ZThn8_N7NCrypto6NWzAes10CBaseCoderD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	movq	$_ZTV7CBufferIhE+16, 40(%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB16_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB16_1:                               # %_ZN7NCrypto6NWzAes10CBaseCoderD2Ev.exit
	retq
.Lfunc_end16:
	.size	_ZThn8_N7NCrypto6NWzAes10CBaseCoderD1Ev, .Lfunc_end16-_ZThn8_N7NCrypto6NWzAes10CBaseCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto6NWzAes10CBaseCoderD0Ev,"axG",@progbits,_ZThn8_N7NCrypto6NWzAes10CBaseCoderD0Ev,comdat
	.weak	_ZThn8_N7NCrypto6NWzAes10CBaseCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto6NWzAes10CBaseCoderD0Ev,@function
_ZThn8_N7NCrypto6NWzAes10CBaseCoderD0Ev: # @_ZThn8_N7NCrypto6NWzAes10CBaseCoderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 16
.Lcfi97:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rax)
	movq	$_ZTV7CBufferIhE+16, 40(%rax)
	movq	56(%rax), %rdi
	leaq	-8(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB17_2
# BB#1:
	callq	_ZdaPv
.LBB17_2:                               # %_ZN7NCrypto6NWzAes10CBaseCoderD0Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end17:
	.size	_ZThn8_N7NCrypto6NWzAes10CBaseCoderD0Ev, .Lfunc_end17-_ZThn8_N7NCrypto6NWzAes10CBaseCoderD0Ev
	.cfi_endproc

	.section	.text._ZN7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZN7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZN7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 32
.Lcfi101:
	.cfi_offset %rbx, -32
.Lcfi102:
	.cfi_offset %r14, -24
.Lcfi103:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB18_3
# BB#1:
	movl	$IID_ICryptoSetPassword, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB18_2
.LBB18_3:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB18_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB18_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB18_4
.Lfunc_end18:
	.size	_ZN7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end18-_ZN7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN7NCrypto6NWzAes8CEncoder6AddRefEv,"axG",@progbits,_ZN7NCrypto6NWzAes8CEncoder6AddRefEv,comdat
	.weak	_ZN7NCrypto6NWzAes8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CEncoder6AddRefEv,@function
_ZN7NCrypto6NWzAes8CEncoder6AddRefEv:   # @_ZN7NCrypto6NWzAes8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end19:
	.size	_ZN7NCrypto6NWzAes8CEncoder6AddRefEv, .Lfunc_end19-_ZN7NCrypto6NWzAes8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN7NCrypto6NWzAes8CEncoder7ReleaseEv,"axG",@progbits,_ZN7NCrypto6NWzAes8CEncoder7ReleaseEv,comdat
	.weak	_ZN7NCrypto6NWzAes8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CEncoder7ReleaseEv,@function
_ZN7NCrypto6NWzAes8CEncoder7ReleaseEv:  # @_ZN7NCrypto6NWzAes8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi104:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB20_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB20_2:
	popq	%rcx
	retq
.Lfunc_end20:
	.size	_ZN7NCrypto6NWzAes8CEncoder7ReleaseEv, .Lfunc_end20-_ZN7NCrypto6NWzAes8CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN7NCrypto6NWzAes10CBaseCoderD2Ev,"axG",@progbits,_ZN7NCrypto6NWzAes10CBaseCoderD2Ev,comdat
	.weak	_ZN7NCrypto6NWzAes10CBaseCoderD2Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes10CBaseCoderD2Ev,@function
_ZN7NCrypto6NWzAes10CBaseCoderD2Ev:     # @_ZN7NCrypto6NWzAes10CBaseCoderD2Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rdi)
	movq	$_ZTV7CBufferIhE+16, 48(%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB21_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB21_1:                               # %_ZN7NCrypto6NWzAes8CKeyInfoD2Ev.exit
	retq
.Lfunc_end21:
	.size	_ZN7NCrypto6NWzAes10CBaseCoderD2Ev, .Lfunc_end21-_ZN7NCrypto6NWzAes10CBaseCoderD2Ev
	.cfi_endproc

	.section	.text._ZN7NCrypto6NWzAes8CEncoderD0Ev,"axG",@progbits,_ZN7NCrypto6NWzAes8CEncoderD0Ev,comdat
	.weak	_ZN7NCrypto6NWzAes8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CEncoderD0Ev,@function
_ZN7NCrypto6NWzAes8CEncoderD0Ev:        # @_ZN7NCrypto6NWzAes8CEncoderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 16
.Lcfi106:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTV7CBufferIhE+16, 48(%rbx)
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_2
# BB#1:
	callq	_ZdaPv
.LBB22_2:                               # %_ZN7NCrypto6NWzAes10CBaseCoderD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end22:
	.size	_ZN7NCrypto6NWzAes8CEncoderD0Ev, .Lfunc_end22-_ZN7NCrypto6NWzAes8CEncoderD0Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi107:
	.cfi_def_cfa_offset 16
	movq	%rdi, %r8
	leaq	-8(%r8), %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB23_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB23_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB23_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB23_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB23_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB23_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB23_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB23_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB23_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB23_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB23_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB23_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB23_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB23_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB23_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB23_32
.LBB23_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICryptoSetPassword(%rip), %cl
	jne	.LBB23_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+1(%rip), %cl
	jne	.LBB23_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+2(%rip), %cl
	jne	.LBB23_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+3(%rip), %cl
	jne	.LBB23_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+4(%rip), %cl
	jne	.LBB23_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+5(%rip), %cl
	jne	.LBB23_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+6(%rip), %cl
	jne	.LBB23_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+7(%rip), %cl
	jne	.LBB23_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+8(%rip), %cl
	jne	.LBB23_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+9(%rip), %cl
	jne	.LBB23_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+10(%rip), %cl
	jne	.LBB23_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+11(%rip), %cl
	jne	.LBB23_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+12(%rip), %cl
	jne	.LBB23_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+13(%rip), %cl
	jne	.LBB23_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+14(%rip), %cl
	jne	.LBB23_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+15(%rip), %cl
	jne	.LBB23_33
.LBB23_32:
	movq	%r8, (%rdx)
	movq	-8(%r8), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB23_33:                              # %_ZN7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end23:
	.size	_ZThn8_N7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end23-_ZThn8_N7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto6NWzAes8CEncoder6AddRefEv,"axG",@progbits,_ZThn8_N7NCrypto6NWzAes8CEncoder6AddRefEv,comdat
	.weak	_ZThn8_N7NCrypto6NWzAes8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto6NWzAes8CEncoder6AddRefEv,@function
_ZThn8_N7NCrypto6NWzAes8CEncoder6AddRefEv: # @_ZThn8_N7NCrypto6NWzAes8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end24:
	.size	_ZThn8_N7NCrypto6NWzAes8CEncoder6AddRefEv, .Lfunc_end24-_ZThn8_N7NCrypto6NWzAes8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto6NWzAes8CEncoder7ReleaseEv,"axG",@progbits,_ZThn8_N7NCrypto6NWzAes8CEncoder7ReleaseEv,comdat
	.weak	_ZThn8_N7NCrypto6NWzAes8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto6NWzAes8CEncoder7ReleaseEv,@function
_ZThn8_N7NCrypto6NWzAes8CEncoder7ReleaseEv: # @_ZThn8_N7NCrypto6NWzAes8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi108:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB25_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB25_2:                               # %_ZN7NCrypto6NWzAes8CEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end25:
	.size	_ZThn8_N7NCrypto6NWzAes8CEncoder7ReleaseEv, .Lfunc_end25-_ZThn8_N7NCrypto6NWzAes8CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto6NWzAes8CEncoderD1Ev,"axG",@progbits,_ZThn8_N7NCrypto6NWzAes8CEncoderD1Ev,comdat
	.weak	_ZThn8_N7NCrypto6NWzAes8CEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto6NWzAes8CEncoderD1Ev,@function
_ZThn8_N7NCrypto6NWzAes8CEncoderD1Ev:   # @_ZThn8_N7NCrypto6NWzAes8CEncoderD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	movq	$_ZTV7CBufferIhE+16, 40(%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB26_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB26_1:                               # %_ZN7NCrypto6NWzAes10CBaseCoderD2Ev.exit
	retq
.Lfunc_end26:
	.size	_ZThn8_N7NCrypto6NWzAes8CEncoderD1Ev, .Lfunc_end26-_ZThn8_N7NCrypto6NWzAes8CEncoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto6NWzAes8CEncoderD0Ev,"axG",@progbits,_ZThn8_N7NCrypto6NWzAes8CEncoderD0Ev,comdat
	.weak	_ZThn8_N7NCrypto6NWzAes8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto6NWzAes8CEncoderD0Ev,@function
_ZThn8_N7NCrypto6NWzAes8CEncoderD0Ev:   # @_ZThn8_N7NCrypto6NWzAes8CEncoderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 16
.Lcfi110:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rax)
	movq	$_ZTV7CBufferIhE+16, 40(%rax)
	movq	56(%rax), %rdi
	leaq	-8(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB27_2
# BB#1:
	callq	_ZdaPv
.LBB27_2:                               # %_ZN7NCrypto6NWzAes8CEncoderD0Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end27:
	.size	_ZThn8_N7NCrypto6NWzAes8CEncoderD0Ev, .Lfunc_end27-_ZThn8_N7NCrypto6NWzAes8CEncoderD0Ev
	.cfi_endproc

	.section	.text._ZN7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi111:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB28_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB28_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB28_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB28_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB28_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB28_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB28_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB28_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB28_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB28_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB28_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB28_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB28_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB28_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB28_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB28_16
.LBB28_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICryptoSetPassword(%rip), %cl
	jne	.LBB28_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICryptoSetPassword+1(%rip), %al
	jne	.LBB28_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICryptoSetPassword+2(%rip), %al
	jne	.LBB28_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICryptoSetPassword+3(%rip), %al
	jne	.LBB28_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICryptoSetPassword+4(%rip), %al
	jne	.LBB28_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICryptoSetPassword+5(%rip), %al
	jne	.LBB28_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICryptoSetPassword+6(%rip), %al
	jne	.LBB28_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICryptoSetPassword+7(%rip), %al
	jne	.LBB28_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICryptoSetPassword+8(%rip), %al
	jne	.LBB28_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICryptoSetPassword+9(%rip), %al
	jne	.LBB28_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICryptoSetPassword+10(%rip), %al
	jne	.LBB28_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICryptoSetPassword+11(%rip), %al
	jne	.LBB28_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICryptoSetPassword+12(%rip), %al
	jne	.LBB28_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICryptoSetPassword+13(%rip), %al
	jne	.LBB28_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICryptoSetPassword+14(%rip), %al
	jne	.LBB28_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit8
	movb	15(%rsi), %al
	cmpb	IID_ICryptoSetPassword+15(%rip), %al
	jne	.LBB28_33
.LBB28_16:
	leaq	8(%rdi), %rax
	jmp	.LBB28_50
.LBB28_33:                              # %_ZeqRK4GUIDS1_.exit8.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressSetDecoderProperties2(%rip), %cl
	jne	.LBB28_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+1(%rip), %cl
	jne	.LBB28_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+2(%rip), %cl
	jne	.LBB28_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+3(%rip), %cl
	jne	.LBB28_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+4(%rip), %cl
	jne	.LBB28_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+5(%rip), %cl
	jne	.LBB28_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+6(%rip), %cl
	jne	.LBB28_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+7(%rip), %cl
	jne	.LBB28_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+8(%rip), %cl
	jne	.LBB28_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+9(%rip), %cl
	jne	.LBB28_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+10(%rip), %cl
	jne	.LBB28_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+11(%rip), %cl
	jne	.LBB28_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+12(%rip), %cl
	jne	.LBB28_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+13(%rip), %cl
	jne	.LBB28_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+14(%rip), %cl
	jne	.LBB28_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+15(%rip), %cl
	jne	.LBB28_51
# BB#49:
	leaq	592(%rdi), %rax
.LBB28_50:                              # %_ZeqRK4GUIDS1_.exit10.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB28_51:                              # %_ZeqRK4GUIDS1_.exit10.thread
	popq	%rcx
	retq
.Lfunc_end28:
	.size	_ZN7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end28-_ZN7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN7NCrypto6NWzAes8CDecoder6AddRefEv,"axG",@progbits,_ZN7NCrypto6NWzAes8CDecoder6AddRefEv,comdat
	.weak	_ZN7NCrypto6NWzAes8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CDecoder6AddRefEv,@function
_ZN7NCrypto6NWzAes8CDecoder6AddRefEv:   # @_ZN7NCrypto6NWzAes8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end29:
	.size	_ZN7NCrypto6NWzAes8CDecoder6AddRefEv, .Lfunc_end29-_ZN7NCrypto6NWzAes8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN7NCrypto6NWzAes8CDecoder7ReleaseEv,"axG",@progbits,_ZN7NCrypto6NWzAes8CDecoder7ReleaseEv,comdat
	.weak	_ZN7NCrypto6NWzAes8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CDecoder7ReleaseEv,@function
_ZN7NCrypto6NWzAes8CDecoder7ReleaseEv:  # @_ZN7NCrypto6NWzAes8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi112:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB30_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB30_2:
	popq	%rcx
	retq
.Lfunc_end30:
	.size	_ZN7NCrypto6NWzAes8CDecoder7ReleaseEv, .Lfunc_end30-_ZN7NCrypto6NWzAes8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN7NCrypto6NWzAes8CDecoderD2Ev,"axG",@progbits,_ZN7NCrypto6NWzAes8CDecoderD2Ev,comdat
	.weak	_ZN7NCrypto6NWzAes8CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CDecoderD2Ev,@function
_ZN7NCrypto6NWzAes8CDecoderD2Ev:        # @_ZN7NCrypto6NWzAes8CDecoderD2Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rdi)
	movq	$_ZTV7CBufferIhE+16, 48(%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB31_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB31_1:                               # %_ZN7NCrypto6NWzAes10CBaseCoderD2Ev.exit
	retq
.Lfunc_end31:
	.size	_ZN7NCrypto6NWzAes8CDecoderD2Ev, .Lfunc_end31-_ZN7NCrypto6NWzAes8CDecoderD2Ev
	.cfi_endproc

	.section	.text._ZN7NCrypto6NWzAes8CDecoderD0Ev,"axG",@progbits,_ZN7NCrypto6NWzAes8CDecoderD0Ev,comdat
	.weak	_ZN7NCrypto6NWzAes8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto6NWzAes8CDecoderD0Ev,@function
_ZN7NCrypto6NWzAes8CDecoderD0Ev:        # @_ZN7NCrypto6NWzAes8CDecoderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 16
.Lcfi114:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTV7CBufferIhE+16, 48(%rbx)
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_2
# BB#1:
	callq	_ZdaPv
.LBB32_2:                               # %_ZN7NCrypto6NWzAes8CDecoderD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end32:
	.size	_ZN7NCrypto6NWzAes8CDecoderD0Ev, .Lfunc_end32-_ZN7NCrypto6NWzAes8CDecoderD0Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end33:
	.size	_ZThn8_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end33-_ZThn8_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto6NWzAes8CDecoder6AddRefEv,"axG",@progbits,_ZThn8_N7NCrypto6NWzAes8CDecoder6AddRefEv,comdat
	.weak	_ZThn8_N7NCrypto6NWzAes8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto6NWzAes8CDecoder6AddRefEv,@function
_ZThn8_N7NCrypto6NWzAes8CDecoder6AddRefEv: # @_ZThn8_N7NCrypto6NWzAes8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end34:
	.size	_ZThn8_N7NCrypto6NWzAes8CDecoder6AddRefEv, .Lfunc_end34-_ZThn8_N7NCrypto6NWzAes8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto6NWzAes8CDecoder7ReleaseEv,"axG",@progbits,_ZThn8_N7NCrypto6NWzAes8CDecoder7ReleaseEv,comdat
	.weak	_ZThn8_N7NCrypto6NWzAes8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto6NWzAes8CDecoder7ReleaseEv,@function
_ZThn8_N7NCrypto6NWzAes8CDecoder7ReleaseEv: # @_ZThn8_N7NCrypto6NWzAes8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi115:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB35_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB35_2:                               # %_ZN7NCrypto6NWzAes8CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end35:
	.size	_ZThn8_N7NCrypto6NWzAes8CDecoder7ReleaseEv, .Lfunc_end35-_ZThn8_N7NCrypto6NWzAes8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto6NWzAes8CDecoderD1Ev,"axG",@progbits,_ZThn8_N7NCrypto6NWzAes8CDecoderD1Ev,comdat
	.weak	_ZThn8_N7NCrypto6NWzAes8CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto6NWzAes8CDecoderD1Ev,@function
_ZThn8_N7NCrypto6NWzAes8CDecoderD1Ev:   # @_ZThn8_N7NCrypto6NWzAes8CDecoderD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	movq	$_ZTV7CBufferIhE+16, 40(%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB36_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB36_1:                               # %_ZN7NCrypto6NWzAes8CDecoderD2Ev.exit
	retq
.Lfunc_end36:
	.size	_ZThn8_N7NCrypto6NWzAes8CDecoderD1Ev, .Lfunc_end36-_ZThn8_N7NCrypto6NWzAes8CDecoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto6NWzAes8CDecoderD0Ev,"axG",@progbits,_ZThn8_N7NCrypto6NWzAes8CDecoderD0Ev,comdat
	.weak	_ZThn8_N7NCrypto6NWzAes8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto6NWzAes8CDecoderD0Ev,@function
_ZThn8_N7NCrypto6NWzAes8CDecoderD0Ev:   # @_ZThn8_N7NCrypto6NWzAes8CDecoderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 16
.Lcfi117:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rax)
	movq	$_ZTV7CBufferIhE+16, 40(%rax)
	movq	56(%rax), %rdi
	leaq	-8(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB37_2
# BB#1:
	callq	_ZdaPv
.LBB37_2:                               # %_ZN7NCrypto6NWzAes8CDecoderD0Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end37:
	.size	_ZThn8_N7NCrypto6NWzAes8CDecoderD0Ev, .Lfunc_end37-_ZThn8_N7NCrypto6NWzAes8CDecoderD0Ev
	.cfi_endproc

	.section	.text._ZThn592_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn592_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn592_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn592_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn592_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn592_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-592, %rdi             # imm = 0xFDB0
	jmp	_ZN7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end38:
	.size	_ZThn592_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end38-_ZThn592_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn592_N7NCrypto6NWzAes8CDecoder6AddRefEv,"axG",@progbits,_ZThn592_N7NCrypto6NWzAes8CDecoder6AddRefEv,comdat
	.weak	_ZThn592_N7NCrypto6NWzAes8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn592_N7NCrypto6NWzAes8CDecoder6AddRefEv,@function
_ZThn592_N7NCrypto6NWzAes8CDecoder6AddRefEv: # @_ZThn592_N7NCrypto6NWzAes8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	-576(%rdi), %eax
	incl	%eax
	movl	%eax, -576(%rdi)
	retq
.Lfunc_end39:
	.size	_ZThn592_N7NCrypto6NWzAes8CDecoder6AddRefEv, .Lfunc_end39-_ZThn592_N7NCrypto6NWzAes8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn592_N7NCrypto6NWzAes8CDecoder7ReleaseEv,"axG",@progbits,_ZThn592_N7NCrypto6NWzAes8CDecoder7ReleaseEv,comdat
	.weak	_ZThn592_N7NCrypto6NWzAes8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn592_N7NCrypto6NWzAes8CDecoder7ReleaseEv,@function
_ZThn592_N7NCrypto6NWzAes8CDecoder7ReleaseEv: # @_ZThn592_N7NCrypto6NWzAes8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi118:
	.cfi_def_cfa_offset 16
	movl	-576(%rdi), %eax
	decl	%eax
	movl	%eax, -576(%rdi)
	jne	.LBB40_2
# BB#1:
	addq	$-592, %rdi             # imm = 0xFDB0
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB40_2:                               # %_ZN7NCrypto6NWzAes8CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end40:
	.size	_ZThn592_N7NCrypto6NWzAes8CDecoder7ReleaseEv, .Lfunc_end40-_ZThn592_N7NCrypto6NWzAes8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn592_N7NCrypto6NWzAes8CDecoderD1Ev,"axG",@progbits,_ZThn592_N7NCrypto6NWzAes8CDecoderD1Ev,comdat
	.weak	_ZThn592_N7NCrypto6NWzAes8CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn592_N7NCrypto6NWzAes8CDecoderD1Ev,@function
_ZThn592_N7NCrypto6NWzAes8CDecoderD1Ev: # @_ZThn592_N7NCrypto6NWzAes8CDecoderD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -592(%rdi)
	movq	$_ZTV7CBufferIhE+16, -544(%rdi)
	movq	-528(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB41_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB41_1:                               # %_ZN7NCrypto6NWzAes8CDecoderD2Ev.exit
	retq
.Lfunc_end41:
	.size	_ZThn592_N7NCrypto6NWzAes8CDecoderD1Ev, .Lfunc_end41-_ZThn592_N7NCrypto6NWzAes8CDecoderD1Ev
	.cfi_endproc

	.section	.text._ZThn592_N7NCrypto6NWzAes8CDecoderD0Ev,"axG",@progbits,_ZThn592_N7NCrypto6NWzAes8CDecoderD0Ev,comdat
	.weak	_ZThn592_N7NCrypto6NWzAes8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn592_N7NCrypto6NWzAes8CDecoderD0Ev,@function
_ZThn592_N7NCrypto6NWzAes8CDecoderD0Ev: # @_ZThn592_N7NCrypto6NWzAes8CDecoderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 16
.Lcfi120:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -592(%rax)
	movq	$_ZTV7CBufferIhE+16, -544(%rax)
	movq	-528(%rax), %rdi
	leaq	-592(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB42_2
# BB#1:
	callq	_ZdaPv
.LBB42_2:                               # %_ZN7NCrypto6NWzAes8CDecoderD0Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end42:
	.size	_ZThn592_N7NCrypto6NWzAes8CDecoderD0Ev, .Lfunc_end42-_ZThn592_N7NCrypto6NWzAes8CDecoderD0Ev
	.cfi_endproc

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB43_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB43_1:
	retq
.Lfunc_end43:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end43-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi121:
	.cfi_def_cfa_offset 16
.Lcfi122:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB44_2
# BB#1:
	callq	_ZdaPv
.LBB44_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end44:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end44-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB45_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB45_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB45_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB45_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB45_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB45_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB45_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB45_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB45_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB45_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB45_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB45_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB45_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB45_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB45_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB45_16:
	xorl	%eax, %eax
	retq
.Lfunc_end45:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end45-_ZeqRK4GUIDS1_
	.cfi_endproc

	.type	_ZTVN7NCrypto6NWzAes10CBaseCoderE,@object # @_ZTVN7NCrypto6NWzAes10CBaseCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN7NCrypto6NWzAes10CBaseCoderE
	.p2align	3
_ZTVN7NCrypto6NWzAes10CBaseCoderE:
	.quad	0
	.quad	_ZTIN7NCrypto6NWzAes10CBaseCoderE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN7NCrypto6NWzAes10CBaseCoderD2Ev
	.quad	_ZN7NCrypto6NWzAes10CBaseCoderD0Ev
	.quad	_ZN7NCrypto6NWzAes10CBaseCoder4InitEv
	.quad	__cxa_pure_virtual
	.quad	_ZN7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj
	.quad	-8
	.quad	_ZTIN7NCrypto6NWzAes10CBaseCoderE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZThn8_N7NCrypto6NWzAes10CBaseCoderD1Ev
	.quad	_ZThn8_N7NCrypto6NWzAes10CBaseCoderD0Ev
	.quad	_ZThn8_N7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj
	.size	_ZTVN7NCrypto6NWzAes10CBaseCoderE, 144

	.type	_ZTSN7NCrypto6NWzAes10CBaseCoderE,@object # @_ZTSN7NCrypto6NWzAes10CBaseCoderE
	.globl	_ZTSN7NCrypto6NWzAes10CBaseCoderE
	.p2align	4
_ZTSN7NCrypto6NWzAes10CBaseCoderE:
	.asciz	"N7NCrypto6NWzAes10CBaseCoderE"
	.size	_ZTSN7NCrypto6NWzAes10CBaseCoderE, 30

	.type	_ZTS15ICompressFilter,@object # @_ZTS15ICompressFilter
	.section	.rodata._ZTS15ICompressFilter,"aG",@progbits,_ZTS15ICompressFilter,comdat
	.weak	_ZTS15ICompressFilter
	.p2align	4
_ZTS15ICompressFilter:
	.asciz	"15ICompressFilter"
	.size	_ZTS15ICompressFilter, 18

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI15ICompressFilter,@object # @_ZTI15ICompressFilter
	.section	.rodata._ZTI15ICompressFilter,"aG",@progbits,_ZTI15ICompressFilter,comdat
	.weak	_ZTI15ICompressFilter
	.p2align	4
_ZTI15ICompressFilter:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15ICompressFilter
	.quad	_ZTI8IUnknown
	.size	_ZTI15ICompressFilter, 24

	.type	_ZTS18ICryptoSetPassword,@object # @_ZTS18ICryptoSetPassword
	.section	.rodata._ZTS18ICryptoSetPassword,"aG",@progbits,_ZTS18ICryptoSetPassword,comdat
	.weak	_ZTS18ICryptoSetPassword
	.p2align	4
_ZTS18ICryptoSetPassword:
	.asciz	"18ICryptoSetPassword"
	.size	_ZTS18ICryptoSetPassword, 21

	.type	_ZTI18ICryptoSetPassword,@object # @_ZTI18ICryptoSetPassword
	.section	.rodata._ZTI18ICryptoSetPassword,"aG",@progbits,_ZTI18ICryptoSetPassword,comdat
	.weak	_ZTI18ICryptoSetPassword
	.p2align	4
_ZTI18ICryptoSetPassword:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18ICryptoSetPassword
	.quad	_ZTI8IUnknown
	.size	_ZTI18ICryptoSetPassword, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN7NCrypto6NWzAes10CBaseCoderE,@object # @_ZTIN7NCrypto6NWzAes10CBaseCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN7NCrypto6NWzAes10CBaseCoderE
	.p2align	4
_ZTIN7NCrypto6NWzAes10CBaseCoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN7NCrypto6NWzAes10CBaseCoderE
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI15ICompressFilter
	.quad	2                       # 0x2
	.quad	_ZTI18ICryptoSetPassword
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTIN7NCrypto6NWzAes10CBaseCoderE, 72

	.type	_ZTVN7NCrypto6NWzAes8CEncoderE,@object # @_ZTVN7NCrypto6NWzAes8CEncoderE
	.globl	_ZTVN7NCrypto6NWzAes8CEncoderE
	.p2align	3
_ZTVN7NCrypto6NWzAes8CEncoderE:
	.quad	0
	.quad	_ZTIN7NCrypto6NWzAes8CEncoderE
	.quad	_ZN7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN7NCrypto6NWzAes8CEncoder6AddRefEv
	.quad	_ZN7NCrypto6NWzAes8CEncoder7ReleaseEv
	.quad	_ZN7NCrypto6NWzAes10CBaseCoderD2Ev
	.quad	_ZN7NCrypto6NWzAes8CEncoderD0Ev
	.quad	_ZN7NCrypto6NWzAes10CBaseCoder4InitEv
	.quad	_ZN7NCrypto6NWzAes8CEncoder6FilterEPhj
	.quad	_ZN7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj
	.quad	-8
	.quad	_ZTIN7NCrypto6NWzAes8CEncoderE
	.quad	_ZThn8_N7NCrypto6NWzAes8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N7NCrypto6NWzAes8CEncoder6AddRefEv
	.quad	_ZThn8_N7NCrypto6NWzAes8CEncoder7ReleaseEv
	.quad	_ZThn8_N7NCrypto6NWzAes8CEncoderD1Ev
	.quad	_ZThn8_N7NCrypto6NWzAes8CEncoderD0Ev
	.quad	_ZThn8_N7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj
	.size	_ZTVN7NCrypto6NWzAes8CEncoderE, 144

	.type	_ZTSN7NCrypto6NWzAes8CEncoderE,@object # @_ZTSN7NCrypto6NWzAes8CEncoderE
	.globl	_ZTSN7NCrypto6NWzAes8CEncoderE
	.p2align	4
_ZTSN7NCrypto6NWzAes8CEncoderE:
	.asciz	"N7NCrypto6NWzAes8CEncoderE"
	.size	_ZTSN7NCrypto6NWzAes8CEncoderE, 27

	.type	_ZTIN7NCrypto6NWzAes8CEncoderE,@object # @_ZTIN7NCrypto6NWzAes8CEncoderE
	.globl	_ZTIN7NCrypto6NWzAes8CEncoderE
	.p2align	4
_ZTIN7NCrypto6NWzAes8CEncoderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7NCrypto6NWzAes8CEncoderE
	.quad	_ZTIN7NCrypto6NWzAes10CBaseCoderE
	.size	_ZTIN7NCrypto6NWzAes8CEncoderE, 24

	.type	_ZTVN7NCrypto6NWzAes8CDecoderE,@object # @_ZTVN7NCrypto6NWzAes8CDecoderE
	.globl	_ZTVN7NCrypto6NWzAes8CDecoderE
	.p2align	3
_ZTVN7NCrypto6NWzAes8CDecoderE:
	.quad	0
	.quad	_ZTIN7NCrypto6NWzAes8CDecoderE
	.quad	_ZN7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN7NCrypto6NWzAes8CDecoder6AddRefEv
	.quad	_ZN7NCrypto6NWzAes8CDecoder7ReleaseEv
	.quad	_ZN7NCrypto6NWzAes8CDecoderD2Ev
	.quad	_ZN7NCrypto6NWzAes8CDecoderD0Ev
	.quad	_ZN7NCrypto6NWzAes10CBaseCoder4InitEv
	.quad	_ZN7NCrypto6NWzAes8CDecoder6FilterEPhj
	.quad	_ZN7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj
	.quad	_ZN7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj
	.quad	-8
	.quad	_ZTIN7NCrypto6NWzAes8CDecoderE
	.quad	_ZThn8_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N7NCrypto6NWzAes8CDecoder6AddRefEv
	.quad	_ZThn8_N7NCrypto6NWzAes8CDecoder7ReleaseEv
	.quad	_ZThn8_N7NCrypto6NWzAes8CDecoderD1Ev
	.quad	_ZThn8_N7NCrypto6NWzAes8CDecoderD0Ev
	.quad	_ZThn8_N7NCrypto6NWzAes10CBaseCoder17CryptoSetPasswordEPKhj
	.quad	-592
	.quad	_ZTIN7NCrypto6NWzAes8CDecoderE
	.quad	_ZThn592_N7NCrypto6NWzAes8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn592_N7NCrypto6NWzAes8CDecoder6AddRefEv
	.quad	_ZThn592_N7NCrypto6NWzAes8CDecoder7ReleaseEv
	.quad	_ZThn592_N7NCrypto6NWzAes8CDecoderD1Ev
	.quad	_ZThn592_N7NCrypto6NWzAes8CDecoderD0Ev
	.quad	_ZThn592_N7NCrypto6NWzAes8CDecoder21SetDecoderProperties2EPKhj
	.size	_ZTVN7NCrypto6NWzAes8CDecoderE, 216

	.type	_ZTSN7NCrypto6NWzAes8CDecoderE,@object # @_ZTSN7NCrypto6NWzAes8CDecoderE
	.globl	_ZTSN7NCrypto6NWzAes8CDecoderE
	.p2align	4
_ZTSN7NCrypto6NWzAes8CDecoderE:
	.asciz	"N7NCrypto6NWzAes8CDecoderE"
	.size	_ZTSN7NCrypto6NWzAes8CDecoderE, 27

	.type	_ZTS30ICompressSetDecoderProperties2,@object # @_ZTS30ICompressSetDecoderProperties2
	.section	.rodata._ZTS30ICompressSetDecoderProperties2,"aG",@progbits,_ZTS30ICompressSetDecoderProperties2,comdat
	.weak	_ZTS30ICompressSetDecoderProperties2
	.p2align	4
_ZTS30ICompressSetDecoderProperties2:
	.asciz	"30ICompressSetDecoderProperties2"
	.size	_ZTS30ICompressSetDecoderProperties2, 33

	.type	_ZTI30ICompressSetDecoderProperties2,@object # @_ZTI30ICompressSetDecoderProperties2
	.section	.rodata._ZTI30ICompressSetDecoderProperties2,"aG",@progbits,_ZTI30ICompressSetDecoderProperties2,comdat
	.weak	_ZTI30ICompressSetDecoderProperties2
	.p2align	4
_ZTI30ICompressSetDecoderProperties2:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS30ICompressSetDecoderProperties2
	.quad	_ZTI8IUnknown
	.size	_ZTI30ICompressSetDecoderProperties2, 24

	.type	_ZTIN7NCrypto6NWzAes8CDecoderE,@object # @_ZTIN7NCrypto6NWzAes8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN7NCrypto6NWzAes8CDecoderE
	.p2align	4
_ZTIN7NCrypto6NWzAes8CDecoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN7NCrypto6NWzAes8CDecoderE
	.long	1                       # 0x1
	.long	2                       # 0x2
	.quad	_ZTIN7NCrypto6NWzAes10CBaseCoderE
	.quad	2                       # 0x2
	.quad	_ZTI30ICompressSetDecoderProperties2
	.quad	151554                  # 0x25002
	.size	_ZTIN7NCrypto6NWzAes8CDecoderE, 56

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16


	.globl	_ZN7NCrypto6NWzAes8CAesCtr2C1Ev
	.type	_ZN7NCrypto6NWzAes8CAesCtr2C1Ev,@function
_ZN7NCrypto6NWzAes8CAesCtr2C1Ev = _ZN7NCrypto6NWzAes8CAesCtr2C2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
