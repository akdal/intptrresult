	.text
	.file	"smg_relax.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.text
	.globl	hypre_SMGRelaxCreate
	.p2align	4, 0x90
	.type	hypre_SMGRelaxCreate,@function
hypre_SMGRelaxCreate:                   # @hypre_SMGRelaxCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	$1, %edi
	movl	$208, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movl	$1, (%rbx)
	movl	$1, 4(%rbx)
	movl	$1, 8(%rbx)
	movl	%ebp, 12(%rbx)
	movq	$0, 112(%rbx)
	movl	$.L.str, %edi
	callq	hypre_InitializeTiming
	movl	%eax, 196(%rbx)
	movl	$0, 16(%rbx)
	movabsq	$4517329193108106637, %rax # imm = 0x3EB0C6F7A0B5ED8D
	movq	%rax, 24(%rbx)
	movq	$1000, 32(%rbx)         # imm = 0x3E8
	movl	$1, 40(%rbx)
	movl	$4, %edi
	callq	hypre_MAlloc
	movq	%rax, 48(%rbx)
	movl	$4, %edi
	callq	hypre_MAlloc
	movq	%rax, 56(%rbx)
	movq	48(%rbx), %rcx
	movl	$0, (%rcx)
	movl	$1, (%rax)
	movl	$0, 64(%rbx)
	movl	$1, 68(%rbx)
	movq	$0, 72(%rbx)
	movl	$4, %edi
	callq	hypre_MAlloc
	movq	%rax, 80(%rbx)
	movl	$0, (%rax)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,0,0,1]
	movups	%xmm0, 88(%rbx)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 104(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movq	%rax, 200(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_SMGRelaxCreate, .Lfunc_end0-hypre_SMGRelaxCreate
	.cfi_endproc

	.globl	hypre_SMGRelaxDestroyTempVec
	.p2align	4, 0x90
	.type	hypre_SMGRelaxDestroyTempVec,@function
hypre_SMGRelaxDestroyTempVec:           # @hypre_SMGRelaxDestroyTempVec
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	152(%rbx), %rdi
	callq	hypre_StructVectorDestroy
	movl	$1, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	hypre_SMGRelaxDestroyTempVec, .Lfunc_end1-hypre_SMGRelaxDestroyTempVec
	.cfi_endproc

	.globl	hypre_SMGRelaxDestroyARem
	.p2align	4, 0x90
	.type	hypre_SMGRelaxDestroyARem,@function
hypre_SMGRelaxDestroyARem:              # @hypre_SMGRelaxDestroyARem
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpq	$0, 168(%r14)
	je	.LBB2_5
# BB#1:                                 # %.preheader
	movq	176(%r14), %rdi
	cmpl	$0, 40(%r14)
	jle	.LBB2_4
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rdi
	callq	hypre_SMGResidualDestroy
	incq	%rbx
	movslq	40(%r14), %rax
	movq	176(%r14), %rdi
	cmpq	%rax, %rbx
	jl	.LBB2_3
.LBB2_4:                                # %._crit_edge
	callq	hypre_Free
	movq	$0, 176(%r14)
	movq	168(%r14), %rdi
	callq	hypre_StructMatrixDestroy
	movq	$0, 168(%r14)
.LBB2_5:
	movl	$1, 4(%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	hypre_SMGRelaxDestroyARem, .Lfunc_end2-hypre_SMGRelaxDestroyARem
	.cfi_endproc

	.globl	hypre_SMGRelaxDestroyASol
	.p2align	4, 0x90
	.type	hypre_SMGRelaxDestroyASol,@function
hypre_SMGRelaxDestroyASol:              # @hypre_SMGRelaxDestroyASol
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpq	$0, 160(%r15)
	je	.LBB3_9
# BB#1:
	cmpl	$0, 40(%r15)
	jle	.LBB3_2
# BB#3:                                 # %.lr.ph
	leaq	184(%r15), %r14
	cmpl	$2, 120(%r15)
	jle	.LBB3_4
# BB#6:                                 # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	184(%r15), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_SMGDestroy
	incq	%rbx
	movslq	40(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB3_7
	jmp	.LBB3_8
.LBB3_2:                                # %.._crit_edge_crit_edge
	leaq	184(%r15), %r14
	jmp	.LBB3_8
.LBB3_4:                                # %.lr.ph.split.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	184(%r15), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_CyclicReductionDestroy
	incq	%rbx
	movslq	40(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB3_5
.LBB3_8:                                # %._crit_edge
	movq	(%r14), %rdi
	callq	hypre_Free
	movq	$0, 184(%r15)
	movq	160(%r15), %rdi
	callq	hypre_StructMatrixDestroy
	movq	$0, 160(%r15)
.LBB3_9:
	movl	$1, 8(%r15)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	hypre_SMGRelaxDestroyASol, .Lfunc_end3-hypre_SMGRelaxDestroyASol
	.cfi_endproc

	.globl	hypre_SMGRelaxDestroy
	.p2align	4, 0x90
	.type	hypre_SMGRelaxDestroy,@function
hypre_SMGRelaxDestroy:                  # @hypre_SMGRelaxDestroy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB4_7
# BB#1:
	movq	48(%r14), %rdi
	callq	hypre_Free
	movq	$0, 48(%r14)
	movq	56(%r14), %rdi
	callq	hypre_Free
	movq	$0, 56(%r14)
	movq	72(%r14), %rdi
	callq	hypre_Free
	movq	$0, 72(%r14)
	movq	80(%r14), %rdi
	callq	hypre_Free
	movq	$0, 80(%r14)
	movq	112(%r14), %rdi
	callq	hypre_BoxArrayDestroy
	movq	128(%r14), %rdi
	callq	hypre_StructMatrixDestroy
	movq	136(%r14), %rdi
	callq	hypre_StructVectorDestroy
	movq	144(%r14), %rdi
	callq	hypre_StructVectorDestroy
	movq	152(%r14), %rdi
	callq	hypre_StructVectorDestroy
	movl	$1, (%r14)
	cmpq	$0, 168(%r14)
	je	.LBB4_6
# BB#2:                                 # %.preheader.i
	movq	176(%r14), %rdi
	cmpl	$0, 40(%r14)
	jle	.LBB4_5
# BB#3:                                 # %.lr.ph.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_4:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rdi
	callq	hypre_SMGResidualDestroy
	incq	%rbx
	movslq	40(%r14), %rax
	movq	176(%r14), %rdi
	cmpq	%rax, %rbx
	jl	.LBB4_4
.LBB4_5:                                # %._crit_edge.i
	callq	hypre_Free
	movq	$0, 176(%r14)
	movq	168(%r14), %rdi
	callq	hypre_StructMatrixDestroy
	movq	$0, 168(%r14)
.LBB4_6:                                # %hypre_SMGRelaxDestroyARem.exit
	movl	$1, 4(%r14)
	movq	%r14, %rdi
	callq	hypre_SMGRelaxDestroyASol
	movl	196(%r14), %edi
	callq	hypre_FinalizeTiming
	movq	%r14, %rdi
	callq	hypre_Free
.LBB4_7:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	hypre_SMGRelaxDestroy, .Lfunc_end4-hypre_SMGRelaxDestroy
	.cfi_endproc

	.globl	hypre_SMGRelax
	.p2align	4, 0x90
	.type	hypre_SMGRelax,@function
hypre_SMGRelax:                         # @hypre_SMGRelax
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 160
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	196(%r15), %edi
	callq	hypre_BeginTiming
	cmpl	$0, 8(%r15)
	jle	.LBB5_2
# BB#1:
	movl	$2, 8(%r15)
.LBB5_2:
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	callq	hypre_SMGRelaxSetup
	movl	120(%r15), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	152(%r15), %r12
	movq	160(%r15), %r14
	movq	168(%r15), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	176(%r15), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	184(%r15), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpl	$0, 36(%r15)
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	je	.LBB5_3
# BB#4:
	leaq	100(%r15), %rdx
	movq	112(%r15), %rsi
	xorps	%xmm0, %xmm0
	movq	%r13, %rdi
	callq	hypre_SMGSetStructVectorConstantValues
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jmp	.LBB5_5
.LBB5_3:
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
.LBB5_5:                                # %.preheader.lr.ph
	movl	64(%r15), %eax
	testl	%eax, %eax
	movq	%r14, 32(%rsp)          # 8-byte Spill
	jle	.LBB5_11
# BB#6:                                 # %.preheader.us
	movq	72(%r15), %rbx
	cmpl	$2, (%rsp)              # 4-byte Folded Reload
	jle	.LBB5_7
	.p2align	4, 0x90
.LBB5_10:                               # %.lr.ph.split.us.us
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rbx), %r14
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r14,8), %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r13, %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%r12, %r8
	movq	%rbx, %rbp
	movq	%rax, %rbx
	callq	hypre_SMGResidual
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r14,8), %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	movq	%r13, %rcx
	callq	hypre_SMGSolve
	movq	%rbx, %rax
	movq	%rbp, %rbx
	addq	$4, %rbx
	decq	%rax
	jne	.LBB5_10
	jmp	.LBB5_11
	.p2align	4, 0x90
.LBB5_7:                                # %.lr.ph..lr.ph.split_crit_edge.us
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rbx), %r14
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r14,8), %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r13, %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%r12, %r8
	movq	%r13, %rbp
	movq	%rax, %r13
	callq	hypre_SMGResidual
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r14,8), %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	movq	%rbp, %rcx
	callq	hypre_CyclicReduction
	movq	%r13, %rax
	movq	%rbp, %r13
	addq	$4, %rbx
	decq	%rax
	jne	.LBB5_7
.LBB5_11:                               # %._crit_edge105.loopexit114
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%r13, 64(%rsp)          # 8-byte Spill
	movl	$1, 192(%r15)
	movl	32(%r15), %esi
	testl	%esi, %esi
	jle	.LBB5_23
# BB#12:                                # %.preheader.lr.ph.1
	movl	68(%r15), %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	jle	.LBB5_13
# BB#18:                                # %.preheader.us.1.preheader
	movq	80(%r15), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%r15, 88(%rsp)          # 8-byte Spill
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_19:                               # %.preheader.us.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_20 Depth 2
                                        #     Child Loop BB5_21 Depth 2
	movl	%eax, 52(%rsp)          # 4-byte Spill
	cmpl	$2, (%rsp)              # 4-byte Folded Reload
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	%r12, %r15
	movq	96(%rsp), %r14          # 8-byte Reload
	movq	%r14, %r13
	jle	.LBB5_20
	.p2align	4, 0x90
.LBB5_21:                               # %.lr.ph.split.us.us.1
                                        #   Parent Loop BB5_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%r12), %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r8
	callq	hypre_SMGResidual
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	hypre_SMGSolve
	addq	$4, %r12
	decq	%r14
	jne	.LBB5_21
	jmp	.LBB5_22
	.p2align	4, 0x90
.LBB5_20:                               # %.lr.ph..lr.ph.split_crit_edge.us.1
                                        #   Parent Loop BB5_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%r15), %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r8
	callq	hypre_SMGResidual
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	hypre_CyclicReduction
	addq	$4, %r15
	decq	%r13
	jne	.LBB5_20
.LBB5_22:                               # %._crit_edge.us.1
                                        #   in Loop: Header=BB5_19 Depth=1
	movl	52(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movq	88(%rsp), %r15          # 8-byte Reload
	movl	%eax, 192(%r15)
	movq	80(%rsp), %rsi          # 8-byte Reload
	cmpl	%esi, %eax
	jne	.LBB5_19
	jmp	.LBB5_23
.LBB5_13:                               # %.preheader.1.preheader
	leal	-1(%rsi), %ecx
	movl	%esi, %edx
	xorl	%eax, %eax
	andl	$7, %edx
	je	.LBB5_15
	.p2align	4, 0x90
.LBB5_14:                               # %.preheader.1.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	cmpl	%eax, %edx
	jne	.LBB5_14
.LBB5_15:                               # %.preheader.1.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB5_17
	.p2align	4, 0x90
.LBB5_16:                               # %.preheader.1
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %eax
	cmpl	%eax, %esi
	jne	.LBB5_16
.LBB5_17:                               # %._crit_edge105.1.loopexit123
	movl	%eax, 192(%r15)
.LBB5_23:                               # %._crit_edge105.1
	movl	(%rsp), %eax            # 4-byte Reload
	decl	%eax
	cmpl	16(%r15), %eax
	jg	.LBB5_9
# BB#8:
	movq	%r15, %rdi
	callq	hypre_SMGRelaxDestroyASol
.LBB5_9:
	movl	196(%r15), %edi
	callq	hypre_EndTiming
	movl	4(%rsp), %eax           # 4-byte Reload
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	hypre_SMGRelax, .Lfunc_end5-hypre_SMGRelax
	.cfi_endproc

	.globl	hypre_SMGRelaxSetup
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetup,@function
hypre_SMGRelaxSetup:                    # @hypre_SMGRelaxSetup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 64
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	24(%r15), %rax
	movl	16(%rax), %ebp
	movl	%ebp, 120(%rbx)
	movq	128(%rbx), %rdi
	callq	hypre_StructMatrixDestroy
	movq	136(%rbx), %rdi
	callq	hypre_StructVectorDestroy
	movq	144(%rbx), %rdi
	callq	hypre_StructVectorDestroy
	movq	%r15, %rdi
	callq	hypre_StructMatrixRef
	movq	%rax, 128(%rbx)
	movq	%r12, %rdi
	callq	hypre_StructVectorRef
	movq	%rax, 136(%rbx)
	movq	%r13, %rdi
	callq	hypre_StructVectorRef
	movq	%rax, 144(%rbx)
	decl	%ebp
	xorl	%r14d, %r14d
	cmpl	16(%rbx), %ebp
	setle	%bpl
	cmpl	$0, (%rbx)
	jle	.LBB6_4
# BB#1:
	cmpq	$0, 152(%rbx)
	jne	.LBB6_3
# BB#2:
	movl	(%r12), %edi
	movq	8(%r12), %rsi
	callq	hypre_StructVectorCreate
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rax, %r13
	leaq	48(%r12), %rsi
	movq	%r13, %rdi
	callq	hypre_StructVectorSetNumGhost
	movq	%r13, %rdi
	callq	hypre_StructVectorInitialize
	movq	%r13, %rdi
	callq	hypre_StructVectorAssemble
	movq	%r13, 152(%rbx)
	movq	(%rsp), %r13            # 8-byte Reload
.LBB6_3:                                # %hypre_SMGRelaxSetupTempVec.exit
	movl	$0, (%rbx)
.LBB6_4:
	movb	%bpl, %r14b
	cmpl	$0, 4(%rbx)
	jle	.LBB6_6
# BB#5:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r13, %rcx
	callq	hypre_SMGRelaxSetupARem
.LBB6_6:
	cmpl	%r14d, 8(%rbx)
	jle	.LBB6_8
# BB#7:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r13, %rcx
	callq	hypre_SMGRelaxSetupASol
.LBB6_8:
	cmpq	$0, 112(%rbx)
	jne	.LBB6_10
# BB#9:
	movq	8(%r13), %rax
	movq	8(%rax), %rdi
	callq	hypre_BoxArrayDuplicate
	movq	%rax, %r14
	leaq	88(%rbx), %rsi
	leaq	100(%rbx), %rdx
	movq	%r14, %rdi
	callq	hypre_ProjectBoxArray
	movq	%r14, 112(%rbx)
.LBB6_10:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	hypre_SMGRelaxSetup, .Lfunc_end6-hypre_SMGRelaxSetup
	.cfi_endproc

	.globl	hypre_SMGRelaxSetupTempVec
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetupTempVec,@function
hypre_SMGRelaxSetupTempVec:             # @hypre_SMGRelaxSetupTempVec
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -32
.Lcfi53:
	.cfi_offset %r14, -24
.Lcfi54:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rdi, %r14
	cmpq	$0, 152(%r14)
	jne	.LBB7_2
# BB#1:
	movl	(%rbx), %edi
	movq	8(%rbx), %rsi
	callq	hypre_StructVectorCreate
	movq	%rax, %r15
	addq	$48, %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	hypre_StructVectorSetNumGhost
	movq	%r15, %rdi
	callq	hypre_StructVectorInitialize
	movq	%r15, %rdi
	callq	hypre_StructVectorAssemble
	movq	%r15, 152(%r14)
.LBB7_2:
	movl	$0, (%r14)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	hypre_SMGRelaxSetupTempVec, .Lfunc_end7-hypre_SMGRelaxSetupTempVec
	.cfi_endproc

	.globl	hypre_SMGRelaxSetupARem
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetupARem,@function
hypre_SMGRelaxSetupARem:                # @hypre_SMGRelaxSetupARem
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi61:
	.cfi_def_cfa_offset 144
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	movl	40(%rbp), %r15d
	movq	48(%rbp), %r12
	movq	56(%rbp), %r13
	movq	152(%rbp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	24(%rsi), %rax
	movq	(%rax), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	8(%rax), %ecx
	movslq	16(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	$0, 168(%rbp)
	je	.LBB8_5
# BB#1:                                 # %.preheader.i
	movq	176(%rbp), %rdi
	testl	%r15d, %r15d
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	jle	.LBB8_4
# BB#2:                                 # %.lr.ph.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rdi
	callq	hypre_SMGResidualDestroy
	incq	%rbx
	movslq	40(%rbp), %rax
	movq	176(%rbp), %rdi
	cmpq	%rax, %rbx
	jl	.LBB8_3
.LBB8_4:                                # %._crit_edge.i
	callq	hypre_Free
	movq	$0, 176(%rbp)
	movq	168(%rbp), %rdi
	callq	hypre_StructMatrixDestroy
	movq	$0, 168(%rbp)
	movq	48(%rsp), %rcx          # 8-byte Reload
.LBB8_5:                                # %hypre_SMGRelaxDestroyARem.exit
	movq	%r14, 72(%rsp)          # 8-byte Spill
	movl	$1, 4(%rbp)
	movl	88(%rbp), %eax
	movl	%eax, 36(%rsp)
	movl	92(%rbp), %eax
	movl	%eax, 40(%rsp)
	movl	96(%rbp), %eax
	movl	%eax, 44(%rsp)
	movl	100(%rbp), %eax
	movl	%eax, 24(%rsp)
	movl	104(%rbp), %eax
	movl	%eax, 28(%rsp)
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movl	108(%rbp), %eax
	movl	%eax, 32(%rsp)
	leal	(,%rcx,4), %edi
	movq	%rcx, %rbx
	callq	hypre_MAlloc
	movq	%rax, %r14
	testl	%ebx, %ebx
	jle	.LBB8_6
# BB#11:                                # %.lr.ph90
	testb	$1, %bl
	jne	.LBB8_13
# BB#12:
	xorl	%eax, %eax
                                        # implicit-def: %ESI
	xorl	%ebp, %ebp
	movq	(%rsp), %rdi            # 8-byte Reload
	cmpl	$1, %ebx
	jne	.LBB8_17
	jmp	.LBB8_7
.LBB8_6:
	xorl	%esi, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	jmp	.LBB8_7
.LBB8_13:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	$0, -4(%rcx,%rax,4)
	movq	(%rsp), %rdi            # 8-byte Reload
	je	.LBB8_14
# BB#15:
	movl	$0, (%r14)
	movl	$1, %esi
	movl	$1, %eax
	movl	$1, %ebp
	cmpl	$1, %ebx
	jne	.LBB8_17
	jmp	.LBB8_7
.LBB8_14:
	xorl	%esi, %esi
	movl	$1, %eax
	xorl	%ebp, %ebp
	cmpl	$1, %ebx
	je	.LBB8_7
.LBB8_17:                               # %.lr.ph90.new
	movq	16(%rsp), %rcx          # 8-byte Reload
	decq	%rcx
	subq	%rax, %rbx
	leaq	(%rax,%rax,2), %rdx
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rsi,%rdx,4), %rdx
	leaq	(%rdx,%rcx,4), %rcx
	xorl	%edx, %edx
	movl	%ebp, %esi
	.p2align	4, 0x90
.LBB8_18:                               # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rcx)
	je	.LBB8_20
# BB#19:                                #   in Loop: Header=BB8_18 Depth=1
	movslq	%esi, %rsi
	leal	(%rax,%rdx), %ebp
	movl	%ebp, (%r14,%rsi,4)
	incl	%esi
.LBB8_20:                               #   in Loop: Header=BB8_18 Depth=1
	cmpl	$0, 12(%rcx)
	je	.LBB8_22
# BB#21:                                #   in Loop: Header=BB8_18 Depth=1
	movslq	%esi, %rsi
	leal	1(%rax,%rdx), %ebp
	movl	%ebp, (%r14,%rsi,4)
	incl	%esi
.LBB8_22:                               #   in Loop: Header=BB8_18 Depth=1
	addq	$2, %rdx
	addq	$24, %rcx
	cmpq	%rdx, %rbx
	jne	.LBB8_18
.LBB8_7:                                # %._crit_edge91
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r14, %rdx
	callq	hypre_StructMatrixCreateMask
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r14, %rdi
	callq	hypre_Free
	leal	(,%r15,8), %edi
	callq	hypre_MAlloc
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testl	%r15d, %r15d
	movq	56(%rsp), %rbp          # 8-byte Reload
	jle	.LBB8_10
# BB#8:                                 # %.lr.ph
	movq	16(%rsp), %rax          # 8-byte Reload
	decl	%eax
	movslq	%eax, %r14
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB8_9:                                # =>This Inner Loop Header: Depth=1
	movl	(%r12), %eax
	movl	%eax, 36(%rsp,%r14,4)
	movl	(%r13), %eax
	movl	%eax, 24(%rsp,%r14,4)
	callq	hypre_SMGResidualCreate
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	leaq	36(%rsp), %rsi
	leaq	24(%rsp), %rdx
	callq	hypre_SMGResidualSetBase
	movq	(%rbx), %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, %r8
	callq	hypre_SMGResidualSetup
	addq	$4, %r12
	addq	$4, %r13
	addq	$8, %rbx
	decq	%r15
	jne	.LBB8_9
.LBB8_10:                               # %._crit_edge
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, 168(%rax)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 176(%rax)
	movl	$0, 4(%rax)
	xorl	%eax, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	hypre_SMGRelaxSetupARem, .Lfunc_end8-hypre_SMGRelaxSetupARem
	.cfi_endproc

	.globl	hypre_SMGRelaxSetupASol
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetupASol,@function
hypre_SMGRelaxSetupASol:                # @hypre_SMGRelaxSetupASol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 144
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	movl	40(%r12), %r14d
	movq	48(%r12), %r15
	movq	56(%r12), %r13
	movq	152(%r12), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	200(%r12), %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movl	204(%r12), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	24(%rsi), %rax
	movq	(%rax), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	8(%rax), %ebp
	movslq	16(%rax), %rbx
	callq	hypre_SMGRelaxDestroyASol
	movl	88(%r12), %eax
	movl	%eax, 36(%rsp)
	movl	92(%r12), %eax
	movl	%eax, 40(%rsp)
	movl	96(%r12), %eax
	movl	%eax, 44(%rsp)
	movl	100(%r12), %eax
	movl	%eax, 24(%rsp)
	movl	104(%r12), %eax
	movl	%eax, 28(%rsp)
	movl	108(%r12), %eax
	movl	%eax, 32(%rsp)
	leal	(,%rbp,4), %edi
	callq	hypre_MAlloc
	movq	%rax, %r9
	movq	%rbx, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	-1(%rbx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	jle	.LBB9_1
# BB#6:                                 # %.lr.ph122
	testb	$1, %bpl
	jne	.LBB9_8
# BB#7:
	xorl	%eax, %eax
                                        # implicit-def: %ESI
	xorl	%r8d, %r8d
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpl	$1, %ebp
	jne	.LBB9_12
	jmp	.LBB9_2
.LBB9_1:
	xorl	%esi, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB9_2
.LBB9_8:
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, -4(%rcx,%rax,4)
	movq	8(%rsp), %rdi           # 8-byte Reload
	je	.LBB9_10
# BB#9:
	xorl	%esi, %esi
	movl	$1, %eax
	xorl	%r8d, %r8d
	cmpl	$1, %ebp
	jne	.LBB9_12
	jmp	.LBB9_2
.LBB9_10:
	movl	$0, (%r9)
	movl	$1, %esi
	movl	$1, %eax
	movl	$1, %r8d
	cmpl	$1, %ebp
	je	.LBB9_2
.LBB9_12:                               # %.lr.ph122.new
	subq	%rax, %rbp
	leaq	(%rax,%rax,2), %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rcx
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	(%rcx,%rdx,4), %rcx
	xorl	%edx, %edx
	movl	%r8d, %esi
	.p2align	4, 0x90
.LBB9_13:                               # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rcx)
	jne	.LBB9_15
# BB#14:                                #   in Loop: Header=BB9_13 Depth=1
	movslq	%esi, %rsi
	leal	(%rax,%rdx), %ebx
	movl	%ebx, (%r9,%rsi,4)
	incl	%esi
.LBB9_15:                               #   in Loop: Header=BB9_13 Depth=1
	cmpl	$0, 12(%rcx)
	jne	.LBB9_17
# BB#16:                                #   in Loop: Header=BB9_13 Depth=1
	movslq	%esi, %rsi
	leal	1(%rax,%rdx), %ebx
	movl	%ebx, (%r9,%rsi,4)
	incl	%esi
.LBB9_17:                               #   in Loop: Header=BB9_13 Depth=1
	addq	$2, %rdx
	addq	$24, %rcx
	cmpq	%rdx, %rbp
	jne	.LBB9_13
.LBB9_2:                                # %._crit_edge123
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r9, %rdx
	movq	%r9, %rbx
	callq	hypre_StructMatrixCreateMask
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%rax), %rax
	movq	64(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, 16(%rax)
	movq	%rbx, %rdi
	callq	hypre_Free
	leal	(,%r14,8), %edi
	callq	hypre_MAlloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	movq	%r12, %rbx
	jle	.LBB9_20
# BB#3:                                 # %.lr.ph
	movslq	%ebp, %r12
	cmpl	$2, 56(%rsp)            # 4-byte Folded Reload
	jle	.LBB9_4
# BB#18:                                # %.lr.ph.split.us.preheader
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB9_19:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15), %eax
	movl	%eax, 36(%rsp,%r12,4)
	movl	(%r13), %eax
	movl	%eax, 24(%rsp,%r12,4)
	movl	12(%rbx), %edi
	callq	hypre_SMGCreate
	movq	%rax, (%rbp)
	movq	%rax, %rdi
	movl	52(%rsp), %esi          # 4-byte Reload
	callq	hypre_SMGSetNumPreRelax
	movq	(%rbp), %rdi
	movl	48(%rsp), %esi          # 4-byte Reload
	callq	hypre_SMGSetNumPostRelax
	movq	(%rbp), %rdi
	leaq	36(%rsp), %rsi
	leaq	24(%rsp), %rdx
	callq	hypre_SMGSetBase
	movq	(%rbp), %rdi
	movl	16(%rbx), %esi
	callq	hypre_SMGSetMemoryUse
	movq	(%rbp), %rdi
	xorps	%xmm0, %xmm0
	callq	hypre_SMGSetTol
	movq	(%rbp), %rdi
	movl	$1, %esi
	callq	hypre_SMGSetMaxIter
	movq	(%rbp), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	callq	hypre_SMGSetup
	addq	$4, %r15
	addq	$4, %r13
	addq	$8, %rbp
	decq	%r14
	jne	.LBB9_19
	jmp	.LBB9_20
.LBB9_4:
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB9_5:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15), %eax
	movl	%eax, 36(%rsp,%r12,4)
	movl	(%r13), %eax
	movl	%eax, 24(%rsp,%r12,4)
	movl	12(%rbx), %edi
	callq	hypre_CyclicReductionCreate
	movq	%rax, (%rbp)
	movq	%rax, %rdi
	leaq	36(%rsp), %rsi
	leaq	24(%rsp), %rdx
	callq	hypre_CyclicReductionSetBase
	movq	(%rbp), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	callq	hypre_CyclicReductionSetup
	addq	$4, %r15
	addq	$4, %r13
	addq	$8, %rbp
	decq	%r14
	jne	.LBB9_5
.LBB9_20:                               # %._crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 160(%rbx)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 184(%rbx)
	movl	$0, 8(%rbx)
	xorl	%eax, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	hypre_SMGRelaxSetupASol, .Lfunc_end9-hypre_SMGRelaxSetupASol
	.cfi_endproc

	.globl	hypre_SMGRelaxSetupBaseBoxArray
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetupBaseBoxArray,@function
hypre_SMGRelaxSetupBaseBoxArray:        # @hypre_SMGRelaxSetupBaseBoxArray
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi83:
	.cfi_def_cfa_offset 32
.Lcfi84:
	.cfi_offset %rbx, -24
.Lcfi85:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rcx), %rax
	movq	8(%rax), %rdi
	callq	hypre_BoxArrayDuplicate
	movq	%rax, %r14
	leaq	88(%rbx), %rsi
	leaq	100(%rbx), %rdx
	movq	%r14, %rdi
	callq	hypre_ProjectBoxArray
	movq	%r14, 112(%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	hypre_SMGRelaxSetupBaseBoxArray, .Lfunc_end10-hypre_SMGRelaxSetupBaseBoxArray
	.cfi_endproc

	.globl	hypre_SMGRelaxSetTempVec
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetTempVec,@function
hypre_SMGRelaxSetTempVec:               # @hypre_SMGRelaxSetTempVec
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 32
.Lcfi89:
	.cfi_offset %rbx, -24
.Lcfi90:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	152(%rbx), %rdi
	callq	hypre_StructVectorDestroy
	movl	$1, (%rbx)
	movq	%r14, %rdi
	callq	hypre_StructVectorRef
	movq	%rax, 152(%rbx)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, (%rbx)
	movl	$1, 8(%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	hypre_SMGRelaxSetTempVec, .Lfunc_end11-hypre_SMGRelaxSetTempVec
	.cfi_endproc

	.globl	hypre_SMGRelaxSetMemoryUse
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetMemoryUse,@function
hypre_SMGRelaxSetMemoryUse:             # @hypre_SMGRelaxSetMemoryUse
	.cfi_startproc
# BB#0:
	movl	%esi, 16(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end12:
	.size	hypre_SMGRelaxSetMemoryUse, .Lfunc_end12-hypre_SMGRelaxSetMemoryUse
	.cfi_endproc

	.globl	hypre_SMGRelaxSetTol
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetTol,@function
hypre_SMGRelaxSetTol:                   # @hypre_SMGRelaxSetTol
	.cfi_startproc
# BB#0:
	movsd	%xmm0, 24(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end13:
	.size	hypre_SMGRelaxSetTol, .Lfunc_end13-hypre_SMGRelaxSetTol
	.cfi_endproc

	.globl	hypre_SMGRelaxSetMaxIter
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetMaxIter,@function
hypre_SMGRelaxSetMaxIter:               # @hypre_SMGRelaxSetMaxIter
	.cfi_startproc
# BB#0:
	movl	%esi, 32(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end14:
	.size	hypre_SMGRelaxSetMaxIter, .Lfunc_end14-hypre_SMGRelaxSetMaxIter
	.cfi_endproc

	.globl	hypre_SMGRelaxSetZeroGuess
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetZeroGuess,@function
hypre_SMGRelaxSetZeroGuess:             # @hypre_SMGRelaxSetZeroGuess
	.cfi_startproc
# BB#0:
	movl	%esi, 36(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end15:
	.size	hypre_SMGRelaxSetZeroGuess, .Lfunc_end15-hypre_SMGRelaxSetZeroGuess
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI16_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI16_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI16_2:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI16_3:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	hypre_SMGRelaxSetNumSpaces
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetNumSpaces,@function
hypre_SMGRelaxSetNumSpaces:             # @hypre_SMGRelaxSetNumSpaces
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 48
.Lcfi96:
	.cfi_offset %rbx, -48
.Lcfi97:
	.cfi_offset %r12, -40
.Lcfi98:
	.cfi_offset %r14, -32
.Lcfi99:
	.cfi_offset %r15, -24
.Lcfi100:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	movl	%r14d, 40(%r15)
	movq	48(%r15), %rdi
	callq	hypre_Free
	movq	$0, 48(%r15)
	movq	56(%r15), %rdi
	callq	hypre_Free
	movq	$0, 56(%r15)
	movq	72(%r15), %rdi
	callq	hypre_Free
	movq	$0, 72(%r15)
	movq	80(%r15), %rdi
	callq	hypre_Free
	movq	$0, 80(%r15)
	leal	(,%r14,4), %ebp
	movl	%ebp, %edi
	callq	hypre_MAlloc
	movq	%rax, 48(%r15)
	movl	%ebp, %edi
	callq	hypre_MAlloc
	movq	%rax, 56(%r15)
	movl	$0, 64(%r15)
	movl	%r14d, 68(%r15)
	movq	$0, 72(%r15)
	movl	%ebp, %edi
	callq	hypre_MAlloc
	movq	%rax, 80(%r15)
	testl	%r14d, %r14d
	jle	.LBB16_16
# BB#1:                                 # %.lr.ph
	movq	48(%r15), %rcx
	movq	56(%r15), %rdx
	movl	%r14d, %r9d
	cmpl	$8, %r14d
	jb	.LBB16_10
# BB#3:                                 # %min.iters.checked
	andl	$7, %r14d
	movq	%r9, %r8
	subq	%r14, %r8
	je	.LBB16_10
# BB#4:                                 # %vector.memcheck
	leaq	(%rcx,%r9,4), %rsi
	leaq	(%rdx,%r9,4), %rdi
	leaq	(%rax,%r9,4), %rbp
	cmpq	%rdi, %rcx
	sbbb	%bl, %bl
	cmpq	%rsi, %rdx
	sbbb	%r12b, %r12b
	andb	%bl, %r12b
	cmpq	%rbp, %rcx
	sbbb	%bl, %bl
	cmpq	%rsi, %rax
	sbbb	%r11b, %r11b
	cmpq	%rbp, %rdx
	sbbb	%sil, %sil
	cmpq	%rdi, %rax
	sbbb	%r10b, %r10b
	xorl	%edi, %edi
	testb	$1, %r12b
	jne	.LBB16_11
# BB#5:                                 # %vector.memcheck
	andb	%r11b, %bl
	andb	$1, %bl
	jne	.LBB16_11
# BB#6:                                 # %vector.memcheck
	andb	%r10b, %sil
	andb	$1, %sil
	jne	.LBB16_11
# BB#7:                                 # %vector.body.preheader
	leaq	16(%rcx), %rdi
	leaq	16(%rdx), %rbp
	leaq	16(%rax), %rsi
	movdqa	.LCPI16_0(%rip), %xmm0  # xmm0 = [0,1,2,3]
	xorps	%xmm1, %xmm1
	movaps	.LCPI16_1(%rip), %xmm2  # xmm2 = [1,1,1,1]
	movdqa	.LCPI16_2(%rip), %xmm3  # xmm3 = [4,4,4,4]
	movdqa	.LCPI16_3(%rip), %xmm4  # xmm4 = [8,8,8,8]
	movq	%r8, %rbx
	.p2align	4, 0x90
.LBB16_8:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm1, -16(%rdi)
	movups	%xmm1, (%rdi)
	movups	%xmm2, -16(%rbp)
	movups	%xmm2, (%rbp)
	movdqa	%xmm0, %xmm5
	paddd	%xmm3, %xmm5
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm5, (%rsi)
	paddd	%xmm4, %xmm0
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rbx
	jne	.LBB16_8
# BB#9:                                 # %middle.block
	testl	%r14d, %r14d
	movq	%r8, %rdi
	jne	.LBB16_11
	jmp	.LBB16_16
.LBB16_10:
	xorl	%edi, %edi
.LBB16_11:                              # %scalar.ph.preheader
	movl	%r9d, %ebp
	subl	%edi, %ebp
	leaq	-1(%r9), %rsi
	subq	%rdi, %rsi
	andq	$3, %rbp
	je	.LBB16_14
# BB#12:                                # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB16_13:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rcx,%rdi,4)
	movl	$1, (%rdx,%rdi,4)
	movl	%edi, (%rax,%rdi,4)
	incq	%rdi
	incq	%rbp
	jne	.LBB16_13
.LBB16_14:                              # %scalar.ph.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB16_16
	.p2align	4, 0x90
.LBB16_15:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rcx,%rdi,4)
	movl	$1, (%rdx,%rdi,4)
	movl	%edi, (%rax,%rdi,4)
	movl	$0, 4(%rcx,%rdi,4)
	movl	$1, 4(%rdx,%rdi,4)
	leal	1(%rdi), %esi
	movl	%esi, 4(%rax,%rdi,4)
	movl	$0, 8(%rcx,%rdi,4)
	movl	$1, 8(%rdx,%rdi,4)
	leal	2(%rdi), %esi
	movl	%esi, 8(%rax,%rdi,4)
	movl	$0, 12(%rcx,%rdi,4)
	movl	$1, 12(%rdx,%rdi,4)
	leal	3(%rdi), %esi
	movl	%esi, 12(%rax,%rdi,4)
	addq	$4, %rdi
	cmpq	%r9, %rdi
	jne	.LBB16_15
.LBB16_16:                              # %._crit_edge
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, (%r15)
	movl	$1, 8(%r15)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	hypre_SMGRelaxSetNumSpaces, .Lfunc_end16-hypre_SMGRelaxSetNumSpaces
	.cfi_endproc

	.globl	hypre_SMGRelaxSetNumPreSpaces
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetNumPreSpaces,@function
hypre_SMGRelaxSetNumPreSpaces:          # @hypre_SMGRelaxSetNumPreSpaces
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 32
.Lcfi104:
	.cfi_offset %rbx, -24
.Lcfi105:
	.cfi_offset %r14, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	%r14d, 64(%rbx)
	movq	72(%rbx), %rdi
	callq	hypre_Free
	movq	$0, 72(%rbx)
	leal	(,%r14,4), %edi
	callq	hypre_MAlloc
	movq	%rax, 72(%rbx)
	testl	%r14d, %r14d
	jle	.LBB17_2
# BB#1:                                 # %.lr.ph
	decl	%r14d
	leaq	4(,%r14,4), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	memset
.LBB17_2:                               # %._crit_edge
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end17:
	.size	hypre_SMGRelaxSetNumPreSpaces, .Lfunc_end17-hypre_SMGRelaxSetNumPreSpaces
	.cfi_endproc

	.globl	hypre_SMGRelaxSetNumRegSpaces
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetNumRegSpaces,@function
hypre_SMGRelaxSetNumRegSpaces:          # @hypre_SMGRelaxSetNumRegSpaces
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi108:
	.cfi_def_cfa_offset 32
.Lcfi109:
	.cfi_offset %rbx, -24
.Lcfi110:
	.cfi_offset %r14, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	%r14d, 68(%rbx)
	movq	80(%rbx), %rdi
	callq	hypre_Free
	movq	$0, 80(%rbx)
	leal	(,%r14,4), %edi
	callq	hypre_MAlloc
	movq	%rax, 80(%rbx)
	testl	%r14d, %r14d
	jle	.LBB18_2
# BB#1:                                 # %.lr.ph
	decl	%r14d
	leaq	4(,%r14,4), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	memset
.LBB18_2:                               # %._crit_edge
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	hypre_SMGRelaxSetNumRegSpaces, .Lfunc_end18-hypre_SMGRelaxSetNumRegSpaces
	.cfi_endproc

	.globl	hypre_SMGRelaxSetSpace
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetSpace,@function
hypre_SMGRelaxSetSpace:                 # @hypre_SMGRelaxSetSpace
	.cfi_startproc
# BB#0:
	movq	48(%rdi), %rax
	movslq	%esi, %rsi
	movl	%edx, (%rax,%rsi,4)
	movq	56(%rdi), %rax
	movl	%ecx, (%rax,%rsi,4)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, (%rdi)
	movl	$1, 8(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end19:
	.size	hypre_SMGRelaxSetSpace, .Lfunc_end19-hypre_SMGRelaxSetSpace
	.cfi_endproc

	.globl	hypre_SMGRelaxSetRegSpaceRank
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetRegSpaceRank,@function
hypre_SMGRelaxSetRegSpaceRank:          # @hypre_SMGRelaxSetRegSpaceRank
	.cfi_startproc
# BB#0:
	movq	80(%rdi), %rax
	movslq	%esi, %rcx
	movl	%edx, (%rax,%rcx,4)
	xorl	%eax, %eax
	retq
.Lfunc_end20:
	.size	hypre_SMGRelaxSetRegSpaceRank, .Lfunc_end20-hypre_SMGRelaxSetRegSpaceRank
	.cfi_endproc

	.globl	hypre_SMGRelaxSetPreSpaceRank
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetPreSpaceRank,@function
hypre_SMGRelaxSetPreSpaceRank:          # @hypre_SMGRelaxSetPreSpaceRank
	.cfi_startproc
# BB#0:
	movq	72(%rdi), %rax
	movslq	%esi, %rcx
	movl	%edx, (%rax,%rcx,4)
	xorl	%eax, %eax
	retq
.Lfunc_end21:
	.size	hypre_SMGRelaxSetPreSpaceRank, .Lfunc_end21-hypre_SMGRelaxSetPreSpaceRank
	.cfi_endproc

	.globl	hypre_SMGRelaxSetBase
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetBase,@function
hypre_SMGRelaxSetBase:                  # @hypre_SMGRelaxSetBase
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 16
.Lcfi112:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	(%rsi), %eax
	movl	%eax, 88(%rbx)
	movl	(%rdx), %eax
	movl	%eax, 100(%rbx)
	movl	4(%rsi), %eax
	movl	%eax, 92(%rbx)
	movl	4(%rdx), %eax
	movl	%eax, 104(%rbx)
	movl	8(%rsi), %eax
	movl	%eax, 96(%rbx)
	movl	8(%rdx), %eax
	movl	%eax, 108(%rbx)
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_2
# BB#1:
	callq	hypre_BoxArrayDestroy
	movq	$0, 112(%rbx)
.LBB22_2:
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, (%rbx)
	movl	$1, 8(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end22:
	.size	hypre_SMGRelaxSetBase, .Lfunc_end22-hypre_SMGRelaxSetBase
	.cfi_endproc

	.globl	hypre_SMGRelaxSetNumPreRelax
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetNumPreRelax,@function
hypre_SMGRelaxSetNumPreRelax:           # @hypre_SMGRelaxSetNumPreRelax
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	movl	$1, %eax
	cmovgl	%esi, %eax
	movl	%eax, 200(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end23:
	.size	hypre_SMGRelaxSetNumPreRelax, .Lfunc_end23-hypre_SMGRelaxSetNumPreRelax
	.cfi_endproc

	.globl	hypre_SMGRelaxSetNumPostRelax
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetNumPostRelax,@function
hypre_SMGRelaxSetNumPostRelax:          # @hypre_SMGRelaxSetNumPostRelax
	.cfi_startproc
# BB#0:
	movl	%esi, 204(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end24:
	.size	hypre_SMGRelaxSetNumPostRelax, .Lfunc_end24-hypre_SMGRelaxSetNumPostRelax
	.cfi_endproc

	.globl	hypre_SMGRelaxSetNewMatrixStencil
	.p2align	4, 0x90
	.type	hypre_SMGRelaxSetNewMatrixStencil,@function
hypre_SMGRelaxSetNewMatrixStencil:      # @hypre_SMGRelaxSetNewMatrixStencil
	.cfi_startproc
# BB#0:
	movl	8(%rsi), %eax
	testl	%eax, %eax
	jle	.LBB25_16
# BB#1:                                 # %.lr.ph
	movq	(%rsi), %rcx
	movslq	16(%rsi), %rdx
	testb	$1, %al
	jne	.LBB25_3
# BB#2:
	xorl	%esi, %esi
	cmpl	$1, %eax
	jne	.LBB25_8
	jmp	.LBB25_16
.LBB25_3:
	cmpl	$0, -4(%rcx,%rdx,4)
	je	.LBB25_5
# BB#4:
	movl	$1, 4(%rdi)
	jmp	.LBB25_6
.LBB25_5:
	movl	$1, 8(%rdi)
.LBB25_6:                               # %.prol.loopexit
	movl	$1, %esi
	cmpl	$1, %eax
	je	.LBB25_16
.LBB25_8:                               # %.lr.ph.new
	decq	%rdx
	subq	%rsi, %rax
	leaq	(%rsi,%rsi,2), %rsi
	leaq	(%rcx,%rsi,4), %rcx
	leaq	(%rcx,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB25_9:                               # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rcx)
	je	.LBB25_11
# BB#10:                                #   in Loop: Header=BB25_9 Depth=1
	movl	$1, 4(%rdi)
	cmpl	$0, 12(%rcx)
	jne	.LBB25_13
	jmp	.LBB25_14
	.p2align	4, 0x90
.LBB25_11:                              #   in Loop: Header=BB25_9 Depth=1
	movl	$1, 8(%rdi)
	cmpl	$0, 12(%rcx)
	je	.LBB25_14
.LBB25_13:                              #   in Loop: Header=BB25_9 Depth=1
	movl	$1, 4(%rdi)
	jmp	.LBB25_15
	.p2align	4, 0x90
.LBB25_14:                              #   in Loop: Header=BB25_9 Depth=1
	movl	$1, 8(%rdi)
.LBB25_15:                              #   in Loop: Header=BB25_9 Depth=1
	addq	$24, %rcx
	addq	$-2, %rax
	jne	.LBB25_9
.LBB25_16:                              # %._crit_edge
	xorl	%eax, %eax
	retq
.Lfunc_end25:
	.size	hypre_SMGRelaxSetNewMatrixStencil, .Lfunc_end25-hypre_SMGRelaxSetNewMatrixStencil
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"SMGRelax"
	.size	.L.str, 9


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
