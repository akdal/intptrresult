	.text
	.file	"terminator.bc"
	.globl	red_Terminator
	.p2align	4, 0x90
	.type	red_Terminator,@function
red_Terminator:                         # @red_Terminator
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rcx, %rbx
	movq	%rdx, %rbp
	movq	%rdi, %r13
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	addl	72(%r13), %eax
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	setg	%cl
	subl	%ecx, %esi
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbx, 8(%r14)
	movq	$0, (%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbp, 8(%r15)
	movq	%r14, (%r15)
	movl	64(%r13), %eax
	addl	68(%r13), %eax
	addl	72(%r13), %eax
	decl	%eax
	js	.LBB0_1
# BB#2:                                 # %.lr.ph.i26
	movq	$-1, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movq	56(%r13), %rax
	movq	8(%rax,%rbx,8), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r12, 8(%rbp)
	movq	%r14, (%rbp)
	movl	64(%r13), %eax
	movl	72(%r13), %ecx
	addl	68(%r13), %eax
	leal	-1(%rcx,%rax), %eax
	cltq
	incq	%rbx
	cmpq	%rax, %rbx
	movq	%rbp, %r14
	jl	.LBB0_3
	jmp	.LBB0_4
.LBB0_1:
	xorl	%ebp, %ebp
.LBB0_4:                                # %clause_GetLiteralList.exit
	movl	52(%r13), %r8d
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	4(%rsp), %edi           # 4-byte Reload
	movq	%rbp, %rsi
	movq	%r15, %r9
	pushq	8(%rsp)                 # 8-byte Folded Reload
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	red_SearchTerminator
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	testq	%r15, %r15
	je	.LBB0_6
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph.i24
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r15)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r15, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r15
	jne	.LBB0_5
.LBB0_6:                                # %list_Delete.exit25
	testq	%rbp, %rbp
	je	.LBB0_8
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbp, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbp
	jne	.LBB0_7
.LBB0_8:                                # %list_Delete.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	red_Terminator, .Lfunc_end0-red_Terminator
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_SearchTerminator,@function
red_SearchTerminator:                   # @red_SearchTerminator
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 176
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%r9, %rbp
	movl	%r8d, %r9d
	movq	%rcx, %rax
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movl	%edi, 60(%rsp)          # 4-byte Spill
	movq	184(%rsp), %r8
	movq	176(%rsp), %rcx
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#35:
	movl	$red_TerminatorLitIsBetter, %ecx
	movq	%rbx, %rdi
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rax, %rsi
	movl	%r9d, 56(%rsp)          # 4-byte Spill
	movl	%r9d, %edx
	callq	clause_MoveBestLiteralToFront
	movq	(%rbx), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	8(%rbx), %r13
	movq	24(%r13), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB1_37
# BB#36:
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB1_37:                               # %clause_LiteralAtom.exit
	callq	term_Copy
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	jmp	.LBB1_38
	.p2align	4, 0x90
.LBB1_103:                              # %list_Delete.exit
                                        #   in Loop: Header=BB1_38 Depth=1
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsi), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rax)
	movq	16(%rsi), %rax
	movq	(%rax), %rax
	movq	%rdx, 8(%rax)
	movl	$1, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rbp          # 8-byte Reload
.LBB1_38:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_41 Depth 2
                                        #       Child Loop BB1_43 Depth 3
                                        #         Child Loop BB1_47 Depth 4
                                        #       Child Loop BB1_53 Depth 3
                                        #         Child Loop BB1_56 Depth 4
                                        #     Child Loop BB1_68 Depth 2
                                        #       Child Loop BB1_69 Depth 3
                                        #       Child Loop BB1_73 Depth 3
                                        #       Child Loop BB1_80 Depth 3
                                        #       Child Loop BB1_87 Depth 3
                                        #       Child Loop BB1_92 Depth 3
                                        #       Child Loop BB1_95 Depth 3
                                        #     Child Loop BB1_102 Depth 2
	movb	$1, %cl
	testq	%rbp, %rbp
	je	.LBB1_39
# BB#40:                                # %.lr.ph76.i.preheader
                                        #   in Loop: Header=BB1_38 Depth=1
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%rbp, %rax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_41:                               # %.lr.ph76.i
                                        #   Parent Loop BB1_38 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_43 Depth 3
                                        #         Child Loop BB1_47 Depth 4
                                        #       Child Loop BB1_53 Depth 3
                                        #         Child Loop BB1_56 Depth 4
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	callq	st_GetUnifier
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB1_64
# BB#42:                                # %.lr.ph42.i
                                        #   in Loop: Header=BB1_41 Depth=2
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	je	.LBB1_53
	.p2align	4, 0x90
.LBB1_43:                               # %.lr.ph42.split.us.i
                                        #   Parent Loop BB1_38 Depth=1
                                        #     Parent Loop BB1_41 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_47 Depth 4
	movq	8(%r12), %rdi
	cmpl	$0, (%rdi)
	jg	.LBB1_46
# BB#44:                                #   in Loop: Header=BB1_43 Depth=3
	callq	sharing_NAtomDataList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB1_47
	jmp	.LBB1_46
	.p2align	4, 0x90
.LBB1_52:                               #   in Loop: Header=BB1_47 Depth=4
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB1_46
.LBB1_47:                               # %.lr.ph.split.us.us.i
                                        #   Parent Loop BB1_38 Depth=1
                                        #     Parent Loop BB1_41 Depth=2
                                        #       Parent Loop BB1_43 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rbp), %rbx
	movq	24(%r13), %rax
	movl	(%rax), %ecx
	movl	fol_NOT(%rip), %eax
	movq	24(%rbx), %rdx
	movl	(%rdx), %edx
	cmpl	%ecx, %eax
	jne	.LBB1_49
# BB#48:                                #   in Loop: Header=BB1_47 Depth=4
	cmpl	%ecx, %edx
	movl	%ecx, %edx
	jne	.LBB1_51
.LBB1_49:                               # %clause_LiteralsAreComplementary.exit.us.us.i
                                        #   in Loop: Header=BB1_47 Depth=4
	cmpl	%ecx, %eax
	je	.LBB1_52
# BB#50:                                # %clause_LiteralsAreComplementary.exit.us.us.i
                                        #   in Loop: Header=BB1_47 Depth=4
	cmpl	%edx, %eax
	jne	.LBB1_52
.LBB1_51:                               # %clause_LiteralsAreComplementary.exit.thread.us.us.i
                                        #   in Loop: Header=BB1_47 Depth=4
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
	jmp	.LBB1_52
	.p2align	4, 0x90
.LBB1_46:                               # %.loopexit.us.i
                                        #   in Loop: Header=BB1_43 Depth=3
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB1_43
	jmp	.LBB1_64
	.p2align	4, 0x90
.LBB1_53:                               # %.lr.ph42.split.i
                                        #   Parent Loop BB1_38 Depth=1
                                        #     Parent Loop BB1_41 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_56 Depth 4
	movq	8(%r12), %rdi
	cmpl	$0, (%rdi)
	jg	.LBB1_63
# BB#54:                                #   in Loop: Header=BB1_53 Depth=3
	callq	sharing_NAtomDataList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB1_56
	jmp	.LBB1_63
	.p2align	4, 0x90
.LBB1_62:                               #   in Loop: Header=BB1_56 Depth=4
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB1_63
.LBB1_56:                               # %.lr.ph.split.i
                                        #   Parent Loop BB1_38 Depth=1
                                        #     Parent Loop BB1_41 Depth=2
                                        #       Parent Loop BB1_53 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rbp), %r15
	movq	24(%r13), %rax
	movl	(%rax), %ecx
	movl	fol_NOT(%rip), %eax
	movq	24(%r15), %rdx
	movl	(%rdx), %edx
	cmpl	%ecx, %eax
	jne	.LBB1_58
# BB#57:                                #   in Loop: Header=BB1_56 Depth=4
	cmpl	%ecx, %edx
	movl	%ecx, %edx
	jne	.LBB1_60
.LBB1_58:                               # %clause_LiteralsAreComplementary.exit.i
                                        #   in Loop: Header=BB1_56 Depth=4
	cmpl	%ecx, %eax
	je	.LBB1_62
# BB#59:                                # %clause_LiteralsAreComplementary.exit.i
                                        #   in Loop: Header=BB1_56 Depth=4
	cmpl	%edx, %eax
	jne	.LBB1_62
.LBB1_60:                               # %clause_LiteralsAreComplementary.exit.thread.i
                                        #   in Loop: Header=BB1_56 Depth=4
	movq	16(%r15), %rax
	movl	68(%rax), %ecx
	addl	64(%rax), %ecx
	addl	72(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB1_62
# BB#61:                                #   in Loop: Header=BB1_56 Depth=4
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
	jmp	.LBB1_62
	.p2align	4, 0x90
.LBB1_63:                               # %.loopexit.i
                                        #   in Loop: Header=BB1_53 Depth=3
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB1_53
.LBB1_64:                               # %._crit_edge.i103
                                        #   in Loop: Header=BB1_41 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB1_41
# BB#65:                                # %red_GetTerminatorPartnerLits.exit.preheader
                                        #   in Loop: Header=BB1_38 Depth=1
	testq	%r14, %r14
	je	.LBB1_66
# BB#67:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_38 Depth=1
	movabsq	$4294967296, %rbp       # imm = 0x100000000
	movq	16(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_68:                               # %.lr.ph
                                        #   Parent Loop BB1_38 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_69 Depth 3
                                        #       Child Loop BB1_73 Depth 3
                                        #       Child Loop BB1_80 Depth 3
                                        #       Child Loop BB1_87 Depth 3
                                        #       Child Loop BB1_92 Depth 3
                                        #       Child Loop BB1_95 Depth 3
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	8(%r14), %rax
	movq	16(%rax), %rdi
	movq	56(%rdi), %rcx
	movq	$-1, %rdx
	xorl	%esi, %esi
	movabsq	$-4294967296, %rbx      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB1_69:                               #   Parent Loop BB1_38 Depth=1
                                        #     Parent Loop BB1_68 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%esi, %r15d
	addq	%rbp, %rbx
	leal	1(%r15), %esi
	cmpq	%rax, 8(%rcx,%rdx,8)
	leaq	1(%rdx), %rdx
	jne	.LBB1_69
# BB#70:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB1_68 Depth=2
	callq	clause_Copy
	movq	%rax, %r12
	movl	68(%r12), %eax
	addl	64(%r12), %eax
	addl	72(%r12), %eax
	movl	$0, 36(%rsp)            # 4-byte Folded Spill
	cmpl	$1, %eax
	setne	15(%rsp)                # 1-byte Folded Spill
	movq	%r12, %rdi
	movl	56(%rsp), %esi          # 4-byte Reload
	callq	clause_RenameVarsBiggerThan
	movq	56(%r12), %rax
	sarq	$29, %rbx
	movq	(%rax,%rbx), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r13, 8(%rbp)
	movq	%rbx, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movl	64(%r12), %ecx
	movl	68(%r12), %edx
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movl	72(%r12), %esi
	leal	(%rcx,%rdx), %eax
	addl	%esi, %eax
	decl	%eax
	js	.LBB1_71
# BB#72:                                # %.lr.ph.i109
                                        #   in Loop: Header=BB1_68 Depth=2
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movl	%r15d, %edi
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_73:                               #   Parent Loop BB1_38 Depth=1
                                        #     Parent Loop BB1_68 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbx, %rdi
	je	.LBB1_75
# BB#74:                                #   in Loop: Header=BB1_73 Depth=3
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbp, %r12
	movq	%r14, %rbp
	movq	%r13, %r14
	movq	%rdi, %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, %rdi
	movq	%r14, %r13
	movq	%rbp, %r14
	movq	%rax, %rbp
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rbp)
	movq	%r12, (%rbp)
	movl	64(%r15), %ecx
	movl	68(%r15), %edx
	movl	72(%r15), %esi
.LBB1_75:                               #   in Loop: Header=BB1_73 Depth=3
	leal	(%rsi,%rdx), %eax
	leal	-1(%rcx,%rax), %eax
	cltq
	cmpq	%rax, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB1_73
# BB#76:                                # %clause_GetLiteralListExcept.exit
                                        #   in Loop: Header=BB1_68 Depth=2
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	list_Copy
	testq	%rbp, %rbp
	je	.LBB1_77
# BB#78:                                #   in Loop: Header=BB1_68 Depth=2
	testq	%rax, %rax
	movq	104(%rsp), %rbx         # 8-byte Reload
	je	.LBB1_82
# BB#79:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB1_68 Depth=2
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB1_80:                               # %.preheader.i
                                        #   Parent Loop BB1_38 Depth=1
                                        #     Parent Loop BB1_68 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB1_80
# BB#81:                                #   in Loop: Header=BB1_68 Depth=2
	movq	%rax, (%rcx)
	jmp	.LBB1_82
	.p2align	4, 0x90
.LBB1_71:                               # %clause_GetLiteralListExcept.exit.thread
                                        #   in Loop: Header=BB1_68 Depth=2
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	list_Copy
	movq	%rax, %rbp
	jmp	.LBB1_82
	.p2align	4, 0x90
.LBB1_77:                               #   in Loop: Header=BB1_68 Depth=2
	movq	%rax, %rbp
	movq	104(%rsp), %rbx         # 8-byte Reload
.LBB1_82:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB1_68 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	clause_SearchMaxVar
	movl	%eax, %r12d
	movl	56(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %r12d
	cmovll	%eax, %r12d
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	24(%rbx), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB1_84
# BB#83:                                #   in Loop: Header=BB1_68 Depth=2
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB1_84:                               # %clause_LiteralAtom.exit122
                                        #   in Loop: Header=BB1_68 Depth=2
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	unify_UnifyNoOC
	testl	%eax, %eax
	je	.LBB1_107
# BB#85:                                #   in Loop: Header=BB1_68 Depth=2
	movl	36(%rsp), %eax          # 4-byte Reload
	movb	15(%rsp), %cl           # 1-byte Reload
	movb	%cl, %al
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	48(%rsp), %rsi
	leaq	64(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB1_88
# BB#86:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB1_68 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB1_87:                               # %.lr.ph.i146
                                        #   Parent Loop BB1_38 Depth=1
                                        #     Parent Loop BB1_68 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB1_87
.LBB1_88:                               # %cont_Reset.exit
                                        #   in Loop: Header=BB1_68 Depth=2
	movl	60(%rsp), %ebx          # 4-byte Reload
	subl	36(%rsp), %ebx          # 4-byte Folded Reload
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	48(%rsp), %r15
	movq	64(%rsp), %rax
	testq	%r15, %r15
	movq	88(%rsp), %rdi          # 8-byte Reload
	je	.LBB1_89
# BB#90:                                #   in Loop: Header=BB1_68 Depth=2
	testq	%rax, %rax
	je	.LBB1_94
# BB#91:                                # %.preheader.i143.preheader
                                        #   in Loop: Header=BB1_68 Depth=2
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB1_92:                               # %.preheader.i143
                                        #   Parent Loop BB1_38 Depth=1
                                        #     Parent Loop BB1_68 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB1_92
# BB#93:                                #   in Loop: Header=BB1_68 Depth=2
	movq	%rax, (%rcx)
	jmp	.LBB1_94
	.p2align	4, 0x90
.LBB1_89:                               #   in Loop: Header=BB1_68 Depth=2
	movq	%rax, %r15
.LBB1_94:                               # %subst_NUnion.exit
                                        #   in Loop: Header=BB1_68 Depth=2
	movq	%r15, 48(%rsp)
	movq	%r15, 64(%rsp)
	callq	subst_Copy
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	subst_Compose
	movq	%rax, 48(%rsp)
	movq	64(%rsp), %rdi
	callq	subst_Delete
	movq	48(%rsp), %rcx
	movl	%ebx, %edi
	movq	%rbp, %rsi
	movq	112(%rsp), %rdx         # 8-byte Reload
	movl	%r12d, %r8d
	movq	96(%rsp), %r9           # 8-byte Reload
	pushq	184(%rsp)
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	callq	red_SearchTerminator
	addq	$16, %rsp
.Lcfi31:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %r15
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	clause_Delete
	movq	48(%rsp), %rdi
	callq	subst_Delete
	testq	%rbp, %rbp
	je	.LBB1_96
	.p2align	4, 0x90
.LBB1_95:                               # %.lr.ph.i134
                                        #   Parent Loop BB1_38 Depth=1
                                        #     Parent Loop BB1_68 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB1_95
.LBB1_96:                               # %list_Delete.exit136
                                        #   in Loop: Header=BB1_68 Depth=2
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	8(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	(%rsi), %r12
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rsi, (%rax)
	movq	(%r14), %rbx
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	testq	%rbx, %rbx
	sete	%cl
	setne	%al
	testq	%r15, %r15
	jne	.LBB1_98
# BB#97:                                # %list_Delete.exit136
                                        #   in Loop: Header=BB1_68 Depth=2
	testb	%al, %al
	movq	%rbx, %r14
	movabsq	$4294967296, %rbp       # imm = 0x100000000
	jne	.LBB1_68
	jmp	.LBB1_98
	.p2align	4, 0x90
.LBB1_39:                               #   in Loop: Header=BB1_38 Depth=1
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	testq	%r15, %r15
	je	.LBB1_99
	jmp	.LBB1_104
	.p2align	4, 0x90
.LBB1_66:                               #   in Loop: Header=BB1_38 Depth=1
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	movq	16(%rsp), %r12          # 8-byte Reload
	movb	$1, %cl
.LBB1_98:                               # %red_GetTerminatorPartnerLits.exit._crit_edge
                                        #   in Loop: Header=BB1_38 Depth=1
	testq	%r15, %r15
	jne	.LBB1_104
.LBB1_99:                               # %red_GetTerminatorPartnerLits.exit._crit_edge
                                        #   in Loop: Header=BB1_38 Depth=1
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_104
# BB#100:                               # %red_GetTerminatorPartnerLits.exit._crit_edge
                                        #   in Loop: Header=BB1_38 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpl	%eax, fol_EQUALITY(%rip)
	jne	.LBB1_104
# BB#101:                               #   in Loop: Header=BB1_38 Depth=1
	testb	%cl, %cl
	jne	.LBB1_103
	.p2align	4, 0x90
.LBB1_102:                              # %.lr.ph.i125
                                        #   Parent Loop BB1_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB1_102
	jmp	.LBB1_103
.LBB1_104:
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	%ecx, %ebp
	callq	term_Delete
	testb	%bpl, %bpl
	jne	.LBB1_106
	.p2align	4, 0x90
.LBB1_105:                              # %.lr.ph.i140
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB1_105
	jmp	.LBB1_106
.LBB1_1:
	xorl	%r13d, %r13d
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	clause_Create
	movq	%rax, %r15
	testq	%r12, %r12
	je	.LBB1_2
# BB#3:                                 # %.lr.ph.i
	movq	%r15, %r14
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
                                        #     Child Loop BB1_7 Depth 2
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	8(%r12), %rax
	movq	(%rax), %rbp
	movq	16(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%rbx, 8(%r12)
	movq	%r15, (%r12)
	movl	8(%rbx), %esi
	movl	%r13d, %edi
	callq	misc_Max
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movslq	(%rbx), %rbx
	movq	32(%r14), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 32(%r14)
	movq	16(%rbp), %rax
	movq	56(%rax), %rax
	movabsq	$-4294967296, %rbx      # imm = 0xFFFFFFFF00000000
	movabsq	$4294967296, %r13       # imm = 0x100000000
	.p2align	4, 0x90
.LBB1_5:                                #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%r13, %rbx
	cmpq	%rbp, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB1_5
# BB#6:                                 # %clause_LiteralGetIndex.exit59.i
                                        #   in Loop: Header=BB1_4 Depth=1
	sarq	$32, %rbx
	movq	40(%r14), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 40(%r14)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	movq	8(%rax), %rbp
	movq	16(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	%r12, (%r15)
	movl	8(%rbx), %esi
	movl	24(%rsp), %edi          # 4-byte Reload
	callq	misc_Max
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	(%rbx), %rbx
	movq	32(%r14), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movq	%r14, %rcx
	movq	%rax, 32(%r14)
	movq	16(%rbp), %rax
	movq	56(%rax), %rax
	movabsq	$-4294967296, %rbx      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB1_7:                                #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%r13, %rbx
	cmpq	%rbp, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB1_7
# BB#8:                                 # %clause_LiteralGetIndex.exit.i
                                        #   in Loop: Header=BB1_4 Depth=1
	sarq	$32, %rbx
	movq	%rcx, %r14
	movq	40(%r14), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 40(%r14)
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	(%r12), %r12
	testq	%r12, %r12
	movq	24(%rsp), %r13          # 8-byte Reload
	jne	.LBB1_4
# BB#9:                                 # %._crit_edge.i.loopexit
	movl	%r13d, %eax
	incl	%eax
	movq	%r15, %r13
	movq	%r14, %r15
	jmp	.LBB1_10
.LBB1_2:
	movl	$1, %eax
.LBB1_10:                               # %._crit_edge.i
	movl	$26, 76(%r15)
	movl	%eax, 8(%r15)
	movl	24(%r15), %ecx
	testq	%r13, %r13
	je	.LBB1_27
# BB#11:                                # %.lr.ph60.i.i
	movl	%ecx, %ebx
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB1_12:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rdx
	testb	$8, 48(%rdx)
	je	.LBB1_14
# BB#13:                                #   in Loop: Header=BB1_12 Depth=1
	orb	$8, 48(%r15)
.LBB1_14:                               #   in Loop: Header=BB1_12 Depth=1
	movl	12(%rdx), %esi
	cmpl	12(%r15), %esi
	movq	%r15, %rsi
	cmovaq	%rdx, %rsi
	movl	12(%rsi), %esi
	movl	%esi, 12(%r15)
	movl	24(%rdx), %edx
	cmpl	%edx, %ebx
	cmovbl	%edx, %ebx
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB1_12
# BB#15:                                # %._crit_edge61.i.i
	cmpl	%ecx, %ebx
	jbe	.LBB1_27
# BB#16:
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB1_24
# BB#17:
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB1_18
# BB#23:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB1_24
.LBB1_27:                               # %.preheader51.i.i
	testl	%ecx, %ecx
	jne	.LBB1_25
	jmp	.LBB1_28
.LBB1_18:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rdx, %rbp
	movq	%r8, (%rbp)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_20
# BB#19:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB1_20:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB1_22
# BB#21:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB1_22:
	addq	$-16, %rdi
	callq	free
.LBB1_24:                               # %.preheader51.thread.i.i
	leal	(,%rbx,8), %edi
	callq	memory_Malloc
	movq	%rax, 16(%r15)
	movl	%ebx, 24(%r15)
.LBB1_25:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_26:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rcx
	movl	%eax, %edx
	movq	$0, (%rcx,%rdx,8)
	incl	%eax
	cmpl	24(%r15), %eax
	jb	.LBB1_26
.LBB1_28:                               # %.preheader.i.i
	testq	%r13, %r13
	je	.LBB1_106
# BB#29:
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB1_31:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_33 Depth 2
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	cmpl	$0, 24(%rcx)
	je	.LBB1_30
# BB#32:                                # %.lr.ph.i50.i
                                        #   in Loop: Header=BB1_31 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_33:                               #   Parent Loop BB1_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r15), %rsi
	movl	%edx, %edi
	movq	16(%rcx), %rbp
	movq	(%rbp,%rdi,8), %rbp
	orq	%rbp, (%rsi,%rdi,8)
	incl	%edx
	cmpl	24(%rcx), %edx
	jb	.LBB1_33
.LBB1_30:                               # %.loopexit.i.i
                                        #   in Loop: Header=BB1_31 Depth=1
	testq	%rax, %rax
	jne	.LBB1_31
	.p2align	4, 0x90
.LBB1_34:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB1_34
.LBB1_106:                              # %red_CreateTerminatorEmptyClause.exit
	movq	%r15, %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_107:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$244, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end1:
	.size	red_SearchTerminator, .Lfunc_end1-red_SearchTerminator
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_TerminatorLitIsBetter,@function
red_TerminatorLitIsBetter:              # @red_TerminatorLitIsBetter
	.cfi_startproc
# BB#0:
	movq	24(%rdx), %rax
	movl	(%rax), %eax
	movl	fol_NOT(%rip), %edx
	movq	24(%rdi), %rdi
	movl	(%rdi), %edi
	cmpl	%eax, %edx
	jne	.LBB2_2
# BB#1:
	cmpl	%edi, %eax
	movl	%eax, %edi
	jne	.LBB2_5
.LBB2_2:                                # %._crit_edge
	cmpl	%ecx, %esi
	jbe	.LBB2_4
# BB#3:                                 # %._crit_edge
	cmpl	%edi, %edx
	je	.LBB2_4
.LBB2_5:
	movl	$1, %eax
	retq
.LBB2_4:
	cmpl	%esi, %ecx
	sbbb	%cl, %cl
	cmpl	%eax, %edx
	sete	%al
	andb	%cl, %al
	movzbl	%al, %eax
	retq
.Lfunc_end2:
	.size	red_TerminatorLitIsBetter, .Lfunc_end2-red_TerminatorLitIsBetter
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str, 31

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/SPASS/terminator.c"
	.size	.L.str.1, 82

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n In red_SearchTerminator: Unification failed."
	.size	.L.str.2, 47

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.3, 133

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n\n"
	.size	.L.str.4, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
