	.text
	.file	"XzHandler.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	_ZN8NArchive3NXz8CHandlerC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandlerC2Ev,@function
_ZN8NArchive3NXz8CHandlerC2Ev:          # @_ZN8NArchive3NXz8CHandlerC2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$_ZTV15IArchiveOpenSeq+16, %eax
	movd	%rax, %xmm0
	movl	$_ZTV10IInArchive+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTV14ISetProperties+16, %eax
	movd	%rax, %xmm0
	movl	$_ZTV11IOutArchive+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	leaq	32(%rbx), %r14
	leaq	40(%rbx), %r15
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 48(%rbx)
	movq	$8, 64(%rbx)
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, 40(%rbx)
.Ltmp0:
	movq	%r14, %rdi
	callq	_ZN8NArchive11COutHandler4InitEv
.Ltmp1:
# BB#1:                                 # %_ZN8NArchive11COutHandlerC2Ev.exit
	movl	$0, 132(%rbx)
	movl	$_ZTVN8NArchive3NXz8CHandlerE+184, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3NXz8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN8NArchive3NXz8CHandlerE+320, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3NXz8CHandlerE+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 168(%rbx)
.Ltmp12:
	movl	$4, %edi
	callq	_Znam
.Ltmp13:
# BB#2:
	leaq	168(%rbx), %r12
	movq	%rax, 168(%rbx)
	movb	$0, (%rax)
	movl	$4, 180(%rbx)
	leaq	208(%rbx), %r13
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 208(%rbx)
	movl	$4, 224(%rbx)
.Ltmp15:
	movq	%r14, %rdi
	callq	_ZN8NArchive11COutHandler4InitEv
.Ltmp16:
# BB#3:                                 # %_ZN8NArchive3NXz8CHandler4InitEv.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB0_11:
.Ltmp17:
	movq	%rax, %r14
	movq	216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_13
# BB#12:
	movq	(%rdi), %rax
.Ltmp18:
	callq	*16(%rax)
.Ltmp19:
.LBB0_13:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB0_15
# BB#14:
	movq	(%rdi), %rax
.Ltmp20:
	callq	*16(%rax)
.Ltmp21:
.LBB0_15:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB0_17
# BB#16:
	callq	_ZdaPv
	jmp	.LBB0_17
.LBB0_10:
.Ltmp14:
	movq	%rax, %r14
.LBB0_17:                               # %_ZN11CStringBaseIcED2Ev.exit
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, (%r15)
.Ltmp22:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp23:
# BB#18:                                # %_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev.exit.i
.Ltmp28:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp29:
	jmp	.LBB0_19
.LBB0_20:
.Ltmp24:
	movq	%rax, %rbx
.Ltmp25:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp26:
	jmp	.LBB0_23
.LBB0_21:
.Ltmp27:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_22:
.Ltmp30:
	movq	%rax, %rbx
.LBB0_23:                               # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB0_4:
.Ltmp2:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, (%r15)
.Ltmp3:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp4:
# BB#5:
.Ltmp9:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp10:
.LBB0_19:                               # %_ZN8NArchive11COutHandlerD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_8:
.Ltmp11:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB0_6:
.Ltmp5:
	movq	%rax, %rbx
.Ltmp6:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp7:
# BB#9:                                 # %.body.i
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB0_7:
.Ltmp8:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN8NArchive3NXz8CHandlerC2Ev, .Lfunc_end0-_ZN8NArchive3NXz8CHandlerC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp21-.Ltmp18         #   Call between .Ltmp18 and .Ltmp21
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	1                       #   On action: 1
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	1                       #   On action: 1
	.long	.Ltmp28-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	1                       #   On action: 1
	.long	.Ltmp25-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin0   #     jumps to .Ltmp27
	.byte	1                       #   On action: 1
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 8 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 9 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp6-.Ltmp10          #   Call between .Ltmp10 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 11 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN8NArchive3NXz8CHandler21GetNumberOfPropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler21GetNumberOfPropertiesEPj,@function
_ZN8NArchive3NXz8CHandler21GetNumberOfPropertiesEPj: # @_ZN8NArchive3NXz8CHandler21GetNumberOfPropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$3, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	_ZN8NArchive3NXz8CHandler21GetNumberOfPropertiesEPj, .Lfunc_end2-_ZN8NArchive3NXz8CHandler21GetNumberOfPropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive3NXz8CHandler15GetPropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler15GetPropertyInfoEjPPwPjPt,@function
_ZN8NArchive3NXz8CHandler15GetPropertyInfoEjPPwPjPt: # @_ZN8NArchive3NXz8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$2, %esi
	ja	.LBB3_2
# BB#1:
	movl	%esi, %eax
	shlq	$4, %rax
	movl	_ZN8NArchive3NXz6kPropsE+8(%rax), %esi
	movl	%esi, (%rcx)
	movzwl	_ZN8NArchive3NXz6kPropsE+12(%rax), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB3_2:
	retq
.Lfunc_end3:
	.size	_ZN8NArchive3NXz8CHandler15GetPropertyInfoEjPPwPjPt, .Lfunc_end3-_ZN8NArchive3NXz8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive3NXz8CHandler28GetNumberOfArchivePropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler28GetNumberOfArchivePropertiesEPj,@function
_ZN8NArchive3NXz8CHandler28GetNumberOfArchivePropertiesEPj: # @_ZN8NArchive3NXz8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$2, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	_ZN8NArchive3NXz8CHandler28GetNumberOfArchivePropertiesEPj, .Lfunc_end4-_ZN8NArchive3NXz8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive3NXz8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler22GetArchivePropertyInfoEjPPwPjPt,@function
_ZN8NArchive3NXz8CHandler22GetArchivePropertyInfoEjPPwPjPt: # @_ZN8NArchive3NXz8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$1, %esi
	ja	.LBB5_2
# BB#1:
	movl	%esi, %eax
	shlq	$4, %rax
	movl	_ZN8NArchive3NXz9kArcPropsE+8(%rax), %esi
	movl	%esi, (%rcx)
	movzwl	_ZN8NArchive3NXz9kArcPropsE+12(%rax), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB5_2:
	retq
.Lfunc_end5:
	.size	_ZN8NArchive3NXz8CHandler22GetArchivePropertyInfoEjPPwPjPt, .Lfunc_end5-_ZN8NArchive3NXz8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive3NXz8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler18GetArchivePropertyEjP14tagPROPVARIANT,@function
_ZN8NArchive3NXz8CHandler18GetArchivePropertyEjP14tagPROPVARIANT: # @_ZN8NArchive3NXz8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 48
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rdx, %rbx
	movl	$0, 8(%rsp)
	cmpl	$22, %esi
	je	.LBB6_8
# BB#1:
	cmpl	$44, %esi
	je	.LBB6_6
# BB#2:
	cmpl	$38, %esi
	jne	.LBB6_10
# BB#3:
	cmpb	$0, 184(%rdi)
	jne	.LBB6_10
# BB#4:
	movq	160(%rdi), %rsi
.Ltmp35:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp36:
	jmp	.LBB6_10
.LBB6_6:
	cmpq	$0, 200(%rdi)
	je	.LBB6_10
# BB#7:
	movq	144(%rdi), %rsi
.Ltmp33:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp34:
	jmp	.LBB6_10
.LBB6_8:
	cmpl	$0, 176(%rdi)
	je	.LBB6_10
# BB#9:
	movq	168(%rdi), %rsi
.Ltmp31:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKc
.Ltmp32:
.LBB6_10:
.Ltmp37:
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp38:
# BB#11:
.Ltmp43:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp44:
# BB#12:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit12
	xorl	%eax, %eax
.LBB6_18:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB6_13:
.Ltmp45:
	movq	%rdx, %r14
	movq	%rax, %rbx
	jmp	.LBB6_14
.LBB6_5:
.Ltmp39:
	movq	%rdx, %r14
	movq	%rax, %rbx
.Ltmp40:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp41:
.LBB6_14:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB6_15
# BB#17:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB6_18
.LBB6_15:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp46:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp47:
# BB#20:
.LBB6_16:
.Ltmp48:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_19:
.Ltmp42:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN8NArchive3NXz8CHandler18GetArchivePropertyEjP14tagPROPVARIANT, .Lfunc_end6-_ZN8NArchive3NXz8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\334"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp38-.Ltmp35         #   Call between .Ltmp35 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin1   #     jumps to .Ltmp39
	.byte	3                       #   On action: 2
	.long	.Ltmp43-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin1   #     jumps to .Ltmp45
	.byte	3                       #   On action: 2
	.long	.Ltmp40-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin1   #     jumps to .Ltmp42
	.byte	1                       #   On action: 1
	.long	.Ltmp41-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp46-.Ltmp41         #   Call between .Ltmp41 and .Ltmp46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin1   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Lfunc_end6-.Ltmp47     #   Call between .Ltmp47 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3NXz8CHandler16GetNumberOfItemsEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler16GetNumberOfItemsEPj,@function
_ZN8NArchive3NXz8CHandler16GetNumberOfItemsEPj: # @_ZN8NArchive3NXz8CHandler16GetNumberOfItemsEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	_ZN8NArchive3NXz8CHandler16GetNumberOfItemsEPj, .Lfunc_end7-_ZN8NArchive3NXz8CHandler16GetNumberOfItemsEPj
	.cfi_endproc

	.globl	_ZN8NArchive3NXz8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler11GetPropertyEjjP14tagPROPVARIANT,@function
_ZN8NArchive3NXz8CHandler11GetPropertyEjjP14tagPROPVARIANT: # @_ZN8NArchive3NXz8CHandler11GetPropertyEjjP14tagPROPVARIANT
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rcx, %rbx
	movl	$0, 8(%rsp)
	cmpl	$22, %edx
	je	.LBB8_8
# BB#1:
	cmpl	$8, %edx
	je	.LBB8_6
# BB#2:
	cmpl	$7, %edx
	jne	.LBB8_10
# BB#3:
	cmpq	$0, 192(%rdi)
	je	.LBB8_10
# BB#4:
	movq	152(%rdi), %rsi
.Ltmp53:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp54:
	jmp	.LBB8_10
.LBB8_6:
	cmpq	$0, 200(%rdi)
	je	.LBB8_10
# BB#7:
	movq	144(%rdi), %rsi
.Ltmp51:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp52:
	jmp	.LBB8_10
.LBB8_8:
	cmpl	$0, 176(%rdi)
	je	.LBB8_10
# BB#9:
	movq	168(%rdi), %rsi
.Ltmp49:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKc
.Ltmp50:
.LBB8_10:
.Ltmp55:
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp56:
# BB#11:
.Ltmp61:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp62:
# BB#12:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit12
	xorl	%eax, %eax
.LBB8_18:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB8_13:
.Ltmp63:
	movq	%rdx, %r14
	movq	%rax, %rbx
	jmp	.LBB8_14
.LBB8_5:
.Ltmp57:
	movq	%rdx, %r14
	movq	%rax, %rbx
.Ltmp58:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp59:
.LBB8_14:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB8_15
# BB#17:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB8_18
.LBB8_15:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp64:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp65:
# BB#20:
.LBB8_16:
.Ltmp66:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB8_19:
.Ltmp60:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN8NArchive3NXz8CHandler11GetPropertyEjjP14tagPROPVARIANT, .Lfunc_end8-_ZN8NArchive3NXz8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\334"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp53-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp56-.Ltmp53         #   Call between .Ltmp53 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin2   #     jumps to .Ltmp57
	.byte	3                       #   On action: 2
	.long	.Ltmp61-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin2   #     jumps to .Ltmp63
	.byte	3                       #   On action: 2
	.long	.Ltmp58-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	1                       #   On action: 1
	.long	.Ltmp59-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp64-.Ltmp59         #   Call between .Ltmp59 and .Ltmp64
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin2   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Lfunc_end8-.Ltmp65     #   Call between .Ltmp65 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3NXz17COpenCallbackWrapC2EP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz17COpenCallbackWrapC2EP20IArchiveOpenCallback,@function
_ZN8NArchive3NXz17COpenCallbackWrapC2EP20IArchiveOpenCallback: # @_ZN8NArchive3NXz17COpenCallbackWrapC2EP20IArchiveOpenCallback
	.cfi_startproc
# BB#0:
	movq	$_ZN8NArchive3NXzL20OpenCallbackProgressEPvyy, (%rdi)
	movq	%rsi, 8(%rdi)
	movl	$0, 16(%rdi)
	retq
.Lfunc_end9:
	.size	_ZN8NArchive3NXz17COpenCallbackWrapC2EP20IArchiveOpenCallback, .Lfunc_end9-_ZN8NArchive3NXz17COpenCallbackWrapC2EP20IArchiveOpenCallback
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN8NArchive3NXzL20OpenCallbackProgressEPvyy,@function
_ZN8NArchive3NXzL20OpenCallbackProgressEPvyy: # @_ZN8NArchive3NXzL20OpenCallbackProgressEPvyy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	%rsi, 8(%rsp)
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	8(%rsp), %rdx
	xorl	%esi, %esi
	callq	*48(%rax)
	movl	%eax, 16(%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end10:
	.size	_ZN8NArchive3NXzL20OpenCallbackProgressEPvyy, .Lfunc_end10-_ZN8NArchive3NXzL20OpenCallbackProgressEPvyy
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
.LCPI11_1:
	.zero	16
	.text
	.globl	_ZN8NArchive3NXz8CHandler5Open2EP9IInStreamP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler5Open2EP9IInStreamP20IArchiveOpenCallback,@function
_ZN8NArchive3NXz8CHandler5Open2EP9IInStreamP20IArchiveOpenCallback: # @_ZN8NArchive3NXz8CHandler5Open2EP9IInStreamP20IArchiveOpenCallback
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$16872, %rsp            # imm = 0x41E8
.Lcfi29:
	.cfi_def_cfa_offset 16928
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	leaq	400(%rsp), %r14
	movq	%r14, %rdi
	callq	_ZN17CSeekInStreamWrapC1EP9IInStream
	leaq	432(%rsp), %r12
	movl	$1, %esi
	movq	%r12, %rdi
	callq	LookToRead_CreateVTable
	movq	%r14, 464(%rsp)
	movq	%r12, %rdi
	callq	LookToRead_Init
	movq	$_ZN8NArchive3NXzL20OpenCallbackProgressEPvyy, 200(%rsp)
	movq	%r15, 208(%rsp)
	movl	$0, 216(%rsp)
	movq	(%rbp), %rax
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	leaq	144(%rbx), %r13
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	movq	%r13, %rcx
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB11_250
# BB#1:
	movq	(%r15), %rax
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r13, %rdx
	callq	*40(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB11_250
# BB#2:
	leaq	152(%rsp), %rbx
	movq	%rbx, %rdi
	callq	Xzs_Construct
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	136(%rax), %rdx
.Ltmp67:
	leaq	432(%rsp), %rsi
	leaq	200(%rsp), %rcx
	movl	$_ZL7g_Alloc, %r8d
	movq	%rbx, %rdi
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	callq	Xzs_ReadBackward
	movl	%eax, %r15d
.Ltmp68:
# BB#3:
	testl	%r15d, %r15d
	je	.LBB11_6
# BB#4:
	cmpl	$17, %r15d
	jne	.LBB11_231
# BB#5:
	cmpq	$0, 152(%rsp)
	je	.LBB11_23
.LBB11_6:                               # %.thread
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	subq	%rax, (%r13)
.Ltmp70:
	leaq	152(%rsp), %rdi
	callq	Xzs_GetUnpackSize
.Ltmp71:
# BB#7:
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 152(%rcx)
	movaps	.LCPI11_0(%rip), %xmm0  # xmm0 = [1,1]
	movups	%xmm0, 192(%rcx)
.Ltmp72:
	leaq	152(%rsp), %rdi
	callq	Xzs_GetNumBlocks
.Ltmp73:
# BB#8:
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 160(%rcx)
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp74:
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp75:
# BB#9:
	testl	%ebp, %ebp
	jne	.LBB11_249
# BB#10:
.Ltmp76:
	leaq	376(%rsp), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	_ZN16CSeqInStreamWrapC1EP19ISequentialInStream
.Ltmp77:
# BB#11:
.Ltmp79:
	leaq	126(%rsp), %rdi
	leaq	376(%rsp), %rsi
	callq	Xz_ReadHeader
.Ltmp80:
# BB#12:
	testl	%eax, %eax
	jne	.LBB11_158
# BB#13:
.Ltmp82:
	leaq	224(%rsp), %rdi
	leaq	376(%rsp), %rsi
	leaq	48(%rsp), %rdx
	leaq	196(%rsp), %rcx
	callq	XzBlock_ReadHeader
.Ltmp83:
# BB#14:
	orl	48(%rsp), %eax
	jne	.LBB11_158
# BB#15:
	movb	240(%rsp), %al
	andb	$3, %al
	incb	%al
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	168(%rcx), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movzbl	%al, %eax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	jmp	.LBB11_16
.LBB11_23:
	movl	$17, %r15d
	jmp	.LBB11_231
.LBB11_24:                              # %vector.body.preheader
                                        #   in Loop: Header=BB11_16 Depth=1
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB11_27
# BB#25:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB11_16 Depth=1
	negq	%rsi
	xorl	%edx, %edx
.LBB11_26:                              # %vector.body.prol
                                        #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbp,%rdx)
	movups	%xmm1, 16(%rbp,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB11_26
	jmp	.LBB11_28
.LBB11_27:                              #   in Loop: Header=BB11_16 Depth=1
	xorl	%edx, %edx
.LBB11_28:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB11_16 Depth=1
	cmpq	$96, %r8
	jb	.LBB11_31
# BB#29:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbp,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB11_30:                              # %vector.body
                                        #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB11_30
.LBB11_31:                              # %middle.block
                                        #   in Loop: Header=BB11_16 Depth=1
	cmpq	%rcx, %r9
	jne	.LBB11_74
	jmp	.LBB11_81
.LBB11_16:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_18 Depth 2
                                        #       Child Loop BB11_20 Depth 3
                                        #       Child Loop BB11_34 Depth 3
                                        #       Child Loop BB11_38 Depth 3
                                        #       Child Loop BB11_50 Depth 3
                                        #       Child Loop BB11_53 Depth 3
                                        #       Child Loop BB11_57 Depth 3
                                        #     Child Loop BB11_63 Depth 2
                                        #     Child Loop BB11_26 Depth 2
                                        #     Child Loop BB11_30 Depth 2
                                        #     Child Loop BB11_76 Depth 2
                                        #     Child Loop BB11_79 Depth 2
                                        #     Child Loop BB11_84 Depth 2
                                        #     Child Loop BB11_110 Depth 2
                                        #     Child Loop BB11_102 Depth 2
                                        #     Child Loop BB11_105 Depth 2
                                        #     Child Loop BB11_134 Depth 2
                                        #     Child Loop BB11_137 Depth 2
                                        #     Child Loop BB11_142 Depth 2
                                        #     Child Loop BB11_122 Depth 2
                                        #     Child Loop BB11_125 Depth 2
                                        #     Child Loop BB11_130 Depth 2
                                        #     Child Loop BB11_146 Depth 2
                                        #     Child Loop BB11_93 Depth 2
                                        #     Child Loop BB11_96 Depth 2
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp85:
	movl	$4, %edi
	callq	_Znam
.Ltmp86:
# BB#17:                                # %.noexc
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	$4, 12(%rsp)
	movq	%rbx, 184(%rsp)         # 8-byte Spill
	shlq	$5, %rbx
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	leaq	248(%rsp,%rbx), %rbx
	xorl	%esi, %esi
	xorl	%r15d, %r15d
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	jmp	.LBB11_18
.LBB11_32:                              # %vector.body250.preheader
                                        #   in Loop: Header=BB11_18 Depth=2
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB11_35
# BB#33:                                # %vector.body250.prol.preheader
                                        #   in Loop: Header=BB11_18 Depth=2
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB11_34:                              # %vector.body250.prol
                                        #   Parent Loop BB11_16 Depth=1
                                        #     Parent Loop BB11_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbp,%rdx)
	movups	%xmm1, 16(%rbp,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB11_34
	jmp	.LBB11_36
.LBB11_35:                              #   in Loop: Header=BB11_18 Depth=2
	xorl	%edx, %edx
.LBB11_36:                              # %vector.body250.prol.loopexit
                                        #   in Loop: Header=BB11_18 Depth=2
	cmpq	$96, %r8
	jb	.LBB11_39
# BB#37:                                # %vector.body250.preheader.new
                                        #   in Loop: Header=BB11_18 Depth=2
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbp,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB11_38:                              # %vector.body250
                                        #   Parent Loop BB11_16 Depth=1
                                        #     Parent Loop BB11_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB11_38
.LBB11_39:                              # %middle.block251
                                        #   in Loop: Header=BB11_18 Depth=2
	cmpq	%rcx, %rax
	jne	.LBB11_48
	jmp	.LBB11_55
	.p2align	4, 0x90
.LBB11_18:                              #   Parent Loop BB11_16 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_20 Depth 3
                                        #       Child Loop BB11_34 Depth 3
                                        #       Child Loop BB11_38 Depth 3
                                        #       Child Loop BB11_50 Depth 3
                                        #       Child Loop BB11_53 Depth 3
                                        #       Child Loop BB11_57 Depth 3
	movq	%r15, %rax
	shlq	$4, %rax
	movl	_ZN8NArchive3NXzL11g_NamePairsE(%rax), %ecx
	cmpq	(%rbx), %rcx
	jne	.LBB11_59
# BB#19:                                #   in Loop: Header=BB11_18 Depth=2
	movq	_ZN8NArchive3NXzL11g_NamePairsE+8(%rax), %r13
	movl	$0, 8(%rsp)
	movq	(%rsp), %rax
	movb	$0, (%rax)
	xorl	%r12d, %r12d
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB11_20:                              #   Parent Loop BB11_16 Depth=1
                                        #     Parent Loop BB11_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%r12d
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB11_20
# BB#21:                                # %_Z11MyStringLenIcEiPKT_.exit.i.i
                                        #   in Loop: Header=BB11_18 Depth=2
	leal	-1(%r12), %esi
	movl	12(%rsp), %r14d
	cmpl	%r12d, %r14d
	jne	.LBB11_40
# BB#22:                                # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i.i
                                        #   in Loop: Header=BB11_18 Depth=2
	movq	(%rsp), %rbp
	jmp	.LBB11_57
	.p2align	4, 0x90
.LBB11_40:                              #   in Loop: Header=BB11_18 Depth=2
	movslq	%r12d, %rdi
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	cmpl	$-1, %esi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp88:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp89:
# BB#41:                                # %.noexc.i
                                        #   in Loop: Header=BB11_18 Depth=2
	testl	%r14d, %r14d
	jle	.LBB11_56
# BB#42:                                # %.preheader.i.i.i
                                        #   in Loop: Header=BB11_18 Depth=2
	movslq	8(%rsp), %rax
	testq	%rax, %rax
	movq	(%rsp), %rdi
	jle	.LBB11_54
# BB#43:                                # %.lr.ph.preheader.i.i.i
                                        #   in Loop: Header=BB11_18 Depth=2
	cmpl	$31, %eax
	jbe	.LBB11_47
# BB#44:                                # %min.iters.checked254
                                        #   in Loop: Header=BB11_18 Depth=2
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB11_47
# BB#45:                                # %vector.memcheck265
                                        #   in Loop: Header=BB11_18 Depth=2
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB11_32
# BB#46:                                # %vector.memcheck265
                                        #   in Loop: Header=BB11_18 Depth=2
	leaq	(%rbp,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB11_32
.LBB11_47:                              #   in Loop: Header=BB11_18 Depth=2
	xorl	%ecx, %ecx
.LBB11_48:                              # %.lr.ph.i.i.i.preheader
                                        #   in Loop: Header=BB11_18 Depth=2
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %r8
	subq	%rcx, %r8
	andq	$7, %rsi
	je	.LBB11_51
# BB#49:                                # %.lr.ph.i.i.i.prol.preheader
                                        #   in Loop: Header=BB11_18 Depth=2
	negq	%rsi
	.p2align	4, 0x90
.LBB11_50:                              # %.lr.ph.i.i.i.prol
                                        #   Parent Loop BB11_16 Depth=1
                                        #     Parent Loop BB11_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB11_50
.LBB11_51:                              # %.lr.ph.i.i.i.prol.loopexit
                                        #   in Loop: Header=BB11_18 Depth=2
	cmpq	$7, %r8
	jb	.LBB11_55
# BB#52:                                # %.lr.ph.i.i.i.preheader.new
                                        #   in Loop: Header=BB11_18 Depth=2
	subq	%rcx, %rax
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB11_53:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB11_16 Depth=1
                                        #     Parent Loop BB11_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB11_53
	jmp	.LBB11_55
.LBB11_54:                              # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB11_18 Depth=2
	testq	%rdi, %rdi
	je	.LBB11_56
.LBB11_55:                              # %._crit_edge.thread.i.i.i
                                        #   in Loop: Header=BB11_18 Depth=2
	callq	_ZdaPv
.LBB11_56:                              # %._crit_edge17.i.i.i
                                        #   in Loop: Header=BB11_18 Depth=2
	movq	%rbp, (%rsp)
	movslq	8(%rsp), %rax
	movb	$0, (%rbp,%rax)
	movl	%r12d, 12(%rsp)
	movq	144(%rsp), %rbx         # 8-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
	.p2align	4, 0x90
.LBB11_57:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i
                                        #   Parent Loop BB11_16 Depth=1
                                        #     Parent Loop BB11_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%r13), %eax
	incq	%r13
	movb	%al, (%rbp)
	incq	%rbp
	testb	%al, %al
	jne	.LBB11_57
# BB#58:                                # %_ZN11CStringBaseIcEaSEPKc.exit.i
                                        #   in Loop: Header=BB11_18 Depth=2
	movl	%esi, 8(%rsp)
.LBB11_59:                              #   in Loop: Header=BB11_18 Depth=2
	incq	%r15
	cmpq	$9, %r15
	jb	.LBB11_18
# BB#60:                                #   in Loop: Header=BB11_16 Depth=1
	testl	%esi, %esi
	jne	.LBB11_86
# BB#61:                                #   in Loop: Header=BB11_16 Depth=1
	movq	(%rbx), %rdi
.Ltmp91:
	movl	$10, %edx
	leaq	80(%rsp), %rsi
	callq	_Z21ConvertUInt64ToStringyPcj
.Ltmp92:
# BB#62:                                #   in Loop: Header=BB11_16 Depth=1
	movl	$0, 8(%rsp)
	movq	(%rsp), %rax
	movb	$0, (%rax)
	xorl	%ebx, %ebx
	leaq	80(%rsp), %rcx
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB11_63:                              #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebx
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB11_63
# BB#64:                                # %_Z11MyStringLenIcEiPKT_.exit.i35.i
                                        #   in Loop: Header=BB11_16 Depth=1
	leal	-1(%rbx), %r15d
	movl	12(%rsp), %r14d
	cmpl	%ebx, %r14d
	jne	.LBB11_66
# BB#65:                                # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i37.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	(%rsp), %rbp
	jmp	.LBB11_83
.LBB11_66:                              #   in Loop: Header=BB11_16 Depth=1
	movslq	%ebx, %rdi
	cmpl	$-1, %r15d
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp93:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp94:
# BB#67:                                # %.noexc50.i
                                        #   in Loop: Header=BB11_16 Depth=1
	testl	%r14d, %r14d
	jle	.LBB11_82
# BB#68:                                # %.preheader.i.i38.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movslq	8(%rsp), %r9
	testq	%r9, %r9
	movq	(%rsp), %rdi
	jle	.LBB11_80
# BB#69:                                # %.lr.ph.preheader.i.i39.i
                                        #   in Loop: Header=BB11_16 Depth=1
	cmpl	$31, %r9d
	jbe	.LBB11_73
# BB#70:                                # %min.iters.checked
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	%r9, %rcx
	andq	$-32, %rcx
	je	.LBB11_73
# BB#71:                                # %vector.memcheck
                                        #   in Loop: Header=BB11_16 Depth=1
	leaq	(%rdi,%r9), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB11_24
# BB#72:                                # %vector.memcheck
                                        #   in Loop: Header=BB11_16 Depth=1
	leaq	(%rbp,%r9), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB11_24
.LBB11_73:                              #   in Loop: Header=BB11_16 Depth=1
	xorl	%ecx, %ecx
.LBB11_74:                              # %.lr.ph.i.i44.i.preheader
                                        #   in Loop: Header=BB11_16 Depth=1
	movl	%r9d, %esi
	subl	%ecx, %esi
	leaq	-1(%r9), %r8
	subq	%rcx, %r8
	andq	$7, %rsi
	je	.LBB11_77
# BB#75:                                # %.lr.ph.i.i44.i.prol.preheader
                                        #   in Loop: Header=BB11_16 Depth=1
	negq	%rsi
.LBB11_76:                              # %.lr.ph.i.i44.i.prol
                                        #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB11_76
.LBB11_77:                              # %.lr.ph.i.i44.i.prol.loopexit
                                        #   in Loop: Header=BB11_16 Depth=1
	cmpq	$7, %r8
	jb	.LBB11_81
# BB#78:                                # %.lr.ph.i.i44.i.preheader.new
                                        #   in Loop: Header=BB11_16 Depth=1
	subq	%rcx, %r9
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB11_79:                              # %.lr.ph.i.i44.i
                                        #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r9
	jne	.LBB11_79
	jmp	.LBB11_81
.LBB11_80:                              # %._crit_edge.i.i40.i
                                        #   in Loop: Header=BB11_16 Depth=1
	testq	%rdi, %rdi
	je	.LBB11_82
.LBB11_81:                              # %._crit_edge.thread.i.i45.i
                                        #   in Loop: Header=BB11_16 Depth=1
	callq	_ZdaPv
.LBB11_82:                              # %._crit_edge17.i.i46.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	%rbp, (%rsp)
	movslq	8(%rsp), %rax
	movb	$0, (%rbp,%rax)
	movl	%ebx, 12(%rsp)
	leaq	80(%rsp), %rcx
.LBB11_83:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i49.i.preheader
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	%rcx, %rax
	movq	144(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB11_84:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i49.i
                                        #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB11_84
# BB#85:                                #   in Loop: Header=BB11_16 Depth=1
	movl	%r15d, 8(%rsp)
.LBB11_86:                              #   in Loop: Header=BB11_16 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 256(%rsp,%rax)
	je	.LBB11_152
# BB#87:                                #   in Loop: Header=BB11_16 Depth=1
.Ltmp96:
	movl	$58, %esi
	movq	%rsp, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp97:
# BB#88:                                #   in Loop: Header=BB11_16 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	256(%rsp,%rax), %r15
	movq	(%rbx), %rcx
	movl	(%r15), %eax
	cmpq	$33, %rcx
	jne	.LBB11_98
# BB#89:                                #   in Loop: Header=BB11_16 Depth=1
	cmpl	$1, %eax
	jne	.LBB11_98
# BB#90:                                #   in Loop: Header=BB11_16 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movb	260(%rsp,%rax), %bpl
	movl	%ebp, %ebx
	andb	$1, %bl
	jne	.LBB11_118
# BB#91:                                #   in Loop: Header=BB11_16 Depth=1
	shrb	%bpl
	addb	$12, %bpl
	movzbl	%bpl, %edi
.Ltmp140:
	leaq	80(%rsp), %rsi
	callq	_Z21ConvertUInt32ToStringjPc
.Ltmp141:
# BB#92:                                # %.noexc52.i
                                        #   in Loop: Header=BB11_16 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$-1, %ebx
	leaq	80(%rsp), %rax
	.p2align	4, 0x90
.LBB11_93:                              #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebx
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB11_93
# BB#94:                                # %_Z11MyStringLenIcEiPKT_.exit.i.i.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	leal	1(%rbx), %ebp
	movslq	%ebp, %rdi
	cmpl	$-1, %ebx
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp142:
	callq	_Znam
.Ltmp143:
# BB#95:                                # %.noexc53.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	%rax, 16(%rsp)
	movb	$0, (%rax)
	movl	%ebp, 28(%rsp)
	leaq	80(%rsp), %rcx
	.p2align	4, 0x90
.LBB11_96:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i.i.i
                                        #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB11_96
# BB#97:                                # %_ZN8NArchive3NXzL21ConvertUInt32ToStringEj.exit.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movl	%ebx, 24(%rsp)
	jmp	.LBB11_149
.LBB11_98:                              #   in Loop: Header=BB11_16 Depth=1
	cmpq	$3, %rcx
	jne	.LBB11_107
# BB#99:                                #   in Loop: Header=BB11_16 Depth=1
	cmpl	$1, %eax
	jne	.LBB11_107
# BB#100:                               #   in Loop: Header=BB11_16 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movzbl	260(%rsp,%rax), %edi
	incl	%edi
.Ltmp108:
	leaq	80(%rsp), %rbp
	movq	%rbp, %rsi
	callq	_Z21ConvertUInt32ToStringjPc
.Ltmp109:
# BB#101:                               # %.noexc60.i
                                        #   in Loop: Header=BB11_16 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB11_102:                             #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebx
	cmpb	$0, (%rbp)
	leaq	1(%rbp), %rbp
	jne	.LBB11_102
# BB#103:                               # %_Z11MyStringLenIcEiPKT_.exit.i.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	leal	1(%rbx), %ebp
	movslq	%ebp, %rdi
	cmpl	$-1, %ebx
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp110:
	callq	_Znam
.Ltmp111:
	leaq	80(%rsp), %rcx
# BB#104:                               # %.noexc61.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	%rax, 16(%rsp)
	movb	$0, (%rax)
	movl	%ebp, 28(%rsp)
	.p2align	4, 0x90
.LBB11_105:                             # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i59.i
                                        #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB11_105
# BB#106:                               #   in Loop: Header=BB11_16 Depth=1
	movl	%ebx, 24(%rsp)
.Ltmp113:
	movq	%rsp, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZN11CStringBaseIcEpLERKS0_
.Ltmp114:
	jmp	.LBB11_150
.LBB11_107:                             #   in Loop: Header=BB11_16 Depth=1
.Ltmp98:
	movl	$91, %esi
	movq	%rsp, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp99:
# BB#108:                               # %.preheader.i
                                        #   in Loop: Header=BB11_16 Depth=1
	cmpl	$0, (%r15)
	je	.LBB11_117
# BB#109:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB11_16 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_110:                             # %.lr.ph.i
                                        #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %eax
	movq	72(%rsp), %rcx          # 8-byte Reload
	leaq	224(%rsp,%rcx), %rcx
	movzbl	36(%rax,%rcx), %ebx
	movl	%ebx, %eax
	shrb	$4, %al
	cmpb	$-96, %bl
	movb	$48, %cl
	jb	.LBB11_112
# BB#111:                               # %.lr.ph.i
                                        #   in Loop: Header=BB11_110 Depth=2
	movb	$55, %cl
.LBB11_112:                             # %.lr.ph.i
                                        #   in Loop: Header=BB11_110 Depth=2
	addb	%al, %cl
	movzbl	%cl, %esi
.Ltmp100:
	movq	%rsp, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp101:
# BB#113:                               # %.noexc64.i
                                        #   in Loop: Header=BB11_110 Depth=2
	andb	$15, %bl
	cmpb	$10, %bl
	movb	$48, %al
	jb	.LBB11_115
# BB#114:                               # %.noexc64.i
                                        #   in Loop: Header=BB11_110 Depth=2
	movb	$55, %al
.LBB11_115:                             # %.noexc64.i
                                        #   in Loop: Header=BB11_110 Depth=2
	addb	%bl, %al
	movzbl	%al, %esi
.Ltmp102:
	movq	%rsp, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp103:
# BB#116:                               # %_ZN8NArchive3NXzL14AddHexToStringER11CStringBaseIcEh.exit.i
                                        #   in Loop: Header=BB11_110 Depth=2
	incl	%ebp
	cmpl	(%r15), %ebp
	jb	.LBB11_110
.LBB11_117:                             # %._crit_edge.i
                                        #   in Loop: Header=BB11_16 Depth=1
.Ltmp105:
	movl	$93, %esi
	movq	%rsp, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp106:
	jmp	.LBB11_152
.LBB11_118:                             #   in Loop: Header=BB11_16 Depth=1
.Ltmp116:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp117:
# BB#119:                               # %.noexc54.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movb	$0, (%r15)
	orb	$2, %bl
	movzbl	%bl, %edi
	movl	%ebp, %ecx
	shrb	%cl
	incb	%cl
	shll	%cl, %edi
	cmpb	$18, %bpl
	jb	.LBB11_132
# BB#120:                               #   in Loop: Header=BB11_16 Depth=1
	shrl	$10, %edi
.Ltmp126:
	leaq	80(%rsp), %rsi
	callq	_Z21ConvertUInt32ToStringjPc
.Ltmp127:
# BB#121:                               # %.noexc.i.i.preheader
                                        #   in Loop: Header=BB11_16 Depth=1
	movl	$-1, %ebp
	leaq	80(%rsp), %rax
	.p2align	4, 0x90
.LBB11_122:                             # %.noexc.i.i
                                        #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebp
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB11_122
# BB#123:                               # %_Z11MyStringLenIcEiPKT_.exit.i.i17.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	leal	1(%rbp), %eax
	movslq	%eax, %rbx
	cmpl	$-1, %ebp
	movq	$-1, %rax
	cmovlq	%rax, %rbx
.Ltmp128:
	movq	%rbx, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp129:
# BB#124:                               # %.noexc21.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movb	$0, (%r13)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_125:                             # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i20.i.i
                                        #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	80(%rsp,%rax), %ecx
	movb	%cl, (%r13,%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB11_125
# BB#126:                               #   in Loop: Header=BB11_16 Depth=1
	movb	$0, (%r15)
	cmpl	$3, %ebp
	je	.LBB11_129
# BB#127:                               #   in Loop: Header=BB11_16 Depth=1
.Ltmp131:
	movq	%rbx, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp132:
# BB#128:                               # %._crit_edge17.i.i.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	%r15, %rdi
	callq	_ZdaPv
	movb	$0, (%rbx)
	movq	%rbx, %r15
.LBB11_129:                             # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_130:                             #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r13,%rax), %ecx
	movb	%cl, (%r15,%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB11_130
# BB#131:                               # %_ZN11CStringBaseIcED2Ev.exit.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	%r13, %rdi
	callq	_ZdaPv
	movb	$109, %r14b
	jmp	.LBB11_144
.LBB11_132:                             #   in Loop: Header=BB11_16 Depth=1
.Ltmp118:
	leaq	80(%rsp), %rsi
	callq	_Z21ConvertUInt32ToStringjPc
.Ltmp119:
# BB#133:                               # %.noexc31.i.i.preheader
                                        #   in Loop: Header=BB11_16 Depth=1
	movl	$-1, %ebp
	leaq	80(%rsp), %rax
	.p2align	4, 0x90
.LBB11_134:                             # %.noexc31.i.i
                                        #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebp
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB11_134
# BB#135:                               # %_Z11MyStringLenIcEiPKT_.exit.i.i27.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	leal	1(%rbp), %eax
	movslq	%eax, %r13
	cmpl	$-1, %ebp
	movq	$-1, %rax
	cmovlq	%rax, %r13
.Ltmp120:
	movq	%r13, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp121:
# BB#136:                               # %.noexc32.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movb	$0, (%rbx)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_137:                             # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i30.i.i
                                        #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	80(%rsp,%rax), %ecx
	movb	%cl, (%rbx,%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB11_137
# BB#138:                               #   in Loop: Header=BB11_16 Depth=1
	movb	$0, (%r15)
	cmpl	$3, %ebp
	je	.LBB11_141
# BB#139:                               #   in Loop: Header=BB11_16 Depth=1
.Ltmp123:
	movq	%r13, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp124:
# BB#140:                               # %._crit_edge17.i.i44.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	%r15, %rdi
	callq	_ZdaPv
	movb	$0, (%r13)
	movq	%r13, %r15
.LBB11_141:                             # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i45.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_142:                             #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx,%rax), %ecx
	movb	%cl, (%r15,%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB11_142
# BB#143:                               # %_ZN11CStringBaseIcED2Ev.exit50.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movb	$107, %r14b
.LBB11_144:                             #   in Loop: Header=BB11_16 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	leal	1(%rbp), %ebx
	movslq	%ebx, %rdi
	cmpl	$-1, %ebp
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp134:
	callq	_Znam
.Ltmp135:
# BB#145:                               # %.noexc55.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	%rax, 16(%rsp)
	movb	$0, (%rax)
	movl	%ebx, 28(%rsp)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_146:                             # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i52.i.i
                                        #   Parent Loop BB11_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r15,%rcx), %edx
	movb	%dl, (%rax,%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB11_146
# BB#147:                               # %_ZN11CStringBaseIcEC2ERKS0_.exit.i.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movl	%ebp, 24(%rsp)
.Ltmp137:
	movsbl	%r14b, %esi
	leaq	16(%rsp), %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp138:
# BB#148:                               # %_ZN11CStringBaseIcED2Ev.exit56.i.i
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB11_149:                             # %_ZN8NArchive3NXzL17Lzma2PropToStringEi.exit.i
                                        #   in Loop: Header=BB11_16 Depth=1
.Ltmp145:
	movq	%rsp, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZN11CStringBaseIcEpLERKS0_
.Ltmp146:
.LBB11_150:                             #   in Loop: Header=BB11_16 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_152
# BB#151:                               #   in Loop: Header=BB11_16 Depth=1
	callq	_ZdaPv
.LBB11_152:                             # %_ZN8NArchive3NXzL15GetMethodStringERK9CXzFilter.exit
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 176(%rax)
	je	.LBB11_154
# BB#153:                               #   in Loop: Header=BB11_16 Depth=1
.Ltmp148:
	movl	$32, %esi
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp149:
.LBB11_154:                             # %.noexc103
                                        #   in Loop: Header=BB11_16 Depth=1
.Ltmp150:
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	%rsp, %rsi
	callq	_ZN11CStringBaseIcEpLERKS0_
.Ltmp151:
# BB#155:                               # %_ZN8NArchive3NXzL9AddStringER11CStringBaseIcERKS2_.exit
                                        #   in Loop: Header=BB11_16 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	movq	184(%rsp), %rbx         # 8-byte Reload
	je	.LBB11_157
# BB#156:                               #   in Loop: Header=BB11_16 Depth=1
	callq	_ZdaPv
.LBB11_157:                             # %_ZN11CStringBaseIcED2Ev.exit
                                        #   in Loop: Header=BB11_16 Depth=1
	incq	%rbx
	cmpq	176(%rsp), %rbx         # 8-byte Folded Reload
	jl	.LBB11_16
.LBB11_158:
	movq	152(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB11_161
# BB#159:                               # %.lr.ph.i106.preheader
	movq	168(%rsp), %rax
	testb	$1, %dl
	jne	.LBB11_162
# BB#160:
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	cmpq	$1, %rdx
	jne	.LBB11_163
	jmp	.LBB11_165
.LBB11_161:
	xorl	%ebx, %ebx
	jmp	.LBB11_165
.LBB11_162:                             # %.lr.ph.i106.prol
	movb	(%rax), %cl
	andb	$15, %cl
	movl	$1, %ebx
	shll	%cl, %ebx
	movl	$1, %ecx
	cmpq	$1, %rdx
	je	.LBB11_165
.LBB11_163:                             # %.lr.ph.i106.preheader.new
	subq	%rcx, %rdx
	leaq	(%rcx,%rcx,4), %rcx
	leaq	(%rax,%rcx,8), %rsi
	.p2align	4, 0x90
.LBB11_164:                             # %.lr.ph.i106
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %ecx
	movzbl	40(%rsi), %eax
	andb	$15, %cl
	movl	$1, %edi
	shll	%cl, %edi
	orl	%ebx, %edi
	andb	$15, %al
	movl	$1, %ebx
	movl	%eax, %ecx
	shll	%cl, %ebx
	orl	%edi, %ebx
	addq	$80, %rsi
	addq	$-2, %rdx
	jne	.LBB11_164
.LBB11_165:                             # %._crit_edge.i107
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
.Ltmp153:
	movl	$4, %edi
	callq	_Znam
.Ltmp154:
# BB#166:                               # %.noexc133
	movq	%rax, 48(%rsp)
	movb	$0, (%rax)
	movl	$4, 60(%rsp)
	xorl	%r13d, %r13d
	leaq	80(%rsp), %r14
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	jmp	.LBB11_167
.LBB11_180:                             # %vector.body277.preheader
                                        #   in Loop: Header=BB11_167 Depth=1
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB11_183
# BB#181:                               # %vector.body277.prol.preheader
                                        #   in Loop: Header=BB11_167 Depth=1
	negq	%rsi
	xorl	%ebp, %ebp
.LBB11_182:                             # %vector.body277.prol
                                        #   Parent Loop BB11_167 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%r15,%rbp)
	movups	%xmm1, 16(%r15,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB11_182
	jmp	.LBB11_184
.LBB11_183:                             #   in Loop: Header=BB11_167 Depth=1
	xorl	%ebp, %ebp
.LBB11_184:                             # %vector.body277.prol.loopexit
                                        #   in Loop: Header=BB11_167 Depth=1
	cmpq	$96, %rdx
	jb	.LBB11_187
# BB#185:                               # %vector.body277.preheader.new
                                        #   in Loop: Header=BB11_167 Depth=1
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r15,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB11_186:                             # %vector.body277
                                        #   Parent Loop BB11_167 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB11_186
.LBB11_187:                             # %middle.block278
                                        #   in Loop: Header=BB11_167 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB11_203
	jmp	.LBB11_210
	.p2align	4, 0x90
.LBB11_167:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_189 Depth 2
                                        #     Child Loop BB11_193 Depth 2
                                        #     Child Loop BB11_172 Depth 2
                                        #     Child Loop BB11_175 Depth 2
                                        #     Child Loop BB11_182 Depth 2
                                        #     Child Loop BB11_186 Depth 2
                                        #     Child Loop BB11_205 Depth 2
                                        #     Child Loop BB11_208 Depth 2
                                        #     Child Loop BB11_213 Depth 2
	btl	%r13d, %ebx
	jae	.LBB11_224
# BB#168:                               #   in Loop: Header=BB11_167 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
.Ltmp156:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp157:
# BB#169:                               #   in Loop: Header=BB11_167 Depth=1
	movq	%rbx, 16(%rsp)
	movb	$0, (%rbx)
	movl	$4, 28(%rsp)
	movl	$1043, %eax             # imm = 0x413
	btq	%r13, %rax
	jb	.LBB11_188
# BB#170:                               #   in Loop: Header=BB11_167 Depth=1
.Ltmp161:
	movl	%r13d, %edi
	movq	%r14, %rsi
	callq	_Z21ConvertUInt32ToStringjPc
.Ltmp162:
# BB#171:                               # %.noexc27.i
                                        #   in Loop: Header=BB11_167 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %ebx
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB11_172:                             #   Parent Loop BB11_167 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebx
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB11_172
# BB#173:                               # %_Z11MyStringLenIcEiPKT_.exit.i.i.i118
                                        #   in Loop: Header=BB11_167 Depth=1
	leal	1(%rbx), %ebp
	movslq	%ebp, %rdi
	cmpl	$-1, %ebx
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp163:
	callq	_Znam
.Ltmp164:
# BB#174:                               # %.noexc28.i
                                        #   in Loop: Header=BB11_167 Depth=1
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	%ebp, 12(%rsp)
	.p2align	4, 0x90
.LBB11_175:                             # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i.i121
                                        #   Parent Loop BB11_167 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r14), %edx
	incq	%r14
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB11_175
# BB#176:                               # %_Z11MyStringLenIcEiPKT_.exit.i.i31.i
                                        #   in Loop: Header=BB11_167 Depth=1
	movl	%ebx, 8(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 224(%rsp)
.Ltmp166:
	movl	$7, %edi
	callq	_Znam
.Ltmp167:
# BB#177:                               # %.noexc35.i
                                        #   in Loop: Header=BB11_167 Depth=1
	movq	%rax, 224(%rsp)
	movl	$7, 236(%rsp)
	movb	$67, (%rax)
	movb	$104, 1(%rax)
	movb	$101, 2(%rax)
	movb	$99, 3(%rax)
	movb	$107, 4(%rax)
	movb	$45, 5(%rax)
	movb	$0, 6(%rax)
	movl	$6, 232(%rsp)
.Ltmp169:
	leaq	224(%rsp), %rdi
	movq	%rsp, %rsi
	callq	_ZN11CStringBaseIcEpLERKS0_
.Ltmp170:
# BB#178:                               # %_ZplIcE11CStringBaseIT_EPKS1_RKS2_.exit.i
                                        #   in Loop: Header=BB11_167 Depth=1
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rax
	movb	$0, (%rax)
	movslq	232(%rsp), %rax
	leaq	1(%rax), %r14
	movl	28(%rsp), %ebp
	cmpl	%ebp, %r14d
	jne	.LBB11_195
# BB#179:                               # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i.i
                                        #   in Loop: Header=BB11_167 Depth=1
	movq	16(%rsp), %r15
	jmp	.LBB11_212
	.p2align	4, 0x90
.LBB11_188:                             #   in Loop: Header=BB11_167 Depth=1
	movq	_ZN8NArchive3NXzL7kChecksE(,%r13,8), %rbp
	movl	$0, 24(%rsp)
	movb	$0, (%rbx)
	movl	$-1, %r12d
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB11_189:                             #   Parent Loop BB11_167 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%r12d
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB11_189
# BB#190:                               # %_Z11MyStringLenIcEiPKT_.exit.i.i110
                                        #   in Loop: Header=BB11_167 Depth=1
	cmpl	$3, %r12d
	je	.LBB11_193
# BB#191:                               #   in Loop: Header=BB11_167 Depth=1
	leal	1(%r12), %r14d
	movslq	%r14d, %rdi
	cmpl	$-1, %r12d
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp159:
	callq	_Znam
	movq	%rax, %r15
.Ltmp160:
# BB#192:                               # %._crit_edge17.i.i.i111
                                        #   in Loop: Header=BB11_167 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
	movq	%r15, 16(%rsp)
	movb	$0, (%r15,%rax)
	movl	%r14d, 28(%rsp)
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB11_193:                             # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i114
                                        #   Parent Loop BB11_167 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp), %eax
	incq	%rbp
	movb	%al, (%rbx)
	incq	%rbx
	testb	%al, %al
	jne	.LBB11_193
# BB#194:                               # %_ZN11CStringBaseIcEaSEPKc.exit.i115
                                        #   in Loop: Header=BB11_167 Depth=1
	movl	%r12d, 24(%rsp)
	cmpl	$0, 56(%rsp)
	jne	.LBB11_219
	jmp	.LBB11_220
.LBB11_195:                             #   in Loop: Header=BB11_167 Depth=1
	cmpl	$-1, %eax
	movq	%r14, %rdi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp172:
	callq	_Znam
	movq	%rax, %r15
.Ltmp173:
# BB#196:                               # %.noexc49.i
                                        #   in Loop: Header=BB11_167 Depth=1
	testl	%ebp, %ebp
	jle	.LBB11_211
# BB#197:                               # %.preheader.i.i37.i
                                        #   in Loop: Header=BB11_167 Depth=1
	movslq	24(%rsp), %rax
	testq	%rax, %rax
	movq	16(%rsp), %rdi
	jle	.LBB11_209
# BB#198:                               # %.lr.ph.preheader.i.i38.i
                                        #   in Loop: Header=BB11_167 Depth=1
	cmpl	$31, %eax
	jbe	.LBB11_202
# BB#199:                               # %min.iters.checked281
                                        #   in Loop: Header=BB11_167 Depth=1
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB11_202
# BB#200:                               # %vector.memcheck292
                                        #   in Loop: Header=BB11_167 Depth=1
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r15
	jae	.LBB11_180
# BB#201:                               # %vector.memcheck292
                                        #   in Loop: Header=BB11_167 Depth=1
	leaq	(%r15,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB11_180
.LBB11_202:                             #   in Loop: Header=BB11_167 Depth=1
	xorl	%ecx, %ecx
.LBB11_203:                             # %.lr.ph.i.i43.i.preheader
                                        #   in Loop: Header=BB11_167 Depth=1
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB11_206
# BB#204:                               # %.lr.ph.i.i43.i.prol.preheader
                                        #   in Loop: Header=BB11_167 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB11_205:                             # %.lr.ph.i.i43.i.prol
                                        #   Parent Loop BB11_167 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r15,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB11_205
.LBB11_206:                             # %.lr.ph.i.i43.i.prol.loopexit
                                        #   in Loop: Header=BB11_167 Depth=1
	cmpq	$7, %rdx
	jb	.LBB11_210
# BB#207:                               # %.lr.ph.i.i43.i.preheader.new
                                        #   in Loop: Header=BB11_167 Depth=1
	subq	%rcx, %rax
	leaq	7(%r15,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB11_208:                             # %.lr.ph.i.i43.i
                                        #   Parent Loop BB11_167 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB11_208
	jmp	.LBB11_210
.LBB11_209:                             # %._crit_edge.i.i39.i
                                        #   in Loop: Header=BB11_167 Depth=1
	testq	%rdi, %rdi
	je	.LBB11_211
.LBB11_210:                             # %._crit_edge.thread.i.i44.i
                                        #   in Loop: Header=BB11_167 Depth=1
	callq	_ZdaPv
.LBB11_211:                             # %._crit_edge17.i.i45.i
                                        #   in Loop: Header=BB11_167 Depth=1
	movq	%r15, 16(%rsp)
	movslq	24(%rsp), %rax
	movb	$0, (%r15,%rax)
	movl	%r14d, 28(%rsp)
.LBB11_212:                             # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i46.i
                                        #   in Loop: Header=BB11_167 Depth=1
	movq	224(%rsp), %rax
	.p2align	4, 0x90
.LBB11_213:                             #   Parent Loop BB11_167 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%r15)
	incq	%r15
	testb	%cl, %cl
	jne	.LBB11_213
# BB#214:                               #   in Loop: Header=BB11_167 Depth=1
	movl	232(%rsp), %eax
	movl	%eax, 24(%rsp)
	movq	224(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_216
# BB#215:                               #   in Loop: Header=BB11_167 Depth=1
	callq	_ZdaPv
.LBB11_216:                             # %_ZN11CStringBaseIcED2Ev.exit.i125
                                        #   in Loop: Header=BB11_167 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_218
# BB#217:                               #   in Loop: Header=BB11_167 Depth=1
	callq	_ZdaPv
.LBB11_218:                             #   in Loop: Header=BB11_167 Depth=1
	cmpl	$0, 56(%rsp)
	je	.LBB11_220
.LBB11_219:                             #   in Loop: Header=BB11_167 Depth=1
.Ltmp175:
	movl	$32, %esi
	leaq	48(%rsp), %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp176:
.LBB11_220:                             # %.noexc53.i129
                                        #   in Loop: Header=BB11_167 Depth=1
.Ltmp177:
	leaq	48(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	_ZN11CStringBaseIcEpLERKS0_
.Ltmp178:
# BB#221:                               # %_ZN8NArchive3NXzL9AddStringER11CStringBaseIcERKS2_.exit.i
                                        #   in Loop: Header=BB11_167 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_223
# BB#222:                               #   in Loop: Header=BB11_167 Depth=1
	callq	_ZdaPv
.LBB11_223:                             # %_ZN11CStringBaseIcED2Ev.exit55.i130
                                        #   in Loop: Header=BB11_167 Depth=1
	movq	112(%rsp), %rbx         # 8-byte Reload
	leaq	80(%rsp), %r14
.LBB11_224:                             #   in Loop: Header=BB11_167 Depth=1
	incq	%r13
	cmpq	$16, %r13
	jb	.LBB11_167
# BB#225:                               # %_ZN8NArchive3NXzL14GetCheckStringERK4CXzs.exit
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	168(%rax), %rbx
	cmpl	$0, 176(%rax)
	je	.LBB11_227
# BB#226:
.Ltmp180:
	movl	$32, %esi
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp181:
.LBB11_227:                             # %.noexc136
.Ltmp182:
	leaq	48(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIcEpLERKS0_
.Ltmp183:
# BB#228:                               # %_ZN8NArchive3NXzL9AddStringER11CStringBaseIcERKS2_.exit138
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_230
# BB#229:
	callq	_ZdaPv
.LBB11_230:                             # %_ZN11CStringBaseIcED2Ev.exit139
	xorl	%r15d, %r15d
	movq	136(%rsp), %rax         # 8-byte Reload
	cmpq	$0, (%rax)
	je	.LBB11_238
.LBB11_231:                             # %.thread149
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp185:
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp186:
# BB#232:
	testl	%ebp, %ebp
	jne	.LBB11_249
# BB#233:
.Ltmp187:
	leaq	224(%rsp), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	_ZN16CSeqInStreamWrapC1EP19ISequentialInStream
.Ltmp188:
# BB#234:
.Ltmp190:
	leaq	80(%rsp), %rdi
	leaq	224(%rsp), %rsi
	callq	Xz_ReadHeader
.Ltmp191:
# BB#235:
	testl	%eax, %eax
	je	.LBB11_237
# BB#236:
	movl	$1, %ebp
	cmpl	$17, %r15d
	jne	.LBB11_238
	jmp	.LBB11_249
.LBB11_237:                             # %.thread224
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	$0, 136(%rax)
	movb	$1, 184(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 192(%rax)
	xorl	%r15d, %r15d
.LBB11_238:                             # %.thread222
.Ltmp193:
	movl	%r15d, %edi
	callq	_Z13SResToHRESULTi
	movl	%eax, %ebp
.Ltmp194:
# BB#239:
	testl	%ebp, %ebp
	jne	.LBB11_249
# BB#240:
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB11_242
# BB#241:
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp195:
	callq	*8(%rax)
.Ltmp196:
.LBB11_242:                             # %.noexc141
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	208(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB11_244
# BB#243:
	movq	(%rdi), %rax
.Ltmp197:
	callq	*16(%rax)
.Ltmp198:
.LBB11_244:
	movq	40(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 208(%rcx)
	je	.LBB11_246
# BB#245:
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp199:
	callq	*8(%rax)
.Ltmp200:
.LBB11_246:                             # %.noexc143
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	216(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB11_248
# BB#247:
	movq	(%rdi), %rax
.Ltmp201:
	callq	*16(%rax)
.Ltmp202:
.LBB11_248:                             # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 216(%rcx)
	xorl	%ebp, %ebp
.LBB11_249:
	leaq	152(%rsp), %rdi
	movl	$_ZL7g_Alloc, %esi
	callq	Xzs_Free
.LBB11_250:
	movl	%ebp, %eax
	addq	$16872, %rsp            # imm = 0x41E8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_251:                             # %_ZN11CStringBaseIcED2Ev.exit51.i.i
.Ltmp125:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB11_260
.LBB11_252:                             # %_ZN11CStringBaseIcED2Ev.exit24.i.i
.Ltmp133:
	movq	%rax, %r14
	movq	%r13, %rdi
	callq	_ZdaPv
	jmp	.LBB11_260
.LBB11_253:
.Ltmp84:
	jmp	.LBB11_286
.LBB11_254:
.Ltmp139:
	movq	%rax, %r14
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_260
# BB#255:
	callq	_ZdaPv
	jmp	.LBB11_260
.LBB11_256:
.Ltmp136:
	jmp	.LBB11_259
.LBB11_257:
.Ltmp122:
	jmp	.LBB11_259
.LBB11_258:
.Ltmp130:
.LBB11_259:                             # %_ZN11CStringBaseIcED2Ev.exit57.i.i
	movq	%rax, %r14
.LBB11_260:                             # %_ZN11CStringBaseIcED2Ev.exit57.i.i
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB11_292
.LBB11_261:
.Ltmp115:
	movq	%rax, %r14
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_292
# BB#262:
	callq	_ZdaPv
	jmp	.LBB11_292
.LBB11_263:
.Ltmp155:
	jmp	.LBB11_286
.LBB11_264:
.Ltmp81:
	jmp	.LBB11_286
.LBB11_265:
.Ltmp78:
	jmp	.LBB11_286
.LBB11_266:
.Ltmp192:
	jmp	.LBB11_286
.LBB11_267:
.Ltmp189:
	jmp	.LBB11_286
.LBB11_268:
.Ltmp147:
	movq	%rax, %r14
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_292
# BB#269:
	callq	_ZdaPv
	jmp	.LBB11_292
.LBB11_270:
.Ltmp112:
	jmp	.LBB11_291
.LBB11_271:
.Ltmp184:
	movq	%rax, %r14
	jmp	.LBB11_297
.LBB11_272:
.Ltmp144:
	jmp	.LBB11_291
.LBB11_273:
.Ltmp95:
	jmp	.LBB11_291
.LBB11_274:
.Ltmp107:
	jmp	.LBB11_291
.LBB11_275:
.Ltmp87:
	jmp	.LBB11_286
.LBB11_276:
.Ltmp69:
	jmp	.LBB11_286
.LBB11_277:
.Ltmp174:
	jmp	.LBB11_280
.LBB11_278:
.Ltmp152:
	jmp	.LBB11_291
.LBB11_279:
.Ltmp171:
.LBB11_280:
	movq	%rax, %r14
	movq	224(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_283
# BB#281:
	callq	_ZdaPv
	jmp	.LBB11_283
.LBB11_282:
.Ltmp168:
	movq	%rax, %r14
.LBB11_283:                             # %_ZN11CStringBaseIcED2Ev.exit51.i
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_295
# BB#284:
	callq	_ZdaPv
	jmp	.LBB11_295
.LBB11_285:
.Ltmp203:
.LBB11_286:
	movq	%rax, %r14
	jmp	.LBB11_299
.LBB11_287:
.Ltmp158:
	movq	%rax, %r14
	jmp	.LBB11_297
.LBB11_288:
.Ltmp165:
	jmp	.LBB11_294
.LBB11_289:
.Ltmp104:
	jmp	.LBB11_291
.LBB11_290:
.Ltmp90:
.LBB11_291:
	movq	%rax, %r14
.LBB11_292:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB11_298
	jmp	.LBB11_299
.LBB11_293:
.Ltmp179:
.LBB11_294:
	movq	%rax, %r14
.LBB11_295:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_297
# BB#296:
	callq	_ZdaPv
.LBB11_297:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_299
.LBB11_298:
	callq	_ZdaPv
.LBB11_299:
.Ltmp204:
	leaq	152(%rsp), %rdi
	movl	$_ZL7g_Alloc, %esi
	callq	Xzs_Free
.Ltmp205:
# BB#300:                               # %_ZN8NArchive3NXz7CXzsCPPD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB11_301:
.Ltmp206:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN8NArchive3NXz8CHandler5Open2EP9IInStreamP20IArchiveOpenCallback, .Lfunc_end11-_ZN8NArchive3NXz8CHandler5Open2EP9IInStreamP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\236\204\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\225\004"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp67-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp67
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin3   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp75-.Ltmp70         #   Call between .Ltmp70 and .Ltmp75
	.long	.Ltmp203-.Lfunc_begin3  #     jumps to .Ltmp203
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin3   #     jumps to .Ltmp78
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin3   #     jumps to .Ltmp81
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin3   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin3   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin3   #     jumps to .Ltmp90
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp94-.Ltmp91         #   Call between .Ltmp91 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin3   #     jumps to .Ltmp95
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp97-.Ltmp96         #   Call between .Ltmp96 and .Ltmp97
	.long	.Ltmp107-.Lfunc_begin3  #     jumps to .Ltmp107
	.byte	0                       #   On action: cleanup
	.long	.Ltmp140-.Lfunc_begin3  # >> Call Site 11 <<
	.long	.Ltmp143-.Ltmp140       #   Call between .Ltmp140 and .Ltmp143
	.long	.Ltmp144-.Lfunc_begin3  #     jumps to .Ltmp144
	.byte	0                       #   On action: cleanup
	.long	.Ltmp108-.Lfunc_begin3  # >> Call Site 12 <<
	.long	.Ltmp111-.Ltmp108       #   Call between .Ltmp108 and .Ltmp111
	.long	.Ltmp112-.Lfunc_begin3  #     jumps to .Ltmp112
	.byte	0                       #   On action: cleanup
	.long	.Ltmp113-.Lfunc_begin3  # >> Call Site 13 <<
	.long	.Ltmp114-.Ltmp113       #   Call between .Ltmp113 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin3  #     jumps to .Ltmp115
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin3   # >> Call Site 14 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp107-.Lfunc_begin3  #     jumps to .Ltmp107
	.byte	0                       #   On action: cleanup
	.long	.Ltmp100-.Lfunc_begin3  # >> Call Site 15 <<
	.long	.Ltmp103-.Ltmp100       #   Call between .Ltmp100 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin3  #     jumps to .Ltmp104
	.byte	0                       #   On action: cleanup
	.long	.Ltmp105-.Lfunc_begin3  # >> Call Site 16 <<
	.long	.Ltmp106-.Ltmp105       #   Call between .Ltmp105 and .Ltmp106
	.long	.Ltmp107-.Lfunc_begin3  #     jumps to .Ltmp107
	.byte	0                       #   On action: cleanup
	.long	.Ltmp116-.Lfunc_begin3  # >> Call Site 17 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp144-.Lfunc_begin3  #     jumps to .Ltmp144
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin3  # >> Call Site 18 <<
	.long	.Ltmp129-.Ltmp126       #   Call between .Ltmp126 and .Ltmp129
	.long	.Ltmp130-.Lfunc_begin3  #     jumps to .Ltmp130
	.byte	0                       #   On action: cleanup
	.long	.Ltmp131-.Lfunc_begin3  # >> Call Site 19 <<
	.long	.Ltmp132-.Ltmp131       #   Call between .Ltmp131 and .Ltmp132
	.long	.Ltmp133-.Lfunc_begin3  #     jumps to .Ltmp133
	.byte	0                       #   On action: cleanup
	.long	.Ltmp118-.Lfunc_begin3  # >> Call Site 20 <<
	.long	.Ltmp121-.Ltmp118       #   Call between .Ltmp118 and .Ltmp121
	.long	.Ltmp122-.Lfunc_begin3  #     jumps to .Ltmp122
	.byte	0                       #   On action: cleanup
	.long	.Ltmp123-.Lfunc_begin3  # >> Call Site 21 <<
	.long	.Ltmp124-.Ltmp123       #   Call between .Ltmp123 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin3  #     jumps to .Ltmp125
	.byte	0                       #   On action: cleanup
	.long	.Ltmp134-.Lfunc_begin3  # >> Call Site 22 <<
	.long	.Ltmp135-.Ltmp134       #   Call between .Ltmp134 and .Ltmp135
	.long	.Ltmp136-.Lfunc_begin3  #     jumps to .Ltmp136
	.byte	0                       #   On action: cleanup
	.long	.Ltmp137-.Lfunc_begin3  # >> Call Site 23 <<
	.long	.Ltmp138-.Ltmp137       #   Call between .Ltmp137 and .Ltmp138
	.long	.Ltmp139-.Lfunc_begin3  #     jumps to .Ltmp139
	.byte	0                       #   On action: cleanup
	.long	.Ltmp145-.Lfunc_begin3  # >> Call Site 24 <<
	.long	.Ltmp146-.Ltmp145       #   Call between .Ltmp145 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin3  #     jumps to .Ltmp147
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin3  # >> Call Site 25 <<
	.long	.Ltmp151-.Ltmp148       #   Call between .Ltmp148 and .Ltmp151
	.long	.Ltmp152-.Lfunc_begin3  #     jumps to .Ltmp152
	.byte	0                       #   On action: cleanup
	.long	.Ltmp153-.Lfunc_begin3  # >> Call Site 26 <<
	.long	.Ltmp154-.Ltmp153       #   Call between .Ltmp153 and .Ltmp154
	.long	.Ltmp155-.Lfunc_begin3  #     jumps to .Ltmp155
	.byte	0                       #   On action: cleanup
	.long	.Ltmp156-.Lfunc_begin3  # >> Call Site 27 <<
	.long	.Ltmp157-.Ltmp156       #   Call between .Ltmp156 and .Ltmp157
	.long	.Ltmp158-.Lfunc_begin3  #     jumps to .Ltmp158
	.byte	0                       #   On action: cleanup
	.long	.Ltmp161-.Lfunc_begin3  # >> Call Site 28 <<
	.long	.Ltmp164-.Ltmp161       #   Call between .Ltmp161 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin3  #     jumps to .Ltmp165
	.byte	0                       #   On action: cleanup
	.long	.Ltmp166-.Lfunc_begin3  # >> Call Site 29 <<
	.long	.Ltmp167-.Ltmp166       #   Call between .Ltmp166 and .Ltmp167
	.long	.Ltmp168-.Lfunc_begin3  #     jumps to .Ltmp168
	.byte	0                       #   On action: cleanup
	.long	.Ltmp169-.Lfunc_begin3  # >> Call Site 30 <<
	.long	.Ltmp170-.Ltmp169       #   Call between .Ltmp169 and .Ltmp170
	.long	.Ltmp171-.Lfunc_begin3  #     jumps to .Ltmp171
	.byte	0                       #   On action: cleanup
	.long	.Ltmp159-.Lfunc_begin3  # >> Call Site 31 <<
	.long	.Ltmp160-.Ltmp159       #   Call between .Ltmp159 and .Ltmp160
	.long	.Ltmp179-.Lfunc_begin3  #     jumps to .Ltmp179
	.byte	0                       #   On action: cleanup
	.long	.Ltmp172-.Lfunc_begin3  # >> Call Site 32 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	.Ltmp174-.Lfunc_begin3  #     jumps to .Ltmp174
	.byte	0                       #   On action: cleanup
	.long	.Ltmp175-.Lfunc_begin3  # >> Call Site 33 <<
	.long	.Ltmp178-.Ltmp175       #   Call between .Ltmp175 and .Ltmp178
	.long	.Ltmp179-.Lfunc_begin3  #     jumps to .Ltmp179
	.byte	0                       #   On action: cleanup
	.long	.Ltmp180-.Lfunc_begin3  # >> Call Site 34 <<
	.long	.Ltmp183-.Ltmp180       #   Call between .Ltmp180 and .Ltmp183
	.long	.Ltmp184-.Lfunc_begin3  #     jumps to .Ltmp184
	.byte	0                       #   On action: cleanup
	.long	.Ltmp185-.Lfunc_begin3  # >> Call Site 35 <<
	.long	.Ltmp186-.Ltmp185       #   Call between .Ltmp185 and .Ltmp186
	.long	.Ltmp203-.Lfunc_begin3  #     jumps to .Ltmp203
	.byte	0                       #   On action: cleanup
	.long	.Ltmp187-.Lfunc_begin3  # >> Call Site 36 <<
	.long	.Ltmp188-.Ltmp187       #   Call between .Ltmp187 and .Ltmp188
	.long	.Ltmp189-.Lfunc_begin3  #     jumps to .Ltmp189
	.byte	0                       #   On action: cleanup
	.long	.Ltmp190-.Lfunc_begin3  # >> Call Site 37 <<
	.long	.Ltmp191-.Ltmp190       #   Call between .Ltmp190 and .Ltmp191
	.long	.Ltmp192-.Lfunc_begin3  #     jumps to .Ltmp192
	.byte	0                       #   On action: cleanup
	.long	.Ltmp193-.Lfunc_begin3  # >> Call Site 38 <<
	.long	.Ltmp202-.Ltmp193       #   Call between .Ltmp193 and .Ltmp202
	.long	.Ltmp203-.Lfunc_begin3  #     jumps to .Ltmp203
	.byte	0                       #   On action: cleanup
	.long	.Ltmp202-.Lfunc_begin3  # >> Call Site 39 <<
	.long	.Ltmp204-.Ltmp202       #   Call between .Ltmp202 and .Ltmp204
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp204-.Lfunc_begin3  # >> Call Site 40 <<
	.long	.Ltmp205-.Ltmp204       #   Call between .Ltmp204 and .Ltmp205
	.long	.Ltmp206-.Lfunc_begin3  #     jumps to .Ltmp206
	.byte	1                       #   On action: 1
	.long	.Ltmp205-.Lfunc_begin3  # >> Call Site 41 <<
	.long	.Lfunc_end11-.Ltmp205   #   Call between .Ltmp205 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3NXz8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback,@function
_ZN8NArchive3NXz8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback: # @_ZN8NArchive3NXz8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 32
.Lcfi39:
	.cfi_offset %rbx, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	(%rbx), %rax
.Ltmp207:
	callq	*48(%rax)
.Ltmp208:
# BB#1:
.Ltmp209:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	_ZN8NArchive3NXz8CHandler5Open2EP9IInStreamP20IArchiveOpenCallback
	movl	%eax, %ebx
.Ltmp210:
.LBB12_7:
	movl	%ebx, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB12_6:
.Ltmp211:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movl	$1, %ebx
.Ltmp212:
	callq	__cxa_end_catch
.Ltmp213:
	jmp	.LBB12_7
.LBB12_2:
.Ltmp214:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB12_3
# BB#4:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebx      # imm = 0x8007000E
	jmp	.LBB12_7
.LBB12_3:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp215:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp216:
# BB#8:
.LBB12_5:
.Ltmp217:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN8NArchive3NXz8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback, .Lfunc_end12-_ZN8NArchive3NXz8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\334"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp207-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp210-.Ltmp207       #   Call between .Ltmp207 and .Ltmp210
	.long	.Ltmp211-.Lfunc_begin4  #     jumps to .Ltmp211
	.byte	1                       #   On action: 1
	.long	.Ltmp210-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp212-.Ltmp210       #   Call between .Ltmp210 and .Ltmp212
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp212-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp213-.Ltmp212       #   Call between .Ltmp212 and .Ltmp213
	.long	.Ltmp214-.Lfunc_begin4  #     jumps to .Ltmp214
	.byte	3                       #   On action: 2
	.long	.Ltmp213-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp215-.Ltmp213       #   Call between .Ltmp213 and .Ltmp215
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp215-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp216-.Ltmp215       #   Call between .Ltmp215 and .Ltmp216
	.long	.Ltmp217-.Lfunc_begin4  #     jumps to .Ltmp217
	.byte	0                       #   On action: cleanup
	.long	.Ltmp216-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Lfunc_end12-.Ltmp216   #   Call between .Ltmp216 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream,@function
_ZN8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream: # @_ZN8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -24
.Lcfi46:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*48(%rax)
	testq	%r14, %r14
	je	.LBB13_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB13_2:
	movq	216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB13_4:                               # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	%r14, 216(%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end13:
	.size	_ZN8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream, .Lfunc_end13-_ZN8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZThn8_N8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream,@function
_ZThn8_N8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream: # @_ZThn8_N8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 32
.Lcfi50:
	.cfi_offset %rbx, -24
.Lcfi51:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	-8(%rbx), %rax
	addq	$-8, %rbx
	movq	%rbx, %rdi
	callq	*48(%rax)
	testq	%r14, %r14
	je	.LBB14_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB14_2:
	movq	216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB14_4:                               # %_ZN8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream.exit
	movq	%r14, 216(%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end14:
	.size	_ZThn8_N8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream, .Lfunc_end14-_ZThn8_N8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN8NArchive3NXz8CHandler5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler5CloseEv,@function
_ZN8NArchive3NXz8CHandler5CloseEv:      # @_ZN8NArchive3NXz8CHandler5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 16
.Lcfi53:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$0, 160(%rbx)
	movb	$1, 184(%rbx)
	movl	$0, 176(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 192(%rbx)
	movq	168(%rbx), %rax
	movb	$0, (%rax)
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 208(%rbx)
.LBB15_2:                               # %_ZN9CMyComPtrI9IInStreamE7ReleaseEv.exit
	movq	216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 216(%rbx)
.LBB15_4:                               # %_ZN9CMyComPtrI19ISequentialInStreamE7ReleaseEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end15:
	.size	_ZN8NArchive3NXz8CHandler5CloseEv, .Lfunc_end15-_ZN8NArchive3NXz8CHandler5CloseEv
	.cfi_endproc

	.globl	_ZN8NArchive3NXz16CSeekToSeqStream4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz16CSeekToSeqStream4ReadEPvjPj,@function
_ZN8NArchive3NXz16CSeekToSeqStream4ReadEPvjPj: # @_ZN8NArchive3NXz16CSeekToSeqStream4ReadEPvjPj
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end16:
	.size	_ZN8NArchive3NXz16CSeekToSeqStream4ReadEPvjPj, .Lfunc_end16-_ZN8NArchive3NXz16CSeekToSeqStream4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN8NArchive3NXz16CSeekToSeqStream4SeekExjPy
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz16CSeekToSeqStream4SeekExjPy,@function
_ZN8NArchive3NXz16CSeekToSeqStream4SeekExjPy: # @_ZN8NArchive3NXz16CSeekToSeqStream4SeekExjPy
	.cfi_startproc
# BB#0:
	movl	$-2147467263, %eax      # imm = 0x80004001
	retq
.Lfunc_end17:
	.size	_ZN8NArchive3NXz16CSeekToSeqStream4SeekExjPy, .Lfunc_end17-_ZN8NArchive3NXz16CSeekToSeqStream4SeekExjPy
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_0:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
	.text
	.globl	_ZN8NArchive3NXz8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler7ExtractEPKjjiP23IArchiveExtractCallback,@function
_ZN8NArchive3NXz8CHandler7ExtractEPKjjiP23IArchiveExtractCallback: # @_ZN8NArchive3NXz8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	subq	$1928, %rsp             # imm = 0x788
.Lcfi60:
	.cfi_def_cfa_offset 1984
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movl	%ecx, %ebx
	movq	%rdi, %r15
	cmpl	$-1, %edx
	je	.LBB18_6
# BB#1:
	testl	%edx, %edx
	je	.LBB18_2
# BB#3:
	cmpl	$1, %edx
	jne	.LBB18_5
# BB#4:
	cmpl	$0, (%rsi)
	je	.LBB18_6
.LBB18_5:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	jmp	.LBB18_98
.LBB18_6:
	movq	(%r12), %rax
	movq	144(%r15), %rsi
.Ltmp218:
	movq	%r12, %rdi
	callq	*40(%rax)
.Ltmp219:
# BB#7:
	movq	$0, 80(%rsp)
	movq	(%r12), %rax
.Ltmp221:
	leaq	80(%rsp), %rsi
	movq	%r12, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp222:
# BB#8:
	testl	%ebp, %ebp
	jne	.LBB18_98
# BB#9:
	movq	$0, 24(%rsp)
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	setne	%r14b
	movq	(%r12), %rax
.Ltmp224:
	leaq	24(%rsp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%r14d, %ecx
	callq	*56(%rax)
	movl	%eax, %ebp
.Ltmp225:
# BB#10:
	testl	%ebp, %ebp
	je	.LBB18_11
.LBB18_76:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit173
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_98
# BB#77:
	movq	(%rdi), %rax
.Ltmp293:
	callq	*16(%rax)
.Ltmp294:
	jmp	.LBB18_98
.LBB18_2:
	xorl	%ebp, %ebp
.LBB18_98:
	movl	%ebp, %eax
	addq	$1928, %rsp             # imm = 0x788
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_11:
	testl	%ebx, %ebx
	sete	%al
	cmpq	$0, 24(%rsp)
	jne	.LBB18_13
# BB#12:
	xorl	%ebp, %ebp
	testb	%al, %al
	jne	.LBB18_98
.LBB18_13:
	movq	(%r12), %rax
.Ltmp226:
	movq	%r12, %rdi
	movl	%r14d, %esi
	callq	*64(%rax)
.Ltmp227:
# BB#14:
	movq	208(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB18_17
# BB#15:
	movq	(%rdi), %rax
	movq	136(%r15), %rsi
.Ltmp228:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp229:
# BB#16:
	testl	%ebp, %ebp
	jne	.LBB18_76
.LBB18_17:
.Ltmp230:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp231:
# BB#18:
.Ltmp233:
	movq	%r14, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp234:
# BB#19:
	movq	(%r14), %rax
.Ltmp236:
	movq	%r14, %rdi
	callq	*8(%rax)
.Ltmp237:
# BB#20:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit
.Ltmp239:
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp240:
# BB#21:
.Ltmp241:
	leaq	88(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN21CCompressProgressWrapC1EP21ICompressProgressInfo
.Ltmp242:
# BB#22:
	movl	$0, 20(%rsp)
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 112(%rsp)
	leaq	128(%rsp), %r13
.Ltmp244:
	movl	$_ZL7g_Alloc, %esi
	movq	%r13, %rdi
	callq	XzUnpacker_Create
	movl	%eax, %ecx
.Ltmp245:
# BB#23:
	testl	%ecx, %ecx
	je	.LBB18_24
.LBB18_50:                              # %.thread
	cmpl	$17, %ecx
	ja	.LBB18_66
.LBB18_51:                              # %.thread
	movl	$196610, %eax           # imm = 0x30002
	btl	%ecx, %eax
	jb	.LBB18_67
# BB#52:                                # %.thread
	movl	$9, %eax
	btl	%ecx, %eax
	jb	.LBB18_68
# BB#53:                                # %.thread
	cmpl	$4, %ecx
	jne	.LBB18_66
# BB#54:
	movl	$1, %ecx
	jmp	.LBB18_68
.LBB18_24:
.Ltmp246:
	movl	$32768, %edi            # imm = 0x8000
	callq	MyAlloc
.Ltmp247:
# BB#25:
	movq	%rax, 112(%rsp)
.Ltmp248:
	movl	$2097152, %edi          # imm = 0x200000
	callq	MyAlloc
.Ltmp249:
# BB#26:
	movq	%rax, 120(%rsp)
	testq	%rax, %rax
	movl	$2, %ecx
	je	.LBB18_66
# BB#27:
	movq	112(%rsp), %rax
	testq	%rax, %rax
	je	.LBB18_66
# BB#28:                                # %.preheader
	xorl	%edx, %edx
	xorl	%edi, %edi
	xorl	%ebp, %ebp
	movq	%r14, 40(%rsp)          # 8-byte Spill
	jmp	.LBB18_29
.LBB18_55:                              #   in Loop: Header=BB18_29 Depth=1
	movl	$0, 36(%rsp)            # 4-byte Folded Spill
	testl	%eax, %eax
	je	.LBB18_44
# BB#56:                                #   in Loop: Header=BB18_29 Depth=1
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_44
# BB#57:                                #   in Loop: Header=BB18_29 Depth=1
	movl	%ecx, %r13d
	movq	120(%rsp), %rsi
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movl	%eax, %edx
.Ltmp257:
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
.Ltmp258:
# BB#58:                                #   in Loop: Header=BB18_29 Depth=1
	testl	%eax, %eax
	movl	%r13d, %ecx
	je	.LBB18_44
# BB#59:                                #   in Loop: Header=BB18_29 Depth=1
	movl	$1, %ecx
	movl	%eax, %ebp
	movl	52(%rsp), %edx          # 4-byte Reload
	jmp	.LBB18_64
.LBB18_29:                              # =>This Inner Loop Header: Depth=1
	movl	20(%rsp), %eax
	cmpl	%eax, %edi
	jne	.LBB18_40
# BB#30:                                #   in Loop: Header=BB18_29 Depth=1
	movl	%edx, %ebx
	movl	$0, 20(%rsp)
	movq	216(%r15), %rdi
	movq	(%rdi), %rax
	movq	112(%rsp), %rsi
.Ltmp251:
	movl	$32768, %edx            # imm = 0x8000
	leaq	20(%rsp), %rcx
	callq	*40(%rax)
.Ltmp252:
# BB#31:                                #   in Loop: Header=BB18_29 Depth=1
	testl	%eax, %eax
	jne	.LBB18_32
# BB#39:                                # %._crit_edge
                                        #   in Loop: Header=BB18_29 Depth=1
	xorl	%edi, %edi
	movl	20(%rsp), %eax
	movl	%ebx, %edx
.LBB18_40:                              #   in Loop: Header=BB18_29 Depth=1
	movl	%eax, %ecx
	subl	%edi, %ecx
	movq	%rcx, 72(%rsp)
	movl	$2097152, %ecx          # imm = 0x200000
	subl	%edx, %ecx
	movq	%rcx, 64(%rsp)
	movl	%edx, %esi
	addq	120(%rsp), %rsi
	movl	%edi, 56(%rsp)          # 4-byte Spill
	movl	%edi, %ecx
	addq	112(%rsp), %rcx
	xorl	%r9d, %r9d
	testl	%eax, %eax
	sete	%r9b
.Ltmp254:
	leaq	60(%rsp), %rax
	movq	%rax, (%rsp)
	movq	%r13, %rdi
	movq	%r13, %rbx
	movl	%edx, %r13d
	leaq	64(%rsp), %rdx
	leaq	72(%rsp), %r8
	callq	XzUnpacker_Code
	movl	%eax, %ecx
.Ltmp255:
# BB#41:                                #   in Loop: Header=BB18_29 Depth=1
	movq	72(%rsp), %rbx
	movq	64(%rsp), %rax
	addl	%eax, %r13d
	movdqu	48(%r14), %xmm0
	movd	%rax, %xmm1
	movd	%rbx, %xmm2
	punpcklqdq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	paddq	%xmm0, %xmm2
	movdqu	%xmm2, 48(%r14)
	orq	%rbx, %rax
	sete	%al
	testl	%ecx, %ecx
	setne	%r14b
	orb	%al, %r14b
	cmpl	$2097152, %r13d         # imm = 0x200000
	movl	%r13d, %eax
	je	.LBB18_55
# BB#42:                                #   in Loop: Header=BB18_29 Depth=1
	testb	%r14b, %r14b
	jne	.LBB18_55
# BB#43:                                #   in Loop: Header=BB18_29 Depth=1
	movl	%eax, 36(%rsp)          # 4-byte Spill
.LBB18_44:                              #   in Loop: Header=BB18_29 Depth=1
	testb	%r14b, %r14b
	jne	.LBB18_45
# BB#62:                                #   in Loop: Header=BB18_29 Depth=1
.Ltmp259:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_ZN14CLocalProgress6SetCurEv
.Ltmp260:
# BB#63:                                #   in Loop: Header=BB18_29 Depth=1
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	cmovnel	%eax, %ebp
	movl	36(%rsp), %edx          # 4-byte Reload
.LBB18_64:                              #   in Loop: Header=BB18_29 Depth=1
	addl	56(%rsp), %ebx          # 4-byte Folded Reload
	testl	%ecx, %ecx
	movl	%ebx, %edi
	movq	40(%rsp), %r14          # 8-byte Reload
	leaq	128(%rsp), %r13
	je	.LBB18_29
	jmp	.LBB18_72
.LBB18_67:
	movl	$2, %ecx
.LBB18_68:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_71
# BB#69:
	movl	%ecx, %ebx
	movq	(%rdi), %rax
.Ltmp265:
	callq	*16(%rax)
.Ltmp266:
# BB#70:                                # %.noexc178
	movq	$0, 24(%rsp)
	movl	%ebx, %ecx
.LBB18_71:                              # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
	movq	(%r12), %rax
.Ltmp267:
	movq	%r12, %rdi
	movl	%ecx, %esi
	callq	*72(%rax)
	movl	%eax, %ebp
.Ltmp268:
	jmp	.LBB18_72
.LBB18_45:
	movq	40(%rsp), %r14          # 8-byte Reload
	movups	48(%r14), %xmm0
	movups	%xmm0, 144(%r15)
	movdqa	.LCPI18_0(%rip), %xmm0  # xmm0 = [1,1]
	movdqu	%xmm0, 192(%r15)
	testl	%ecx, %ecx
	leaq	128(%rsp), %r13
	jne	.LBB18_50
# BB#46:
	movl	$1, %ecx
	cmpl	$3, 60(%rsp)
	jne	.LBB18_50
# BB#47:
.Ltmp262:
	movq	%r13, %rdi
	callq	XzUnpacker_IsStreamWasFinished
.Ltmp263:
# BB#48:
	testl	%eax, %eax
	movl	$1, %ecx
	je	.LBB18_50
# BB#49:
	movq	192(%rsp), %rax
	subq	%rax, 144(%r15)
	xorl	%ecx, %ecx
	cmpl	$17, %ecx
	jbe	.LBB18_51
.LBB18_66:                              # %.thread.thread
.Ltmp269:
	movl	%ecx, %edi
	callq	_Z13SResToHRESULTi
	movl	%eax, %ebp
.Ltmp270:
.LBB18_72:                              # %.loopexit190
.Ltmp278:
	movq	%r13, %rdi
	callq	XzUnpacker_Free
.Ltmp279:
# BB#73:                                # %.noexc174
	movq	112(%rsp), %rdi
.Ltmp280:
	callq	MyFree
.Ltmp281:
# BB#74:                                # %.noexc175
	movq	120(%rsp), %rdi
.Ltmp282:
	callq	MyFree
.Ltmp283:
# BB#75:
	movq	(%r14), %rax
.Ltmp287:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp288:
	jmp	.LBB18_76
.LBB18_32:
	movl	%eax, %ebp
	jmp	.LBB18_72
.LBB18_61:                              # %.loopexit.split-lp192
.Ltmp264:
	jmp	.LBB18_80
.LBB18_84:
.Ltmp289:
	jmp	.LBB18_35
.LBB18_36:
.Ltmp238:
	jmp	.LBB18_35
.LBB18_88:
.Ltmp235:
	movq	%rdx, %r15
	movq	%rax, %rbp
	movq	%r14, %rdi
	callq	_ZdlPv
	jmp	.LBB18_89
.LBB18_79:
.Ltmp271:
	jmp	.LBB18_80
.LBB18_38:                              # %.loopexit.split-lp
.Ltmp250:
	jmp	.LBB18_80
.LBB18_85:
.Ltmp243:
	jmp	.LBB18_86
.LBB18_78:
.Ltmp284:
.LBB18_86:
	movq	%rdx, %r15
	movq	%rax, %rbp
	jmp	.LBB18_87
.LBB18_91:
.Ltmp295:
	jmp	.LBB18_93
.LBB18_34:
.Ltmp232:
.LBB18_35:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	%rdx, %r15
	movq	%rax, %rbp
	jmp	.LBB18_89
.LBB18_92:
.Ltmp223:
	jmp	.LBB18_93
.LBB18_33:
.Ltmp220:
.LBB18_93:
	movq	%rdx, %r15
	movq	%rax, %rbp
	jmp	.LBB18_94
.LBB18_37:                              # %.loopexit
.Ltmp253:
.LBB18_80:
	movq	%rdx, %r15
	movq	%rax, %rbp
	jmp	.LBB18_81
.LBB18_65:
.Ltmp256:
	movq	%rdx, %r15
	movq	%rax, %rbp
	movq	%rbx, %r13
	jmp	.LBB18_81
.LBB18_60:                              # %.loopexit191
.Ltmp261:
	movq	%rdx, %r15
	movq	%rax, %rbp
	movq	40(%rsp), %r14          # 8-byte Reload
	leaq	128(%rsp), %r13
.LBB18_81:
.Ltmp272:
	movq	%r13, %rdi
	callq	XzUnpacker_Free
.Ltmp273:
# BB#82:                                # %.noexc168
	movq	112(%rsp), %rdi
.Ltmp274:
	callq	MyFree
.Ltmp275:
# BB#83:                                # %.noexc169
	movq	120(%rsp), %rdi
.Ltmp276:
	callq	MyFree
.Ltmp277:
.LBB18_87:
	movq	(%r14), %rax
.Ltmp285:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp286:
.LBB18_89:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_94
# BB#90:
	movq	(%rdi), %rax
.Ltmp290:
	callq	*16(%rax)
.Ltmp291:
.LBB18_94:
	movq	%rbp, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r15d
	je	.LBB18_95
# BB#97:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB18_98
.LBB18_95:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp296:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp297:
# BB#100:
.LBB18_96:
.Ltmp298:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB18_99:
.Ltmp292:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN8NArchive3NXz8CHandler7ExtractEPKjjiP23IArchiveExtractCallback, .Lfunc_end18-_ZN8NArchive3NXz8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\255\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\236\002"              # Call site table length
	.long	.Ltmp218-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp219-.Ltmp218       #   Call between .Ltmp218 and .Ltmp219
	.long	.Ltmp220-.Lfunc_begin5  #     jumps to .Ltmp220
	.byte	3                       #   On action: 2
	.long	.Ltmp221-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp222-.Ltmp221       #   Call between .Ltmp221 and .Ltmp222
	.long	.Ltmp223-.Lfunc_begin5  #     jumps to .Ltmp223
	.byte	3                       #   On action: 2
	.long	.Ltmp224-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp225-.Ltmp224       #   Call between .Ltmp224 and .Ltmp225
	.long	.Ltmp232-.Lfunc_begin5  #     jumps to .Ltmp232
	.byte	3                       #   On action: 2
	.long	.Ltmp293-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp294-.Ltmp293       #   Call between .Ltmp293 and .Ltmp294
	.long	.Ltmp295-.Lfunc_begin5  #     jumps to .Ltmp295
	.byte	3                       #   On action: 2
	.long	.Ltmp226-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp231-.Ltmp226       #   Call between .Ltmp226 and .Ltmp231
	.long	.Ltmp232-.Lfunc_begin5  #     jumps to .Ltmp232
	.byte	3                       #   On action: 2
	.long	.Ltmp233-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp234-.Ltmp233       #   Call between .Ltmp233 and .Ltmp234
	.long	.Ltmp235-.Lfunc_begin5  #     jumps to .Ltmp235
	.byte	3                       #   On action: 2
	.long	.Ltmp236-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp237-.Ltmp236       #   Call between .Ltmp236 and .Ltmp237
	.long	.Ltmp238-.Lfunc_begin5  #     jumps to .Ltmp238
	.byte	3                       #   On action: 2
	.long	.Ltmp239-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp242-.Ltmp239       #   Call between .Ltmp239 and .Ltmp242
	.long	.Ltmp243-.Lfunc_begin5  #     jumps to .Ltmp243
	.byte	3                       #   On action: 2
	.long	.Ltmp244-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp249-.Ltmp244       #   Call between .Ltmp244 and .Ltmp249
	.long	.Ltmp250-.Lfunc_begin5  #     jumps to .Ltmp250
	.byte	3                       #   On action: 2
	.long	.Ltmp257-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp258-.Ltmp257       #   Call between .Ltmp257 and .Ltmp258
	.long	.Ltmp261-.Lfunc_begin5  #     jumps to .Ltmp261
	.byte	3                       #   On action: 2
	.long	.Ltmp251-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp252-.Ltmp251       #   Call between .Ltmp251 and .Ltmp252
	.long	.Ltmp253-.Lfunc_begin5  #     jumps to .Ltmp253
	.byte	3                       #   On action: 2
	.long	.Ltmp254-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Ltmp255-.Ltmp254       #   Call between .Ltmp254 and .Ltmp255
	.long	.Ltmp256-.Lfunc_begin5  #     jumps to .Ltmp256
	.byte	3                       #   On action: 2
	.long	.Ltmp259-.Lfunc_begin5  # >> Call Site 13 <<
	.long	.Ltmp260-.Ltmp259       #   Call between .Ltmp259 and .Ltmp260
	.long	.Ltmp261-.Lfunc_begin5  #     jumps to .Ltmp261
	.byte	3                       #   On action: 2
	.long	.Ltmp265-.Lfunc_begin5  # >> Call Site 14 <<
	.long	.Ltmp268-.Ltmp265       #   Call between .Ltmp265 and .Ltmp268
	.long	.Ltmp271-.Lfunc_begin5  #     jumps to .Ltmp271
	.byte	3                       #   On action: 2
	.long	.Ltmp262-.Lfunc_begin5  # >> Call Site 15 <<
	.long	.Ltmp263-.Ltmp262       #   Call between .Ltmp262 and .Ltmp263
	.long	.Ltmp264-.Lfunc_begin5  #     jumps to .Ltmp264
	.byte	3                       #   On action: 2
	.long	.Ltmp269-.Lfunc_begin5  # >> Call Site 16 <<
	.long	.Ltmp270-.Ltmp269       #   Call between .Ltmp269 and .Ltmp270
	.long	.Ltmp271-.Lfunc_begin5  #     jumps to .Ltmp271
	.byte	3                       #   On action: 2
	.long	.Ltmp278-.Lfunc_begin5  # >> Call Site 17 <<
	.long	.Ltmp283-.Ltmp278       #   Call between .Ltmp278 and .Ltmp283
	.long	.Ltmp284-.Lfunc_begin5  #     jumps to .Ltmp284
	.byte	3                       #   On action: 2
	.long	.Ltmp287-.Lfunc_begin5  # >> Call Site 18 <<
	.long	.Ltmp288-.Ltmp287       #   Call between .Ltmp287 and .Ltmp288
	.long	.Ltmp289-.Lfunc_begin5  #     jumps to .Ltmp289
	.byte	3                       #   On action: 2
	.long	.Ltmp272-.Lfunc_begin5  # >> Call Site 19 <<
	.long	.Ltmp291-.Ltmp272       #   Call between .Ltmp272 and .Ltmp291
	.long	.Ltmp292-.Lfunc_begin5  #     jumps to .Ltmp292
	.byte	1                       #   On action: 1
	.long	.Ltmp291-.Lfunc_begin5  # >> Call Site 20 <<
	.long	.Ltmp296-.Ltmp291       #   Call between .Ltmp291 and .Ltmp296
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp296-.Lfunc_begin5  # >> Call Site 21 <<
	.long	.Ltmp297-.Ltmp296       #   Call between .Ltmp296 and .Ltmp297
	.long	.Ltmp298-.Lfunc_begin5  #     jumps to .Ltmp298
	.byte	0                       #   On action: cleanup
	.long	.Ltmp297-.Lfunc_begin5  # >> Call Site 22 <<
	.long	.Lfunc_end18-.Ltmp297   #   Call between .Ltmp297 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3NXz8CHandler15GetFileTimeTypeEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler15GetFileTimeTypeEPj,@function
_ZN8NArchive3NXz8CHandler15GetFileTimeTypeEPj: # @_ZN8NArchive3NXz8CHandler15GetFileTimeTypeEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end19:
	.size	_ZN8NArchive3NXz8CHandler15GetFileTimeTypeEPj, .Lfunc_end19-_ZN8NArchive3NXz8CHandler15GetFileTimeTypeEPj
	.cfi_endproc

	.globl	_ZThn16_N8NArchive3NXz8CHandler15GetFileTimeTypeEPj
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive3NXz8CHandler15GetFileTimeTypeEPj,@function
_ZThn16_N8NArchive3NXz8CHandler15GetFileTimeTypeEPj: # @_ZThn16_N8NArchive3NXz8CHandler15GetFileTimeTypeEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end20:
	.size	_ZThn16_N8NArchive3NXz8CHandler15GetFileTimeTypeEPj, .Lfunc_end20-_ZThn16_N8NArchive3NXz8CHandler15GetFileTimeTypeEPj
	.cfi_endproc

	.globl	_ZN8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback,@function
_ZN8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback: # @_ZN8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi73:
	.cfi_def_cfa_offset 272
.Lcfi74:
	.cfi_offset %rbx, -56
.Lcfi75:
	.cfi_offset %r12, -48
.Lcfi76:
	.cfi_offset %r13, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movl	%edx, %ebx
	movq	%rsi, %r12
	movq	%rdi, %r14
	leaq	72(%rsp), %rdi
	callq	_ZN17CSeqOutStreamWrapC1EP20ISequentialOutStream
	cmpl	$1, %ebx
	je	.LBB21_3
# BB#1:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	testl	%ebx, %ebx
	jne	.LBB21_72
# BB#2:
	leaq	72(%rsp), %rdi
	callq	Xz_EncodeEmpty
	movl	%eax, %edi
	callq	_Z13SResToHRESULTi
.LBB21_71:
	movl	%eax, %ebp
	jmp	.LBB21_72
.LBB21_3:
	testq	%r15, %r15
	je	.LBB21_4
# BB#5:
	movq	(%r15), %rax
	leaq	28(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	20(%rsp), %r8
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	*56(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB21_72
# BB#6:
	cmpl	$0, 24(%rsp)
	je	.LBB21_16
# BB#7:
	movl	$0, 32(%rsp)
	movq	(%r15), %rax
.Ltmp299:
	leaq	32(%rsp), %rcx
	xorl	%esi, %esi
	movl	$6, %edx
	movq	%r15, %rdi
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp300:
# BB#8:
	testl	%ebp, %ebp
	je	.LBB21_9
.LBB21_12:                              # %.thread
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	jmp	.LBB21_72
.LBB21_4:
	movl	$-2147467259, %ebp      # imm = 0x80004005
.LBB21_72:
	movl	%ebp, %eax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_9:
	movzwl	32(%rsp), %eax
	testw	%ax, %ax
	je	.LBB21_15
# BB#10:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$11, %eax
	jne	.LBB21_12
# BB#11:
	cmpw	$0, 40(%rsp)
	jne	.LBB21_12
.LBB21_15:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.LBB21_16:
	cmpl	$0, 28(%rsp)
	je	.LBB21_65
# BB#17:
	movl	$0, 56(%rsp)
	movq	(%r15), %rax
.Ltmp304:
	leaq	56(%rsp), %rcx
	xorl	%esi, %esi
	movl	$7, %edx
	movq	%r15, %rdi
	callq	*64(%rax)
	movl	%eax, %ebx
.Ltmp305:
# BB#18:
	movl	$1, %r12d
	testl	%ebx, %ebx
	jne	.LBB21_22
# BB#19:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	movzwl	56(%rsp), %eax
	cmpl	$21, %eax
	jne	.LBB21_23
# BB#20:
	movq	64(%rsp), %rsi
	movq	(%r15), %rax
.Ltmp306:
	movq	%r15, %rdi
	callq	*40(%rax)
.Ltmp307:
# BB#21:
	xorl	%r12d, %r12d
	testl	%eax, %eax
	setne	%r12b
	cmovnel	%eax, %ebx
.LBB21_22:                              # %select.unfold
	movl	%ebx, %ebp
.LBB21_23:                              # %select.unfold
	leaq	56(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	testl	%r12d, %r12d
	jne	.LBB21_72
# BB#24:
	leaq	152(%rsp), %rdi
	callq	Lzma2EncProps_Init
	movl	108(%r14), %eax
	movl	%eax, 152(%rsp)
	movq	$0, 8(%rsp)
	movq	(%r15), %rax
.Ltmp311:
	leaq	8(%rsp), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	*72(%rax)
	movl	%eax, %ebp
.Ltmp312:
# BB#25:
	testl	%ebp, %ebp
	jne	.LBB21_54
# BB#26:
	movq	8(%rsp), %rsi
.Ltmp313:
	leaq	128(%rsp), %rdi
	callq	_ZN16CSeqInStreamWrapC1EP19ISequentialInStream
.Ltmp314:
# BB#27:                                # %.preheader171
	leaq	32(%r14), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	cmpl	$0, 52(%r14)
	jle	.LBB21_41
# BB#28:                                # %.lr.ph174
	xorl	%r12d, %r12d
.LBB21_29:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_36 Depth 2
	movq	56(%r14), %rax
	movq	(%rax,%r12,8), %rbx
	movl	32(%r14), %edx
.Ltmp316:
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	_ZN8NArchive11COutHandler21SetCompressionMethod2ERNS_14COneMethodInfoEj
.Ltmp317:
# BB#30:                                #   in Loop: Header=BB21_29 Depth=1
.Ltmp318:
	movq	%rbx, %rdi
	callq	_ZNK8NArchive14COneMethodInfo6IsLzmaEv
.Ltmp319:
# BB#31:                                #   in Loop: Header=BB21_29 Depth=1
	testb	%al, %al
	je	.LBB21_40
# BB#32:                                # %.preheader
                                        #   in Loop: Header=BB21_29 Depth=1
	cmpl	$0, 12(%rbx)
	jle	.LBB21_40
# BB#33:                                # %.lr.ph
                                        #   in Loop: Header=BB21_29 Depth=1
	xorl	%r13d, %r13d
.LBB21_36:                              #   Parent Loop BB21_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rax
	movq	(%rax,%r13,8), %rsi
	movl	(%rsi), %edi
	addq	$8, %rsi
.Ltmp321:
	leaq	152(%rsp), %rdx
	callq	_ZN9NCompress6NLzma212SetLzma2PropEjRK14tagPROPVARIANTR14CLzma2EncProps
	movl	%eax, %ebp
.Ltmp322:
# BB#37:                                #   in Loop: Header=BB21_36 Depth=2
	testl	%ebp, %ebp
	jne	.LBB21_54
# BB#35:                                #   in Loop: Header=BB21_36 Depth=2
	incq	%r13
	movslq	12(%rbx), %rax
	cmpq	%rax, %r13
	jl	.LBB21_36
.LBB21_40:                              # %.thread164
                                        #   in Loop: Header=BB21_29 Depth=1
	incq	%r12
	movslq	52(%r14), %rax
	cmpq	%rax, %r12
	jl	.LBB21_29
.LBB21_41:                              # %._crit_edge
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movl	%eax, 212(%rsp)
.Ltmp324:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp325:
# BB#42:
.Ltmp327:
	movq	%rbx, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp328:
# BB#43:
	movq	(%rbx), %rax
.Ltmp330:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp331:
# BB#44:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit
.Ltmp333:
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp334:
# BB#45:
.Ltmp335:
	leaq	104(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN21CCompressProgressWrapC1EP21ICompressProgressInfo
.Ltmp336:
# BB#46:
.Ltmp338:
	leaq	72(%rsp), %rdi
	leaq	128(%rsp), %rsi
	leaq	152(%rsp), %rdx
	leaq	104(%rsp), %r8
	xorl	%ecx, %ecx
	callq	Xz_Encode
.Ltmp339:
# BB#47:
	testl	%eax, %eax
	je	.LBB21_48
# BB#52:
.Ltmp341:
	movl	%eax, %edi
	callq	_Z13SResToHRESULTi
	movl	%eax, %ebp
.Ltmp342:
	jmp	.LBB21_53
.LBB21_65:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	cmpl	$0, 20(%rsp)
	jne	.LBB21_72
# BB#66:
	movq	208(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB21_67
# BB#68:
	movq	(%rdi), %rax
	movq	136(%r14), %rsi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB21_72
# BB#69:                                # %._crit_edge178
	movq	208(%r14), %rdi
	jmp	.LBB21_70
.LBB21_67:
	xorl	%edi, %edi
.LBB21_70:
	xorl	%edx, %edx
	movq	%r12, %rsi
	callq	_ZN9NCompress10CopyStreamEP19ISequentialInStreamP20ISequentialOutStreamP21ICompressProgressInfo
	jmp	.LBB21_71
.LBB21_48:
	movq	(%r15), %rax
.Ltmp343:
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	*80(%rax)
	movl	%eax, %ebp
.Ltmp344:
.LBB21_53:
	movq	(%rbx), %rax
.Ltmp348:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp349:
.LBB21_54:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_72
# BB#55:
	movq	(%rdi), %rax
	callq	*16(%rax)
	jmp	.LBB21_72
.LBB21_57:
.Ltmp350:
	jmp	.LBB21_62
.LBB21_51:
.Ltmp345:
	jmp	.LBB21_59
.LBB21_56:
.Ltmp340:
	jmp	.LBB21_59
.LBB21_50:
.Ltmp332:
	jmp	.LBB21_62
.LBB21_49:
.Ltmp329:
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB21_63
.LBB21_60:
.Ltmp326:
	jmp	.LBB21_62
.LBB21_58:
.Ltmp337:
.LBB21_59:
	movq	%rax, %rbp
	movq	(%rbx), %rax
.Ltmp346:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp347:
	jmp	.LBB21_63
.LBB21_61:
.Ltmp315:
	jmp	.LBB21_62
.LBB21_39:
.Ltmp320:
	jmp	.LBB21_62
.LBB21_34:
.Ltmp308:
	movq	%rax, %rbp
.Ltmp309:
	leaq	56(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp310:
	jmp	.LBB21_14
.LBB21_13:
.Ltmp301:
	movq	%rax, %rbp
.Ltmp302:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp303:
	jmp	.LBB21_14
.LBB21_38:
.Ltmp323:
.LBB21_62:
	movq	%rax, %rbp
.LBB21_63:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_14
# BB#64:
	movq	(%rdi), %rax
.Ltmp351:
	callq	*16(%rax)
.Ltmp352:
.LBB21_14:
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB21_73:
.Ltmp353:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end21:
	.size	_ZN8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback, .Lfunc_end21-_ZN8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\215\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\204\002"              # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp299-.Lfunc_begin6  #   Call between .Lfunc_begin6 and .Ltmp299
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp299-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp300-.Ltmp299       #   Call between .Ltmp299 and .Ltmp300
	.long	.Ltmp301-.Lfunc_begin6  #     jumps to .Ltmp301
	.byte	0                       #   On action: cleanup
	.long	.Ltmp300-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp304-.Ltmp300       #   Call between .Ltmp300 and .Ltmp304
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp304-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp307-.Ltmp304       #   Call between .Ltmp304 and .Ltmp307
	.long	.Ltmp308-.Lfunc_begin6  #     jumps to .Ltmp308
	.byte	0                       #   On action: cleanup
	.long	.Ltmp307-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp311-.Ltmp307       #   Call between .Ltmp307 and .Ltmp311
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp311-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp314-.Ltmp311       #   Call between .Ltmp311 and .Ltmp314
	.long	.Ltmp315-.Lfunc_begin6  #     jumps to .Ltmp315
	.byte	0                       #   On action: cleanup
	.long	.Ltmp316-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp319-.Ltmp316       #   Call between .Ltmp316 and .Ltmp319
	.long	.Ltmp320-.Lfunc_begin6  #     jumps to .Ltmp320
	.byte	0                       #   On action: cleanup
	.long	.Ltmp321-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp322-.Ltmp321       #   Call between .Ltmp321 and .Ltmp322
	.long	.Ltmp323-.Lfunc_begin6  #     jumps to .Ltmp323
	.byte	0                       #   On action: cleanup
	.long	.Ltmp324-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp325-.Ltmp324       #   Call between .Ltmp324 and .Ltmp325
	.long	.Ltmp326-.Lfunc_begin6  #     jumps to .Ltmp326
	.byte	0                       #   On action: cleanup
	.long	.Ltmp327-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp328-.Ltmp327       #   Call between .Ltmp327 and .Ltmp328
	.long	.Ltmp329-.Lfunc_begin6  #     jumps to .Ltmp329
	.byte	0                       #   On action: cleanup
	.long	.Ltmp330-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp331-.Ltmp330       #   Call between .Ltmp330 and .Ltmp331
	.long	.Ltmp332-.Lfunc_begin6  #     jumps to .Ltmp332
	.byte	0                       #   On action: cleanup
	.long	.Ltmp333-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Ltmp336-.Ltmp333       #   Call between .Ltmp333 and .Ltmp336
	.long	.Ltmp337-.Lfunc_begin6  #     jumps to .Ltmp337
	.byte	0                       #   On action: cleanup
	.long	.Ltmp338-.Lfunc_begin6  # >> Call Site 13 <<
	.long	.Ltmp339-.Ltmp338       #   Call between .Ltmp338 and .Ltmp339
	.long	.Ltmp340-.Lfunc_begin6  #     jumps to .Ltmp340
	.byte	0                       #   On action: cleanup
	.long	.Ltmp341-.Lfunc_begin6  # >> Call Site 14 <<
	.long	.Ltmp342-.Ltmp341       #   Call between .Ltmp341 and .Ltmp342
	.long	.Ltmp345-.Lfunc_begin6  #     jumps to .Ltmp345
	.byte	0                       #   On action: cleanup
	.long	.Ltmp342-.Lfunc_begin6  # >> Call Site 15 <<
	.long	.Ltmp343-.Ltmp342       #   Call between .Ltmp342 and .Ltmp343
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp343-.Lfunc_begin6  # >> Call Site 16 <<
	.long	.Ltmp344-.Ltmp343       #   Call between .Ltmp343 and .Ltmp344
	.long	.Ltmp345-.Lfunc_begin6  #     jumps to .Ltmp345
	.byte	0                       #   On action: cleanup
	.long	.Ltmp348-.Lfunc_begin6  # >> Call Site 17 <<
	.long	.Ltmp349-.Ltmp348       #   Call between .Ltmp348 and .Ltmp349
	.long	.Ltmp350-.Lfunc_begin6  #     jumps to .Ltmp350
	.byte	0                       #   On action: cleanup
	.long	.Ltmp349-.Lfunc_begin6  # >> Call Site 18 <<
	.long	.Ltmp346-.Ltmp349       #   Call between .Ltmp349 and .Ltmp346
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp346-.Lfunc_begin6  # >> Call Site 19 <<
	.long	.Ltmp352-.Ltmp346       #   Call between .Ltmp346 and .Ltmp352
	.long	.Ltmp353-.Lfunc_begin6  #     jumps to .Ltmp353
	.byte	1                       #   On action: 1
	.long	.Ltmp352-.Lfunc_begin6  # >> Call Site 20 <<
	.long	.Lfunc_end21-.Ltmp352   #   Call between .Ltmp352 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn16_N8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback,@function
_ZThn16_N8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback: # @_ZThn16_N8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback # TAILCALL
.Lfunc_end22:
	.size	_ZThn16_N8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback, .Lfunc_end22-_ZThn16_N8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_endproc

	.globl	_ZN8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi,@function
_ZN8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi: # @_ZN8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi83:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 48
.Lcfi85:
	.cfi_offset %rbx, -48
.Lcfi86:
	.cfi_offset %r12, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	addq	$32, %r15
.Ltmp354:
	movq	%r15, %rdi
	callq	_ZN8NArchive11COutHandler17BeforeSetPropertyEv
.Ltmp355:
# BB#1:                                 # %.preheader
	testl	%ebp, %ebp
	jle	.LBB23_7
# BB#2:                                 # %.lr.ph.preheader
	movslq	%ebp, %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB23_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbp,8), %rsi
.Ltmp357:
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	_ZN8NArchive11COutHandler11SetPropertyEPKwRK14tagPROPVARIANT
.Ltmp358:
# BB#4:                                 #   in Loop: Header=BB23_3 Depth=1
	testl	%eax, %eax
	jne	.LBB23_12
# BB#5:                                 #   in Loop: Header=BB23_3 Depth=1
	incq	%rbp
	addq	$16, %rbx
	cmpq	%r12, %rbp
	jl	.LBB23_3
.LBB23_7:
	xorl	%eax, %eax
	jmp	.LBB23_12
.LBB23_8:
.Ltmp356:
	jmp	.LBB23_10
.LBB23_9:
.Ltmp359:
.LBB23_10:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbp
	cmpl	$2, %ebx
	je	.LBB23_13
# BB#11:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
.LBB23_12:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_13:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbp, (%rax)
.Ltmp360:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp361:
# BB#14:
.LBB23_15:
.Ltmp362:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end23:
	.size	_ZN8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi, .Lfunc_end23-_ZN8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\317\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp354-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp355-.Ltmp354       #   Call between .Ltmp354 and .Ltmp355
	.long	.Ltmp356-.Lfunc_begin7  #     jumps to .Ltmp356
	.byte	3                       #   On action: 2
	.long	.Ltmp357-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp358-.Ltmp357       #   Call between .Ltmp357 and .Ltmp358
	.long	.Ltmp359-.Lfunc_begin7  #     jumps to .Ltmp359
	.byte	3                       #   On action: 2
	.long	.Ltmp358-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp360-.Ltmp358       #   Call between .Ltmp358 and .Ltmp360
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp360-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp361-.Ltmp360       #   Call between .Ltmp360 and .Ltmp361
	.long	.Ltmp362-.Lfunc_begin7  #     jumps to .Ltmp362
	.byte	0                       #   On action: cleanup
	.long	.Ltmp361-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Lfunc_end23-.Ltmp361   #   Call between .Ltmp361 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn24_N8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi,@function
_ZThn24_N8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi: # @_ZThn24_N8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi # TAILCALL
.Lfunc_end24:
	.size	_ZThn24_N8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi, .Lfunc_end24-_ZThn24_N8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_endproc

	.section	.text._ZN8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi90:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB25_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB25_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB25_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB25_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB25_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB25_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB25_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB25_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB25_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB25_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB25_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB25_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB25_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB25_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB25_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB25_16
.LBB25_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_IInArchive(%rip), %cl
	jne	.LBB25_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_IInArchive+1(%rip), %al
	jne	.LBB25_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_IInArchive+2(%rip), %al
	jne	.LBB25_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_IInArchive+3(%rip), %al
	jne	.LBB25_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_IInArchive+4(%rip), %al
	jne	.LBB25_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_IInArchive+5(%rip), %al
	jne	.LBB25_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_IInArchive+6(%rip), %al
	jne	.LBB25_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_IInArchive+7(%rip), %al
	jne	.LBB25_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_IInArchive+8(%rip), %al
	jne	.LBB25_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_IInArchive+9(%rip), %al
	jne	.LBB25_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_IInArchive+10(%rip), %al
	jne	.LBB25_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_IInArchive+11(%rip), %al
	jne	.LBB25_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_IInArchive+12(%rip), %al
	jne	.LBB25_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_IInArchive+13(%rip), %al
	jne	.LBB25_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_IInArchive+14(%rip), %al
	jne	.LBB25_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %al
	cmpb	IID_IInArchive+15(%rip), %al
	jne	.LBB25_33
.LBB25_16:
	movq	%rdi, (%rdx)
	jmp	.LBB25_85
.LBB25_33:                              # %_ZeqRK4GUIDS1_.exit12.thread
	cmpb	IID_IArchiveOpenSeq(%rip), %cl
	jne	.LBB25_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+1(%rip), %al
	jne	.LBB25_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+2(%rip), %al
	jne	.LBB25_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+3(%rip), %al
	jne	.LBB25_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+4(%rip), %al
	jne	.LBB25_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+5(%rip), %al
	jne	.LBB25_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+6(%rip), %al
	jne	.LBB25_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+7(%rip), %al
	jne	.LBB25_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+8(%rip), %al
	jne	.LBB25_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+9(%rip), %al
	jne	.LBB25_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+10(%rip), %al
	jne	.LBB25_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+11(%rip), %al
	jne	.LBB25_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+12(%rip), %al
	jne	.LBB25_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+13(%rip), %al
	jne	.LBB25_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+14(%rip), %al
	jne	.LBB25_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit16
	movb	15(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+15(%rip), %al
	jne	.LBB25_50
# BB#49:
	leaq	8(%rdi), %rax
	jmp	.LBB25_84
.LBB25_50:                              # %_ZeqRK4GUIDS1_.exit16.thread
	cmpb	IID_IOutArchive(%rip), %cl
	jne	.LBB25_67
# BB#51:
	movb	1(%rsi), %al
	cmpb	IID_IOutArchive+1(%rip), %al
	jne	.LBB25_67
# BB#52:
	movb	2(%rsi), %al
	cmpb	IID_IOutArchive+2(%rip), %al
	jne	.LBB25_67
# BB#53:
	movb	3(%rsi), %al
	cmpb	IID_IOutArchive+3(%rip), %al
	jne	.LBB25_67
# BB#54:
	movb	4(%rsi), %al
	cmpb	IID_IOutArchive+4(%rip), %al
	jne	.LBB25_67
# BB#55:
	movb	5(%rsi), %al
	cmpb	IID_IOutArchive+5(%rip), %al
	jne	.LBB25_67
# BB#56:
	movb	6(%rsi), %al
	cmpb	IID_IOutArchive+6(%rip), %al
	jne	.LBB25_67
# BB#57:
	movb	7(%rsi), %al
	cmpb	IID_IOutArchive+7(%rip), %al
	jne	.LBB25_67
# BB#58:
	movb	8(%rsi), %al
	cmpb	IID_IOutArchive+8(%rip), %al
	jne	.LBB25_67
# BB#59:
	movb	9(%rsi), %al
	cmpb	IID_IOutArchive+9(%rip), %al
	jne	.LBB25_67
# BB#60:
	movb	10(%rsi), %al
	cmpb	IID_IOutArchive+10(%rip), %al
	jne	.LBB25_67
# BB#61:
	movb	11(%rsi), %al
	cmpb	IID_IOutArchive+11(%rip), %al
	jne	.LBB25_67
# BB#62:
	movb	12(%rsi), %al
	cmpb	IID_IOutArchive+12(%rip), %al
	jne	.LBB25_67
# BB#63:
	movb	13(%rsi), %al
	cmpb	IID_IOutArchive+13(%rip), %al
	jne	.LBB25_67
# BB#64:
	movb	14(%rsi), %al
	cmpb	IID_IOutArchive+14(%rip), %al
	jne	.LBB25_67
# BB#65:                                # %_ZeqRK4GUIDS1_.exit18
	movb	15(%rsi), %al
	cmpb	IID_IOutArchive+15(%rip), %al
	jne	.LBB25_67
# BB#66:
	leaq	16(%rdi), %rax
	jmp	.LBB25_84
.LBB25_67:                              # %_ZeqRK4GUIDS1_.exit18.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ISetProperties(%rip), %cl
	jne	.LBB25_86
# BB#68:
	movb	1(%rsi), %cl
	cmpb	IID_ISetProperties+1(%rip), %cl
	jne	.LBB25_86
# BB#69:
	movb	2(%rsi), %cl
	cmpb	IID_ISetProperties+2(%rip), %cl
	jne	.LBB25_86
# BB#70:
	movb	3(%rsi), %cl
	cmpb	IID_ISetProperties+3(%rip), %cl
	jne	.LBB25_86
# BB#71:
	movb	4(%rsi), %cl
	cmpb	IID_ISetProperties+4(%rip), %cl
	jne	.LBB25_86
# BB#72:
	movb	5(%rsi), %cl
	cmpb	IID_ISetProperties+5(%rip), %cl
	jne	.LBB25_86
# BB#73:
	movb	6(%rsi), %cl
	cmpb	IID_ISetProperties+6(%rip), %cl
	jne	.LBB25_86
# BB#74:
	movb	7(%rsi), %cl
	cmpb	IID_ISetProperties+7(%rip), %cl
	jne	.LBB25_86
# BB#75:
	movb	8(%rsi), %cl
	cmpb	IID_ISetProperties+8(%rip), %cl
	jne	.LBB25_86
# BB#76:
	movb	9(%rsi), %cl
	cmpb	IID_ISetProperties+9(%rip), %cl
	jne	.LBB25_86
# BB#77:
	movb	10(%rsi), %cl
	cmpb	IID_ISetProperties+10(%rip), %cl
	jne	.LBB25_86
# BB#78:
	movb	11(%rsi), %cl
	cmpb	IID_ISetProperties+11(%rip), %cl
	jne	.LBB25_86
# BB#79:
	movb	12(%rsi), %cl
	cmpb	IID_ISetProperties+12(%rip), %cl
	jne	.LBB25_86
# BB#80:
	movb	13(%rsi), %cl
	cmpb	IID_ISetProperties+13(%rip), %cl
	jne	.LBB25_86
# BB#81:
	movb	14(%rsi), %cl
	cmpb	IID_ISetProperties+14(%rip), %cl
	jne	.LBB25_86
# BB#82:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %cl
	cmpb	IID_ISetProperties+15(%rip), %cl
	jne	.LBB25_86
# BB#83:
	leaq	24(%rdi), %rax
.LBB25_84:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movq	%rax, (%rdx)
.LBB25_85:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB25_86:                              # %_ZeqRK4GUIDS1_.exit14.thread
	popq	%rcx
	retq
.Lfunc_end25:
	.size	_ZN8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end25-_ZN8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive3NXz8CHandler6AddRefEv,"axG",@progbits,_ZN8NArchive3NXz8CHandler6AddRefEv,comdat
	.weak	_ZN8NArchive3NXz8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler6AddRefEv,@function
_ZN8NArchive3NXz8CHandler6AddRefEv:     # @_ZN8NArchive3NXz8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	132(%rdi), %eax
	incl	%eax
	movl	%eax, 132(%rdi)
	retq
.Lfunc_end26:
	.size	_ZN8NArchive3NXz8CHandler6AddRefEv, .Lfunc_end26-_ZN8NArchive3NXz8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive3NXz8CHandler7ReleaseEv,"axG",@progbits,_ZN8NArchive3NXz8CHandler7ReleaseEv,comdat
	.weak	_ZN8NArchive3NXz8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandler7ReleaseEv,@function
_ZN8NArchive3NXz8CHandler7ReleaseEv:    # @_ZN8NArchive3NXz8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 16
	movl	132(%rdi), %eax
	decl	%eax
	movl	%eax, 132(%rdi)
	jne	.LBB27_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB27_2:
	popq	%rcx
	retq
.Lfunc_end27:
	.size	_ZN8NArchive3NXz8CHandler7ReleaseEv, .Lfunc_end27-_ZN8NArchive3NXz8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive3NXz8CHandlerD2Ev,"axG",@progbits,_ZN8NArchive3NXz8CHandlerD2Ev,comdat
	.weak	_ZN8NArchive3NXz8CHandlerD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandlerD2Ev,@function
_ZN8NArchive3NXz8CHandlerD2Ev:          # @_ZN8NArchive3NXz8CHandlerD2Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 32
.Lcfi95:
	.cfi_offset %rbx, -24
.Lcfi96:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive3NXz8CHandlerE+184, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3NXz8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN8NArchive3NXz8CHandlerE+320, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3NXz8CHandlerE+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp363:
	callq	*16(%rax)
.Ltmp364:
.LBB28_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp368:
	callq	*16(%rax)
.Ltmp369:
.LBB28_4:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	168(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_6
# BB#5:
	callq	_ZdaPv
.LBB28_6:                               # %_ZN11CStringBaseIcED2Ev.exit
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, 40(%rbx)
	addq	$40, %rbx
.Ltmp380:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp381:
# BB#7:                                 # %_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev.exit.i
.Ltmp386:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp387:
# BB#8:                                 # %_ZN8NArchive11COutHandlerD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB28_13:
.Ltmp370:
	movq	%rax, %r14
	jmp	.LBB28_14
.LBB28_11:
.Ltmp365:
	movq	%rax, %r14
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_14
# BB#12:
	movq	(%rdi), %rax
.Ltmp366:
	callq	*16(%rax)
.Ltmp367:
.LBB28_14:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit10
	movq	168(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_16
# BB#15:
	callq	_ZdaPv
.LBB28_16:                              # %_ZN11CStringBaseIcED2Ev.exit11
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, 40(%rbx)
	addq	$40, %rbx
.Ltmp371:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp372:
# BB#17:                                # %_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev.exit.i12
.Ltmp377:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp378:
	jmp	.LBB28_21
.LBB28_18:
.Ltmp373:
	movq	%rax, %r14
.Ltmp374:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp375:
	jmp	.LBB28_23
.LBB28_19:
.Ltmp376:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB28_22:
.Ltmp379:
	movq	%rax, %r14
.LBB28_23:                              # %.body13
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB28_20:
.Ltmp388:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB28_9:
.Ltmp382:
	movq	%rax, %r14
.Ltmp383:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp384:
.LBB28_21:                              # %_ZN8NArchive11COutHandlerD2Ev.exit16
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB28_10:
.Ltmp385:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end28:
	.size	_ZN8NArchive3NXz8CHandlerD2Ev, .Lfunc_end28-_ZN8NArchive3NXz8CHandlerD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp363-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp364-.Ltmp363       #   Call between .Ltmp363 and .Ltmp364
	.long	.Ltmp365-.Lfunc_begin8  #     jumps to .Ltmp365
	.byte	0                       #   On action: cleanup
	.long	.Ltmp368-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp369-.Ltmp368       #   Call between .Ltmp368 and .Ltmp369
	.long	.Ltmp370-.Lfunc_begin8  #     jumps to .Ltmp370
	.byte	0                       #   On action: cleanup
	.long	.Ltmp380-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp381-.Ltmp380       #   Call between .Ltmp380 and .Ltmp381
	.long	.Ltmp382-.Lfunc_begin8  #     jumps to .Ltmp382
	.byte	0                       #   On action: cleanup
	.long	.Ltmp386-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp387-.Ltmp386       #   Call between .Ltmp386 and .Ltmp387
	.long	.Ltmp388-.Lfunc_begin8  #     jumps to .Ltmp388
	.byte	0                       #   On action: cleanup
	.long	.Ltmp366-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp367-.Ltmp366       #   Call between .Ltmp366 and .Ltmp367
	.long	.Ltmp379-.Lfunc_begin8  #     jumps to .Ltmp379
	.byte	1                       #   On action: 1
	.long	.Ltmp371-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp372-.Ltmp371       #   Call between .Ltmp371 and .Ltmp372
	.long	.Ltmp373-.Lfunc_begin8  #     jumps to .Ltmp373
	.byte	1                       #   On action: 1
	.long	.Ltmp377-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Ltmp378-.Ltmp377       #   Call between .Ltmp377 and .Ltmp378
	.long	.Ltmp379-.Lfunc_begin8  #     jumps to .Ltmp379
	.byte	1                       #   On action: 1
	.long	.Ltmp374-.Lfunc_begin8  # >> Call Site 8 <<
	.long	.Ltmp375-.Ltmp374       #   Call between .Ltmp374 and .Ltmp375
	.long	.Ltmp376-.Lfunc_begin8  #     jumps to .Ltmp376
	.byte	1                       #   On action: 1
	.long	.Ltmp375-.Lfunc_begin8  # >> Call Site 9 <<
	.long	.Ltmp383-.Ltmp375       #   Call between .Ltmp375 and .Ltmp383
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp383-.Lfunc_begin8  # >> Call Site 10 <<
	.long	.Ltmp384-.Ltmp383       #   Call between .Ltmp383 and .Ltmp384
	.long	.Ltmp385-.Lfunc_begin8  #     jumps to .Ltmp385
	.byte	1                       #   On action: 1
	.long	.Ltmp384-.Lfunc_begin8  # >> Call Site 11 <<
	.long	.Lfunc_end28-.Ltmp384   #   Call between .Ltmp384 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3NXz8CHandlerD0Ev,"axG",@progbits,_ZN8NArchive3NXz8CHandlerD0Ev,comdat
	.weak	_ZN8NArchive3NXz8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz8CHandlerD0Ev,@function
_ZN8NArchive3NXz8CHandlerD0Ev:          # @_ZN8NArchive3NXz8CHandlerD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 32
.Lcfi100:
	.cfi_offset %rbx, -24
.Lcfi101:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp389:
	callq	_ZN8NArchive3NXz8CHandlerD2Ev
.Ltmp390:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB29_2:
.Ltmp391:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end29:
	.size	_ZN8NArchive3NXz8CHandlerD0Ev, .Lfunc_end29-_ZN8NArchive3NXz8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp389-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp390-.Ltmp389       #   Call between .Ltmp389 and .Ltmp390
	.long	.Ltmp391-.Lfunc_begin9  #     jumps to .Ltmp391
	.byte	0                       #   On action: cleanup
	.long	.Ltmp390-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end29-.Ltmp390   #   Call between .Ltmp390 and .Lfunc_end29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end30:
	.size	_ZThn8_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end30-_ZThn8_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3NXz8CHandler6AddRefEv,"axG",@progbits,_ZThn8_N8NArchive3NXz8CHandler6AddRefEv,comdat
	.weak	_ZThn8_N8NArchive3NXz8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3NXz8CHandler6AddRefEv,@function
_ZThn8_N8NArchive3NXz8CHandler6AddRefEv: # @_ZThn8_N8NArchive3NXz8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	124(%rdi), %eax
	incl	%eax
	movl	%eax, 124(%rdi)
	retq
.Lfunc_end31:
	.size	_ZThn8_N8NArchive3NXz8CHandler6AddRefEv, .Lfunc_end31-_ZThn8_N8NArchive3NXz8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3NXz8CHandler7ReleaseEv,"axG",@progbits,_ZThn8_N8NArchive3NXz8CHandler7ReleaseEv,comdat
	.weak	_ZThn8_N8NArchive3NXz8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3NXz8CHandler7ReleaseEv,@function
_ZThn8_N8NArchive3NXz8CHandler7ReleaseEv: # @_ZThn8_N8NArchive3NXz8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi102:
	.cfi_def_cfa_offset 16
	movl	124(%rdi), %eax
	decl	%eax
	movl	%eax, 124(%rdi)
	jne	.LBB32_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB32_2:                               # %_ZN8NArchive3NXz8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end32:
	.size	_ZThn8_N8NArchive3NXz8CHandler7ReleaseEv, .Lfunc_end32-_ZThn8_N8NArchive3NXz8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3NXz8CHandlerD1Ev,"axG",@progbits,_ZThn8_N8NArchive3NXz8CHandlerD1Ev,comdat
	.weak	_ZThn8_N8NArchive3NXz8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3NXz8CHandlerD1Ev,@function
_ZThn8_N8NArchive3NXz8CHandlerD1Ev:     # @_ZThn8_N8NArchive3NXz8CHandlerD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive3NXz8CHandlerD2Ev # TAILCALL
.Lfunc_end33:
	.size	_ZThn8_N8NArchive3NXz8CHandlerD1Ev, .Lfunc_end33-_ZThn8_N8NArchive3NXz8CHandlerD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3NXz8CHandlerD0Ev,"axG",@progbits,_ZThn8_N8NArchive3NXz8CHandlerD0Ev,comdat
	.weak	_ZThn8_N8NArchive3NXz8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3NXz8CHandlerD0Ev,@function
_ZThn8_N8NArchive3NXz8CHandlerD0Ev:     # @_ZThn8_N8NArchive3NXz8CHandlerD0Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi105:
	.cfi_def_cfa_offset 32
.Lcfi106:
	.cfi_offset %rbx, -24
.Lcfi107:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp392:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3NXz8CHandlerD2Ev
.Ltmp393:
# BB#1:                                 # %_ZN8NArchive3NXz8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB34_2:
.Ltmp394:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end34:
	.size	_ZThn8_N8NArchive3NXz8CHandlerD0Ev, .Lfunc_end34-_ZThn8_N8NArchive3NXz8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp392-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp393-.Ltmp392       #   Call between .Ltmp392 and .Ltmp393
	.long	.Ltmp394-.Lfunc_begin10 #     jumps to .Ltmp394
	.byte	0                       #   On action: cleanup
	.long	.Ltmp393-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Lfunc_end34-.Ltmp393   #   Call between .Ltmp393 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn16_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end35:
	.size	_ZThn16_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end35-_ZThn16_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive3NXz8CHandler6AddRefEv,"axG",@progbits,_ZThn16_N8NArchive3NXz8CHandler6AddRefEv,comdat
	.weak	_ZThn16_N8NArchive3NXz8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive3NXz8CHandler6AddRefEv,@function
_ZThn16_N8NArchive3NXz8CHandler6AddRefEv: # @_ZThn16_N8NArchive3NXz8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	116(%rdi), %eax
	incl	%eax
	movl	%eax, 116(%rdi)
	retq
.Lfunc_end36:
	.size	_ZThn16_N8NArchive3NXz8CHandler6AddRefEv, .Lfunc_end36-_ZThn16_N8NArchive3NXz8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive3NXz8CHandler7ReleaseEv,"axG",@progbits,_ZThn16_N8NArchive3NXz8CHandler7ReleaseEv,comdat
	.weak	_ZThn16_N8NArchive3NXz8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive3NXz8CHandler7ReleaseEv,@function
_ZThn16_N8NArchive3NXz8CHandler7ReleaseEv: # @_ZThn16_N8NArchive3NXz8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi108:
	.cfi_def_cfa_offset 16
	movl	116(%rdi), %eax
	decl	%eax
	movl	%eax, 116(%rdi)
	jne	.LBB37_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB37_2:                               # %_ZN8NArchive3NXz8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end37:
	.size	_ZThn16_N8NArchive3NXz8CHandler7ReleaseEv, .Lfunc_end37-_ZThn16_N8NArchive3NXz8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive3NXz8CHandlerD1Ev,"axG",@progbits,_ZThn16_N8NArchive3NXz8CHandlerD1Ev,comdat
	.weak	_ZThn16_N8NArchive3NXz8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive3NXz8CHandlerD1Ev,@function
_ZThn16_N8NArchive3NXz8CHandlerD1Ev:    # @_ZThn16_N8NArchive3NXz8CHandlerD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN8NArchive3NXz8CHandlerD2Ev # TAILCALL
.Lfunc_end38:
	.size	_ZThn16_N8NArchive3NXz8CHandlerD1Ev, .Lfunc_end38-_ZThn16_N8NArchive3NXz8CHandlerD1Ev
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive3NXz8CHandlerD0Ev,"axG",@progbits,_ZThn16_N8NArchive3NXz8CHandlerD0Ev,comdat
	.weak	_ZThn16_N8NArchive3NXz8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive3NXz8CHandlerD0Ev,@function
_ZThn16_N8NArchive3NXz8CHandlerD0Ev:    # @_ZThn16_N8NArchive3NXz8CHandlerD0Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi111:
	.cfi_def_cfa_offset 32
.Lcfi112:
	.cfi_offset %rbx, -24
.Lcfi113:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp395:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3NXz8CHandlerD2Ev
.Ltmp396:
# BB#1:                                 # %_ZN8NArchive3NXz8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB39_2:
.Ltmp397:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end39:
	.size	_ZThn16_N8NArchive3NXz8CHandlerD0Ev, .Lfunc_end39-_ZThn16_N8NArchive3NXz8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table39:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp395-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp396-.Ltmp395       #   Call between .Ltmp395 and .Ltmp396
	.long	.Ltmp397-.Lfunc_begin11 #     jumps to .Ltmp397
	.byte	0                       #   On action: cleanup
	.long	.Ltmp396-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Lfunc_end39-.Ltmp396   #   Call between .Ltmp396 and .Lfunc_end39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn24_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn24_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn24_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn24_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn24_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end40:
	.size	_ZThn24_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end40-_ZThn24_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive3NXz8CHandler6AddRefEv,"axG",@progbits,_ZThn24_N8NArchive3NXz8CHandler6AddRefEv,comdat
	.weak	_ZThn24_N8NArchive3NXz8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive3NXz8CHandler6AddRefEv,@function
_ZThn24_N8NArchive3NXz8CHandler6AddRefEv: # @_ZThn24_N8NArchive3NXz8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	108(%rdi), %eax
	incl	%eax
	movl	%eax, 108(%rdi)
	retq
.Lfunc_end41:
	.size	_ZThn24_N8NArchive3NXz8CHandler6AddRefEv, .Lfunc_end41-_ZThn24_N8NArchive3NXz8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive3NXz8CHandler7ReleaseEv,"axG",@progbits,_ZThn24_N8NArchive3NXz8CHandler7ReleaseEv,comdat
	.weak	_ZThn24_N8NArchive3NXz8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive3NXz8CHandler7ReleaseEv,@function
_ZThn24_N8NArchive3NXz8CHandler7ReleaseEv: # @_ZThn24_N8NArchive3NXz8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 16
	movl	108(%rdi), %eax
	decl	%eax
	movl	%eax, 108(%rdi)
	jne	.LBB42_2
# BB#1:
	addq	$-24, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB42_2:                               # %_ZN8NArchive3NXz8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end42:
	.size	_ZThn24_N8NArchive3NXz8CHandler7ReleaseEv, .Lfunc_end42-_ZThn24_N8NArchive3NXz8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive3NXz8CHandlerD1Ev,"axG",@progbits,_ZThn24_N8NArchive3NXz8CHandlerD1Ev,comdat
	.weak	_ZThn24_N8NArchive3NXz8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive3NXz8CHandlerD1Ev,@function
_ZThn24_N8NArchive3NXz8CHandlerD1Ev:    # @_ZThn24_N8NArchive3NXz8CHandlerD1Ev
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN8NArchive3NXz8CHandlerD2Ev # TAILCALL
.Lfunc_end43:
	.size	_ZThn24_N8NArchive3NXz8CHandlerD1Ev, .Lfunc_end43-_ZThn24_N8NArchive3NXz8CHandlerD1Ev
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive3NXz8CHandlerD0Ev,"axG",@progbits,_ZThn24_N8NArchive3NXz8CHandlerD0Ev,comdat
	.weak	_ZThn24_N8NArchive3NXz8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive3NXz8CHandlerD0Ev,@function
_ZThn24_N8NArchive3NXz8CHandlerD0Ev:    # @_ZThn24_N8NArchive3NXz8CHandlerD0Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi117:
	.cfi_def_cfa_offset 32
.Lcfi118:
	.cfi_offset %rbx, -24
.Lcfi119:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-24, %rbx
.Ltmp398:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3NXz8CHandlerD2Ev
.Ltmp399:
# BB#1:                                 # %_ZN8NArchive3NXz8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB44_2:
.Ltmp400:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end44:
	.size	_ZThn24_N8NArchive3NXz8CHandlerD0Ev, .Lfunc_end44-_ZThn24_N8NArchive3NXz8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table44:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp398-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp399-.Ltmp398       #   Call between .Ltmp398 and .Ltmp399
	.long	.Ltmp400-.Lfunc_begin12 #     jumps to .Ltmp400
	.byte	0                       #   On action: cleanup
	.long	.Ltmp399-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Lfunc_end44-.Ltmp399   #   Call between .Ltmp399 and .Lfunc_end44
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive3NXz16CSeekToSeqStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive3NXz16CSeekToSeqStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive3NXz16CSeekToSeqStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz16CSeekToSeqStream14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive3NXz16CSeekToSeqStream14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive3NXz16CSeekToSeqStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi120:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB45_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB45_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB45_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB45_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB45_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB45_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB45_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB45_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB45_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB45_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB45_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB45_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB45_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB45_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB45_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB45_32
.LBB45_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IInStream(%rip), %cl
	jne	.LBB45_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_IInStream+1(%rip), %cl
	jne	.LBB45_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_IInStream+2(%rip), %cl
	jne	.LBB45_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_IInStream+3(%rip), %cl
	jne	.LBB45_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_IInStream+4(%rip), %cl
	jne	.LBB45_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_IInStream+5(%rip), %cl
	jne	.LBB45_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_IInStream+6(%rip), %cl
	jne	.LBB45_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_IInStream+7(%rip), %cl
	jne	.LBB45_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_IInStream+8(%rip), %cl
	jne	.LBB45_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_IInStream+9(%rip), %cl
	jne	.LBB45_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_IInStream+10(%rip), %cl
	jne	.LBB45_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_IInStream+11(%rip), %cl
	jne	.LBB45_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_IInStream+12(%rip), %cl
	jne	.LBB45_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_IInStream+13(%rip), %cl
	jne	.LBB45_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_IInStream+14(%rip), %cl
	jne	.LBB45_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %cl
	cmpb	IID_IInStream+15(%rip), %cl
	jne	.LBB45_33
.LBB45_32:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB45_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	popq	%rcx
	retq
.Lfunc_end45:
	.size	_ZN8NArchive3NXz16CSeekToSeqStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end45-_ZN8NArchive3NXz16CSeekToSeqStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive3NXz16CSeekToSeqStream6AddRefEv,"axG",@progbits,_ZN8NArchive3NXz16CSeekToSeqStream6AddRefEv,comdat
	.weak	_ZN8NArchive3NXz16CSeekToSeqStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz16CSeekToSeqStream6AddRefEv,@function
_ZN8NArchive3NXz16CSeekToSeqStream6AddRefEv: # @_ZN8NArchive3NXz16CSeekToSeqStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end46:
	.size	_ZN8NArchive3NXz16CSeekToSeqStream6AddRefEv, .Lfunc_end46-_ZN8NArchive3NXz16CSeekToSeqStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive3NXz16CSeekToSeqStream7ReleaseEv,"axG",@progbits,_ZN8NArchive3NXz16CSeekToSeqStream7ReleaseEv,comdat
	.weak	_ZN8NArchive3NXz16CSeekToSeqStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz16CSeekToSeqStream7ReleaseEv,@function
_ZN8NArchive3NXz16CSeekToSeqStream7ReleaseEv: # @_ZN8NArchive3NXz16CSeekToSeqStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi121:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB47_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB47_2:
	popq	%rcx
	retq
.Lfunc_end47:
	.size	_ZN8NArchive3NXz16CSeekToSeqStream7ReleaseEv, .Lfunc_end47-_ZN8NArchive3NXz16CSeekToSeqStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive3NXz16CSeekToSeqStreamD2Ev,"axG",@progbits,_ZN8NArchive3NXz16CSeekToSeqStreamD2Ev,comdat
	.weak	_ZN8NArchive3NXz16CSeekToSeqStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz16CSeekToSeqStreamD2Ev,@function
_ZN8NArchive3NXz16CSeekToSeqStreamD2Ev: # @_ZN8NArchive3NXz16CSeekToSeqStreamD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTVN8NArchive3NXz16CSeekToSeqStreamE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB48_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB48_1:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	retq
.Lfunc_end48:
	.size	_ZN8NArchive3NXz16CSeekToSeqStreamD2Ev, .Lfunc_end48-_ZN8NArchive3NXz16CSeekToSeqStreamD2Ev
	.cfi_endproc

	.section	.text._ZN8NArchive3NXz16CSeekToSeqStreamD0Ev,"axG",@progbits,_ZN8NArchive3NXz16CSeekToSeqStreamD0Ev,comdat
	.weak	_ZN8NArchive3NXz16CSeekToSeqStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXz16CSeekToSeqStreamD0Ev,@function
_ZN8NArchive3NXz16CSeekToSeqStreamD0Ev: # @_ZN8NArchive3NXz16CSeekToSeqStreamD0Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi124:
	.cfi_def_cfa_offset 32
.Lcfi125:
	.cfi_offset %rbx, -24
.Lcfi126:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive3NXz16CSeekToSeqStreamE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB49_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp401:
	callq	*16(%rax)
.Ltmp402:
.LBB49_2:                               # %_ZN8NArchive3NXz16CSeekToSeqStreamD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB49_3:
.Ltmp403:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end49:
	.size	_ZN8NArchive3NXz16CSeekToSeqStreamD0Ev, .Lfunc_end49-_ZN8NArchive3NXz16CSeekToSeqStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table49:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp401-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp402-.Ltmp401       #   Call between .Ltmp401 and .Ltmp402
	.long	.Ltmp403-.Lfunc_begin13 #     jumps to .Ltmp403
	.byte	0                       #   On action: cleanup
	.long	.Ltmp402-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Lfunc_end49-.Ltmp402   #   Call between .Ltmp402 and .Lfunc_end49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN10IInArchiveD0Ev,"axG",@progbits,_ZN10IInArchiveD0Ev,comdat
	.weak	_ZN10IInArchiveD0Ev
	.p2align	4, 0x90
	.type	_ZN10IInArchiveD0Ev,@function
_ZN10IInArchiveD0Ev:                    # @_ZN10IInArchiveD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end50:
	.size	_ZN10IInArchiveD0Ev, .Lfunc_end50-_ZN10IInArchiveD0Ev
	.cfi_endproc

	.section	.text._ZN15IArchiveOpenSeqD0Ev,"axG",@progbits,_ZN15IArchiveOpenSeqD0Ev,comdat
	.weak	_ZN15IArchiveOpenSeqD0Ev
	.p2align	4, 0x90
	.type	_ZN15IArchiveOpenSeqD0Ev,@function
_ZN15IArchiveOpenSeqD0Ev:               # @_ZN15IArchiveOpenSeqD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end51:
	.size	_ZN15IArchiveOpenSeqD0Ev, .Lfunc_end51-_ZN15IArchiveOpenSeqD0Ev
	.cfi_endproc

	.section	.text._ZN11IOutArchiveD0Ev,"axG",@progbits,_ZN11IOutArchiveD0Ev,comdat
	.weak	_ZN11IOutArchiveD0Ev
	.p2align	4, 0x90
	.type	_ZN11IOutArchiveD0Ev,@function
_ZN11IOutArchiveD0Ev:                   # @_ZN11IOutArchiveD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end52:
	.size	_ZN11IOutArchiveD0Ev, .Lfunc_end52-_ZN11IOutArchiveD0Ev
	.cfi_endproc

	.section	.text._ZN14ISetPropertiesD0Ev,"axG",@progbits,_ZN14ISetPropertiesD0Ev,comdat
	.weak	_ZN14ISetPropertiesD0Ev
	.p2align	4, 0x90
	.type	_ZN14ISetPropertiesD0Ev,@function
_ZN14ISetPropertiesD0Ev:                # @_ZN14ISetPropertiesD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end53:
	.size	_ZN14ISetPropertiesD0Ev, .Lfunc_end53-_ZN14ISetPropertiesD0Ev
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev,@function
_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev: # @_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi129:
	.cfi_def_cfa_offset 32
.Lcfi130:
	.cfi_offset %rbx, -24
.Lcfi131:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, (%rbx)
.Ltmp404:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp405:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB54_2:
.Ltmp406:
	movq	%rax, %r14
.Ltmp407:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp408:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB54_4:
.Ltmp409:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end54:
	.size	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev, .Lfunc_end54-_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table54:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp404-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp405-.Ltmp404       #   Call between .Ltmp404 and .Ltmp405
	.long	.Ltmp406-.Lfunc_begin14 #     jumps to .Ltmp406
	.byte	0                       #   On action: cleanup
	.long	.Ltmp405-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp407-.Ltmp405       #   Call between .Ltmp405 and .Ltmp407
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp407-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp408-.Ltmp407       #   Call between .Ltmp407 and .Ltmp408
	.long	.Ltmp409-.Lfunc_begin14 #     jumps to .Ltmp409
	.byte	1                       #   On action: 1
	.long	.Ltmp408-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end54-.Ltmp408   #   Call between .Ltmp408 and .Lfunc_end54
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev,@function
_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev: # @_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi134:
	.cfi_def_cfa_offset 32
.Lcfi135:
	.cfi_offset %rbx, -24
.Lcfi136:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, (%rbx)
.Ltmp410:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp411:
# BB#1:
.Ltmp416:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp417:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB55_5:
.Ltmp418:
	movq	%rax, %r14
	jmp	.LBB55_6
.LBB55_3:
.Ltmp412:
	movq	%rax, %r14
.Ltmp413:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp414:
.LBB55_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB55_4:
.Ltmp415:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end55:
	.size	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev, .Lfunc_end55-_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table55:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp410-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp411-.Ltmp410       #   Call between .Ltmp410 and .Ltmp411
	.long	.Ltmp412-.Lfunc_begin15 #     jumps to .Ltmp412
	.byte	0                       #   On action: cleanup
	.long	.Ltmp416-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp417-.Ltmp416       #   Call between .Ltmp416 and .Ltmp417
	.long	.Ltmp418-.Lfunc_begin15 #     jumps to .Ltmp418
	.byte	0                       #   On action: cleanup
	.long	.Ltmp413-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp414-.Ltmp413       #   Call between .Ltmp413 and .Ltmp414
	.long	.Ltmp415-.Lfunc_begin15 #     jumps to .Ltmp415
	.byte	1                       #   On action: 1
	.long	.Ltmp414-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Lfunc_end55-.Ltmp414   #   Call between .Ltmp414 and .Lfunc_end55
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi137:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi138:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi139:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi140:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi141:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi143:
	.cfi_def_cfa_offset 64
.Lcfi144:
	.cfi_offset %rbx, -56
.Lcfi145:
	.cfi_offset %r12, -48
.Lcfi146:
	.cfi_offset %r13, -40
.Lcfi147:
	.cfi_offset %r14, -32
.Lcfi148:
	.cfi_offset %r15, -24
.Lcfi149:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB56_9
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB56_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB56_8
# BB#3:                                 #   in Loop: Header=BB56_2 Depth=1
	movq	32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB56_5
# BB#4:                                 #   in Loop: Header=BB56_2 Depth=1
	callq	_ZdaPv
.LBB56_5:                               # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB56_2 Depth=1
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbp)
.Ltmp419:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp420:
# BB#6:                                 # %_ZN13CObjectVectorI5CPropED2Ev.exit.i
                                        #   in Loop: Header=BB56_2 Depth=1
.Ltmp425:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp426:
# BB#7:                                 # %_ZN8NArchive14COneMethodInfoD2Ev.exit
                                        #   in Loop: Header=BB56_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB56_8:                               #   in Loop: Header=BB56_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB56_2
.LBB56_9:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB56_10:
.Ltmp421:
	movq	%rax, %rbx
.Ltmp422:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp423:
	jmp	.LBB56_13
.LBB56_11:
.Ltmp424:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB56_12:
.Ltmp427:
	movq	%rax, %rbx
.LBB56_13:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end56:
	.size	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii, .Lfunc_end56-_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table56:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp419-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp420-.Ltmp419       #   Call between .Ltmp419 and .Ltmp420
	.long	.Ltmp421-.Lfunc_begin16 #     jumps to .Ltmp421
	.byte	0                       #   On action: cleanup
	.long	.Ltmp425-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp426-.Ltmp425       #   Call between .Ltmp425 and .Ltmp426
	.long	.Ltmp427-.Lfunc_begin16 #     jumps to .Ltmp427
	.byte	0                       #   On action: cleanup
	.long	.Ltmp426-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Ltmp422-.Ltmp426       #   Call between .Ltmp426 and .Ltmp422
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp422-.Lfunc_begin16 # >> Call Site 4 <<
	.long	.Ltmp423-.Ltmp422       #   Call between .Ltmp422 and .Ltmp423
	.long	.Ltmp424-.Lfunc_begin16 #     jumps to .Ltmp424
	.byte	1                       #   On action: 1
	.long	.Ltmp423-.Lfunc_begin16 # >> Call Site 5 <<
	.long	.Lfunc_end56-.Ltmp423   #   Call between .Ltmp423 and .Lfunc_end56
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropED2Ev,"axG",@progbits,_ZN13CObjectVectorI5CPropED2Ev,comdat
	.weak	_ZN13CObjectVectorI5CPropED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropED2Ev,@function
_ZN13CObjectVectorI5CPropED2Ev:         # @_ZN13CObjectVectorI5CPropED2Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi152:
	.cfi_def_cfa_offset 32
.Lcfi153:
	.cfi_offset %rbx, -24
.Lcfi154:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp428:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp429:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB57_2:
.Ltmp430:
	movq	%rax, %r14
.Ltmp431:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp432:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB57_4:
.Ltmp433:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end57:
	.size	_ZN13CObjectVectorI5CPropED2Ev, .Lfunc_end57-_ZN13CObjectVectorI5CPropED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table57:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp428-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp429-.Ltmp428       #   Call between .Ltmp428 and .Ltmp429
	.long	.Ltmp430-.Lfunc_begin17 #     jumps to .Ltmp430
	.byte	0                       #   On action: cleanup
	.long	.Ltmp429-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp431-.Ltmp429       #   Call between .Ltmp429 and .Ltmp431
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp431-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Ltmp432-.Ltmp431       #   Call between .Ltmp431 and .Ltmp432
	.long	.Ltmp433-.Lfunc_begin17 #     jumps to .Ltmp433
	.byte	1                       #   On action: 1
	.long	.Ltmp432-.Lfunc_begin17 # >> Call Site 4 <<
	.long	.Lfunc_end57-.Ltmp432   #   Call between .Ltmp432 and .Lfunc_end57
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropED0Ev,"axG",@progbits,_ZN13CObjectVectorI5CPropED0Ev,comdat
	.weak	_ZN13CObjectVectorI5CPropED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropED0Ev,@function
_ZN13CObjectVectorI5CPropED0Ev:         # @_ZN13CObjectVectorI5CPropED0Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi155:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi157:
	.cfi_def_cfa_offset 32
.Lcfi158:
	.cfi_offset %rbx, -24
.Lcfi159:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp434:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp435:
# BB#1:
.Ltmp440:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp441:
# BB#2:                                 # %_ZN13CObjectVectorI5CPropED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB58_5:
.Ltmp442:
	movq	%rax, %r14
	jmp	.LBB58_6
.LBB58_3:
.Ltmp436:
	movq	%rax, %r14
.Ltmp437:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp438:
.LBB58_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB58_4:
.Ltmp439:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end58:
	.size	_ZN13CObjectVectorI5CPropED0Ev, .Lfunc_end58-_ZN13CObjectVectorI5CPropED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table58:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp434-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp435-.Ltmp434       #   Call between .Ltmp434 and .Ltmp435
	.long	.Ltmp436-.Lfunc_begin18 #     jumps to .Ltmp436
	.byte	0                       #   On action: cleanup
	.long	.Ltmp440-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp441-.Ltmp440       #   Call between .Ltmp440 and .Ltmp441
	.long	.Ltmp442-.Lfunc_begin18 #     jumps to .Ltmp442
	.byte	0                       #   On action: cleanup
	.long	.Ltmp437-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Ltmp438-.Ltmp437       #   Call between .Ltmp437 and .Ltmp438
	.long	.Ltmp439-.Lfunc_begin18 #     jumps to .Ltmp439
	.byte	1                       #   On action: 1
	.long	.Ltmp438-.Lfunc_begin18 # >> Call Site 4 <<
	.long	.Lfunc_end58-.Ltmp438   #   Call between .Ltmp438 and .Lfunc_end58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI5CPropE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI5CPropE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropE6DeleteEii,@function
_ZN13CObjectVectorI5CPropE6DeleteEii:   # @_ZN13CObjectVectorI5CPropE6DeleteEii
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi162:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi163:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi164:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi166:
	.cfi_def_cfa_offset 64
.Lcfi167:
	.cfi_offset %rbx, -56
.Lcfi168:
	.cfi_offset %r12, -48
.Lcfi169:
	.cfi_offset %r13, -40
.Lcfi170:
	.cfi_offset %r14, -32
.Lcfi171:
	.cfi_offset %r15, -24
.Lcfi172:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB59_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB59_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB59_5
# BB#3:                                 #   in Loop: Header=BB59_2 Depth=1
	leaq	8(%rbp), %rdi
.Ltmp443:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp444:
# BB#4:                                 # %_ZN5CPropD2Ev.exit
                                        #   in Loop: Header=BB59_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB59_5:                               #   in Loop: Header=BB59_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB59_2
.LBB59_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB59_7:
.Ltmp445:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end59:
	.size	_ZN13CObjectVectorI5CPropE6DeleteEii, .Lfunc_end59-_ZN13CObjectVectorI5CPropE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table59:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp443-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp444-.Ltmp443       #   Call between .Ltmp443 and .Ltmp444
	.long	.Ltmp445-.Lfunc_begin19 #     jumps to .Ltmp445
	.byte	0                       #   On action: cleanup
	.long	.Ltmp444-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Lfunc_end59-.Ltmp444   #   Call between .Ltmp444 and .Lfunc_end59
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL7SzAllocPvm,@function
_ZL7SzAllocPvm:                         # @_ZL7SzAllocPvm
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	MyAlloc                 # TAILCALL
.Lfunc_end60:
	.size	_ZL7SzAllocPvm, .Lfunc_end60-_ZL7SzAllocPvm
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL6SzFreePvS_,@function
_ZL6SzFreePvS_:                         # @_ZL6SzFreePvS_
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	MyFree                  # TAILCALL
.Lfunc_end61:
	.size	_ZL6SzFreePvS_, .Lfunc_end61-_ZL6SzFreePvS_
	.cfi_endproc

	.section	.text._ZN11CStringBaseIcEpLEc,"axG",@progbits,_ZN11CStringBaseIcEpLEc,comdat
	.weak	_ZN11CStringBaseIcEpLEc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEc,@function
_ZN11CStringBaseIcEpLEc:                # @_ZN11CStringBaseIcEpLEc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi173:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi174:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi175:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi176:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi177:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi178:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi179:
	.cfi_def_cfa_offset 64
.Lcfi180:
	.cfi_offset %rbx, -56
.Lcfi181:
	.cfi_offset %r12, -48
.Lcfi182:
	.cfi_offset %r13, -40
.Lcfi183:
	.cfi_offset %r14, -32
.Lcfi184:
	.cfi_offset %r15, -24
.Lcfi185:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB62_28
# BB#1:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB62_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB62_3:                               # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rsi,%rbx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB62_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB62_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB62_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB62_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB62_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB62_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB62_17
.LBB62_7:
	xorl	%ecx, %ecx
.LBB62_8:                               # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB62_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB62_10:                              # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB62_10
.LBB62_11:                              # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB62_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB62_13:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB62_13
	jmp	.LBB62_26
.LBB62_25:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB62_27
.LBB62_26:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB62_27:
	movq	%r12, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%r13)
.LBB62_28:                              # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movb	%r14b, (%rax,%rcx)
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movb	$0, 1(%rax,%rcx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB62_17:                              # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB62_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB62_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r12,%rbx)
	movups	%xmm1, 16(%r12,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB62_20
	jmp	.LBB62_21
.LBB62_18:
	xorl	%ebx, %ebx
.LBB62_21:                              # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB62_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB62_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB62_23
.LBB62_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB62_8
	jmp	.LBB62_26
.Lfunc_end62:
	.size	_ZN11CStringBaseIcEpLEc, .Lfunc_end62-_ZN11CStringBaseIcEpLEc
	.cfi_endproc

	.section	.text._ZN11CStringBaseIcEpLERKS0_,"axG",@progbits,_ZN11CStringBaseIcEpLERKS0_,comdat
	.weak	_ZN11CStringBaseIcEpLERKS0_
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLERKS0_,@function
_ZN11CStringBaseIcEpLERKS0_:            # @_ZN11CStringBaseIcEpLERKS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi186:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi187:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi188:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi189:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi190:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi191:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi192:
	.cfi_def_cfa_offset 64
.Lcfi193:
	.cfi_offset %rbx, -56
.Lcfi194:
	.cfi_offset %r12, -48
.Lcfi195:
	.cfi_offset %r13, -40
.Lcfi196:
	.cfi_offset %r14, -32
.Lcfi197:
	.cfi_offset %r15, -24
.Lcfi198:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	8(%r14), %eax
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %ecx
	subl	%ebp, %ecx
	cmpl	%eax, %ecx
	jg	.LBB63_28
# BB#1:
	decl	%ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB63_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB63_3:                               # %select.end
	movl	%eax, %esi
	subl	%ecx, %esi
	addl	%edx, %ecx
	cmpl	%eax, %ecx
	cmovgel	%edx, %esi
	leal	1(%rsi,%rbx), %r12d
	cmpl	%ebx, %r12d
	je	.LBB63_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r12d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
	testl	%ebx, %ebx
	jle	.LBB63_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB63_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB63_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB63_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r15
	jae	.LBB63_17
# BB#16:                                # %vector.memcheck
	leaq	(%r15,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB63_17
.LBB63_7:
	xorl	%ecx, %ecx
.LBB63_8:                               # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB63_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB63_10:                              # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r15,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB63_10
.LBB63_11:                              # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB63_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r15,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB63_13:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB63_13
	jmp	.LBB63_26
.LBB63_25:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB63_27
.LBB63_26:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB63_27:
	movq	%r15, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r15,%rax)
	movl	%r12d, 12(%r13)
.LBB63_28:                              # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movslq	%ebp, %rax
	addq	(%r13), %rax
	movq	(%r14), %rcx
	.p2align	4, 0x90
.LBB63_29:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB63_29
# BB#30:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit
	movl	8(%r14), %eax
	addl	%eax, 8(%r13)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB63_17:                              # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB63_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB63_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r15,%rbx)
	movups	%xmm1, 16(%r15,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB63_20
	jmp	.LBB63_21
.LBB63_18:
	xorl	%ebx, %ebx
.LBB63_21:                              # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB63_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r15,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB63_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB63_23
.LBB63_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB63_8
	jmp	.LBB63_26
.Lfunc_end63:
	.size	_ZN11CStringBaseIcEpLERKS0_, .Lfunc_end63-_ZN11CStringBaseIcEpLERKS0_
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXzL9CreateArcEv,@function
_ZN8NArchive3NXzL9CreateArcEv:          # @_ZN8NArchive3NXzL9CreateArcEv
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r14
.Lcfi199:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi200:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi201:
	.cfi_def_cfa_offset 32
.Lcfi202:
	.cfi_offset %rbx, -24
.Lcfi203:
	.cfi_offset %r14, -16
	movl	$232, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp446:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3NXz8CHandlerC2Ev
.Ltmp447:
# BB#1:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB64_2:
.Ltmp448:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end64:
	.size	_ZN8NArchive3NXzL9CreateArcEv, .Lfunc_end64-_ZN8NArchive3NXzL9CreateArcEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table64:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin20-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp446-.Lfunc_begin20 #   Call between .Lfunc_begin20 and .Ltmp446
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp446-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Ltmp447-.Ltmp446       #   Call between .Ltmp446 and .Ltmp447
	.long	.Ltmp448-.Lfunc_begin20 #     jumps to .Ltmp448
	.byte	0                       #   On action: cleanup
	.long	.Ltmp447-.Lfunc_begin20 # >> Call Site 3 <<
	.long	.Lfunc_end64-.Ltmp447   #   Call between .Ltmp447 and .Lfunc_end64
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive3NXzL12CreateArcOutEv,@function
_ZN8NArchive3NXzL12CreateArcOutEv:      # @_ZN8NArchive3NXzL12CreateArcOutEv
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%r14
.Lcfi204:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi205:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi206:
	.cfi_def_cfa_offset 32
.Lcfi207:
	.cfi_offset %rbx, -24
.Lcfi208:
	.cfi_offset %r14, -16
	movl	$232, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp449:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3NXz8CHandlerC2Ev
.Ltmp450:
# BB#1:
	addq	$16, %rbx
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB65_2:
.Ltmp451:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end65:
	.size	_ZN8NArchive3NXzL12CreateArcOutEv, .Lfunc_end65-_ZN8NArchive3NXzL12CreateArcOutEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table65:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin21-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp449-.Lfunc_begin21 #   Call between .Lfunc_begin21 and .Ltmp449
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp449-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Ltmp450-.Ltmp449       #   Call between .Ltmp449 and .Ltmp450
	.long	.Ltmp451-.Lfunc_begin21 #     jumps to .Ltmp451
	.byte	0                       #   On action: cleanup
	.long	.Ltmp450-.Lfunc_begin21 # >> Call Site 3 <<
	.long	.Lfunc_end65-.Ltmp450   #   Call between .Ltmp450 and .Lfunc_end65
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end66:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end66-_ZN8IUnknownD2Ev
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_XzHandler.ii,@function
_GLOBAL__sub_I_XzHandler.ii:            # @_GLOBAL__sub_I_XzHandler.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi209:
	.cfi_def_cfa_offset 16
	callq	Crc64GenerateTable
	movl	$_ZN8NArchive3NXzL9g_ArcInfoE, %edi
	popq	%rax
	jmp	_Z11RegisterArcPK8CArcInfo # TAILCALL
.Lfunc_end67:
	.size	_GLOBAL__sub_I_XzHandler.ii, .Lfunc_end67-_GLOBAL__sub_I_XzHandler.ii
	.cfi_endproc

	.type	_ZN8NArchive3NXz16g_Crc64TableInitE,@object # @_ZN8NArchive3NXz16g_Crc64TableInitE
	.bss
	.globl	_ZN8NArchive3NXz16g_Crc64TableInitE
_ZN8NArchive3NXz16g_Crc64TableInitE:
	.zero	1
	.size	_ZN8NArchive3NXz16g_Crc64TableInitE, 1

	.type	_ZTVN8NArchive3NXz8CHandlerE,@object # @_ZTVN8NArchive3NXz8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive3NXz8CHandlerE
	.p2align	3
_ZTVN8NArchive3NXz8CHandlerE:
	.quad	0
	.quad	_ZTIN8NArchive3NXz8CHandlerE
	.quad	_ZN8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive3NXz8CHandler6AddRefEv
	.quad	_ZN8NArchive3NXz8CHandler7ReleaseEv
	.quad	_ZN8NArchive3NXz8CHandlerD2Ev
	.quad	_ZN8NArchive3NXz8CHandlerD0Ev
	.quad	_ZN8NArchive3NXz8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.quad	_ZN8NArchive3NXz8CHandler5CloseEv
	.quad	_ZN8NArchive3NXz8CHandler16GetNumberOfItemsEPj
	.quad	_ZN8NArchive3NXz8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.quad	_ZN8NArchive3NXz8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.quad	_ZN8NArchive3NXz8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.quad	_ZN8NArchive3NXz8CHandler21GetNumberOfPropertiesEPj
	.quad	_ZN8NArchive3NXz8CHandler15GetPropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive3NXz8CHandler28GetNumberOfArchivePropertiesEPj
	.quad	_ZN8NArchive3NXz8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream
	.quad	_ZN8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.quad	_ZN8NArchive3NXz8CHandler15GetFileTimeTypeEPj
	.quad	_ZN8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.quad	-8
	.quad	_ZTIN8NArchive3NXz8CHandlerE
	.quad	_ZThn8_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N8NArchive3NXz8CHandler6AddRefEv
	.quad	_ZThn8_N8NArchive3NXz8CHandler7ReleaseEv
	.quad	_ZThn8_N8NArchive3NXz8CHandlerD1Ev
	.quad	_ZThn8_N8NArchive3NXz8CHandlerD0Ev
	.quad	_ZThn8_N8NArchive3NXz8CHandler7OpenSeqEP19ISequentialInStream
	.quad	-16
	.quad	_ZTIN8NArchive3NXz8CHandlerE
	.quad	_ZThn16_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N8NArchive3NXz8CHandler6AddRefEv
	.quad	_ZThn16_N8NArchive3NXz8CHandler7ReleaseEv
	.quad	_ZThn16_N8NArchive3NXz8CHandlerD1Ev
	.quad	_ZThn16_N8NArchive3NXz8CHandlerD0Ev
	.quad	_ZThn16_N8NArchive3NXz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.quad	_ZThn16_N8NArchive3NXz8CHandler15GetFileTimeTypeEPj
	.quad	-24
	.quad	_ZTIN8NArchive3NXz8CHandlerE
	.quad	_ZThn24_N8NArchive3NXz8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn24_N8NArchive3NXz8CHandler6AddRefEv
	.quad	_ZThn24_N8NArchive3NXz8CHandler7ReleaseEv
	.quad	_ZThn24_N8NArchive3NXz8CHandlerD1Ev
	.quad	_ZThn24_N8NArchive3NXz8CHandlerD0Ev
	.quad	_ZThn24_N8NArchive3NXz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.size	_ZTVN8NArchive3NXz8CHandlerE, 368

	.type	_ZN8NArchive3NXz6kPropsE,@object # @_ZN8NArchive3NXz6kPropsE
	.data
	.globl	_ZN8NArchive3NXz6kPropsE
	.p2align	4
_ZN8NArchive3NXz6kPropsE:
	.quad	0
	.long	7                       # 0x7
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	8                       # 0x8
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	22                      # 0x16
	.short	8                       # 0x8
	.zero	2
	.size	_ZN8NArchive3NXz6kPropsE, 48

	.type	_ZN8NArchive3NXz9kArcPropsE,@object # @_ZN8NArchive3NXz9kArcPropsE
	.globl	_ZN8NArchive3NXz9kArcPropsE
	.p2align	4
_ZN8NArchive3NXz9kArcPropsE:
	.quad	0
	.long	22                      # 0x16
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	38                      # 0x26
	.short	19                      # 0x13
	.zero	2
	.size	_ZN8NArchive3NXz9kArcPropsE, 32

	.type	_ZL7g_Alloc,@object     # @_ZL7g_Alloc
	.p2align	3
_ZL7g_Alloc:
	.quad	_ZL7SzAllocPvm
	.quad	_ZL6SzFreePvS_
	.size	_ZL7g_Alloc, 16

	.type	_ZTSN8NArchive3NXz8CHandlerE,@object # @_ZTSN8NArchive3NXz8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTSN8NArchive3NXz8CHandlerE
	.p2align	4
_ZTSN8NArchive3NXz8CHandlerE:
	.asciz	"N8NArchive3NXz8CHandlerE"
	.size	_ZTSN8NArchive3NXz8CHandlerE, 25

	.type	_ZTS10IInArchive,@object # @_ZTS10IInArchive
	.section	.rodata._ZTS10IInArchive,"aG",@progbits,_ZTS10IInArchive,comdat
	.weak	_ZTS10IInArchive
_ZTS10IInArchive:
	.asciz	"10IInArchive"
	.size	_ZTS10IInArchive, 13

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI10IInArchive,@object # @_ZTI10IInArchive
	.section	.rodata._ZTI10IInArchive,"aG",@progbits,_ZTI10IInArchive,comdat
	.weak	_ZTI10IInArchive
	.p2align	4
_ZTI10IInArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IInArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI10IInArchive, 24

	.type	_ZTS15IArchiveOpenSeq,@object # @_ZTS15IArchiveOpenSeq
	.section	.rodata._ZTS15IArchiveOpenSeq,"aG",@progbits,_ZTS15IArchiveOpenSeq,comdat
	.weak	_ZTS15IArchiveOpenSeq
	.p2align	4
_ZTS15IArchiveOpenSeq:
	.asciz	"15IArchiveOpenSeq"
	.size	_ZTS15IArchiveOpenSeq, 18

	.type	_ZTI15IArchiveOpenSeq,@object # @_ZTI15IArchiveOpenSeq
	.section	.rodata._ZTI15IArchiveOpenSeq,"aG",@progbits,_ZTI15IArchiveOpenSeq,comdat
	.weak	_ZTI15IArchiveOpenSeq
	.p2align	4
_ZTI15IArchiveOpenSeq:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15IArchiveOpenSeq
	.quad	_ZTI8IUnknown
	.size	_ZTI15IArchiveOpenSeq, 24

	.type	_ZTS11IOutArchive,@object # @_ZTS11IOutArchive
	.section	.rodata._ZTS11IOutArchive,"aG",@progbits,_ZTS11IOutArchive,comdat
	.weak	_ZTS11IOutArchive
_ZTS11IOutArchive:
	.asciz	"11IOutArchive"
	.size	_ZTS11IOutArchive, 14

	.type	_ZTI11IOutArchive,@object # @_ZTI11IOutArchive
	.section	.rodata._ZTI11IOutArchive,"aG",@progbits,_ZTI11IOutArchive,comdat
	.weak	_ZTI11IOutArchive
	.p2align	4
_ZTI11IOutArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS11IOutArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI11IOutArchive, 24

	.type	_ZTS14ISetProperties,@object # @_ZTS14ISetProperties
	.section	.rodata._ZTS14ISetProperties,"aG",@progbits,_ZTS14ISetProperties,comdat
	.weak	_ZTS14ISetProperties
	.p2align	4
_ZTS14ISetProperties:
	.asciz	"14ISetProperties"
	.size	_ZTS14ISetProperties, 17

	.type	_ZTI14ISetProperties,@object # @_ZTI14ISetProperties
	.section	.rodata._ZTI14ISetProperties,"aG",@progbits,_ZTI14ISetProperties,comdat
	.weak	_ZTI14ISetProperties
	.p2align	4
_ZTI14ISetProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ISetProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI14ISetProperties, 24

	.type	_ZTSN8NArchive11COutHandlerE,@object # @_ZTSN8NArchive11COutHandlerE
	.section	.rodata._ZTSN8NArchive11COutHandlerE,"aG",@progbits,_ZTSN8NArchive11COutHandlerE,comdat
	.weak	_ZTSN8NArchive11COutHandlerE
	.p2align	4
_ZTSN8NArchive11COutHandlerE:
	.asciz	"N8NArchive11COutHandlerE"
	.size	_ZTSN8NArchive11COutHandlerE, 25

	.type	_ZTIN8NArchive11COutHandlerE,@object # @_ZTIN8NArchive11COutHandlerE
	.section	.rodata._ZTIN8NArchive11COutHandlerE,"aG",@progbits,_ZTIN8NArchive11COutHandlerE,comdat
	.weak	_ZTIN8NArchive11COutHandlerE
	.p2align	3
_ZTIN8NArchive11COutHandlerE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8NArchive11COutHandlerE
	.size	_ZTIN8NArchive11COutHandlerE, 16

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive3NXz8CHandlerE,@object # @_ZTIN8NArchive3NXz8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive3NXz8CHandlerE
	.p2align	4
_ZTIN8NArchive3NXz8CHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive3NXz8CHandlerE
	.long	1                       # 0x1
	.long	6                       # 0x6
	.quad	_ZTI10IInArchive
	.quad	2                       # 0x2
	.quad	_ZTI15IArchiveOpenSeq
	.quad	2050                    # 0x802
	.quad	_ZTI11IOutArchive
	.quad	4098                    # 0x1002
	.quad	_ZTI14ISetProperties
	.quad	6146                    # 0x1802
	.quad	_ZTIN8NArchive11COutHandlerE
	.quad	8194                    # 0x2002
	.quad	_ZTI13CMyUnknownImp
	.quad	33794                   # 0x8402
	.size	_ZTIN8NArchive3NXz8CHandlerE, 120

	.type	_ZTVN8NArchive3NXz16CSeekToSeqStreamE,@object # @_ZTVN8NArchive3NXz16CSeekToSeqStreamE
	.globl	_ZTVN8NArchive3NXz16CSeekToSeqStreamE
	.p2align	3
_ZTVN8NArchive3NXz16CSeekToSeqStreamE:
	.quad	0
	.quad	_ZTIN8NArchive3NXz16CSeekToSeqStreamE
	.quad	_ZN8NArchive3NXz16CSeekToSeqStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive3NXz16CSeekToSeqStream6AddRefEv
	.quad	_ZN8NArchive3NXz16CSeekToSeqStream7ReleaseEv
	.quad	_ZN8NArchive3NXz16CSeekToSeqStreamD2Ev
	.quad	_ZN8NArchive3NXz16CSeekToSeqStreamD0Ev
	.quad	_ZN8NArchive3NXz16CSeekToSeqStream4ReadEPvjPj
	.quad	_ZN8NArchive3NXz16CSeekToSeqStream4SeekExjPy
	.size	_ZTVN8NArchive3NXz16CSeekToSeqStreamE, 72

	.type	_ZTSN8NArchive3NXz16CSeekToSeqStreamE,@object # @_ZTSN8NArchive3NXz16CSeekToSeqStreamE
	.globl	_ZTSN8NArchive3NXz16CSeekToSeqStreamE
	.p2align	4
_ZTSN8NArchive3NXz16CSeekToSeqStreamE:
	.asciz	"N8NArchive3NXz16CSeekToSeqStreamE"
	.size	_ZTSN8NArchive3NXz16CSeekToSeqStreamE, 34

	.type	_ZTS9IInStream,@object  # @_ZTS9IInStream
	.section	.rodata._ZTS9IInStream,"aG",@progbits,_ZTS9IInStream,comdat
	.weak	_ZTS9IInStream
_ZTS9IInStream:
	.asciz	"9IInStream"
	.size	_ZTS9IInStream, 11

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTI9IInStream,@object  # @_ZTI9IInStream
	.section	.rodata._ZTI9IInStream,"aG",@progbits,_ZTI9IInStream,comdat
	.weak	_ZTI9IInStream
	.p2align	4
_ZTI9IInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS9IInStream
	.quad	_ZTI19ISequentialInStream
	.size	_ZTI9IInStream, 24

	.type	_ZTIN8NArchive3NXz16CSeekToSeqStreamE,@object # @_ZTIN8NArchive3NXz16CSeekToSeqStreamE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive3NXz16CSeekToSeqStreamE
	.p2align	4
_ZTIN8NArchive3NXz16CSeekToSeqStreamE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive3NXz16CSeekToSeqStreamE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI9IInStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN8NArchive3NXz16CSeekToSeqStreamE, 56

	.type	_ZTV10IInArchive,@object # @_ZTV10IInArchive
	.section	.rodata._ZTV10IInArchive,"aG",@progbits,_ZTV10IInArchive,comdat
	.weak	_ZTV10IInArchive
	.p2align	3
_ZTV10IInArchive:
	.quad	0
	.quad	_ZTI10IInArchive
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN10IInArchiveD0Ev
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.size	_ZTV10IInArchive, 136

	.type	_ZTV15IArchiveOpenSeq,@object # @_ZTV15IArchiveOpenSeq
	.section	.rodata._ZTV15IArchiveOpenSeq,"aG",@progbits,_ZTV15IArchiveOpenSeq,comdat
	.weak	_ZTV15IArchiveOpenSeq
	.p2align	3
_ZTV15IArchiveOpenSeq:
	.quad	0
	.quad	_ZTI15IArchiveOpenSeq
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN15IArchiveOpenSeqD0Ev
	.quad	__cxa_pure_virtual
	.size	_ZTV15IArchiveOpenSeq, 64

	.type	_ZTV11IOutArchive,@object # @_ZTV11IOutArchive
	.section	.rodata._ZTV11IOutArchive,"aG",@progbits,_ZTV11IOutArchive,comdat
	.weak	_ZTV11IOutArchive
	.p2align	3
_ZTV11IOutArchive:
	.quad	0
	.quad	_ZTI11IOutArchive
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN11IOutArchiveD0Ev
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.size	_ZTV11IOutArchive, 72

	.type	_ZTV14ISetProperties,@object # @_ZTV14ISetProperties
	.section	.rodata._ZTV14ISetProperties,"aG",@progbits,_ZTV14ISetProperties,comdat
	.weak	_ZTV14ISetProperties
	.p2align	3
_ZTV14ISetProperties:
	.quad	0
	.quad	_ZTI14ISetProperties
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN14ISetPropertiesD0Ev
	.quad	__cxa_pure_virtual
	.size	_ZTV14ISetProperties, 64

	.type	_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE,@object # @_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE
	.quad	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE,@object # @_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE:
	.asciz	"13CObjectVectorIN8NArchive14COneMethodInfoEE"
	.size	_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE, 45

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE,@object # @_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE, 24

	.type	_ZTV13CObjectVectorI5CPropE,@object # @_ZTV13CObjectVectorI5CPropE
	.section	.rodata._ZTV13CObjectVectorI5CPropE,"aG",@progbits,_ZTV13CObjectVectorI5CPropE,comdat
	.weak	_ZTV13CObjectVectorI5CPropE
	.p2align	3
_ZTV13CObjectVectorI5CPropE:
	.quad	0
	.quad	_ZTI13CObjectVectorI5CPropE
	.quad	_ZN13CObjectVectorI5CPropED2Ev
	.quad	_ZN13CObjectVectorI5CPropED0Ev
	.quad	_ZN13CObjectVectorI5CPropE6DeleteEii
	.size	_ZTV13CObjectVectorI5CPropE, 40

	.type	_ZTS13CObjectVectorI5CPropE,@object # @_ZTS13CObjectVectorI5CPropE
	.section	.rodata._ZTS13CObjectVectorI5CPropE,"aG",@progbits,_ZTS13CObjectVectorI5CPropE,comdat
	.weak	_ZTS13CObjectVectorI5CPropE
	.p2align	4
_ZTS13CObjectVectorI5CPropE:
	.asciz	"13CObjectVectorI5CPropE"
	.size	_ZTS13CObjectVectorI5CPropE, 24

	.type	_ZTI13CObjectVectorI5CPropE,@object # @_ZTI13CObjectVectorI5CPropE
	.section	.rodata._ZTI13CObjectVectorI5CPropE,"aG",@progbits,_ZTI13CObjectVectorI5CPropE,comdat
	.weak	_ZTI13CObjectVectorI5CPropE
	.p2align	4
_ZTI13CObjectVectorI5CPropE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI5CPropE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI5CPropE, 24

	.type	_ZN8NArchive3NXzL11g_NamePairsE,@object # @_ZN8NArchive3NXzL11g_NamePairsE
	.section	.rodata,"a",@progbits
	.p2align	4
_ZN8NArchive3NXzL11g_NamePairsE:
	.long	1                       # 0x1
	.zero	4
	.quad	.L.str
	.long	3                       # 0x3
	.zero	4
	.quad	.L.str.2
	.long	4                       # 0x4
	.zero	4
	.quad	.L.str.3
	.long	5                       # 0x5
	.zero	4
	.quad	.L.str.4
	.long	6                       # 0x6
	.zero	4
	.quad	.L.str.5
	.long	7                       # 0x7
	.zero	4
	.quad	.L.str.6
	.long	8                       # 0x8
	.zero	4
	.quad	.L.str.7
	.long	9                       # 0x9
	.zero	4
	.quad	.L.str.8
	.long	33                      # 0x21
	.zero	4
	.quad	.L.str.9
	.size	_ZN8NArchive3NXzL11g_NamePairsE, 144

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"SB"
	.size	.L.str, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Delta"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"x86"
	.size	.L.str.3, 4

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"PPC"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"IA64"
	.size	.L.str.5, 5

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"ARM"
	.size	.L.str.6, 4

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"ARMT"
	.size	.L.str.7, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"SPARC"
	.size	.L.str.8, 6

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"LZMA2"
	.size	.L.str.9, 6

	.type	_ZN8NArchive3NXzL7kChecksE,@object # @_ZN8NArchive3NXzL7kChecksE
	.section	.rodata,"a",@progbits
	.p2align	4
_ZN8NArchive3NXzL7kChecksE:
	.quad	.L.str.11
	.quad	.L.str.12
	.quad	0
	.quad	0
	.quad	.L.str.13
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.L.str.14
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.size	_ZN8NArchive3NXzL7kChecksE, 128

	.type	.L.str.11,@object       # @.str.11
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.11:
	.asciz	"NoCheck"
	.size	.L.str.11, 8

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"CRC32"
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"CRC64"
	.size	.L.str.13, 6

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"SHA256"
	.size	.L.str.14, 7

	.type	_ZN8NArchive3NXzL9g_ArcInfoE,@object # @_ZN8NArchive3NXzL9g_ArcInfoE
	.data
	.p2align	3
_ZN8NArchive3NXzL9g_ArcInfoE:
	.quad	.L.str.15
	.quad	.L.str.16
	.quad	.L.str.17
	.byte	12                      # 0xc
	.asciz	"\3757zXZ\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.zero	3
	.long	6                       # 0x6
	.byte	1                       # 0x1
	.zero	3
	.quad	_ZN8NArchive3NXzL9CreateArcEv
	.quad	_ZN8NArchive3NXzL12CreateArcOutEv
	.size	_ZN8NArchive3NXzL9g_ArcInfoE, 80

	.type	.L.str.15,@object       # @.str.15
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.15:
	.long	120                     # 0x78
	.long	122                     # 0x7a
	.long	0                       # 0x0
	.size	.L.str.15, 12

	.type	.L.str.16,@object       # @.str.16
	.p2align	2
.L.str.16:
	.long	120                     # 0x78
	.long	122                     # 0x7a
	.long	32                      # 0x20
	.long	116                     # 0x74
	.long	120                     # 0x78
	.long	122                     # 0x7a
	.long	0                       # 0x0
	.size	.L.str.16, 28

	.type	.L.str.17,@object       # @.str.17
	.p2align	2
.L.str.17:
	.long	42                      # 0x2a
	.long	32                      # 0x20
	.long	46                      # 0x2e
	.long	116                     # 0x74
	.long	97                      # 0x61
	.long	114                     # 0x72
	.long	0                       # 0x0
	.size	.L.str.17, 28

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_XzHandler.ii

	.globl	_ZN8NArchive3NXz8CHandlerC1Ev
	.type	_ZN8NArchive3NXz8CHandlerC1Ev,@function
_ZN8NArchive3NXz8CHandlerC1Ev = _ZN8NArchive3NXz8CHandlerC2Ev
	.globl	_ZN8NArchive3NXz17COpenCallbackWrapC1EP20IArchiveOpenCallback
	.type	_ZN8NArchive3NXz17COpenCallbackWrapC1EP20IArchiveOpenCallback,@function
_ZN8NArchive3NXz17COpenCallbackWrapC1EP20IArchiveOpenCallback = _ZN8NArchive3NXz17COpenCallbackWrapC2EP20IArchiveOpenCallback
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
