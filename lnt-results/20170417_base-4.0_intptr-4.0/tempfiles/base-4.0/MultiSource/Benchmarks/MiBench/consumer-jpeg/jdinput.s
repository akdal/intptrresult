	.text
	.file	"jdinput.bc"
	.globl	jinit_input_controller
	.p2align	4, 0x90
	.type	jinit_input_controller,@function
jinit_input_controller:                 # @jinit_input_controller
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	xorl	%esi, %esi
	movl	$48, %edx
	callq	*(%rax)
	movq	%rax, 560(%rbx)
	movq	$consume_markers, (%rax)
	movl	$start_input_pass, %ecx
	movd	%rcx, %xmm0
	movl	$reset_input_controller, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 8(%rax)
	movq	$finish_input_pass, 24(%rax)
	movq	$0, 32(%rax)
	movl	$1, 40(%rax)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	jinit_input_controller, .Lfunc_end0-jinit_input_controller
	.cfi_endproc

	.p2align	4, 0x90
	.type	consume_markers,@function
consume_markers:                        # @consume_markers
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 48
.Lcfi7:
	.cfi_offset %rbx, -48
.Lcfi8:
	.cfi_offset %r12, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	560(%r12), %r14
	movl	$2, %ebp
	cmpl	$0, 36(%r14)
	je	.LBB1_1
.LBB1_34:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_1:
	movq	568(%r12), %rax
	movq	%r12, %rdi
	callq	*8(%rax)
	cmpl	$2, %eax
	je	.LBB1_29
# BB#2:
	cmpl	$1, %eax
	movl	%eax, %ebp
	jne	.LBB1_34
# BB#3:
	cmpl	$0, 40(%r14)
	je	.LBB1_26
# BB#4:
	cmpl	$65500, 44(%r12)        # imm = 0xFFDC
	ja	.LBB1_6
# BB#5:
	cmpl	$65501, 40(%r12)        # imm = 0xFFDD
	jb	.LBB1_7
.LBB1_6:
	movq	(%r12), %rax
	movl	$40, 40(%rax)
	movl	$65500, 44(%rax)        # imm = 0xFFDC
	movq	%r12, %rdi
	callq	*(%rax)
.LBB1_7:
	movl	288(%r12), %eax
	cmpl	$8, %eax
	je	.LBB1_9
# BB#8:
	movq	(%r12), %rcx
	movl	$13, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%r12, %rdi
	callq	*(%rcx)
.LBB1_9:
	movl	48(%r12), %r8d
	cmpl	$11, %r8d
	jl	.LBB1_11
# BB#10:
	movq	(%r12), %rcx
	movl	$24, 40(%rcx)
	movl	%r8d, 44(%rcx)
	movl	$10, 48(%rcx)
	movq	%r12, %rdi
	callq	*(%rcx)
	movl	48(%r12), %r8d
.LBB1_11:
	movabsq	$4294967297, %rcx       # imm = 0x100000001
	movq	%rcx, 388(%r12)
	testl	%r8d, %r8d
	jle	.LBB1_12
# BB#13:                                # %.lr.ph92.i
	movq	296(%r12), %rbp
	addq	$12, %rbp
	xorl	%ebx, %ebx
	movl	$1, %edx
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB1_14:                               # =>This Inner Loop Header: Depth=1
	movl	-4(%rbp), %esi
	leal	-1(%rsi), %edi
	cmpl	$3, %edi
	ja	.LBB1_16
# BB#15:                                #   in Loop: Header=BB1_14 Depth=1
	movl	(%rbp), %r9d
	leal	-1(%r9), %eax
	cmpl	$4, %eax
	jb	.LBB1_17
.LBB1_16:                               # %._crit_edge102.i
                                        #   in Loop: Header=BB1_14 Depth=1
	movq	(%r12), %rax
	movl	$16, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
	movl	48(%r12), %r8d
	movl	388(%r12), %ecx
	movl	-4(%rbp), %esi
	movl	(%rbp), %r9d
	movl	392(%r12), %edx
.LBB1_17:                               #   in Loop: Header=BB1_14 Depth=1
	movl	%ecx, %eax
	movl	%edx, %edi
	cmpl	%esi, %eax
	movl	%esi, %ecx
	cmovgel	%eax, %ecx
	movl	%ecx, 388(%r12)
	cmpl	%r9d, %edi
	movl	%r9d, %edx
	cmovgel	%edi, %edx
	movl	%edx, 392(%r12)
	incl	%ebx
	addq	$96, %rbp
	cmpl	%r8d, %ebx
	jl	.LBB1_14
# BB#18:                                # %._crit_edge93.i
	movl	$8, 396(%r12)
	testl	%r8d, %r8d
	jle	.LBB1_23
# BB#19:                                # %.lr.ph.i
	movq	296(%r12), %rbp
	addq	$8, %rbp
	movl	$1, %r15d
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_21:                               # %._crit_edge99.i
                                        #   in Loop: Header=BB1_20 Depth=1
	movl	388(%r12), %ecx
	addq	$96, %rbp
	incl	%r15d
.LBB1_20:                               # =>This Inner Loop Header: Depth=1
	movl	$8, 28(%rbp)
	movl	40(%r12), %eax
	movslq	(%rbp), %rdi
	imulq	%rax, %rdi
	shll	$3, %ecx
	movslq	%ecx, %rsi
	callq	jdiv_round_up
	movl	%eax, 20(%rbp)
	movl	44(%r12), %eax
	movslq	4(%rbp), %rdi
	imulq	%rax, %rdi
	movslq	392(%r12), %rsi
	shlq	$3, %rsi
	callq	jdiv_round_up
	movl	%eax, 24(%rbp)
	movl	40(%r12), %eax
	movslq	(%rbp), %rdi
	imulq	%rax, %rdi
	movslq	388(%r12), %rsi
	callq	jdiv_round_up
	movl	%eax, 32(%rbp)
	movl	44(%r12), %eax
	movslq	4(%rbp), %rdi
	imulq	%rax, %rdi
	movslq	392(%r12), %rsi
	callq	jdiv_round_up
	movl	%eax, 36(%rbp)
	movl	$1, 40(%rbp)
	movq	$0, 72(%rbp)
	cmpl	48(%r12), %r15d
	jl	.LBB1_21
# BB#22:                                # %._crit_edge.loopexit.i
	movl	392(%r12), %edx
	jmp	.LBB1_23
.LBB1_29:
	movl	$1, 36(%r14)
	cmpl	$0, 40(%r14)
	je	.LBB1_32
# BB#30:
	movq	568(%r12), %rax
	cmpl	$0, 164(%rax)
	je	.LBB1_34
# BB#31:
	movq	(%r12), %rax
	movl	$58, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
	jmp	.LBB1_34
.LBB1_26:
	cmpl	$0, 32(%r14)
	jne	.LBB1_28
# BB#27:
	movq	(%r12), %rax
	movl	$34, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
.LBB1_28:
	movq	%r12, %rdi
	callq	start_input_pass
	movl	$1, %ebp
	jmp	.LBB1_34
.LBB1_32:
	movl	164(%r12), %eax
	cmpl	%eax, 172(%r12)
	jle	.LBB1_34
# BB#33:
	movl	%eax, 172(%r12)
	jmp	.LBB1_34
.LBB1_12:                               # %._crit_edge93.thread.i
	movl	$8, 396(%r12)
	movl	$1, %edx
.LBB1_23:                               # %._crit_edge.i
	movl	44(%r12), %edi
	shll	$3, %edx
	movslq	%edx, %rsi
	callq	jdiv_round_up
	movl	%eax, 400(%r12)
	movl	416(%r12), %eax
	movl	$1, %ebp
	cmpl	48(%r12), %eax
	movl	$1, %eax
	jl	.LBB1_25
# BB#24:
	xorl	%eax, %eax
	cmpl	$0, 304(%r12)
	setne	%al
.LBB1_25:                               # %initial_setup.exit
	movq	560(%r12), %rcx
	movl	%eax, 32(%rcx)
	movl	$0, 40(%r14)
	jmp	.LBB1_34
.Lfunc_end1:
	.size	consume_markers, .Lfunc_end1-consume_markers
	.cfi_endproc

	.p2align	4, 0x90
	.type	reset_input_controller,@function
reset_input_controller:                 # @reset_input_controller
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	560(%rbx), %rax
	movq	$consume_markers, (%rax)
	movq	$0, 32(%rax)
	movl	$1, 40(%rax)
	movq	(%rbx), %rax
	callq	*32(%rax)
	movq	568(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	$0, 184(%rbx)
	popq	%rbx
	retq
.Lfunc_end2:
	.size	reset_input_controller, .Lfunc_end2-reset_input_controller
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_input_pass,@function
start_input_pass:                       # @start_input_pass
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -48
.Lcfi20:
	.cfi_offset %r12, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	416(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB3_2
# BB#1:                                 # %per_scan_setup.exit.thread
	movq	424(%rbx), %rcx
	movl	28(%rcx), %eax
	movl	%eax, 456(%rbx)
	movl	32(%rcx), %eax
	movl	%eax, 460(%rbx)
	movabsq	$4294967297, %rdx       # imm = 0x100000001
	movq	%rdx, 52(%rcx)
	movl	$1, 60(%rcx)
	movl	36(%rcx), %edx
	movl	%edx, 64(%rcx)
	movl	$1, 68(%rcx)
	movl	12(%rcx), %esi
	xorl	%edx, %edx
	divl	%esi
	testl	%edx, %edx
	cmovel	%esi, %edx
	movl	%edx, 72(%rcx)
	movq	$1, 464(%rbx)
	movl	$1, %eax
	jmp	.LBB3_14
.LBB3_2:
	leal	-1(%rax), %ecx
	cmpl	$4, %ecx
	jb	.LBB3_4
# BB#3:
	movq	(%rbx), %rcx
	movl	$24, 40(%rcx)
	movl	%eax, 44(%rcx)
	movl	$4, 48(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB3_4:
	movl	40(%rbx), %edi
	movslq	388(%rbx), %rsi
	shlq	$3, %rsi
	callq	jdiv_round_up
	movl	%eax, 456(%rbx)
	movl	44(%rbx), %edi
	movslq	392(%rbx), %rsi
	shlq	$3, %rsi
	callq	jdiv_round_up
	movl	%eax, 460(%rbx)
	movl	$0, 464(%rbx)
	cmpl	$0, 416(%rbx)
	jle	.LBB3_21
# BB#5:                                 # %.lr.ph82.i
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
	jmp	.LBB3_6
	.p2align	4, 0x90
.LBB3_12:                               # %._crit_edge._crit_edge.i
                                        #   in Loop: Header=BB3_6 Depth=1
	movl	464(%rbx), %r8d
.LBB3_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_10 Depth 2
	movq	424(%rbx,%r14,8), %rsi
	movl	8(%rsi), %edi
	movl	%edi, 52(%rsi)
	movl	12(%rsi), %ecx
	movl	%ecx, 56(%rsi)
	movl	%ecx, %ebp
	imull	%edi, %ebp
	movl	%ebp, 60(%rsi)
	movl	36(%rsi), %eax
	imull	%edi, %eax
	movl	%eax, 64(%rsi)
	movl	28(%rsi), %eax
	xorl	%edx, %edx
	divl	%edi
	testl	%edx, %edx
	cmovel	%edi, %edx
	movl	%edx, 68(%rsi)
	movl	32(%rsi), %eax
	xorl	%edx, %edx
	divl	%ecx
	testl	%edx, %edx
	cmovel	%ecx, %edx
	movl	%edx, 72(%rsi)
	addl	%ebp, %r8d
	cmpl	$11, %r8d
	jl	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_6 Depth=1
	movq	(%rbx), %rax
	movl	$11, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB3_8:                                # %.preheader.i
                                        #   in Loop: Header=BB3_6 Depth=1
	testl	%ebp, %ebp
	jle	.LBB3_11
# BB#9:                                 # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB3_6 Depth=1
	incl	%ebp
	.p2align	4, 0x90
.LBB3_10:                               # %.lr.ph.i
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	464(%rbx), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 464(%rbx)
	movl	%r14d, 468(%rbx,%rax,4)
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB3_10
.LBB3_11:                               # %._crit_edge.i
                                        #   in Loop: Header=BB3_6 Depth=1
	incq	%r14
	movslq	416(%rbx), %rax
	cmpq	%rax, %r14
	jl	.LBB3_12
# BB#13:                                # %per_scan_setup.exit
	testl	%eax, %eax
	jle	.LBB3_21
.LBB3_14:                               # %.lr.ph.i8
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_15:                               # =>This Inner Loop Header: Depth=1
	movq	424(%rbx,%r15,8), %rbp
	cmpq	$0, 80(%rbp)
	jne	.LBB3_20
# BB#16:                                #   in Loop: Header=BB3_15 Depth=1
	movslq	16(%rbp), %r12
	cmpq	$3, %r12
	ja	.LBB3_18
# BB#17:                                #   in Loop: Header=BB3_15 Depth=1
	cmpq	$0, 192(%rbx,%r12,8)
	jne	.LBB3_19
.LBB3_18:                               # %._crit_edge34.i
                                        #   in Loop: Header=BB3_15 Depth=1
	movq	(%rbx), %rax
	movl	$51, 40(%rax)
	movl	%r12d, 44(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB3_19:                               #   in Loop: Header=BB3_15 Depth=1
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$132, %edx
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, %r14
	movq	192(%rbx,%r12,8), %rsi
	movl	$132, %edx
	movq	%r14, %rdi
	callq	memcpy
	movq	%r14, 80(%rbp)
	movl	416(%rbx), %eax
.LBB3_20:                               #   in Loop: Header=BB3_15 Depth=1
	incq	%r15
	movslq	%eax, %rcx
	cmpq	%rcx, %r15
	jl	.LBB3_15
.LBB3_21:                               # %latch_quant_tables.exit
	movq	576(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	544(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	544(%rbx), %rax
	movq	560(%rbx), %rcx
	movq	8(%rax), %rax
	movq	%rax, (%rcx)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	start_input_pass, .Lfunc_end3-start_input_pass
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_input_pass,@function
finish_input_pass:                      # @finish_input_pass
	.cfi_startproc
# BB#0:
	movq	560(%rdi), %rax
	movq	$consume_markers, (%rax)
	retq
.Lfunc_end4:
	.size	finish_input_pass, .Lfunc_end4-finish_input_pass
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
