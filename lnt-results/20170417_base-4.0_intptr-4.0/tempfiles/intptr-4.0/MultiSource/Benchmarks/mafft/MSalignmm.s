	.text
	.file	"MSalignmm.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI0_1:
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_2:
	.quad	4607182418800017408     # double 1
.LCPI0_3:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	MSalignmm
	.p2align	4, 0x90
	.type	MSalignmm,@function
MSalignmm:                              # @MSalignmm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 192
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, (%rsp)            # 4-byte Spill
	movl	%r8d, %ebp
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %r12
	cvtsi2ssl	penalty(%rip), %xmm0
	movss	%xmm0, 112(%rsp)        # 4-byte Spill
	movq	(%r12), %rdi
	callq	seqlen
	movl	%eax, 116(%rsp)         # 4-byte Spill
	movq	(%r13), %rdi
	callq	seqlen
	movl	%eax, 108(%rsp)         # 4-byte Spill
	movq	(%r12), %rdi
	callq	strlen
	movq	%rax, %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movl	%eax, %r15d
	movq	(%r13), %rdi
	callq	strlen
	movq	%rax, %r14
	leal	200(%r15,%r14), %ebx
	movl	%ebp, %edi
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	(%rsp), %edi            # 4-byte Reload
	movl	%ebx, %esi
	callq	AllocateCharMtx
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	$4, %edi
	xorl	%esi, %esi
	callq	AllocateFloatMtx
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	%r15, 16(%rsp)          # 8-byte Spill
	leal	102(%r15), %ebx
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r14, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leal	102(%r14), %ebp
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$27, %esi
	movl	%ebx, %edi
	callq	AllocateFloatMtx
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	$27, %esi
	movl	%ebp, %edi
	callq	AllocateFloatMtx
	movq	%rax, 88(%rsp)          # 8-byte Spill
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB0_7
# BB#1:                                 # %.lr.ph252
	movslq	120(%rsp), %r14         # 4-byte Folded Reload
	movslq	4(%rsp), %rbp           # 4-byte Folded Reload
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbx,8), %rdi
	callq	strlen
	cmpq	%r14, %rax
	jne	.LBB0_3
# BB#6:                                 #   in Loop: Header=BB0_2 Depth=1
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB0_2
.LBB0_7:                                # %.preheader227
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	jle	.LBB0_12
# BB#8:                                 # %.lr.ph248
	movslq	8(%rsp), %r14           # 4-byte Folded Reload
	movslq	(%rsp), %rbp            # 4-byte Folded Reload
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %rdi
	callq	strlen
	cmpq	%r14, %rax
	jne	.LBB0_10
# BB#11:                                #   in Loop: Header=BB0_9 Depth=1
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB0_9
.LBB0_12:                               # %._crit_edge249
	movq	%r12, %rdi
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %r14
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %ecx
	movl	4(%rsp), %ebp           # 4-byte Reload
	movl	%ebp, %r8d
	callq	MScpmx_calc_new
	movq	%r13, %rdi
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	(%rsp), %r15d           # 4-byte Reload
	movl	%r15d, %r8d
	callq	MScpmx_calc_new
	movq	200(%rsp), %r9
	testq	%r9, %r9
	je	.LBB0_14
# BB#13:
	movq	208(%rsp), %rbx
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	new_OpeningGapCount
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movq	%r13, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	%r14d, %r8d
	movq	%rbx, %r9
	movq	16(%rsp), %rbx          # 8-byte Reload
	callq	new_OpeningGapCount
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	movq	%r12, %rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%ebx, %r8d
	movq	224(%rsp), %r9
	callq	new_FinalGapCount
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %ebp
	movl	%r15d, %esi
	movq	%r13, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%r14d, %r8d
	movq	224(%rsp), %r9
	callq	new_FinalGapCount
	jmp	.LBB0_15
.LBB0_14:
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	movq	%r12, %rdx
	movq	%r14, %rcx
	movl	%ebx, %r8d
	callq	st_OpeningGapCount
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movq	%r13, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	%r14d, %r8d
	callq	st_OpeningGapCount
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	movq	%r12, %rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%ebx, %r8d
	callq	st_FinalGapCount
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %ebp
	movl	%r15d, %esi
	movq	%r13, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%r14d, %r8d
	callq	st_FinalGapCount
.LBB0_15:                               # %.preheader226
	testl	%ebx, %ebx
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movss	112(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	48(%rsp), %r10          # 8-byte Reload
	movl	%ebp, %esi
	jle	.LBB0_26
# BB#16:                                # %.lr.ph246
	cvtss2sd	%xmm7, %xmm0
	movq	120(%rsp), %rdi         # 8-byte Reload
	movl	%edi, %eax
	cmpq	$3, %rax
	jbe	.LBB0_17
# BB#20:                                # %min.iters.checked
	andl	$3, %edi
	movq	%rax, %rcx
	subq	%rdi, %rcx
	je	.LBB0_17
# BB#21:                                # %vector.memcheck
	leaq	(%r9,%rax,4), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB0_23
# BB#22:                                # %vector.memcheck
	leaq	(%rbx,%rax,4), %rdx
	cmpq	%rdx, %r9
	jae	.LBB0_23
.LBB0_17:
	xorl	%ecx, %ecx
.LBB0_18:                               # %scalar.ph.preheader
	leaq	(%rbx,%rcx,4), %rdx
	leaq	(%r9,%rcx,4), %rdi
	subq	%rcx, %rax
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_3(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB0_19:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rdx)
	movss	(%rdi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rdi)
	addq	$4, %rdx
	addq	$4, %rdi
	decq	%rax
	jne	.LBB0_19
.LBB0_26:                               # %.preheader225
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB0_37
# BB#27:                                # %.lr.ph243
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm7, %xmm0
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpq	$3, %rax
	jbe	.LBB0_28
# BB#31:                                # %min.iters.checked306
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB0_28
# BB#32:                                # %vector.memcheck319
	leaq	(%r8,%rax,4), %rdi
	cmpq	%rdi, %r10
	jae	.LBB0_34
# BB#33:                                # %vector.memcheck319
	leaq	(%r10,%rax,4), %rdi
	cmpq	%rdi, %r8
	jae	.LBB0_34
.LBB0_28:
	xorl	%ecx, %ecx
.LBB0_29:                               # %scalar.ph304.preheader
	leaq	(%r10,%rcx,4), %rdx
	leaq	(%r8,%rcx,4), %rdi
	subq	%rcx, %rax
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_3(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB0_30:                               # %scalar.ph304
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rdx)
	movss	(%rdi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rdi)
	addq	$4, %rdx
	addq	$4, %rdi
	decq	%rax
	jne	.LBB0_30
.LBB0_37:                               # %._crit_edge244
	movq	128(%rsp), %rbp         # 8-byte Reload
	movq	%rbx, (%rbp)
	movq	%r9, 8(%rbp)
	movq	%r10, 16(%rbp)
	movq	%r8, 24(%rbp)
	movq	16(%rsp), %rbx          # 8-byte Reload
	decl	%ebx
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	-1(%rax), %eax
	movl	4(%rsp), %r15d          # 4-byte Reload
	movl	%r15d, %edi
	movq	%r12, %rdx
	movq	%r13, %rcx
	movq	96(%rsp), %r8           # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	pushq	%rbp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)                # 8-byte Folded Reload
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)               # 8-byte Folded Reload
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	callq	MSalignmm_rec
	addq	$64, %rsp
.Lcfi21:
	.cfi_adjust_cfa_offset -64
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	testl	%r15d, %r15d
	jle	.LBB0_40
# BB#38:                                # %.lr.ph241.preheader
	movl	%r15d, %r14d
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB0_39:                               # %.lr.ph241
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rbp), %rsi
	callq	strcpy
	addq	$8, %rbx
	addq	$8, %rbp
	decq	%r14
	jne	.LBB0_39
.LBB0_40:                               # %.preheader224
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB0_43
# BB#41:                                # %.lr.ph237.preheader
	movl	(%rsp), %r14d           # 4-byte Reload
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB0_42:                               # %.lr.ph237
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rbp), %rsi
	callq	strcpy
	addq	$8, %rbx
	addq	$8, %rbp
	decq	%r14
	jne	.LBB0_42
.LBB0_43:                               # %._crit_edge238
	movq	(%r12), %rdi
	callq	seqlen
	movl	116(%rsp), %ebp         # 4-byte Reload
	cmpl	%ebp, %eax
	jne	.LBB0_63
# BB#44:
	movq	(%r13), %rdi
	callq	seqlen
	movl	108(%rsp), %ebp         # 4-byte Reload
	cmpl	%ebp, %eax
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
	jne	.LBB0_64
# BB#45:
	callq	FreeFloatVec
	movq	%rbx, %rdi
	callq	FreeFloatVec
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatVec
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatVec
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatMtx
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatMtx
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	FreeCharMtx
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	FreeCharMtx
	movq	(%r13), %r14
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testl	%r15d, %r15d
	jle	.LBB0_48
# BB#46:                                # %.lr.ph235
	movq	(%r12), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movslq	%eax, %rbp
	movq	%rbx, %rdi
	callq	strlen
	cmpq	%rbp, %rax
	jne	.LBB0_47
# BB#54:                                # %.lr.ph293.preheader
	movslq	4(%rsp), %r15           # 4-byte Folded Reload
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_55:                               # %.lr.ph293
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r15, %rbx
	jge	.LBB0_48
# BB#56:                                # %._crit_edge277
                                        #   in Loop: Header=BB0_55 Depth=1
	movq	(%r12,%rbx,8), %rdi
	callq	strlen
	incq	%rbx
	cmpq	%rbp, %rax
	je	.LBB0_55
# BB#51:                                # %._crit_edge294
	decl	%ebx
.LBB0_52:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movl	4(%rsp), %ecx           # 4-byte Reload
	jmp	.LBB0_53
.LBB0_48:                               # %.preheader
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB0_62
# BB#49:                                # %.lr.ph
	movslq	16(%rsp), %r15          # 4-byte Folded Reload
	movq	%r14, %rdi
	callq	strlen
	cmpq	%r15, %rax
	jne	.LBB0_50
# BB#59:                                # %.lr.ph290.preheader
	movslq	(%rsp), %rbp            # 4-byte Folded Reload
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_60:                               # %.lr.ph290
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rbp, %rbx
	jge	.LBB0_62
# BB#61:                                # %._crit_edge278
                                        #   in Loop: Header=BB0_60 Depth=1
	movq	(%r13,%rbx,8), %rdi
	callq	strlen
	incq	%rbx
	cmpq	%r15, %rax
	je	.LBB0_60
# BB#57:                                # %._crit_edge291
	decl	%ebx
.LBB0_58:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movl	(%rsp), %ecx            # 4-byte Reload
.LBB0_53:
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$42, %esi
	jmp	.LBB0_5
.LBB0_62:                               # %._crit_edge
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_23:                               # %vector.ph
	movq	%rdi, %r11
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movapd	.LCPI0_0(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI0_1(%rip), %xmm3   # xmm3 = [5.000000e-01,5.000000e-01]
	movq	%rcx, %rdx
	movq	%r9, %rbp
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB0_24:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	8(%rdi), %xmm4
	cvtps2pd	(%rdi), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rdi)
	cvtps2pd	8(%rbp), %xmm4
	cvtps2pd	(%rbp), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rbp)
	addq	$16, %rdi
	addq	$16, %rbp
	addq	$-4, %rdx
	jne	.LBB0_24
# BB#25:                                # %middle.block
	testq	%r11, %r11
	jne	.LBB0_18
	jmp	.LBB0_26
.LBB0_34:                               # %vector.ph320
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movapd	.LCPI0_0(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI0_1(%rip), %xmm3   # xmm3 = [5.000000e-01,5.000000e-01]
	movq	%rcx, %rsi
	movq	%r8, %rdi
	movq	%r10, %rbp
	.p2align	4, 0x90
.LBB0_35:                               # %vector.body302
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	8(%rbp), %xmm4
	cvtps2pd	(%rbp), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rbp)
	cvtps2pd	8(%rdi), %xmm4
	cvtps2pd	(%rdi), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rdi)
	addq	$16, %rbp
	addq	$16, %rdi
	addq	$-4, %rsi
	jne	.LBB0_35
# BB#36:                                # %middle.block303
	testq	%rdx, %rdx
	movl	(%rsp), %esi            # 4-byte Reload
	jne	.LBB0_29
	jmp	.LBB0_37
.LBB0_3:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movl	4(%rsp), %ecx           # 4-byte Reload
	jmp	.LBB0_4
.LBB0_10:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movl	(%rsp), %ecx            # 4-byte Reload
.LBB0_4:
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$27, %esi
.LBB0_5:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB0_63:
	movq	stderr(%rip), %rbx
	movq	(%r12), %rdi
	callq	seqlen
	movl	%eax, %ecx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ecx, %edx
	movl	%ebp, %ecx
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	(%r12), %rdx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB0_64:
	movq	stderr(%rip), %rbx
	movq	(%r13), %rdi
	callq	seqlen
	movl	%eax, %ecx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ecx, %edx
	movl	%ebp, %ecx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB0_47:
	xorl	%ebx, %ebx
	jmp	.LBB0_52
.LBB0_50:
	xorl	%ebx, %ebx
	jmp	.LBB0_58
.Lfunc_end0:
	.size	MSalignmm, .Lfunc_end0-MSalignmm
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_1:
	.long	3407386240              # float -1.0E+7
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_2:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI1_3:
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
.LCPI1_4:
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
.LCPI1_5:
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.text
	.p2align	4, 0x90
	.type	MSalignmm_rec,@function
MSalignmm_rec:                          # @MSalignmm_rec
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$616, %rsp              # imm = 0x268
.Lcfi28:
	.cfi_def_cfa_offset 672
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%r9, 328(%rsp)          # 8-byte Spill
	movq	%r8, 320(%rsp)          # 8-byte Spill
	movq	%rcx, %rbx
	movq	%rdx, %r13
	movl	%esi, %r14d
	movl	688(%rsp), %edx
	movl	672(%rsp), %r15d
	movl	680(%rsp), %eax
	movq	728(%rsp), %rcx
	movq	(%rcx), %rsi
	movq	%rsi, 168(%rsp)         # 8-byte Spill
	movq	8(%rcx), %rsi
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	16(%rcx), %rsi
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movq	24(%rcx), %rsi
	incl	reccycle(%rip)
	movl	%eax, %ebp
	subl	%r15d, %ebp
	movl	696(%rsp), %ecx
	movl	%ecx, %eax
	subl	%edx, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%edx, %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movslq	%ebp, %rcx
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	leaq	1(%rcx), %rdx
	movq	704(%rsp), %rcx
	movslq	%r15d, %r12
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	js	.LBB1_1
# BB#18:
	movq	%rsi, 184(%rsp)         # 8-byte Spill
	movq	%rbx, 312(%rsp)         # 8-byte Spill
	movq	%rbp, 176(%rsp)         # 8-byte Spill
	cltq
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movl	%edi, 36(%rsp)          # 4-byte Spill
	callq	AllocateCharMtx
	movq	%rax, %r15
	xorl	%esi, %esi
	movl	%r14d, %edi
	callq	AllocateCharMtx
	movq	704(%rsp), %r8
	movl	36(%rsp), %ecx          # 4-byte Reload
	testl	%ecx, %ecx
	jle	.LBB1_33
# BB#19:                                # %.lr.ph150.preheader
	movl	%ecx, %ebx
	cmpl	$3, %ecx
	jbe	.LBB1_20
# BB#27:                                # %min.iters.checked
	movl	%ecx, %edx
	andl	$3, %edx
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	je	.LBB1_20
# BB#28:                                # %vector.memcheck
	leaq	(%r8,%rbx,8), %rsi
	cmpq	%rsi, %r15
	jae	.LBB1_30
# BB#29:                                # %vector.memcheck
	leaq	(%r15,%rbx,8), %rsi
	cmpq	%r8, %rsi
	jbe	.LBB1_30
.LBB1_20:
	xorl	%ecx, %ecx
.LBB1_21:                               # %.lr.ph150.preheader856
	movl	%ebx, %esi
	subl	%ecx, %esi
	leaq	-1(%rbx), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB1_24
# BB#22:                                # %.lr.ph150.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph150.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rcx,8), %rdi
	movq	%rdi, (%r15,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB1_23
.LBB1_24:                               # %.lr.ph150.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB1_33
# BB#25:                                # %.lr.ph150.preheader856.new
	subq	%rcx, %rbx
	leaq	56(%r15,%rcx,8), %rdx
	leaq	56(%r8,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB1_26:                               # %.lr.ph150
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rsi
	movq	%rsi, -56(%rdx)
	movq	-48(%rcx), %rsi
	movq	%rsi, -48(%rdx)
	movq	-40(%rcx), %rsi
	movq	%rsi, -40(%rdx)
	movq	-32(%rcx), %rsi
	movq	%rsi, -32(%rdx)
	movq	-24(%rcx), %rsi
	movq	%rsi, -24(%rdx)
	movq	-16(%rcx), %rsi
	movq	%rsi, -16(%rdx)
	movq	-8(%rcx), %rsi
	movq	%rsi, -8(%rdx)
	movq	(%rcx), %rsi
	movq	%rsi, (%rdx)
	addq	$64, %rdx
	addq	$64, %rcx
	addq	$-8, %rbx
	jne	.LBB1_26
.LBB1_33:                               # %.preheader42
	incq	48(%rsp)                # 8-byte Folded Spill
	testl	%r14d, %r14d
	movq	160(%rsp), %r8          # 8-byte Reload
	jle	.LBB1_48
# BB#34:                                # %.lr.ph146.preheader
	movl	%r14d, %ebx
	cmpl	$3, %r14d
	jbe	.LBB1_35
# BB#42:                                # %min.iters.checked323
	movl	%r14d, %edx
	andl	$3, %edx
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	je	.LBB1_35
# BB#43:                                # %vector.memcheck336
	movq	712(%rsp), %rdi
	leaq	(%rdi,%rbx,8), %rsi
	cmpq	%rsi, %rax
	jae	.LBB1_45
# BB#44:                                # %vector.memcheck336
	leaq	(%rax,%rbx,8), %rsi
	cmpq	%rdi, %rsi
	jbe	.LBB1_45
.LBB1_35:
	xorl	%ecx, %ecx
.LBB1_36:                               # %.lr.ph146.preheader855
	movl	%ebx, %esi
	subl	%ecx, %esi
	leaq	-1(%rbx), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB1_39
# BB#37:                                # %.lr.ph146.prol.preheader
	negq	%rsi
	movq	712(%rsp), %rbp
	.p2align	4, 0x90
.LBB1_38:                               # %.lr.ph146.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rcx,8), %rdi
	movq	%rdi, (%rax,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB1_38
.LBB1_39:                               # %.lr.ph146.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB1_48
# BB#40:                                # %.lr.ph146.preheader855.new
	subq	%rcx, %rbx
	leaq	56(%rax,%rcx,8), %rdx
	movq	712(%rsp), %rsi
	leaq	56(%rsi,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB1_41:                               # %.lr.ph146
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rsi
	movq	%rsi, -56(%rdx)
	movq	-48(%rcx), %rsi
	movq	%rsi, -48(%rdx)
	movq	-40(%rcx), %rsi
	movq	%rsi, -40(%rdx)
	movq	-32(%rcx), %rsi
	movq	%rsi, -32(%rdx)
	movq	-24(%rcx), %rsi
	movq	%rsi, -24(%rdx)
	movq	-16(%rcx), %rsi
	movq	%rsi, -16(%rdx)
	movq	-8(%rcx), %rsi
	movq	%rsi, -8(%rdx)
	movq	(%rcx), %rsi
	movq	%rsi, (%rdx)
	addq	$64, %rdx
	addq	$64, %rcx
	addq	$-8, %rbx
	jne	.LBB1_41
.LBB1_48:                               # %._crit_edge147
	movq	%r12, 88(%rsp)          # 8-byte Spill
	movl	688(%rsp), %ecx
	movslq	%ecx, %r12
	cmpl	$100, %r8d
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movq	%r15, 216(%rsp)         # 8-byte Spill
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%r12, 104(%rsp)         # 8-byte Spill
	movq	%r13, 304(%rsp)         # 8-byte Spill
	movq	%rax, 48(%rsp)          # 8-byte Spill
	jl	.LBB1_50
# BB#49:                                # %._crit_edge147
	cmpl	$99, %eax
	jle	.LBB1_50
# BB#182:
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	168(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,4), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%rcx,4), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	176(%rsp), %rbp         # 8-byte Reload
	leal	101(%rbp), %r15d
	movq	64(%rsp), %rax          # 8-byte Reload
	leal	101(%rax), %ecx
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	leal	103(%rax), %r13d
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, 384(%rsp)         # 8-byte Spill
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, 344(%rsp)         # 8-byte Spill
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	%r13d, %edi
	callq	AllocateIntVec
	movq	%rax, 400(%rsp)         # 8-byte Spill
	movl	%r13d, %edi
	callq	AllocateIntVec
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movl	%r13d, %edi
	callq	AllocateIntVec
	movq	%rax, 376(%rsp)         # 8-byte Spill
	movl	%r13d, %edi
	callq	AllocateIntVec
	movq	%rax, 368(%rsp)         # 8-byte Spill
	movl	%r13d, %edi
	callq	AllocateIntVec
	movq	%rax, 440(%rsp)         # 8-byte Spill
	movl	%r13d, %edi
	callq	AllocateIntVec
	movq	%rax, 432(%rsp)         # 8-byte Spill
	leal	103(%rbp), %ebx
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, %r14
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, 392(%rsp)         # 8-byte Spill
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%r13d, %edi
	callq	AllocateIntVec
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	40(%rsp), %ebx          # 4-byte Reload
	cmpl	%ebx, %r15d
	cmovgel	%r15d, %ebx
	movq	%rbp, %r15
	addl	$2, %ebx
	movl	%ebx, %edi
	callq	AllocateCharVec
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movl	$26, %esi
	movl	%ebx, %edi
	callq	AllocateFloatMtx
	movq	%rax, %r13
	movl	$26, %esi
	movl	%ebx, %edi
	callq	AllocateIntMtx
	movq	%rax, %rbp
	movq	328(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12,8), %r12
	movq	320(%rsp), %rax         # 8-byte Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rbx
	movl	$0, %ecx
	movq	%r14, 96(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	160(%rsp), %r8          # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%r13, %r9
	pushq	$1
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset -16
	movl	$0, %ecx
	movq	288(%rsp), %rdi         # 8-byte Reload
	movq	%rbx, 408(%rsp)         # 8-byte Spill
	movq	%rbx, %rsi
	movq	%r12, 256(%rsp)         # 8-byte Spill
	movq	%r12, %rdx
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%r13, 360(%rsp)         # 8-byte Spill
	movq	%r13, %r9
	pushq	$1
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	movq	%rbp, 360(%rsp)         # 8-byte Spill
	pushq	%rbp
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	movq	96(%rsp), %rdi          # 8-byte Reload
	addq	$16, %rsp
.Lcfi40:
	.cfi_adjust_cfa_offset -16
	leal	2(%r15), %eax
	cmpl	$2, %eax
	jl	.LBB1_197
# BB#183:                               # %.lr.ph144.preheader
	movl	%eax, %eax
	leaq	-1(%rax), %r9
	cmpq	$7, %r9
	jbe	.LBB1_184
# BB#190:                               # %min.iters.checked352
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB1_184
# BB#191:                               # %vector.memcheck371
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	4(%rcx), %rdx
	leaq	(%rcx,%rax,4), %rsi
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	%rdi, %rbp
	leaq	(%rcx,%rax), %rdi
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	-4(%rcx,%rdi,4), %rdi
	cmpq	%rbp, %rdx
	sbbb	%bl, %bl
	cmpq	%rsi, %rbp
	sbbb	%cl, %cl
	andb	%bl, %cl
	cmpq	%rdi, %rdx
	sbbb	%bl, %bl
	cmpq	%rsi, 200(%rsp)         # 8-byte Folded Reload
	sbbb	%sil, %sil
	movl	$1, %edx
	testb	$1, %cl
	jne	.LBB1_192
# BB#193:                               # %vector.memcheck371
	andb	%sil, %bl
	andb	$1, %bl
	movq	80(%rsp), %rdi          # 8-byte Reload
	jne	.LBB1_185
# BB#194:                               # %vector.body348.preheader
	movq	%r8, %rdx
	orq	$1, %rdx
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	leaq	16(%rsi,%rcx,4), %rdi
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	20(%rcx), %rbp
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB1_195:                              # %vector.body348
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi), %xmm1
	movups	(%rdi), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %rsi
	jne	.LBB1_195
# BB#196:                               # %middle.block349
	cmpq	%r8, %r9
	movq	80(%rsp), %rdi          # 8-byte Reload
	jne	.LBB1_185
	jmp	.LBB1_197
.LBB1_1:                                # %.preheader29
	testl	%edi, %edi
	jle	.LBB1_4
# BB#2:                                 # %.lr.ph57
	movl	%edi, %ebx
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdi
	movq	(%r13), %rsi
	addq	%r12, %rsi
	movq	%rcx, %r14
	callq	strncpy
	movq	%r14, %rcx
	movq	160(%rsp), %rdx         # 8-byte Reload
	movq	(%rcx), %rax
	movb	$0, (%rax,%rdx)
	addq	$8, %rcx
	addq	$8, %r13
	decq	%rbx
	jne	.LBB1_3
.LBB1_4:                                # %.preheader
	xorps	%xmm0, %xmm0
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_402
# BB#5:                                 # %.lr.ph55
	testl	%ebp, %ebp
	js	.LBB1_6
# BB#13:                                # %.lr.ph55.split.preheader
	movl	680(%rsp), %r12d
	incl	%r12d
	subl	%r15d, %r12d
	movl	12(%rsp), %r14d         # 4-byte Reload
	xorl	%r15d, %r15d
	movq	712(%rsp), %r13
	.p2align	4, 0x90
.LBB1_14:                               # %.lr.ph55.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_15 Depth 2
	movq	(%r13,%r15,8), %rax
	movb	$0, (%rax)
	movl	%r12d, %ebp
	.p2align	4, 0x90
.LBB1_15:                               #   Parent Loop BB1_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13,%r15,8), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movw	$45, (%rbx,%rax)
	decl	%ebp
	jne	.LBB1_15
# BB#16:                                # %._crit_edge
                                        #   in Loop: Header=BB1_14 Depth=1
	incq	%r15
	cmpq	%r14, %r15
	jne	.LBB1_14
# BB#17:
	xorps	%xmm0, %xmm0
	jmp	.LBB1_402
.LBB1_50:
	movq	88(%rsp), %rsi          # 8-byte Reload
	leaq	(,%rsi,4), %rcx
	movq	728(%rsp), %rax
	addq	(%rax), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	8(%rax), %rdx
	movq	24(%rax), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	leaq	(%rdx,%rsi,4), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	(,%r12,4), %r15
	addq	16(%rax), %r15
	movq	176(%rsp), %r12         # 8-byte Reload
	leal	101(%r12), %ecx
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	leal	101(%rax), %r14d
	leal	103(%rax), %ebp
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, 168(%rsp)         # 8-byte Spill
	leal	103(%r12), %ebx
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	%ebp, %edi
	callq	AllocateIntVec
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	40(%rsp), %eax          # 4-byte Reload
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	addl	$2, %r14d
	movl	$27, %esi
	movl	%r14d, %edi
	callq	AllocateFloatMtx
	movq	%rax, %rbx
	movl	$27, %esi
	movl	%r14d, %edi
	callq	AllocateIntMtx
	movq	%rax, %rbp
	leal	102(%r12), %edi
	movq	64(%rsp), %rax          # 8-byte Reload
	leal	102(%rax), %esi
	callq	AllocateIntMtx
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	328(%rsp), %rax         # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	leaq	(%rax,%rcx,8), %r14
	movq	320(%rsp), %rax         # 8-byte Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,8), %r12
	movl	$0, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	160(%rsp), %r8          # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%rbx, %r9
	pushq	$1
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi43:
	.cfi_adjust_cfa_offset -16
	movl	$0, %ecx
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	%r12, 208(%rsp)         # 8-byte Spill
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movq	%rbx, %r9
	pushq	$1
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	pushq	%rbp
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	movq	32(%rsp), %rdi          # 8-byte Reload
	addq	$16, %rsp
.Lcfi46:
	.cfi_adjust_cfa_offset -16
	movq	176(%rsp), %rax         # 8-byte Reload
	leal	2(%rax), %r8d
	cmpl	$2, %r8d
	jl	.LBB1_65
# BB#51:                                # %.lr.ph41.preheader.i
	movl	%r8d, %eax
	leaq	-1(%rax), %rcx
	cmpq	$7, %rcx
	jbe	.LBB1_52
# BB#58:                                # %min.iters.checked633
	movq	%rcx, %r9
	andq	$-8, %r9
	je	.LBB1_52
# BB#59:                                # %vector.memcheck654
	movq	(%rsp), %rsi            # 8-byte Reload
	leaq	4(%rsi), %rdx
	leaq	(%rsi,%rax,4), %rsi
	movq	%rdi, %rbp
	movq	88(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rax), %rdi
	movq	56(%rsp), %rbx          # 8-byte Reload
	leaq	-4(%rbx,%rdi,4), %rdi
	cmpq	%rbp, %rdx
	sbbb	%r10b, %r10b
	cmpq	%rsi, %rbp
	sbbb	%bl, %bl
	andb	%r10b, %bl
	cmpq	%rdi, %rdx
	sbbb	%dl, %dl
	cmpq	%rsi, 24(%rsp)          # 8-byte Folded Reload
	sbbb	%dil, %dil
	movl	$1, %esi
	testb	$1, %bl
	jne	.LBB1_60
# BB#61:                                # %vector.memcheck654
	andb	%dil, %dl
	andb	$1, %dl
	movq	16(%rsp), %rdi          # 8-byte Reload
	jne	.LBB1_53
# BB#62:                                # %vector.body629.preheader
	movq	%r9, %rsi
	orq	$1, %rsi
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	16(%rdi,%rdx,4), %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	leaq	20(%rdi), %rbp
	movq	%r9, %rdi
	.p2align	4, 0x90
.LBB1_63:                               # %vector.body629
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdx), %xmm1
	movups	(%rdx), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rdx
	addq	$32, %rbp
	addq	$-8, %rdi
	jne	.LBB1_63
# BB#64:                                # %middle.block630
	cmpq	%r9, %rcx
	movq	16(%rsp), %rdi          # 8-byte Reload
	jne	.LBB1_53
	jmp	.LBB1_65
.LBB1_184:
	movl	$1, %edx
.LBB1_185:                              # %.lr.ph144.preheader854
	movl	%eax, %esi
	subl	%edx, %esi
	testb	$1, %sil
	movq	%rdx, %rsi
	je	.LBB1_187
# BB#186:                               # %.lr.ph144.prol
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	200(%rsp), %rcx         # 8-byte Reload
	addss	-4(%rcx,%rdx,4), %xmm0
	movq	96(%rsp), %rcx          # 8-byte Reload
	addss	(%rcx,%rdx,4), %xmm0
	movss	%xmm0, (%rcx,%rdx,4)
	leaq	1(%rdx), %rsi
.LBB1_187:                              # %.lr.ph144.prol.loopexit
	cmpq	%rdx, %r9
	je	.LBB1_197
# BB#188:                               # %.lr.ph144.preheader854.new
	subq	%rsi, %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rsi,%rcx), %rcx
	movq	(%rsp), %rdx            # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rcx
	movq	96(%rsp), %rdx          # 8-byte Reload
	leaq	4(%rdx,%rsi,4), %rdx
	.p2align	4, 0x90
.LBB1_189:                              # %.lr.ph144
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rcx), %xmm0
	addss	-4(%rdx), %xmm0
	movss	%xmm0, -4(%rdx)
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rcx), %xmm0
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rcx
	addq	$8, %rdx
	addq	$-2, %rax
	jne	.LBB1_189
.LBB1_197:                              # %.preheader41
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	leaq	(%rax,%rcx,4), %r14
	movq	184(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,4), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	64(%rsp), %r15          # 8-byte Reload
	leal	2(%r15), %eax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	cmpl	$2, %eax
	movq	160(%rsp), %r10         # 8-byte Reload
	movq	288(%rsp), %r13         # 8-byte Reload
	jl	.LBB1_225
# BB#198:                               # %.lr.ph140.preheader
	movl	192(%rsp), %eax         # 4-byte Reload
	leaq	-1(%rax), %r9
	cmpq	$7, %r9
	jbe	.LBB1_199
# BB#205:                               # %min.iters.checked389
	movq	%r9, %rsi
	andq	$-8, %rsi
	je	.LBB1_199
# BB#206:                               # %vector.memcheck410
	leaq	4(%r13), %rcx
	leaq	(%r13,%rax,4), %rdx
	movq	104(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rax), %rdi
	movq	184(%rsp), %rbp         # 8-byte Reload
	leaq	-4(%rbp,%rdi,4), %rdi
	cmpq	%r14, %rcx
	sbbb	%r8b, %r8b
	cmpq	%rdx, %r14
	sbbb	%bl, %bl
	andb	%r8b, %bl
	cmpq	%rdi, %rcx
	sbbb	%cl, %cl
	cmpq	%rdx, 128(%rsp)         # 8-byte Folded Reload
	sbbb	%dil, %dil
	movl	$1, %edx
	testb	$1, %bl
	jne	.LBB1_200
# BB#207:                               # %vector.memcheck410
	andb	%dil, %cl
	andb	$1, %cl
	jne	.LBB1_200
# BB#208:                               # %vector.body385.preheader
	movq	%rsi, %rdx
	orq	$1, %rdx
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	184(%rsp), %rcx         # 8-byte Reload
	movq	104(%rsp), %rdi         # 8-byte Reload
	leaq	16(%rcx,%rdi,4), %rdi
	leaq	20(%r13), %rbp
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB1_209:                              # %vector.body385
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi), %xmm1
	movups	(%rdi), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %rcx
	jne	.LBB1_209
# BB#210:                               # %middle.block386
	cmpq	%rsi, %r9
	jne	.LBB1_200
	jmp	.LBB1_211
.LBB1_52:
	movl	$1, %esi
.LBB1_53:                               # %.lr.ph41.i.preheader
	movl	%eax, %edx
	subl	%esi, %edx
	testb	$1, %dl
	movq	%rsi, %rdx
	je	.LBB1_55
# BB#54:                                # %.lr.ph41.i.prol
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	24(%rsp), %rdx          # 8-byte Reload
	addss	-4(%rdx,%rsi,4), %xmm0
	movq	(%rsp), %rdx            # 8-byte Reload
	addss	(%rdx,%rsi,4), %xmm0
	movss	%xmm0, (%rdx,%rsi,4)
	leaq	1(%rsi), %rdx
.LBB1_55:                               # %.lr.ph41.i.prol.loopexit
	cmpq	%rsi, %rcx
	je	.LBB1_65
# BB#56:                                # %.lr.ph41.i.preheader.new
	subq	%rdx, %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rdx,%rcx), %rcx
	movq	56(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,4), %rcx
	movq	(%rsp), %rsi            # 8-byte Reload
	leaq	4(%rsi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB1_57:                               # %.lr.ph41.i
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rcx), %xmm0
	addss	-4(%rdx), %xmm0
	movss	%xmm0, -4(%rdx)
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rcx), %xmm0
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rcx
	addq	$8, %rdx
	addq	$-2, %rax
	jne	.LBB1_57
.LBB1_65:                               # %.preheader10.i
	movq	104(%rsp), %rbx         # 8-byte Reload
	movq	144(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rbx,4), %r11
	movq	64(%rsp), %r14          # 8-byte Reload
	leal	2(%r14), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	cmpl	$2, %eax
	movq	112(%rsp), %r12         # 8-byte Reload
	jl	.LBB1_93
# BB#66:                                # %.lr.ph37.preheader.i
	movl	56(%rsp), %esi          # 4-byte Reload
	leaq	-1(%rsi), %r10
	cmpq	$7, %r10
	jbe	.LBB1_67
# BB#73:                                # %min.iters.checked674
	movq	%r10, %r9
	andq	$-8, %r9
	je	.LBB1_67
# BB#74:                                # %vector.memcheck695
	leaq	4(%r12), %rax
	leaq	(%r12,%rsi,4), %rdi
	leaq	(%rbx,%rsi), %rdx
	leaq	-4(%rbp,%rdx,4), %rbp
	cmpq	%r15, %rax
	sbbb	%dl, %dl
	cmpq	%rdi, %r15
	sbbb	%cl, %cl
	andb	%dl, %cl
	cmpq	%rbp, %rax
	sbbb	%dl, %dl
	cmpq	%rdi, %r11
	sbbb	%dil, %dil
	movl	$1, %eax
	testb	$1, %cl
	jne	.LBB1_75
# BB#76:                                # %vector.memcheck695
	andb	%dil, %dl
	andb	$1, %dl
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	144(%rsp), %rbp         # 8-byte Reload
	jne	.LBB1_68
# BB#77:                                # %vector.body670.preheader
	movq	%r9, %rax
	orq	$1, %rax
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%rbp,%rbx,4), %rdx
	leaq	20(%r12), %rbp
	movq	%r9, %rdi
	.p2align	4, 0x90
.LBB1_78:                               # %vector.body670
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdx), %xmm1
	movups	(%rdx), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rdx
	addq	$32, %rbp
	addq	$-8, %rdi
	jne	.LBB1_78
# BB#79:                                # %middle.block671
	cmpq	%r9, %r10
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	144(%rsp), %rbp         # 8-byte Reload
	jne	.LBB1_68
	jmp	.LBB1_80
.LBB1_199:
	movl	$1, %edx
.LBB1_200:                              # %.lr.ph140.preheader853
	movl	%eax, %ecx
	subl	%edx, %ecx
	testb	$1, %cl
	movq	%rdx, %rsi
	je	.LBB1_202
# BB#201:                               # %.lr.ph140.prol
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	128(%rsp), %rcx         # 8-byte Reload
	addss	-4(%rcx,%rdx,4), %xmm0
	addss	(%r13,%rdx,4), %xmm0
	movss	%xmm0, (%r13,%rdx,4)
	leaq	1(%rdx), %rsi
.LBB1_202:                              # %.lr.ph140.prol.loopexit
	cmpq	%rdx, %r9
	je	.LBB1_211
# BB#203:                               # %.lr.ph140.preheader853.new
	movq	%rax, %rdx
	subq	%rsi, %rdx
	movq	104(%rsp), %rcx         # 8-byte Reload
	leaq	(%rsi,%rcx), %rcx
	movq	184(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rcx,4), %rcx
	leaq	4(%r13,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB1_204:                              # %.lr.ph140
                                        # =>This Inner Loop Header: Depth=1
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rcx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rcx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rcx
	addq	$8, %rsi
	addq	$-2, %rdx
	jne	.LBB1_204
.LBB1_211:                              # %.preheader40
	cmpl	$2, 192(%rsp)           # 4-byte Folded Reload
	jl	.LBB1_225
# BB#212:                               # %.lr.ph137
	movq	80(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdi), %rdx
	cmpq	$7, %r9
	jbe	.LBB1_213
# BB#219:                               # %min.iters.checked430
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB1_213
# BB#220:                               # %vector.memcheck451
	movq	24(%rsp), %rbp          # 8-byte Reload
	leaq	4(%rbp), %rcx
	leaq	(%rbp,%rax,4), %rsi
	movq	%rdi, %r11
	leaq	-4(%r13,%rax,4), %rdi
	cmpq	%rdi, %rcx
	sbbb	%cl, %cl
	cmpq	%rsi, %r13
	sbbb	%bl, %bl
	andb	%cl, %bl
	cmpq	%r11, %rbp
	sbbb	%cl, %cl
	cmpq	%rsi, %rdx
	sbbb	%dil, %dil
	movl	$1, %esi
	testb	$1, %bl
	jne	.LBB1_214
# BB#221:                               # %vector.memcheck451
	andb	%dil, %cl
	andb	$1, %cl
	jne	.LBB1_214
# BB#222:                               # %vector.body426.preheader
	movq	%r8, %rsi
	orq	$1, %rsi
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r13), %rbp
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	20(%rcx), %rbx
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	20(%rcx), %rdi
	xorps	%xmm1, %xmm1
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB1_223:                              # %vector.body426
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbp), %xmm2
	movups	(%rbp), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm0, %xmm3
	movups	%xmm2, -16(%rbx)
	movups	%xmm3, (%rbx)
	movups	%xmm1, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rbp
	addq	$32, %rbx
	addq	$32, %rdi
	addq	$-8, %rcx
	jne	.LBB1_223
# BB#224:                               # %middle.block427
	cmpq	%r8, %r9
	jne	.LBB1_214
	jmp	.LBB1_225
.LBB1_67:
	movl	$1, %eax
	movq	16(%rsp), %rdi          # 8-byte Reload
.LBB1_68:                               # %.lr.ph37.i.preheader
	movl	%esi, %ecx
	subl	%eax, %ecx
	testb	$1, %cl
	movq	%rax, %rdx
	je	.LBB1_70
# BB#69:                                # %.lr.ph37.i.prol
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%r11,%rax,4), %xmm0
	addss	(%r12,%rax,4), %xmm0
	movss	%xmm0, (%r12,%rax,4)
	leaq	1(%rax), %rdx
.LBB1_70:                               # %.lr.ph37.i.prol.loopexit
	cmpq	%rax, %r10
	je	.LBB1_80
# BB#71:                                # %.lr.ph37.i.preheader.new
	movq	%rsi, %rax
	subq	%rdx, %rax
	leaq	(%rdx,%rbx), %rcx
	leaq	(%rbp,%rcx,4), %rcx
	leaq	4(%r12,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB1_72:                               # %.lr.ph37.i
                                        # =>This Inner Loop Header: Depth=1
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rcx), %xmm0
	addss	-4(%rdx), %xmm0
	movss	%xmm0, -4(%rdx)
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rcx), %xmm0
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rcx
	addq	$8, %rdx
	addq	$-2, %rax
	jne	.LBB1_72
.LBB1_80:                               # %.lr.ph34.i
	leaq	4(%rdi), %rcx
	cmpq	$7, %r10
	jbe	.LBB1_81
# BB#87:                                # %min.iters.checked715
	movq	%r10, %r9
	andq	$-8, %r9
	je	.LBB1_81
# BB#88:                                # %vector.memcheck736
	movq	72(%rsp), %rbp          # 8-byte Reload
	leaq	4(%rbp), %rax
	leaq	(%rbp,%rsi,4), %rdi
	leaq	-4(%r12,%rsi,4), %rdx
	cmpq	%rdx, %rax
	sbbb	%al, %al
	cmpq	%rdi, %r12
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	16(%rsp), %rbp          # 8-byte Folded Reload
	sbbb	%dl, %dl
	cmpq	%rdi, %rcx
	sbbb	%dil, %dil
	movl	$1, %eax
	testb	$1, %bl
	jne	.LBB1_82
# BB#89:                                # %vector.memcheck736
	andb	%dil, %dl
	andb	$1, %dl
	jne	.LBB1_82
# BB#90:                                # %vector.body711.preheader
	movq	%r9, %rax
	orq	$1, %rax
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r12), %rbx
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	20(%rdx), %rdx
	movq	80(%rsp), %rdi          # 8-byte Reload
	leaq	20(%rdi), %rbp
	xorps	%xmm1, %xmm1
	movq	%r9, %rdi
	.p2align	4, 0x90
.LBB1_91:                               # %vector.body711
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx), %xmm2
	movups	(%rbx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm0, %xmm3
	movups	%xmm2, -16(%rdx)
	movups	%xmm3, (%rdx)
	movups	%xmm1, -16(%rbp)
	movups	%xmm1, (%rbp)
	addq	$32, %rbx
	addq	$32, %rdx
	addq	$32, %rbp
	addq	$-8, %rdi
	jne	.LBB1_91
# BB#92:                                # %middle.block712
	cmpq	%r9, %r10
	jne	.LBB1_82
	jmp	.LBB1_93
.LBB1_81:
	movl	$1, %eax
.LBB1_82:                               # %scalar.ph713.preheader
	movl	%esi, %edx
	subl	%eax, %edx
	testb	$1, %dl
	movq	%rax, %rdi
	je	.LBB1_84
# BB#83:                                # %scalar.ph713.prol
	movss	-4(%r12,%rax,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rcx), %xmm0
	movq	72(%rsp), %rdx          # 8-byte Reload
	movss	%xmm0, (%rdx,%rax,4)
	movq	80(%rsp), %rdx          # 8-byte Reload
	movl	$0, (%rdx,%rax,4)
	leaq	1(%rax), %rdi
.LBB1_84:                               # %scalar.ph713.prol.loopexit
	cmpq	%rax, %r10
	je	.LBB1_93
# BB#85:                                # %scalar.ph713.preheader.new
	subq	%rdi, %rsi
	leaq	(%r12,%rdi,4), %rax
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	4(%rdx,%rdi,4), %rdx
	movq	72(%rsp), %rbp          # 8-byte Reload
	leaq	4(%rbp,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB1_86:                               # %scalar.ph713
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	(%rcx), %xmm0
	movss	%xmm0, -4(%rdi)
	movl	$0, -4(%rdx)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rcx), %xmm0
	movss	%xmm0, (%rdi)
	movl	$0, (%rdx)
	addq	$8, %rax
	addq	$8, %rdx
	addq	$8, %rdi
	addq	$-2, %rsi
	jne	.LBB1_86
.LBB1_93:                               # %._crit_edge35.i
	movslq	%r14d, %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	(%r12,%rax,4), %eax
	movq	136(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx)
	xorps	%xmm5, %xmm5
	cmpl	$2, %r8d
	jl	.LBB1_115
# BB#94:                                # %.lr.ph30.i
	movl	56(%rsp), %r10d         # 4-byte Reload
	movl	%r8d, %eax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	xorps	%xmm5, %xmm5
	movl	$1, %r12d
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	168(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_95:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_96 Depth 2
                                        #       Child Loop BB1_97 Depth 3
                                        #     Child Loop BB1_100 Depth 2
                                        #       Child Loop BB1_102 Depth 3
                                        #     Child Loop BB1_106 Depth 2
	movq	%r13, %r9
	movq	%rax, %r13
	leaq	-1(%r12), %r14
	movq	(%rsp), %rax            # 8-byte Reload
	movl	-4(%rax,%r12,4), %eax
	movl	%eax, (%r13)
	movq	208(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r12,8), %rax
	addq	$4, %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_96:                               #   Parent Loop BB1_95 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_97 Depth 3
	movl	$0, 512(%rsp,%rdx,4)
	xorps	%xmm0, %xmm0
	movq	$-2704, %rcx            # imm = 0xF570
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB1_97:                               #   Parent Loop BB1_95 Depth=1
                                        #     Parent Loop BB1_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm1, %xmm1
	cvtsi2ssl	n_dis+2704(%rcx,%rdx,4), %xmm1
	mulss	-4(%rsi), %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	n_dis+2808(%rcx,%rdx,4), %xmm0
	mulss	(%rsi), %xmm0
	addss	%xmm1, %xmm0
	addq	$8, %rsi
	addq	$208, %rcx
	jne	.LBB1_97
# BB#98:                                #   in Loop: Header=BB1_96 Depth=2
	movss	%xmm0, 512(%rsp,%rdx,4)
	incq	%rdx
	cmpq	$26, %rdx
	jne	.LBB1_96
# BB#99:                                # %.preheader.i.i
                                        #   in Loop: Header=BB1_95 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	120(%rsp), %rbp         # 8-byte Reload
	movq	128(%rsp), %rbx         # 8-byte Reload
	movq	%r9, %rdx
	je	.LBB1_104
	.p2align	4, 0x90
.LBB1_100:                              # %.lr.ph85.i.i
                                        #   Parent Loop BB1_95 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_102 Depth 3
	decl	%eax
	movl	$0, (%rdx)
	movq	(%rbp), %rdi
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	js	.LBB1_103
# BB#101:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB1_100 Depth=2
	movq	(%rbx), %rsi
	addq	$4, %rdi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_102:                              #   Parent Loop BB1_95 Depth=1
                                        #     Parent Loop BB1_100 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ecx, %rcx
	movss	512(%rsp,%rcx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rsi), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdx)
	movl	(%rdi), %ecx
	addq	$4, %rdi
	addq	$4, %rsi
	testl	%ecx, %ecx
	jns	.LBB1_102
.LBB1_103:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB1_100 Depth=2
	addq	$4, %rdx
	addq	$8, %rbp
	addq	$8, %rbx
	testl	%eax, %eax
	jne	.LBB1_100
.LBB1_104:                              # %match_calc.exit.i
                                        #   in Loop: Header=BB1_95 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax,%r12,4), %eax
	movl	%eax, (%r9)
	cmpl	$2, 56(%rsp)            # 4-byte Folded Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	jl	.LBB1_114
# BB#105:                               # %.lr.ph.i
                                        #   in Loop: Header=BB1_95 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %rbx
	movss	(%r13), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	xorl	%eax, %eax
	movl	$-1, %ebp
	movl	$2, %r8d
	jmp	.LBB1_106
	.p2align	4, 0x90
.LBB1_113:                              # %._crit_edge68.i
                                        #   in Loop: Header=BB1_106 Depth=2
	movaps	%xmm0, %xmm3
	cmpnless	%xmm2, %xmm3
	movaps	%xmm3, %xmm1
	andnps	%xmm2, %xmm1
	andps	%xmm0, %xmm3
	orps	%xmm1, %xmm3
	movss	-4(%r13,%r8,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	decl	%ebp
	incq	%r8
	movaps	%xmm3, %xmm0
.LBB1_106:                              #   Parent Loop BB1_95 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, -4(%rbx,%r8,4)
	movss	-8(%r11,%r8,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm2
	ucomiss	%xmm1, %xmm2
	movaps	%xmm1, %xmm5
	jbe	.LBB1_108
# BB#107:                               #   in Loop: Header=BB1_106 Depth=2
	leal	(%rbp,%rax), %ecx
	movl	%ecx, -4(%rbx,%r8,4)
	movaps	%xmm2, %xmm5
.LBB1_108:                              #   in Loop: Header=BB1_106 Depth=2
	movss	-4(%r15,%r8,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm2
	leal	-2(%r8), %ecx
	ucomiss	%xmm0, %xmm2
	cmovael	%ecx, %eax
	movss	-4(%rdx,%r8,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movq	24(%rsp), %rcx          # 8-byte Reload
	movss	-4(%rcx,%r12,4), %xmm4  # xmm4 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	ucomiss	%xmm5, %xmm4
	jbe	.LBB1_110
# BB#109:                               #   in Loop: Header=BB1_106 Depth=2
	movl	%r12d, %ecx
	subl	-4(%rsi,%r8,4), %ecx
	movl	%ecx, -4(%rbx,%r8,4)
	movaps	%xmm4, %xmm5
.LBB1_110:                              #   in Loop: Header=BB1_106 Depth=2
	addss	(%rdi,%r12,4), %xmm1
	ucomiss	%xmm3, %xmm1
	jb	.LBB1_112
# BB#111:                               #   in Loop: Header=BB1_106 Depth=2
	movss	%xmm1, -4(%rdx,%r8,4)
	movl	%r14d, -4(%rsi,%r8,4)
.LBB1_112:                              #   in Loop: Header=BB1_106 Depth=2
	movss	-4(%r9,%r8,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm1
	movss	%xmm1, -4(%r9,%r8,4)
	cmpq	%r8, %r10
	jne	.LBB1_113
.LBB1_114:                              # %._crit_edge.i
                                        #   in Loop: Header=BB1_95 Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	(%r9,%rax,4), %eax
	movq	136(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx,%r12,4)
	incq	%r12
	cmpq	264(%rsp), %r12         # 8-byte Folded Reload
	movq	%r9, %rax
	jne	.LBB1_95
.LBB1_115:                              # %._crit_edge31.i
	movss	%xmm5, 16(%rsp)         # 4-byte Spill
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	160(%rsp), %rax         # 8-byte Reload
	leal	1(%r12,%rax), %r14d
	movl	%r14d, %edi
	callq	AllocateCharVec
	movq	%rax, %r15
	movl	%r14d, %edi
	callq	AllocateCharVec
	cmpl	$-1, 176(%rsp)          # 4-byte Folded Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	jl	.LBB1_122
# BB#116:                               # %.lr.ph41.preheader.i.i
	movl	$2, %ebx
	movl	672(%rsp), %ecx
	subl	%ecx, %ebx
	movl	680(%rsp), %ecx
	addl	%ecx, %ebx
	leaq	-1(%rbx), %rcx
	movq	%rbx, %rsi
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB1_119
# BB#117:                               # %.lr.ph41.i.i.prol.preheader
	movq	40(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_118:                              # %.lr.ph41.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB1_118
.LBB1_119:                              # %.lr.ph41.i.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB1_122
# BB#120:                               # %.lr.ph41.preheader.i.i.new
	negq	%rbx
	leaq	4(%rdx,%rbx), %r9
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	56(%rcx,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_121:                              # %.lr.ph41.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%r9,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB1_121
.LBB1_122:                              # %.preheader1.i.i
	movq	160(%rsp), %r11         # 8-byte Reload
	leal	(%r12,%r11), %ecx
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	cmpl	$-1, %r8d
	movq	40(%rsp), %rbp          # 8-byte Reload
	jl	.LBB1_131
# BB#123:                               # %.lr.ph37.i.i
	movq	(%rbp), %rcx
	movl	$2, %esi
	movl	688(%rsp), %edx
	subl	%edx, %esi
	movl	696(%rsp), %edx
	addl	%edx, %esi
	cmpl	$7, %esi
	jbe	.LBB1_124
# BB#127:                               # %min.iters.checked754
	movl	%esi, %edi
	andl	$7, %edi
	movq	%rsi, %rbx
	subq	%rdi, %rbx
	je	.LBB1_124
# BB#128:                               # %vector.body750.preheader
	movl	%ebx, %edx
	xorl	%ebp, %ebp
	movdqa	.LCPI1_2(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI1_3(%rip), %xmm1   # xmm1 = [4,5,6,7]
	pcmpeqd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB1_129:                              # %vector.body750
                                        # =>This Inner Loop Header: Depth=1
	movd	%ebp, %xmm3
	pshufd	$0, %xmm3, %xmm3        # xmm3 = xmm3[0,0,0,0]
	movdqa	%xmm3, %xmm4
	paddd	%xmm0, %xmm4
	paddd	%xmm1, %xmm3
	pxor	%xmm2, %xmm4
	pxor	%xmm2, %xmm3
	movdqu	%xmm4, (%rcx,%rbp,4)
	movdqu	%xmm3, 16(%rcx,%rbp,4)
	addq	$8, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB1_129
# BB#130:                               # %middle.block751
	testl	%edi, %edi
	movq	40(%rsp), %rbp          # 8-byte Reload
	jne	.LBB1_125
	jmp	.LBB1_131
.LBB1_124:
	xorl	%ebx, %ebx
	xorl	%edx, %edx
.LBB1_125:                              # %scalar.ph752.preheader
	leaq	(%rcx,%rbx,4), %rcx
	subq	%rbx, %rsi
	notl	%edx
	.p2align	4, 0x90
.LBB1_126:                              # %scalar.ph752
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rcx)
	addq	$4, %rcx
	decl	%edx
	decq	%rsi
	jne	.LBB1_126
.LBB1_131:                              # %._crit_edge38.i.i
	movq	%r15, 56(%rsp)          # 8-byte Spill
	leaq	(%r15,%r11), %r15
	movb	$0, (%r12,%r15)
	addq	%r12, %r15
	movq	%r11, %rcx
	movq	%rax, 208(%rsp)         # 8-byte Spill
	addq	%rax, %rcx
	leaq	(%rcx,%r12), %r14
	movb	$0, (%r12,%rcx)
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	js	.LBB1_175
# BB#132:                               # %.lr.ph29.i.i.preheader
	xorl	%r13d, %r13d
	movdqa	.LCPI1_5(%rip), %xmm0   # xmm0 = [45,0,45,0,45,0,45,0,45,0,45,0,45,0,45,0]
	packuswb	%xmm0, %xmm0
	movdqa	.LCPI1_4(%rip), %xmm1   # xmm1 = [111,0,111,0,111,0,111,0,111,0,111,0,111,0,111,0]
	packuswb	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB1_133:                              # %.lr.ph29.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_147 Depth 2
                                        #     Child Loop BB1_151 Depth 2
                                        #     Child Loop BB1_154 Depth 2
                                        #     Child Loop BB1_163 Depth 2
                                        #     Child Loop BB1_167 Depth 2
                                        #     Child Loop BB1_170 Depth 2
	movslq	%r11d, %rax
	movq	(%rbp,%rax,8), %rax
	movslq	%r12d, %rcx
	movl	(%rax,%rcx,4), %r10d
	testl	%r10d, %r10d
	js	.LBB1_134
# BB#135:                               #   in Loop: Header=BB1_133 Depth=1
	je	.LBB1_137
# BB#136:                               #   in Loop: Header=BB1_133 Depth=1
	movl	%r11d, %r9d
	subl	%r10d, %r9d
	jmp	.LBB1_138
	.p2align	4, 0x90
.LBB1_134:                              #   in Loop: Header=BB1_133 Depth=1
	leal	-1(%r11), %r9d
	jmp	.LBB1_139
	.p2align	4, 0x90
.LBB1_137:                              #   in Loop: Header=BB1_133 Depth=1
	leal	-1(%r11), %r9d
.LBB1_138:                              #   in Loop: Header=BB1_133 Depth=1
	movl	$-1, %r10d
.LBB1_139:                              #   in Loop: Header=BB1_133 Depth=1
	movl	%r11d, %edi
	subl	%r9d, %edi
	decl	%edi
	movq	%r12, 48(%rsp)          # 8-byte Spill
	je	.LBB1_156
# BB#140:                               # %.lr.ph10.preheader.i.i
                                        #   in Loop: Header=BB1_133 Depth=1
	leal	-2(%r11), %r12d
	subl	%r9d, %r12d
	leaq	1(%r12), %rsi
	cmpq	$16, %rsi
	jae	.LBB1_142
# BB#141:                               #   in Loop: Header=BB1_133 Depth=1
	movq	%r14, %rax
	movq	%r15, %rdx
	jmp	.LBB1_149
	.p2align	4, 0x90
.LBB1_142:                              # %min.iters.checked815
                                        #   in Loop: Header=BB1_133 Depth=1
	movq	%r11, %rcx
	movl	%esi, %r11d
	andl	$15, %r11d
	subq	%r11, %rsi
	je	.LBB1_143
# BB#144:                               # %vector.memcheck828
                                        #   in Loop: Header=BB1_133 Depth=1
	movq	%r15, %rax
	subq	%r12, %rax
	decq	%rax
	cmpq	%r14, %rax
	jae	.LBB1_146
# BB#145:                               # %vector.memcheck828
                                        #   in Loop: Header=BB1_133 Depth=1
	movq	%r14, %rax
	subq	%r12, %rax
	decq	%rax
	cmpq	%r15, %rax
	jae	.LBB1_146
.LBB1_143:                              #   in Loop: Header=BB1_133 Depth=1
	movq	%r14, %rax
	movq	%r15, %rdx
	movq	%rcx, %r11
	jmp	.LBB1_149
.LBB1_146:                              # %vector.body811.preheader
                                        #   in Loop: Header=BB1_133 Depth=1
	subl	%esi, %edi
	leaq	-1(%r11), %rdx
	subq	%r12, %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r15, %rdx
	leaq	-8(%r15), %rbx
	leaq	-8(%r14), %rbp
	.p2align	4, 0x90
.LBB1_147:                              # %vector.body811
                                        #   Parent Loop BB1_133 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm1, (%rbx)
	movq	%xmm1, -8(%rbx)
	movq	%xmm0, (%rbp)
	movq	%xmm0, -8(%rbp)
	addq	$-16, %rbx
	addq	$-16, %rbp
	addq	$-16, %rsi
	jne	.LBB1_147
# BB#148:                               # %middle.block812
                                        #   in Loop: Header=BB1_133 Depth=1
	testq	%r11, %r11
	movq	%rcx, %r11
	movq	40(%rsp), %rbp          # 8-byte Reload
	je	.LBB1_155
	.p2align	4, 0x90
.LBB1_149:                              # %.lr.ph10.i.i.preheader
                                        #   in Loop: Header=BB1_133 Depth=1
	leal	-1(%rdi), %esi
	movl	%edi, %ebx
	andl	$7, %ebx
	je	.LBB1_152
# BB#150:                               # %.lr.ph10.i.i.prol.preheader
                                        #   in Loop: Header=BB1_133 Depth=1
	negl	%ebx
	.p2align	4, 0x90
.LBB1_151:                              # %.lr.ph10.i.i.prol
                                        #   Parent Loop BB1_133 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, -1(%rdx)
	decq	%rdx
	movb	$45, -1(%rax)
	decq	%rax
	decl	%edi
	incl	%ebx
	jne	.LBB1_151
.LBB1_152:                              # %.lr.ph10.i.i.prol.loopexit
                                        #   in Loop: Header=BB1_133 Depth=1
	cmpl	$7, %esi
	jb	.LBB1_155
# BB#153:                               # %.lr.ph10.i.i.preheader.new
                                        #   in Loop: Header=BB1_133 Depth=1
	decq	%rax
	decq	%rdx
	.p2align	4, 0x90
.LBB1_154:                              # %.lr.ph10.i.i
                                        #   Parent Loop BB1_133 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, (%rdx)
	movb	$45, (%rax)
	movb	$111, -1(%rdx)
	movb	$45, -1(%rax)
	movb	$111, -2(%rdx)
	movb	$45, -2(%rax)
	movb	$111, -3(%rdx)
	movb	$45, -3(%rax)
	movb	$111, -4(%rdx)
	movb	$45, -4(%rax)
	movb	$111, -5(%rdx)
	movb	$45, -5(%rax)
	movb	$111, -6(%rdx)
	movb	$45, -6(%rax)
	movb	$111, -7(%rdx)
	movb	$45, -7(%rax)
	addq	$-8, %rax
	addq	$-8, %rdx
	addl	$-8, %edi
	jne	.LBB1_154
.LBB1_155:                              # %._crit_edge11.loopexit.i.i
                                        #   in Loop: Header=BB1_133 Depth=1
	subq	%r12, %r14
	subq	%r12, %r15
	decq	%r15
	decq	%r14
	leal	-1(%r13,%r11), %r13d
	subl	%r9d, %r13d
.LBB1_156:                              # %._crit_edge11.i.i
                                        #   in Loop: Header=BB1_133 Depth=1
	cmpl	$-1, %r10d
	je	.LBB1_157
# BB#158:                               # %.lr.ph18.preheader.i.i
                                        #   in Loop: Header=BB1_133 Depth=1
	movq	%r11, %r8
	movl	%r10d, %r12d
	notl	%r12d
	movl	$-2, %ebp
	subl	%r10d, %ebp
	leaq	1(%rbp), %rbx
	cmpq	$16, %rbx
	movl	%r12d, %eax
	movq	%r14, %rdx
	movq	%r15, %rsi
	jb	.LBB1_165
# BB#159:                               # %min.iters.checked778
                                        #   in Loop: Header=BB1_133 Depth=1
	movl	%ebx, %r11d
	andl	$15, %r11d
	subq	%r11, %rbx
	movl	%r12d, %eax
	movq	%r14, %rdx
	movq	%r15, %rsi
	je	.LBB1_165
# BB#160:                               # %vector.memcheck791
                                        #   in Loop: Header=BB1_133 Depth=1
	movq	%r15, %rax
	subq	%rbp, %rax
	decq	%rax
	cmpq	%r14, %rax
	jae	.LBB1_162
# BB#161:                               # %vector.memcheck791
                                        #   in Loop: Header=BB1_133 Depth=1
	movq	%r14, %rax
	subq	%rbp, %rax
	decq	%rax
	cmpq	%r15, %rax
	movl	%r12d, %eax
	movq	%r14, %rdx
	movq	%r15, %rsi
	jb	.LBB1_165
.LBB1_162:                              # %vector.body774.preheader
                                        #   in Loop: Header=BB1_133 Depth=1
	movl	%r12d, %eax
	subl	%ebx, %eax
	leaq	-1(%r11), %rsi
	subq	%rbp, %rsi
	leaq	(%r14,%rsi), %rdx
	addq	%r15, %rsi
	leaq	-8(%r15), %rdi
	leaq	-8(%r14), %rcx
	.p2align	4, 0x90
.LBB1_163:                              # %vector.body774
                                        #   Parent Loop BB1_133 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm0, (%rdi)
	movq	%xmm0, -8(%rdi)
	movq	%xmm1, (%rcx)
	movq	%xmm1, -8(%rcx)
	addq	$-16, %rdi
	addq	$-16, %rcx
	addq	$-16, %rbx
	jne	.LBB1_163
# BB#164:                               # %middle.block775
                                        #   in Loop: Header=BB1_133 Depth=1
	testq	%r11, %r11
	je	.LBB1_171
	.p2align	4, 0x90
.LBB1_165:                              # %.lr.ph18.i.i.preheader
                                        #   in Loop: Header=BB1_133 Depth=1
	leal	-1(%rax), %r11d
	movl	%eax, %ebx
	andl	$7, %ebx
	je	.LBB1_168
# BB#166:                               # %.lr.ph18.i.i.prol.preheader
                                        #   in Loop: Header=BB1_133 Depth=1
	negl	%ebx
	.p2align	4, 0x90
.LBB1_167:                              # %.lr.ph18.i.i.prol
                                        #   Parent Loop BB1_133 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, -1(%rsi)
	decq	%rsi
	movb	$111, -1(%rdx)
	decq	%rdx
	decl	%eax
	incl	%ebx
	jne	.LBB1_167
.LBB1_168:                              # %.lr.ph18.i.i.prol.loopexit
                                        #   in Loop: Header=BB1_133 Depth=1
	cmpl	$7, %r11d
	jb	.LBB1_171
# BB#169:                               # %.lr.ph18.i.i.preheader.new
                                        #   in Loop: Header=BB1_133 Depth=1
	decq	%rdx
	decq	%rsi
	.p2align	4, 0x90
.LBB1_170:                              # %.lr.ph18.i.i
                                        #   Parent Loop BB1_133 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, (%rsi)
	movb	$111, (%rdx)
	movb	$45, -1(%rsi)
	movb	$111, -1(%rdx)
	movb	$45, -2(%rsi)
	movb	$111, -2(%rdx)
	movb	$45, -3(%rsi)
	movb	$111, -3(%rdx)
	movb	$45, -4(%rsi)
	movb	$111, -4(%rdx)
	movb	$45, -5(%rsi)
	movb	$111, -5(%rdx)
	movb	$45, -6(%rsi)
	movb	$111, -6(%rdx)
	movb	$45, -7(%rsi)
	movb	$111, -7(%rdx)
	addq	$-8, %rdx
	addq	$-8, %rsi
	addl	$-8, %eax
	jne	.LBB1_170
.LBB1_171:                              # %._crit_edge19.loopexit.i.i
                                        #   in Loop: Header=BB1_133 Depth=1
	subq	%rbp, %r14
	subq	%rbp, %r15
	decq	%r15
	decq	%r14
	addl	%r12d, %r13d
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	%r8, %r11
	movq	40(%rsp), %rbp          # 8-byte Reload
	testl	%r11d, %r11d
	jg	.LBB1_173
	jmp	.LBB1_175
	.p2align	4, 0x90
.LBB1_157:                              #   in Loop: Header=BB1_133 Depth=1
	movq	48(%rsp), %r12          # 8-byte Reload
	testl	%r11d, %r11d
	jle	.LBB1_175
.LBB1_173:                              # %._crit_edge19.i.i
                                        #   in Loop: Header=BB1_133 Depth=1
	testl	%r12d, %r12d
	jle	.LBB1_175
# BB#174:                               #   in Loop: Header=BB1_133 Depth=1
	addl	%r10d, %r12d
	movb	$111, -1(%r15)
	decq	%r15
	movb	$111, -1(%r14)
	decq	%r14
	addl	$2, %r13d
	cmpl	24(%rsp), %r13d         # 4-byte Folded Reload
	movl	%r9d, %r11d
	jle	.LBB1_133
.LBB1_175:                              # %._crit_edge30.i.i
	movl	36(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movq	304(%rsp), %rbx         # 8-byte Reload
	movq	88(%rsp), %r12          # 8-byte Reload
	jle	.LBB1_178
# BB#176:                               # %.lr.ph5.i.i
	movl	%eax, %r13d
	movq	216(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_177:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movq	(%rbx), %rsi
	addq	%r12, %rsi
	movq	%r15, %rdx
	callq	gapireru
	addq	$8, %rbx
	addq	$8, %rbp
	decq	%r13
	jne	.LBB1_177
.LBB1_178:                              # %.preheader.i3.i
	movl	12(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movq	232(%rsp), %r15         # 8-byte Reload
	movq	312(%rsp), %rbx         # 8-byte Reload
	movq	104(%rsp), %r12         # 8-byte Reload
	jle	.LBB1_181
# BB#179:                               # %.lr.ph.i4.i
	movl	%eax, %r13d
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB1_180:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movq	(%rbx), %rsi
	addq	%r12, %rsi
	movq	%r14, %rdx
	callq	gapireru
	addq	$8, %rbx
	addq	$8, %rbp
	decq	%r13
	jne	.LBB1_180
.LBB1_181:                              # %MSalignmm_tanni.exit
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	208(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	168(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	FreeFloatVec
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatVec
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	FreeIntVec
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatMtx
	movq	120(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntMtx
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	FreeIntMtx
	movq	216(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	%r15, %rdi
	jmp	.LBB1_401
.LBB1_6:                                # %.lr.ph55.split.us.preheader
	movl	12(%rsp), %eax          # 4-byte Reload
	leaq	-1(%rax), %rcx
	movq	%rax, %rsi
	andq	$7, %rsi
	movq	712(%rsp), %rbp
	je	.LBB1_7
# BB#8:                                 # %.lr.ph55.split.us.prol.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph55.split.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rdx,8), %rdi
	movb	$0, (%rdi)
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB1_9
	jmp	.LBB1_10
.LBB1_30:                               # %vector.body.preheader
	leaq	16(%r8), %rsi
	leaq	16(%r15), %rdi
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB1_31:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-4, %rbp
	jne	.LBB1_31
# BB#32:                                # %middle.block
	testl	%edx, %edx
	movl	12(%rsp), %r14d         # 4-byte Reload
	jne	.LBB1_21
	jmp	.LBB1_33
.LBB1_45:                               # %vector.body319.preheader
	leaq	16(%rdi), %rsi
	leaq	16(%rax), %rdi
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB1_46:                               # %vector.body319
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-4, %rbp
	jne	.LBB1_46
# BB#47:                                # %middle.block320
	testl	%edx, %edx
	jne	.LBB1_36
	jmp	.LBB1_48
.LBB1_213:
	movl	$1, %esi
.LBB1_214:                              # %scalar.ph428.preheader
	movl	%eax, %edi
	subl	%esi, %edi
	leaq	-1(%rax), %rcx
	testb	$1, %dil
	movq	%rsi, %rdi
	je	.LBB1_216
# BB#215:                               # %scalar.ph428.prol
	movss	-4(%r13,%rsi,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movq	24(%rsp), %rdi          # 8-byte Reload
	movss	%xmm0, (%rdi,%rsi,4)
	movq	136(%rsp), %rdi         # 8-byte Reload
	movl	$0, (%rdi,%rsi,4)
	leaq	1(%rsi), %rdi
.LBB1_216:                              # %scalar.ph428.prol.loopexit
	cmpq	%rsi, %rcx
	je	.LBB1_225
# BB#217:                               # %scalar.ph428.preheader.new
	subq	%rdi, %rax
	leaq	(%r13,%rdi,4), %rcx
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	4(%rsi,%rdi,4), %rsi
	movq	136(%rsp), %rbp         # 8-byte Reload
	leaq	4(%rbp,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB1_218:                              # %scalar.ph428
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movss	%xmm0, -4(%rsi)
	movl	$0, -4(%rdi)
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rsi)
	movl	$0, (%rdi)
	addq	$8, %rcx
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rax
	jne	.LBB1_218
.LBB1_225:                              # %._crit_edge138
	movq	%r14, 336(%rsp)         # 8-byte Spill
	movslq	%r15d, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	(%r13,%rax,4), %eax
	movq	392(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r10d, %xmm0
	mulsd	.LCPI1_0(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	movq	384(%rsp), %rsi         # 8-byte Reload
	movq	120(%rsp), %r8          # 8-byte Reload
	jle	.LBB1_252
# BB#226:                               # %.lr.ph132
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	leal	1(%rax), %eax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movl	192(%rsp), %r15d        # 4-byte Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	leaq	8(%rcx,%rax,4), %r11
	addq	$-2, %r15
	movl	$1, %r12d
	movq	288(%rsp), %rax         # 8-byte Reload
	movq	384(%rsp), %rsi         # 8-byte Reload
	movq	344(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_227:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_228 Depth 2
                                        #       Child Loop BB1_229 Depth 3
                                        #     Child Loop BB1_232 Depth 2
                                        #       Child Loop BB1_234 Depth 3
                                        #     Child Loop BB1_241 Depth 2
                                        #     Child Loop BB1_246 Depth 2
	movq	%rsi, %r10
	movq	%rax, %rcx
	leaq	-1(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	-4(%rax,%r12,4), %eax
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%eax, (%rcx)
	movq	408(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r12,8), %rax
	addq	$4, %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_228:                              #   Parent Loop BB1_227 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_229 Depth 3
	movl	$0, 512(%rsp,%rcx,4)
	xorpd	%xmm0, %xmm0
	movq	$-2704, %rdx            # imm = 0xF570
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB1_229:                              #   Parent Loop BB1_227 Depth=1
                                        #     Parent Loop BB1_228 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm1, %xmm1
	cvtsi2ssl	n_dis+2704(%rdx,%rcx,4), %xmm1
	mulss	-4(%rsi), %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	n_dis+2808(%rdx,%rcx,4), %xmm0
	mulss	(%rsi), %xmm0
	addss	%xmm1, %xmm0
	addq	$8, %rsi
	addq	$208, %rdx
	jne	.LBB1_229
# BB#230:                               #   in Loop: Header=BB1_228 Depth=2
	movss	%xmm0, 512(%rsp,%rcx,4)
	incq	%rcx
	cmpq	$26, %rcx
	jne	.LBB1_228
# BB#231:                               # %.preheader.i5
                                        #   in Loop: Header=BB1_227 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	movl	%eax, %ebp
	movq	352(%rsp), %rbx         # 8-byte Reload
	movq	360(%rsp), %rax         # 8-byte Reload
	movq	%r10, %rcx
	movq	128(%rsp), %r9          # 8-byte Reload
	je	.LBB1_236
	.p2align	4, 0x90
.LBB1_232:                              # %.lr.ph85.i
                                        #   Parent Loop BB1_227 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_234 Depth 3
	decl	%ebp
	movl	$0, (%rcx)
	movq	(%rbx), %rsi
	movl	(%rsi), %edi
	testl	%edi, %edi
	js	.LBB1_235
# BB#233:                               # %.lr.ph.i7
                                        #   in Loop: Header=BB1_232 Depth=2
	movq	(%rax), %rdx
	addq	$4, %rsi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_234:                              #   Parent Loop BB1_227 Depth=1
                                        #     Parent Loop BB1_232 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%edi, %rdi
	movss	512(%rsp,%rdi,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdx), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rcx)
	movl	(%rsi), %edi
	addq	$4, %rsi
	addq	$4, %rdx
	testl	%edi, %edi
	jns	.LBB1_234
.LBB1_235:                              # %._crit_edge.i10
                                        #   in Loop: Header=BB1_232 Depth=2
	addq	$4, %rcx
	addq	$8, %rbx
	addq	$8, %rax
	testl	%ebp, %ebp
	jne	.LBB1_232
.LBB1_236:                              # %match_calc.exit
                                        #   in Loop: Header=BB1_227 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r12,4), %eax
	movl	%eax, (%r10)
	movq	80(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx,%r12,4), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	cmpq	144(%rsp), %r12         # 8-byte Folded Reload
	jne	.LBB1_238
# BB#237:                               #   in Loop: Header=BB1_227 Depth=1
	movl	%eax, (%r8)
.LBB1_238:                              #   in Loop: Header=BB1_227 Depth=1
	cmpl	$2, 192(%rsp)           # 4-byte Folded Reload
	movq	200(%rsp), %rcx         # 8-byte Reload
	movq	400(%rsp), %r13         # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	jl	.LBB1_251
# BB#239:                               # %.lr.ph126
                                        #   in Loop: Header=BB1_227 Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movq	336(%rsp), %rax         # 8-byte Reload
	movss	4(%rax), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	addss	%xmm2, %xmm0
	cmpq	144(%rsp), %r12         # 8-byte Folded Reload
	jne	.LBB1_240
# BB#245:                               # %.lr.ph126.split.us.preheader
                                        #   in Loop: Header=BB1_227 Depth=1
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	jmp	.LBB1_246
	.p2align	4, 0x90
.LBB1_250:                              # %..lr.ph126.split.us_crit_edge
                                        #   in Loop: Header=BB1_246 Depth=2
	movss	4(%rsi,%rbp,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	(%r11,%rbp,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	incq	%rbp
.LBB1_246:                              # %.lr.ph126.split.us
                                        #   Parent Loop BB1_227 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movss	(%r9,%rbp,4), %xmm3     # xmm3 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm3
	maxss	%xmm1, %xmm3
	addss	%xmm1, %xmm2
	ucomiss	%xmm0, %xmm2
	movaps	%xmm0, %xmm4
	cmpnless	%xmm2, %xmm4
	movaps	%xmm4, %xmm5
	andnps	%xmm2, %xmm5
	andps	%xmm0, %xmm4
	orps	%xmm5, %xmm4
	movaps	%xmm4, %xmm0
	cmovael	%ebp, %ebx
	movss	4(%rax,%rbp,4), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	-4(%rcx,%r12,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm2
	maxss	%xmm3, %xmm2
	addss	(%rdx,%r12,4), %xmm1
	ucomiss	%xmm4, %xmm1
	jae	.LBB1_248
# BB#247:                               # %.lr.ph126.split.us._crit_edge
                                        #   in Loop: Header=BB1_246 Depth=2
	movq	%rax, %r9
	movq	136(%rsp), %rax         # 8-byte Reload
	movl	4(%rax,%rbp,4), %eax
	jmp	.LBB1_249
	.p2align	4, 0x90
.LBB1_248:                              #   in Loop: Header=BB1_246 Depth=2
	movss	%xmm1, 4(%rax,%rbp,4)
	movq	136(%rsp), %rsi         # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%edi, 4(%rsi,%rbp,4)
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rax, %r9
	movl	%edi, %eax
.LBB1_249:                              #   in Loop: Header=BB1_246 Depth=2
	movq	%r10, %rdi
	addss	4(%rdi,%rbp,4), %xmm2
	movss	%xmm2, 4(%rdi,%rbp,4)
	movq	208(%rsp), %r8          # 8-byte Reload
	movl	%eax, 4(%r8,%rbp,4)
	movl	%ebx, 4(%r13,%rbp,4)
	movq	56(%rsp), %rax          # 8-byte Reload
	movss	%xmm2, 4(%rax,%rbp,4)
	movl	4(%r9,%rbp,4), %eax
	movq	120(%rsp), %r8          # 8-byte Reload
	movl	%eax, 4(%r8,%rbp,4)
	movss	%xmm0, 4(%r14,%rbp,4)
	cmpq	%rbp, %r15
	movq	128(%rsp), %r9          # 8-byte Reload
	jne	.LBB1_250
	jmp	.LBB1_251
	.p2align	4, 0x90
.LBB1_240:                              # %.lr.ph126.split.preheader
                                        #   in Loop: Header=BB1_227 Depth=1
	xorl	%eax, %eax
	jmp	.LBB1_241
	.p2align	4, 0x90
.LBB1_244:                              # %..lr.ph126.split_crit_edge
                                        #   in Loop: Header=BB1_241 Depth=2
	addss	%xmm2, %xmm1
	movaps	%xmm0, %xmm3
	cmpless	%xmm1, %xmm3
	andps	%xmm3, %xmm1
	andnps	%xmm0, %xmm3
	orps	%xmm1, %xmm3
	movss	4(%rsi,%rax,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	(%r11,%rax,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	incq	%rax
	movaps	%xmm3, %xmm0
.LBB1_241:                              # %.lr.ph126.split
                                        #   Parent Loop BB1_227 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%r9,%rax,4), %xmm4     # xmm4 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	maxss	%xmm1, %xmm4
	movq	24(%rsp), %rsi          # 8-byte Reload
	movss	4(%rsi,%rax,4), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movss	-4(%rcx,%r12,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm3
	maxss	%xmm4, %xmm3
	movss	(%rdx,%r12,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm4
	ucomiss	%xmm5, %xmm4
	jb	.LBB1_243
# BB#242:                               #   in Loop: Header=BB1_241 Depth=2
	movq	24(%rsp), %rsi          # 8-byte Reload
	movss	%xmm4, 4(%rsi,%rax,4)
	movq	136(%rsp), %rsi         # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%edi, 4(%rsi,%rax,4)
.LBB1_243:                              #   in Loop: Header=BB1_241 Depth=2
	movq	%r10, %rsi
	addss	4(%rsi,%rax,4), %xmm3
	movss	%xmm3, 4(%rsi,%rax,4)
	cmpq	%rax, %r15
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB1_244
.LBB1_251:                              # %._crit_edge127
                                        #   in Loop: Header=BB1_227 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%r10, %r13
	movl	(%r13,%rax,4), %eax
	movq	392(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx,%r12,4)
	incq	%r12
	cmpq	264(%rsp), %r12         # 8-byte Folded Reload
	movq	%r13, %rax
	jne	.LBB1_227
.LBB1_252:                              # %._crit_edge133
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	256(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rsi
	movq	408(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	160(%rsp), %r8          # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	360(%rsp), %r14         # 8-byte Reload
	movq	%r14, %r9
	pushq	$1
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	movq	360(%rsp), %rbp         # 8-byte Reload
	pushq	%rbp
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi49:
	.cfi_adjust_cfa_offset -16
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	176(%rsp), %r15         # 8-byte Reload
	movl	%r15d, %ecx
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%r14, %r9
	pushq	$1
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi52:
	.cfi_adjust_cfa_offset -16
	testl	%r15d, %r15d
	movq	136(%rsp), %r10         # 8-byte Reload
	movq	184(%rsp), %r11         # 8-byte Reload
	jle	.LBB1_267
# BB#253:                               # %.lr.ph114
	movslq	%r15d, %rax
	movq	200(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,4), %rax
	movl	%r15d, %ecx
	cmpl	$7, %r15d
	jbe	.LBB1_254
# BB#260:                               # %min.iters.checked469
	movl	%r15d, %r9d
	andl	$7, %r9d
	movq	%rcx, %r8
	subq	%r9, %r8
	je	.LBB1_254
# BB#261:                               # %vector.memcheck490
	movq	%r11, %r14
	movq	96(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rcx,4), %rsi
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	168(%rsp), %rbx         # 8-byte Reload
	leaq	4(%rbx,%rdx,4), %r11
	leaq	(%rdx,%rcx), %rbp
	leaq	4(%rbx,%rbp,4), %rbp
	cmpq	%rax, %rdi
	sbbb	%bl, %bl
	cmpq	%rsi, %rax
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%rbp, %rdi
	sbbb	%bl, %bl
	cmpq	%rsi, %r11
	sbbb	%sil, %sil
	xorl	%edi, %edi
	testb	$1, %dl
	jne	.LBB1_262
# BB#263:                               # %vector.memcheck490
	andb	%sil, %bl
	andb	$1, %bl
	movq	%r14, %r11
	jne	.LBB1_255
# BB#264:                               # %vector.body465.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	168(%rsp), %rsi         # 8-byte Reload
	leaq	20(%rsi,%rdx,4), %rdi
	movq	96(%rsp), %rdx          # 8-byte Reload
	leaq	16(%rdx), %rbp
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB1_265:                              # %vector.body465
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi), %xmm1
	movups	(%rdi), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %rsi
	jne	.LBB1_265
# BB#266:                               # %middle.block466
	testl	%r9d, %r9d
	movq	%r8, %rdi
	jne	.LBB1_255
	jmp	.LBB1_267
.LBB1_254:
	xorl	%edi, %edi
.LBB1_255:                              # %scalar.ph467.preheader
	movl	%ecx, %esi
	subl	%edi, %esi
	leaq	-1(%rcx), %rdx
	testb	$1, %sil
	movq	%rdi, %rsi
	je	.LBB1_257
# BB#256:                               # %scalar.ph467.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	leaq	1(%rdi), %rsi
	movq	80(%rsp), %rbp          # 8-byte Reload
	addss	4(%rbp,%rdi,4), %xmm0
	movq	96(%rsp), %rbp          # 8-byte Reload
	addss	(%rbp,%rdi,4), %xmm0
	movss	%xmm0, (%rbp,%rdi,4)
.LBB1_257:                              # %scalar.ph467.prol.loopexit
	cmpq	%rdi, %rdx
	je	.LBB1_267
# BB#258:                               # %scalar.ph467.preheader.new
	subq	%rsi, %rcx
	movq	88(%rsp), %rdx          # 8-byte Reload
	leaq	(%rsi,%rdx), %rdx
	movq	168(%rsp), %rdi         # 8-byte Reload
	leaq	8(%rdi,%rdx,4), %rdx
	movq	96(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdi,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB1_259:                              # %scalar.ph467
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB1_259
.LBB1_267:                              # %.preheader39
	movl	720(%rsp), %eax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	64(%rsp), %rdx          # 8-byte Reload
	testl	%edx, %edx
	movq	120(%rsp), %r14         # 8-byte Reload
	movq	(%rsp), %rbx            # 8-byte Reload
	jle	.LBB1_283
# BB#268:                               # %.lr.ph111
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,4), %rax
	movl	%edx, %ecx
	cmpl	$7, %edx
	jbe	.LBB1_269
# BB#275:                               # %min.iters.checked508
	movl	%edx, %r9d
	andl	$7, %r9d
	movq	%rcx, %r8
	subq	%r9, %r8
	je	.LBB1_269
# BB#276:                               # %vector.memcheck530
	leaq	(%r13,%rcx,4), %rsi
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	112(%rsp), %rbx         # 8-byte Reload
	leaq	4(%rbx,%rdx,4), %rdi
	leaq	(%rdx,%rcx), %rbp
	leaq	4(%rbx,%rbp,4), %rbp
	cmpq	%rax, %r13
	sbbb	%bl, %bl
	cmpq	%rsi, %rax
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%rbp, %r13
	sbbb	%bl, %bl
	cmpq	%rsi, %rdi
	sbbb	%sil, %sil
	xorl	%edi, %edi
	testb	$1, %dl
	jne	.LBB1_277
# BB#278:                               # %vector.memcheck530
	andb	%sil, %bl
	andb	$1, %bl
	movq	(%rsp), %rbx            # 8-byte Reload
	jne	.LBB1_270
# BB#279:                               # %vector.body504.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
	leaq	20(%rsi,%rdx,4), %rdi
	leaq	16(%r13), %rbp
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB1_280:                              # %vector.body504
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi), %xmm1
	movups	(%rdi), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %rsi
	jne	.LBB1_280
# BB#281:                               # %middle.block505
	testl	%r9d, %r9d
	movq	%r8, %rdi
	jne	.LBB1_270
	jmp	.LBB1_282
.LBB1_269:
	xorl	%edi, %edi
.LBB1_270:                              # %scalar.ph506.preheader
	movl	%ecx, %esi
	subl	%edi, %esi
	leaq	-1(%rcx), %rdx
	testb	$1, %sil
	movq	%rdi, %rsi
	je	.LBB1_272
# BB#271:                               # %scalar.ph506.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	leaq	1(%rdi), %rsi
	movq	336(%rsp), %rbp         # 8-byte Reload
	addss	4(%rbp,%rdi,4), %xmm0
	addss	(%r13,%rdi,4), %xmm0
	movss	%xmm0, (%r13,%rdi,4)
.LBB1_272:                              # %scalar.ph506.prol.loopexit
	cmpq	%rdi, %rdx
	je	.LBB1_282
# BB#273:                               # %scalar.ph506.preheader.new
	subq	%rsi, %rcx
	movq	104(%rsp), %rdx         # 8-byte Reload
	leaq	(%rsi,%rdx), %rdx
	movq	112(%rsp), %rdi         # 8-byte Reload
	leaq	8(%rdi,%rdx,4), %rdx
	leaq	4(%r13,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB1_274:                              # %scalar.ph506
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB1_274
.LBB1_282:                              # %.preheader38
	movq	64(%rsp), %rdx          # 8-byte Reload
	testl	%edx, %edx
	movq	120(%rsp), %r14         # 8-byte Reload
	jle	.LBB1_283
# BB#287:                               # %.lr.ph109
	movq	%r11, %r12
	leal	-1(%rdx), %r11d
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	-4(%rax,%rbx,4), %rdi
	movq	%rbx, %rsi
	notq	%rsi
	cmpq	$-3, %rsi
	movq	$-2, %rax
	cmovgq	%rsi, %rax
	leaq	2(%rax,%rbx), %r9
	cmpq	$3, %r9
	movq	%rbx, %rdx
	movq	%rbx, %rbp
	movq	%rdi, 240(%rsp)         # 8-byte Spill
	jbe	.LBB1_288
# BB#292:                               # %min.iters.checked549
	movq	%r9, %r8
	andq	$-4, %r8
	movq	%rbp, %rdx
	je	.LBB1_288
# BB#293:                               # %vector.memcheck573
	cmpq	$-3, %rsi
	movq	$-2, %rcx
	cmovleq	%rcx, %rsi
	subq	%rsi, %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,4), %rcx
	leaq	(%rax,%rbp,4), %rdx
	notq	%rsi
	leaq	(%r13,%rsi,4), %rsi
	leaq	4(%r13,%rbp,4), %rdi
	cmpq	%rdi, %rcx
	movq	240(%rsp), %rdi         # 8-byte Reload
	sbbb	%bl, %bl
	cmpq	%rdx, %rsi
	sbbb	%al, %al
	andb	%bl, %al
	cmpq	%rdi, %rcx
	sbbb	%bl, %bl
	cmpq	%rdx, %rdi
	sbbb	%cl, %cl
	testb	$1, %al
	movq	%rbp, %rdx
	jne	.LBB1_288
# BB#294:                               # %vector.memcheck573
	andb	%cl, %bl
	andb	$1, %bl
	movq	%rbp, %rdx
	jne	.LBB1_288
# BB#295:                               # %vector.ph574
	movd	%r15d, %xmm1
	leaq	-4(%r8), %rax
	movq	%rax, %rsi
	shrq	$2, %rsi
	btl	$2, %eax
	jb	.LBB1_296
# BB#297:                               # %vector.body544.prol
	movq	(%rsp), %rcx            # 8-byte Reload
	movups	-12(%r13,%rcx,4), %xmm0
	shufps	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	addps	%xmm0, %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	movq	24(%rsp), %rax          # 8-byte Reload
	movups	%xmm2, -16(%rax,%rcx,4)
	pshufd	$0, %xmm1, %xmm0        # xmm0 = xmm1[0,0,0,0]
	movdqu	%xmm0, -12(%r10,%rcx,4)
	movl	$4, %edx
	testq	%rsi, %rsi
	jne	.LBB1_299
	jmp	.LBB1_301
.LBB1_283:                              # %.preheader38..preheader36_crit_edge
	leal	-1(%rdx), %ecx
	movslq	%edx, %rax
	movq	128(%rsp), %r9          # 8-byte Reload
	leaq	-4(%r9,%rax,4), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movq	224(%rsp), %rdi         # 8-byte Reload
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	jmp	.LBB1_284
.LBB1_7:
	xorl	%edx, %edx
.LBB1_10:                               # %.lr.ph55.split.us.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB1_402
# BB#11:                                # %.lr.ph55.split.us.preheader.new
	subq	%rdx, %rax
	leaq	56(%rbp,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph55.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-48(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-40(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-32(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-24(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-16(%rcx), %rdx
	movb	$0, (%rdx)
	movq	-8(%rcx), %rdx
	movb	$0, (%rdx)
	movq	(%rcx), %rdx
	movb	$0, (%rdx)
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB1_12
	jmp	.LBB1_402
.LBB1_262:
	movq	%r14, %r11
	jmp	.LBB1_255
.LBB1_277:
	movq	(%rsp), %rbx            # 8-byte Reload
	jmp	.LBB1_270
.LBB1_192:
	movq	80(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB1_185
.LBB1_60:
	movq	16(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB1_53
.LBB1_75:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	144(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB1_68
.LBB1_296:
	xorl	%edx, %edx
	movq	24(%rsp), %rax          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB1_301
.LBB1_299:                              # %vector.ph574.new
	movq	240(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movq	%r8, %rsi
	negq	%rsi
	negq	%rdx
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	-12(%r13,%rcx,4), %rdi
	leaq	-16(%rax,%rcx,4), %rbp
	leaq	-12(%r10,%rcx,4), %rbx
	.p2align	4, 0x90
.LBB1_300:                              # %vector.body544
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx,4), %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	addps	%xmm0, %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	movups	%xmm2, (%rbp,%rdx,4)
	movdqu	%xmm1, (%rbx,%rdx,4)
	movups	-16(%rdi,%rdx,4), %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	addps	%xmm0, %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	movups	%xmm2, -16(%rbp,%rdx,4)
	movdqu	%xmm1, -16(%rbx,%rdx,4)
	addq	$-8, %rdx
	cmpq	%rdx, %rsi
	jne	.LBB1_300
.LBB1_301:                              # %middle.block545
	cmpq	%r8, %r9
	jne	.LBB1_303
# BB#302:
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	224(%rsp), %rdi         # 8-byte Reload
	movq	56(%rsp), %r8           # 8-byte Reload
	jmp	.LBB1_291
.LBB1_303:
	movq	(%rsp), %rdx            # 8-byte Reload
	subq	%r8, %rdx
	movq	240(%rsp), %rdi         # 8-byte Reload
.LBB1_288:                              # %scalar.ph546.preheader
	incq	%rdx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_289:                              # %scalar.ph546
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%r13,%rdx,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rdi), %xmm0
	movss	%xmm0, -8(%rax,%rdx,4)
	movl	%r15d, -4(%r10,%rdx,4)
	decq	%rdx
	cmpq	$1, %rdx
	jg	.LBB1_289
# BB#290:
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	224(%rsp), %rdi         # 8-byte Reload
.LBB1_291:                              # %.preheader36
	movq	120(%rsp), %r14         # 8-byte Reload
	movq	128(%rsp), %r9          # 8-byte Reload
	movl	%r11d, %ecx
	movq	%r12, %r11
.LBB1_284:                              # %.preheader36
	movq	296(%rsp), %rbx         # 8-byte Reload
	incl	%ebx
	movq	%rbx, 296(%rsp)         # 8-byte Spill
	movslq	48(%rsp), %rsi          # 4-byte Folded Reload
	leaq	-8(%rax,%rsi,4), %r15
	leaq	(%r10,%rsi,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %r12d
	leal	-2(%rdx), %eax
	movl	%ecx, 420(%rsp)         # 4-byte Spill
	subl	%ecx, %eax
	movl	%eax, 276(%rsp)         # 4-byte Spill
	movq	192(%rsp), %rbx         # 8-byte Reload
	movslq	%ebx, %rax
	movq	%rax, 464(%rsp)         # 8-byte Spill
	movl	%edx, %eax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	leaq	-3(%rax), %rax
	movq	%rax, 456(%rsp)         # 8-byte Spill
	andl	$3, %ebx
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	movq	112(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	leaq	(%r14,%rcx,4), %rbp
	movq	%rbp, 224(%rsp)         # 8-byte Spill
	leaq	-4(%r11,%rax,4), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	leaq	-4(%r8,%rcx,4), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	%rsi, 488(%rsp)         # 8-byte Spill
	leaq	-2(%rsi), %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	leaq	-4(%r9,%rcx,4), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	%r12, %r11
	movq	%rdx, %rbp
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	movq	%rbx, %rax
	movq	%rcx, %r12
	negq	%rax
	movq	%rax, 448(%rsp)         # 8-byte Spill
	movss	.LCPI1_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movq	376(%rsp), %rax         # 8-byte Reload
	leaq	-4(%rax,%r12,4), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	368(%rsp), %rax         # 8-byte Reload
	leaq	-4(%rax,%r12,4), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	344(%rsp), %rax         # 8-byte Reload
	leaq	-4(%rax,%r12,4), %rax
	movq	%rax, 504(%rsp)         # 8-byte Spill
	movq	336(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12,4), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	xorl	%r9d, %r9d
	movl	$0, 156(%rsp)           # 4-byte Folded Spill
	xorl	%ebx, %ebx
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%r11, 248(%rsp)         # 8-byte Spill
	movq	%rbp, 472(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_285:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_305 Depth 2
                                        #       Child Loop BB1_306 Depth 3
                                        #     Child Loop BB1_309 Depth 2
                                        #       Child Loop BB1_311 Depth 3
                                        #     Child Loop BB1_318 Depth 2
                                        #     Child Loop BB1_325 Depth 2
                                        #     Child Loop BB1_342 Depth 2
                                        #     Child Loop BB1_403 Depth 2
                                        #     Child Loop BB1_346 Depth 2
	movl	%ebx, 40(%rsp)          # 4-byte Spill
	movaps	%xmm0, %xmm1
	movq	%r13, %r10
	movq	%rax, %r13
	movq	%rdi, %r11
	testq	%r11, %r11
	jle	.LBB1_286
# BB#304:                               #   in Loop: Header=BB1_285 Depth=1
	leaq	-1(%r11), %r8
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r11,4), %eax
	movl	%eax, (%r10,%r12,4)
	movq	408(%rsp), %rax         # 8-byte Reload
	movq	-8(%rax,%r11,8), %rdx
	addq	$4, %rdx
	xorl	%esi, %esi
	movq	64(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_305:                              #   Parent Loop BB1_285 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_306 Depth 3
	movl	$0, 512(%rsp,%rsi,4)
	xorps	%xmm0, %xmm0
	movq	$-2704, %rax            # imm = 0xF570
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB1_306:                              #   Parent Loop BB1_285 Depth=1
                                        #     Parent Loop BB1_305 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm2, %xmm2
	cvtsi2ssl	n_dis+2704(%rax,%rsi,4), %xmm2
	mulss	-4(%rcx), %xmm2
	addss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	cvtsi2ssl	n_dis+2808(%rax,%rsi,4), %xmm0
	mulss	(%rcx), %xmm0
	addss	%xmm2, %xmm0
	addq	$8, %rcx
	addq	$208, %rax
	jne	.LBB1_306
# BB#307:                               #   in Loop: Header=BB1_305 Depth=2
	movss	%xmm0, 512(%rsp,%rsi,4)
	incq	%rsi
	cmpq	$26, %rsi
	jne	.LBB1_305
# BB#308:                               # %.preheader.i11
                                        #   in Loop: Header=BB1_285 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	movl	%eax, %edx
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	360(%rsp), %rdi         # 8-byte Reload
	movq	%r13, %rbx
	movq	%rbp, %r14
	je	.LBB1_313
	.p2align	4, 0x90
.LBB1_309:                              # %.lr.ph85.i22
                                        #   Parent Loop BB1_285 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_311 Depth 3
	decl	%edx
	movl	$0, (%rbx)
	movq	(%rsi), %rbp
	movl	(%rbp), %ecx
	testl	%ecx, %ecx
	js	.LBB1_312
# BB#310:                               # %.lr.ph.i23
                                        #   in Loop: Header=BB1_309 Depth=2
	movq	(%rdi), %rax
	addq	$4, %rbp
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_311:                              #   Parent Loop BB1_285 Depth=1
                                        #     Parent Loop BB1_309 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ecx, %rcx
	movss	512(%rsp,%rcx,4), %xmm2 # xmm2 = mem[0],zero,zero,zero
	mulss	(%rax), %xmm2
	addss	%xmm2, %xmm0
	movss	%xmm0, (%rbx)
	movl	(%rbp), %ecx
	addq	$4, %rbp
	addq	$4, %rax
	testl	%ecx, %ecx
	jns	.LBB1_311
.LBB1_312:                              # %._crit_edge.i26
                                        #   in Loop: Header=BB1_309 Depth=2
	addq	$4, %rbx
	addq	$8, %rsi
	addq	$8, %rdi
	testl	%edx, %edx
	jne	.LBB1_309
.LBB1_313:                              # %match_calc.exit27
                                        #   in Loop: Header=BB1_285 Depth=1
	testl	%r14d, %r14d
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	-4(%rax,%r11,4), %eax
	movq	(%rsp), %r12            # 8-byte Reload
	movl	%eax, (%r13,%r12,4)
	movss	(%r10,%r12,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	%r13, 496(%rsp)         # 8-byte Spill
	jle	.LBB1_314
# BB#315:                               # %.lr.ph91
                                        #   in Loop: Header=BB1_285 Depth=1
	movq	240(%rsp), %rax         # 8-byte Reload
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm2
	movq	488(%rsp), %rax         # 8-byte Reload
	leaq	(%r13,%rax,4), %rbx
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	%r8, %rdi
	leaq	(%rax,%rdi,4), %r13
	movq	72(%rsp), %rcx          # 8-byte Reload
	cmpl	%ecx, %r11d
	je	.LBB1_324
# BB#316:                               # %.lr.ph91
                                        #   in Loop: Header=BB1_285 Depth=1
	cmpl	156(%rsp), %edi         # 4-byte Folded Reload
	je	.LBB1_324
# BB#317:                               # %.lr.ph91.split.preheader
                                        #   in Loop: Header=BB1_285 Depth=1
	movq	424(%rsp), %rax         # 8-byte Reload
	leaq	(%r10,%rax,4), %rdx
	xorl	%esi, %esi
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	120(%rsp), %r8          # 8-byte Reload
	movq	472(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_318:                              # %.lr.ph91.split
                                        #   Parent Loop BB1_285 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rbp,%rsi,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm4
	maxss	%xmm0, %xmm4
	movq	160(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%rsi,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm5
	movaps	%xmm2, %xmm3
	cmpless	%xmm5, %xmm3
	andps	%xmm3, %xmm5
	andnps	%xmm2, %xmm3
	orps	%xmm5, %xmm3
	movss	(%r15,%rsi,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	movss	(%rcx,%r11,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm2
	addss	(%r13), %xmm0
	ucomiss	%xmm5, %xmm0
	jb	.LBB1_320
# BB#319:                               #   in Loop: Header=BB1_318 Depth=2
	movss	%xmm0, (%r15,%rsi,4)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r11d, -8(%rax,%rsi,4)
.LBB1_320:                              #   in Loop: Header=BB1_318 Depth=2
	maxss	%xmm4, %xmm2
	cmpl	72(%rsp), %edi          # 4-byte Folded Reload
	jne	.LBB1_322
# BB#321:                               #   in Loop: Header=BB1_318 Depth=2
	movq	184(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%rsi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm0
	movss	%xmm0, (%rax,%rsi,4)
	movss	(%r15,%rsi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	224(%rsp), %rax         # 8-byte Reload
	addss	(%rax,%rsi,4), %xmm0
	movss	%xmm0, (%rax,%rsi,4)
.LBB1_322:                              #   in Loop: Header=BB1_318 Depth=2
	addss	-8(%rbx,%rsi,4), %xmm2
	movss	%xmm2, -8(%rbx,%rsi,4)
	movss	(%rdx,%rsi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	-1(%rax,%rsi), %rax
	decq	%rsi
	testq	%rax, %rax
	movaps	%xmm3, %xmm2
	jg	.LBB1_318
# BB#323:                               #   in Loop: Header=BB1_285 Depth=1
	movl	276(%rsp), %eax         # 4-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	(%rsp), %r12            # 8-byte Reload
	jmp	.LBB1_335
	.p2align	4, 0x90
.LBB1_314:                              # %match_calc.exit27.._crit_edge92_crit_edge
                                        #   in Loop: Header=BB1_285 Depth=1
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	%r8, %rdi
	leaq	(%rax,%rdi,4), %r13
	movl	420(%rsp), %eax         # 4-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	120(%rsp), %r8          # 8-byte Reload
	jmp	.LBB1_335
	.p2align	4, 0x90
.LBB1_324:                              # %.lr.ph91.split.us.preheader
                                        #   in Loop: Header=BB1_285 Depth=1
	movq	%r14, %rax
	movq	424(%rsp), %rdx         # 8-byte Reload
	movq	%r10, 480(%rsp)         # 8-byte Spill
	leaq	(%r10,%rdx,4), %r14
	xorl	%edi, %edi
	movl	%eax, %esi
	movq	256(%rsp), %r10         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_325:                              # %.lr.ph91.split.us
                                        #   Parent Loop BB1_285 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm2, %xmm5
	movq	168(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%rdi,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm3
	leaq	(%r10,%rdi), %r12
	ucomiss	%xmm0, %xmm3
	maxss	%xmm0, %xmm3
	movl	%r12d, %edx
	cmoval	%esi, %edx
	movq	144(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%rdi,4), %xmm6    # xmm6 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm6
	ucomiss	%xmm5, %xmm6
	movaps	%xmm5, %xmm4
	cmpnless	%xmm6, %xmm4
	movaps	%xmm4, %xmm2
	andnps	%xmm6, %xmm2
	andps	%xmm5, %xmm4
	cmovael	%r12d, %esi
	movss	(%r15,%rdi,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	movq	80(%rsp), %rax          # 8-byte Reload
	movss	(%rax,%r11,4), %xmm6    # xmm6 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm6
	ucomiss	%xmm3, %xmm6
	movl	%r11d, %eax
	jbe	.LBB1_327
# BB#326:                               #   in Loop: Header=BB1_325 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	-8(%rax,%rdi,4), %eax
	movaps	%xmm6, %xmm3
	movl	%r12d, %edx
.LBB1_327:                              #   in Loop: Header=BB1_325 Depth=2
	orps	%xmm4, %xmm2
	addss	(%r13), %xmm0
	ucomiss	%xmm5, %xmm0
	jb	.LBB1_329
# BB#328:                               #   in Loop: Header=BB1_325 Depth=2
	movss	%xmm0, (%r15,%rdi,4)
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	%r11d, -8(%rbp,%rdi,4)
	movq	256(%rsp), %r10         # 8-byte Reload
.LBB1_329:                              #   in Loop: Header=BB1_325 Depth=2
	movq	264(%rsp), %rbp         # 8-byte Reload
	movl	%eax, (%rbp,%rdi,4)
	movq	112(%rsp), %rax         # 8-byte Reload
	movl	%edx, (%rax,%rdi,4)
	cmpl	%ecx, %r8d
	jne	.LBB1_331
# BB#330:                               #   in Loop: Header=BB1_325 Depth=2
	movq	184(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%rdi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm0
	movss	%xmm0, (%rax,%rdi,4)
	movss	(%r15,%rdi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	224(%rsp), %rax         # 8-byte Reload
	addss	(%rax,%rdi,4), %xmm0
	movss	%xmm0, (%rax,%rdi,4)
.LBB1_331:                              #   in Loop: Header=BB1_325 Depth=2
	cmpl	%ecx, %r11d
	jne	.LBB1_333
# BB#332:                               #   in Loop: Header=BB1_325 Depth=2
	movq	504(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%rdi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm0
	movss	%xmm0, (%rax,%rdi,4)
.LBB1_333:                              #   in Loop: Header=BB1_325 Depth=2
	addss	-8(%rbx,%rdi,4), %xmm3
	movss	%xmm3, -8(%rbx,%rdi,4)
	movss	(%r14,%rdi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	-1(%rax,%rdi), %rax
	decq	%rdi
	testq	%rax, %rax
	jg	.LBB1_325
# BB#334:                               #   in Loop: Header=BB1_285 Depth=1
	movl	276(%rsp), %eax         # 4-byte Reload
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	%r8, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	120(%rsp), %r8          # 8-byte Reload
	movq	(%rsp), %r12            # 8-byte Reload
	movq	480(%rsp), %r10         # 8-byte Reload
.LBB1_335:                              # %._crit_edge92
                                        #   in Loop: Header=BB1_285 Depth=1
	addss	(%r13), %xmm0
	ucomiss	%xmm1, %xmm0
	maxss	%xmm1, %xmm0
	movq	176(%rsp), %rcx         # 8-byte Reload
	cmoval	%r11d, %ecx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	cmpl	72(%rsp), %edi          # 4-byte Folded Reload
	jne	.LBB1_337
# BB#336:                               #   in Loop: Header=BB1_285 Depth=1
	cltq
	movss	4(%r8,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%r8,%rax,4)
.LBB1_337:                              #   in Loop: Header=BB1_285 Depth=1
	movq	248(%rsp), %r11         # 8-byte Reload
	cmpl	%r11d, %edi
	movq	496(%rsp), %r13         # 8-byte Reload
	movl	40(%rsp), %ebx          # 4-byte Reload
	jne	.LBB1_354
# BB#338:                               #   in Loop: Header=BB1_285 Depth=1
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	xorl	%r9d, %r9d
	cmpl	$3, %r14d
	jl	.LBB1_344
# BB#339:                               # %.lr.ph99.preheader
                                        #   in Loop: Header=BB1_285 Depth=1
	cmpq	$0, 192(%rsp)           # 8-byte Folded Reload
	movq	448(%rsp), %rdx         # 8-byte Reload
	je	.LBB1_340
# BB#341:                               # %.lr.ph99.prol.preheader
                                        #   in Loop: Header=BB1_285 Depth=1
	xorl	%r9d, %r9d
	movl	$2, %eax
	movaps	%xmm1, %xmm2
	.p2align	4, 0x90
.LBB1_342:                              # %.lr.ph99.prol
                                        #   Parent Loop BB1_285 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rsi,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	maxss	%xmm2, %xmm1
	cmoval	%eax, %r9d
	leaq	1(%rdx,%rax), %rcx
	incq	%rax
	cmpq	$2, %rcx
	movaps	%xmm1, %xmm2
	jne	.LBB1_342
	jmp	.LBB1_343
.LBB1_340:                              #   in Loop: Header=BB1_285 Depth=1
	xorl	%r9d, %r9d
	movl	$2, %eax
.LBB1_343:                              # %.lr.ph99.prol.loopexit
                                        #   in Loop: Header=BB1_285 Depth=1
	cmpq	$3, 456(%rsp)           # 8-byte Folded Reload
	movq	256(%rsp), %rbp         # 8-byte Reload
	jb	.LBB1_344
	.p2align	4, 0x90
.LBB1_403:                              # %.lr.ph99
                                        #   Parent Loop BB1_285 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rsi,%rax,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	maxss	%xmm1, %xmm2
	cmoval	%eax, %r9d
	movss	4(%rsi,%rax,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	leal	1(%rax), %ecx
	ucomiss	%xmm2, %xmm1
	maxss	%xmm2, %xmm1
	cmovbel	%r9d, %ecx
	movss	8(%rsi,%rax,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	leal	2(%rax), %edx
	ucomiss	%xmm1, %xmm2
	maxss	%xmm1, %xmm2
	cmovbel	%ecx, %edx
	movss	12(%rsi,%rax,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	leal	3(%rax), %r9d
	ucomiss	%xmm2, %xmm1
	maxss	%xmm2, %xmm1
	cmovbel	%edx, %r9d
	addq	$4, %rax
	cmpq	%rbp, %rax
	jne	.LBB1_403
.LBB1_344:                              # %.preheader35
                                        #   in Loop: Header=BB1_285 Depth=1
	cmpl	$-1, %r14d
	jl	.LBB1_347
# BB#345:                               # %.lr.ph105.preheader
                                        #   in Loop: Header=BB1_285 Depth=1
	xorl	%eax, %eax
	movq	464(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_346:                              # %.lr.ph105
                                        #   Parent Loop BB1_285 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%r8,%rax,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	maxss	%xmm1, %xmm2
	cmoval	%eax, %r9d
	incq	%rax
	cmpq	%rcx, %rax
	movaps	%xmm2, %xmm1
	jl	.LBB1_346
.LBB1_347:                              # %._crit_edge106
                                        #   in Loop: Header=BB1_285 Depth=1
	movslq	%r9d, %rax
	movss	(%rsi,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	leal	-1(%rax), %ebx
	testl	%eax, %eax
	jle	.LBB1_348
# BB#349:                               #   in Loop: Header=BB1_285 Depth=1
	movslq	%ebx, %rcx
	movq	344(%rsp), %rdx         # 8-byte Reload
	movss	(%rdx,%rcx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	movl	%ebx, %ecx
	jbe	.LBB1_351
# BB#350:                               #   in Loop: Header=BB1_285 Depth=1
	movq	400(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %ecx
	movaps	%xmm2, %xmm1
	jmp	.LBB1_351
.LBB1_348:                              #   in Loop: Header=BB1_285 Depth=1
	movl	%ebx, %ecx
.LBB1_351:                              #   in Loop: Header=BB1_285 Depth=1
	movss	(%r8,%rax,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jbe	.LBB1_352
# BB#353:                               #   in Loop: Header=BB1_285 Depth=1
	movq	208(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %eax
	movl	%eax, 156(%rsp)         # 4-byte Spill
	jmp	.LBB1_354
	.p2align	4, 0x90
.LBB1_352:                              #   in Loop: Header=BB1_285 Depth=1
	movl	%r11d, %eax
	movl	%eax, 156(%rsp)         # 4-byte Spill
	movl	%ecx, %ebx
.LBB1_354:                              #   in Loop: Header=BB1_285 Depth=1
	cmpl	156(%rsp), %edi         # 4-byte Folded Reload
	movq	%r10, %rax
	jne	.LBB1_285
# BB#355:
	testl	%r9d, %r9d
	movq	232(%rsp), %r12         # 8-byte Reload
	movq	216(%rsp), %r13         # 8-byte Reload
	je	.LBB1_356
# BB#357:
	movq	64(%rsp), %r14          # 8-byte Reload
	cmpl	%r14d, %r9d
	movq	48(%rsp), %r15          # 8-byte Reload
	jg	.LBB1_359
# BB#358:
	movslq	%ebx, %rax
	movq	376(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	368(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %r15d
	movl	%edi, %r11d
	movl	%ebx, %r14d
	jmp	.LBB1_359
.LBB1_286:
	movl	%r9d, %r15d
	movl	156(%rsp), %eax         # 4-byte Reload
	movl	%eax, %r11d
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r14d
	movq	232(%rsp), %r12         # 8-byte Reload
	movq	216(%rsp), %r13         # 8-byte Reload
	jmp	.LBB1_359
.LBB1_356:
	movq	176(%rsp), %rax         # 8-byte Reload
	leal	-1(%rax), %r11d
	xorl	%r14d, %r14d
	movl	$1, %r15d
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	movq	%rax, 72(%rsp)          # 8-byte Spill
.LBB1_359:                              # %.loopexit37
	movq	288(%rsp), %rdi         # 8-byte Reload
	movq	%r11, %rbp
	callq	FreeFloatVec
	movq	384(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatVec
	movq	392(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatVec
	movq	120(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	344(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatVec
	movq	400(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	208(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	376(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	368(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	440(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	432(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	FreeFloatVec
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntVec
	movq	360(%rsp), %rdi         # 8-byte Reload
	callq	FreeFloatMtx
	movq	352(%rsp), %rdi         # 8-byte Reload
	callq	FreeIntMtx
	movl	672(%rsp), %eax
	movq	%rax, %r10
	leal	(%rbp,%r10), %eax
	movl	688(%rsp), %ecx
	movq	%rcx, %r11
	leal	(%r14,%r11), %ebx
	movl	36(%rsp), %edi          # 4-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	304(%rsp), %rdx         # 8-byte Reload
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	320(%rsp), %r8          # 8-byte Reload
	movq	328(%rsp), %r9          # 8-byte Reload
	pushq	728(%rsp)
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	304(%rsp)               # 8-byte Folded Reload
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	callq	MSalignmm_rec
	addq	$64, %rsp
.Lcfi61:
	.cfi_adjust_cfa_offset -64
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movq	704(%rsp), %rax
	movq	(%rax), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%r14, %rcx
	movq	%r15, 48(%rsp)          # 8-byte Spill
                                        # kill: %R15D<def> %R15D<kill> %R15<kill> %R15<def>
	subl	%ecx, %r15d
	cmpl	$2, %r15d
	movq	%rbp, 248(%rsp)         # 8-byte Spill
	jl	.LBB1_369
# BB#360:                               # %.lr.ph80.preheader
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	decl	%r15d
	movslq	%r15d, %r13
	testl	%r13d, %r13d
	movl	$1, %edx
	cmovgl	%r13d, %edx
	decl	%edx
	incq	%rdx
	movl	$45, %esi
	movq	280(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdi
	callq	memset
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_361:                              # %.lr.ph80
                                        # =>This Inner Loop Header: Depth=1
	incq	%rax
	cmpq	%r13, %rax
	jl	.LBB1_361
# BB#362:                               # %._crit_edge81
	movb	$0, (%r14,%rax)
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_365
# BB#363:                               # %.lr.ph78
	movq	(%rsp), %rax            # 8-byte Reload
	leal	(%rax,%r15), %eax
	movslq	%eax, %r12
	movl	36(%rsp), %ebp          # 4-byte Reload
	movq	704(%rsp), %rbx
	.p2align	4, 0x90
.LBB1_364:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	callq	strcat
	movq	(%rbx), %rax
	movb	$0, (%rax,%r12)
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB1_364
.LBB1_365:                              # %.preheader33
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movq	104(%rsp), %r12         # 8-byte Reload
	jle	.LBB1_368
# BB#366:                               # %.lr.ph74
	movslq	64(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	(%rsp), %rax            # 8-byte Reload
	addl	%r15d, %eax
	movslq	%eax, %r14
	movl	12(%rsp), %r15d         # 4-byte Reload
	movq	312(%rsp), %rbx         # 8-byte Reload
	movq	712(%rsp), %rbp
	.p2align	4, 0x90
.LBB1_367:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movq	(%rbx), %rax
	addq	%r12, %rax
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	1(%rdx,%rax), %rsi
	movq	%r13, %rdx
	callq	strncat
	movq	(%rbp), %rax
	movb	$0, (%rax,%r14)
	addq	$8, %rbp
	addq	$8, %rbx
	decq	%r15
	jne	.LBB1_367
.LBB1_368:                              # %._crit_edge75
	movslq	64(%rsp), %rax          # 4-byte Folded Reload
	movq	336(%rsp), %rcx         # 8-byte Reload
	movss	4(%rcx,%rax,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movslq	48(%rsp), %rax          # 4-byte Folded Reload
	movq	128(%rsp), %rcx         # 8-byte Reload
	addss	-4(%rcx,%rax,4), %xmm0
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 16(%rsp)         # 4-byte Spill
	movq	704(%rsp), %rax
	movq	(%rax), %rbx
	movq	232(%rsp), %r12         # 8-byte Reload
	movq	216(%rsp), %r13         # 8-byte Reload
	movq	248(%rsp), %rbp         # 8-byte Reload
.LBB1_369:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rcx
	movq	72(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	subl	%ebp, %eax
	cmpl	$2, %eax
	jl	.LBB1_379
# BB#370:                               # %.lr.ph71.preheader
	movq	%rcx, (%rsp)            # 8-byte Spill
	decl	%eax
	movq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movslq	%eax, %rbp
	testl	%ebp, %ebp
	movl	$1, %edx
	cmovgl	%ebp, %edx
	decl	%edx
	incq	%rdx
	movl	$45, %esi
	movq	280(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdi
	callq	memset
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_371:                              # %.lr.ph71
                                        # =>This Inner Loop Header: Depth=1
	incq	%rax
	cmpq	%rbp, %rax
	jl	.LBB1_371
# BB#372:                               # %._crit_edge72
	movb	$0, (%rbx,%rax)
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movq	88(%rsp), %r12          # 8-byte Reload
	movq	704(%rsp), %r15
	jle	.LBB1_375
# BB#373:                               # %.lr.ph69
	movslq	248(%rsp), %rax         # 4-byte Folded Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	(%rsp), %rax            # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	leal	(%rax,%rdx), %eax
	movslq	%eax, %r13
	movl	36(%rsp), %ebx          # 4-byte Reload
	movq	304(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_374:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rdi
	movq	(%r14), %rax
	addq	%r12, %rax
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	1(%rdx,%rax), %rsi
	movq	%rbp, %rdx
	callq	strncat
	movq	(%r15), %rax
	movb	$0, (%rax,%r13)
	addq	$8, %r15
	addq	$8, %r14
	decq	%rbx
	jne	.LBB1_374
.LBB1_375:                              # %.preheader31
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movq	712(%rsp), %rbx
	movq	280(%rsp), %r15         # 8-byte Reload
	jle	.LBB1_378
# BB#376:                               # %.lr.ph65
	movq	(%rsp), %rax            # 8-byte Reload
	addl	40(%rsp), %eax          # 4-byte Folded Reload
	movslq	%eax, %r14
	movl	12(%rsp), %ebp          # 4-byte Reload
	.p2align	4, 0x90
.LBB1_377:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	callq	strcat
	movq	(%rbx), %rax
	movb	$0, (%rax,%r14)
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB1_377
.LBB1_378:                              # %._crit_edge66
	movslq	248(%rsp), %rax         # 4-byte Folded Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movss	4(%rcx,%rax,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movslq	72(%rsp), %rax          # 4-byte Folded Reload
	movq	200(%rsp), %rcx         # 8-byte Reload
	addss	-4(%rcx,%rax,4), %xmm0
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 16(%rsp)         # 4-byte Spill
	movq	232(%rsp), %r12         # 8-byte Reload
	movq	216(%rsp), %r13         # 8-byte Reload
.LBB1_379:
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	672(%rsp), %r15d
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	36(%rsp), %edi          # 4-byte Reload
	testl	%edi, %edi
	jle	.LBB1_390
# BB#380:                               # %.lr.ph63
	movslq	%eax, %r11
	addq	%r11, %rbx
	movq	%rbx, (%r13)
	cmpl	$1, %edi
	je	.LBB1_390
# BB#381:                               # %._crit_edge278.lr.ph
	movl	%edi, %edx
	leaq	-1(%rdx), %r9
	cmpq	$3, %r9
	ja	.LBB1_385
# BB#382:
	movl	$1, %r10d
	jmp	.LBB1_383
.LBB1_385:                              # %min.iters.checked593
	movq	%r9, %r10
	andq	$-4, %r10
	movq	%r9, %r8
	andq	$-4, %r8
	je	.LBB1_386
# BB#387:                               # %vector.body589.preheader
	orq	$1, %r10
	leaq	24(%r13), %rbx
	movq	%r8, %r14
	.p2align	4, 0x90
.LBB1_388:                              # %vector.body589
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rbx), %xmm0
	movdqu	(%rbx), %xmm1
	movd	%xmm0, %rdi
	addq	%r11, %rdi
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %rsi
	addq	%r11, %rsi
	movd	%xmm1, %rcx
	addq	%r11, %rcx
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	movd	%xmm0, %rbp
	addq	%r11, %rbp
	movd	%rdi, %xmm0
	movd	%rsi, %xmm1
	punpcklqdq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movd	%rcx, %xmm1
	movd	%rbp, %xmm2
	punpcklqdq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm1, (%rbx)
	addq	$32, %rbx
	addq	$-4, %r14
	jne	.LBB1_388
# BB#389:                               # %middle.block590
	cmpq	%r8, %r9
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	36(%rsp), %edi          # 4-byte Reload
	jne	.LBB1_383
	jmp	.LBB1_390
.LBB1_386:
	movl	$1, %r10d
	movl	36(%rsp), %edi          # 4-byte Reload
.LBB1_383:                              # %._crit_edge278.preheader
	subq	%r10, %rdx
	leaq	(%r13,%r10,8), %rsi
	.p2align	4, 0x90
.LBB1_384:                              # %._crit_edge278
                                        # =>This Inner Loop Header: Depth=1
	addq	%r11, (%rsi)
	addq	$8, %rsi
	decq	%rdx
	jne	.LBB1_384
.LBB1_390:                              # %.preheader30
	testl	%ebp, %ebp
	jle	.LBB1_400
# BB#391:                               # %.lr.ph59
	movslq	%eax, %r10
	movl	%ebp, %ecx
	cmpl	$3, %ebp
	ja	.LBB1_395
# BB#392:
	xorl	%r9d, %r9d
	jmp	.LBB1_393
.LBB1_395:                              # %min.iters.checked614
	movl	%ebp, %r8d
	andl	$3, %r8d
	movq	%rcx, %r9
	subq	%r8, %r9
	je	.LBB1_396
# BB#397:                               # %vector.body610.preheader
	leaq	16(%r12), %rdi
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB1_398:                              # %vector.body610
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movd	%xmm0, %rbx
	addq	%r10, %rbx
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %rsi
	addq	%r10, %rsi
	movd	%xmm1, %rbp
	addq	%r10, %rbp
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	movd	%xmm0, %rax
	addq	%r10, %rax
	movd	%rbx, %xmm0
	movd	%rsi, %xmm1
	punpcklqdq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movd	%rbp, %xmm1
	movd	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm1, (%rdi)
	addq	$32, %rdi
	addq	$-4, %rdx
	jne	.LBB1_398
# BB#399:                               # %middle.block611
	testl	%r8d, %r8d
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	36(%rsp), %edi          # 4-byte Reload
	jne	.LBB1_393
	jmp	.LBB1_400
.LBB1_396:
	xorl	%r9d, %r9d
	movl	36(%rsp), %edi          # 4-byte Reload
.LBB1_393:                              # %scalar.ph612.preheader
	leaq	(%r12,%r9,8), %rdx
	subq	%r9, %rcx
	.p2align	4, 0x90
.LBB1_394:                              # %scalar.ph612
                                        # =>This Inner Loop Header: Depth=1
	addq	%r10, (%rdx)
	addq	$8, %rdx
	decq	%rcx
	jne	.LBB1_394
.LBB1_400:                              # %._crit_edge60
	movq	72(%rsp), %rbx          # 8-byte Reload
	addl	%r15d, %ebx
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	688(%rsp), %ecx
	addl	%ecx, %eax
	movl	%ebp, %esi
	movq	304(%rsp), %rdx         # 8-byte Reload
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	320(%rsp), %r8          # 8-byte Reload
	movq	328(%rsp), %r9          # 8-byte Reload
	pushq	728(%rsp)
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	304(%rsp)               # 8-byte Folded Reload
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	movl	728(%rsp), %ebp
	pushq	%rbp
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	movl	728(%rsp), %eax
	pushq	%rax
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	callq	MSalignmm_rec
	addq	$64, %rsp
.Lcfi70:
	.cfi_adjust_cfa_offset -64
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 16(%rsp)         # 4-byte Spill
	movq	280(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	%r12, %rdi
.LBB1_401:                              # %.loopexit
	callq	free
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
.LBB1_402:                              # %.loopexit
	addq	$616, %rsp              # imm = 0x268
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	MSalignmm_rec, .Lfunc_end1-MSalignmm_rec
	.cfi_endproc

	.p2align	4, 0x90
	.type	match_calc,@function
match_calc:                             # @match_calc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 56
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	56(%rsp), %r11
	cmpl	$0, 64(%rsp)
	je	.LBB2_10
# BB#1:
	testl	%r8d, %r8d
	jle	.LBB2_10
# BB#2:                                 # %.preheader79.preheader
	movl	%r8d, %r10d
	xorl	%r15d, %r15d
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader79
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
	movq	(%rdx,%r15,8), %r14
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_4:                                #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%r14,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB2_5
	jnp	.LBB2_6
.LBB2_5:                                #   in Loop: Header=BB2_4 Depth=2
	movq	(%r9,%r15,8), %r12
	movslq	%r13d, %r13
	movss	%xmm1, (%r12,%r13,4)
	movq	(%r11,%r15,8), %rbx
	movl	%eax, (%rbx,%r13,4)
	incl	%r13d
.LBB2_6:                                #   in Loop: Header=BB2_4 Depth=2
	movss	4(%r14,%rax,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB2_7
	jnp	.LBB2_8
.LBB2_7:                                #   in Loop: Header=BB2_4 Depth=2
	movq	(%r9,%r15,8), %rbx
	movslq	%r13d, %r13
	movss	%xmm1, (%rbx,%r13,4)
	movq	(%r11,%r15,8), %rbx
	leal	1(%rax), %ebp
	movl	%ebp, (%rbx,%r13,4)
	incl	%r13d
.LBB2_8:                                #   in Loop: Header=BB2_4 Depth=2
	addq	$2, %rax
	cmpq	$26, %rax
	jne	.LBB2_4
# BB#9:                                 #   in Loop: Header=BB2_3 Depth=1
	movq	(%r11,%r15,8), %rax
	movslq	%r13d, %rbx
	movl	$-1, (%rax,%rbx,4)
	incq	%r15
	cmpq	%r10, %r15
	jne	.LBB2_3
.LBB2_10:                               # %.preheader78
	movslq	%ecx, %rax
	movq	(%rsi,%rax,8), %rax
	addq	$4, %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_12 Depth 2
	movl	$0, -104(%rsp,%rcx,4)
	xorps	%xmm0, %xmm0
	movq	$-2704, %rdx            # imm = 0xF570
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB2_12:                               #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	n_dis+2704(%rdx,%rcx,4), %xmm1
	mulss	-4(%rsi), %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	n_dis+2808(%rdx,%rcx,4), %xmm0
	mulss	(%rsi), %xmm0
	addss	%xmm1, %xmm0
	addq	$8, %rsi
	addq	$208, %rdx
	jne	.LBB2_12
# BB#13:                                #   in Loop: Header=BB2_11 Depth=1
	movss	%xmm0, -104(%rsp,%rcx,4)
	incq	%rcx
	cmpq	$26, %rcx
	jne	.LBB2_11
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_18:                               # %._crit_edge
                                        #   in Loop: Header=BB2_14 Depth=1
	addq	$4, %rdi
	addq	$8, %r11
	addq	$8, %r9
.LBB2_14:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_17 Depth 2
	testl	%r8d, %r8d
	je	.LBB2_19
# BB#15:                                # %.lr.ph85
                                        #   in Loop: Header=BB2_14 Depth=1
	decl	%r8d
	movl	$0, (%rdi)
	movq	(%r11), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	js	.LBB2_18
# BB#16:                                # %.lr.ph
                                        #   in Loop: Header=BB2_14 Depth=1
	movq	(%r9), %rcx
	addq	$4, %rax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB2_17:                               #   Parent Loop BB2_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	movss	-104(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdi)
	movl	(%rax), %edx
	addq	$4, %rcx
	addq	$4, %rax
	testl	%edx, %edx
	jns	.LBB2_17
	jmp	.LBB2_18
.LBB2_19:                               # %._crit_edge86
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	match_calc, .Lfunc_end2-match_calc
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"i = %d / %d\n"
	.size	.L.str, 13

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"bug! hairetsu ga kowareta!\n"
	.size	.L.str.1, 28

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"j = %d / %d\n"
	.size	.L.str.2, 13

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"bug! hairetsu ga kowareta! (nglen1) seqlen(seq1[0])=%d but nglen1=%d\n"
	.size	.L.str.3, 70

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"seq1[0] = %s\n"
	.size	.L.str.4, 14

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"bug! hairetsu ga kowareta! (nglen2) seqlen(seq2[0])=%d but nglen2=%d\n"
	.size	.L.str.5, 70

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"hairetsu ga kowareta (end of MSalignmm) !\n"
	.size	.L.str.6, 43

	.type	reccycle,@object        # @reccycle
	.local	reccycle
	.comm	reccycle,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
