	.text
	.file	"btGjkPairDetector.bc"
	.globl	_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.p2align	4, 0x90
	.type	_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver,@function
_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver: # @_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movq	$_ZTV17btGjkPairDetector+16, (%rbx)
	movl	$0, 8(%rbx)
	movl	$1065353216, 12(%rbx)   # imm = 0x3F800000
	movl	$0, 16(%rbx)
	movl	$0, 20(%rbx)
	movq	%r8, 24(%rbx)
	movq	%rcx, 32(%rbx)
	movq	%rsi, 40(%rbx)
	movq	%r14, 48(%rbx)
	movl	8(%rsi), %eax
	movl	%eax, 56(%rbx)
	movl	8(%r14), %eax
	movl	%eax, 60(%rbx)
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	callq	*88(%rax)
	movss	%xmm0, 64(%rbx)
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	movss	%xmm0, 68(%rbx)
	movb	$0, 72(%rbx)
	movl	$-1, 80(%rbx)
	movl	$1, 92(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver, .Lfunc_end0-_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.cfi_endproc

	.globl	_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.p2align	4, 0x90
	.type	_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver,@function
_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver: # @_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.cfi_startproc
# BB#0:
	movq	8(%rsp), %rax
	movq	$_ZTV17btGjkPairDetector+16, (%rdi)
	movl	$0, 8(%rdi)
	movl	$1065353216, 12(%rdi)   # imm = 0x3F800000
	movl	$0, 16(%rdi)
	movl	$0, 20(%rdi)
	movq	%rax, 24(%rdi)
	movq	%r9, 32(%rdi)
	movq	%rsi, 40(%rdi)
	movq	%rdx, 48(%rdi)
	movl	%ecx, 56(%rdi)
	movl	%r8d, 60(%rdi)
	movss	%xmm0, 64(%rdi)
	movss	%xmm1, 68(%rdi)
	movb	$0, 72(%rdi)
	movl	$-1, 80(%rdi)
	movl	$1, 92(%rdi)
	retq
.Lfunc_end1:
	.size	_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver, .Lfunc_end1-_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.cfi_endproc

	.globl	_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
	.p2align	4, 0x90
	.type	_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb,@function
_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb: # @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
	.cfi_startproc
# BB#0:
	jmp	_ZN17btGjkPairDetector26getClosestPointsNonVirtualERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDraw # TAILCALL
.Lfunc_end2:
	.size	_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb, .Lfunc_end2-_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
.LCPI3_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_1:
	.long	1056964608              # float 0.5
.LCPI3_2:
	.long	1566444395              # float 9.99999984E+17
.LCPI3_4:
	.long	897988541               # float 9.99999997E-7
.LCPI3_5:
	.long	872415232               # float 1.1920929E-7
.LCPI3_7:
	.long	679477248               # float 1.42108547E-14
.LCPI3_8:
	.long	1065353216              # float 1
.LCPI3_10:
	.long	0                       # float 0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_6:
	.quad	4547007122018943789     # double 1.0E-4
.LCPI3_9:
	.quad	4576918229304087675     # double 0.01
	.text
	.globl	_ZN17btGjkPairDetector26getClosestPointsNonVirtualERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDraw
	.p2align	4, 0x90
	.type	_ZN17btGjkPairDetector26getClosestPointsNonVirtualERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDraw,@function
_ZN17btGjkPairDetector26getClosestPointsNonVirtualERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDraw: # @_ZN17btGjkPairDetector26getClosestPointsNonVirtualERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDraw
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$360, %rsp              # imm = 0x168
.Lcfi11:
	.cfi_def_cfa_offset 416
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rcx, 304(%rsp)         # 8-byte Spill
	movq	%rdx, 312(%rsp)         # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movl	$0, 76(%r15)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	(%rbx), %xmm0
	movaps	%xmm0, 224(%rsp)
	movups	16(%rbx), %xmm0
	movaps	%xmm0, 240(%rsp)
	movups	32(%rbx), %xmm0
	movaps	%xmm0, 256(%rsp)
	movups	48(%rbx), %xmm0
	movaps	%xmm0, 272(%rsp)
	movups	64(%rbx), %xmm0
	movaps	%xmm0, 160(%rsp)
	movups	80(%rbx), %xmm0
	movaps	%xmm0, 176(%rsp)
	movups	96(%rbx), %xmm0
	movaps	%xmm0, 192(%rsp)
	movups	112(%rbx), %xmm0
	movaps	%xmm0, 208(%rsp)
	movaps	272(%rsp), %xmm0
	movaps	208(%rsp), %xmm1
	movaps	%xmm0, %xmm6
	addps	%xmm1, %xmm6
	movss	280(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	216(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm7
	addss	%xmm3, %xmm7
	mulps	.LCPI3_0(%rip), %xmm6
	movaps	%xmm6, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	mulss	.LCPI3_1(%rip), %xmm7
	movaps	%xmm0, %xmm5
	subss	%xmm6, %xmm5
	movss	%xmm5, 272(%rsp)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm4, %xmm0
	movss	%xmm0, 276(%rsp)
	subss	%xmm7, %xmm2
	movss	%xmm2, 280(%rsp)
	movaps	%xmm1, %xmm0
	movaps	%xmm6, 336(%rsp)        # 16-byte Spill
	subss	%xmm6, %xmm0
	movss	%xmm0, 208(%rsp)
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	subss	%xmm4, %xmm1
	movss	%xmm1, 212(%rsp)
	movss	%xmm7, 140(%rsp)        # 4-byte Spill
	subss	%xmm7, %xmm3
	movss	%xmm3, 216(%rsp)
	movq	40(%r15), %rax
	movl	8(%rax), %eax
	addl	$-17, %eax
	cmpl	$1, %eax
	ja	.LBB3_2
# BB#1:
	movq	48(%r15), %rax
	movl	8(%rax), %eax
	addl	$-17, %eax
	cmpl	$2, %eax
	setb	%r12b
	jmp	.LBB3_3
.LBB3_2:
	xorl	%r12d, %r12d
.LBB3_3:                                # %._crit_edge
	movss	64(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 68(%rsp)         # 4-byte Spill
	movss	68(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	incl	gNumGjkChecks(%rip)
	movb	72(%r15), %al
	movb	%al, 11(%rsp)           # 1-byte Spill
	testb	%al, %al
	movl	$0, 84(%r15)
	movl	$0, 8(%r15)
	movl	$1065353216, 12(%r15)   # imm = 0x3F800000
	movl	$0, 16(%r15)
	movl	$0, 20(%r15)
	movl	$0, 88(%r15)
	movl	$-1, 80(%r15)
	je	.LBB3_5
# BB#4:                                 # %._crit_edge
	xorps	%xmm0, %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
.LBB3_5:                                # %._crit_edge
	leaq	8(%r15), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	32(%r15), %rdi
	callq	_ZN22btVoronoiSimplexSolver5resetEv
	movss	.LCPI3_2(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	xorl	%r14d, %r14d
	leaq	32(%rsp), %rbp
	.p2align	4, 0x90
.LBB3_6:                                # =>This Inner Loop Header: Depth=1
	movss	%xmm2, 4(%rsp)          # 4-byte Spill
	movd	8(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movd	12(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movdqa	%xmm1, %xmm3
	movdqa	.LCPI3_3(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movdqa	%xmm2, %xmm6
	pxor	%xmm6, %xmm3
	movdqa	%xmm0, %xmm4
	pxor	%xmm6, %xmm4
	movd	16(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movdqa	%xmm2, %xmm5
	pxor	%xmm6, %xmm5
	movsd	(%rbx), %xmm6           # xmm6 = mem[0],zero
	pshufd	$224, %xmm3, %xmm7      # xmm7 = xmm3[0,0,2,3]
	mulps	%xmm6, %xmm7
	movsd	16(%rbx), %xmm6         # xmm6 = mem[0],zero
	pshufd	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm6, %xmm4
	addps	%xmm7, %xmm4
	movsd	32(%rbx), %xmm6         # xmm6 = mem[0],zero
	pshufd	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm6, %xmm5
	addps	%xmm4, %xmm5
	movss	8(%rbx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	movss	24(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	subss	%xmm3, %xmm4
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	subss	%xmm3, %xmm4
	xorps	%xmm3, %xmm3
	movss	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1,2,3]
	movlps	%xmm5, 48(%rsp)
	movlps	%xmm3, 56(%rsp)
	movsd	64(%rbx), %xmm3         # xmm3 = mem[0],zero
	movdqa	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	movsd	80(%rbx), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm0, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm4, %xmm5
	movsd	96(%rbx), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm5, %xmm4
	mulss	72(%rbx), %xmm1
	mulss	88(%rbx), %xmm0
	addss	%xmm1, %xmm0
	mulss	104(%rbx), %xmm2
	addss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm4, 32(%rsp)
	movlps	%xmm0, 40(%rsp)
	movq	40(%r15), %rdi
	leaq	48(%rsp), %rsi
	callq	_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	movq	48(%r15), %rdi
	movq	%rbp, %rsi
	callq	_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3
	movaps	144(%rsp), %xmm4        # 16-byte Reload
	movaps	%xmm4, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movaps	%xmm4, %xmm2
	movaps	%xmm4, %xmm8
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	240(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	224(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	228(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm2, %xmm5
	movss	244(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0],xmm6[1],xmm2[1]
	mulps	%xmm3, %xmm6
	addps	%xmm5, %xmm6
	movaps	112(%rsp), %xmm7        # 16-byte Reload
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	248(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	232(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	mulps	%xmm4, %xmm2
	addps	%xmm6, %xmm2
	addps	272(%rsp), %xmm2
	movss	256(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm4
	movss	260(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm4, %xmm5
	movss	264(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm4
	addss	%xmm5, %xmm4
	addss	280(%rsp), %xmm4
	xorps	%xmm3, %xmm3
	movss	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1,2,3]
	movlps	%xmm2, 72(%rsp)
	movlps	%xmm3, 80(%rsp)
	movaps	%xmm0, %xmm3
	movss	192(%rsp), %xmm8        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm8
	movaps	%xmm0, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	176(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	160(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	164(%rsp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	mulps	%xmm3, %xmm6
	movss	180(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1]
	mulps	%xmm5, %xmm7
	addps	%xmm6, %xmm7
	movss	200(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	184(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	168(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm1    # xmm1 = xmm1[0],xmm6[0],xmm1[1],xmm6[1]
	mulps	%xmm3, %xmm1
	addps	%xmm7, %xmm1
	addps	208(%rsp), %xmm1
	movss	196(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	addss	%xmm8, %xmm3
	addss	%xmm3, %xmm0
	addss	216(%rsp), %xmm0
	xorps	%xmm3, %xmm3
	movss	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1,2,3]
	movlps	%xmm1, 88(%rsp)
	movlps	%xmm3, 96(%rsp)
	testb	%r12b, %r12b
	je	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_6 Depth=1
	movl	$0, 80(%rsp)
	movl	$0, 96(%rsp)
	xorps	%xmm0, %xmm0
	xorps	%xmm4, %xmm4
.LBB3_8:                                #   in Loop: Header=BB3_6 Depth=1
	subps	%xmm1, %xmm2
	subss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm2, 320(%rsp)
	movaps	%xmm2, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movlps	%xmm0, 328(%rsp)
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	mulss	12(%r15), %xmm1
	addss	%xmm0, %xmm1
	mulss	16(%r15), %xmm4
	addss	%xmm1, %xmm4
	ucomiss	.LCPI3_10, %xmm4
	jbe	.LBB3_11
# BB#9:                                 #   in Loop: Header=BB3_6 Depth=1
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	movss	128(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	4(%rsp), %xmm1          # 4-byte Folded Reload
	ucomiss	%xmm1, %xmm0
	jbe	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_6 Depth=1
	movl	$10, 88(%r15)
	jmp	.LBB3_24
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_6 Depth=1
	movss	%xmm4, 112(%rsp)        # 4-byte Spill
	movq	32(%r15), %rdi
	leaq	320(%rsp), %rsi
	callq	_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3
	testb	%al, %al
	je	.LBB3_13
# BB#12:                                #   in Loop: Header=BB3_6 Depth=1
	movl	$1, 88(%r15)
	jmp	.LBB3_24
	.p2align	4, 0x90
.LBB3_13:                               #   in Loop: Header=BB3_6 Depth=1
	movss	4(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	subss	112(%rsp), %xmm0        # 4-byte Folded Reload
	movaps	%xmm2, %xmm1
	mulss	.LCPI3_4(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	jae	.LBB3_17
# BB#14:                                #   in Loop: Header=BB3_6 Depth=1
	movq	32(%r15), %rdi
	leaq	320(%rsp), %rsi
	leaq	72(%rsp), %rdx
	leaq	88(%rsp), %rcx
	callq	_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_
	movq	32(%r15), %rdi
	leaq	288(%rsp), %rsi
	callq	_ZN22btVoronoiSimplexSolver7closestER9btVector3
	testb	%al, %al
	je	.LBB3_19
# BB#15:                                #   in Loop: Header=BB3_6 Depth=1
	movss	288(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	292(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	296(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm3
	addss	%xmm1, %xmm3
	movups	288(%rsp), %xmm0
	movq	104(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
	movss	.LCPI3_4(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm0
	movss	4(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jbe	.LBB3_20
# BB#16:                                #   in Loop: Header=BB3_6 Depth=1
	movl	$6, 88(%r15)
	jmp	.LBB3_18
.LBB3_17:                               #   in Loop: Header=BB3_6 Depth=1
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	sbbl	%eax, %eax
	andl	$1, %eax
	leal	2(%rax,%rax,8), %eax
	movl	%eax, 88(%r15)
.LBB3_18:                               #   in Loop: Header=BB3_6 Depth=1
	movb	$1, %r14b
	movl	$2, %r13d
	testl	%r13d, %r13d
	je	.LBB3_6
	jmp	.LBB3_29
.LBB3_19:                               #   in Loop: Header=BB3_6 Depth=1
	movl	$3, 88(%r15)
	jmp	.LBB3_24
.LBB3_20:                               #   in Loop: Header=BB3_6 Depth=1
	movaps	%xmm2, %xmm0
	subss	%xmm3, %xmm0
	mulss	.LCPI3_5(%rip), %xmm2
	ucomiss	%xmm0, %xmm2
	jae	.LBB3_23
# BB#21:                                #   in Loop: Header=BB3_6 Depth=1
	movl	84(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 84(%r15)
	movl	$2, %r13d
	cmpl	$1000, %eax             # imm = 0x3E8
	jle	.LBB3_26
# BB#22:                                #   in Loop: Header=BB3_6 Depth=1
	movaps	%xmm3, %xmm2
	testl	%r13d, %r13d
	je	.LBB3_6
	jmp	.LBB3_29
.LBB3_23:                               #   in Loop: Header=BB3_6 Depth=1
	movq	32(%r15), %rdi
	movq	104(%rsp), %rsi         # 8-byte Reload
	movss	%xmm3, 4(%rsp)          # 4-byte Spill
	callq	_ZN22btVoronoiSimplexSolver14backup_closestER9btVector3
	movl	$12, 88(%r15)
	.p2align	4, 0x90
.LBB3_24:                               #   in Loop: Header=BB3_6 Depth=1
	movb	$1, %r14b
	movl	$2, %r13d
.LBB3_25:                               #   in Loop: Header=BB3_6 Depth=1
	movss	4(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	testl	%r13d, %r13d
	je	.LBB3_6
	jmp	.LBB3_29
.LBB3_26:                               #   in Loop: Header=BB3_6 Depth=1
	movq	32(%r15), %rdi
	cmpl	$4, (%rdi)
	jne	.LBB3_28
# BB#27:                                #   in Loop: Header=BB3_6 Depth=1
	movq	104(%rsp), %rsi         # 8-byte Reload
	movss	%xmm3, 4(%rsp)          # 4-byte Spill
	callq	_ZN22btVoronoiSimplexSolver14backup_closestER9btVector3
	movl	$13, 88(%r15)
	jmp	.LBB3_25
.LBB3_28:                               #   in Loop: Header=BB3_6 Depth=1
	xorl	%r13d, %r13d
	movaps	%xmm3, %xmm2
	testl	%r13d, %r13d
	je	.LBB3_6
.LBB3_29:
	cmpb	$0, 11(%rsp)            # 1-byte Folded Reload
	xorps	%xmm8, %xmm8
	je	.LBB3_31
# BB#30:
	xorps	%xmm0, %xmm0
	movss	%xmm0, 68(%rsp)         # 4-byte Spill
.LBB3_31:
	movss	68(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	addss	12(%rsp), %xmm3         # 4-byte Folded Reload
	testb	$1, %r14b
	jne	.LBB3_33
# BB#32:
	xorl	%r14d, %r14d
	cmpl	$0, 92(%r15)
	jne	.LBB3_43
	jmp	.LBB3_48
.LBB3_33:
	movss	%xmm3, 112(%rsp)        # 4-byte Spill
	movss	%xmm2, 4(%rsp)          # 4-byte Spill
	movq	32(%r15), %rdi
	leaq	48(%rsp), %rsi
	leaq	32(%rsp), %rdx
	callq	_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_
	movsd	48(%rsp), %xmm0         # xmm0 = mem[0],zero
	movsd	32(%rsp), %xmm1         # xmm1 = mem[0],zero
	subps	%xmm1, %xmm0
	movss	56(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	40(%rsp), %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm2, 24(%rsp)
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	16(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm1
	movsd	.LCPI3_6(%rip), %xmm2   # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jbe	.LBB3_35
# BB#34:
	movl	$5, 88(%r15)
.LBB3_35:
	ucomiss	.LCPI3_7(%rip), %xmm0
	movss	4(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jbe	.LBB3_41
# BB#36:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB3_38
# BB#37:                                # %call.sqrt
	callq	sqrtf
	movss	4(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB3_38:                               # %.split
	movss	.LCPI3_8(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm6
	divss	%xmm1, %xmm6
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	movss	%xmm0, 16(%rsp)
	movss	20(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	movss	%xmm0, 20(%rsp)
	movss	24(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	movss	%xmm0, 24(%rsp)
	xorps	%xmm0, %xmm0
	sqrtss	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB3_40
# BB#39:                                # %call.sqrt334
	movaps	%xmm2, %xmm0
	movss	%xmm6, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	.LCPI3_8(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
.LBB3_40:                               # %.split.split
	movss	68(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movss	8(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	12(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	mulss	%xmm2, %xmm4
	movaps	%xmm2, %xmm5
	mulss	%xmm1, %xmm5
	movss	16(%r15), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	movss	48(%rsp), %xmm7         # xmm7 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm7
	movss	%xmm7, 48(%rsp)
	movss	52(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm4
	movss	%xmm4, 52(%rsp)
	movss	56(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm4
	movss	%xmm4, 56(%rsp)
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	mulss	%xmm2, %xmm3
	mulss	%xmm2, %xmm1
	mulss	%xmm9, %xmm2
	addss	32(%rsp), %xmm3
	movss	%xmm3, 32(%rsp)
	addss	36(%rsp), %xmm1
	movss	%xmm1, 36(%rsp)
	addss	40(%rsp), %xmm2
	movss	%xmm2, 40(%rsp)
	divss	%xmm6, %xmm8
	movss	112(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm8
	movb	$1, %r14b
	movl	$1, %eax
	jmp	.LBB3_42
.LBB3_41:
	xorps	%xmm8, %xmm8
	movl	$2, %eax
	xorl	%r14d, %r14d
	movss	112(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB3_42:                               # %.sink.split
	movl	%eax, 80(%r15)
	cmpl	$0, 92(%r15)
	je	.LBB3_48
.LBB3_43:
	cmpq	$0, 24(%r15)
	je	.LBB3_48
# BB#44:
	cmpl	$0, 88(%r15)
	je	.LBB3_48
# BB#45:
	movaps	%xmm3, %xmm0
	addss	%xmm8, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	.LCPI3_9(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	seta	%al
	testb	%r14b, %r14b
	jne	.LBB3_49
	jmp	.LBB3_50
.LBB3_48:
	xorl	%eax, %eax
	testb	%r14b, %r14b
	je	.LBB3_50
.LBB3_49:
	testb	%al, %al
	je	.LBB3_72
.LBB3_50:
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB3_72
# BB#51:
	movss	%xmm3, 112(%rsp)        # 4-byte Spill
	movss	%xmm8, 4(%rsp)          # 4-byte Spill
	incl	gNumDeepPenetrationChecks(%rip)
	xorps	%xmm0, %xmm0
	movq	104(%rsp), %rbp         # 8-byte Reload
	movups	%xmm0, (%rbp)
	movq	(%rdi), %r10
	movq	32(%r15), %rsi
	movq	40(%r15), %rdx
	movq	48(%r15), %rcx
	subq	$8, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	leaq	232(%rsp), %r8
	leaq	168(%rsp), %r9
	pushq	136(%rbx)
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	320(%rsp)               # 8-byte Folded Reload
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	leaq	112(%rsp), %rax
	pushq	%rax
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	leaq	104(%rsp), %rax
	pushq	%rax
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	callq	*16(%r10)
	addq	$48, %rsp
.Lcfi24:
	.cfi_adjust_cfa_offset -48
	testb	%al, %al
	je	.LBB3_54
# BB#52:
	movsd	88(%rsp), %xmm4         # xmm4 = mem[0],zero
	movsd	72(%rsp), %xmm0         # xmm0 = mem[0],zero
	subps	%xmm0, %xmm4
	movaps	%xmm4, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movss	96(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	80(%rsp), %xmm1
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm1, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	movss	.LCPI3_7(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	jae	.LBB3_62
# BB#53:
	xorps	%xmm3, %xmm3
	movss	%xmm1, %xmm3            # xmm3 = xmm1[0],xmm3[1,2,3]
	jmp	.LBB3_63
.LBB3_54:
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	16(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	jbe	.LBB3_72
# BB#55:
	movss	72(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	76(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	88(%rsp), %xmm1
	subss	92(%rsp), %xmm2
	movss	80(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	96(%rsp), %xmm0
	mulss	%xmm1, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	sqrtss	%xmm0, %xmm7
	ucomiss	%xmm7, %xmm7
	jnp	.LBB3_57
# BB#56:                                # %call.sqrt339
	callq	sqrtf
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm7
.LBB3_57:                               # %.split338
	testb	%r14b, %r14b
	subss	112(%rsp), %xmm7        # 4-byte Folded Reload
	je	.LBB3_59
# BB#58:                                # %.split338
	movl	$5, %eax
	ucomiss	%xmm7, %xmm8
	jbe	.LBB3_71
.LBB3_59:
	movups	72(%rsp), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	88(%rsp), %xmm0
	movaps	%xmm0, 32(%rsp)
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	68(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm2
	mulss	%xmm0, %xmm2
	movaps	%xmm6, %xmm3
	mulss	%xmm1, %xmm3
	movss	16(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm6
	movss	48(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm5
	movss	%xmm5, 48(%rsp)
	movss	52(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
	movss	%xmm2, 52(%rsp)
	movss	56(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm2
	movss	%xmm2, 56(%rsp)
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	mulss	%xmm2, %xmm1
	mulss	%xmm4, %xmm2
	addss	32(%rsp), %xmm0
	movss	%xmm0, 32(%rsp)
	addss	36(%rsp), %xmm1
	movss	%xmm1, 36(%rsp)
	addss	40(%rsp), %xmm2
	movss	%xmm2, 40(%rsp)
	movq	104(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm0
	movaps	%xmm0, 16(%rsp)
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	24(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB3_61
# BB#60:                                # %call.sqrt341
	movss	%xmm7, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm7          # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB3_61:                               # %.split340
	movss	.LCPI3_8(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	16(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 16(%rsp)
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 20(%rsp)
	mulss	24(%rsp), %xmm0
	movss	%xmm0, 24(%rsp)
	movb	$1, %r14b
	movl	$6, %eax
	movaps	%xmm7, %xmm8
	jmp	.LBB3_71
.LBB3_62:
	movsd	8(%r15), %xmm4          # xmm4 = mem[0],zero
	movsd	16(%r15), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	movss	12(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
.LBB3_63:
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movl	$9, %eax
	ucomiss	.LCPI3_7(%rip), %xmm0
	jbe	.LBB3_71
# BB#64:
	sqrtss	%xmm0, %xmm5
	ucomiss	%xmm5, %xmm5
	jnp	.LBB3_66
# BB#65:                                # %call.sqrt336
	movaps	%xmm3, 112(%rsp)        # 16-byte Spill
	movaps	%xmm4, 144(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	144(%rsp), %xmm4        # 16-byte Reload
	movaps	112(%rsp), %xmm3        # 16-byte Reload
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm5
.LBB3_66:                               # %.split335
	movss	72(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	76(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	88(%rsp), %xmm0
	subss	92(%rsp), %xmm2
	movss	80(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	96(%rsp), %xmm1
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB3_68
# BB#67:                                # %call.sqrt337
	movaps	%xmm1, %xmm0
	movaps	%xmm3, 112(%rsp)        # 16-byte Spill
	movaps	%xmm4, 144(%rsp)        # 16-byte Spill
	movss	%xmm5, 12(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	144(%rsp), %xmm4        # 16-byte Reload
	movaps	112(%rsp), %xmm3        # 16-byte Reload
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
.LBB3_68:                               # %.split335.split
	testb	%r14b, %r14b
	xorps	.LCPI3_3(%rip), %xmm0
	je	.LBB3_70
# BB#69:                                # %.split335.split
	movl	$8, %eax
	ucomiss	%xmm0, %xmm8
	jbe	.LBB3_71
.LBB3_70:
	movss	.LCPI3_8(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm5, %xmm1
	mulss	%xmm1, %xmm3
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm4
	movups	72(%rsp), %xmm1
	movaps	%xmm1, 48(%rsp)
	movups	88(%rsp), %xmm1
	movaps	%xmm1, 32(%rsp)
	movlps	%xmm4, 16(%rsp)
	movlps	%xmm3, 24(%rsp)
	movb	$1, %r14b
	movl	$3, %eax
	movaps	%xmm0, %xmm8
.LBB3_71:                               # %.sink.split330
	movl	%eax, 80(%r15)
.LBB3_72:
	testb	%r14b, %r14b
	je	.LBB3_76
# BB#73:
	xorps	%xmm0, %xmm0
	ucomiss	%xmm8, %xmm0
	ja	.LBB3_75
# BB#74:
	movaps	%xmm8, %xmm0
	mulss	%xmm0, %xmm0
	movss	128(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB3_76
.LBB3_75:
	movaps	16(%rsp), %xmm0
	movq	104(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, (%rax)
	movss	%xmm8, 76(%r15)
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	movaps	336(%rsp), %xmm1        # 16-byte Reload
	addps	32(%rsp), %xmm1
	movss	140(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	addss	40(%rsp), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, 72(%rsp)
	movlps	%xmm0, 80(%rsp)
	leaq	16(%rsp), %rsi
	leaq	72(%rsp), %rdx
	movaps	%xmm8, %xmm0
	callq	*%rax
.LBB3_76:
	addq	$360, %rsp              # imm = 0x168
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN17btGjkPairDetector26getClosestPointsNonVirtualERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDraw, .Lfunc_end3-_ZN17btGjkPairDetector26getClosestPointsNonVirtualERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDraw
	.cfi_endproc

	.section	.text._ZN36btDiscreteCollisionDetectorInterfaceD2Ev,"axG",@progbits,_ZN36btDiscreteCollisionDetectorInterfaceD2Ev,comdat
	.weak	_ZN36btDiscreteCollisionDetectorInterfaceD2Ev
	.p2align	4, 0x90
	.type	_ZN36btDiscreteCollisionDetectorInterfaceD2Ev,@function
_ZN36btDiscreteCollisionDetectorInterfaceD2Ev: # @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end4:
	.size	_ZN36btDiscreteCollisionDetectorInterfaceD2Ev, .Lfunc_end4-_ZN36btDiscreteCollisionDetectorInterfaceD2Ev
	.cfi_endproc

	.section	.text._ZN17btGjkPairDetectorD0Ev,"axG",@progbits,_ZN17btGjkPairDetectorD0Ev,comdat
	.weak	_ZN17btGjkPairDetectorD0Ev
	.p2align	4, 0x90
	.type	_ZN17btGjkPairDetectorD0Ev,@function
_ZN17btGjkPairDetectorD0Ev:             # @_ZN17btGjkPairDetectorD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end5:
	.size	_ZN17btGjkPairDetectorD0Ev, .Lfunc_end5-_ZN17btGjkPairDetectorD0Ev
	.cfi_endproc

	.type	gNumDeepPenetrationChecks,@object # @gNumDeepPenetrationChecks
	.bss
	.globl	gNumDeepPenetrationChecks
	.p2align	2
gNumDeepPenetrationChecks:
	.long	0                       # 0x0
	.size	gNumDeepPenetrationChecks, 4

	.type	gNumGjkChecks,@object   # @gNumGjkChecks
	.globl	gNumGjkChecks
	.p2align	2
gNumGjkChecks:
	.long	0                       # 0x0
	.size	gNumGjkChecks, 4

	.type	_ZTV17btGjkPairDetector,@object # @_ZTV17btGjkPairDetector
	.section	.rodata,"a",@progbits
	.globl	_ZTV17btGjkPairDetector
	.p2align	3
_ZTV17btGjkPairDetector:
	.quad	0
	.quad	_ZTI17btGjkPairDetector
	.quad	_ZN36btDiscreteCollisionDetectorInterfaceD2Ev
	.quad	_ZN17btGjkPairDetectorD0Ev
	.quad	_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
	.size	_ZTV17btGjkPairDetector, 40

	.type	_ZTS17btGjkPairDetector,@object # @_ZTS17btGjkPairDetector
	.globl	_ZTS17btGjkPairDetector
	.p2align	4
_ZTS17btGjkPairDetector:
	.asciz	"17btGjkPairDetector"
	.size	_ZTS17btGjkPairDetector, 20

	.type	_ZTS36btDiscreteCollisionDetectorInterface,@object # @_ZTS36btDiscreteCollisionDetectorInterface
	.section	.rodata._ZTS36btDiscreteCollisionDetectorInterface,"aG",@progbits,_ZTS36btDiscreteCollisionDetectorInterface,comdat
	.weak	_ZTS36btDiscreteCollisionDetectorInterface
	.p2align	4
_ZTS36btDiscreteCollisionDetectorInterface:
	.asciz	"36btDiscreteCollisionDetectorInterface"
	.size	_ZTS36btDiscreteCollisionDetectorInterface, 39

	.type	_ZTI36btDiscreteCollisionDetectorInterface,@object # @_ZTI36btDiscreteCollisionDetectorInterface
	.section	.rodata._ZTI36btDiscreteCollisionDetectorInterface,"aG",@progbits,_ZTI36btDiscreteCollisionDetectorInterface,comdat
	.weak	_ZTI36btDiscreteCollisionDetectorInterface
	.p2align	3
_ZTI36btDiscreteCollisionDetectorInterface:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS36btDiscreteCollisionDetectorInterface
	.size	_ZTI36btDiscreteCollisionDetectorInterface, 16

	.type	_ZTI17btGjkPairDetector,@object # @_ZTI17btGjkPairDetector
	.section	.rodata,"a",@progbits
	.globl	_ZTI17btGjkPairDetector
	.p2align	4
_ZTI17btGjkPairDetector:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS17btGjkPairDetector
	.quad	_ZTI36btDiscreteCollisionDetectorInterface
	.size	_ZTI17btGjkPairDetector, 24


	.globl	_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.type	_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver,@function
_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver = _ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.globl	_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.type	_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver,@function
_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver = _ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
