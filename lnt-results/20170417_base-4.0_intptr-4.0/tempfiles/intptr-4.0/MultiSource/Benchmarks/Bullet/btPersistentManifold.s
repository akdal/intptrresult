	.text
	.file	"btPersistentManifold.bc"
	.globl	_ZN20btPersistentManifoldC2Ev
	.p2align	4, 0x90
	.type	_ZN20btPersistentManifoldC2Ev,@function
_ZN20btPersistentManifoldC2Ev:          # @_ZN20btPersistentManifoldC2Ev
	.cfi_startproc
# BB#0:
	movl	$1, (%rdi)
	movq	$0, 120(%rdi)
	movl	$0, 128(%rdi)
	movb	$0, 132(%rdi)
	movl	$0, 136(%rdi)
	movl	$0, 140(%rdi)
	movl	$0, 144(%rdi)
	movq	$0, 296(%rdi)
	movl	$0, 304(%rdi)
	movb	$0, 308(%rdi)
	movl	$0, 312(%rdi)
	movl	$0, 316(%rdi)
	movl	$0, 320(%rdi)
	movq	$0, 472(%rdi)
	movl	$0, 480(%rdi)
	movb	$0, 484(%rdi)
	movl	$0, 488(%rdi)
	movl	$0, 492(%rdi)
	movl	$0, 496(%rdi)
	movq	$0, 648(%rdi)
	movl	$0, 656(%rdi)
	movb	$0, 660(%rdi)
	movl	$0, 664(%rdi)
	movl	$0, 668(%rdi)
	movl	$0, 672(%rdi)
	movl	$0, 740(%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 712(%rdi)
	movl	$0, 728(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN20btPersistentManifoldC2Ev, .Lfunc_end0-_ZN20btPersistentManifoldC2Ev
	.cfi_endproc

	.globl	_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint
	.p2align	4, 0x90
	.type	_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint,@function
_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint: # @_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_3
# BB#1:
	movq	gContactDestroyedCallback(%rip), %rax
	testq	%rax, %rax
	je	.LBB1_3
# BB#2:
	callq	*%rax
	movq	$0, 112(%rbx)
.LBB1_3:
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint, .Lfunc_end1-_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_1:
	.long	3713928043              # float -9.99999984E+17
	.text
	.globl	_ZN20btPersistentManifold16sortCachedPointsERK15btManifoldPoint
	.p2align	4, 0x90
	.type	_ZN20btPersistentManifold16sortCachedPointsERK15btManifoldPoint,@function
_ZN20btPersistentManifold16sortCachedPointsERK15btManifoldPoint: # @_ZN20btPersistentManifold16sortCachedPointsERK15btManifoldPoint
	.cfi_startproc
# BB#0:
	movss	(%rsi), %xmm11          # xmm11 = mem[0],zero,zero,zero
	movss	80(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	88(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	264(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm0
	minss	%xmm0, %xmm1
	movl	$-1, %ecx
	cmoval	%eax, %ecx
	ucomiss	%xmm2, %xmm1
	minss	%xmm1, %xmm2
	movl	$1, %eax
	cmovbel	%ecx, %eax
	movss	440(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm2
	minss	%xmm2, %xmm4
	movl	$2, %ecx
	cmovbel	%eax, %ecx
	movss	616(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, -4(%rsp)         # 4-byte Spill
	ucomiss	%xmm0, %xmm4
	movl	$3, %eax
	cmovbel	%ecx, %eax
	testl	%eax, %eax
	movss	%xmm11, -12(%rsp)       # 4-byte Spill
	je	.LBB2_1
# BB#2:
	movss	188(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm2
	movss	%xmm0, -20(%rsp)        # 4-byte Spill
	subss	%xmm0, %xmm2
	movss	192(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm6
	movss	%xmm0, -24(%rsp)        # 4-byte Spill
	subss	%xmm0, %xmm6
	movss	536(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	360(%rdi), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm8
	subss	%xmm10, %xmm3
	movss	540(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	364(%rdi), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm13
	movss	%xmm5, -36(%rsp)        # 4-byte Spill
	subss	%xmm5, %xmm13
	movss	544(%rdi), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	368(%rdi), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm14
	subss	%xmm12, %xmm7
	movaps	%xmm6, %xmm5
	mulss	%xmm3, %xmm6
	mulss	%xmm2, %xmm3
	mulss	%xmm7, %xmm2
	mulss	%xmm13, %xmm5
	subss	%xmm5, %xmm2
	movss	184(%rdi), %xmm15       # xmm15 = mem[0],zero,zero,zero
	subss	%xmm15, %xmm11
	mulss	%xmm11, %xmm7
	subss	%xmm7, %xmm6
	mulss	%xmm11, %xmm13
	subss	%xmm3, %xmm13
	mulss	%xmm2, %xmm2
	mulss	%xmm6, %xmm6
	addss	%xmm2, %xmm6
	mulss	%xmm13, %xmm13
	addss	%xmm6, %xmm13
	cmpl	$1, %eax
	jne	.LBB2_3
# BB#8:                                 # %._crit_edge182
	movss	8(%rdi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, -32(%rsp)        # 4-byte Spill
	movss	12(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, -28(%rsp)        # 4-byte Spill
	movss	16(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm6, %xmm6
	movss	-12(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm11
	movaps	%xmm14, %xmm15
	movaps	%xmm2, %xmm14
	jmp	.LBB2_5
.LBB2_1:                                # %._crit_edge
	movss	%xmm4, -16(%rsp)        # 4-byte Spill
	movss	4(%rsi), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	536(%rdi), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movss	360(%rdi), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movss	364(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	540(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	544(%rdi), %xmm15       # xmm15 = mem[0],zero,zero,zero
	movss	368(%rdi), %xmm12       # xmm12 = mem[0],zero,zero,zero
	xorps	%xmm13, %xmm13
	jmp	.LBB2_4
.LBB2_3:
	movss	%xmm4, -16(%rsp)        # 4-byte Spill
	movaps	%xmm14, %xmm15
	movss	-36(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm5
.LBB2_4:
	movss	%xmm5, -8(%rsp)         # 4-byte Spill
	movss	8(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm3
	movss	%xmm2, -28(%rsp)        # 4-byte Spill
	subss	%xmm2, %xmm3
	movss	16(%rdi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm2
	subss	%xmm14, %xmm2
	movaps	%xmm8, %xmm5
	subss	%xmm10, %xmm5
	movaps	%xmm0, %xmm6
	movss	%xmm4, -36(%rsp)        # 4-byte Spill
	subss	%xmm4, %xmm6
	movaps	%xmm15, %xmm7
	subss	%xmm12, %xmm7
	movaps	%xmm2, %xmm4
	mulss	%xmm5, %xmm2
	mulss	%xmm3, %xmm5
	mulss	%xmm7, %xmm3
	mulss	%xmm6, %xmm4
	subss	%xmm4, %xmm3
	movaps	%xmm12, %xmm11
	movaps	%xmm10, %xmm12
	movaps	%xmm9, %xmm10
	movss	-12(%rsp), %xmm9        # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm4
	movss	%xmm1, -32(%rsp)        # 4-byte Spill
	subss	%xmm1, %xmm4
	mulss	%xmm4, %xmm7
	subss	%xmm7, %xmm2
	movaps	%xmm9, %xmm7
	movaps	%xmm10, %xmm9
	movaps	%xmm12, %xmm10
	movaps	%xmm11, %xmm12
	mulss	%xmm4, %xmm6
	subss	%xmm5, %xmm6
	mulss	%xmm3, %xmm3
	mulss	%xmm2, %xmm2
	addss	%xmm3, %xmm2
	mulss	%xmm6, %xmm6
	addss	%xmm2, %xmm6
	cmpl	$2, %eax
	movss	184(%rdi), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movss	188(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, -20(%rsp)        # 4-byte Spill
	movss	192(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, -24(%rsp)        # 4-byte Spill
	xorps	%xmm2, %xmm2
	movss	-16(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	-8(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	je	.LBB2_6
.LBB2_5:                                # %._crit_edge189
	ucomiss	-4(%rsp), %xmm4         # 4-byte Folded Reload
	movaps	%xmm9, %xmm2
	subss	-28(%rsp), %xmm2        # 4-byte Folded Reload
	movaps	%xmm1, %xmm5
	subss	%xmm14, %xmm5
	subss	%xmm11, %xmm8
	subss	-20(%rsp), %xmm0        # 4-byte Folded Reload
	subss	-24(%rsp), %xmm15       # 4-byte Folded Reload
	movaps	%xmm5, %xmm3
	mulss	%xmm8, %xmm5
	mulss	%xmm2, %xmm8
	mulss	%xmm15, %xmm2
	mulss	%xmm0, %xmm3
	subss	%xmm3, %xmm2
	movaps	%xmm7, %xmm3
	subss	-32(%rsp), %xmm3        # 4-byte Folded Reload
	mulss	%xmm3, %xmm15
	subss	%xmm15, %xmm5
	mulss	%xmm3, %xmm0
	subss	%xmm8, %xmm0
	mulss	%xmm2, %xmm2
	mulss	%xmm5, %xmm5
	addss	%xmm2, %xmm5
	mulss	%xmm0, %xmm0
	addss	%xmm5, %xmm0
	xorps	%xmm3, %xmm3
	movaps	%xmm0, %xmm2
	ja	.LBB2_7
.LBB2_6:                                # %._crit_edge232
	subss	-32(%rsp), %xmm7        # 4-byte Folded Reload
	subss	-28(%rsp), %xmm9        # 4-byte Folded Reload
	subss	%xmm14, %xmm1
	subss	%xmm11, %xmm10
	movss	-36(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	subss	-20(%rsp), %xmm4        # 4-byte Folded Reload
	subss	-24(%rsp), %xmm12       # 4-byte Folded Reload
	movaps	%xmm9, %xmm0
	mulss	%xmm12, %xmm0
	movaps	%xmm1, %xmm3
	mulss	%xmm4, %xmm3
	subss	%xmm3, %xmm0
	mulss	%xmm10, %xmm1
	mulss	%xmm7, %xmm12
	subss	%xmm12, %xmm1
	mulss	%xmm7, %xmm4
	mulss	%xmm9, %xmm10
	subss	%xmm10, %xmm4
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm4, %xmm4
	addss	%xmm1, %xmm4
	movaps	%xmm4, %xmm3
	movaps	%xmm2, %xmm0
.LBB2_7:
	movaps	.LCPI2_0(%rip), %xmm2   # xmm2 = [nan,nan,nan,nan]
	andps	%xmm2, %xmm13
	andps	%xmm2, %xmm6
	andps	%xmm2, %xmm0
	andps	%xmm2, %xmm3
	xorl	%eax, %eax
	movss	.LCPI2_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm13
	movl	$-1, %ecx
	cmoval	%eax, %ecx
	maxss	%xmm2, %xmm13
	ucomiss	%xmm13, %xmm6
	movl	$1, %eax
	cmovbel	%ecx, %eax
	maxss	%xmm13, %xmm6
	ucomiss	%xmm6, %xmm0
	movl	$2, %ecx
	cmovbel	%eax, %ecx
	maxss	%xmm6, %xmm0
	ucomiss	%xmm0, %xmm3
	movl	$3, %eax
	cmovbel	%ecx, %eax
	retq
.Lfunc_end2:
	.size	_ZN20btPersistentManifold16sortCachedPointsERK15btManifoldPoint, .Lfunc_end2-_ZN20btPersistentManifold16sortCachedPointsERK15btManifoldPoint
	.cfi_endproc

	.globl	_ZNK20btPersistentManifold13getCacheEntryERK15btManifoldPoint
	.p2align	4, 0x90
	.type	_ZNK20btPersistentManifold13getCacheEntryERK15btManifoldPoint,@function
_ZNK20btPersistentManifold13getCacheEntryERK15btManifoldPoint: # @_ZNK20btPersistentManifold13getCacheEntryERK15btManifoldPoint
	.cfi_startproc
# BB#0:
	movl	728(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.LBB3_1
# BB#3:                                 # %.lr.ph
	movss	732(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm3
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	addq	$16, %rdi
	movl	$-1, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_4:                                # =>This Inner Loop Header: Depth=1
	movss	-8(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	-4(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm4
	subss	%xmm1, %xmm5
	movss	(%rdi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm6
	mulss	%xmm4, %xmm4
	mulss	%xmm5, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm6, %xmm6
	addss	%xmm5, %xmm6
	ucomiss	%xmm6, %xmm3
	cmoval	%edx, %eax
	minss	%xmm3, %xmm6
	incq	%rdx
	addq	$176, %rdi
	cmpq	%rdx, %rcx
	movaps	%xmm6, %xmm3
	jne	.LBB3_4
	jmp	.LBB3_2
.LBB3_1:
	movl	$-1, %eax
.LBB3_2:                                # %._crit_edge
	retq
.Lfunc_end3:
	.size	_ZNK20btPersistentManifold13getCacheEntryERK15btManifoldPoint, .Lfunc_end3-_ZNK20btPersistentManifold13getCacheEntryERK15btManifoldPoint
	.cfi_endproc

	.globl	_ZNK20btPersistentManifold27getContactBreakingThresholdEv
	.p2align	4, 0x90
	.type	_ZNK20btPersistentManifold27getContactBreakingThresholdEv,@function
_ZNK20btPersistentManifold27getContactBreakingThresholdEv: # @_ZNK20btPersistentManifold27getContactBreakingThresholdEv
	.cfi_startproc
# BB#0:
	movss	732(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end4:
	.size	_ZNK20btPersistentManifold27getContactBreakingThresholdEv, .Lfunc_end4-_ZNK20btPersistentManifold27getContactBreakingThresholdEv
	.cfi_endproc

	.globl	_ZN20btPersistentManifold16addManifoldPointERK15btManifoldPoint
	.p2align	4, 0x90
	.type	_ZN20btPersistentManifold16addManifoldPointERK15btManifoldPoint,@function
_ZN20btPersistentManifold16addManifoldPointERK15btManifoldPoint: # @_ZN20btPersistentManifold16addManifoldPointERK15btManifoldPoint
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 48
.Lcfi7:
	.cfi_offset %rbx, -40
.Lcfi8:
	.cfi_offset %r12, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	728(%r15), %r12d
	cmpl	$4, %r12d
	jne	.LBB5_4
# BB#1:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_ZN20btPersistentManifold16sortCachedPointsERK15btManifoldPoint
	movl	%eax, %r12d
	movslq	%r12d, %rax
	imulq	$176, %rax, %rax
	movq	120(%r15,%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_5
# BB#2:
	movq	gContactDestroyedCallback(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB5_5
# BB#3:
	leaq	120(%r15,%rax), %rbx
	callq	*%rcx
	movq	$0, (%rbx)
	jmp	.LBB5_5
.LBB5_4:
	leal	1(%r12), %eax
	movl	%eax, 728(%r15)
.LBB5_5:                                # %_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint.exit
	xorl	%ebx, %ebx
	testl	%r12d, %r12d
	cmovnsl	%r12d, %ebx
	imulq	$176, %rbx, %rax
	leaq	8(%r15,%rax), %rdi
	movl	$172, %edx
	movq	%r14, %rsi
	callq	memcpy
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	_ZN20btPersistentManifold16addManifoldPointERK15btManifoldPoint, .Lfunc_end5-_ZN20btPersistentManifold16addManifoldPointERK15btManifoldPoint
	.cfi_endproc

	.globl	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_
	.p2align	4, 0x90
	.type	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_,@function
_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_: # @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -40
.Lcfi17:
	.cfi_offset %r12, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movslq	728(%r14), %rax
	testq	%rax, %rax
	jle	.LBB6_15
# BB#1:                                 # %.lr.ph104
	imulq	$176, %rax, %rcx
	incq	%rax
	leaq	-168(%r14,%rcx), %rcx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movss	(%rcx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	16(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movaps	%xmm3, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	movss	20(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm1, %xmm4
	movss	24(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	movaps	%xmm0, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm5, %xmm6
	addps	%xmm4, %xmm6
	movsd	48(%rsi), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm6, %xmm1
	mulss	32(%rsi), %xmm3
	mulss	36(%rsi), %xmm2
	addss	%xmm3, %xmm2
	mulss	40(%rsi), %xmm0
	addss	%xmm2, %xmm0
	addss	56(%rsi), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 48(%rcx)
	movlps	%xmm2, 56(%rcx)
	movss	16(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	24(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	16(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm6, %xmm5
	movss	20(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1]
	movaps	%xmm3, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm7, %xmm6
	addps	%xmm5, %xmm6
	movss	24(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm7    # xmm7 = xmm7[0],xmm5[0],xmm7[1],xmm5[1]
	movaps	%xmm2, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm7, %xmm5
	addps	%xmm6, %xmm5
	movsd	48(%rdx), %xmm6         # xmm6 = mem[0],zero
	addps	%xmm5, %xmm6
	mulss	32(%rdx), %xmm4
	mulss	36(%rdx), %xmm3
	addss	%xmm4, %xmm3
	mulss	40(%rdx), %xmm2
	addss	%xmm3, %xmm2
	addss	56(%rdx), %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movlps	%xmm6, 32(%rcx)
	movlps	%xmm3, 40(%rcx)
	subss	%xmm6, %xmm1
	movss	52(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	36(%rcx), %xmm3
	subss	%xmm2, %xmm0
	mulss	64(%rcx), %xmm1
	mulss	68(%rcx), %xmm3
	addss	%xmm1, %xmm3
	mulss	72(%rcx), %xmm0
	addss	%xmm3, %xmm0
	movss	%xmm0, 80(%rcx)
	incl	136(%rcx)
	decq	%rax
	addq	$-176, %rcx
	cmpq	$1, %rax
	jg	.LBB6_2
# BB#3:                                 # %._crit_edge105
	movslq	728(%r14), %r15
	testq	%r15, %r15
	jle	.LBB6_15
# BB#4:                                 # %.lr.ph
	imulq	$176, %r15, %rax
	incq	%r15
	leaq	-168(%r14,%rax), %rbx
	jmp	.LBB6_5
	.p2align	4, 0x90
.LBB6_13:                               #   in Loop: Header=BB6_5 Depth=1
	movq	gContactProcessedCallback(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB6_14
# BB#16:                                #   in Loop: Header=BB6_5 Depth=1
	movq	712(%r14), %rsi
	movq	720(%r14), %rdx
	movq	%rbx, %rdi
	callq	*%rcx
	jmp	.LBB6_14
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	leaq	-2(%r15), %r12
	movss	80(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	732(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jb	.LBB6_6
# BB#12:                                #   in Loop: Header=BB6_5 Depth=1
	movss	64(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	68(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	mulss	72(%rbx), %xmm1
	movss	48(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm4
	movss	52(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
	movss	56(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm3
	movss	32(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	36(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	subss	%xmm2, %xmm5
	movss	40(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
	mulss	%xmm1, %xmm1
	mulss	%xmm5, %xmm5
	addss	%xmm1, %xmm5
	mulss	%xmm2, %xmm2
	addss	%xmm5, %xmm2
	mulss	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm2
	jbe	.LBB6_13
.LBB6_6:                                #   in Loop: Header=BB6_5 Depth=1
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_9
# BB#7:                                 #   in Loop: Header=BB6_5 Depth=1
	movq	gContactDestroyedCallback(%rip), %rax
	testq	%rax, %rax
	je	.LBB6_9
# BB#8:                                 #   in Loop: Header=BB6_5 Depth=1
	callq	*%rax
	movq	$0, 112(%rbx)
.LBB6_9:                                # %_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint.exit.i74
                                        #   in Loop: Header=BB6_5 Depth=1
	movslq	728(%r14), %rax
	movq	%rax, %rcx
	decq	%rcx
	movl	%ecx, %edx
	cmpq	%rdx, %r12
	je	.LBB6_11
# BB#10:                                #   in Loop: Header=BB6_5 Depth=1
	imulq	$176, %rcx, %r12
	leaq	8(%r14,%r12), %rsi
	movl	$172, %edx
	movq	%rbx, %rdi
	callq	memcpy
	movq	$0, 120(%r14,%r12)
	movl	$0, 128(%r14,%r12)
	movb	$0, 132(%r14,%r12)
	movl	$0, 136(%r14,%r12)
	movq	$0, 140(%r14,%r12)
	movl	728(%r14), %eax
.LBB6_11:                               # %_ZN20btPersistentManifold18removeContactPointEi.exit76
                                        #   in Loop: Header=BB6_5 Depth=1
	decl	%eax
	movl	%eax, 728(%r14)
.LBB6_14:                               # %.backedge
                                        #   in Loop: Header=BB6_5 Depth=1
	decq	%r15
	addq	$-176, %rbx
	cmpq	$1, %r15
	jg	.LBB6_5
.LBB6_15:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_, .Lfunc_end6-_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_
	.cfi_endproc

	.type	gContactBreakingThreshold,@object # @gContactBreakingThreshold
	.data
	.globl	gContactBreakingThreshold
	.p2align	2
gContactBreakingThreshold:
	.long	1017370378              # float 0.0199999996
	.size	gContactBreakingThreshold, 4

	.type	gContactDestroyedCallback,@object # @gContactDestroyedCallback
	.bss
	.globl	gContactDestroyedCallback
	.p2align	3
gContactDestroyedCallback:
	.quad	0
	.size	gContactDestroyedCallback, 8

	.type	gContactProcessedCallback,@object # @gContactProcessedCallback
	.globl	gContactProcessedCallback
	.p2align	3
gContactProcessedCallback:
	.quad	0
	.size	gContactProcessedCallback, 8


	.globl	_ZN20btPersistentManifoldC1Ev
	.type	_ZN20btPersistentManifoldC1Ev,@function
_ZN20btPersistentManifoldC1Ev = _ZN20btPersistentManifoldC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
