	.text
	.file	"psub.bc"
	.globl	psub
	.p2align	4, 0x90
	.type	psub,@function
psub:                                   # @psub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	incw	(%rbx)
.LBB0_2:
	testq	%r15, %r15
	je	.LBB0_4
# BB#3:
	incw	(%r15)
.LBB0_4:
	movb	6(%r15), %al
	cmpb	%al, 6(%rbx)
	jne	.LBB0_5
# BB#6:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	pcmp
	movl	%eax, %ebp
	negl	%ebp
	cmpb	$0, 6(%rbx)
	cmovel	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB0_8
# BB#7:
	movq	%rbx, %r14
	jmp	.LBB0_9
.LBB0_5:                                # %.thread
	movq	$0, (%rsp)
	testb	%al, %al
	sete	6(%r15)
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	padd
	movq	%rsp, %rdi
	movq	%rax, %rsi
	callq	psetq
	cmpb	$0, 6(%r15)
	sete	6(%r15)
	movq	%rbx, %r14
	decw	(%r14)
	jne	.LBB0_31
	jmp	.LBB0_30
.LBB0_8:
	movq	%rbx, (%rsp)
	movq	%r15, %r14
	movq	%rbx, %r15
.LBB0_9:
	movzwl	4(%r14), %edi
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	callq	palloc
	movq	%rax, (%rsp)
	testq	%rax, %rax
	je	.LBB0_35
# BB#10:
	movb	6(%r14), %cl
	testb	%cl, %cl
	sete	%dl
	testl	%ebp, %ebp
	js	.LBB0_12
# BB#11:
	movl	%ecx, %edx
.LBB0_12:
	movb	%dl, 6(%rax)
	movzwl	4(%r15), %ecx
	leaq	8(%r15,%rcx,2), %r8
	leaq	10(%r15), %rdi
	cmpq	%rdi, %r8
	movq	%rdi, %rcx
	cmovaq	%r8, %rcx
	movq	$-9, %rdx
	subq	%r15, %rdx
	addq	%rcx, %rdx
	movq	%rdx, %r9
	shrq	%r9
	btl	$1, %edx
	jb	.LBB0_13
# BB#14:
	movzwl	8(%r15), %ecx
	xorl	$65535, %ecx            # imm = 0xFFFF
	leaq	10(%r14), %rbx
	movzwl	8(%r14), %edx
	leal	1(%rcx,%rdx), %ecx
	movw	%cx, 8(%rax)
	movl	%ecx, %edx
	shrl	$16, %edx
	leaq	10(%rax), %rcx
	jmp	.LBB0_15
.LBB0_13:
	leaq	8(%r14), %rbx
	leaq	8(%rax), %rcx
	leaq	8(%r15), %rdi
	movl	$1, %edx
.LBB0_15:                               # %.prol.loopexit
	addq	$10, %rax
	testq	%r9, %r9
	je	.LBB0_17
	.p2align	4, 0x90
.LBB0_16:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%rdi), %ebp
	xorl	$65535, %ebp            # imm = 0xFFFF
	movzwl	(%rbx), %esi
	addl	%edx, %ebp
	addl	%esi, %ebp
	movw	%bp, (%rcx)
	shrl	$16, %ebp
	movzwl	2(%rdi), %edx
	xorl	$65535, %edx            # imm = 0xFFFF
	movzwl	2(%rbx), %esi
	addl	%ebp, %edx
	addl	%esi, %edx
	movw	%dx, 2(%rcx)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	shrl	$16, %edx
	addq	$4, %rdi
	addq	$4, %rbx
	addq	$4, %rcx
	cmpq	%r8, %rdi
	jb	.LBB0_16
.LBB0_17:
	leaq	10(%r14,%r9,2), %rdi
	leaq	(%rax,%r9,2), %rsi
	movzwl	4(%r14), %ecx
	leaq	8(%r14,%rcx,2), %rbx
	cmpq	%rbx, %rdi
	jae	.LBB0_25
# BB#18:                                # %.lr.ph.preheader
	leaq	7(%rcx,%rcx), %r8
	subq	%rdi, %r8
	addq	%r14, %r8
	shrq	%r8
	leaq	-3(%rcx,%rcx), %r10
	addq	%r9, %r9
	subq	%r9, %r10
	movl	%r10d, %ecx
	shrl	%ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB0_19
# BB#20:                                # %.lr.ph.prol.preheader
	leaq	10(%r14), %rdi
	negq	%rcx
	.p2align	4, 0x90
.LBB0_21:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%r9,%rdi), %ebp
	leal	65535(%rdx,%rbp), %edx
	movw	%dx, (%r9,%rax)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	shrl	$16, %edx
	addq	$2, %rdi
	addq	$2, %rax
	incq	%rcx
	jne	.LBB0_21
# BB#22:                                # %.lr.ph.prol.loopexit.unr-lcssa
	addq	%r9, %rdi
	addq	%r9, %rax
	jmp	.LBB0_23
.LBB0_19:
	movq	%rsi, %rax
.LBB0_23:                               # %.lr.ph.prol.loopexit
	incq	%r8
	cmpq	$6, %r10
	jb	.LBB0_24
	.p2align	4, 0x90
.LBB0_36:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rdi), %ecx
	leal	65535(%rdx,%rcx), %ecx
	movw	%cx, (%rax)
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	shrl	$16, %ecx
	movzwl	2(%rdi), %edx
	leal	65535(%rcx,%rdx), %ecx
	movw	%cx, 2(%rax)
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	shrl	$16, %ecx
	movzwl	4(%rdi), %edx
	leal	65535(%rcx,%rdx), %ecx
	movw	%cx, 4(%rax)
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	shrl	$16, %ecx
	movzwl	6(%rdi), %edx
	leal	65535(%rcx,%rdx), %ecx
	movw	%cx, 6(%rax)
	movl	%ecx, %edx
	shrl	$16, %edx
	addq	$8, %rdi
	addq	$8, %rax
	cmpq	%rbx, %rdi
	jb	.LBB0_36
.LBB0_24:                               # %.preheader.loopexit
	leaq	(%rsi,%r8,2), %rsi
.LBB0_25:                               # %.preheader
	movq	(%rsp), %rax
	leaq	8(%rax), %rcx
	addq	$-2, %rsi
	.p2align	4, 0x90
.LBB0_26:                               # =>This Inner Loop Header: Depth=1
	leaq	-2(%rsi), %rdx
	cmpw	$0, (%rsi)
	jne	.LBB0_28
# BB#27:                                #   in Loop: Header=BB0_26 Depth=1
	cmpq	%rcx, %rsi
	movq	%rdx, %rsi
	ja	.LBB0_26
.LBB0_28:
	addl	$2, %edx
	subl	%ecx, %edx
	shrl	%edx
	incl	%edx
	movw	%dx, 4(%rax)
	testq	%r14, %r14
	je	.LBB0_31
# BB#29:
	decw	(%r14)
	jne	.LBB0_31
.LBB0_30:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	pfree
.LBB0_31:
	testq	%r15, %r15
	je	.LBB0_34
# BB#32:
	decw	(%r15)
	jne	.LBB0_34
# BB#33:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	pfree
.LBB0_34:
	movq	(%rsp), %rdi
	callq	presult
	movq	%rax, %rbx
.LBB0_35:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	psub, .Lfunc_end0-psub
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
