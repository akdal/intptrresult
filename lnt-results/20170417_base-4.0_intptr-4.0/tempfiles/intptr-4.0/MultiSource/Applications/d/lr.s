	.text
	.file	"lr.bc"
	.globl	elem_symbol
	.p2align	4, 0x90
	.type	elem_symbol,@function
elem_symbol:                            # @elem_symbol
	.cfi_startproc
# BB#0:
	cmpl	$0, (%rsi)
	je	.LBB0_1
# BB#2:
	movq	16(%rsi), %rax
	movl	4(%rax), %eax
	addl	8(%rdi), %eax
	retq
.LBB0_1:
	movq	16(%rsi), %rax
	movl	56(%rax), %eax
	retq
.Lfunc_end0:
	.size	elem_symbol, .Lfunc_end0-elem_symbol
	.cfi_endproc

	.globl	sort_VecAction
	.p2align	4, 0x90
	.type	sort_VecAction,@function
sort_VecAction:                         # @sort_VecAction
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movl	(%rdi), %esi
	movl	$8, %edx
	movl	$actioncmp, %ecx
	movq	%rax, %rdi
	jmp	qsort                   # TAILCALL
.Lfunc_end1:
	.size	sort_VecAction, .Lfunc_end1-sort_VecAction
	.cfi_endproc

	.p2align	4, 0x90
	.type	actioncmp,@function
actioncmp:                              # @actioncmp
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	(%rsi), %rcx
	cmpl	$1, (%rax)
	jne	.LBB2_2
# BB#1:
	movq	8(%rax), %rdx
	movl	$1000000, %eax          # imm = 0xF4240
	addl	4(%rdx), %eax
	cmpl	$1, (%rcx)
	je	.LBB2_4
	jmp	.LBB2_5
.LBB2_2:
	movq	16(%rax), %rax
	movl	(%rax), %eax
	cmpl	$1, (%rcx)
	jne	.LBB2_5
.LBB2_4:
	movq	8(%rcx), %rdx
	movl	$1000000, %ecx          # imm = 0xF4240
	addl	4(%rdx), %ecx
	jmp	.LBB2_6
.LBB2_5:
	movq	16(%rcx), %rcx
	movl	(%rcx), %ecx
.LBB2_6:
	xorl	%edx, %edx
	cmpl	%ecx, %eax
	movl	$-1, %ecx
	cmovgel	%edx, %ecx
	movl	$1, %eax
	cmovlel	%ecx, %eax
	retq
.Lfunc_end2:
	.size	actioncmp, .Lfunc_end2-actioncmp
	.cfi_endproc

	.globl	goto_State
	.p2align	4, 0x90
	.type	goto_State,@function
goto_State:                             # @goto_State
	.cfi_startproc
# BB#0:
	movl	96(%rdi), %r9d
	testl	%r9d, %r9d
	je	.LBB3_1
# BB#3:                                 # %.lr.ph
	movq	104(%rdi), %r8
	movq	16(%rsi), %rsi
	xorl	%eax, %eax
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_4:                                # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rdi,8), %rdx
	movq	(%rdx), %rcx
	cmpq	%rsi, 16(%rcx)
	je	.LBB3_5
# BB#2:                                 #   in Loop: Header=BB3_4 Depth=1
	incq	%rdi
	cmpl	%r9d, %edi
	jb	.LBB3_4
	jmp	.LBB3_6
.LBB3_1:
	xorl	%eax, %eax
	retq
.LBB3_5:
	movq	8(%rdx), %rax
.LBB3_6:                                # %.loopexit
	retq
.Lfunc_end3:
	.size	goto_State, .Lfunc_end3-goto_State
	.cfi_endproc

	.globl	build_LR_tables
	.p2align	4, 0x90
	.type	build_LR_tables,@function
build_LR_tables:                        # @build_LR_tables
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 160
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movl	$0, 580(%r13)
	movl	$432, %edi              # imm = 0x1B0
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, %r14
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movl	$432, %edx              # imm = 0x1B0
	movq	%rbx, %rdi
	callq	memset
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	40(%rax), %rax
	movq	(%rax), %r12
	leaq	56(%rbx), %rdi
	movq	%r12, %rsi
	callq	set_add
	testl	%eax, %eax
	je	.LBB4_8
# BB#1:
	movq	24(%r14), %rax
	leaq	32(%rbx), %rdx
	testq	%rax, %rax
	je	.LBB4_4
# BB#2:
	addq	$16, %rbx
	movl	(%rbx), %ecx
	cmpq	%rdx, %rax
	je	.LBB4_5
# BB#3:
	testb	$7, %cl
	jne	.LBB4_6
	jmp	.LBB4_7
.LBB4_4:
	movq	%rdx, 24(%r14)
	movl	16(%r14), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 16(%r14)
	movq	%r12, 32(%r14,%rax,8)
	jmp	.LBB4_8
.LBB4_5:
	cmpl	$2, %ecx
	ja	.LBB4_7
.LBB4_6:
	leal	1(%rcx), %edx
	movl	%edx, (%rbx)
	movq	%r12, (%rax,%rcx,8)
	jmp	.LBB4_8
.LBB4_7:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	vec_add_internal
.LBB4_8:                                # %insert_item.exit.i
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	build_closure
	movl	8(%r13), %eax
	testl	%eax, %eax
	je	.LBB4_22
# BB#9:                                 # %.lr.ph.i.i
	movq	16(%r13), %r14
	xorl	%ebx, %ebx
	movl	%eax, %r15d
	.p2align	4, 0x90
.LBB4_10:                               # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %rax
	testb	$28, 60(%rax)
	jne	.LBB4_21
# BB#11:                                #   in Loop: Header=BB4_10 Depth=1
	cmpq	$0, 208(%rax)
	je	.LBB4_21
# BB#12:                                #   in Loop: Header=BB4_10 Depth=1
	movl	$432, %edi              # imm = 0x1B0
	callq	malloc
	movq	%rax, %rbp
	xorl	%esi, %esi
	movl	$432, %edx              # imm = 0x1B0
	movq	%rbp, %rdi
	callq	memset
	movq	(%r14,%rbx,8), %rax
	movq	208(%rax), %r14
	leaq	56(%rbp), %rdi
	movq	%r14, %rsi
	callq	set_add
	testl	%eax, %eax
	je	.LBB4_20
# BB#13:                                #   in Loop: Header=BB4_10 Depth=1
	movq	24(%rbp), %rax
	leaq	32(%rbp), %rdx
	testq	%rax, %rax
	je	.LBB4_16
# BB#14:                                #   in Loop: Header=BB4_10 Depth=1
	movq	%rbp, %rdi
	addq	$16, %rdi
	movl	(%rdi), %ecx
	cmpq	%rdx, %rax
	je	.LBB4_17
# BB#15:                                #   in Loop: Header=BB4_10 Depth=1
	testb	$7, %cl
	jne	.LBB4_18
	jmp	.LBB4_19
.LBB4_16:                               #   in Loop: Header=BB4_10 Depth=1
	movq	%rdx, 24(%rbp)
	movl	16(%rbp), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 16(%rbp)
	movq	%r14, 32(%rbp,%rax,8)
	jmp	.LBB4_20
.LBB4_17:                               #   in Loop: Header=BB4_10 Depth=1
	cmpl	$2, %ecx
	ja	.LBB4_19
.LBB4_18:                               #   in Loop: Header=BB4_10 Depth=1
	leal	1(%rcx), %edx
	movl	%edx, (%rdi)
	movq	%r14, (%rax,%rcx,8)
	jmp	.LBB4_20
.LBB4_19:                               #   in Loop: Header=BB4_10 Depth=1
	movq	%r14, %rsi
	callq	vec_add_internal
	.p2align	4, 0x90
.LBB4_20:                               # %insert_item.exit.i.i
                                        #   in Loop: Header=BB4_10 Depth=1
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	build_closure
	movq	16(%r13), %r14
	movq	(%r14,%rbx,8), %rcx
	movq	%rax, 200(%rcx)
	movl	8(%r13), %r15d
.LBB4_21:                               #   in Loop: Header=BB4_10 Depth=1
	incq	%rbx
	cmpl	%r15d, %ebx
	jb	.LBB4_10
.LBB4_22:                               # %build_states_for_each_production.exit.i
	cmpl	$0, 88(%r13)
	je	.LBB4_136
# BB#23:                                # %.lr.ph29.i.i
	xorl	%r12d, %r12d
	leaq	72(%rsp), %r14
	.p2align	4, 0x90
.LBB4_24:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_26 Depth 2
                                        #     Child Loop BB4_30 Depth 2
	movq	96(%r13), %rax
	movq	(%rax,%r12,8), %rbx
	cmpl	$0, 48(%r13)
	je	.LBB4_28
# BB#25:                                # %.lr.ph.i10.i.preheader
                                        #   in Loop: Header=BB4_24 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_26:                               # %.lr.ph.i10.i
                                        #   Parent Loop BB4_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, 72(%rsp)
	movq	56(%r13), %rax
	movq	(%rax,%rbp,8), %rax
	movq	%rax, 88(%rsp)
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	build_state_for
	incq	%rbp
	cmpl	48(%r13), %ebp
	jb	.LBB4_26
# BB#27:                                # %.preheader.i.loopexit.i
                                        #   in Loop: Header=BB4_24 Depth=1
	movl	8(%r13), %r15d
.LBB4_28:                               # %.preheader.i.i
                                        #   in Loop: Header=BB4_24 Depth=1
	testl	%r15d, %r15d
	je	.LBB4_31
# BB#29:                                # %.lr.ph26.i.i.preheader
                                        #   in Loop: Header=BB4_24 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_30:                               # %.lr.ph26.i.i
                                        #   Parent Loop BB4_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, 72(%rsp)
	movq	16(%r13), %rax
	movq	(%rax,%rbp,8), %rax
	movq	%rax, 88(%rsp)
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	build_state_for
	incq	%rbp
	movl	8(%r13), %r15d
	cmpl	%r15d, %ebp
	jb	.LBB4_30
	jmp	.LBB4_32
	.p2align	4, 0x90
.LBB4_31:                               #   in Loop: Header=BB4_24 Depth=1
	xorl	%r15d, %r15d
.LBB4_32:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB4_24 Depth=1
	incq	%r12
	movl	88(%r13), %eax
	cmpl	%eax, %r12d
	jb	.LBB4_24
# BB#33:                                # %build_new_states.exit.i
	testl	%eax, %eax
	je	.LBB4_136
# BB#34:                                # %.lr.ph.i11.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_35:                               # %.lr.ph.i11.i
                                        # =>This Inner Loop Header: Depth=1
	movq	96(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movq	104(%rax), %rdi
	movl	96(%rax), %esi
	movl	$8, %edx
	movl	$gotocmp, %ecx
	callq	qsort
	incq	%rbx
	movl	88(%r13), %eax
	cmpl	%eax, %ebx
	jb	.LBB4_35
# BB#36:                                # %build_LR_sets.exit
	testl	%eax, %eax
	je	.LBB4_136
# BB#37:                                # %.lr.ph45.i
	xorl	%ebp, %ebp
	movq	%r13, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_38:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_40 Depth 2
                                        #       Child Loop BB4_44 Depth 3
                                        #         Child Loop BB4_47 Depth 4
                                        #       Child Loop BB4_63 Depth 3
	movq	96(%r13), %rax
	movq	(%rax,%rbp,8), %rbx
	cmpl	$0, 16(%rbx)
	je	.LBB4_76
# BB#39:                                # %.lr.ph42.i
                                        #   in Loop: Header=BB4_38 Depth=1
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	leaq	176(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	184(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	192(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	136(%rbx), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	144(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	152(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_40:                               #   Parent Loop BB4_38 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_44 Depth 3
                                        #         Child Loop BB4_47 Depth 4
                                        #       Child Loop BB4_63 Depth 3
	movq	24(%rbx), %rax
	movq	(%rax,%rcx,8), %r12
	movl	(%r12), %eax
	cmpl	$3, %eax
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	je	.LBB4_60
# BB#41:                                #   in Loop: Header=BB4_40 Depth=2
	cmpl	$1, %eax
	jne	.LBB4_74
# BB#42:                                # %.preheader.i
                                        #   in Loop: Header=BB4_40 Depth=2
	cmpl	$0, 96(%rbx)
	je	.LBB4_74
# BB#43:                                # %.lr.ph.i7
                                        #   in Loop: Header=BB4_40 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_44:                               #   Parent Loop BB4_38 Depth=1
                                        #     Parent Loop BB4_40 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_47 Depth 4
	movq	104(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax), %rcx
	movq	16(%rcx), %r13
	cmpq	16(%r12), %r13
	jne	.LBB4_59
# BB#45:                                #   in Loop: Header=BB4_44 Depth=3
	movq	8(%rax), %r14
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	testl	%eax, %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx), %r15
	je	.LBB4_51
# BB#46:                                # %.lr.ph75.i.i.preheader
                                        #   in Loop: Header=BB4_44 Depth=3
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_47:                               # %.lr.ph75.i.i
                                        #   Parent Loop BB4_38 Depth=1
                                        #     Parent Loop BB4_40 Depth=2
                                        #       Parent Loop BB4_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r15,%rcx,8), %rdx
	cmpq	%r13, 8(%rdx)
	jne	.LBB4_50
# BB#48:                                #   in Loop: Header=BB4_47 Depth=4
	cmpq	%r14, 24(%rdx)
	jne	.LBB4_50
# BB#49:                                #   in Loop: Header=BB4_47 Depth=4
	cmpl	$1, (%rdx)
	je	.LBB4_59
	.p2align	4, 0x90
.LBB4_50:                               #   in Loop: Header=BB4_47 Depth=4
	incq	%rcx
	cmpl	%eax, %ecx
	jb	.LBB4_47
.LBB4_51:                               # %._crit_edge76.i.i
                                        #   in Loop: Header=BB4_44 Depth=3
	movl	$48, %edi
	callq	malloc
	movl	$1, (%rax)
	movq	%r13, 8(%rax)
	movq	$0, 16(%rax)
	movq	%r14, 24(%rax)
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	580(%rsi), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 580(%rsi)
	movl	%ecx, 32(%rax)
	testq	%r15, %r15
	je	.LBB4_55
# BB#52:                                #   in Loop: Header=BB4_44 Depth=3
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	(%rcx), %ecx
	cmpq	16(%rsp), %r15          # 8-byte Folded Reload
	je	.LBB4_56
# BB#53:                                #   in Loop: Header=BB4_44 Depth=3
	testb	$7, %cl
	je	.LBB4_58
# BB#54:                                #   in Loop: Header=BB4_44 Depth=3
	leal	1(%rcx), %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%rax, (%r15,%rcx,8)
	jmp	.LBB4_59
.LBB4_55:                               #   in Loop: Header=BB4_44 Depth=3
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 144(%rbx)
	movl	136(%rbx), %ecx
	jmp	.LBB4_57
.LBB4_56:                               #   in Loop: Header=BB4_44 Depth=3
	cmpl	$2, %ecx
	ja	.LBB4_58
.LBB4_57:                               #   in Loop: Header=BB4_44 Depth=3
	leal	1(%rcx), %edx
	movl	%edx, 136(%rbx)
	movq	%rax, 152(%rbx,%rcx,8)
	jmp	.LBB4_59
.LBB4_58:                               #   in Loop: Header=BB4_44 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rax, %rsi
	callq	vec_add_internal
	.p2align	4, 0x90
.LBB4_59:                               # %add_action.exit38.i
                                        #   in Loop: Header=BB4_44 Depth=3
	incq	%rbp
	cmpl	96(%rbx), %ebp
	jb	.LBB4_44
	jmp	.LBB4_74
	.p2align	4, 0x90
.LBB4_60:                               #   in Loop: Header=BB4_40 Depth=2
	movq	8(%r12), %rbp
	movq	8(%rbp), %rax
	cmpl	$0, 56(%rax)
	je	.LBB4_69
# BB#61:                                #   in Loop: Header=BB4_40 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	testl	%eax, %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %r14
	je	.LBB4_65
# BB#62:                                # %.lr.ph.i.i13.preheader
                                        #   in Loop: Header=BB4_40 Depth=2
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_63:                               # %.lr.ph.i.i13
                                        #   Parent Loop BB4_38 Depth=1
                                        #     Parent Loop BB4_40 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14,%rcx,8), %rdx
	cmpq	%rbp, 16(%rdx)
	je	.LBB4_74
# BB#64:                                #   in Loop: Header=BB4_63 Depth=3
	incq	%rcx
	cmpl	%eax, %ecx
	jb	.LBB4_63
.LBB4_65:                               # %._crit_edge.i.i14
                                        #   in Loop: Header=BB4_40 Depth=2
	movl	$48, %edi
	callq	malloc
	movl	$2, (%rax)
	movq	$0, 8(%rax)
	movq	%rbp, 16(%rax)
	movq	$0, 24(%rax)
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	580(%rsi), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 580(%rsi)
	movl	%ecx, 32(%rax)
	testq	%r14, %r14
	je	.LBB4_70
# BB#66:                                #   in Loop: Header=BB4_40 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
	cmpq	32(%rsp), %r14          # 8-byte Folded Reload
	je	.LBB4_71
# BB#67:                                #   in Loop: Header=BB4_40 Depth=2
	testb	$7, %cl
	je	.LBB4_73
# BB#68:                                #   in Loop: Header=BB4_40 Depth=2
	leal	1(%rcx), %edx
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%rax, (%r14,%rcx,8)
	jmp	.LBB4_74
.LBB4_69:                               #   in Loop: Header=BB4_40 Depth=2
	orb	$1, 376(%rbx)
	jmp	.LBB4_74
.LBB4_70:                               #   in Loop: Header=BB4_40 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 184(%rbx)
	movl	176(%rbx), %ecx
	jmp	.LBB4_72
.LBB4_71:                               #   in Loop: Header=BB4_40 Depth=2
	cmpl	$2, %ecx
	ja	.LBB4_73
.LBB4_72:                               #   in Loop: Header=BB4_40 Depth=2
	leal	1(%rcx), %edx
	movl	%edx, 176(%rbx)
	movq	%rax, 192(%rbx,%rcx,8)
	jmp	.LBB4_74
.LBB4_73:                               #   in Loop: Header=BB4_40 Depth=2
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	vec_add_internal
	.p2align	4, 0x90
.LBB4_74:                               # %add_action.exit.i
                                        #   in Loop: Header=BB4_40 Depth=2
	movq	56(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpl	16(%rbx), %ecx
	jb	.LBB4_40
# BB#75:                                #   in Loop: Header=BB4_38 Depth=1
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	jmp	.LBB4_77
	.p2align	4, 0x90
.LBB4_76:                               # %.._crit_edge_crit_edge.i
                                        #   in Loop: Header=BB4_38 Depth=1
	leaq	144(%rbx), %rax
	leaq	184(%rbx), %r14
.LBB4_77:                               # %._crit_edge.i16
                                        #   in Loop: Header=BB4_38 Depth=1
	movq	(%rax), %rdi
	movl	136(%rbx), %esi
	movl	$8, %edx
	movl	$actioncmp, %ecx
	callq	qsort
	movq	(%r14), %rdi
	movl	176(%rbx), %esi
	movl	$8, %edx
	movl	$actioncmp, %ecx
	callq	qsort
	incq	%rbp
	movl	88(%r13), %eax
	cmpl	%eax, %ebp
	jb	.LBB4_38
# BB#78:                                # %build_actions.exit
	testl	%eax, %eax
	je	.LBB4_136
# BB#79:                                # %.lr.ph95.i
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_80:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_105 Depth 2
                                        #       Child Loop BB4_84 Depth 3
                                        #       Child Loop BB4_88 Depth 3
                                        #         Child Loop BB4_90 Depth 4
	movq	96(%r13), %rax
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %r13
	cmpl	$0, 16(%r13)
	je	.LBB4_107
# BB#81:                                # %.lr.ph91.i
                                        #   in Loop: Header=BB4_80 Depth=1
	leaq	216(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	232(%r13), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r15d, %r15d
	jmp	.LBB4_105
	.p2align	4, 0x90
.LBB4_82:                               #   in Loop: Header=BB4_105 Depth=2
	movq	8(%rax), %r14
	movl	4(%rax), %r8d
	movl	32(%r14), %r12d
	cmpl	%r12d, %r8d
	movq	%r13, %rbx
	jae	.LBB4_96
# BB#83:                                # %.lr.ph.i17
                                        #   in Loop: Header=BB4_105 Depth=2
	movslq	%r8d, %rcx
	movq	40(%r14), %r9
	leaq	(%r9,%rcx,8), %rsi
	movl	%r8d, %edi
	.p2align	4, 0x90
.LBB4_84:                               #   Parent Loop BB4_80 Depth=1
                                        #     Parent Loop BB4_105 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rsi), %rax
	cmpl	$0, (%rax)
	jne	.LBB4_106
# BB#85:                                #   in Loop: Header=BB4_84 Depth=3
	movq	16(%rax), %rax
	cmpq	$0, 64(%rax)
	je	.LBB4_106
# BB#86:                                #   in Loop: Header=BB4_84 Depth=3
	incl	%edi
	addq	$8, %rsi
	cmpl	%r12d, %edi
	jb	.LBB4_84
# BB#87:                                #   in Loop: Header=BB4_105 Depth=2
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB4_88:                               # %.lr.ph85.i
                                        #   Parent Loop BB4_80 Depth=1
                                        #     Parent Loop BB4_105 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_90 Depth 4
	movl	96(%rbx), %esi
	testl	%esi, %esi
	je	.LBB4_93
# BB#89:                                # %.lr.ph.i.i21
                                        #   in Loop: Header=BB4_88 Depth=3
	movq	(%r9,%rcx,8), %rax
	movq	104(%rbx), %rdi
	movq	16(%rax), %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_90:                               #   Parent Loop BB4_80 Depth=1
                                        #     Parent Loop BB4_105 Depth=2
                                        #       Parent Loop BB4_88 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rdi,%rax,8), %rdx
	movq	(%rdx), %rbp
	cmpq	%rbx, 16(%rbp)
	je	.LBB4_94
# BB#91:                                #   in Loop: Header=BB4_90 Depth=4
	incq	%rax
	cmpl	%esi, %eax
	jb	.LBB4_90
.LBB4_93:                               #   in Loop: Header=BB4_88 Depth=3
	xorl	%ebx, %ebx
	jmp	.LBB4_95
	.p2align	4, 0x90
.LBB4_94:                               #   in Loop: Header=BB4_88 Depth=3
	movq	8(%rdx), %rbx
.LBB4_95:                               # %goto_State.exit.i
                                        #   in Loop: Header=BB4_88 Depth=3
	incq	%rcx
	cmpl	%r12d, %ecx
	jb	.LBB4_88
.LBB4_96:                               # %._crit_edge86.i
                                        #   in Loop: Header=BB4_105 Depth=2
	testl	%r12d, %r12d
	je	.LBB4_106
# BB#97:                                # %._crit_edge86.i
                                        #   in Loop: Header=BB4_105 Depth=2
	testq	%rbx, %rbx
	je	.LBB4_106
# BB#98:                                #   in Loop: Header=BB4_105 Depth=2
	movq	224(%r13), %rcx
	testq	%rcx, %rcx
	je	.LBB4_101
# BB#99:                                #   in Loop: Header=BB4_105 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %eax
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	je	.LBB4_102
# BB#100:                               #   in Loop: Header=BB4_105 Depth=2
	testb	$7, %al
	jne	.LBB4_103
	jmp	.LBB4_104
.LBB4_101:                              #   in Loop: Header=BB4_105 Depth=2
	notl	%r8d
	addl	%r8d, %r12d
	movl	$24, %edi
	callq	malloc
	movl	%r12d, (%rax)
	movq	%rbx, 8(%rax)
	movq	%r14, 16(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, 224(%r13)
	movl	216(%r13), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 216(%r13)
	movq	%rax, 232(%r13,%rcx,8)
	jmp	.LBB4_106
.LBB4_102:                              #   in Loop: Header=BB4_105 Depth=2
	cmpl	$2, %eax
	ja	.LBB4_104
.LBB4_103:                              #   in Loop: Header=BB4_105 Depth=2
	notl	%r8d
	addl	%r8d, %r12d
	movl	$24, %edi
	callq	malloc
	movl	%r12d, (%rax)
	movq	%rbx, 8(%rax)
	movq	%r14, 16(%rax)
	movq	224(%r13), %rcx
	movl	216(%r13), %edx
	leal	1(%rdx), %esi
	movl	%esi, 216(%r13)
	movq	%rax, (%rcx,%rdx,8)
	jmp	.LBB4_106
.LBB4_104:                              #   in Loop: Header=BB4_105 Depth=2
	notl	%r8d
	addl	%r8d, %r12d
	movl	$24, %edi
	callq	malloc
	movl	%r12d, (%rax)
	movq	%rbx, 8(%rax)
	movq	%r14, 16(%rax)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, %rsi
	callq	vec_add_internal
	jmp	.LBB4_106
	.p2align	4, 0x90
.LBB4_105:                              #   Parent Loop BB4_80 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_84 Depth 3
                                        #       Child Loop BB4_88 Depth 3
                                        #         Child Loop BB4_90 Depth 4
	movq	24(%r13), %rax
	movq	(%rax,%r15,8), %rax
	cmpl	$3, (%rax)
	jne	.LBB4_82
.LBB4_106:                              # %.loopexit.i
                                        #   in Loop: Header=BB4_105 Depth=2
	incq	%r15
	cmpl	16(%r13), %r15d
	jb	.LBB4_105
.LBB4_107:                              # %._crit_edge92.i
                                        #   in Loop: Header=BB4_80 Depth=1
	movl	216(%r13), %esi
	cmpq	$2, %rsi
	jb	.LBB4_109
# BB#108:                               #   in Loop: Header=BB4_80 Depth=1
	movq	224(%r13), %rdi
	movl	$8, %edx
	movl	$hintcmp, %ecx
	callq	qsort
.LBB4_109:                              #   in Loop: Header=BB4_80 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movq	48(%rsp), %r13          # 8-byte Reload
	movl	88(%r13), %eax
	cmpl	%eax, %ecx
	jb	.LBB4_80
# BB#110:                               # %build_right_epsilon_hints.exit
	testl	%eax, %eax
	je	.LBB4_136
# BB#111:                               # %.lr.ph100.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_112:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_129 Depth 2
                                        #       Child Loop BB4_116 Depth 3
	movq	96(%r13), %rax
	movq	(%rax,%rbp,8), %r15
	cmpl	$0, 16(%r15)
	je	.LBB4_134
# BB#113:                               # %.lr.ph96.i
                                        #   in Loop: Header=BB4_112 Depth=1
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	leaq	256(%r15), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	264(%r15), %r8
	leaq	272(%r15), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	%r8, 32(%rsp)           # 8-byte Spill
	jmp	.LBB4_129
.LBB4_114:                              #   in Loop: Header=BB4_129 Depth=2
	movl	4(%rax), %r12d
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %esi
	testl	%esi, %esi
	movq	(%r8), %r13
	je	.LBB4_120
# BB#115:                               # %.lr.ph.i
                                        #   in Loop: Header=BB4_129 Depth=2
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	%r12d, 8(%rsp)          # 4-byte Spill
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movl	32(%rdx), %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB4_116:                              #   Parent Loop BB4_112 Depth=1
                                        #     Parent Loop BB4_129 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13,%r15,8), %r12
	movq	16(%r12), %rax
	movq	40(%rax), %rcx
	movl	32(%rax), %eax
	decl	%eax
	movq	(%rcx,%rax,8), %rax
	movq	16(%rax), %rax
	cmpl	32(%rax), %ebx
	jne	.LBB4_118
# BB#117:                               #   in Loop: Header=BB4_116 Depth=3
	movq	24(%rdx), %rdi
	movl	%esi, %r14d
	movq	24(%rax), %rsi
	movq	%rdx, %rbp
	callq	strcmp
	movl	%r14d, %esi
	movq	%rbp, %rdx
	testl	%eax, %eax
	je	.LBB4_126
.LBB4_118:                              #   in Loop: Header=BB4_116 Depth=3
	incq	%r15
	cmpl	%esi, %r15d
	jb	.LBB4_116
# BB#119:                               #   in Loop: Header=BB4_129 Depth=2
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	8(%rsp), %r12d          # 4-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	cmpq	56(%rsp), %r13          # 8-byte Folded Reload
	jne	.LBB4_122
	jmp	.LBB4_123
.LBB4_120:                              # %._crit_edge.i
                                        #   in Loop: Header=BB4_129 Depth=2
	xorl	%esi, %esi
	testq	%r13, %r13
	je	.LBB4_128
# BB#121:                               # %._crit_edge.thread.i
                                        #   in Loop: Header=BB4_129 Depth=2
	cmpq	56(%rsp), %r13          # 8-byte Folded Reload
	je	.LBB4_123
.LBB4_122:                              #   in Loop: Header=BB4_129 Depth=2
	movq	%r8, %r14
	testb	$7, %sil
	jne	.LBB4_124
	jmp	.LBB4_125
.LBB4_123:                              #   in Loop: Header=BB4_129 Depth=2
	movq	%r8, %r14
	cmpl	$2, %esi
	ja	.LBB4_125
.LBB4_124:                              #   in Loop: Header=BB4_129 Depth=2
	movl	$24, %edi
	callq	malloc
	movl	%r12d, (%rax)
	movq	$0, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%r14, %r8
	movq	(%r8), %rcx
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	(%rdi), %edx
	leal	1(%rdx), %esi
	movl	%esi, (%rdi)
	movq	%rax, (%rcx,%rdx,8)
	jmp	.LBB4_132
.LBB4_125:                              #   in Loop: Header=BB4_129 Depth=2
	movl	$24, %edi
	callq	malloc
	movl	%r12d, (%rax)
	movq	$0, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rax, %rsi
	callq	vec_add_internal
	movq	%r14, %r8
	jmp	.LBB4_132
.LBB4_126:                              #   in Loop: Header=BB4_129 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpl	%eax, (%r12)
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	jbe	.LBB4_132
# BB#127:                               #   in Loop: Header=BB4_129 Depth=2
	movl	%eax, (%r12)
	jmp	.LBB4_132
.LBB4_128:                              #   in Loop: Header=BB4_129 Depth=2
	movl	$24, %edi
	movq	%r8, %r14
	callq	malloc
	movq	%r14, %r8
	movl	%r12d, (%rax)
	movq	$0, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 264(%r15)
	movl	256(%r15), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 256(%r15)
	movq	%rax, 272(%r15,%rcx,8)
	jmp	.LBB4_132
	.p2align	4, 0x90
.LBB4_129:                              #   Parent Loop BB4_112 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_116 Depth 3
	movq	24(%r15), %rax
	movq	(%rax,%rbp,8), %rax
	movq	8(%rax), %rbx
	movl	32(%rbx), %ecx
	cmpl	$2, %ecx
	jb	.LBB4_132
# BB#130:                               #   in Loop: Header=BB4_129 Depth=2
	movq	40(%rbx), %rdx
	decl	%ecx
	movq	(%rdx,%rcx,8), %rcx
	cmpl	$1, (%rcx)
	jne	.LBB4_132
# BB#131:                               #   in Loop: Header=BB4_129 Depth=2
	movq	16(%rcx), %rdx
	cmpl	$0, (%rdx)
	je	.LBB4_114
	.p2align	4, 0x90
.LBB4_132:                              #   in Loop: Header=BB4_129 Depth=2
	incq	%rbp
	cmpl	16(%r15), %ebp
	jb	.LBB4_129
# BB#133:                               #   in Loop: Header=BB4_112 Depth=1
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB4_135
	.p2align	4, 0x90
.LBB4_134:                              # %.._crit_edge97_crit_edge.i
                                        #   in Loop: Header=BB4_112 Depth=1
	leaq	264(%r15), %r8
.LBB4_135:                              # %._crit_edge97.i
                                        #   in Loop: Header=BB4_112 Depth=1
	movq	(%r8), %rdi
	movl	256(%r15), %esi
	movl	$8, %edx
	movl	$hintcmp, %ecx
	callq	qsort
	incq	%rbp
	cmpl	88(%r13), %ebp
	jb	.LBB4_112
.LBB4_136:                              # %build_error_recovery.exit
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	build_LR_tables, .Lfunc_end4-build_LR_tables
	.cfi_endproc

	.p2align	4, 0x90
	.type	build_closure,@function
build_closure:                          # @build_closure
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 96
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movl	16(%r13), %eax
	testl	%eax, %eax
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	je	.LBB5_1
# BB#2:                                 # %.lr.ph55
	leaq	56(%r13), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	16(%r13), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leaq	32(%r13), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_6 Depth 2
	movq	24(%r13), %rcx
	movq	(%rcx,%r14,8), %rbp
	cmpl	$0, (%rbp)
	jne	.LBB5_18
# BB#4:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	16(%rbp), %r15
	cmpl	$0, 16(%r15)
	je	.LBB5_18
# BB#5:                                 # %.lr.ph52
                                        #   in Loop: Header=BB5_3 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB5_6:                                #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%r15), %rax
	movq	(%rax,%r12,8), %rax
	movq	40(%rax), %rcx
	addq	$72, %rax
	testq	%rcx, %rcx
	cmovneq	%rcx, %rax
	movq	(%rax), %rbx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	set_add
	testl	%eax, %eax
	je	.LBB5_16
# BB#7:                                 #   in Loop: Header=BB5_6 Depth=2
	movq	%r13, %rdx
	movq	24(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB5_8
# BB#9:                                 #   in Loop: Header=BB5_6 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpq	24(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB5_10
# BB#13:                                #   in Loop: Header=BB5_6 Depth=2
	testb	$7, %al
	je	.LBB5_15
# BB#14:                                #   in Loop: Header=BB5_6 Depth=2
	leal	1(%rax), %edx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%rbx, (%rcx,%rax,8)
	jmp	.LBB5_16
	.p2align	4, 0x90
.LBB5_8:                                #   in Loop: Header=BB5_6 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 24(%rdx)
	movl	16(%rdx), %eax
	leal	1(%rax), %ecx
	jmp	.LBB5_12
.LBB5_10:                               #   in Loop: Header=BB5_6 Depth=2
	cmpl	$2, %eax
	ja	.LBB5_15
# BB#11:                                #   in Loop: Header=BB5_6 Depth=2
	leal	1(%rax), %ecx
	movq	%r13, %rdx
.LBB5_12:                               # %insert_item.exit
                                        #   in Loop: Header=BB5_6 Depth=2
	movl	%ecx, 16(%rdx)
	movq	%rbx, 32(%rdx,%rax,8)
	jmp	.LBB5_16
.LBB5_15:                               #   in Loop: Header=BB5_6 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	vec_add_internal
	.p2align	4, 0x90
.LBB5_16:                               # %insert_item.exit
                                        #   in Loop: Header=BB5_6 Depth=2
	incq	%r12
	movq	16(%rbp), %rax
	cmpl	16(%rax), %r12d
	jb	.LBB5_6
# BB#17:                                # %.loopexit.loopexit
                                        #   in Loop: Header=BB5_3 Depth=1
	movl	16(%r13), %eax
.LBB5_18:                               # %.loopexit
                                        #   in Loop: Header=BB5_3 Depth=1
	incq	%r14
	cmpl	%eax, %r14d
	jb	.LBB5_3
	jmp	.LBB5_19
.LBB5_1:
	xorl	%eax, %eax
.LBB5_19:                               # %._crit_edge56
	movq	24(%r13), %rdi
	movl	%eax, %esi
	movl	$8, %edx
	movl	$itemcmp, %ecx
	callq	qsort
	movq	$0, 8(%r13)
	movl	16(%r13), %eax
	xorl	%r9d, %r9d
	testq	%rax, %rax
	je	.LBB5_20
# BB#21:                                # %.lr.ph
	movq	24(%r13), %rsi
	testb	$1, %al
	jne	.LBB5_23
# BB#22:
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	jmp	.LBB5_24
.LBB5_20:
	xorl	%ecx, %ecx
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB5_27
.LBB5_23:
	movq	(%rsi), %rdi
	movq	8(%rdi), %rbp
	movl	(%rbp), %ecx
	shll	$8, %ecx
	addq	$32, %rbp
	cmpl	$3, (%rdi)
	leaq	4(%rdi), %rdi
	cmoveq	%rbp, %rdi
	addl	(%rdi), %ecx
	movl	$1, %edi
.LBB5_24:                               # %.prol.loopexit
	movq	8(%rsp), %r14           # 8-byte Reload
	cmpl	$1, %eax
	je	.LBB5_26
	.p2align	4, 0x90
.LBB5_25:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rdi,8), %rbp
	movq	8(%rbp), %rbx
	movl	(%rbx), %edx
	shll	$8, %edx
	addq	$32, %rbx
	cmpl	$3, (%rbp)
	leaq	4(%rbp), %rbp
	cmoveq	%rbx, %rbp
	addl	(%rbp), %edx
	addq	%rcx, %rdx
	movq	8(%rsi,%rdi,8), %rbp
	movq	8(%rbp), %rbx
	movl	(%rbx), %ecx
	shll	$8, %ecx
	addq	$32, %rbx
	cmpl	$3, (%rbp)
	leaq	4(%rbp), %rbp
	cmoveq	%rbx, %rbp
	addl	(%rbp), %ecx
	addq	%rdx, %rcx
	addq	$2, %rdi
	cmpq	%rax, %rdi
	jb	.LBB5_25
.LBB5_26:                               # %._crit_edge
	movq	%rcx, 8(%r13)
.LBB5_27:
	movl	88(%r14), %r10d
	testl	%r10d, %r10d
	movq	96(%r14), %r8
	je	.LBB5_37
# BB#28:                                # %.lr.ph67.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_29:                               # %.lr.ph67.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_34 Depth 2
	movq	(%r8,%rbx,8), %rdx
	cmpq	8(%rdx), %rcx
	jne	.LBB5_35
# BB#30:                                #   in Loop: Header=BB5_29 Depth=1
	cmpl	16(%rdx), %eax
	jne	.LBB5_35
# BB#31:                                # %.preheader.i
                                        #   in Loop: Header=BB5_29 Depth=1
	testl	%eax, %eax
	movq	24(%r13), %rdi
	je	.LBB5_39
# BB#32:                                # %.lr.ph.i
                                        #   in Loop: Header=BB5_29 Depth=1
	movq	24(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_34:                               #   Parent Loop BB5_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rsi,8), %rbp
	cmpq	(%rdx,%rsi,8), %rbp
	jne	.LBB5_35
# BB#33:                                #   in Loop: Header=BB5_34 Depth=2
	incq	%rsi
	cmpl	%eax, %esi
	jb	.LBB5_34
	jmp	.LBB5_39
	.p2align	4, 0x90
.LBB5_35:                               # %.loopexit.i
                                        #   in Loop: Header=BB5_29 Depth=1
	incq	%rbx
	cmpl	%r10d, %ebx
	jb	.LBB5_29
# BB#36:
	movl	%r10d, %r9d
.LBB5_37:                               # %._crit_edge68.i
	movl	%r9d, (%r13)
	leaq	104(%r14), %rcx
	testq	%r8, %r8
	je	.LBB5_38
# BB#46:
	addq	$88, %r14
	movl	(%r14), %eax
	cmpq	%rcx, %r8
	je	.LBB5_47
# BB#49:
	testb	$7, %al
	jne	.LBB5_48
	jmp	.LBB5_50
.LBB5_39:                               # %.preheader._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB5_42
# BB#40:                                # %.preheader._crit_edge.i
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.LBB5_42
# BB#41:
	callq	free
.LBB5_42:
	movl	$0, 16(%r13)
	movq	$0, 24(%r13)
	movq	64(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB5_45
# BB#43:
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.LBB5_45
# BB#44:
	callq	free
.LBB5_45:                               # %free_state.exit.i
	movq	%r13, %rdi
	callq	free
	movq	96(%r14), %rax
	movq	(%rax,%rbx,8), %r13
	jmp	.LBB5_51
.LBB5_38:
	movq	%rcx, 96(%r14)
	movl	88(%r14), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 88(%r14)
	movq	%r13, 104(%r14,%rax,8)
	jmp	.LBB5_51
.LBB5_47:
	cmpl	$2, %eax
	ja	.LBB5_50
.LBB5_48:
	leal	1(%rax), %ecx
	movl	%ecx, (%r14)
	movq	%r13, (%r8,%rax,8)
	jmp	.LBB5_51
.LBB5_50:
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	vec_add_internal
.LBB5_51:                               # %maybe_add_state.exit
	movq	%r13, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	build_closure, .Lfunc_end5-build_closure
	.cfi_endproc

	.p2align	4, 0x90
	.type	itemcmp,@function
itemcmp:                                # @itemcmp
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	8(%rax), %rcx
	movl	(%rcx), %edx
	shll	$8, %edx
	addq	$32, %rcx
	cmpl	$3, (%rax)
	leaq	4(%rax), %rax
	cmoveq	%rcx, %rax
	addl	(%rax), %edx
	movq	(%rsi), %rax
	movq	8(%rax), %rcx
	movl	(%rcx), %esi
	shll	$8, %esi
	addq	$32, %rcx
	cmpl	$3, (%rax)
	leaq	4(%rax), %rax
	cmoveq	%rcx, %rax
	addl	(%rax), %esi
	cmpl	%esi, %edx
	sbbl	%ecx, %ecx
	cmpl	%esi, %edx
	movl	$1, %eax
	cmovbel	%ecx, %eax
	retq
.Lfunc_end6:
	.size	itemcmp, .Lfunc_end6-itemcmp
	.cfi_endproc

	.p2align	4, 0x90
	.type	build_state_for,@function
build_state_for:                        # @build_state_for
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	cmpl	$0, 16(%r15)
	je	.LBB7_25
# BB#1:                                 # %.lr.ph
	movq	%rdi, (%rsp)            # 8-byte Spill
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	jmp	.LBB7_2
.LBB7_5:                                #   in Loop: Header=BB7_2 Depth=1
	testq	%r12, %r12
	jne	.LBB7_7
# BB#6:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	$432, %edi              # imm = 0x1B0
	callq	malloc
	movq	%rax, %r12
	xorl	%esi, %esi
	movl	$432, %edx              # imm = 0x1B0
	movq	%r12, %rdi
	callq	memset
.LBB7_7:                                #   in Loop: Header=BB7_2 Depth=1
	movl	4(%rbx), %ecx
	movq	8(%rbx), %rax
	incl	%ecx
	cmpl	32(%rax), %ecx
	jae	.LBB7_8
# BB#9:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	%ecx, %ecx
	shlq	$3, %rcx
	addq	40(%rax), %rcx
	movq	%rcx, %rax
	jmp	.LBB7_10
.LBB7_8:                                #   in Loop: Header=BB7_2 Depth=1
	addq	$72, %rax
.LBB7_10:                               # %next_elem.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	(%rax), %r13
	leaq	56(%r12), %rdi
	movq	%r13, %rsi
	callq	set_add
	testl	%eax, %eax
	je	.LBB7_18
# BB#11:                                #   in Loop: Header=BB7_2 Depth=1
	movq	24(%r12), %rax
	leaq	32(%r12), %rdx
	testq	%rax, %rax
	je	.LBB7_12
# BB#13:                                #   in Loop: Header=BB7_2 Depth=1
	leaq	16(%r12), %rdi
	movl	(%rdi), %ecx
	cmpq	%rdx, %rax
	je	.LBB7_14
# BB#16:                                #   in Loop: Header=BB7_2 Depth=1
	testb	$7, %cl
	jne	.LBB7_15
	jmp	.LBB7_17
.LBB7_12:                               #   in Loop: Header=BB7_2 Depth=1
	movq	%rdx, 24(%r12)
	movl	16(%r12), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 16(%r12)
	movq	%r13, 32(%r12,%rax,8)
	jmp	.LBB7_18
.LBB7_14:                               #   in Loop: Header=BB7_2 Depth=1
	cmpl	$2, %ecx
	ja	.LBB7_17
.LBB7_15:                               #   in Loop: Header=BB7_2 Depth=1
	leal	1(%rcx), %edx
	movl	%edx, (%rdi)
	movq	%r13, (%rax,%rcx,8)
	jmp	.LBB7_18
.LBB7_17:                               #   in Loop: Header=BB7_2 Depth=1
	movq	%r13, %rsi
	callq	vec_add_internal
	jmp	.LBB7_18
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rax
	movq	(%rax,%rbp,8), %rbx
	movl	(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB7_18
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	cmpl	(%r14), %eax
	jne	.LBB7_18
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	16(%rbx), %rax
	cmpq	16(%r14), %rax
	je	.LBB7_5
	.p2align	4, 0x90
.LBB7_18:                               # %insert_item.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	incq	%rbp
	cmpl	16(%r15), %ebp
	jb	.LBB7_2
# BB#19:                                # %._crit_edge
	testq	%r12, %r12
	movq	(%rsp), %rdi            # 8-byte Reload
	je	.LBB7_25
# BB#20:
	movq	%r12, %rsi
	callq	build_closure
	movq	%rax, %r12
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%r12, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, (%rbx)
	movq	104(%r15), %rax
	leaq	112(%r15), %rdx
	testq	%rax, %rax
	je	.LBB7_21
# BB#22:
	addq	$96, %r15
	movl	(%r15), %ecx
	cmpq	%rdx, %rax
	je	.LBB7_23
# BB#26:
	testb	$7, %cl
	jne	.LBB7_24
	jmp	.LBB7_27
.LBB7_21:
	movq	%rdx, 104(%r15)
	movl	96(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 96(%r15)
	movq	%rbx, 112(%r15,%rax,8)
	jmp	.LBB7_25
.LBB7_23:
	cmpl	$2, %ecx
	ja	.LBB7_27
.LBB7_24:
	leal	1(%rcx), %edx
	movl	%edx, (%r15)
	movq	%rbx, (%rax,%rcx,8)
.LBB7_25:                               # %add_goto.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_27:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	vec_add_internal        # TAILCALL
.Lfunc_end7:
	.size	build_state_for, .Lfunc_end7-build_state_for
	.cfi_endproc

	.p2align	4, 0x90
	.type	gotocmp,@function
gotocmp:                                # @gotocmp
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	(%rsi), %rcx
	movq	8(%rax), %rax
	movl	(%rax), %eax
	movq	8(%rcx), %rcx
	xorl	%edx, %edx
	cmpl	(%rcx), %eax
	movl	$-1, %ecx
	cmovgel	%edx, %ecx
	movl	$1, %eax
	cmovlel	%ecx, %eax
	retq
.Lfunc_end8:
	.size	gotocmp, .Lfunc_end8-gotocmp
	.cfi_endproc

	.p2align	4, 0x90
	.type	hintcmp,@function
hintcmp:                                # @hintcmp
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rcx
	movq	(%rsi), %rdx
	movl	(%rdx), %esi
	movl	$1, %eax
	cmpl	%esi, (%rcx)
	ja	.LBB9_4
# BB#1:
	movl	$-1, %eax
	jb	.LBB9_4
# BB#2:
	movq	16(%rcx), %rax
	movq	16(%rdx), %rcx
	movl	(%rcx), %ecx
	cmpl	%ecx, (%rax)
	movl	$1, %eax
	ja	.LBB9_4
# BB#3:
	sbbl	%eax, %eax
.LBB9_4:
	retq
.Lfunc_end9:
	.size	hintcmp, .Lfunc_end9-hintcmp
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
