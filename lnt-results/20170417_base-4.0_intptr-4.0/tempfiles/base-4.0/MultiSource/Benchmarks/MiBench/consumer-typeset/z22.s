	.text
	.file	"z22.bc"
	.globl	Interpose
	.p2align	4, 0x90
	.type	Interpose,@function
Interpose:                              # @Interpose
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movl	%esi, %ebp
	movq	%rdi, %r12
	movslq	%ebp, %rax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
# BB#2:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_3
.LBB0_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB0_3:
	movb	%bpl, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	34(%r14), %eax
	movw	%ax, 34(%rbx)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r14), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbx), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbx)
	andl	36(%r14), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movq	%r12, zz_hold(%rip)
	movq	24(%r12), %rax
	cmpq	%r12, %rax
	je	.LBB0_4
# BB#5:
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r12), %rcx
	movq	%rax, 24(%rcx)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%bpl
	je	.LBB0_8
# BB#6:
	testq	%rax, %rax
	je	.LBB0_8
# BB#7:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB0_8
.LBB0_4:                                # %.thread
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%bpl
.LBB0_8:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_9
# BB#10:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_11
.LBB0_9:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_11:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	sete	%cl
	orb	%cl, %bpl
	jne	.LBB0_13
# BB#12:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_13:
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_15
# BB#14:
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_15:
	movl	48(%r15), %eax
	movl	%eax, 48(%rbx)
	movl	56(%r15), %eax
	movl	%eax, 56(%rbx)
	movl	52(%r14), %eax
	movl	%eax, 52(%rbx)
	movl	60(%r14), %eax
	movl	%eax, 60(%rbx)
	movl	$1610612736, %eax       # imm = 0x60000000
	andl	40(%r12), %eax
	movl	$-1610612737, %ecx      # imm = 0x9FFFFFFF
	andl	40(%rbx), %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Interpose, .Lfunc_end0-Interpose
	.cfi_endproc

	.globl	FlushInners
	.p2align	4, 0x90
	.type	FlushInners,@function
FlushInners:                            # @FlushInners
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB1_1
# BB#11:
	movq	24(%r14), %rax
	cmpq	%r14, %rax
	jne	.LBB1_13
# BB#12:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	24(%r14), %rax
	.p2align	4, 0x90
.LBB1_13:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB1_13
# BB#14:
	movq	80(%rax), %rax
	movq	80(%rax), %rax
	cmpq	PrintSym(%rip), %rax
	jne	.LBB1_1
# BB#15:
	movq	%r15, %rdi
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	DisposeObject           # TAILCALL
.LBB1_10:                               #   in Loop: Header=BB1_1 Depth=1
	movq	no_fpos(%rip), %rbx
	callq	Image
	movq	%rax, (%rsp)
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.5, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	.p2align	4, 0x90
.LBB1_1:                                # %.preheader55
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
                                        #     Child Loop BB1_20 Depth 2
                                        #     Child Loop BB1_22 Depth 2
                                        #     Child Loop BB1_27 Depth 2
                                        #     Child Loop BB1_18 Depth 2
	movq	8(%r15), %rax
	cmpq	%r15, %rax
	je	.LBB1_31
# BB#2:                                 # %.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB1_3:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edi
	testl	%edi, %edi
	je	.LBB1_3
# BB#4:                                 #   in Loop: Header=BB1_1 Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rdx
	cmpq	%rax, %rdx
	je	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_1 Depth=1
	movq	%rdx, zz_res(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rax), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_6:                                #   in Loop: Header=BB1_1 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rdx
	cmpq	%rax, %rdx
	je	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_1 Depth=1
	movq	%rdx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rax, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rax
.LBB1_8:                                #   in Loop: Header=BB1_1 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %edx
	leaq	zz_lengths(%rdx), %rsi
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
	addb	$-11, %dl
	leaq	33(%rax), %rbx
	cmpb	$2, %dl
	cmovbq	%rbx, %rsi
	movzbl	(%rsi), %edx
	movl	%edx, zz_size(%rip)
	movq	zz_free(,%rdx,8), %rsi
	movq	%rsi, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rdx,8)
	movl	%edi, %eax
	addb	$-119, %al
	cmpb	$5, %al
	ja	.LBB1_9
# BB#16:                                #   in Loop: Header=BB1_1 Depth=1
	movzbl	%al, %eax
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_17:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rcx), %rdi
	cmpq	%rcx, %rdi
	je	.LBB1_1
	.p2align	4, 0x90
.LBB1_18:                               # %.preheader66
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB1_18
.LBB1_28:                               #   in Loop: Header=BB1_1 Depth=1
	movq	88(%rdi), %rax
	testq	%rax, %rax
	je	.LBB1_30
# BB#29:                                #   in Loop: Header=BB1_1 Depth=1
	movzwl	41(%rax), %eax
	testb	$8, %ah
	jne	.LBB1_1
.LBB1_30:                               #   in Loop: Header=BB1_1 Depth=1
	callq	FlushGalley
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_9:                                #   in Loop: Header=BB1_1 Depth=1
	cmpb	$-123, %dil
	je	.LBB1_1
	jmp	.LBB1_10
.LBB1_19:                               #   in Loop: Header=BB1_1 Depth=1
	movq	8(%rcx), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_20:                               #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	leaq	16(%rcx), %rax
	cmpb	$0, 32(%rcx)
	je	.LBB1_20
# BB#21:                                #   in Loop: Header=BB1_1 Depth=1
	movq	16(%rcx), %rbx
	cmpq	%rbx, 24(%rcx)
	je	.LBB1_1
	.p2align	4, 0x90
.LBB1_22:                               #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_22
# BB#23:                                #   in Loop: Header=BB1_1 Depth=1
	cmpb	$125, %al
	je	.LBB1_25
# BB#24:                                #   in Loop: Header=BB1_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_25:                               # %.loopexit
                                        #   in Loop: Header=BB1_1 Depth=1
	movzwl	42(%rbx), %eax
	testb	$32, %al
	je	.LBB1_1
# BB#26:                                #   in Loop: Header=BB1_1 Depth=1
	andl	$65503, %eax            # imm = 0xFFDF
	movw	%ax, 42(%rbx)
	movq	24(%rbx), %rdi
	.p2align	4, 0x90
.LBB1_27:                               #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB1_27
	jmp	.LBB1_28
.LBB1_31:
	movq	%r15, zz_hold(%rip)
	movzbl	32(%r15), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r15), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r15)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	FlushInners, .Lfunc_end1-FlushInners
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_1
	.quad	.LBB1_17
	.quad	.LBB1_10
	.quad	.LBB1_17
	.quad	.LBB1_10
	.quad	.LBB1_19

	.text
	.globl	ExpandRecursives
	.p2align	4, 0x90
	.type	ExpandRecursives,@function
ExpandRecursives:                       # @ExpandRecursives
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 160
.Lcfi24:
	.cfi_offset %rbx, -56
.Lcfi25:
	.cfi_offset %r12, -48
.Lcfi26:
	.cfi_offset %r13, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$8388607, 88(%rsp)      # imm = 0x7FFFFF
	movl	$8388607, 92(%rsp)      # imm = 0x7FFFFF
	movl	$8388607, 96(%rsp)      # imm = 0x7FFFFF
	movq	$0, 48(%rsp)
	testq	%rbx, %rbx
	jne	.LBB2_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_2:                                # %.preheader189
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB2_140
# BB#3:                                 # %.preheader188.lr.ph.lr.ph
                                        # implicit-def: %RCX
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	jmp	.LBB2_80
.LBB2_4:                                #   in Loop: Header=BB2_80 Depth=1
	testb	$16, 42(%r14)
	jne	.LBB2_11
# BB#5:                                 #   in Loop: Header=BB2_80 Depth=1
	movl	$1, %edx
	movq	%r14, %rdi
	leaq	56(%rsp), %rsi
	leaq	40(%rsp), %rcx
	callq	Constrained
	movq	(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB2_6:                                #   Parent Loop BB2_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	leaq	16(%rdx), %rax
	cmpb	$0, 32(%rdx)
	je	.LBB2_6
# BB#7:                                 #   in Loop: Header=BB2_80 Depth=1
	movl	52(%rdx), %eax
	cmpl	56(%rsp), %eax
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	jg	.LBB2_48
# BB#8:                                 #   in Loop: Header=BB2_80 Depth=1
	movl	60(%rdx), %ecx
	addl	%ecx, %eax
	cmpl	60(%rsp), %eax
	jg	.LBB2_48
# BB#9:                                 #   in Loop: Header=BB2_80 Depth=1
	cmpl	64(%rsp), %ecx
	jg	.LBB2_48
# BB#10:                                # %._crit_edge275
                                        #   in Loop: Header=BB2_80 Depth=1
	movl	48(%r12), %esi
	movl	56(%r12), %edx
.LBB2_11:                               #   in Loop: Header=BB2_80 Depth=1
	xorl	%ecx, %ecx
	movq	%r14, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	AdjustSize
	testb	$16, 42(%r14)
	jne	.LBB2_13
# BB#12:                                #   in Loop: Header=BB2_80 Depth=1
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	52(%rbx), %esi
	movl	60(%rbx), %edx
	movl	$1, %ecx
	movq	%r14, %rdi
	callq	AdjustSize
	movl	$19, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	%rbx, %rcx
	callq	Interpose
.LBB2_13:                               #   in Loop: Header=BB2_80 Depth=1
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	Promote
	movq	%r12, xx_hold(%rip)
	movq	24(%r12), %rax
	cmpq	%r12, %rax
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB2_20
	.p2align	4, 0x90
.LBB2_14:                               # %.lr.ph206
                                        #   Parent Loop BB2_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_16
# BB#15:                                #   in Loop: Header=BB2_14 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_16:                               #   in Loop: Header=BB2_14 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_18
# BB#17:                                #   in Loop: Header=BB2_14 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_18:                               #   in Loop: Header=BB2_14 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %r12
	movq	24(%r12), %rax
	cmpq	%r12, %rax
	jne	.LBB2_14
# BB#19:                                # %..preheader187_crit_edge
                                        #   in Loop: Header=BB2_80 Depth=1
	movl	%ecx, zz_size(%rip)
.LBB2_20:                               # %.preheader187
                                        #   in Loop: Header=BB2_80 Depth=1
	movq	8(%r12), %rax
	cmpq	%r12, %rax
	je	.LBB2_27
	.p2align	4, 0x90
.LBB2_21:                               # %.lr.ph208
                                        #   Parent Loop BB2_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_21 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_23:                               #   in Loop: Header=BB2_21 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_25
# BB#24:                                #   in Loop: Header=BB2_21 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_25:                               #   in Loop: Header=BB2_21 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %r12
	movq	8(%r12), %rax
	cmpq	%r12, %rax
	jne	.LBB2_21
# BB#26:                                # %._crit_edge209
                                        #   in Loop: Header=BB2_80 Depth=1
	movl	%ecx, zz_size(%rip)
.LBB2_27:                               #   in Loop: Header=BB2_80 Depth=1
	movq	%r12, zz_hold(%rip)
	movzbl	32(%r12), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r12), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r12)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	%rbp, xx_hold(%rip)
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB2_34
	.p2align	4, 0x90
.LBB2_28:                               # %.lr.ph213
                                        #   Parent Loop BB2_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_30
# BB#29:                                #   in Loop: Header=BB2_28 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_30:                               #   in Loop: Header=BB2_28 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_32
# BB#31:                                #   in Loop: Header=BB2_28 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_32:                               #   in Loop: Header=BB2_28 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbp
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_28
# BB#33:                                # %..preheader186_crit_edge
                                        #   in Loop: Header=BB2_80 Depth=1
	movl	%ecx, zz_size(%rip)
.LBB2_34:                               # %.preheader186
                                        #   in Loop: Header=BB2_80 Depth=1
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB2_41
	.p2align	4, 0x90
.LBB2_35:                               # %.lr.ph215
                                        #   Parent Loop BB2_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_37
# BB#36:                                #   in Loop: Header=BB2_35 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_37:                               #   in Loop: Header=BB2_35 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_39
# BB#38:                                #   in Loop: Header=BB2_35 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_39:                               #   in Loop: Header=BB2_35 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbp
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_35
# BB#40:                                # %._crit_edge216
                                        #   in Loop: Header=BB2_80 Depth=1
	movl	%ecx, zz_size(%rip)
.LBB2_41:                               #   in Loop: Header=BB2_80 Depth=1
	movq	%rbp, zz_hold(%rip)
	movzbl	32(%rbp), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbp), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbp)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_43
# BB#42:                                #   in Loop: Header=BB2_80 Depth=1
	xorl	%esi, %esi
	callq	FlushInners
.LBB2_43:                               #   in Loop: Header=BB2_80 Depth=1
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.LBB2_139
# BB#44:                                #   in Loop: Header=BB2_80 Depth=1
	movq	%rbx, xx_res(%rip)
	movq	%rax, xx_hold(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_50
# BB#45:                                #   in Loop: Header=BB2_80 Depth=1
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rcx, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB2_51
# BB#46:                                #   in Loop: Header=BB2_80 Depth=1
	testq	%rbx, %rbx
	je	.LBB2_51
# BB#47:                                #   in Loop: Header=BB2_80 Depth=1
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rbx), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rbx), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rbx)
	movq	%rbx, 24(%rdx)
	jmp	.LBB2_51
.LBB2_48:                               #   in Loop: Header=BB2_80 Depth=1
	movq	24(%r12), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_55
# BB#49:                                #   in Loop: Header=BB2_80 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB2_56
.LBB2_50:                               # %.thread
                                        #   in Loop: Header=BB2_80 Depth=1
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB2_51:                               #   in Loop: Header=BB2_80 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_78
# BB#52:                                #   in Loop: Header=BB2_80 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	zz_res(%rip), %rax
	movq	xx_res(%rip), %rcx
	movq	%rax, xx_tmp(%rip)
	movq	%rcx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_79
# BB#53:                                #   in Loop: Header=BB2_80 Depth=1
	testq	%rcx, %rcx
	je	.LBB2_79
# BB#54:                                #   in Loop: Header=BB2_80 Depth=1
	movq	(%rax), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	jmp	.LBB2_79
.LBB2_55:                               #   in Loop: Header=BB2_80 Depth=1
	xorl	%ecx, %ecx
.LBB2_56:                               #   in Loop: Header=BB2_80 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_58
# BB#57:                                #   in Loop: Header=BB2_80 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_58:                               #   in Loop: Header=BB2_80 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB2_60
# BB#59:                                #   in Loop: Header=BB2_80 Depth=1
	callq	DisposeObject
.LBB2_60:                               #   in Loop: Header=BB2_80 Depth=1
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_62
# BB#61:                                #   in Loop: Header=BB2_80 Depth=1
	callq	DisposeObject
.LBB2_62:                               #   in Loop: Header=BB2_80 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_64
# BB#63:                                #   in Loop: Header=BB2_80 Depth=1
	callq	DisposeObject
.LBB2_64:                               #   in Loop: Header=BB2_80 Depth=1
	movq	%rbp, xx_hold(%rip)
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB2_71
	.p2align	4, 0x90
.LBB2_65:                               # %.lr.ph220
                                        #   Parent Loop BB2_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_67
# BB#66:                                #   in Loop: Header=BB2_65 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_67:                               #   in Loop: Header=BB2_65 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_69
# BB#68:                                #   in Loop: Header=BB2_65 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_69:                               #   in Loop: Header=BB2_65 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbp
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_65
# BB#70:                                # %..preheader185_crit_edge
                                        #   in Loop: Header=BB2_80 Depth=1
	movl	%ecx, zz_size(%rip)
.LBB2_71:                               # %.preheader185
                                        #   in Loop: Header=BB2_80 Depth=1
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB2_138
	.p2align	4, 0x90
.LBB2_72:                               # %.lr.ph222
                                        #   Parent Loop BB2_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_74
# BB#73:                                #   in Loop: Header=BB2_72 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_74:                               #   in Loop: Header=BB2_72 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_76
# BB#75:                                #   in Loop: Header=BB2_72 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_76:                               #   in Loop: Header=BB2_72 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbp
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_72
	jmp	.LBB2_137
.LBB2_78:                               # %.thread294
                                        #   in Loop: Header=BB2_80 Depth=1
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB2_79:                               #   in Loop: Header=BB2_80 Depth=1
	movq	xx_hold(%rip), %rbp
	jmp	.LBB2_138
	.p2align	4, 0x90
.LBB2_80:                               # %.preheader188
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_81 Depth 2
                                        #     Child Loop BB2_6 Depth 2
                                        #     Child Loop BB2_14 Depth 2
                                        #     Child Loop BB2_21 Depth 2
                                        #     Child Loop BB2_28 Depth 2
                                        #     Child Loop BB2_35 Depth 2
                                        #     Child Loop BB2_65 Depth 2
                                        #     Child Loop BB2_72 Depth 2
                                        #     Child Loop BB2_125 Depth 2
                                        #     Child Loop BB2_132 Depth 2
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB2_81:                               #   Parent Loop BB2_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB2_81
# BB#82:                                #   in Loop: Header=BB2_80 Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rdx
	cmpq	%rax, %rdx
	je	.LBB2_84
# BB#83:                                #   in Loop: Header=BB2_80 Depth=1
	movq	%rdx, zz_res(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rax), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_84:                               #   in Loop: Header=BB2_80 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rdx
	cmpq	%rax, %rdx
	je	.LBB2_86
# BB#85:                                #   in Loop: Header=BB2_80 Depth=1
	movq	%rdx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rax, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rax
.LBB2_86:                               #   in Loop: Header=BB2_80 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %edx
	leaq	zz_lengths(%rdx), %rsi
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
	addb	$-11, %dl
	leaq	33(%rax), %rdi
	cmpb	$2, %dl
	cmovbq	%rdi, %rsi
	movzbl	(%rsi), %edx
	movl	%edx, zz_size(%rip)
	movq	zz_free(,%rdx,8), %rsi
	movq	%rsi, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rdx,8)
	cmpb	$123, %cl
	je	.LBB2_88
# BB#87:                                #   in Loop: Header=BB2_80 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_88:                               #   in Loop: Header=BB2_80 Depth=1
	movq	80(%rbp), %r14
	movzbl	zz_lengths+8(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r12
	testq	%r12, %r12
	je	.LBB2_90
# BB#89:                                #   in Loop: Header=BB2_80 Depth=1
	movq	%r12, zz_hold(%rip)
	movq	(%r12), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_91
	.p2align	4, 0x90
.LBB2_90:                               #   in Loop: Header=BB2_80 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r12
	movq	%r12, zz_hold(%rip)
.LBB2_91:                               #   in Loop: Header=BB2_80 Depth=1
	movb	$8, 32(%r12)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%r12, 8(%r12)
	movq	%r12, (%r12)
	movq	80(%r14), %rax
	movq	%rax, 80(%r12)
	movzwl	42(%r12), %eax
	andl	$61311, %eax            # imm = 0xEF7F
	orl	$128, %eax
	movw	%ax, 42(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%r12)
	movups	%xmm0, 144(%r12)
	movups	%xmm0, 128(%r12)
	movq	80(%r14), %rcx
	movzbl	43(%rcx), %ecx
	andl	$32, %ecx
	andl	$-4355, %eax            # imm = 0xEEFD
	leal	(%rax,%rcx,8), %eax
	movups	%xmm0, 88(%r12)
	movb	$-127, 40(%r12)
	movw	%ax, 42(%r12)
	leaq	32(%r14), %rsi
	movq	%r14, %rdi
	callq	CopyObject
	movq	%rax, %r13
	movq	%r13, %rdi
	callq	DetachEnv
	movq	%rax, %r15
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_93
# BB#92:                                #   in Loop: Header=BB2_80 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_94
	.p2align	4, 0x90
.LBB2_93:                               #   in Loop: Header=BB2_80 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_94:                               #   in Loop: Header=BB2_80 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB2_97
# BB#95:                                #   in Loop: Header=BB2_80 Depth=1
	testq	%rax, %rax
	je	.LBB2_97
# BB#96:                                #   in Loop: Header=BB2_80 Depth=1
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_97:                               #   in Loop: Header=BB2_80 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB2_100
# BB#98:                                #   in Loop: Header=BB2_80 Depth=1
	testq	%rax, %rax
	je	.LBB2_100
# BB#99:                                #   in Loop: Header=BB2_80 Depth=1
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_100:                              #   in Loop: Header=BB2_80 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_102
# BB#101:                               #   in Loop: Header=BB2_80 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_103
	.p2align	4, 0x90
.LBB2_102:                              #   in Loop: Header=BB2_80 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_103:                              #   in Loop: Header=BB2_80 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_105
# BB#104:                               #   in Loop: Header=BB2_80 Depth=1
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_105:                              #   in Loop: Header=BB2_80 Depth=1
	testq	%r12, %r12
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	je	.LBB2_108
# BB#106:                               #   in Loop: Header=BB2_80 Depth=1
	testq	%rax, %rax
	je	.LBB2_108
# BB#107:                               #   in Loop: Header=BB2_80 Depth=1
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_108:                              #   in Loop: Header=BB2_80 Depth=1
	movzwl	42(%r14), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	andl	$1, %edx
	xorl	%ecx, %ecx
	testb	$1, 43(%r12)
	je	.LBB2_110
# BB#109:                               #   in Loop: Header=BB2_80 Depth=1
	shrl	$2, %eax
	andl	$1, %eax
	movl	%eax, %ecx
.LBB2_110:                              #   in Loop: Header=BB2_80 Depth=1
	leaq	64(%r14), %rax
	subq	$8, %rsp
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%r12, %rdi
	movq	%r15, %rsi
	pushq	$0
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	leaq	40(%rsp), %rbx
	pushq	%rbx
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	leaq	40(%rsp), %rbx
	pushq	%rbx
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	leaq	80(%rsp), %rbx
	pushq	%rbx
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	leaq	136(%rsp), %rbx
	pushq	%rbx
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	callq	SizeGalley
	addq	$64, %rsp
.Lcfi38:
	.cfi_adjust_cfa_offset -64
	xorl	%edx, %edx
	movq	%r14, %rdi
	leaq	72(%rsp), %rsi
	leaq	40(%rsp), %rcx
	callq	Constrained
	movl	48(%r12), %esi
	cmpl	72(%rsp), %esi
	jg	.LBB2_113
# BB#111:                               #   in Loop: Header=BB2_80 Depth=1
	movl	56(%r12), %edx
	leal	(%rdx,%rsi), %eax
	cmpl	76(%rsp), %eax
	jg	.LBB2_113
# BB#112:                               #   in Loop: Header=BB2_80 Depth=1
	cmpl	80(%rsp), %edx
	jle	.LBB2_4
.LBB2_113:                              #   in Loop: Header=BB2_80 Depth=1
	movq	24(%r12), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_115
# BB#114:                               #   in Loop: Header=BB2_80 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB2_116
	.p2align	4, 0x90
.LBB2_115:                              #   in Loop: Header=BB2_80 Depth=1
	xorl	%ecx, %ecx
.LBB2_116:                              #   in Loop: Header=BB2_80 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_118
# BB#117:                               #   in Loop: Header=BB2_80 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_118:                              #   in Loop: Header=BB2_80 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB2_120
# BB#119:                               #   in Loop: Header=BB2_80 Depth=1
	callq	DisposeObject
.LBB2_120:                              #   in Loop: Header=BB2_80 Depth=1
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_122
# BB#121:                               #   in Loop: Header=BB2_80 Depth=1
	callq	DisposeObject
.LBB2_122:                              #   in Loop: Header=BB2_80 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_124
# BB#123:                               #   in Loop: Header=BB2_80 Depth=1
	callq	DisposeObject
.LBB2_124:                              #   in Loop: Header=BB2_80 Depth=1
	movq	%rbp, xx_hold(%rip)
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB2_131
	.p2align	4, 0x90
.LBB2_125:                              # %.lr.ph
                                        #   Parent Loop BB2_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_127
# BB#126:                               #   in Loop: Header=BB2_125 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_127:                              #   in Loop: Header=BB2_125 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_129
# BB#128:                               #   in Loop: Header=BB2_125 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_129:                              #   in Loop: Header=BB2_125 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbp
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_125
# BB#130:                               # %..preheader_crit_edge
                                        #   in Loop: Header=BB2_80 Depth=1
	movl	%ecx, zz_size(%rip)
.LBB2_131:                              # %.preheader
                                        #   in Loop: Header=BB2_80 Depth=1
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB2_138
	.p2align	4, 0x90
.LBB2_132:                              # %.lr.ph202
                                        #   Parent Loop BB2_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_134
# BB#133:                               #   in Loop: Header=BB2_132 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_134:                              #   in Loop: Header=BB2_132 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_136
# BB#135:                               #   in Loop: Header=BB2_132 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_136:                              #   in Loop: Header=BB2_132 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbp
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_132
.LBB2_137:                              # %._crit_edge
                                        #   in Loop: Header=BB2_80 Depth=1
	movl	%ecx, zz_size(%rip)
.LBB2_138:                              # %.outer.backedge.sink.split
                                        #   in Loop: Header=BB2_80 Depth=1
	movq	%rbp, zz_hold(%rip)
	movzbl	32(%rbp), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbp), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbp)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
.LBB2_139:                              # %.outer.backedge
                                        #   in Loop: Header=BB2_80 Depth=1
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB2_80
.LBB2_140:                              # %.outer._crit_edge
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	ExpandRecursives, .Lfunc_end2-ExpandRecursives
	.cfi_endproc

	.globl	Promote
	.p2align	4, 0x90
	.type	Promote,@function
Promote:                                # @Promote
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 128
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %r12
	cmpb	$8, 32(%r12)
	je	.LBB3_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_2:
	cmpq	%r12, %rbp
	je	.LBB3_5
# BB#3:
	movb	32(%rbp), %al
	testb	%al, %al
	je	.LBB3_5
# BB#4:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.18, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_5:
	cmpq	%rbp, 8(%r12)
	jne	.LBB3_7
# BB#6:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.19, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_7:
	cmpq	%r12, %rbp
	movq	24(%rsp), %rax          # 8-byte Reload
	movb	$122, 32(%rax)
	movq	80(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	je	.LBB3_13
# BB#8:                                 # %.preheader581.preheader
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB3_9:                                # %.preheader581
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB3_9
# BB#10:                                # %.preheader581
	cmpb	$1, %al
	je	.LBB3_12
# BB#11:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_12:                               # %.loopexit582
	movq	8(%rbp), %r14
	jmp	.LBB3_25
.LBB3_13:
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB3_15
# BB#14:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB3_16
.LBB3_15:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB3_16:
	movb	$1, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	34(%r12), %eax
	movw	%ax, 34(%rbx)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r12), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbx), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbx)
	andl	36(%r12), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movw	$256, 41(%rbx)          # imm = 0x100
	movzwl	44(%rbx), %eax
	andl	$1, %r15d
	shll	$9, %r15d
	andl	$127, %eax
	leal	1024(%r15,%rax), %eax
	movw	%ax, 44(%rbx)
	movw	$0, 46(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_18
# BB#17:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_19
.LBB3_18:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_19:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB3_21
# BB#20:
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_21:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB3_24
# BB#22:
	testq	%rax, %rax
	je	.LBB3_24
# BB#23:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_24:
	movq	%r12, %r14
.LBB3_25:
	leaq	8(%r12), %rcx
	movq	104(%r12), %rax
	testq	%rax, %rax
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	je	.LBB3_76
# BB#26:
	movq	(%rax), %rbp
	cmpq	%rax, %rbp
	je	.LBB3_30
# BB#27:
	testb	$4, 43(%r12)
	jne	.LBB3_30
	.p2align	4, 0x90
.LBB3_28:                               # %.preheader580
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB3_28
	jmp	.LBB3_31
.LBB3_30:
	xorl	%ebp, %ebp
.LBB3_31:                               # %.preheader578
	movq	(%rcx), %r13
	cmpq	%r14, %r13
	je	.LBB3_76
# BB#32:                                # %.preheader577.lr.ph
	movl	$4095, %r15d            # imm = 0xFFF
	jmp	.LBB3_49
.LBB3_33:                               #   in Loop: Header=BB3_49 Depth=1
	xorl	%ecx, %ecx
.LBB3_34:                               #   in Loop: Header=BB3_49 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_36
# BB#35:                                #   in Loop: Header=BB3_49 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_36:                               #   in Loop: Header=BB3_49 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB3_38
# BB#37:                                #   in Loop: Header=BB3_49 Depth=1
	callq	DisposeObject
.LBB3_38:                               #   in Loop: Header=BB3_49 Depth=1
	movq	8(%rbx), %rdi
	cmpq	%rbx, %rdi
	je	.LBB3_48
	.p2align	4, 0x90
.LBB3_39:                               # %.preheader576
                                        #   Parent Loop BB3_49 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB3_39
# BB#40:                                #   in Loop: Header=BB3_49 Depth=1
	movq	no_fpos(%rip), %rsi
	callq	CopyObject
	movq	%rax, %r15
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_42
# BB#41:                                #   in Loop: Header=BB3_49 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_43
.LBB3_42:                               #   in Loop: Header=BB3_49 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_43:                               #   in Loop: Header=BB3_49 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB3_45
# BB#44:                                #   in Loop: Header=BB3_49 Depth=1
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_45:                               #   in Loop: Header=BB3_49 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB3_48
# BB#46:                                #   in Loop: Header=BB3_49 Depth=1
	testq	%rax, %rax
	je	.LBB3_48
# BB#47:                                #   in Loop: Header=BB3_49 Depth=1
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_48:                               #   in Loop: Header=BB3_49 Depth=1
	orb	$2, 45(%rbp)
	movl	$4095, %r15d            # imm = 0xFFF
	jmp	.LBB3_75
	.p2align	4, 0x90
.LBB3_49:                               # %.preheader577
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_50 Depth 2
                                        #     Child Loop BB3_39 Depth 2
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB3_50:                               #   Parent Loop BB3_49 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB3_50
# BB#51:                                #   in Loop: Header=BB3_49 Depth=1
	leaq	32(%rbx), %rdx
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jae	.LBB3_53
# BB#52:                                #   in Loop: Header=BB3_49 Depth=1
	andb	$-5, 43(%r12)
	movzbl	32(%rbx), %edi
	leaq	64(%rbx), %rsi
	callq	MakeWord
	movq	%rax, %rbp
	movzbl	43(%r12), %eax
	andl	$1, %eax
	movl	48(%rbx,%rax,4), %eax
	movl	%eax, 48(%rbp)
	movzbl	43(%r12), %eax
	andl	$1, %eax
	movl	56(%rbx,%rax,4), %eax
	movl	%eax, 56(%rbp)
	movl	40(%rbx), %ecx
	andl	%r15d, %ecx
	movl	40(%rbp), %eax
	movl	$-4096, %edx            # imm = 0xF000
	andl	%edx, %eax
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movl	40(%rbx), %ecx
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	%edx, %ecx
	andl	$-4190209, %eax         # imm = 0xFFC00FFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movl	40(%rbx), %ecx
	movl	$4194304, %edx          # imm = 0x400000
	andl	%edx, %ecx
	andl	$-4194305, %eax         # imm = 0xFFBFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movl	40(%rbx), %ecx
	movl	$528482304, %edx        # imm = 0x1F800000
	andl	%edx, %ecx
	andl	$-528482305, %eax       # imm = 0xE07FFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movl	40(%rbx), %ecx
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	%edx, %ecx
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	jmp	.LBB3_66
	.p2align	4, 0x90
.LBB3_53:                               #   in Loop: Header=BB3_49 Depth=1
	cmpb	$1, %al
	jne	.LBB3_59
# BB#54:                                #   in Loop: Header=BB3_49 Depth=1
	testq	%rbp, %rbp
	je	.LBB3_61
# BB#55:                                #   in Loop: Header=BB3_49 Depth=1
	cmpb	$1, 32(%rbp)
	jne	.LBB3_62
# BB#56:                                #   in Loop: Header=BB3_49 Depth=1
	movzwl	34(%rbx), %eax
	movw	%ax, 34(%rbp)
	movl	36(%rbx), %eax
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %eax
	movl	36(%rbp), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	movl	36(%rbx), %ecx
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	movzwl	44(%rbx), %ecx
	andl	$128, %ecx
	movzwl	44(%rbp), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movzwl	44(%rbx), %ecx
	andl	$256, %ecx              # imm = 0x100
	andl	$-257, %eax             # imm = 0xFEFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movzwl	44(%rbx), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %eax             # imm = 0xFDFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movzwl	44(%rbx), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %eax            # imm = 0xE3FF
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movzwl	44(%rbx), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %eax             # imm = 0x1FFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movzwl	46(%rbx), %eax
	movw	%ax, 46(%rbp)
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB3_38
# BB#57:                                #   in Loop: Header=BB3_49 Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_33
# BB#58:                                #   in Loop: Header=BB3_49 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB3_34
	.p2align	4, 0x90
.LBB3_59:                               #   in Loop: Header=BB3_49 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB3_75
# BB#60:                                #   in Loop: Header=BB3_49 Depth=1
	andb	$-5, 43(%r12)
	movl	$12, %edi
	movl	$.L.str.21, %esi
	callq	MakeWord
	movq	%rax, %rbp
	movzbl	43(%r12), %eax
	andl	$1, %eax
	movl	48(%rbx,%rax,4), %eax
	movl	%eax, 48(%rbp)
	movzbl	43(%r12), %eax
	andl	$1, %eax
	movl	56(%rbx,%rax,4), %eax
	movl	%eax, 56(%rbp)
	jmp	.LBB3_66
.LBB3_61:                               #   in Loop: Header=BB3_49 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB3_75
.LBB3_62:                               #   in Loop: Header=BB3_49 Depth=1
	andb	$-5, 43(%r12)
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB3_64
# BB#63:                                #   in Loop: Header=BB3_49 Depth=1
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB3_65
.LBB3_64:                               #   in Loop: Header=BB3_49 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB3_65:                               #   in Loop: Header=BB3_49 Depth=1
	movb	$1, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movzwl	34(%rbx), %eax
	movw	%ax, 34(%rbp)
	movl	36(%rbx), %eax
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %eax
	movl	36(%rbp), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	movl	36(%rbx), %ecx
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	movzwl	44(%rbx), %ecx
	andl	$128, %ecx
	movzwl	44(%rbp), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movzwl	44(%rbx), %ecx
	andl	$256, %ecx              # imm = 0x100
	andl	$-257, %eax             # imm = 0xFEFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movzwl	44(%rbx), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %eax             # imm = 0xFDFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movzwl	44(%rbx), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %eax            # imm = 0xE3FF
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movzwl	44(%rbx), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %eax             # imm = 0x1FFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movzwl	46(%rbx), %ecx
	movw	%cx, 46(%rbp)
	orl	$512, %eax              # imm = 0x200
	movw	%ax, 44(%rbp)
	movw	$1, 41(%rbp)
	.p2align	4, 0x90
.LBB3_66:                               #   in Loop: Header=BB3_49 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_68
# BB#67:                                #   in Loop: Header=BB3_49 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_69
	.p2align	4, 0x90
.LBB3_68:                               #   in Loop: Header=BB3_49 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_69:                               #   in Loop: Header=BB3_49 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	104(%r12), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB3_72
# BB#70:                                #   in Loop: Header=BB3_49 Depth=1
	testq	%rcx, %rcx
	je	.LBB3_72
# BB#71:                                #   in Loop: Header=BB3_49 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_72:                               #   in Loop: Header=BB3_49 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB3_75
# BB#73:                                #   in Loop: Header=BB3_49 Depth=1
	testq	%rax, %rax
	je	.LBB3_75
# BB#74:                                #   in Loop: Header=BB3_49 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_75:                               #   in Loop: Header=BB3_49 Depth=1
	movq	8(%r13), %r13
	cmpq	%r14, %r13
	jne	.LBB3_49
.LBB3_76:                               # %.loopexit579
	leaq	32(%r12), %r15
	movzwl	42(%r12), %eax
	movl	%eax, %ecx
	andl	$288, %ecx              # imm = 0x120
	cmpl	$288, %ecx              # imm = 0x120
	jne	.LBB3_79
# BB#77:
	movq	8(%rsp), %rbp           # 8-byte Reload
	testb	$4, 42(%rbp)
	je	.LBB3_80
# BB#78:
	movq	80(%r12), %rdi
	callq	SymName
	movq	%rax, %rbp
	movl	$22, %edi
	movl	$3, %esi
	movl	$.L.str.22, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	movq	%rbp, %r9
	movq	8(%rsp), %rbp           # 8-byte Reload
	callq	Error
	movw	42(%r12), %ax
	testb	$1, %ah
	je	.LBB3_81
	jmp	.LBB3_128
.LBB3_79:
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB3_80:
	testb	$1, %ah
	jne	.LBB3_128
.LBB3_81:
	movq	80(%rbp), %rax
	cmpq	PrintSym(%rip), %rax
	je	.LBB3_137
# BB#82:
	movzwl	42(%rbp), %eax
	testb	$16, %al
	je	.LBB3_84
# BB#83:
	testb	$1, 43(%r12)
	jne	.LBB3_255
.LBB3_84:
	testb	$8, %al
	je	.LBB3_86
# BB#85:
	testb	$1, 43(%r12)
	je	.LBB3_315
.LBB3_86:
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	(%r13), %rdi
	cmpq	%r14, %rdi
	je	.LBB3_124
# BB#87:                                # %.preheader569.lr.ph.lr.ph
	movzwl	42(%r12), %r15d
	andl	$256, %r15d             # imm = 0x100
	movq	%r13, %rbp
.LBB3_88:                               # %.preheader569.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_106 Depth 2
                                        #       Child Loop BB3_107 Depth 3
                                        #       Child Loop BB3_110 Depth 3
                                        #     Child Loop BB3_89 Depth 2
                                        #       Child Loop BB3_90 Depth 3
                                        #       Child Loop BB3_93 Depth 3
	testw	%r15w, %r15w
	jne	.LBB3_106
	.p2align	4, 0x90
.LBB3_89:                               # %.preheader569.us
                                        #   Parent Loop BB3_88 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_90 Depth 3
                                        #       Child Loop BB3_93 Depth 3
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB3_90:                               #   Parent Loop BB3_88 Depth=1
                                        #     Parent Loop BB3_89 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %eax
	testb	%al, %al
	je	.LBB3_90
# BB#91:                                #   in Loop: Header=BB3_89 Depth=2
	cmpb	$9, %al
	jne	.LBB3_94
# BB#92:                                #   in Loop: Header=BB3_89 Depth=2
	movq	8(%rcx), %rsi
	addq	$16, %rsi
	.p2align	4, 0x90
.LBB3_93:                               #   Parent Loop BB3_88 Depth=1
                                        #     Parent Loop BB3_89 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rsi), %rbx
	movzbl	32(%rbx), %eax
	leaq	16(%rbx), %rsi
	testb	%al, %al
	je	.LBB3_93
	jmp	.LBB3_95
	.p2align	4, 0x90
.LBB3_94:                               # %.loopexit568.us.loopexit846
                                        #   in Loop: Header=BB3_89 Depth=2
	movq	%rcx, %rbx
.LBB3_95:                               # %.loopexit568.us
                                        #   in Loop: Header=BB3_89 Depth=2
	movl	%eax, %edx
	andb	$-4, %dl
	cmpb	$20, %dl
	jne	.LBB3_99
# BB#96:                                #   in Loop: Header=BB3_89 Depth=2
	cmpq	%rbx, %rcx
	je	.LBB3_98
# BB#97:                                #   in Loop: Header=BB3_89 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.35, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_98:                               #   in Loop: Header=BB3_89 Depth=2
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	HandleHeader
	jmp	.LBB3_105
	.p2align	4, 0x90
.LBB3_99:                               #   in Loop: Header=BB3_89 Depth=2
	addb	$-119, %al
	cmpb	$19, %al
	ja	.LBB3_123
# BB#100:                               #   in Loop: Header=BB3_89 Depth=2
	movq	%rdi, xx_link(%rip)
	movq	%rdi, zz_hold(%rip)
	movq	8(%rdi), %rcx
	cmpq	%rdi, %rcx
	je	.LBB3_102
# BB#101:                               #   in Loop: Header=BB3_89 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rdi), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rdi
.LBB3_102:                              #   in Loop: Header=BB3_89 Depth=2
	movq	%rdi, zz_res(%rip)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rdi, %rdi
	je	.LBB3_105
# BB#103:                               #   in Loop: Header=BB3_89 Depth=2
	testq	%rcx, %rcx
	je	.LBB3_105
# BB#104:                               #   in Loop: Header=BB3_89 Depth=2
	movq	(%rcx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rdi), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB3_105:                              # %.backedge571.us
                                        #   in Loop: Header=BB3_89 Depth=2
	movq	(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.LBB3_89
	jmp	.LBB3_124
	.p2align	4, 0x90
.LBB3_106:                              # %.preheader569
                                        #   Parent Loop BB3_88 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_107 Depth 3
                                        #       Child Loop BB3_110 Depth 3
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB3_107:                              #   Parent Loop BB3_88 Depth=1
                                        #     Parent Loop BB3_106 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB3_107
# BB#108:                               #   in Loop: Header=BB3_106 Depth=2
	cmpb	$9, %dl
	jne	.LBB3_111
# BB#109:                               #   in Loop: Header=BB3_106 Depth=2
	movq	(%rcx), %rsi
	addq	$16, %rsi
	.p2align	4, 0x90
.LBB3_110:                              #   Parent Loop BB3_88 Depth=1
                                        #     Parent Loop BB3_106 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rsi), %rbx
	movzbl	32(%rbx), %edx
	leaq	16(%rbx), %rsi
	testb	%dl, %dl
	je	.LBB3_110
	jmp	.LBB3_112
	.p2align	4, 0x90
.LBB3_111:                              # %.loopexit568.loopexit847
                                        #   in Loop: Header=BB3_106 Depth=2
	movq	%rcx, %rbx
.LBB3_112:                              # %.loopexit568
                                        #   in Loop: Header=BB3_106 Depth=2
	movl	%edx, %eax
	andb	$-4, %al
	cmpb	$20, %al
	jne	.LBB3_116
# BB#113:                               #   in Loop: Header=BB3_106 Depth=2
	cmpq	%rbx, %rcx
	je	.LBB3_115
# BB#114:                               #   in Loop: Header=BB3_106 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.35, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_115:                              #   in Loop: Header=BB3_106 Depth=2
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	HandleHeader
	jmp	.LBB3_122
	.p2align	4, 0x90
.LBB3_116:                              #   in Loop: Header=BB3_106 Depth=2
	addb	$-119, %dl
	cmpb	$19, %dl
	ja	.LBB3_123
# BB#117:                               #   in Loop: Header=BB3_106 Depth=2
	movq	%rdi, xx_link(%rip)
	movq	%rdi, zz_hold(%rip)
	movq	8(%rdi), %rcx
	cmpq	%rdi, %rcx
	je	.LBB3_119
# BB#118:                               #   in Loop: Header=BB3_106 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rdi), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rdi
.LBB3_119:                              #   in Loop: Header=BB3_106 Depth=2
	movq	%rdi, zz_res(%rip)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rdi, %rdi
	je	.LBB3_122
# BB#120:                               #   in Loop: Header=BB3_106 Depth=2
	testq	%rcx, %rcx
	je	.LBB3_122
# BB#121:                               #   in Loop: Header=BB3_106 Depth=2
	movq	(%rcx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rdi), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB3_122:                              # %.backedge571
                                        #   in Loop: Header=BB3_106 Depth=2
	movq	(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.LBB3_106
	jmp	.LBB3_124
.LBB3_123:                              # %.outer.loopexit
                                        #   in Loop: Header=BB3_88 Depth=1
	movq	%rdi, %rbp
	movq	8(%rdi), %rax
	addq	$8, %rbp
	cmpq	%r14, %rax
	movq	%rax, %rdi
	jne	.LBB3_88
.LBB3_124:                              # %.outer._crit_edge
	cmpq	%r14, (%r13)
	jne	.LBB3_126
# BB#125:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.36, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_126:
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	16(%rdx), %rax
	leaq	24(%rdx), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	cmpq	24(%rdx), %rax
	je	.LBB3_316
# BB#127:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.37, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB3_316
.LBB3_128:
	testb	$32, %al
	je	.LBB3_81
# BB#129:
	movzwl	42(%rbp), %eax
	andl	$16, %eax
	testw	%ax, %ax
	jne	.LBB3_81
# BB#130:
	testb	$2, 45(%rbx)
	je	.LBB3_81
# BB#131:
	movq	24(%rbp), %rbx
	.p2align	4, 0x90
.LBB3_132:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB3_132
# BB#133:
	cmpb	$19, %al
	je	.LBB3_135
# BB#134:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_135:                              # %.loopexit575
	movzbl	zz_lengths+5(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r13
	testq	%r13, %r13
	je	.LBB3_265
# BB#136:
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB3_266
.LBB3_137:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	cmpq	%r14, %rax
	je	.LBB3_323
# BB#138:                               # %.preheader566.lr.ph
	xorl	%r15d, %r15d
	leaq	4(%rsp), %rbx
	movq	%r12, %r13
	jmp	.LBB3_222
.LBB3_236:                              #   in Loop: Header=BB3_222 Depth=1
	addq	$32, %rbp
	callq	Image
	movq	%rax, %rbx
	movl	$22, %edi
	movl	$10, %esi
	movl	$.L.str.29, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	%rbx, %r9
	callq	Error
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_146
# BB#237:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB3_147
.LBB3_139:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_151
# BB#140:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB3_152
.LBB3_141:                              #   in Loop: Header=BB3_222 Depth=1
	xorl	%ecx, %ecx
.LBB3_142:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_144
# BB#143:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_144:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB3_254
# BB#145:                               #   in Loop: Header=BB3_222 Depth=1
	callq	DisposeObject
	jmp	.LBB3_254
.LBB3_146:                              #   in Loop: Header=BB3_222 Depth=1
	xorl	%ecx, %ecx
.LBB3_147:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	leaq	4(%rsp), %rbx
	je	.LBB3_149
# BB#148:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_149:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB3_254
# BB#150:                               #   in Loop: Header=BB3_222 Depth=1
	callq	DisposeObject
	jmp	.LBB3_254
.LBB3_151:                              #   in Loop: Header=BB3_222 Depth=1
	xorl	%ecx, %ecx
.LBB3_152:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_154
# BB#153:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_154:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB3_254
# BB#155:                               #   in Loop: Header=BB3_222 Depth=1
	callq	DisposeObject
	jmp	.LBB3_254
.LBB3_156:                              #   in Loop: Header=BB3_222 Depth=1
	movq	Promote.page_label(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_158
# BB#157:                               #   in Loop: Header=BB3_222 Depth=1
	callq	DisposeObject
	movq	$0, Promote.page_label(%rip)
.LBB3_158:                              #   in Loop: Header=BB3_222 Depth=1
	movq	8(%rbp), %rbx
	.p2align	4, 0x90
.LBB3_159:                              #   Parent Loop BB3_222 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB3_159
# BB#160:                               #   in Loop: Header=BB3_222 Depth=1
	cmpb	$4, %al
	je	.LBB3_162
# BB#161:                               #   in Loop: Header=BB3_222 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.27, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_162:                              # %.loopexit564
                                        #   in Loop: Header=BB3_222 Depth=1
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB3_164
# BB#163:                               #   in Loop: Header=BB3_222 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.28, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbx), %rax
.LBB3_164:                              # %.preheader.preheader
                                        #   in Loop: Header=BB3_222 Depth=1
	leaq	4(%rsp), %rbx
	.p2align	4, 0x90
.LBB3_165:                              # %.preheader
                                        #   Parent Loop BB3_222 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB3_165
# BB#166:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, Promote.page_label(%rip)
	movq	24(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_168
# BB#167:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB3_168:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_170
# BB#169:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_170:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_194
# BB#171:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB3_195
.LBB3_172:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	80(%rbp), %rdi
	movb	32(%rdi), %r14b
	xorl	%ebx, %ebx
	cmpb	$40, %r14b
	setne	%bl
	leaq	48(%rsp), %rsi
	movl	%ebx, %edx
	leaq	64(%rsp), %rcx
	callq	Constrained
	cmpl	$8388607, 48(%rsp)      # imm = 0x7FFFFF
	jne	.LBB3_175
# BB#173:                               #   in Loop: Header=BB3_222 Depth=1
	cmpl	$8388607, 52(%rsp)      # imm = 0x7FFFFF
	jne	.LBB3_175
# BB#174:                               #   in Loop: Header=BB3_222 Depth=1
	cmpl	$8388607, 56(%rsp)      # imm = 0x7FFFFF
	je	.LBB3_176
.LBB3_175:                              #   in Loop: Header=BB3_222 Depth=1
	xorl	%eax, %eax
	cmpb	$40, %r14b
	setne	%al
	movq	80(%rbp), %rcx
	movl	48(%rcx,%rax,4), %edx
	movl	%edx, 40(%rsp)
	movl	56(%rcx,%rax,4), %eax
	movl	%eax, 36(%rsp)
	leaq	40(%rsp), %rdi
	leaq	36(%rsp), %rsi
	leaq	48(%rsp), %rdx
	callq	EnlargeToConstraint
	movq	80(%rbp), %rdi
	movl	40(%rsp), %esi
	movl	36(%rsp), %edx
	movl	%ebx, %ecx
	callq	AdjustSize
.LBB3_176:                              #   in Loop: Header=BB3_222 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_199
# BB#177:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB3_200
.LBB3_178:                              #   in Loop: Header=BB3_222 Depth=1
	movq	no_fpos(%rip), %rbx
	callq	Image
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.5, %edx
	movl	$0, %ecx
	movl	$.L.str.31, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	leaq	12(%rsp), %rbx
	pushq	%rbp
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi54:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB3_254
.LBB3_179:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_204
# BB#180:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB3_205
.LBB3_181:                              #   in Loop: Header=BB3_222 Depth=1
	movq	8(%rbp), %rbx
	cmpq	%rbp, %rbx
	jne	.LBB3_183
# BB#182:                               #   in Loop: Header=BB3_222 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.24, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbp), %rbx
	.p2align	4, 0x90
.LBB3_183:                              #   Parent Loop BB3_222 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB3_183
# BB#184:                               #   in Loop: Header=BB3_222 Depth=1
	cmpb	$8, %al
	je	.LBB3_186
# BB#185:                               #   in Loop: Header=BB3_222 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.25, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_186:                              # %.loopexit
                                        #   in Loop: Header=BB3_222 Depth=1
	testb	$2, 42(%rbx)
	jne	.LBB3_193
# BB#187:                               #   in Loop: Header=BB3_222 Depth=1
	movzbl	40(%rbx), %edx
	cmpl	$133, %edx
	je	.LBB3_189
# BB#188:                               #   in Loop: Header=BB3_222 Depth=1
	cmpb	$-126, %dl
	jne	.LBB3_209
.LBB3_189:                              #   in Loop: Header=BB3_222 Depth=1
	movb	%dl, 32(%rbp)
	movq	$0, 88(%rbp)
	movq	8(%rbx), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB3_190:                              #   Parent Loop BB3_222 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rsi
	leaq	16(%rsi), %rax
	cmpb	$0, 32(%rsi)
	je	.LBB3_190
# BB#191:                               #   in Loop: Header=BB3_222 Depth=1
	movq	88(%rbx), %rdi
	callq	CrossMake
	movq	%rax, 80(%rbp)
	movq	8(%rbp), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_210
# BB#192:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB3_211
.LBB3_193:                              #   in Loop: Header=BB3_222 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r13
	leaq	4(%rsp), %rbx
	jmp	.LBB3_254
.LBB3_194:                              #   in Loop: Header=BB3_222 Depth=1
	xorl	%ecx, %ecx
.LBB3_195:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_197
# BB#196:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_197:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB3_254
# BB#198:                               #   in Loop: Header=BB3_222 Depth=1
	callq	DisposeObject
	jmp	.LBB3_254
.LBB3_199:                              #   in Loop: Header=BB3_222 Depth=1
	xorl	%ecx, %ecx
.LBB3_200:                              #   in Loop: Header=BB3_222 Depth=1
	movq	8(%rsp), %r14           # 8-byte Reload
	leaq	4(%rsp), %rbx
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_202
# BB#201:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_202:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB3_254
# BB#203:                               #   in Loop: Header=BB3_222 Depth=1
	callq	DisposeObject
	jmp	.LBB3_254
.LBB3_204:                              #   in Loop: Header=BB3_222 Depth=1
	xorl	%ecx, %ecx
.LBB3_205:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_207
# BB#206:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_207:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB3_254
# BB#208:                               #   in Loop: Header=BB3_222 Depth=1
	callq	DisposeObject
	jmp	.LBB3_254
.LBB3_209:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%r14, 8(%rsp)           # 8-byte Spill
	leaq	32(%rbx), %r14
	movq	80(%rbx), %rdi
	callq	SymName
	movq	%rax, %rbp
	movl	$22, %edi
	movl	$4, %esi
	movl	$.L.str.26, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	%rbp, %r9
	callq	Error
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	KillGalley
	leaq	4(%rsp), %rbx
	jmp	.LBB3_254
.LBB3_210:                              #   in Loop: Header=BB3_222 Depth=1
	xorl	%ecx, %ecx
.LBB3_211:                              #   in Loop: Header=BB3_222 Depth=1
	leaq	4(%rsp), %rbx
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_213
# BB#212:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_213:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB3_215
# BB#214:                               #   in Loop: Header=BB3_222 Depth=1
	callq	DisposeObject
.LBB3_215:                              #   in Loop: Header=BB3_222 Depth=1
	movq	80(%rbp), %rdi
	callq	CrossSequence
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_217
# BB#216:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB3_218
.LBB3_217:                              #   in Loop: Header=BB3_222 Depth=1
	xorl	%ecx, %ecx
.LBB3_218:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_220
# BB#219:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_220:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB3_254
# BB#221:                               #   in Loop: Header=BB3_222 Depth=1
	callq	DisposeObject
	jmp	.LBB3_254
	.p2align	4, 0x90
.LBB3_222:                              # %.preheader566
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_223 Depth 2
                                        #     Child Loop BB3_226 Depth 2
                                        #     Child Loop BB3_183 Depth 2
                                        #     Child Loop BB3_190 Depth 2
                                        #     Child Loop BB3_159 Depth 2
                                        #     Child Loop BB3_165 Depth 2
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB3_223:                              #   Parent Loop BB3_222 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB3_223
# BB#224:                               #   in Loop: Header=BB3_222 Depth=1
	cmpb	$9, %cl
	jne	.LBB3_227
# BB#225:                               #   in Loop: Header=BB3_222 Depth=1
	movq	(%rbp), %rcx
	addq	$16, %rcx
	.p2align	4, 0x90
.LBB3_226:                              #   Parent Loop BB3_222 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rbp
	leaq	16(%rbp), %rcx
	cmpb	$0, 32(%rbp)
	je	.LBB3_226
.LBB3_227:                              # %.loopexit565
                                        #   in Loop: Header=BB3_222 Depth=1
	movzbl	32(%rbp), %edi
	testb	%r15b, %r15b
	jne	.LBB3_178
# BB#228:                               # %.loopexit565
                                        #   in Loop: Header=BB3_222 Depth=1
	movl	%edi, %ecx
	addb	$-128, %cl
	movzbl	%cl, %ecx
	jmpq	*.LJTI3_1(,%rcx,8)
.LBB3_229:                              #   in Loop: Header=BB3_222 Depth=1
	addb	$-2, %dil
	cmpb	$7, %dil
	jb	.LBB3_246
# BB#230:                               #   in Loop: Header=BB3_222 Depth=1
	movl	52(%rbp), %ecx
	addl	60(%rbp), %ecx
	je	.LBB3_246
# BB#231:                               #   in Loop: Header=BB3_222 Depth=1
	xorl	%edi, %edi
	callq	SetLengthDim
	movl	48(%rbp), %esi
	movl	56(%rbp), %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbp, %rdi
	movl	%esi, %edx
	pushq	%rbx
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	leaq	52(%rsp), %rax
	pushq	%rax
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi59:
	.cfi_adjust_cfa_offset -32
	movq	Promote.page_label(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB3_239
# BB#232:                               #   in Loop: Header=BB3_222 Depth=1
	movb	32(%rdx), %al
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB3_239
# BB#233:                               #   in Loop: Header=BB3_222 Depth=1
	addq	$64, %rdx
	jmp	.LBB3_240
.LBB3_234:                              #   in Loop: Header=BB3_222 Depth=1
	movq	80(%rbp), %rdi
	callq	CrossSequence
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_141
# BB#235:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB3_142
.LBB3_239:                              #   in Loop: Header=BB3_222 Depth=1
	movl	$.L.str.30, %edx
.LBB3_240:                              #   in Loop: Header=BB3_222 Depth=1
	movb	Promote.first(%rip), %cl
	movq	BackEnd(%rip), %rax
	testb	%cl, %cl
	je	.LBB3_242
# BB#241:                               #   in Loop: Header=BB3_222 Depth=1
	movl	56(%r12), %edi
	addl	48(%r12), %edi
	movl	60(%rbp), %esi
	addl	52(%rbp), %esi
	callq	*96(%rax)
	jmp	.LBB3_243
.LBB3_242:                              #   in Loop: Header=BB3_222 Depth=1
	movl	56(%r12), %edi
	addl	48(%r12), %edi
	movl	60(%rbp), %esi
	addl	52(%rbp), %esi
	callq	*88(%rax)
	movb	$1, Promote.first(%rip)
.LBB3_243:                              #   in Loop: Header=BB3_222 Depth=1
	movq	Promote.page_label(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_245
# BB#244:                               #   in Loop: Header=BB3_222 Depth=1
	callq	DisposeObject
	movq	$0, Promote.page_label(%rip)
.LBB3_245:                              #   in Loop: Header=BB3_222 Depth=1
	movl	$1, %edi
	callq	SetLengthDim
	movl	52(%rbp), %esi
	movl	60(%rbp), %ecx
	leal	(%rcx,%rsi), %eax
	movl	$1, %r8d
	movl	$0, %r9d
	movq	%rbp, %rdi
	movl	%esi, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	pushq	%rbx
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	leaq	52(%rsp), %rbp
	pushq	%rbp
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	callq	FixAndPrintObject
	addq	$32, %rsp
.Lcfi64:
	.cfi_adjust_cfa_offset -32
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
.LBB3_246:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_248
# BB#247:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB3_249
	.p2align	4, 0x90
.LBB3_248:                              #   in Loop: Header=BB3_222 Depth=1
	xorl	%ecx, %ecx
.LBB3_249:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_251
# BB#250:                               #   in Loop: Header=BB3_222 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_251:                              #   in Loop: Header=BB3_222 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB3_253
# BB#252:                               #   in Loop: Header=BB3_222 Depth=1
	callq	DisposeObject
.LBB3_253:                              #   in Loop: Header=BB3_222 Depth=1
	xorl	%edi, %edi
	callq	FilterScavenge
.LBB3_254:                              # %.backedge
                                        #   in Loop: Header=BB3_222 Depth=1
	leaq	8(%r13), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%r13), %rax
	cmpq	%r14, %rax
	jne	.LBB3_222
	jmp	.LBB3_323
.LBB3_255:
	testb	$4, %al
	je	.LBB3_315
# BB#256:
	movq	24(%rbp), %r13
	.p2align	4, 0x90
.LBB3_257:                              # =>This Inner Loop Header: Depth=1
	movq	(%r13), %r13
	movzbl	32(%r13), %eax
	testb	%al, %al
	je	.LBB3_257
# BB#258:
	movq	%r14, 8(%rsp)           # 8-byte Spill
	cmpb	$16, %al
	je	.LBB3_260
# BB#259:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.32, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_260:                              # %.preheader574.preheader
	movq	%r12, %rax
                                        # implicit-def: %R14
	.p2align	4, 0x90
.LBB3_261:                              # %.preheader574
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_263 Depth 2
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	je	.LBB3_287
# BB#262:                               # %.preheader34.i.preheader
                                        #   in Loop: Header=BB3_261 Depth=1
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB3_263:                              # %.preheader34.i
                                        #   Parent Loop BB3_261 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r14), %r14
	movzbl	32(%r14), %ecx
	testb	%cl, %cl
	je	.LBB3_263
# BB#264:                               #   in Loop: Header=BB3_261 Depth=1
	addb	$-9, %cl
	cmpb	$91, %cl
	jae	.LBB3_261
	jmp	.LBB3_288
.LBB3_265:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r13
	movq	%r13, zz_hold(%rip)
.LBB3_266:
	movb	$5, 32(%r13)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%r13, 8(%r13)
	movq	%r13, (%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r13)
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB3_268
# BB#267:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB3_269
.LBB3_268:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB3_269:
	movb	$1, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movw	$0, 41(%rbp)
	movzwl	44(%rbp), %eax
	andl	$127, %eax
	orl	$9216, %eax             # imm = 0x2400
	movw	%ax, 44(%rbp)
	movw	$0, 46(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_271
# BB#270:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_272
.LBB3_271:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_272:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%rbx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB3_275
# BB#273:
	testq	%rcx, %rcx
	je	.LBB3_275
# BB#274:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_275:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB3_278
# BB#276:
	testq	%rax, %rax
	je	.LBB3_278
# BB#277:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_278:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_280
# BB#279:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_281
.LBB3_280:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_281:
	movq	8(%rsp), %rbp           # 8-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%rbx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB3_284
# BB#282:
	testq	%rcx, %rcx
	je	.LBB3_284
# BB#283:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_284:
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB3_81
# BB#285:
	testq	%rax, %rax
	je	.LBB3_81
# BB#286:
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB3_81
.LBB3_287:                              # %.thread.i
	movl	$22, %edi
	movl	$1, %esi
	movl	$.L.str.64, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
	.p2align	4, 0x90
.LBB3_288:                              # %.outer.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_290 Depth 2
                                        #     Child Loop BB3_296 Depth 2
                                        #     Child Loop BB3_294 Depth 2
	leaq	32(%r14), %rbp
	jmp	.LBB3_290
	.p2align	4, 0x90
.LBB3_289:                              #   in Loop: Header=BB3_290 Depth=2
	callq	Image
	movq	%rax, %rbx
	movl	$22, %edi
	movl	$2, %esi
	movl	$.L.str.65, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	%rbx, %r9
	callq	Error
.LBB3_290:                              # %.backedge.i
                                        #   Parent Loop BB3_288 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp), %edi
	movl	%edi, %eax
	addb	$-2, %al
	movzbl	%al, %eax
	cmpb	$97, %al
	ja	.LBB3_292
# BB#291:                               # %.backedge.i
                                        #   in Loop: Header=BB3_290 Depth=2
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_292:                              #   in Loop: Header=BB3_290 Depth=2
	movq	no_fpos(%rip), %r15
	callq	Image
	movq	%rax, %rbx
	subq	$8, %rsp
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.5, %edx
	movl	$0, %ecx
	movl	$.L.str.66, %r9d
	xorl	%eax, %eax
	movq	%r15, %r8
	pushq	%rbx
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi67:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB3_290
	.p2align	4, 0x90
.LBB3_293:                              #   in Loop: Header=BB3_288 Depth=1
	movq	8(%r14), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB3_294:                              #   Parent Loop BB3_288 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %r14
	cmpb	$0, 32(%r14)
	leaq	16(%r14), %rax
	je	.LBB3_294
	jmp	.LBB3_288
.LBB3_295:                              #   in Loop: Header=BB3_288 Depth=1
	movq	(%r14), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB3_296:                              #   Parent Loop BB3_288 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %r14
	cmpb	$0, 32(%r14)
	leaq	16(%r14), %rax
	je	.LBB3_296
	jmp	.LBB3_288
.LBB3_297:                              # %FindSplitInGalley.exit
	cmpb	$9, 32(%r14)
	je	.LBB3_299
# BB#298:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.33, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_299:
	movq	8(%r14), %rbp
	addq	$16, %rbp
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB3_301
	.p2align	4, 0x90
.LBB3_300:                              #   in Loop: Header=BB3_301 Depth=1
	addq	$16, %rbp
.LBB3_301:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB3_300
# BB#302:
	cmpb	$16, %al
	je	.LBB3_304
# BB#303:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.34, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_304:                              # %.loopexit572
	cmpq	%rbp, %r13
	je	.LBB3_315
# BB#305:
	movl	48(%r13), %eax
	movl	56(%r13), %ecx
	cmpl	48(%rbp), %eax
	movq	%r13, %rax
	cmovlq	%rbp, %rax
	movl	48(%rax), %r15d
	cmpl	56(%rbp), %ecx
	movq	%r13, %rax
	cmovlq	%rbp, %rax
	movl	56(%rax), %ebx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movl	%r15d, %esi
	movl	%ebx, %edx
	callq	AdjustSize
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	movl	%r15d, %esi
	movl	%ebx, %edx
	callq	AdjustSize
	movq	%r13, xx_res(%rip)
	movq	%rbp, xx_hold(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB3_308
# BB#306:
	movq	16(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rbp), %rcx
	movq	%rax, 24(%rcx)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rax, xx_tmp(%rip)
	movq	%r13, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB3_309
# BB#307:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r13), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r13)
	movq	%r13, 24(%rcx)
	jmp	.LBB3_309
.LBB3_308:                              # %.thread
	movq	$0, xx_tmp(%rip)
	movq	%r13, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB3_309:
	movq	%rbp, zz_hold(%rip)
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB3_313
# BB#310:
	movq	%rax, zz_res(%rip)
	movq	(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	zz_res(%rip), %rax
	movq	xx_res(%rip), %rcx
	movq	%rax, xx_tmp(%rip)
	movq	%rcx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB3_314
# BB#311:
	testq	%rcx, %rcx
	je	.LBB3_314
# BB#312:
	movq	(%rax), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	jmp	.LBB3_314
.LBB3_313:                              # %.thread729
	movq	$0, xx_tmp(%rip)
	movq	%r13, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB3_314:
	movq	xx_hold(%rip), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
.LBB3_315:
	addq	$24, 24(%rsp)           # 8-byte Folded Spill
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB3_316:
	movq	(%r13), %rbx
	cmpq	%r14, %rbx
	je	.LBB3_323
# BB#317:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbp
	cmpb	$0, 32(%rbx)
	je	.LBB3_319
# BB#318:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.38, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_319:
	movq	%rbx, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB3_321
# BB#320:
	movq	(%r14), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB3_321:
	movq	%rbx, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB3_323
# BB#322:
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB3_323:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	Promote, .Lfunc_end3-Promote
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_289
	.quad	.LBB3_292
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_297
	.quad	.LBB3_292
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_293
	.quad	.LBB3_295
	.quad	.LBB3_289
	.quad	.LBB3_295
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_293
	.quad	.LBB3_293
	.quad	.LBB3_293
	.quad	.LBB3_293
	.quad	.LBB3_293
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_293
	.quad	.LBB3_289
	.quad	.LBB3_293
	.quad	.LBB3_289
	.quad	.LBB3_293
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_292
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_289
	.quad	.LBB3_289
.LJTI3_1:
	.quad	.LBB3_234
	.quad	.LBB3_234
	.quad	.LBB3_234
	.quad	.LBB3_234
	.quad	.LBB3_234
	.quad	.LBB3_234
	.quad	.LBB3_234
	.quad	.LBB3_156
	.quad	.LBB3_139
	.quad	.LBB3_139
	.quad	.LBB3_172
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_179
	.quad	.LBB3_229
	.quad	.LBB3_178
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_229
	.quad	.LBB3_178
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_178
	.quad	.LBB3_236
	.quad	.LBB3_236
	.quad	.LBB3_236
	.quad	.LBB3_236
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_229
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_181
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_139
	.quad	.LBB3_178
	.quad	.LBB3_178
	.quad	.LBB3_234

	.text
	.globl	HandleHeader
	.p2align	4, 0x90
	.type	HandleHeader,@function
HandleHeader:                           # @HandleHeader
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 32
.Lcfi71:
	.cfi_offset %rbx, -32
.Lcfi72:
	.cfi_offset %r14, -24
.Lcfi73:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movb	32(%r15), %al
	andb	$-4, %al
	cmpb	$20, %al
	je	.LBB4_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.9, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_2:
	movq	24(%r15), %rax
	cmpq	%r15, %rax
	je	.LBB4_4
# BB#3:
	cmpq	16(%r15), %rax
	je	.LBB4_5
.LBB4_4:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.10, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_5:
	movb	32(%r15), %al
	addb	$-20, %al
	cmpb	$3, %al
	ja	.LBB4_64
# BB#6:
	movzbl	%al, %eax
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_38:
	movq	144(%r14), %rax
	testq	%rax, %rax
	je	.LBB4_39
# BB#40:
	addq	$144, %r14
	cmpq	%rax, (%rax)
	jne	.LBB4_42
# BB#41:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r14), %rax
.LBB4_42:                               # %._crit_edge
	movq	(%rax), %rcx
	.p2align	4, 0x90
.LBB4_43:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB4_43
# BB#44:
	cmpb	$1, %dl
	je	.LBB4_46
# BB#45:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.14, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r14), %rax
.LBB4_46:                               # %.loopexit
	movq	(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_47
# BB#48:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB4_49
.LBB4_8:
	movq	%r14, %rdi
	callq	DisposeHeaders
.LBB4_9:
	cmpq	$0, 144(%r14)
	jne	.LBB4_14
# BB#10:
	movzwl	42(%r14), %eax
	andl	$256, %eax              # imm = 0x100
	shrq	$7, %rax
	movzbl	zz_lengths+17(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_11
# BB#12:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_13
.LBB4_7:
	movq	%r14, %rdi
	callq	DisposeHeaders
	jmp	.LBB4_64
.LBB4_39:
	addq	$32, %r14
	movl	$22, %edi
	movl	$11, %esi
	movl	$.L.str.11, %edx
	movl	$2, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
	jmp	.LBB4_64
.LBB4_11:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_13:
	movzwl	42(%r14), %ecx
	shrl	$7, %ecx
	andb	$2, %cl
	orb	$17, %cl
	movb	%cl, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, 144(%r14)
.LBB4_14:
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB4_15
# BB#16:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB4_17
.LBB4_15:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB4_17:
	movb	$1, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	andb	$-97, 43(%rbx)
	movq	8(%r15), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_19
# BB#18:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB4_19:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB4_22
# BB#20:
	testq	%rax, %rax
	je	.LBB4_22
# BB#21:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB4_22:
	movq	8(%rbx), %rax
	.p2align	4, 0x90
.LBB4_23:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB4_23
# BB#24:
	movzwl	64(%r15), %ecx
	andl	$128, %ecx
	movzwl	44(%rbx), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 44(%rbx)
	movzwl	64(%r15), %ecx
	andl	$256, %ecx              # imm = 0x100
	andl	$-257, %eax             # imm = 0xFEFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbx)
	movzwl	64(%r15), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %eax             # imm = 0xFDFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbx)
	movzwl	64(%r15), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %eax            # imm = 0xE3FF
	orl	%ecx, %eax
	movw	%ax, 44(%rbx)
	movzwl	64(%r15), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %eax             # imm = 0x1FFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbx)
	movzwl	66(%r15), %ecx
	movw	%cx, 46(%rbx)
	andl	$64767, %eax            # imm = 0xFCFF
	movw	%ax, 44(%rbx)
	movq	(%r15), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_26
# BB#25:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB4_26:
	movq	%rax, zz_res(%rip)
	movq	144(%r14), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB4_29
# BB#27:
	testq	%rcx, %rcx
	je	.LBB4_29
# BB#28:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB4_29:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_30
# BB#31:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_32
.LBB4_30:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_32:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	144(%r14), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB4_35
# BB#33:
	testq	%rcx, %rcx
	je	.LBB4_35
# BB#34:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB4_35:
	testq	%rbx, %rbx
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB4_64
# BB#36:
	testq	%rax, %rax
	je	.LBB4_64
# BB#37:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB4_64
.LBB4_47:
	xorl	%ecx, %ecx
.LBB4_49:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_51
# BB#50:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB4_51:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB4_53
# BB#52:
	callq	DisposeObject
.LBB4_53:
	movq	(%r14), %rax
	cmpq	%rax, (%rax)
	jne	.LBB4_55
# BB#54:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r14), %rax
.LBB4_55:
	movq	(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_56
# BB#57:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB4_58
.LBB4_56:
	xorl	%ecx, %ecx
.LBB4_58:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_60
# BB#59:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB4_60:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB4_62
# BB#61:
	callq	DisposeObject
.LBB4_62:
	movq	(%r14), %rdi
	cmpq	%rdi, 8(%rdi)
	jne	.LBB4_64
# BB#63:
	callq	DisposeObject
	movq	$0, (%r14)
.LBB4_64:
	movq	24(%r15), %rax
	movq	8(%rax), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB4_66
# BB#65:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_66:
	movb	32(%r15), %al
	orb	$2, %al
	cmpb	$23, %al
	jne	.LBB4_77
# BB#67:
	movq	8(%r15), %rax
	cmpq	%r15, %rax
	je	.LBB4_69
# BB#68:
	cmpq	(%r15), %rax
	je	.LBB4_70
.LBB4_69:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.16, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%r15), %rax
.LBB4_70:
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_71
# BB#72:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB4_73
.LBB4_71:
	xorl	%ecx, %ecx
.LBB4_73:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_75
# BB#74:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB4_75:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB4_77
# BB#76:
	callq	DisposeObject
.LBB4_77:
	movq	24(%r15), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_78
# BB#79:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB4_80
.LBB4_78:
	xorl	%ecx, %ecx
.LBB4_80:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_82
# BB#81:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB4_82:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB4_84
# BB#83:
	callq	DisposeObject
.LBB4_84:
	movq	%rbx, xx_link(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB4_85
# BB#86:
	movq	%rax, zz_res(%rip)
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rbx), %rcx
	movq	%rax, 24(%rcx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	jmp	.LBB4_87
.LBB4_85:
	xorl	%eax, %eax
.LBB4_87:
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB4_89
# BB#88:
	movq	%rax, zz_res(%rip)
	movq	(%rbx), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rbx
.LBB4_89:
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	je	.LBB4_91
# BB#90:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB4_91:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	DisposeObject           # TAILCALL
.Lfunc_end4:
	.size	HandleHeader, .Lfunc_end4-HandleHeader
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_9
	.quad	.LBB4_38
	.quad	.LBB4_8
	.quad	.LBB4_7

	.text
	.p2align	4, 0x90
	.type	DisposeHeaders,@function
DisposeHeaders:                         # @DisposeHeaders
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 16
.Lcfi75:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	144(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB5_13
# BB#1:
	movb	32(%rcx), %al
	orb	$2, %al
	cmpb	$19, %al
	je	.LBB5_4
# BB#2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.63, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_3:                                # %.preheader
	movq	144(%rbx), %rcx
.LBB5_4:                                # %.preheader
	movq	8(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB5_12
# BB#5:                                 # %.lr.ph
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB5_6
# BB#7:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB5_8
.LBB5_6:
	xorl	%ecx, %ecx
.LBB5_8:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB5_10
# BB#9:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB5_10:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB5_3
# BB#11:
	callq	DisposeObject
	jmp	.LBB5_3
.LBB5_12:                               # %._crit_edge
	movq	$0, 144(%rbx)
.LBB5_13:
	popq	%rbx
	retq
.Lfunc_end5:
	.size	DisposeHeaders, .Lfunc_end5-DisposeHeaders
	.cfi_endproc

	.globl	KillGalley
	.p2align	4, 0x90
	.type	KillGalley,@function
KillGalley:                             # @KillGalley
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi80:
	.cfi_def_cfa_offset 48
.Lcfi81:
	.cfi_offset %rbx, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	cmpb	$8, 32(%r15)
	jne	.LBB6_2
# BB#1:
	movq	24(%r15), %rbp
	cmpq	%r15, %rbp
	jne	.LBB6_3
.LBB6_2:                                # %._crit_edge115
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.39, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	24(%r15), %rbp
	.p2align	4, 0x90
.LBB6_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB6_3
# BB#4:
	cmpb	$120, %al
	je	.LBB6_6
# BB#5:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.40, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_6:                                # %.loopexit
	cmpq	%rbp, 24(%rbp)
	jne	.LBB6_8
# BB#7:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.41, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_8:
	movq	96(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB6_10
# BB#9:
	callq	DisposeObject
	movq	$0, 96(%r15)
	jmp	.LBB6_10
	.p2align	4, 0x90
.LBB6_42:                               # %.preheader63.thread
                                        #   in Loop: Header=BB6_10 Depth=1
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
.LBB6_10:                               # %.preheader67
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_12 Depth 2
                                        #     Child Loop BB6_26 Depth 2
                                        #       Child Loop BB6_27 Depth 3
                                        #     Child Loop BB6_30 Depth 2
                                        #     Child Loop BB6_36 Depth 2
                                        #     Child Loop BB6_46 Depth 2
                                        #     Child Loop BB6_53 Depth 2
                                        #     Child Loop BB6_22 Depth 2
	movq	8(%r15), %rax
	cmpq	%r15, %rax
	je	.LBB6_64
# BB#11:                                # %.preheader66
                                        #   in Loop: Header=BB6_10 Depth=1
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB6_12:                               #   Parent Loop BB6_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %ecx
	cmpb	$7, %cl
	jg	.LBB6_16
# BB#13:                                #   in Loop: Header=BB6_12 Depth=2
	testb	%cl, %cl
	je	.LBB6_12
	jmp	.LBB6_14
	.p2align	4, 0x90
.LBB6_16:                               #   in Loop: Header=BB6_10 Depth=1
	cmpb	$120, %cl
	jg	.LBB6_24
# BB#17:                                #   in Loop: Header=BB6_10 Depth=1
	cmpb	$8, %cl
	je	.LBB6_58
# BB#18:                                #   in Loop: Header=BB6_10 Depth=1
	cmpb	$120, %cl
	jne	.LBB6_14
# BB#19:                                #   in Loop: Header=BB6_10 Depth=1
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB6_21
# BB#20:                                #   in Loop: Header=BB6_10 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.43, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbx), %rax
.LBB6_21:                               #   in Loop: Header=BB6_10 Depth=1
	addq	$16, %rax
	.p2align	4, 0x90
.LBB6_22:                               #   Parent Loop BB6_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB6_22
# BB#23:                                #   in Loop: Header=BB6_10 Depth=1
	xorl	%esi, %esi
	callq	KillGalley
	jmp	.LBB6_10
	.p2align	4, 0x90
.LBB6_24:                               #   in Loop: Header=BB6_10 Depth=1
	cmpb	$121, %cl
	je	.LBB6_43
# BB#25:                                #   in Loop: Header=BB6_10 Depth=1
	cmpb	$122, %cl
	je	.LBB6_26
.LBB6_14:                               #   in Loop: Header=BB6_10 Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_15
# BB#59:                                #   in Loop: Header=BB6_10 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB6_60
	.p2align	4, 0x90
.LBB6_28:                               #   in Loop: Header=BB6_26 Depth=2
	callq	DetachGalley
.LBB6_26:                               # %.preheader64
                                        #   Parent Loop BB6_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_27 Depth 3
	movq	8(%rbx), %rdi
	cmpq	%rbx, %rdi
	je	.LBB6_29
	.p2align	4, 0x90
.LBB6_27:                               #   Parent Loop BB6_10 Depth=1
                                        #     Parent Loop BB6_26 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB6_27
	jmp	.LBB6_28
.LBB6_58:                               #   in Loop: Header=BB6_10 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.44, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB6_10
.LBB6_43:                               #   in Loop: Header=BB6_10 Depth=1
	cmpq	%rbx, 8(%rbx)
	je	.LBB6_45
# BB#44:                                #   in Loop: Header=BB6_10 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.42, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_45:                               #   in Loop: Header=BB6_10 Depth=1
	movq	%rbx, xx_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB6_52
	.p2align	4, 0x90
.LBB6_46:                               # %.lr.ph
                                        #   Parent Loop BB6_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_48
# BB#47:                                #   in Loop: Header=BB6_46 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB6_48:                               #   in Loop: Header=BB6_46 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_50
# BB#49:                                #   in Loop: Header=BB6_46 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB6_50:                               #   in Loop: Header=BB6_46 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB6_46
# BB#51:                                # %..preheader65_crit_edge
                                        #   in Loop: Header=BB6_10 Depth=1
	movl	%ecx, zz_size(%rip)
.LBB6_52:                               # %.preheader65
                                        #   in Loop: Header=BB6_10 Depth=1
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB6_42
	.p2align	4, 0x90
.LBB6_53:                               # %.lr.ph78
                                        #   Parent Loop BB6_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_55
# BB#54:                                #   in Loop: Header=BB6_53 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB6_55:                               #   in Loop: Header=BB6_53 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_57
# BB#56:                                #   in Loop: Header=BB6_53 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB6_57:                               #   in Loop: Header=BB6_53 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB6_53
	jmp	.LBB6_41
.LBB6_29:                               # %._crit_edge80
                                        #   in Loop: Header=BB6_10 Depth=1
	movq	%rbx, xx_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB6_42
	.p2align	4, 0x90
.LBB6_30:                               # %.lr.ph82
                                        #   Parent Loop BB6_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_32
# BB#31:                                #   in Loop: Header=BB6_30 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB6_32:                               #   in Loop: Header=BB6_30 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_34
# BB#33:                                #   in Loop: Header=BB6_30 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB6_34:                               #   in Loop: Header=BB6_30 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rdx
	movq	24(%rdx), %rax
	cmpq	%rdx, %rax
	jne	.LBB6_30
# BB#35:                                # %.preheader63
                                        #   in Loop: Header=BB6_10 Depth=1
	movl	%ecx, zz_size(%rip)
	movq	8(%rdx), %rax
	cmpq	%rdx, %rax
	movq	%rax, %rbx
	je	.LBB6_42
	.p2align	4, 0x90
.LBB6_36:                               # %.lr.ph84
                                        #   Parent Loop BB6_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_38
# BB#37:                                #   in Loop: Header=BB6_36 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB6_38:                               #   in Loop: Header=BB6_36 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_40
# BB#39:                                #   in Loop: Header=BB6_36 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB6_40:                               #   in Loop: Header=BB6_36 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB6_36
.LBB6_41:                               # %._crit_edge85
                                        #   in Loop: Header=BB6_10 Depth=1
	movl	%ecx, zz_size(%rip)
	jmp	.LBB6_42
.LBB6_15:                               #   in Loop: Header=BB6_10 Depth=1
	xorl	%ecx, %ecx
.LBB6_60:                               #   in Loop: Header=BB6_10 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_62
# BB#61:                                #   in Loop: Header=BB6_10 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB6_62:                               #   in Loop: Header=BB6_10 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB6_10
# BB#63:                                #   in Loop: Header=BB6_10 Depth=1
	callq	DisposeObject
	jmp	.LBB6_10
.LBB6_64:                               # %._crit_edge88
	testl	%r14d, %r14d
	je	.LBB6_67
# BB#65:
	cmpq	$0, 104(%r15)
	je	.LBB6_67
# BB#66:
	movq	%r15, %rdi
	callq	CalculateOptimize
.LBB6_67:
	movq	MakeDead.dead_store(%rip), %rax
	testq	%rax, %rax
	jne	.LBB6_72
# BB#68:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_69
# BB#70:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_71
.LBB6_69:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_71:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, MakeDead.dead_store(%rip)
.LBB6_72:
	movb	$119, 32(%rbp)
	movq	24(%rbp), %rcx
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB6_74
# BB#73:
	movq	%rdx, zz_res(%rip)
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rcx
	movq	MakeDead.dead_store(%rip), %rax
.LBB6_74:
	movq	%rcx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB6_77
# BB#75:
	testq	%rcx, %rcx
	je	.LBB6_77
# BB#76:
	movq	(%rax), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB6_77:
	movl	MakeDead.dead_count(%rip), %eax
	cmpl	$150, %eax
	jl	.LBB6_85
# BB#78:
	movq	MakeDead.dead_store(%rip), %rax
	movq	8(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_79
# BB#80:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB6_81
.LBB6_85:
	incl	%eax
	movl	%eax, MakeDead.dead_count(%rip)
	jmp	.LBB6_86
.LBB6_79:
	xorl	%ecx, %ecx
.LBB6_81:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_83
# BB#82:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB6_83:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	je	.LBB6_84
.LBB6_86:                               # %MakeDead.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_84:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	DisposeObject           # TAILCALL
.Lfunc_end6:
	.size	KillGalley, .Lfunc_end6-KillGalley
	.cfi_endproc

	.globl	FreeGalley
	.p2align	4, 0x90
	.type	FreeGalley,@function
FreeGalley:                             # @FreeGalley
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi91:
	.cfi_def_cfa_offset 80
.Lcfi92:
	.cfi_offset %rbx, -56
.Lcfi93:
	.cfi_offset %r12, -48
.Lcfi94:
	.cfi_offset %r13, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	%r8, (%rsp)             # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %r13
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	cmpb	$8, 32(%rbx)
	jne	.LBB7_2
# BB#1:
	testb	$2, 42(%rbx)
	jne	.LBB7_3
.LBB7_2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.45, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_3:
	cmpq	%rbx, 24(%rbx)
	jne	.LBB7_5
# BB#4:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.46, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_5:
	movq	(%r13), %rax
	testq	%rax, %rax
	je	.LBB7_8
# BB#6:
	cmpb	$17, 32(%rax)
	je	.LBB7_8
# BB#7:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.47, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_8:                                # %.preheader78
	movq	8(%rbx), %r14
	cmpq	%rbp, %r14
	je	.LBB7_17
# BB#9:                                 # %.preheader77.lr.ph
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB7_10:                               # %.preheader77
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_11 Depth 2
                                        #     Child Loop BB7_20 Depth 2
                                        #       Child Loop BB7_21 Depth 3
                                        #       Child Loop BB7_30 Depth 3
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB7_11:                               #   Parent Loop BB7_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r12), %r12
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.LBB7_11
# BB#12:                                #   in Loop: Header=BB7_10 Depth=1
	cmpb	$121, %al
	je	.LBB7_50
# BB#13:                                #   in Loop: Header=BB7_10 Depth=1
	cmpb	$122, %al
	jne	.LBB7_16
# BB#14:                                #   in Loop: Header=BB7_10 Depth=1
	movq	80(%r12), %r8
	movq	80(%r8), %rax
	cmpq	InputSym(%rip), %rax
	je	.LBB7_15
# BB#18:                                # %.loopexit.thread
                                        #   in Loop: Header=BB7_10 Depth=1
	movq	8(%r12), %r15
	cmpq	%r12, %r15
	jne	.LBB7_20
	jmp	.LBB7_49
	.p2align	4, 0x90
.LBB7_48:                               #   in Loop: Header=BB7_20 Depth=2
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB7_19:                               # %.loopexit.thread
                                        #   in Loop: Header=BB7_20 Depth=2
	cmpq	%r12, %r15
	je	.LBB7_49
.LBB7_20:                               # %.preheader
                                        #   Parent Loop BB7_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_21 Depth 3
                                        #       Child Loop BB7_30 Depth 3
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB7_21:                               #   Parent Loop BB7_10 Depth=1
                                        #     Parent Loop BB7_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB7_21
# BB#22:                                #   in Loop: Header=BB7_20 Depth=2
	movq	8(%r15), %r15
	cmpb	$8, %al
	je	.LBB7_24
# BB#23:                                #   in Loop: Header=BB7_20 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.49, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_24:                               #   in Loop: Header=BB7_20 Depth=2
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB7_36
# BB#25:                                #   in Loop: Header=BB7_20 Depth=2
	movq	88(%rbx), %rsi
	cmpq	(%rsp), %rsi            # 8-byte Folded Reload
	je	.LBB7_36
# BB#26:                                #   in Loop: Header=BB7_20 Depth=2
	movl	$1, %edx
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%r9d, %r9d
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	SearchGalley
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB7_36
# BB#27:                                #   in Loop: Header=BB7_20 Depth=2
	cmpq	$0, 104(%rbx)
	je	.LBB7_29
# BB#28:                                #   in Loop: Header=BB7_20 Depth=2
	movq	80(%r12), %rsi
	movq	%rbx, %rdi
	callq	GazumpOptimize
.LBB7_29:                               #   in Loop: Header=BB7_20 Depth=2
	movq	%rbx, %rdi
	callq	DetachGalley
	movq	24(%rbx), %rax
	.p2align	4, 0x90
.LBB7_30:                               #   Parent Loop BB7_10 Depth=1
                                        #     Parent Loop BB7_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB7_30
# BB#31:                                #   in Loop: Header=BB7_20 Depth=2
	movq	24(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB7_33
# BB#32:                                #   in Loop: Header=BB7_20 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB7_33:                               #   in Loop: Header=BB7_20 Depth=2
	movq	%rax, zz_res(%rip)
	movq	24(%rbp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB7_19
# BB#34:                                #   in Loop: Header=BB7_20 Depth=2
	testq	%rcx, %rcx
	je	.LBB7_19
# BB#35:                                #   in Loop: Header=BB7_20 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	cmpq	%r12, %r15
	jne	.LBB7_20
	jmp	.LBB7_49
	.p2align	4, 0x90
.LBB7_36:                               #   in Loop: Header=BB7_20 Depth=2
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	(%rsp), %r8             # 8-byte Reload
	callq	FreeGalley
	cmpq	$0, (%r13)
	jne	.LBB7_41
# BB#37:                                #   in Loop: Header=BB7_20 Depth=2
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB7_38
# BB#39:                                #   in Loop: Header=BB7_20 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB7_40
.LBB7_38:                               #   in Loop: Header=BB7_20 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB7_40:                               #   in Loop: Header=BB7_20 Depth=2
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, (%r13)
.LBB7_41:                               #   in Loop: Header=BB7_20 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB7_42
# BB#43:                                #   in Loop: Header=BB7_20 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB7_44
	.p2align	4, 0x90
.LBB7_42:                               #   in Loop: Header=BB7_20 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB7_44:                               #   in Loop: Header=BB7_20 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%r13), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB7_47
# BB#45:                                #   in Loop: Header=BB7_20 Depth=2
	testq	%rcx, %rcx
	je	.LBB7_47
# BB#46:                                #   in Loop: Header=BB7_20 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB7_47:                               #   in Loop: Header=BB7_20 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%rax, %rax
	jne	.LBB7_48
	jmp	.LBB7_19
	.p2align	4, 0x90
.LBB7_50:                               #   in Loop: Header=BB7_10 Depth=1
	orb	$1, 42(%r12)
	jmp	.LBB7_16
	.p2align	4, 0x90
.LBB7_49:                               # %._crit_edge
                                        #   in Loop: Header=BB7_10 Depth=1
	orb	$1, 42(%r12)
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB7_16
.LBB7_15:                               #   in Loop: Header=BB7_10 Depth=1
	addq	$32, %r8
	movl	$22, %edi
	movl	$5, %esi
	movl	$.L.str.48, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	callq	Error
	.p2align	4, 0x90
.LBB7_16:                               # %.backedge79
                                        #   in Loop: Header=BB7_10 Depth=1
	movq	8(%r14), %r14
	cmpq	%rbp, %r14
	jne	.LBB7_10
.LBB7_17:                               # %._crit_edge90
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	FreeGalley, .Lfunc_end7-FreeGalley
	.cfi_endproc

	.globl	SetTarget
	.p2align	4, 0x90
	.type	SetTarget,@function
SetTarget:                              # @SetTarget
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi101:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi102:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi104:
	.cfi_def_cfa_offset 144
.Lcfi105:
	.cfi_offset %rbx, -56
.Lcfi106:
	.cfi_offset %r12, -48
.Lcfi107:
	.cfi_offset %r13, -40
.Lcfi108:
	.cfi_offset %r14, -32
.Lcfi109:
	.cfi_offset %r15, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpb	$8, 32(%r14)
	je	.LBB8_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.50, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB8_2:
	movq	8(%r14), %rbx
	.p2align	4, 0x90
.LBB8_3:                                # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB8_3
# BB#4:
	cmpb	$2, %al
	je	.LBB8_6
# BB#5:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.51, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB8_6:                                # %.loopexit207
	movq	80(%rbx), %rax
	movzwl	41(%rax), %eax
	testb	$64, %al
	jne	.LBB8_8
# BB#7:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.52, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB8_8:                                # %.preheader206
	movq	8(%rbx), %rax
	xorl	%r12d, %r12d
	cmpq	%rbx, %rax
	jne	.LBB8_10
	jmp	.LBB8_20
	.p2align	4, 0x90
.LBB8_14:                               # %.backedge
                                        #   in Loop: Header=BB8_10 Depth=1
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	je	.LBB8_20
.LBB8_10:                               # %.preheader204
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_11 Depth 2
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB8_11:                               #   Parent Loop BB8_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB8_11
# BB#12:                                #   in Loop: Header=BB8_10 Depth=1
	cmpb	$10, %cl
	jne	.LBB8_14
# BB#13:                                #   in Loop: Header=BB8_10 Depth=1
	movq	80(%rbp), %rcx
	movzwl	41(%rcx), %ecx
	testb	$32, %cl
	je	.LBB8_14
# BB#15:
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB8_17
# BB#16:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.53, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbp), %rax
.LBB8_17:
	addq	$16, %rax
	.p2align	4, 0x90
.LBB8_18:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %r15
	leaq	16(%r15), %rax
	cmpb	$0, 32(%r15)
	je	.LBB8_18
# BB#19:
	testq	%r15, %r15
	movq	%r15, %r12
	jne	.LBB8_28
.LBB8_20:                               # %.thread
	movq	80(%rbx), %rax
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	jne	.LBB8_23
	jmp	.LBB8_27
	.p2align	4, 0x90
.LBB8_21:                               #   in Loop: Header=BB8_23 Depth=1
	movq	8(%rcx), %rcx
	cmpq	%rax, %rcx
	je	.LBB8_27
.LBB8_23:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_24 Depth 2
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB8_24:                               #   Parent Loop BB8_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdx), %rdx
	cmpb	$0, 32(%rdx)
	je	.LBB8_24
# BB#25:                                #   in Loop: Header=BB8_23 Depth=1
	movzwl	41(%rdx), %esi
	testb	$32, %sil
	je	.LBB8_21
# BB#26:                                # %.loopexit203
	movq	56(%rdx), %r12
	testq	%r12, %r12
	movq	%r12, %r15
	jne	.LBB8_28
.LBB8_27:                               # %.loopexit203.thread
	movq	no_fpos(%rip), %r8
	xorl	%r15d, %r15d
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.54, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB8_28:                               # %.thread201
	movb	32(%r12), %al
	andb	$-2, %al
	xorl	%ecx, %ecx
	cmpb	$6, %al
	je	.LBB8_30
# BB#29:
	leaq	32(%rbx), %rsi
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 64(%rsp)
	movq	$0, 24(%rsp)
	movq	$0, 32(%rsp)
	movq	$0, 40(%rsp)
	movq	%r15, %rdi
	callq	CopyObject
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	GetEnv
	subq	$8, %rsp
.Lcfi111:
	.cfi_adjust_cfa_offset 8
	leaq	32(%rsp), %rbp
	leaq	40(%rsp), %r10
	leaq	72(%rsp), %rcx
	leaq	56(%rsp), %r8
	leaq	48(%rsp), %r9
	movl	$InitialStyle, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	pushq	$1
.Lcfi112:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi113:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi114:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi115:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi116:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi117:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r15
	movl	$1, %ecx
.LBB8_30:
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	leaq	32(%r15), %rbp
	movb	32(%r15), %al
	andb	$-2, %al
	cmpb	$6, %al
	je	.LBB8_32
# BB#31:
	movq	80(%rbx), %rdi
	callq	SymName
	movq	%rax, %rbx
	movl	$22, %edi
	movl	$6, %esi
	movl	$.L.str.55, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	%rbx, %r9
	callq	Error
.LBB8_32:
	movq	8(%r15), %r12
	.p2align	4, 0x90
.LBB8_33:                               # =>This Inner Loop Header: Depth=1
	addq	$16, %r12
	movq	(%r12), %r12
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.LBB8_33
# BB#34:
	cmpb	$2, %al
	je	.LBB8_36
# BB#35:
	leaq	32(%r12), %r8
	movl	$22, %edi
	movl	$7, %esi
	movl	$.L.str.56, %edx
	movl	$1, %ecx
	movl	$.L.str.57, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB8_36:                               # %.loopexit
	movq	80(%r12), %rax
	movq	%rax, 88(%r14)
	movq	8(%r15), %rax
	movq	8(%rax), %rbx
	.p2align	4, 0x90
.LBB8_37:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB8_37
# BB#38:
	leaq	32(%rbx), %r13
	addb	$-11, %al
	cmpb	$2, %al
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	jae	.LBB8_39
# BB#40:
	addq	$64, %rbx
	movl	$.L.str.60, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB8_41
# BB#42:
	movl	$.L.str.59, %esi
	movq	%rbx, %rdi
	callq	strcmp
	movb	$-127, %bpl
	testl	%eax, %eax
	je	.LBB8_46
# BB#43:
	movl	$.L.str.61, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB8_44
# BB#45:
	movq	80(%r12), %rdi
	callq	SymName
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	80(%r12), %rdi
	callq	SymName
	movq	%rax, %r10
	subq	$8, %rsp
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	movl	$22, %edi
	movl	$9, %esi
	movl	$.L.str.62, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	%r13, %r8
	movq	16(%rsp), %r9           # 8-byte Reload
	pushq	$.L.str.59
.Lcfi119:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.57
.Lcfi120:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi121:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi122:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.57
.Lcfi123:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$48, %rsp
.Lcfi124:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB8_46
.LBB8_39:
	movq	80(%r12), %rdi
	callq	SymName
	movq	%rax, %rbp
	movq	80(%r12), %rdi
	callq	SymName
	movq	%rax, %rbx
	movl	$22, %edi
	movl	$8, %esi
	movl	$.L.str.58, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	%r13, %r8
	movq	%rbp, %r9
	pushq	$.L.str.59
.Lcfi125:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.57
.Lcfi126:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi127:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.57
.Lcfi128:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$32, %rsp
.Lcfi129:
	.cfi_adjust_cfa_offset -32
	movb	$-127, %bpl
	jmp	.LBB8_46
.LBB8_41:
	movb	$-123, %bpl
	jmp	.LBB8_46
.LBB8_44:
	movb	$-126, %bpl
.LBB8_46:
	movl	4(%rsp), %edx           # 4-byte Reload
	movb	%bpl, 40(%r14)
	movq	80(%r14), %rax
	cmpb	$0, 41(%rax)
	js	.LBB8_47
# BB#48:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpb	$7, (%rax)
	sete	%al
	jmp	.LBB8_49
.LBB8_47:
	movb	$1, %al
.LBB8_49:
	movzbl	%al, %eax
	movzwl	42(%r14), %ecx
	shll	$12, %eax
	andl	$61439, %ecx            # imm = 0xEFFF
	orl	%eax, %ecx
	movw	%cx, 42(%r14)
	testl	%edx, %edx
	je	.LBB8_51
# BB#50:
	movq	%r15, %rdi
	callq	DisposeObject
.LBB8_51:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	SetTarget, .Lfunc_end8-SetTarget
	.cfi_endproc

	.globl	CheckComponentOrder
	.p2align	4, 0x90
	.type	CheckComponentOrder,@function
CheckComponentOrder:                    # @CheckComponentOrder
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdx
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB9_1:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rcx
	cmpb	$0, 32(%rcx)
	je	.LBB9_1
# BB#2:
	movq	24(%rsi), %rsi
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB9_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB9_3
# BB#4:
	cmpq	%rax, %rcx
	je	.LBB9_5
	.p2align	4, 0x90
.LBB9_10:                               # %.preheader34
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_12 Depth 2
                                        #     Child Loop BB9_14 Depth 2
	movq	24(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB9_11
	.p2align	4, 0x90
.LBB9_12:                               #   Parent Loop BB9_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdx
	cmpb	$0, 32(%rdx)
	je	.LBB9_12
# BB#13:                                #   in Loop: Header=BB9_10 Depth=1
	movq	24(%rdx), %rcx
	.p2align	4, 0x90
.LBB9_14:                               #   Parent Loop BB9_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rcx
	cmpb	$0, 32(%rcx)
	je	.LBB9_14
# BB#15:                                #   in Loop: Header=BB9_10 Depth=1
	cmpq	%rax, %rcx
	jne	.LBB9_10
# BB#16:
	movl	$156, %eax
	retq
.LBB9_5:                                # %.preheader
	movl	$155, %eax
	cmpq	%rcx, %rsi
	jne	.LBB9_8
	jmp	.LBB9_17
	.p2align	4, 0x90
.LBB9_6:                                #   in Loop: Header=BB9_8 Depth=1
	movq	(%rsi), %rsi
	cmpq	%rcx, %rsi
	je	.LBB9_17
.LBB9_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rdx, %rsi
	jne	.LBB9_6
# BB#9:
	movl	$157, %eax
	retq
.LBB9_17:                               # %.loopexit
	retq
.LBB9_11:
	movl	$154, %eax
	retq
.Lfunc_end9:
	.size	CheckComponentOrder, .Lfunc_end9-CheckComponentOrder
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"assert failed in %s"
	.size	.L.str.1, 20

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"FlushInners: Up(hd)!"
	.size	.L.str.2, 21

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"FlushInners: FOLLOWS!"
	.size	.L.str.4, 22

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"assert failed in %s %s"
	.size	.L.str.5, 23

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"FlushInners:"
	.size	.L.str.6, 13

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"ExpandRecursives: recs == nilobj!"
	.size	.L.str.7, 34

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"ExpandRecursives: index!"
	.size	.L.str.8, 25

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"HandleHeader: type(header)!"
	.size	.L.str.9, 28

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"HandleHeader: header parents!"
	.size	.L.str.10, 30

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"no header for %s to remove"
	.size	.L.str.11, 27

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"@EndHeaderComponent"
	.size	.L.str.12, 20

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Promote/END_HEADER!"
	.size	.L.str.13, 20

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"HandleHeader: END_HEADER/gap!"
	.size	.L.str.14, 30

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"HandleHeader: type(gaplink)!"
	.size	.L.str.15, 29

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"HH!"
	.size	.L.str.16, 4

	.type	Promote.first,@object   # @Promote.first
	.local	Promote.first
	.comm	Promote.first,1,4
	.type	Promote.page_label,@object # @Promote.page_label
	.local	Promote.page_label
	.comm	Promote.page_label,8,8
	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Promote: hd!"
	.size	.L.str.17, 13

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Promote: stop_link!"
	.size	.L.str.18, 20

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Promote: stop_link == Down(hd)!"
	.size	.L.str.19, 32

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Promote: missing GAP_OBJ!"
	.size	.L.str.20, 26

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"w"
	.size	.L.str.21, 2

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"galley %s must have a single column mark"
	.size	.L.str.22, 41

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"Promote: nojoin case, can't find VCAT"
	.size	.L.str.23, 38

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"FlushRootGalley: UNATTACHED!"
	.size	.L.str.24, 29

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"FlushRootGalley: unattached HEAD!"
	.size	.L.str.25, 34

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"galley %s deleted (never attached)"
	.size	.L.str.26, 35

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Promote: type(z) != PAGE_LABEL!"
	.size	.L.str.27, 32

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Promote: PAGE_LABEL Down(z) == z!"
	.size	.L.str.28, 34

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"%s symbol ignored (out of place)"
	.size	.L.str.29, 33

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"?"
	.size	.L.str.30, 2

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Promote:"
	.size	.L.str.31, 9

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Promote: tmp1 not COL_THR!"
	.size	.L.str.32, 27

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Promote: FindSplitInGalley!"
	.size	.L.str.33, 28

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"Promote: tmp2 not COL_THR!"
	.size	.L.str.34, 27

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"Promote: header under SPLIT!"
	.size	.L.str.35, 29

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"Promote: Down(hd) == stop_link!"
	.size	.L.str.36, 32

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Promote: dims!"
	.size	.L.str.37, 15

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"TransferLinks: start_link!"
	.size	.L.str.38, 27

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"KillGalley: precondition!"
	.size	.L.str.39, 26

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"KillGalley: UNATTACHED precondition!"
	.size	.L.str.40, 37

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"KillGalley: prnt!"
	.size	.L.str.41, 18

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"KillGalley: RECEPTIVE!"
	.size	.L.str.42, 23

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"KillGalley: UNATTACHED!"
	.size	.L.str.43, 24

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"KillGalley: head"
	.size	.L.str.44, 17

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"FreeGalley: pre!"
	.size	.L.str.45, 17

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"FreeGalley: Up(hd)!"
	.size	.L.str.46, 20

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"FreeGalley: ACAT!"
	.size	.L.str.47, 18

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"forcing galley after input point"
	.size	.L.str.48, 33

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"FreeGalley/RECEIVING: type(z) != HEAD!"
	.size	.L.str.49, 39

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"SetTarget: type(hd) != HEAD!"
	.size	.L.str.50, 29

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"SetTarget: type(x) != CLOSURE!"
	.size	.L.str.51, 31

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"SetTarget: x has no target!"
	.size	.L.str.52, 28

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"SetTarget: Down(PAR)!"
	.size	.L.str.53, 22

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"SetTarget:  cr == nilobj!"
	.size	.L.str.54, 26

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"target of %s is not a cross reference"
	.size	.L.str.55, 38

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"left parameter of %s is not a symbol"
	.size	.L.str.56, 37

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"&&"
	.size	.L.str.57, 3

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"replacing %s%s? by %s%s%s"
	.size	.L.str.58, 26

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"following"
	.size	.L.str.59, 10

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"preceding"
	.size	.L.str.60, 10

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"foll_or_prec"
	.size	.L.str.61, 13

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"replacing %s%s%s by %s%s%s"
	.size	.L.str.62, 27

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"DisposeHeaders: type(headers(hd))!"
	.size	.L.str.63, 35

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"FindSplit: missing galley component"
	.size	.L.str.64, 36

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"FindSplitInGalley failed"
	.size	.L.str.65, 25

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"FindSplitInGalley:"
	.size	.L.str.66, 19

	.type	MakeDead.dead_count,@object # @MakeDead.dead_count
	.local	MakeDead.dead_count
	.comm	MakeDead.dead_count,4,4
	.type	MakeDead.dead_store,@object # @MakeDead.dead_store
	.local	MakeDead.dead_store
	.comm	MakeDead.dead_store,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
