	.text
	.file	"createIndexEntry.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	4286578687              # float -3.40282347E+38
	.long	4286578687              # float -3.40282347E+38
	.long	4286578687              # float -3.40282347E+38
	.long	4286578687              # float -3.40282347E+38
.LCPI0_1:
	.long	2139095039              # float 3.40282347E+38
	.long	2139095039              # float 3.40282347E+38
	.long	2139095039              # float 3.40282347E+38
	.long	2139095039              # float 3.40282347E+38
	.text
	.globl	createIndexEntry
	.p2align	4, 0x90
	.type	createIndexEntry,@function
createIndexEntry:                       # @createIndexEntry
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
# BB#2:
	movq	$0, (%rbx)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [-3.402823e+38,-3.402823e+38,-3.402823e+38,-3.402823e+38]
	movups	%xmm0, 8(%rbx)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [3.402823e+38,3.402823e+38,3.402823e+38,3.402823e+38]
	movups	%xmm0, 24(%rbx)
	movq	$0, 40(%rbx)
	jmp	.LBB0_3
.LBB0_1:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$createIndexEntry.name, %edi
	movl	$1, %esi
	callq	errorMessage
.LBB0_3:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	createIndexEntry, .Lfunc_end0-createIndexEntry
	.cfi_endproc

	.type	createIndexEntry.name,@object # @createIndexEntry.name
	.data
	.p2align	4
createIndexEntry.name:
	.asciz	"createIndexEntry"
	.size	createIndexEntry.name, 17

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"allocation failure"
	.size	.L.str, 19


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
