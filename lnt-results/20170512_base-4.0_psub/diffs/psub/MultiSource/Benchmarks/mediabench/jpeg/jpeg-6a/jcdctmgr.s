	.text
	.file	"jcdctmgr.bc"
	.globl	jinit_forward_dct
	.p2align	4, 0x90
	.type	jinit_forward_dct,@function
jinit_forward_dct:                      # @jinit_forward_dct
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	8(%r14), %rax
	movl	$1, %esi
	movl	$96, %edx
	callq	*(%rax)
	movq	%rax, %rbx
	movq	%rbx, 480(%r14)
	movq	$start_pass_fdctmgr, (%rbx)
	movl	268(%r14), %eax
	cmpl	$2, %eax
	je	.LBB0_5
# BB#1:
	cmpl	$1, %eax
	je	.LBB0_4
# BB#2:
	testl	%eax, %eax
	jne	.LBB0_6
# BB#3:
	movq	$forward_DCT, 8(%rbx)
	movq	$jpeg_fdct_islow, 16(%rbx)
	jmp	.LBB0_7
.LBB0_5:
	movq	$forward_DCT_float, 8(%rbx)
	movq	$jpeg_fdct_float, 56(%rbx)
	jmp	.LBB0_7
.LBB0_4:
	movq	$forward_DCT, 8(%rbx)
	movq	$jpeg_fdct_ifast, 16(%rbx)
	jmp	.LBB0_7
.LBB0_6:
	movq	(%r14), %rax
	movl	$47, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB0_7:                                # %.preheader
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 40(%rbx)
	movups	%xmm0, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	jinit_forward_dct, .Lfunc_end0-jinit_forward_dct
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.zero	8
	.quad	4608925491301736943     # double 1.3870398450000001
.LCPI1_1:
	.quad	4608563055654957002     # double 1.3065629649999999
	.quad	4607974492095648170     # double 1.1758756020000001
.LCPI1_2:
	.quad	4620693217682128896     # double 8
	.quad	4620693217682128896     # double 8
.LCPI1_3:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI1_4:
	.zero	8
	.quad	4605252130585427771     # double 0.785694958
.LCPI1_5:
	.quad	4603049880653865147     # double 0.54119609999999996
	.quad	4598641781279730525     # double 0.275899379
.LCPI1_6:
	.quad	1024                    # 0x400
	.quad	1024                    # 0x400
	.text
	.p2align	4, 0x90
	.type	start_pass_fdctmgr,@function
start_pass_fdctmgr:                     # @start_pass_fdctmgr
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 64
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	cmpl	$0, 68(%r15)
	jle	.LBB1_21
# BB#1:                                 # %.lr.ph
	movq	480(%r15), %r14
	movq	80(%r15), %r12
	xorl	%r13d, %r13d
	movapd	.LCPI1_0(%rip), %xmm5   # xmm5 = <u,1.3870398450000001>
	movapd	.LCPI1_1(%rip), %xmm6   # xmm6 = [1.306563e+00,1.175876e+00]
	movapd	.LCPI1_2(%rip), %xmm7   # xmm7 = [8.000000e+00,8.000000e+00]
	movapd	.LCPI1_3(%rip), %xmm8   # xmm8 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI1_4(%rip), %xmm9   # xmm9 = <u,0.785694958>
	movapd	.LCPI1_5(%rip), %xmm10  # xmm10 = [5.411961e-01,2.758994e-01]
	pxor	%xmm11, %xmm11
	movdqa	.LCPI1_6(%rip), %xmm12  # xmm12 = [1024,1024]
	.p2align	4, 0x90
.LBB1_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_14 Depth 2
                                        #     Child Loop BB1_18 Depth 2
	movslq	16(%r12), %rbp
	cmpq	$3, %rbp
	ja	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	88(%r15,%rbp,8), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_5
.LBB1_4:                                # %._crit_edge116
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	(%r15), %rax
	movl	$51, 40(%rax)
	movl	%ebp, 44(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	movdqa	.LCPI1_6(%rip), %xmm12  # xmm12 = [1024,1024]
	pxor	%xmm11, %xmm11
	movapd	.LCPI1_5(%rip), %xmm10  # xmm10 = [5.411961e-01,2.758994e-01]
	movapd	.LCPI1_4(%rip), %xmm9   # xmm9 = <u,0.785694958>
	movapd	.LCPI1_3(%rip), %xmm8   # xmm8 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI1_2(%rip), %xmm7   # xmm7 = [8.000000e+00,8.000000e+00]
	movapd	.LCPI1_1(%rip), %xmm6   # xmm6 = [1.306563e+00,1.175876e+00]
	movapd	.LCPI1_0(%rip), %xmm5   # xmm5 = <u,1.3870398450000001>
	movq	88(%r15,%rbp,8), %rbx
.LBB1_5:                                #   in Loop: Header=BB1_2 Depth=1
	movl	268(%r15), %eax
	cmpl	$2, %eax
	je	.LBB1_15
# BB#6:                                 #   in Loop: Header=BB1_2 Depth=1
	cmpl	$1, %eax
	je	.LBB1_11
# BB#7:                                 #   in Loop: Header=BB1_2 Depth=1
	testl	%eax, %eax
	jne	.LBB1_19
# BB#8:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	24(%r14,%rbp,8), %rax
	testq	%rax, %rax
	jne	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$256, %edx              # imm = 0x100
	movq	%r15, %rdi
	callq	*(%rax)
	movdqa	.LCPI1_6(%rip), %xmm12  # xmm12 = [1024,1024]
	pxor	%xmm11, %xmm11
	movapd	.LCPI1_5(%rip), %xmm10  # xmm10 = [5.411961e-01,2.758994e-01]
	movapd	.LCPI1_4(%rip), %xmm9   # xmm9 = <u,0.785694958>
	movapd	.LCPI1_3(%rip), %xmm8   # xmm8 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI1_2(%rip), %xmm7   # xmm7 = [8.000000e+00,8.000000e+00]
	movapd	.LCPI1_1(%rip), %xmm6   # xmm6 = [1.306563e+00,1.175876e+00]
	movapd	.LCPI1_0(%rip), %xmm5   # xmm5 = <u,1.3870398450000001>
	movq	%rax, 24(%r14,%rbp,8)
.LBB1_10:                               # %vector.body
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	(%rbx), %xmm0           # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, (%rax)
	movq	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 16(%rax)
	movq	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 32(%rax)
	movq	24(%rbx), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 48(%rax)
	movq	32(%rbx), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 64(%rax)
	movq	40(%rbx), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 80(%rax)
	movq	48(%rbx), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 96(%rax)
	movq	56(%rbx), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 112(%rax)
	movq	64(%rbx), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 128(%rax)
	movq	72(%rbx), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 144(%rax)
	movq	80(%rbx), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 160(%rax)
	movq	88(%rbx), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 176(%rax)
	movq	96(%rbx), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 192(%rax)
	movq	104(%rbx), %xmm0        # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 208(%rax)
	movq	112(%rbx), %xmm0        # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 224(%rax)
	movq	120(%rbx), %xmm0        # xmm0 = mem[0],zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	pslld	$3, %xmm0
	movdqu	%xmm0, 240(%rax)
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_11:                               #   in Loop: Header=BB1_2 Depth=1
	movq	24(%r14,%rbp,8), %rax
	testq	%rax, %rax
	jne	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_2 Depth=1
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$256, %edx              # imm = 0x100
	movq	%r15, %rdi
	callq	*(%rax)
	movdqa	.LCPI1_6(%rip), %xmm12  # xmm12 = [1024,1024]
	pxor	%xmm11, %xmm11
	movapd	.LCPI1_5(%rip), %xmm10  # xmm10 = [5.411961e-01,2.758994e-01]
	movapd	.LCPI1_4(%rip), %xmm9   # xmm9 = <u,0.785694958>
	movapd	.LCPI1_3(%rip), %xmm8   # xmm8 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI1_2(%rip), %xmm7   # xmm7 = [8.000000e+00,8.000000e+00]
	movapd	.LCPI1_1(%rip), %xmm6   # xmm6 = [1.306563e+00,1.175876e+00]
	movapd	.LCPI1_0(%rip), %xmm5   # xmm5 = <u,1.3870398450000001>
	movq	%rax, 24(%r14,%rbp,8)
.LBB1_13:                               # %vector.ph126
                                        #   in Loop: Header=BB1_2 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_14:                               # %vector.body122
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	(%rbx,%rcx), %xmm0      # xmm0 = mem[0],zero,zero,zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	punpckldq	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1]
	movd	start_pass_fdctmgr.aanscales(%rcx), %xmm1 # xmm1 = mem[0],zero,zero,zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	psrad	$16, %xmm1
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movdqa	%xmm1, %xmm2
	pmuludq	%xmm0, %xmm2
	psrlq	$32, %xmm1
	pmuludq	%xmm0, %xmm1
	psllq	$32, %xmm1
	paddq	%xmm12, %xmm2
	paddq	%xmm1, %xmm2
	psrlq	$11, %xmm2
	pshufd	$232, %xmm2, %xmm0      # xmm0 = xmm2[0,2,2,3]
	movq	%xmm0, (%rax,%rcx,2)
	movd	4(%rbx,%rcx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	punpcklwd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3]
	punpckldq	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1]
	movd	start_pass_fdctmgr.aanscales+4(%rcx), %xmm1 # xmm1 = mem[0],zero,zero,zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	psrad	$16, %xmm1
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movdqa	%xmm1, %xmm2
	pmuludq	%xmm0, %xmm2
	psrlq	$32, %xmm1
	pmuludq	%xmm0, %xmm1
	psllq	$32, %xmm1
	paddq	%xmm12, %xmm2
	paddq	%xmm1, %xmm2
	psrlq	$11, %xmm2
	pshufd	$232, %xmm2, %xmm0      # xmm0 = xmm2[0,2,2,3]
	movq	%xmm0, 8(%rax,%rcx,2)
	addq	$8, %rcx
	cmpq	$128, %rcx
	jne	.LBB1_14
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_15:                               #   in Loop: Header=BB1_2 Depth=1
	movq	64(%r14,%rbp,8), %rax
	testq	%rax, %rax
	jne	.LBB1_17
# BB#16:                                #   in Loop: Header=BB1_2 Depth=1
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$256, %edx              # imm = 0x100
	movq	%r15, %rdi
	callq	*(%rax)
	movdqa	.LCPI1_6(%rip), %xmm12  # xmm12 = [1024,1024]
	pxor	%xmm11, %xmm11
	movapd	.LCPI1_5(%rip), %xmm10  # xmm10 = [5.411961e-01,2.758994e-01]
	movapd	.LCPI1_4(%rip), %xmm9   # xmm9 = <u,0.785694958>
	movapd	.LCPI1_3(%rip), %xmm8   # xmm8 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI1_2(%rip), %xmm7   # xmm7 = [8.000000e+00,8.000000e+00]
	movapd	.LCPI1_1(%rip), %xmm6   # xmm6 = [1.306563e+00,1.175876e+00]
	movapd	.LCPI1_0(%rip), %xmm5   # xmm5 = <u,1.3870398450000001>
	movq	%rax, 64(%r14,%rbp,8)
.LBB1_17:                               #   in Loop: Header=BB1_2 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_18:                               # %.preheader
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	start_pass_fdctmgr.aanscalefactor(%rcx), %xmm0 # xmm0 = mem[0],zero
	movzwl	(%rbx,%rcx,2), %edx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	movzwl	2(%rbx,%rcx,2), %edx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edx, %xmm2
	mulsd	%xmm0, %xmm2
	movzwl	4(%rbx,%rcx,2), %edx
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edx, %xmm3
	mulsd	%xmm0, %xmm3
	movzwl	6(%rbx,%rcx,2), %edx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%edx, %xmm4
	mulsd	%xmm0, %xmm4
	unpcklpd	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0]
	unpcklpd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	movapd	%xmm5, %xmm2
	movsd	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1]
	mulpd	%xmm1, %xmm2
	mulpd	%xmm6, %xmm3
	mulpd	%xmm7, %xmm3
	mulpd	%xmm7, %xmm2
	movapd	%xmm8, %xmm1
	divpd	%xmm2, %xmm1
	movapd	%xmm8, %xmm2
	divpd	%xmm3, %xmm2
	cvtpd2ps	%xmm2, %xmm2
	cvtpd2ps	%xmm1, %xmm1
	unpcklpd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	movupd	%xmm1, (%rax,%rcx,4)
	movzwl	8(%rbx,%rcx,2), %edx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	movzwl	10(%rbx,%rcx,2), %edx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edx, %xmm2
	mulsd	%xmm0, %xmm2
	movzwl	12(%rbx,%rcx,2), %edx
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edx, %xmm3
	mulsd	%xmm0, %xmm3
	movzwl	14(%rbx,%rcx,2), %edx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%edx, %xmm4
	mulsd	%xmm0, %xmm4
	unpcklpd	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0]
	unpcklpd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	movapd	%xmm9, %xmm2
	movsd	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1]
	mulpd	%xmm1, %xmm2
	mulpd	%xmm10, %xmm3
	mulpd	%xmm7, %xmm3
	mulpd	%xmm7, %xmm2
	movapd	%xmm8, %xmm0
	divpd	%xmm2, %xmm0
	movapd	%xmm8, %xmm1
	divpd	%xmm3, %xmm1
	cvtpd2ps	%xmm1, %xmm1
	cvtpd2ps	%xmm0, %xmm0
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movupd	%xmm0, 16(%rax,%rcx,4)
	addq	$8, %rcx
	cmpq	$64, %rcx
	jne	.LBB1_18
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_19:                               #   in Loop: Header=BB1_2 Depth=1
	movq	(%r15), %rax
	movl	$47, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	movdqa	.LCPI1_6(%rip), %xmm12  # xmm12 = [1024,1024]
	pxor	%xmm11, %xmm11
	movapd	.LCPI1_5(%rip), %xmm10  # xmm10 = [5.411961e-01,2.758994e-01]
	movapd	.LCPI1_4(%rip), %xmm9   # xmm9 = <u,0.785694958>
	movapd	.LCPI1_3(%rip), %xmm8   # xmm8 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI1_2(%rip), %xmm7   # xmm7 = [8.000000e+00,8.000000e+00]
	movapd	.LCPI1_1(%rip), %xmm6   # xmm6 = [1.306563e+00,1.175876e+00]
	movapd	.LCPI1_0(%rip), %xmm5   # xmm5 = <u,1.3870398450000001>
.LBB1_20:                               # %.loopexit
                                        #   in Loop: Header=BB1_2 Depth=1
	incl	%r13d
	addq	$96, %r12
	cmpl	68(%r15), %r13d
	jl	.LBB1_2
.LBB1_21:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	start_pass_fdctmgr, .Lfunc_end1-start_pass_fdctmgr
	.cfi_endproc

	.p2align	4, 0x90
	.type	forward_DCT,@function
forward_DCT:                            # @forward_DCT
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$408, %rsp              # imm = 0x198
.Lcfi24:
	.cfi_def_cfa_offset 464
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
	movq	%rcx, %r14
	movl	464(%rsp), %eax
	testl	%eax, %eax
	je	.LBB2_23
# BB#1:                                 # %.lr.ph
	movq	480(%rdi), %rcx
	movq	16(%rcx), %rdi
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movslq	16(%rsi), %rsi
	movq	24(%rcx,%rsi,8), %r13
	movl	%r8d, %ecx
	leaq	(%rdx,%rcx,8), %rbp
	movl	%eax, %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
                                        #     Child Loop BB2_5 Depth 2
	movl	%ebx, %ebx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_3:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp,%rax), %rcx
	movzbl	(%rcx,%rbx), %edx
	addl	$-128, %edx
	movl	%edx, 144(%rsp,%rax,4)
	movzbl	1(%rcx,%rbx), %edx
	addl	$-128, %edx
	movl	%edx, 148(%rsp,%rax,4)
	movzbl	2(%rcx,%rbx), %edx
	addl	$-128, %edx
	movl	%edx, 152(%rsp,%rax,4)
	movzbl	3(%rcx,%rbx), %edx
	addl	$-128, %edx
	movl	%edx, 156(%rsp,%rax,4)
	movzbl	4(%rcx,%rbx), %edx
	addl	$-128, %edx
	movl	%edx, 160(%rsp,%rax,4)
	movzbl	5(%rcx,%rbx), %edx
	addl	$-128, %edx
	movl	%edx, 164(%rsp,%rax,4)
	movzbl	6(%rcx,%rbx), %edx
	addl	$-128, %edx
	movl	%edx, 168(%rsp,%rax,4)
	movzbl	7(%rcx,%rbx), %ecx
	addl	$-128, %ecx
	movl	%ecx, 172(%rsp,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB2_3
# BB#4:                                 # %min.iters.checked
                                        #   in Loop: Header=BB2_2 Depth=1
	leaq	144(%rsp), %rdi
	callq	*8(%rsp)                # 8-byte Folded Reload
	movq	%r14, %rcx
	xorl	%esi, %esi
	pcmpeqd	%xmm10, %xmm10
	.p2align	4, 0x90
.LBB2_5:                                # %vector.body
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%r13,%rsi,4), %xmm2
	movdqa	144(%rsp,%rsi,4), %xmm6
	movdqa	%xmm2, %xmm5
	psrad	$1, %xmm5
	movdqa	%xmm6, %xmm7
	paddd	%xmm5, %xmm7
	pxor	%xmm8, %xmm8
	pcmpgtd	%xmm6, %xmm8
	movdqa	%xmm2, %xmm0
	pcmpgtd	%xmm7, %xmm0
	movdqa	%xmm0, %xmm3
	pxor	%xmm10, %xmm3
	movdqa	%xmm8, %xmm1
	pandn	%xmm3, %xmm1
	movdqa	%xmm1, 128(%rsp)
	testb	$1, 128(%rsp)
                                        # implicit-def: %XMM9
	je	.LBB2_7
# BB#6:                                 # %pred.sdiv.if
                                        #   in Loop: Header=BB2_5 Depth=2
	movd	%xmm7, %eax
	movd	%xmm2, %edi
	cltd
	idivl	%edi
	movd	%eax, %xmm9
.LBB2_7:                                # %pred.sdiv.continue
                                        #   in Loop: Header=BB2_5 Depth=2
	movdqa	%xmm1, 112(%rsp)
	testb	$1, 116(%rsp)
	je	.LBB2_9
# BB#8:                                 # %pred.sdiv.if96
                                        #   in Loop: Header=BB2_5 Depth=2
	pshufd	$229, %xmm7, %xmm3      # xmm3 = xmm7[1,1,2,3]
	movd	%xmm3, %eax
	pshufd	$229, %xmm2, %xmm3      # xmm3 = xmm2[1,1,2,3]
	movd	%xmm3, %edi
	cltd
	idivl	%edi
	movd	%eax, %xmm3
	shufps	$0, %xmm9, %xmm3        # xmm3 = xmm3[0,0],xmm9[0,0]
	shufps	$226, %xmm9, %xmm3      # xmm3 = xmm3[2,0],xmm9[2,3]
	movaps	%xmm3, %xmm9
.LBB2_9:                                # %pred.sdiv.continue97
                                        #   in Loop: Header=BB2_5 Depth=2
	movdqa	%xmm1, 96(%rsp)
	testb	$1, 104(%rsp)
	je	.LBB2_11
# BB#10:                                # %pred.sdiv.if98
                                        #   in Loop: Header=BB2_5 Depth=2
	pshufd	$78, %xmm7, %xmm3       # xmm3 = xmm7[2,3,0,1]
	movd	%xmm3, %eax
	pshufd	$78, %xmm2, %xmm3       # xmm3 = xmm2[2,3,0,1]
	movd	%xmm3, %edi
	cltd
	idivl	%edi
	movd	%eax, %xmm3
	shufps	$48, %xmm9, %xmm3       # xmm3 = xmm3[0,0],xmm9[3,0]
	shufps	$132, %xmm3, %xmm9      # xmm9 = xmm9[0,1],xmm3[0,2]
.LBB2_11:                               # %pred.sdiv.continue99
                                        #   in Loop: Header=BB2_5 Depth=2
	movdqa	%xmm1, 80(%rsp)
	testb	$1, 92(%rsp)
	je	.LBB2_13
# BB#12:                                # %pred.sdiv.if100
                                        #   in Loop: Header=BB2_5 Depth=2
	pshufd	$231, %xmm7, %xmm3      # xmm3 = xmm7[3,1,2,3]
	movd	%xmm3, %eax
	pshufd	$231, %xmm2, %xmm3      # xmm3 = xmm2[3,1,2,3]
	movd	%xmm3, %edi
	cltd
	idivl	%edi
	movd	%eax, %xmm3
	shufps	$32, %xmm9, %xmm3       # xmm3 = xmm3[0,0],xmm9[2,0]
	shufps	$36, %xmm3, %xmm9       # xmm9 = xmm9[0,1],xmm3[2,0]
.LBB2_13:                               # %pred.sdiv.continue101
                                        #   in Loop: Header=BB2_5 Depth=2
	psubd	%xmm6, %xmm5
	movdqa	%xmm2, %xmm4
	pcmpgtd	%xmm5, %xmm4
	movdqa	%xmm4, %xmm6
	pandn	%xmm8, %xmm6
	movdqa	%xmm6, 64(%rsp)
	testb	$1, 64(%rsp)
                                        # implicit-def: %XMM7
	je	.LBB2_15
# BB#14:                                # %pred.sdiv.if102
                                        #   in Loop: Header=BB2_5 Depth=2
	movd	%xmm5, %eax
	movd	%xmm2, %edi
	cltd
	idivl	%edi
	movd	%eax, %xmm7
.LBB2_15:                               # %pred.sdiv.continue103
                                        #   in Loop: Header=BB2_5 Depth=2
	movdqa	%xmm6, 48(%rsp)
	testb	$1, 52(%rsp)
	je	.LBB2_17
# BB#16:                                # %pred.sdiv.if104
                                        #   in Loop: Header=BB2_5 Depth=2
	pshufd	$229, %xmm5, %xmm3      # xmm3 = xmm5[1,1,2,3]
	movd	%xmm3, %eax
	pshufd	$229, %xmm2, %xmm3      # xmm3 = xmm2[1,1,2,3]
	movd	%xmm3, %edi
	cltd
	idivl	%edi
	movd	%eax, %xmm3
	shufps	$0, %xmm7, %xmm3        # xmm3 = xmm3[0,0],xmm7[0,0]
	shufps	$226, %xmm7, %xmm3      # xmm3 = xmm3[2,0],xmm7[2,3]
	movaps	%xmm3, %xmm7
.LBB2_17:                               # %pred.sdiv.continue105
                                        #   in Loop: Header=BB2_5 Depth=2
	movdqa	%xmm6, 32(%rsp)
	testb	$1, 40(%rsp)
	je	.LBB2_19
# BB#18:                                # %pred.sdiv.if106
                                        #   in Loop: Header=BB2_5 Depth=2
	pshufd	$78, %xmm5, %xmm3       # xmm3 = xmm5[2,3,0,1]
	movd	%xmm3, %eax
	pshufd	$78, %xmm2, %xmm3       # xmm3 = xmm2[2,3,0,1]
	movd	%xmm3, %edi
	cltd
	idivl	%edi
	movd	%eax, %xmm3
	shufps	$48, %xmm7, %xmm3       # xmm3 = xmm3[0,0],xmm7[3,0]
	shufps	$132, %xmm3, %xmm7      # xmm7 = xmm7[0,1],xmm3[0,2]
.LBB2_19:                               # %pred.sdiv.continue107
                                        #   in Loop: Header=BB2_5 Depth=2
	movdqa	%xmm8, %xmm3
	pxor	%xmm10, %xmm3
	movdqa	%xmm6, 16(%rsp)
	testb	$1, 28(%rsp)
	je	.LBB2_21
# BB#20:                                # %pred.sdiv.if108
                                        #   in Loop: Header=BB2_5 Depth=2
	pshufd	$231, %xmm5, %xmm5      # xmm5 = xmm5[3,1,2,3]
	movd	%xmm5, %eax
	pshufd	$231, %xmm2, %xmm2      # xmm2 = xmm2[3,1,2,3]
	movd	%xmm2, %edi
	cltd
	idivl	%edi
	movd	%eax, %xmm2
	shufps	$32, %xmm7, %xmm2       # xmm2 = xmm2[0,0],xmm7[2,0]
	shufps	$36, %xmm2, %xmm7       # xmm7 = xmm7[0,1],xmm2[2,0]
.LBB2_21:                               # %pred.sdiv.continue109
                                        #   in Loop: Header=BB2_5 Depth=2
	pand	%xmm8, %xmm4
	xorps	%xmm2, %xmm2
	psubd	%xmm7, %xmm2
	pandn	%xmm2, %xmm4
	pslld	$31, %xmm1
	psrad	$31, %xmm1
	andps	%xmm1, %xmm9
	pandn	%xmm4, %xmm1
	por	%xmm9, %xmm1
	pand	%xmm3, %xmm0
	pslld	$31, %xmm0
	psrad	$31, %xmm0
	pandn	%xmm1, %xmm0
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	movq	%xmm0, (%rcx)
	addq	$4, %rsi
	addq	$8, %rcx
	cmpq	$64, %rsi
	jne	.LBB2_5
# BB#22:                                # %middle.block
                                        #   in Loop: Header=BB2_2 Depth=1
	incq	%r12
	addl	$8, %ebx
	subq	$-128, %r14
	cmpq	%r15, %r12
	jne	.LBB2_2
.LBB2_23:                               # %._crit_edge
	addq	$408, %rsp              # imm = 0x198
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	forward_DCT, .Lfunc_end2-forward_DCT
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	1182793984              # float 16384.5
	.long	1182793984              # float 16384.5
	.long	1182793984              # float 16384.5
	.long	1182793984              # float 16384.5
.LCPI3_1:
	.long	49152                   # 0xc000
	.long	49152                   # 0xc000
	.long	49152                   # 0xc000
	.long	49152                   # 0xc000
	.text
	.p2align	4, 0x90
	.type	forward_DCT_float,@function
forward_DCT_float:                      # @forward_DCT_float
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi37:
	.cfi_def_cfa_offset 336
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
	movq	%rcx, %r14
	movl	336(%rsp), %eax
	testl	%eax, %eax
	je	.LBB3_7
# BB#1:                                 # %.lr.ph
	movq	480(%rdi), %rcx
	movq	56(%rcx), %rdi
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movslq	16(%rsi), %rsi
	movq	64(%rcx,%rsi,8), %r13
	movl	%r8d, %ecx
	leaq	(%rdx,%rcx,8), %rbp
	movl	%eax, %r15d
	addq	$8, %r14
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_3 Depth 2
                                        #     Child Loop BB3_5 Depth 2
	movl	%ebx, %ebx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_3:                                #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp,%rax), %rcx
	movzbl	(%rcx,%rbx), %edx
	addl	$-128, %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	movss	%xmm0, 16(%rsp,%rax,4)
	movzbl	1(%rcx,%rbx), %edx
	addl	$-128, %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	movss	%xmm0, 20(%rsp,%rax,4)
	movzbl	2(%rcx,%rbx), %edx
	addl	$-128, %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	movss	%xmm0, 24(%rsp,%rax,4)
	movzbl	3(%rcx,%rbx), %edx
	addl	$-128, %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	movss	%xmm0, 28(%rsp,%rax,4)
	movzbl	4(%rcx,%rbx), %edx
	addl	$-128, %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	movss	%xmm0, 32(%rsp,%rax,4)
	movzbl	5(%rcx,%rbx), %edx
	addl	$-128, %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	movss	%xmm0, 36(%rsp,%rax,4)
	movzbl	6(%rcx,%rbx), %edx
	addl	$-128, %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	movss	%xmm0, 40(%rsp,%rax,4)
	movzbl	7(%rcx,%rbx), %ecx
	addl	$-128, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	movd	%xmm0, 44(%rsp,%rax,4)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.LBB3_3
# BB#4:                                 # %min.iters.checked
                                        #   in Loop: Header=BB3_2 Depth=1
	leaq	16(%rsp), %rdi
	callq	*8(%rsp)                # 8-byte Folded Reload
	movq	%r14, %rax
	xorl	%ecx, %ecx
	movaps	.LCPI3_0(%rip), %xmm1   # xmm1 = [1.638450e+04,1.638450e+04,1.638450e+04,1.638450e+04]
	movdqa	.LCPI3_1(%rip), %xmm2   # xmm2 = [49152,49152,49152,49152]
	.p2align	4, 0x90
.LBB3_5:                                # %vector.body
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r13,%rcx,4), %xmm0
	mulps	16(%rsp,%rcx,4), %xmm0
	addps	%xmm1, %xmm0
	cvttps2dq	%xmm0, %xmm0
	paddw	%xmm2, %xmm0
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	movq	%xmm0, -8(%rax)
	movups	16(%r13,%rcx,4), %xmm0
	mulps	32(%rsp,%rcx,4), %xmm0
	addps	%xmm1, %xmm0
	cvttps2dq	%xmm0, %xmm0
	paddw	%xmm2, %xmm0
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	movq	%xmm0, (%rax)
	addq	$8, %rcx
	addq	$16, %rax
	cmpq	$64, %rcx
	jne	.LBB3_5
# BB#6:                                 # %middle.block
                                        #   in Loop: Header=BB3_2 Depth=1
	incq	%r12
	addl	$8, %ebx
	subq	$-128, %r14
	cmpq	%r15, %r12
	jne	.LBB3_2
.LBB3_7:                                # %._crit_edge
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	forward_DCT_float, .Lfunc_end3-forward_DCT_float
	.cfi_endproc

	.type	start_pass_fdctmgr.aanscales,@object # @start_pass_fdctmgr.aanscales
	.section	.rodata,"a",@progbits
	.p2align	4
start_pass_fdctmgr.aanscales:
	.short	16384                   # 0x4000
	.short	22725                   # 0x58c5
	.short	21407                   # 0x539f
	.short	19266                   # 0x4b42
	.short	16384                   # 0x4000
	.short	12873                   # 0x3249
	.short	8867                    # 0x22a3
	.short	4520                    # 0x11a8
	.short	22725                   # 0x58c5
	.short	31521                   # 0x7b21
	.short	29692                   # 0x73fc
	.short	26722                   # 0x6862
	.short	22725                   # 0x58c5
	.short	17855                   # 0x45bf
	.short	12299                   # 0x300b
	.short	6270                    # 0x187e
	.short	21407                   # 0x539f
	.short	29692                   # 0x73fc
	.short	27969                   # 0x6d41
	.short	25172                   # 0x6254
	.short	21407                   # 0x539f
	.short	16819                   # 0x41b3
	.short	11585                   # 0x2d41
	.short	5906                    # 0x1712
	.short	19266                   # 0x4b42
	.short	26722                   # 0x6862
	.short	25172                   # 0x6254
	.short	22654                   # 0x587e
	.short	19266                   # 0x4b42
	.short	15137                   # 0x3b21
	.short	10426                   # 0x28ba
	.short	5315                    # 0x14c3
	.short	16384                   # 0x4000
	.short	22725                   # 0x58c5
	.short	21407                   # 0x539f
	.short	19266                   # 0x4b42
	.short	16384                   # 0x4000
	.short	12873                   # 0x3249
	.short	8867                    # 0x22a3
	.short	4520                    # 0x11a8
	.short	12873                   # 0x3249
	.short	17855                   # 0x45bf
	.short	16819                   # 0x41b3
	.short	15137                   # 0x3b21
	.short	12873                   # 0x3249
	.short	10114                   # 0x2782
	.short	6967                    # 0x1b37
	.short	3552                    # 0xde0
	.short	8867                    # 0x22a3
	.short	12299                   # 0x300b
	.short	11585                   # 0x2d41
	.short	10426                   # 0x28ba
	.short	8867                    # 0x22a3
	.short	6967                    # 0x1b37
	.short	4799                    # 0x12bf
	.short	2446                    # 0x98e
	.short	4520                    # 0x11a8
	.short	6270                    # 0x187e
	.short	5906                    # 0x1712
	.short	5315                    # 0x14c3
	.short	4520                    # 0x11a8
	.short	3552                    # 0xde0
	.short	2446                    # 0x98e
	.short	1247                    # 0x4df
	.size	start_pass_fdctmgr.aanscales, 128

	.type	start_pass_fdctmgr.aanscalefactor,@object # @start_pass_fdctmgr.aanscalefactor
	.p2align	4
start_pass_fdctmgr.aanscalefactor:
	.quad	4607182418800017408     # double 1
	.quad	4608925491301736943     # double 1.3870398450000001
	.quad	4608563055654957002     # double 1.3065629649999999
	.quad	4607974492095648170     # double 1.1758756020000001
	.quad	4607182418800017408     # double 1
	.quad	4605252130585427771     # double 0.785694958
	.quad	4603049880653865147     # double 0.54119609999999996
	.quad	4598641781279730525     # double 0.275899379
	.size	start_pass_fdctmgr.aanscalefactor, 64


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
