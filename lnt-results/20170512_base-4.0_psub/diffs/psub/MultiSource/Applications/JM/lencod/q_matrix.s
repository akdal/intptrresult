	.text
	.file	"q_matrix.bc"
	.globl	CheckParameterName
	.p2align	4, 0x90
	.type	CheckParameterName,@function
CheckParameterName:                     # @CheckParameterName
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$0, (%r15)
	movl	$MatrixType4x4, %ebx
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_8
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	cmpq	$5, %r12
	jg	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	incq	%r12
	cmpq	$-20, %rbx
	leaq	20(%rbx), %rbx
	jne	.LBB0_1
.LBB0_4:
	movl	$1, (%r15)
	movl	$MatrixType8x8, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_9
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=1
	incq	%rbx
	movl	$-1, %r12d
	cmpq	$1, %rbx
	jg	.LBB0_10
# BB#7:                                 #   in Loop: Header=BB0_5 Depth=1
	cmpq	$-20, %r15
	leaq	20(%r15), %r15
	jne	.LBB0_5
	jmp	.LBB0_10
.LBB0_8:                                # %.loopexit.loopexit28
	decl	%r12d
	jmp	.LBB0_10
.LBB0_9:                                # %..loopexit.loopexit_crit_edge
	movl	%ebx, %r12d
.LBB0_10:                               # %.loopexit
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	CheckParameterName, .Lfunc_end0-CheckParameterName
	.cfi_endproc

	.globl	ParseMatrix
	.p2align	4, 0x90
	.type	ParseMatrix,@function
ParseMatrix:                            # @ParseMatrix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$8040, %rsp             # imm = 0x1F68
.Lcfi15:
	.cfi_def_cfa_offset 8096
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	testl	%esi, %esi
	jle	.LBB1_45
# BB#1:                                 # %.lr.ph134.preheader
	movslq	%esi, %rax
	addq	%rdi, %rax
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph134
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_7 Depth 2
	movsbl	(%rdi), %esi
	addl	$-9, %esi
	cmpl	$35, %esi
	ja	.LBB1_26
# BB#3:                                 # %.lr.ph134
                                        #   in Loop: Header=BB1_2 Depth=1
	jmpq	*.LJTI1_0(,%rsi,8)
.LBB1_11:                               #   in Loop: Header=BB1_2 Depth=1
	leaq	1(%rdi), %rsi
	testl	%edx, %edx
	je	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_2 Depth=1
	movq	%rsi, %rdi
	cmpq	%rax, %rdi
	jb	.LBB1_2
	jmp	.LBB1_16
.LBB1_26:                               #   in Loop: Header=BB1_2 Depth=1
	testl	%ecx, %ecx
	jne	.LBB1_28
# BB#27:                                #   in Loop: Header=BB1_2 Depth=1
	movl	12(%rsp), %esi          # 4-byte Reload
	movslq	%esi, %rcx
	incl	%esi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movq	%rdi, 32(%rsp,%rcx,8)
	movl	$-1, %ecx
.LBB1_28:                               #   in Loop: Header=BB1_2 Depth=1
	incq	%rdi
	cmpq	%rax, %rdi
	jb	.LBB1_2
	jmp	.LBB1_16
.LBB1_5:                                #   in Loop: Header=BB1_2 Depth=1
	movb	$0, (%rdi)
	xorl	%ecx, %ecx
	cmpq	%rax, %rdi
	jae	.LBB1_14
# BB#6:                                 # %._crit_edge138.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	incq	%rdi
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB1_7:                                # %._crit_edge138
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%rdx), %rdi
	cmpq	%rax, %rdx
	jae	.LBB1_9
# BB#8:                                 # %._crit_edge138
                                        #   in Loop: Header=BB1_7 Depth=2
	cmpb	$10, (%rdx)
	movq	%rdi, %rdx
	jne	.LBB1_7
.LBB1_9:                                # %.critedge.backedge.loopexit
                                        #   in Loop: Header=BB1_2 Depth=1
	decq	%rdi
	jmp	.LBB1_14
.LBB1_10:                               #   in Loop: Header=BB1_2 Depth=1
	movb	$0, (%rdi)
	incq	%rdi
	xorl	%ecx, %ecx
	jmp	.LBB1_14
.LBB1_4:                                #   in Loop: Header=BB1_2 Depth=1
	incq	%rdi
	cmpq	%rax, %rdi
	jb	.LBB1_2
	jmp	.LBB1_16
.LBB1_22:                               #   in Loop: Header=BB1_2 Depth=1
	movb	$0, (%rdi)
	incq	%rdi
	xorl	%esi, %esi
	testl	%edx, %edx
	jne	.LBB1_24
# BB#23:                                #   in Loop: Header=BB1_2 Depth=1
	movl	12(%rsp), %ebp          # 4-byte Reload
	movslq	%ebp, %rsi
	incl	%ebp
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	%rdi, 32(%rsp,%rsi,8)
	notl	%ecx
	movl	%ecx, %esi
.LBB1_24:                               #   in Loop: Header=BB1_2 Depth=1
	notl	%edx
	movl	%esi, %ecx
	cmpq	%rax, %rdi
	jb	.LBB1_2
	jmp	.LBB1_16
.LBB1_25:                               #   in Loop: Header=BB1_2 Depth=1
	incq	%rdi
	xorl	%ecx, %ecx
	cmpq	%rax, %rdi
	jb	.LBB1_2
	jmp	.LBB1_16
.LBB1_13:                               #   in Loop: Header=BB1_2 Depth=1
	movb	$0, (%rdi)
	xorl	%ecx, %ecx
	movq	%rsi, %rdi
.LBB1_14:                               # %.critedge.backedge
                                        #   in Loop: Header=BB1_2 Depth=1
	xorl	%edx, %edx
	cmpq	%rax, %rdi
	jb	.LBB1_2
.LBB1_16:                               # %.critedge._crit_edge
	cmpl	$2, 12(%rsp)            # 4-byte Folded Reload
	jl	.LBB1_45
# BB#17:                                # %.lr.ph.preheader
	decl	12(%rsp)                # 4-byte Folded Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_18:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_19 Depth 2
                                        #     Child Loop BB1_32 Depth 2
                                        #     Child Loop BB1_41 Depth 2
	movslq	%r14d, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	32(%rsp,%rax,8), %r15
	movl	$MatrixType4x4, %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_19:                               #   Parent Loop BB1_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_20
# BB#29:                                #   in Loop: Header=BB1_19 Depth=2
	incq	%r12
	cmpq	$5, %r12
	jg	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_19 Depth=2
	cmpq	$-20, %rbx
	leaq	20(%rbx), %rbx
	jne	.LBB1_19
.LBB1_31:                               # %.preheader.preheader
                                        #   in Loop: Header=BB1_18 Depth=1
	movl	$MatrixType8x8, %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_32:                               # %.preheader
                                        #   Parent Loop BB1_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_33
# BB#34:                                #   in Loop: Header=BB1_32 Depth=2
	incq	%r12
	movl	$1, %r13d
	movq	$-1, %rbp
	cmpq	$1, %r12
	jg	.LBB1_36
# BB#35:                                #   in Loop: Header=BB1_32 Depth=2
	cmpq	$-20, %rbx
	leaq	20(%rbx), %rbx
	jne	.LBB1_32
	jmp	.LBB1_36
	.p2align	4, 0x90
.LBB1_20:                               #   in Loop: Header=BB1_18 Depth=1
	xorl	%r13d, %r13d
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_33:                               #   in Loop: Header=BB1_18 Depth=1
	movl	$1, %r13d
.LBB1_21:                               # %CheckParameterName.exit
                                        #   in Loop: Header=BB1_18 Depth=1
	testl	%r12d, %r12d
	movq	%r12, %rbp
	jns	.LBB1_37
.LBB1_36:                               # %CheckParameterName.exit.thread
                                        #   in Loop: Header=BB1_18 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str, %edx
	xorl	%eax, %eax
	movq	%r15, %rcx
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
	movq	%rbp, %r12
.LBB1_37:                               #   in Loop: Header=BB1_18 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp,%rax,8), %rax
	cmpb	$61, (%rax)
	jne	.LBB1_39
# BB#38:                                #   in Loop: Header=BB1_18 Depth=1
	cmpb	$0, 1(%rax)
	je	.LBB1_40
.LBB1_39:                               # %.thread
                                        #   in Loop: Header=BB1_18 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
.LBB1_40:                               #   in Loop: Header=BB1_18 Depth=1
	shlq	$32, %r12
	movq	%r12, %rax
	sarq	$25, %rax
	movq	%r12, %rcx
	sarq	$30, %rcx
	sarq	$27, %r12
	addl	$2, %r14d
	testl	%r13d, %r13d
	leaq	ScalingList8x8input(%rax), %rax
	leaq	matrix8x8_check(%rcx), %rdx
	leaq	ScalingList4x4input(%r12), %r12
	leaq	matrix4x4_check(%rcx), %rcx
	cmovneq	%rax, %r12
	cmovneq	%rdx, %rcx
	movl	$1, (%rcx)
	movl	$64, %r13d
	movl	$16, %eax
	cmoveq	%rax, %r13
	movl	%r14d, 16(%rsp)         # 4-byte Spill
	movslq	%r14d, %rax
	leaq	32(%rsp,%rax,8), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_41:                               #   Parent Loop BB1_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rbp,8), %rbx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	leaq	28(%rsp), %rdx
	callq	sscanf
	cmpl	$1, %eax
	je	.LBB1_43
# BB#42:                                #   in Loop: Header=BB1_41 Depth=2
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.4, %edx
	xorl	%eax, %eax
	movq	%r15, %rcx
	movq	%rbx, %r8
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
.LBB1_43:                               #   in Loop: Header=BB1_41 Depth=2
	movzwl	28(%rsp), %eax
	movw	%ax, (%r12,%rbp,2)
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB1_41
# BB#44:                                #   in Loop: Header=BB1_18 Depth=1
	movl	$46, %edi
	callq	putchar
	movl	16(%rsp), %r14d         # 4-byte Reload
	addl	%ebp, %r14d
	cmpl	12(%rsp), %r14d         # 4-byte Folded Reload
	jl	.LBB1_18
.LBB1_45:                               # %._crit_edge
	addq	$8040, %rsp             # imm = 0x1F68
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	ParseMatrix, .Lfunc_end1-ParseMatrix
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_11
	.quad	.LBB1_10
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_4
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_11
	.quad	.LBB1_26
	.quad	.LBB1_22
	.quad	.LBB1_5
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_25

	.text
	.globl	PatchMatrix
	.p2align	4, 0x90
	.type	PatchMatrix,@function
PatchMatrix:                            # @PatchMatrix
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movl	$ScalingList8x8input+6, %r14d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_14 Depth 2
	movq	input(%rip), %rax
	cmpl	$0, 5212(%rax,%rbx,4)
	je	.LBB2_10
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movq	%rbx, %rax
	shlq	$5, %rax
	leaq	ScalingList4x4input(%rax), %r15
	cmpl	$0, matrix4x4_check(,%rbx,4)
	je	.LBB2_8
# BB#3:                                 # %.preheader46.preheader
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	(%r15), %ecx
	cmpl	$255, %ecx
	ja	.LBB2_5
# BB#4:                                 # %.preheader46.150
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+2(%rax), %ecx
	cmpl	$256, %ecx              # imm = 0x100
	jae	.LBB2_5
# BB#21:                                # %.preheader46.251
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+4(%rax), %ecx
	cmpl	$255, %ecx
	ja	.LBB2_5
# BB#22:                                # %.preheader46.352
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+6(%rax), %ecx
	cmpl	$255, %ecx
	ja	.LBB2_5
# BB#23:                                # %.preheader46.453
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+8(%rax), %ecx
	cmpl	$255, %ecx
	ja	.LBB2_5
# BB#24:                                # %.preheader46.554
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+10(%rax), %ecx
	cmpl	$255, %ecx
	ja	.LBB2_5
# BB#25:                                # %.preheader46.655
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+12(%rax), %ecx
	cmpl	$255, %ecx
	ja	.LBB2_5
# BB#26:                                # %.preheader46.756
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+14(%rax), %ecx
	cmpl	$255, %ecx
	ja	.LBB2_5
# BB#27:                                # %.preheader46.857
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+16(%rax), %ecx
	cmpl	$255, %ecx
	ja	.LBB2_5
# BB#28:                                # %.preheader46.958
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+18(%rax), %ecx
	cmpl	$255, %ecx
	ja	.LBB2_5
# BB#29:                                # %.preheader46.1059
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+20(%rax), %ecx
	cmpl	$255, %ecx
	ja	.LBB2_5
# BB#30:                                # %.preheader46.1160
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+22(%rax), %ecx
	cmpl	$255, %ecx
	ja	.LBB2_5
# BB#31:                                # %.preheader46.1261
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+24(%rax), %ecx
	cmpl	$255, %ecx
	ja	.LBB2_5
# BB#32:                                # %.preheader46.1362
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+26(%rax), %ecx
	cmpl	$255, %ecx
	ja	.LBB2_5
# BB#33:                                # %.preheader46.1463
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+28(%rax), %ecx
	cmpl	$255, %ecx
	ja	.LBB2_5
# BB#34:                                # %.preheader46.1564
                                        #   in Loop: Header=BB2_1 Depth=1
	movzwl	ScalingList4x4input+30(%rax), %eax
	cmpl	$255, %eax
	jbe	.LBB2_10
	.p2align	4, 0x90
.LBB2_5:                                #   in Loop: Header=BB2_1 Depth=1
	leaq	(%rbx,%rbx,4), %rax
	leaq	MatrixType4x4(,%rax,4), %rsi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	cmpq	$3, %rbx
	jge	.LBB2_7
	jmp	.LBB2_9
	.p2align	4, 0x90
.LBB2_8:                                #   in Loop: Header=BB2_1 Depth=1
	leaq	(%rbx,%rbx,4), %rax
	leaq	MatrixType4x4(,%rax,4), %rsi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	cmpq	$3, %rbx
	jl	.LBB2_9
.LBB2_7:                                #   in Loop: Header=BB2_1 Depth=1
	movaps	Quant_inter_default+16(%rip), %xmm0
	movaps	%xmm0, 16(%r15)
	movaps	Quant_inter_default(%rip), %xmm0
	jmp	.LBB2_18
	.p2align	4, 0x90
.LBB2_9:                                #   in Loop: Header=BB2_1 Depth=1
	movaps	Quant_intra_default+16(%rip), %xmm0
	movaps	%xmm0, 16(%r15)
	movaps	Quant_intra_default(%rip), %xmm0
	movaps	%xmm0, (%r15)
.LBB2_10:                               # %.critedge
                                        #   in Loop: Header=BB2_1 Depth=1
	cmpq	$1, %rbx
	jg	.LBB2_19
# BB#11:                                #   in Loop: Header=BB2_1 Depth=1
	movq	input(%rip), %rax
	cmpl	$0, 5236(%rax,%rbx,4)
	je	.LBB2_19
# BB#12:                                #   in Loop: Header=BB2_1 Depth=1
	movq	%rbx, %rax
	shlq	$7, %rax
	leaq	ScalingList8x8input(%rax), %r15
	cmpl	$0, matrix8x8_check(,%rbx,4)
	je	.LBB2_16
# BB#13:                                # %.preheader.preheader
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	%r14, %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_14:                               # %.preheader
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-6(%rax), %edx
	cmpl	$256, %edx              # imm = 0x100
	jae	.LBB2_15
# BB#35:                                # %.preheader.168
                                        #   in Loop: Header=BB2_14 Depth=2
	movzwl	-4(%rax), %edx
	cmpl	$255, %edx
	ja	.LBB2_15
# BB#36:                                # %.preheader.269
                                        #   in Loop: Header=BB2_14 Depth=2
	movzwl	-2(%rax), %edx
	cmpl	$255, %edx
	ja	.LBB2_15
# BB#37:                                # %.preheader.370
                                        #   in Loop: Header=BB2_14 Depth=2
	movzwl	(%rax), %edx
	cmpl	$255, %edx
	ja	.LBB2_15
# BB#38:                                #   in Loop: Header=BB2_14 Depth=2
	addq	$4, %rcx
	addq	$8, %rax
	cmpq	$64, %rcx
	jl	.LBB2_14
	jmp	.LBB2_19
.LBB2_15:                               #   in Loop: Header=BB2_1 Depth=1
	leaq	(%rbx,%rbx,4), %rax
	leaq	MatrixType8x8(,%rax,4), %rsi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB2_17
.LBB2_16:                               #   in Loop: Header=BB2_1 Depth=1
	leaq	(%rbx,%rbx,4), %rax
	leaq	MatrixType8x8(,%rax,4), %rsi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
.LBB2_17:                               # %.critedge45
                                        #   in Loop: Header=BB2_1 Depth=1
	movaps	Quant8_intra_default+112(%rip), %xmm0
	movaps	%xmm0, 112(%r15)
	movaps	Quant8_intra_default+96(%rip), %xmm0
	movaps	%xmm0, 96(%r15)
	movaps	Quant8_intra_default+80(%rip), %xmm0
	movaps	%xmm0, 80(%r15)
	movaps	Quant8_intra_default+64(%rip), %xmm0
	movaps	%xmm0, 64(%r15)
	movaps	Quant8_intra_default+48(%rip), %xmm0
	movaps	%xmm0, 48(%r15)
	movaps	Quant8_intra_default+32(%rip), %xmm0
	movaps	%xmm0, 32(%r15)
	movaps	Quant8_intra_default+16(%rip), %xmm0
	movaps	%xmm0, 16(%r15)
	movaps	Quant8_intra_default(%rip), %xmm0
.LBB2_18:                               # %.critedge45
                                        #   in Loop: Header=BB2_1 Depth=1
	movaps	%xmm0, (%r15)
.LBB2_19:                               # %.critedge45
                                        #   in Loop: Header=BB2_1 Depth=1
	incq	%rbx
	subq	$-128, %r14
	cmpq	$6, %rbx
	jne	.LBB2_1
# BB#20:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	PatchMatrix, .Lfunc_end2-PatchMatrix
	.cfi_endproc

	.globl	allocate_QMatrix
	.p2align	4, 0x90
	.type	allocate_QMatrix,@function
allocate_QMatrix:                       # @allocate_QMatrix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	input(%rip), %rax
	movl	5256(%rax), %eax
	leal	(%rax,%rax,2), %eax
	leal	4(%rax,%rax), %ebp
	movslq	%ebp, %rbx
	shlq	$2, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, qp_per_matrix(%rip)
	testq	%rax, %rax
	jne	.LBB3_2
# BB#1:
	movl	$.L.str.9, %edi
	callq	no_mem_exit
.LBB3_2:
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, qp_rem_matrix(%rip)
	testq	%rax, %rax
	jne	.LBB3_4
# BB#3:
	movl	$.L.str.9, %edi
	callq	no_mem_exit
.LBB3_4:                                # %.preheader
	testl	%ebp, %ebp
	jle	.LBB3_7
# BB#5:                                 # %.lr.ph.new
	movq	qp_per_matrix(%rip), %rax
	movq	qp_rem_matrix(%rip), %rcx
	movl	%ebp, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_6:                                # =>This Inner Loop Header: Depth=1
	movslq	%esi, %rdi
	imulq	$715827883, %rdi, %rbp  # imm = 0x2AAAAAAB
	movq	%rbp, %rbx
	shrq	$63, %rbx
	shrq	$32, %rbp
	addl	%ebx, %ebp
	movl	%ebp, (%rax,%rsi,4)
	addl	%ebp, %ebp
	leal	(%rbp,%rbp,2), %ebp
	subl	%ebp, %edi
	movl	%edi, (%rcx,%rsi,4)
	leaq	1(%rsi), %rdi
	movslq	%edi, %rdi
	imulq	$715827883, %rdi, %rbp  # imm = 0x2AAAAAAB
	movq	%rbp, %rbx
	shrq	$63, %rbx
	shrq	$32, %rbp
	addl	%ebx, %ebp
	movl	%ebp, 4(%rax,%rsi,4)
	addl	%ebp, %ebp
	leal	(%rbp,%rbp,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 4(%rcx,%rsi,4)
	addq	$2, %rsi
	cmpq	%rdx, %rsi
	jne	.LBB3_6
.LBB3_7:                                # %._crit_edge
	movl	$LevelScale4x4Luma, %edi
	movl	$2, %esi
	movl	$6, %edx
	movl	$4, %ecx
	movl	$4, %r8d
	callq	get_mem4Dint
	movl	$LevelScale4x4Chroma, %edi
	movl	$2, %esi
	movl	$2, %edx
	movl	$6, %ecx
	movl	$4, %r8d
	movl	$4, %r9d
	callq	get_mem5Dint
	movl	$LevelScale8x8Luma, %edi
	movl	$2, %esi
	movl	$6, %edx
	movl	$8, %ecx
	movl	$8, %r8d
	callq	get_mem4Dint
	movl	$InvLevelScale4x4Luma, %edi
	movl	$2, %esi
	movl	$6, %edx
	movl	$4, %ecx
	movl	$4, %r8d
	callq	get_mem4Dint
	movl	$InvLevelScale4x4Chroma, %edi
	movl	$2, %esi
	movl	$2, %edx
	movl	$6, %ecx
	movl	$4, %r8d
	movl	$4, %r9d
	callq	get_mem5Dint
	movl	$InvLevelScale8x8Luma, %edi
	movl	$2, %esi
	movl	$6, %edx
	movl	$8, %ecx
	movl	$8, %r8d
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	get_mem4Dint            # TAILCALL
.Lfunc_end3:
	.size	allocate_QMatrix, .Lfunc_end3-allocate_QMatrix
	.cfi_endproc

	.globl	free_QMatrix
	.p2align	4, 0x90
	.type	free_QMatrix,@function
free_QMatrix:                           # @free_QMatrix
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 16
	movq	qp_rem_matrix(%rip), %rdi
	callq	free
	movq	qp_per_matrix(%rip), %rdi
	callq	free
	movq	LevelScale4x4Luma(%rip), %rdi
	movl	$2, %esi
	movl	$6, %edx
	callq	free_mem4Dint
	movq	LevelScale4x4Chroma(%rip), %rdi
	movl	$2, %esi
	movl	$2, %edx
	movl	$6, %ecx
	callq	free_mem5Dint
	movq	LevelScale8x8Luma(%rip), %rdi
	movl	$2, %esi
	movl	$6, %edx
	callq	free_mem4Dint
	movq	InvLevelScale4x4Luma(%rip), %rdi
	movl	$2, %esi
	movl	$6, %edx
	callq	free_mem4Dint
	movq	InvLevelScale4x4Chroma(%rip), %rdi
	movl	$2, %esi
	movl	$2, %edx
	movl	$6, %ecx
	callq	free_mem5Dint
	movq	InvLevelScale8x8Luma(%rip), %rdi
	movl	$2, %esi
	movl	$6, %edx
	popq	%rax
	jmp	free_mem4Dint           # TAILCALL
.Lfunc_end4:
	.size	free_QMatrix, .Lfunc_end4-free_QMatrix
	.cfi_endproc

	.globl	Init_QMatrix
	.p2align	4, 0x90
	.type	Init_QMatrix,@function
Init_QMatrix:                           # @Init_QMatrix
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 16
.Lcfi35:
	.cfi_offset %rbx, -16
	callq	allocate_QMatrix
	movq	input(%rip), %rsi
	cmpl	$0, 5208(%rsi)
	je	.LBB5_5
# BB#1:
	addq	$1304, %rsi             # imm = 0x518
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	printf
	movq	input(%rip), %rdi
	addq	$1304, %rdi             # imm = 0x518
	xorl	%esi, %esi
	callq	GetConfigFileContent
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_3
# BB#2:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	ParseMatrix
	jmp	.LBB5_4
.LBB5_5:
	popq	%rbx
	retq
.LBB5_3:
	movl	$.L.str.11, %edi
	movl	$errortext, %esi
	xorl	%eax, %eax
	callq	printf
.LBB5_4:
	callq	PatchMatrix
	movl	$10, %edi
	callq	putchar
	movl	$0, UseDefaultScalingMatrix4x4Flag+8(%rip)
	movq	$0, UseDefaultScalingMatrix4x4Flag(%rip)
	movl	$0, UseDefaultScalingMatrix8x8Flag(%rip)
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end5:
	.size	Init_QMatrix, .Lfunc_end5-Init_QMatrix
	.cfi_endproc

	.globl	CalculateQuantParam
	.p2align	4, 0x90
	.type	CalculateQuantParam,@function
CalculateQuantParam:                    # @CalculateQuantParam
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 208
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	active_sps(%rip), %rax
	cmpl	$0, 36(%rax)
	je	.LBB6_1
# BB#3:                                 # %.preheader407
	movq	56(%rax), %rcx
	movq	%rcx, -112(%rsp)
	movups	40(%rax), %xmm0
	movaps	%xmm0, -128(%rsp)
	jmp	.LBB6_4
.LBB6_1:
	movq	active_pps(%rip), %rax
	cmpl	$0, 20(%rax)
	je	.LBB6_11
# BB#2:                                 # %.thread
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -128(%rsp)
	movq	$0, -112(%rsp)
.LBB6_4:                                # %.loopexit408
	movq	active_pps(%rip), %rax
	cmpl	$0, 20(%rax)
	je	.LBB6_5
# BB#29:                                # %.critedge.preheader.loopexit433
	movl	-128(%rsp), %ecx
	orl	24(%rax), %ecx
	movl	%ecx, -56(%rsp)         # 4-byte Spill
	movl	%ecx, -128(%rsp)
	movl	28(%rax), %ecx
	movl	%ecx, -64(%rsp)         # 4-byte Spill
	movl	%ecx, -124(%rsp)
	movl	32(%rax), %ecx
	movl	%ecx, -72(%rsp)         # 4-byte Spill
	movl	%ecx, -120(%rsp)
	movl	-116(%rsp), %ecx
	orl	36(%rax), %ecx
	movl	%ecx, -80(%rsp)         # 4-byte Spill
	movl	%ecx, -116(%rsp)
	movl	40(%rax), %ecx
	movl	%ecx, -88(%rsp)         # 4-byte Spill
	movl	%ecx, -112(%rsp)
	movl	44(%rax), %eax
	movl	%eax, -96(%rsp)         # 4-byte Spill
	movl	%eax, -108(%rsp)
	jmp	.LBB6_6
.LBB6_5:                                # %.loopexit408..critedge.preheader_crit_edge
	movl	-128(%rsp), %eax
	movl	%eax, -56(%rsp)         # 4-byte Spill
	movl	-124(%rsp), %eax
	movl	%eax, -64(%rsp)         # 4-byte Spill
	movl	-120(%rsp), %eax
	movl	%eax, -72(%rsp)         # 4-byte Spill
	movl	-116(%rsp), %eax
	movl	%eax, -80(%rsp)         # 4-byte Spill
	movl	-112(%rsp), %eax
	movl	%eax, -88(%rsp)         # 4-byte Spill
	movl	-108(%rsp), %eax
	movl	%eax, -96(%rsp)         # 4-byte Spill
.LBB6_6:                                # %.critedge.preheader
	cmpw	$0, UseDefaultScalingMatrix4x4Flag+10(%rip)
	movl	$Quant_inter_default, %eax
	movl	$ScalingList4x4+160, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmpw	$0, UseDefaultScalingMatrix4x4Flag+8(%rip)
	movl	$ScalingList4x4+128, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	cmpw	$0, UseDefaultScalingMatrix4x4Flag+4(%rip)
	movl	$Quant_intra_default, %eax
	movl	$ScalingList4x4+64, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	cmpw	$0, UseDefaultScalingMatrix4x4Flag+2(%rip)
	movl	$ScalingList4x4+32, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	LevelScale4x4Luma(%rip), %rax
	movq	InvLevelScale4x4Luma(%rip), %rcx
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	8(%rcx), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB6_7:                                # %.preheader404
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_8 Depth 2
                                        #       Child Loop BB6_9 Depth 3
	movq	-32(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r14,8), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r14,8), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	xorl	%r8d, %r8d
	movq	%r10, -24(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB6_8:                                # %.preheader403
                                        #   Parent Loop BB6_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_9 Depth 3
	movq	-8(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r15,8), %rdi
	movq	-16(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r15,8), %r12
	movq	%r8, 8(%rsp)            # 8-byte Spill
	xorl	%r9d, %r9d
	movq	%r10, (%rsp)            # 8-byte Spill
	movq	%rdi, -104(%rsp)        # 8-byte Spill
	movq	%r12, -48(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB6_9:                                #   Parent Loop BB6_7 Depth=1
                                        #     Parent Loop BB6_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	quant_coef(%r10), %ecx
	shll	$4, %ecx
	cmpw	$0, UseDefaultScalingMatrix4x4Flag(%rip)
	leaq	ScalingList4x4(%r8), %rax
	leaq	Quant_intra_default(%r8), %rdx
	cmovneq	%rdx, %rax
	cmpl	$0, -56(%rsp)           # 4-byte Folded Reload
	cmoveq	%rdx, %rax
	movswl	(%rax), %esi
	movl	%ecx, %eax
	cltd
	idivl	%esi
	movl	%eax, (%rdi,%r9)
	movl	dequant_coef(%r10), %r11d
	imull	%r11d, %esi
	movl	%esi, (%r12,%r9)
	cmpl	$0, -64(%rsp)           # 4-byte Folded Reload
	je	.LBB6_10
# BB#15:                                #   in Loop: Header=BB6_9 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movswl	(%rax,%r8), %r12d
	movl	%ecx, %eax
	cltd
	idivl	%r12d
	movq	LevelScale4x4Chroma(%rip), %rbx
	movq	(%rbx), %r13
	movq	8(%r13), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r15,8), %rdx
	movl	%eax, (%rdx,%r9)
	imull	%r11d, %r12d
	jmp	.LBB6_16
	.p2align	4, 0x90
.LBB6_10:                               #   in Loop: Header=BB6_9 Depth=3
	movl	(%rdi,%r9), %eax
	movq	LevelScale4x4Chroma(%rip), %rbx
	movq	(%rbx), %r13
	movq	8(%r13), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r15,8), %rdx
	movl	%eax, (%rdx,%r9)
	movl	(%r12,%r9), %r12d
.LBB6_16:                               #   in Loop: Header=BB6_9 Depth=3
	movq	InvLevelScale4x4Chroma(%rip), %rbp
	movq	(%rbp), %rdi
	movq	8(%rdi), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movl	%r12d, (%rax,%r9)
	cmpl	$0, -72(%rsp)           # 4-byte Folded Reload
	je	.LBB6_17
# BB#18:                                #   in Loop: Header=BB6_9 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	movswl	(%rax,%r8), %esi
	movl	%ecx, %eax
	cltd
	idivl	%esi
	movq	8(%rbx), %rbx
	movq	8(%rbx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r15,8), %rdx
	movl	%eax, (%rdx,%r9)
	imull	%r11d, %esi
	jmp	.LBB6_19
	.p2align	4, 0x90
.LBB6_17:                               #   in Loop: Header=BB6_9 Depth=3
	movl	(%rdx,%r9), %edx
	movq	8(%rbx), %rbx
	movq	8(%rbx), %rsi
	movq	(%rsi,%r14,8), %rsi
	movq	(%rsi,%r15,8), %rsi
	movl	%edx, (%rsi,%r9)
	movl	(%rax,%r9), %esi
.LBB6_19:                               #   in Loop: Header=BB6_9 Depth=3
	movq	-48(%rsp), %r12         # 8-byte Reload
	movq	8(%rbp), %rbp
	movq	8(%rbp), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movl	%esi, (%rax,%r9)
	cmpw	$0, UseDefaultScalingMatrix4x4Flag+6(%rip)
	leaq	ScalingList4x4+96(%r8), %rax
	leaq	Quant_inter_default(%r8), %rdx
	cmovneq	%rdx, %rax
	cmpl	$0, -80(%rsp)           # 4-byte Folded Reload
	cmoveq	%rdx, %rax
	movswl	(%rax), %esi
	movl	%ecx, %eax
	cltd
	idivl	%esi
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r15,8), %rdx
	movl	%eax, (%rdx,%r9)
	imull	%r11d, %esi
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movl	%esi, (%rax,%r9)
	cmpl	$0, -88(%rsp)           # 4-byte Folded Reload
	je	.LBB6_20
# BB#21:                                #   in Loop: Header=BB6_9 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	movswl	(%rax,%r8), %esi
	movl	%ecx, %eax
	cltd
	idivl	%esi
	movq	(%r13), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r15,8), %rdx
	movl	%eax, (%rdx,%r9)
	imull	%r11d, %esi
	jmp	.LBB6_22
	.p2align	4, 0x90
.LBB6_20:                               #   in Loop: Header=BB6_9 Depth=3
	movl	(%rdx,%r9), %esi
	movq	(%r13), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r15,8), %rdx
	movl	%esi, (%rdx,%r9)
	movl	(%rax,%r9), %esi
.LBB6_22:                               #   in Loop: Header=BB6_9 Depth=3
	movq	(%rdi), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movl	%esi, (%rax,%r9)
	cmpl	$0, -96(%rsp)           # 4-byte Folded Reload
	je	.LBB6_23
# BB#24:                                #   in Loop: Header=BB6_9 Depth=3
	movq	40(%rsp), %rax          # 8-byte Reload
	movswl	(%rax,%r8), %esi
	movl	%ecx, %eax
	cltd
	idivl	%esi
	movq	(%rbx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx,%r15,8), %rcx
	movl	%eax, (%rcx,%r9)
	imull	%esi, %r11d
	jmp	.LBB6_25
	.p2align	4, 0x90
.LBB6_23:                               #   in Loop: Header=BB6_9 Depth=3
	movl	(%rdx,%r9), %ecx
	movq	(%rbx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r15,8), %rdx
	movl	%ecx, (%rdx,%r9)
	movl	(%rax,%r9), %r11d
.LBB6_25:                               #   in Loop: Header=BB6_9 Depth=3
	movq	-104(%rsp), %rdi        # 8-byte Reload
	movq	(%rbp), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movl	%r11d, (%rax,%r9)
	addq	$4, %r10
	addq	$4, %r9
	addq	$2, %r8
	cmpq	$16, %r9
	jne	.LBB6_9
# BB#26:                                #   in Loop: Header=BB6_8 Depth=2
	incq	%r15
	movq	(%rsp), %r10            # 8-byte Reload
	addq	$16, %r10
	movq	8(%rsp), %r8            # 8-byte Reload
	addq	$8, %r8
	cmpq	$4, %r15
	jne	.LBB6_8
# BB#27:                                # %.critedge
                                        #   in Loop: Header=BB6_7 Depth=1
	incq	%r14
	movq	-24(%rsp), %r10         # 8-byte Reload
	addq	$64, %r10
	cmpq	$6, %r14
	jne	.LBB6_7
.LBB6_28:                               # %.loopexit
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_11:                               # %.preheader402
	movq	LevelScale4x4Luma(%rip), %rax
	movq	(%rax), %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	InvLevelScale4x4Luma(%rip), %rax
	movq	(%rax), %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	LevelScale4x4Chroma(%rip), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	(%rcx), %rdx
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	8(%rcx), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	InvLevelScale4x4Chroma(%rip), %rcx
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movq	(%rdx), %rsi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movq	8(%rdx), %rdx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	(%rax), %rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	(%rcx), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	8(%rcx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_12:                               # %.preheader401
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_13 Depth 2
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rdx, -32(%rsp)         # 8-byte Spill
	movq	(%rax,%rdx,8), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	%rbp, -24(%rsp)         # 8-byte Spill
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_13:                               # %.preheader
                                        #   Parent Loop BB6_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-88(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdi), %r11
	movq	-96(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdi), %rbx
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdi), %r10
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdi), %r8
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdi), %r9
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdi), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdi), %rdx
	movq	%rdx, -104(%rsp)        # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdi), %r14
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rdi), %r15
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax,%rdi), %r12
	movq	-8(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdi), %r13
	movq	-16(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdi), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movl	quant_coef(%rbp), %esi
	movl	%esi, (%r11)
	movl	dequant_coef(%rbp), %eax
	shll	$4, %eax
	movl	%eax, (%rbx)
	movl	%esi, (%r10)
	movq	%r10, -80(%rsp)         # 8-byte Spill
	movl	%eax, (%r8)
	movl	%esi, (%r9)
	movq	%r9, -72(%rsp)          # 8-byte Spill
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx)
	movl	%esi, (%rdx)
	movl	%eax, (%r14)
	movl	%esi, (%r15)
	movl	%eax, (%r12)
	movl	%esi, (%r13)
	movq	-56(%rsp), %rdx         # 8-byte Reload
	movl	%eax, (%rdx)
	movl	quant_coef+4(%rbp), %eax
	movl	%eax, 4(%r11)
	movl	dequant_coef+4(%rbp), %esi
	shll	$4, %esi
	movl	%esi, 4(%rbx)
	movl	%eax, 4(%r10)
	movl	%esi, 4(%r8)
	movq	%r8, -64(%rsp)          # 8-byte Spill
	movl	%eax, 4(%r9)
	movl	%esi, 4(%rcx)
	movq	%rcx, %r10
	movq	-104(%rsp), %rcx        # 8-byte Reload
	movl	%eax, 4(%rcx)
	movl	%esi, 4(%r14)
	movl	%eax, 4(%r15)
	movl	%esi, 4(%r12)
	movl	%eax, 4(%r13)
	movl	%esi, 4(%rdx)
	movl	quant_coef+8(%rbp), %eax
	movl	%eax, 8(%r11)
	movl	dequant_coef+8(%rbp), %esi
	shll	$4, %esi
	movl	%esi, 8(%rbx)
	movq	-80(%rsp), %r9          # 8-byte Reload
	movl	%eax, 8(%r9)
	movl	%esi, 8(%r8)
	movq	-72(%rsp), %r8          # 8-byte Reload
	movl	%eax, 8(%r8)
	movl	%esi, 8(%r10)
	movl	%eax, 8(%rcx)
	movl	%esi, 8(%r14)
	movl	%eax, 8(%r15)
	movl	%esi, 8(%r12)
	movl	%eax, 8(%r13)
	movl	%esi, 8(%rdx)
	movq	%rdx, %rcx
	movl	quant_coef+12(%rbp), %eax
	movl	%eax, 12(%r11)
	movl	dequant_coef+12(%rbp), %edx
	shll	$4, %edx
	movl	%edx, 12(%rbx)
	movl	%eax, 12(%r9)
	movq	-64(%rsp), %rsi         # 8-byte Reload
	movl	%edx, 12(%rsi)
	movl	%eax, 12(%r8)
	movl	%edx, 12(%r10)
	movq	-104(%rsp), %rsi        # 8-byte Reload
	movl	%eax, 12(%rsi)
	movl	%edx, 12(%r14)
	movl	%eax, 12(%r15)
	movl	%edx, 12(%r12)
	movl	%eax, 12(%r13)
	movl	%edx, 12(%rcx)
	addq	$8, %rdi
	addq	$16, %rbp
	cmpq	$32, %rdi
	jne	.LBB6_13
# BB#14:                                #   in Loop: Header=BB6_12 Depth=1
	movq	-32(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	movq	-24(%rsp), %rbp         # 8-byte Reload
	addq	$64, %rbp
	cmpq	$6, %rdx
	jne	.LBB6_12
	jmp	.LBB6_28
.Lfunc_end6:
	.size	CalculateQuantParam, .Lfunc_end6-CalculateQuantParam
	.cfi_endproc

	.globl	CalculateQuant8Param
	.p2align	4, 0x90
	.type	CalculateQuant8Param,@function
CalculateQuant8Param:                   # @CalculateQuant8Param
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	active_sps(%rip), %rax
	cmpl	$0, 36(%rax)
	je	.LBB7_1
# BB#3:                                 # %.preheader175
	movq	64(%rax), %rax
	jmp	.LBB7_4
.LBB7_1:
	movq	active_pps(%rip), %rax
	cmpl	$0, 20(%rax)
	je	.LBB7_12
# BB#2:
	xorl	%eax, %eax
.LBB7_4:                                # %.loopexit176
	movq	%rax, -80(%rsp)
	movq	active_pps(%rip), %rax
	cmpl	$0, 20(%rax)
	je	.LBB7_5
# BB#6:                                 # %.preheader174
	movl	-80(%rsp), %ecx
	orl	48(%rax), %ecx
	movl	%ecx, -36(%rsp)         # 4-byte Spill
	movl	%ecx, -80(%rsp)
	movl	-76(%rsp), %ebp
	orl	52(%rax), %ebp
	movl	%ebp, -76(%rsp)
	jmp	.LBB7_7
.LBB7_5:                                # %.loopexit176..critedge.preheader_crit_edge
	movl	-80(%rsp), %eax
	movl	%eax, -36(%rsp)         # 4-byte Spill
	movl	-76(%rsp), %ebp
.LBB7_7:                                # %.critedge.preheader
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB7_8:                                # %.preheader172
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_9 Depth 2
                                        #       Child Loop BB7_11 Depth 3
                                        #       Child Loop BB7_17 Depth 3
	movl	$Quant8_inter_default, %ecx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	movl	$Quant8_intra_default, %ecx
	movq	%rcx, -56(%rsp)         # 8-byte Spill
	movl	$ScalingList8x8, %ecx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	%rax, -72(%rsp)         # 8-byte Spill
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_9:                                # %.preheader171
                                        #   Parent Loop BB7_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_11 Depth 3
                                        #       Child Loop BB7_17 Depth 3
	cmpl	$0, -36(%rsp)           # 4-byte Folded Reload
	movq	LevelScale8x8Luma(%rip), %rax
	movq	InvLevelScale8x8Luma(%rip), %rcx
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	-32(%rsp), %rsi         # 8-byte Reload
	movq	(%rdx,%rsi,8), %rdx
	movq	(%rdx,%rdi,8), %r12
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movq	(%rdx,%rsi,8), %rdx
	movq	(%rdx,%rdi,8), %r13
	movq	(%rax,%rsi,8), %rax
	movq	(%rax,%rdi,8), %r14
	movq	(%rcx,%rsi,8), %rax
	movq	(%rax,%rdi,8), %rax
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	je	.LBB7_16
# BB#10:                                # %.preheader171.split.us.preheader
                                        #   in Loop: Header=BB7_9 Depth=2
	movq	-48(%rsp), %r11         # 8-byte Reload
	movq	-56(%rsp), %r10         # 8-byte Reload
	movq	-64(%rsp), %r15         # 8-byte Reload
	xorl	%r9d, %r9d
	movq	-72(%rsp), %rsi         # 8-byte Reload
	movq	%rax, -24(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB7_11:                               # %.preheader171.split.us
                                        #   Parent Loop BB7_8 Depth=1
                                        #     Parent Loop BB7_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	quant_coef8(%rsi), %ecx
	shll	$4, %ecx
	cmpw	$0, UseDefaultScalingMatrix8x8Flag(%rip)
	movq	%r10, %rax
	cmoveq	%r15, %rax
	movq	%r12, %rdi
	movswl	(%rax), %r12d
	movl	%ecx, %eax
	cltd
	idivl	%r12d
	movl	%eax, (%r14,%r9)
	movq	%r14, %r8
	movq	%r13, %r14
	movl	dequant_coef8(%rsi), %r13d
	imull	%r13d, %r12d
	movq	-24(%rsp), %rax         # 8-byte Reload
	movl	%r12d, (%rax,%r9)
	movq	%rdi, %r12
	movq	%r15, %rax
	subq	$-128, %rax
	cmpw	$0, UseDefaultScalingMatrix8x8Flag+2(%rip)
	cmovneq	%r11, %rax
	testl	%ebp, %ebp
	cmoveq	%r11, %rax
	movswl	(%rax), %ebx
	movl	%ecx, %eax
	cltd
	idivl	%ebx
	movl	%eax, (%r12,%r9)
	imull	%r13d, %ebx
	movq	%r14, %r13
	movq	%r8, %r14
	movl	%ebx, (%r13,%r9)
	addq	$4, %rsi
	addq	$4, %r9
	addq	$2, %r15
	addq	$2, %r10
	addq	$2, %r11
	cmpq	$32, %r9
	jne	.LBB7_11
	jmp	.LBB7_18
	.p2align	4, 0x90
.LBB7_16:                               # %.preheader171.split.preheader
                                        #   in Loop: Header=BB7_9 Depth=2
	xorl	%esi, %esi
	movq	%rax, %r8
	.p2align	4, 0x90
.LBB7_17:                               # %.preheader171.split
                                        #   Parent Loop BB7_8 Depth=1
                                        #     Parent Loop BB7_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-72(%rsp), %rdi         # 8-byte Reload
	movl	quant_coef8(%rdi,%rsi,2), %ecx
	shll	$4, %ecx
	movq	-56(%rsp), %rax         # 8-byte Reload
	movswl	(%rax,%rsi), %ebx
	movl	%ecx, %eax
	cltd
	idivl	%ebx
	movl	%eax, (%r14,%rsi,2)
	movl	dequant_coef8(%rdi,%rsi,2), %edi
	imull	%edi, %ebx
	movl	%ebx, (%r8,%rsi,2)
	cmpw	$0, UseDefaultScalingMatrix8x8Flag+2(%rip)
	movq	-48(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi), %rax
	movq	-64(%rsp), %rdx         # 8-byte Reload
	leaq	128(%rdx,%rsi), %rdx
	cmovneq	%rax, %rdx
	testl	%ebp, %ebp
	cmoveq	%rax, %rdx
	movswl	(%rdx), %ebx
	movl	%ecx, %eax
	cltd
	idivl	%ebx
	movl	%eax, (%r12,%rsi,2)
	imull	%edi, %ebx
	movl	%ebx, (%r13,%rsi,2)
	addq	$2, %rsi
	cmpq	$16, %rsi
	jne	.LBB7_17
.LBB7_18:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB7_9 Depth=2
	movq	-8(%rsp), %rdi          # 8-byte Reload
	incq	%rdi
	addq	$32, -72(%rsp)          # 8-byte Folded Spill
	addq	$16, -64(%rsp)          # 8-byte Folded Spill
	addq	$16, -56(%rsp)          # 8-byte Folded Spill
	addq	$16, -48(%rsp)          # 8-byte Folded Spill
	cmpq	$8, %rdi
	jne	.LBB7_9
# BB#19:                                # %.critedge
                                        #   in Loop: Header=BB7_8 Depth=1
	movq	-32(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rdx
	incq	%rdx
	movq	-16(%rsp), %rax         # 8-byte Reload
	addq	$256, %rax              # imm = 0x100
	movq	%rdx, %rcx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	cmpq	$6, %rdx
	jne	.LBB7_8
.LBB7_20:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_12:                               # %.preheader170
	movq	LevelScale8x8Luma(%rip), %rax
	movq	(%rax), %rcx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movq	InvLevelScale8x8Luma(%rip), %rax
	movq	(%rax), %rcx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB7_13:                               # %.preheader169
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_14 Depth 2
	movq	-56(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r15,8), %r12
	movq	-72(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r15,8), %r13
	movq	-48(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r15,8), %r11
	movq	-64(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r15,8), %r14
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	movq	%rcx, %rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB7_14:                               # %.preheader
                                        #   Parent Loop BB7_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rsi), %r8
	movq	(%r13,%rsi), %rbp
	movq	(%r11,%rsi), %rbx
	movq	(%r14,%rsi), %rax
	movl	quant_coef8(%rdi), %r9d
	movl	%r9d, (%r8)
	movl	dequant_coef8(%rdi), %r10d
	shll	$4, %r10d
	movl	%r10d, (%rbp)
	movl	%r9d, (%rbx)
	movl	%r10d, (%rax)
	movl	quant_coef8+4(%rdi), %ecx
	movl	%ecx, 4(%r8)
	movl	dequant_coef8+4(%rdi), %edx
	shll	$4, %edx
	movl	%edx, 4(%rbp)
	movl	%ecx, 4(%rbx)
	movl	%edx, 4(%rax)
	movl	quant_coef8+8(%rdi), %ecx
	movl	%ecx, 8(%r8)
	movl	dequant_coef8+8(%rdi), %edx
	shll	$4, %edx
	movl	%edx, 8(%rbp)
	movl	%ecx, 8(%rbx)
	movl	%edx, 8(%rax)
	movl	quant_coef8+12(%rdi), %ecx
	movl	%ecx, 12(%r8)
	movl	dequant_coef8+12(%rdi), %edx
	shll	$4, %edx
	movl	%edx, 12(%rbp)
	movl	%ecx, 12(%rbx)
	movl	%edx, 12(%rax)
	movl	quant_coef8+16(%rdi), %ecx
	movl	%ecx, 16(%r8)
	movl	dequant_coef8+16(%rdi), %edx
	shll	$4, %edx
	movl	%edx, 16(%rbp)
	movl	%ecx, 16(%rbx)
	movl	%edx, 16(%rax)
	movl	quant_coef8+20(%rdi), %ecx
	movl	%ecx, 20(%r8)
	movl	dequant_coef8+20(%rdi), %edx
	shll	$4, %edx
	movl	%edx, 20(%rbp)
	movl	%ecx, 20(%rbx)
	movl	%edx, 20(%rax)
	movl	quant_coef8+24(%rdi), %ecx
	movl	%ecx, 24(%r8)
	movl	dequant_coef8+24(%rdi), %edx
	shll	$4, %edx
	movl	%edx, 24(%rbp)
	movl	%ecx, 24(%rbx)
	movl	%edx, 24(%rax)
	movl	quant_coef8+28(%rdi), %ecx
	movl	%ecx, 28(%r8)
	movl	dequant_coef8+28(%rdi), %edx
	shll	$4, %edx
	movl	%edx, 28(%rbp)
	movl	%ecx, 28(%rbx)
	movl	%edx, 28(%rax)
	addq	$8, %rsi
	addq	$32, %rdi
	cmpq	$64, %rsi
	jne	.LBB7_14
# BB#15:                                #   in Loop: Header=BB7_13 Depth=1
	incq	%r15
	movq	-24(%rsp), %rcx         # 8-byte Reload
	addq	$256, %rcx              # imm = 0x100
	cmpq	$6, %r15
	jne	.LBB7_13
	jmp	.LBB7_20
.Lfunc_end7:
	.size	CalculateQuant8Param, .Lfunc_end7-CalculateQuant8Param
	.cfi_endproc

	.type	matrix4x4_check,@object # @matrix4x4_check
	.bss
	.globl	matrix4x4_check
	.p2align	4
matrix4x4_check:
	.zero	24
	.size	matrix4x4_check, 24

	.type	matrix8x8_check,@object # @matrix8x8_check
	.globl	matrix8x8_check
	.p2align	2
matrix8x8_check:
	.zero	8
	.size	matrix8x8_check, 8

	.type	MatrixType4x4,@object   # @MatrixType4x4
	.section	.rodata,"a",@progbits
	.p2align	4
MatrixType4x4:
	.asciz	"INTRA4X4_LUMA\000\000\000\000\000\000"
	.asciz	"INTRA4X4_CHROMAU\000\000\000"
	.asciz	"INTRA4X4_CHROMAV\000\000\000"
	.asciz	"INTER4X4_LUMA\000\000\000\000\000\000"
	.asciz	"INTER4X4_CHROMAU\000\000\000"
	.asciz	"INTER4X4_CHROMAV\000\000\000"
	.size	MatrixType4x4, 120

	.type	MatrixType8x8,@object   # @MatrixType8x8
	.p2align	4
MatrixType8x8:
	.asciz	"INTRA8X8_LUMA\000\000\000\000\000\000"
	.asciz	"INTER8X8_LUMA\000\000\000\000\000\000"
	.size	MatrixType8x8, 40

	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" Parsing error in config file: Parameter Name '%s' not recognized."
	.size	.L.str, 67

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"="
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" Parsing error in config file: '=' expected as the second token in each item."
	.size	.L.str.2, 78

	.type	ScalingList4x4input,@object # @ScalingList4x4input
	.comm	ScalingList4x4input,192,16
	.type	ScalingList8x8input,@object # @ScalingList8x8input
	.comm	ScalingList8x8input,256,16
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%d"
	.size	.L.str.3, 3

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	" Parsing error: Expected numerical value for Parameter of %s, found '%s'."
	.size	.L.str.4, 74

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n%s value exceed range. (Value must be 1 to 255)\n"
	.size	.L.str.6, 50

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Setting default values for this matrix."
	.size	.L.str.7, 40

	.type	Quant_inter_default,@object # @Quant_inter_default
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
Quant_inter_default:
	.short	10                      # 0xa
	.short	14                      # 0xe
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	14                      # 0xe
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	27                      # 0x1b
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	27                      # 0x1b
	.short	30                      # 0x1e
	.short	24                      # 0x18
	.short	27                      # 0x1b
	.short	30                      # 0x1e
	.short	34                      # 0x22
	.size	Quant_inter_default, 32

	.type	Quant_intra_default,@object # @Quant_intra_default
	.p2align	4
Quant_intra_default:
	.short	6                       # 0x6
	.short	13                      # 0xd
	.short	20                      # 0x14
	.short	28                      # 0x1c
	.short	13                      # 0xd
	.short	20                      # 0x14
	.short	28                      # 0x1c
	.short	32                      # 0x20
	.short	20                      # 0x14
	.short	28                      # 0x1c
	.short	32                      # 0x20
	.short	37                      # 0x25
	.short	28                      # 0x1c
	.short	32                      # 0x20
	.short	37                      # 0x25
	.short	42                      # 0x2a
	.size	Quant_intra_default, 32

	.type	.L.str.8,@object        # @.str.8
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.8:
	.asciz	"\n%s matrix definition not found. Setting default values."
	.size	.L.str.8, 57

	.type	Quant8_inter_default,@object # @Quant8_inter_default
	.section	.rodata,"a",@progbits
	.p2align	4
Quant8_inter_default:
	.short	9                       # 0x9
	.short	13                      # 0xd
	.short	15                      # 0xf
	.short	17                      # 0x11
	.short	19                      # 0x13
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	24                      # 0x18
	.short	13                      # 0xd
	.short	13                      # 0xd
	.short	17                      # 0x11
	.short	19                      # 0x13
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	15                      # 0xf
	.short	17                      # 0x11
	.short	19                      # 0x13
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	27                      # 0x1b
	.short	17                      # 0x11
	.short	19                      # 0x13
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	19                      # 0x13
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	30                      # 0x1e
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	30                      # 0x1e
	.short	32                      # 0x20
	.short	22                      # 0x16
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	30                      # 0x1e
	.short	32                      # 0x20
	.short	33                      # 0x21
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	30                      # 0x1e
	.short	32                      # 0x20
	.short	33                      # 0x21
	.short	35                      # 0x23
	.size	Quant8_inter_default, 128

	.type	Quant8_intra_default,@object # @Quant8_intra_default
	.p2align	4
Quant8_intra_default:
	.short	6                       # 0x6
	.short	10                      # 0xa
	.short	13                      # 0xd
	.short	16                      # 0x10
	.short	18                      # 0x12
	.short	23                      # 0x17
	.short	25                      # 0x19
	.short	27                      # 0x1b
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	16                      # 0x10
	.short	18                      # 0x12
	.short	23                      # 0x17
	.short	25                      # 0x19
	.short	27                      # 0x1b
	.short	29                      # 0x1d
	.short	13                      # 0xd
	.short	16                      # 0x10
	.short	18                      # 0x12
	.short	23                      # 0x17
	.short	25                      # 0x19
	.short	27                      # 0x1b
	.short	29                      # 0x1d
	.short	31                      # 0x1f
	.short	16                      # 0x10
	.short	18                      # 0x12
	.short	23                      # 0x17
	.short	25                      # 0x19
	.short	27                      # 0x1b
	.short	29                      # 0x1d
	.short	31                      # 0x1f
	.short	33                      # 0x21
	.short	18                      # 0x12
	.short	23                      # 0x17
	.short	25                      # 0x19
	.short	27                      # 0x1b
	.short	29                      # 0x1d
	.short	31                      # 0x1f
	.short	33                      # 0x21
	.short	36                      # 0x24
	.short	23                      # 0x17
	.short	25                      # 0x19
	.short	27                      # 0x1b
	.short	29                      # 0x1d
	.short	31                      # 0x1f
	.short	33                      # 0x21
	.short	36                      # 0x24
	.short	38                      # 0x26
	.short	25                      # 0x19
	.short	27                      # 0x1b
	.short	29                      # 0x1d
	.short	31                      # 0x1f
	.short	33                      # 0x21
	.short	36                      # 0x24
	.short	38                      # 0x26
	.short	40                      # 0x28
	.short	27                      # 0x1b
	.short	29                      # 0x1d
	.short	31                      # 0x1f
	.short	33                      # 0x21
	.short	36                      # 0x24
	.short	38                      # 0x26
	.short	40                      # 0x28
	.short	42                      # 0x2a
	.size	Quant8_intra_default, 128

	.type	qp_per_matrix,@object   # @qp_per_matrix
	.comm	qp_per_matrix,8,8
	.type	.L.str.9,@object        # @.str.9
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.9:
	.asciz	"init_global_buffers: qp_per_matrix"
	.size	.L.str.9, 35

	.type	qp_rem_matrix,@object   # @qp_rem_matrix
	.comm	qp_rem_matrix,8,8
	.type	LevelScale4x4Luma,@object # @LevelScale4x4Luma
	.comm	LevelScale4x4Luma,8,8
	.type	LevelScale4x4Chroma,@object # @LevelScale4x4Chroma
	.comm	LevelScale4x4Chroma,8,8
	.type	LevelScale8x8Luma,@object # @LevelScale8x8Luma
	.comm	LevelScale8x8Luma,8,8
	.type	InvLevelScale4x4Luma,@object # @InvLevelScale4x4Luma
	.comm	InvLevelScale4x4Luma,8,8
	.type	InvLevelScale4x4Chroma,@object # @InvLevelScale4x4Chroma
	.comm	InvLevelScale4x4Chroma,8,8
	.type	InvLevelScale8x8Luma,@object # @InvLevelScale8x8Luma
	.comm	InvLevelScale8x8Luma,8,8
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Parsing QMatrix file %s "
	.size	.L.str.10, 25

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\nError: %s\nProceeding with default values for all matrices."
	.size	.L.str.11, 60

	.type	UseDefaultScalingMatrix4x4Flag,@object # @UseDefaultScalingMatrix4x4Flag
	.comm	UseDefaultScalingMatrix4x4Flag,12,2
	.type	UseDefaultScalingMatrix8x8Flag,@object # @UseDefaultScalingMatrix8x8Flag
	.comm	UseDefaultScalingMatrix8x8Flag,4,2
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	ScalingList4x4,@object  # @ScalingList4x4
	.comm	ScalingList4x4,192,16
	.type	ScalingList8x8,@object  # @ScalingList8x8
	.comm	ScalingList8x8,256,16
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
