	.text
	.file	"fftmisc.bc"
	.globl	IsPowerOfTwo
	.p2align	4, 0x90
	.type	IsPowerOfTwo,@function
IsPowerOfTwo:                           # @IsPowerOfTwo
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	xorl	%eax, %eax
	cmpl	$2, %edi
	jb	.LBB0_2
# BB#1:
	leal	-1(%rdi), %ecx
	xorl	%eax, %eax
	testl	%edi, %ecx
	sete	%al
.LBB0_2:
	retq
.Lfunc_end0:
	.size	IsPowerOfTwo, .Lfunc_end0-IsPowerOfTwo
	.cfi_endproc

	.globl	NumberOfBitsNeeded
	.p2align	4, 0x90
	.type	NumberOfBitsNeeded,@function
NumberOfBitsNeeded:                     # @NumberOfBitsNeeded
	.cfi_startproc
# BB#0:
	movl	%edi, %ecx
	cmpl	$2, %ecx
	jb	.LBB1_4
# BB#1:                                 # %.preheader.preheader
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	btl	%eax, %ecx
	jae	.LBB1_2
# BB#3:
	retq
.LBB1_4:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	NumberOfBitsNeeded, .Lfunc_end1-NumberOfBitsNeeded
	.cfi_endproc

	.globl	ReverseBits
	.p2align	4, 0x90
	.type	ReverseBits,@function
ReverseBits:                            # @ReverseBits
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%esi, %esi
	je	.LBB2_1
# BB#2:                                 # %.lr.ph.preheader
	leal	-1(%rsi), %r8d
	movl	%esi, %r9d
	xorl	%eax, %eax
	xorl	%edx, %edx
	andl	$3, %r9d
	je	.LBB2_4
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, %ecx
	andl	$1, %ecx
	leal	(%rcx,%rax,2), %eax
	shrl	%edi
	incl	%edx
	cmpl	%edx, %r9d
	jne	.LBB2_3
.LBB2_4:                                # %.lr.ph.prol.loopexit
	cmpl	$3, %r8d
	jb	.LBB2_7
# BB#5:                                 # %.lr.ph.preheader.new
	subl	%edx, %esi
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, %ecx
	andl	$1, %ecx
	leal	(%rcx,%rax,2), %eax
	movl	%edi, %ecx
	andl	$2, %ecx
	leal	(%rcx,%rax,4), %eax
	movl	%edi, %ecx
	shrl	$2, %ecx
	andl	$1, %ecx
	orl	%eax, %ecx
	movl	%edi, %eax
	shrl	$3, %eax
	andl	$1, %eax
	leal	(%rax,%rcx,2), %eax
	shrl	$4, %edi
	addl	$-4, %esi
	jne	.LBB2_6
.LBB2_7:                                # %._crit_edge
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB2_1:
	xorl	%eax, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end2:
	.size	ReverseBits, .Lfunc_end2-ReverseBits
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	Index_to_frequency
	.p2align	4, 0x90
	.type	Index_to_frequency,@function
Index_to_frequency:                     # @Index_to_frequency
	.cfi_startproc
# BB#0:
	xorpd	%xmm0, %xmm0
	movl	%edi, %eax
	subl	%esi, %eax
	jbe	.LBB3_5
# BB#1:
	movl	%edi, %ecx
	shrl	%ecx
	cmpl	%esi, %ecx
	jae	.LBB3_2
# BB#3:
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	xorpd	.LCPI3_0(%rip), %xmm0
	jmp	.LBB3_4
.LBB3_2:
	movl	%esi, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.LBB3_4:
	movl	%edi, %eax
	cvtsi2sdq	%rax, %xmm1
	divsd	%xmm1, %xmm0
.LBB3_5:
	retq
.Lfunc_end3:
	.size	Index_to_frequency, .Lfunc_end3-Index_to_frequency
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	">>> Error in fftmisc.c: argument %d to NumberOfBitsNeeded is too small.\n"
	.size	.L.str, 73


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
