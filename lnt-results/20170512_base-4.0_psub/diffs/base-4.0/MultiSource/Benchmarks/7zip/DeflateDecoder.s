	.text
	.file	"DeflateDecoder.bc"
	.globl	_ZN9NCompress8NDeflate8NDecoder6CCoderC2Ebb
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoderC2Ebb,@function
_ZN9NCompress8NDeflate8NDecoder6CCoderC2Ebb: # @_ZN9NCompress8NDeflate8NDecoder6CCoderC2Ebb
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$0, 40(%rbx)
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+264, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+328, 32(%rbx)
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movl	$0, 56(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 88(%rbx)
	leaq	112(%rbx), %rdi
.Ltmp0:
	callq	_ZN9CInBufferC1Ev
.Ltmp1:
# BB#1:                                 # %_ZN5NBitl8CDecoderI9CInBufferEC2Ev.exit
	movb	%r14b, 3456(%rbx)
	movb	%bpl, 3457(%rbx)
	movb	$0, 3458(%rbx)
	movb	$1, 3459(%rbx)
	movb	$0, 3469(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_2:
.Ltmp2:
	movq	%rax, %rbp
.Ltmp3:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer4FreeEv
.Ltmp4:
# BB#3:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp9:
	callq	*16(%rax)
.Ltmp10:
.LBB0_5:                                # %_ZN10COutBufferD2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB0_9:
.Ltmp11:
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	__clang_call_terminate
.LBB0_6:
.Ltmp5:
	movq	%rax, %rbp
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_10
# BB#7:
	movq	(%rdi), %rax
.Ltmp6:
	callq	*16(%rax)
.Ltmp7:
.LBB0_10:                               # %.body
	movq	%rbp, %rdi
	callq	__clang_call_terminate
.LBB0_8:
.Ltmp8:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoderC2Ebb, .Lfunc_end0-_ZN9NCompress8NDeflate8NDecoder6CCoderC2Ebb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp6-.Ltmp10          #   Call between .Ltmp10 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi: # @_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	104(%rbx), %edx
	cmpl	$7, %edx
	jbe	.LBB2_1
# BB#2:                                 # %.lr.ph.i.i
	leaq	112(%rbx), %rbp
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB2_6
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=1
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB2_7
# BB#5:                                 # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	(%rbp), %rax
.LBB2_6:                                # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i
                                        #   in Loop: Header=BB2_3 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	(%rax), %eax
	jmp	.LBB2_8
	.p2align	4, 0x90
.LBB2_7:                                # %_ZN9CInBuffer8ReadByteERh.exit.i.i
                                        #   in Loop: Header=BB2_3 Depth=1
	incl	160(%rbx)
	movb	$-1, %al
.LBB2_8:                                #   in Loop: Header=BB2_3 Depth=1
	movzbl	%al, %eax
	movl	104(%rbx), %edx
	movl	$32, %ecx
	subl	%edx, %ecx
	movl	%eax, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	164(%rbx), %esi
	movl	%esi, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rax), %eax
	orl	%ecx, %eax
	movl	%eax, 108(%rbx)
	addl	$-8, %edx
	movl	%edx, 104(%rbx)
	cmpl	$7, %edx
	ja	.LBB2_3
	jmp	.LBB2_9
.LBB2_1:                                # %._ZN5NBitl8CDecoderI9CInBufferE9NormalizeEv.exit_crit_edge.i
	movl	164(%rbx), %esi
.LBB2_9:                                # %_ZN5NBitl8CDecoderI9CInBufferE8ReadBitsEj.exit
	movl	$1, %eax
	movl	%r14d, %ecx
	shll	%cl, %eax
	decl	%eax
	andl	%esi, %eax
	addl	%r14d, %edx
	movl	%edx, 104(%rbx)
	shrl	%cl, %esi
	movl	%esi, 164(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi, .Lfunc_end2-_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NDecoder6CCoder16DeCodeLevelTableEPhi
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder16DeCodeLevelTableEPhi,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder16DeCodeLevelTableEPhi: # @_ZN9NCompress8NDeflate8NDecoder6CCoder16DeCodeLevelTableEPhi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 80
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %rbx
	leaq	112(%rbx), %rbp
	movl	%edx, 20(%rsp)          # 4-byte Spill
	movslq	%edx, %r12
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_2 Depth 2
                                        #     Child Loop BB3_12 Depth 2
                                        #     Child Loop BB3_47 Depth 2
                                        #     Child Loop BB3_22 Depth 2
                                        #     Child Loop BB3_39 Depth 2
                                        #     Child Loop BB3_56 Depth 2
	movl	104(%rbx), %eax
	cmpl	$8, %eax
	jb	.LBB3_8
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.i.i
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=2
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB3_6
# BB#4:                                 # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB3_2 Depth=2
	movq	(%rbp), %rax
.LBB3_5:                                # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i
                                        #   in Loop: Header=BB3_2 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	(%rax), %eax
	jmp	.LBB3_7
	.p2align	4, 0x90
.LBB3_6:                                # %_ZN9CInBuffer8ReadByteERh.exit.i.i
                                        #   in Loop: Header=BB3_2 Depth=2
	incl	160(%rbx)
	movb	$-1, %al
.LBB3_7:                                #   in Loop: Header=BB3_2 Depth=2
	movzbl	%al, %edx
	movl	104(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%edx, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	%esi, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rdx), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%rbx)
	addl	$-8, %eax
	movl	%eax, 104(%rbx)
	cmpl	$7, %eax
	ja	.LBB3_2
	jmp	.LBB3_9
	.p2align	4, 0x90
.LBB3_8:                                # %._ZN5NBitl8CDecoderI9CInBufferE9NormalizeEv.exit_crit_edge.i
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	108(%rbx), %esi
.LBB3_9:                                # %_ZN5NBitl8CDecoderI9CInBufferE8GetValueEj.exit
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	$8, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	shrl	$9, %esi
	andl	$32767, %esi            # imm = 0x7FFF
	cmpl	2764(%rbx), %esi
	jae	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_1 Depth=1
	movl	%esi, %ecx
	shrl	$6, %ecx
	movzbl	2932(%rbx,%rcx), %ecx
	jmp	.LBB3_14
	.p2align	4, 0x90
.LBB3_11:                               # %.preheader.i.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	$692, %ecx              # imm = 0x2B4
	.p2align	4, 0x90
.LBB3_12:                               # %.preheader.i
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	(%rbx,%rcx,4), %esi
	leaq	1(%rcx), %rcx
	jae	.LBB3_12
# BB#13:                                # %.loopexit.loopexit.i
                                        #   in Loop: Header=BB3_1 Depth=1
	addl	$-683, %ecx             # imm = 0xFD55
.LBB3_14:                               # %.loopexit.i
                                        #   in Loop: Header=BB3_1 Depth=1
	addl	%ecx, %eax
	movl	%eax, 104(%rbx)
	movl	164(%rbx), %edx
	shrl	%cl, %edx
	movl	%edx, 164(%rbx)
	movslq	%ecx, %rdi
	subl	2724(%rbx,%rdi,4), %esi
	movl	$15, %ecx
	subl	%edi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	addl	2792(%rbx,%rdi,4), %esi
	cmpl	$18, %esi
	ja	.LBB3_62
# BB#15:                                # %_ZN9NCompress8NHuffman8CDecoderILi15ELj19EE12DecodeSymbolIN5NBitl8CDecoderI9CInBufferEEEEjPT_.exit
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	%esi, %ecx
	movl	2856(%rbx,%rcx,4), %ecx
	cmpl	$15, %ecx
	ja	.LBB3_17
# BB#16:                                #   in Loop: Header=BB3_1 Depth=1
	movslq	%r13d, %rax
	incl	%r13d
	movq	8(%rsp), %rdx           # 8-byte Reload
	movb	%cl, (%rdx,%rax)
	cmpl	20(%rsp), %r13d         # 4-byte Folded Reload
	jl	.LBB3_1
	jmp	.LBB3_59
	.p2align	4, 0x90
.LBB3_17:                               #   in Loop: Header=BB3_1 Depth=1
	cmpl	$18, %ecx
	ja	.LBB3_62
# BB#18:                                #   in Loop: Header=BB3_1 Depth=1
	cmpl	$17, %ecx
	je	.LBB3_38
# BB#19:                                #   in Loop: Header=BB3_1 Depth=1
	cmpl	$16, %ecx
	jne	.LBB3_46
# BB#20:                                #   in Loop: Header=BB3_1 Depth=1
	testl	%r13d, %r13d
	je	.LBB3_62
# BB#21:                                #   in Loop: Header=BB3_1 Depth=1
	cmpl	$8, %eax
	jb	.LBB3_28
	.p2align	4, 0x90
.LBB3_22:                               # %.lr.ph.i.i.i
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB3_25
# BB#23:                                #   in Loop: Header=BB3_22 Depth=2
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB3_26
# BB#24:                                # %._crit_edge.i.i.i.i
                                        #   in Loop: Header=BB3_22 Depth=2
	movq	(%rbp), %rax
.LBB3_25:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i
                                        #   in Loop: Header=BB3_22 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	(%rax), %eax
	jmp	.LBB3_27
	.p2align	4, 0x90
.LBB3_26:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i
                                        #   in Loop: Header=BB3_22 Depth=2
	incl	160(%rbx)
	movb	$-1, %al
.LBB3_27:                               #   in Loop: Header=BB3_22 Depth=2
	movzbl	%al, %esi
	movl	104(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	164(%rbx), %edx
	movl	%edx, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%rbx)
	addl	$-8, %eax
	movl	%eax, 104(%rbx)
	cmpl	$7, %eax
	ja	.LBB3_22
.LBB3_28:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi.exit
                                        #   in Loop: Header=BB3_1 Depth=1
	addl	$2, %eax
	movl	%eax, 104(%rbx)
	movl	%edx, %eax
	shrl	$2, %eax
	movl	%eax, 164(%rbx)
	cmpl	20(%rsp), %r13d         # 4-byte Folded Reload
	jge	.LBB3_58
# BB#29:                                # %.lr.ph
                                        #   in Loop: Header=BB3_1 Depth=1
	movslq	%r13d, %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movb	-1(%rsi,%rax), %cl
	movb	%cl, (%rsi,%rax)
	leaq	1(%rax), %r13
	cmpq	%r12, %r13
	jge	.LBB3_58
# BB#30:                                # %.lr.ph.1
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	movb	%cl, 1(%rsi,%rax)
	leaq	2(%rax), %r13
	cmpq	%r12, %r13
	jge	.LBB3_58
# BB#31:                                # %.lr.ph.2
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	movb	%cl, 2(%rsi,%rax)
	leaq	3(%rax), %r13
	cmpq	%r12, %r13
	jge	.LBB3_58
# BB#32:                                # %.lr.ph.2
                                        #   in Loop: Header=BB3_1 Depth=1
	andl	$3, %edx
	je	.LBB3_58
# BB#33:                                # %.lr.ph.3
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	movb	%cl, 3(%rsi,%rax)
	leaq	4(%rax), %r13
	cmpq	%r12, %r13
	jge	.LBB3_58
# BB#34:                                # %.lr.ph.3
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpl	$2, %edx
	jb	.LBB3_58
# BB#35:                                # %.lr.ph.4
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	movb	%cl, 4(%rsi,%rax)
	leaq	5(%rax), %r13
	cmpq	%r12, %r13
	jge	.LBB3_58
# BB#36:                                # %.lr.ph.4
                                        #   in Loop: Header=BB3_1 Depth=1
	decl	%edx
	cmpl	$2, %edx
	jl	.LBB3_58
# BB#37:                                # %.lr.ph.5
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movb	%cl, 5(%rdx,%rax)
	addq	$6, %rax
	movq	%rax, %r13
	cmpl	20(%rsp), %r13d         # 4-byte Folded Reload
	jl	.LBB3_1
	jmp	.LBB3_59
.LBB3_38:                               #   in Loop: Header=BB3_1 Depth=1
	cmpl	$8, %eax
	jb	.LBB3_45
	.p2align	4, 0x90
.LBB3_39:                               # %.lr.ph.i.i.i44
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB3_42
# BB#40:                                #   in Loop: Header=BB3_39 Depth=2
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB3_43
# BB#41:                                # %._crit_edge.i.i.i.i46
                                        #   in Loop: Header=BB3_39 Depth=2
	movq	(%rbp), %rax
.LBB3_42:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i47
                                        #   in Loop: Header=BB3_39 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	(%rax), %eax
	jmp	.LBB3_44
	.p2align	4, 0x90
.LBB3_43:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i48
                                        #   in Loop: Header=BB3_39 Depth=2
	incl	160(%rbx)
	movb	$-1, %al
.LBB3_44:                               #   in Loop: Header=BB3_39 Depth=2
	movzbl	%al, %esi
	movl	104(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	164(%rbx), %edx
	movl	%edx, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%rbx)
	addl	$-8, %eax
	movl	%eax, 104(%rbx)
	cmpl	$7, %eax
	ja	.LBB3_39
.LBB3_45:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi.exit51
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	%edx, %ecx
	andl	$7, %ecx
	addl	$3, %eax
	movl	%eax, 104(%rbx)
	shrl	$3, %edx
	movl	$3, %eax
	jmp	.LBB3_54
.LBB3_46:                               #   in Loop: Header=BB3_1 Depth=1
	cmpl	$8, %eax
	jb	.LBB3_53
	.p2align	4, 0x90
.LBB3_47:                               # %.lr.ph.i.i.i55
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB3_50
# BB#48:                                #   in Loop: Header=BB3_47 Depth=2
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB3_51
# BB#49:                                # %._crit_edge.i.i.i.i57
                                        #   in Loop: Header=BB3_47 Depth=2
	movq	(%rbp), %rax
.LBB3_50:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i58
                                        #   in Loop: Header=BB3_47 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	(%rax), %eax
	jmp	.LBB3_52
	.p2align	4, 0x90
.LBB3_51:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i59
                                        #   in Loop: Header=BB3_47 Depth=2
	incl	160(%rbx)
	movb	$-1, %al
.LBB3_52:                               #   in Loop: Header=BB3_47 Depth=2
	movzbl	%al, %esi
	movl	104(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	164(%rbx), %edx
	movl	%edx, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%rbx)
	addl	$-8, %eax
	movl	%eax, 104(%rbx)
	cmpl	$7, %eax
	ja	.LBB3_47
.LBB3_53:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi.exit62
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	%edx, %ecx
	andl	$127, %ecx
	addl	$7, %eax
	movl	%eax, 104(%rbx)
	shrl	$7, %edx
	movl	$11, %eax
.LBB3_54:                               # %.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	%edx, 164(%rbx)
	cmpl	20(%rsp), %r13d         # 4-byte Folded Reload
	jge	.LBB3_58
# BB#55:                                # %.lr.ph79.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	leal	(%rcx,%rax), %r15d
	movslq	%r13d, %r14
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	(%rdx,%r14), %rdi
	movq	%r14, %rsi
	subq	%r12, %rsi
	leal	-1(%rcx,%rax), %edx
	notq	%rdx
	cmpq	%rdx, %rsi
	cmovaq	%rsi, %rdx
	negq	%rdx
	xorl	%esi, %esi
	callq	memset
	incq	%r14
	.p2align	4, 0x90
.LBB3_56:                               # %.lr.ph79
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%r13d
	cmpq	%r12, %r14
	jge	.LBB3_58
# BB#57:                                # %.lr.ph79
                                        #   in Loop: Header=BB3_56 Depth=2
	incq	%r14
	cmpl	$1, %r15d
	leal	-1(%r15), %eax
	movl	%eax, %r15d
	jg	.LBB3_56
	.p2align	4, 0x90
.LBB3_58:                               # %.loopexit
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpl	20(%rsp), %r13d         # 4-byte Folded Reload
	jl	.LBB3_1
.LBB3_59:
	movb	$1, %al
	jmp	.LBB3_63
.LBB3_62:
	xorl	%eax, %eax
.LBB3_63:                               # %.critedge
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder16DeCodeLevelTableEPhi, .Lfunc_end3-_ZN9NCompress8NDeflate8NDecoder6CCoder16DeCodeLevelTableEPhi
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.zero	16,8
.LCPI4_1:
	.zero	16,9
.LCPI4_2:
	.zero	16,7
.LCPI4_3:
	.zero	16,5
	.text
	.globl	_ZN9NCompress8NDeflate8NDecoder6CCoder10ReadTablesEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder10ReadTablesEv,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder10ReadTablesEv: # @_ZN9NCompress8NDeflate8NDecoder6CCoder10ReadTablesEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$680, %rsp              # imm = 0x2A8
.Lcfi34:
	.cfi_def_cfa_offset 736
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	104(%rbx), %edx
	cmpl	$7, %edx
	jbe	.LBB4_8
# BB#1:                                 # %.lr.ph.i.i.i
	leaq	112(%rbx), %r14
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB4_5
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB4_6
# BB#4:                                 # %._crit_edge.i.i.i.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%r14), %rax
.LBB4_5:                                # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i
                                        #   in Loop: Header=BB4_2 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB4_7
	.p2align	4, 0x90
.LBB4_6:                                # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i
                                        #   in Loop: Header=BB4_2 Depth=1
	incl	160(%rbx)
	movb	$-1, %al
.LBB4_7:                                #   in Loop: Header=BB4_2 Depth=1
	movzbl	%al, %esi
	movl	104(%rbx), %edx
	movl	$32, %ecx
	subl	%edx, %ecx
	movl	%esi, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	164(%rbx), %eax
	movl	%eax, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%rbx)
	addl	$-8, %edx
	movl	%edx, 104(%rbx)
	cmpl	$7, %edx
	ja	.LBB4_2
	jmp	.LBB4_9
.LBB4_8:                                # %._ZN5NBitl8CDecoderI9CInBufferE9NormalizeEv.exit_crit_edge.i.i
	movl	164(%rbx), %eax
.LBB4_9:                                # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi.exit
	incl	%edx
	movl	%edx, 104(%rbx)
	movl	%eax, %ebp
	shrl	%ebp
	movl	%ebp, 164(%rbx)
	andb	$1, %al
	movb	%al, 3448(%rbx)
	cmpl	$7, %edx
	jbe	.LBB4_17
# BB#10:                                # %.lr.ph.i.i.i34
	leaq	112(%rbx), %r14
	.p2align	4, 0x90
.LBB4_11:                               # =>This Inner Loop Header: Depth=1
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB4_14
# BB#12:                                #   in Loop: Header=BB4_11 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB4_15
# BB#13:                                # %._crit_edge.i.i.i.i36
                                        #   in Loop: Header=BB4_11 Depth=1
	movq	(%r14), %rax
.LBB4_14:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i37
                                        #   in Loop: Header=BB4_11 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB4_16
	.p2align	4, 0x90
.LBB4_15:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i38
                                        #   in Loop: Header=BB4_11 Depth=1
	incl	160(%rbx)
	movb	$-1, %al
.LBB4_16:                               #   in Loop: Header=BB4_11 Depth=1
	movzbl	%al, %eax
	movl	104(%rbx), %edx
	movl	$32, %ecx
	subl	%edx, %ecx
	movl	%eax, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	orl	164(%rbx), %ebp
	movl	%ebp, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rax), %eax
	orl	%ecx, %eax
	movl	%eax, 108(%rbx)
	addl	$-8, %edx
	movl	%edx, 104(%rbx)
	cmpl	$7, %edx
	ja	.LBB4_11
.LBB4_17:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi.exit41
	movl	%ebp, %ecx
	andl	$3, %ecx
	leal	2(%rdx), %eax
	movl	%eax, 104(%rbx)
	shrl	$2, %ebp
	movl	%ebp, 164(%rbx)
	cmpl	$3, %ecx
	jne	.LBB4_20
.LBB4_18:
	xorl	%eax, %eax
.LBB4_19:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$680, %rsp              # imm = 0x2A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_20:
	testl	%ecx, %ecx
	je	.LBB4_25
# BB#21:
	movb	$0, 3449(%rbx)
	cmpl	$1, %ecx
	jne	.LBB4_42
# BB#22:
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8]
	movaps	%xmm0, 160(%rsp)
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	.LCPI4_1(%rip), %xmm0   # xmm0 = [9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9]
	movaps	%xmm0, 272(%rsp)
	movaps	%xmm0, 256(%rsp)
	movaps	%xmm0, 240(%rsp)
	movaps	%xmm0, 224(%rsp)
	movaps	%xmm0, 208(%rsp)
	movaps	%xmm0, 192(%rsp)
	movaps	%xmm0, 176(%rsp)
	movabsq	$506381209866536711, %rax # imm = 0x707070707070707
	movq	%rax, 304(%rsp)
	movaps	.LCPI4_2(%rip), %xmm0   # xmm0 = [7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7]
	movaps	%xmm0, 288(%rsp)
	movabsq	$578721382704613384, %rax # imm = 0x808080808080808
	movq	%rax, 312(%rsp)
	movaps	.LCPI4_3(%rip), %xmm0   # xmm0 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	movaps	%xmm0, 336(%rsp)
	movaps	%xmm0, 320(%rsp)
	movzbl	3457(%rbx), %eax
	leal	30(%rax,%rax), %eax
	movl	%eax, 3452(%rbx)
.LBB4_23:
	leaq	168(%rbx), %rdi
	leaq	32(%rsp), %rsi
	callq	_ZN9NCompress8NHuffman8CDecoderILi15ELj288EE14SetCodeLengthsEPKh
	testb	%al, %al
	je	.LBB4_18
# BB#24:
	addq	$1960, %rbx             # imm = 0x7A8
	leaq	320(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NHuffman8CDecoderILi15ELj32EE14SetCodeLengthsEPKh
                                        # kill: %AL<def> %AL<kill> %EAX<def>
	jmp	.LBB4_19
.LBB4_25:
	movb	$1, 3449(%rbx)
	movl	$-2, %ecx
	subl	%edx, %ecx
	andl	$7, %ecx
	addl	%ecx, %eax
	movl	%eax, 104(%rbx)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	movl	%ebp, 164(%rbx)
	cmpl	$8, %eax
	jb	.LBB4_33
# BB#26:                                # %.lr.ph.i.i.i45
	leaq	112(%rbx), %r14
	.p2align	4, 0x90
.LBB4_27:                               # =>This Inner Loop Header: Depth=1
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB4_30
# BB#28:                                #   in Loop: Header=BB4_27 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB4_31
# BB#29:                                # %._crit_edge.i.i.i.i47
                                        #   in Loop: Header=BB4_27 Depth=1
	movq	(%r14), %rax
.LBB4_30:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i48
                                        #   in Loop: Header=BB4_27 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB4_32
	.p2align	4, 0x90
.LBB4_31:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i49
                                        #   in Loop: Header=BB4_27 Depth=1
	incl	160(%rbx)
	movb	$-1, %al
.LBB4_32:                               #   in Loop: Header=BB4_27 Depth=1
	movzbl	%al, %edx
	movl	104(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%edx, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	orl	164(%rbx), %ebp
	movl	%ebp, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rdx), %edx
	orl	%ecx, %edx
	movl	%edx, 108(%rbx)
	addl	$-8, %eax
	movl	%eax, 104(%rbx)
	cmpl	$7, %eax
	ja	.LBB4_27
.LBB4_33:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi.exit52
	movzwl	%bp, %r15d
	addl	$16, %eax
	movl	%eax, 104(%rbx)
	shrl	$16, %ebp
	movl	%ebp, 164(%rbx)
	movl	%r15d, 3444(%rbx)
	movb	$1, %al
	cmpb	$0, 3456(%rbx)
	jne	.LBB4_19
# BB#34:                                # %.lr.ph.i.i.i56
	leaq	112(%rbx), %r14
	.p2align	4, 0x90
.LBB4_35:                               # =>This Inner Loop Header: Depth=1
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB4_38
# BB#36:                                #   in Loop: Header=BB4_35 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB4_39
# BB#37:                                # %._crit_edge.i.i.i.i58
                                        #   in Loop: Header=BB4_35 Depth=1
	movq	(%r14), %rax
.LBB4_38:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i59
                                        #   in Loop: Header=BB4_35 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB4_40
	.p2align	4, 0x90
.LBB4_39:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i60
                                        #   in Loop: Header=BB4_35 Depth=1
	incl	160(%rbx)
	movb	$-1, %al
.LBB4_40:                               #   in Loop: Header=BB4_35 Depth=1
	movzbl	%al, %esi
	movl	104(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	164(%rbx), %edx
	movl	%edx, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%rbx)
	leal	-8(%rax), %ecx
	movl	%ecx, 104(%rbx)
	cmpl	$7, %ecx
	ja	.LBB4_35
# BB#41:                                # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi.exit63
	movl	%edx, %ecx
	notl	%ecx
	addl	$8, %eax
	movl	%eax, 104(%rbx)
	shrl	$16, %edx
	movl	%edx, 164(%rbx)
	movzwl	%cx, %eax
	cmpl	%eax, %r15d
	sete	%al
	jmp	.LBB4_19
.LBB4_42:
	cmpl	$7, %eax
	jbe	.LBB4_50
# BB#43:                                # %.lr.ph.i.i.i67
	leaq	112(%rbx), %r14
	.p2align	4, 0x90
.LBB4_44:                               # =>This Inner Loop Header: Depth=1
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB4_47
# BB#45:                                #   in Loop: Header=BB4_44 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB4_48
# BB#46:                                # %._crit_edge.i.i.i.i69
                                        #   in Loop: Header=BB4_44 Depth=1
	movq	(%r14), %rax
.LBB4_47:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i70
                                        #   in Loop: Header=BB4_44 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB4_49
	.p2align	4, 0x90
.LBB4_48:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i71
                                        #   in Loop: Header=BB4_44 Depth=1
	incl	160(%rbx)
	movb	$-1, %al
.LBB4_49:                               #   in Loop: Header=BB4_44 Depth=1
	movzbl	%al, %edx
	movl	104(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%edx, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	orl	164(%rbx), %ebp
	movl	%ebp, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rdx), %edx
	orl	%ecx, %edx
	movl	%edx, 108(%rbx)
	addl	$-8, %eax
	movl	%eax, 104(%rbx)
	cmpl	$7, %eax
	ja	.LBB4_44
.LBB4_50:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi.exit74
	addl	$5, %eax
	movl	%eax, 104(%rbx)
	movl	%ebp, %esi
	shrl	$5, %esi
	movl	%esi, 164(%rbx)
	cmpl	$7, %eax
	jbe	.LBB4_58
# BB#51:                                # %.lr.ph.i.i.i78
	leaq	112(%rbx), %r14
	.p2align	4, 0x90
.LBB4_52:                               # =>This Inner Loop Header: Depth=1
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB4_55
# BB#53:                                #   in Loop: Header=BB4_52 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB4_56
# BB#54:                                # %._crit_edge.i.i.i.i80
                                        #   in Loop: Header=BB4_52 Depth=1
	movq	(%r14), %rax
.LBB4_55:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i81
                                        #   in Loop: Header=BB4_52 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB4_57
	.p2align	4, 0x90
.LBB4_56:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i82
                                        #   in Loop: Header=BB4_52 Depth=1
	incl	160(%rbx)
	movb	$-1, %al
.LBB4_57:                               #   in Loop: Header=BB4_52 Depth=1
	movzbl	%al, %edx
	movl	104(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%edx, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	164(%rbx), %esi
	movl	%esi, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rdx), %edx
	orl	%ecx, %edx
	movl	%edx, 108(%rbx)
	addl	$-8, %eax
	movl	%eax, 104(%rbx)
	cmpl	$7, %eax
	ja	.LBB4_52
.LBB4_58:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi.exit85
	movl	%esi, %ecx
	andl	$31, %ecx
	addl	$5, %eax
	movl	%eax, 104(%rbx)
	shrl	$5, %esi
	movl	%esi, 164(%rbx)
	incl	%ecx
	movl	%ecx, 3452(%rbx)
	cmpl	$7, %eax
	jbe	.LBB4_66
# BB#59:                                # %.lr.ph.i.i.i89
	leaq	112(%rbx), %r14
	.p2align	4, 0x90
.LBB4_60:                               # =>This Inner Loop Header: Depth=1
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB4_63
# BB#61:                                #   in Loop: Header=BB4_60 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB4_64
# BB#62:                                # %._crit_edge.i.i.i.i91
                                        #   in Loop: Header=BB4_60 Depth=1
	movq	(%r14), %rax
.LBB4_63:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i92
                                        #   in Loop: Header=BB4_60 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB4_65
	.p2align	4, 0x90
.LBB4_64:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i93
                                        #   in Loop: Header=BB4_60 Depth=1
	incl	160(%rbx)
	movb	$-1, %al
.LBB4_65:                               #   in Loop: Header=BB4_60 Depth=1
	movzbl	%al, %edx
	movl	104(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%edx, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	164(%rbx), %esi
	movl	%esi, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rdx), %edx
	orl	%ecx, %edx
	movl	%edx, 108(%rbx)
	addl	$-8, %eax
	movl	%eax, 104(%rbx)
	cmpl	$7, %eax
	ja	.LBB4_60
.LBB4_66:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi.exit96
	addl	$4, %eax
	movl	%eax, 104(%rbx)
	movl	%esi, %edx
	shrl	$4, %edx
	movl	%edx, 164(%rbx)
	cmpb	$0, 3457(%rbx)
	jne	.LBB4_69
# BB#67:
	cmpl	$30, 3452(%rbx)
	ja	.LBB4_18
.LBB4_69:
	andl	$31, %ebp
	addl	$257, %ebp              # imm = 0x101
	andl	$15, %esi
	addl	$4, %esi
	leaq	112(%rbx), %r14
	movl	%esi, %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_70:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_72 Depth 2
	movzbl	_ZN9NCompress8NDeflateL24kCodeLengthAlphabetOrderE(%r12), %r13d
	cmpq	%r15, %r12
	jge	.LBB4_79
# BB#71:                                #   in Loop: Header=BB4_70 Depth=1
	cmpl	$8, %eax
	jb	.LBB4_78
	.p2align	4, 0x90
.LBB4_72:                               # %.lr.ph.i.i.i100
                                        #   Parent Loop BB4_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB4_75
# BB#73:                                #   in Loop: Header=BB4_72 Depth=2
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB4_76
# BB#74:                                # %._crit_edge.i.i.i.i102
                                        #   in Loop: Header=BB4_72 Depth=2
	movq	(%r14), %rax
.LBB4_75:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i103
                                        #   in Loop: Header=BB4_72 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB4_77
	.p2align	4, 0x90
.LBB4_76:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i104
                                        #   in Loop: Header=BB4_72 Depth=2
	incl	160(%rbx)
	movb	$-1, %al
.LBB4_77:                               #   in Loop: Header=BB4_72 Depth=2
	movzbl	%al, %esi
	movl	104(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	164(%rbx), %edx
	movl	%edx, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%rbx)
	addl	$-8, %eax
	movl	%eax, 104(%rbx)
	cmpl	$7, %eax
	ja	.LBB4_72
.LBB4_78:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadBitsEi.exit107
                                        #   in Loop: Header=BB4_70 Depth=1
	addl	$3, %eax
	movl	%eax, 104(%rbx)
	movb	%dl, %cl
	shrl	$3, %edx
	movl	%edx, 164(%rbx)
	andb	$7, %cl
	jmp	.LBB4_80
	.p2align	4, 0x90
.LBB4_79:                               #   in Loop: Header=BB4_70 Depth=1
	xorl	%ecx, %ecx
.LBB4_80:                               #   in Loop: Header=BB4_70 Depth=1
	movb	%cl, (%rsp,%r13)
	incq	%r12
	cmpq	$19, %r12
	jne	.LBB4_70
# BB#81:
	leaq	2728(%rbx), %rdi
	movq	%rsp, %rsi
	callq	_ZN9NCompress8NHuffman8CDecoderILi15ELj19EE14SetCodeLengthsEPKh
	testb	%al, %al
	je	.LBB4_18
# BB#82:
	movl	3452(%rbx), %edx
	addl	%ebp, %edx
	leaq	352(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoder16DeCodeLevelTableEPhi
	testb	%al, %al
	je	.LBB4_18
# BB#83:                                # %.thread111
	xorps	%xmm0, %xmm0
	movups	%xmm0, 336(%rsp)
	movups	%xmm0, 321(%rsp)
	movups	%xmm0, 305(%rsp)
	movups	%xmm0, 289(%rsp)
	movl	%ebp, %r14d
	leaq	32(%rsp), %rdi
	leaq	352(%rsp), %rsi
	movq	%r14, %rdx
	callq	memcpy
	leaq	320(%rsp), %rdi
	leaq	352(%rsp,%r14), %rsi
	movl	3452(%rbx), %edx
	callq	memcpy
	jmp	.LBB4_23
.Lfunc_end4:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder10ReadTablesEv, .Lfunc_end4-_ZN9NCompress8NDeflate8NDecoder6CCoder10ReadTablesEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NHuffman8CDecoderILi15ELj19EE14SetCodeLengthsEPKh,"axG",@progbits,_ZN9NCompress8NHuffman8CDecoderILi15ELj19EE14SetCodeLengthsEPKh,comdat
	.weak	_ZN9NCompress8NHuffman8CDecoderILi15ELj19EE14SetCodeLengthsEPKh
	.p2align	4, 0x90
	.type	_ZN9NCompress8NHuffman8CDecoderILi15ELj19EE14SetCodeLengthsEPKh,@function
_ZN9NCompress8NHuffman8CDecoderILi15ELj19EE14SetCodeLengthsEPKh: # @_ZN9NCompress8NHuffman8CDecoderILi15ELj19EE14SetCodeLengthsEPKh
	.cfi_startproc
# BB#0:                                 # %.preheader66.preheader
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi47:
	.cfi_def_cfa_offset 192
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rsp)
	movups	%xmm0, 36(%rsp)
	movups	%xmm0, 20(%rsp)
	movups	%xmm0, 4(%rsp)
	xorl	%eax, %eax
	jmp	.LBB5_2
	.p2align	4, 0x90
.LBB5_1:                                # %.critedge.1
                                        #   in Loop: Header=BB5_2 Depth=1
	incl	(%rsp,%rcx,4)
	movl	$-1, 132(%r15,%rax,4)
	addq	$2, %rax
.LBB5_2:                                # %.preheader66
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	cmpq	$15, %rcx
	ja	.LBB5_6
# BB#3:                                 # %.critedge
                                        #   in Loop: Header=BB5_2 Depth=1
	incl	(%rsp,%rcx,4)
	movl	$-1, 128(%r15,%rax,4)
	leal	1(%rax), %ecx
	cmpl	$19, %ecx
	jae	.LBB5_7
# BB#4:                                 # %.preheader66.1
                                        #   in Loop: Header=BB5_2 Depth=1
	movzbl	1(%r14,%rax), %ecx
	cmpq	$15, %rcx
	jbe	.LBB5_1
.LBB5_6:
	xorl	%eax, %eax
	jmp	.LBB5_24
.LBB5_7:
	movl	$0, (%rsp)
	movl	$0, (%r15)
	movl	$0, 64(%r15)
	xorl	%eax, %eax
	movl	$14, %r13d
	movl	$1, %ebx
	movl	$32768, %esi            # imm = 0x8000
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_8:                                # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edx
	movl	(%rsp,%rbx,4), %ebp
	movl	%r13d, %ecx
	shll	%cl, %ebp
	addl	%edx, %ebp
	cmpl	$32768, %ebp            # imm = 0x8000
	ja	.LBB5_22
# BB#9:                                 #   in Loop: Header=BB5_8 Depth=1
	cmpq	$15, %rbx
	movl	%ebp, %r12d
	cmovel	%esi, %r12d
	movl	%r12d, (%r15,%rbx,4)
	movl	-4(%rsp,%rbx,4), %ecx
	addl	60(%r15,%rbx,4), %ecx
	movl	%ecx, 64(%r15,%rbx,4)
	movl	%ecx, 64(%rsp,%rbx,4)
	cmpq	$9, %rbx
	jg	.LBB5_13
# BB#10:                                #   in Loop: Header=BB5_8 Depth=1
	shrl	$6, %r12d
	cmpl	%r12d, %eax
	jae	.LBB5_13
# BB#11:                                # %.lr.ph
                                        #   in Loop: Header=BB5_8 Depth=1
	movl	%eax, %eax
	leaq	204(%r15,%rax), %rdi
	movl	%r12d, %edx
	subq	%rax, %rdx
	movl	%ebx, %esi
	callq	memset
	movl	$32768, %esi            # imm = 0x8000
	jmp	.LBB5_14
	.p2align	4, 0x90
.LBB5_13:                               #   in Loop: Header=BB5_8 Depth=1
	movl	%eax, %r12d
.LBB5_14:                               # %.loopexit64
                                        #   in Loop: Header=BB5_8 Depth=1
	incq	%rbx
	decl	%r13d
	cmpq	$16, %rbx
	movl	%r12d, %eax
	jl	.LBB5_8
# BB#15:                                # %.preheader.preheader
	xorl	%eax, %eax
	jmp	.LBB5_17
	.p2align	4, 0x90
.LBB5_16:                               #   in Loop: Header=BB5_17 Depth=1
	addq	$2, %rax
.LBB5_17:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB5_19
# BB#18:                                #   in Loop: Header=BB5_17 Depth=1
	movl	64(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 64(%rsp,%rcx,4)
	movl	%eax, 128(%r15,%rdx,4)
.LBB5_19:                               #   in Loop: Header=BB5_17 Depth=1
	cmpq	$18, %rax
	je	.LBB5_23
# BB#20:                                # %.preheader.1
                                        #   in Loop: Header=BB5_17 Depth=1
	movzbl	1(%r14,%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB5_16
# BB#21:                                #   in Loop: Header=BB5_17 Depth=1
	movl	64(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 64(%rsp,%rcx,4)
	leal	1(%rax), %ecx
	movl	%ecx, 128(%r15,%rdx,4)
	jmp	.LBB5_16
.LBB5_22:
	xorl	%eax, %eax
	jmp	.LBB5_24
.LBB5_23:
	movb	$1, %al
.LBB5_24:                               # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN9NCompress8NHuffman8CDecoderILi15ELj19EE14SetCodeLengthsEPKh, .Lfunc_end5-_ZN9NCompress8NHuffman8CDecoderILi15ELj19EE14SetCodeLengthsEPKh
	.cfi_endproc

	.section	.text._ZN9NCompress8NHuffman8CDecoderILi15ELj288EE14SetCodeLengthsEPKh,"axG",@progbits,_ZN9NCompress8NHuffman8CDecoderILi15ELj288EE14SetCodeLengthsEPKh,comdat
	.weak	_ZN9NCompress8NHuffman8CDecoderILi15ELj288EE14SetCodeLengthsEPKh
	.p2align	4, 0x90
	.type	_ZN9NCompress8NHuffman8CDecoderILi15ELj288EE14SetCodeLengthsEPKh,@function
_ZN9NCompress8NHuffman8CDecoderILi15ELj288EE14SetCodeLengthsEPKh: # @_ZN9NCompress8NHuffman8CDecoderILi15ELj288EE14SetCodeLengthsEPKh
	.cfi_startproc
# BB#0:                                 # %.preheader66.preheader
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi60:
	.cfi_def_cfa_offset 192
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rsp)
	movups	%xmm0, 36(%rsp)
	movups	%xmm0, 20(%rsp)
	movups	%xmm0, 4(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_1:                                # %.preheader66
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	cmpq	$15, %rcx
	ja	.LBB6_19
# BB#2:                                 # %.critedge
                                        #   in Loop: Header=BB6_1 Depth=1
	incl	(%rsp,%rcx,4)
	movl	$-1, 128(%r15,%rax,4)
	movzbl	1(%r14,%rax), %ecx
	cmpq	$15, %rcx
	ja	.LBB6_19
# BB#3:                                 # %.critedge.1
                                        #   in Loop: Header=BB6_1 Depth=1
	incl	(%rsp,%rcx,4)
	movl	$-1, 132(%r15,%rax,4)
	addq	$2, %rax
	cmpl	$288, %eax              # imm = 0x120
	jb	.LBB6_1
# BB#4:
	movl	$0, (%rsp)
	movl	$0, (%r15)
	movl	$0, 64(%r15)
	xorl	%eax, %eax
	movl	$14, %r13d
	movl	$1, %ebx
	movl	$32768, %esi            # imm = 0x8000
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edx
	movl	(%rsp,%rbx,4), %ebp
	movl	%r13d, %ecx
	shll	%cl, %ebp
	addl	%edx, %ebp
	cmpl	$32768, %ebp            # imm = 0x8000
	ja	.LBB6_19
# BB#6:                                 #   in Loop: Header=BB6_5 Depth=1
	cmpq	$15, %rbx
	movl	%ebp, %r12d
	cmovel	%esi, %r12d
	movl	%r12d, (%r15,%rbx,4)
	movl	-4(%rsp,%rbx,4), %ecx
	addl	60(%r15,%rbx,4), %ecx
	movl	%ecx, 64(%r15,%rbx,4)
	movl	%ecx, 64(%rsp,%rbx,4)
	cmpq	$9, %rbx
	jg	.LBB6_10
# BB#7:                                 #   in Loop: Header=BB6_5 Depth=1
	shrl	$6, %r12d
	cmpl	%r12d, %eax
	jae	.LBB6_10
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB6_5 Depth=1
	movl	%eax, %eax
	leaq	1280(%r15,%rax), %rdi
	movl	%r12d, %edx
	subq	%rax, %rdx
	movl	%ebx, %esi
	callq	memset
	movl	$32768, %esi            # imm = 0x8000
	jmp	.LBB6_11
	.p2align	4, 0x90
.LBB6_10:                               #   in Loop: Header=BB6_5 Depth=1
	movl	%eax, %r12d
.LBB6_11:                               # %.loopexit64
                                        #   in Loop: Header=BB6_5 Depth=1
	incq	%rbx
	decl	%r13d
	cmpq	$16, %rbx
	movl	%r12d, %eax
	jl	.LBB6_5
# BB#12:                                # %.preheader.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_13:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB6_15
# BB#14:                                #   in Loop: Header=BB6_13 Depth=1
	movl	64(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 64(%rsp,%rcx,4)
	movl	%eax, 128(%r15,%rdx,4)
.LBB6_15:                               # %.preheader.194
                                        #   in Loop: Header=BB6_13 Depth=1
	movzbl	1(%r14,%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB6_17
# BB#16:                                #   in Loop: Header=BB6_13 Depth=1
	movl	64(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 64(%rsp,%rcx,4)
	leal	1(%rax), %ecx
	movl	%ecx, 128(%r15,%rdx,4)
.LBB6_17:                               #   in Loop: Header=BB6_13 Depth=1
	addq	$2, %rax
	cmpq	$288, %rax              # imm = 0x120
	jne	.LBB6_13
# BB#18:
	movb	$1, %al
	jmp	.LBB6_20
.LBB6_19:
	xorl	%eax, %eax
.LBB6_20:                               # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN9NCompress8NHuffman8CDecoderILi15ELj288EE14SetCodeLengthsEPKh, .Lfunc_end6-_ZN9NCompress8NHuffman8CDecoderILi15ELj288EE14SetCodeLengthsEPKh
	.cfi_endproc

	.section	.text._ZN9NCompress8NHuffman8CDecoderILi15ELj32EE14SetCodeLengthsEPKh,"axG",@progbits,_ZN9NCompress8NHuffman8CDecoderILi15ELj32EE14SetCodeLengthsEPKh,comdat
	.weak	_ZN9NCompress8NHuffman8CDecoderILi15ELj32EE14SetCodeLengthsEPKh
	.p2align	4, 0x90
	.type	_ZN9NCompress8NHuffman8CDecoderILi15ELj32EE14SetCodeLengthsEPKh,@function
_ZN9NCompress8NHuffman8CDecoderILi15ELj32EE14SetCodeLengthsEPKh: # @_ZN9NCompress8NHuffman8CDecoderILi15ELj32EE14SetCodeLengthsEPKh
	.cfi_startproc
# BB#0:                                 # %.preheader66.preheader
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi73:
	.cfi_def_cfa_offset 192
.Lcfi74:
	.cfi_offset %rbx, -56
.Lcfi75:
	.cfi_offset %r12, -48
.Lcfi76:
	.cfi_offset %r13, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rsp)
	movups	%xmm0, 36(%rsp)
	movups	%xmm0, 20(%rsp)
	movups	%xmm0, 4(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_1:                                # %.preheader66
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	cmpq	$15, %rcx
	ja	.LBB7_19
# BB#2:                                 # %.critedge
                                        #   in Loop: Header=BB7_1 Depth=1
	incl	(%rsp,%rcx,4)
	movl	$-1, 128(%r15,%rax,4)
	movzbl	1(%r14,%rax), %ecx
	cmpq	$15, %rcx
	ja	.LBB7_19
# BB#3:                                 # %.critedge.1
                                        #   in Loop: Header=BB7_1 Depth=1
	incl	(%rsp,%rcx,4)
	movl	$-1, 132(%r15,%rax,4)
	addq	$2, %rax
	cmpl	$32, %eax
	jb	.LBB7_1
# BB#4:
	movl	$0, (%rsp)
	movl	$0, (%r15)
	movl	$0, 64(%r15)
	xorl	%eax, %eax
	movl	$14, %r13d
	movl	$1, %ebx
	movl	$32768, %esi            # imm = 0x8000
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_5:                                # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edx
	movl	(%rsp,%rbx,4), %ebp
	movl	%r13d, %ecx
	shll	%cl, %ebp
	addl	%edx, %ebp
	cmpl	$32768, %ebp            # imm = 0x8000
	ja	.LBB7_19
# BB#6:                                 #   in Loop: Header=BB7_5 Depth=1
	cmpq	$15, %rbx
	movl	%ebp, %r12d
	cmovel	%esi, %r12d
	movl	%r12d, (%r15,%rbx,4)
	movl	-4(%rsp,%rbx,4), %ecx
	addl	60(%r15,%rbx,4), %ecx
	movl	%ecx, 64(%r15,%rbx,4)
	movl	%ecx, 64(%rsp,%rbx,4)
	cmpq	$9, %rbx
	jg	.LBB7_10
# BB#7:                                 #   in Loop: Header=BB7_5 Depth=1
	shrl	$6, %r12d
	cmpl	%r12d, %eax
	jae	.LBB7_10
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB7_5 Depth=1
	movl	%eax, %eax
	leaq	256(%r15,%rax), %rdi
	movl	%r12d, %edx
	subq	%rax, %rdx
	movl	%ebx, %esi
	callq	memset
	movl	$32768, %esi            # imm = 0x8000
	jmp	.LBB7_11
	.p2align	4, 0x90
.LBB7_10:                               #   in Loop: Header=BB7_5 Depth=1
	movl	%eax, %r12d
.LBB7_11:                               # %.loopexit64
                                        #   in Loop: Header=BB7_5 Depth=1
	incq	%rbx
	decl	%r13d
	cmpq	$16, %rbx
	movl	%r12d, %eax
	jl	.LBB7_5
# BB#12:                                # %.preheader.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_13:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB7_15
# BB#14:                                #   in Loop: Header=BB7_13 Depth=1
	movl	64(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 64(%rsp,%rcx,4)
	movl	%eax, 128(%r15,%rdx,4)
.LBB7_15:                               # %.preheader.194
                                        #   in Loop: Header=BB7_13 Depth=1
	movzbl	1(%r14,%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB7_17
# BB#16:                                #   in Loop: Header=BB7_13 Depth=1
	movl	64(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 64(%rsp,%rcx,4)
	leal	1(%rax), %ecx
	movl	%ecx, 128(%r15,%rdx,4)
.LBB7_17:                               #   in Loop: Header=BB7_13 Depth=1
	addq	$2, %rax
	cmpq	$32, %rax
	jne	.LBB7_13
# BB#18:
	movb	$1, %al
	jmp	.LBB7_20
.LBB7_19:
	xorl	%eax, %eax
.LBB7_20:                               # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN9NCompress8NHuffman8CDecoderILi15ELj32EE14SetCodeLengthsEPKh, .Lfunc_end7-_ZN9NCompress8NHuffman8CDecoderILi15ELj32EE14SetCodeLengthsEPKh
	.cfi_endproc

	.text
	.globl	_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeSpecEj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeSpecEj,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeSpecEj: # @_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeSpecEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi83:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi84:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi86:
	.cfi_def_cfa_offset 96
.Lcfi87:
	.cfi_offset %rbx, -56
.Lcfi88:
	.cfi_offset %r12, -48
.Lcfi89:
	.cfi_offset %r13, -40
.Lcfi90:
	.cfi_offset %r14, -32
.Lcfi91:
	.cfi_offset %r15, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r12
	movl	3460(%r12), %eax
	xorl	%esi, %esi
	cmpl	$-1, %eax
	je	.LBB8_129
# BB#1:
	cmpl	$-2, %eax
	jne	.LBB8_9
# BB#2:
	cmpb	$0, 3458(%r12)
	jne	.LBB8_4
# BB#3:
	cmpb	$0, 3457(%r12)
	leaq	48(%r12), %rdi
	movl	$65536, %eax            # imm = 0x10000
	movl	$32768, %esi            # imm = 0x8000
	cmovnel	%eax, %esi
	callq	_ZN10COutBuffer6CreateEj
	movl	$-2147024882, %esi      # imm = 0x8007000E
	testb	%al, %al
	je	.LBB8_129
.LBB8_4:
	movb	3459(%r12), %bl
	leaq	112(%r12), %r14
	movl	$131072, %esi           # imm = 0x20000
	movq	%r14, %rdi
	callq	_ZN9CInBuffer6CreateEj
	testb	%al, %al
	je	.LBB8_7
# BB#5:
	testb	%bl, %bl
	je	.LBB8_7
# BB#6:                                 # %_ZN9NCompress8NDeflate8NDecoder6CCoder12InitInStreamEb.exit.thread
	movq	%r14, %rdi
	callq	_ZN9CInBuffer4InitEv
	movl	$32, 104(%r12)
	movl	$0, 108(%r12)
	movl	$0, 160(%r12)
	movl	$0, 164(%r12)
	movb	$0, 3459(%r12)
	jmp	.LBB8_8
.LBB8_7:                                # %_ZN9NCompress8NDeflate8NDecoder6CCoder12InitInStreamEb.exit
	movl	$-2147024882, %esi      # imm = 0x8007000E
	testb	%al, %al
	je	.LBB8_129
.LBB8_8:
	leaq	48(%r12), %rdi
	movzbl	3458(%r12), %esi
	callq	_ZN12CLzOutWindow4InitEb
	movb	$0, 3448(%r12)
	movl	$0, 3460(%r12)
	movb	$1, 3468(%r12)
	xorl	%eax, %eax
.LBB8_9:
	testl	%ebp, %ebp
	movl	$0, %esi
	je	.LBB8_129
# BB#10:                                # %.preheader129
	testl	%eax, %eax
	jle	.LBB8_17
# BB#11:                                # %.lr.ph154
	leaq	48(%r12), %r14
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB8_12:                               # =>This Inner Loop Header: Depth=1
	decl	%eax
	movl	%eax, 3460(%r12)
	movl	56(%r12), %eax
	movl	%eax, %ecx
	subl	3464(%r12), %ecx
	leal	-1(%rcx), %edx
	movl	68(%r12), %esi
	cmpl	%esi, %edx
	cmovbl	%r15d, %esi
	leal	-1(%rsi,%rcx), %ecx
	movq	48(%r12), %rdx
	movzbl	(%rdx,%rcx), %ecx
	leal	1(%rax), %esi
	movl	%esi, 56(%r12)
	movb	%cl, (%rdx,%rax)
	movl	56(%r12), %eax
	cmpl	60(%r12), %eax
	jne	.LBB8_14
# BB#13:                                #   in Loop: Header=BB8_12 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB8_14:                               # %_ZN12CLzOutWindow7PutByteEh.exit
                                        #   in Loop: Header=BB8_12 Depth=1
	leal	-1(%rbp), %r13d
	cmpl	$1, %ebp
	je	.LBB8_16
# BB#15:                                # %_ZN12CLzOutWindow7PutByteEh.exit
                                        #   in Loop: Header=BB8_12 Depth=1
	movl	3460(%r12), %eax
	testl	%eax, %eax
	movl	%r13d, %ebp
	jg	.LBB8_12
.LBB8_16:                               # %.preheader127
	testl	%r13d, %r13d
	jne	.LBB8_18
	jmp	.LBB8_128
.LBB8_17:
	movl	%ebp, %r13d
.LBB8_18:                               # %.lr.ph149
	leaq	112(%r12), %r14
	leaq	48(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB8_19
.LBB8_31:                               # %.thread119
                                        #   in Loop: Header=BB8_19 Depth=1
	movb	$1, 3468(%r12)
	jmp	.LBB8_124
.LBB8_103:                              #   in Loop: Header=BB8_19 Depth=1
	movl	%eax, 3460(%r12)
	movl	%r15d, 3464(%r12)
	jmp	.LBB8_124
	.p2align	4, 0x90
.LBB8_19:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_26 Depth 2
                                        #     Child Loop BB8_39 Depth 2
                                        #       Child Loop BB8_42 Depth 3
                                        #       Child Loop BB8_52 Depth 3
                                        #       Child Loop BB8_61 Depth 3
                                        #       Child Loop BB8_68 Depth 3
                                        #       Child Loop BB8_77 Depth 3
                                        #       Child Loop BB8_82 Depth 3
                                        #       Child Loop BB8_99 Depth 3
                                        #       Child Loop BB8_115 Depth 3
                                        #       Child Loop BB8_119 Depth 3
                                        #       Child Loop BB8_110 Depth 3
                                        #       Child Loop BB8_112 Depth 3
	cmpb	$0, 3468(%r12)
	je	.LBB8_23
# BB#20:                                #   in Loop: Header=BB8_19 Depth=1
	cmpb	$0, 3448(%r12)
	jne	.LBB8_127
# BB#21:                                #   in Loop: Header=BB8_19 Depth=1
	movq	%r12, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoder10ReadTablesEv
	testb	%al, %al
	je	.LBB8_130
# BB#22:                                #   in Loop: Header=BB8_19 Depth=1
	movb	$0, 3468(%r12)
.LBB8_23:                               #   in Loop: Header=BB8_19 Depth=1
	cmpb	$0, 3449(%r12)
	je	.LBB8_39
# BB#24:                                # %.preheader
                                        #   in Loop: Header=BB8_19 Depth=1
	movl	3444(%r12), %eax
	testl	%r13d, %r13d
	je	.LBB8_122
# BB#25:                                # %.preheader
                                        #   in Loop: Header=BB8_19 Depth=1
	testl	%eax, %eax
	movl	%r13d, %ecx
	jne	.LBB8_26
	jmp	.LBB8_123
.LBB8_32:                               # %_ZN9CInBuffer8ReadByteERh.exit.i
                                        #   in Loop: Header=BB8_26 Depth=2
	incl	160(%r12)
	movb	$-1, %al
	jmp	.LBB8_34
	.p2align	4, 0x90
.LBB8_26:                               # %.lr.ph
                                        #   Parent Loop BB8_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	104(%r12), %ecx
	cmpl	$32, %ecx
	jne	.LBB8_33
# BB#27:                                #   in Loop: Header=BB8_26 Depth=2
	movq	112(%r12), %rax
	cmpq	120(%r12), %rax
	jb	.LBB8_30
# BB#28:                                #   in Loop: Header=BB8_26 Depth=2
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB8_32
# BB#29:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB8_26 Depth=2
	movq	(%r14), %rax
.LBB8_30:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i
                                        #   in Loop: Header=BB8_26 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movb	(%rax), %al
	jmp	.LBB8_34
	.p2align	4, 0x90
.LBB8_33:                               #   in Loop: Header=BB8_26 Depth=2
	movl	164(%r12), %eax
	addl	$8, %ecx
	movl	%ecx, 104(%r12)
	movl	%eax, %ecx
	shrl	$8, %ecx
	movl	%ecx, 164(%r12)
.LBB8_34:                               # %_ZN5NBitl8CDecoderI9CInBufferE8ReadByteEv.exit
                                        #   in Loop: Header=BB8_26 Depth=2
	movq	48(%r12), %rcx
	movl	56(%r12), %edx
	leal	1(%rdx), %esi
	movl	%esi, 56(%r12)
	movb	%al, (%rcx,%rdx)
	movl	56(%r12), %eax
	cmpl	60(%r12), %eax
	jne	.LBB8_36
# BB#35:                                #   in Loop: Header=BB8_26 Depth=2
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB8_36:                               # %_ZN12CLzOutWindow7PutByteEh.exit67
                                        #   in Loop: Header=BB8_26 Depth=2
	movl	3444(%r12), %eax
	decl	%eax
	movl	%eax, 3444(%r12)
	leal	-1(%r13), %ecx
	cmpl	$1, %r13d
	je	.LBB8_123
# BB#37:                                # %_ZN12CLzOutWindow7PutByteEh.exit67
                                        #   in Loop: Header=BB8_26 Depth=2
	testl	%eax, %eax
	movl	%ecx, %r13d
	jne	.LBB8_26
	jmp	.LBB8_123
	.p2align	4, 0x90
.LBB8_38:                               # %.thread118
                                        #   in Loop: Header=BB8_39 Depth=2
	decl	%r13d
.LBB8_39:                               # %.thread123.preheader
                                        #   Parent Loop BB8_19 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_42 Depth 3
                                        #       Child Loop BB8_52 Depth 3
                                        #       Child Loop BB8_61 Depth 3
                                        #       Child Loop BB8_68 Depth 3
                                        #       Child Loop BB8_77 Depth 3
                                        #       Child Loop BB8_82 Depth 3
                                        #       Child Loop BB8_99 Depth 3
                                        #       Child Loop BB8_115 Depth 3
                                        #       Child Loop BB8_119 Depth 3
                                        #       Child Loop BB8_110 Depth 3
                                        #       Child Loop BB8_112 Depth 3
	testl	%r13d, %r13d
	je	.LBB8_128
# BB#40:                                # %.lr.ph147
                                        #   in Loop: Header=BB8_39 Depth=2
	movl	$1, %esi
	cmpl	$4, 160(%r12)
	ja	.LBB8_129
# BB#41:                                #   in Loop: Header=BB8_39 Depth=2
	movl	104(%r12), %eax
	cmpl	$8, %eax
	jb	.LBB8_48
	.p2align	4, 0x90
.LBB8_42:                               # %.lr.ph.i.i
                                        #   Parent Loop BB8_19 Depth=1
                                        #     Parent Loop BB8_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	112(%r12), %rax
	cmpq	120(%r12), %rax
	jb	.LBB8_45
# BB#43:                                #   in Loop: Header=BB8_42 Depth=3
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB8_46
# BB#44:                                # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB8_42 Depth=3
	movq	(%r14), %rax
.LBB8_45:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i
                                        #   in Loop: Header=BB8_42 Depth=3
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB8_47
	.p2align	4, 0x90
.LBB8_46:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i
                                        #   in Loop: Header=BB8_42 Depth=3
	incl	160(%r12)
	movb	$-1, %al
.LBB8_47:                               #   in Loop: Header=BB8_42 Depth=3
	movzbl	%al, %edx
	movl	104(%r12), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%edx, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	%esi, 164(%r12)
	movl	108(%r12), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rdx), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%r12)
	addl	$-8, %eax
	movl	%eax, 104(%r12)
	cmpl	$7, %eax
	ja	.LBB8_42
	jmp	.LBB8_49
	.p2align	4, 0x90
.LBB8_48:                               # %._ZN5NBitl8CDecoderI9CInBufferE9NormalizeEv.exit_crit_edge.i
                                        #   in Loop: Header=BB8_39 Depth=2
	movl	108(%r12), %esi
.LBB8_49:                               # %_ZN5NBitl8CDecoderI9CInBufferE8GetValueEj.exit
                                        #   in Loop: Header=BB8_39 Depth=2
	movl	$8, %ecx
	subl	%eax, %ecx
	movl	%esi, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	shrl	$9, %edi
	andl	$32767, %edi            # imm = 0x7FFF
	cmpl	204(%r12), %edi
	jae	.LBB8_51
# BB#50:                                #   in Loop: Header=BB8_39 Depth=2
	movl	%edi, %ecx
	shrl	$6, %ecx
	movzbl	1448(%r12,%rcx), %ecx
	jmp	.LBB8_54
	.p2align	4, 0x90
.LBB8_51:                               # %.preheader.i.preheader
                                        #   in Loop: Header=BB8_39 Depth=2
	movl	$52, %ecx
	.p2align	4, 0x90
.LBB8_52:                               # %.preheader.i
                                        #   Parent Loop BB8_19 Depth=1
                                        #     Parent Loop BB8_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	(%r12,%rcx,4), %edi
	leaq	1(%rcx), %rcx
	jae	.LBB8_52
# BB#53:                                # %.loopexit.loopexit.i
                                        #   in Loop: Header=BB8_39 Depth=2
	addl	$-43, %ecx
.LBB8_54:                               # %.loopexit.i
                                        #   in Loop: Header=BB8_39 Depth=2
	addl	%ecx, %eax
	movl	%eax, 104(%r12)
	movl	164(%r12), %edx
	shrl	%cl, %edx
	movl	%edx, 164(%r12)
	movslq	%ecx, %rbp
	subl	164(%r12,%rbp,4), %edi
	movl	$15, %ecx
	subl	%ebp, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	addl	232(%r12,%rbp,4), %edi
	cmpl	$287, %edi              # imm = 0x11F
	ja	.LBB8_126
# BB#55:                                # %_ZN9NCompress8NHuffman8CDecoderILi15ELj288EE12DecodeSymbolIN5NBitl8CDecoderI9CInBufferEEEEjPT_.exit
                                        #   in Loop: Header=BB8_39 Depth=2
	movl	%edi, %ecx
	movl	296(%r12,%rcx,4), %ecx
	cmpl	$255, %ecx
	ja	.LBB8_58
# BB#56:                                #   in Loop: Header=BB8_39 Depth=2
	movq	48(%r12), %rax
	movl	56(%r12), %edx
	leal	1(%rdx), %esi
	movl	%esi, 56(%r12)
	movb	%cl, (%rax,%rdx)
	movl	56(%r12), %eax
	cmpl	60(%r12), %eax
	jne	.LBB8_38
# BB#57:                                #   in Loop: Header=BB8_39 Depth=2
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
	jmp	.LBB8_38
	.p2align	4, 0x90
.LBB8_58:                               #   in Loop: Header=BB8_39 Depth=2
	cmpl	$256, %ecx              # imm = 0x100
	je	.LBB8_31
# BB#59:                                #   in Loop: Header=BB8_39 Depth=2
	cmpl	$285, %ecx              # imm = 0x11D
	ja	.LBB8_126
# BB#60:                                #   in Loop: Header=BB8_39 Depth=2
	addl	$-257, %ecx             # imm = 0xFEFF
	cmpb	$0, 3457(%r12)
	movl	$_ZN9NCompress8NDeflateL11kLenStart64E, %edi
	movl	$_ZN9NCompress8NDeflateL11kLenStart32E, %ebp
	cmoveq	%rbp, %rdi
	movl	$_ZN9NCompress8NDeflateL16kLenDirectBits64E, %ebp
	movl	$_ZN9NCompress8NDeflateL16kLenDirectBits32E, %ebx
	cmoveq	%rbx, %rbp
	movzbl	(%rbp,%rcx), %ebp
	movzbl	(%rdi,%rcx), %r15d
	cmpl	$8, %eax
	jb	.LBB8_67
	.p2align	4, 0x90
.LBB8_61:                               # %.lr.ph.i.i73
                                        #   Parent Loop BB8_19 Depth=1
                                        #     Parent Loop BB8_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	112(%r12), %rax
	cmpq	120(%r12), %rax
	jb	.LBB8_64
# BB#62:                                #   in Loop: Header=BB8_61 Depth=3
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB8_65
# BB#63:                                # %._crit_edge.i.i.i75
                                        #   in Loop: Header=BB8_61 Depth=3
	movq	(%r14), %rax
.LBB8_64:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i76
                                        #   in Loop: Header=BB8_61 Depth=3
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB8_66
	.p2align	4, 0x90
.LBB8_65:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i77
                                        #   in Loop: Header=BB8_61 Depth=3
	incl	160(%r12)
	movb	$-1, %al
.LBB8_66:                               #   in Loop: Header=BB8_61 Depth=3
	movzbl	%al, %esi
	movl	104(%r12), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	164(%r12), %edx
	movl	%edx, 164(%r12)
	movl	108(%r12), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%r12)
	addl	$-8, %eax
	movl	%eax, 104(%r12)
	cmpl	$7, %eax
	ja	.LBB8_61
.LBB8_67:                               # %_ZN5NBitl8CDecoderI9CInBufferE8ReadBitsEj.exit
                                        #   in Loop: Header=BB8_39 Depth=2
	movl	$1, %ebx
	movl	%ebp, %ecx
	shll	%cl, %ebx
	decl	%ebx
	andl	%edx, %ebx
	addl	%ebp, %eax
	movl	%eax, 104(%r12)
	shrl	%cl, %edx
	movl	%edx, 164(%r12)
	leal	3(%r15,%rbx), %ebp
	cmpl	%r13d, %ebp
	movl	%ebp, %ecx
	cmoval	%r13d, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	cmpl	$8, %eax
	jb	.LBB8_74
	.p2align	4, 0x90
.LBB8_68:                               # %.lr.ph.i.i89
                                        #   Parent Loop BB8_19 Depth=1
                                        #     Parent Loop BB8_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	112(%r12), %rax
	cmpq	120(%r12), %rax
	jb	.LBB8_71
# BB#69:                                #   in Loop: Header=BB8_68 Depth=3
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB8_72
# BB#70:                                # %._crit_edge.i.i.i91
                                        #   in Loop: Header=BB8_68 Depth=3
	movq	(%r14), %rax
.LBB8_71:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i92
                                        #   in Loop: Header=BB8_68 Depth=3
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB8_73
	.p2align	4, 0x90
.LBB8_72:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i93
                                        #   in Loop: Header=BB8_68 Depth=3
	incl	160(%r12)
	movb	$-1, %al
.LBB8_73:                               #   in Loop: Header=BB8_68 Depth=3
	movzbl	%al, %esi
	movl	104(%r12), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	164(%r12), %edx
	movl	%edx, 164(%r12)
	movl	108(%r12), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%r12)
	addl	$-8, %eax
	movl	%eax, 104(%r12)
	cmpl	$7, %eax
	ja	.LBB8_68
.LBB8_74:                               # %_ZN5NBitl8CDecoderI9CInBufferE8GetValueEj.exit95
                                        #   in Loop: Header=BB8_39 Depth=2
	movl	$8, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	shrl	$9, %esi
	andl	$32767, %esi            # imm = 0x7FFF
	cmpl	1996(%r12), %esi
	jae	.LBB8_76
# BB#75:                                #   in Loop: Header=BB8_39 Depth=2
	movl	%esi, %ecx
	shrl	$6, %ecx
	movzbl	2216(%r12,%rcx), %ecx
	jmp	.LBB8_79
	.p2align	4, 0x90
.LBB8_76:                               # %.preheader.i81.preheader
                                        #   in Loop: Header=BB8_39 Depth=2
	movl	$500, %ecx              # imm = 0x1F4
	.p2align	4, 0x90
.LBB8_77:                               # %.preheader.i81
                                        #   Parent Loop BB8_19 Depth=1
                                        #     Parent Loop BB8_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	(%r12,%rcx,4), %esi
	leaq	1(%rcx), %rcx
	jae	.LBB8_77
# BB#78:                                # %.loopexit.loopexit.i82
                                        #   in Loop: Header=BB8_39 Depth=2
	addl	$-491, %ecx             # imm = 0xFE15
.LBB8_79:                               # %.loopexit.i84
                                        #   in Loop: Header=BB8_39 Depth=2
	addl	%ecx, %eax
	movl	%eax, 104(%r12)
	shrl	%cl, %edx
	movl	%edx, 164(%r12)
	movslq	%ecx, %rdi
	subl	1956(%r12,%rdi,4), %esi
	movl	$15, %ecx
	subl	%edi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	addl	2024(%r12,%rdi,4), %esi
	cmpl	$31, %esi
	ja	.LBB8_126
# BB#80:                                # %_ZN9NCompress8NHuffman8CDecoderILi15ELj32EE12DecodeSymbolIN5NBitl8CDecoderI9CInBufferEEEEjPT_.exit
                                        #   in Loop: Header=BB8_39 Depth=2
	movl	%esi, %ecx
	movl	2088(%r12,%rcx,4), %ecx
	cmpl	3452(%r12), %ecx
	jae	.LBB8_126
# BB#81:                                #   in Loop: Header=BB8_39 Depth=2
	movl	%ebp, 36(%rsp)          # 4-byte Spill
	movl	_ZN9NCompress8NDeflateL10kDistStartE(,%rcx,4), %esi
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movzbl	_ZN9NCompress8NDeflateL15kDistDirectBitsE(%rcx), %ebp
	cmpl	$8, %eax
	jb	.LBB8_88
	.p2align	4, 0x90
.LBB8_82:                               # %.lr.ph.i.i99
                                        #   Parent Loop BB8_19 Depth=1
                                        #     Parent Loop BB8_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	112(%r12), %rax
	cmpq	120(%r12), %rax
	jb	.LBB8_85
# BB#83:                                #   in Loop: Header=BB8_82 Depth=3
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB8_86
# BB#84:                                # %._crit_edge.i.i.i101
                                        #   in Loop: Header=BB8_82 Depth=3
	movq	(%r14), %rax
.LBB8_85:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i102
                                        #   in Loop: Header=BB8_82 Depth=3
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB8_87
	.p2align	4, 0x90
.LBB8_86:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i103
                                        #   in Loop: Header=BB8_82 Depth=3
	incl	160(%r12)
	movb	$-1, %al
.LBB8_87:                               #   in Loop: Header=BB8_82 Depth=3
	movzbl	%al, %esi
	movl	104(%r12), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	164(%r12), %edx
	movl	%edx, 164(%r12)
	movl	108(%r12), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%r12)
	addl	$-8, %eax
	movl	%eax, 104(%r12)
	cmpl	$7, %eax
	ja	.LBB8_82
.LBB8_88:                               # %_ZN5NBitl8CDecoderI9CInBufferE8ReadBitsEj.exit106
                                        #   in Loop: Header=BB8_39 Depth=2
	movq	%r15, %rdi
	movl	$1, %r15d
	movl	%ebp, %ecx
	shll	%cl, %r15d
	decl	%r15d
	andl	%edx, %r15d
	addl	%ebp, %eax
	movl	%eax, 104(%r12)
	shrl	%cl, %edx
	movl	%edx, 164(%r12)
	addl	16(%rsp), %r15d         # 4-byte Folded Reload
	movl	56(%r12), %edx
	movl	%edx, %ebp
	subl	%r15d, %ebp
	decl	%ebp
	cmpl	%r15d, %edx
	jbe	.LBB8_90
# BB#89:                                #   in Loop: Header=BB8_39 Depth=2
	movl	12(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB8_93
	.p2align	4, 0x90
.LBB8_90:                               #   in Loop: Header=BB8_39 Depth=2
	cmpb	$0, 96(%r12)
	movl	12(%rsp), %ecx          # 4-byte Reload
	je	.LBB8_130
# BB#91:                                #   in Loop: Header=BB8_39 Depth=2
	movl	68(%r12), %eax
	cmpl	%r15d, %eax
	movl	$1, %esi
	jbe	.LBB8_129
# BB#92:                                #   in Loop: Header=BB8_39 Depth=2
	addl	%ebp, %eax
	movl	%eax, %ebp
.LBB8_93:                               #   in Loop: Header=BB8_39 Depth=2
	movl	60(%r12), %eax
	subl	%edx, %eax
	cmpl	%ecx, %eax
	jbe	.LBB8_97
# BB#94:                                #   in Loop: Header=BB8_39 Depth=2
	movl	68(%r12), %eax
	subl	%ebp, %eax
	cmpl	%ecx, %eax
	jbe	.LBB8_97
# BB#95:                                #   in Loop: Header=BB8_39 Depth=2
	movq	48(%r12), %r11
	movl	%ebp, %r10d
	leaq	(%r11,%r10), %rax
	movl	%ecx, %r9d
	leaq	(%r11,%rdx), %rcx
	movl	%edx, %esi
	addl	%r9d, %esi
	movl	%esi, 56(%r12)
	movl	$-4, %ebp
	subl	%ebx, %ebp
	subl	%edi, %ebp
	movl	%r13d, %ebx
	notl	%ebx
	cmpl	%ebx, %ebp
	movl	%ebx, %esi
	cmoval	%ebp, %esi
	movl	$-2, %r8d
	subl	%esi, %r8d
	incq	%r8
	cmpq	$32, %r8
	jae	.LBB8_104
# BB#96:                                #   in Loop: Header=BB8_39 Depth=2
	movl	%r9d, %edx
	jmp	.LBB8_108
	.p2align	4, 0x90
.LBB8_97:                               # %.preheader.i107.preheader
                                        #   in Loop: Header=BB8_39 Depth=2
	movl	$-4, %eax
	subl	%ebx, %eax
	subl	%edi, %eax
	movl	%r13d, %ebx
	notl	%ebx
	cmpl	%ebx, %eax
	cmoval	%eax, %ebx
	addl	$2, %ebx
	jmp	.LBB8_99
	.p2align	4, 0x90
.LBB8_98:                               # %._crit_edge.i
                                        #   in Loop: Header=BB8_99 Depth=3
	incl	%ebp
	movl	56(%r12), %edx
	incl	%ebx
.LBB8_99:                               # %.preheader.i107
                                        #   Parent Loop BB8_19 Depth=1
                                        #     Parent Loop BB8_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	68(%r12), %ebp
	movl	$0, %eax
	cmovel	%eax, %ebp
	movq	48(%r12), %rax
	movzbl	(%rax,%rbp), %ecx
	leal	1(%rdx), %esi
	movl	%esi, 56(%r12)
	movl	%edx, %edx
	movb	%cl, (%rax,%rdx)
	movl	56(%r12), %eax
	cmpl	60(%r12), %eax
	jne	.LBB8_101
# BB#100:                               #   in Loop: Header=BB8_99 Depth=3
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB8_101:                              #   in Loop: Header=BB8_99 Depth=3
	testl	%ebx, %ebx
	jne	.LBB8_98
.LBB8_102:                              # %.loopexit
                                        #   in Loop: Header=BB8_39 Depth=2
	movl	12(%rsp), %ecx          # 4-byte Reload
	subl	%ecx, %r13d
	movl	36(%rsp), %eax          # 4-byte Reload
	subl	%ecx, %eax
	je	.LBB8_39
	jmp	.LBB8_103
.LBB8_104:                              # %min.iters.checked
                                        #   in Loop: Header=BB8_39 Depth=2
	movq	%r8, %rdi
	movabsq	$8589934560, %rsi       # imm = 0x1FFFFFFE0
	andq	%rsi, %rdi
	je	.LBB8_107
# BB#105:                               # %vector.memcheck
                                        #   in Loop: Header=BB8_39 Depth=2
	cmpl	%ebx, %ebp
	cmoval	%ebp, %ebx
	movl	$-2, %ebp
	subl	%ebx, %ebp
	leaq	(%r10,%rbp), %rsi
	leaq	1(%r11,%rsi), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB8_113
# BB#106:                               # %vector.memcheck
                                        #   in Loop: Header=BB8_39 Depth=2
	addq	%rdx, %rbp
	leaq	1(%r11,%rbp), %rsi
	cmpq	%rsi, %rax
	jae	.LBB8_113
.LBB8_107:                              #   in Loop: Header=BB8_39 Depth=2
	movl	12(%rsp), %edx          # 4-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
.LBB8_108:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB8_39 Depth=2
	leal	-1(%rdx), %esi
	movl	%edx, %edi
	andl	$7, %edi
	je	.LBB8_111
# BB#109:                               # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB8_39 Depth=2
	negl	%edi
	.p2align	4, 0x90
.LBB8_110:                              # %scalar.ph.prol
                                        #   Parent Loop BB8_19 Depth=1
                                        #     Parent Loop BB8_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ebx
	incq	%rax
	movb	%bl, (%rcx)
	incq	%rcx
	decl	%edx
	incl	%edi
	jne	.LBB8_110
.LBB8_111:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB8_39 Depth=2
	cmpl	$7, %esi
	jb	.LBB8_102
	.p2align	4, 0x90
.LBB8_112:                              # %scalar.ph
                                        #   Parent Loop BB8_19 Depth=1
                                        #     Parent Loop BB8_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ebx
	movb	%bl, (%rcx)
	movzbl	1(%rax), %ebx
	movb	%bl, 1(%rcx)
	movzbl	2(%rax), %ebx
	movb	%bl, 2(%rcx)
	movzbl	3(%rax), %ebx
	movb	%bl, 3(%rcx)
	movzbl	4(%rax), %ebx
	movb	%bl, 4(%rcx)
	movzbl	5(%rax), %ebx
	movb	%bl, 5(%rcx)
	movzbl	6(%rax), %ebx
	movb	%bl, 6(%rcx)
	movzbl	7(%rax), %ebx
	movb	%bl, 7(%rcx)
	addq	$8, %rax
	addq	$8, %rcx
	addl	$-8, %edx
	jne	.LBB8_112
	jmp	.LBB8_102
.LBB8_113:                              # %vector.body.preheader
                                        #   in Loop: Header=BB8_39 Depth=2
	leaq	-32(%rdi), %rbx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	shrl	$5, %ebx
	incl	%ebx
	andq	$3, %rbx
	je	.LBB8_116
# BB#114:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB8_39 Depth=2
	leaq	16(%r11,%rdx), %rsi
	leaq	16(%r11,%r10), %r9
	negq	%rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_115:                              # %vector.body.prol
                                        #   Parent Loop BB8_19 Depth=1
                                        #     Parent Loop BB8_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-16(%r9,%rbp), %xmm0
	movups	(%r9,%rbp), %xmm1
	movups	%xmm0, -16(%rsi,%rbp)
	movups	%xmm1, (%rsi,%rbp)
	addq	$32, %rbp
	incq	%rbx
	jne	.LBB8_115
	jmp	.LBB8_117
.LBB8_116:                              #   in Loop: Header=BB8_39 Depth=2
	xorl	%ebp, %ebp
.LBB8_117:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB8_39 Depth=2
	cmpq	$96, 16(%rsp)           # 8-byte Folded Reload
	jb	.LBB8_120
# BB#118:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB8_39 Depth=2
	movq	%rdi, %rbx
	subq	%rbp, %rbx
	leaq	(%rdx,%rbp), %rdx
	leaq	112(%r11,%rdx), %rdx
	addq	%rbp, %r10
	leaq	112(%r11,%r10), %rsi
	.p2align	4, 0x90
.LBB8_119:                              # %vector.body
                                        #   Parent Loop BB8_19 Depth=1
                                        #     Parent Loop BB8_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-128, %rbx
	jne	.LBB8_119
.LBB8_120:                              # %middle.block
                                        #   in Loop: Header=BB8_39 Depth=2
	cmpq	%rdi, %r8
	je	.LBB8_102
# BB#121:                               #   in Loop: Header=BB8_39 Depth=2
	movl	12(%rsp), %edx          # 4-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	subl	%edi, %edx
	addq	%rdi, %rax
	addq	%rdi, %rcx
	jmp	.LBB8_108
.LBB8_122:                              #   in Loop: Header=BB8_19 Depth=1
	movl	%r13d, %ecx
.LBB8_123:                              # %._crit_edge
                                        #   in Loop: Header=BB8_19 Depth=1
	testl	%eax, %eax
	sete	3468(%r12)
	movl	%ecx, %r13d
.LBB8_124:                              # %.backedge
                                        #   in Loop: Header=BB8_19 Depth=1
	xorl	%esi, %esi
	testl	%r13d, %r13d
	jne	.LBB8_19
	jmp	.LBB8_129
.LBB8_126:
	movl	$1, %esi
	jmp	.LBB8_129
.LBB8_130:
	movl	$1, %esi
	jmp	.LBB8_129
.LBB8_127:
	movl	$-1, 3460(%r12)
.LBB8_128:                              # %.thread117
	xorl	%esi, %esi
.LBB8_129:                              # %.thread117
	movl	%esi, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeSpecEj, .Lfunc_end8-_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeSpecEj
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeRealEP20ISequentialOutStreamPKyP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeRealEP20ISequentialOutStreamPKyP21ICompressProgressInfo,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeRealEP20ISequentialOutStreamPKyP21ICompressProgressInfo: # @_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeRealEP20ISequentialOutStreamPKyP21ICompressProgressInfo
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi96:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi97:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi99:
	.cfi_def_cfa_offset 96
.Lcfi100:
	.cfi_offset %rbx, -56
.Lcfi101:
	.cfi_offset %r12, -48
.Lcfi102:
	.cfi_offset %r13, -40
.Lcfi103:
	.cfi_offset %r14, -32
.Lcfi104:
	.cfi_offset %r15, -24
.Lcfi105:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %r14
	movq	%rdi, %r13
	leaq	48(%r13), %rdi
.Ltmp12:
	movq	%rdi, (%rsp)            # 8-byte Spill
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
.Ltmp13:
# BB#1:
	cmpb	$0, 3459(%r13)
	je	.LBB9_3
# BB#2:
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB9_4
.LBB9_3:
	movq	112(%r13), %rax
	addq	144(%r13), %rax
	subq	128(%r13), %rax
	movl	160(%r13), %ecx
	addq	%rax, %rcx
	movl	$32, %eax
	subl	104(%r13), %eax
	shrl	$3, %eax
	subq	%rax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
.LBB9_4:
.Ltmp15:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
	movq	%rax, %rbp
.Ltmp16:
# BB#5:                                 # %.thread101.preheader
	leaq	112(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_6:                                # %.thread101.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_16 Depth 2
	testq	%r14, %r14
	je	.LBB9_7
	.p2align	4, 0x90
.LBB9_16:                               # %.thread101
                                        #   Parent Loop BB9_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rbx
.Ltmp18:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
.Ltmp19:
# BB#17:                                #   in Loop: Header=BB9_16 Depth=2
	movq	%rbp, %rcx
	subq	%rax, %rcx
	addq	%rbx, %rcx
	cmpq	$262144, %rcx           # imm = 0x40000
	movl	$262144, %esi           # imm = 0x40000
	jae	.LBB9_19
# BB#18:                                #   in Loop: Header=BB9_16 Depth=2
	testl	%ecx, %ecx
	movl	%ecx, %esi
	je	.LBB9_34
.LBB9_19:                               # %.thread
                                        #   in Loop: Header=BB9_16 Depth=2
.Ltmp20:
	movq	%r13, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeSpecEj
	movl	%eax, %r15d
.Ltmp21:
# BB#20:                                #   in Loop: Header=BB9_16 Depth=2
	testl	%r15d, %r15d
	jne	.LBB9_26
# BB#21:                                #   in Loop: Header=BB9_16 Depth=2
	cmpl	$-1, 3460(%r13)
	je	.LBB9_35
# BB#22:                                #   in Loop: Header=BB9_16 Depth=2
	testq	%r12, %r12
	je	.LBB9_16
	jmp	.LBB9_23
	.p2align	4, 0x90
.LBB9_7:                                # %.thread101.outer.split.us
                                        #   in Loop: Header=BB9_6 Depth=1
	testq	%r12, %r12
	je	.LBB9_8
# BB#12:                                # %.thread.us
                                        #   in Loop: Header=BB9_6 Depth=1
.Ltmp23:
	movl	$262144, %esi           # imm = 0x40000
	movq	%r13, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeSpecEj
	movl	%eax, %r15d
.Ltmp24:
# BB#13:                                #   in Loop: Header=BB9_6 Depth=1
	testl	%r15d, %r15d
	jne	.LBB9_26
# BB#14:                                #   in Loop: Header=BB9_6 Depth=1
	cmpl	$-1, 3460(%r13)
	je	.LBB9_35
.LBB9_23:                               # %.us-lcssa125.us
                                        #   in Loop: Header=BB9_6 Depth=1
	movq	144(%r13), %rax
	movl	160(%r13), %ecx
	movl	$32, %edx
	subl	104(%r13), %edx
	shrl	$3, %edx
	subq	16(%rsp), %rax          # 8-byte Folded Reload
	addq	112(%r13), %rax
	subq	128(%r13), %rax
	addq	%rcx, %rax
	subq	%rdx, %rax
	movq	%rax, 32(%rsp)
.Ltmp26:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
.Ltmp27:
# BB#24:                                #   in Loop: Header=BB9_6 Depth=1
	subq	%rbp, %rax
	movq	%rax, 24(%rsp)
	movq	(%r12), %rax
.Ltmp29:
	movq	%r12, %rdi
	leaq	32(%rsp), %rsi
	leaq	24(%rsp), %rdx
	callq	*40(%rax)
	movl	%eax, %r15d
.Ltmp30:
# BB#25:                                #   in Loop: Header=BB9_6 Depth=1
	testl	%r15d, %r15d
	je	.LBB9_6
	jmp	.LBB9_26
	.p2align	4, 0x90
.LBB9_8:                                # %.thread101.us.us
                                        # =>This Inner Loop Header: Depth=1
.Ltmp32:
	movl	$262144, %esi           # imm = 0x40000
	movq	%r13, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeSpecEj
	movl	%eax, %r15d
.Ltmp33:
# BB#9:                                 #   in Loop: Header=BB9_8 Depth=1
	testl	%r15d, %r15d
	jne	.LBB9_26
# BB#10:                                #   in Loop: Header=BB9_8 Depth=1
	cmpl	$-1, 3460(%r13)
	jne	.LBB9_8
	jmp	.LBB9_35
.LBB9_26:                               # %.loopexit115.thread
.Ltmp35:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10COutBuffer5FlushEv
.Ltmp36:
	jmp	.LBB9_52
.LBB9_34:                               # %.thread100
	cmpl	$-1, 3460(%r13)
	jne	.LBB9_47
.LBB9_35:                               # %.thread100.thread
	cmpb	$0, 3469(%r13)
	je	.LBB9_47
# BB#36:
	movl	104(%r13), %edx
	movl	%edx, %ecx
	negl	%ecx
	andl	$7, %ecx
	addl	%ecx, %edx
	movl	%edx, 104(%r13)
	movl	164(%r13), %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	movl	%eax, 164(%r13)
	cmpl	$32, %edx
	jne	.LBB9_43
# BB#37:
	movq	112(%r13), %rax
	cmpq	120(%r13), %rax
	movq	8(%rsp), %rbx           # 8-byte Reload
	jb	.LBB9_41
# BB#38:
.Ltmp37:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp38:
# BB#39:                                # %.noexc88
	testb	%al, %al
	je	.LBB9_42
# BB#40:                                # %._crit_edge.i.i
	movq	(%rbx), %rax
.LBB9_41:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movb	(%rax), %al
	jmp	.LBB9_44
.LBB9_43:
	addl	$8, %edx
	movl	%edx, 104(%r13)
	movl	%eax, %ecx
	shrl	$8, %ecx
	movl	%ecx, 164(%r13)
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB9_44:                               # %_ZN5NBitl8CDecoderI9CInBufferE8ReadByteEv.exit
	movb	%al, 3470(%r13)
	movl	104(%r13), %ecx
	cmpl	$32, %ecx
	jne	.LBB9_45
# BB#67:
	movq	112(%r13), %rax
	cmpq	120(%r13), %rax
	jb	.LBB9_72
# BB#68:
.Ltmp39:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp40:
# BB#69:                                # %.noexc88.1
	testb	%al, %al
	je	.LBB9_70
# BB#71:                                # %._crit_edge.i.i.1
	movq	(%rbx), %rax
.LBB9_72:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movb	(%rax), %al
	jmp	.LBB9_73
.LBB9_45:
	movl	164(%r13), %eax
	addl	$8, %ecx
	movl	%ecx, 104(%r13)
	movl	%eax, %ecx
	shrl	$8, %ecx
	movl	%ecx, 164(%r13)
.LBB9_73:                               # %_ZN5NBitl8CDecoderI9CInBufferE8ReadByteEv.exit.1
	movb	%al, 3471(%r13)
	movl	104(%r13), %ecx
	cmpl	$32, %ecx
	jne	.LBB9_74
# BB#75:
	movq	112(%r13), %rax
	cmpq	120(%r13), %rax
	jb	.LBB9_80
# BB#76:
.Ltmp41:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp42:
# BB#77:                                # %.noexc88.2
	testb	%al, %al
	je	.LBB9_78
# BB#79:                                # %._crit_edge.i.i.2
	movq	(%rbx), %rax
.LBB9_80:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.2
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movb	(%rax), %al
	jmp	.LBB9_81
.LBB9_74:
	movl	164(%r13), %eax
	addl	$8, %ecx
	movl	%ecx, 104(%r13)
	movl	%eax, %ecx
	shrl	$8, %ecx
	movl	%ecx, 164(%r13)
.LBB9_81:                               # %_ZN5NBitl8CDecoderI9CInBufferE8ReadByteEv.exit.2
	movb	%al, 3472(%r13)
	movl	104(%r13), %eax
	cmpl	$32, %eax
	jne	.LBB9_82
# BB#83:
	movq	112(%r13), %rax
	cmpq	120(%r13), %rax
	jb	.LBB9_88
# BB#84:
.Ltmp43:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp44:
# BB#85:                                # %.noexc88.3
	testb	%al, %al
	je	.LBB9_86
# BB#87:                                # %._crit_edge.i.i.3
	movq	(%rbx), %rax
.LBB9_88:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.3
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movb	(%rax), %al
	movb	%al, 3473(%r13)
	jmp	.LBB9_47
.LBB9_82:
	movl	164(%r13), %ecx
	addl	$8, %eax
	movl	%eax, 104(%r13)
	movl	%ecx, %eax
	shrl	$8, %eax
	movl	%eax, 164(%r13)
	movb	%cl, 3473(%r13)
.LBB9_47:                               # %.loopexit
.Ltmp48:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10COutBuffer5FlushEv
.Ltmp49:
# BB#48:                                # %_ZN9NCompress8NDeflate8NDecoder6CCoder5FlushEv.exit
	testl	%eax, %eax
	je	.LBB9_49
.LBB9_51:                               # %_ZNK9NCompress8NDeflate8NDecoder6CCoder13InputEofErrorEv.exit.thread
	movl	%eax, %r15d
.LBB9_52:                               # %._crit_edge.i84
	movq	72(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB9_65
# BB#53:
	movq	(%rdi), %rax
.Ltmp54:
	callq	*16(%rax)
.Ltmp55:
# BB#54:                                # %.noexc86
	movq	$0, 72(%r13)
	jmp	.LBB9_65
.LBB9_49:
	movl	160(%r13), %ecx
	testl	%ecx, %ecx
	je	.LBB9_51
# BB#50:                                # %_ZNK9NCompress8NDeflate8NDecoder6CCoder13InputEofErrorEv.exit
	movl	$32, %edx
	subl	104(%r13), %edx
	shll	$3, %ecx
	movl	$1, %r15d
	cmpl	%ecx, %edx
	jae	.LBB9_51
	jmp	.LBB9_52
.LBB9_42:                               # %_ZN9CInBuffer8ReadByteERh.exit.i
	incl	160(%r13)
	movb	$-1, %al
	jmp	.LBB9_44
.LBB9_70:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.1
	incl	160(%r13)
	movb	$-1, %al
	jmp	.LBB9_73
.LBB9_78:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.2
	incl	160(%r13)
	movb	$-1, %al
	jmp	.LBB9_81
.LBB9_86:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.3
	incl	160(%r13)
	movb	$-1, %al
	movb	%al, 3473(%r13)
	jmp	.LBB9_47
.LBB9_46:
.Ltmp45:
	jmp	.LBB9_30
.LBB9_56:
.Ltmp50:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	jmp	.LBB9_57
.LBB9_55:
.Ltmp56:
	jmp	.LBB9_28
.LBB9_29:
.Ltmp17:
	jmp	.LBB9_30
.LBB9_27:
.Ltmp14:
.LBB9_28:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder14CCoderReleaserD2Ev.exit
	movq	%rdx, %rbx
	movq	%rax, %rbp
	jmp	.LBB9_60
.LBB9_11:                               # %.us-lcssa.us.us-lcssa.us
.Ltmp34:
	jmp	.LBB9_30
.LBB9_15:                               # %.us-lcssa.us.us-lcssa
.Ltmp25:
	jmp	.LBB9_30
.LBB9_33:
.Ltmp31:
	jmp	.LBB9_30
.LBB9_32:
.Ltmp28:
	jmp	.LBB9_30
.LBB9_31:                               # %.us-lcssa
.Ltmp22:
.LBB9_30:
	movq	%rdx, %rbx
	movq	%rax, %rbp
.Ltmp46:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN10COutBuffer5FlushEv
.Ltmp47:
.LBB9_57:                               # %._crit_edge.i
	movq	72(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB9_60
# BB#58:
	movq	(%rdi), %rax
.Ltmp51:
	callq	*16(%rax)
.Ltmp52:
# BB#59:                                # %.noexc82
	movq	$0, 72(%r13)
.LBB9_60:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder14CCoderReleaserD2Ev.exit
	movq	%rbp, %rdi
	cmpl	$3, %ebx
	jne	.LBB9_62
# BB#61:
	callq	__cxa_begin_catch
	jmp	.LBB9_63
.LBB9_62:
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB9_64
.LBB9_63:
	movl	(%rax), %r15d
	callq	__cxa_end_catch
	jmp	.LBB9_65
.LBB9_64:
	callq	__cxa_end_catch
	movl	$1, %r15d
.LBB9_65:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder14CCoderReleaserD2Ev.exit87
	movl	%r15d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_66:
.Ltmp53:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeRealEP20ISequentialOutStreamPKyP21ICompressProgressInfo, .Lfunc_end9-_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeRealEP20ISequentialOutStreamPKyP21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\276\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\251\001"              # Call site table length
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	5                       #   On action: 3
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	5                       #   On action: 3
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp21-.Ltmp18         #   Call between .Ltmp18 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin1   #     jumps to .Ltmp22
	.byte	5                       #   On action: 3
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin1   #     jumps to .Ltmp25
	.byte	5                       #   On action: 3
	.long	.Ltmp26-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin1   #     jumps to .Ltmp28
	.byte	5                       #   On action: 3
	.long	.Ltmp29-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin1   #     jumps to .Ltmp31
	.byte	5                       #   On action: 3
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin1   #     jumps to .Ltmp34
	.byte	5                       #   On action: 3
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp56-.Lfunc_begin1   #     jumps to .Ltmp56
	.byte	5                       #   On action: 3
	.long	.Ltmp37-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp44-.Ltmp37         #   Call between .Ltmp37 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin1   #     jumps to .Ltmp45
	.byte	5                       #   On action: 3
	.long	.Ltmp48-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin1   #     jumps to .Ltmp50
	.byte	5                       #   On action: 3
	.long	.Ltmp54-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin1   #     jumps to .Ltmp56
	.byte	5                       #   On action: 3
	.long	.Ltmp46-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Ltmp52-.Ltmp46         #   Call between .Ltmp46 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin1   #     jumps to .Ltmp53
	.byte	1                       #   On action: 1
	.long	.Ltmp52-.Lfunc_begin1   # >> Call Site 13 <<
	.long	.Lfunc_end9-.Ltmp52     #   Call between .Ltmp52 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
	.byte	3                       # >> Action Record 3 <<
                                        #   Catch TypeInfo 3
	.byte	125                     #   Continue to action 2
                                        # >> Catch TypeInfos <<
	.long	_ZTI18CInBufferException # TypeInfo 3
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress8NDeflate8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo: # @_ZN9NCompress8NDeflate8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi109:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 48
.Lcfi111:
	.cfi_offset %rbx, -48
.Lcfi112:
	.cfi_offset %r12, -40
.Lcfi113:
	.cfi_offset %r14, -32
.Lcfi114:
	.cfi_offset %r15, -24
.Lcfi115:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r12
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*48(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	*64(%rax)
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeRealEP20ISequentialOutStreamPKyP21ICompressProgressInfo
	movl	%eax, %ebp
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo, .Lfunc_end10-_ZN9NCompress8NDeflate8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy: # @_ZN9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB11_1
# BB#2:
	movq	112(%rdi), %rax
	addq	144(%rdi), %rax
	subq	128(%rdi), %rax
	movl	160(%rdi), %ecx
	addq	%rax, %rcx
	movl	$32, %eax
	subl	104(%rdi), %eax
	shrl	$3, %eax
	subq	%rax, %rcx
	movq	%rcx, (%rsi)
	xorl	%eax, %eax
	retq
.LBB11_1:
	movl	$-2147024809, %eax      # imm = 0x80070057
	retq
.Lfunc_end11:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy, .Lfunc_end11-_ZN9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy
	.cfi_endproc

	.globl	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB12_1
# BB#2:
	movq	104(%rdi), %rax
	addq	136(%rdi), %rax
	subq	120(%rdi), %rax
	movl	152(%rdi), %ecx
	addq	%rax, %rcx
	movl	$32, %eax
	subl	96(%rdi), %eax
	shrl	$3, %eax
	subq	%rax, %rcx
	movq	%rcx, (%rsi)
	xorl	%eax, %eax
	retq
.LBB12_1:
	movl	$-2147024809, %eax      # imm = 0x80070057
	retq
.Lfunc_end12:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy, .Lfunc_end12-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream: # @_ZN9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 16
	addq	$112, %rdi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream, .Lfunc_end13-_ZN9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi117:
	.cfi_def_cfa_offset 16
	addq	$96, %rdi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end14:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream, .Lfunc_end14-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv: # @_ZN9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 16
.Lcfi119:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 136(%rbx)
.LBB15_2:                               # %_ZN5NBitl12CBaseDecoderI9CInBufferE13ReleaseStreamEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end15:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv, .Lfunc_end15-_ZN9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv
	.cfi_endproc

	.globl	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 16
.Lcfi121:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB16_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 120(%rbx)
.LBB16_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end16:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv, .Lfunc_end16-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy: # @_ZN9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi122:
	.cfi_def_cfa_offset 16
	movl	$-2, 3460(%rdi)
	movb	$1, 3459(%rdi)
	movzbl	3458(%rdi), %esi
	addq	$48, %rdi
	callq	_ZN12CLzOutWindow4InitEb
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end17:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy, .Lfunc_end17-_ZN9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy
	.cfi_endproc

	.globl	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi123:
	.cfi_def_cfa_offset 16
	movl	$-2, 3436(%rdi)
	movb	$1, 3435(%rdi)
	movzbl	3434(%rdi), %esi
	addq	$24, %rdi
	callq	_ZN12CLzOutWindow4InitEb
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end18:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy, .Lfunc_end18-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj: # @_ZN9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi126:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi127:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi128:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi130:
	.cfi_def_cfa_offset 64
.Lcfi131:
	.cfi_offset %rbx, -56
.Lcfi132:
	.cfi_offset %r12, -48
.Lcfi133:
	.cfi_offset %r13, -40
.Lcfi134:
	.cfi_offset %r14, -32
.Lcfi135:
	.cfi_offset %r15, -24
.Lcfi136:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movl	%edx, %r13d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.LBB19_2
# BB#1:
	movl	$0, (%r12)
.LBB19_2:
	leaq	48(%rbx), %r14
.Ltmp57:
	movq	%r14, %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
	movq	%rax, %r15
.Ltmp58:
# BB#3:
	movq	%rbp, 88(%rbx)
.Ltmp60:
	movq	%rbx, %rdi
	movl	%r13d, %esi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeSpecEj
	movl	%eax, %ebp
.Ltmp61:
# BB#4:
	testl	%ebp, %ebp
	jne	.LBB19_16
# BB#5:
	testq	%r12, %r12
	je	.LBB19_8
# BB#6:
.Ltmp62:
	movq	%r14, %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
.Ltmp63:
# BB#7:
	subl	%r15d, %eax
	movl	%eax, (%r12)
.LBB19_8:
.Ltmp64:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer5FlushEv
	movl	%eax, %ebp
.Ltmp65:
.LBB19_16:                              # %_ZN9NCompress8NDeflate8NDecoder6CCoder5FlushEv.exit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB19_9:
.Ltmp59:
	jmp	.LBB19_11
.LBB19_10:
.Ltmp66:
.LBB19_11:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	cmpl	$3, %ebx
	jne	.LBB19_13
# BB#12:
	callq	__cxa_begin_catch
	jmp	.LBB19_14
.LBB19_13:
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB19_15
.LBB19_14:
	movl	(%rax), %ebp
	callq	__cxa_end_catch
	jmp	.LBB19_16
.LBB19_15:
	callq	__cxa_end_catch
	movl	$1, %ebp
	jmp	.LBB19_16
.Lfunc_end19:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj, .Lfunc_end19-_ZN9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\273\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp57-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin2   #     jumps to .Ltmp59
	.byte	5                       #   On action: 3
	.long	.Ltmp60-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp65-.Ltmp60         #   Call between .Ltmp60 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin2   #     jumps to .Ltmp66
	.byte	5                       #   On action: 3
	.long	.Ltmp65-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Lfunc_end19-.Ltmp65    #   Call between .Ltmp65 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
	.byte	3                       # >> Action Record 3 <<
                                        #   Catch TypeInfo 3
	.byte	125                     #   Continue to action 2
                                        # >> Catch TypeInfos <<
	.long	_ZTI18CInBufferException # TypeInfo 3
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj # TAILCALL
.Lfunc_end20:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj, .Lfunc_end20-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN9NCompress8NDeflate8NDecoder6CCoder10CodeResumeEP20ISequentialOutStreamPKyP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder10CodeResumeEP20ISequentialOutStreamPKyP21ICompressProgressInfo,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder10CodeResumeEP20ISequentialOutStreamPKyP21ICompressProgressInfo: # @_ZN9NCompress8NDeflate8NDecoder6CCoder10CodeResumeEP20ISequentialOutStreamPKyP21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi137:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi138:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi139:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi140:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi141:
	.cfi_def_cfa_offset 48
.Lcfi142:
	.cfi_offset %rbx, -40
.Lcfi143:
	.cfi_offset %r12, -32
.Lcfi144:
	.cfi_offset %r14, -24
.Lcfi145:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movl	$-2, 3460(%rbx)
	leaq	48(%rbx), %rdi
	movzbl	3458(%rbx), %esi
	callq	_ZN12CLzOutWindow4InitEb
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder8CodeRealEP20ISequentialOutStreamPKyP21ICompressProgressInfo # TAILCALL
.Lfunc_end21:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder10CodeResumeEP20ISequentialOutStreamPKyP21ICompressProgressInfo, .Lfunc_end21-_ZN9NCompress8NDeflate8NDecoder6CCoder10CodeResumeEP20ISequentialOutStreamPKyP21ICompressProgressInfo
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi146:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB22_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB22_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB22_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB22_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB22_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB22_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB22_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB22_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB22_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB22_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB22_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB22_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB22_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB22_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB22_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB22_16
.LBB22_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICompressGetInStreamProcessedSize(%rip), %cl
	jne	.LBB22_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+1(%rip), %al
	jne	.LBB22_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+2(%rip), %al
	jne	.LBB22_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+3(%rip), %al
	jne	.LBB22_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+4(%rip), %al
	jne	.LBB22_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+5(%rip), %al
	jne	.LBB22_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+6(%rip), %al
	jne	.LBB22_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+7(%rip), %al
	jne	.LBB22_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+8(%rip), %al
	jne	.LBB22_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+9(%rip), %al
	jne	.LBB22_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+10(%rip), %al
	jne	.LBB22_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+11(%rip), %al
	jne	.LBB22_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+12(%rip), %al
	jne	.LBB22_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+13(%rip), %al
	jne	.LBB22_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+14(%rip), %al
	jne	.LBB22_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+15(%rip), %al
	jne	.LBB22_33
.LBB22_16:
	leaq	8(%rdi), %rax
	jmp	.LBB22_84
.LBB22_33:                              # %_ZeqRK4GUIDS1_.exit12.thread
	cmpb	IID_ICompressSetInStream(%rip), %cl
	jne	.LBB22_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetInStream+1(%rip), %al
	jne	.LBB22_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetInStream+2(%rip), %al
	jne	.LBB22_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetInStream+3(%rip), %al
	jne	.LBB22_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetInStream+4(%rip), %al
	jne	.LBB22_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetInStream+5(%rip), %al
	jne	.LBB22_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetInStream+6(%rip), %al
	jne	.LBB22_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetInStream+7(%rip), %al
	jne	.LBB22_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetInStream+8(%rip), %al
	jne	.LBB22_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetInStream+9(%rip), %al
	jne	.LBB22_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetInStream+10(%rip), %al
	jne	.LBB22_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetInStream+11(%rip), %al
	jne	.LBB22_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetInStream+12(%rip), %al
	jne	.LBB22_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetInStream+13(%rip), %al
	jne	.LBB22_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetInStream+14(%rip), %al
	jne	.LBB22_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit16
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetInStream+15(%rip), %al
	jne	.LBB22_50
# BB#49:
	leaq	16(%rdi), %rax
	jmp	.LBB22_84
.LBB22_50:                              # %_ZeqRK4GUIDS1_.exit16.thread
	cmpb	IID_ICompressSetOutStreamSize(%rip), %cl
	jne	.LBB22_67
# BB#51:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+1(%rip), %al
	jne	.LBB22_67
# BB#52:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+2(%rip), %al
	jne	.LBB22_67
# BB#53:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+3(%rip), %al
	jne	.LBB22_67
# BB#54:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+4(%rip), %al
	jne	.LBB22_67
# BB#55:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+5(%rip), %al
	jne	.LBB22_67
# BB#56:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+6(%rip), %al
	jne	.LBB22_67
# BB#57:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+7(%rip), %al
	jne	.LBB22_67
# BB#58:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+8(%rip), %al
	jne	.LBB22_67
# BB#59:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+9(%rip), %al
	jne	.LBB22_67
# BB#60:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+10(%rip), %al
	jne	.LBB22_67
# BB#61:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+11(%rip), %al
	jne	.LBB22_67
# BB#62:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+12(%rip), %al
	jne	.LBB22_67
# BB#63:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+13(%rip), %al
	jne	.LBB22_67
# BB#64:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+14(%rip), %al
	jne	.LBB22_67
# BB#65:                                # %_ZeqRK4GUIDS1_.exit18
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+15(%rip), %al
	jne	.LBB22_67
# BB#66:
	leaq	24(%rdi), %rax
	jmp	.LBB22_84
.LBB22_67:                              # %_ZeqRK4GUIDS1_.exit18.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ISequentialInStream(%rip), %cl
	jne	.LBB22_85
# BB#68:
	movb	1(%rsi), %cl
	cmpb	IID_ISequentialInStream+1(%rip), %cl
	jne	.LBB22_85
# BB#69:
	movb	2(%rsi), %cl
	cmpb	IID_ISequentialInStream+2(%rip), %cl
	jne	.LBB22_85
# BB#70:
	movb	3(%rsi), %cl
	cmpb	IID_ISequentialInStream+3(%rip), %cl
	jne	.LBB22_85
# BB#71:
	movb	4(%rsi), %cl
	cmpb	IID_ISequentialInStream+4(%rip), %cl
	jne	.LBB22_85
# BB#72:
	movb	5(%rsi), %cl
	cmpb	IID_ISequentialInStream+5(%rip), %cl
	jne	.LBB22_85
# BB#73:
	movb	6(%rsi), %cl
	cmpb	IID_ISequentialInStream+6(%rip), %cl
	jne	.LBB22_85
# BB#74:
	movb	7(%rsi), %cl
	cmpb	IID_ISequentialInStream+7(%rip), %cl
	jne	.LBB22_85
# BB#75:
	movb	8(%rsi), %cl
	cmpb	IID_ISequentialInStream+8(%rip), %cl
	jne	.LBB22_85
# BB#76:
	movb	9(%rsi), %cl
	cmpb	IID_ISequentialInStream+9(%rip), %cl
	jne	.LBB22_85
# BB#77:
	movb	10(%rsi), %cl
	cmpb	IID_ISequentialInStream+10(%rip), %cl
	jne	.LBB22_85
# BB#78:
	movb	11(%rsi), %cl
	cmpb	IID_ISequentialInStream+11(%rip), %cl
	jne	.LBB22_85
# BB#79:
	movb	12(%rsi), %cl
	cmpb	IID_ISequentialInStream+12(%rip), %cl
	jne	.LBB22_85
# BB#80:
	movb	13(%rsi), %cl
	cmpb	IID_ISequentialInStream+13(%rip), %cl
	jne	.LBB22_85
# BB#81:
	movb	14(%rsi), %cl
	cmpb	IID_ISequentialInStream+14(%rip), %cl
	jne	.LBB22_85
# BB#82:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %cl
	cmpb	IID_ISequentialInStream+15(%rip), %cl
	jne	.LBB22_85
# BB#83:
	leaq	32(%rdi), %rax
.LBB22_84:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB22_85:                              # %_ZeqRK4GUIDS1_.exit14.thread
	popq	%rcx
	retq
.Lfunc_end22:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end22-_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	40(%rdi), %eax
	incl	%eax
	movl	%eax, 40(%rdi)
	retq
.Lfunc_end23:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end23-_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi147:
	.cfi_def_cfa_offset 16
	movl	40(%rdi), %eax
	decl	%eax
	movl	%eax, 40(%rdi)
	jne	.LBB24_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB24_2:
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end24-_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev,@function
_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev: # @_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi148:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi149:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi150:
	.cfi_def_cfa_offset 32
.Lcfi151:
	.cfi_offset %rbx, -24
.Lcfi152:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+264, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+328, 32(%rbx)
	leaq	112(%rbx), %rdi
.Ltmp67:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp68:
# BB#1:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp73:
	callq	*16(%rax)
.Ltmp74:
.LBB25_3:                               # %_ZN5NBitl12CBaseDecoderI9CInBufferED2Ev.exit
	leaq	48(%rbx), %rdi
.Ltmp85:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp86:
# BB#4:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp91:
	callq	*16(%rax)
.Ltmp92:
.LBB25_6:                               # %_ZN10COutBufferD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB25_20:
.Ltmp93:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB25_13:
.Ltmp75:
	movq	%rax, %r14
	jmp	.LBB25_14
.LBB25_10:
.Ltmp87:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_21
# BB#11:
	movq	(%rdi), %rax
.Ltmp88:
	callq	*16(%rax)
.Ltmp89:
	jmp	.LBB25_21
.LBB25_12:
.Ltmp90:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB25_7:
.Ltmp69:
	movq	%rax, %r14
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_14
# BB#8:
	movq	(%rdi), %rax
.Ltmp70:
	callq	*16(%rax)
.Ltmp71:
.LBB25_14:                              # %.body
	leaq	48(%rbx), %rdi
.Ltmp76:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp77:
# BB#15:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_21
# BB#16:
	movq	(%rdi), %rax
.Ltmp82:
	callq	*16(%rax)
.Ltmp83:
.LBB25_21:                              # %_ZN10COutBufferD2Ev.exit14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB25_9:
.Ltmp72:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB25_22:
.Ltmp84:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB25_17:
.Ltmp78:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_23
# BB#18:
	movq	(%rdi), %rax
.Ltmp79:
	callq	*16(%rax)
.Ltmp80:
.LBB25_23:                              # %.body12
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB25_19:
.Ltmp81:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end25:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev, .Lfunc_end25-_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp67-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin3   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin3   #     jumps to .Ltmp75
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin3   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin3   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp88-.Ltmp92         #   Call between .Ltmp92 and .Ltmp88
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin3   #     jumps to .Ltmp90
	.byte	1                       #   On action: 1
	.long	.Ltmp70-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin3   #     jumps to .Ltmp72
	.byte	1                       #   On action: 1
	.long	.Ltmp76-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin3   #     jumps to .Ltmp78
	.byte	1                       #   On action: 1
	.long	.Ltmp82-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin3   #     jumps to .Ltmp84
	.byte	1                       #   On action: 1
	.long	.Ltmp83-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp79-.Ltmp83         #   Call between .Ltmp83 and .Ltmp79
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin3   # >> Call Site 11 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin3   #     jumps to .Ltmp81
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoderD0Ev,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoderD0Ev,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoderD0Ev,@function
_ZN9NCompress8NDeflate8NDecoder6CCoderD0Ev: # @_ZN9NCompress8NDeflate8NDecoder6CCoderD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi153:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi154:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi155:
	.cfi_def_cfa_offset 32
.Lcfi156:
	.cfi_offset %rbx, -24
.Lcfi157:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp94:
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp95:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB26_2:
.Ltmp96:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end26:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoderD0Ev, .Lfunc_end26-_ZN9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp94-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin4   #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.long	.Ltmp95-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end26-.Ltmp95    #   Call between .Ltmp95 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end27:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end27-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	32(%rdi), %eax
	incl	%eax
	movl	%eax, 32(%rdi)
	retq
.Lfunc_end28:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end28-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi158:
	.cfi_def_cfa_offset 16
	movl	32(%rdi), %eax
	decl	%eax
	movl	%eax, 32(%rdi)
	jne	.LBB29_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB29_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end29:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end29-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD1Ev,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD1Ev,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD1Ev,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD1Ev: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end30:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD1Ev, .Lfunc_end30-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD0Ev,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD0Ev,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD0Ev,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD0Ev: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi161:
	.cfi_def_cfa_offset 32
.Lcfi162:
	.cfi_offset %rbx, -24
.Lcfi163:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp97:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp98:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder6CCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB31_2:
.Ltmp99:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end31:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD0Ev, .Lfunc_end31-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table31:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp97-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin5   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end31-.Ltmp98    #   Call between .Ltmp98 and .Lfunc_end31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end32:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end32-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end33:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end33-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi164:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB34_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB34_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end34:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end34-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD1Ev,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD1Ev,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD1Ev,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD1Ev: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end35:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD1Ev, .Lfunc_end35-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD0Ev,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD0Ev,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD0Ev,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD0Ev: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi165:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi166:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi167:
	.cfi_def_cfa_offset 32
.Lcfi168:
	.cfi_offset %rbx, -24
.Lcfi169:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp100:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp101:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder6CCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB36_2:
.Ltmp102:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end36:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD0Ev, .Lfunc_end36-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp100-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin6  #     jumps to .Ltmp102
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Lfunc_end36-.Ltmp101   #   Call between .Ltmp101 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end37:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end37-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end38:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end38-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi170:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB39_2
# BB#1:
	addq	$-24, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB39_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end39:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end39-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD1Ev,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD1Ev,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD1Ev,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD1Ev: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end40:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD1Ev, .Lfunc_end40-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD0Ev,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD0Ev,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD0Ev,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD0Ev: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi173:
	.cfi_def_cfa_offset 32
.Lcfi174:
	.cfi_offset %rbx, -24
.Lcfi175:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-24, %rbx
.Ltmp103:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp104:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder6CCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB41_2:
.Ltmp105:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end41:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD0Ev, .Lfunc_end41-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table41:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp103-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin7  #     jumps to .Ltmp105
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Lfunc_end41-.Ltmp104   #   Call between .Ltmp104 and .Lfunc_end41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end42:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end42-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end43:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end43-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi176:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB44_2
# BB#1:
	addq	$-32, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB44_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end44:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end44-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD1Ev,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD1Ev,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD1Ev,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD1Ev: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end45:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD1Ev, .Lfunc_end45-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD0Ev,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD0Ev,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD0Ev,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD0Ev: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi177:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi178:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi179:
	.cfi_def_cfa_offset 32
.Lcfi180:
	.cfi_offset %rbx, -24
.Lcfi181:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-32, %rbx
.Ltmp106:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp107:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder6CCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB46_2:
.Ltmp108:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end46:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD0Ev, .Lfunc_end46-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table46:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp106-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin8  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp107-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Lfunc_end46-.Ltmp107   #   Call between .Ltmp107 and .Lfunc_end46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTVN9NCompress8NDeflate8NDecoder6CCoderE,@object # @_ZTVN9NCompress8NDeflate8NDecoder6CCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress8NDeflate8NDecoder6CCoderE
	.p2align	3
_ZTVN9NCompress8NDeflate8NDecoder6CCoderE:
	.quad	0
	.quad	_ZTIN9NCompress8NDeflate8NDecoder6CCoderE
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder10CodeResumeEP20ISequentialOutStreamPKyP21ICompressProgressInfo
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy
	.quad	-8
	.quad	_ZTIN9NCompress8NDeflate8NDecoder6CCoderE
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy
	.quad	-16
	.quad	_ZTIN9NCompress8NDeflate8NDecoder6CCoderE
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv
	.quad	-24
	.quad	_ZTIN9NCompress8NDeflate8NDecoder6CCoderE
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy
	.quad	-32
	.quad	_ZTIN9NCompress8NDeflate8NDecoder6CCoderE
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD1Ev
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoderD0Ev
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj
	.size	_ZTVN9NCompress8NDeflate8NDecoder6CCoderE, 376

	.type	_ZN9NCompress8NDeflateL24kCodeLengthAlphabetOrderE,@object # @_ZN9NCompress8NDeflateL24kCodeLengthAlphabetOrderE
	.p2align	4
_ZN9NCompress8NDeflateL24kCodeLengthAlphabetOrderE:
	.ascii	"\020\021\022\000\b\007\t\006\n\005\013\004\f\003\r\002\016\001\017"
	.size	_ZN9NCompress8NDeflateL24kCodeLengthAlphabetOrderE, 19

	.type	_ZN9NCompress8NDeflateL11kLenStart64E,@object # @_ZN9NCompress8NDeflateL11kLenStart64E
	.p2align	4
_ZN9NCompress8NDeflateL11kLenStart64E:
	.asciz	"\000\001\002\003\004\005\006\007\b\n\f\016\020\024\030\034 (08@P`p\200\240\300\340\000\000"
	.size	_ZN9NCompress8NDeflateL11kLenStart64E, 31

	.type	_ZN9NCompress8NDeflateL16kLenDirectBits64E,@object # @_ZN9NCompress8NDeflateL16kLenDirectBits64E
	.p2align	4
_ZN9NCompress8NDeflateL16kLenDirectBits64E:
	.asciz	"\000\000\000\000\000\000\000\000\001\001\001\001\002\002\002\002\003\003\003\003\004\004\004\004\005\005\005\005\020\000"
	.size	_ZN9NCompress8NDeflateL16kLenDirectBits64E, 31

	.type	_ZN9NCompress8NDeflateL11kLenStart32E,@object # @_ZN9NCompress8NDeflateL11kLenStart32E
	.p2align	4
_ZN9NCompress8NDeflateL11kLenStart32E:
	.asciz	"\000\001\002\003\004\005\006\007\b\n\f\016\020\024\030\034 (08@P`p\200\240\300\340\377\000"
	.size	_ZN9NCompress8NDeflateL11kLenStart32E, 31

	.type	_ZN9NCompress8NDeflateL16kLenDirectBits32E,@object # @_ZN9NCompress8NDeflateL16kLenDirectBits32E
	.p2align	4
_ZN9NCompress8NDeflateL16kLenDirectBits32E:
	.asciz	"\000\000\000\000\000\000\000\000\001\001\001\001\002\002\002\002\003\003\003\003\004\004\004\004\005\005\005\005\000\000"
	.size	_ZN9NCompress8NDeflateL16kLenDirectBits32E, 31

	.type	_ZN9NCompress8NDeflateL10kDistStartE,@object # @_ZN9NCompress8NDeflateL10kDistStartE
	.p2align	4
_ZN9NCompress8NDeflateL10kDistStartE:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	16                      # 0x10
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	48                      # 0x30
	.long	64                      # 0x40
	.long	96                      # 0x60
	.long	128                     # 0x80
	.long	192                     # 0xc0
	.long	256                     # 0x100
	.long	384                     # 0x180
	.long	512                     # 0x200
	.long	768                     # 0x300
	.long	1024                    # 0x400
	.long	1536                    # 0x600
	.long	2048                    # 0x800
	.long	3072                    # 0xc00
	.long	4096                    # 0x1000
	.long	6144                    # 0x1800
	.long	8192                    # 0x2000
	.long	12288                   # 0x3000
	.long	16384                   # 0x4000
	.long	24576                   # 0x6000
	.long	32768                   # 0x8000
	.long	49152                   # 0xc000
	.size	_ZN9NCompress8NDeflateL10kDistStartE, 128

	.type	_ZN9NCompress8NDeflateL15kDistDirectBitsE,@object # @_ZN9NCompress8NDeflateL15kDistDirectBitsE
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
_ZN9NCompress8NDeflateL15kDistDirectBitsE:
	.ascii	"\000\000\000\000\001\001\002\002\003\003\004\004\005\005\006\006\007\007\b\b\t\t\n\n\013\013\f\f\r\r\016\016"
	.size	_ZN9NCompress8NDeflateL15kDistDirectBitsE, 32

	.type	_ZTS18CInBufferException,@object # @_ZTS18CInBufferException
	.section	.rodata._ZTS18CInBufferException,"aG",@progbits,_ZTS18CInBufferException,comdat
	.weak	_ZTS18CInBufferException
	.p2align	4
_ZTS18CInBufferException:
	.asciz	"18CInBufferException"
	.size	_ZTS18CInBufferException, 21

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	_ZTI18CInBufferException,@object # @_ZTI18CInBufferException
	.section	.rodata._ZTI18CInBufferException,"aG",@progbits,_ZTI18CInBufferException,comdat
	.weak	_ZTI18CInBufferException
	.p2align	4
_ZTI18CInBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18CInBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI18CInBufferException, 24

	.type	_ZTS19COutBufferException,@object # @_ZTS19COutBufferException
	.section	.rodata._ZTS19COutBufferException,"aG",@progbits,_ZTS19COutBufferException,comdat
	.weak	_ZTS19COutBufferException
	.p2align	4
_ZTS19COutBufferException:
	.asciz	"19COutBufferException"
	.size	_ZTS19COutBufferException, 22

	.type	_ZTI19COutBufferException,@object # @_ZTI19COutBufferException
	.section	.rodata._ZTI19COutBufferException,"aG",@progbits,_ZTI19COutBufferException,comdat
	.weak	_ZTI19COutBufferException
	.p2align	4
_ZTI19COutBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19COutBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI19COutBufferException, 24

	.type	_ZTSN9NCompress8NDeflate8NDecoder6CCoderE,@object # @_ZTSN9NCompress8NDeflate8NDecoder6CCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTSN9NCompress8NDeflate8NDecoder6CCoderE
	.p2align	4
_ZTSN9NCompress8NDeflate8NDecoder6CCoderE:
	.asciz	"N9NCompress8NDeflate8NDecoder6CCoderE"
	.size	_ZTSN9NCompress8NDeflate8NDecoder6CCoderE, 38

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS33ICompressGetInStreamProcessedSize,@object # @_ZTS33ICompressGetInStreamProcessedSize
	.section	.rodata._ZTS33ICompressGetInStreamProcessedSize,"aG",@progbits,_ZTS33ICompressGetInStreamProcessedSize,comdat
	.weak	_ZTS33ICompressGetInStreamProcessedSize
	.p2align	4
_ZTS33ICompressGetInStreamProcessedSize:
	.asciz	"33ICompressGetInStreamProcessedSize"
	.size	_ZTS33ICompressGetInStreamProcessedSize, 36

	.type	_ZTI33ICompressGetInStreamProcessedSize,@object # @_ZTI33ICompressGetInStreamProcessedSize
	.section	.rodata._ZTI33ICompressGetInStreamProcessedSize,"aG",@progbits,_ZTI33ICompressGetInStreamProcessedSize,comdat
	.weak	_ZTI33ICompressGetInStreamProcessedSize
	.p2align	4
_ZTI33ICompressGetInStreamProcessedSize:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS33ICompressGetInStreamProcessedSize
	.quad	_ZTI8IUnknown
	.size	_ZTI33ICompressGetInStreamProcessedSize, 24

	.type	_ZTS20ICompressSetInStream,@object # @_ZTS20ICompressSetInStream
	.section	.rodata._ZTS20ICompressSetInStream,"aG",@progbits,_ZTS20ICompressSetInStream,comdat
	.weak	_ZTS20ICompressSetInStream
	.p2align	4
_ZTS20ICompressSetInStream:
	.asciz	"20ICompressSetInStream"
	.size	_ZTS20ICompressSetInStream, 23

	.type	_ZTI20ICompressSetInStream,@object # @_ZTI20ICompressSetInStream
	.section	.rodata._ZTI20ICompressSetInStream,"aG",@progbits,_ZTI20ICompressSetInStream,comdat
	.weak	_ZTI20ICompressSetInStream
	.p2align	4
_ZTI20ICompressSetInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ICompressSetInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ICompressSetInStream, 24

	.type	_ZTS25ICompressSetOutStreamSize,@object # @_ZTS25ICompressSetOutStreamSize
	.section	.rodata._ZTS25ICompressSetOutStreamSize,"aG",@progbits,_ZTS25ICompressSetOutStreamSize,comdat
	.weak	_ZTS25ICompressSetOutStreamSize
	.p2align	4
_ZTS25ICompressSetOutStreamSize:
	.asciz	"25ICompressSetOutStreamSize"
	.size	_ZTS25ICompressSetOutStreamSize, 28

	.type	_ZTI25ICompressSetOutStreamSize,@object # @_ZTI25ICompressSetOutStreamSize
	.section	.rodata._ZTI25ICompressSetOutStreamSize,"aG",@progbits,_ZTI25ICompressSetOutStreamSize,comdat
	.weak	_ZTI25ICompressSetOutStreamSize
	.p2align	4
_ZTI25ICompressSetOutStreamSize:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS25ICompressSetOutStreamSize
	.quad	_ZTI8IUnknown
	.size	_ZTI25ICompressSetOutStreamSize, 24

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress8NDeflate8NDecoder6CCoderE,@object # @_ZTIN9NCompress8NDeflate8NDecoder6CCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress8NDeflate8NDecoder6CCoderE
	.p2align	4
_ZTIN9NCompress8NDeflate8NDecoder6CCoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress8NDeflate8NDecoder6CCoderE
	.long	1                       # 0x1
	.long	6                       # 0x6
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI33ICompressGetInStreamProcessedSize
	.quad	2050                    # 0x802
	.quad	_ZTI20ICompressSetInStream
	.quad	4098                    # 0x1002
	.quad	_ZTI25ICompressSetOutStreamSize
	.quad	6146                    # 0x1802
	.quad	_ZTI19ISequentialInStream
	.quad	8194                    # 0x2002
	.quad	_ZTI13CMyUnknownImp
	.quad	10242                   # 0x2802
	.size	_ZTIN9NCompress8NDeflate8NDecoder6CCoderE, 120


	.globl	_ZN9NCompress8NDeflate8NDecoder6CCoderC1Ebb
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoderC1Ebb,@function
_ZN9NCompress8NDeflate8NDecoder6CCoderC1Ebb = _ZN9NCompress8NDeflate8NDecoder6CCoderC2Ebb
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
