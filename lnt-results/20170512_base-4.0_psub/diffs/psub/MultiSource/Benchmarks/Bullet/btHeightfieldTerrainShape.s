	.text
	.file	"btHeightfieldTerrainShape.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_1:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZN25btHeightfieldTerrainShapeC2EiiPvfffi14PHY_ScalarTypeb
	.p2align	4, 0x90
	.type	_ZN25btHeightfieldTerrainShapeC2EiiPvfffi14PHY_ScalarTypeb,@function
_ZN25btHeightfieldTerrainShapeC2EiiPvfffi14PHY_ScalarTypeb: # @_ZN25btHeightfieldTerrainShapeC2EiiPvfffi14PHY_ScalarTypeb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, %r12d
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rcx, %r15
	movl	%edx, %r13d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	_ZN14btConcaveShapeC2Ev
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	movq	$_ZTV25btHeightfieldTerrainShape+16, (%rbx)
	movl	$24, 8(%rbx)
	movl	%ebp, 76(%rbx)
	movl	%r13d, 80(%rbx)
	movss	%xmm3, 84(%rbx)
	leaq	88(%rbx), %rdi
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 88(%rbx)
	decl	%ebp
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	leaq	92(%rbx), %rax
	movss	%xmm0, 92(%rbx)
	decl	%r13d
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r13d, %xmm0
	leaq	96(%rbx), %rcx
	movss	%xmm0, 96(%rbx)
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 100(%rbx)
	movq	%r15, 104(%rbx)
	movl	%r14d, 112(%rbx)
	movb	96(%rsp), %dl
	movb	%dl, 116(%rbx)
	movb	$0, 117(%rbx)
	movl	%r12d, 120(%rbx)
	movl	$1065353216, 124(%rbx)  # imm = 0x3F800000
	movl	$1065353216, 128(%rbx)  # imm = 0x3F800000
	movl	$1065353216, 132(%rbx)  # imm = 0x3F800000
	movl	$0, 136(%rbx)
	testl	%r12d, %r12d
	je	.LBB0_4
# BB#1:
	cmpl	$1, %r12d
	je	.LBB0_5
# BB#2:
	cmpl	$2, %r12d
	jne	.LBB0_3
# BB#6:
	movq	$0, 28(%rbx)
	xorps	%xmm0, %xmm0
	movq	%rax, %rsi
	movq	%rcx, %rax
	movq	%rdi, %rcx
	jmp	.LBB0_7
.LBB0_4:
	movss	%xmm3, 28(%rbx)
	movl	$0, 32(%rbx)
	xorps	%xmm0, %xmm0
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	xorps	%xmm3, %xmm3
	movq	%rdi, %rsi
	jmp	.LBB0_7
.LBB0_5:
	movl	$0, 28(%rbx)
	movss	%xmm3, 32(%rbx)
	xorps	%xmm1, %xmm1
	shufps	$0, %xmm1, %xmm3        # xmm3 = xmm3[0,0],xmm1[0,0]
	movaps	%xmm3, %xmm0
	shufps	$226, %xmm1, %xmm0      # xmm0 = xmm0[2,0],xmm1[2,3]
	xorps	%xmm3, %xmm3
	movq	%rax, %rsi
	movq	%rdi, %rax
.LBB0_7:                                # %.sink.split.i
	movss	%xmm3, 36(%rbx)
	movl	$0, 40(%rbx)
	movl	(%rsi), %edx
	movl	%edx, 44(%rbx)
	movl	(%rax), %eax
	movl	%eax, 48(%rbx)
	movl	(%rcx), %ecx
	movl	%ecx, 52(%rbx)
	movl	$0, 56(%rbx)
	movd	%edx, %xmm1
	movd	%eax, %xmm2
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movd	%ecx, %xmm2
	jmp	.LBB0_8
.LBB0_3:                                # %._crit_edge.i
	movsd	28(%rbx), %xmm0         # xmm0 = mem[0],zero
	movq	44(%rbx), %xmm1         # xmm1 = mem[0],zero
	movss	36(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movd	52(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
.LBB0_8:
	addps	%xmm0, %xmm1
	addss	%xmm3, %xmm2
	mulps	.LCPI0_0(%rip), %xmm1
	mulss	.LCPI0_1(%rip), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, 60(%rbx)
	movlps	%xmm0, 68(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN25btHeightfieldTerrainShapeC2EiiPvfffi14PHY_ScalarTypeb, .Lfunc_end0-_ZN25btHeightfieldTerrainShapeC2EiiPvfffi14PHY_ScalarTypeb
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_1:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZN25btHeightfieldTerrainShape10initializeEiiPvfffi14PHY_ScalarTypeb
	.p2align	4, 0x90
	.type	_ZN25btHeightfieldTerrainShape10initializeEiiPvfffi14PHY_ScalarTypeb,@function
_ZN25btHeightfieldTerrainShape10initializeEiiPvfffi14PHY_ScalarTypeb: # @_ZN25btHeightfieldTerrainShape10initializeEiiPvfffi14PHY_ScalarTypeb
	.cfi_startproc
# BB#0:
	movb	8(%rsp), %r11b
	movl	$24, 8(%rdi)
	movl	%esi, 76(%rdi)
	movl	%edx, 80(%rdi)
	movss	%xmm1, 84(%rdi)
	leaq	88(%rdi), %r10
	movss	%xmm2, 88(%rdi)
	decl	%esi
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%esi, %xmm2
	leaq	92(%rdi), %rax
	movss	%xmm2, 92(%rdi)
	decl	%edx
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%edx, %xmm2
	leaq	96(%rdi), %rdx
	movss	%xmm2, 96(%rdi)
	movss	%xmm0, 100(%rdi)
	movq	%rcx, 104(%rdi)
	movl	%r9d, 112(%rdi)
	movb	%r11b, 116(%rdi)
	movb	$0, 117(%rdi)
	movl	%r8d, 120(%rdi)
	movl	$1065353216, 124(%rdi)  # imm = 0x3F800000
	movl	$1065353216, 128(%rdi)  # imm = 0x3F800000
	movl	$1065353216, 132(%rdi)  # imm = 0x3F800000
	movl	$0, 136(%rdi)
	testl	%r8d, %r8d
	je	.LBB1_4
# BB#1:
	cmpl	$1, %r8d
	je	.LBB1_5
# BB#2:
	cmpl	$2, %r8d
	jne	.LBB1_3
# BB#6:
	movq	$0, 28(%rdi)
	xorps	%xmm0, %xmm0
	movq	%rax, %rcx
	movq	%rdx, %rax
	movq	%r10, %rdx
	jmp	.LBB1_7
.LBB1_4:
	movss	%xmm1, 28(%rdi)
	movl	$0, 32(%rdi)
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	xorps	%xmm1, %xmm1
	movq	%r10, %rcx
	jmp	.LBB1_7
.LBB1_5:
	movl	$0, 28(%rdi)
	movss	%xmm1, 32(%rdi)
	xorps	%xmm2, %xmm2
	shufps	$0, %xmm2, %xmm1        # xmm1 = xmm1[0,0],xmm2[0,0]
	movaps	%xmm1, %xmm0
	shufps	$226, %xmm2, %xmm0      # xmm0 = xmm0[2,0],xmm2[2,3]
	xorps	%xmm1, %xmm1
	movq	%rax, %rcx
	movq	%r10, %rax
.LBB1_7:                                # %.sink.split
	movss	%xmm1, 36(%rdi)
	movl	$0, 40(%rdi)
	movl	(%rcx), %ecx
	movl	%ecx, 44(%rdi)
	movl	(%rax), %eax
	movl	%eax, 48(%rdi)
	movl	(%rdx), %edx
	movl	%edx, 52(%rdi)
	movl	$0, 56(%rdi)
	movd	%ecx, %xmm3
	movd	%eax, %xmm2
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movd	%edx, %xmm2
	jmp	.LBB1_8
.LBB1_3:                                # %._crit_edge
	movsd	28(%rdi), %xmm0         # xmm0 = mem[0],zero
	movq	44(%rdi), %xmm3         # xmm3 = mem[0],zero
	movss	36(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movd	52(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
.LBB1_8:
	addps	%xmm3, %xmm0
	addss	%xmm1, %xmm2
	mulps	.LCPI1_0(%rip), %xmm0
	mulss	.LCPI1_1(%rip), %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, 60(%rdi)
	movlps	%xmm1, 68(%rdi)
	retq
.Lfunc_end1:
	.size	_ZN25btHeightfieldTerrainShape10initializeEiiPvfffi14PHY_ScalarTypeb, .Lfunc_end1-_ZN25btHeightfieldTerrainShape10initializeEiiPvfffi14PHY_ScalarTypeb
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	1199570688              # float 65535
.LCPI2_2:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_1:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.text
	.globl	_ZN25btHeightfieldTerrainShapeC2EiiPvfibb
	.p2align	4, 0x90
	.type	_ZN25btHeightfieldTerrainShapeC2EiiPvfibb,@function
_ZN25btHeightfieldTerrainShapeC2EiiPvfibb: # @_ZN25btHeightfieldTerrainShapeC2EiiPvfibb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%r9d, %r15d
	movl	%r8d, %r12d
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	%rcx, %r14
	movl	%edx, %r13d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	_ZN14btConcaveShapeC2Ev
	movq	$_ZTV25btHeightfieldTerrainShape+16, (%rbx)
	xorl	%eax, %eax
	testb	%r15b, %r15b
	sete	%al
	leal	(%rax,%rax,4), %esi
	movl	$24, 8(%rbx)
	movl	%ebp, 76(%rbx)
	movl	%r13d, 80(%rbx)
	movl	$0, 84(%rbx)
	leaq	88(%rbx), %rdi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 88(%rbx)
	divss	.LCPI2_0(%rip), %xmm0
	movaps	%xmm0, %xmm1
	decl	%ebp
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	leaq	92(%rbx), %rax
	movss	%xmm0, 92(%rbx)
	decl	%r13d
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r13d, %xmm0
	leaq	96(%rbx), %rcx
	movss	%xmm0, 96(%rbx)
	movss	%xmm1, 100(%rbx)
	movq	%r14, 104(%rbx)
	movl	%esi, 112(%rbx)
	movb	64(%rsp), %dl
	movb	%dl, 116(%rbx)
	movb	$0, 117(%rbx)
	movl	%r12d, 120(%rbx)
	movl	$1065353216, 124(%rbx)  # imm = 0x3F800000
	movl	$1065353216, 128(%rbx)  # imm = 0x3F800000
	movl	$1065353216, 132(%rbx)  # imm = 0x3F800000
	movl	$0, 136(%rbx)
	testl	%r12d, %r12d
	je	.LBB2_4
# BB#1:
	cmpl	$1, %r12d
	je	.LBB2_5
# BB#2:
	cmpl	$2, %r12d
	jne	.LBB2_3
# BB#6:
	movq	$0, 28(%rbx)
	movq	%rax, %rsi
	movq	%rcx, %rax
	movq	%rdi, %rcx
	jmp	.LBB2_7
.LBB2_4:
	movq	$0, 28(%rbx)
	movq	%rdi, %rsi
	jmp	.LBB2_7
.LBB2_5:
	movq	$0, 28(%rbx)
	movq	%rax, %rsi
	movq	%rdi, %rax
.LBB2_7:                                # %.sink.split.i
	movq	$0, 36(%rbx)
	movl	(%rsi), %edx
	movl	%edx, 44(%rbx)
	movl	(%rax), %eax
	movl	%eax, 48(%rbx)
	movl	(%rcx), %ecx
	movl	%ecx, 52(%rbx)
	movl	$0, 56(%rbx)
	movd	%edx, %xmm0
	movd	%eax, %xmm1
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movd	%ecx, %xmm1
	xorps	%xmm2, %xmm2
	xorps	%xmm3, %xmm3
	jmp	.LBB2_8
.LBB2_3:                                # %._crit_edge.i
	movsd	28(%rbx), %xmm2         # xmm2 = mem[0],zero
	movq	44(%rbx), %xmm0         # xmm0 = mem[0],zero
	movss	36(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movd	52(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
.LBB2_8:
	addps	%xmm2, %xmm0
	addss	%xmm3, %xmm1
	mulps	.LCPI2_1(%rip), %xmm0
	mulss	.LCPI2_2(%rip), %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 60(%rbx)
	movlps	%xmm2, 68(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN25btHeightfieldTerrainShapeC2EiiPvfibb, .Lfunc_end2-_ZN25btHeightfieldTerrainShapeC2EiiPvfibb
	.cfi_endproc

	.globl	_ZN25btHeightfieldTerrainShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN25btHeightfieldTerrainShapeD2Ev,@function
_ZN25btHeightfieldTerrainShapeD2Ev:     # @_ZN25btHeightfieldTerrainShapeD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV25btHeightfieldTerrainShape+16, (%rdi)
	jmp	_ZN14btConcaveShapeD2Ev # TAILCALL
.Lfunc_end3:
	.size	_ZN25btHeightfieldTerrainShapeD2Ev, .Lfunc_end3-_ZN25btHeightfieldTerrainShapeD2Ev
	.cfi_endproc

	.globl	_ZN25btHeightfieldTerrainShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN25btHeightfieldTerrainShapeD0Ev,@function
_ZN25btHeightfieldTerrainShapeD0Ev:     # @_ZN25btHeightfieldTerrainShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV25btHeightfieldTerrainShape+16, (%rbx)
.Ltmp0:
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp1:
# BB#1:                                 # %_ZN25btHeightfieldTerrainShapeD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB4_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN25btHeightfieldTerrainShapeD0Ev, .Lfunc_end4-_ZN25btHeightfieldTerrainShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end4-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZNK25btHeightfieldTerrainShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK25btHeightfieldTerrainShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK25btHeightfieldTerrainShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK25btHeightfieldTerrainShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 32
	subq	$96, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 128
.Lcfi35:
	.cfi_offset %rbx, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movss	44(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	28(%rbx), %xmm1
	movss	48(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	32(%rbx), %xmm2
	movss	52(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	36(%rbx), %xmm0
	mulss	124(%rbx), %xmm1
	mulss	128(%rbx), %xmm2
	mulss	132(%rbx), %xmm0
	movss	.LCPI5_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm2
	mulss	%xmm3, %xmm0
	movss	16(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm10         # xmm10 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movaps	.LCPI5_1(%rip), %xmm8   # xmm8 = [nan,nan,nan,nan]
	andps	%xmm8, %xmm4
	movss	20(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm10   # xmm10 = xmm10[0],xmm5[0],xmm10[1],xmm5[1]
	andps	%xmm8, %xmm10
	movss	24(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm5    # xmm5 = xmm5[0],xmm6[0],xmm5[1],xmm6[1]
	andps	%xmm8, %xmm5
	movss	32(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	andps	%xmm8, %xmm6
	movss	36(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	andps	%xmm8, %xmm7
	movss	40(%rsi), %xmm9         # xmm9 = mem[0],zero,zero,zero
	andps	%xmm8, %xmm9
	movsd	48(%rsi), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm3, 80(%rsp)         # 16-byte Spill
	movss	56(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	mulss	%xmm1, %xmm6
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	mulss	%xmm2, %xmm7
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm10, %xmm2
	addps	%xmm1, %xmm2
	mulss	%xmm0, %xmm9
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm5, %xmm0
	addps	%xmm2, %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	addss	%xmm6, %xmm7
	addss	%xmm7, %xmm9
	movaps	%xmm9, 64(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	callq	*88(%rax)
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	32(%rsp), %xmm2         # 16-byte Reload
	unpcklps	16(%rsp), %xmm2 # 16-byte Folded Reload
                                        # xmm2 = xmm2[0],mem[0],xmm2[1],mem[1]
	addps	48(%rsp), %xmm2         # 16-byte Folded Reload
	addss	64(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	80(%rsp), %xmm6         # 16-byte Reload
	movaps	%xmm6, %xmm1
	subps	%xmm2, %xmm1
	movaps	%xmm2, %xmm7
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm2
	subss	%xmm0, %xmm2
	xorps	%xmm3, %xmm3
	xorps	%xmm4, %xmm4
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
	movlps	%xmm1, (%r15)
	movlps	%xmm4, 8(%r15)
	addps	%xmm6, %xmm7
	addss	%xmm5, %xmm0
	movss	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1,2,3]
	movlps	%xmm7, (%r14)
	movlps	%xmm3, 8(%r14)
	addq	$96, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	_ZNK25btHeightfieldTerrainShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end5-_ZNK25btHeightfieldTerrainShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.globl	_ZNK25btHeightfieldTerrainShape22getRawHeightFieldValueEii
	.p2align	4, 0x90
	.type	_ZNK25btHeightfieldTerrainShape22getRawHeightFieldValueEii,@function
_ZNK25btHeightfieldTerrainShape22getRawHeightFieldValueEii: # @_ZNK25btHeightfieldTerrainShape22getRawHeightFieldValueEii
	.cfi_startproc
# BB#0:
	movl	112(%rdi), %eax
	cmpl	$5, %eax
	je	.LBB6_4
# BB#1:
	cmpl	$3, %eax
	je	.LBB6_5
# BB#2:
	xorps	%xmm0, %xmm0
	testl	%eax, %eax
	jne	.LBB6_7
# BB#3:
	movq	104(%rdi), %rax
	imull	76(%rdi), %edx
	addl	%esi, %edx
	movslq	%edx, %rcx
	movss	(%rax,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	retq
.LBB6_4:
	movq	104(%rdi), %rax
	imull	76(%rdi), %edx
	addl	%esi, %edx
	movslq	%edx, %rcx
	movzbl	(%rax,%rcx), %eax
	jmp	.LBB6_6
.LBB6_5:
	movq	104(%rdi), %rax
	imull	76(%rdi), %edx
	addl	%esi, %edx
	movslq	%edx, %rcx
	movswl	(%rax,%rcx,2), %eax
.LBB6_6:
	cvtsi2ssl	%eax, %xmm0
	mulss	100(%rdi), %xmm0
.LBB6_7:
	retq
.Lfunc_end6:
	.size	_ZNK25btHeightfieldTerrainShape22getRawHeightFieldValueEii, .Lfunc_end6-_ZNK25btHeightfieldTerrainShape22getRawHeightFieldValueEii
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_0:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3,@function
_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3: # @_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 48
.Lcfi43:
	.cfi_offset %rbx, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*104(%rax)
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.LBB7_4
# BB#1:
	cmpl	$1, %eax
	je	.LBB7_6
# BB#2:
	cmpl	$2, %eax
	jne	.LBB7_3
# BB#7:
	movss	.LCPI7_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	92(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	cvtsi2ssl	%ebp, %xmm3
	subss	%xmm1, %xmm3
	mulss	96(%rbx), %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r15d, %xmm1
	subss	%xmm2, %xmm1
	subss	68(%rbx), %xmm0
	movaps	%xmm0, %xmm2
	movaps	%xmm3, %xmm0
	jmp	.LBB7_8
.LBB7_4:
	subss	60(%rbx), %xmm0
	movss	.LCPI7_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	cvtsi2ssl	%ebp, %xmm1
	subss	%xmm2, %xmm1
	jmp	.LBB7_5
.LBB7_6:
	movss	.LCPI7_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	movaps	%xmm0, %xmm1
	cvtsi2ssl	%ebp, %xmm0
	subss	%xmm2, %xmm0
	subss	64(%rbx), %xmm1
.LBB7_5:                                # %.sink.split
	mulss	96(%rbx), %xmm3
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%r15d, %xmm2
	subss	%xmm3, %xmm2
.LBB7_8:                                # %.sink.split
	movss	%xmm0, (%r14)
	movss	%xmm1, 4(%r14)
	leaq	8(%r14), %rax
	movss	%xmm2, 8(%r14)
	movl	$0, 12(%r14)
	leaq	4(%r14), %rcx
	jmp	.LBB7_9
.LBB7_3:                                # %._crit_edge
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	leaq	4(%r14), %rcx
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movq	%r14, %rax
	addq	$8, %rax
.LBB7_9:
	mulss	124(%rbx), %xmm0
	movss	%xmm0, (%r14)
	mulss	128(%rbx), %xmm1
	movss	%xmm1, (%rcx)
	mulss	132(%rbx), %xmm2
	movss	%xmm2, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3, .Lfunc_end7-_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_0:
	.quad	-4620693217682128896    # double -0.5
.LCPI8_1:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	_ZNK25btHeightfieldTerrainShape17quantizeWithClampEPiRK9btVector3i
	.p2align	4, 0x90
	.type	_ZNK25btHeightfieldTerrainShape17quantizeWithClampEPiRK9btVector3i,@function
_ZNK25btHeightfieldTerrainShape17quantizeWithClampEPiRK9btVector3i: # @_ZNK25btHeightfieldTerrainShape17quantizeWithClampEPiRK9btVector3i
	.cfi_startproc
# BB#0:                                 # %_Z8btSetMaxIfEvRT_RKS0_.exit.i
	movss	28(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	32(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	maxss	(%rdx), %xmm0
	maxss	4(%rdx), %xmm1
	movss	36(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	maxss	8(%rdx), %xmm4
	movss	44(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	minss	%xmm0, %xmm2
	movss	48(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	minss	%xmm1, %xmm5
	movss	52(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm6
	xorps	%xmm3, %xmm3
	ucomiss	%xmm2, %xmm3
	movsd	.LCPI8_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI8_1(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm7
	ja	.LBB8_2
# BB#1:                                 # %_Z8btSetMaxIfEvRT_RKS0_.exit.i
	movapd	%xmm2, %xmm7
.LBB8_2:                                # %_Z8btSetMaxIfEvRT_RKS0_.exit.i
	minss	%xmm4, %xmm1
	addsd	%xmm7, %xmm6
	cvttsd2si	%xmm6, %eax
	movl	%eax, (%rsi)
	xorps	%xmm4, %xmm4
	cvtss2sd	%xmm5, %xmm4
	ucomiss	%xmm5, %xmm3
	movapd	%xmm0, %xmm5
	ja	.LBB8_4
# BB#3:                                 # %_Z8btSetMaxIfEvRT_RKS0_.exit.i
	movapd	%xmm2, %xmm5
.LBB8_4:                                # %_Z8btSetMaxIfEvRT_RKS0_.exit.i
	addsd	%xmm5, %xmm4
	cvttsd2si	%xmm4, %eax
	movl	%eax, 4(%rsi)
	xorps	%xmm4, %xmm4
	cvtss2sd	%xmm1, %xmm4
	ucomiss	%xmm1, %xmm3
	ja	.LBB8_6
# BB#5:                                 # %_Z8btSetMaxIfEvRT_RKS0_.exit.i
	movapd	%xmm2, %xmm0
.LBB8_6:                                # %_Z8btSetMaxIfEvRT_RKS0_.exit.i
	addsd	%xmm0, %xmm4
	cvttsd2si	%xmm4, %eax
	movl	%eax, 8(%rsi)
	retq
.Lfunc_end8:
	.size	_ZNK25btHeightfieldTerrainShape17quantizeWithClampEPiRK9btVector3i, .Lfunc_end8-_ZNK25btHeightfieldTerrainShape17quantizeWithClampEPiRK9btVector3i
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	1065353216              # float 1
.LCPI9_3:
	.long	1056964608              # float 0.5
.LCPI9_4:
	.long	0                       # float 0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI9_1:
	.quad	-4620693217682128896    # double -0.5
.LCPI9_2:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	_ZNK25btHeightfieldTerrainShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK25btHeightfieldTerrainShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_,@function
_ZNK25btHeightfieldTerrainShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_: # @_ZNK25btHeightfieldTerrainShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi53:
	.cfi_def_cfa_offset 128
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movss	.LCPI9_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	divss	124(%rbx), %xmm4
	movaps	%xmm3, %xmm5
	divss	128(%rbx), %xmm5
	divss	132(%rbx), %xmm3
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	4(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm6
	movss	8(%rdx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm7
	mulss	(%rcx), %xmm4
	mulss	4(%rcx), %xmm5
	mulss	8(%rcx), %xmm3
	movss	60(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	addss	%xmm0, %xmm1
	movss	64(%rbx), %xmm15        # xmm15 = mem[0],zero,zero,zero
	addss	%xmm15, %xmm6
	movss	68(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	addss	%xmm8, %xmm7
	movss	28(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	32(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	maxss	%xmm1, %xmm0
	movaps	%xmm2, %xmm1
	maxss	%xmm6, %xmm1
	movss	36(%rbx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm13
	maxss	%xmm7, %xmm13
	movss	44(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm7
	minss	%xmm0, %xmm7
	movss	48(%rbx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm6
	minss	%xmm1, %xmm6
	movss	52(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	ucomiss	%xmm7, %xmm0
	movsd	.LCPI9_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm0
	ja	.LBB9_2
# BB#1:
	movsd	.LCPI9_2(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB9_2:
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movaps	%xmm12, %xmm0
	minss	%xmm13, %xmm0
	movss	%xmm0, (%rsp)           # 4-byte Spill
	xorps	%xmm0, %xmm0
	ucomiss	%xmm6, %xmm0
	movapd	%xmm1, %xmm13
	ja	.LBB9_4
# BB#3:
	movsd	.LCPI9_2(%rip), %xmm13  # xmm13 = mem[0],zero
.LBB9_4:
	addss	4(%rsp), %xmm4          # 4-byte Folded Reload
	addss	%xmm15, %xmm5
	addss	%xmm8, %xmm3
	xorps	%xmm15, %xmm15
	ucomiss	(%rsp), %xmm15          # 4-byte Folded Reload
	movapd	%xmm1, %xmm8
	ja	.LBB9_6
# BB#5:
	movsd	.LCPI9_2(%rip), %xmm8   # xmm8 = mem[0],zero
.LBB9_6:
	cvtss2sd	%xmm7, %xmm7
	cvtss2sd	%xmm6, %xmm6
	maxss	%xmm4, %xmm9
	maxss	%xmm5, %xmm2
	maxss	%xmm3, %xmm11
	minss	%xmm9, %xmm10
	minss	%xmm2, %xmm14
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm10, %xmm0
	ucomiss	%xmm10, %xmm15
	movapd	%xmm1, %xmm2
	ja	.LBB9_8
# BB#7:
	movsd	.LCPI9_2(%rip), %xmm2   # xmm2 = mem[0],zero
.LBB9_8:
	addsd	8(%rsp), %xmm7          # 8-byte Folded Reload
	addsd	%xmm13, %xmm6
	minss	%xmm11, %xmm12
	addsd	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm14, %xmm2
	ucomiss	%xmm14, %xmm15
	movapd	%xmm1, %xmm3
	ja	.LBB9_10
# BB#9:
	movsd	.LCPI9_2(%rip), %xmm3   # xmm3 = mem[0],zero
.LBB9_10:
	cvttsd2si	%xmm7, %ecx
	cvttsd2si	%xmm6, %r9d
	cvttsd2si	%xmm0, %edx
	addsd	%xmm3, %xmm2
	cvttsd2si	%xmm2, %r8d
	ucomiss	%xmm12, %xmm15
	ja	.LBB9_12
# BB#11:
	movsd	.LCPI9_2(%rip), %xmm1   # xmm1 = mem[0],zero
.LBB9_12:
	decl	%ecx
	incl	%edx
	decl	%r9d
	incl	%r8d
	movl	76(%rbx), %esi
	decl	%esi
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movl	80(%rbx), %eax
	decl	%eax
	movl	120(%rbx), %ebp
	cmpl	$2, %ebp
	je	.LBB9_17
# BB#13:
	movss	(%rsp), %xmm0           # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	addsd	%xmm8, %xmm0
	cvttsd2si	%xmm0, %edi
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm12, %xmm0
	addsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %esi
	decl	%edi
	incl	%esi
	cmpl	$1, %ebp
	je	.LBB9_16
# BB#14:
	movl	$0, 64(%rsp)            # 4-byte Folded Spill
	xorl	%r15d, %r15d
	testl	%ebp, %ebp
	jne	.LBB9_18
# BB#15:
	testl	%r9d, %r9d
	cmovsl	%r15d, %r9d
	movl	4(%rsp), %ecx           # 4-byte Reload
	cmpl	%ecx, %r8d
	cmovlel	%r8d, %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	testl	%edi, %edi
	cmovnsl	%edi, %r15d
	cmpl	%eax, %esi
	cmovlel	%esi, %eax
	movl	%r9d, 64(%rsp)          # 4-byte Spill
	jmp	.LBB9_18
.LBB9_17:
	xorl	%r15d, %r15d
	testl	%ecx, %ecx
	cmovsl	%r15d, %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	movl	4(%rsp), %ecx           # 4-byte Reload
	cmpl	%ecx, %edx
	cmovlel	%edx, %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	testl	%r9d, %r9d
	cmovnsl	%r9d, %r15d
	cmpl	%eax, %r8d
	cmovlel	%r8d, %eax
	jmp	.LBB9_18
.LBB9_16:
	xorl	%r15d, %r15d
	testl	%ecx, %ecx
	cmovsl	%r15d, %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	movl	4(%rsp), %ecx           # 4-byte Reload
	cmpl	%ecx, %edx
	cmovlel	%edx, %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	testl	%edi, %edi
	cmovnsl	%edi, %r15d
	cmpl	%eax, %esi
	cmovlel	%esi, %eax
.LBB9_18:
	movl	%eax, 68(%rsp)          # 4-byte Spill
	cmpl	%eax, %r15d
	jge	.LBB9_29
	.p2align	4, 0x90
.LBB9_19:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_21 Depth 2
	leal	1(%r15), %r13d
	movl	4(%rsp), %eax           # 4-byte Reload
	cmpl	%eax, 64(%rsp)          # 4-byte Folded Reload
	jge	.LBB9_28
# BB#20:                                # %.lr.ph
                                        #   in Loop: Header=BB9_19 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r13d, %xmm0
	movss	%xmm0, (%rsp)           # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r15d, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movl	64(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	.p2align	4, 0x90
.LBB9_21:                               #   Parent Loop BB9_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, 116(%rbx)
	je	.LBB9_22
.LBB9_24:                               #   in Loop: Header=BB9_21 Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%r15d, %edx
	callq	*104(%rax)
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.LBB9_30
# BB#25:                                #   in Loop: Header=BB9_21 Depth=2
	cmpl	$1, %eax
	je	.LBB9_32
# BB#26:                                #   in Loop: Header=BB9_21 Depth=2
	cmpl	$2, %eax
	jne	.LBB9_27
# BB#33:                                #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm1
	cvtsi2ssl	%ebp, %xmm3
	subss	%xmm1, %xmm3
	movss	96(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	subss	68(%rbx), %xmm0
	movaps	%xmm0, %xmm2
	movaps	%xmm3, %xmm0
	jmp	.LBB9_34
	.p2align	4, 0x90
.LBB9_22:                               #   in Loop: Header=BB9_21 Depth=2
	cmpb	$0, 117(%rbx)
	je	.LBB9_81
# BB#23:                                #   in Loop: Header=BB9_21 Depth=2
	leal	(%rbp,%r15), %eax
	testb	$1, %al
	je	.LBB9_24
.LBB9_81:                               #   in Loop: Header=BB9_21 Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%r15d, %edx
	callq	*104(%rax)
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.LBB9_85
# BB#82:                                #   in Loop: Header=BB9_21 Depth=2
	cmpl	$1, %eax
	je	.LBB9_87
# BB#83:                                #   in Loop: Header=BB9_21 Depth=2
	cmpl	$2, %eax
	jne	.LBB9_84
# BB#88:                                #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm1
	cvtsi2ssl	%ebp, %xmm3
	subss	%xmm1, %xmm3
	movss	96(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	subss	68(%rbx), %xmm0
	movaps	%xmm0, %xmm2
	movaps	%xmm3, %xmm0
	jmp	.LBB9_89
.LBB9_30:                               #   in Loop: Header=BB9_21 Depth=2
	subss	60(%rbx), %xmm0
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ebp, %xmm1
	subss	%xmm2, %xmm1
	jmp	.LBB9_31
.LBB9_32:                               #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	movaps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	subss	%xmm2, %xmm0
	subss	64(%rbx), %xmm1
.LBB9_31:                               # %.sink.split.i282
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
.LBB9_34:                               # %.sink.split.i282
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	%xmm0, 16(%rsp)
	movss	%xmm1, 20(%rsp)
	movss	%xmm2, 24(%rsp)
	movl	$0, 28(%rsp)
	jmp	.LBB9_35
.LBB9_27:                               # %._crit_edge.i278
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
.LBB9_35:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit286
                                        #   in Loop: Header=BB9_21 Depth=2
	mulss	124(%rbx), %xmm0
	movss	%xmm0, 16(%rsp)
	mulss	128(%rbx), %xmm1
	movss	%xmm1, 20(%rsp)
	mulss	132(%rbx), %xmm2
	movss	%xmm2, 24(%rsp)
	leal	1(%rbp), %r12d
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	callq	*104(%rax)
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.LBB9_39
# BB#36:                                # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit286
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$1, %eax
	je	.LBB9_41
# BB#37:                                # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit286
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$2, %eax
	jne	.LBB9_38
# BB#42:                                #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm1
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%r12d, %xmm3
	subss	%xmm1, %xmm3
	movss	96(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	subss	68(%rbx), %xmm0
	movaps	%xmm0, %xmm2
	movaps	%xmm3, %xmm0
	jmp	.LBB9_43
.LBB9_39:                               #   in Loop: Header=BB9_21 Depth=2
	subss	60(%rbx), %xmm0
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r12d, %xmm1
	subss	%xmm2, %xmm1
	jmp	.LBB9_40
.LBB9_41:                               #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	movaps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r12d, %xmm0
	subss	%xmm2, %xmm0
	subss	64(%rbx), %xmm1
.LBB9_40:                               # %.sink.split.i267
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
.LBB9_43:                               # %.sink.split.i267
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	%xmm0, 32(%rsp)
	movss	%xmm1, 36(%rsp)
	movss	%xmm2, 40(%rsp)
	movl	$0, 44(%rsp)
	jmp	.LBB9_44
.LBB9_38:                               # %._crit_edge.i263
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	32(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	40(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
.LBB9_44:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit271
                                        #   in Loop: Header=BB9_21 Depth=2
	mulss	124(%rbx), %xmm0
	movss	%xmm0, 32(%rsp)
	mulss	128(%rbx), %xmm1
	movss	%xmm1, 36(%rsp)
	mulss	132(%rbx), %xmm2
	movss	%xmm2, 40(%rsp)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movl	%r13d, %edx
	callq	*104(%rax)
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.LBB9_48
# BB#45:                                # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit271
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$1, %eax
	je	.LBB9_50
# BB#46:                                # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit271
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$2, %eax
	jne	.LBB9_47
# BB#51:                                #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm1
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%r12d, %xmm3
	subss	%xmm1, %xmm3
	movss	96(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	movss	(%rsp), %xmm1           # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	subss	68(%rbx), %xmm0
	movaps	%xmm0, %xmm2
	movaps	%xmm3, %xmm0
	jmp	.LBB9_52
.LBB9_48:                               #   in Loop: Header=BB9_21 Depth=2
	subss	60(%rbx), %xmm0
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r12d, %xmm1
	subss	%xmm2, %xmm1
	jmp	.LBB9_49
.LBB9_50:                               #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	movaps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r12d, %xmm0
	subss	%xmm2, %xmm0
	subss	64(%rbx), %xmm1
.LBB9_49:                               # %.sink.split.i252
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
.LBB9_52:                               # %.sink.split.i252
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	%xmm0, 48(%rsp)
	movss	%xmm1, 52(%rsp)
	movss	%xmm2, 56(%rsp)
	movl	$0, 60(%rsp)
	jmp	.LBB9_53
.LBB9_47:                               # %._crit_edge.i248
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	48(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	52(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	56(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
.LBB9_53:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit256
                                        #   in Loop: Header=BB9_21 Depth=2
	mulss	124(%rbx), %xmm0
	movss	%xmm0, 48(%rsp)
	mulss	128(%rbx), %xmm1
	movss	%xmm1, 52(%rsp)
	mulss	132(%rbx), %xmm2
	movss	%xmm2, 56(%rsp)
	movq	(%r14), %rax
	movq	%r14, %rdi
	leaq	16(%rsp), %rsi
	movl	%ebp, %edx
	movl	%r15d, %ecx
	callq	*16(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%r15d, %edx
	callq	*104(%rax)
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.LBB9_57
# BB#54:                                # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit256
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$1, %eax
	je	.LBB9_59
# BB#55:                                # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit256
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$2, %eax
	jne	.LBB9_56
# BB#60:                                #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm1
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ebp, %xmm3
	subss	%xmm1, %xmm3
	movss	96(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	subss	68(%rbx), %xmm0
	movaps	%xmm0, %xmm2
	movaps	%xmm3, %xmm0
	jmp	.LBB9_61
.LBB9_57:                               #   in Loop: Header=BB9_21 Depth=2
	subss	60(%rbx), %xmm0
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ebp, %xmm1
	subss	%xmm2, %xmm1
	jmp	.LBB9_58
.LBB9_59:                               #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	movaps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	subss	%xmm2, %xmm0
	subss	64(%rbx), %xmm1
.LBB9_58:                               # %.sink.split.i219
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
.LBB9_61:                               # %.sink.split.i219
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	%xmm0, 16(%rsp)
	movss	%xmm1, 20(%rsp)
	movss	%xmm2, 24(%rsp)
	movl	$0, 28(%rsp)
	jmp	.LBB9_62
.LBB9_56:                               # %._crit_edge.i215
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
.LBB9_62:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit223
                                        #   in Loop: Header=BB9_21 Depth=2
	mulss	124(%rbx), %xmm0
	movss	%xmm0, 16(%rsp)
	mulss	128(%rbx), %xmm1
	movss	%xmm1, 20(%rsp)
	mulss	132(%rbx), %xmm2
	movss	%xmm2, 24(%rsp)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movl	%r13d, %edx
	callq	*104(%rax)
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.LBB9_66
# BB#63:                                # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit223
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$1, %eax
	je	.LBB9_68
# BB#64:                                # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit223
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$2, %eax
	jne	.LBB9_65
# BB#69:                                #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm1
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%r12d, %xmm3
	subss	%xmm1, %xmm3
	movss	96(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	movss	(%rsp), %xmm1           # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	subss	68(%rbx), %xmm0
	movaps	%xmm0, %xmm2
	movaps	%xmm3, %xmm0
	jmp	.LBB9_70
.LBB9_66:                               #   in Loop: Header=BB9_21 Depth=2
	subss	60(%rbx), %xmm0
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r12d, %xmm1
	subss	%xmm2, %xmm1
	jmp	.LBB9_67
.LBB9_68:                               #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	movaps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r12d, %xmm0
	subss	%xmm2, %xmm0
	subss	64(%rbx), %xmm1
.LBB9_67:                               # %.sink.split.i204
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
.LBB9_70:                               # %.sink.split.i204
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	%xmm0, 32(%rsp)
	movss	%xmm1, 36(%rsp)
	movss	%xmm2, 40(%rsp)
	movl	$0, 44(%rsp)
	jmp	.LBB9_71
.LBB9_65:                               # %._crit_edge.i200
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	32(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	40(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
.LBB9_71:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit208
                                        #   in Loop: Header=BB9_21 Depth=2
	mulss	124(%rbx), %xmm0
	movss	%xmm0, 32(%rsp)
	mulss	128(%rbx), %xmm1
	movss	%xmm1, 36(%rsp)
	mulss	132(%rbx), %xmm2
	movss	%xmm2, 40(%rsp)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%r13d, %edx
	callq	*104(%rax)
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.LBB9_75
# BB#72:                                # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit208
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$1, %eax
	je	.LBB9_78
# BB#73:                                # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit208
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$2, %eax
	jne	.LBB9_74
# BB#80:                                #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm1
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ebp, %xmm3
	jmp	.LBB9_130
.LBB9_75:                               #   in Loop: Header=BB9_21 Depth=2
	subss	60(%rbx), %xmm0
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ebp, %xmm1
	jmp	.LBB9_76
.LBB9_78:                               #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	movaps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	jmp	.LBB9_79
.LBB9_85:                               #   in Loop: Header=BB9_21 Depth=2
	subss	60(%rbx), %xmm0
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ebp, %xmm1
	subss	%xmm2, %xmm1
	jmp	.LBB9_86
.LBB9_87:                               #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	movaps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	subss	%xmm2, %xmm0
	subss	64(%rbx), %xmm1
.LBB9_86:                               # %.sink.split.i168
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
.LBB9_89:                               # %.sink.split.i168
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	%xmm0, 16(%rsp)
	movss	%xmm1, 20(%rsp)
	movss	%xmm2, 24(%rsp)
	movl	$0, 28(%rsp)
	jmp	.LBB9_90
.LBB9_84:                               # %._crit_edge.i164
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
.LBB9_90:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit172
                                        #   in Loop: Header=BB9_21 Depth=2
	mulss	124(%rbx), %xmm0
	movss	%xmm0, 16(%rsp)
	mulss	128(%rbx), %xmm1
	movss	%xmm1, 20(%rsp)
	mulss	132(%rbx), %xmm2
	movss	%xmm2, 24(%rsp)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%r13d, %edx
	callq	*104(%rax)
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.LBB9_94
# BB#91:                                # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit172
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$1, %eax
	je	.LBB9_96
# BB#92:                                # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit172
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$2, %eax
	jne	.LBB9_93
# BB#97:                                #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm1
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ebp, %xmm3
	subss	%xmm1, %xmm3
	movss	96(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	movss	(%rsp), %xmm1           # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	subss	68(%rbx), %xmm0
	movaps	%xmm0, %xmm2
	movaps	%xmm3, %xmm0
	jmp	.LBB9_98
.LBB9_94:                               #   in Loop: Header=BB9_21 Depth=2
	subss	60(%rbx), %xmm0
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ebp, %xmm1
	subss	%xmm2, %xmm1
	jmp	.LBB9_95
.LBB9_96:                               #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	movaps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	subss	%xmm2, %xmm0
	subss	64(%rbx), %xmm1
.LBB9_95:                               # %.sink.split.i153
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
.LBB9_98:                               # %.sink.split.i153
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	%xmm0, 32(%rsp)
	movss	%xmm1, 36(%rsp)
	movss	%xmm2, 40(%rsp)
	movl	$0, 44(%rsp)
	jmp	.LBB9_99
.LBB9_93:                               # %._crit_edge.i149
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	32(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	40(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
.LBB9_99:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit157
                                        #   in Loop: Header=BB9_21 Depth=2
	mulss	124(%rbx), %xmm0
	movss	%xmm0, 32(%rsp)
	mulss	128(%rbx), %xmm1
	movss	%xmm1, 36(%rsp)
	mulss	132(%rbx), %xmm2
	movss	%xmm2, 40(%rsp)
	leal	1(%rbp), %r12d
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	callq	*104(%rax)
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.LBB9_103
# BB#100:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit157
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$1, %eax
	je	.LBB9_105
# BB#101:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit157
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$2, %eax
	jne	.LBB9_102
# BB#106:                               #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm1
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%r12d, %xmm3
	subss	%xmm1, %xmm3
	movss	96(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	subss	68(%rbx), %xmm0
	movaps	%xmm0, %xmm2
	movaps	%xmm3, %xmm0
	jmp	.LBB9_107
.LBB9_103:                              #   in Loop: Header=BB9_21 Depth=2
	subss	60(%rbx), %xmm0
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r12d, %xmm1
	subss	%xmm2, %xmm1
	jmp	.LBB9_104
.LBB9_105:                              #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	movaps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r12d, %xmm0
	subss	%xmm2, %xmm0
	subss	64(%rbx), %xmm1
.LBB9_104:                              # %.sink.split.i138
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
.LBB9_107:                              # %.sink.split.i138
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	%xmm0, 48(%rsp)
	movss	%xmm1, 52(%rsp)
	movss	%xmm2, 56(%rsp)
	movl	$0, 60(%rsp)
	jmp	.LBB9_108
.LBB9_102:                              # %._crit_edge.i134
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	48(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	52(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	56(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
.LBB9_108:                              # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit142
                                        #   in Loop: Header=BB9_21 Depth=2
	mulss	124(%rbx), %xmm0
	movss	%xmm0, 48(%rsp)
	mulss	128(%rbx), %xmm1
	movss	%xmm1, 52(%rsp)
	mulss	132(%rbx), %xmm2
	movss	%xmm2, 56(%rsp)
	movq	(%r14), %rax
	movq	%r14, %rdi
	leaq	16(%rsp), %rsi
	movl	%ebp, %edx
	movl	%r15d, %ecx
	callq	*16(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	callq	*104(%rax)
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.LBB9_112
# BB#109:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit142
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$1, %eax
	je	.LBB9_114
# BB#110:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit142
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$2, %eax
	jne	.LBB9_111
# BB#115:                               #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm1
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%r12d, %xmm3
	subss	%xmm1, %xmm3
	movss	96(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	subss	68(%rbx), %xmm0
	movaps	%xmm0, %xmm2
	movaps	%xmm3, %xmm0
	jmp	.LBB9_116
.LBB9_112:                              #   in Loop: Header=BB9_21 Depth=2
	subss	60(%rbx), %xmm0
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r12d, %xmm1
	subss	%xmm2, %xmm1
	jmp	.LBB9_113
.LBB9_114:                              #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	movaps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r12d, %xmm0
	subss	%xmm2, %xmm0
	subss	64(%rbx), %xmm1
.LBB9_113:                              # %.sink.split.i123
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
.LBB9_116:                              # %.sink.split.i123
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	%xmm0, 16(%rsp)
	movss	%xmm1, 20(%rsp)
	movss	%xmm2, 24(%rsp)
	movl	$0, 28(%rsp)
	jmp	.LBB9_117
.LBB9_111:                              # %._crit_edge.i119
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
.LBB9_117:                              # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit127
                                        #   in Loop: Header=BB9_21 Depth=2
	mulss	124(%rbx), %xmm0
	movss	%xmm0, 16(%rsp)
	mulss	128(%rbx), %xmm1
	movss	%xmm1, 20(%rsp)
	mulss	132(%rbx), %xmm2
	movss	%xmm2, 24(%rsp)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%r13d, %edx
	callq	*104(%rax)
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.LBB9_121
# BB#118:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit127
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$1, %eax
	je	.LBB9_123
# BB#119:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit127
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$2, %eax
	jne	.LBB9_120
# BB#124:                               #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm1
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ebp, %xmm3
	subss	%xmm1, %xmm3
	movss	96(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	movss	(%rsp), %xmm1           # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	subss	68(%rbx), %xmm0
	movaps	%xmm0, %xmm2
	movaps	%xmm3, %xmm0
	jmp	.LBB9_125
.LBB9_121:                              #   in Loop: Header=BB9_21 Depth=2
	subss	60(%rbx), %xmm0
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ebp, %xmm1
	subss	%xmm2, %xmm1
	jmp	.LBB9_122
.LBB9_123:                              #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	movaps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	subss	%xmm2, %xmm0
	subss	64(%rbx), %xmm1
.LBB9_122:                              # %.sink.split.i108
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
.LBB9_125:                              # %.sink.split.i108
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	%xmm0, 32(%rsp)
	movss	%xmm1, 36(%rsp)
	movss	%xmm2, 40(%rsp)
	movl	$0, 44(%rsp)
	jmp	.LBB9_126
.LBB9_120:                              # %._crit_edge.i104
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	32(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	40(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
.LBB9_126:                              # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit112
                                        #   in Loop: Header=BB9_21 Depth=2
	mulss	124(%rbx), %xmm0
	movss	%xmm0, 32(%rsp)
	mulss	128(%rbx), %xmm1
	movss	%xmm1, 36(%rsp)
	mulss	132(%rbx), %xmm2
	movss	%xmm2, 40(%rsp)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movl	%r13d, %edx
	callq	*104(%rax)
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.LBB9_133
# BB#127:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit112
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$1, %eax
	je	.LBB9_134
# BB#128:                               # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit112
                                        #   in Loop: Header=BB9_21 Depth=2
	cmpl	$2, %eax
	jne	.LBB9_74
# BB#129:                               #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm1
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%r12d, %xmm3
.LBB9_130:                              # %.sink.split.i
                                        #   in Loop: Header=BB9_21 Depth=2
	subss	%xmm1, %xmm3
	movss	96(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	movss	(%rsp), %xmm1           # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	subss	68(%rbx), %xmm0
	movaps	%xmm0, %xmm2
	movaps	%xmm3, %xmm0
	jmp	.LBB9_131
	.p2align	4, 0x90
.LBB9_74:                               # %._crit_edge.i185
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	48(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	52(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	56(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB9_132
.LBB9_133:                              #   in Loop: Header=BB9_21 Depth=2
	subss	60(%rbx), %xmm0
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r12d, %xmm1
.LBB9_76:                               # %.sink.split.i189
                                        #   in Loop: Header=BB9_21 Depth=2
	subss	%xmm2, %xmm1
	jmp	.LBB9_77
.LBB9_134:                              #   in Loop: Header=BB9_21 Depth=2
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI9_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	movaps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r12d, %xmm0
.LBB9_79:                               # %.sink.split.i189
                                        #   in Loop: Header=BB9_21 Depth=2
	subss	%xmm2, %xmm0
	subss	64(%rbx), %xmm1
.LBB9_77:                               # %.sink.split.i189
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
.LBB9_131:                              # %.sink.split.i
                                        #   in Loop: Header=BB9_21 Depth=2
	movss	%xmm0, 48(%rsp)
	movss	%xmm1, 52(%rsp)
	movss	%xmm2, 56(%rsp)
	movl	$0, 60(%rsp)
.LBB9_132:                              # %_ZNK25btHeightfieldTerrainShape9getVertexEiiR9btVector3.exit
                                        #   in Loop: Header=BB9_21 Depth=2
	mulss	124(%rbx), %xmm0
	movss	%xmm0, 48(%rsp)
	mulss	128(%rbx), %xmm1
	movss	%xmm1, 52(%rsp)
	mulss	132(%rbx), %xmm2
	movss	%xmm2, 56(%rsp)
	movq	(%r14), %rax
	movq	%r14, %rdi
	leaq	16(%rsp), %rsi
	movl	%ebp, %edx
	movl	%r15d, %ecx
	callq	*16(%rax)
	cmpl	4(%rsp), %r12d          # 4-byte Folded Reload
	movl	%r12d, %ebp
	jne	.LBB9_21
.LBB9_28:                               # %._crit_edge
                                        #   in Loop: Header=BB9_19 Depth=1
	cmpl	68(%rsp), %r13d         # 4-byte Folded Reload
	movl	%r13d, %r15d
	jne	.LBB9_19
.LBB9_29:                               # %._crit_edge340
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_ZNK25btHeightfieldTerrainShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_, .Lfunc_end9-_ZNK25btHeightfieldTerrainShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.cfi_endproc

	.globl	_ZNK25btHeightfieldTerrainShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK25btHeightfieldTerrainShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK25btHeightfieldTerrainShape21calculateLocalInertiaEfR9btVector3: # @_ZNK25btHeightfieldTerrainShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
	retq
.Lfunc_end10:
	.size	_ZNK25btHeightfieldTerrainShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end10-_ZNK25btHeightfieldTerrainShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.globl	_ZN25btHeightfieldTerrainShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN25btHeightfieldTerrainShape15setLocalScalingERK9btVector3,@function
_ZN25btHeightfieldTerrainShape15setLocalScalingERK9btVector3: # @_ZN25btHeightfieldTerrainShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	movups	(%rsi), %xmm0
	movups	%xmm0, 124(%rdi)
	retq
.Lfunc_end11:
	.size	_ZN25btHeightfieldTerrainShape15setLocalScalingERK9btVector3, .Lfunc_end11-_ZN25btHeightfieldTerrainShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.globl	_ZNK25btHeightfieldTerrainShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK25btHeightfieldTerrainShape15getLocalScalingEv,@function
_ZNK25btHeightfieldTerrainShape15getLocalScalingEv: # @_ZNK25btHeightfieldTerrainShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	124(%rdi), %rax
	retq
.Lfunc_end12:
	.size	_ZNK25btHeightfieldTerrainShape15getLocalScalingEv, .Lfunc_end12-_ZNK25btHeightfieldTerrainShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK25btHeightfieldTerrainShape7getNameEv,"axG",@progbits,_ZNK25btHeightfieldTerrainShape7getNameEv,comdat
	.weak	_ZNK25btHeightfieldTerrainShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK25btHeightfieldTerrainShape7getNameEv,@function
_ZNK25btHeightfieldTerrainShape7getNameEv: # @_ZNK25btHeightfieldTerrainShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end13:
	.size	_ZNK25btHeightfieldTerrainShape7getNameEv, .Lfunc_end13-_ZNK25btHeightfieldTerrainShape7getNameEv
	.cfi_endproc

	.section	.text._ZN14btConcaveShape9setMarginEf,"axG",@progbits,_ZN14btConcaveShape9setMarginEf,comdat
	.weak	_ZN14btConcaveShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN14btConcaveShape9setMarginEf,@function
_ZN14btConcaveShape9setMarginEf:        # @_ZN14btConcaveShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 24(%rdi)
	retq
.Lfunc_end14:
	.size	_ZN14btConcaveShape9setMarginEf, .Lfunc_end14-_ZN14btConcaveShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK14btConcaveShape9getMarginEv,"axG",@progbits,_ZNK14btConcaveShape9getMarginEv,comdat
	.weak	_ZNK14btConcaveShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK14btConcaveShape9getMarginEv,@function
_ZNK14btConcaveShape9getMarginEv:       # @_ZNK14btConcaveShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	24(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end15:
	.size	_ZNK14btConcaveShape9getMarginEv, .Lfunc_end15-_ZNK14btConcaveShape9getMarginEv
	.cfi_endproc

	.type	_ZTV25btHeightfieldTerrainShape,@object # @_ZTV25btHeightfieldTerrainShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV25btHeightfieldTerrainShape
	.p2align	3
_ZTV25btHeightfieldTerrainShape:
	.quad	0
	.quad	_ZTI25btHeightfieldTerrainShape
	.quad	_ZN25btHeightfieldTerrainShapeD2Ev
	.quad	_ZN25btHeightfieldTerrainShapeD0Ev
	.quad	_ZNK25btHeightfieldTerrainShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN25btHeightfieldTerrainShape15setLocalScalingERK9btVector3
	.quad	_ZNK25btHeightfieldTerrainShape15getLocalScalingEv
	.quad	_ZNK25btHeightfieldTerrainShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK25btHeightfieldTerrainShape7getNameEv
	.quad	_ZN14btConcaveShape9setMarginEf
	.quad	_ZNK14btConcaveShape9getMarginEv
	.quad	_ZNK25btHeightfieldTerrainShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.quad	_ZNK25btHeightfieldTerrainShape22getRawHeightFieldValueEii
	.size	_ZTV25btHeightfieldTerrainShape, 128

	.type	_ZTS25btHeightfieldTerrainShape,@object # @_ZTS25btHeightfieldTerrainShape
	.globl	_ZTS25btHeightfieldTerrainShape
	.p2align	4
_ZTS25btHeightfieldTerrainShape:
	.asciz	"25btHeightfieldTerrainShape"
	.size	_ZTS25btHeightfieldTerrainShape, 28

	.type	_ZTI25btHeightfieldTerrainShape,@object # @_ZTI25btHeightfieldTerrainShape
	.globl	_ZTI25btHeightfieldTerrainShape
	.p2align	4
_ZTI25btHeightfieldTerrainShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS25btHeightfieldTerrainShape
	.quad	_ZTI14btConcaveShape
	.size	_ZTI25btHeightfieldTerrainShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"HEIGHTFIELD"
	.size	.L.str, 12


	.globl	_ZN25btHeightfieldTerrainShapeC1EiiPvfffi14PHY_ScalarTypeb
	.type	_ZN25btHeightfieldTerrainShapeC1EiiPvfffi14PHY_ScalarTypeb,@function
_ZN25btHeightfieldTerrainShapeC1EiiPvfffi14PHY_ScalarTypeb = _ZN25btHeightfieldTerrainShapeC2EiiPvfffi14PHY_ScalarTypeb
	.globl	_ZN25btHeightfieldTerrainShapeC1EiiPvfibb
	.type	_ZN25btHeightfieldTerrainShapeC1EiiPvfibb,@function
_ZN25btHeightfieldTerrainShapeC1EiiPvfibb = _ZN25btHeightfieldTerrainShapeC2EiiPvfibb
	.globl	_ZN25btHeightfieldTerrainShapeD1Ev
	.type	_ZN25btHeightfieldTerrainShapeD1Ev,@function
_ZN25btHeightfieldTerrainShapeD1Ev = _ZN25btHeightfieldTerrainShapeD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
