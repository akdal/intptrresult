	.text
	.file	"ldump.bc"
	.hidden	luaU_dump
	.globl	luaU_dump
	.p2align	4, 0x90
	.type	luaU_dump,@function
luaU_dump:                              # @luaU_dump
	.cfi_startproc
# BB#0:                                 # %DumpHeader.exit
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 96
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r13, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rcx, %r12
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	%rbx, (%rsp)
	movq	%r14, 8(%rsp)
	movq	%r12, 16(%rsp)
	movl	%r8d, 24(%rsp)
	leaq	36(%rsp), %r13
	movq	%r13, %rdi
	callq	luaU_header
	movl	$12, %edx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%r12, %rcx
	callq	*%r14
	movl	%eax, 28(%rsp)
	movq	%rsp, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	DumpFunction
	movl	28(%rsp), %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	luaU_dump, .Lfunc_end0-luaU_dump
	.cfi_endproc

	.p2align	4, 0x90
	.type	DumpFunction,@function
DumpFunction:                           # @DumpFunction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 80
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rdi, %r14
	movq	64(%r14), %r15
	cmpq	%rsi, %r15
	je	.LBB1_3
# BB#1:
	testq	%r15, %r15
	je	.LBB1_3
# BB#2:
	movl	24(%r13), %eax
	testl	%eax, %eax
	jne	.LBB1_3
# BB#5:
	movq	16(%r15), %rax
	incq	%rax
	movq	%rax, 8(%rsp)
	movl	28(%r13), %eax
	testl	%eax, %eax
	jne	.LBB1_9
# BB#6:                                 # %DumpBlock.exit7.i
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	leaq	8(%rsp), %rsi
	movl	$8, %edx
	callq	*8(%r13)
	movl	%eax, 28(%r13)
	testl	%eax, %eax
	jne	.LBB1_9
# BB#7:
	addq	$24, %r15
	movq	8(%rsp), %rdx
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movq	%r15, %rsi
	jmp	.LBB1_8
.LBB1_3:                                # %.thread
	movq	$0, 8(%rsp)
	movl	28(%r13), %eax
	testl	%eax, %eax
	jne	.LBB1_9
# BB#4:
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	leaq	8(%rsp), %rsi
	movl	$8, %edx
.LBB1_8:                                # %DumpBlock.exit8.i
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_9:                                # %DumpBlock.exit8.i
	leaq	28(%r13), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	96(%r14), %ecx
	movl	%ecx, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_11
# BB#10:
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	leaq	8(%rsp), %rsi
	movl	$4, %edx
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_11:                               # %DumpInt.exit
	movl	100(%r14), %ecx
	movl	%ecx, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_13
# BB#12:
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	leaq	8(%rsp), %rsi
	movl	$4, %edx
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_13:                               # %DumpInt.exit26
	movb	112(%r14), %cl
	movb	%cl, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_15
# BB#14:
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	leaq	8(%rsp), %rsi
	movl	$1, %edx
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_15:                               # %DumpChar.exit
	movb	113(%r14), %cl
	movb	%cl, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_17
# BB#16:
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	leaq	8(%rsp), %rsi
	movl	$1, %edx
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_17:                               # %DumpChar.exit29
	movb	114(%r14), %cl
	movb	%cl, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_19
# BB#18:
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	leaq	8(%rsp), %rsi
	movl	$1, %edx
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_19:                               # %DumpChar.exit31
	movb	115(%r14), %cl
	movb	%cl, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_21
# BB#20:
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	leaq	8(%rsp), %rsi
	movl	$1, %edx
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_21:                               # %DumpChar.exit33
	movq	24(%r14), %r15
	movslq	80(%r14), %rbx
	movl	%ebx, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_23
# BB#22:                                # %DumpInt.exit.i34
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	leaq	8(%rsp), %rsi
	movl	$4, %edx
	callq	*8(%r13)
	movl	%eax, 28(%r13)
	testl	%eax, %eax
	je	.LBB1_24
.LBB1_23:                               # %DumpVector.exit.thread
	movl	76(%r14), %ebx
	movl	%ebx, 8(%rsp)
.LBB1_26:                               # %DumpInt.exit.i36
	testl	%ebx, %ebx
	jle	.LBB1_45
.LBB1_27:                               # %.lr.ph43
	movl	%ebx, %r15d
	xorl	%ebp, %ebp
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB1_28:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rbx
	movl	8(%rbx,%rbp), %ecx
	movb	%cl, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_30
# BB#29:                                #   in Loop: Header=BB1_28 Depth=1
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	callq	*8(%r13)
	movl	%eax, 28(%r13)
	movl	8(%rbx,%rbp), %ecx
.LBB1_30:                               # %DumpChar.exit.i
                                        #   in Loop: Header=BB1_28 Depth=1
	cmpl	$4, %ecx
	je	.LBB1_38
# BB#31:                                # %DumpChar.exit.i
                                        #   in Loop: Header=BB1_28 Depth=1
	cmpl	$3, %ecx
	je	.LBB1_36
# BB#32:                                # %DumpChar.exit.i
                                        #   in Loop: Header=BB1_28 Depth=1
	cmpl	$1, %ecx
	jne	.LBB1_44
# BB#33:                                #   in Loop: Header=BB1_28 Depth=1
	movzbl	(%rbx,%rbp), %ecx
	movb	%cl, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_44
# BB#34:                                #   in Loop: Header=BB1_28 Depth=1
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movl	$1, %edx
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_38:                               #   in Loop: Header=BB1_28 Depth=1
	movq	(%rbx,%rbp), %r12
	testq	%r12, %r12
	je	.LBB1_39
# BB#40:                                #   in Loop: Header=BB1_28 Depth=1
	movq	16(%r12), %rcx
	incq	%rcx
	movq	%rcx, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_43
# BB#41:                                # %DumpBlock.exit7.i.i38
                                        #   in Loop: Header=BB1_28 Depth=1
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movl	$8, %edx
	leaq	8(%rsp), %rsi
	callq	*8(%r13)
	movl	%eax, 28(%r13)
	testl	%eax, %eax
	jne	.LBB1_43
# BB#42:                                #   in Loop: Header=BB1_28 Depth=1
	addq	$24, %r12
	movq	8(%rsp), %rdx
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movq	%r12, %rsi
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_43:                               # %DumpBlock.exit8.i.i39
                                        #   in Loop: Header=BB1_28 Depth=1
	leaq	8(%rsp), %r12
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_36:                               #   in Loop: Header=BB1_28 Depth=1
	movq	(%rbx,%rbp), %rcx
	movq	%rcx, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_44
	jmp	.LBB1_37
.LBB1_39:                               #   in Loop: Header=BB1_28 Depth=1
	movq	$0, 8(%rsp)
	testl	%eax, %eax
	leaq	8(%rsp), %r12
	jne	.LBB1_44
.LBB1_37:                               #   in Loop: Header=BB1_28 Depth=1
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movl	$8, %edx
.LBB1_35:                               # %DumpString.exit.i40
                                        #   in Loop: Header=BB1_28 Depth=1
	movq	%r12, %rsi
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_44:                               # %DumpString.exit.i40
                                        #   in Loop: Header=BB1_28 Depth=1
	addq	$16, %rbp
	decq	%r15
	jne	.LBB1_28
.LBB1_45:                               # %._crit_edge
	movl	88(%r14), %ebx
	movl	%ebx, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_47
# BB#46:
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	leaq	8(%rsp), %rsi
	movl	$4, %edx
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_47:                               # %DumpInt.exit33.i
	testl	%ebx, %ebx
	jle	.LBB1_50
# BB#48:                                # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_49:                               # =>This Inner Loop Header: Depth=1
	movq	32(%r14), %rax
	movq	64(%r14), %rsi
	movq	(%rax,%rbp,8), %rdi
	movq	%r13, %rdx
	callq	DumpFunction
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB1_49
.LBB1_50:                               # %DumpConstants.exit
	xorl	%ebp, %ebp
	cmpl	$0, 24(%r13)
	jne	.LBB1_52
# BB#51:
	movl	84(%r14), %ebp
.LBB1_52:
	movq	40(%r14), %rbx
	movl	%ebp, 8(%rsp)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.LBB1_55
# BB#53:                                # %DumpInt.exit.i.i
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	leaq	8(%rsp), %rsi
	movl	$4, %edx
	callq	*8(%r13)
	movl	%eax, 28(%r13)
	testl	%eax, %eax
	jne	.LBB1_55
# BB#54:
	movslq	%ebp, %rdx
	shlq	$2, %rdx
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movq	%rbx, %rsi
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_55:                               # %DumpVector.exit.i
	xorl	%ebx, %ebx
	cmpl	$0, 24(%r13)
	jne	.LBB1_57
# BB#56:
	movl	92(%r14), %ebx
.LBB1_57:
	movl	%ebx, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_59
# BB#58:
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	leaq	8(%rsp), %rsi
	movl	$4, %edx
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_59:                               # %DumpInt.exit.i
	testl	%ebx, %ebx
	jle	.LBB1_73
# BB#60:                                # %.lr.ph45.i
	movl	%ebx, %r12d
	xorl	%ebp, %ebp
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB1_61:                               # =>This Inner Loop Header: Depth=1
	movq	48(%r14), %rcx
	movq	(%rcx,%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB1_62
# BB#64:                                #   in Loop: Header=BB1_61 Depth=1
	movq	16(%rbx), %rcx
	incq	%rcx
	movq	%rcx, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_68
# BB#65:                                # %DumpBlock.exit7.i.i
                                        #   in Loop: Header=BB1_61 Depth=1
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movl	$8, %edx
	movq	%r15, %rsi
	callq	*8(%r13)
	movl	%eax, 28(%r13)
	testl	%eax, %eax
	jne	.LBB1_68
# BB#66:                                #   in Loop: Header=BB1_61 Depth=1
	addq	$24, %rbx
	movq	8(%rsp), %rdx
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movq	%rbx, %rsi
	jmp	.LBB1_67
	.p2align	4, 0x90
.LBB1_62:                               #   in Loop: Header=BB1_61 Depth=1
	movq	$0, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_68
# BB#63:                                #   in Loop: Header=BB1_61 Depth=1
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movl	$8, %edx
	movq	%r15, %rsi
.LBB1_67:                               # %DumpString.exit.i
                                        #   in Loop: Header=BB1_61 Depth=1
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_68:                               # %DumpString.exit.i
                                        #   in Loop: Header=BB1_61 Depth=1
	movq	48(%r14), %rcx
	movl	8(%rcx,%rbp), %edx
	movl	%edx, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_70
# BB#69:                                #   in Loop: Header=BB1_61 Depth=1
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movl	$4, %edx
	movq	%r15, %rsi
	callq	*8(%r13)
	movl	%eax, 28(%r13)
	movq	48(%r14), %rcx
.LBB1_70:                               # %DumpInt.exit34.i
                                        #   in Loop: Header=BB1_61 Depth=1
	movl	12(%rcx,%rbp), %ecx
	movl	%ecx, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_72
# BB#71:                                #   in Loop: Header=BB1_61 Depth=1
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movl	$4, %edx
	movq	%r15, %rsi
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_72:                               # %DumpInt.exit36.i
                                        #   in Loop: Header=BB1_61 Depth=1
	addq	$16, %rbp
	decq	%r12
	jne	.LBB1_61
.LBB1_73:                               # %._crit_edge46.i
	xorl	%ebx, %ebx
	cmpl	$0, 24(%r13)
	jne	.LBB1_75
# BB#74:
	movl	72(%r14), %ebx
.LBB1_75:
	movl	%ebx, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_77
# BB#76:
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	leaq	8(%rsp), %rsi
	movl	$4, %edx
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_77:                               # %DumpInt.exit38.i
	testl	%ebx, %ebx
	jle	.LBB1_87
# BB#78:                                # %.lr.ph.i
	movl	%ebx, %r12d
	xorl	%ebp, %ebp
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB1_79:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rcx
	movq	(%rcx,%rbp,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_80
# BB#82:                                #   in Loop: Header=BB1_79 Depth=1
	movq	16(%rbx), %rcx
	incq	%rcx
	movq	%rcx, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_86
# BB#83:                                # %DumpBlock.exit7.i40.i
                                        #   in Loop: Header=BB1_79 Depth=1
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movl	$8, %edx
	movq	%r15, %rsi
	callq	*8(%r13)
	movl	%eax, 28(%r13)
	testl	%eax, %eax
	jne	.LBB1_86
# BB#84:                                #   in Loop: Header=BB1_79 Depth=1
	addq	$24, %rbx
	movq	8(%rsp), %rdx
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movq	%rbx, %rsi
	jmp	.LBB1_85
	.p2align	4, 0x90
.LBB1_80:                               #   in Loop: Header=BB1_79 Depth=1
	movq	$0, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_86
# BB#81:                                #   in Loop: Header=BB1_79 Depth=1
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movl	$8, %edx
	movq	%r15, %rsi
.LBB1_85:                               # %DumpString.exit42.i
                                        #   in Loop: Header=BB1_79 Depth=1
	callq	*8(%r13)
	movl	%eax, 28(%r13)
.LBB1_86:                               # %DumpString.exit42.i
                                        #   in Loop: Header=BB1_79 Depth=1
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB1_79
.LBB1_87:                               # %DumpDebug.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_24:                               # %DumpVector.exit
	shlq	$2, %rbx
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	*8(%r13)
	movl	%eax, 28(%r13)
	movl	76(%r14), %ebx
	movl	%ebx, 8(%rsp)
	testl	%eax, %eax
	jne	.LBB1_26
# BB#25:
	leaq	8(%rsp), %rsi
	movq	(%r13), %rdi
	movq	16(%r13), %rcx
	movl	$4, %edx
	callq	*8(%r13)
	movl	%eax, 28(%r13)
	testl	%ebx, %ebx
	jg	.LBB1_27
	jmp	.LBB1_45
.Lfunc_end1:
	.size	DumpFunction, .Lfunc_end1-DumpFunction
	.cfi_endproc

	.hidden	luaU_header

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
