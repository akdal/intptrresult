	.text
	.file	"sim.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
.LCPI0_1:
	.quad	-4616189618054758400    # double -1
.LCPI0_2:
	.quad	4596373779694328218     # double 0.20000000000000001
.LCPI0_3:
	.quad	4618441417868443648     # double 6
.LCPI0_4:
	.quad	4621819117588971520     # double 10
.LCPI0_5:
	.quad	4602678819172646912     # double 0.5
.LCPI0_6:
	.quad	-4620693217682128896    # double -0.5
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$262248, %rsp           # imm = 0x40068
.Lcfi6:
	.cfi_def_cfa_offset 262304
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movl	%edi, %r13d
	movl	$.L.str, %edi
	movl	$.L.str.1, %esi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_44
# BB#1:
	xorl	%edi, %edi
	movl	$rusage, %esi
	callq	getrusage
	cmpl	$4, %r13d
	jge	.LBB0_3
# BB#2:
	movl	$.L.str.5, %eax
	movl	$.L.str.4, %r15d
	movl	$.L.str.3, %ebx
	jmp	.LBB0_4
.LBB0_3:
	movq	8(%r12), %rbx
	movq	16(%r12), %r15
	movq	24(%r12), %rax
.LBB0_4:
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	20(%rsp), %rdx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sscanf
	cmpl	$0, 20(%rsp)
	je	.LBB0_45
# BB#5:
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	movl	$.L.str.8, %esi
	movq	%r15, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_46
# BB#6:                                 # %ckopen.exit.outer.preheader
	movl	$1, %ebp
	jmp	.LBB0_7
.LBB0_9:                                #   in Loop: Header=BB0_7 Depth=1
	incl	%ebp
	.p2align	4, 0x90
.LBB0_7:                                # %ckopen.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB0_7
# BB#8:                                 # %ckopen.exit
                                        #   in Loop: Header=BB0_7 Depth=1
	cmpl	$-1, %eax
	jne	.LBB0_9
# BB#10:
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	fclose
	movq	%r15, name1(%rip)
	movslq	%ebp, %rdi
	callq	malloc
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$.L.str.8, %esi
	movq	%r15, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_46
# BB#11:                                # %ckopen.exit128.outer.preheader
	xorl	%r14d, %r14d
	jmp	.LBB0_12
.LBB0_14:                               #   in Loop: Header=BB0_12 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movb	%al, 1(%rcx,%r14)
	incq	%r14
	.p2align	4, 0x90
.LBB0_12:                               # %ckopen.exit128
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB0_12
# BB#13:                                # %ckopen.exit128
                                        #   in Loop: Header=BB0_12 Depth=1
	cmpl	$-1, %eax
	jne	.LBB0_14
# BB#15:
	movq	%r15, %rdi
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_16
# BB#18:
	movl	$.L.str.8, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_47
# BB#19:                                # %ckopen.exit129.outer.preheader
	movl	$1, %ebp
	jmp	.LBB0_20
.LBB0_22:                               #   in Loop: Header=BB0_20 Depth=1
	incl	%ebp
	.p2align	4, 0x90
.LBB0_20:                               # %ckopen.exit129
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB0_20
# BB#21:                                # %ckopen.exit129
                                        #   in Loop: Header=BB0_20 Depth=1
	cmpl	$-1, %eax
	jne	.LBB0_22
# BB#23:
	movq	%rbx, %rdi
	callq	fclose
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, name2(%rip)
	movslq	%ebp, %rdi
	callq	malloc
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$.L.str.8, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_47
# BB#24:                                # %ckopen.exit130.outer.preheader
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB0_25
.LBB0_27:                               #   in Loop: Header=BB0_25 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movb	%al, 1(%rcx,%rdx)
	incq	%rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_25:                               # %ckopen.exit130
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB0_25
# BB#26:                                # %ckopen.exit130
                                        #   in Loop: Header=BB0_25 Depth=1
	cmpl	$-1, %eax
	je	.LBB0_17
	jmp	.LBB0_27
.LBB0_16:
                                        # implicit-def: %RAX
	movq	%rax, 32(%rsp)          # 8-byte Spill
                                        # implicit-def: %EAX
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB0_17:                               # %.loopexit
	movsd	.LCPI0_0(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	movsd	.LCPI0_3(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	movsd	%xmm1, 56(%rsp)         # 8-byte Spill
	movsd	%xmm1, 48(%rsp)         # 8-byte Spill
	jmp	.LBB0_33
	.p2align	4, 0x90
.LBB0_29:                               # %.outer131.loopexit
                                        #   in Loop: Header=BB0_33 Depth=1
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_28:                               # %.outer.loopexit
                                        #   in Loop: Header=BB0_33 Depth=1
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_30:                               # %.outer137.loopexit
                                        #   in Loop: Header=BB0_33 Depth=1
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_31:                               # %.outer142.loopexit
                                        #   in Loop: Header=BB0_33 Depth=1
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
.LBB0_32:                               # %.outer142
                                        #   in Loop: Header=BB0_33 Depth=1
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_33:                               # %.outer142
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_34 Depth 2
	movslq	%r13d, %rbx
	decq	%rbx
	movapd	%xmm2, %xmm0
.LBB0_34:                               #   Parent Loop BB0_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	cmpq	$4, %rbx
	jl	.LBB0_40
# BB#35:                                #   in Loop: Header=BB0_34 Depth=2
	movq	(%r12,%rbx,8), %rbp
	cmpb	$61, 1(%rbp)
	je	.LBB0_37
# BB#36:                                #   in Loop: Header=BB0_34 Depth=2
	leal	-1(%r13), %esi
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	fatalf
	movq	(%r12,%rbx,8), %rbp
.LBB0_37:                               #   in Loop: Header=BB0_34 Depth=2
	leaq	2(%rbp), %rdi
	xorl	%esi, %esi
	callq	strtod
	movsbl	(%rbp), %eax
	addl	$-69, %eax
	cmpl	$17, %eax
	ja	.LBB0_39
# BB#38:                                #   in Loop: Header=BB0_34 Depth=2
	decl	%r13d
	decq	%rbx
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_39:
	movl	$.L.str.10, %edi
	callq	fatal
.LBB0_40:
	movl	$.Lstr, %edi
	callq	puts
	movl	$.L.str.12, %edi
	movb	$5, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	48(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	56(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	64(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	72(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	callq	printf
	movsd	.LCPI0_4(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	xorps	%xmm4, %xmm4
	cmpltsd	%xmm1, %xmm4
	movapd	%xmm1, %xmm6
	movsd	.LCPI0_5(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm4, %xmm5
	andpd	%xmm1, %xmm5
	movsd	.LCPI0_6(%rip), %xmm3   # xmm3 = mem[0],zero
	andnpd	%xmm3, %xmm4
	orpd	%xmm5, %xmm4
	addsd	%xmm6, %xmm4
	cvttsd2si	%xmm4, %eax
	movl	%eax, 86448(%rsp)
	movl	%eax, 73084(%rsp)
	movl	%eax, 68972(%rsp)
	movl	%eax, 66916(%rsp)
	movsd	48(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	mulsd	%xmm2, %xmm5
	xorpd	%xmm4, %xmm4
	cmpltsd	%xmm5, %xmm4
	movapd	%xmm5, %xmm6
	movapd	%xmm4, %xmm5
	andpd	%xmm1, %xmm5
	andnpd	%xmm3, %xmm4
	orpd	%xmm5, %xmm4
	addsd	%xmm6, %xmm4
	cvttsd2si	%xmm4, %eax
	movl	%eax, 86380(%rsp)
	movl	%eax, 69040(%rsp)
	movl	%eax, 73060(%rsp)
	movl	%eax, 66940(%rsp)
	movsd	56(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	mulsd	%xmm2, %xmm5
	xorpd	%xmm4, %xmm4
	cmpltsd	%xmm5, %xmm4
	movapd	%xmm5, %xmm6
	movapd	%xmm4, %xmm5
	andpd	%xmm1, %xmm5
	andnpd	%xmm3, %xmm4
	orpd	%xmm5, %xmm4
	addsd	%xmm6, %xmm4
	cvttsd2si	%xmm4, %eax
	movl	%eax, 86396(%rsp)
	movl	%eax, 86372(%rsp)
	movl	%eax, 73136(%rsp)
	movl	%eax, 73068(%rsp)
	movl	%eax, 68988(%rsp)
	movl	%eax, 68964(%rsp)
	movl	%eax, 66992(%rsp)
	movl	%eax, 66924(%rsp)
	movsd	64(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	mulsd	%xmm2, %xmm5
	xorpd	%xmm4, %xmm4
	cmpltsd	%xmm5, %xmm4
	movapd	%xmm5, %xmm6
	movapd	%xmm4, %xmm5
	andpd	%xmm1, %xmm5
	andnpd	%xmm3, %xmm4
	orpd	%xmm5, %xmm4
	addsd	%xmm6, %xmm4
	cvttsd2si	%xmm4, %ebx
	movsd	72(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	mulsd	%xmm2, %xmm4
	cmpltsd	%xmm4, %xmm0
	andpd	%xmm0, %xmm1
	andnpd	%xmm3, %xmm0
	orpd	%xmm1, %xmm0
	addsd	%xmm4, %xmm0
	cvttsd2si	%xmm0, %ebp
	movq	%r15, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	strcmp
	testl	%eax, %eax
	movl	20(%rsp), %r8d
	jne	.LBB0_41
# BB#42:
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	leaq	104(%rsp), %r9
	movl	$0, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, %rsi
	movl	%r14d, %edx
	movl	%r14d, %ecx
	pushq	$1
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	SIM
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_43
.LBB0_41:
	subq	$8, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	leaq	104(%rsp), %r9
	movl	$0, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	%r14d, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	pushq	$2
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	callq	SIM
	addq	$32, %rsp
.Lcfi22:
	.cfi_adjust_cfa_offset -32
.LBB0_43:
	callq	dtime
	movl	$10, %edi
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	callq	fputc
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movq	(%rsp), %r8             # 8-byte Reload
	callq	fprintf
	movl	$.L.str.15, %edi
	movl	$56, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	%rbx, %rdi
	callq	fclose
	xorl	%edi, %edi
	callq	exit
.LBB0_46:
	movl	$.L.str.33, %edi
	movq	%r15, %rsi
	callq	fatalf
.LBB0_47:
	movl	$.L.str.33, %edi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	fatalf
.LBB0_44:
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.LBB0_45:
	movl	$.L.str.7, %edi
	callq	fatal
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_29
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_31
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_34
	.quad	.LBB0_39
	.quad	.LBB0_28
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_39
	.quad	.LBB0_30

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4621819117588971520     # double 10
	.text
	.globl	SIM
	.p2align	4, 0x90
	.type	SIM,@function
SIM:                                    # @SIM
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 128
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movl	%r8d, 20(%rsp)          # 4-byte Spill
	movl	%ecx, %r15d
	movl	%edx, %r12d
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	leal	4(,%r15,4), %eax
	movslq	%eax, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, CC(%rip)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, DD(%rip)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, RR(%rip)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, SS(%rip)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, EE(%rip)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, FF(%rip)
	leal	1(%r12), %r13d
	movslq	%r13d, %rbx
	leal	4(,%r12,4), %r14d
	movslq	%r14d, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, HH(%rip)
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, WW(%rip)
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, II(%rip)
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, JJ(%rip)
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, XX(%rip)
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, YY(%rip)
	movq	%r15, 40(%rsp)          # 8-byte Spill
	leal	4(%r14,%r15,4), %eax
	movslq	%eax, %rdi
	callq	malloc
	movq	%rax, 56(%rsp)          # 8-byte Spill
	shlq	$3, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, row(%rip)
	testl	%r12d, %r12d
	jle	.LBB1_13
# BB#1:                                 # %.lr.ph116
	cmpl	$2, 144(%rsp)
	movl	%r13d, %r14d
	jne	.LBB1_2
# BB#5:                                 # %.lr.ph116.split.us.preheader
	movq	$0, 8(%rbx)
	cmpl	$2, %r13d
	je	.LBB1_13
# BB#6:                                 # %.lr.ph116.split.us..lr.ph116.split.us_crit_edge.preheader
	leal	6(%r14), %edx
	leaq	-3(%r14), %rcx
	andq	$7, %rdx
	je	.LBB1_7
# BB#8:                                 # %.lr.ph116.split.us..lr.ph116.split.us_crit_edge.prol.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph116.split.us..lr.ph116.split.us_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	row(%rip), %rsi
	movq	$0, 16(%rsi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB1_9
# BB#10:                                # %.lr.ph116.split.us..lr.ph116.split.us_crit_edge.prol.loopexit.unr-lcssa
	addq	$2, %rax
	cmpq	$7, %rcx
	jae	.LBB1_12
	jmp	.LBB1_13
.LBB1_2:                                # %.lr.ph116.split.preheader
	movl	$16, %edi
	callq	malloc
	movq	%rax, z(%rip)
	movq	%rax, 8(%rbx)
	movl	$1, (%rax)
	movq	$0, 8(%rax)
	cmpl	$2, %r13d
	je	.LBB1_13
# BB#3:                                 # %.lr.ph116.split..lr.ph116.split_crit_edge.preheader
	movl	$2, %ebx
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph116.split..lr.ph116.split_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	row(%rip), %rbp
	movl	$16, %edi
	callq	malloc
	movq	%rax, z(%rip)
	movq	%rax, (%rbp,%rbx,8)
	movq	z(%rip), %rax
	movl	%ebx, (%rax)
	movq	$0, 8(%rax)
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB1_4
	jmp	.LBB1_13
.LBB1_7:
	movl	$2, %eax
	cmpq	$7, %rcx
	jb	.LBB1_13
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph116.split.us..lr.ph116.split.us_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	row(%rip), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	row(%rip), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	row(%rip), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	row(%rip), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	row(%rip), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	row(%rip), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	row(%rip), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	row(%rip), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	cmpq	%rax, %r14
	jne	.LBB1_12
.LBB1_13:                               # %._crit_edge117
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, v(%rip)
	movl	128(%rsp), %eax
	movl	%eax, %ecx
	movl	%ecx, q(%rip)
	movl	136(%rsp), %eax
	movl	%eax, r(%rip)
	addl	%ecx, %eax
	movl	%eax, qr(%rip)
	movslq	20(%rsp), %rbp          # 4-byte Folded Reload
	leaq	(,%rbp,8), %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, LIST(%rip)
	testl	%ebp, %ebp
	jle	.LBB1_17
# BB#14:                                # %.lr.ph112.preheader
	movl	$36, %edi
	callq	malloc
	movq	%rax, (%rbx)
	cmpl	$1, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB1_17
# BB#15:                                # %.lr.ph112..lr.ph112_crit_edge.preheader
	movl	20(%rsp), %r14d         # 4-byte Reload
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB1_16:                               # %.lr.ph112..lr.ph112_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	LIST(%rip), %rbx
	movl	$36, %edi
	callq	malloc
	movq	%rax, (%rbx,%rbp,8)
	incq	%rbp
	cmpq	%rbp, %r14
	jne	.LBB1_16
.LBB1_17:                               # %._crit_edge113
	movq	name1(%rip), %rbx
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%rbx, %rsi
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	printf
	movq	name2(%rip), %rbx
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%rbx, %rsi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %esi
	callq	printf
	movl	$0, min(%rip)
	movl	$0, numnode(%rip)
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%r12d, %edx
	movl	%ebx, %ecx
	movl	20(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %r8d
	movl	144(%rsp), %r9d
	callq	big_pass
	testl	%ebx, %ebx
	jle	.LBB1_34
# BB#18:                                # %.lr.ph.preheader
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r8d
	.p2align	4, 0x90
.LBB1_19:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_25 Depth 2
	movslq	numnode(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB1_35
# BB#20:                                #   in Loop: Header=BB1_19 Depth=1
	xorl	%edx, %edx
	movq	LIST(%rip), %rax
	cmpl	$2, %ecx
	jl	.LBB1_26
# BB#21:                                # %.lr.ph.i
                                        #   in Loop: Header=BB1_19 Depth=1
	testb	$1, %cl
	jne	.LBB1_22
# BB#23:                                #   in Loop: Header=BB1_19 Depth=1
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	movl	(%rdx), %edi
	xorl	%edx, %edx
	cmpl	(%rsi), %edi
	setg	%dl
	movl	$2, %esi
	cmpl	$2, %ecx
	jne	.LBB1_25
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_22:                               #   in Loop: Header=BB1_19 Depth=1
	xorl	%edx, %edx
	movl	$1, %esi
	cmpl	$2, %ecx
	je	.LBB1_26
	.p2align	4, 0x90
.LBB1_25:                               #   Parent Loop BB1_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rsi,8), %rdi
	movl	(%rdi), %edi
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rbp
	cmpl	(%rbp), %edi
	cmovgl	%esi, %edx
	movq	8(%rax,%rsi,8), %rdi
	movl	(%rdi), %edi
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rbp
	leal	1(%rsi), %ebx
	cmpl	(%rbp), %edi
	cmovgl	%ebx, %edx
	addq	$2, %rsi
	cmpq	%rcx, %rsi
	jne	.LBB1_25
.LBB1_26:                               # %._crit_edge.i
                                        #   in Loop: Header=BB1_19 Depth=1
	movslq	%edx, %rsi
	movq	(%rax,%rsi,8), %rdx
	leal	-1(%rcx), %edi
	movl	%edi, numnode(%rip)
	cmpl	%edi, %esi
	je	.LBB1_28
# BB#27:                                #   in Loop: Header=BB1_19 Depth=1
	movq	-8(%rax,%rcx,8), %rdi
	movq	%rdi, (%rax,%rsi,8)
	movq	LIST(%rip), %rax
	movq	%rdx, -8(%rax,%rcx,8)
	movq	LIST(%rip), %rax
.LBB1_28:                               #   in Loop: Header=BB1_19 Depth=1
	movq	%r8, 40(%rsp)           # 8-byte Spill
	leal	-1(%r8), %r13d
	movq	(%rax), %rcx
	movq	%rcx, most(%rip)
	cmpq	%rdx, low(%rip)
	jne	.LBB1_30
# BB#29:                                #   in Loop: Header=BB1_19 Depth=1
	movq	(%rax), %rax
	movq	%rax, low(%rip)
.LBB1_30:                               # %findmax.exit
                                        #   in Loop: Header=BB1_19 Depth=1
	movslq	4(%rdx), %rax
	leaq	1(%rax), %r12
	cvtsi2sdl	(%rdx), %xmm0
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	movl	%r12d, 4(%rdx)
	movslq	8(%rdx), %rsi
	leaq	1(%rsi), %r14
	movl	%r14d, 8(%rdx)
	movl	12(%rdx), %edi
	movl	%edi, 32(%rsp)          # 4-byte Spill
	movl	16(%rdx), %ebp
	movl	20(%rdx), %ecx
	movl	%ecx, m1(%rip)
	movl	24(%rdx), %ecx
	movl	%ecx, mm(%rip)
	movl	28(%rdx), %ecx
	movl	%ecx, n1(%rip)
	movl	32(%rdx), %ecx
	movl	%ecx, nn(%rip)
	movl	%edi, %edx
	subl	%eax, %edx
	movl	%edx, rl(%rip)
	movl	%ebp, %ecx
	subl	%esi, %ecx
	movl	%ecx, cl(%rip)
	movl	%eax, I(%rip)
	movl	%esi, J(%rip)
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, sapp(%rip)
	movl	$0, last(%rip)
	movl	$0, al_len(%rip)
	movl	$0, no_mat(%rip)
	movl	$0, no_mis(%rip)
	movq	48(%rsp), %rdi          # 8-byte Reload
	leaq	1(%rdi,%rax), %r15
	decq	%r15
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax,%rsi), %rbx
	decq	%rbx
	movl	q(%rip), %r8d
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movl	%r8d, %r9d
	callq	diff
	movl	$.Lstr.2, %edi
	callq	puts
	movl	20(%rsp), %esi          # 4-byte Reload
	subl	%r13d, %esi
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	64(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	.LCPI1_0(%rip), %xmm0
	movl	$.L.str.22, %edi
	movb	$1, %al
	callq	printf
	imull	$100, no_mat(%rip), %eax
	cltd
	idivl	al_len(%rip)
	movl	%eax, %ecx
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	movl	no_mat(%rip), %esi
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	printf
	movl	no_mis(%rip), %esi
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	printf
	movl	al_len(%rip), %esi
	subl	no_mat(%rip), %esi
	subl	no_mis(%rip), %esi
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	movl	%r14d, %edx
	movl	32(%rsp), %ecx          # 4-byte Reload
	movl	%ebp, %r8d
	callq	printf
	movl	rl(%rip), %edx
	movl	cl(%rip), %ecx
	movl	%r14d, (%rsp)
	movq	%r15, %rdi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	%rbx, %rsi
	movq	56(%rsp), %r8           # 8-byte Reload
	movl	%r12d, %r9d
	callq	display
	movq	stdout(%rip), %rdi
	callq	fflush
	testl	%r13d, %r13d
	movq	40(%rsp), %r14          # 8-byte Reload
	je	.LBB1_34
# BB#31:                                #   in Loop: Header=BB1_19 Depth=1
	movb	$0, flag(%rip)
	movq	%rbp, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	144(%rsp), %edx
	callq	locate
	cmpb	$1, flag(%rip)
	jne	.LBB1_33
# BB#32:                                #   in Loop: Header=BB1_19 Depth=1
	movq	%rbp, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%r13d, %edx
	movl	144(%rsp), %ecx
	callq	small_pass
.LBB1_33:                               # %.backedge
                                        #   in Loop: Header=BB1_19 Depth=1
	cmpl	$1, %r14d
	movl	%r13d, %r8d
	jg	.LBB1_19
.LBB1_34:                               # %._crit_edge
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_35:
	movl	$.L.str.19, %edi
	callq	fatal
.Lfunc_end1:
	.size	SIM, .Lfunc_end1-SIM
	.cfi_endproc

	.globl	big_pass
	.p2align	4, 0x90
	.type	big_pass,@function
big_pass:                               # @big_pass
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 192
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movl	%r9d, 28(%rsp)          # 4-byte Spill
	movl	%r8d, 48(%rsp)          # 4-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	jle	.LBB2_7
# BB#1:                                 # %.lr.ph211
	movq	CC(%rip), %rsi
	movq	RR(%rip), %rax
	movq	EE(%rip), %r9
	xorl	%r12d, %r12d
	subl	q(%rip), %r12d
	movq	DD(%rip), %r11
	movq	SS(%rip), %r10
	movq	FF(%rip), %r14
	movq	40(%rsp), %rcx          # 8-byte Reload
	leal	1(%rcx), %r15d
	testb	$1, %r15b
	jne	.LBB2_2
# BB#3:
	movl	$0, 4(%rsi)
	movl	$0, 4(%rax)
	movl	$1, 4(%r9)
	movl	%r12d, 4(%r11)
	movl	$0, 4(%r10)
	movl	$1, 4(%r14)
	movl	$2, %r13d
	cmpl	$2, %r15d
	jne	.LBB2_5
	jmp	.LBB2_7
.LBB2_2:
	movl	$1, %r13d
	cmpl	$2, %r15d
	je	.LBB2_7
.LBB2_5:                                # %.lr.ph211.new
	leaq	4(%rsi,%r13,4), %r8
	leaq	4(%rax,%r13,4), %rdi
	leaq	4(%r9,%r13,4), %rbx
	leaq	4(%r11,%r13,4), %rbp
	leaq	4(%r10,%r13,4), %rcx
	leaq	4(%r14,%r13,4), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_6:                                # =>This Inner Loop Header: Depth=1
	movl	$0, -4(%r8,%rdx,4)
	movl	$0, -4(%rdi,%rdx,4)
	leaq	(%r13,%rdx), %rsi
	movl	%esi, -4(%rbx,%rdx,4)
	movl	%r12d, -4(%rbp,%rdx,4)
	movl	$0, -4(%rcx,%rdx,4)
	movl	%esi, -4(%rax,%rdx,4)
	movl	$0, (%r8,%rdx,4)
	movl	$0, (%rdi,%rdx,4)
	incl	%esi
	movl	%esi, (%rbx,%rdx,4)
	movl	%r12d, (%rbp,%rdx,4)
	movl	$0, (%rcx,%rdx,4)
	movl	%esi, (%rax,%rdx,4)
	leaq	2(%r13,%rdx), %rsi
	addq	$2, %rdx
	cmpq	%r15, %rsi
	jne	.LBB2_6
.LBB2_7:                                # %.preheader
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_47
# BB#8:                                 # %.lr.ph207
	movslq	40(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, %eax
	.p2align	4, 0x90
.LBB2_10:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_15 Depth 2
                                        #       Child Loop BB2_29 Depth 3
	movq	%rax, %r10
	xorl	%eax, %eax
	subl	q(%rip), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movq	v(%rip), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	movsbq	(%rax,%r10), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	cmpl	$2, 28(%rsp)            # 4-byte Folded Reload
	jne	.LBB2_12
# BB#11:                                #   in Loop: Header=BB2_10 Depth=1
	leal	-1(%r10), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	jmp	.LBB2_13
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_10 Depth=1
	movq	CC(%rip), %rax
	movl	(%rax,%r10,4), %eax
	movq	RR(%rip), %rcx
	movl	(%rcx,%r10,4), %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movq	EE(%rip), %rcx
	movl	(%rcx,%r10,4), %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movl	%r10d, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
.LBB2_13:                               #   in Loop: Header=BB2_10 Depth=1
	leaq	1(%r10), %rcx
	cmpl	$2, 28(%rsp)            # 4-byte Folded Reload
	movq	%rcx, 72(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	$1, %edx
	cmovel	%edx, %ecx
	cmpl	40(%rsp), %ecx          # 4-byte Folded Reload
	jg	.LBB2_9
# BB#14:                                # %.lr.ph205.preheader
                                        #   in Loop: Header=BB2_10 Depth=1
	movslq	%ecx, %r14
	xorl	%ebp, %ebp
	movl	%r10d, %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	12(%rsp), %r15d         # 4-byte Reload
	movl	%r10d, %r12d
	movq	%r10, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_15:                               # %.lr.ph205
                                        #   Parent Loop BB2_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_29 Depth 3
	movl	r(%rip), %ebx
	movl	24(%rsp), %ecx          # 4-byte Reload
	subl	%ebx, %ecx
	movl	qr(%rip), %edx
	subl	%edx, %ebp
	cmpl	%ebp, %ecx
	jge	.LBB2_17
# BB#16:                                #   in Loop: Header=BB2_15 Depth=2
	movl	%ebp, 24(%rsp)          # 4-byte Spill
.LBB2_19:                               #   in Loop: Header=BB2_15 Depth=2
	movl	%r12d, 8(%rsp)          # 4-byte Spill
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	jmp	.LBB2_21
	.p2align	4, 0x90
.LBB2_17:                               #   in Loop: Header=BB2_15 Depth=2
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	jne	.LBB2_21
# BB#18:                                #   in Loop: Header=BB2_15 Depth=2
	movl	8(%rsp), %edi           # 4-byte Reload
	cmpl	%r12d, %edi
	jl	.LBB2_19
# BB#20:                                #   in Loop: Header=BB2_15 Depth=2
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmpl	%r15d, %ecx
	cmovgel	%ecx, %r15d
	cmpl	%r12d, %edi
	cmovel	%r15d, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	%edi, 8(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB2_21:                               #   in Loop: Header=BB2_15 Depth=2
	movl	16(%rsp), %r11d         # 4-byte Reload
	movq	CC(%rip), %rcx
	movq	%rcx, %r13
	movl	(%rcx,%r14,4), %esi
	movl	%esi, %r8d
	subl	%edx, %r8d
	movq	RR(%rip), %rcx
	movq	%rcx, %r12
	movl	(%rcx,%r14,4), %ecx
	movq	EE(%rip), %rdx
	movq	%rdx, %r9
	movl	(%rdx,%r14,4), %r15d
	movq	DD(%rip), %rdx
	movl	(%rdx,%r14,4), %ebp
	subl	%ebx, %ebp
	movq	SS(%rip), %rbx
	movq	FF(%rip), %rdi
	cmpl	%r8d, %ebp
	movl	%esi, 52(%rsp)          # 4-byte Spill
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	jge	.LBB2_23
.LBB2_22:                               #   in Loop: Header=BB2_15 Depth=2
	movl	%ecx, %esi
.LBB2_27:                               #   in Loop: Header=BB2_15 Depth=2
	movl	20(%rsp), %ebp          # 4-byte Reload
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%r15d, %r13d
	jmp	.LBB2_28
	.p2align	4, 0x90
.LBB2_23:                               #   in Loop: Header=BB2_15 Depth=2
	movl	(%rbx,%r14,4), %esi
	movl	(%rdi,%r14,4), %edx
	jne	.LBB2_24
# BB#25:                                #   in Loop: Header=BB2_15 Depth=2
	cmpl	%ecx, %esi
	jl	.LBB2_22
# BB#26:                                #   in Loop: Header=BB2_15 Depth=2
	cmpl	%r15d, %edx
	cmovgel	%edx, %r15d
	cmpl	%ecx, %esi
	cmovnel	%edx, %r15d
	jmp	.LBB2_27
	.p2align	4, 0x90
.LBB2_24:                               #   in Loop: Header=BB2_15 Depth=2
	movl	%ebp, %r8d
	movl	20(%rsp), %ebp          # 4-byte Reload
	movq	%r13, %rcx
	movl	%edx, %r13d
	movq	%r12, %rdx
.LBB2_28:                               #   in Loop: Header=BB2_15 Depth=2
	movl	8(%rsp), %r12d          # 4-byte Reload
	movl	%r11d, %edi
	movq	%rcx, %r11
	movb	$1, tt(%rip)
	movq	row(%rip), %rcx
	movq	(%rcx,%r10,8), %rcx
	jmp	.LBB2_29
	.p2align	4, 0x90
.LBB2_31:                               #   in Loop: Header=BB2_29 Depth=3
	movq	8(%rcx), %rcx
.LBB2_29:                               #   Parent Loop BB2_10 Depth=1
                                        #     Parent Loop BB2_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, z(%rip)
	testq	%rcx, %rcx
	je	.LBB2_30
# BB#32:                                # %.lr.ph
                                        #   in Loop: Header=BB2_29 Depth=3
	cmpl	%r14d, (%rcx)
	jne	.LBB2_31
# BB#33:                                # %.thread
                                        #   in Loop: Header=BB2_15 Depth=2
	movb	$0, tt(%rip)
	xorl	%eax, %eax
	jmp	.LBB2_34
	.p2align	4, 0x90
.LBB2_30:                               # %._crit_edge
                                        #   in Loop: Header=BB2_15 Depth=2
	movq	80(%rsp), %rcx          # 8-byte Reload
	movsbq	(%rcx,%r14), %rcx
	movq	%rdx, %rbx
	movq	88(%rsp), %rdx          # 8-byte Reload
	shlq	$10, %rdx
	addq	96(%rsp), %rdx          # 8-byte Folded Reload
	addl	(%rdx,%rcx,4), %eax
	movq	%rbx, %rdx
.LBB2_34:                               #   in Loop: Header=BB2_15 Depth=2
	testl	%eax, %eax
	movl	$0, %ecx
	cmovlel	%ecx, %eax
	cmovlel	%r10d, %ebp
	cmovlel	%r14d, %edi
	cmpl	%r8d, %eax
	movl	%r8d, %ecx
	movl	%esi, %r10d
	movl	%r13d, %ebx
	jl	.LBB2_39
# BB#35:                                #   in Loop: Header=BB2_15 Depth=2
	jne	.LBB2_36
# BB#37:                                #   in Loop: Header=BB2_15 Depth=2
	cmpl	%esi, %ebp
	movl	%r8d, %ecx
	movl	%esi, %r10d
	movl	%r13d, %ebx
	jl	.LBB2_39
# BB#38:                                #   in Loop: Header=BB2_15 Depth=2
	cmpl	%r13d, %edi
	movl	%edi, %ebx
	cmovll	%r13d, %ebx
	cmpl	%esi, %ebp
	cmovnel	%edi, %ebx
	movl	%r8d, %ecx
	movl	%ebp, %r10d
	jmp	.LBB2_39
	.p2align	4, 0x90
.LBB2_36:                               #   in Loop: Header=BB2_15 Depth=2
	movl	%eax, %ecx
	movl	%ebp, %r10d
	movl	%edi, %ebx
.LBB2_39:                               #   in Loop: Header=BB2_15 Depth=2
	movl	24(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %ecx
	movl	%eax, %ebp
	movl	%r12d, 8(%rsp)          # 4-byte Spill
	movl	12(%rsp), %r15d         # 4-byte Reload
	jl	.LBB2_44
# BB#40:                                #   in Loop: Header=BB2_15 Depth=2
	jne	.LBB2_41
# BB#42:                                #   in Loop: Header=BB2_15 Depth=2
	movl	8(%rsp), %r12d          # 4-byte Reload
	cmpl	%r12d, %r10d
	movl	%eax, %ebp
	movl	12(%rsp), %r15d         # 4-byte Reload
	jl	.LBB2_44
# BB#43:                                #   in Loop: Header=BB2_15 Depth=2
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmpl	%ecx, %ebx
	movl	%ebx, %r15d
	cmovll	%ecx, %r15d
	cmpl	8(%rsp), %r10d          # 4-byte Folded Reload
	cmovnel	%ebx, %r15d
	movl	%eax, %ebp
	movl	%r10d, %r12d
	jmp	.LBB2_44
	.p2align	4, 0x90
.LBB2_41:                               #   in Loop: Header=BB2_15 Depth=2
	movl	%ecx, %ebp
	movl	%r10d, %r12d
	movl	%ebx, %r15d
.LBB2_44:                               #   in Loop: Header=BB2_15 Depth=2
	movl	%ebp, (%r11,%r14,4)
	movl	(%rdx,%r14,4), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	(%r9,%r14,4), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	%r12d, (%rdx,%r14,4)
	movl	%r15d, (%r9,%r14,4)
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	%r8d, (%rax,%r14,4)
	movq	120(%rsp), %rax         # 8-byte Reload
	movl	%esi, (%rax,%r14,4)
	movq	112(%rsp), %rax         # 8-byte Reload
	movl	%r13d, (%rax,%r14,4)
	movl	min(%rip), %eax
	cmpl	%eax, %ebp
	jle	.LBB2_46
# BB#45:                                #   in Loop: Header=BB2_15 Depth=2
	movl	%eax, (%rsp)
	movl	%ebp, %edi
	movl	%r12d, %esi
	movl	%r15d, %edx
	movq	56(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r14d, %r8d
	movl	48(%rsp), %r9d          # 4-byte Reload
	callq	addnode
	movl	%eax, min(%rip)
.LBB2_46:                               #   in Loop: Header=BB2_15 Depth=2
	cmpq	104(%rsp), %r14         # 8-byte Folded Reload
	leaq	1(%r14), %r14
	movl	52(%rsp), %eax          # 4-byte Reload
	movq	56(%rsp), %r10          # 8-byte Reload
	jl	.LBB2_15
.LBB2_9:                                # %.loopexit
                                        #   in Loop: Header=BB2_10 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB2_10
.LBB2_47:                               # %._crit_edge208
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	big_pass, .Lfunc_end2-big_pass
	.cfi_endproc

	.globl	locate
	.p2align	4, 0x90
	.type	locate,@function
locate:                                 # @locate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	subq	$128, %rsp
.Lcfi55:
	.cfi_def_cfa_offset 184
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	movslq	nn(%rip), %rcx
	movl	%ecx, %r14d
	movl	n1(%rip), %eax
	movl	%eax, -120(%rsp)        # 4-byte Spill
	movslq	%eax, %rbp
	cmpl	%ebp, %ecx
	movl	%edx, -76(%rsp)         # 4-byte Spill
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movq	%r14, 72(%rsp)          # 8-byte Spill
	jge	.LBB3_2
# BB#1:                                 # %.._crit_edge659_crit_edge
	movl	mm(%rip), %r15d
	jmp	.LBB3_9
.LBB3_2:                                # %.lr.ph658
	movq	CC(%rip), %r9
	movl	%edx, %eax
	movq	EE(%rip), %r10
	xorl	%r13d, %r13d
	subl	q(%rip), %r13d
	movq	DD(%rip), %rdx
	movq	FF(%rip), %rbx
	movl	mm(%rip), %r15d
	leal	1(%r15), %ecx
	cmpl	$2, %eax
	movq	SS(%rip), %rax
	movq	RR(%rip), %rdi
	jne	.LBB3_3
# BB#6:                                 # %.lr.ph658.split.us.preheader
	movq	-64(%rsp), %rsi         # 8-byte Reload
	leaq	1(%rsi), %r8
	leaq	(%r9,%rsi,4), %r9
	leaq	(%r10,%rsi,4), %r10
	leaq	(%rdx,%rsi,4), %r12
	leaq	(%rbx,%rsi,4), %rbx
	leaq	(%rax,%rsi,4), %rdx
	leaq	(%rdi,%rsi,4), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph658.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%r9,%rdi,4)
	leal	(%r14,%rdi), %eax
	movl	%eax, (%r10,%rdi,4)
	movl	%r13d, (%r12,%rdi,4)
	movl	%eax, (%rbx,%rdi,4)
	movl	%ecx, (%rdx,%rdi,4)
	movl	%ecx, (%rsi,%rdi,4)
	leaq	-1(%r8,%rdi), %rax
	decq	%rdi
	cmpq	%rbp, %rax
	jg	.LBB3_7
# BB#8:
	movl	-76(%rsp), %edx         # 4-byte Reload
	jmp	.LBB3_9
.LBB3_3:                                # %.lr.ph658.split.preheader
	movq	%rbp, %r14
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movslq	%r15d, %r8
	movq	-64(%rsp), %rsi         # 8-byte Reload
	leaq	(%r9,%rsi,4), %r9
	leaq	(%r10,%rsi,4), %r10
	leaq	(%rdx,%rsi,4), %r11
	leaq	(%rbx,%rsi,4), %r15
	leaq	(%rdi,%rsi,4), %r12
	leaq	(%rax,%rsi,4), %rdx
	movl	%esi, %edi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph658.split
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rsi,%rbp), %rax
	movl	$0, (%r9,%rbp,4)
	leal	(%rdi,%rbp), %ebx
	movl	%ebx, (%r10,%rbp,4)
	movl	%r13d, (%r11,%rbp,4)
	movl	%ebx, (%r15,%rbp,4)
	cmpq	%r8, %rax
	cmovgl	%ecx, %ebx
	movl	%ebx, (%rdx,%rbp,4)
	movl	%ebx, (%r12,%rbp,4)
	leaq	-1(%rsi,%rbp), %rax
	decq	%rbp
	incq	%rax
	cmpq	%r14, %rax
	jg	.LBB3_4
# BB#5:
	movl	-76(%rsp), %edx         # 4-byte Reload
	movq	%r14, %rax
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	%rax, %rbp
.LBB3_9:                                # %._crit_edge659
	movl	m1(%rip), %ecx
	movq	v(%rip), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	cmpl	%ecx, %r15d
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movl	%ecx, -116(%rsp)        # 4-byte Spill
	jge	.LBB3_11
# BB#10:                                # %._crit_edge659.._crit_edge652_crit_edge
	movq	CC(%rip), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	HH(%rip), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	RR(%rip), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movq	II(%rip), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	EE(%rip), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movq	JJ(%rip), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	DD(%rip), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movq	WW(%rip), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	SS(%rip), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movq	XX(%rip), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	FF(%rip), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movq	YY(%rip), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
                                        # implicit-def: %EBX
                                        # implicit-def: %R11D
	jmp	.LBB3_51
.LBB3_11:                               # %.lr.ph651
	movslq	%ecx, %rsi
	movq	CC(%rip), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	HH(%rip), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	RR(%rip), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movq	II(%rip), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	EE(%rip), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movq	JJ(%rip), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	DD(%rip), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movq	WW(%rip), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	SS(%rip), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movq	XX(%rip), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	FF(%rip), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movq	YY(%rip), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movslq	%r15d, %rcx
	xorl	%eax, %eax
	subl	q(%rip), %eax
	movl	%eax, -108(%rsp)        # 4-byte Spill
	movq	-64(%rsp), %rax         # 8-byte Reload
	leal	1(%rax), %eax
	movl	%eax, 96(%rsp)          # 4-byte Spill
                                        # implicit-def: %R11D
                                        # implicit-def: %EBX
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%rsi, -88(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_12:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_14 Depth 2
                                        #       Child Loop BB3_30 Depth 3
	cmpl	$2, %edx
	sete	%dil
	leal	1(%rcx), %r9d
	cmpq	%rcx, %rbp
	setg	%al
	orb	%dil, %al
	movl	%r9d, %edi
	cmovnel	-120(%rsp), %edi        # 4-byte Folded Reload
	cmpl	%edi, -64(%rsp)         # 4-byte Folded Reload
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	jl	.LBB3_48
# BB#13:                                # %.lr.ph642.preheader
                                        #   in Loop: Header=BB3_12 Depth=1
	movb	%al, 48(%rsp)           # 1-byte Spill
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	-104(%rsp), %rdx        # 8-byte Reload
	movsbq	(%rcx,%rdx), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	-104(%rsp), %rcx        # 8-byte Reload
	movslq	%edi, %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movl	r(%rip), %ebx
	xorl	%edx, %edx
	movl	qr(%rip), %edi
	movq	-64(%rsp), %r11         # 8-byte Reload
	movl	96(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r10d
	movl	%eax, -128(%rsp)        # 4-byte Spill
	movl	%ecx, %esi
	movl	%esi, -124(%rsp)        # 4-byte Spill
	movl	%eax, %ebp
	movl	%ecx, %eax
	movl	-108(%rsp), %r14d       # 4-byte Reload
	xorl	%r15d, %r15d
	movl	%ebx, 56(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB3_14:                               # %.lr.ph642
                                        #   Parent Loop BB3_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_30 Depth 3
	subl	%ebx, %r14d
	subl	%edi, %r15d
	cmpl	%r15d, %r14d
	jge	.LBB3_16
# BB#15:                                #   in Loop: Header=BB3_14 Depth=2
	movl	%r15d, %r14d
.LBB3_18:                               #   in Loop: Header=BB3_14 Depth=2
	movl	%eax, -124(%rsp)        # 4-byte Spill
	movl	%ebp, -128(%rsp)        # 4-byte Spill
	jmp	.LBB3_20
	.p2align	4, 0x90
.LBB3_16:                               #   in Loop: Header=BB3_14 Depth=2
	jne	.LBB3_20
# BB#17:                                #   in Loop: Header=BB3_14 Depth=2
	movl	-124(%rsp), %esi        # 4-byte Reload
	cmpl	%eax, %esi
	jl	.LBB3_18
# BB#19:                                #   in Loop: Header=BB3_14 Depth=2
	movq	%rcx, %r8
	movl	-128(%rsp), %ecx        # 4-byte Reload
	cmpl	%ebp, %ecx
	cmovgel	%ecx, %ebp
	cmpl	%eax, %esi
	cmovel	%ebp, %ecx
	movl	%ecx, -128(%rsp)        # 4-byte Spill
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB3_20:                               #   in Loop: Header=BB3_14 Depth=2
	movq	-16(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%r11,4), %ebp
	movl	%ebp, %r8d
	movl	%edi, %r15d
	subl	%edi, %r8d
	movq	-24(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%r11,4), %eax
	movq	-32(%rsp), %rsi         # 8-byte Reload
	movl	(%rsi,%r11,4), %r12d
	movq	-40(%rsp), %rsi         # 8-byte Reload
	movl	(%rsi,%r11,4), %edi
	subl	%ebx, %edi
	cmpl	%r8d, %edi
	movl	%ebp, -112(%rsp)        # 4-byte Spill
	jge	.LBB3_22
# BB#21:                                #   in Loop: Header=BB3_14 Depth=2
	movl	%eax, %r13d
	jmp	.LBB3_28
	.p2align	4, 0x90
.LBB3_22:                               #   in Loop: Header=BB3_14 Depth=2
	movq	-48(%rsp), %rsi         # 8-byte Reload
	movl	(%rsi,%r11,4), %ecx
	movq	-56(%rsp), %rsi         # 8-byte Reload
	movl	(%rsi,%r11,4), %esi
	jne	.LBB3_23
# BB#24:                                #   in Loop: Header=BB3_14 Depth=2
	cmpl	%eax, %ecx
	jge	.LBB3_26
# BB#25:                                #   in Loop: Header=BB3_14 Depth=2
	movl	%eax, %r13d
	jmp	.LBB3_27
	.p2align	4, 0x90
.LBB3_23:                               #   in Loop: Header=BB3_14 Depth=2
	movl	%ecx, %r13d
	movl	%edi, %r8d
	movl	%esi, %r12d
	jmp	.LBB3_27
.LBB3_26:                               #   in Loop: Header=BB3_14 Depth=2
	cmpl	%r12d, %esi
	cmovgel	%esi, %r12d
	movl	%ecx, %r13d
	cmpl	%eax, %ecx
	cmovnel	%esi, %r12d
.LBB3_27:                               #   in Loop: Header=BB3_14 Depth=2
	movq	-104(%rsp), %rcx        # 8-byte Reload
.LBB3_28:                               #   in Loop: Header=BB3_14 Depth=2
	movl	%r15d, %edi
	movq	row(%rip), %rax
	movq	(%rax,%rcx,8), %rax
	jmp	.LBB3_30
	.p2align	4, 0x90
.LBB3_29:                               #   in Loop: Header=BB3_30 Depth=3
	movq	8(%rax), %rax
.LBB3_30:                               #   Parent Loop BB3_12 Depth=1
                                        #     Parent Loop BB3_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, z(%rip)
	testq	%rax, %rax
	je	.LBB3_33
# BB#31:                                # %.lr.ph629
                                        #   in Loop: Header=BB3_30 Depth=3
	cmpl	%r11d, (%rax)
	jne	.LBB3_29
# BB#32:                                #   in Loop: Header=BB3_14 Depth=2
	movl	$0, -72(%rsp)           # 4-byte Folded Spill
	xorl	%edx, %edx
	jmp	.LBB3_34
	.p2align	4, 0x90
.LBB3_33:                               # %._crit_edge630
                                        #   in Loop: Header=BB3_14 Depth=2
	movq	112(%rsp), %rax         # 8-byte Reload
	movsbq	(%rax,%r11), %rax
	movq	80(%rsp), %rsi          # 8-byte Reload
	shlq	$10, %rsi
	addq	120(%rsp), %rsi         # 8-byte Folded Reload
	addl	(%rsi,%rax,4), %edx
	movb	$1, %al
	movl	%eax, -72(%rsp)         # 4-byte Spill
.LBB3_34:                               # %.thread
                                        #   in Loop: Header=BB3_14 Depth=2
	testl	%edx, %edx
	movl	$0, %eax
	cmovlel	%eax, %edx
	cmovlel	%ecx, %r9d
	cmovlel	%r11d, %r10d
	cmpl	%r8d, %edx
	movl	%r9d, %eax
	movl	%r8d, %r9d
	movl	%r13d, %esi
	movl	%r12d, %ebx
	jl	.LBB3_39
# BB#35:                                #   in Loop: Header=BB3_14 Depth=2
	movl	%eax, %esi
	jne	.LBB3_36
# BB#37:                                #   in Loop: Header=BB3_14 Depth=2
	movl	%r13d, %eax
	movl	%esi, %edx
	cmpl	%eax, %edx
	movl	%r8d, %r9d
	movl	%eax, %esi
	movl	%r12d, %ebx
	jl	.LBB3_39
# BB#38:                                #   in Loop: Header=BB3_14 Depth=2
	movl	%r12d, %eax
	cmpl	%eax, %r10d
	movl	%r10d, %ebx
	cmovll	%eax, %ebx
	cmpl	%r13d, %edx
	cmovnel	%r10d, %ebx
	movl	%r8d, %r9d
	movl	%edx, %esi
	jmp	.LBB3_39
	.p2align	4, 0x90
.LBB3_36:                               #   in Loop: Header=BB3_14 Depth=2
	movl	%edx, %r9d
	movl	%r10d, %ebx
.LBB3_39:                               #   in Loop: Header=BB3_14 Depth=2
	cmpl	%r14d, %r9d
	movl	%r14d, %r15d
	movl	-124(%rsp), %edx        # 4-byte Reload
	movl	%edx, %eax
	movl	-128(%rsp), %ebp        # 4-byte Reload
	jl	.LBB3_44
# BB#40:                                #   in Loop: Header=BB3_14 Depth=2
	jne	.LBB3_41
# BB#42:                                #   in Loop: Header=BB3_14 Depth=2
	cmpl	%edx, %esi
	movl	%r14d, %r15d
	movl	%edx, %eax
	movl	-128(%rsp), %ebp        # 4-byte Reload
	jl	.LBB3_44
# BB#43:                                #   in Loop: Header=BB3_14 Depth=2
	movl	-128(%rsp), %eax        # 4-byte Reload
	cmpl	%eax, %ebx
	movl	%ebx, %ebp
	cmovll	%eax, %ebp
	cmpl	%edx, %esi
	cmovnel	%ebx, %ebp
	movl	%r14d, %r15d
	movl	%esi, %eax
	jmp	.LBB3_44
	.p2align	4, 0x90
.LBB3_41:                               #   in Loop: Header=BB3_14 Depth=2
	movl	%r9d, %r15d
	movl	%esi, %eax
	movl	%ebx, %ebp
.LBB3_44:                               #   in Loop: Header=BB3_14 Depth=2
	movq	-16(%rsp), %rdx         # 8-byte Reload
	movl	%r15d, (%rdx,%r11,4)
	movq	-24(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx,%r11,4), %r9d
	movq	-32(%rsp), %rsi         # 8-byte Reload
	movl	(%rsi,%r11,4), %r10d
	movl	%eax, (%rdx,%r11,4)
	movl	%ebp, (%rsi,%r11,4)
	movq	-40(%rsp), %rdx         # 8-byte Reload
	movl	%r8d, (%rdx,%r11,4)
	movq	-48(%rsp), %rdx         # 8-byte Reload
	movl	%r13d, (%rdx,%r11,4)
	movq	-56(%rsp), %rdx         # 8-byte Reload
	movl	%r12d, (%rdx,%r11,4)
	cmpl	min(%rip), %r15d
	jle	.LBB3_46
# BB#45:                                #   in Loop: Header=BB3_14 Depth=2
	movb	$1, flag(%rip)
.LBB3_46:                               #   in Loop: Header=BB3_14 Depth=2
	cmpq	-96(%rsp), %r11         # 8-byte Folded Reload
	leaq	-1(%r11), %r11
	movl	-112(%rsp), %edx        # 4-byte Reload
	movl	56(%rsp), %ebx          # 4-byte Reload
	jg	.LBB3_14
# BB#47:                                # %._crit_edge643
                                        #   in Loop: Header=BB3_12 Depth=1
	movl	-72(%rsp), %eax         # 4-byte Reload
	movb	%al, tt(%rip)
	movl	-76(%rsp), %edx         # 4-byte Reload
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	64(%rsp), %r15          # 8-byte Reload
	movl	%r12d, %r11d
	movl	%r13d, %ebx
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	-88(%rsp), %rsi         # 8-byte Reload
	movb	48(%rsp), %al           # 1-byte Reload
.LBB3_48:                               #   in Loop: Header=BB3_12 Depth=1
	testb	$1, %al
	je	.LBB3_50
# BB#49:                                #   in Loop: Header=BB3_12 Depth=1
	movq	-16(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rbp,4), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	-104(%rsp), %rdi        # 8-byte Reload
	movl	%eax, (%rcx,%rdi,4)
	movq	-24(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rbp,4), %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	-104(%rsp), %rdi        # 8-byte Reload
	movl	%eax, (%rcx,%rdi,4)
	movq	-32(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rbp,4), %eax
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movq	-104(%rsp), %rdi        # 8-byte Reload
	movl	%eax, (%rcx,%rdi,4)
	movq	-40(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rbp,4), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	-104(%rsp), %rdi        # 8-byte Reload
	movl	%eax, (%rcx,%rdi,4)
	movq	-48(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rbp,4), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	-104(%rsp), %rdi        # 8-byte Reload
	movl	%eax, (%rcx,%rdi,4)
	movq	-56(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rbp,4), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	-104(%rsp), %rdi        # 8-byte Reload
	movl	%eax, (%rcx,%rdi,4)
	movq	-104(%rsp), %rcx        # 8-byte Reload
.LBB3_50:                               #   in Loop: Header=BB3_12 Depth=1
	cmpq	%rsi, %rcx
	leaq	-1(%rcx), %rcx
	jg	.LBB3_12
.LBB3_51:                               # %._crit_edge652
	movl	-116(%rsp), %ecx        # 4-byte Reload
	movl	%ecx, rl(%rip)
	movl	-120(%rsp), %eax        # 4-byte Reload
	movl	%eax, cl(%rip)
	incl	%r14d
	leal	1(%r15), %esi
	movl	%esi, 92(%rsp)          # 4-byte Spill
	movl	%eax, %ebp
	movl	%ecx, %r13d
	movq	%r14, 72(%rsp)          # 8-byte Spill
	jmp	.LBB3_52
	.p2align	4, 0x90
.LBB3_172:                              #   in Loop: Header=BB3_52 Depth=1
	movslq	numnode(%rip), %r10
	testq	%r10, %r10
	jle	.LBB3_173
# BB#174:                               # %.lr.ph.i
                                        #   in Loop: Header=BB3_52 Depth=1
	movq	LIST(%rip), %rsi
	leal	-1(%r13), %r9d
	movq	-88(%rsp), %rax         # 8-byte Reload
	leal	-1(%rax), %r8d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_175:                              #   Parent Loop BB3_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi,%rcx,8), %rbp
	movl	4(%rbp), %eax
	cmpl	%r15d, %eax
	jg	.LBB3_186
# BB#176:                               #   in Loop: Header=BB3_175 Depth=2
	movl	8(%rbp), %edi
	cmpl	-64(%rsp), %edi         # 4-byte Folded Reload
	jg	.LBB3_186
# BB#177:                               #   in Loop: Header=BB3_175 Depth=2
	cmpl	%r9d, 24(%rbp)
	jl	.LBB3_186
# BB#178:                               #   in Loop: Header=BB3_175 Depth=2
	cmpl	%r8d, 32(%rbp)
	jl	.LBB3_186
# BB#179:                               #   in Loop: Header=BB3_175 Depth=2
	cmpl	-116(%rsp), %eax        # 4-byte Folded Reload
	jl	.LBB3_181
# BB#180:                               #   in Loop: Header=BB3_175 Depth=2
	cmpl	-120(%rsp), %edi        # 4-byte Folded Reload
	jl	.LBB3_181
	.p2align	4, 0x90
.LBB3_186:                              #   in Loop: Header=BB3_175 Depth=2
	incq	%rcx
	cmpq	%r10, %rcx
	jl	.LBB3_175
	jmp	.LBB3_187
	.p2align	4, 0x90
.LBB3_173:                              #   in Loop: Header=BB3_52 Depth=1
	xorl	%ecx, %ecx
.LBB3_187:                              # %no_cross.exit
                                        #   in Loop: Header=BB3_52 Depth=1
	cmpl	%r10d, %ecx
	movq	-88(%rsp), %rbp         # 8-byte Reload
	jne	.LBB3_52
	jmp	.LBB3_188
.LBB3_181:                              #   in Loop: Header=BB3_52 Depth=1
	movl	-116(%rsp), %esi        # 4-byte Reload
	cmpl	%esi, %eax
	jge	.LBB3_183
# BB#182:                               #   in Loop: Header=BB3_52 Depth=1
	movl	%eax, rl(%rip)
	movl	%eax, %esi
.LBB3_183:                              #   in Loop: Header=BB3_52 Depth=1
	movl	%esi, -116(%rsp)        # 4-byte Spill
	cmpl	-120(%rsp), %edi        # 4-byte Folded Reload
	jge	.LBB3_185
# BB#184:                               #   in Loop: Header=BB3_52 Depth=1
	movl	%edi, cl(%rip)
	movl	%edi, -120(%rsp)        # 4-byte Spill
.LBB3_185:                              #   in Loop: Header=BB3_52 Depth=1
	movb	$1, flag(%rip)
	jmp	.LBB3_187
	.p2align	4, 0x90
.LBB3_52:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_53 Depth 2
                                        #       Child Loop BB3_54 Depth 3
                                        #         Child Loop BB3_55 Depth 4
                                        #           Child Loop BB3_62 Depth 5
                                        #             Child Loop BB3_76 Depth 6
                                        #         Child Loop BB3_121 Depth 4
                                        #           Child Loop BB3_137 Depth 5
                                        #     Child Loop BB3_175 Depth 2
	movw	$1, %r8w
	movq	%r13, %rax
	movl	%ebp, %r13d
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB3_53:                               # %.backedge.outer
                                        #   Parent Loop BB3_52 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_54 Depth 3
                                        #         Child Loop BB3_55 Depth 4
                                        #           Child Loop BB3_62 Depth 5
                                        #             Child Loop BB3_76 Depth 6
                                        #         Child Loop BB3_121 Depth 4
                                        #           Child Loop BB3_137 Depth 5
	movw	$1, %ax
	movl	%eax, -96(%rsp)         # 4-byte Spill
.LBB3_54:                               # %.backedge.outer720
                                        #   Parent Loop BB3_52 Depth=1
                                        #     Parent Loop BB3_53 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_55 Depth 4
                                        #           Child Loop BB3_62 Depth 5
                                        #             Child Loop BB3_76 Depth 6
                                        #         Child Loop BB3_121 Depth 4
                                        #           Child Loop BB3_137 Depth 5
	movl	%r13d, %ebp
	movslq	%ebp, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r13d
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	%rbp, -88(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_55:                               # %.backedge
                                        #   Parent Loop BB3_52 Depth=1
                                        #     Parent Loop BB3_53 Depth=2
                                        #       Parent Loop BB3_54 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB3_62 Depth 5
                                        #             Child Loop BB3_76 Depth 6
	cmpw	$0, -96(%rsp)           # 2-byte Folded Reload
	je	.LBB3_57
# BB#56:                                # %.backedge
                                        #   in Loop: Header=BB3_55 Depth=4
	cmpl	$1, %r13d
	jle	.LBB3_57
# BB#60:                                #   in Loop: Header=BB3_55 Depth=4
	movslq	%r13d, %r12
	leaq	-1(%r12), %r9
	cmpl	%ebp, -64(%rsp)         # 4-byte Folded Reload
	movl	%r9d, m1(%rip)
	movl	%r9d, %r10d
	movl	%r14d, %edi
	movl	%r9d, %eax
	movl	%eax, -124(%rsp)        # 4-byte Spill
	movl	%r14d, %eax
	movl	%eax, -128(%rsp)        # 4-byte Spill
	movl	$0, -96(%rsp)           # 4-byte Folded Spill
	jl	.LBB3_102
# BB#61:                                # %.lr.ph593.preheader
                                        #   in Loop: Header=BB3_55 Depth=4
	movl	$0, -112(%rsp)          # 4-byte Folded Spill
	movl	%r8d, -108(%rsp)        # 4-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%r12, -72(%rsp)         # 8-byte Spill
	movsbq	-1(%rax,%r12), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r8d, %r8d
	subl	q(%rip), %r8d
	movl	r(%rip), %esi
	movl	qr(%rip), %edx
	movq	%r14, %r11
	movq	-64(%rsp), %r15         # 8-byte Reload
	movl	$0, -96(%rsp)           # 4-byte Folded Spill
	movl	%r11d, %eax
	movl	%eax, -128(%rsp)        # 4-byte Spill
	movl	%r9d, %eax
	movl	%eax, -124(%rsp)        # 4-byte Spill
	movl	%r11d, %edi
	movl	%r9d, %r10d
	xorl	%ecx, %ecx
	movl	%esi, -104(%rsp)        # 4-byte Spill
	movl	%edx, 80(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB3_62:                               # %.lr.ph593
                                        #   Parent Loop BB3_52 Depth=1
                                        #     Parent Loop BB3_53 Depth=2
                                        #       Parent Loop BB3_54 Depth=3
                                        #         Parent Loop BB3_55 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB3_76 Depth 6
	movl	-112(%rsp), %r11d       # 4-byte Reload
	subl	%esi, %r8d
	subl	%edx, %ecx
	cmpl	%ecx, %r8d
	jge	.LBB3_64
# BB#63:                                #   in Loop: Header=BB3_62 Depth=5
	movl	%ecx, %r8d
	jmp	.LBB3_66
	.p2align	4, 0x90
.LBB3_64:                               #   in Loop: Header=BB3_62 Depth=5
	jne	.LBB3_68
# BB#65:                                #   in Loop: Header=BB3_62 Depth=5
	movl	-124(%rsp), %ecx        # 4-byte Reload
	cmpl	%r10d, %ecx
	jge	.LBB3_67
	.p2align	4, 0x90
.LBB3_66:                               #   in Loop: Header=BB3_62 Depth=5
	movl	%r10d, -124(%rsp)       # 4-byte Spill
	movl	%edi, -128(%rsp)        # 4-byte Spill
.LBB3_68:                               #   in Loop: Header=BB3_62 Depth=5
	movq	-16(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%r15,4), %ebx
	movl	%ebx, -112(%rsp)        # 4-byte Spill
	subl	%edx, %ebx
	movq	-24(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%r15,4), %eax
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%r15,4), %edi
	movq	-40(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%r15,4), %edx
	subl	%esi, %edx
	cmpl	%ebx, %edx
	jl	.LBB3_69
# BB#70:                                #   in Loop: Header=BB3_62 Depth=5
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%r15,4), %ebp
	movq	-56(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%r15,4), %ecx
	jne	.LBB3_71
# BB#72:                                #   in Loop: Header=BB3_62 Depth=5
	cmpl	%eax, %ebp
	jge	.LBB3_73
	.p2align	4, 0x90
.LBB3_69:                               #   in Loop: Header=BB3_62 Depth=5
	movl	%eax, %ebp
	jmp	.LBB3_74
	.p2align	4, 0x90
.LBB3_71:                               #   in Loop: Header=BB3_62 Depth=5
	movl	%edx, %ebx
	movl	%ecx, %edi
	jmp	.LBB3_74
.LBB3_67:                               #   in Loop: Header=BB3_62 Depth=5
	movl	-128(%rsp), %eax        # 4-byte Reload
	cmpl	%edi, %eax
	cmovgel	%eax, %edi
	cmpl	%r10d, %ecx
	cmovel	%edi, %eax
	movl	%eax, -128(%rsp)        # 4-byte Spill
	jmp	.LBB3_68
.LBB3_73:                               #   in Loop: Header=BB3_62 Depth=5
	cmpl	%edi, %ecx
	cmovgel	%ecx, %edi
	cmpl	%eax, %ebp
	cmovnel	%ecx, %edi
	.p2align	4, 0x90
.LBB3_74:                               #   in Loop: Header=BB3_62 Depth=5
	movq	-72(%rsp), %rcx         # 8-byte Reload
	movl	%r14d, %edx
	movq	row(%rip), %rax
	movq	-8(%rax,%rcx,8), %rax
	jmp	.LBB3_76
	.p2align	4, 0x90
.LBB3_75:                               #   in Loop: Header=BB3_76 Depth=6
	movq	8(%rax), %rax
.LBB3_76:                               #   Parent Loop BB3_52 Depth=1
                                        #     Parent Loop BB3_53 Depth=2
                                        #       Parent Loop BB3_54 Depth=3
                                        #         Parent Loop BB3_55 Depth=4
                                        #           Parent Loop BB3_62 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	%rax, z(%rip)
	testq	%rax, %rax
	je	.LBB3_79
# BB#77:                                # %.lr.ph
                                        #   in Loop: Header=BB3_76 Depth=6
	cmpl	%r15d, (%rax)
	jne	.LBB3_75
# BB#78:                                #   in Loop: Header=BB3_62 Depth=5
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	xorl	%r11d, %r11d
	jmp	.LBB3_80
	.p2align	4, 0x90
.LBB3_79:                               # %._crit_edge
                                        #   in Loop: Header=BB3_62 Depth=5
	movq	112(%rsp), %rax         # 8-byte Reload
	movsbq	(%rax,%r15), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	shlq	$10, %rcx
	addq	120(%rsp), %rcx         # 8-byte Folded Reload
	addl	(%rcx,%rax,4), %r11d
	movb	$1, %al
	movl	%eax, 48(%rsp)          # 4-byte Spill
.LBB3_80:                               # %.thread569
                                        #   in Loop: Header=BB3_62 Depth=5
	testl	%r11d, %r11d
	movl	$0, %eax
	cmovlel	%eax, %r11d
	movq	%r9, %rsi
	cmovlel	%r9d, %r13d
	cmovlel	%r15d, %edx
	cmpl	%ebx, %r11d
	movq	%r13, %rcx
	movl	%ebx, %r13d
	movl	%ebp, %r12d
	movl	%ebp, %eax
	movl	%edi, %r9d
	movl	%edi, %r14d
	jl	.LBB3_85
# BB#81:                                #   in Loop: Header=BB3_62 Depth=5
	jne	.LBB3_82
# BB#83:                                #   in Loop: Header=BB3_62 Depth=5
	movl	%r12d, %eax
	cmpl	%eax, %ecx
	movl	%ebx, %r13d
	movl	%r9d, %r14d
	jl	.LBB3_85
# BB#84:                                #   in Loop: Header=BB3_62 Depth=5
	movl	%r9d, %eax
	cmpl	%eax, %edx
	movl	%edx, %r14d
	cmovll	%eax, %r14d
	cmpl	%r12d, %ecx
	cmovnel	%edx, %r14d
	movl	%ebx, %r13d
	movl	%ecx, %eax
	jmp	.LBB3_85
	.p2align	4, 0x90
.LBB3_82:                               #   in Loop: Header=BB3_62 Depth=5
	movl	%r11d, %r13d
	movl	%ecx, %eax
	movl	%edx, %r14d
.LBB3_85:                               #   in Loop: Header=BB3_62 Depth=5
	cmpl	%r8d, %r13d
	movl	%r8d, %ecx
	movl	-124(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %r10d
	movl	-128(%rsp), %edi        # 4-byte Reload
	jl	.LBB3_90
# BB#86:                                #   in Loop: Header=BB3_62 Depth=5
	jne	.LBB3_87
# BB#88:                                #   in Loop: Header=BB3_62 Depth=5
	cmpl	%ebp, %eax
	movl	%r8d, %ecx
	movl	%ebp, %r10d
	movl	-128(%rsp), %edi        # 4-byte Reload
	jl	.LBB3_90
# BB#89:                                #   in Loop: Header=BB3_62 Depth=5
	movl	-128(%rsp), %ecx        # 4-byte Reload
	cmpl	%ecx, %r14d
	movl	%r14d, %edi
	cmovll	%ecx, %edi
	cmpl	%ebp, %eax
	cmovnel	%r14d, %edi
	movl	%r8d, %ecx
	movl	%eax, %r10d
	jmp	.LBB3_90
	.p2align	4, 0x90
.LBB3_87:                               #   in Loop: Header=BB3_62 Depth=5
	movl	%r13d, %ecx
	movl	%eax, %r10d
	movl	%r14d, %edi
.LBB3_90:                               #   in Loop: Header=BB3_62 Depth=5
	movq	-16(%rsp), %rax         # 8-byte Reload
	movl	%ecx, (%rax,%r15,4)
	movq	-24(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%r15,4), %r13d
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx,%r15,4), %r14d
	movl	%r10d, (%rax,%r15,4)
	movl	%edi, (%rdx,%r15,4)
	movq	-40(%rsp), %rax         # 8-byte Reload
	movl	%ebx, (%rax,%r15,4)
	movq	-48(%rsp), %rax         # 8-byte Reload
	movl	%r12d, %ebx
	movl	%ebx, (%rax,%r15,4)
	movq	-56(%rsp), %rax         # 8-byte Reload
	movl	%r9d, %r11d
	movl	%r11d, (%rax,%r15,4)
	cmpl	min(%rip), %ecx
	jle	.LBB3_92
# BB#91:                                #   in Loop: Header=BB3_62 Depth=5
	movb	$1, flag(%rip)
.LBB3_92:                               #   in Loop: Header=BB3_62 Depth=5
	cmpw	$0, -96(%rsp)           # 2-byte Folded Reload
	movq	%rsi, %r9
	movl	-104(%rsp), %esi        # 4-byte Reload
	jne	.LBB3_100
# BB#93:                                #   in Loop: Header=BB3_62 Depth=5
	cmpl	-116(%rsp), %r10d       # 4-byte Folded Reload
	jle	.LBB3_95
# BB#94:                                #   in Loop: Header=BB3_62 Depth=5
	cmpl	-120(%rsp), %edi        # 4-byte Folded Reload
	jg	.LBB3_99
.LBB3_95:                               #   in Loop: Header=BB3_62 Depth=5
	cmpl	-116(%rsp), %ebx        # 4-byte Folded Reload
	jle	.LBB3_97
# BB#96:                                #   in Loop: Header=BB3_62 Depth=5
	cmpl	-120(%rsp), %r11d       # 4-byte Folded Reload
	jg	.LBB3_99
.LBB3_97:                               #   in Loop: Header=BB3_62 Depth=5
	movl	$0, -96(%rsp)           # 4-byte Folded Spill
	cmpl	-116(%rsp), %ebp        # 4-byte Folded Reload
	jle	.LBB3_100
# BB#98:                                #   in Loop: Header=BB3_62 Depth=5
	movl	-128(%rsp), %eax        # 4-byte Reload
	cmpl	-120(%rsp), %eax        # 4-byte Folded Reload
	jle	.LBB3_100
	.p2align	4, 0x90
.LBB3_99:                               #   in Loop: Header=BB3_62 Depth=5
	movw	$1, %ax
	movl	%eax, -96(%rsp)         # 4-byte Spill
.LBB3_100:                              #   in Loop: Header=BB3_62 Depth=5
	cmpq	56(%rsp), %r15          # 8-byte Folded Reload
	leaq	-1(%r15), %r15
	movl	80(%rsp), %edx          # 4-byte Reload
	jg	.LBB3_62
# BB#101:                               # %._crit_edge594
                                        #   in Loop: Header=BB3_55 Depth=4
	movl	48(%rsp), %eax          # 4-byte Reload
	movb	%al, tt(%rip)
	movl	-76(%rsp), %edx         # 4-byte Reload
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	-88(%rsp), %rbp         # 8-byte Reload
	movl	-108(%rsp), %r8d        # 4-byte Reload
	movq	-72(%rsp), %r12         # 8-byte Reload
.LBB3_102:                              # %._crit_edge707
                                        #   in Loop: Header=BB3_55 Depth=4
	movq	-16(%rsp), %rax         # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	movl	(%rax,%rsi,4), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, -4(%rcx,%r12,4)
	movq	-24(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rsi,4), %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%eax, -4(%rcx,%r12,4)
	movq	-32(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rsi,4), %eax
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movl	%eax, -4(%rcx,%r12,4)
	movq	-40(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rsi,4), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%eax, -4(%rcx,%r12,4)
	movq	-48(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rsi,4), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, -4(%rcx,%r12,4)
	movq	-56(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rsi,4), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, -4(%rcx,%r12,4)
	testw	%r8w, %r8w
	jne	.LBB3_110
# BB#103:                               #   in Loop: Header=BB3_55 Depth=4
	cmpl	-116(%rsp), %r10d       # 4-byte Folded Reload
	jle	.LBB3_105
# BB#104:                               #   in Loop: Header=BB3_55 Depth=4
	cmpl	-120(%rsp), %edi        # 4-byte Folded Reload
	jg	.LBB3_109
.LBB3_105:                              #   in Loop: Header=BB3_55 Depth=4
	cmpl	-116(%rsp), %ebx        # 4-byte Folded Reload
	jle	.LBB3_107
# BB#106:                               #   in Loop: Header=BB3_55 Depth=4
	cmpl	-120(%rsp), %r11d       # 4-byte Folded Reload
	jg	.LBB3_109
.LBB3_107:                              #   in Loop: Header=BB3_55 Depth=4
	xorl	%r8d, %r8d
	movl	-124(%rsp), %eax        # 4-byte Reload
	cmpl	-116(%rsp), %eax        # 4-byte Folded Reload
	jle	.LBB3_110
# BB#108:                               #   in Loop: Header=BB3_55 Depth=4
	movl	-128(%rsp), %eax        # 4-byte Reload
	cmpl	-120(%rsp), %eax        # 4-byte Folded Reload
	jle	.LBB3_110
.LBB3_109:                              #   in Loop: Header=BB3_55 Depth=4
	movw	$1, %r8w
	cmpl	$1, %edx
	je	.LBB3_111
	jmp	.LBB3_112
	.p2align	4, 0x90
.LBB3_57:                               #   in Loop: Header=BB3_55 Depth=4
	testw	%r8w, %r8w
	je	.LBB3_170
# BB#58:                                #   in Loop: Header=BB3_55 Depth=4
	cmpl	$1, %ebp
	jle	.LBB3_170
# BB#59:                                #   in Loop: Header=BB3_55 Depth=4
	movl	%r13d, %r9d
.LBB3_110:                              # %.critedge.thread714
                                        #   in Loop: Header=BB3_55 Depth=4
	cmpl	$1, %edx
	jne	.LBB3_112
.LBB3_111:                              #   in Loop: Header=BB3_55 Depth=4
	leal	1(%r9), %eax
	cmpl	%eax, %ebp
	movl	$0, %eax
	cmovnew	%r8w, %ax
	cmpw	$0, -96(%rsp)           # 2-byte Folded Reload
	cmovnew	%r8w, %ax
	movw	%ax, %r8w
.LBB3_112:                              #   in Loop: Header=BB3_55 Depth=4
	testw	%r8w, %r8w
	movl	%r9d, %r13d
	je	.LBB3_55
# BB#113:                               #   in Loop: Header=BB3_55 Depth=4
	cmpl	$2, %ebp
	movl	%r9d, %r13d
	jl	.LBB3_55
# BB#114:                               #   in Loop: Header=BB3_54 Depth=3
	movslq	%ebp, %rsi
	leaq	-1(%rsi), %r13
	movl	%r13d, n1(%rip)
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
	subl	q(%rip), %r14d
	cmpl	$2, %edx
	movq	112(%rsp), %rax         # 8-byte Reload
	movsbq	-1(%rax,%rsi), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	je	.LBB3_115
# BB#116:                               #   in Loop: Header=BB3_54 Depth=3
	cmpl	%r13d, %r15d
	jl	.LBB3_115
# BB#117:                               #   in Loop: Header=BB3_54 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	-4(%rax,%rsi,4), %r8d
	movq	(%rsp), %rax            # 8-byte Reload
	movl	-4(%rax,%rsi,4), %edi
	movq	-8(%rsp), %rax          # 8-byte Reload
	movl	-4(%rax,%rsi,4), %ecx
	addl	$-2, %ebp
	movl	%ebp, %eax
	movl	%r13d, %r15d
	movl	%ecx, %ebp
	jmp	.LBB3_118
	.p2align	4, 0x90
.LBB3_115:                              #   in Loop: Header=BB3_54 Depth=3
	movl	%r15d, %eax
	movl	92(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %r15d
	movl	%ecx, %edi
.LBB3_118:                              # %.preheader
                                        #   in Loop: Header=BB3_54 Depth=3
	cmpl	%r9d, %eax
	movq	%r9, 40(%rsp)           # 8-byte Spill
	jge	.LBB3_120
# BB#119:                               # %.preheader._crit_edge
                                        #   in Loop: Header=BB3_54 Depth=3
	movslq	%r9d, %rdi
	xorl	%r8d, %r8d
	movl	%r13d, %r12d
	movl	%r15d, -124(%rsp)       # 4-byte Spill
	movl	%r13d, %eax
	movl	%eax, -112(%rsp)        # 4-byte Spill
	jmp	.LBB3_163
	.p2align	4, 0x90
.LBB3_120:                              # %.lr.ph617.preheader
                                        #   in Loop: Header=BB3_54 Depth=3
	movl	%edi, %ebx
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	movq	%rbp, -88(%rsp)         # 8-byte Spill
	movslq	%eax, %rdx
	movl	r(%rip), %eax
	movl	%eax, -104(%rsp)        # 4-byte Spill
	movl	qr(%rip), %eax
	movl	%eax, 80(%rsp)          # 4-byte Spill
	movslq	%r9d, %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	movl	$0, -108(%rsp)          # 4-byte Folded Spill
	movl	%r13d, %eax
	movl	%eax, -112(%rsp)        # 4-byte Spill
	movl	%r15d, -124(%rsp)       # 4-byte Spill
	movl	%r13d, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_121:                              # %.lr.ph617
                                        #   Parent Loop BB3_52 Depth=1
                                        #     Parent Loop BB3_53 Depth=2
                                        #       Parent Loop BB3_54 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB3_137 Depth 5
	movl	-104(%rsp), %esi        # 4-byte Reload
	subl	%esi, %r14d
	movl	80(%rsp), %edi          # 4-byte Reload
	subl	%edi, %ebp
	cmpl	%ebp, %r14d
	jge	.LBB3_123
# BB#122:                               #   in Loop: Header=BB3_121 Depth=4
	movl	%ebp, %r14d
	movl	%r12d, -112(%rsp)       # 4-byte Spill
	jmp	.LBB3_128
	.p2align	4, 0x90
.LBB3_123:                              #   in Loop: Header=BB3_121 Depth=4
	jne	.LBB3_124
# BB#125:                               #   in Loop: Header=BB3_121 Depth=4
	movl	-124(%rsp), %ecx        # 4-byte Reload
	cmpl	%r15d, %ecx
	jge	.LBB3_127
# BB#126:                               #   in Loop: Header=BB3_121 Depth=4
	movl	%r12d, -112(%rsp)       # 4-byte Spill
	jmp	.LBB3_128
	.p2align	4, 0x90
.LBB3_124:                              #   in Loop: Header=BB3_121 Depth=4
	movl	-124(%rsp), %r15d       # 4-byte Reload
	jmp	.LBB3_128
.LBB3_127:                              #   in Loop: Header=BB3_121 Depth=4
	movl	-112(%rsp), %eax        # 4-byte Reload
	cmpl	%r12d, %eax
	cmovgel	%eax, %r12d
	cmpl	%r15d, %ecx
	cmovel	%r12d, %eax
	movl	%eax, -112(%rsp)        # 4-byte Spill
	movl	%ecx, %r15d
	.p2align	4, 0x90
.LBB3_128:                              #   in Loop: Header=BB3_121 Depth=4
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax,%rdx,4), %r9d
	movl	%r9d, 56(%rsp)          # 4-byte Spill
	subl	%edi, %r9d
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax,%rdx,4), %eax
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%rdx,4), %r10d
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%rdx,4), %edi
	subl	%esi, %edi
	cmpl	%r9d, %edi
	jge	.LBB3_130
# BB#129:                               #   in Loop: Header=BB3_121 Depth=4
	movl	%eax, -128(%rsp)        # 4-byte Spill
	jmp	.LBB3_135
	.p2align	4, 0x90
.LBB3_130:                              #   in Loop: Header=BB3_121 Depth=4
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%rdx,4), %ebp
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%rdx,4), %ecx
	jne	.LBB3_131
# BB#132:                               #   in Loop: Header=BB3_121 Depth=4
	cmpl	%eax, %ebp
	jge	.LBB3_134
# BB#133:                               #   in Loop: Header=BB3_121 Depth=4
	movl	%eax, -128(%rsp)        # 4-byte Spill
	jmp	.LBB3_135
	.p2align	4, 0x90
.LBB3_131:                              #   in Loop: Header=BB3_121 Depth=4
	movl	%ebp, -128(%rsp)        # 4-byte Spill
	movl	%edi, %r9d
	movl	%ecx, %r10d
	jmp	.LBB3_135
.LBB3_134:                              #   in Loop: Header=BB3_121 Depth=4
	cmpl	%r10d, %ecx
	cmovgel	%ecx, %r10d
	movl	%ebp, -128(%rsp)        # 4-byte Spill
	cmpl	%eax, %ebp
	cmovnel	%ecx, %r10d
	.p2align	4, 0x90
.LBB3_135:                              #   in Loop: Header=BB3_121 Depth=4
	movq	row(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	jmp	.LBB3_137
	.p2align	4, 0x90
.LBB3_136:                              #   in Loop: Header=BB3_137 Depth=5
	movq	8(%rax), %rax
.LBB3_137:                              #   Parent Loop BB3_52 Depth=1
                                        #     Parent Loop BB3_53 Depth=2
                                        #       Parent Loop BB3_54 Depth=3
                                        #         Parent Loop BB3_121 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rax, z(%rip)
	testq	%rax, %rax
	je	.LBB3_140
# BB#138:                               # %.lr.ph604
                                        #   in Loop: Header=BB3_137 Depth=5
	cmpl	%r13d, (%rax)
	jne	.LBB3_136
# BB#139:                               #   in Loop: Header=BB3_121 Depth=4
	xorl	%r11d, %r11d
	xorl	%r8d, %r8d
	jmp	.LBB3_141
	.p2align	4, 0x90
.LBB3_140:                              # %._crit_edge605
                                        #   in Loop: Header=BB3_121 Depth=4
	movq	104(%rsp), %rax         # 8-byte Reload
	movsbq	(%rax,%rdx), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	shlq	$10, %rcx
	addq	120(%rsp), %rcx         # 8-byte Folded Reload
	addl	(%rcx,%rax,4), %r8d
	movb	$1, %r11b
.LBB3_141:                              # %.thread570
                                        #   in Loop: Header=BB3_121 Depth=4
	testl	%r8d, %r8d
	movl	$0, %eax
	cmovlel	%eax, %r8d
	cmovlel	%edx, %ebx
	movq	-88(%rsp), %rax         # 8-byte Reload
	cmovlel	%r13d, %eax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	cmpl	%r9d, %r8d
	movl	%r9d, %edi
	movl	-128(%rsp), %ecx        # 4-byte Reload
	movl	%r10d, %eax
	jl	.LBB3_146
# BB#142:                               #   in Loop: Header=BB3_121 Depth=4
	jne	.LBB3_143
# BB#144:                               #   in Loop: Header=BB3_121 Depth=4
	movl	-128(%rsp), %eax        # 4-byte Reload
	cmpl	%eax, %ebx
	movl	%r9d, %edi
	movl	%eax, %ecx
	movl	%r10d, %eax
	jl	.LBB3_146
# BB#145:                               #   in Loop: Header=BB3_121 Depth=4
	movl	%r10d, %ecx
	movq	-88(%rsp), %rsi         # 8-byte Reload
	cmpl	%ecx, %esi
	movl	%esi, %eax
	cmovll	%ecx, %eax
	cmpl	-128(%rsp), %ebx        # 4-byte Folded Reload
	cmovnel	%esi, %eax
	movl	%r9d, %edi
	movl	%ebx, %ecx
	jmp	.LBB3_146
	.p2align	4, 0x90
.LBB3_143:                              #   in Loop: Header=BB3_121 Depth=4
	movl	%r8d, %edi
	movl	%ebx, %ecx
	movq	-88(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
.LBB3_146:                              #   in Loop: Header=BB3_121 Depth=4
	cmpl	%r14d, %edi
	movl	%r14d, %ebp
	movl	%r15d, -124(%rsp)       # 4-byte Spill
	movl	-112(%rsp), %r12d       # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	jl	.LBB3_151
# BB#147:                               #   in Loop: Header=BB3_121 Depth=4
	jne	.LBB3_148
# BB#149:                               #   in Loop: Header=BB3_121 Depth=4
	movl	-124(%rsp), %esi        # 4-byte Reload
	cmpl	%esi, %ecx
	movl	%r14d, %ebp
	movl	%esi, %r15d
	movl	-112(%rsp), %r12d       # 4-byte Reload
	jl	.LBB3_151
# BB#150:                               #   in Loop: Header=BB3_121 Depth=4
	movl	-112(%rsp), %esi        # 4-byte Reload
	cmpl	%esi, %eax
	movl	%eax, %r12d
	cmovll	%esi, %r12d
	cmpl	-124(%rsp), %ecx        # 4-byte Folded Reload
	cmovnel	%eax, %r12d
	movl	%r14d, %ebp
	movl	%ecx, %r15d
	jmp	.LBB3_151
	.p2align	4, 0x90
.LBB3_148:                              #   in Loop: Header=BB3_121 Depth=4
	movl	%edi, %ebp
	movl	%ecx, %r15d
	movl	%eax, %r12d
.LBB3_151:                              #   in Loop: Header=BB3_121 Depth=4
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%ebp, (%rax,%rdx,4)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax,%rdx,4), %ebx
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%rdx,4), %esi
	movq	%rsi, -88(%rsp)         # 8-byte Spill
	movl	%r15d, (%rax,%rdx,4)
	movl	%r12d, (%rcx,%rdx,4)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%r9d, (%rax,%rdx,4)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	-128(%rsp), %ecx        # 4-byte Reload
	movl	%ecx, (%rax,%rdx,4)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r10d, (%rax,%rdx,4)
	cmpl	min(%rip), %ebp
	jle	.LBB3_153
# BB#152:                               #   in Loop: Header=BB3_121 Depth=4
	movb	$1, flag(%rip)
.LBB3_153:                              #   in Loop: Header=BB3_121 Depth=4
	cmpw	$0, -108(%rsp)          # 2-byte Folded Reload
	movq	-72(%rsp), %rdi         # 8-byte Reload
	jne	.LBB3_161
# BB#154:                               #   in Loop: Header=BB3_121 Depth=4
	cmpl	-116(%rsp), %r15d       # 4-byte Folded Reload
	jle	.LBB3_156
# BB#155:                               #   in Loop: Header=BB3_121 Depth=4
	cmpl	-120(%rsp), %r12d       # 4-byte Folded Reload
	jg	.LBB3_160
.LBB3_156:                              #   in Loop: Header=BB3_121 Depth=4
	movl	-128(%rsp), %eax        # 4-byte Reload
	cmpl	-116(%rsp), %eax        # 4-byte Folded Reload
	jle	.LBB3_158
# BB#157:                               #   in Loop: Header=BB3_121 Depth=4
	cmpl	-120(%rsp), %r10d       # 4-byte Folded Reload
	jg	.LBB3_160
.LBB3_158:                              #   in Loop: Header=BB3_121 Depth=4
	movl	$0, -108(%rsp)          # 4-byte Folded Spill
	movl	-124(%rsp), %eax        # 4-byte Reload
	cmpl	-116(%rsp), %eax        # 4-byte Folded Reload
	jle	.LBB3_161
# BB#159:                               #   in Loop: Header=BB3_121 Depth=4
	movl	-112(%rsp), %eax        # 4-byte Reload
	cmpl	-120(%rsp), %eax        # 4-byte Folded Reload
	jle	.LBB3_161
	.p2align	4, 0x90
.LBB3_160:                              #   in Loop: Header=BB3_121 Depth=4
	movw	$1, %ax
	movl	%eax, -108(%rsp)        # 4-byte Spill
.LBB3_161:                              #   in Loop: Header=BB3_121 Depth=4
	cmpq	%rdi, %rdx
	leaq	-1(%rdx), %rdx
	jg	.LBB3_121
# BB#162:                               # %._crit_edge618
                                        #   in Loop: Header=BB3_54 Depth=3
	movb	%r11b, tt(%rip)
	movl	-76(%rsp), %edx         # 4-byte Reload
	movl	%r10d, %r11d
	movl	-128(%rsp), %ebx        # 4-byte Reload
	movl	-108(%rsp), %r8d        # 4-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
.LBB3_163:                              #   in Loop: Header=BB3_54 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax,%rdi,4), %eax
	movq	-16(%rsp), %rcx         # 8-byte Reload
	movl	%eax, -4(%rcx,%rsi,4)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax,%rdi,4), %eax
	movq	-24(%rsp), %rcx         # 8-byte Reload
	movl	%eax, -4(%rcx,%rsi,4)
	movq	-8(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%rdi,4), %eax
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movl	%eax, -4(%rcx,%rsi,4)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%rdi,4), %eax
	movq	-40(%rsp), %rcx         # 8-byte Reload
	movl	%eax, -4(%rcx,%rsi,4)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%rdi,4), %eax
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movl	%eax, -4(%rcx,%rsi,4)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%rdi,4), %eax
	movq	-56(%rsp), %rcx         # 8-byte Reload
	movl	%eax, -4(%rcx,%rsi,4)
	cmpw	$0, -96(%rsp)           # 2-byte Folded Reload
	jne	.LBB3_54
# BB#164:                               #   in Loop: Header=BB3_54 Depth=3
	cmpl	-116(%rsp), %r15d       # 4-byte Folded Reload
	jle	.LBB3_166
# BB#165:                               #   in Loop: Header=BB3_54 Depth=3
	cmpl	-120(%rsp), %r12d       # 4-byte Folded Reload
	jg	.LBB3_53
.LBB3_166:                              #   in Loop: Header=BB3_54 Depth=3
	cmpl	-116(%rsp), %ebx        # 4-byte Folded Reload
	jle	.LBB3_168
# BB#167:                               #   in Loop: Header=BB3_54 Depth=3
	cmpl	-120(%rsp), %r11d       # 4-byte Folded Reload
	jg	.LBB3_53
.LBB3_168:                              #   in Loop: Header=BB3_54 Depth=3
	movl	$0, -96(%rsp)           # 4-byte Folded Spill
	movl	-124(%rsp), %eax        # 4-byte Reload
	cmpl	-116(%rsp), %eax        # 4-byte Folded Reload
	jle	.LBB3_54
# BB#169:                               #   in Loop: Header=BB3_54 Depth=3
	movl	-112(%rsp), %eax        # 4-byte Reload
	cmpl	-120(%rsp), %eax        # 4-byte Folded Reload
	jg	.LBB3_53
	jmp	.LBB3_54
	.p2align	4, 0x90
.LBB3_170:                              #   in Loop: Header=BB3_52 Depth=1
	cmpl	$1, %r13d
	jne	.LBB3_172
# BB#171:                               #   in Loop: Header=BB3_52 Depth=1
	cmpl	$1, %ebp
	jne	.LBB3_172
.LBB3_188:
	decl	%r13d
	movl	%r13d, m1(%rip)
	decl	%ebp
	movl	%ebp, n1(%rip)
	addq	$128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	locate, .Lfunc_end3-locate
	.cfi_endproc

	.globl	small_pass
	.p2align	4, 0x90
	.type	small_pass,@function
small_pass:                             # @small_pass
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 192
.Lcfi69:
	.cfi_offset %rbx, -56
.Lcfi70:
	.cfi_offset %r12, -48
.Lcfi71:
	.cfi_offset %r13, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	movl	%edx, 60(%rsp)          # 4-byte Spill
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	movl	n1(%rip), %r14d
	movl	nn(%rip), %edi
	cmpl	%edi, %r14d
	jge	.LBB4_1
# BB#2:                                 # %.lr.ph217
	movslq	%r14d, %r11
	movslq	%edi, %r8
	movl	%edi, %esi
	movq	CC(%rip), %r9
	movl	m1(%rip), %eax
	movq	RR(%rip), %r10
	movq	EE(%rip), %r15
	xorl	%ebx, %ebx
	subl	q(%rip), %ebx
	movq	DD(%rip), %r12
	movq	SS(%rip), %rcx
	movq	FF(%rip), %rdx
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%r14, %r13
	subl	%r14d, %esi
	leaq	-1(%r8), %r14
	testb	$1, %sil
	movq	%r11, %rsi
	je	.LBB4_4
# BB#3:
	leaq	1(%r11), %rsi
	movl	$0, 4(%r9,%r11,4)
	movl	%eax, 4(%r10,%r11,4)
	movl	%esi, 4(%r15,%r11,4)
	movl	%ebx, 4(%r12,%r11,4)
	movl	%eax, 4(%rcx,%r11,4)
	movl	%esi, 4(%rdx,%r11,4)
.LBB4_4:                                # %.prol.loopexit
	cmpq	%r11, %r14
	movq	%r13, %r14
	je	.LBB4_8
# BB#5:                                 # %.lr.ph217.new
	movl	%edi, %r11d
	.p2align	4, 0x90
.LBB4_6:                                # =>This Inner Loop Header: Depth=1
	movl	$0, 4(%r9,%rsi,4)
	movl	%eax, 4(%r10,%rsi,4)
	leal	1(%rsi), %edi
	movl	%edi, 4(%r15,%rsi,4)
	movl	%ebx, 4(%r12,%rsi,4)
	movl	%eax, 4(%rcx,%rsi,4)
	movl	%edi, 4(%rdx,%rsi,4)
	leaq	2(%rsi), %rdi
	movl	$0, 8(%r9,%rsi,4)
	movl	%eax, 8(%r10,%rsi,4)
	movl	%edi, 8(%r15,%rsi,4)
	movl	%ebx, 8(%r12,%rsi,4)
	movl	%eax, 8(%rcx,%rsi,4)
	movl	%edi, 8(%rdx,%rsi,4)
	cmpq	%r8, %rdi
	movq	%rdi, %rsi
	jl	.LBB4_6
# BB#7:
	movl	%r11d, %edi
	cmpl	mm(%rip), %eax
	jl	.LBB4_9
	jmp	.LBB4_51
.LBB4_1:                                # %.._crit_edge218_crit_edge
	movl	m1(%rip), %eax
.LBB4_8:                                # %._crit_edge218
	cmpl	mm(%rip), %eax
	jge	.LBB4_51
.LBB4_9:                                # %.lr.ph212
	movslq	%eax, %rsi
	jmp	.LBB4_12
	.p2align	4, 0x90
.LBB4_10:                               # %.loopexit
                                        #   in Loop: Header=BB4_12 Depth=1
	movslq	mm(%rip), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpq	%rax, %rcx
	jge	.LBB4_51
# BB#11:                                # %.loopexit._crit_edge
                                        #   in Loop: Header=BB4_12 Depth=1
	movl	n1(%rip), %r14d
	movq	%rcx, %rsi
.LBB4_12:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_18 Depth 2
                                        #       Child Loop BB4_33 Depth 3
	leaq	1(%rsi), %rdx
	xorl	%eax, %eax
	subl	q(%rip), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	cmpl	$2, 56(%rsp)            # 4-byte Folded Reload
	movq	v(%rip), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movsbq	1(%rax,%rsi), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	je	.LBB4_14
# BB#13:                                #   in Loop: Header=BB4_12 Depth=1
	cmpl	%r14d, %esi
	jl	.LBB4_14
# BB#15:                                #   in Loop: Header=BB4_12 Depth=1
	movq	CC(%rip), %rax
	movl	4(%rax,%rsi,4), %ecx
	movq	RR(%rip), %rax
	movl	4(%rax,%rsi,4), %ebx
	movq	EE(%rip), %rax
	movl	4(%rax,%rsi,4), %ebp
	leal	2(%rsi), %eax
	movl	%edx, %r14d
	jmp	.LBB4_16
	.p2align	4, 0x90
.LBB4_14:                               #   in Loop: Header=BB4_12 Depth=1
	leal	1(%r14), %eax
	xorl	%ecx, %ecx
	movl	%esi, %ebx
	movl	%r14d, %ebp
.LBB4_16:                               # %.preheader
                                        #   in Loop: Header=BB4_12 Depth=1
	cmpl	%edi, %eax
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	jg	.LBB4_10
# BB#17:                                # %.lr.ph207.preheader
                                        #   in Loop: Header=BB4_12 Depth=1
	movl	%ebp, 40(%rsp)          # 4-byte Spill
	movl	%ebx, 44(%rsp)          # 4-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movslq	%eax, %rbx
	xorl	%r12d, %r12d
	movl	%edx, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%r14, %rbp
	movl	%edx, %r15d
	.p2align	4, 0x90
.LBB4_18:                               # %.lr.ph207
                                        #   Parent Loop BB4_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_33 Depth 3
	movl	r(%rip), %ebp
	movl	20(%rsp), %edx          # 4-byte Reload
	subl	%ebp, %edx
	movl	qr(%rip), %eax
	subl	%eax, %r12d
	cmpl	%r12d, %edx
	jge	.LBB4_20
# BB#19:                                #   in Loop: Header=BB4_18 Depth=2
	movl	%edi, %r8d
	movl	%r12d, 20(%rsp)         # 4-byte Spill
.LBB4_23:                               #   in Loop: Header=BB4_18 Depth=2
	movl	%r15d, 16(%rsp)         # 4-byte Spill
                                        # kill: %R14D<def> %R14D<kill> %R14<def>
	jmp	.LBB4_25
	.p2align	4, 0x90
.LBB4_20:                               #   in Loop: Header=BB4_18 Depth=2
	movl	%edx, 20(%rsp)          # 4-byte Spill
	jne	.LBB4_21
# BB#22:                                #   in Loop: Header=BB4_18 Depth=2
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	%edi, %r8d
	cmpl	%r15d, %edx
	jl	.LBB4_23
# BB#24:                                #   in Loop: Header=BB4_18 Depth=2
	movl	%edx, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	cmpl	%r14d, %edx
	cmovgel	%edx, %r14d
	cmpl	%r15d, %esi
	cmovel	%r14d, %edx
	movq	%rdx, %r14
	jmp	.LBB4_25
	.p2align	4, 0x90
.LBB4_21:                               #   in Loop: Header=BB4_18 Depth=2
	movl	%edi, %r8d
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB4_25:                               #   in Loop: Header=BB4_18 Depth=2
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	CC(%rip), %rdx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movl	(%rdx,%rbx,4), %r9d
	movl	%r9d, 64(%rsp)          # 4-byte Spill
	subl	%eax, %r9d
	movq	RR(%rip), %r10
	movl	(%r10,%rbx,4), %edx
	movq	EE(%rip), %r13
	movl	(%r13,%rbx,4), %r11d
	movq	DD(%rip), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	(%rax,%rbx,4), %edi
	subl	%ebp, %edi
	movq	SS(%rip), %rax
	movq	FF(%rip), %rsi
	cmpl	%r9d, %edi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movl	%r8d, 68(%rsp)          # 4-byte Spill
	movq	%rax, 112(%rsp)         # 8-byte Spill
	jge	.LBB4_27
# BB#26:                                #   in Loop: Header=BB4_18 Depth=2
	movl	%edx, %ebp
	jmp	.LBB4_32
	.p2align	4, 0x90
.LBB4_27:                               #   in Loop: Header=BB4_18 Depth=2
	movl	(%rax,%rbx,4), %ebp
	movl	(%rsi,%rbx,4), %esi
	jne	.LBB4_28
# BB#29:                                #   in Loop: Header=BB4_18 Depth=2
	cmpl	%edx, %ebp
	jge	.LBB4_31
# BB#30:                                #   in Loop: Header=BB4_18 Depth=2
	movl	%edx, %ebp
	jmp	.LBB4_32
	.p2align	4, 0x90
.LBB4_28:                               #   in Loop: Header=BB4_18 Depth=2
	movl	%edi, %r9d
	movl	%esi, %r11d
	jmp	.LBB4_32
.LBB4_31:                               #   in Loop: Header=BB4_18 Depth=2
	cmpl	%r11d, %esi
	cmovgel	%esi, %r11d
	cmpl	%edx, %ebp
	cmovnel	%esi, %r11d
	.p2align	4, 0x90
.LBB4_32:                               #   in Loop: Header=BB4_18 Depth=2
	movb	$1, tt(%rip)
	movq	row(%rip), %rdx
	movq	8(%rdx,%r12,8), %rdx
	jmp	.LBB4_33
	.p2align	4, 0x90
.LBB4_35:                               #   in Loop: Header=BB4_33 Depth=3
	movq	8(%rdx), %rdx
.LBB4_33:                               #   Parent Loop BB4_12 Depth=1
                                        #     Parent Loop BB4_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, z(%rip)
	testq	%rdx, %rdx
	je	.LBB4_34
# BB#36:                                # %.lr.ph
                                        #   in Loop: Header=BB4_33 Depth=3
	cmpl	%ebx, (%rdx)
	jne	.LBB4_35
# BB#37:                                # %.thread
                                        #   in Loop: Header=BB4_18 Depth=2
	movb	$0, tt(%rip)
	xorl	%ecx, %ecx
	jmp	.LBB4_38
	.p2align	4, 0x90
.LBB4_34:                               # %._crit_edge
                                        #   in Loop: Header=BB4_18 Depth=2
	movq	80(%rsp), %rdx          # 8-byte Reload
	movsbq	(%rdx,%rbx), %rdx
	movq	88(%rsp), %rsi          # 8-byte Reload
	shlq	$10, %rsi
	addq	96(%rsp), %rsi          # 8-byte Folded Reload
	addl	(%rsi,%rdx,4), %ecx
.LBB4_38:                               #   in Loop: Header=BB4_18 Depth=2
	testl	%ecx, %ecx
	movl	$0, %edx
	cmovlel	%edx, %ecx
	movl	44(%rsp), %edx          # 4-byte Reload
	cmovlel	%r15d, %edx
	movl	%edx, %edi
	movl	40(%rsp), %eax          # 4-byte Reload
	cmovlel	%ebx, %eax
	cmpl	%r9d, %ecx
	movl	%r9d, %edx
	movl	%ebp, %r8d
	movl	%r11d, %esi
	jl	.LBB4_43
# BB#39:                                #   in Loop: Header=BB4_18 Depth=2
	jne	.LBB4_40
# BB#41:                                #   in Loop: Header=BB4_18 Depth=2
	cmpl	%ebp, %edi
	movl	%r9d, %edx
	movl	%ebp, %r8d
	movl	%r11d, %esi
	jl	.LBB4_43
# BB#42:                                #   in Loop: Header=BB4_18 Depth=2
	cmpl	%r11d, %eax
	movl	%eax, %esi
	cmovll	%r11d, %esi
	cmpl	%ebp, %edi
	cmovnel	%eax, %esi
	movl	%r9d, %edx
	movl	%edi, %r8d
	jmp	.LBB4_43
	.p2align	4, 0x90
.LBB4_40:                               #   in Loop: Header=BB4_18 Depth=2
	movl	%ecx, %edx
	movl	%edi, %r8d
	movl	%eax, %esi
.LBB4_43:                               #   in Loop: Header=BB4_18 Depth=2
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %edx
	movl	%eax, %r12d
	movl	16(%rsp), %r15d         # 4-byte Reload
	movq	%r14, 32(%rsp)          # 8-byte Spill
                                        # kill: %R14D<def> %R14D<kill> %R14<kill>
	jl	.LBB4_48
# BB#44:                                #   in Loop: Header=BB4_18 Depth=2
	jne	.LBB4_45
# BB#46:                                #   in Loop: Header=BB4_18 Depth=2
	movl	16(%rsp), %r15d         # 4-byte Reload
	cmpl	%r15d, %r8d
	movl	%eax, %r12d
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r14d
	jl	.LBB4_48
# BB#47:                                #   in Loop: Header=BB4_18 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	%ecx, %esi
	movl	%esi, %r14d
	cmovll	%ecx, %r14d
	cmpl	16(%rsp), %r8d          # 4-byte Folded Reload
	cmovnel	%esi, %r14d
	movl	%eax, %r12d
	movl	%r8d, %r15d
	jmp	.LBB4_48
	.p2align	4, 0x90
.LBB4_45:                               #   in Loop: Header=BB4_18 Depth=2
	movl	%edx, %r12d
	movl	%r8d, %r15d
	movl	%esi, %r14d
.LBB4_48:                               #   in Loop: Header=BB4_18 Depth=2
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	%r12d, (%rax,%rbx,4)
	movl	(%r10,%rbx,4), %ecx
	movl	%ecx, 44(%rsp)          # 4-byte Spill
	movl	(%r13,%rbx,4), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	%r15d, (%r10,%rbx,4)
	movl	%r14d, (%r13,%rbx,4)
	movq	120(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax,%rbx,4)
	movq	112(%rsp), %rax         # 8-byte Reload
	movl	%ebp, (%rax,%rbx,4)
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%r11d, (%rax,%rbx,4)
	movl	min(%rip), %eax
	cmpl	%eax, %r12d
	movl	68(%rsp), %edi          # 4-byte Reload
	jle	.LBB4_50
# BB#49:                                #   in Loop: Header=BB4_18 Depth=2
	movl	%eax, (%rsp)
	movl	%r12d, %edi
	movl	%r15d, %esi
	movl	%r14d, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%ebx, %r8d
	movl	60(%rsp), %r9d          # 4-byte Reload
	callq	addnode
	movl	%eax, min(%rip)
	movl	nn(%rip), %edi
.LBB4_50:                               #   in Loop: Header=BB4_18 Depth=2
	movslq	%edi, %rax
	cmpq	%rax, %rbx
	leaq	1(%rbx), %rbx
	movl	64(%rsp), %ecx          # 4-byte Reload
	jl	.LBB4_18
	jmp	.LBB4_10
.LBB4_51:                               # %._crit_edge213
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	small_pass, .Lfunc_end4-small_pass
	.cfi_endproc

	.globl	addnode
	.p2align	4, 0x90
	.type	addnode,@function
addnode:                                # @addnode
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 16
.Lcfi76:
	.cfi_offset %rbx, -16
	movq	most(%rip), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#1:
	cmpl	%esi, 4(%rax)
	jne	.LBB5_3
# BB#2:
	cmpl	%edx, 8(%rax)
	je	.LBB5_10
.LBB5_3:                                # %.preheader
	movslq	numnode(%rip), %r10
	testq	%r10, %r10
	jle	.LBB5_8
# BB#4:                                 # %.lr.ph46
	movq	LIST(%rip), %r11
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%r11,%rbx,8), %rax
	movq	%rax, most(%rip)
	cmpl	%esi, 4(%rax)
	jne	.LBB5_7
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=1
	cmpl	%edx, 8(%rax)
	je	.LBB5_10
.LBB5_7:                                #   in Loop: Header=BB5_5 Depth=1
	incq	%rbx
	cmpq	%r10, %rbx
	jl	.LBB5_5
.LBB5_8:                                # %.critedge
	cmpl	%r9d, %r10d
	jne	.LBB5_20
# BB#9:
	movl	$low, %r10d
	jmp	.LBB5_21
.LBB5_10:                               # %.loopexit42
	cmpl	%edi, (%rax)
	jge	.LBB5_12
# BB#11:
	movl	%edi, (%rax)
	movl	%ecx, 12(%rax)
	movl	%r8d, 16(%rax)
.LBB5_12:
	cmpl	%ecx, 20(%rax)
	jle	.LBB5_14
# BB#13:
	movl	%ecx, 20(%rax)
.LBB5_14:
	cmpl	%ecx, 24(%rax)
	jge	.LBB5_16
# BB#15:
	movl	%ecx, 24(%rax)
.LBB5_16:
	cmpl	%r8d, 28(%rax)
	jle	.LBB5_18
# BB#17:
	movl	%r8d, 28(%rax)
.LBB5_18:
	cmpl	%r8d, 32(%rax)
	jge	.LBB5_23
# BB#19:
	leaq	32(%rax), %rcx
	jmp	.LBB5_22
.LBB5_20:
	leal	1(%r10), %eax
	shlq	$3, %r10
	addq	LIST(%rip), %r10
	movl	%eax, numnode(%rip)
.LBB5_21:
	movq	(%r10), %rax
	movq	%rax, most(%rip)
	movl	%edi, (%rax)
	movl	%esi, 4(%rax)
	movl	%edx, 8(%rax)
	movl	%ecx, 12(%rax)
	movl	%r8d, 16(%rax)
	movl	%ecx, 24(%rax)
	movl	%ecx, 20(%rax)
	movl	%r8d, 32(%rax)
	leaq	28(%rax), %rcx
.LBB5_22:                               # %.sink.split
	movl	%r8d, (%rcx)
.LBB5_23:
	cmpl	%r9d, numnode(%rip)
	jne	.LBB5_30
# BB#24:
	movq	low(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB5_26
# BB#25:
	cmpq	%rax, %rcx
	jne	.LBB5_41
.LBB5_26:
	movq	LIST(%rip), %rax
	movq	(%rax), %rsi
	movq	%rsi, low(%rip)
	cmpl	$2, %r9d
	jl	.LBB5_31
# BB#27:                                # %.lr.ph
	testb	$1, %r9b
	jne	.LBB5_32
# BB#28:
	movq	8(%rax), %rcx
	movl	(%rcx), %edx
	cmpl	(%rsi), %edx
	jge	.LBB5_33
# BB#29:
	movq	%rcx, low(%rip)
	movl	$2, %edx
	cmpl	$2, %r9d
	jne	.LBB5_35
	jmp	.LBB5_41
.LBB5_30:
	movl	16(%rsp), %eax
	popq	%rbx
	retq
.LBB5_31:
	movq	%rsi, %rcx
	jmp	.LBB5_41
.LBB5_32:
	movl	$1, %edx
	jmp	.LBB5_34
.LBB5_33:
	movl	$2, %edx
.LBB5_34:
	movq	%rsi, %rcx
	cmpl	$2, %r9d
	je	.LBB5_41
.LBB5_35:                               # %.lr.ph.new
	movslq	%r9d, %rsi
	.p2align	4, 0x90
.LBB5_36:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rdx,8), %rdi
	movl	(%rdi), %ebx
	cmpl	(%rcx), %ebx
	jge	.LBB5_38
# BB#37:                                #   in Loop: Header=BB5_36 Depth=1
	movq	%rdi, low(%rip)
	movq	%rdi, %rcx
.LBB5_38:                               #   in Loop: Header=BB5_36 Depth=1
	movq	8(%rax,%rdx,8), %rdi
	movl	(%rdi), %ebx
	cmpl	(%rcx), %ebx
	jge	.LBB5_40
# BB#39:                                #   in Loop: Header=BB5_36 Depth=1
	movq	%rdi, low(%rip)
	movq	%rdi, %rcx
.LBB5_40:                               #   in Loop: Header=BB5_36 Depth=1
	addq	$2, %rdx
	cmpq	%rsi, %rdx
	jl	.LBB5_36
.LBB5_41:                               # %.loopexit
	movl	(%rcx), %eax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	addnode, .Lfunc_end5-addnode
	.cfi_endproc

	.globl	findmax
	.p2align	4, 0x90
	.type	findmax,@function
findmax:                                # @findmax
	.cfi_startproc
# BB#0:
	movslq	numnode(%rip), %r8
	xorl	%eax, %eax
	cmpq	$2, %r8
	movq	LIST(%rip), %r9
	jl	.LBB6_6
# BB#1:                                 # %.lr.ph
	testb	$1, %r8b
	jne	.LBB6_2
# BB#3:
	movq	(%r9), %rsi
	movq	8(%r9), %rax
	movl	(%rax), %edi
	xorl	%eax, %eax
	cmpl	(%rsi), %edi
	setg	%al
	movl	$2, %esi
	cmpl	$2, %r8d
	jne	.LBB6_5
	jmp	.LBB6_6
.LBB6_2:
	xorl	%eax, %eax
	movl	$1, %esi
	cmpl	$2, %r8d
	je	.LBB6_6
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rsi,8), %rdi
	movl	(%rdi), %edi
	cltq
	movq	(%r9,%rax,8), %rdx
	cmpl	(%rdx), %edi
	cmovgl	%esi, %eax
	movq	8(%r9,%rsi,8), %rdx
	movl	(%rdx), %edx
	cltq
	movq	(%r9,%rax,8), %rdi
	leal	1(%rsi), %ecx
	cmpl	(%rdi), %edx
	cmovgl	%ecx, %eax
	addq	$2, %rsi
	cmpq	%r8, %rsi
	jl	.LBB6_5
.LBB6_6:                                # %._crit_edge
	movslq	%eax, %rsi
	movq	(%r9,%rsi,8), %rax
	leal	-1(%r8), %ecx
	movl	%ecx, numnode(%rip)
	cmpl	%ecx, %esi
	je	.LBB6_8
# BB#7:
	movq	-8(%r9,%r8,8), %rcx
	movq	%rcx, (%r9,%rsi,8)
	movq	LIST(%rip), %rcx
	movq	%rax, -8(%rcx,%r8,8)
	movq	LIST(%rip), %r9
.LBB6_8:
	movq	(%r9), %rcx
	movq	%rcx, most(%rip)
	cmpq	%rax, low(%rip)
	jne	.LBB6_10
# BB#9:
	movq	(%r9), %rcx
	movq	%rcx, low(%rip)
.LBB6_10:
	retq
.Lfunc_end6:
	.size	findmax, .Lfunc_end6-findmax
	.cfi_endproc

	.globl	no_cross
	.p2align	4, 0x90
	.type	no_cross,@function
no_cross:                               # @no_cross
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 32
.Lcfi80:
	.cfi_offset %rbx, -32
.Lcfi81:
	.cfi_offset %r14, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movslq	numnode(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB7_1
# BB#2:                                 # %.lr.ph
	movq	LIST(%rip), %rax
	movl	mm(%rip), %esi
	movl	nn(%rip), %r14d
	movl	m1(%rip), %r9d
	decl	%r9d
	movl	n1(%rip), %r10d
	decl	%r10d
	movl	rl(%rip), %r11d
	movl	cl(%rip), %r8d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB7_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rdx,8), %rdi
	movl	4(%rdi), %ebp
	cmpl	%esi, %ebp
	jg	.LBB7_14
# BB#4:                                 #   in Loop: Header=BB7_3 Depth=1
	movl	8(%rdi), %ebx
	cmpl	%r14d, %ebx
	jg	.LBB7_14
# BB#5:                                 #   in Loop: Header=BB7_3 Depth=1
	cmpl	%r9d, 24(%rdi)
	jl	.LBB7_14
# BB#6:                                 #   in Loop: Header=BB7_3 Depth=1
	cmpl	%r10d, 32(%rdi)
	jl	.LBB7_14
# BB#7:                                 #   in Loop: Header=BB7_3 Depth=1
	cmpl	%r11d, %ebp
	jl	.LBB7_9
# BB#8:                                 #   in Loop: Header=BB7_3 Depth=1
	cmpl	%r8d, %ebx
	jl	.LBB7_9
	.p2align	4, 0x90
.LBB7_14:                               #   in Loop: Header=BB7_3 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB7_3
	jmp	.LBB7_15
.LBB7_1:
	xorl	%edx, %edx
.LBB7_15:                               # %.loopexit
	xorl	%eax, %eax
	cmpl	%ecx, %edx
	sete	%al
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB7_9:
	cmpl	%r11d, %ebp
	jge	.LBB7_11
# BB#10:
	movl	%ebp, rl(%rip)
.LBB7_11:
	cmpl	%r8d, %ebx
	jge	.LBB7_13
# BB#12:
	movl	%ebx, cl(%rip)
.LBB7_13:
	movb	$1, flag(%rip)
	jmp	.LBB7_15
.Lfunc_end7:
	.size	no_cross, .Lfunc_end7-no_cross
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI8_1:
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.text
	.globl	diff
	.p2align	4, 0x90
	.type	diff,@function
diff:                                   # @diff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi87:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi89:
	.cfi_def_cfa_offset 208
.Lcfi90:
	.cfi_offset %rbx, -56
.Lcfi91:
	.cfi_offset %r12, -48
.Lcfi92:
	.cfi_offset %r13, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	jle	.LBB8_17
# BB#1:
	cmpl	$1, %edx
	jg	.LBB8_20
# BB#2:
	jne	.LBB8_44
# BB#3:                                 # %.lr.ph291.preheader
	movl	r(%rip), %r11d
	movl	q(%rip), %r10d
	movq	32(%rsp), %rax          # 8-byte Reload
	movsbq	1(%rax), %r9
	cmpl	%ebx, %r8d
	cmovgl	%ebx, %r8d
	addl	%r11d, %r8d
	movl	%r11d, %edx
	imull	%ecx, %edx
	addl	%r10d, %edx
	addl	%r8d, %edx
	negl	%edx
	movslq	%ecx, %rdi
	leal	1(%rcx), %r15d
	movq	row(%rip), %r12
	movslq	I(%rip), %r13
	xorl	%r8d, %r8d
	movl	$1, %ebp
	shlq	$10, %r9
	addq	v(%rip), %r9
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph291
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_6 Depth 2
	movq	8(%r12,%r13,8), %rax
	movq	%rax, z(%rip)
	testq	%rax, %rax
	je	.LBB8_8
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB8_4 Depth=1
	movl	J(%rip), %esi
	addl	%ebp, %esi
	.p2align	4, 0x90
.LBB8_6:                                #   Parent Loop BB8_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%esi, (%rax)
	je	.LBB8_11
# BB#7:                                 #   in Loop: Header=BB8_6 Depth=2
	movq	8(%rax), %rax
	movq	%rax, z(%rip)
	testq	%rax, %rax
	jne	.LBB8_6
.LBB8_8:                                # %._crit_edge
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movsbq	(%rax,%rbp), %rax
	movl	(%r9,%rax,4), %ebx
	xorl	%eax, %eax
	cmpq	$2, %rbp
	jl	.LBB8_10
# BB#9:                                 #   in Loop: Header=BB8_4 Depth=1
	leal	-1(%rbp), %eax
	imull	%r11d, %eax
	addl	%r10d, %eax
.LBB8_10:                               #   in Loop: Header=BB8_4 Depth=1
	movq	%rdi, %rsi
	subq	%rbp, %rsi
	imull	%r11d, %esi
	addl	%r10d, %esi
	cmpq	%rbp, %rdi
	cmovlel	%r8d, %esi
	subl	%eax, %ebx
	subl	%esi, %ebx
	cmpl	%edx, %ebx
	cmovgl	%ebp, %r14d
	cmovgel	%ebx, %edx
	movb	$1, %al
	jmp	.LBB8_12
	.p2align	4, 0x90
.LBB8_11:                               #   in Loop: Header=BB8_4 Depth=1
	xorl	%eax, %eax
.LBB8_12:                               # %.thread276
                                        #   in Loop: Header=BB8_4 Depth=1
	incq	%rbp
	cmpq	%r15, %rbp
	jne	.LBB8_4
# BB#13:
	movb	%al, tt(%rip)
	testl	%r14d, %r14d
	je	.LBB8_76
# BB#14:
	cmpl	$1, %r14d
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rcx, %r13
	jle	.LBB8_83
# BB#15:
	leal	-1(%r14), %eax
	movl	J(%rip), %ebx
	addl	%eax, %ebx
	movl	%ebx, J(%rip)
	movl	al_len(%rip), %r12d
	addl	%eax, %r12d
	movl	%r12d, al_len(%rip)
	movl	last(%rip), %edx
	movq	sapp(%rip), %rcx
	testl	%edx, %edx
	js	.LBB8_93
# BB#16:
	leaq	4(%rcx), %r15
	movq	%r15, sapp(%rip)
	movl	%eax, (%rcx)
	movl	%eax, last(%rip)
	jmp	.LBB8_94
.LBB8_17:
	testl	%edx, %edx
	jle	.LBB8_46
# BB#18:
	addl	%edx, I(%rip)
	addl	%edx, al_len(%rip)
	cmpl	$0, last(%rip)
	js	.LBB8_74
# BB#19:
	movl	%edx, %eax
	negl	%eax
	movq	sapp(%rip), %rcx
	leaq	4(%rcx), %rsi
	movq	%rsi, sapp(%rip)
	movl	%eax, (%rcx)
	jmp	.LBB8_75
.LBB8_20:                               # %.lr.ph355
	movl	%r8d, 60(%rsp)          # 4-byte Spill
	movl	%edx, %eax
	shrl	%eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	CC(%rip), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$0, (%rax)
	movl	q(%rip), %r13d
	movl	%r13d, %ebp
	negl	%ebp
	movl	r(%rip), %r10d
	movq	DD(%rip), %r15
	leal	1(%rcx), %edi
	leaq	-1(%rdi), %r11
	cmpq	$8, %r11
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	%ebx, 56(%rsp)          # 4-byte Spill
	jb	.LBB8_24
# BB#21:                                # %min.iters.checked
	movq	%r11, %r12
	andq	$-8, %r12
	je	.LBB8_24
# BB#22:                                # %vector.memcheck
	movq	40(%rsp), %r9           # 8-byte Reload
	leaq	4(%r9), %rax
	leaq	(%r15,%rdi,4), %rsi
	cmpq	%rsi, %rax
	jae	.LBB8_84
# BB#23:                                # %vector.memcheck
	leaq	(%r9,%rdi,4), %rax
	leaq	4(%r15), %rsi
	cmpq	%rax, %rsi
	jae	.LBB8_84
.LBB8_24:
	movl	$1, %r14d
	movl	%ebp, %r8d
.LBB8_25:                               # %scalar.ph.preheader
	movq	64(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	subl	%r14d, %eax
	subq	%r14, %r11
	andq	$3, %rax
	je	.LBB8_29
# BB#26:                                # %scalar.ph.prol.preheader
	leaq	(%r15,%r14,4), %r9
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r14,4), %rdi
	movl	%r10d, %edx
	negl	%edx
	movl	%edx, %ebx
	subl	%r13d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_27:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	%r8d, %esi
	subl	%r10d, %r8d
	leal	(%rdx,%rsi), %ecx
	movl	%ecx, (%rdi,%rbp,4)
	addl	%ebx, %esi
	movl	%esi, (%r9,%rbp,4)
	incq	%rbp
	cmpq	%rbp, %rax
	jne	.LBB8_27
# BB#28:                                # %scalar.ph.prol.loopexit.unr-lcssa
	addq	%rbp, %r14
.LBB8_29:                               # %scalar.ph.prol.loopexit
	cmpq	$3, %r11
	jb	.LBB8_32
# BB#30:                                # %scalar.ph.preheader.new
	movq	64(%rsp), %rcx          # 8-byte Reload
	subq	%r14, %rcx
	leaq	12(%r15,%r14,4), %rax
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	12(%rdx,%r14,4), %rdi
	leal	(,%r10,4), %edx
	negl	%edx
	movl	%r13d, %r12d
	negl	%r12d
	movl	%edx, %r11d
	subl	%r13d, %r11d
	imull	$-3, %r10d, %r14d
	movl	%r14d, %r15d
	subl	%r13d, %r15d
	leal	(%r10,%r10), %r9d
	negl	%r9d
	movl	%r13d, %esi
	movl	%r9d, %r13d
	subl	%esi, %r13d
	movl	%r10d, %ebp
	negl	%ebp
	subl	%r10d, %r12d
	.p2align	4, 0x90
.LBB8_31:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r8d, %ebx
	subl	%r10d, %ebx
	leal	(%rbp,%r8), %esi
	movl	%esi, -12(%rdi)
	leal	(%r12,%r8), %esi
	movl	%esi, -12(%rax)
	subl	%r10d, %ebx
	leal	(%r9,%r8), %esi
	movl	%esi, -8(%rdi)
	leal	(%r13,%r8), %esi
	movl	%esi, -8(%rax)
	subl	%r10d, %ebx
	leal	(%r14,%r8), %esi
	movl	%esi, -4(%rdi)
	leal	(%r15,%r8), %esi
	movl	%esi, -4(%rax)
	subl	%r10d, %ebx
	leal	(%rdx,%r8), %esi
	movl	%esi, (%rdi)
	addl	%r11d, %r8d
	movl	%r8d, (%rax)
	addq	$16, %rax
	addq	$16, %rdi
	addq	$-4, %rcx
	movl	%ebx, %r8d
	jne	.LBB8_31
.LBB8_32:                               # %._crit_edge356
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	(%rbx), %ecx
	movq	80(%rsp), %r12          # 8-byte Reload
	testl	%r12d, %r12d
	je	.LBB8_47
# BB#33:                                # %.lr.ph349.split.preheader
	movl	60(%rsp), %r15d         # 4-byte Reload
	negl	%r15d
	movq	v(%rip), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	DD(%rip), %rdi
	leal	1(%r12), %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	r(%rip), %ebp
	movl	q(%rip), %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movl	qr(%rip), %r9d
	movq	row(%rip), %r13
	movslq	I(%rip), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %esi
	.p2align	4, 0x90
.LBB8_34:                               # %.lr.ph349.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_35 Depth 2
                                        #       Child Loop BB8_37 Depth 3
	subl	%ebp, %r15d
	movl	%r15d, (%rbx)
	movl	%r15d, %eax
	subl	104(%rsp), %eax         # 4-byte Folded Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movsbq	(%rdx,%rsi), %r8
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movslq	%esi, %r10
	addq	8(%rsp), %r10           # 8-byte Folded Reload
	movl	$1, %r14d
	movl	%r15d, 72(%rsp)         # 4-byte Spill
	movl	%r15d, %r12d
	.p2align	4, 0x90
.LBB8_35:                               #   Parent Loop BB8_34 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_37 Depth 3
	subl	%r9d, %r12d
	subl	%ebp, %eax
	cmpl	%eax, %r12d
	cmovgel	%r12d, %eax
	movl	(%rbx,%r14,4), %edx
	movl	%edx, %r12d
	subl	%r9d, %r12d
	movq	%rdi, %r15
	movl	(%rdi,%r14,4), %edi
	subl	%ebp, %edi
	cmpl	%edi, %r12d
	cmovgel	%r12d, %edi
	movq	(%r13,%r10,8), %rsi
	movq	%rsi, z(%rip)
	testq	%rsi, %rsi
	je	.LBB8_39
# BB#36:                                # %.lr.ph335
                                        #   in Loop: Header=BB8_35 Depth=2
	movl	J(%rip), %ebx
	addl	%r14d, %ebx
	.p2align	4, 0x90
.LBB8_37:                               #   Parent Loop BB8_34 Depth=1
                                        #     Parent Loop BB8_35 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ebx, (%rsi)
	je	.LBB8_40
# BB#38:                                #   in Loop: Header=BB8_37 Depth=3
	movq	8(%rsi), %rsi
	movq	%rsi, z(%rip)
	testq	%rsi, %rsi
	jne	.LBB8_37
.LBB8_39:                               # %._crit_edge336
                                        #   in Loop: Header=BB8_35 Depth=2
	movq	48(%rsp), %rsi          # 8-byte Reload
	movsbq	(%rsi,%r14), %rsi
	movq	%r8, %rbx
	shlq	$10, %rbx
	addq	(%rsp), %rbx            # 8-byte Folded Reload
	addl	(%rbx,%rsi,4), %ecx
	movb	$1, %r11b
	movl	%ecx, %r12d
	jmp	.LBB8_41
	.p2align	4, 0x90
.LBB8_40:                               #   in Loop: Header=BB8_35 Depth=2
	xorl	%r11d, %r11d
.LBB8_41:                               # %.thread277
                                        #   in Loop: Header=BB8_35 Depth=2
	cmpl	%edi, %r12d
	cmovll	%edi, %r12d
	cmpl	%eax, %r12d
	cmovll	%eax, %r12d
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	%r12d, (%rbx,%r14,4)
	movl	%edi, (%r15,%r14,4)
	movq	%r15, %rdi
	incq	%r14
	cmpq	64(%rsp), %r14          # 8-byte Folded Reload
	movl	%edx, %ecx
	jne	.LBB8_35
# BB#42:                                # %._crit_edge343
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	incq	%rsi
	movl	(%rbx), %ecx
	cmpq	88(%rsp), %rsi          # 8-byte Folded Reload
	movl	72(%rsp), %r15d         # 4-byte Reload
	jne	.LBB8_34
# BB#43:                                # %._crit_edge350.loopexit
	movb	%r11b, tt(%rip)
	movq	96(%rsp), %r15          # 8-byte Reload
	movq	80(%rsp), %r12          # 8-byte Reload
	jmp	.LBB8_48
.LBB8_44:
	addl	%ecx, J(%rip)
	addl	%ecx, al_len(%rip)
	movl	last(%rip), %esi
	movq	sapp(%rip), %rax
	testl	%esi, %esi
	js	.LBB8_79
# BB#45:
	leaq	4(%rax), %rdx
	movq	%rdx, sapp(%rip)
	movl	%ecx, (%rax)
	movl	%ecx, last(%rip)
	jmp	.LBB8_80
.LBB8_46:
	xorl	%edx, %edx
	negl	%edx
	jmp	.LBB8_97
.LBB8_47:                               # %._crit_edge356.._crit_edge350_crit_edge
	movq	DD(%rip), %rdi
	movq	96(%rsp), %r15          # 8-byte Reload
.LBB8_48:                               # %._crit_edge350
	movq	64(%rsp), %r9           # 8-byte Reload
	movl	%ecx, (%rdi)
	movq	RR(%rip), %rcx
	movslq	%r15d, %r14
	movl	$0, (%rcx,%r14,4)
	testl	%r15d, %r15d
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%rcx, (%rsp)            # 8-byte Spill
	jle	.LBB8_59
# BB#49:                                # %.lr.ph332
	movl	q(%rip), %eax
	movq	%r14, %rdx
	movl	%eax, %r14d
	negl	%r14d
	movl	r(%rip), %r11d
	movq	SS(%rip), %r10
	cmpl	$8, %r15d
	movq	%rdx, %rbp
	movl	%r14d, %esi
	jb	.LBB8_56
# BB#50:                                # %min.iters.checked469
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, %r8
	andq	$-8, %r8
	movq	%rcx, %rbp
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%r14d, %esi
	je	.LBB8_56
# BB#51:                                # %vector.memcheck482
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%r10,%rcx,4), %rcx
	cmpq	%rcx, (%rsp)            # 8-byte Folded Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	jae	.LBB8_53
# BB#52:                                # %vector.memcheck482
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rcx,%rdx,4), %rcx
	cmpq	%rcx, %r10
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rdx, %rbp
	movl	%r14d, %esi
	jb	.LBB8_56
.LBB8_53:                               # %vector.ph483
	movq	%rdi, %r13
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rdi, %rbp
	subq	%r8, %rbp
	movl	%r11d, %esi
	imull	%r8d, %esi
	negl	%esi
	subl	%eax, %esi
	movq	%rcx, %rbx
	movl	%r11d, %ecx
	negl	%ecx
	movd	%r11d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movd	%eax, %xmm1
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movd	%ecx, %xmm2
	pshufd	$0, %xmm2, %xmm3        # xmm3 = xmm2[0,0,0,0]
	movdqa	.LCPI8_0(%rip), %xmm2   # xmm2 = [0,1,2,3]
	pshufd	$245, %xmm2, %xmm4      # xmm4 = xmm2[1,1,3,3]
	pmuludq	%xmm3, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	movdqa	.LCPI8_1(%rip), %xmm4   # xmm4 = [4,5,6,7]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	leal	(,%r11,8), %eax
	leaq	-16(%r10,%rdi,4), %rdx
	leaq	-16(%rbx,%rdi,4), %rdi
	movq	%r8, %rcx
	movl	%r14d, %ebx
	.p2align	4, 0x90
.LBB8_54:                               # %vector.body465
                                        # =>This Inner Loop Header: Depth=1
	movd	%ebx, %xmm4
	pshufd	$0, %xmm4, %xmm4        # xmm4 = xmm4[0,0,0,0]
	movdqa	%xmm4, %xmm5
	paddd	%xmm2, %xmm5
	paddd	%xmm3, %xmm4
	psubd	%xmm0, %xmm5
	psubd	%xmm0, %xmm4
	pshufd	$27, %xmm5, %xmm6       # xmm6 = xmm5[3,2,1,0]
	movdqu	%xmm6, (%rdi)
	pshufd	$27, %xmm4, %xmm6       # xmm6 = xmm4[3,2,1,0]
	movdqu	%xmm6, -16(%rdi)
	psubd	%xmm1, %xmm5
	psubd	%xmm1, %xmm4
	pshufd	$27, %xmm5, %xmm5       # xmm5 = xmm5[3,2,1,0]
	movdqu	%xmm5, (%rdx)
	pshufd	$27, %xmm4, %xmm4       # xmm4 = xmm4[3,2,1,0]
	movdqu	%xmm4, -16(%rdx)
	subl	%eax, %ebx
	addq	$-32, %rdx
	addq	$-32, %rdi
	addq	$-8, %rcx
	jne	.LBB8_54
# BB#55:                                # %middle.block466
	cmpq	%r8, 8(%rsp)            # 8-byte Folded Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%r13, %rdi
	movq	(%rsp), %rcx            # 8-byte Reload
	je	.LBB8_58
.LBB8_56:                               # %scalar.ph467.preheader
	subl	%r11d, %esi
	.p2align	4, 0x90
.LBB8_57:                               # %scalar.ph467
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, -4(%rcx,%rbp,4)
	leal	(%r14,%rsi), %eax
	movl	%eax, -4(%r10,%rbp,4)
	decq	%rbp
	subl	%r11d, %esi
	testq	%rbp, %rbp
	jg	.LBB8_57
.LBB8_58:                               # %._crit_edge333.loopexit
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	(%rcx,%r14,4), %r13d
	jmp	.LBB8_60
.LBB8_59:
	xorl	%r13d, %r13d
.LBB8_60:                               # %._crit_edge333
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %esi
	movl	%esi, 20(%rsp)          # 4-byte Spill
	cmpl	%eax, %r12d
	jge	.LBB8_73
# BB#61:                                # %.lr.ph326
	movl	56(%rsp), %ebp          # 4-byte Reload
	negl	%ebp
	testl	%r15d, %r15d
	movq	SS(%rip), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	jle	.LBB8_81
# BB#62:                                # %.lr.ph326.split.us.preheader
	movl	%ebp, 88(%rsp)          # 4-byte Spill
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	v(%rip), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movslq	24(%rsp), %rdi          # 4-byte Folded Reload
	movl	%r12d, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	r(%rip), %ebx
	movl	q(%rip), %eax
	movl	%eax, 124(%rsp)         # 4-byte Spill
	movl	qr(%rip), %r10d
	movq	row(%rip), %r8
	movslq	I(%rip), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_63:                               # %.lr.ph326.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_64 Depth 2
                                        #       Child Loop BB8_66 Depth 3
	movl	88(%rsp), %r15d         # 4-byte Reload
	subl	%ebx, %r15d
	movl	%r15d, (%rcx,%r14,4)
	movl	%r15d, %ebp
	subl	124(%rsp), %ebp         # 4-byte Folded Reload
	movl	%edi, %ecx
	decq	%rdi
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	movslq	%ecx, %r9
	movq	32(%rsp), %rcx          # 8-byte Reload
	movsbq	(%rcx,%r9), %r11
	movq	(%rsp), %rcx            # 8-byte Reload
	addq	136(%rsp), %r9          # 8-byte Folded Reload
	movl	%r15d, 88(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB8_64:                               #   Parent Loop BB8_63 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_66 Depth 3
	leaq	-1(%r14), %rdi
	subl	%r10d, %r15d
	subl	%ebx, %ebp
	cmpl	%ebp, %r15d
	cmovgel	%r15d, %ebp
	movl	-4(%rcx,%r14,4), %edx
	movl	%edx, %r15d
	subl	%r10d, %r15d
	movl	-4(%rax,%r14,4), %r12d
	subl	%ebx, %r12d
	cmpl	%r12d, %r15d
	cmovgel	%r15d, %r12d
	movq	(%r8,%r9,8), %rsi
	movq	%rsi, z(%rip)
	testq	%rsi, %rsi
	je	.LBB8_68
# BB#65:                                # %.lr.ph312.us
                                        #   in Loop: Header=BB8_64 Depth=2
	movl	J(%rip), %ecx
	addl	%r14d, %ecx
	.p2align	4, 0x90
.LBB8_66:                               #   Parent Loop BB8_63 Depth=1
                                        #     Parent Loop BB8_64 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ecx, (%rsi)
	je	.LBB8_69
# BB#67:                                #   in Loop: Header=BB8_66 Depth=3
	movq	8(%rsi), %rsi
	movq	%rsi, z(%rip)
	testq	%rsi, %rsi
	jne	.LBB8_66
.LBB8_68:                               # %._crit_edge313.us
                                        #   in Loop: Header=BB8_64 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	movsbq	(%rcx,%r14), %rcx
	movq	%r11, %rsi
	shlq	$10, %rsi
	addq	112(%rsp), %rsi         # 8-byte Folded Reload
	addl	(%rsi,%rcx,4), %r13d
	movb	$1, %sil
	movl	%r13d, %r15d
	jmp	.LBB8_70
	.p2align	4, 0x90
.LBB8_69:                               #   in Loop: Header=BB8_64 Depth=2
	xorl	%esi, %esi
.LBB8_70:                               # %.thread278.us
                                        #   in Loop: Header=BB8_64 Depth=2
	movq	(%rsp), %rcx            # 8-byte Reload
	cmpl	%r12d, %r15d
	cmovll	%r12d, %r15d
	cmpl	%ebp, %r15d
	cmovll	%ebp, %r15d
	movl	%r15d, -4(%rcx,%r14,4)
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	%r12d, -4(%rax,%r14,4)
	testq	%rdi, %rdi
	movq	%rdi, %r14
	movl	%edx, %r13d
	jg	.LBB8_64
# BB#71:                                # %..loopexit_crit_edge.us
                                        #   in Loop: Header=BB8_63 Depth=1
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	(%rcx,%r14,4), %r13d
	movq	104(%rsp), %rdi         # 8-byte Reload
	cmpq	144(%rsp), %rdi         # 8-byte Folded Reload
	jg	.LBB8_63
# BB#72:                                # %._crit_edge327.loopexit
	movb	%sil, tt(%rip)
	movq	96(%rsp), %r15          # 8-byte Reload
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	64(%rsp), %r9           # 8-byte Reload
	movq	128(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB8_110
.LBB8_73:                               # %._crit_edge333.._crit_edge327_crit_edge
	movq	SS(%rip), %rax
	jmp	.LBB8_110
.LBB8_74:
	movq	sapp(%rip), %rcx
	movl	-4(%rcx), %eax
	subl	%edx, %eax
	movl	%eax, -4(%rcx)
.LBB8_75:
	movl	%eax, last(%rip)
	imull	r(%rip), %edx
	addl	q(%rip), %edx
	negl	%edx
	jmp	.LBB8_97
.LBB8_76:
	addl	%ecx, J(%rip)
	movl	al_len(%rip), %edi
	addl	%ecx, %edi
	movl	%edi, al_len(%rip)
	movl	last(%rip), %ebp
	movq	sapp(%rip), %rax
	testl	%ebp, %ebp
	js	.LBB8_91
# BB#77:
	leaq	4(%rax), %rsi
	movq	%rsi, sapp(%rip)
	movl	%ecx, (%rax)
	incl	I(%rip)
	movl	%ecx, last(%rip)
	incl	%edi
	movl	%edi, al_len(%rip)
	testl	%ecx, %ecx
	js	.LBB8_92
# BB#78:
	leaq	8(%rax), %rcx
	movq	%rcx, sapp(%rip)
	movl	$-1, 4(%rax)
	movl	$-1, %eax
	movl	%eax, last(%rip)
	jmp	.LBB8_97
.LBB8_79:
	movl	%ecx, -4(%rax)
	leaq	4(%rax), %rdx
	movq	%rdx, sapp(%rip)
	movl	%esi, (%rax)
.LBB8_80:
	imull	r(%rip), %ecx
	addl	q(%rip), %ecx
	negl	%ecx
	movl	%ecx, %edx
	jmp	.LBB8_97
.LBB8_81:                               # %.lr.ph326.split.preheader
	movl	r(%rip), %r10d
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r11d
	subl	%r12d, %r11d
	cmpl	$7, %r11d
	ja	.LBB8_87
# BB#82:
	movl	20(%rsp), %eax          # 4-byte Reload
	jmp	.LBB8_107
.LBB8_83:                               # %._crit_edge412
	movq	sapp(%rip), %r15
	movl	al_len(%rip), %r12d
	movl	J(%rip), %ebx
	jmp	.LBB8_94
.LBB8_84:                               # %vector.ph
	movq	%r12, %r14
	orq	$1, %r14
	movl	%r10d, %r8d
	imull	%r12d, %r8d
	negl	%r8d
	subl	%r13d, %r8d
	movl	%r10d, %eax
	negl	%eax
	movd	%r10d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movd	%r13d, %xmm1
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movd	%eax, %xmm2
	pshufd	$0, %xmm2, %xmm3        # xmm3 = xmm2[0,0,0,0]
	movdqa	.LCPI8_0(%rip), %xmm2   # xmm2 = [0,1,2,3]
	pshufd	$245, %xmm2, %xmm4      # xmm4 = xmm2[1,1,3,3]
	pmuludq	%xmm3, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	movdqa	.LCPI8_1(%rip), %xmm4   # xmm4 = [4,5,6,7]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	leaq	20(%r9), %rbx
	leaq	20(%r15), %rax
	leal	(,%r10,8), %ecx
	movq	%r12, %rdi
	.p2align	4, 0x90
.LBB8_85:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	%ebp, %xmm4
	pshufd	$0, %xmm4, %xmm4        # xmm4 = xmm4[0,0,0,0]
	movdqa	%xmm4, %xmm5
	paddd	%xmm2, %xmm5
	paddd	%xmm3, %xmm4
	psubd	%xmm0, %xmm5
	psubd	%xmm0, %xmm4
	movdqu	%xmm5, -16(%rbx)
	movdqu	%xmm4, (%rbx)
	psubd	%xmm1, %xmm5
	psubd	%xmm1, %xmm4
	movdqu	%xmm5, -16(%rax)
	movdqu	%xmm4, (%rax)
	addq	$32, %rbx
	addq	$32, %rax
	subl	%ecx, %ebp
	addq	$-8, %rdi
	jne	.LBB8_85
# BB#86:                                # %middle.block
	cmpq	%r12, %r11
	jne	.LBB8_25
	jmp	.LBB8_32
.LBB8_87:                               # %min.iters.checked517
	movl	%r11d, %ecx
	andl	$-8, %ecx
	movl	%r11d, %r13d
	andl	$-8, %r13d
	je	.LBB8_98
# BB#88:                                # %vector.ph521
	movq	%rdi, %rsi
	movd	%ebp, %xmm0
	movd	%r10d, %xmm1
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	leal	-8(%r13), %eax
	movl	%eax, %edi
	shrl	$3, %edi
	incl	%edi
	andl	$7, %edi
	je	.LBB8_99
# BB#89:                                # %vector.body513.prol.preheader
	negl	%edi
	pxor	%xmm2, %xmm2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_90:                               # %vector.body513.prol
                                        # =>This Inner Loop Header: Depth=1
	psubd	%xmm1, %xmm0
	psubd	%xmm1, %xmm2
	addl	$8, %ebp
	incl	%edi
	jne	.LBB8_90
	jmp	.LBB8_100
.LBB8_91:                               # %.thread423
	movl	%ecx, -4(%rax)
	leaq	4(%rax), %rsi
	movq	%rsi, sapp(%rip)
	movl	%ebp, (%rax)
	incl	I(%rip)
	incl	%edi
	movl	%edi, al_len(%rip)
.LBB8_92:
	movl	(%rax), %ecx
	decl	%ecx
	movl	%ecx, (%rax)
	movl	%ecx, last(%rip)
	jmp	.LBB8_97
.LBB8_93:
	movl	%eax, -4(%rcx)
	leaq	4(%rcx), %r15
	movq	%r15, sapp(%rip)
	movl	%edx, (%rcx)
.LBB8_94:
	leaq	4(%r15), %rax
	movq	%rax, sapp(%rip)
	movl	$0, (%r15)
	movl	$0, last(%rip)
	incl	%r12d
	movl	%r12d, al_len(%rip)
	movq	32(%rsp), %rax          # 8-byte Reload
	movb	1(%rax), %al
	movslq	%r14d, %rcx
	movq	48(%rsp), %rdx          # 8-byte Reload
	cmpb	(%rdx,%rcx), %al
	movl	$no_mat, %eax
	movl	$no_mis, %ecx
	cmoveq	%rax, %rcx
	incl	(%rcx)
	movslq	I(%rip), %rbp
	leaq	1(%rbp), %rax
	movl	%eax, I(%rip)
	incl	%ebx
	movl	%ebx, J(%rip)
	movl	$16, %edi
	callq	malloc
	movq	%rax, z(%rip)
	movl	%ebx, (%rax)
	movq	row(%rip), %rcx
	movq	8(%rcx,%rbp,8), %rdx
	movq	%rdx, 8(%rax)
	movq	%rax, 8(%rcx,%rbp,8)
	cmpl	%r13d, %r14d
	jge	.LBB8_96
# BB#95:
	subl	%r14d, %r13d
	addl	%r13d, %ebx
	movl	%ebx, J(%rip)
	addl	%r13d, %r12d
	movl	%r12d, al_len(%rip)
	leaq	8(%r15), %rax
	movq	%rax, sapp(%rip)
	movl	%r13d, 4(%r15)
	movl	%r13d, last(%rip)
.LBB8_96:
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB8_97:                               # %.thread279
	movl	%edx, %eax
	jmp	.LBB8_127
.LBB8_98:
	movl	20(%rsp), %eax          # 4-byte Reload
	jmp	.LBB8_106
.LBB8_99:
	xorl	%ebp, %ebp
	pxor	%xmm2, %xmm2
.LBB8_100:                              # %vector.body513.prol.loopexit
	cmpl	$56, %eax
	jb	.LBB8_103
# BB#101:                               # %vector.ph521.new
	movl	%r13d, %eax
	subl	%ebp, %eax
	.p2align	4, 0x90
.LBB8_102:                              # %vector.body513
                                        # =>This Inner Loop Header: Depth=1
	psubd	%xmm1, %xmm0
	psubd	%xmm1, %xmm2
	psubd	%xmm1, %xmm0
	psubd	%xmm1, %xmm2
	psubd	%xmm1, %xmm0
	psubd	%xmm1, %xmm2
	psubd	%xmm1, %xmm0
	psubd	%xmm1, %xmm2
	psubd	%xmm1, %xmm0
	psubd	%xmm1, %xmm2
	psubd	%xmm1, %xmm0
	psubd	%xmm1, %xmm2
	psubd	%xmm1, %xmm0
	psubd	%xmm1, %xmm2
	psubd	%xmm1, %xmm0
	psubd	%xmm1, %xmm2
	addl	$-64, %eax
	jne	.LBB8_102
.LBB8_103:                              # %middle.block514
	paddd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ebp
	cmpl	%r13d, %r11d
	movq	%rsi, %rdi
	jne	.LBB8_105
# BB#104:
	movq	(%rsp), %rcx            # 8-byte Reload
	jmp	.LBB8_109
.LBB8_105:
	movl	20(%rsp), %eax          # 4-byte Reload
	subl	%ecx, %eax
.LBB8_106:                              # %.lr.ph326.split.preheader538
	movq	(%rsp), %rcx            # 8-byte Reload
.LBB8_107:                              # %.lr.ph326.split.preheader538
	incl	%eax
	.p2align	4, 0x90
.LBB8_108:                              # %.lr.ph326.split
                                        # =>This Inner Loop Header: Depth=1
	subl	%r10d, %ebp
	decl	%eax
	cmpl	%r12d, %eax
	jg	.LBB8_108
.LBB8_109:                              # %._crit_edge327.loopexit426
	movl	%ebp, (%rcx,%r14,4)
	movl	%ebp, %r13d
	movq	72(%rsp), %rax          # 8-byte Reload
.LBB8_110:                              # %._crit_edge327
	movq	%rax, %r10
	movl	%r13d, (%rax,%r14,4)
	movl	(%rbx), %edx
	movl	(%rcx), %ecx
	leal	(%rcx,%rdx), %r14d
	testl	%r15d, %r15d
	js	.LBB8_125
# BB#111:                               # %.lr.ph308
	decq	%r9
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	jmp	.LBB8_113
	.p2align	4, 0x90
.LBB8_112:                              # %._crit_edge402
                                        #   in Loop: Header=BB8_113 Depth=1
	movl	4(%rbx,%rax,4), %edx
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	4(%rcx,%rax,4), %ecx
	incq	%rax
.LBB8_113:                              # =>This Inner Loop Header: Depth=1
	leal	(%rcx,%rdx), %esi
	cmpl	%r14d, %esi
	jge	.LBB8_114
.LBB8_118:                              #   in Loop: Header=BB8_113 Depth=1
	cmpq	%rax, %r9
	jne	.LBB8_112
	jmp	.LBB8_119
	.p2align	4, 0x90
.LBB8_114:                              #   in Loop: Header=BB8_113 Depth=1
	jle	.LBB8_116
.LBB8_115:                              #   in Loop: Header=BB8_113 Depth=1
	movl	%eax, %ebp
	movl	%esi, %r14d
	cmpq	%rax, %r9
	jne	.LBB8_112
	jmp	.LBB8_119
	.p2align	4, 0x90
.LBB8_116:                              #   in Loop: Header=BB8_113 Depth=1
	cmpl	(%rdi,%rax,4), %edx
	je	.LBB8_118
# BB#117:                               #   in Loop: Header=BB8_113 Depth=1
	cmpl	(%r10,%rax,4), %ecx
	je	.LBB8_115
	jmp	.LBB8_118
.LBB8_119:                              # %.preheader
	testl	%r15d, %r15d
	js	.LBB8_126
# BB#120:                               # %.lr.ph299
	movl	q(%rip), %ecx
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	(%rdi,%rsi,4), %r13d
	addl	%ecx, %r13d
	xorl	%eax, %eax
	cmpl	%r14d, %r13d
	setg	%al
	cmovgl	%r15d, %ebp
	cmovll	%r14d, %r13d
	incl	%eax
	testl	%r15d, %r15d
	je	.LBB8_123
# BB#121:                               # %._crit_edge406.preheader
	incq	%rsi
	leal	-1(%r15), %edx
	movl	$2, %r8d
	movq	%rsi, %rbx
	.p2align	4, 0x90
.LBB8_122:                              # %._crit_edge406
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%r10,%rbx,4), %esi
	addl	-8(%rdi,%rbx,4), %esi
	addl	%ecx, %esi
	cmpl	%r13d, %esi
	cmovgl	%edx, %ebp
	cmovgl	%r8d, %eax
	cmovgel	%esi, %r13d
	decq	%rbx
	decl	%edx
	cmpq	$1, %rbx
	jg	.LBB8_122
.LBB8_123:                              # %._crit_edge300
	cmpl	$1, %eax
	jne	.LBB8_128
# BB#124:
	movl	%r13d, %r14d
	jmp	.LBB8_126
.LBB8_125:
	xorl	%ebp, %ebp
.LBB8_126:                              # %._crit_edge300.thread
	movl	q(%rip), %r9d
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	80(%rsp), %r13          # 8-byte Reload
	movl	%r13d, %edx
	movl	%ebp, %ecx
	movl	60(%rsp), %r8d          # 4-byte Reload
	callq	diff
	movl	%r13d, %edi
	addq	%r12, %rdi
	movslq	%ebp, %rax
	addq	%rax, %rbx
	movq	24(%rsp), %rdx          # 8-byte Reload
	subl	%r13d, %edx
	subl	%eax, %r15d
	movl	q(%rip), %r8d
	movq	%rbx, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r15d, %ecx
	movl	56(%rsp), %r9d          # 4-byte Reload
	callq	diff
	movl	%r14d, %eax
.LBB8_127:                              # %.thread279
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_128:
	leal	-1(%r12), %edx
	xorl	%r9d, %r9d
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	%ebp, %ecx
	movl	60(%rsp), %r8d          # 4-byte Reload
	callq	diff
	addl	$2, I(%rip)
	addl	$2, al_len(%rip)
	movq	sapp(%rip), %rax
	cmpl	$0, last(%rip)
	js	.LBB8_130
# BB#129:
	leaq	4(%rax), %rcx
	movq	%rcx, sapp(%rip)
	movl	$-2, (%rax)
	movl	$-2, %ecx
	jmp	.LBB8_131
.LBB8_130:
	movl	-4(%rax), %ecx
	addl	$-2, %ecx
	movl	%ecx, -4(%rax)
.LBB8_131:
	movl	%ecx, last(%rip)
	movl	%r12d, %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx,%rax), %rdi
	movslq	%ebp, %rax
	movq	48(%rsp), %rsi          # 8-byte Reload
	addq	%rax, %rsi
	movl	20(%rsp), %edx          # 4-byte Reload
	subl	%r12d, %edx
	subl	%ebp, %r15d
	xorl	%r8d, %r8d
	movl	%r15d, %ecx
	movl	56(%rsp), %r9d          # 4-byte Reload
	callq	diff
	movl	%r13d, %eax
	jmp	.LBB8_127
.Lfunc_end8:
	.size	diff, .Lfunc_end8-diff
	.cfi_endproc

	.globl	display
	.p2align	4, 0x90
	.type	display,@function
display:                                # @display
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 112
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movl	112(%rsp), %ebp
	movl	$ALINE+50, %r10d
	movl	$0, %r13d
	movl	$0, %r14d
	xorl	%r12d, %r12d
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movq	%r9, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%r9d, %r11d
	movl	%ebp, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	jmp	.LBB9_1
	.p2align	4, 0x90
.LBB9_29:                               #   in Loop: Header=BB9_1 Depth=1
	incl	8(%rsp)                 # 4-byte Folded Spill
	movl	$.L.str.31, %edi
	movl	$ALINE, %edx
	movl	$CLINE, %ecx
	movl	$BLINE, %r9d
	xorl	%eax, %eax
	movl	20(%rsp), %esi          # 4-byte Reload
	movl	4(%rsp), %r8d           # 4-byte Reload
	callq	printf
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%r13,%rax), %r11d
	movl	112(%rsp), %eax
	leal	(%r14,%rax), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	48(%rsp), %r8           # 8-byte Reload
	movl	16(%rsp), %ecx          # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	$ALINE+50, %r10d
.LBB9_1:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_2 Depth 2
                                        #     Child Loop BB9_26 Depth 2
	xorl	%eax, %eax
	jmp	.LBB9_2
.LBB9_6:                                #   in Loop: Header=BB9_2 Depth=2
	movslq	%r13d, %r13
	movzbl	1(%rdi,%r13), %ebx
	incq	%r13
	movb	%bl, ALINE(%r15)
	movslq	%r14d, %r14
	movzbl	1(%rsi,%r14), %r9d
	incq	%r14
	movb	%r9b, BLINE(%r15)
	cmpb	%r9b, %bl
	movb	$124, %al
	je	.LBB9_8
# BB#7:                                 #   in Loop: Header=BB9_2 Depth=2
	movb	$32, %al
.LBB9_8:                                #   in Loop: Header=BB9_2 Depth=2
	cmpb	$65, %bl
	jne	.LBB9_10
# BB#9:                                 #   in Loop: Header=BB9_2 Depth=2
	cmpb	$71, %r9b
	je	.LBB9_16
.LBB9_10:                               #   in Loop: Header=BB9_2 Depth=2
	cmpb	$67, %bl
	jne	.LBB9_12
# BB#11:                                #   in Loop: Header=BB9_2 Depth=2
	cmpb	$84, %r9b
	je	.LBB9_16
.LBB9_12:                               #   in Loop: Header=BB9_2 Depth=2
	cmpb	$71, %bl
	jne	.LBB9_14
# BB#13:                                #   in Loop: Header=BB9_2 Depth=2
	cmpb	$65, %r9b
	je	.LBB9_16
.LBB9_14:                               #   in Loop: Header=BB9_2 Depth=2
	xorl	%r12d, %r12d
	cmpb	$84, %bl
	jne	.LBB9_21
# BB#15:                                #   in Loop: Header=BB9_2 Depth=2
	cmpb	$67, %r9b
	jne	.LBB9_21
.LBB9_16:                               #   in Loop: Header=BB9_2 Depth=2
	xorl	%r12d, %r12d
	movb	$58, %al
	jmp	.LBB9_21
	.p2align	4, 0x90
.LBB9_2:                                #   Parent Loop BB9_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %r15
	cmpl	%edx, %r13d
	jl	.LBB9_4
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=2
	cmpl	%ecx, %r14d
	jge	.LBB9_30
.LBB9_4:                                # %.critedge
                                        #   in Loop: Header=BB9_2 Depth=2
	testl	%r12d, %r12d
	jne	.LBB9_17
# BB#5:                                 #   in Loop: Header=BB9_2 Depth=2
	movl	(%r8), %r12d
	addq	$4, %r8
	testl	%r12d, %r12d
	je	.LBB9_6
.LBB9_17:                               # %.critedge106
                                        #   in Loop: Header=BB9_2 Depth=2
	testl	%r12d, %r12d
	jle	.LBB9_19
# BB#18:                                #   in Loop: Header=BB9_2 Depth=2
	movb	$32, ALINE(%r15)
	movslq	%r14d, %r14
	movzbl	1(%rsi,%r14), %eax
	incq	%r14
	movl	$-1, %ebp
	jmp	.LBB9_20
	.p2align	4, 0x90
.LBB9_19:                               #   in Loop: Header=BB9_2 Depth=2
	movslq	%r13d, %r13
	movzbl	1(%rdi,%r13), %eax
	incq	%r13
	movb	%al, ALINE(%r15)
	movl	$1, %ebp
	movb	$32, %al
.LBB9_20:                               #   in Loop: Header=BB9_2 Depth=2
	movb	%al, BLINE(%r15)
	addl	%ebp, %r12d
	movb	$45, %al
.LBB9_21:                               #   in Loop: Header=BB9_2 Depth=2
	leaq	ALINE+1(%r15), %rbx
	movb	%al, CLINE(%r15)
	cmpq	%r10, %rbx
	jae	.LBB9_24
# BB#22:                                #   in Loop: Header=BB9_2 Depth=2
	leaq	1(%r15), %rax
	cmpl	%edx, %r13d
	jl	.LBB9_2
# BB#23:                                #   in Loop: Header=BB9_2 Depth=2
	cmpl	%ecx, %r14d
	jl	.LBB9_2
.LBB9_24:                               #   in Loop: Header=BB9_1 Depth=1
	movl	%r11d, 20(%rsp)         # 4-byte Spill
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movb	$0, CLINE+1(%r15)
	movb	$0, BLINE+1(%r15)
	movb	$0, ALINE+1(%r15)
	imull	$50, 8(%rsp), %esi      # 4-byte Folded Reload
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$ALINE+10, %eax
	cmpq	%rax, %rbx
	movl	$ALINE+10, %ebp
	jb	.LBB9_27
# BB#25:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB9_1 Depth=1
	movl	$ALINE+10, %ebp
	.p2align	4, 0x90
.LBB9_26:                               # %.lr.ph
                                        #   Parent Loop BB9_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	addq	$10, %rbp
	cmpq	%rbx, %rbp
	jbe	.LBB9_26
.LBB9_27:                               # %._crit_edge
                                        #   in Loop: Header=BB9_1 Depth=1
	leaq	ALINE+6(%r15), %rax
	cmpq	%rax, %rbp
	ja	.LBB9_29
# BB#28:                                #   in Loop: Header=BB9_1 Depth=1
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB9_29
.LBB9_30:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	display, .Lfunc_end9-display
	.cfi_endproc

	.globl	fatal
	.p2align	4, 0x90
	.type	fatal,@function
fatal:                                  # @fatal
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi109:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movq	stderr(%rip), %rdi
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end10:
	.size	fatal, .Lfunc_end10-fatal
	.cfi_endproc

	.globl	fatalf
	.p2align	4, 0x90
	.type	fatalf,@function
fatalf:                                 # @fatalf
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi110:
	.cfi_def_cfa_offset 16
	movq	%rsi, %rcx
	movq	%rdi, %rdx
	movq	stderr(%rip), %rdi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	callq	fprintf
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %edi
	callq	exit
.Lfunc_end11:
	.size	fatalf, .Lfunc_end11-fatalf
	.cfi_endproc

	.globl	ckopen
	.p2align	4, 0x90
	.type	ckopen,@function
ckopen:                                 # @ckopen
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 16
.Lcfi112:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	fopen
	testq	%rax, %rax
	je	.LBB12_2
# BB#1:
	popq	%rbx
	retq
.LBB12_2:
	movl	$.L.str.33, %edi
	movq	%rbx, %rsi
	callq	fatalf
.Lfunc_end12:
	.size	ckopen, .Lfunc_end12-ckopen
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI13_0:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.text
	.globl	dtime
	.p2align	4, 0x90
	.type	dtime,@function
dtime:                                  # @dtime
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi113:
	.cfi_def_cfa_offset 16
	xorl	%edi, %edi
	movl	$rusage, %esi
	callq	getrusage
	cvtsi2sdq	rusage(%rip), %xmm1
	cvtsi2sdq	rusage+8(%rip), %xmm0
	mulsd	.LCPI13_0(%rip), %xmm0
	addsd	%xmm1, %xmm0
	popq	%rax
	retq
.Lfunc_end13:
	.size	dtime, .Lfunc_end13-dtime
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Output/sim.res"
	.size	.L.str, 15

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"a+"
	.size	.L.str.1, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"8"
	.size	.L.str.3, 2

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"tob.38-44"
	.size	.L.str.4, 10

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"liv.42-48"
	.size	.L.str.5, 10

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%d"
	.size	.L.str.6, 3

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"specified 0 alignments"
	.size	.L.str.7, 23

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"r"
	.size	.L.str.8, 2

	.type	name1,@object           # @name1
	.local	name1
	.comm	name1,8,8
	.type	name2,@object           # @name2
	.local	name2
	.comm	name2,8,8
	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"argument %d has improper form"
	.size	.L.str.9, 30

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"options are M, I, V, O and E."
	.size	.L.str.10, 30

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\t\tM = %g, I = %g, V = %g\n\t\tO = %g, E = %g\n\n"
	.size	.L.str.12, 44

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"  Run Line: %s %s %s\n"
	.size	.L.str.14, 22

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"#######################################################\n"
	.size	.L.str.15, 57

	.type	low,@object             # @low
	.bss
	.globl	low
	.p2align	3
low:
	.quad	0
	.size	low, 8

	.type	most,@object            # @most
	.globl	most
	.p2align	3
most:
	.quad	0
	.size	most, 8

	.type	CC,@object              # @CC
	.local	CC
	.comm	CC,8,8
	.type	DD,@object              # @DD
	.local	DD
	.comm	DD,8,8
	.type	RR,@object              # @RR
	.local	RR
	.comm	RR,8,8
	.type	SS,@object              # @SS
	.local	SS
	.comm	SS,8,8
	.type	EE,@object              # @EE
	.local	EE
	.comm	EE,8,8
	.type	FF,@object              # @FF
	.local	FF
	.comm	FF,8,8
	.type	HH,@object              # @HH
	.local	HH
	.comm	HH,8,8
	.type	WW,@object              # @WW
	.local	WW
	.comm	WW,8,8
	.type	II,@object              # @II
	.local	II
	.comm	II,8,8
	.type	JJ,@object              # @JJ
	.local	JJ
	.comm	JJ,8,8
	.type	XX,@object              # @XX
	.local	XX
	.comm	XX,8,8
	.type	YY,@object              # @YY
	.local	YY
	.comm	YY,8,8
	.type	row,@object             # @row
	.comm	row,8,8
	.type	z,@object               # @z
	.comm	z,8,8
	.type	v,@object               # @v
	.local	v
	.comm	v,8,8
	.type	q,@object               # @q
	.local	q
	.comm	q,4,4
	.type	r,@object               # @r
	.local	r
	.comm	r,4,4
	.type	qr,@object              # @qr
	.local	qr
	.comm	qr,4,4
	.type	LIST,@object            # @LIST
	.comm	LIST,8,8
	.type	.L.str.16,@object       # @.str.16
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.16:
	.asciz	"                 Upper Sequence : %s\n"
	.size	.L.str.16, 38

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"                         Length : %d\n"
	.size	.L.str.17, 38

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"                 Lower Sequence : %s\n"
	.size	.L.str.18, 38

	.type	min,@object             # @min
	.local	min
	.comm	min,4,4
	.type	numnode,@object         # @numnode
	.local	numnode
	.comm	numnode,4,4
	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"The number of alignments computed is too large"
	.size	.L.str.19, 47

	.type	m1,@object              # @m1
	.local	m1
	.comm	m1,4,4
	.type	mm,@object              # @mm
	.local	mm
	.comm	mm,4,4
	.type	n1,@object              # @n1
	.local	n1
	.comm	n1,4,4
	.type	nn,@object              # @nn
	.local	nn
	.comm	nn,4,4
	.type	rl,@object              # @rl
	.local	rl
	.comm	rl,4,4
	.type	cl,@object              # @cl
	.local	cl
	.comm	cl,4,4
	.type	I,@object               # @I
	.local	I
	.comm	I,4,4
	.type	J,@object               # @J
	.local	J
	.comm	J,4,4
	.type	sapp,@object            # @sapp
	.local	sapp
	.comm	sapp,8,8
	.type	last,@object            # @last
	.local	last
	.comm	last,4,4
	.type	al_len,@object          # @al_len
	.local	al_len
	.comm	al_len,4,4
	.type	no_mat,@object          # @no_mat
	.local	no_mat
	.comm	no_mat,4,4
	.type	no_mis,@object          # @no_mis
	.local	no_mis
	.comm	no_mis,4,4
	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"      Number %d Local Alignment\n"
	.size	.L.str.21, 33

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"      Similarity Score : %g\n"
	.size	.L.str.22, 29

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"      Match Percentage : %d%%\n"
	.size	.L.str.23, 31

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"      Number of Matches : %d\n"
	.size	.L.str.24, 30

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"      Number of Mismatches : %d\n"
	.size	.L.str.25, 33

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"      Total Length of Gaps : %d\n"
	.size	.L.str.26, 33

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"      Begins at (%d, %d) and Ends at (%d, %d)\n"
	.size	.L.str.27, 47

	.type	flag,@object            # @flag
	.local	flag
	.comm	flag,1,2
	.type	tt,@object              # @tt
	.local	tt
	.comm	tt,1,2
	.type	ALINE,@object           # @ALINE
	.local	ALINE
	.comm	ALINE,51,16
	.type	BLINE,@object           # @BLINE
	.local	BLINE
	.comm	BLINE,51,16
	.type	CLINE,@object           # @CLINE
	.local	CLINE
	.comm	CLINE,51,16
	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"\n%5d "
	.size	.L.str.28, 6

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"    .    :"
	.size	.L.str.29, 11

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"    ."
	.size	.L.str.30, 6

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"\n%5d %s\n      %s\n%5d %s\n"
	.size	.L.str.31, 25

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"%s\n"
	.size	.L.str.32, 4

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Cannot open %s."
	.size	.L.str.33, 16

	.type	rusage,@object          # @rusage
	.comm	rusage,144,8
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"\t\tSIM output with parameters:"
	.size	.Lstr, 30

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"Can not open Output/sim.res\n"
	.size	.Lstr.1, 29

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"\n*********************************************************"
	.size	.Lstr.2, 59


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
