	.text
	.file	"clamscan_manager.bc"
	.globl	scanmanager
	.p2align	4, 0x90
	.type	scanmanager,@function
scanmanager:                            # @scanmanager
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$8424, %rsp             # imm = 0x20E8
.Lcfi6:
	.cfi_def_cfa_offset 8480
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	$0, 16(%rsp)
	movl	$.L.str, %esi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB0_10
# BB#1:
	movl	$.L.str.1, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB0_10
# BB#2:
	movl	$.L.str.2, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB0_10
# BB#3:
	movl	$.L.str.3, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB0_10
# BB#4:
	movl	$.L.str.4, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB0_10
# BB#5:
	movl	$.L.str.5, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB0_10
# BB#6:
	movl	$.L.str.6, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB0_10
# BB#7:
	movl	$.L.str.7, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB0_10
# BB#8:
	movl	$.L.str.8, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_9
.LBB0_10:
	callq	geteuid
	movb	$1, %r14b
	testl	%eax, %eax
	je	.LBB0_12
.LBB0_11:
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB0_13
.LBB0_12:
	movl	$.L.str.9, %edi
	callq	getpwnam
	movq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_114
.LBB0_13:
	movl	$.L.str.11, %esi
	movq	%r15, %rdi
	callq	opt_check
	xorl	%ebx, %ebx
	testl	%eax, %eax
	sete	%bl
	leal	(%rbx,%rbx), %r12d
	movl	$.L.str.12, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	leal	8(%rbx,%rbx), %r13d
	cmovnel	%r12d, %r13d
	movl	$.L.str.13, %esi
	movq	%r15, %rdi
	callq	opt_check
	xorl	%ebx, %ebx
	testl	%eax, %eax
	sete	%bl
	shll	$10, %ebx
	movl	$.L.str.14, %esi
	movq	%r15, %rdi
	callq	opt_check
	leal	2048(%rbx), %ebp
	testl	%eax, %eax
	cmovel	%ebx, %ebp
	movl	$.L.str.15, %esi
	movq	%r15, %rdi
	callq	opt_check
	movl	%ebp, %r12d
	orl	$4096, %r12d            # imm = 0x1000
	testl	%eax, %eax
	cmovel	%ebp, %r12d
	movl	$.L.str.16, %esi
	movq	%r15, %rdi
	callq	opt_check
	movl	%r13d, %ebx
	orl	$4, %ebx
	testl	%eax, %eax
	cmovel	%r13d, %ebx
	movl	$.L.str.17, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_15
# BB#14:
	movl	$.L.str.17, %esi
	movq	%r15, %rdi
	callq	opt_arg
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	strtol
	movzbl	%al, %esi
	movl	$2, %edi
	callq	cli_ac_setdepth
.LBB0_15:
	movl	$.L.str.18, %esi
	movq	%r15, %rdi
	callq	opt_check
	movl	%ebx, %ebp
	orl	$16, %ebp
	testl	%eax, %eax
	cmovel	%ebx, %ebp
	movl	$.L.str.19, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_19
# BB#16:
	movl	$.L.str.19, %esi
	movq	%r15, %rdi
	callq	opt_arg
	leaq	16(%rsp), %rsi
	movl	$info, %edx
	movq	%rax, %rdi
	movl	%ebp, %ecx
	callq	cl_load
	testl	%eax, %eax
	je	.LBB0_22
# BB#17:
	movl	%eax, %edi
	callq	cl_strerror
	movq	%rax, %rcx
	movl	$.L.str.20, %edi
	jmp	.LBB0_18
.LBB0_19:
	callq	freshdbdir
	movq	%rax, %rbx
	leaq	16(%rsp), %rsi
	movl	$info, %edx
	movq	%rbx, %rdi
	movl	%ebp, %ecx
	callq	cl_load
	testl	%eax, %eax
	je	.LBB0_21
# BB#20:                                # %.critedge
	movl	%eax, %edi
	callq	cl_strerror
	movq	%rax, %rcx
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	logg
	movq	%rbx, %rdi
	callq	free
	movl	$50, %eax
	jmp	.LBB0_113
.LBB0_21:
	movq	%rbx, %rdi
	callq	free
.LBB0_22:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_23
# BB#24:
	callq	cl_build
	testl	%eax, %eax
	je	.LBB0_26
# BB#25:
	movl	%eax, %edi
	callq	cl_strerror
	movq	%rax, %rcx
	movl	$.L.str.22, %edi
.LBB0_18:
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	logg
	movl	$50, %eax
	jmp	.LBB0_113
.LBB0_23:
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	logg
	movl	$50, %eax
	jmp	.LBB0_113
.LBB0_26:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movl	$.L.str.23, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_30
# BB#27:
	movl	$.L.str.23, %esi
	movq	%r15, %rdi
	callq	opt_arg
	movq	%rax, %rbx
	callq	__ctype_tolower_loc
	movq	(%rax), %rbp
	movq	%rbx, %rdi
	callq	strlen
	movsbq	-1(%rbx,%rax), %rcx
	cmpl	$109, (%rbp,%rcx,4)
	jne	.LBB0_29
# BB#28:
	movl	$1, %esi
	movq	%rax, %rdi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	strlen
	leaq	-1(%rax), %rdx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	strncpy
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strtol
	shll	$20, %eax
	cltq
	movq	%rax, 56(%rsp)
	movq	%rbp, %rdi
	callq	free
	jmp	.LBB0_31
.LBB0_30:
	movq	$10485760, 56(%rsp)     # imm = 0xA00000
	jmp	.LBB0_31
.LBB0_29:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	shll	$10, %eax
	cltq
	movq	%rax, 56(%rsp)
.LBB0_31:
	movl	$.L.str.24, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_32
# BB#33:
	movl	$.L.str.24, %esi
	movq	%r15, %rdi
	callq	opt_arg
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	strtol
	jmp	.LBB0_34
.LBB0_32:
	movl	$500, %eax              # imm = 0x1F4
.LBB0_34:
	movl	%eax, 36(%rsp)
	movl	$.L.str.25, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_35
# BB#36:
	movl	$.L.str.25, %esi
	movq	%r15, %rdi
	callq	opt_arg
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	strtol
	jmp	.LBB0_37
.LBB0_35:
	movl	$8, %eax
.LBB0_37:
	movl	%eax, 32(%rsp)
	movl	$.L.str.26, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_38
# BB#39:
	movl	$.L.str.26, %esi
	movq	%r15, %rdi
	callq	opt_arg
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	strtol
	jmp	.LBB0_40
.LBB0_38:
	movl	$64, %eax
.LBB0_40:
	movl	%eax, 40(%rsp)
	movl	$.L.str.27, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_41
# BB#42:
	movl	$.L.str.27, %esi
	movq	%r15, %rdi
	callq	opt_arg
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	strtol
	jmp	.LBB0_43
.LBB0_41:
	movl	$250, %eax
.LBB0_43:
	movl	%eax, 44(%rsp)
	movl	$.L.str.28, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB0_45
# BB#44:
	movl	$.L.str.29, %esi
	movq	%r15, %rdi
	callq	opt_check
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	orl	%ecx, %r12d
.LBB0_45:
	movl	$.L.str.30, %esi
	movq	%r15, %rdi
	callq	opt_check
	movl	%r12d, %ebx
	orl	$64, %ebx
	testl	%eax, %eax
	cmovel	%r12d, %ebx
	movl	$.L.str.31, %esi
	movq	%r15, %rdi
	callq	opt_check
	movl	%ebx, %ebp
	orl	$8, %ebp
	testl	%eax, %eax
	cmovel	%ebx, %ebp
	movl	$.L.str.32, %esi
	movq	%r15, %rdi
	callq	opt_check
	movl	%ebp, %ebx
	orl	$256, %ebx              # imm = 0x100
	testl	%eax, %eax
	cmovel	%ebp, %ebx
	movl	$.L.str.33, %esi
	movq	%r15, %rdi
	callq	opt_check
	movl	%ebx, %ecx
	andl	$-33, %ecx
	orl	$32, %ebx
	testl	%eax, %eax
	cmovnel	%ecx, %ebx
	movl	$.L.str.34, %esi
	movq	%r15, %rdi
	callq	opt_check
	movl	%ebx, %ecx
	andl	$-8193, %ecx            # imm = 0xDFFF
	orl	$8192, %ebx             # imm = 0x2000
	testl	%eax, %eax
	cmovnel	%ecx, %ebx
	movl	$.L.str.35, %esi
	movq	%r15, %rdi
	callq	opt_check
	movl	%ebx, %ecx
	andl	$-5, %ecx
	orl	$4, %ebx
	testl	%eax, %eax
	cmovnel	%ecx, %ebx
	movl	$.L.str.36, %esi
	movq	%r15, %rdi
	callq	opt_check
	movl	%ebx, %ecx
	andl	$-16385, %ecx           # imm = 0xBFFF
	orl	$16384, %ebx            # imm = 0x4000
	testl	%eax, %eax
	cmovnel	%ecx, %ebx
	movl	$.L.str.37, %esi
	movq	%r15, %rdi
	callq	opt_check
	movl	%ebx, %ecx
	andl	$-17, %ecx
	orl	$16, %ebx
	testl	%eax, %eax
	cmovnel	%ecx, %ebx
	movl	$.L.str.38, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_47
# BB#46:
	andl	$-3, %ebx
	jmp	.LBB0_48
.LBB0_47:
	movl	$.L.str.39, %esi
	movq	%r15, %rdi
	callq	opt_check
	testl	%eax, %eax
	movl	$2, %eax
	movl	$130, %ecx
	cmovel	%eax, %ecx
	orl	%ecx, %ebx
.LBB0_48:
	movl	$.L.str.40, %esi
	movq	%r15, %rdi
	callq	opt_check
	movl	%ebx, %ecx
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	$512, %ebx              # imm = 0x200
	testl	%eax, %eax
	cmovnel	%ecx, %ebx
	movq	$0, procdev(%rip)
	leaq	80(%rsp), %rdx
	movl	$1, %edi
	movl	$.L.str.41, %esi
	callq	__xstat
	cmpl	$-1, %eax
	je	.LBB0_51
# BB#49:
	cmpq	$0, 128(%rsp)
	jne	.LBB0_51
# BB#50:
	movq	80(%rsp), %rax
	movq	%rax, procdev(%rip)
.LBB0_51:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB0_53
# BB#52:
	movb	(%rdi), %al
	testb	%al, %al
	je	.LBB0_53
# BB#55:
	cmpb	$45, %al
	jne	.LBB0_57
# BB#56:
	cmpb	$0, 1(%rdi)
	je	.LBB0_82
.LBB0_57:                               # %.preheader
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$.L.str.44, %edx
	callq	cli_strtok
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_111
# BB#58:                                # %.lr.ph241
	xorl	%r12d, %r12d
	testb	%r14b, %r14b
	je	.LBB0_59
	.p2align	4, 0x90
.LBB0_61:                               # %.lr.ph241.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_64 Depth 2
	movl	$2, %esi
	movq	%rbp, %rdi
	callq	fileinfo
	movl	%eax, %r13d
	cmpl	$-1, %r13d
	je	.LBB0_80
# BB#62:                                #   in Loop: Header=BB0_61 Depth=1
	movq	%rbp, %rdi
	callq	strlen
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	addq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB0_65
# BB#63:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB0_61 Depth=1
	cltq
	incq	%rax
	.p2align	4, 0x90
.LBB0_64:                               # %.lr.ph.us
                                        #   Parent Loop BB0_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$47, -1(%rbp,%rax)
	jne	.LBB0_65
# BB#79:                                #   in Loop: Header=BB0_64 Depth=2
	movb	$0, -1(%rbp,%rax)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB0_64
.LBB0_65:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_61 Depth=1
	movb	(%rbp), %al
	cmpb	$47, %al
	movq	%rbp, %r14
	je	.LBB0_70
# BB#66:                                # %._crit_edge.us
                                        #   in Loop: Header=BB0_61 Depth=1
	cmpb	$92, %al
	movq	%rbp, %r14
	je	.LBB0_70
# BB#67:                                #   in Loop: Header=BB0_61 Depth=1
	cmpb	$58, 1(%rbp)
	movq	%rbp, %r14
	je	.LBB0_70
# BB#68:                                #   in Loop: Header=BB0_61 Depth=1
	movl	$1024, %esi             # imm = 0x400
	leaq	224(%rsp), %r14
	movq	%r14, %rdi
	callq	getcwd
	testq	%rax, %rax
	je	.LBB0_109
# BB#69:                                #   in Loop: Header=BB0_61 Depth=1
	movl	$512, %edi              # imm = 0x200
	callq	malloc
	movq	%r14, %rcx
	movq	%rax, %r14
	movl	$512, %esi              # imm = 0x200
	movl	$.L.str.46, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbp, %r8
	callq	snprintf
	movl	$.L.str.47, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	logg
	.p2align	4, 0x90
.LBB0_70:                               #   in Loop: Header=BB0_61 Depth=1
	andl	$61440, %r13d           # imm = 0xF000
	cmpl	$32768, %r13d           # imm = 0x8000
	je	.LBB0_73
# BB#71:                                #   in Loop: Header=BB0_61 Depth=1
	movzwl	%r13w, %eax
	cmpl	$16384, %eax            # imm = 0x4000
	jne	.LBB0_74
# BB#72:                                #   in Loop: Header=BB0_61 Depth=1
	movq	16(%rsp), %rsi
	movl	$1, (%rsp)
	movq	%r14, %rdi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	leaq	32(%rsp), %r8
	movl	%ebx, %r9d
	callq	treewalk
	movl	%eax, %r13d
	jmp	.LBB0_75
.LBB0_73:                               #   in Loop: Header=BB0_61 Depth=1
	movq	16(%rsp), %rsi
	movq	%r14, %rdi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	leaq	32(%rsp), %r8
	movl	%ebx, %r9d
	callq	scanfile
	movl	%eax, %r13d
	jmp	.LBB0_75
.LBB0_74:                               #   in Loop: Header=BB0_61 Depth=1
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	logg
	movl	$52, %r13d
.LBB0_75:                               #   in Loop: Header=BB0_61 Depth=1
	movb	(%rbp), %al
	cmpb	$47, %al
	je	.LBB0_81
# BB#76:                                #   in Loop: Header=BB0_61 Depth=1
	cmpb	$92, %al
	je	.LBB0_81
# BB#77:                                #   in Loop: Header=BB0_61 Depth=1
	cmpb	$58, 1(%rbp)
	je	.LBB0_81
# BB#78:                                #   in Loop: Header=BB0_61 Depth=1
	movq	%r14, %rdi
	callq	free
	jmp	.LBB0_81
.LBB0_80:                               #   in Loop: Header=BB0_61 Depth=1
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	logg
	movq	%rbp, %rdi
	callq	perror
	movl	$56, %r13d
	.p2align	4, 0x90
.LBB0_81:                               # %.thread.us
                                        #   in Loop: Header=BB0_61 Depth=1
	movq	%rbp, %rdi
	callq	free
	incl	%r12d
	movq	8(%r15), %rdi
	movl	$.L.str.44, %edx
	movl	%r12d, %esi
	callq	cli_strtok
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB0_61
	jmp	.LBB0_111
.LBB0_53:
	leaq	224(%rsp), %rdi
	movl	$1024, %esi             # imm = 0x400
	callq	getcwd
	testq	%rax, %rax
	je	.LBB0_54
# BB#110:
	movq	16(%rsp), %rsi
	movl	$1, (%rsp)
	leaq	224(%rsp), %rdi
	leaq	32(%rsp), %r8
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movl	%ebx, %r9d
	callq	treewalk
	movl	%eax, %r13d
	jmp	.LBB0_111
.LBB0_54:
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	logg
	movl	$57, %r13d
	jmp	.LBB0_111
.LBB0_60:                               #   in Loop: Header=BB0_59 Depth=1
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	logg
	movq	%rbp, %rdi
	callq	perror
	movl	$56, %r13d
	jmp	.LBB0_107
	.p2align	4, 0x90
.LBB0_59:                               # %.lr.ph241.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_100 Depth 2
	movl	%ebx, %r14d
	movl	$2, %esi
	movq	%rbp, %rdi
	callq	fileinfo
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB0_60
# BB#98:                                #   in Loop: Header=BB0_59 Depth=1
	movq	%rbp, %rdi
	callq	strlen
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	addq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB0_102
# BB#99:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_59 Depth=1
	cltq
	incq	%rax
	.p2align	4, 0x90
.LBB0_100:                              # %.lr.ph
                                        #   Parent Loop BB0_59 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$47, -1(%rbp,%rax)
	jne	.LBB0_102
# BB#101:                               #   in Loop: Header=BB0_100 Depth=2
	movb	$0, -1(%rbp,%rax)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB0_100
.LBB0_102:                              # %._crit_edge
                                        #   in Loop: Header=BB0_59 Depth=1
	andl	$61440, %ebx            # imm = 0xF000
	cmpl	$16384, %ebx            # imm = 0x4000
	je	.LBB0_105
# BB#103:                               # %._crit_edge
                                        #   in Loop: Header=BB0_59 Depth=1
	movzwl	%bx, %eax
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB0_106
# BB#104:                               #   in Loop: Header=BB0_59 Depth=1
	movq	16(%rsp), %rsi
	movq	%rbp, %rdi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	leaq	32(%rsp), %r8
	movl	%r14d, %ebx
	movl	%ebx, %r9d
	callq	scanfile
	movl	%eax, %r13d
	jmp	.LBB0_108
.LBB0_105:                              #   in Loop: Header=BB0_59 Depth=1
	movq	16(%rsp), %rsi
	movl	$1, (%rsp)
	movq	%rbp, %rdi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	leaq	32(%rsp), %r8
	movl	%r14d, %ebx
	movl	%ebx, %r9d
	callq	treewalk
	movl	%eax, %r13d
	jmp	.LBB0_108
.LBB0_106:                              #   in Loop: Header=BB0_59 Depth=1
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	logg
	movl	$52, %r13d
.LBB0_107:                              # %.thread
                                        #   in Loop: Header=BB0_59 Depth=1
	movl	%r14d, %ebx
.LBB0_108:                              # %.thread
                                        #   in Loop: Header=BB0_59 Depth=1
	movq	%rbp, %rdi
	callq	free
	incl	%r12d
	movq	8(%r15), %rdi
	movl	$.L.str.44, %edx
	movl	%r12d, %esi
	callq	cli_strtok
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB0_59
	jmp	.LBB0_111
.LBB0_82:
	movl	%ebx, %r13d
	movq	16(%rsp), %r14
	movl	$.L.str.74, %edi
	callq	getenv
	testq	%rax, %rax
	movl	$.L.str.75, %ebx
	cmovneq	%rax, %rbx
	movl	$.L.str.9, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	checkaccess
	cmpl	$1, %eax
	jne	.LBB0_83
# BB#84:
	movq	%rbx, %rdi
	callq	cli_gentemp
	movq	%rax, %r15
	movl	$.L.str.77, %esi
	movq	%r15, %rdi
	callq	fopen
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB0_115
# BB#85:
	leaq	224(%rsp), %rbx
.LBB0_86:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	stdin(%rip), %rcx
	movl	$1, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%rbx, %rdi
	callq	fread
	testl	%eax, %eax
	je	.LBB0_89
# BB#87:                                #   in Loop: Header=BB0_86 Depth=1
	movslq	%eax, %rbp
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	movq	%r12, %rcx
	callq	fwrite
	cmpq	%rbp, %rax
	jae	.LBB0_86
# BB#88:
	movl	$.L.str.79, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	logg
	movq	%r15, %rdi
	callq	free
	movl	$58, %r13d
	jmp	.LBB0_111
.LBB0_83:
	movl	$.L.str.76, %edi
	xorl	%eax, %eax
	callq	logg
	movl	$64, %r13d
	jmp	.LBB0_111
.LBB0_115:
	movl	$.L.str.78, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	logg
	movq	%r15, %rdi
	callq	free
	movl	$63, %r13d
	jmp	.LBB0_111
.LBB0_109:                              # %.us-lcssa.us
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	logg
	movl	$57, %eax
	jmp	.LBB0_113
.LBB0_89:
	movq	%r12, %rdi
	callq	fclose
	movl	$.L.str.80, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	logg
	incl	info+8(%rip)
	leaq	72(%rsp), %rsi
	leaq	32(%rsp), %r8
	movl	$info+24, %edx
	movq	%r15, %rdi
	movq	%r14, %rcx
	movl	%r13d, %r9d
	callq	cl_scanfile
	movl	%eax, %r13d
	cmpl	$1, %r13d
	jne	.LBB0_92
# BB#90:
	movq	72(%rsp), %rsi
	movl	$.L.str.81, %edi
	xorl	%eax, %eax
	callq	logg
	incl	info+12(%rip)
	cmpw	$0, bell(%rip)
	je	.LBB0_97
# BB#91:
	movq	stderr(%rip), %rsi
	movl	$7, %edi
	callq	fputc
	jmp	.LBB0_97
.LBB0_92:
	movzwl	printinfected(%rip), %eax
	testl	%r13d, %r13d
	je	.LBB0_93
# BB#95:
	testw	%ax, %ax
	jne	.LBB0_97
# BB#96:
	movl	%r13d, %edi
	callq	cl_strerror
	movq	%rax, %rcx
	movl	$.L.str.84, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	logg
	jmp	.LBB0_97
.LBB0_93:
	testw	%ax, %ax
	jne	.LBB0_97
# BB#94:
	movl	$.L.str.83, %edi
	xorl	%eax, %eax
	callq	mprintf
.LBB0_97:
	movq	%r15, %rdi
	callq	unlink
	movq	%r15, %rdi
	callq	free
.LBB0_111:                              # %.thread235
	movq	16(%rsp), %rdi
	callq	cl_free
	movl	$1, %eax
	cmpl	$0, info+12(%rip)
	jne	.LBB0_113
# BB#112:
	xorl	%eax, %eax
	cmpl	$50, %r13d
	cmovgel	%r13d, %eax
.LBB0_113:
	addq	$8424, %rsp             # imm = 0x20E8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_9:
	xorl	%r14d, %r14d
	jmp	.LBB0_11
.LBB0_114:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	logg
	movl	$60, %edi
	callq	exit
.Lfunc_end0:
	.size	scanmanager, .Lfunc_end0-scanmanager
	.cfi_endproc

	.globl	scanfile
	.p2align	4, 0x90
	.type	scanfile,@function
scanfile:                               # @scanfile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$408, %rsp              # imm = 0x198
.Lcfi19:
	.cfi_def_cfa_offset 464
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
	movq	%r8, %r12
	movq	%rcx, %r14
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	movq	%rdi, %r15
	cmpq	$0, procdev(%rip)
	je	.LBB1_5
# BB#1:
	leaq	264(%rsp), %rdx
	movl	$1, %edi
	movq	%r15, %rsi
	callq	__xstat
	cmpl	$-1, %eax
	je	.LBB1_5
# BB#2:
	movq	264(%rsp), %rax
	cmpq	procdev(%rip), %rax
	jne	.LBB1_5
# BB#3:
	xorl	%r12d, %r12d
	cmpw	$0, printinfected(%rip)
	jne	.LBB1_96
# BB#4:
	xorl	%r12d, %r12d
	movl	$.L.str.49, %edi
	jmp	.LBB1_95
.LBB1_5:
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movl	$.L.str.50, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB1_10
# BB#6:
	leaq	96(%rsp), %rdx
	movl	$.L.str.50, %esi
	movq	%r14, %rdi
	callq	opt_firstarg
	testq	%rax, %rax
	je	.LBB1_10
# BB#7:                                 # %.lr.ph107.preheader
	leaq	96(%rsp), %rbp
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph107
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	match_regex
	cmpl	$1, %eax
	je	.LBB1_22
# BB#9:                                 #   in Loop: Header=BB1_8 Depth=1
	movl	$.L.str.50, %esi
	movq	%rbp, %rdi
	callq	opt_nextarg
	testq	%rax, %rax
	jne	.LBB1_8
.LBB1_10:                               # %.loopexit
	movl	$.L.str.51, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB1_17
# BB#11:
	leaq	96(%rsp), %rdx
	movl	$.L.str.51, %esi
	movq	%r14, %rdi
	callq	opt_firstarg
	testq	%rax, %rax
	je	.LBB1_15
# BB#12:                                # %.lr.ph.preheader
	leaq	96(%rsp), %rbp
	.p2align	4, 0x90
.LBB1_13:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	match_regex
	cmpl	$1, %eax
	je	.LBB1_17
# BB#14:                                #   in Loop: Header=BB1_13 Depth=1
	movl	$.L.str.51, %esi
	movq	%rbp, %rdi
	callq	opt_nextarg
	testq	%rax, %rax
	jne	.LBB1_13
.LBB1_15:                               # %.critedge100
	xorl	%r12d, %r12d
	cmpw	$0, printinfected(%rip)
	jne	.LBB1_96
# BB#16:
	xorl	%r12d, %r12d
	movl	$.L.str.52, %edi
	jmp	.LBB1_95
.LBB1_17:                               # %.critedge
	movl	$1, %esi
	movq	%r15, %rdi
	callq	fileinfo
	testl	%eax, %eax
	je	.LBB1_23
# BB#18:
	movq	%r12, 88(%rsp)          # 8-byte Spill
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	callq	geteuid
	testl	%eax, %eax
	je	.LBB1_20
# BB#19:
	xorl	%r12d, %r12d
	xorl	%esi, %esi
	movl	$4, %edx
	movq	%r15, %rdi
	callq	checkaccess
	cmpl	$1, %eax
	jne	.LBB1_25
.LBB1_20:
	incl	info+8(%rip)
	movl	$.L.str.55, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_27
# BB#21:
	movw	$1, %bp
	testb	$1, %r13b
	movq	16(%rsp), %r12          # 8-byte Reload
	jne	.LBB1_29
	jmp	.LBB1_33
.LBB1_22:
	xorl	%r12d, %r12d
	jmp	.LBB1_96
.LBB1_23:
	xorl	%r12d, %r12d
	cmpw	$0, printinfected(%rip)
	jne	.LBB1_96
# BB#24:
	xorl	%r12d, %r12d
	movl	$.L.str.53, %edi
	jmp	.LBB1_95
.LBB1_25:
	cmpw	$0, printinfected(%rip)
	jne	.LBB1_96
# BB#26:
	xorl	%r12d, %r12d
	movl	$.L.str.54, %edi
	jmp	.LBB1_95
.LBB1_27:
	movl	$.L.str.56, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testb	$1, %r13b
	movw	$1, %bp
	movq	16(%rsp), %r12          # 8-byte Reload
	je	.LBB1_33
# BB#28:
	testl	%eax, %eax
	je	.LBB1_33
.LBB1_29:
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	88(%rsp), %rdx          # 8-byte Reload
	movl	%r13d, %ecx
	callq	checkfile
	testl	%eax, %eax
	je	.LBB1_72
# BB#30:
	cmpl	$54, %eax
	je	.LBB1_73
# BB#31:
	cmpl	$1, %eax
	je	.LBB1_60
# BB#32:
	andl	$-2, %r13d
	xorl	%ebp, %ebp
.LBB1_33:
	movl	$.L.str.55, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_35
# BB#34:
	movl	$.L.str, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB1_53
.LBB1_35:
	movl	$.L.str.56, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_37
# BB#36:
	movl	$.L.str.1, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB1_53
.LBB1_37:
	movl	$.L.str.62, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_39
# BB#38:
	movl	$.L.str.2, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB1_53
.LBB1_39:
	movl	$.L.str.63, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_41
# BB#40:
	movl	$.L.str.3, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB1_53
.LBB1_41:
	movl	$.L.str.64, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_43
# BB#42:
	movl	$.L.str.4, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB1_53
.LBB1_43:
	movl	$.L.str.65, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_45
# BB#44:
	movl	$.L.str.5, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB1_53
.LBB1_45:
	movl	$.L.str.66, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_47
# BB#46:
	movl	$.L.str.6, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB1_53
.LBB1_47:
	movl	$.L.str.67, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_49
# BB#48:
	movl	$.L.str.8, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB1_53
.LBB1_49:
	movl	$.L.str.68, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB1_51
# BB#50:
	movl	$.L.str.69, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_58
.LBB1_51:
	movl	$.L.str.7, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB1_53
# BB#52:
	movl	$.L.str.8, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB1_58
.LBB1_53:
	movl	$.L.str.9, %esi
	movl	$4, %edx
	movq	%r15, %rdi
	callq	checkaccess
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	addl	$2, %eax
	cmpl	$3, %eax
	ja	.LBB1_58
# BB#54:
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_55:
	callq	geteuid
	testl	%eax, %eax
	je	.LBB1_78
# BB#56:
	xorl	%r12d, %r12d
	cmpw	$0, printinfected(%rip)
	jne	.LBB1_96
# BB#57:
	xorl	%r12d, %r12d
	movl	$.L.str.72, %edi
	jmp	.LBB1_95
.LBB1_58:
	movzwl	%bp, %r8d
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	88(%rsp), %rdx          # 8-byte Reload
	movl	%r13d, %ecx
.LBB1_59:
	callq	checkfile
	movl	%eax, %r12d
	cmpl	$1, %r12d
	jne	.LBB1_96
.LBB1_60:
	movl	$.L.str.57, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB1_74
# BB#61:
	movq	%r15, %rdi
	callq	unlink
	testl	%eax, %eax
	je	.LBB1_83
# BB#62:
	movl	$.L.str.58, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	logg
	incl	info+16(%rip)
	movl	$1, %r12d
	jmp	.LBB1_96
.LBB1_63:
	leaq	120(%rsp), %rdx
	movl	$1, %edi
	movq	%r15, %rsi
	callq	__xstat
	movl	$61440, %eax            # imm = 0xF000
	andl	144(%rsp), %eax
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB1_94
# BB#64:
	movl	$.L.str.74, %edi
	callq	getenv
	testq	%rax, %rax
	movl	$.L.str.75, %ebp
	cmovneq	%rax, %rbp
	movl	$.L.str.9, %esi
	movl	$2, %edx
	movq	%rbp, %rdi
	callq	checkaccess
	cmpl	$1, %eax
	jne	.LBB1_147
# BB#65:
	movq	%rbp, %rdi
	callq	cli_gentemp
	movq	%rax, %rbp
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbp, %rdi
	callq	mkdir
	testl	%eax, %eax
	jne	.LBB1_148
# BB#66:
	movq	24(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB1_68
# BB#67:
	movl	16(%rbx), %esi
	movl	20(%rbx), %edx
	movq	%rbp, %rdi
	callq	chown
.LBB1_68:
	movl	$.L.str.55, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_97
# BB#69:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movl	$.L.str.111, %eax
	movd	%rax, %xmm0
	movl	$.L.str, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 32(%rsp)
	movl	$.L.str.113, %eax
	movd	%rax, %xmm0
	movl	$.L.str.112, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 48(%rsp)
	movq	%r15, 64(%rsp)
	movl	$.L.str, %esi
.LBB1_70:
	movq	%r14, %rdi
	callq	opt_arg
	testq	%rax, %rax
	jne	.LBB1_108
# BB#71:
	leaq	32(%rsp), %rsi
	movl	$.L.str, %edi
	jmp	.LBB1_109
.LBB1_72:
	xorl	%r12d, %r12d
	jmp	.LBB1_96
.LBB1_73:
	movl	$54, %r12d
	jmp	.LBB1_96
.LBB1_74:
	movl	$.L.str.60, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB1_76
# BB#75:
	movl	$.L.str.61, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB1_77
.LBB1_76:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	move_infected
.LBB1_77:                               # %.loopexit104
	movl	$1, %r12d
	jmp	.LBB1_96
.LBB1_78:
	movq	88(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB1_84
# BB#79:
	cmpq	$0, 24(%rbx)
	je	.LBB1_84
# BB#80:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	fileinfo
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shrl	$10, %eax
	cmpq	24(%rbx), %rax
	jbe	.LBB1_84
# BB#81:
	xorl	%r12d, %r12d
	cmpw	$0, printinfected(%rip)
	jne	.LBB1_96
# BB#82:
	xorl	%r12d, %r12d
	movl	$.L.str.73, %edi
	jmp	.LBB1_95
.LBB1_83:
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	logg
	movl	$1, %r12d
	jmp	.LBB1_96
.LBB1_84:
	leaq	120(%rsp), %rdx
	movl	$1, %edi
	movq	%r15, %rsi
	callq	__xstat
	movl	$61440, %eax            # imm = 0xF000
	andl	144(%rsp), %eax
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB1_94
# BB#85:
	movl	$.L.str.74, %edi
	callq	getenv
	testq	%rax, %rax
	movl	$.L.str.75, %r12d
	cmovneq	%rax, %r12
	movl	$.L.str.9, %esi
	movl	$2, %edx
	movq	%r12, %rdi
	callq	checkaccess
	cmpl	$1, %eax
	jne	.LBB1_151
# BB#86:
	movq	%r12, %rdi
	callq	cli_gentemp
	movq	%rax, %r12
	movl	$448, %esi              # imm = 0x1C0
	movq	%r12, %rdi
	callq	mkdir
	testl	%eax, %eax
	jne	.LBB1_152
# BB#87:
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%r15, %rdi
	callq	strlen
	leaq	10(%rbp,%rax), %rdi
	callq	malloc
	movq	%rax, %rbp
	movl	$47, %esi
	movq	%r15, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rcx
	cmoveq	%r15, %rcx
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r12, %rdx
	callq	sprintf
	movq	%r15, %rdi
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movq	%rbp, %rsi
	callq	filecopy
	cmpl	$-1, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	je	.LBB1_154
# BB#88:
	movq	%r12, %rdi
	callq	fixperms
	movq	24(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB1_90
# BB#89:
	movl	16(%rbp), %esi
	movl	20(%rbp), %edx
	movq	%r12, %rdi
	callq	chown
	movl	16(%rbp), %esi
	movl	20(%rbp), %edx
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	chown
.LBB1_90:
	movl	$1, (%rsp)
	movq	%r12, 112(%rsp)         # 8-byte Spill
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%r14, %rcx
	movq	88(%rsp), %r8           # 8-byte Reload
	movl	%r13d, %r9d
	callq	treewalk
	movl	%eax, %r12d
	cmpl	$1, %r12d
	jne	.LBB1_123
# BB#91:
	movl	$.L.str.108, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	logg
	movl	$.L.str.57, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB1_103
# BB#92:
	movq	%r15, %rdi
	callq	unlink
	testl	%eax, %eax
	je	.LBB1_122
# BB#93:
	movl	$.L.str.58, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	logg
	incl	info+16(%rip)
	jmp	.LBB1_123
.LBB1_94:
	xorl	%r12d, %r12d
	movl	$.L.str.103, %edi
.LBB1_95:                               # %.loopexit104
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	logg
.LBB1_96:                               # %.loopexit104
	movl	%r12d, %eax
	addq	$408, %rsp              # imm = 0x198
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_97:
	movl	$.L.str.56, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_100
# BB#98:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movl	$.L.str.114, %eax
	movd	%rax, %xmm0
	movl	$.L.str.1, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 32(%rsp)
	movl	$.L.str.116, %eax
	movd	%rax, %xmm0
	movl	$.L.str.115, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 48(%rsp)
	movq	%r15, 64(%rsp)
	movl	$.L.str.1, %esi
	movq	%r14, %rdi
	callq	opt_arg
	testq	%rax, %rax
	jne	.LBB1_108
# BB#99:
	leaq	32(%rsp), %rsi
	movl	$.L.str.1, %edi
	jmp	.LBB1_109
.LBB1_100:
	movl	$.L.str.62, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_106
# BB#101:
	movq	$0, 64(%rsp)
	movl	$.L.str.114, %eax
	movd	%rax, %xmm0
	movl	$.L.str.2, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 32(%rsp)
	movq	$.L.str.116, 48(%rsp)
	movq	%r15, 56(%rsp)
	movl	$.L.str.2, %esi
	movq	%r14, %rdi
	callq	opt_arg
	testq	%rax, %rax
	jne	.LBB1_108
# BB#102:
	leaq	32(%rsp), %rsi
	movl	$.L.str.2, %edi
	jmp	.LBB1_109
.LBB1_103:
	movl	$.L.str.60, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB1_105
# BB#104:
	movl	$.L.str.61, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB1_123
.LBB1_105:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	move_infected
	jmp	.LBB1_123
.LBB1_106:
	movl	$.L.str.63, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_124
# BB#107:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movl	$.L.str.117, %eax
	movd	%rax, %xmm0
	movl	$.L.str.3, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 32(%rsp)
	movl	$.L.str.119, %eax
	movd	%rax, %xmm0
	movl	$.L.str.118, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 48(%rsp)
	movq	%r15, 64(%rsp)
	movl	$.L.str.3, %esi
	movq	%r14, %rdi
	callq	opt_arg
	testq	%rax, %rax
	je	.LBB1_126
.LBB1_108:
	leaq	32(%rsp), %rsi
	movq	%rax, %rdi
.LBB1_109:
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movq	%r14, %r8
	callq	clamav_unpack
	movl	%eax, %ebx
.LBB1_110:
	movq	%rbp, %rdi
	callq	fixperms
	testl	%ebx, %ebx
	movq	%rbx, %rax
	jne	.LBB1_112
.LBB1_111:
	movq	%rbp, %rbx
	movzwl	recursion(%rip), %ebp
	movw	$1, recursion(%rip)
	movl	$1, (%rsp)
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	movq	88(%rsp), %r8           # 8-byte Reload
	movl	%r13d, %r9d
	callq	treewalk
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movw	%bp, recursion(%rip)
	movq	%rbx, %rbp
.LBB1_112:
	movq	%rax, %rbx
	movl	$.L.str.127, %esi
	movq	%r14, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB1_114
# BB#113:
	movq	%rbp, %rdi
	callq	clamav_rmdirs
.LBB1_114:
	movq	%rbp, %rdi
	callq	free
	movq	%rbx, %rsi
	leal	3(%rsi), %eax
	xorl	%r12d, %r12d
	cmpl	$4, %eax
	ja	.LBB1_117
# BB#115:
	jmpq	*.LJTI1_1(,%rax,8)
.LBB1_116:
	movl	$.L.str.129, %edi
	xorl	%eax, %eax
	callq	logg
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	jmp	.LBB1_119
.LBB1_117:
	movl	$.L.str.131, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	logg
	jmp	.LBB1_96
.LBB1_118:
	xorl	%ecx, %ecx
	movl	$1, %r8d
.LBB1_119:
	movq	%r15, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB1_59
.LBB1_120:
	movl	$.L.str.130, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	logg
	cmpw	$0, bell(%rip)
	je	.LBB1_60
# BB#121:
	movq	stderr(%rip), %rsi
	movl	$7, %edi
	callq	fputc
	jmp	.LBB1_60
.LBB1_122:
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	logg
.LBB1_123:
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdi
	callq	clamav_rmdirs
	movq	%rbx, %rdi
	callq	free
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	free
	jmp	.LBB1_96
.LBB1_124:
	movl	$.L.str.64, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_127
# BB#125:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movl	$.L.str.111, %eax
	movd	%rax, %xmm0
	movl	$.L.str, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 32(%rsp)
	movl	$.L.str.113, %eax
	movd	%rax, %xmm0
	movl	$.L.str.112, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 48(%rsp)
	movq	%r15, 64(%rsp)
	movl	$.L.str.4, %esi
	jmp	.LBB1_70
.LBB1_126:
	leaq	32(%rsp), %rsi
	movl	$.L.str.3, %edi
	jmp	.LBB1_109
.LBB1_127:
	movl	$.L.str.65, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_130
# BB#128:
	movaps	.Lscancompressed.args+16(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movdqa	.Lscancompressed.args(%rip), %xmm0
	movdqa	%xmm0, 32(%rsp)
	movq	%r15, 48(%rsp)
	movl	$.L.str.5, %esi
	movq	%r14, %rdi
	callq	opt_arg
	testq	%rax, %rax
	jne	.LBB1_108
# BB#129:
	leaq	32(%rsp), %rsi
	movl	$.L.str.5, %edi
	jmp	.LBB1_109
.LBB1_130:
	movl	$.L.str.66, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_134
# BB#131:
	movaps	.Lscancompressed.args.122+16(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movdqa	.Lscancompressed.args.122(%rip), %xmm0
	movdqa	%xmm0, 32(%rsp)
	movq	%r15, 48(%rsp)
	movl	$.L.str.6, %esi
	movq	%r14, %rdi
	callq	opt_arg
	testq	%rax, %rax
	jne	.LBB1_136
# BB#132:
	leaq	32(%rsp), %rsi
.LBB1_133:
	movl	$.L.str.6, %edi
	jmp	.LBB1_138
.LBB1_134:
	movl	$.L.str.67, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_139
# BB#135:
	movaps	.Lscancompressed.args.124+16(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movdqa	.Lscancompressed.args.124(%rip), %xmm0
	movdqa	%xmm0, 32(%rsp)
	movq	%r15, 48(%rsp)
	movl	$.L.str.8, %esi
	movq	%r14, %rdi
	callq	opt_arg
	testq	%rax, %rax
	je	.LBB1_143
.LBB1_136:
	leaq	32(%rsp), %rsi
.LBB1_137:
	movq	%rax, %rdi
.LBB1_138:
	movq	%rbp, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%r14, %r8
	callq	clamav_unpack
	movl	%eax, %ebx
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_110
.LBB1_139:
	movl	$.L.str.68, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB1_141
# BB#140:
	movl	$.L.str.69, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB1_144
.LBB1_141:
	movaps	.Lscancompressed.args.126+16(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movdqa	.Lscancompressed.args.126(%rip), %xmm0
	movdqa	%xmm0, 32(%rsp)
	movq	%r15, 48(%rsp)
	movl	$.L.str.7, %esi
	movq	%r14, %rdi
	callq	opt_arg
	leaq	32(%rsp), %rsi
	testq	%rax, %rax
	jne	.LBB1_137
	jmp	.LBB1_133
.LBB1_143:
	leaq	32(%rsp), %rsi
	movl	$.L.str.123, %edi
	jmp	.LBB1_138
.LBB1_144:                              # %.thread.i
	movq	%rbp, %rdi
	callq	fixperms
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_111
.LBB1_145:
	movl	$.L.str.71, %edi
	jmp	.LBB1_150
.LBB1_146:
	movl	$.L.str.70, %edi
	xorl	%eax, %eax
	callq	logg
	movl	$60, %edi
	callq	exit
.LBB1_147:
	movl	$.L.str.109, %edi
	xorl	%eax, %eax
	callq	logg
	movl	$64, %edi
	callq	exit
.LBB1_148:
	movl	$.L.str.110, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	jmp	.LBB1_153
.LBB1_149:
	movl	$.L.str.128, %edi
.LBB1_150:
	xorl	%eax, %eax
	callq	logg
	movl	$61, %edi
	callq	exit
.LBB1_151:
	movl	$.L.str.104, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	logg
	movl	$64, %edi
	callq	exit
.LBB1_152:
	movl	$.L.str.105, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
.LBB1_153:
	callq	logg
	movl	$63, %edi
	callq	exit
.LBB1_154:
	movl	$.L.str.106, %edi
	xorl	%eax, %eax
	callq	logg
	movl	$.L.str.107, %edi
	callq	perror
	movl	$58, %edi
	callq	exit
.Lfunc_end1:
	.size	scanfile, .Lfunc_end1-scanfile
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_145
	.quad	.LBB1_146
	.quad	.LBB1_55
	.quad	.LBB1_63
.LJTI1_1:
	.quad	.LBB1_96
	.quad	.LBB1_116
	.quad	.LBB1_149
	.quad	.LBB1_118
	.quad	.LBB1_120

	.text
	.p2align	4, 0x90
	.type	checkfile,@function
checkfile:                              # @checkfile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	movl	%ecx, %r14d
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movl	$47, %esi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%rbx, %rsi
	movl	$.L.str.85, %edi
	xorl	%eax, %eax
	callq	logg
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB2_1
# BB#2:
	movq	%rsp, %rsi
	movl	$info+24, %edx
	movl	%ebp, %edi
	movq	%r13, %rcx
	movq	%r12, %r8
	movl	%r14d, %r9d
	callq	cl_scandesc
	movl	%eax, %r14d
	cmpl	$1, %r14d
	jne	.LBB2_5
# BB#3:
	movq	(%rsp), %rdx
	movl	$.L.str.87, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	logg
	incl	info+12(%rip)
	cmpw	$0, bell(%rip)
	je	.LBB2_11
# BB#4:
	movq	stderr(%rip), %rsi
	movl	$7, %edi
	callq	fputc
	jmp	.LBB2_11
.LBB2_1:
	movl	$.L.str.86, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	logg
	movl	$54, %r14d
	jmp	.LBB2_12
.LBB2_5:
	movzwl	printinfected(%rip), %eax
	testl	%r14d, %r14d
	je	.LBB2_6
# BB#9:
	testw	%ax, %ax
	jne	.LBB2_11
# BB#10:
	movl	%r14d, %edi
	callq	cl_strerror
	movq	%rax, %rcx
	movl	$.L.str.89, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	callq	logg
	jmp	.LBB2_11
.LBB2_6:
	testw	%r15w, %r15w
	je	.LBB2_11
# BB#7:
	testw	%ax, %ax
	jne	.LBB2_11
# BB#8:
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%rbx, %rsi
	movl	$.L.str.88, %edi
	xorl	%eax, %eax
	callq	mprintf
.LBB2_11:
	movl	%ebp, %edi
	callq	close
.LBB2_12:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	checkfile, .Lfunc_end2-checkfile
	.cfi_endproc

	.p2align	4, 0x90
	.type	move_infected,@function
move_infected:                          # @move_infected
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$328, %rsp              # imm = 0x148
.Lcfi45:
	.cfi_def_cfa_offset 384
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$.L.str.60, %esi
	movq	%rbx, %rdi
	callq	opt_check
	movl	%eax, %r15d
	testl	%r15d, %r15d
	je	.LBB3_2
# BB#1:
	movl	$.L.str.60, %esi
	jmp	.LBB3_3
.LBB3_2:
	movl	$.L.str.61, %esi
.LBB3_3:
	movq	%rbx, %rdi
	callq	opt_arg
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_15
# BB#4:                                 # %.thread
	movl	$3, %esi
	movq	%rbp, %rdi
	callq	access
	cmpl	$-1, %eax
	je	.LBB3_16
# BB#5:
	movl	$47, %esi
	movq	%r14, %rdi
	callq	strrchr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	cmoveq	%r14, %rbx
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %r12
	movq	%rbx, %rdi
	callq	strlen
	leal	(%r12,%rax), %eax
	shlq	$32, %rax
	movabsq	$30064771072, %rdi      # imm = 0x700000000
	addq	%rax, %rdi
	sarq	$32, %rdi
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB3_31
# BB#6:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	cli_strrcpy
	testq	%rax, %rax
	je	.LBB3_17
# BB#7:
	movq	%r12, %rdi
	callq	strlen
	movw	$47, (%r12,%rax)
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	strcat
	testq	%rax, %rax
	je	.LBB3_18
# BB#8:
	leaq	40(%rsp), %rdx
	movl	$1, %edi
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%r14, %rsi
	callq	__xstat
	leaq	184(%rsp), %rdx
	movl	$1, %edi
	movq	%r12, %rsi
	callq	__xstat
	testl	%eax, %eax
	je	.LBB3_21
.LBB3_9:                                # %.loopexit
	testl	%r15d, %r15d
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB3_11
# BB#10:
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	rename
	cmpl	$-1, %eax
	jne	.LBB3_20
.LBB3_11:
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	filecopy
	cmpl	$-1, %eax
	je	.LBB3_27
# BB#12:
	movl	64(%rsp), %esi
	movq	%r12, %rdi
	callq	chmod
	movl	68(%rsp), %esi
	movl	72(%rsp), %edx
	movq	%r12, %rdi
	callq	chown
	movq	112(%rsp), %rax
	movq	%rax, 24(%rsp)
	movq	128(%rsp), %rax
	movq	%rax, 32(%rsp)
	leaq	24(%rsp), %rsi
	movq	%r12, %rdi
	callq	utime
	testl	%r15d, %r15d
	je	.LBB3_20
# BB#13:
	movq	%rbp, %rdi
	callq	unlink
	testl	%eax, %eax
	je	.LBB3_20
# BB#14:
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.99, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	movq	%rcx, %rdx
	callq	logg
	incl	info+16(%rip)
	jmp	.LBB3_29
.LBB3_15:
	movl	$.L.str.90, %edi
	xorl	%eax, %eax
	callq	logg
	incl	info+20(%rip)
	jmp	.LBB3_30
.LBB3_16:
	testl	%r15d, %r15d
	movl	$.L.str.60, %eax
	movl	$.L.str.61, %ebx
	cmovneq	%rax, %rbx
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %r8
	movl	$.L.str.91, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%rbp, %rcx
	callq	logg
	incl	info+20(%rip)
	jmp	.LBB3_30
.LBB3_17:
	movl	$.L.str.93, %edi
	jmp	.LBB3_19
.LBB3_18:
	movl	$.L.str.95, %edi
.LBB3_19:
	xorl	%eax, %eax
	callq	logg
.LBB3_28:
	incl	info+20(%rip)
	jmp	.LBB3_29
.LBB3_20:
	testl	%r15d, %r15d
	movl	$.L.str.101, %eax
	movl	$.L.str.102, %edx
	cmovneq	%rax, %rdx
	movl	$.L.str.100, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	movq	%r12, %rcx
	callq	logg
.LBB3_29:
	movq	%r12, %rdi
	callq	free
.LBB3_30:
	addq	$328, %rsp              # imm = 0x148
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_21:
	movq	40(%rsp), %rax
	cmpq	184(%rsp), %rax
	jne	.LBB3_24
# BB#22:
	movq	48(%rsp), %rax
	cmpq	192(%rsp), %rax
	jne	.LBB3_24
# BB#23:
	movl	$.L.str.96, %edi
	xorl	%eax, %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	logg
	jmp	.LBB3_28
.LBB3_24:
	movq	%r12, %rdi
	callq	strlen
	movslq	%eax, %rbp
	leaq	19(%rsp), %rbx
	leaq	184(%rsp), %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_25:                               # =>This Inner Loop Header: Depth=1
	movl	%r14d, %edx
	movb	$0, (%r12,%rbp)
	leal	1(%rdx), %r14d
	movl	$.L.str.97, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	sprintf
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	strcat
	movl	$1, %edi
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	__xstat
	cmpl	$999, %r14d             # imm = 0x3E7
	jg	.LBB3_9
# BB#26:                                #   in Loop: Header=BB3_25 Depth=1
	testl	%eax, %eax
	je	.LBB3_25
	jmp	.LBB3_9
.LBB3_27:
	testl	%r15d, %r15d
	movl	$.L.str.60, %eax
	movl	$.L.str.61, %ebx
	cmovneq	%rax, %rbx
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %r8
	movl	$.L.str.98, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%r12, %rcx
	callq	logg
	jmp	.LBB3_28
.LBB3_31:
	movl	$.L.str.92, %edi
	xorl	%eax, %eax
	callq	logg
	movl	$71, %edi
	callq	exit
.Lfunc_end3:
	.size	move_infected, .Lfunc_end3-move_infected
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.zero	16
	.text
	.p2align	4, 0x90
	.type	clamav_unpack,@function
clamav_unpack:                          # @clamav_unpack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 112
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movq	%r8, %rbp
	movq	%rcx, %r13
	movq	%rdx, %r12
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movl	$.L.str.24, %esi
	movq	%rbp, %rdi
	callq	opt_check
	xorl	%r14d, %r14d
	testl	%eax, %eax
	movl	$0, %r15d
	je	.LBB4_2
# BB#1:
	movl	$.L.str.24, %esi
	movq	%rbp, %rdi
	callq	opt_arg
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	strtol
	movq	%rax, %r15
.LBB4_2:
	movl	$.L.str.23, %esi
	movq	%rbp, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB4_6
# BB#3:
	movl	$.L.str.23, %esi
	movq	%rbp, %rdi
	callq	opt_arg
	movq	%rax, %rbp
	callq	__ctype_tolower_loc
	movq	(%rax), %rbx
	movq	%rbp, %rdi
	callq	strlen
	movsbq	-1(%rbp,%rax), %rcx
	cmpl	$109, (%rbx,%rcx,4)
	jne	.LBB4_5
# BB#4:
	movl	$1, %esi
	movq	%rax, %rdi
	callq	calloc
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	strlen
	leaq	-1(%rax), %rdx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	strncpy
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %r14
	shll	$10, %r14d
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB4_6
.LBB4_5:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strtol
	movq	%rax, %r14
.LBB4_6:
	movl	$-1, %ebx
	callq	fork
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB4_49
# BB#7:
	testl	%ebp, %ebp
	je	.LBB4_8
# BB#24:
	movl	%r14d, %eax
	leaq	12(%rsp), %rsi
	orl	%r15d, %eax
	je	.LBB4_41
# BB#25:                                # %.preheader
	movl	$1, %edx
	movl	%ebp, %edi
	callq	waitpid
	testl	%eax, %eax
	jne	.LBB4_42
# BB#26:                                # %.lr.ph
	testl	%r15d, %r15d
	movl	%r14d, %r13d
	je	.LBB4_32
# BB#27:                                # %.lr.ph.split.preheader
	testl	%r14d, %r14d
	setne	40(%rsp)                # 1-byte Folded Spill
	leaq	16(%rsp), %r14
	leaq	12(%rsp), %rbx
	.p2align	4, 0x90
.LBB4_28:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	du
	testl	%eax, %eax
	jne	.LBB4_31
# BB#29:                                #   in Loop: Header=BB4_28 Depth=1
	movl	16(%rsp), %esi
	movq	24(%rsp), %rdx
	cmpq	%rdx, %r13
	sbbb	%al, %al
	cmpl	%r15d, %esi
	ja	.LBB4_40
# BB#30:                                #   in Loop: Header=BB4_28 Depth=1
	andb	40(%rsp), %al           # 1-byte Folded Reload
	je	.LBB4_31
.LBB4_40:                               # %._crit_edge
                                        #   in Loop: Header=BB4_28 Depth=1
	movl	$.L.str.138, %edi
	xorl	%eax, %eax
	callq	logg
	movl	$9, %esi
	movl	%ebp, %edi
	callq	kill
	.p2align	4, 0x90
.LBB4_31:                               # %.backedge
                                        #   in Loop: Header=BB4_28 Depth=1
	movl	$1, %edx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	waitpid
	testl	%eax, %eax
	je	.LBB4_28
	jmp	.LBB4_42
.LBB4_41:
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	waitpid
.LBB4_42:                               # %.loopexit
	movl	12(%rsp), %eax
	movl	%eax, %esi
	andl	$127, %esi
	movl	%esi, %ecx
	shll	$24, %ecx
	addl	$16777216, %ecx         # imm = 0x1000000
	sarl	$25, %ecx
	testl	%ecx, %ecx
	jle	.LBB4_43
# BB#44:
	andb	$127, %al
	cmpb	$6, %al
	je	.LBB4_47
# BB#45:
	cmpb	$9, %al
	jne	.LBB4_48
# BB#46:
	xorl	%ebx, %ebx
	movl	$.L.str.139, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	logg
	jmp	.LBB4_49
.LBB4_43:
	xorl	%ebx, %ebx
	jmp	.LBB4_49
.LBB4_47:
	movl	$.L.str.140, %edi
	xorl	%eax, %eax
	movq	48(%rsp), %rsi          # 8-byte Reload
	callq	logg
	movl	$-2, %ebx
	jmp	.LBB4_49
.LBB4_48:
	movl	$.L.str.141, %edi
	xorl	%eax, %eax
	callq	logg
	movl	$-3, %ebx
.LBB4_49:
	movl	%ebx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_32:                               # %.lr.ph.split.us
	testl	%r14d, %r14d
	je	.LBB4_38
# BB#33:
	leaq	16(%rsp), %r14
	leaq	12(%rsp), %rbx
	.p2align	4, 0x90
.LBB4_34:                               # %.lr.ph.split.us.split
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	du
	testl	%eax, %eax
	jne	.LBB4_37
# BB#35:                                # %.lr.ph.split.us.split
                                        #   in Loop: Header=BB4_34 Depth=1
	movq	24(%rsp), %rdx
	cmpq	%r13, %rdx
	jbe	.LBB4_37
# BB#36:                                #   in Loop: Header=BB4_34 Depth=1
	movl	16(%rsp), %esi
	movl	$.L.str.138, %edi
	xorl	%eax, %eax
	callq	logg
	movl	$9, %esi
	movl	%ebp, %edi
	callq	kill
.LBB4_37:                               # %.backedge.us
                                        #   in Loop: Header=BB4_34 Depth=1
	movl	$1, %edx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	waitpid
	testl	%eax, %eax
	je	.LBB4_34
	jmp	.LBB4_42
.LBB4_38:                               # %.lr.ph.split.us.split.us.preheader
	leaq	16(%rsp), %r14
	leaq	12(%rsp), %rbx
	.p2align	4, 0x90
.LBB4_39:                               # %.lr.ph.split.us.split.us
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	du
	movl	$1, %edx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	waitpid
	testl	%eax, %eax
	je	.LBB4_39
	jmp	.LBB4_42
.LBB4_8:
	callq	geteuid
	testq	%r13, %r13
	je	.LBB4_15
# BB#9:
	testl	%eax, %eax
	jne	.LBB4_15
# BB#10:
	movl	20(%r13), %edi
	callq	setgid
	testl	%eax, %eax
	jne	.LBB4_11
# BB#13:
	movl	16(%r13), %edi
	callq	setuid
	testl	%eax, %eax
	jne	.LBB4_14
.LBB4_15:
	movq	%r12, %rdi
	callq	chdir
	cmpl	$-1, %eax
	jne	.LBB4_16
# BB#50:
	movq	stderr(%rip), %rdi
	movl	$.L.str.134, %esi
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB4_16:
	cmpw	$0, printinfected(%rip)
	je	.LBB4_20
# BB#17:
	movl	$.L.str.135, %edi
	movl	$1, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	jne	.LBB4_19
# BB#18:
	movl	$.L.str.136, %edi
	xorl	%eax, %eax
	callq	logg
	movw	$0, printinfected(%rip)
	jmp	.LBB4_20
.LBB4_11:
	movq	stderr(%rip), %rdi
	movl	20(%r13), %edx
	movl	$.L.str.132, %esi
	jmp	.LBB4_12
.LBB4_14:
	movq	stderr(%rip), %rdi
	movl	16(%r13), %edx
	movl	$.L.str.133, %esi
.LBB4_12:
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB4_19:
	movl	$1, %esi
	movl	%ebx, %edi
	callq	dup2
	movl	$2, %esi
	movl	%ebx, %edi
	callq	dup2
.LBB4_20:
	movl	$47, %esi
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	strchr
	testq	%rax, %rax
	jne	.LBB4_21
# BB#22:
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	execvp
	jmp	.LBB4_23
.LBB4_21:
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	execv
.LBB4_23:
	movl	$.L.str.137, %edi
	callq	perror
	callq	abort
.Lfunc_end4:
	.size	clamav_unpack, .Lfunc_end4-clamav_unpack
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"unzip"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"unrar"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"arj"
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"unzoo"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"jar"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"lha"
	.size	.L.str.5, 4

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"tar"
	.size	.L.str.6, 4

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"tgz"
	.size	.L.str.7, 4

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"deb"
	.size	.L.str.8, 4

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"clamav"
	.size	.L.str.9, 7

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"!Can't get information about user clamav (required to run external unpackers)\n"
	.size	.L.str.10, 79

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"no-phishing-sigs"
	.size	.L.str.11, 17

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"no-phishing-scan-urls"
	.size	.L.str.12, 22

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"no-phishing-restrictedscan"
	.size	.L.str.13, 27

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"phishing-ssl"
	.size	.L.str.14, 13

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"phishing-cloak"
	.size	.L.str.15, 15

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"dev-ac-only"
	.size	.L.str.16, 12

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"dev-ac-depth"
	.size	.L.str.17, 13

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"detect-pua"
	.size	.L.str.18, 11

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"database"
	.size	.L.str.19, 9

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"!%s\n"
	.size	.L.str.20, 5

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"!Can't initialize the virus database\n"
	.size	.L.str.21, 38

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"!Database initialization error: %s\n"
	.size	.L.str.22, 36

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"max-space"
	.size	.L.str.23, 10

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"max-files"
	.size	.L.str.24, 10

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"max-recursion"
	.size	.L.str.25, 14

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"max-mail-recursion"
	.size	.L.str.26, 19

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"max-ratio"
	.size	.L.str.27, 10

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"disable-archive"
	.size	.L.str.28, 16

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"no-archive"
	.size	.L.str.29, 11

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"detect-broken"
	.size	.L.str.30, 14

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"block-encrypted"
	.size	.L.str.31, 16

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"block-max"
	.size	.L.str.32, 10

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"no-pe"
	.size	.L.str.33, 6

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"no-elf"
	.size	.L.str.34, 7

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"no-ole2"
	.size	.L.str.35, 8

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"no-pdf"
	.size	.L.str.36, 7

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"no-html"
	.size	.L.str.37, 8

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"no-mail"
	.size	.L.str.38, 8

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"mail-follow-urls"
	.size	.L.str.39, 17

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"no-algorithmic"
	.size	.L.str.40, 15

	.type	procdev,@object         # @procdev
	.comm	procdev,8,8
	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"/proc"
	.size	.L.str.41, 6

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"!Can't get absolute pathname of current working directory\n"
	.size	.L.str.42, 59

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"-"
	.size	.L.str.43, 2

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"\t"
	.size	.L.str.44, 2

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"^Can't access file %s\n"
	.size	.L.str.45, 23

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"%s/%s"
	.size	.L.str.46, 6

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"*Full path: %s\n"
	.size	.L.str.47, 16

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"!Not supported file type (%s)\n"
	.size	.L.str.48, 31

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"%s: Excluded (/proc)\n"
	.size	.L.str.49, 22

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"exclude"
	.size	.L.str.50, 8

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"include"
	.size	.L.str.51, 8

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"%s: Excluded\n"
	.size	.L.str.52, 14

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"%s: Empty file\n"
	.size	.L.str.53, 16

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"%s: Access denied\n"
	.size	.L.str.54, 19

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	".zip"
	.size	.L.str.55, 5

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	".rar"
	.size	.L.str.56, 5

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"remove"
	.size	.L.str.57, 7

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"^%s: Can't remove\n"
	.size	.L.str.58, 19

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"%s: Removed\n"
	.size	.L.str.59, 13

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"move"
	.size	.L.str.60, 5

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"copy"
	.size	.L.str.61, 5

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	".arj"
	.size	.L.str.62, 5

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	".zoo"
	.size	.L.str.63, 5

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	".jar"
	.size	.L.str.64, 5

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	".lzh"
	.size	.L.str.65, 5

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	".tar"
	.size	.L.str.66, 5

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	".deb"
	.size	.L.str.67, 5

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	".tar.gz"
	.size	.L.str.68, 8

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	".tgz"
	.size	.L.str.69, 5

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"^Can't get information about user clamav\n"
	.size	.L.str.70, 42

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"^Can't fork\n"
	.size	.L.str.71, 13

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"^%s: Access denied to archive\n"
	.size	.L.str.72, 31

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"^%s: Archive too big\n"
	.size	.L.str.73, 22

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"TMPDIR"
	.size	.L.str.74, 7

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"/tmp"
	.size	.L.str.75, 5

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"!Can't write to temporary directory\n"
	.size	.L.str.76, 37

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"wb"
	.size	.L.str.77, 3

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"!Can't open %s for writing\n"
	.size	.L.str.78, 28

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"!Can't write to %s\n"
	.size	.L.str.79, 20

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"*Checking %s\n"
	.size	.L.str.80, 14

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"stdin: %s FOUND\n"
	.size	.L.str.81, 17

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"stdin: OK\n"
	.size	.L.str.83, 11

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"stdin: %s\n"
	.size	.L.str.84, 11

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"*Scanning %s\n"
	.size	.L.str.85, 14

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"^Can't open file %s\n"
	.size	.L.str.86, 21

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"%s: %s FOUND\n"
	.size	.L.str.87, 14

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"%s: OK\n"
	.size	.L.str.88, 8

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"%s: %s\n"
	.size	.L.str.89, 8

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"!opt_arg() returned NULL\n"
	.size	.L.str.90, 26

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"!Can't %s file '%s': cannot write to '%s': %s\n"
	.size	.L.str.91, 47

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"!malloc() failed\n"
	.size	.L.str.92, 18

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"!cli_strrcpy() returned NULL\n"
	.size	.L.str.93, 30

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"!strcat() returned NULL\n"
	.size	.L.str.95, 25

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"File excluded '%s'\n"
	.size	.L.str.96, 20

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	".%03d"
	.size	.L.str.97, 6

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"!Can't %s '%s' to '%s': %s\n"
	.size	.L.str.98, 28

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"!Can't unlink '%s': %s\n"
	.size	.L.str.99, 24

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"%s: %s to '%s'\n"
	.size	.L.str.100, 16

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"moved"
	.size	.L.str.101, 6

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"copied"
	.size	.L.str.102, 7

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"^Suspect archive %s (not a regular file)\n"
	.size	.L.str.103, 42

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"!Can't write to the temporary directory %s\n"
	.size	.L.str.104, 44

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"^Can't create the temporary directory %s\n"
	.size	.L.str.105, 42

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"!I/O error\n"
	.size	.L.str.106, 12

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"copyfile()"
	.size	.L.str.107, 11

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"(Real infected archive: %s)\n"
	.size	.L.str.108, 29

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"!Can't write to the temporary directory\n"
	.size	.L.str.109, 41

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"!Can't create the temporary directory %s\n"
	.size	.L.str.110, 42

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"-P"
	.size	.L.str.111, 3

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"clam"
	.size	.L.str.112, 5

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"-o"
	.size	.L.str.113, 3

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"x"
	.size	.L.str.114, 2

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"-p-"
	.size	.L.str.115, 4

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"-y"
	.size	.L.str.116, 3

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"-x"
	.size	.L.str.117, 3

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"-j"
	.size	.L.str.118, 3

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"./"
	.size	.L.str.119, 3

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"xf"
	.size	.L.str.120, 3

	.type	.Lscancompressed.args,@object # @scancompressed.args
	.section	.rodata,"a",@progbits
	.p2align	4
.Lscancompressed.args:
	.quad	.L.str.5
	.quad	.L.str.120
	.quad	0
	.quad	0
	.size	.Lscancompressed.args, 32

	.type	.L.str.121,@object      # @.str.121
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.121:
	.asciz	"-xpvf"
	.size	.L.str.121, 6

	.type	.Lscancompressed.args.122,@object # @scancompressed.args.122
	.section	.rodata,"a",@progbits
	.p2align	4
.Lscancompressed.args.122:
	.quad	.L.str.6
	.quad	.L.str.121
	.quad	0
	.quad	0
	.size	.Lscancompressed.args.122, 32

	.type	.L.str.123,@object      # @.str.123
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.123:
	.asciz	"ar"
	.size	.L.str.123, 3

	.type	.Lscancompressed.args.124,@object # @scancompressed.args.124
	.section	.rodata,"a",@progbits
	.p2align	4
.Lscancompressed.args.124:
	.quad	.L.str.123
	.quad	.L.str.114
	.quad	0
	.quad	0
	.size	.Lscancompressed.args.124, 32

	.type	.L.str.125,@object      # @.str.125
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.125:
	.asciz	"-zxpvf"
	.size	.L.str.125, 7

	.type	.Lscancompressed.args.126,@object # @scancompressed.args.126
	.section	.rodata,"a",@progbits
	.p2align	4
.Lscancompressed.args.126:
	.quad	.L.str.6
	.quad	.L.str.125
	.quad	0
	.quad	0
	.size	.Lscancompressed.args.126, 32

	.type	.L.str.127,@object      # @.str.127
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.127:
	.asciz	"leave-temps"
	.size	.L.str.127, 12

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"!Can't fork()\n"
	.size	.L.str.128, 15

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"^Can't execute some unpacker. Check paths and permissions on the temporary directory\n"
	.size	.L.str.129, 86

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"%s: Infected.Archive FOUND\n"
	.size	.L.str.130, 28

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"^Strange value (%d) returned in scancompressed()\n"
	.size	.L.str.131, 50

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"ERROR: setgid(%d) failed\n"
	.size	.L.str.132, 26

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"ERROR: setuid(%d) failed\n"
	.size	.L.str.133, 26

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"ERROR: chdir(%s) failed\n"
	.size	.L.str.134, 25

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"/dev/null"
	.size	.L.str.135, 10

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"Non fatal error: cannot open /dev/null. Continuing with full output\n"
	.size	.L.str.136, 69

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"execv(p)"
	.size	.L.str.137, 9

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"*n.files: %u, n.space: %lu\n"
	.size	.L.str.138, 28

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"\nUnpacker process %d stopped due to exceeded limits\n"
	.size	.L.str.139, 53

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"^Can't run %s\n"
	.size	.L.str.140, 15

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"^\nUnpacker stopped with external signal %d\n"
	.size	.L.str.141, 44


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
