	.text
	.file	"l3bitstream.bc"
	.globl	putMyBits
	.p2align	4, 0x90
	.type	putMyBits,@function
putMyBits:                              # @putMyBits
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	movl	%edi, %ecx
	movq	bs(%rip), %rdi
	movl	%ecx, %esi
	movl	%eax, %edx
	jmp	putbits                 # TAILCALL
.Lfunc_end0:
	.size	putMyBits, .Lfunc_end0-putMyBits
	.cfi_endproc

	.globl	III_format_bitstream
	.p2align	4, 0x90
	.type	III_format_bitstream,@function
III_format_bitstream:                   # @III_format_bitstream
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 160
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rcx, %r14
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movl	%esi, 60(%rsp)          # 4-byte Spill
	movq	%rdi, %r15
	movq	%r9, bs(%rip)
	cmpq	$0, frameData(%rip)
	jne	.LBB1_2
# BB#1:
	movl	$1, %edi
	movl	$184, %esi
	callq	calloc
	movq	%rax, frameData(%rip)
.LBB1_2:
	cmpq	$0, frameResults(%rip)
	jne	.LBB1_4
# BB#3:
	movl	$1, %edi
	movl	$12, %esi
	callq	calloc
	movq	%rax, frameResults(%rip)
.LBB1_4:
	cmpl	$0, PartHoldersInitialized(%rip)
	jne	.LBB1_6
# BB#5:                                 # %.preheader71.preheader117
	movl	$14, %edi
	callq	BF_newPartHolder
	movq	%rax, headerPH(%rip)
	movl	$12, %edi
	callq	BF_newPartHolder
	movq	%rax, frameSIPH(%rip)
	movl	$8, %edi
	callq	BF_newPartHolder
	movq	%rax, channelSIPH(%rip)
	movl	$8, %edi
	callq	BF_newPartHolder
	movq	%rax, channelSIPH+8(%rip)
	movl	$32, %edi
	callq	BF_newPartHolder
	movq	%rax, spectrumSIPH(%rip)
	movl	$64, %edi
	callq	BF_newPartHolder
	movq	%rax, scaleFactorsPH(%rip)
	movl	$576, %edi              # imm = 0x240
	callq	BF_newPartHolder
	movq	%rax, codedDataPH(%rip)
	movl	$4, %edi
	callq	BF_newPartHolder
	movq	%rax, userSpectrumPH(%rip)
	movl	$32, %edi
	callq	BF_newPartHolder
	movq	%rax, spectrumSIPH+8(%rip)
	movl	$64, %edi
	callq	BF_newPartHolder
	movq	%rax, scaleFactorsPH+8(%rip)
	movl	$576, %edi              # imm = 0x240
	callq	BF_newPartHolder
	movq	%rax, codedDataPH+8(%rip)
	movl	$4, %edi
	callq	BF_newPartHolder
	movq	%rax, userSpectrumPH+8(%rip)
	movl	$32, %edi
	callq	BF_newPartHolder
	movq	%rax, spectrumSIPH+16(%rip)
	movl	$64, %edi
	callq	BF_newPartHolder
	movq	%rax, scaleFactorsPH+16(%rip)
	movl	$576, %edi              # imm = 0x240
	callq	BF_newPartHolder
	movq	%rax, codedDataPH+16(%rip)
	movl	$4, %edi
	callq	BF_newPartHolder
	movq	%rax, userSpectrumPH+16(%rip)
	movl	$32, %edi
	callq	BF_newPartHolder
	movq	%rax, spectrumSIPH+24(%rip)
	movl	$64, %edi
	callq	BF_newPartHolder
	movq	%rax, scaleFactorsPH+24(%rip)
	movl	$576, %edi              # imm = 0x240
	callq	BF_newPartHolder
	movq	%rax, codedDataPH+24(%rip)
	movl	$4, %edi
	callq	BF_newPartHolder
	movq	%rax, userSpectrumPH+24(%rip)
	movl	$8, %edi
	callq	BF_newPartHolder
	movq	%rax, userFrameDataPH(%rip)
	movl	$1, PartHoldersInitialized(%rip)
.LBB1_6:
	movl	$65535, crc(%rip)       # imm = 0xFFFF
	movq	headerPH(%rip), %rdi
	movq	8(%rdi), %rax
	movl	$0, (%rax)
	movl	$4095, %esi             # imm = 0xFFF
	movl	$12, %edx
	callq	BF_addEntry
	movq	%rax, headerPH(%rip)
	movl	192(%r15), %esi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, headerPH(%rip)
	movl	$1, %esi
	movl	$2, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, headerPH(%rip)
	xorl	%esi, %esi
	cmpl	$0, 60(%r15)
	sete	%sil
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, headerPH(%rip)
	movl	220(%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$4, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, headerPH(%rip)
	movl	224(%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$2, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, headerPH(%rip)
	movl	196(%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, headerPH(%rip)
	movl	68(%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, headerPH(%rip)
	movl	36(%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$2, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, headerPH(%rip)
	movl	228(%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$2, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, headerPH(%rip)
	movl	52(%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, headerPH(%rip)
	movl	56(%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, headerPH(%rip)
	movl	164(%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$2, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, headerPH(%rip)
	movq	frameSIPH(%rip), %rdi
	movq	8(%rdi), %rax
	movl	$0, (%rax)
	movslq	204(%r15), %r9
	testq	%r9, %r9
	jle	.LBB1_7
# BB#8:                                 # %.lr.ph200.i
	leaq	-1(%r9), %r8
	movq	%r9, %rcx
	xorl	%eax, %eax
	andq	$3, %rcx
	je	.LBB1_10
	.p2align	4, 0x90
.LBB1_9:                                # =>This Inner Loop Header: Depth=1
	movq	channelSIPH(,%rax,8), %rdx
	movq	8(%rdx), %rdx
	movl	$0, (%rdx)
	incq	%rax
	cmpq	%rax, %rcx
	jne	.LBB1_9
.LBB1_10:                               # %.prol.loopexit161
	cmpq	$3, %r8
	jb	.LBB1_11
	.p2align	4, 0x90
.LBB1_135:                              # =>This Inner Loop Header: Depth=1
	movq	channelSIPH(,%rax,8), %rcx
	movq	8(%rcx), %rcx
	movl	$0, (%rcx)
	movq	channelSIPH+8(,%rax,8), %rcx
	movq	8(%rcx), %rcx
	movl	$0, (%rcx)
	movq	channelSIPH+16(,%rax,8), %rcx
	movq	8(%rcx), %rcx
	movl	$0, (%rcx)
	movq	channelSIPH+24(,%rax,8), %rcx
	movq	8(%rcx), %rcx
	movl	$0, (%rcx)
	addq	$4, %rax
	cmpq	%rax, %r9
	jne	.LBB1_135
.LBB1_11:                               # %.preheader179.i
	leaq	200(%r15), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movslq	200(%r15), %r10
	testq	%r10, %r10
	jle	.LBB1_21
# BB#12:                                # %.preheader178.us.preheader.i
	movl	$spectrumSIPH, %r11d
	movl	%r9d, %eax
	andl	$3, %eax
	xorl	%ebx, %ebx
	movl	$spectrumSIPH+16, %ecx
	.p2align	4, 0x90
.LBB1_13:                               # %.preheader178.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_16 Depth 2
                                        #     Child Loop BB1_19 Depth 2
	testq	%rax, %rax
	je	.LBB1_14
# BB#15:                                # %.prol.preheader155
                                        #   in Loop: Header=BB1_13 Depth=1
	movq	%r11, %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_16:                               #   Parent Loop BB1_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rbp
	movq	8(%rbp), %rbp
	movl	$0, (%rbp)
	incq	%rdx
	addq	$8, %rsi
	cmpq	%rdx, %rax
	jne	.LBB1_16
	jmp	.LBB1_17
	.p2align	4, 0x90
.LBB1_14:                               #   in Loop: Header=BB1_13 Depth=1
	xorl	%edx, %edx
.LBB1_17:                               # %.prol.loopexit156
                                        #   in Loop: Header=BB1_13 Depth=1
	cmpq	$3, %r8
	jb	.LBB1_20
# BB#18:                                # %.preheader178.us.i.new
                                        #   in Loop: Header=BB1_13 Depth=1
	movq	%r9, %rbp
	subq	%rdx, %rbp
	leaq	(%rcx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB1_19:                               #   Parent Loop BB1_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-16(%rdx), %rsi
	movq	8(%rsi), %rsi
	movl	$0, (%rsi)
	movq	-8(%rdx), %rsi
	movq	8(%rsi), %rsi
	movl	$0, (%rsi)
	movq	(%rdx), %rsi
	movq	8(%rsi), %rsi
	movl	$0, (%rsi)
	movq	8(%rdx), %rsi
	movq	8(%rsi), %rsi
	movl	$0, (%rsi)
	addq	$32, %rdx
	addq	$-4, %rbp
	jne	.LBB1_19
.LBB1_20:                               # %._crit_edge196.us.i
                                        #   in Loop: Header=BB1_13 Depth=1
	incq	%rbx
	addq	$16, %r11
	addq	$16, %rcx
	cmpq	%r10, %rbx
	jne	.LBB1_13
	jmp	.LBB1_21
.LBB1_7:                                # %.preheader179.i.thread
	leaq	200(%r15), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
.LBB1_21:                               # %._crit_edge198.i
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %eax
	shrl	$15, %ecx
	cmpl	$1, 192(%r15)
	movl	(%r14), %esi
	movq	%r14, (%rsp)            # 8-byte Spill
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movl	%esi, %edx
	jne	.LBB1_22
# BB#30:                                # %.lr.ph.i35.preheader.i
	shrl	$8, %edx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	movl	%esi, %ecx
	shrl	$7, %ecx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	movl	%esi, %ecx
	shrl	$6, %ecx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	movl	%esi, %ecx
	shrl	$5, %ecx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	movl	%esi, %ecx
	shrl	$4, %ecx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	movl	%esi, %ecx
	shrl	$3, %ecx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	movl	%esi, %ecx
	shrl	$2, %ecx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	movl	%esi, %ecx
	shrl	%ecx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	xorl	%esi, %edx
	movl	%eax, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dl
	cmovel	%eax, %ecx
	movzwl	%cx, %eax
	movl	%eax, crc(%rip)
	movl	$9, %edx
	callq	BF_addEntry
	movq	%rax, frameSIPH(%rip)
	movl	4(%r14), %esi
	xorl	%ecx, %ecx
	cmpl	$2, 204(%r15)
	setne	%cl
	leal	3(%rcx,%rcx), %edx
	movl	$1, %edi
	movl	%edx, %ecx
	shll	%cl, %edi
	shrl	%edi
	movl	crc(%rip), %ecx
	.p2align	4, 0x90
.LBB1_31:                               # %.lr.ph.i39.i
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rcx,%rcx), %r8d
	shrl	$15, %ecx
	notl	%ecx
	andl	$1, %ecx
	xorl	%ebx, %ebx
	testl	%esi, %edi
	sete	%bl
	movl	%r8d, %ebp
	xorl	$32773, %ebp            # imm = 0x8005
	cmpl	%ebx, %ecx
	movl	%ebp, %ecx
	cmovel	%r8d, %ecx
	shrl	%edi
	jne	.LBB1_31
# BB#32:                                # %CRC_BF_addEntry.exit40.i
	movzwl	%cx, %ecx
	movl	%ecx, crc(%rip)
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, frameSIPH(%rip)
	movl	204(%r15), %eax
	testl	%eax, %eax
	jle	.LBB1_35
# BB#33:                                # %.preheader173.i.preheader
	leaq	24(%r14), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_34:                               # %.preheader173.i
                                        # =>This Inner Loop Header: Depth=1
	movq	channelSIPH(,%rbp,8), %rdi
	movl	-12(%rbx), %esi
	movl	crc(%rip), %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	xorl	%esi, %eax
	movl	%ecx, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %al
	cmovel	%ecx, %edx
	movzwl	%dx, %eax
	movl	%eax, crc(%rip)
	movl	$1, %edx
	callq	BF_addEntry
	movq	%rax, channelSIPH(,%rbp,8)
	movl	-8(%rbx), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, channelSIPH(,%rbp,8)
	movl	-4(%rbx), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, channelSIPH(,%rbp,8)
	movl	(%rbx), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, channelSIPH(,%rbp,8)
	incq	%rbp
	movslq	204(%r15), %rax
	addq	$16, %rbx
	cmpq	%rax, %rbp
	jl	.LBB1_34
.LBB1_35:                               # %.preheader170.i.preheader
	leaq	80(%r14), %rcx
	xorl	%edx, %edx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_36:                               # %.preheader170.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_38 Depth 2
                                        #       Child Loop BB1_40 Depth 3
	testl	%eax, %eax
	jle	.LBB1_44
# BB#37:                                # %.lr.ph.i47.i.preheader
                                        #   in Loop: Header=BB1_36 Depth=1
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	movq	(%rsp), %r12            # 8-byte Reload
	.p2align	4, 0x90
.LBB1_38:                               # %.lr.ph.i47.i
                                        #   Parent Loop BB1_36 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_40 Depth 3
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rbp
	shlq	$4, %rbp
	leaq	spectrumSIPH(%rbp,%r13,8), %rbx
	movq	spectrumSIPH(%rbp,%r13,8), %rdi
	imulq	$240, %rax, %r14
	addq	%r12, %r14
	imulq	$120, %r13, %r15
	movl	48(%r15,%r14), %esi
	movl	crc(%rip), %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$11, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$10, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$9, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$8, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$7, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$6, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$5, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$4, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$3, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$2, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	%edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	xorl	%esi, %eax
	movl	%ecx, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %al
	cmovel	%ecx, %edx
	movzwl	%dx, %eax
	movl	%eax, crc(%rip)
	movl	$12, %edx
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(%rbp,%r13,8)
	movl	52(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$8, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$7, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$6, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$5, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$4, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$9, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(%rbp,%r13,8)
	movl	60(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$7, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$6, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$5, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$4, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$8, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(%rbp,%r13,8)
	movl	64(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$4, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(%rbp,%r13,8)
	movl	68(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(%rbp,%r13,8)
	cmpl	$0, 68(%r15,%r14)
	je	.LBB1_39
# BB#136:                               # %.lr.ph.i67.i
                                        #   in Loop: Header=BB1_38 Depth=2
	movl	72(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$2, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, (%rbx)
	movl	76(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, (%rbx)
	movl	80(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$4, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$5, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, (%rbx)
	movl	84(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$4, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$5, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, (%rbx)
	movl	92(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$3, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, (%rbx)
	movl	96(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$3, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, (%rbx)
	movl	100(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$3, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, %rdi
	jmp	.LBB1_42
	.p2align	4, 0x90
.LBB1_39:                               # %.lr.ph.i83.i.preheader
                                        #   in Loop: Header=BB1_38 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	$3, %r12d
	.p2align	4, 0x90
.LBB1_40:                               # %.lr.ph.i83.i
                                        #   Parent Loop BB1_36 Depth=1
                                        #     Parent Loop BB1_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbp), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$4, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$5, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, (%rbx)
	addq	$4, %rbp
	decq	%r12
	jne	.LBB1_40
# BB#41:                                # %.lr.ph.i87.i
                                        #   in Loop: Header=BB1_38 Depth=2
	movl	104(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$4, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, (%rbx)
	movl	108(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$3, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, %rdi
	movq	(%rsp), %r12            # 8-byte Reload
.LBB1_42:                               # %.loopexit.i
                                        #   in Loop: Header=BB1_38 Depth=2
	movq	%rdi, (%rbx)
	movl	112(%r15,%r14), %esi
	movl	crc(%rip), %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	xorl	%esi, %eax
	movl	%ecx, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %al
	cmovel	%ecx, %edx
	movzwl	%dx, %eax
	movl	%eax, crc(%rip)
	movl	$1, %edx
	callq	BF_addEntry
	movq	%rax, (%rbx)
	movl	116(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, (%rbx)
	movl	120(%r15,%r14), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, (%rbx)
	incq	%r13
	movq	48(%rsp), %rax          # 8-byte Reload
	movslq	204(%rax), %rax
	addq	$120, 8(%rsp)           # 8-byte Folded Spill
	cmpq	%rax, %r13
	jl	.LBB1_38
# BB#43:                                #   in Loop: Header=BB1_36 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB1_44:                               # %._crit_edge.i
                                        #   in Loop: Header=BB1_36 Depth=1
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rsi
	incq	%rsi
	addq	$240, %rcx
	movq	%rsi, %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	cmpq	$2, %rsi
	jne	.LBB1_36
	jmp	.LBB1_47
.LBB1_22:                               # %.lr.ph.i107.preheader.i
	shrl	$7, %edx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	movl	%esi, %ecx
	shrl	$6, %ecx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	movl	%esi, %ecx
	shrl	$5, %ecx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	movl	%esi, %ecx
	shrl	$4, %ecx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	movl	%esi, %ecx
	shrl	$3, %ecx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	movl	%esi, %ecx
	shrl	$2, %ecx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	movl	%esi, %ecx
	shrl	%ecx
	xorl	%edx, %ecx
	movl	%eax, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %cl
	cmovel	%eax, %edx
	leal	(%rdx,%rdx), %eax
	shrl	$15, %edx
	xorl	%esi, %edx
	movl	%eax, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dl
	cmovel	%eax, %ecx
	movzwl	%cx, %eax
	movl	%eax, crc(%rip)
	movl	$8, %edx
	callq	BF_addEntry
	movq	%rax, frameSIPH(%rip)
	xorl	%edx, %edx
	cmpl	$2, 204(%r15)
	sete	%dl
	movl	4(%r14), %esi
	incl	%edx
	movl	$1, %edi
	movl	%edx, %ecx
	shll	%cl, %edi
	shrl	%edi
	movl	crc(%rip), %ecx
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph.i111.i
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rcx,%rcx), %r8d
	shrl	$15, %ecx
	notl	%ecx
	andl	$1, %ecx
	xorl	%ebx, %ebx
	testl	%esi, %edi
	sete	%bl
	movl	%r8d, %ebp
	xorl	$32773, %ebp            # imm = 0x8005
	cmpl	%ebx, %ecx
	movl	%ebp, %ecx
	cmovel	%r8d, %ecx
	shrl	%edi
	jne	.LBB1_23
# BB#24:                                # %CRC_BF_addEntry.exit112.i
	movzwl	%cx, %ecx
	movl	%ecx, crc(%rip)
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, frameSIPH(%rip)
	cmpl	$0, 204(%r15)
	jle	.LBB1_47
# BB#25:                                # %.lr.ph193.i.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	80(%rax), %r14
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_26:                               # %.lr.ph193.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_28 Depth 2
	movq	spectrumSIPH(,%r12,8), %rdi
	imulq	$120, %r12, %r15
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	48(%rbx,%r15), %esi
	movl	crc(%rip), %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$11, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$10, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$9, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$8, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$7, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$6, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$5, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$4, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$3, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	$2, %edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	movl	%esi, %edx
	shrl	%edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	xorl	$32773, %eax            # imm = 0x8005
	testb	$1, %dl
	cmovel	%ecx, %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	xorl	%esi, %eax
	movl	%ecx, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %al
	cmovel	%ecx, %edx
	movzwl	%dx, %eax
	movl	%eax, crc(%rip)
	movl	$12, %edx
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	movl	52(%rbx,%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$8, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$7, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$6, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$5, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$4, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$9, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	movl	60(%rbx,%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$7, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$6, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$5, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$4, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$8, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	movl	64(%rbx,%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$8, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$7, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$6, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$5, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$4, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$9, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	movl	68(%rbx,%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	cmpl	$0, 68(%rbx,%r15)
	je	.LBB1_27
# BB#45:                                # %.lr.ph.i135.i
                                        #   in Loop: Header=BB1_26 Depth=1
	movl	72(%rbx,%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$2, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	movl	76(%rbx,%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	movl	80(%rbx,%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$4, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$5, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	movl	84(%rbx,%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$4, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$5, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	movl	92(%rbx,%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$3, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	movl	96(%rbx,%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$3, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	movl	100(%rbx,%r15), %esi
	jmp	.LBB1_46
	.p2align	4, 0x90
.LBB1_27:                               # %.lr.ph.i151.i.preheader
                                        #   in Loop: Header=BB1_26 Depth=1
	movq	%r14, %rbp
	movl	$3, %ebx
	.p2align	4, 0x90
.LBB1_28:                               # %.lr.ph.i151.i
                                        #   Parent Loop BB1_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$4, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$5, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	addq	$4, %rbp
	decq	%rbx
	jne	.LBB1_28
# BB#29:                                # %.lr.ph.i155.i
                                        #   in Loop: Header=BB1_26 Depth=1
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	104(%rbx,%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$3, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$4, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	movl	108(%rbx,%r15), %esi
.LBB1_46:                               # %.loopexit176.i
                                        #   in Loop: Header=BB1_26 Depth=1
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	$2, %edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	movl	%esi, %edi
	shrl	%edi
	xorl	%ecx, %edi
	movl	%edx, %ecx
	xorl	$32773, %ecx            # imm = 0x8005
	testb	$1, %dil
	cmovel	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$3, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, %rdi
	movq	%rdi, spectrumSIPH(,%r12,8)
	movl	116(%rbx,%r15), %esi
	movl	crc(%rip), %eax
	leal	(%rax,%rax), %ecx
	shrl	$15, %eax
	xorl	%esi, %eax
	movl	%ecx, %edx
	xorl	$32773, %edx            # imm = 0x8005
	testb	$1, %al
	cmovel	%ecx, %edx
	movzwl	%dx, %eax
	movl	%eax, crc(%rip)
	movl	$1, %edx
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	movl	120(%rbx,%r15), %esi
	movl	crc(%rip), %ecx
	leal	(%rcx,%rcx), %edx
	shrl	$15, %ecx
	xorl	%esi, %ecx
	movl	%edx, %edi
	xorl	$32773, %edi            # imm = 0x8005
	testb	$1, %cl
	cmovel	%edx, %edi
	movzwl	%di, %ecx
	movl	%ecx, crc(%rip)
	movl	$1, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, spectrumSIPH(,%r12,8)
	incq	%r12
	movq	48(%rsp), %rax          # 8-byte Reload
	movslq	204(%rax), %rax
	addq	$120, %r14
	cmpq	%rax, %r12
	jl	.LBB1_26
.LBB1_47:                               # %.loopexit172.i
	movq	48(%rsp), %r12          # 8-byte Reload
	cmpl	$0, 60(%r12)
	je	.LBB1_49
# BB#48:
	movq	headerPH(%rip), %rdi
	movl	crc(%rip), %esi
	movl	$16, %edx
	callq	BF_addEntry
	movq	%rax, headerPH(%rip)
.LBB1_49:                               # %encodeSideInfo.exit
	movq	64(%rsp), %rax          # 8-byte Reload
	movslq	(%rax), %r9
	testq	%r9, %r9
	movl	204(%r12), %eax
	movq	(%rsp), %r15            # 8-byte Reload
	jle	.LBB1_69
# BB#50:                                # %encodeSideInfo.exit
	testl	%eax, %eax
	jle	.LBB1_69
# BB#51:                                # %.preheader230.us.preheader.i
	movslq	%eax, %r8
	leaq	-1(%r8), %r10
	movl	$scaleFactorsPH, %r11d
	movl	%eax, %edi
	andl	$3, %edi
	xorl	%ebx, %ebx
	movl	$scaleFactorsPH+16, %edx
	.p2align	4, 0x90
.LBB1_52:                               # %.preheader230.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_55 Depth 2
                                        #     Child Loop BB1_58 Depth 2
	testq	%rdi, %rdi
	je	.LBB1_53
# BB#54:                                # %.prol.preheader150
                                        #   in Loop: Header=BB1_52 Depth=1
	movq	%r11, %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_55:                               #   Parent Loop BB1_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rbp
	movq	8(%rbp), %rbp
	movl	$0, (%rbp)
	incq	%rsi
	addq	$8, %rcx
	cmpq	%rsi, %rdi
	jne	.LBB1_55
	jmp	.LBB1_56
	.p2align	4, 0x90
.LBB1_53:                               #   in Loop: Header=BB1_52 Depth=1
	xorl	%esi, %esi
.LBB1_56:                               # %.prol.loopexit151
                                        #   in Loop: Header=BB1_52 Depth=1
	cmpq	$3, %r10
	jb	.LBB1_59
# BB#57:                                # %.preheader230.us.i.new
                                        #   in Loop: Header=BB1_52 Depth=1
	movq	%r8, %rcx
	subq	%rsi, %rcx
	leaq	(%rdx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB1_58:                               #   Parent Loop BB1_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-16(%rsi), %rbp
	movq	8(%rbp), %rbp
	movl	$0, (%rbp)
	movq	-8(%rsi), %rbp
	movq	8(%rbp), %rbp
	movl	$0, (%rbp)
	movq	(%rsi), %rbp
	movq	8(%rbp), %rbp
	movl	$0, (%rbp)
	movq	8(%rsi), %rbp
	movq	8(%rbp), %rbp
	movl	$0, (%rbp)
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB1_58
.LBB1_59:                               # %._crit_edge264.us.i
                                        #   in Loop: Header=BB1_52 Depth=1
	incq	%rbx
	addq	$16, %r11
	addq	$16, %rdx
	cmpq	%r9, %rbx
	jne	.LBB1_52
# BB#60:                                # %.preheader228.us.i.preheader
	movl	$codedDataPH, %r11d
	xorl	%ebx, %ebx
	movl	$codedDataPH+16, %edx
	.p2align	4, 0x90
.LBB1_61:                               # %.preheader228.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_64 Depth 2
                                        #     Child Loop BB1_67 Depth 2
	testq	%rdi, %rdi
	je	.LBB1_62
# BB#63:                                # %.prol.preheader145
                                        #   in Loop: Header=BB1_61 Depth=1
	movq	%r11, %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_64:                               #   Parent Loop BB1_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rbp
	movq	8(%rbp), %rbp
	movl	$0, (%rbp)
	incq	%rsi
	addq	$8, %rcx
	cmpq	%rsi, %rdi
	jne	.LBB1_64
	jmp	.LBB1_65
	.p2align	4, 0x90
.LBB1_62:                               #   in Loop: Header=BB1_61 Depth=1
	xorl	%esi, %esi
.LBB1_65:                               # %.prol.loopexit146
                                        #   in Loop: Header=BB1_61 Depth=1
	cmpq	$3, %r10
	jb	.LBB1_68
# BB#66:                                # %.preheader228.us.i.new
                                        #   in Loop: Header=BB1_61 Depth=1
	movq	%r8, %rcx
	subq	%rsi, %rcx
	leaq	(%rdx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB1_67:                               #   Parent Loop BB1_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-16(%rsi), %rbp
	movq	8(%rbp), %rbp
	movl	$0, (%rbp)
	movq	-8(%rsi), %rbp
	movq	8(%rbp), %rbp
	movl	$0, (%rbp)
	movq	(%rsi), %rbp
	movq	8(%rbp), %rbp
	movl	$0, (%rbp)
	movq	8(%rsi), %rbp
	movq	8(%rbp), %rbp
	movl	$0, (%rbp)
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB1_67
.LBB1_68:                               # %._crit_edge259.us.i
                                        #   in Loop: Header=BB1_61 Depth=1
	incq	%rbx
	addq	$16, %r11
	addq	$16, %rdx
	cmpq	%r9, %rbx
	jne	.LBB1_61
.LBB1_69:                               # %._crit_edge261.i
	cmpl	$1, 192(%r12)
	jne	.LBB1_75
# BB#70:                                # %.preheader218.i.preheader
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	228(%rcx), %rcx
	leaq	24(%r15), %rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	leaq	48(%r15), %rbx
	xorl	%r13d, %r13d
	xorl	%edi, %edi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_71:                               # %.preheader218.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_73 Depth 2
	testl	%eax, %eax
	jle	.LBB1_97
# BB#72:                                # %.lr.ph.i
                                        #   in Loop: Header=BB1_71 Depth=1
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movq	88(%rsp), %r14          # 8-byte Reload
	movq	%r13, 96(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, %r15
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_73:                               #   Parent Loop BB1_71 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movl	16(%rbx), %eax
	movq	%rbx, %rcx
	movl	slen1_tab(,%rax,4), %r14d
	movl	slen2_tab(,%rax,4), %r12d
	cmpl	$2, 24(%rcx)
	jne	.LBB1_81
# BB#74:                                # %.preheader210.preheader.i
                                        #   in Loop: Header=BB1_73 Depth=2
	movq	scaleFactorsPH(%r13), %rdi
	movl	-140(%r15), %esi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-136(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-132(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-128(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-124(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-120(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-116(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-112(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-108(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-104(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-100(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-96(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-92(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-88(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-84(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-80(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-76(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-72(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-68(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-64(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-60(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-56(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-52(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-48(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-44(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-40(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-36(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-32(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-28(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-24(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-20(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-16(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-12(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-8(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-4(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movq	%r15, %rcx
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB1_94
	.p2align	4, 0x90
.LBB1_81:                               #   in Loop: Header=BB1_73 Depth=2
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB1_83
# BB#82:                                #   in Loop: Header=BB1_73 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, -12(%rax)
	jne	.LBB1_84
.LBB1_83:                               # %.preheader217.preheader.i
                                        #   in Loop: Header=BB1_73 Depth=2
	movq	scaleFactorsPH(%r13), %rdi
	movl	-228(%r15), %esi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-224(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-220(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-216(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-212(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-208(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	movq	%rax, scaleFactorsPH(%r13)
	je	.LBB1_86
.LBB1_84:                               # %.thread.i
                                        #   in Loop: Header=BB1_73 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, -8(%rax)
	jne	.LBB1_88
# BB#85:                                # %.thread..preheader216.preheader_crit_edge.i
                                        #   in Loop: Header=BB1_73 Depth=2
	movq	scaleFactorsPH(%r13), %rax
.LBB1_86:                               # %.preheader216.preheader.i
                                        #   in Loop: Header=BB1_73 Depth=2
	movl	-204(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-200(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-196(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-192(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-188(%r15), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	movq	%rax, scaleFactorsPH(%r13)
	je	.LBB1_87
.LBB1_88:                               # %.thread208.i
                                        #   in Loop: Header=BB1_73 Depth=2
	movq	8(%rsp), %r14           # 8-byte Reload
	cmpl	$0, -4(%r14)
	jne	.LBB1_91
# BB#89:                                # %.thread208..preheader215.preheader_crit_edge.i
                                        #   in Loop: Header=BB1_73 Depth=2
	movq	scaleFactorsPH(%r13), %rax
	jmp	.LBB1_90
.LBB1_87:                               #   in Loop: Header=BB1_73 Depth=2
	movq	8(%rsp), %r14           # 8-byte Reload
.LBB1_90:                               # %.preheader215.preheader.i
                                        #   in Loop: Header=BB1_73 Depth=2
	movl	-184(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-180(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-176(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-172(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-168(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	movq	%rax, scaleFactorsPH(%r13)
	je	.LBB1_93
.LBB1_91:                               # %.thread209.i
                                        #   in Loop: Header=BB1_73 Depth=2
	cmpl	$0, (%r14)
	jne	.LBB1_95
# BB#92:                                # %.thread209..preheader213.preheader_crit_edge.i
                                        #   in Loop: Header=BB1_73 Depth=2
	movq	scaleFactorsPH(%r13), %rax
.LBB1_93:                               # %.preheader213.preheader.i
                                        #   in Loop: Header=BB1_73 Depth=2
	movl	-164(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-160(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-156(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	movl	-152(%r15), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
	imulq	$488, 16(%rsp), %rcx    # 8-byte Folded Reload
                                        # imm = 0x1E8
	addq	32(%rsp), %rcx          # 8-byte Folded Reload
	imulq	$244, 24(%rsp), %rdx    # 8-byte Folded Reload
	leaq	80(%rdx,%rcx), %rcx
.LBB1_94:                               # %.loopexit.i60.sink.split
                                        #   in Loop: Header=BB1_73 Depth=2
	movl	(%rcx), %esi
	movq	%rax, %rdi
	movl	%r12d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(%r13)
.LBB1_95:                               # %.loopexit.i60
                                        #   in Loop: Header=BB1_73 Depth=2
	leaq	codedDataPH(%r13), %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	Huffmancodebits
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rdx
	incq	%rdx
	movq	48(%rsp), %r12          # 8-byte Reload
	movslq	204(%r12), %rax
	addq	$244, %r15
	addq	$8, %r13
	addq	$16, %r14
	addq	$120, %rbx
	addq	$2304, %rbp             # imm = 0x900
	movq	%rdx, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	cmpq	%rax, %rdx
	jl	.LBB1_73
# BB#96:                                #   in Loop: Header=BB1_71 Depth=1
	movq	(%rsp), %r15            # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	96(%rsp), %r13          # 8-byte Reload
.LBB1_97:                               # %._crit_edge.i61
                                        #   in Loop: Header=BB1_71 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, %rbp
	incq	%rbp
	addq	$488, %rcx              # imm = 0x1E8
	addq	$16, %r13
	addq	$240, %rbx
	addq	$4608, 80(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x1200
	movq	%rbp, %rdi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	cmpq	$2, %rbp
	jne	.LBB1_71
	jmp	.LBB1_119
.LBB1_75:                               # %.preheader226.i
	testl	%eax, %eax
	jle	.LBB1_119
# BB#76:                                # %.lr.ph256.i.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_77:                               # %.lr.ph256.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_105 Depth 2
                                        #     Child Loop BB1_109 Depth 2
                                        #     Child Loop BB1_113 Depth 2
                                        #     Child Loop BB1_117 Depth 2
                                        #     Child Loop BB1_79 Depth 2
                                        #       Child Loop BB1_101 Depth 3
	imulq	$120, %rbp, %rax
	leaq	48(%r15,%rax), %r13
	leaq	(%rbp,%rbp,8), %rcx
	shlq	$8, %rcx
	addq	80(%rsp), %rcx          # 8-byte Folded Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpl	$2, 72(%r15,%rax)
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	jne	.LBB1_98
# BB#78:                                # %.preheader222.i.preheader
                                        #   in Loop: Header=BB1_77 Depth=1
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_79:                               # %.preheader222.i
                                        #   Parent Loop BB1_77 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_101 Depth 3
	movq	96(%r13), %rax
	movl	(%rax,%rdx,4), %r13d
	cmpq	$3, %r13
	jb	.LBB1_80
# BB#100:                               # %.preheader221.preheader.i
                                        #   in Loop: Header=BB1_79 Depth=2
	movl	$2863311531, %eax       # imm = 0xAAAAAAAB
	imulq	%rax, %r13
	shrq	$33, %r13
	movq	(%rsp), %rax            # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	152(%rax,%rdx,4), %ebx
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movslq	%esi, %rcx
	movq	scaleFactorsPH(,%rbp,8), %rax
	shlq	$2, %rcx
	leaq	(%rcx,%rcx,2), %r14
	xorl	%r15d, %r15d
	movq	32(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_101:                              # %.preheader221.i
                                        #   Parent Loop BB1_77 Depth=1
                                        #     Parent Loop BB1_79 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	88(%r12,%r14), %esi
	movq	%rax, %rdi
	movl	%ebx, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(,%rbp,8)
	movl	92(%r12,%r14), %esi
	movq	%rax, %rdi
	movl	%ebx, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(,%rbp,8)
	movl	96(%r12,%r14), %esi
	movq	%rax, %rdi
	movl	%ebx, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(,%rbp,8)
	incl	%r15d
	addq	$12, %r14
	cmpl	%r13d, %r15d
	jl	.LBB1_101
# BB#102:                               # %._crit_edge251.loopexit.i
                                        #   in Loop: Header=BB1_79 Depth=2
	movl	16(%rsp), %esi          # 4-byte Reload
	addl	%r15d, %esi
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB1_103
	.p2align	4, 0x90
.LBB1_80:                               #   in Loop: Header=BB1_79 Depth=2
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB1_103:                              # %._crit_edge251.i
                                        #   in Loop: Header=BB1_79 Depth=2
	incq	%rdx
	cmpq	$4, %rdx
	jne	.LBB1_79
	jmp	.LBB1_118
	.p2align	4, 0x90
.LBB1_98:                               # %.preheader224.preheader.i
                                        #   in Loop: Header=BB1_77 Depth=1
	movq	96(%r13), %rax
	movl	(%rax), %r12d
	testl	%r12d, %r12d
	jle	.LBB1_99
# BB#104:                               # %.lr.ph244.preheader.i
                                        #   in Loop: Header=BB1_77 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	152(%rax,%rcx), %r14d
	movq	scaleFactorsPH(,%rbp,8), %rax
	movl	%r12d, %r15d
	movq	32(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_105:                              # %.lr.ph244.i
                                        #   Parent Loop BB1_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(,%rbp,8)
	addq	$4, %rbx
	decl	%r15d
	jne	.LBB1_105
# BB#106:                               # %._crit_edge245.loopexit.i
                                        #   in Loop: Header=BB1_77 Depth=1
	movq	96(%r13), %rax
	jmp	.LBB1_107
.LBB1_99:                               #   in Loop: Header=BB1_77 Depth=1
	xorl	%r12d, %r12d
.LBB1_107:                              # %._crit_edge245.i
                                        #   in Loop: Header=BB1_77 Depth=1
	movl	4(%rax), %r14d
	testl	%r14d, %r14d
	jle	.LBB1_111
# BB#108:                               # %.lr.ph244.preheader.1.i
                                        #   in Loop: Header=BB1_77 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	156(%rax,%rcx), %r15d
	movslq	%r12d, %rcx
	movq	scaleFactorsPH(,%rbp,8), %rax
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rbx
	movl	%r14d, %r13d
	.p2align	4, 0x90
.LBB1_109:                              # %.lr.ph244.1.i
                                        #   Parent Loop BB1_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %esi
	movq	%rax, %rdi
	movl	%r15d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(,%rbp,8)
	addq	$4, %rbx
	decl	%r13d
	jne	.LBB1_109
# BB#110:                               # %._crit_edge245.loopexit.1.i
                                        #   in Loop: Header=BB1_77 Depth=1
	addl	%r14d, %r12d
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	96(%r13), %rax
.LBB1_111:                              # %._crit_edge245.1.i
                                        #   in Loop: Header=BB1_77 Depth=1
	movl	8(%rax), %r14d
	testl	%r14d, %r14d
	jle	.LBB1_115
# BB#112:                               # %.lr.ph244.preheader.2.i
                                        #   in Loop: Header=BB1_77 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	160(%rax,%rcx), %r15d
	movslq	%r12d, %rcx
	movq	scaleFactorsPH(,%rbp,8), %rax
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rbx
	movl	%r14d, %r13d
	.p2align	4, 0x90
.LBB1_113:                              # %.lr.ph244.2.i
                                        #   Parent Loop BB1_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %esi
	movq	%rax, %rdi
	movl	%r15d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(,%rbp,8)
	addq	$4, %rbx
	decl	%r13d
	jne	.LBB1_113
# BB#114:                               # %._crit_edge245.loopexit.2.i
                                        #   in Loop: Header=BB1_77 Depth=1
	addl	%r14d, %r12d
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	96(%r13), %rax
.LBB1_115:                              # %._crit_edge245.2.i
                                        #   in Loop: Header=BB1_77 Depth=1
	movl	12(%rax), %r15d
	testl	%r15d, %r15d
	jle	.LBB1_118
# BB#116:                               # %.lr.ph244.preheader.3.i
                                        #   in Loop: Header=BB1_77 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	164(%rax,%rcx), %r14d
	movslq	%r12d, %rcx
	movq	scaleFactorsPH(,%rbp,8), %rax
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rbx
	.p2align	4, 0x90
.LBB1_117:                              # %.lr.ph244.3.i
                                        #   Parent Loop BB1_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, scaleFactorsPH(,%rbp,8)
	addq	$4, %rbx
	decl	%r15d
	jne	.LBB1_117
.LBB1_118:                              # %.loopexit223.i
                                        #   in Loop: Header=BB1_77 Depth=1
	leaq	codedDataPH(,%rbp,8), %rdi
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	%r13, %rdx
	callq	Huffmancodebits
	incq	%rbp
	movq	48(%rsp), %r12          # 8-byte Reload
	movslq	204(%r12), %rax
	addq	$244, 32(%rsp)          # 8-byte Folded Spill
	cmpq	%rax, %rbp
	movq	(%rsp), %r15            # 8-byte Reload
	jl	.LBB1_77
.LBB1_119:                              # %encodeMainData.exit
	movl	8(%r15), %ecx
	movl	%ecx, %ebp
	sarl	$31, %ebp
	shrl	$27, %ebp
	addl	%ecx, %ebp
	movl	%ebp, %eax
	andl	$-32, %eax
	movl	%ecx, %r14d
	subl	%eax, %r14d
	movq	userFrameDataPH(%rip), %rax
	movq	8(%rax), %rdx
	movl	$0, (%rdx)
	cmpl	$32, %ecx
	jl	.LBB1_122
# BB#120:                               # %.lr.ph.i62.preheader
	sarl	$5, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_121:                              # %.lr.ph.i62
                                        # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movl	$32, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, userFrameDataPH(%rip)
	incl	%ebx
	cmpl	%ebp, %ebx
	jl	.LBB1_121
.LBB1_122:                              # %._crit_edge.i63
	testl	%r14d, %r14d
	je	.LBB1_124
# BB#123:
	xorl	%esi, %esi
	movq	%rax, %rdi
	movl	%r14d, %edx
	callq	BF_addEntry
	movq	%rax, userFrameDataPH(%rip)
.LBB1_124:                              # %drain_into_ancillary_data.exit
	movl	60(%rsp), %ecx          # 4-byte Reload
	movq	frameData(%rip), %rdi
	movl	%ecx, (%rdi)
	movq	64(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx), %r8
	movl	%r8d, 4(%rdi)
	movslq	204(%r12), %rdx
	testq	%rdx, %rdx
	movl	%edx, 8(%rdi)
	movq	headerPH(%rip), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, 16(%rdi)
	movq	frameSIPH(%rip), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, 24(%rdi)
	jle	.LBB1_128
# BB#125:                               # %.lr.ph83
	leaq	-1(%rdx), %rbp
	movq	%rdx, %rcx
	xorl	%esi, %esi
	andq	$3, %rcx
	je	.LBB1_127
	.p2align	4, 0x90
.LBB1_126:                              # =>This Inner Loop Header: Depth=1
	movq	channelSIPH(,%rsi,8), %rbx
	movq	8(%rbx), %rbx
	movq	%rbx, 32(%rdi,%rsi,8)
	incq	%rsi
	cmpq	%rsi, %rcx
	jne	.LBB1_126
.LBB1_127:                              # %.prol.loopexit
	cmpq	$3, %rbp
	jb	.LBB1_128
	.p2align	4, 0x90
.LBB1_137:                              # =>This Inner Loop Header: Depth=1
	movq	channelSIPH(,%rsi,8), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, 32(%rdi,%rsi,8)
	movq	channelSIPH+8(,%rsi,8), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, 40(%rdi,%rsi,8)
	movq	channelSIPH+16(,%rsi,8), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, 48(%rdi,%rsi,8)
	movq	channelSIPH+24(,%rsi,8), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, 56(%rdi,%rsi,8)
	addq	$4, %rsi
	cmpq	%rdx, %rsi
	jl	.LBB1_137
.LBB1_128:                              # %.preheader67
	testl	%r8d, %r8d
	jle	.LBB1_134
# BB#129:                               # %.preheader.lr.ph
	movq	frameData(%rip), %rdi
	testl	%edx, %edx
	jle	.LBB1_134
# BB#130:                               # %.preheader.us.preheader
	xorl	%r9d, %r9d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_131:                              # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_132 Depth 2
	movq	%r9, %rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_132:                              #   Parent Loop BB1_131 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	spectrumSIPH(%rsi), %rbx
	movq	8(%rbx), %rbx
	movq	%rbx, 48(%rdi,%rsi)
	movq	scaleFactorsPH(%rsi), %rbx
	movq	8(%rbx), %rbx
	movq	%rbx, 80(%rdi,%rsi)
	movq	codedDataPH(%rsi), %rbx
	movq	8(%rbx), %rbx
	movq	%rbx, 112(%rdi,%rsi)
	movq	userSpectrumPH(%rsi), %rbx
	movq	8(%rbx), %rbx
	movq	%rbx, 144(%rdi,%rsi)
	incq	%rcx
	addq	$8, %rsi
	cmpq	%rdx, %rcx
	jl	.LBB1_132
# BB#133:                               # %._crit_edge.us
                                        #   in Loop: Header=BB1_131 Depth=1
	incq	%rbp
	addq	$16, %r9
	cmpq	%r8, %rbp
	jl	.LBB1_131
.LBB1_134:                              # %._crit_edge81
	movq	8(%rax), %rax
	movq	%rax, 176(%rdi)
	movq	frameResults(%rip), %rsi
	callq	BF_BitstreamFrame
	movq	frameResults(%rip), %rax
	movl	8(%rax), %eax
	movl	%eax, (%r15)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	III_format_bitstream, .Lfunc_end1-III_format_bitstream
	.cfi_endproc

	.globl	III_FlushBitstream
	.p2align	4, 0x90
	.type	III_FlushBitstream,@function
III_FlushBitstream:                     # @III_FlushBitstream
	.cfi_startproc
# BB#0:
	cmpl	$0, PartHoldersInitialized(%rip)
	je	.LBB2_1
# BB#2:
	movq	frameData(%rip), %rdi
	movq	frameResults(%rip), %rsi
	jmp	BF_FlushBitstream       # TAILCALL
.LBB2_1:
	retq
.Lfunc_end2:
	.size	III_FlushBitstream, .Lfunc_end2-III_FlushBitstream
	.cfi_endproc

	.globl	abs_and_sign
	.p2align	4, 0x90
	.type	abs_and_sign,@function
abs_and_sign:                           # @abs_and_sign
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.LBB3_2
# BB#1:
	negl	%ecx
	movl	%ecx, (%rdi)
	movl	$1, %eax
.LBB3_2:
	retq
.Lfunc_end3:
	.size	abs_and_sign, .Lfunc_end3-abs_and_sign
	.cfi_endproc

	.globl	L3_huffman_coder_count1
	.p2align	4, 0x90
	.type	L3_huffman_coder_count1,@function
L3_huffman_coder_count1:                # @L3_huffman_coder_count1
	.cfi_startproc
# BB#0:                                 # %abs_and_sign.exit
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %rbx
	movl	%r9d, %edi
	negl	%edi
	cmovll	%r9d, %edi
	movl	%r8d, %r13d
	negl	%r13d
	cmovll	%r8d, %r13d
	movl	%ecx, %r15d
	sarl	$31, %r15d
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leal	(%rcx,%r15), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movl	%edx, %eax
	sarl	$31, %eax
	leal	(%rdx,%rax), %r14d
	testl	%edx, %edx
	setle	%r12b
	testl	%r8d, %r8d
	setle	1(%rsp)                 # 1-byte Folded Spill
	testl	%r9d, %r9d
	setle	2(%rsp)                 # 1-byte Folded Spill
	xorl	%ebp, %ebp
	xorl	%eax, %r14d
	setne	3(%rsp)                 # 1-byte Folded Spill
	leal	(,%r14,8), %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	xorl	%r15d, %ecx
	leal	(%rax,%rcx,4), %eax
	leal	(%rax,%r13,2), %eax
	movl	%edi, 8(%rsp)           # 4-byte Spill
	addl	%edi, %eax
	movq	8(%rsi), %rcx
	movq	16(%rsi), %rdx
	movl	(%rcx,%rax,8), %esi
	movzbl	(%rdx,%rax), %edx
	movq	(%rbx), %rdi
	movl	%edx, 12(%rsp)          # 4-byte Spill
	callq	BF_addEntry
	movb	3(%rsp), %cl            # 1-byte Reload
	andb	%cl, %r12b
	xorl	4(%rsp), %r15d          # 4-byte Folded Reload
	movq	%rax, (%rbx)
	movzbl	%r12b, %esi
	je	.LBB4_1
# BB#2:
	xorl	%ecx, %ecx
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	setle	%cl
	leal	(%rcx,%rsi,2), %esi
	cmpl	$1, %r14d
	movl	$1, %ebp
	sbbl	$-1, %ebp
	testl	%r13d, %r13d
	jne	.LBB4_4
	jmp	.LBB4_5
.LBB4_1:
	movb	%cl, %bpl
	testl	%r13d, %r13d
	je	.LBB4_5
.LBB4_4:
	xorl	%ecx, %ecx
	movb	1(%rsp), %dl            # 1-byte Reload
	movb	%dl, %cl
	leal	(%rcx,%rsi,2), %esi
	incl	%ebp
.LBB4_5:
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB4_7
# BB#6:
	xorl	%ecx, %ecx
	movb	2(%rsp), %dl            # 1-byte Reload
	movb	%dl, %cl
	leal	(%rcx,%rsi,2), %esi
	incl	%ebp
.LBB4_7:
	movq	%rax, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%ebp, %edx
	callq	BF_addEntry
	movq	%rax, (%rbx)
	movl	12(%rsp), %eax          # 4-byte Reload
	addl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	L3_huffman_coder_count1, .Lfunc_end4-L3_huffman_coder_count1
	.cfi_endproc

	.globl	HuffmanCode
	.p2align	4, 0x90
	.type	HuffmanCode,@function
HuffmanCode:                            # @HuffmanCode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	56(%rsp), %r10
	movl	$0, (%r9)
	movl	$0, (%r10)
	movl	$0, (%rcx)
	movl	$0, (%r8)
	testl	%edi, %edi
	je	.LBB5_1
# BB#2:                                 # %abs_and_sign.exit
	movl	%esi, %ebx
	negl	%ebx
	cmovll	%esi, %ebx
	xorl	%r11d, %r11d
	testl	%esi, %esi
	setle	%r11b
	movl	%edx, %eax
	negl	%eax
	cmovll	%edx, %eax
	xorl	%r12d, %r12d
	testl	%edx, %edx
	setle	%r12b
	movslq	%edi, %rdx
	cmpl	$16, %edi
	jl	.LBB5_11
# BB#3:
	shlq	$3, %rdx
	movl	ht(%rdx,%rdx,2), %r14d
	cmpl	$14, %ebx
	movl	$15, %r13d
	movl	%ebx, %edi
	cmovgl	%r13d, %edi
	leal	-15(%rax), %esi
	xorl	%r15d, %r15d
	cmpl	$14, %eax
	cmovlel	%eax, %r13d
	cmovgl	%esi, %r15d
	movl	%edi, %esi
	shll	$4, %esi
	addl	%r13d, %esi
	movq	ht+8(%rdx,%rdx,2), %rbp
	movl	(%rbp,%rsi,8), %ebp
	movl	%ebp, (%rcx)
	movq	ht+16(%rdx,%rdx,2), %rcx
	movzbl	(%rcx,%rsi), %ecx
	movl	%ecx, (%r9)
	cmpl	$15, %ebx
	jl	.LBB5_5
# BB#4:                                 # %.thread
	addl	$-15, %ebx
	orl	%ebx, (%r8)
	addl	%r14d, (%r10)
	jmp	.LBB5_6
.LBB5_1:
	xorl	%eax, %eax
	jmp	.LBB5_16
.LBB5_11:
	movl	%ebx, %esi
	shll	$4, %esi
	addl	%eax, %esi
	shlq	$3, %rdx
	movq	ht+8(%rdx,%rdx,2), %rdi
	movl	(%rdi,%rsi,8), %edi
	movl	%edi, (%rcx)
	movq	ht+16(%rdx,%rdx,2), %rdx
	movzbl	(%rdx,%rsi), %edx
	addl	%edx, (%r9)
	testl	%ebx, %ebx
	je	.LBB5_13
# BB#12:
	movl	(%rcx), %edx
	leal	(%r11,%rdx,2), %edx
	movl	%edx, (%rcx)
	incl	(%r9)
.LBB5_13:
	testl	%eax, %eax
	je	.LBB5_15
# BB#14:
	movl	(%rcx), %eax
	leal	(%r12,%rax,2), %eax
	movl	%eax, (%rcx)
	incl	(%r9)
	jmp	.LBB5_15
.LBB5_5:
	testl	%edi, %edi
	je	.LBB5_7
.LBB5_6:
	movl	(%r8), %ecx
	leal	(%r11,%rcx,2), %ecx
	movl	%ecx, (%r8)
	incl	(%r10)
.LBB5_7:
	cmpl	$15, %eax
	jl	.LBB5_9
# BB#8:                                 # %.thread81
	movl	(%r8), %eax
	movl	%r14d, %ecx
	shll	%cl, %eax
	orl	%r15d, %eax
	movl	%eax, (%r8)
	addl	%r14d, (%r10)
	jmp	.LBB5_10
.LBB5_9:
	testl	%r13d, %r13d
	je	.LBB5_15
.LBB5_10:
	movl	(%r8), %eax
	leal	(%r12,%rax,2), %eax
	movl	%eax, (%r8)
	incl	(%r10)
.LBB5_15:
	movl	(%r10), %eax
	addl	(%r9), %eax
.LBB5_16:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	HuffmanCode, .Lfunc_end5-HuffmanCode
	.cfi_endproc

	.p2align	4, 0x90
	.type	Huffmancodebits,@function
Huffmancodebits:                        # @Huffmancodebits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 160
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	4(%rdx), %edi
	xorl	%r13d, %r13d
	addl	%edi, %edi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	je	.LBB6_19
# BB#1:
	cmpl	$0, 28(%rdx)
	je	.LBB6_3
# BB#2:
	movl	$36, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$576, %r12d             # imm = 0x240
	testl	%edi, %edi
	jg	.LBB6_15
	jmp	.LBB6_19
.LBB6_3:
	cmpl	$2, 24(%rdx)
	jne	.LBB6_13
# BB#4:
	leaq	16(%rsi), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	20(%rsi), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	leaq	28(%rsp), %rdi
	leaq	24(%rsp), %r8
	leaq	20(%rsp), %r9
	leaq	32(%rsp), %r10
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB6_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_8 Depth 2
                                        #     Child Loop BB6_10 Depth 2
                                        #     Child Loop BB6_12 Depth 2
	movslq	scalefac_band+92(,%r15,4), %r12
	movslq	scalefac_band+96(,%r15,4), %rdx
	incq	%r15
	xorl	%eax, %eax
	cmpl	$11, %r12d
	setg	%cl
	cmpl	%edx, %r12d
	jge	.LBB6_5
# BB#7:                                 # %.preheader.us.preheader
                                        #   in Loop: Header=BB6_6 Depth=1
	movq	%r15, 96(%rsp)          # 8-byte Spill
	movb	%cl, %al
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	32(%rcx,%rax,4), %ecx
	leaq	(%r12,%r12,2), %rsi
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	leaq	(%rax,%rsi,4), %rbx
	movq	%r12, %rbp
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB6_8:                                #   Parent Loop BB6_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %esi
	movl	12(%rbx), %edx
	movq	%rdi, (%rsp)
	movl	%ecx, %edi
	movq	%r8, %rcx
	movq	%r9, %r8
	movq	%r10, %r9
	callq	HuffmanCode
	movl	%eax, %r15d
	movq	(%r14), %rdi
	movl	24(%rsp), %esi
	movl	32(%rsp), %edx
	callq	BF_addEntry
	movq	%rax, (%r14)
	movl	20(%rsp), %esi
	movl	28(%rsp), %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movl	36(%rsp), %ecx          # 4-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	32(%rsp), %r10
	leaq	20(%rsp), %r9
	leaq	24(%rsp), %r8
	leaq	28(%rsp), %rdi
	movq	%rax, (%r14)
	addl	%r15d, %r13d
	addq	$2, %rbp
	addq	$24, %rbx
	cmpq	%rdx, %rbp
	jl	.LBB6_8
# BB#9:                                 # %._crit_edge164.us.preheader
                                        #   in Loop: Header=BB6_6 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	(%rax,%rdx,4), %rbx
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB6_10:                               # %._crit_edge164.us
                                        #   Parent Loop BB6_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rbx), %esi
	movl	(%rbx), %edx
	movq	%rdi, (%rsp)
	movl	%ecx, %edi
	movq	%r8, %rcx
	movq	%r9, %r8
	movq	%r10, %r9
	callq	HuffmanCode
	movl	%eax, %r15d
	movq	(%r14), %rdi
	movl	24(%rsp), %esi
	movl	32(%rsp), %edx
	callq	BF_addEntry
	movq	%rax, (%r14)
	movl	20(%rsp), %esi
	movl	28(%rsp), %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movl	36(%rsp), %ecx          # 4-byte Reload
	leaq	32(%rsp), %r10
	leaq	20(%rsp), %r9
	leaq	24(%rsp), %r8
	leaq	28(%rsp), %rdi
	movq	%rax, (%r14)
	addl	%r15d, %r13d
	addq	$2, %rbp
	addq	$24, %rbx
	cmpq	40(%rsp), %rbp          # 8-byte Folded Reload
	jl	.LBB6_10
# BB#11:                                # %._crit_edge164.us.1.preheader
                                        #   in Loop: Header=BB6_6 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	(%rax,%rdx,4), %rbx
	movq	96(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_12:                               # %._crit_edge164.us.1
                                        #   Parent Loop BB6_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rbx), %esi
	movl	(%rbx), %edx
	movq	%rdi, (%rsp)
	movl	%ecx, %edi
	movq	%r8, %rcx
	movq	%r9, %r8
	movq	%r10, %r9
	callq	HuffmanCode
	movl	%eax, %ebp
	movq	(%r14), %rdi
	movl	24(%rsp), %esi
	movl	32(%rsp), %edx
	callq	BF_addEntry
	movq	%rax, (%r14)
	movl	20(%rsp), %esi
	movl	28(%rsp), %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movl	36(%rsp), %ecx          # 4-byte Reload
	leaq	32(%rsp), %r10
	leaq	20(%rsp), %r9
	leaq	24(%rsp), %r8
	leaq	28(%rsp), %rdi
	movq	%rax, (%r14)
	addl	%ebp, %r13d
	addq	$2, %r12
	addq	$24, %rbx
	cmpq	40(%rsp), %r12          # 8-byte Folded Reload
	jl	.LBB6_12
.LBB6_5:                                # %.loopexit
                                        #   in Loop: Header=BB6_6 Depth=1
	cmpq	$13, %r15
	jne	.LBB6_6
	jmp	.LBB6_19
.LBB6_13:
	movl	56(%rdx), %eax
	movl	60(%rdx), %ecx
	leal	1(%rax), %edx
	leal	2(%rax,%rcx), %eax
	movslq	scalefac_band(,%rax,4), %r12
	movslq	scalefac_band(,%rdx,4), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testl	%edi, %edi
	jle	.LBB6_19
.LBB6_15:                               # %.lr.ph172.preheader
	movslq	64(%rsp), %r15          # 4-byte Folded Reload
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB6_16:                               # %.lr.ph172
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	cmpq	%r12, %rbx
	setge	%al
	incq	%rax
	cmpq	40(%rsp), %rbx          # 8-byte Folded Reload
	movl	$0, %ecx
	cmovgeq	%rax, %rcx
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	32(%rax,%rcx,4), %edi
	testl	%edi, %edi
	je	.LBB6_18
# BB#17:                                #   in Loop: Header=BB6_16 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	4(%rax,%rbx,4), %edx
	movl	(%rax,%rbx,4), %esi
	leaq	28(%rsp), %rax
	movq	%rax, (%rsp)
	leaq	24(%rsp), %rcx
	leaq	20(%rsp), %r8
	leaq	32(%rsp), %r9
	callq	HuffmanCode
	movl	%eax, %ebp
	movq	(%r14), %rdi
	movl	24(%rsp), %esi
	movl	32(%rsp), %edx
	callq	BF_addEntry
	movq	%rax, (%r14)
	movl	20(%rsp), %esi
	movl	28(%rsp), %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	movq	%rax, (%r14)
	addl	%ebp, %r13d
.LBB6_18:                               #   in Loop: Header=BB6_16 Depth=1
	addq	$2, %rbx
	cmpq	%r15, %rbx
	jl	.LBB6_16
.LBB6_19:                               # %.loopexit153
	movq	56(%rsp), %r15          # 8-byte Reload
	movl	8(%r15), %eax
	movq	64(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rax,4), %eax
	cmpl	%eax, %ecx
	jge	.LBB6_22
# BB#20:                                # %.lr.ph159
	movslq	%ecx, %rbx
	movslq	%eax, %r12
	movq	48(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_21:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rbx,4), %edx
	movl	4(%rbp,%rbx,4), %ecx
	movl	8(%rbp,%rbx,4), %r8d
	movl	12(%rbp,%rbx,4), %r9d
	movl	72(%r15), %eax
	addl	$32, %eax
	leaq	(%rax,%rax,2), %rax
	leaq	ht(,%rax,8), %rsi
	movq	%r14, %rdi
	callq	L3_huffman_coder_count1
	addl	%eax, %r13d
	addq	$4, %rbx
	cmpq	%r12, %rbx
	jl	.LBB6_21
.LBB6_22:                               # %._crit_edge160
	movl	(%r15), %ebp
	subl	76(%r15), %ebp
	subl	%r13d, %ebp
	jne	.LBB6_23
.LBB6_28:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_23:
	movl	%ebp, %r15d
	sarl	$31, %r15d
	shrl	$27, %r15d
	addl	%ebp, %r15d
	movl	%r15d, %eax
	andl	$-32, %eax
	movl	%ebp, %ebx
	subl	%eax, %ebx
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	addl	$31, %ebp
	cmpl	$63, %ebp
	jb	.LBB6_26
# BB#24:                                # %.lr.ph.preheader
	sarl	$5, %r15d
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB6_25:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, %esi
	movl	$32, %edx
	movq	%rax, %rdi
	callq	BF_addEntry
	decl	%r15d
	movq	%rax, (%r14)
	jne	.LBB6_25
.LBB6_26:                               # %._crit_edge
	testl	%ebx, %ebx
	je	.LBB6_28
# BB#27:
	movq	(%r14), %rdi
	movl	$-1, %esi
	movl	%ebx, %edx
	callq	BF_addEntry
	movq	%rax, (%r14)
	jmp	.LBB6_28
.Lfunc_end6:
	.size	Huffmancodebits, .Lfunc_end6-Huffmancodebits
	.cfi_endproc

	.type	frameData,@object       # @frameData
	.bss
	.globl	frameData
	.p2align	3
frameData:
	.quad	0
	.size	frameData, 8

	.type	frameResults,@object    # @frameResults
	.globl	frameResults
	.p2align	3
frameResults:
	.quad	0
	.size	frameResults, 8

	.type	PartHoldersInitialized,@object # @PartHoldersInitialized
	.globl	PartHoldersInitialized
	.p2align	2
PartHoldersInitialized:
	.long	0                       # 0x0
	.size	PartHoldersInitialized, 4

	.type	bs,@object              # @bs
	.local	bs
	.comm	bs,8,8
	.type	headerPH,@object        # @headerPH
	.comm	headerPH,8,8
	.type	frameSIPH,@object       # @frameSIPH
	.comm	frameSIPH,8,8
	.type	channelSIPH,@object     # @channelSIPH
	.comm	channelSIPH,16,16
	.type	spectrumSIPH,@object    # @spectrumSIPH
	.comm	spectrumSIPH,32,16
	.type	scaleFactorsPH,@object  # @scaleFactorsPH
	.comm	scaleFactorsPH,32,16
	.type	codedDataPH,@object     # @codedDataPH
	.comm	codedDataPH,32,16
	.type	userSpectrumPH,@object  # @userSpectrumPH
	.comm	userSpectrumPH,32,16
	.type	userFrameDataPH,@object # @userFrameDataPH
	.comm	userFrameDataPH,8,8
	.type	slen1_tab,@object       # @slen1_tab
	.section	.rodata,"a",@progbits
	.p2align	4
slen1_tab:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.size	slen1_tab, 64

	.type	slen2_tab,@object       # @slen2_tab
	.p2align	4
slen2_tab:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.size	slen2_tab, 64

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"opps - adding stuffing bits = %i.\n"
	.size	.L.str, 35

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"this should not happen...\n"
	.size	.L.str.1, 27

	.type	crc,@object             # @crc
	.local	crc
	.comm	crc,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
