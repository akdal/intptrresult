	.text
	.file	"btConvexPlaneCollisionAlgorithm.bc"
	.globl	_ZN31btConvexPlaneCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_bii
	.p2align	4, 0x90
	.type	_ZN31btConvexPlaneCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_bii,@function
_ZN31btConvexPlaneCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_bii: # @_ZN31btConvexPlaneCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_bii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movq	%r8, %r14
	movq	%rcx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movl	64(%rsp), %r15d
	movq	%rdx, %rsi
	callq	_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	movq	$_ZTV31btConvexPlaneCollisionAlgorithm+16, (%rbx)
	movb	$0, 16(%rbx)
	movq	%r13, 24(%rbx)
	movb	%bpl, 32(%rbx)
	movl	%r15d, 36(%rbx)
	movl	72(%rsp), %eax
	movl	%eax, 40(%rbx)
	testb	%bpl, %bpl
	movq	%r12, %r15
	cmovneq	%r14, %r15
	cmovneq	%r12, %r14
	testq	%r13, %r13
	jne	.LBB0_3
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*48(%rax)
	testb	%al, %al
	je	.LBB0_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*24(%rax)
	movq	%rax, 24(%rbx)
	movb	$1, 16(%rbx)
.LBB0_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN31btConvexPlaneCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_bii, .Lfunc_end0-_ZN31btConvexPlaneCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_bii
	.cfi_endproc

	.globl	_ZN31btConvexPlaneCollisionAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN31btConvexPlaneCollisionAlgorithmD2Ev,@function
_ZN31btConvexPlaneCollisionAlgorithmD2Ev: # @_ZN31btConvexPlaneCollisionAlgorithmD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV31btConvexPlaneCollisionAlgorithm+16, (%rdi)
	cmpb	$0, 16(%rdi)
	je	.LBB1_2
# BB#1:
	movq	24(%rdi), %rsi
	testq	%rsi, %rsi
	je	.LBB1_2
# BB#3:
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.LBB1_2:
	retq
.Lfunc_end1:
	.size	_ZN31btConvexPlaneCollisionAlgorithmD2Ev, .Lfunc_end1-_ZN31btConvexPlaneCollisionAlgorithmD2Ev
	.cfi_endproc

	.globl	_ZN31btConvexPlaneCollisionAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN31btConvexPlaneCollisionAlgorithmD0Ev,@function
_ZN31btConvexPlaneCollisionAlgorithmD0Ev: # @_ZN31btConvexPlaneCollisionAlgorithmD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV31btConvexPlaneCollisionAlgorithm+16, (%rbx)
	cmpb	$0, 16(%rbx)
	je	.LBB2_3
# BB#1:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB2_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp0:
	callq	*32(%rax)
.Ltmp1:
.LBB2_3:                                # %_ZN31btConvexPlaneCollisionAlgorithmD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB2_4:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN31btConvexPlaneCollisionAlgorithmD0Ev, .Lfunc_end2-_ZN31btConvexPlaneCollisionAlgorithmD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end2-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI3_2:
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_1:
	.long	1073741824              # float 2
.LCPI3_3:
	.long	1065353216              # float 1
	.text
	.globl	_ZN31btConvexPlaneCollisionAlgorithm20collideSingleContactERK12btQuaternionP17btCollisionObjectS4_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN31btConvexPlaneCollisionAlgorithm20collideSingleContactERK12btQuaternionP17btCollisionObjectS4_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN31btConvexPlaneCollisionAlgorithm20collideSingleContactERK12btQuaternionP17btCollisionObjectS4_RK16btDispatcherInfoP16btManifoldResult: # @_ZN31btConvexPlaneCollisionAlgorithm20collideSingleContactERK12btQuaternionP17btCollisionObjectS4_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 40
	subq	$568, %rsp              # imm = 0x238
.Lcfi22:
	.cfi_def_cfa_offset 608
.Lcfi23:
	.cfi_offset %rbx, -40
.Lcfi24:
	.cfi_offset %r12, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%r9, %r14
	movq	%rcx, %r12
	movq	%rdi, %r15
	cmpb	$0, 32(%r15)
	movq	%rdx, %rax
	cmovneq	%r12, %rax
	cmovneq	%rdx, %r12
	movss	8(%rax), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movss	24(%rax), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	24(%r12), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	40(%r12), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm15         # xmm15 = mem[0],zero,zero,zero
	movss	12(%r12), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	28(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, 96(%rsp)         # 16-byte Spill
	movss	44(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, 80(%rsp)         # 16-byte Spill
	movss	16(%r12), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	56(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	xorps	.LCPI3_0(%rip), %xmm5
	movss	60(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm3
	mulss	%xmm5, %xmm3
	movaps	%xmm11, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm3
	movaps	%xmm13, %xmm2
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm3
	movaps	%xmm3, 288(%rsp)        # 16-byte Spill
	movaps	%xmm10, %xmm3
	mulss	%xmm5, %xmm3
	movaps	%xmm4, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm3
	movaps	%xmm7, %xmm2
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm3
	movaps	%xmm3, 272(%rsp)        # 16-byte Spill
	movss	32(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	mulss	%xmm14, %xmm5
	movaps	%xmm14, 128(%rsp)       # 16-byte Spill
	mulss	%xmm2, %xmm0
	subss	%xmm0, %xmm5
	movss	48(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm5
	movaps	%xmm5, 512(%rsp)        # 16-byte Spill
	movaps	%xmm9, %xmm0
	movaps	%xmm15, %xmm4
	movaps	%xmm4, 224(%rsp)        # 16-byte Spill
	mulss	%xmm4, %xmm0
	movaps	%xmm12, %xmm1
	movaps	%xmm11, %xmm7
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movss	40(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	movaps	%xmm8, 32(%rsp)         # 16-byte Spill
	movaps	%xmm13, 208(%rsp)       # 16-byte Spill
	mulss	%xmm13, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 304(%rsp)        # 16-byte Spill
	movss	12(%rax), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm4, %xmm0
	movss	28(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm1
	movaps	%xmm2, 48(%rsp)         # 16-byte Spill
	mulss	%xmm7, %xmm1
	movaps	%xmm7, 240(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	44(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	mulss	%xmm13, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 320(%rsp)        # 16-byte Spill
	movss	16(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	movaps	%xmm1, %xmm15
	movaps	%xmm15, 144(%rsp)       # 16-byte Spill
	mulss	%xmm4, %xmm0
	movss	32(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	movaps	%xmm3, %xmm6
	movaps	%xmm6, 16(%rsp)         # 16-byte Spill
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movss	48(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	movaps	%xmm3, 160(%rsp)        # 16-byte Spill
	mulss	%xmm13, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 352(%rsp)        # 16-byte Spill
	movaps	%xmm9, %xmm4
	movaps	%xmm4, %xmm0
	movaps	%xmm10, 192(%rsp)       # 16-byte Spill
	mulss	%xmm10, %xmm0
	movaps	%xmm12, %xmm1
	movaps	96(%rsp), %xmm9         # 16-byte Reload
	mulss	%xmm9, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm8, %xmm0
	movaps	80(%rsp), %xmm8         # 16-byte Reload
	mulss	%xmm8, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 528(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm0
	mulss	%xmm10, %xmm0
	mulss	%xmm9, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm5, %xmm0
	movaps	%xmm5, 256(%rsp)        # 16-byte Spill
	mulss	%xmm8, %xmm0
	addss	%xmm2, %xmm0
	movaps	%xmm0, 544(%rsp)        # 16-byte Spill
	movaps	%xmm15, %xmm0
	mulss	%xmm10, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm9, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm3, %xmm0
	mulss	%xmm8, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 336(%rsp)        # 16-byte Spill
	movaps	%xmm4, %xmm0
	movaps	%xmm4, %xmm7
	mulss	%xmm14, %xmm0
	movaps	%xmm12, %xmm13
	movaps	%xmm12, %xmm2
	mulss	64(%rsp), %xmm13        # 16-byte Folded Reload
	addss	%xmm0, %xmm13
	movsd	(%rsi), %xmm3           # xmm3 = mem[0],zero
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	pshufd	$229, %xmm3, %xmm1      # xmm1 = xmm3[1,1,2,3]
	movdqa	%xmm3, %xmm6
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	movaps	%xmm3, %xmm4
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	12(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	.LCPI3_1(%rip), %xmm10  # xmm10 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm10
	movaps	32(%rsp), %xmm3         # 16-byte Reload
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm5, 480(%rsp)        # 16-byte Spill
	movaps	%xmm4, %xmm1
	mulss	%xmm10, %xmm1
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	movdqa	%xmm6, 384(%rsp)        # 16-byte Spill
	pshufd	$225, %xmm6, %xmm0      # xmm0 = xmm6[1,0,2,3]
	movdqa	%xmm0, 416(%rsp)        # 16-byte Spill
	mulps	%xmm0, %xmm10
	movaps	%xmm8, %xmm0
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm10, %xmm8
	mulss	%xmm1, %xmm0
	movaps	%xmm0, 400(%rsp)        # 16-byte Spill
	mulss	%xmm1, %xmm4
	movaps	%xmm4, 464(%rsp)        # 16-byte Spill
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm6, %xmm1
	movaps	%xmm1, %xmm9
	addss	%xmm8, %xmm9
	movaps	%xmm11, %xmm15
	movaps	%xmm15, %xmm12
	movaps	%xmm7, %xmm0
	unpcklps	%xmm0, %xmm12   # xmm12 = xmm12[0],xmm0[0],xmm12[1],xmm0[1]
	mulss	%xmm9, %xmm0
	movaps	%xmm0, 432(%rsp)        # 16-byte Spill
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movaps	%xmm5, %xmm11
	movaps	%xmm2, %xmm0
	unpcklps	%xmm0, %xmm11   # xmm11 = xmm11[0],xmm0[0],xmm11[1],xmm0[1]
	mulss	%xmm9, %xmm0
	movaps	%xmm0, 448(%rsp)        # 16-byte Spill
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm9
	movaps	176(%rsp), %xmm14       # 16-byte Reload
	mulss	%xmm14, %xmm0
	addss	%xmm13, %xmm0
	movaps	%xmm0, 496(%rsp)        # 16-byte Spill
	movaps	%xmm15, %xmm2
	movaps	128(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm4, %xmm2
	movaps	%xmm5, %xmm0
	movaps	64(%rsp), %xmm13        # 16-byte Reload
	mulss	%xmm13, %xmm0
	addss	%xmm2, %xmm0
	movaps	%xmm1, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movaps	%xmm1, %xmm6
	subps	%xmm8, %xmm6
	addps	%xmm8, %xmm1
	shufps	$229, %xmm8, %xmm8      # xmm8 = xmm8[1,1,2,3]
	subss	%xmm8, %xmm3
	mulss	%xmm3, %xmm15
	mulss	%xmm3, %xmm5
	movaps	%xmm5, 48(%rsp)         # 16-byte Spill
	movaps	256(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm2, %xmm3
	movaps	%xmm2, %xmm8
	movaps	%xmm14, %xmm2
	mulss	%xmm2, %xmm8
	addss	%xmm0, %xmm8
	movaps	%xmm8, 256(%rsp)        # 16-byte Spill
	movaps	144(%rsp), %xmm8        # 16-byte Reload
	mulss	%xmm4, %xmm8
	movaps	%xmm4, %xmm14
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	mulss	%xmm13, %xmm0
	addss	%xmm8, %xmm0
	movaps	160(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm2, %xmm4
	movaps	%xmm2, %xmm7
	addss	%xmm0, %xmm4
	movaps	%xmm4, 32(%rsp)         # 16-byte Spill
	movss	56(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm4
	mulss	224(%rsp), %xmm4        # 16-byte Folded Reload
	movss	60(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	mulss	240(%rsp), %xmm2        # 16-byte Folded Reload
	addss	%xmm4, %xmm2
	movss	64(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	mulss	208(%rsp), %xmm4        # 16-byte Folded Reload
	addss	%xmm2, %xmm4
	addss	288(%rsp), %xmm4        # 16-byte Folded Reload
	movss	%xmm4, 12(%rsp)         # 4-byte Spill
	movaps	%xmm8, %xmm2
	mulss	192(%rsp), %xmm2        # 16-byte Folded Reload
	movaps	%xmm0, %xmm4
	mulss	96(%rsp), %xmm4         # 16-byte Folded Reload
	addss	%xmm2, %xmm4
	movaps	%xmm5, %xmm2
	mulss	80(%rsp), %xmm2         # 16-byte Folded Reload
	addss	%xmm4, %xmm2
	addss	272(%rsp), %xmm2        # 16-byte Folded Reload
	movss	%xmm2, 272(%rsp)        # 4-byte Spill
	mulss	%xmm14, %xmm8
	mulss	%xmm13, %xmm0
	addss	%xmm8, %xmm0
	mulss	%xmm7, %xmm5
	addss	%xmm0, %xmm5
	addss	512(%rsp), %xmm5        # 16-byte Folded Reload
	movss	%xmm5, 288(%rsp)        # 4-byte Spill
	movaps	384(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm10, %xmm0
	mulps	416(%rsp), %xmm10       # 16-byte Folded Reload
	movaps	%xmm0, %xmm2
	movaps	400(%rsp), %xmm4        # 16-byte Reload
	addps	%xmm4, %xmm2
	subps	%xmm4, %xmm0
	shufps	$0, %xmm2, %xmm0        # xmm0 = xmm0[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm0      # xmm0 = xmm0[2,0],xmm2[2,3]
	movaps	464(%rsp), %xmm2        # 16-byte Reload
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	addps	%xmm10, %xmm2
	movaps	.LCPI3_2(%rip), %xmm13  # xmm13 = <1,1,u,u>
	subps	%xmm2, %xmm13
	shufps	$1, %xmm6, %xmm1        # xmm1 = xmm1[1,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm1      # xmm1 = xmm1[2,0],xmm6[2,3]
	movaps	%xmm10, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	addss	%xmm10, %xmm5
	movss	.LCPI3_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm2
	mulps	%xmm0, %xmm12
	movsd	8(%rax), %xmm5          # xmm5 = mem[0],zero
	mulps	%xmm13, %xmm5
	addps	%xmm12, %xmm5
	movaps	144(%rsp), %xmm6        # 16-byte Reload
	movaps	%xmm6, %xmm8
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm1, %xmm8
	addps	%xmm5, %xmm8
	addss	432(%rsp), %xmm15       # 16-byte Folded Reload
	mulss	%xmm2, %xmm6
	addss	%xmm15, %xmm6
	movaps	%xmm6, %xmm12
	mulps	%xmm0, %xmm11
	movsd	24(%rax), %xmm7         # xmm7 = mem[0],zero
	mulps	%xmm13, %xmm7
	addps	%xmm11, %xmm7
	movaps	16(%rsp), %xmm4         # 16-byte Reload
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm1, %xmm5
	addps	%xmm7, %xmm5
	movaps	48(%rsp), %xmm6         # 16-byte Reload
	addss	448(%rsp), %xmm6        # 16-byte Folded Reload
	mulss	%xmm2, %xmm4
	addss	%xmm6, %xmm4
	movaps	%xmm4, 16(%rsp)         # 16-byte Spill
	movaps	480(%rsp), %xmm6        # 16-byte Reload
	mulps	%xmm0, %xmm6
	movsd	40(%rax), %xmm0         # xmm0 = mem[0],zero
	mulps	%xmm13, %xmm0
	addps	%xmm6, %xmm0
	movaps	160(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm4, %xmm2
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm1, %xmm4
	addps	%xmm0, %xmm4
	addss	%xmm9, %xmm3
	addss	%xmm3, %xmm2
	movaps	224(%rsp), %xmm15       # 16-byte Reload
	movaps	%xmm15, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm8, %xmm0
	movaps	240(%rsp), %xmm10       # 16-byte Reload
	movaps	%xmm10, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm0, %xmm3
	movaps	208(%rsp), %xmm11       # 16-byte Reload
	movaps	%xmm11, %xmm14
	shufps	$224, %xmm14, %xmm14    # xmm14 = xmm14[0,0,2,3]
	mulps	%xmm4, %xmm14
	addps	%xmm3, %xmm14
	movaps	192(%rsp), %xmm13       # 16-byte Reload
	movaps	%xmm13, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm8, %xmm0
	movaps	96(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm0, %xmm3
	movaps	80(%rsp), %xmm7         # 16-byte Reload
	movaps	%xmm7, %xmm9
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	mulps	%xmm4, %xmm9
	addps	%xmm3, %xmm9
	mulss	%xmm12, %xmm15
	mulss	%xmm12, %xmm13
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm0, %xmm12
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm8, %xmm3
	movaps	%xmm10, %xmm0
	movaps	16(%rsp), %xmm8         # 16-byte Reload
	mulss	%xmm8, %xmm0
	movaps	%xmm1, %xmm10
	mulss	%xmm8, %xmm10
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm1, %xmm8
	movaps	%xmm1, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm5, %xmm6
	addps	%xmm3, %xmm6
	movaps	%xmm11, %xmm5
	mulss	%xmm2, %xmm5
	mulss	%xmm2, %xmm7
	movaps	176(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm1, %xmm2
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	addps	%xmm6, %xmm3
	addss	%xmm15, %xmm0
	addss	%xmm0, %xmm5
	movaps	%xmm5, %xmm11
	movaps	%xmm10, %xmm0
	addss	%xmm13, %xmm0
	addss	%xmm0, %xmm7
	addss	%xmm12, %xmm8
	addss	%xmm8, %xmm2
	movq	200(%r12), %rbx
	movd	60(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movdqa	.LCPI3_0(%rip), %xmm6   # xmm6 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm6, %xmm4
	pshufd	$224, %xmm4, %xmm5      # xmm5 = xmm4[0,0,2,3]
	mulps	%xmm14, %xmm5
	movss	64(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	xorps	%xmm6, %xmm1
	pshufd	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm9, %xmm1
	addps	%xmm5, %xmm1
	movd	68(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	pxor	%xmm0, %xmm6
	pshufd	$224, %xmm6, %xmm5      # xmm5 = xmm6[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm1, %xmm5
	mulss	%xmm11, %xmm4
	subss	%xmm7, %xmm4
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movq	200(%rax), %rdi
	movq	(%rdi), %rax
	movq	96(%rax), %rax
	movlps	%xmm5, 112(%rsp)
	movlps	%xmm0, 120(%rsp)
	leaq	112(%rsp), %rsi
	callq	*%rax
	movaps	304(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm0, %xmm2
	movaps	528(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm0, %xmm4
	movaps	496(%rsp), %xmm6        # 16-byte Reload
	mulss	%xmm0, %xmm6
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	320(%rsp), %xmm3        # 16-byte Reload
	mulss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	movaps	352(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	addss	12(%rsp), %xmm2         # 4-byte Folded Reload
	movaps	544(%rsp), %xmm5        # 16-byte Reload
	mulss	%xmm0, %xmm5
	addss	%xmm4, %xmm5
	movaps	336(%rsp), %xmm3        # 16-byte Reload
	mulss	%xmm1, %xmm3
	addss	%xmm5, %xmm3
	addss	272(%rsp), %xmm3        # 4-byte Folded Reload
	mulss	256(%rsp), %xmm0        # 16-byte Folded Reload
	addss	%xmm6, %xmm0
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	mulss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	addss	288(%rsp), %xmm4        # 4-byte Folded Reload
	movss	60(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm0, 176(%rsp)        # 4-byte Spill
	movaps	%xmm2, 352(%rsp)        # 16-byte Spill
	mulss	%xmm2, %xmm0
	movss	%xmm1, 208(%rsp)        # 4-byte Spill
	movaps	%xmm3, 336(%rsp)        # 16-byte Spill
	mulss	%xmm3, %xmm1
	addss	%xmm0, %xmm1
	movss	68(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 144(%rsp)        # 4-byte Spill
	movaps	%xmm4, 32(%rsp)         # 16-byte Spill
	mulss	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	subss	76(%rbx), %xmm0
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 128(%rsp)        # 16-byte Spill
	movss	12(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
	movss	16(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movss	24(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 304(%rsp)        # 16-byte Spill
	movss	28(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 320(%rsp)        # 16-byte Spill
	movss	32(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	movsd	56(%r12), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movss	40(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 240(%rsp)        # 4-byte Spill
	movss	44(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
	movss	48(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 64(%rsp)         # 4-byte Spill
	movss	64(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
	movq	24(%r15), %rdi
	callq	_ZNK20btPersistentManifold27getContactBreakingThresholdEv
	movss	16(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movq	24(%r15), %rax
	movq	%rax, 8(%r14)
	ucomiss	%xmm7, %xmm0
	jbe	.LBB3_2
# BB#1:
	movss	176(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	movaps	352(%rsp), %xmm2        # 16-byte Reload
	subss	%xmm0, %xmm2
	movss	208(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	movaps	336(%rsp), %xmm3        # 16-byte Reload
	subss	%xmm0, %xmm3
	movss	144(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	subss	%xmm0, %xmm4
	movaps	128(%rsp), %xmm1        # 16-byte Reload
	unpcklps	304(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	movaps	%xmm2, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	movaps	160(%rsp), %xmm5        # 16-byte Reload
	unpcklps	320(%rsp), %xmm5 # 16-byte Folded Reload
                                        # xmm5 = xmm5[0],mem[0],xmm5[1],mem[1]
	movaps	%xmm3, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm5, %xmm1
	addps	%xmm0, %xmm1
	movaps	224(%rsp), %xmm5        # 16-byte Reload
	unpcklps	192(%rsp), %xmm5 # 16-byte Folded Reload
                                        # xmm5 = xmm5[0],mem[0],xmm5[1],mem[1]
	movaps	%xmm4, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm5, %xmm0
	addps	%xmm1, %xmm0
	movaps	96(%rsp), %xmm8         # 16-byte Reload
	addps	%xmm0, %xmm8
	mulss	240(%rsp), %xmm2        # 4-byte Folded Reload
	mulss	48(%rsp), %xmm3         # 4-byte Folded Reload
	addss	%xmm2, %xmm3
	mulss	64(%rsp), %xmm4         # 4-byte Folded Reload
	addss	%xmm3, %xmm4
	movss	80(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movss	60(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	12(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm3, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	movss	28(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	movaps	%xmm2, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm6, %xmm5
	addps	%xmm4, %xmm5
	movss	32(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	16(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm6, %xmm4
	addps	%xmm5, %xmm4
	mulss	40(%r12), %xmm3
	mulss	44(%r12), %xmm2
	addss	%xmm3, %xmm2
	mulss	48(%r12), %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm4, 112(%rsp)
	movlps	%xmm2, 120(%rsp)
	movlps	%xmm8, 368(%rsp)
	movlps	%xmm0, 376(%rsp)
	movq	(%r14), %rax
	leaq	112(%rsp), %rsi
	leaq	368(%rsp), %rdx
	movq	%r14, %rdi
	movaps	%xmm7, %xmm0
	callq	*32(%rax)
.LBB3_2:
	addq	$568, %rsp              # imm = 0x238
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZN31btConvexPlaneCollisionAlgorithm20collideSingleContactERK12btQuaternionP17btCollisionObjectS4_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end3-_ZN31btConvexPlaneCollisionAlgorithm20collideSingleContactERK12btQuaternionP17btCollisionObjectS4_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1065353216              # 0x3f800000
.LCPI4_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI4_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_2:
	.long	1060439283              # float 0.707106769
.LCPI4_3:
	.long	1065353216              # float 1
.LCPI4_5:
	.long	1053364187              # float 0.392699093
.LCPI4_6:
	.long	1056964608              # float 0.5
.LCPI4_7:
	.long	1086918619              # float 6.28318548
	.text
	.globl	_ZN31btConvexPlaneCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN31btConvexPlaneCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN31btConvexPlaneCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN31btConvexPlaneCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 208
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	cmpq	$0, 24(%rbx)
	je	.LBB4_23
# BB#1:
	cmpb	$0, 32(%rbx)
	movq	%r12, %rax
	cmovneq	%r15, %rax
	movq	%r15, %rcx
	cmovneq	%r12, %rcx
	movq	200(%rax), %r13
	movq	200(%rcx), %rbp
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [0,0,0,1065353216]
	movaps	%xmm0, 80(%rsp)
	leaq	80(%rsp), %rsi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r14, %r9
	callq	_ZN31btConvexPlaneCollisionAlgorithm20collideSingleContactERK12btQuaternionP17btCollisionObjectS4_RK16btDispatcherInfoP16btManifoldResult
	movq	8(%r14), %rax
	movl	728(%rax), %eax
	cmpl	40(%rbx), %eax
	jge	.LBB4_16
# BB#2:
	movss	68(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	.LCPI4_1(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	andps	%xmm0, %xmm1
	ucomiss	.LCPI4_2(%rip), %xmm1
	jbe	.LBB4_6
# BB#3:
	movss	64(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB4_5
# BB#4:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB4_5:                                # %.split
	movss	.LCPI4_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	68(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	xorps	.LCPI4_4(%rip), %xmm1
	mulss	64(%rbp), %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	shufps	$0, %xmm0, %xmm1        # xmm1 = xmm1[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm1      # xmm1 = xmm1[2,0],xmm0[2,3]
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	jmp	.LBB4_9
.LBB4_6:
	movss	60(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	64(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB4_8
# BB#7:                                 # %call.sqrt106
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB4_8:                                # %.split105
	movss	.LCPI4_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	64(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	xorps	.LCPI4_4(%rip), %xmm1
	mulss	60(%rbp), %xmm0
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
.LBB4_9:                                # %_Z13btPlaneSpace1RK9btVector3RS_S2_.exit
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*32(%rax)
	movss	gContactBreakingThreshold(%rip), %xmm1 # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movss	.LCPI4_5(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	minss	%xmm1, %xmm2
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm1, %xmm0
	mulss	%xmm0, %xmm0
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB4_11
# BB#10:                                # %call.sqrt107
	movss	%xmm2, (%rsp)           # 4-byte Spill
	callq	sqrtf
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB4_11:                               # %_Z13btPlaneSpace1RK9btVector3RS_S2_.exit.split
	movss	%xmm1, 64(%rsp)         # 4-byte Spill
	mulss	.LCPI4_6(%rip), %xmm2
	movss	%xmm2, (%rsp)           # 4-byte Spill
	movaps	%xmm2, %xmm0
	callq	sinf
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movss	(%rsp), %xmm0           # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	cosf
	movaps	%xmm0, 128(%rsp)        # 16-byte Spill
	movl	36(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB4_16
# BB#12:                                # %.lr.ph
	movaps	48(%rsp), %xmm2         # 16-byte Reload
	divss	64(%rsp), %xmm2         # 4-byte Folded Reload
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm2, %xmm1
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	mulps	%xmm2, %xmm0
	movaps	%xmm0, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	%xmm2, 112(%rsp)        # 16-byte Spill
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movaps	%xmm1, 96(%rsp)         # 16-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_13:                               # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r13d, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	movss	.LCPI4_7(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
	movss	60(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	68(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB4_15
# BB#14:                                # %call.sqrt109
                                        #   in Loop: Header=BB4_13 Depth=1
	movss	%xmm2, 48(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	48(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB4_15:                               # %.split108
                                        #   in Loop: Header=BB4_13 Depth=1
	movss	%xmm1, (%rsp)           # 4-byte Spill
	mulss	.LCPI4_6(%rip), %xmm2
	movss	%xmm2, 48(%rsp)         # 4-byte Spill
	movaps	%xmm2, %xmm0
	callq	sinf
	divss	(%rsp), %xmm0           # 4-byte Folded Reload
	movss	64(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movss	60(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	68(%rbp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	mulps	%xmm0, %xmm2
	movaps	%xmm2, (%rsp)           # 16-byte Spill
	movss	48(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	cosf
	movdqa	64(%rsp), %xmm3         # 16-byte Reload
	movdqa	%xmm3, %xmm5
	movdqa	.LCPI4_4(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm1, %xmm5
	movdqa	(%rsp), %xmm11          # 16-byte Reload
	pxor	%xmm1, %xmm11
	movaps	32(%rsp), %xmm12        # 16-byte Reload
	movaps	%xmm12, %xmm2
	mulss	%xmm0, %xmm2
	pshufd	$229, %xmm11, %xmm9     # xmm9 = xmm11[1,1,2,3]
	movaps	128(%rsp), %xmm10       # 16-byte Reload
	movaps	%xmm10, %xmm15
	mulss	%xmm9, %xmm15
	addss	%xmm2, %xmm15
	movaps	16(%rsp), %xmm13        # 16-byte Reload
	movaps	%xmm13, %xmm2
	mulss	%xmm3, %xmm2
	subss	%xmm2, %xmm15
	movaps	112(%rsp), %xmm14       # 16-byte Reload
	movaps	%xmm14, %xmm2
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm15
	movaps	%xmm0, %xmm3
	movaps	%xmm10, %xmm6
	mulss	%xmm0, %xmm6
	movaps	%xmm0, %xmm8
	movaps	%xmm0, %xmm7
	shufps	$0, %xmm10, %xmm7       # xmm7 = xmm7[0,0],xmm10[0,0]
	shufps	$226, %xmm10, %xmm7     # xmm7 = xmm7[2,0],xmm10[2,3]
	movdqa	%xmm5, %xmm4
	unpcklps	%xmm13, %xmm4   # xmm4 = xmm4[0],xmm13[0],xmm4[1],xmm13[1]
	mulps	%xmm7, %xmm4
	movaps	%xmm11, %xmm7
	shufps	$16, %xmm12, %xmm7      # xmm7 = xmm7[0,0],xmm12[1,0]
	shufps	$226, %xmm12, %xmm7     # xmm7 = xmm7[2,0],xmm12[2,3]
	unpcklps	%xmm10, %xmm3   # xmm3 = xmm3[0],xmm10[0],xmm3[1],xmm10[1]
	mulps	%xmm7, %xmm3
	movaps	%xmm0, %xmm10
	addps	%xmm4, %xmm3
	movaps	%xmm12, %xmm4
	movaps	(%rsp), %xmm1           # 16-byte Reload
	mulps	%xmm1, %xmm4
	subps	%xmm4, %xmm3
	shufps	$0, %xmm9, %xmm5        # xmm5 = xmm5[0,0],xmm9[0,0]
	shufps	$226, %xmm9, %xmm5      # xmm5 = xmm5[2,0],xmm9[2,3]
	mulps	96(%rsp), %xmm5         # 16-byte Folded Reload
	subps	%xmm5, %xmm3
	mulss	%xmm12, %xmm9
	subss	%xmm9, %xmm6
	movaps	%xmm14, %xmm7
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	mulss	%xmm2, %xmm7
	addss	%xmm6, %xmm7
	mulss	%xmm13, %xmm11
	subss	%xmm11, %xmm7
	unpcklps	%xmm2, %xmm8    # xmm8 = xmm8[0],xmm2[0],xmm8[1],xmm2[1]
	movaps	%xmm2, %xmm12
	movaps	%xmm1, %xmm11
	movaps	%xmm1, %xmm2
	shufps	$229, %xmm11, %xmm11    # xmm11 = xmm11[1,1,2,3]
	shufps	$0, %xmm11, %xmm10      # xmm10 = xmm10[0,0],xmm11[0,0]
	shufps	$226, %xmm11, %xmm10    # xmm10 = xmm10[2,0],xmm11[2,3]
	movaps	%xmm15, %xmm13
	movaps	%xmm12, %xmm6
	mulss	%xmm15, %xmm6
	movaps	%xmm11, %xmm9
	mulss	%xmm15, %xmm11
	movaps	%xmm15, %xmm1
	unpcklps	%xmm7, %xmm1    # xmm1 = xmm1[0],xmm7[0],xmm1[1],xmm7[1]
	mulps	%xmm8, %xmm1
	movaps	%xmm7, %xmm4
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	mulps	%xmm10, %xmm4
	addps	%xmm1, %xmm4
	movaps	%xmm2, %xmm5
	movaps	%xmm5, %xmm1
	mulps	%xmm3, %xmm1
	addps	%xmm4, %xmm1
	movaps	%xmm12, %xmm4
	unpcklps	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movaps	%xmm3, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	shufps	$0, %xmm2, %xmm13       # xmm13 = xmm13[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm13     # xmm13 = xmm13[2,0],xmm2[2,3]
	mulps	%xmm4, %xmm13
	subps	%xmm13, %xmm1
	movaps	%xmm5, %xmm4
	mulss	%xmm7, %xmm4
	mulss	%xmm0, %xmm7
	mulss	%xmm2, %xmm0
	addss	%xmm4, %xmm0
	addss	%xmm0, %xmm6
	mulss	%xmm3, %xmm9
	subss	%xmm9, %xmm6
	subss	%xmm11, %xmm7
	mulss	%xmm12, %xmm3
	subss	%xmm3, %xmm7
	mulss	%xmm5, %xmm2
	subss	%xmm2, %xmm7
	unpcklps	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	movlps	%xmm1, 80(%rsp)
	movlps	%xmm6, 88(%rsp)
	movq	%rbx, %rdi
	leaq	80(%rsp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r14, %r9
	callq	_ZN31btConvexPlaneCollisionAlgorithm20collideSingleContactERK12btQuaternionP17btCollisionObjectS4_RK16btDispatcherInfoP16btManifoldResult
	incl	%r13d
	movl	36(%rbx), %eax
	cmpl	%eax, %r13d
	jl	.LBB4_13
.LBB4_16:                               # %.loopexit
	cmpb	$0, 16(%rbx)
	je	.LBB4_23
# BB#17:
	movq	24(%rbx), %rax
	cmpl	$0, 728(%rax)
	je	.LBB4_23
# BB#18:
	movq	8(%r14), %rdi
	cmpl	$0, 728(%rdi)
	je	.LBB4_23
# BB#19:
	movq	712(%rdi), %rax
	cmpq	144(%r14), %rax
	je	.LBB4_22
# BB#20:
	leaq	80(%r14), %rsi
	addq	$16, %r14
	jmp	.LBB4_21
.LBB4_23:                               # %_ZN16btManifoldResult20refreshContactPointsEv.exit
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_22:
	leaq	16(%r14), %rsi
	addq	$80, %r14
.LBB4_21:
	movq	%r14, %rdx
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_ # TAILCALL
.Lfunc_end4:
	.size	_ZN31btConvexPlaneCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end4-_ZN31btConvexPlaneCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN31btConvexPlaneCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN31btConvexPlaneCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN31btConvexPlaneCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN31btConvexPlaneCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	movss	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end5:
	.size	_ZN31btConvexPlaneCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end5-_ZN31btConvexPlaneCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.text._ZN31btConvexPlaneCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,"axG",@progbits,_ZN31btConvexPlaneCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,comdat
	.weak	_ZN31btConvexPlaneCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.p2align	4, 0x90
	.type	_ZN31btConvexPlaneCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,@function
_ZN31btConvexPlaneCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE: # @_ZN31btConvexPlaneCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 48
.Lcfi45:
	.cfi_offset %rbx, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	24(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB6_18
# BB#1:
	cmpb	$0, 16(%r14)
	je	.LBB6_18
# BB#2:
	movl	4(%rbx), %eax
	cmpl	8(%rbx), %eax
	jne	.LBB6_17
# BB#3:
	leal	(%rax,%rax), %edx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%edx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB6_17
# BB#4:
	testl	%ebp, %ebp
	je	.LBB6_5
# BB#6:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB6_8
	jmp	.LBB6_12
.LBB6_5:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB6_12
.LBB6_8:                                # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB6_10
	.p2align	4, 0x90
.LBB6_9:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB6_9
.LBB6_10:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB6_12
	.p2align	4, 0x90
.LBB6_11:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB6_11
.LBB6_12:                               # %_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_.exit.i.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_16
# BB#13:
	cmpb	$0, 24(%rbx)
	je	.LBB6_15
# BB#14:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%rbx), %eax
.LBB6_15:
	movq	$0, 16(%rbx)
.LBB6_16:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv.exit.i.i
	movb	$1, 24(%rbx)
	movq	%r15, 16(%rbx)
	movl	%ebp, 8(%rbx)
	movq	24(%r14), %rcx
.LBB6_17:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_.exit
	movq	16(%rbx), %rdx
	cltq
	movq	%rcx, (%rdx,%rax,8)
	incl	%eax
	movl	%eax, 4(%rbx)
.LBB6_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN31btConvexPlaneCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE, .Lfunc_end6-_ZN31btConvexPlaneCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_endproc

	.type	_ZTV31btConvexPlaneCollisionAlgorithm,@object # @_ZTV31btConvexPlaneCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTV31btConvexPlaneCollisionAlgorithm
	.p2align	3
_ZTV31btConvexPlaneCollisionAlgorithm:
	.quad	0
	.quad	_ZTI31btConvexPlaneCollisionAlgorithm
	.quad	_ZN31btConvexPlaneCollisionAlgorithmD2Ev
	.quad	_ZN31btConvexPlaneCollisionAlgorithmD0Ev
	.quad	_ZN31btConvexPlaneCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN31btConvexPlaneCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN31btConvexPlaneCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.size	_ZTV31btConvexPlaneCollisionAlgorithm, 56

	.type	_ZTS31btConvexPlaneCollisionAlgorithm,@object # @_ZTS31btConvexPlaneCollisionAlgorithm
	.globl	_ZTS31btConvexPlaneCollisionAlgorithm
	.p2align	4
_ZTS31btConvexPlaneCollisionAlgorithm:
	.asciz	"31btConvexPlaneCollisionAlgorithm"
	.size	_ZTS31btConvexPlaneCollisionAlgorithm, 34

	.type	_ZTS20btCollisionAlgorithm,@object # @_ZTS20btCollisionAlgorithm
	.section	.rodata._ZTS20btCollisionAlgorithm,"aG",@progbits,_ZTS20btCollisionAlgorithm,comdat
	.weak	_ZTS20btCollisionAlgorithm
	.p2align	4
_ZTS20btCollisionAlgorithm:
	.asciz	"20btCollisionAlgorithm"
	.size	_ZTS20btCollisionAlgorithm, 23

	.type	_ZTI20btCollisionAlgorithm,@object # @_ZTI20btCollisionAlgorithm
	.section	.rodata._ZTI20btCollisionAlgorithm,"aG",@progbits,_ZTI20btCollisionAlgorithm,comdat
	.weak	_ZTI20btCollisionAlgorithm
	.p2align	3
_ZTI20btCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS20btCollisionAlgorithm
	.size	_ZTI20btCollisionAlgorithm, 16

	.type	_ZTI31btConvexPlaneCollisionAlgorithm,@object # @_ZTI31btConvexPlaneCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTI31btConvexPlaneCollisionAlgorithm
	.p2align	4
_ZTI31btConvexPlaneCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS31btConvexPlaneCollisionAlgorithm
	.quad	_ZTI20btCollisionAlgorithm
	.size	_ZTI31btConvexPlaneCollisionAlgorithm, 24


	.globl	_ZN31btConvexPlaneCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_bii
	.type	_ZN31btConvexPlaneCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_bii,@function
_ZN31btConvexPlaneCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_bii = _ZN31btConvexPlaneCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_bii
	.globl	_ZN31btConvexPlaneCollisionAlgorithmD1Ev
	.type	_ZN31btConvexPlaneCollisionAlgorithmD1Ev,@function
_ZN31btConvexPlaneCollisionAlgorithmD1Ev = _ZN31btConvexPlaneCollisionAlgorithmD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
