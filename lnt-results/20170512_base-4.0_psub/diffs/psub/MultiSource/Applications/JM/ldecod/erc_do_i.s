	.text
	.file	"erc_do_i.bc"
	.globl	ercConcealIntraFrame
	.p2align	4, 0x90
	.type	ercConcealIntraFrame,@function
ercConcealIntraFrame:                   # @ercConcealIntraFrame
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movl	%edx, %r12d
	movl	%esi, %r15d
	movq	%rdi, %r14
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	je	.LBB0_5
# BB#1:
	cmpl	$0, 64(%rbx)
	je	.LBB0_5
# BB#2:
	cmpl	$0, 60(%rbx)
	je	.LBB0_3
# BB#4:
	movl	%r12d, %esi
	sarl	$3, %esi
	movl	%r15d, %edi
	sarl	$3, %edi
	movq	8(%rbx), %r9
	xorl	%edx, %edx
	movq	%r14, %rcx
	movl	%r15d, %r8d
	callq	concealBlocks
	sarl	$4, %r12d
	movl	%r15d, %r13d
	sarl	$4, %r13d
	movq	16(%rbx), %r9
	movl	$1, %ebp
	movl	$1, %edx
	movl	%r13d, %edi
	movl	%r12d, %esi
	movq	%r14, %rcx
	movl	%r15d, %r8d
	callq	concealBlocks
	movq	24(%rbx), %r9
	movl	$2, %edx
	movl	%r13d, %edi
	movl	%r12d, %esi
	movq	%r14, %rcx
	movl	%r15d, %r8d
	callq	concealBlocks
	jmp	.LBB0_5
.LBB0_3:
	movl	$1, %ebp
.LBB0_5:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	ercConcealIntraFrame, .Lfunc_end0-ercConcealIntraFrame
	.cfi_endproc

	.p2align	4, 0x90
	.type	concealBlocks,@function
concealBlocks:                          # @concealBlocks
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 240
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movl	%esi, %r10d
	movl	%edi, %r9d
	movl	%edx, 20(%rsp)          # 4-byte Spill
	cmpl	$1, %edx
	movl	$1, %r11d
	adcl	$0, %r11d
	testl	%r9d, %r9d
	jle	.LBB1_53
# BB#1:                                 # %.preheader207.lr.ph
	movl	%r8d, %eax
	sarl	%eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movslq	%r10d, %rax
	movslq	%r9d, %rbx
	movl	%r11d, %edx
	negl	%edx
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	movq	%rbx, %rbp
	imulq	%r11, %rbp
	shlq	$2, %rbp
	leaq	(,%r11,4), %rdx
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	leaq	(,%rbx,4), %rdx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movl	%r11d, %edx
	imull	%r9d, %edx
	movl	%edx, 92(%rsp)          # 4-byte Spill
	xorl	%edx, %edx
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	xorl	%esi, %esi
	movl	%r8d, 44(%rsp)          # 4-byte Spill
	movq	%rax, %r8
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	%r10d, 28(%rsp)         # 4-byte Spill
	movl	%r9d, 24(%rsp)          # 4-byte Spill
	movq	%r11, 56(%rsp)          # 8-byte Spill
	movq	%r8, 72(%rsp)           # 8-byte Spill
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%r13, 120(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader207
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
                                        #       Child Loop BB1_6 Depth 3
                                        #       Child Loop BB1_13 Depth 3
                                        #       Child Loop BB1_17 Depth 3
                                        #       Child Loop BB1_40 Depth 3
                                        #       Child Loop BB1_44 Depth 3
                                        #       Child Loop BB1_27 Depth 3
                                        #       Child Loop BB1_31 Depth 3
	testl	%r10d, %r10d
	jle	.LBB1_52
# BB#3:                                 # %.lr.ph220.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	xorl	%eax, %eax
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph220
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_6 Depth 3
                                        #       Child Loop BB1_13 Depth 3
                                        #       Child Loop BB1_17 Depth 3
                                        #       Child Loop BB1_40 Depth 3
                                        #       Child Loop BB1_44 Depth 3
                                        #       Child Loop BB1_27 Depth 3
                                        #       Child Loop BB1_31 Depth 3
	movl	%eax, %edx
	imull	%r9d, %edx
	addl	%esi, %edx
	movslq	%edx, %rdx
	cmpl	$1, (%r13,%rdx,4)
	jg	.LBB1_51
# BB#5:                                 # %.preheader206.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	movl	%eax, %r12d
	movslq	%eax, %r15
	leaq	(%r11,%r15), %rdx
	imulq	%rbx, %rdx
	addq	%rsi, %rdx
	leaq	(%r13,%rdx,4), %rdx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader206
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%r14,%r11), %rdi
	leaq	(%r15,%rdi), %rsi
	cmpq	%r8, %rsi
	jge	.LBB1_54
# BB#7:                                 #   in Loop: Header=BB1_6 Depth=3
	cmpl	$1, (%rdx)
	leaq	(%rdx,%rbp), %rdx
	movq	%rdi, %r14
	jle	.LBB1_6
# BB#8:                                 # %._crit_edge250
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	176(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rax), %edx
	addl	%edx, %edi
	movl	%edi, %r14d
	cmpl	%r10d, %r14d
	jl	.LBB1_23
	jmp	.LBB1_10
	.p2align	4, 0x90
.LBB1_54:                               # %split
                                        #   in Loop: Header=BB1_4 Depth=2
	leal	(%r11,%rax), %edx
	addl	%edx, %r14d
	cmpl	%r10d, %r14d
	jge	.LBB1_10
.LBB1_23:                               #   in Loop: Header=BB1_4 Depth=2
	testl	%eax, %eax
	je	.LBB1_24
# BB#36:                                #   in Loop: Header=BB1_4 Depth=2
	leal	(%r14,%r11), %edx
	movl	%r11d, %esi
	subl	%eax, %esi
	addl	%r14d, %esi
	testl	%esi, %esi
	jle	.LBB1_37
# BB#38:                                # %.lr.ph212
                                        #   in Loop: Header=BB1_4 Depth=2
	movl	%edx, 96(%rsp)          # 4-byte Spill
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	movl	%esi, 104(%rsp)         # 4-byte Spill
	je	.LBB1_43
# BB#39:                                # %.lr.ph212.split.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	xorl	%ebx, %ebx
	movq	%r12, %rdx
	movq	8(%rsp), %r12           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_40:                               # %.lr.ph212.split
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebx, %eax
	andl	$1, %eax
	movl	$0, %r15d
	cmovel	%r11d, %r15d
	movq	%r13, %rcx
	movl	%eax, %r13d
	negl	%r13d
	andl	%r11d, %r13d
	testl	%eax, %eax
	movl	%r14d, %ebp
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	cmovel	%edx, %ebp
	leaq	128(%rsp), %rdi
	movl	%ebp, %esi
	movl	%r12d, %edx
	movl	%r10d, %r8d
	pushq	$1
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	callq	ercCollect8PredBlocks
	addq	$16, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset -16
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	$2, %eax
	je	.LBB1_45
# BB#41:                                # %.lr.ph212.split
                                        #   in Loop: Header=BB1_40 Depth=3
	cmpl	$1, %eax
	movq	8(%rsp), %r12           # 8-byte Reload
	jne	.LBB1_47
# BB#42:                                #   in Loop: Header=BB1_40 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rdi
	movl	$1, %r9d
	movl	%ebp, %esi
	jmp	.LBB1_46
	.p2align	4, 0x90
.LBB1_45:                               #   in Loop: Header=BB1_40 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rdi
	movl	$1, %r9d
	movl	%ebp, %esi
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB1_46:                               #   in Loop: Header=BB1_40 Depth=3
	movl	%r12d, %edx
	leaq	128(%rsp), %rcx
	movl	16(%rsp), %r8d          # 4-byte Reload
	callq	ercPixConcealIMB
.LBB1_47:                               #   in Loop: Header=BB1_40 Depth=3
	subl	%r13d, %r14d
	movq	80(%rsp), %rdx          # 8-byte Reload
	addl	%r15d, %edx
	movl	24(%rsp), %r9d          # 4-byte Reload
	imull	%r9d, %ebp
	addl	%r12d, %ebp
	movslq	%ebp, %rax
	movq	120(%rsp), %r13         # 8-byte Reload
	movl	$2, (%r13,%rax,4)
	movq	56(%rsp), %r11          # 8-byte Reload
	addl	%r11d, %ebx
	cmpl	104(%rsp), %ebx         # 4-byte Folded Reload
	movl	28(%rsp), %r10d         # 4-byte Reload
	jl	.LBB1_40
	jmp	.LBB1_48
	.p2align	4, 0x90
.LBB1_10:                               # %.preheader204
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpl	%r10d, %eax
	movl	%r10d, %eax
	jge	.LBB1_51
# BB#11:                                # %.lr.ph
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB1_16
# BB#12:                                # %.lr.ph.split.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	%rbx, %rax
	imulq	%r15, %rax
	addq	8(%rsp), %rax           # 8-byte Folded Reload
	leaq	(%r13,%rax,4), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_13:                               # %.lr.ph.split
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%r12,%rbp), %r14
	leaq	128(%rsp), %rdi
	movl	%r14d, %esi
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%r13, %rcx
	movl	%r10d, %r8d
	pushq	$1
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	callq	ercCollect8PredBlocks
	addq	$16, %rsp
.Lcfi31:
	.cfi_adjust_cfa_offset -16
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	$2, %eax
	je	.LBB1_19
# BB#14:                                # %.lr.ph.split
                                        #   in Loop: Header=BB1_13 Depth=3
	cmpl	$1, %eax
	jne	.LBB1_21
# BB#15:                                #   in Loop: Header=BB1_13 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rdi
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_19:                               #   in Loop: Header=BB1_13 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rdi
.LBB1_20:                               #   in Loop: Header=BB1_13 Depth=3
	movl	$1, %r9d
	movl	%r14d, %esi
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	leaq	128(%rsp), %rcx
	movl	16(%rsp), %r8d          # 4-byte Reload
	callq	ercPixConcealIMB
.LBB1_21:                               #   in Loop: Header=BB1_13 Depth=3
	movl	$2, (%rbx)
	movq	56(%rsp), %r11          # 8-byte Reload
	addq	%r11, %rbp
	addq	48(%rsp), %rbx          # 8-byte Folded Reload
	leaq	(%r15,%rbp), %rax
	movq	72(%rsp), %r8           # 8-byte Reload
	cmpq	%r8, %rax
	movl	28(%rsp), %r10d         # 4-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	jl	.LBB1_13
# BB#22:                                #   in Loop: Header=BB1_4 Depth=2
	movl	%r10d, %eax
	jmp	.LBB1_50
.LBB1_24:                               # %.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	testl	%r14d, %r14d
	movq	8(%rsp), %r15           # 8-byte Reload
	js	.LBB1_35
# BB#25:                                # %.lr.ph216
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB1_30
# BB#26:                                # %.lr.ph216.split.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	movl	%r9d, %ebx
	imull	%r14d, %ebx
	addl	%r15d, %ebx
	movl	%r14d, %r12d
	.p2align	4, 0x90
.LBB1_27:                               # %.lr.ph216.split
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	128(%rsp), %rbp
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	movq	%r13, %rcx
	movl	%r10d, %r8d
	pushq	$1
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	callq	ercCollect8PredBlocks
	addq	$16, %rsp
.Lcfi34:
	.cfi_adjust_cfa_offset -16
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	$2, %eax
	je	.LBB1_32
# BB#28:                                # %.lr.ph216.split
                                        #   in Loop: Header=BB1_27 Depth=3
	cmpl	$1, %eax
	jne	.LBB1_34
# BB#29:                                #   in Loop: Header=BB1_27 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rdi
	jmp	.LBB1_33
	.p2align	4, 0x90
.LBB1_32:                               #   in Loop: Header=BB1_27 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rdi
.LBB1_33:                               #   in Loop: Header=BB1_27 Depth=3
	movl	$1, %r9d
	movl	%r12d, %esi
	movl	%r15d, %edx
	movq	%rbp, %rcx
	movl	16(%rsp), %r8d          # 4-byte Reload
	callq	ercPixConcealIMB
.LBB1_34:                               #   in Loop: Header=BB1_27 Depth=3
	movslq	%ebx, %rbx
	movl	$2, (%r13,%rbx,4)
	subl	92(%rsp), %ebx          # 4-byte Folded Reload
	movq	56(%rsp), %r11          # 8-byte Reload
	subl	%r11d, %r12d
	movl	28(%rsp), %r10d         # 4-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	jns	.LBB1_27
	jmp	.LBB1_35
.LBB1_37:                               #   in Loop: Header=BB1_4 Depth=2
	movl	%edx, %eax
	jmp	.LBB1_51
.LBB1_16:                               # %.preheader239.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	leaq	1(%r15), %rax
	imulq	160(%rsp), %rax         # 8-byte Folded Reload
	addq	112(%rsp), %rax         # 8-byte Folded Reload
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%rbx, %rax
	imulq	%r15, %rax
	movq	%r12, 80(%rsp)          # 8-byte Spill
	addq	8(%rsp), %rax           # 8-byte Folded Reload
	leaq	(%r13,%rax,4), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_17:                               # %.preheader239
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	80(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rbx), %ebp
	leaq	128(%rsp), %r12
	movq	%r12, %rdi
	movl	%ebp, %esi
	movq	8(%rsp), %r13           # 8-byte Reload
	movl	%r13d, %edx
	movq	120(%rsp), %r12         # 8-byte Reload
	movq	%r12, %rcx
	movl	%r10d, %r8d
	pushq	$1
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	callq	ercCollect8PredBlocks
	addq	$16, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset -16
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movl	$2, %r9d
	movl	%ebp, %esi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movl	%r13d, %edx
	leaq	128(%rsp), %rcx
	movl	44(%rsp), %r8d          # 4-byte Reload
	callq	ercPixConcealIMB
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	56(%rsp), %r11          # 8-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	movl	28(%rsp), %r10d         # 4-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	$2, (%rax,%r14)
	movl	$2, 4(%rax,%r14)
	leaq	(%r12,%r14), %rax
	movq	104(%rsp), %rcx         # 8-byte Reload
	movabsq	$8589934594, %rdx       # imm = 0x200000002
	movq	%rdx, (%rcx,%rax)
	addq	%rbp, %r14
	addq	%r11, %rbx
	leaq	(%r15,%rbx), %rax
	cmpq	%r8, %rax
	jl	.LBB1_17
# BB#18:                                #   in Loop: Header=BB1_4 Depth=2
	movl	%r10d, %eax
	movq	120(%rsp), %r13         # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_51
.LBB1_43:                               # %.preheader237.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	xorl	%ebx, %ebx
	movq	8(%rsp), %r12           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_44:                               # %.preheader237
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebx, %ecx
	andl	$1, %ecx
	movl	$0, %ebp
	cmovel	%r11d, %ebp
	movl	%ecx, %edx
	negl	%edx
	andl	%r11d, %edx
	movl	%r14d, %r15d
	subl	%edx, %r15d
	addl	%eax, %ebp
	testl	%ecx, %ecx
	cmovel	%eax, %r14d
	leaq	128(%rsp), %rax
	movq	%rax, %rdi
	movl	%r14d, %esi
	movl	%r12d, %edx
	movq	%r13, %rcx
	movl	%r10d, %r8d
	pushq	$1
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	callq	ercCollect8PredBlocks
	addq	$16, %rsp
.Lcfi40:
	.cfi_adjust_cfa_offset -16
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movl	$2, %r9d
	movl	%r14d, %esi
	movl	%r12d, %edx
	leaq	128(%rsp), %rcx
	movl	44(%rsp), %r8d          # 4-byte Reload
	callq	ercPixConcealIMB
	movq	56(%rsp), %r11          # 8-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	movl	28(%rsp), %r10d         # 4-byte Reload
	imull	%r9d, %r14d
	addl	%r12d, %r14d
	movslq	%r14d, %rax
	movl	$2, (%r13,%rax,4)
	movl	$2, 4(%r13,%rax,4)
	addl	%r9d, %eax
	cltq
	movabsq	$8589934594, %rcx       # imm = 0x200000002
	movq	%rcx, (%r13,%rax,4)
	addl	%r11d, %ebx
	cmpl	104(%rsp), %ebx         # 4-byte Folded Reload
	movl	%ebp, %eax
	movl	%r15d, %r14d
	jl	.LBB1_44
.LBB1_48:                               #   in Loop: Header=BB1_4 Depth=2
	movl	96(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jmp	.LBB1_49
.LBB1_30:                               # %.preheader235.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	leal	1(%r14), %eax
	imull	%r9d, %eax
	addl	%r15d, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	%r9d, %r12d
	imull	%r14d, %r12d
	addl	%r15d, %r12d
	xorl	%ebx, %ebx
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB1_31:                               # %.preheader235
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	128(%rsp), %r15
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%r13, %rcx
	movl	%r10d, %r8d
	pushq	$1
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	callq	ercCollect8PredBlocks
	addq	$16, %rsp
.Lcfi43:
	.cfi_adjust_cfa_offset -16
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movl	$2, %r9d
	movl	%ebp, %esi
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%r15, %rcx
	movl	44(%rsp), %r8d          # 4-byte Reload
	callq	ercPixConcealIMB
	movq	56(%rsp), %r11          # 8-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	movl	28(%rsp), %r10d         # 4-byte Reload
	leal	(%r12,%rbx), %eax
	cltq
	movl	$2, (%r13,%rax,4)
	leal	1(%r12,%rbx), %eax
	cltq
	movl	$2, (%r13,%rax,4)
	movq	80(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rbx), %eax
	cltq
	movl	$2, (%r13,%rax,4)
	leal	1(%rcx,%rbx), %eax
	cltq
	movl	$2, (%r13,%rax,4)
	subl	92(%rsp), %ebx          # 4-byte Folded Reload
	subl	%r11d, %ebp
	jns	.LBB1_31
.LBB1_35:                               # %._crit_edge
                                        #   in Loop: Header=BB1_4 Depth=2
	addl	%r11d, %r14d
	movl	%r14d, %eax
.LBB1_49:                               # %.loopexit
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	72(%rsp), %r8           # 8-byte Reload
.LBB1_50:                               # %.loopexit
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB1_51:                               # %.loopexit
                                        #   in Loop: Header=BB1_4 Depth=2
	leal	(%r11,%rax), %eax
	cmpl	%r10d, %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	jl	.LBB1_4
.LBB1_52:                               # %._crit_edge221
                                        #   in Loop: Header=BB1_2 Depth=1
	addq	%r11, %rsi
	movq	112(%rsp), %rax         # 8-byte Reload
	addq	168(%rsp), %rax         # 8-byte Folded Reload
	movq	%rax, 112(%rsp)         # 8-byte Spill
	cmpq	%rbx, %rsi
	jl	.LBB1_2
.LBB1_53:                               # %._crit_edge224
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	concealBlocks, .Lfunc_end1-concealBlocks
	.cfi_endproc

	.globl	ercPixConcealIMB
	.p2align	4, 0x90
	.type	ercPixConcealIMB,@function
ercPixConcealIMB:                       # @ercPixConcealIMB
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	cmpl	$0, 16(%rcx)
	je	.LBB2_1
# BB#2:
	movl	%esi, %eax
	subl	%r9d, %eax
	imull	%r8d, %eax
	shll	$3, %eax
	cltq
	leaq	(%rdi,%rax,2), %rax
	leal	(,%rdx,8), %ebp
	movslq	%ebp, %rbp
	leaq	(%rax,%rbp,2), %rax
	jmp	.LBB2_3
.LBB2_1:
	xorl	%eax, %eax
.LBB2_3:
	movq	%rax, -64(%rsp)         # 8-byte Spill
	cmpl	$0, 20(%rcx)
	je	.LBB2_4
# BB#5:
	movl	%esi, %eax
	imull	%r8d, %eax
	shll	$3, %eax
	cltq
	leaq	(%rdi,%rax,2), %rax
	movl	%edx, %ebp
	subl	%r9d, %ebp
	shll	$3, %ebp
	movslq	%ebp, %rbp
	leaq	(%rax,%rbp,2), %rbx
	cmpl	$0, 24(%rcx)
	jne	.LBB2_8
	jmp	.LBB2_7
.LBB2_4:
	xorl	%ebx, %ebx
	cmpl	$0, 24(%rcx)
	je	.LBB2_7
.LBB2_8:
	leal	(%r9,%rsi), %eax
	imull	%r8d, %eax
	shll	$3, %eax
	cltq
	leaq	(%rdi,%rax,2), %rax
	leal	(,%rdx,8), %ebp
	movslq	%ebp, %rbp
	leaq	(%rax,%rbp,2), %r12
	jmp	.LBB2_9
.LBB2_7:
	xorl	%r12d, %r12d
.LBB2_9:
	imull	%r8d, %esi
	shll	$3, %esi
	cmpl	$0, 28(%rcx)
	movslq	%esi, %rax
	je	.LBB2_10
# BB#11:
	leaq	(%rdi,%rax,2), %rcx
	leal	(%r9,%rdx), %esi
	shll	$3, %esi
	movslq	%esi, %rsi
	leaq	(%rcx,%rsi,2), %rcx
	testl	%r9d, %r9d
	jg	.LBB2_13
	jmp	.LBB2_57
.LBB2_10:
	xorl	%ecx, %ecx
	testl	%r9d, %r9d
	jle	.LBB2_57
.LBB2_13:                               # %.preheader.lr.ph.i
	leaq	(%rdi,%rax,2), %rax
	shll	$3, %edx
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,2), %rbp
	leal	(,%r9,8), %esi
	leal	-1(,%r9,8), %eax
	movq	img(%rip), %rdi
	leaq	5896(%rdi), %rdx
	addq	$5892, %rdi             # imm = 0x1704
	cmpl	$8, %esi
	cmoveq	%rdx, %rdi
	movq	%rdi, -80(%rsp)         # 8-byte Spill
	movslq	%eax, %rdx
	movq	%rdx, %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	imull	%r8d, %eax
	cltq
	movl	%esi, -84(%rsp)         # 4-byte Spill
	movslq	%esi, %rsi
	movslq	%r8d, %rdi
	movq	-64(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,2), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movq	%rsi, -40(%rsp)         # 8-byte Spill
	movl	%esi, %r15d
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rdi), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	shll	$4, %r9d
	xorl	%r10d, %r10d
	movq	%rbx, -16(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_14:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_35 Depth 2
                                        #     Child Loop BB2_38 Depth 2
                                        #     Child Loop BB2_17 Depth 2
                                        #     Child Loop BB2_24 Depth 2
	movq	%r10, %rax
	movq	%rax, %rdx
	imulq	-24(%rsp), %rdx         # 8-byte Folded Reload
	leaq	1(%rax), %r10
	movq	%rdx, -72(%rsp)         # 8-byte Spill
	leaq	(%rbx,%rdx,2), %r8
	cmpq	$0, -64(%rsp)           # 8-byte Folded Reload
	movq	%r9, -8(%rsp)           # 8-byte Spill
	je	.LBB2_15
# BB#33:                                # %.lr.ph.split.preheader.i
                                        #   in Loop: Header=BB2_14 Depth=1
	movq	-40(%rsp), %r11         # 8-byte Reload
	subq	%rax, %r11
	testq	%rbx, %rbx
	je	.LBB2_37
# BB#34:                                # %.lr.ph.split.i.preheader
                                        #   in Loop: Header=BB2_14 Depth=1
	movl	%r9d, %r14d
	movq	%r15, %rdi
	movq	%rbp, %r9
	movq	%r9, %rsi
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_35:                               # %.lr.ph.split.i
                                        #   Parent Loop BB2_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-56(%rsp), %rax         # 8-byte Reload
	movzwl	(%rax,%r13,2), %edx
	imull	%r11d, %edx
	movq	-48(%rsp), %rax         # 8-byte Reload
	movzwl	(%r8,%rax,2), %ebx
	movl	%edi, %eax
	imull	%ebx, %eax
	addl	%edx, %eax
	testq	%r12, %r12
	je	.LBB2_36
# BB#49:                                #   in Loop: Header=BB2_35 Depth=2
	movl	-84(%rsp), %edx         # 4-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	subl	%r13d, %edx
	leal	(%r11,%rdx), %ebp
	movzwl	(%r12,%r13,2), %edx
	imull	%r10d, %edx
	addl	%edx, %eax
	addl	%r10d, %ebp
	testq	%rcx, %rcx
	jne	.LBB2_51
	jmp	.LBB2_52
	.p2align	4, 0x90
.LBB2_36:                               #   in Loop: Header=BB2_35 Depth=2
	movl	%r14d, %ebp
	testq	%rcx, %rcx
	je	.LBB2_52
.LBB2_51:                               #   in Loop: Header=BB2_35 Depth=2
	leal	1(%r13), %edx
	movq	-72(%rsp), %rbx         # 8-byte Reload
	movzwl	(%rcx,%rbx,2), %ebx
	imull	%edx, %ebx
	addl	%ebx, %eax
	leal	1(%r13,%rbp), %ebp
.LBB2_52:                               #   in Loop: Header=BB2_35 Depth=2
	testl	%ebp, %ebp
	jle	.LBB2_54
# BB#53:                                #   in Loop: Header=BB2_35 Depth=2
	cltd
	idivl	%ebp
	movzbl	%al, %eax
	jmp	.LBB2_55
	.p2align	4, 0x90
.LBB2_54:                               #   in Loop: Header=BB2_35 Depth=2
	movq	-80(%rsp), %rax         # 8-byte Reload
	movw	(%rax), %ax
.LBB2_55:                               #   in Loop: Header=BB2_35 Depth=2
	movw	%ax, (%rsi)
	incq	%r13
	addq	$2, %rsi
	decl	%r14d
	decq	%rdi
	jne	.LBB2_35
	jmp	.LBB2_56
	.p2align	4, 0x90
.LBB2_15:                               # %.lr.ph.split.us.preheader.i
                                        #   in Loop: Header=BB2_14 Depth=1
	xorl	%esi, %esi
	testq	%rbx, %rbx
	je	.LBB2_23
# BB#16:                                # %.lr.ph.split.us.i.preheader
                                        #   in Loop: Header=BB2_14 Depth=1
	xorl	%edi, %edi
	movq	%rbp, %r9
	.p2align	4, 0x90
.LBB2_17:                               # %.lr.ph.split.us.i
                                        #   Parent Loop BB2_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-48(%rsp), %rax         # 8-byte Reload
	movzwl	(%r8,%rax,2), %edx
	leaq	(%r15,%rsi), %rbp
	movl	%ebp, %eax
	imull	%edx, %eax
	testq	%r12, %r12
	je	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_17 Depth=2
	movl	-84(%rsp), %edx         # 4-byte Reload
	movl	%edx, %ebp
	subl	%edi, %ebp
	movzwl	(%r12,%rdi,2), %edx
	imull	%r10d, %edx
	addl	%edx, %eax
	addl	%r10d, %ebp
.LBB2_19:                               #   in Loop: Header=BB2_17 Depth=2
	testq	%rcx, %rcx
	je	.LBB2_21
# BB#20:                                #   in Loop: Header=BB2_17 Depth=2
	leal	1(%rdi), %edx
	movq	-72(%rsp), %rbx         # 8-byte Reload
	movzwl	(%rcx,%rbx,2), %ebx
	imull	%edx, %ebx
	addl	%ebx, %eax
	leal	1(%rdi,%rbp), %ebp
.LBB2_21:                               #   in Loop: Header=BB2_17 Depth=2
	testl	%ebp, %ebp
	jle	.LBB2_22
# BB#47:                                #   in Loop: Header=BB2_17 Depth=2
	cltd
	idivl	%ebp
	movzbl	%al, %eax
	jmp	.LBB2_48
	.p2align	4, 0x90
.LBB2_22:                               #   in Loop: Header=BB2_17 Depth=2
	movq	-80(%rsp), %rax         # 8-byte Reload
	movw	(%rax), %ax
.LBB2_48:                               #   in Loop: Header=BB2_17 Depth=2
	movw	%ax, (%r9,%rdi,2)
	incq	%rdi
	decq	%rsi
	cmpq	%rdi, %r15
	jne	.LBB2_17
	jmp	.LBB2_56
	.p2align	4, 0x90
.LBB2_37:                               # %.lr.ph.split.i.us.preheader
                                        #   in Loop: Header=BB2_14 Depth=1
	leal	(%r11,%r10), %r8d
	xorl	%edi, %edi
	movq	%rbp, %r9
	.p2align	4, 0x90
.LBB2_38:                               # %.lr.ph.split.i.us
                                        #   Parent Loop BB2_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-56(%rsp), %rax         # 8-byte Reload
	movzwl	(%rax,%rdi,2), %eax
	imull	%r11d, %eax
	testq	%r12, %r12
	je	.LBB2_39
# BB#40:                                #   in Loop: Header=BB2_38 Depth=2
	movzwl	(%r12,%rdi,2), %edx
	imull	%r10d, %edx
	addl	%edx, %eax
	movl	%r8d, %ebp
	testq	%rcx, %rcx
	jne	.LBB2_42
	jmp	.LBB2_43
	.p2align	4, 0x90
.LBB2_39:                               #   in Loop: Header=BB2_38 Depth=2
	movl	%r11d, %ebp
	testq	%rcx, %rcx
	je	.LBB2_43
.LBB2_42:                               #   in Loop: Header=BB2_38 Depth=2
	leal	1(%rdi), %edx
	movq	-72(%rsp), %rsi         # 8-byte Reload
	movzwl	(%rcx,%rsi,2), %esi
	imull	%edx, %esi
	addl	%esi, %eax
	leal	1(%rdi,%rbp), %ebp
.LBB2_43:                               #   in Loop: Header=BB2_38 Depth=2
	testl	%ebp, %ebp
	jle	.LBB2_44
# BB#45:                                #   in Loop: Header=BB2_38 Depth=2
	cltd
	idivl	%ebp
	movzbl	%al, %eax
	jmp	.LBB2_46
	.p2align	4, 0x90
.LBB2_44:                               #   in Loop: Header=BB2_38 Depth=2
	movq	-80(%rsp), %rax         # 8-byte Reload
	movw	(%rax), %ax
.LBB2_46:                               #   in Loop: Header=BB2_38 Depth=2
	movw	%ax, (%r9,%rdi,2)
	incq	%rdi
	cmpq	%rdi, %r15
	jne	.LBB2_38
	jmp	.LBB2_56
.LBB2_23:                               # %.lr.ph.split.us.i.us.preheader
                                        #   in Loop: Header=BB2_14 Depth=1
	movq	%rbp, %r9
	.p2align	4, 0x90
.LBB2_24:                               # %.lr.ph.split.us.i.us
                                        #   Parent Loop BB2_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%r12, %r12
	je	.LBB2_25
# BB#26:                                #   in Loop: Header=BB2_24 Depth=2
	movzwl	(%r12,%rsi,2), %eax
	imull	%r10d, %eax
	movl	%r10d, %edi
	testq	%rcx, %rcx
	jne	.LBB2_28
	jmp	.LBB2_29
	.p2align	4, 0x90
.LBB2_25:                               #   in Loop: Header=BB2_24 Depth=2
	xorl	%eax, %eax
	xorl	%edi, %edi
	testq	%rcx, %rcx
	je	.LBB2_29
.LBB2_28:                               #   in Loop: Header=BB2_24 Depth=2
	leal	1(%rsi), %edx
	movq	-72(%rsp), %rbp         # 8-byte Reload
	movzwl	(%rcx,%rbp,2), %ebp
	imull	%edx, %ebp
	addl	%ebp, %eax
	leal	1(%rsi,%rdi), %edi
.LBB2_29:                               #   in Loop: Header=BB2_24 Depth=2
	testl	%edi, %edi
	jle	.LBB2_30
# BB#31:                                #   in Loop: Header=BB2_24 Depth=2
	cltd
	idivl	%edi
	movzbl	%al, %eax
	jmp	.LBB2_32
	.p2align	4, 0x90
.LBB2_30:                               #   in Loop: Header=BB2_24 Depth=2
	movq	-80(%rsp), %rax         # 8-byte Reload
	movw	(%rax), %ax
.LBB2_32:                               #   in Loop: Header=BB2_24 Depth=2
	movw	%ax, (%r9,%rsi,2)
	incq	%rsi
	cmpq	%rsi, %r15
	jne	.LBB2_24
	.p2align	4, 0x90
.LBB2_56:                               # %._crit_edge.i
                                        #   in Loop: Header=BB2_14 Depth=1
	addq	-32(%rsp), %r9          # 8-byte Folded Reload
	movq	%r9, %rbp
	movq	-8(%rsp), %r9           # 8-byte Reload
	decl	%r9d
	cmpq	%r15, %r10
	movq	-16(%rsp), %rbx         # 8-byte Reload
	jne	.LBB2_14
.LBB2_57:                               # %pixMeanInterpolateBlock.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	ercPixConcealIMB, .Lfunc_end2-ercPixConcealIMB
	.cfi_endproc

	.globl	ercCollect8PredBlocks
	.p2align	4, 0x90
	.type	ercCollect8PredBlocks,@function
ercCollect8PredBlocks:                  # @ercCollect8PredBlocks
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movb	64(%rsp), %r13b
	movl	56(%rsp), %r14d
	xorl	%ebp, %ebp
	testb	%r13b, %r13b
	sete	%bpl
	leal	2(%rbp,%rbp), %r10d
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, (%rdi)
	subl	%r14d, %r8d
	leal	(%r14,%rsi), %ebp
	imull	%r9d, %ebp
	leal	(%rbp,%rdx), %ebx
	movslq	%ebx, %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movl	%r9d, %ebx
	imull	%esi, %ebx
	leal	-1(%rdx,%rbx), %eax
	movslq	%eax, %r12
	leal	-1(%rdx,%rbp), %eax
	cltq
	movq	%rax, -32(%rsp)         # 8-byte Spill
	leal	(%r14,%rdx), %eax
	addl	%ebp, %eax
	movl	%r9d, %r11d
	subl	%r14d, %r11d
	addl	%edx, %ebx
	addl	%r14d, %ebx
	movslq	%ebx, %r15
	cltq
	movq	%rax, -40(%rsp)         # 8-byte Spill
	testl	%esi, %esi
	jle	.LBB3_27
# BB#1:                                 # %.split.us.preheader
	leal	-1(%rsi), %eax
	imull	%r9d, %eax
	leal	(%rax,%rdx), %ebp
	movslq	%ebp, %rbx
	leal	-1(%rdx,%rax), %eax
	cltq
	movq	%rax, -8(%rsp)          # 8-byte Spill
	addl	%ebx, %r14d
	movslq	%r14d, %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movl	$3, %r14d
	.p2align	4, 0x90
.LBB3_2:                                # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rbx,4), %r9d
	xorl	%eax, %eax
	cmpl	%r14d, %r9d
	jl	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	%r9d, 16(%rdi)
	movl	$1, %eax
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	%esi, %r8d
	jle	.LBB3_7
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	-24(%rsp), %rbp         # 8-byte Reload
	movl	(%rcx,%rbp,4), %ebp
	cmpl	%r14d, %ebp
	jl	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	%ebp, 24(%rdi)
	incl	%eax
.LBB3_7:                                #   in Loop: Header=BB3_2 Depth=1
	testl	%edx, %edx
	jle	.LBB3_16
# BB#8:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	(%rcx,%r12,4), %ebp
	cmpl	%r14d, %ebp
	jl	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	%ebp, 20(%rdi)
	incl	%eax
.LBB3_10:                               #   in Loop: Header=BB3_2 Depth=1
	testb	%r13b, %r13b
	jne	.LBB3_16
# BB#11:                                #   in Loop: Header=BB3_2 Depth=1
	movq	-8(%rsp), %rbp          # 8-byte Reload
	movl	(%rcx,%rbp,4), %ebp
	cmpl	%r14d, %ebp
	jl	.LBB3_13
# BB#12:                                #   in Loop: Header=BB3_2 Depth=1
	movl	%ebp, 4(%rdi)
	incl	%eax
.LBB3_13:                               #   in Loop: Header=BB3_2 Depth=1
	cmpl	%esi, %r8d
	jle	.LBB3_16
# BB#14:                                #   in Loop: Header=BB3_2 Depth=1
	movq	-32(%rsp), %rbp         # 8-byte Reload
	movl	(%rcx,%rbp,4), %ebp
	cmpl	%r14d, %ebp
	jl	.LBB3_16
# BB#15:                                #   in Loop: Header=BB3_2 Depth=1
	movl	%ebp, 8(%rdi)
	incl	%eax
	.p2align	4, 0x90
.LBB3_16:                               #   in Loop: Header=BB3_2 Depth=1
	cmpl	%edx, %r11d
	jle	.LBB3_25
# BB#17:                                #   in Loop: Header=BB3_2 Depth=1
	movl	(%rcx,%r15,4), %ebp
	cmpl	%r14d, %ebp
	jl	.LBB3_19
# BB#18:                                #   in Loop: Header=BB3_2 Depth=1
	movl	%ebp, 28(%rdi)
	incl	%eax
.LBB3_19:                               #   in Loop: Header=BB3_2 Depth=1
	testb	%r13b, %r13b
	jne	.LBB3_25
# BB#20:                                #   in Loop: Header=BB3_2 Depth=1
	movq	-16(%rsp), %rbp         # 8-byte Reload
	movl	(%rcx,%rbp,4), %ebp
	cmpl	%r14d, %ebp
	jl	.LBB3_22
# BB#21:                                #   in Loop: Header=BB3_2 Depth=1
	movl	%ebp, (%rdi)
	incl	%eax
.LBB3_22:                               #   in Loop: Header=BB3_2 Depth=1
	cmpl	%esi, %r8d
	jle	.LBB3_25
# BB#23:                                #   in Loop: Header=BB3_2 Depth=1
	movq	-40(%rsp), %rbp         # 8-byte Reload
	movl	(%rcx,%rbp,4), %ebp
	cmpl	%r14d, %ebp
	jl	.LBB3_25
# BB#24:                                #   in Loop: Header=BB3_2 Depth=1
	movl	%ebp, 12(%rdi)
	incl	%eax
	.p2align	4, 0x90
.LBB3_25:                               #   in Loop: Header=BB3_2 Depth=1
	cmpl	$3, %r14d
	jl	.LBB3_45
# BB#26:                                #   in Loop: Header=BB3_2 Depth=1
	decl	%r14d
	cmpl	%r10d, %eax
	jl	.LBB3_2
	jmp	.LBB3_45
.LBB3_27:                               # %.split.preheader
	testb	%r13b, %r13b
	sete	%al
	cmpl	%esi, %r8d
	setg	%r9b
	andb	%al, %r9b
	movl	$3, %ebx
	.p2align	4, 0x90
.LBB3_28:                               # %.split
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	cmpl	%esi, %r8d
	jle	.LBB3_31
# BB#29:                                #   in Loop: Header=BB3_28 Depth=1
	movq	-24(%rsp), %rbp         # 8-byte Reload
	movl	(%rcx,%rbp,4), %ebp
	cmpl	%ebx, %ebp
	jl	.LBB3_31
# BB#30:                                #   in Loop: Header=BB3_28 Depth=1
	movl	%ebp, 24(%rdi)
	movl	$1, %eax
.LBB3_31:                               #   in Loop: Header=BB3_28 Depth=1
	testl	%edx, %edx
	jle	.LBB3_37
# BB#32:                                #   in Loop: Header=BB3_28 Depth=1
	movl	(%rcx,%r12,4), %ebp
	cmpl	%ebx, %ebp
	jl	.LBB3_34
# BB#33:                                #   in Loop: Header=BB3_28 Depth=1
	movl	%ebp, 20(%rdi)
	incl	%eax
.LBB3_34:                               #   in Loop: Header=BB3_28 Depth=1
	testb	%r9b, %r9b
	je	.LBB3_37
# BB#35:                                #   in Loop: Header=BB3_28 Depth=1
	movq	-32(%rsp), %rbp         # 8-byte Reload
	movl	(%rcx,%rbp,4), %ebp
	cmpl	%ebx, %ebp
	jl	.LBB3_37
# BB#36:                                #   in Loop: Header=BB3_28 Depth=1
	movl	%ebp, 8(%rdi)
	incl	%eax
	.p2align	4, 0x90
.LBB3_37:                               #   in Loop: Header=BB3_28 Depth=1
	cmpl	%edx, %r11d
	jle	.LBB3_43
# BB#38:                                #   in Loop: Header=BB3_28 Depth=1
	movl	(%rcx,%r15,4), %ebp
	cmpl	%ebx, %ebp
	jl	.LBB3_40
# BB#39:                                #   in Loop: Header=BB3_28 Depth=1
	movl	%ebp, 28(%rdi)
	incl	%eax
.LBB3_40:                               #   in Loop: Header=BB3_28 Depth=1
	testb	%r9b, %r9b
	je	.LBB3_43
# BB#41:                                #   in Loop: Header=BB3_28 Depth=1
	movq	-40(%rsp), %rbp         # 8-byte Reload
	movl	(%rcx,%rbp,4), %ebp
	cmpl	%ebx, %ebp
	jl	.LBB3_43
# BB#42:                                #   in Loop: Header=BB3_28 Depth=1
	movl	%ebp, 12(%rdi)
	incl	%eax
	.p2align	4, 0x90
.LBB3_43:                               #   in Loop: Header=BB3_28 Depth=1
	cmpl	$3, %ebx
	jl	.LBB3_45
# BB#44:                                #   in Loop: Header=BB3_28 Depth=1
	decl	%ebx
	cmpl	%r10d, %eax
	jl	.LBB3_28
.LBB3_45:                               # %.us-lcssa.us
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	ercCollect8PredBlocks, .Lfunc_end3-ercCollect8PredBlocks
	.cfi_endproc

	.globl	ercCollectColumnBlocks
	.p2align	4, 0x90
	.type	ercCollectColumnBlocks,@function
ercCollectColumnBlocks:                 # @ercCollectColumnBlocks
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	8(%rsp), %r8d
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, (%rdi)
	leal	-1(%rsi), %eax
	imull	%r9d, %eax
	addl	%edx, %eax
	movslq	%eax, %r10
	xorl	%eax, %eax
	cmpl	$2, (%rcx,%r10,4)
	jl	.LBB4_2
# BB#1:
	movl	$1, 16(%rdi)
	movl	$1, %eax
.LBB4_2:
	addl	%esi, %r8d
	imull	%r9d, %r8d
	addl	%edx, %r8d
	movslq	%r8d, %rdx
	cmpl	$2, (%rcx,%rdx,4)
	jl	.LBB4_4
# BB#3:
	movl	$1, 24(%rdi)
	incl	%eax
.LBB4_4:
	retq
.Lfunc_end4:
	.size	ercCollectColumnBlocks, .Lfunc_end4-ercCollectColumnBlocks
	.cfi_endproc

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	last_out_fs,@object     # @last_out_fs
	.comm	last_out_fs,8,8
	.type	pocs_in_dpb,@object     # @pocs_in_dpb
	.comm	pocs_in_dpb,400,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
