	.text
	.file	"whetstone.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
.LCPI0_1:
	.quad	-4616189618054758400    # double -1
.LCPI0_6:
	.quad	4602678819172646912     # double 0.5
.LCPI0_8:
	.quad	4611686018427387904     # double 2
.LCPI0_9:
	.quad	4613937818241073152     # double 3
.LCPI0_10:
	.quad	4604930618986332160     # double 0.75
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_2:
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
.LCPI0_3:
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
.LCPI0_4:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI0_5:
	.quad	4618441417868443648     # double 6
	.quad	4618441417868443648     # double 6
.LCPI0_7:
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	cmpl	$2, %edi
	jl	.LBB0_1
# BB#5:                                 # %.lr.ph288.preheader
	movslq	%edi, %r14
	xorl	%r12d, %r12d
	movl	$100000, %eax           # imm = 0x186A0
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph288
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbx,8), %rbp
	movl	$.L.str, %esi
	movl	$2, %edx
	movq	%rbp, %rdi
	callq	strncmp
	movl	$1, %ecx
	testl	%eax, %eax
	je	.LBB0_10
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	cmpb	$99, (%rbp)
	je	.LBB0_10
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strtol
	movq	%rax, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	testq	%rax, %rax
	movl	%r12d, %ecx
	jle	.LBB0_9
.LBB0_10:                               #   in Loop: Header=BB0_6 Depth=1
	incq	%rbx
	cmpq	%r14, %rbx
	movl	%ecx, %r12d
	jl	.LBB0_6
	jmp	.LBB0_2
.LBB0_1:
	xorl	%ecx, %ecx
	movl	$100000, %eax           # imm = 0x186A0
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB0_2:                                # %.preheader
	movabsq	$9223372036854775800, %rdx # imm = 0x7FFFFFFFFFFFFFF8
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	(,%rdi,4), %rax
	leaq	(%rax,%rax,2), %r15
	imulq	$14, %rdi, %rbp
	imulq	$345, %rdi, %rax        # imm = 0x159
	imulq	$210, %rdi, %r13
	movq	%rdi, %rsi
	shlq	$5, %rsi
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	imulq	$899, %rdi, %rsi        # imm = 0x383
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	imulq	$616, %rdi, %r14        # imm = 0x268
	imulq	$93, %rdi, %rsi
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	testq	%rdi, %rdi
	setle	7(%rsp)                 # 1-byte Folded Spill
	testq	%rax, %rax
	movl	$1, %esi
	movq	%rax, 48(%rsp)          # 8-byte Spill
	cmovgq	%rax, %rsi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	andq	%rsi, %rdx
	leaq	-8(%rdx), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	shrl	$3, %eax
	incl	%eax
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	orq	$1, %rdx
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	andl	$7, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	negq	%rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movl	%ecx, 76(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB0_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_12 Depth 2
                                        #     Child Loop BB0_16 Depth 2
                                        #     Child Loop BB0_25 Depth 2
                                        #     Child Loop BB0_28 Depth 2
                                        #     Child Loop BB0_32 Depth 2
                                        #     Child Loop BB0_35 Depth 2
                                        #     Child Loop BB0_40 Depth 2
                                        #     Child Loop BB0_49 Depth 2
                                        #     Child Loop BB0_52 Depth 2
	xorl	%edi, %edi
	callq	time
	movabsq	$4602678368812684175, %rax # imm = 0x3FDFFF972474538F
	movq	%rax, T(%rip)
	movabsq	$4602681070972460597, %rax # imm = 0x3FE0020C49BA5E35
	movq	%rax, T1(%rip)
	movabsq	$4611686018427387904, %rax # imm = 0x4000000000000000
	movq	%rax, T2(%rip)
	movl	$.L.str.3, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movb	$4, %al
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movaps	%xmm1, %xmm2
	movaps	%xmm1, %xmm3
	callq	printf
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [1.000000e+00,-1.000000e+00]
	movups	%xmm0, E1+8(%rip)
	movaps	.LCPI0_3(%rip), %xmm0   # xmm0 = [-1.000000e+00,-1.000000e+00]
	movups	%xmm0, E1+24(%rip)
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	jle	.LBB0_4
# BB#11:                                # %.lr.ph
                                        #   in Loop: Header=BB0_3 Depth=1
	movsd	T(%rip), %xmm4          # xmm4 = mem[0],zero
	xorl	%eax, %eax
	movsd	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm3
	movapd	%xmm0, %xmm2
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB0_12:                               #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	subsd	%xmm3, %xmm0
	mulsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm1
	subsd	%xmm2, %xmm1
	addsd	%xmm3, %xmm1
	mulsd	%xmm4, %xmm1
	movapd	%xmm0, %xmm5
	subsd	%xmm1, %xmm5
	addsd	%xmm2, %xmm5
	addsd	%xmm3, %xmm5
	movapd	%xmm5, %xmm2
	mulsd	%xmm4, %xmm2
	movapd	%xmm1, %xmm5
	subsd	%xmm0, %xmm5
	addsd	%xmm2, %xmm5
	addsd	%xmm5, %xmm3
	mulsd	%xmm4, %xmm3
	incq	%rax
	cmpq	%r15, %rax
	jl	.LBB0_12
# BB#13:                                #   in Loop: Header=BB0_3 Depth=1
	movsd	%xmm2, E1+24(%rip)
	movsd	%xmm3, E1+32(%rip)
	movsd	%xmm0, E1+8(%rip)
	movsd	%xmm1, E1+16(%rip)
	movl	$.L.str.3, %edi
	movb	$4, %al
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%r15, %rcx
	callq	printf
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	jle	.LBB0_14
# BB#15:                                # %.lr.ph230
                                        #   in Loop: Header=BB0_3 Depth=1
	movsd	T(%rip), %xmm4          # xmm4 = mem[0],zero
	movsd	T2(%rip), %xmm8         # xmm8 = mem[0],zero
	movsd	E1+8(%rip), %xmm0       # xmm0 = mem[0],zero
	movsd	E1+16(%rip), %xmm1      # xmm1 = mem[0],zero
	movsd	E1+24(%rip), %xmm2      # xmm2 = mem[0],zero
	movsd	E1+32(%rip), %xmm3      # xmm3 = mem[0],zero
	xorl	%eax, %eax
	movq	88(%rsp), %rbx          # 8-byte Reload
	movabsq	$4613937818241073152, %r12 # imm = 0x4008000000000000
	.p2align	4, 0x90
.LBB0_16:                               # %PA.exit307
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	subsd	%xmm3, %xmm0
	mulsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm1
	subsd	%xmm2, %xmm1
	addsd	%xmm3, %xmm1
	mulsd	%xmm4, %xmm1
	movapd	%xmm0, %xmm6
	subsd	%xmm1, %xmm6
	addsd	%xmm2, %xmm6
	addsd	%xmm3, %xmm6
	mulsd	%xmm4, %xmm6
	movapd	%xmm1, %xmm2
	subsd	%xmm0, %xmm2
	addsd	%xmm6, %xmm2
	addsd	%xmm3, %xmm2
	divsd	%xmm8, %xmm2
	addsd	%xmm1, %xmm0
	addsd	%xmm6, %xmm0
	subsd	%xmm2, %xmm0
	mulsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm1
	subsd	%xmm6, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm4, %xmm1
	movapd	%xmm0, %xmm7
	subsd	%xmm1, %xmm7
	addsd	%xmm6, %xmm7
	addsd	%xmm2, %xmm7
	mulsd	%xmm4, %xmm7
	movapd	%xmm1, %xmm3
	subsd	%xmm0, %xmm3
	addsd	%xmm7, %xmm3
	addsd	%xmm2, %xmm3
	divsd	%xmm8, %xmm3
	addsd	%xmm1, %xmm0
	addsd	%xmm7, %xmm0
	subsd	%xmm3, %xmm0
	mulsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm1
	subsd	%xmm7, %xmm1
	addsd	%xmm3, %xmm1
	mulsd	%xmm4, %xmm1
	movapd	%xmm0, %xmm6
	subsd	%xmm1, %xmm6
	addsd	%xmm7, %xmm6
	addsd	%xmm3, %xmm6
	mulsd	%xmm4, %xmm6
	movapd	%xmm1, %xmm2
	subsd	%xmm0, %xmm2
	addsd	%xmm6, %xmm2
	addsd	%xmm3, %xmm2
	divsd	%xmm8, %xmm2
	addsd	%xmm1, %xmm0
	addsd	%xmm6, %xmm0
	subsd	%xmm2, %xmm0
	mulsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm1
	subsd	%xmm6, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm4, %xmm1
	movapd	%xmm0, %xmm7
	subsd	%xmm1, %xmm7
	addsd	%xmm6, %xmm7
	addsd	%xmm2, %xmm7
	mulsd	%xmm4, %xmm7
	movapd	%xmm1, %xmm3
	subsd	%xmm0, %xmm3
	addsd	%xmm7, %xmm3
	addsd	%xmm2, %xmm3
	divsd	%xmm8, %xmm3
	addsd	%xmm1, %xmm0
	addsd	%xmm7, %xmm0
	subsd	%xmm3, %xmm0
	mulsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm1
	subsd	%xmm7, %xmm1
	addsd	%xmm3, %xmm1
	mulsd	%xmm4, %xmm1
	movapd	%xmm0, %xmm5
	subsd	%xmm1, %xmm5
	addsd	%xmm7, %xmm5
	addsd	%xmm3, %xmm5
	mulsd	%xmm4, %xmm5
	movapd	%xmm1, %xmm6
	subsd	%xmm0, %xmm6
	addsd	%xmm5, %xmm6
	addsd	%xmm3, %xmm6
	divsd	%xmm8, %xmm6
	addsd	%xmm1, %xmm0
	addsd	%xmm5, %xmm0
	subsd	%xmm6, %xmm0
	mulsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm1
	subsd	%xmm5, %xmm1
	addsd	%xmm6, %xmm1
	mulsd	%xmm4, %xmm1
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	addsd	%xmm5, %xmm2
	addsd	%xmm6, %xmm2
	mulsd	%xmm4, %xmm2
	movapd	%xmm1, %xmm3
	subsd	%xmm0, %xmm3
	addsd	%xmm2, %xmm3
	addsd	%xmm6, %xmm3
	divsd	%xmm8, %xmm3
	incq	%rax
	cmpq	%rbp, %rax
	jl	.LBB0_16
# BB#17:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$6, J(%rip)
	movsd	%xmm0, E1+8(%rip)
	movsd	%xmm1, E1+16(%rip)
	movsd	%xmm2, E1+24(%rip)
	movsd	%xmm3, E1+32(%rip)
	movl	$.L.str.3, %edi
	movb	$4, %al
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r15, %rcx
	callq	printf
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movl	$1, J(%rip)
	jle	.LBB0_37
# BB#18:                                # %.lr.ph242.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	$1, %eax
	cmpq	$8, 80(%rsp)            # 8-byte Folded Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	jae	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_31
	.p2align	4, 0x90
.LBB0_4:                                # %.thread
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	$.L.str.3, %edi
	movb	$4, %al
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%r15, %rcx
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	movapd	%xmm1, %xmm3
	callq	printf
.LBB0_14:                               # %.thread312
                                        #   in Loop: Header=BB0_3 Depth=1
	movsd	E1+8(%rip), %xmm0       # xmm0 = mem[0],zero
	movsd	E1+16(%rip), %xmm1      # xmm1 = mem[0],zero
	movq	E1+24(%rip), %xmm2      # xmm2 = mem[0],zero
	movsd	E1+32(%rip), %xmm3      # xmm3 = mem[0],zero
	movl	$.L.str.3, %edi
	movb	$4, %al
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r15, %rcx
	callq	printf
	movl	$1, %eax
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rbx          # 8-byte Reload
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movabsq	$4613937818241073152, %r12 # imm = 0x4008000000000000
	jmp	.LBB0_33
.LBB0_37:                               # %.critedge
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	$.L.str.3, %edi
	movl	$1, %edx
	movl	$1, %ecx
	movb	$4, %al
	movq	48(%rsp), %rsi          # 8-byte Reload
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movaps	%xmm1, %xmm2
	movaps	%xmm1, %xmm3
	callq	printf
	movl	$1, J(%rip)
	movl	$2, K(%rip)
	movl	$3, L(%rip)
	jmp	.LBB0_38
.LBB0_20:                               # %min.iters.checked
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB0_21
# BB#22:                                # %vector.body.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$0, 120(%rsp)           # 8-byte Folded Reload
	je	.LBB0_23
# BB#24:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	$1, %eax
	movd	%eax, %xmm0
	xorpd	%xmm4, %xmm4
	movq	104(%rsp), %rcx         # 8-byte Reload
	xorl	%eax, %eax
	movdqa	.LCPI0_4(%rip), %xmm5   # xmm5 = [1,1,1,1]
	.p2align	4, 0x90
.LBB0_25:                               # %vector.body.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm4, %xmm3
	movdqa	%xmm0, %xmm2
	pxor	%xmm5, %xmm0
	xorpd	%xmm5, %xmm4
	addq	$8, %rax
	incq	%rcx
	jne	.LBB0_25
	jmp	.LBB0_26
.LBB0_21:                               #   in Loop: Header=BB0_3 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_31
.LBB0_23:                               #   in Loop: Header=BB0_3 Depth=1
	movl	$1, %eax
	movd	%eax, %xmm0
	xorpd	%xmm4, %xmm4
                                        # implicit-def: %XMM2
                                        # implicit-def: %XMM3
	xorl	%eax, %eax
	movapd	.LCPI0_4(%rip), %xmm5   # xmm5 = [1,1,1,1]
.LBB0_26:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	$56, 128(%rsp)          # 8-byte Folded Reload
	jb	.LBB0_30
# BB#27:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	subq	%rax, %rcx
	.p2align	4, 0x90
.LBB0_28:                               # %vector.body
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-64, %rcx
	jne	.LBB0_28
# BB#29:                                # %middle.block.unr-lcssa
                                        #   in Loop: Header=BB0_3 Depth=1
	xorpd	%xmm5, %xmm4
	pxor	%xmm5, %xmm0
	movdqa	%xmm0, %xmm2
	movapd	%xmm4, %xmm3
.LBB0_30:                               # %middle.block
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpq	40(%rsp), %rax          # 8-byte Folded Reload
	pxor	%xmm3, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	pxor	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	pxor	%xmm0, %xmm2
	movd	%xmm2, %eax
	movq	112(%rsp), %rcx         # 8-byte Reload
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	je	.LBB0_33
	.p2align	4, 0x90
.LBB0_31:                               # %.lr.ph242.preheader349
                                        #   in Loop: Header=BB0_3 Depth=1
	decq	%rcx
	.p2align	4, 0x90
.LBB0_32:                               # %.lr.ph242
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	$1, %eax
	incq	%rcx
	cmpq	%rsi, %rcx
	jl	.LBB0_32
.LBB0_33:                               # %.sink.split
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, J(%rip)
	movslq	%eax, %rdx
	movl	$.L.str.3, %edi
	movb	$4, %al
	movq	%rdx, %rcx
	movaps	%xmm1, %xmm2
	movaps	%xmm1, %xmm3
	callq	printf
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movl	$1, J(%rip)
	movl	$2, K(%rip)
	movl	$3, L(%rip)
	jle	.LBB0_41
# BB#34:                                # %.lr.ph247.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_35:                               # %.lr.ph247
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rax
	cmpq	%r13, %rax
	jl	.LBB0_35
# BB#36:                                # %._crit_edge248
                                        #   in Loop: Header=BB0_3 Depth=1
	movaps	.LCPI0_5(%rip), %xmm0   # xmm0 = [6.000000e+00,6.000000e+00]
	movups	%xmm0, E1+8(%rip)
	movl	$2, K(%rip)
	movl	$3, L(%rip)
	movl	$1, J(%rip)
.LBB0_38:                               #   in Loop: Header=BB0_3 Depth=1
	movsd	E1+8(%rip), %xmm0       # xmm0 = mem[0],zero
	movsd	E1+16(%rip), %xmm1      # xmm1 = mem[0],zero
	movsd	E1+24(%rip), %xmm2      # xmm2 = mem[0],zero
	movsd	E1+32(%rip), %xmm3      # xmm3 = mem[0],zero
	movl	$.L.str.3, %edi
	movl	$1, %edx
	movl	$2, %ecx
	movb	$4, %al
	movq	%r13, %rsi
	callq	printf
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	jle	.LBB0_45
# BB#39:                                # %.lr.ph257.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	xorl	%ebp, %ebp
	movsd	.LCPI0_6(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm2
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_40:                               # %.lr.ph257
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm2, 32(%rsp)         # 8-byte Spill
	movsd	T(%rip), %xmm0          # xmm0 = mem[0],zero
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	movsd	T2(%rip), %xmm0         # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	32(%rsp), %xmm0         # 8-byte Folded Reload
	callq	cos
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	subsd	32(%rsp), %xmm0         # 8-byte Folded Reload
	callq	cos
	addsd	56(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	callq	atan
	mulsd	64(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movsd	T(%rip), %xmm0          # xmm0 = mem[0],zero
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	movsd	T2(%rip), %xmm0         # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	callq	cos
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	subsd	32(%rsp), %xmm0         # 8-byte Folded Reload
	callq	cos
	addsd	56(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	.LCPI0_1(%rip), %xmm0
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	callq	atan
	movapd	%xmm0, %xmm2
	mulsd	64(%rsp), %xmm2         # 8-byte Folded Reload
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB0_40
	jmp	.LBB0_42
	.p2align	4, 0x90
.LBB0_41:                               # %._crit_edge258.critedge
                                        #   in Loop: Header=BB0_3 Depth=1
	movsd	E1+8(%rip), %xmm0       # xmm0 = mem[0],zero
	movsd	E1+16(%rip), %xmm1      # xmm1 = mem[0],zero
	movsd	E1+24(%rip), %xmm2      # xmm2 = mem[0],zero
	movsd	E1+32(%rip), %xmm3      # xmm3 = mem[0],zero
	movl	$.L.str.3, %edi
	movl	$1, %edx
	movl	$2, %ecx
	movb	$4, %al
	movq	%r13, %rsi
	callq	printf
	movsd	.LCPI0_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
.LBB0_42:                               # %._crit_edge258
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	J(%rip), %rdx
	movslq	K(%rip), %rcx
	movl	$.L.str.3, %edi
	movb	$4, %al
	movq	%rbx, %rsi
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	movapd	%xmm2, %xmm3
	callq	printf
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	jle	.LBB0_43
# BB#44:                                # %.lr.ph263.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movsd	T(%rip), %xmm3          # xmm3 = mem[0],zero
	movapd	%xmm3, %xmm1
	addsd	%xmm1, %xmm1
	movapd	%xmm1, %xmm2
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm2
	mulsd	%xmm3, %xmm2
	addsd	%xmm1, %xmm2
	divsd	T2(%rip), %xmm2
	movb	7(%rsp), %bl            # 1-byte Reload
	jmp	.LBB0_47
	.p2align	4, 0x90
.LBB0_43:                               #   in Loop: Header=BB0_3 Depth=1
	movb	$1, %bl
	jmp	.LBB0_46
	.p2align	4, 0x90
.LBB0_45:                               # %.critedge329
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	J(%rip), %rdx
	movslq	K(%rip), %rcx
	movl	$.L.str.3, %edi
	movb	$4, %al
	movq	%rbx, %rsi
	movsd	.LCPI0_6(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm2
	movapd	%xmm0, %xmm3
	callq	printf
	movb	7(%rsp), %bl            # 1-byte Reload
.LBB0_46:                               #   in Loop: Header=BB0_3 Depth=1
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm2
.LBB0_47:                               #   in Loop: Header=BB0_3 Depth=1
	movslq	J(%rip), %rdx
	movslq	K(%rip), %rcx
	movl	$.L.str.3, %edi
	movb	$4, %al
	movq	136(%rsp), %rsi         # 8-byte Reload
	movapd	%xmm0, %xmm1
	movapd	%xmm2, %xmm3
	callq	printf
	movl	$1, J(%rip)
	movl	$2, K(%rip)
	movl	$3, L(%rip)
	movaps	.LCPI0_7(%rip), %xmm0   # xmm0 = [1.000000e+00,2.000000e+00]
	movups	%xmm0, E1+8(%rip)
	movq	%r12, E1+24(%rip)
	testb	%bl, %bl
	je	.LBB0_48
# BB#55:                                # %._crit_edge282.critedge
                                        #   in Loop: Header=BB0_3 Depth=1
	movsd	E1+32(%rip), %xmm3      # xmm3 = mem[0],zero
	movl	$.L.str.3, %edi
	movl	$1, %edx
	movl	$2, %ecx
	movb	$4, %al
	movq	%r14, %rsi
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI0_8(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_9(%rip), %xmm2   # xmm2 = mem[0],zero
	callq	printf
	movl	$2, J(%rip)
	movl	$3, K(%rip)
	movl	$.L.str.3, %edi
	xorl	%esi, %esi
	movl	$2, %edx
	movl	$3, %ecx
	movb	$4, %al
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	movapd	%xmm1, %xmm3
	callq	printf
	movsd	.LCPI0_10(%rip), %xmm0  # xmm0 = mem[0],zero
	movq	96(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_56
	.p2align	4, 0x90
.LBB0_48:                               # %.lr.ph276.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	xorl	%eax, %eax
	movsd	.LCPI0_9(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm2
	movapd	%xmm0, %xmm4
	movq	%r12, %rsi
	movsd	.LCPI0_8(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm3
	movapd	%xmm0, %xmm1
	movabsq	$4611686018427387904, %rdx # imm = 0x4000000000000000
	movq	96(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_49:                               # %.lr.ph276
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movapd	%xmm1, %xmm5
	movapd	%xmm3, %xmm0
	movq	%rsi, %rdx
	movapd	%xmm4, %xmm1
	movapd	%xmm2, %xmm3
	incq	%rax
	cmpq	%r14, %rax
	movapd	%xmm0, %xmm2
	movapd	%xmm5, %xmm4
	movq	%rcx, %rsi
	jl	.LBB0_49
# BB#50:                                # %._crit_edge277.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, E1+8(%rip)
	movsd	%xmm1, E1+16(%rip)
	movsd	%xmm0, E1+24(%rip)
	movsd	E1+32(%rip), %xmm3      # xmm3 = mem[0],zero
	movl	$.L.str.3, %edi
	movl	$1, %edx
	movl	$2, %ecx
	movb	$4, %al
	movq	%r14, %rsi
	movapd	%xmm0, %xmm2
	callq	printf
	movl	$2, J(%rip)
	movl	$3, K(%rip)
	movl	$.L.str.3, %edi
	xorl	%esi, %esi
	movl	$2, %edx
	movl	$3, %ecx
	movb	$4, %al
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	movapd	%xmm1, %xmm3
	callq	printf
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movsd	.LCPI0_10(%rip), %xmm0  # xmm0 = mem[0],zero
	jle	.LBB0_56
# BB#51:                                # %.lr.ph281.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	xorl	%ebp, %ebp
	movsd	.LCPI0_10(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB0_52:                               # %.lr.ph281
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	log
	divsd	T1(%rip), %xmm0
	callq	exp
	movapd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_54
# BB#53:                                # %call.sqrt
                                        #   in Loop: Header=BB0_52 Depth=2
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB0_54:                               # %.lr.ph281.split
                                        #   in Loop: Header=BB0_52 Depth=2
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB0_52
.LBB0_56:                               # %._crit_edge282
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	J(%rip), %rdx
	movslq	K(%rip), %rcx
	movl	$.L.str.3, %edi
	movb	$4, %al
	movq	%rbx, %rsi
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm2
	movapd	%xmm0, %xmm3
	callq	printf
	xorl	%r12d, %r12d
	xorl	%edi, %edi
	callq	time
	movl	$10, %edi
	callq	putchar
	movl	76(%rsp), %ecx          # 4-byte Reload
	testl	%ecx, %ecx
	movq	144(%rsp), %rbp         # 8-byte Reload
	jne	.LBB0_3
	jmp	.LBB0_57
.LBB0_9:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %r12d
.LBB0_57:                               # %.loopexit
	movl	%r12d, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.globl	POUT
	.p2align	4, 0x90
	.type	POUT,@function
POUT:                                   # @POUT
	.cfi_startproc
# BB#0:
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movl	$.L.str.3, %edi
	movb	$4, %al
	jmp	printf                  # TAILCALL
.Lfunc_end1:
	.size	POUT, .Lfunc_end1-POUT
	.cfi_endproc

	.globl	PA
	.p2align	4, 0x90
	.type	PA,@function
PA:                                     # @PA
	.cfi_startproc
# BB#0:
	movl	$0, J(%rip)
	movsd	T(%rip), %xmm1          # xmm1 = mem[0],zero
	movsd	T2(%rip), %xmm0         # xmm0 = mem[0],zero
	movsd	16(%rdi), %xmm2         # xmm2 = mem[0],zero
	movsd	24(%rdi), %xmm4         # xmm4 = mem[0],zero
	movsd	32(%rdi), %xmm6         # xmm6 = mem[0],zero
	movsd	8(%rdi), %xmm3          # xmm3 = mem[0],zero
	addsd	%xmm2, %xmm3
	addsd	%xmm4, %xmm3
	subsd	%xmm6, %xmm3
	mulsd	%xmm1, %xmm3
	addsd	%xmm3, %xmm2
	subsd	%xmm4, %xmm2
	addsd	%xmm6, %xmm2
	mulsd	%xmm1, %xmm2
	movapd	%xmm3, %xmm5
	subsd	%xmm2, %xmm5
	addsd	%xmm4, %xmm5
	addsd	%xmm6, %xmm5
	mulsd	%xmm1, %xmm5
	movapd	%xmm2, %xmm4
	subsd	%xmm3, %xmm4
	addsd	%xmm5, %xmm4
	addsd	%xmm6, %xmm4
	divsd	%xmm0, %xmm4
	addsd	%xmm2, %xmm3
	addsd	%xmm5, %xmm3
	subsd	%xmm4, %xmm3
	mulsd	%xmm1, %xmm3
	addsd	%xmm3, %xmm2
	subsd	%xmm5, %xmm2
	addsd	%xmm4, %xmm2
	mulsd	%xmm1, %xmm2
	movapd	%xmm3, %xmm6
	subsd	%xmm2, %xmm6
	addsd	%xmm5, %xmm6
	addsd	%xmm4, %xmm6
	mulsd	%xmm1, %xmm6
	movapd	%xmm2, %xmm5
	subsd	%xmm3, %xmm5
	addsd	%xmm6, %xmm5
	addsd	%xmm4, %xmm5
	divsd	%xmm0, %xmm5
	addsd	%xmm2, %xmm3
	addsd	%xmm6, %xmm3
	subsd	%xmm5, %xmm3
	mulsd	%xmm1, %xmm3
	addsd	%xmm3, %xmm2
	subsd	%xmm6, %xmm2
	addsd	%xmm5, %xmm2
	mulsd	%xmm1, %xmm2
	movapd	%xmm3, %xmm7
	subsd	%xmm2, %xmm7
	addsd	%xmm6, %xmm7
	addsd	%xmm5, %xmm7
	mulsd	%xmm1, %xmm7
	movapd	%xmm2, %xmm4
	subsd	%xmm3, %xmm4
	addsd	%xmm7, %xmm4
	addsd	%xmm5, %xmm4
	divsd	%xmm0, %xmm4
	addsd	%xmm2, %xmm3
	addsd	%xmm7, %xmm3
	subsd	%xmm4, %xmm3
	mulsd	%xmm1, %xmm3
	addsd	%xmm3, %xmm2
	subsd	%xmm7, %xmm2
	addsd	%xmm4, %xmm2
	mulsd	%xmm1, %xmm2
	movapd	%xmm3, %xmm5
	subsd	%xmm2, %xmm5
	addsd	%xmm7, %xmm5
	addsd	%xmm4, %xmm5
	mulsd	%xmm1, %xmm5
	movapd	%xmm2, %xmm6
	subsd	%xmm3, %xmm6
	addsd	%xmm5, %xmm6
	addsd	%xmm4, %xmm6
	divsd	%xmm0, %xmm6
	addsd	%xmm2, %xmm3
	addsd	%xmm5, %xmm3
	subsd	%xmm6, %xmm3
	mulsd	%xmm1, %xmm3
	addsd	%xmm3, %xmm2
	subsd	%xmm5, %xmm2
	addsd	%xmm6, %xmm2
	mulsd	%xmm1, %xmm2
	movapd	%xmm3, %xmm4
	subsd	%xmm2, %xmm4
	addsd	%xmm5, %xmm4
	addsd	%xmm6, %xmm4
	mulsd	%xmm1, %xmm4
	movapd	%xmm2, %xmm5
	subsd	%xmm3, %xmm5
	addsd	%xmm4, %xmm5
	addsd	%xmm6, %xmm5
	divsd	%xmm0, %xmm5
	addsd	%xmm2, %xmm3
	addsd	%xmm4, %xmm3
	subsd	%xmm5, %xmm3
	mulsd	%xmm1, %xmm3
	addsd	%xmm3, %xmm2
	subsd	%xmm4, %xmm2
	addsd	%xmm5, %xmm2
	mulsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm6
	subsd	%xmm3, %xmm6
	movsd	%xmm3, 8(%rdi)
	subsd	%xmm2, %xmm3
	addsd	%xmm4, %xmm3
	addsd	%xmm5, %xmm3
	mulsd	%xmm1, %xmm3
	addsd	%xmm3, %xmm6
	addsd	%xmm5, %xmm6
	divsd	%xmm0, %xmm6
	movsd	%xmm2, 16(%rdi)
	movsd	%xmm3, 24(%rdi)
	movsd	%xmm6, 32(%rdi)
	movl	$6, J(%rip)
	retq
.Lfunc_end2:
	.size	PA, .Lfunc_end2-PA
	.cfi_endproc

	.globl	P3
	.p2align	4, 0x90
	.type	P3,@function
P3:                                     # @P3
	.cfi_startproc
# BB#0:
	movsd	T(%rip), %xmm2          # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	divsd	T2(%rip), %xmm1
	movsd	%xmm1, (%rdi)
	retq
.Lfunc_end3:
	.size	P3, .Lfunc_end3-P3
	.cfi_endproc

	.globl	P0
	.p2align	4, 0x90
	.type	P0,@function
P0:                                     # @P0
	.cfi_startproc
# BB#0:
	movslq	K(%rip), %rax
	movq	E1(,%rax,8), %rcx
	movslq	J(%rip), %rdx
	movq	%rcx, E1(,%rdx,8)
	movslq	L(%rip), %rcx
	movq	E1(,%rcx,8), %rsi
	movq	%rsi, E1(,%rax,8)
	movq	E1(,%rdx,8), %rax
	movq	%rax, E1(,%rcx,8)
	retq
.Lfunc_end4:
	.size	P0, .Lfunc_end4-P0
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"-c"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"usage: whetdc [-c] [loops]\n"
	.size	.L.str.1, 28

	.type	T,@object               # @T
	.comm	T,8,8
	.type	T1,@object              # @T1
	.comm	T1,8,8
	.type	T2,@object              # @T2
	.comm	T2,8,8
	.type	E1,@object              # @E1
	.comm	E1,40,16
	.type	J,@object               # @J
	.comm	J,4,4
	.type	K,@object               # @K
	.comm	K,4,4
	.type	L,@object               # @L
	.comm	L,4,4
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%7ld %7ld %7ld %12.4e %12.4e %12.4e %12.4e\n"
	.size	.L.str.3, 44


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
