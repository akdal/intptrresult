	.text
	.file	"CopyRegister.bc"
	.p2align	4, 0x90
	.type	_ZL11CreateCodecv,@function
_ZL11CreateCodecv:                      # @_ZL11CreateCodecv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$40, %edi
	callq	_Znwm
	movl	$0, 16(%rax)
	movl	$_ZTVN9NCompress10CCopyCoderE+88, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN9NCompress10CCopyCoderE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rax)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rax)
	popq	%rcx
	retq
.Lfunc_end0:
	.size	_ZL11CreateCodecv, .Lfunc_end0-_ZL11CreateCodecv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_CopyRegister.ii,@function
_GLOBAL__sub_I_CopyRegister.ii:         # @_GLOBAL__sub_I_CopyRegister.ii
	.cfi_startproc
# BB#0:
	movl	$_ZL11g_CodecInfo, %edi
	jmp	_Z13RegisterCodecPK10CCodecInfo # TAILCALL
.Lfunc_end1:
	.size	_GLOBAL__sub_I_CopyRegister.ii, .Lfunc_end1-_GLOBAL__sub_I_CopyRegister.ii
	.cfi_endproc

	.type	_ZL11g_CodecInfo,@object # @_ZL11g_CodecInfo
	.data
	.p2align	3
_ZL11g_CodecInfo:
	.quad	_ZL11CreateCodecv
	.quad	_ZL11CreateCodecv
	.quad	0                       # 0x0
	.quad	.L.str
	.long	1                       # 0x1
	.byte	0                       # 0x0
	.zero	3
	.size	_ZL11g_CodecInfo, 40

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	67                      # 0x43
	.long	111                     # 0x6f
	.long	112                     # 0x70
	.long	121                     # 0x79
	.long	0                       # 0x0
	.size	.L.str, 20

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_CopyRegister.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
