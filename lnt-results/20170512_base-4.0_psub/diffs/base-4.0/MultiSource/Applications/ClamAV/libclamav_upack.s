	.text
	.file	"libclamav_upack.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1024                    # 0x400
	.long	1024                    # 0x400
	.long	1024                    # 0x400
	.long	1024                    # 0x400
.LCPI0_1:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	unupack
	.p2align	4, 0x90
	.type	unupack,@function
unupack:                                # @unupack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
	movl	%r8d, %r15d
	movabsq	$8589934584, %r8        # imm = 0x1FFFFFFF8
	testl	%edi, %edi
	je	.LBB0_24
# BB#1:
	xorl	%ebp, %ebp
	cmpb	$-1, 5(%rcx)
	jne	.LBB0_3
# BB#2:
	xorl	%eax, %eax
	cmpb	$54, 6(%rcx)
	sete	%al
	leal	(%rax,%rax,2), %ebp
.LBB0_3:
	movl	$-1, %r9d
	cmpl	$12, %edx
	jb	.LBB0_37
# BB#4:
	movl	1(%rcx), %r12d
	subl	%r15d, %r12d
	addq	%rsi, %r12
	leaq	12(%r12), %rax
	movl	%edx, %r14d
	addq	%rsi, %r14
	cmpq	%r14, %rax
	ja	.LBB0_37
# BB#5:
	cmpq	%rsi, %rax
	jbe	.LBB0_37
# BB#6:
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movl	%edi, 68(%rsp)          # 4-byte Spill
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movq	%rsi, (%rsp)            # 8-byte Spill
	movl	(%r12), %ecx
	movl	%ecx, %edx
	subl	%r15d, %edx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	movl	%edx, 64(%rsp)          # 4-byte Spill
	callq	cli_dbgmsg
	movl	%r13d, %eax
	testl	%ebp, %ebp
	je	.LBB0_36
# BB#7:
	movq	(%rsp), %rbx            # 8-byte Reload
	leaq	7(%rbx,%rax), %rcx
	cmpq	%rbx, %rcx
	movl	$-1, %r9d
	jb	.LBB0_37
# BB#8:
	addq	$5, %rcx
	cmpq	%r14, %rcx
	ja	.LBB0_37
# BB#9:
	cmpq	%rbx, %rcx
	jbe	.LBB0_37
# BB#10:
	addl	$7, %r13d
	cmpb	$-23, (%rbx,%r13)
	jne	.LBB0_37
# BB#11:
	movslq	8(%rbx,%rax), %rcx
	addq	%rbx, %rcx
	leaq	12(%rax,%rcx), %r13
	leaq	49(%rax,%rcx), %rax
	movl	$10, %r8d
                                        # implicit-def: %R12
.LBB0_12:
	cmpq	%rbx, %rax
	movl	12(%rsp), %ecx          # 4-byte Reload
	jb	.LBB0_37
# BB#13:
	leaq	2(%rax), %rdx
	cmpq	%r14, %rdx
	ja	.LBB0_37
# BB#14:
	cmpq	%rbx, %rdx
	jbe	.LBB0_37
# BB#15:
	cmpb	$-75, (%rax)
	jne	.LBB0_37
# BB#16:
	leaq	1(%rax), %rdx
	cmpq	%rbx, %rdx
	jb	.LBB0_37
# BB#17:
	testl	%ecx, %ecx
	je	.LBB0_37
# BB#18:
	movl	%r8d, %esi
	orl	$5, %esi
	cmpl	%ecx, %esi
	ja	.LBB0_118
# BB#19:
	movl	%r8d, %ecx
	leaq	5(%rdx,%rcx), %rsi
	cmpq	%r14, %rsi
	ja	.LBB0_118
# BB#20:
	cmpq	%rbx, %rsi
	jbe	.LBB0_118
# BB#21:
	addq	%rcx, %rdx
	cmpb	$-23, (%rdx)
	movl	$-1, %r9d
	jne	.LBB0_37
# BB#22:
	movzbl	1(%rax), %edi
	movl	1(%rdx), %ecx
	movq	%r13, %rax
	subq	%rbx, %rax
	addq	%rcx, %rax
	testl	%ebp, %ebp
	je	.LBB0_119
# BB#23:
	addq	$53, %rax
	jmp	.LBB0_120
.LBB0_24:
	movl	%r15d, %r12d
	leaq	(%rsi,%r12), %rbx
	movl	%r13d, %r10d
	addq	%r10, %rbx
	xorl	%r11d, %r11d
	cmpb	$-66, (%rcx)
	jne	.LBB0_28
# BB#25:
	cmpb	$-83, 5(%rcx)
	jne	.LBB0_28
# BB#26:
	cmpb	$-117, 6(%rcx)
	jne	.LBB0_28
# BB#27:
	xorl	%r11d, %r11d
	cmpb	$-8, 7(%rcx)
	sete	%r11b
.LBB0_28:
	testl	%r11d, %r11d
	movl	$388, %eax              # imm = 0x184
	movl	$449, %ecx              # imm = 0x1C1
	cmovneq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	$420, %ecx              # imm = 0x1A4
	movl	$535, %eax              # imm = 0x217
	cmovnel	%ecx, %eax
	movl	$27, %ecx
	movl	$58, %r14d
	cmovneq	%rcx, %r14
	movl	$65, %ecx
	movl	$95, %ebp
	cmovneq	%rcx, %rbp
	cmpq	%rsi, %rbx
	movl	$-1, %r9d
	jb	.LBB0_37
# BB#29:
	testl	%edx, %edx
	je	.LBB0_37
# BB#30:
	leal	4(%rax), %ecx
	cmpl	%edx, %ecx
	ja	.LBB0_37
# BB#31:
	movl	%r15d, 24(%rsp)         # 4-byte Spill
	movl	%eax, %eax
	leaq	4(%rbx,%rax), %rcx
	movl	%edx, %r15d
	addq	%rsi, %r15
	movq	%r15, 16(%rsp)          # 8-byte Spill
	cmpq	%r15, %rcx
	ja	.LBB0_37
# BB#32:
	cmpq	%rsi, %rcx
	jbe	.LBB0_37
# BB#33:
	movl	%r11d, 60(%rsp)         # 4-byte Spill
	movq	%r10, 80(%rsp)          # 8-byte Spill
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movl	%edi, 68(%rsp)          # 4-byte Spill
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	$4, %ecx
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %ecx
	leal	(%rbx,%rcx), %ecx
	leal	(%rax,%rcx), %edx
	addl	(%rbx,%rax), %edx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	movl	%edx, 64(%rsp)          # 4-byte Spill
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	callq	cli_dbgmsg
	movq	48(%rsp), %rbx          # 8-byte Reload
	movzbl	(%rbx,%rbp), %eax
	movl	$6, %ecx
	subl	%eax, %ecx
	cmpl	$7, %ecx
	jb	.LBB0_38
.LBB0_34:
	movl	$.L.str.1, %edi
.LBB0_35:                               # %.thread
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_36:
	movq	(%rsp), %rbx            # 8-byte Reload
	leaq	10(%rbx,%rax), %rcx
	cmpq	%rbx, %rcx
	movl	$-1, %r9d
	jb	.LBB0_37
# BB#49:
	addq	$2, %rcx
	cmpq	%r14, %rcx
	ja	.LBB0_37
# BB#50:
	cmpq	%rbx, %rcx
	jbe	.LBB0_37
# BB#51:
	addl	$10, %r13d
	cmpb	$-21, (%rbx,%r13)
	jne	.LBB0_37
# BB#52:
	movsbq	11(%rbx,%rax), %rdx
	addq	%rbx, %rdx
	leaq	38(%rax,%rdx), %rcx
	cmpq	%rbx, %rcx
	jb	.LBB0_37
# BB#53:
	leaq	2(%rcx), %rsi
	cmpq	%r14, %rsi
	ja	.LBB0_78
# BB#54:
	cmpq	%rbx, %rsi
	jbe	.LBB0_78
# BB#55:
	cmpb	$-21, (%rcx)
	movl	$-1, %r9d
	jne	.LBB0_37
# BB#56:
	movl	8(%r12), %r12d
	subl	%r15d, %r12d
	addq	%rbx, %r12
	leaq	12(%rax,%rdx), %r13
	movzbl	1(%rcx), %eax
	leaq	12(%rcx,%rax), %rax
	movl	$8, %r8d
	jmp	.LBB0_12
.LBB0_38:
	movl	$8, %ebp
	subl	%eax, %ebp
	movzbl	(%rbx,%r14), %r13d
	movl	%r13d, %edx
	shll	$8, %edx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	movq	%rdx, %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_dbgmsg
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	je	.LBB0_57
# BB#39:
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r13d, %ecx
	shll	$10, %ecx
	orl	$24, %ecx
	cmpl	12(%rsp), %ecx          # 4-byte Folded Reload
	ja	.LBB0_74
# BB#40:
	movq	(%rsp), %rax            # 8-byte Reload
	movslq	328(%rax), %rsi
	leaq	(%rax,%rsi), %r14
	movl	208(%rsp), %r10d
	subq	%r10, %r14
	leaq	4(%r14), %r15
	cmpq	%rax, %r15
	jb	.LBB0_74
# BB#41:
	movl	%ecx, %ecx
	addq	%r15, %rcx
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	ja	.LBB0_76
# BB#42:
	cmpq	%rax, %rcx
	jbe	.LBB0_76
# BB#43:                                # %.preheader9231018
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movq	%r10, %r9
	negq	%r9
	movl	332(%rax), %r12d
	addq	%rax, %r12
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	movq	%rcx, (%r15)
	movabsq	$4294967297, %rcx       # imm = 0x100000001
	movq	%rcx, 8(%r15)
	movq	%rcx, 16(%r15)
	testb	%r13b, %r13b
	movq	72(%rsp), %rdi          # 8-byte Reload
	je	.LBB0_86
# BB#44:                                # %.lr.ph969.preheader
	leaq	28(%rsi), %rdx
	subq	%r10, %rdx
	addq	%rax, %rdx
	cmpl	$1, %edi
	movl	$1, %r13d
	cmoval	%edi, %r13d
	decl	%r13d
	incq	%r13
	xorl	%ebp, %ebp
	cmpq	$8, %r13
	jb	.LBB0_85
# BB#45:                                # %min.iters.checked
	movabsq	$8589934584, %r11       # imm = 0x1FFFFFFF8
	andq	%r13, %r11
	je	.LBB0_85
# BB#46:                                # %vector.body.preheader
	leaq	-8(%r11), %r8
	movl	%r8d, %ebx
	shrl	$3, %ebx
	incl	%ebx
	andq	$3, %rbx
	je	.LBB0_79
# BB#47:                                # %vector.body.prol.preheader
	leaq	44(%rsi), %rcx
	subq	%r10, %rcx
	addq	%rax, %rcx
	negq	%rbx
	xorl	%ebp, %ebp
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [1024,1024,1024,1024]
.LBB0_48:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rcx,%rbp,4)
	movdqu	%xmm0, (%rcx,%rbp,4)
	addq	$8, %rbp
	incq	%rbx
	jne	.LBB0_48
	jmp	.LBB0_80
.LBB0_57:
	movl	6(%rbx), %ecx
	movl	$0, 6(%rbx)
	testl	%ecx, %ecx
	je	.LBB0_75
# BB#58:
	cmpl	$12, 12(%rsp)           # 4-byte Folded Reload
	jb	.LBB0_76
# BB#59:
	leaq	4(%rbx), %r15
	addl	$-2, %ecx
	subq	%rcx, %r15
	movq	(%rsp), %rdx            # 8-byte Reload
	cmpq	%rdx, %r15
	movl	$-1, %r9d
	jb	.LBB0_37
# BB#60:
	leaq	12(%r15), %rax
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_37
# BB#61:
	cmpq	%rdx, %rax
	jbe	.LBB0_37
# BB#62:
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movl	(%r15), %ecx
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movl	208(%rsp), %ebp
	movl	%ebp, %r8d
	movq	%rdx, %rbx
	callq	cli_dbgmsg
	movslq	(%r15), %rax
	addq	%rbx, %rax
	movl	%ebp, %r14d
	subq	%r14, %rax
	movq	%r15, %rsi
	subq	%rax, %rsi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movq	%rsi, %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movl	4(%r15), %esi
	movl	%esi, %edx
	subl	%ebp, %edx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movl	%esi, 24(%rsp)          # 4-byte Spill
	callq	cli_dbgmsg
	movslq	8(%r15), %rsi
	testq	%rsi, %rsi
	js	.LBB0_77
# BB#63:
	addq	$8, %r15
	addq	$4, %r15
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movq	%rsi, %rbp
	callq	cli_dbgmsg
	cmpq	%rbx, %r15
	jb	.LBB0_118
# BB#64:
	testl	%ebp, %ebp
	movl	$-1, %r9d
	movq	%rbp, %rdi
	je	.LBB0_37
# BB#65:
	movq	%rdi, %rax
	shlq	$2, %rax
	movl	12(%rsp), %esi          # 4-byte Reload
	cmpl	%esi, %eax
	ja	.LBB0_37
# BB#66:
	addq	%r15, %rax
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_37
# BB#67:
	cmpq	%rbx, %rax
	jbe	.LBB0_37
# BB#68:
	movslq	24(%rsp), %rax          # 4-byte Folded Reload
	leaq	(%rbx,%rax), %rcx
	subq	%r14, %rcx
	movq	%rcx, %r10
	cmpq	%rbx, %rcx
	jb	.LBB0_37
# BB#69:
	movq	72(%rsp), %rcx          # 8-byte Reload
	leal	(%rdi,%rcx), %ecx
	leal	-1(,%rcx,4), %edx
	cmpl	%esi, %edx
	jae	.LBB0_37
# BB#70:
	shll	$2, %ecx
	movl	%ecx, %ecx
	movq	%r10, %rdx
	addq	%rdx, %rcx
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	ja	.LBB0_37
# BB#71:
	cmpq	%rbx, %rcx
	jbe	.LBB0_37
# BB#72:                                # %.lr.ph965.preheader
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %r9
	leal	-1(%rdi), %r8d
	leaq	1(%r8), %rdx
	cmpq	$8, %rdx
	jae	.LBB0_138
# BB#73:
	movq	%r10, %rdx
	jmp	.LBB0_158
.LBB0_74:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_76:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_75:
	movl	$.L.str.5, %edi
	jmp	.LBB0_35
.LBB0_78:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_118:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_77:
	movl	$.L.str.9, %edi
	jmp	.LBB0_35
.LBB0_79:
	xorl	%ebp, %ebp
.LBB0_80:                               # %vector.body.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB0_83
# BB#81:                                # %vector.body.preheader.new
	leaq	140(%rsi,%rbp,4), %rsi
	subq	%r10, %rsi
	addq	%rax, %rsi
	movq	%r11, %rcx
	subq	%rbp, %rcx
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [1024,1024,1024,1024]
.LBB0_82:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -112(%rsi)
	movdqu	%xmm0, -96(%rsi)
	movdqu	%xmm0, -80(%rsi)
	movdqu	%xmm0, -64(%rsi)
	movdqu	%xmm0, -48(%rsi)
	movdqu	%xmm0, -32(%rsi)
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB0_82
.LBB0_83:                               # %middle.block
	movq	%r11, %rcx
	cmpq	%r11, %r13
	je	.LBB0_86
# BB#84:
	leaq	(%rdx,%rcx,4), %rdx
	movl	%ecx, %ebp
.LBB0_85:                               # %.lr.ph969
                                        # =>This Inner Loop Header: Depth=1
	movl	$1024, (%rdx)           # imm = 0x400
	incl	%ebp
	addq	$4, %rdx
	cmpl	%edi, %ebp
	jb	.LBB0_85
.LBB0_86:                               # %._crit_edge970
	subq	%r10, %r12
	movslq	336(%rax), %rbp
	addq	%rax, %rbp
	addq	%r9, %rbp
	movslq	304(%rax), %r13
	addq	%rax, %r13
	addq	%r9, %r13
	movq	%rax, %rbx
.LBB0_87:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movl	20(%rsp), %esi          # 4-byte Reload
	movq	%r15, %rcx
	movq	%rbp, %r15
	movq	%rbp, %r9
	pushq	%r12
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)               # 8-byte Folded Reload
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	unupack399
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	cmpl	$-1, %eax
	movl	$-1, %r9d
	je	.LBB0_37
# BB#88:
	movq	48(%rsp), %rax          # 8-byte Reload
	addq	40(%rsp), %rax          # 8-byte Folded Reload
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	24(%rsp), %edx          # 4-byte Reload
	je	.LBB0_91
# BB#89:
	je	.LBB0_93
# BB#90:
	movq	%rax, %rbx
	movl	%edx, %eax
	addq	(%rsp), %rax            # 8-byte Folded Reload
	leaq	372(%rcx,%rax), %r14
	jmp	.LBB0_92
.LBB0_91:
	movq	%rax, %rbx
	addq	$64, %r14
.LBB0_92:                               # %.sink.split
	movl	(%r14), %eax
	jmp	.LBB0_94
.LBB0_93:
	movq	%rax, %rbx
                                        # implicit-def: %EAX
.LBB0_94:
	movl	%eax, 40(%rsp)          # 4-byte Spill
.LBB0_95:
	movl	12(%rsp), %edx          # 4-byte Reload
	testl	%edx, %edx
	movq	(%rsp), %rcx            # 8-byte Reload
	je	.LBB0_117
# BB#96:
	cmpq	%rcx, %rbx
	jb	.LBB0_117
# BB#97:
	leaq	1(%rbx), %rax
	movl	%edx, %r14d
	addq	%rcx, %r14
	cmpq	%r14, %rax
	ja	.LBB0_117
# BB#98:
	cmpq	%rcx, %rax
	jbe	.LBB0_117
# BB#99:
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movzbl	(%rbx), %edx
	xorl	%ebp, %ebp
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	movl	40(%rsp), %r12d         # 4-byte Reload
	movl	%r12d, %esi
	movl	%edx, 32(%rsp)          # 4-byte Spill
	movq	%rcx, %rbx
	callq	cli_dbgmsg
	testl	%r12d, %r12d
	je	.LBB0_115
# BB#100:                               # %.lr.ph.lr.ph
	movl	32(%rsp), %edi          # 4-byte Reload
.LBB0_101:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_104 Depth 2
	movl	12(%rsp), %edx          # 4-byte Reload
	cmpl	$3, %edx
	jbe	.LBB0_142
# BB#102:                               # %.lr.ph.split.split.preheader
                                        #   in Loop: Header=BB0_101 Depth=1
	movl	%ebp, %r8d
	addq	%r15, %r8
	cmpq	%rbx, %r8
	jb	.LBB0_112
# BB#103:                               # %.lr.ph1043.preheader
                                        #   in Loop: Header=BB0_101 Depth=1
	addl	$5, %ebp
.LBB0_104:                              # %.lr.ph1043
                                        #   Parent Loop BB0_101 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%r8), %rax
	cmpq	%r14, %rax
	ja	.LBB0_112
# BB#105:                               # %.lr.ph1043
                                        #   in Loop: Header=BB0_104 Depth=2
	cmpq	%rbx, %rax
	jbe	.LBB0_112
# BB#106:                               #   in Loop: Header=BB0_104 Depth=2
	movzbl	(%r8), %ecx
	andb	$-2, %cl
	cmpb	$-24, %cl
	jne	.LBB0_111
# BB#107:                               #   in Loop: Header=BB0_104 Depth=2
	cmpq	%rbx, %rax
	jb	.LBB0_147
# BB#108:                               #   in Loop: Header=BB0_104 Depth=2
	addq	$5, %r8
	cmpq	%r14, %r8
	ja	.LBB0_147
# BB#109:                               #   in Loop: Header=BB0_104 Depth=2
	cmpq	%rbx, %r8
	jbe	.LBB0_147
# BB#110:                               #   in Loop: Header=BB0_104 Depth=2
	movl	(%rax), %ecx
	movzbl	%cl, %esi
	cmpl	%edi, %esi
	je	.LBB0_114
.LBB0_111:                              # %.lr.ph.split.split.backedge
                                        #   in Loop: Header=BB0_104 Depth=2
	leal	-4(%rbp), %eax
	movl	%eax, %r8d
	addq	%r15, %r8
	incl	%ebp
	cmpq	%rbx, %r8
	jae	.LBB0_104
	jmp	.LBB0_112
.LBB0_114:                              # %.thread916.outer
                                        #   in Loop: Header=BB0_101 Depth=1
	movl	%ecx, %edx
	shrl	$24, %edx
	movl	%ecx, %esi
	shrl	$8, %esi
	andl	$65280, %esi            # imm = 0xFF00
	orl	%edx, %esi
	shll	$8, %ecx
	andl	$16711680, %ecx         # imm = 0xFF0000
	orl	%esi, %ecx
	subl	%ebp, %ecx
	movl	%ecx, (%rax)
	decl	40(%rsp)                # 4-byte Folded Spill
	jne	.LBB0_101
.LBB0_115:                              # %.thread916.outer._crit_edge
	movl	224(%rsp), %eax
	movl	216(%rsp), %ecx
	subl	%r15d, %r13d
	cmpl	$0, 68(%rsp)            # 4-byte Folded Reload
	movl	$0, 120(%rsp)
	movl	%ecx, 112(%rsp)
	movl	%r13d, 124(%rsp)
	movl	%r13d, 116(%rsp)
	movl	%ecx, %ecx
	movl	$0, %edx
	cmoveq	%rcx, %rdx
	addq	%rdx, %rbx
	leaq	112(%rsp), %rsi
	movl	$1, %edx
	movl	$0, %r9d
	movq	%rbx, %rdi
	movl	64(%rsp), %r8d          # 4-byte Reload
	movl	208(%rsp), %ecx
	pushq	%rax
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	callq	cli_rebuildpe
	movl	$1, %r9d
	addq	$16, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB0_37
# BB#116:
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%r9d, %r9d
	jmp	.LBB0_37
.LBB0_117:
	movl	$.L.str.13, %edi
	jmp	.LBB0_35
.LBB0_119:
	movsbq	27(%r13), %rcx
	leaq	52(%rax,%rcx), %rax
.LBB0_120:
	movl	%eax, %ecx
	leaq	(%rbx,%rcx), %rax
	leaq	42(%rbx,%rcx), %rdx
	cmpq	%rbx, %rdx
	jae	.LBB0_123
.LBB0_121:
	leaq	45(%rax), %rcx
	cmpq	%rbx, %rcx
	jae	.LBB0_127
# BB#122:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_123:
	leaq	2(%rdx), %rsi
	cmpq	%r14, %rsi
	ja	.LBB0_121
# BB#124:
	cmpq	%rbx, %rsi
	jbe	.LBB0_121
# BB#125:
	cmpb	$-29, (%rdx)
	jne	.LBB0_121
# BB#126:
	movl	%edi, 16(%rsp)          # 4-byte Spill
	leaq	43(%rbx,%rcx), %rsi
	movl	$8, %edx
	movl	$24, %ecx
	jmp	.LBB0_131
.LBB0_127:
	movl	%edi, 16(%rsp)          # 4-byte Spill
	leaq	2(%rcx), %rdx
	cmpq	%r14, %rdx
	ja	.LBB0_113
# BB#128:
	cmpq	%rbx, %rdx
	jbe	.LBB0_148
# BB#129:
	cmpb	$-29, (%rcx)
	jne	.LBB0_148
# BB#130:
	leaq	46(%rax), %rsi
	xorl	%edi, %edi
	cmpl	$3, %ebp
	sete	%dil
	orl	$2, %edi
	movl	$7, %edx
	movl	$26, %ecx
	movl	%edi, %ebp
.LBB0_131:
	leal	5(%rdx), %edi
	cmpl	12(%rsp), %edi          # 4-byte Folded Reload
	ja	.LBB0_152
# BB#132:
	movzbl	(%rsi), %edi
	leaq	1(%rsi,%rdi), %rsi
	cmpq	(%rsp), %rsi            # 8-byte Folded Reload
	jb	.LBB0_152
# BB#133:
	movl	%edx, %edi
	leaq	5(%rsi,%rdi), %rbx
	cmpq	%r14, %rbx
	ja	.LBB0_176
# BB#134:
	cmpq	(%rsp), %rbx            # 8-byte Folded Reload
	jbe	.LBB0_176
# BB#135:
	addq	%rsi, %rdi
	cmpb	$-23, (%rdi)
	jne	.LBB0_176
# BB#136:
	orl	$32, %edx
	addl	1(%rdi), %edx
	leaq	(%rsi,%rdx), %rdi
	cmpl	$3, %ebp
	leaq	2(%rsi,%rdx), %rbx
	cmovneq	%rdi, %rbx
	addq	%rcx, %rax
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jae	.LBB0_177
# BB#137:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_138:                              # %min.iters.checked1054
	movq	%rdx, %rsi
	movabsq	$8589934584, %rcx       # imm = 0x1FFFFFFF8
	andq	%rcx, %rsi
	je	.LBB0_149
# BB#139:                               # %vector.memcheck
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r8,4), %rdi
	leaq	20(%r12,%rdi), %rdi
	subq	96(%rsp), %rdi          # 8-byte Folded Reload
	addq	(%rsp), %rdi            # 8-byte Folded Reload
	cmpq	%rdi, %r10
	jae	.LBB0_150
# BB#140:                               # %vector.memcheck
	leaq	4(%rax,%r8,4), %rdi
	subq	24(%rsp), %rdi          # 8-byte Folded Reload
	addq	(%rsp), %rdi            # 8-byte Folded Reload
	cmpq	%rdi, %r15
	jae	.LBB0_150
# BB#141:
	movq	%r10, %rdx
	jmp	.LBB0_158
.LBB0_148:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_142:                              # %.lr.ph.split.us.split.preheader
	xorl	%eax, %eax
.LBB0_143:                              # %.lr.ph.split.us.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %r8d
	addq	%r15, %r8
	cmpq	%rbx, %r8
	jb	.LBB0_112
# BB#144:                               #   in Loop: Header=BB0_143 Depth=1
	leaq	1(%r8), %rcx
	cmpq	%r14, %rcx
	ja	.LBB0_112
# BB#145:                               #   in Loop: Header=BB0_143 Depth=1
	cmpq	%rbx, %rcx
	jbe	.LBB0_112
# BB#146:                               #   in Loop: Header=BB0_143 Depth=1
	movzbl	(%r8), %ecx
	andb	$-2, %cl
	incl	%eax
	cmpb	$-24, %cl
	jne	.LBB0_143
.LBB0_147:                              # %.us-lcssa936.us
	movl	$.L.str.16, %edi
	jmp	.LBB0_35
.LBB0_112:                              # %.us-lcssa.us
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%r14, %rcx
	callq	cli_dbgmsg
.LBB0_113:
	movl	$-1, %r9d
.LBB0_37:                               # %.thread
	movl	%r9d, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_152:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_149:
	movq	%r10, %rdx
	jmp	.LBB0_158
.LBB0_150:                              # %vector.body1050.preheader
	leaq	-8(%rsi), %rdi
	movq	%rdi, %rbp
	shrq	$3, %rbp
	btl	$3, %edi
	jb	.LBB0_153
# BB#151:                               # %vector.body1050.prol
	movdqu	(%r15), %xmm0
	movups	16(%r15), %xmm1
	movq	%r10, %rcx
	movdqu	%xmm0, (%rcx)
	movups	%xmm1, 16(%rcx)
	movl	$8, %ebx
	testq	%rbp, %rbp
	jne	.LBB0_154
	jmp	.LBB0_156
.LBB0_153:
	xorl	%ebx, %ebx
	testq	%rbp, %rbp
	je	.LBB0_156
.LBB0_154:                              # %vector.body1050.preheader.new
	movq	%rsi, %rdi
	subq	%rbx, %rdi
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbx,4), %rbp
	leaq	64(%r12,%rbp), %rbp
	subq	96(%rsp), %rbp          # 8-byte Folded Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	addq	%rcx, %rbp
	leaq	48(%rax,%rbx,4), %rbx
	subq	24(%rsp), %rbx          # 8-byte Folded Reload
	addq	%rcx, %rbx
.LBB0_155:                              # %vector.body1050
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movdqu	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movdqu	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	addq	$64, %rbp
	addq	$64, %rbx
	addq	$-16, %rdi
	jne	.LBB0_155
.LBB0_156:                              # %middle.block1051
	cmpq	%rsi, %rdx
	je	.LBB0_163
# BB#157:
	subl	%esi, %r9d
	leaq	(%r10,%rsi,4), %rdx
	leaq	(%r15,%rsi,4), %r15
.LBB0_158:                              # %.lr.ph965.preheader1139
	leal	-1(%r9), %esi
	movl	%r9d, %edi
	andl	$7, %edi
	je	.LBB0_161
# BB#159:                               # %.lr.ph965.prol.preheader
	negl	%edi
.LBB0_160:                              # %.lr.ph965.prol
                                        # =>This Inner Loop Header: Depth=1
	decl	%r9d
	movl	(%r15), %ebp
	movl	%ebp, (%rdx)
	addq	$4, %rdx
	addq	$4, %r15
	incl	%edi
	jne	.LBB0_160
.LBB0_161:                              # %.lr.ph965.prol.loopexit
	cmpl	$7, %esi
	jb	.LBB0_163
.LBB0_162:                              # %.lr.ph965
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15), %esi
	movl	%esi, (%rdx)
	movl	4(%r15), %esi
	movl	%esi, 4(%rdx)
	movl	8(%r15), %esi
	movl	%esi, 8(%rdx)
	movl	12(%r15), %esi
	movl	%esi, 12(%rdx)
	movl	16(%r15), %esi
	movl	%esi, 16(%rdx)
	movl	20(%r15), %esi
	movl	%esi, 20(%rdx)
	movl	24(%r15), %esi
	movl	%esi, 24(%rdx)
	addl	$-8, %r9d
	movl	28(%r15), %esi
	leaq	32(%r15), %r15
	movl	%esi, 28(%rdx)
	leaq	32(%rdx), %rdx
	jne	.LBB0_162
.LBB0_163:                              # %._crit_edge966
	cmpl	$8, 12(%rsp)            # 4-byte Folded Reload
	jb	.LBB0_167
# BB#164:
	leaq	8(%r10), %rdx
	cmpq	16(%rsp), %rdx          # 8-byte Folded Reload
	ja	.LBB0_167
# BB#165:
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	jbe	.LBB0_167
# BB#166:
	movl	(%r10), %edx
	movl	88(%rsp), %esi          # 4-byte Reload
	leaq	4(%rsi), %rdi
	decl	%edx
	imulq	%rdi, %rdx
	addq	%rax, %rdx
	leaq	4(%rsi,%rdx), %rdx
	subq	24(%rsp), %rdx          # 8-byte Folded Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	4(%rcx,%rdx), %rsi
	cmpq	%rcx, %rsi
	jae	.LBB0_168
.LBB0_167:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_168:
	leaq	4(%rsi), %r15
	cmpq	16(%rsp), %r15          # 8-byte Folded Reload
	ja	.LBB0_176
# BB#169:
	cmpq	(%rsp), %r15            # 8-byte Folded Reload
	jbe	.LBB0_176
# BB#170:
	testb	%r13b, %r13b
	movq	72(%rsp), %rcx          # 8-byte Reload
	je	.LBB0_193
# BB#171:                               # %.lr.ph959.preheader
	leaq	4(%rax,%r8,4), %rdx
	subq	24(%rsp), %rdx          # 8-byte Folded Reload
	addq	(%rsp), %rdx            # 8-byte Folded Reload
	movl	(%rsi), %esi
	cmpl	$1, %ecx
	movl	$1, %edi
	cmoval	%ecx, %edi
	decl	%edi
	incq	%rdi
	xorl	%ebp, %ebp
	cmpq	$8, %rdi
	jb	.LBB0_191
# BB#172:                               # %min.iters.checked1087
	andq	%rdi, 32(%rsp)          # 8-byte Folded Spill
	je	.LBB0_191
# BB#173:                               # %vector.ph1091
	leaq	(,%r8,4), %r8
	movd	%esi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	-8(%rcx), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB0_185
# BB#174:                               # %vector.body1082.prol.preheader
	leaq	20(%r8,%rax), %rcx
	subq	24(%rsp), %rcx          # 8-byte Folded Reload
	addq	(%rsp), %rcx            # 8-byte Folded Reload
	negq	%rbp
	xorl	%ebx, %ebx
.LBB0_175:                              # %vector.body1082.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rcx,%rbx,4)
	movdqu	%xmm0, (%rcx,%rbx,4)
	addq	$8, %rbx
	incq	%rbp
	jne	.LBB0_175
	jmp	.LBB0_186
.LBB0_176:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_177:
	leaq	3(%rax), %rcx
	cmpq	%r14, %rcx
	ja	.LBB0_184
# BB#178:
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	jbe	.LBB0_184
# BB#179:
	cmpb	$-63, (%rax)
	jne	.LBB0_184
# BB#180:
	cmpb	$-19, 1(%rax)
	jne	.LBB0_203
# BB#181:
	movzbl	2(%rax), %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-2, %al
	cmpb	$7, %al
	jae	.LBB0_34
# BB#182:
	movl	16(%rsp), %edx          # 4-byte Reload
	shll	$8, %edx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%edx, 48(%rsp)          # 4-byte Spill
	callq	cli_dbgmsg
	cmpl	$3, %ebp
	jne	.LBB0_208
# BB#183:
	leaq	6(%r13), %rax
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jae	.LBB0_210
.LBB0_184:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_185:
	xorl	%ebx, %ebx
.LBB0_186:                              # %vector.body1082.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB0_189
# BB#187:                               # %vector.ph1091.new
	leaq	(%r8,%rbx,4), %rcx
	leaq	116(%rax,%rcx), %rax
	subq	24(%rsp), %rax          # 8-byte Folded Reload
	addq	(%rsp), %rax            # 8-byte Folded Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rbx, %rcx
.LBB0_188:                              # %vector.body1082
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -112(%rax)
	movdqu	%xmm0, -96(%rax)
	movdqu	%xmm0, -80(%rax)
	movdqu	%xmm0, -64(%rax)
	movdqu	%xmm0, -48(%rax)
	movdqu	%xmm0, -32(%rax)
	movdqu	%xmm0, -16(%rax)
	movdqu	%xmm0, (%rax)
	subq	$-128, %rax
	addq	$-32, %rcx
	jne	.LBB0_188
.LBB0_189:                              # %middle.block1083
	cmpq	32(%rsp), %rdi          # 8-byte Folded Reload
	je	.LBB0_193
# BB#190:
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	movl	%eax, %ebp
.LBB0_191:                              # %.lr.ph959.preheader1138
	movq	72(%rsp), %rax          # 8-byte Reload
.LBB0_192:                              # %.lr.ph959
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, (%rdx)
	incl	%ebp
	addq	$4, %rdx
	cmpl	%eax, %ebp
	jb	.LBB0_192
.LBB0_193:                              # %._crit_edge960
	leaq	16(%r15), %rax
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jae	.LBB0_195
# BB#194:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_195:
	addq	$4, %rax
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_199
# BB#196:
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jbe	.LBB0_199
# BB#197:
	addq	$4, %r10
	movq	88(%rsp), %rax          # 8-byte Reload
	addl	%eax, 16(%r15)
	movq	%r10, %rbx
	movl	(%r10), %ebp
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$28, 12(%rsp)           # 4-byte Folded Reload
	jb	.LBB0_199
# BB#198:
	addq	$20, %r15
	leaq	-4(%r15), %rax
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jae	.LBB0_200
.LBB0_199:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_200:
	addq	$28, %rax
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_203
# BB#201:
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jbe	.LBB0_203
# BB#202:
	addq	$4, %rbx
	movq	%rbx, %r14
	leaq	36(%rbx), %rax
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jae	.LBB0_204
.LBB0_203:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_204:
	leaq	4(%rax), %rcx
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	ja	.LBB0_224
# BB#205:
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	jbe	.LBB0_224
# BB#206:
	leaq	64(%r14), %rcx
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	jae	.LBB0_225
# BB#207:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_208:
	leaq	7(%r13), %rax
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jae	.LBB0_218
# BB#209:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_210:
	leaq	16(%r13), %rcx
	cmpq	%r14, %rcx
	ja	.LBB0_229
# BB#211:
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	jbe	.LBB0_229
# BB#212:
	cmpb	$-66, (%rax)
	jne	.LBB0_229
# BB#213:
	cmpb	$-65, 11(%r13)
	jne	.LBB0_229
# BB#214:
	movl	7(%r13), %eax
	movl	%eax, %ecx
	subl	208(%rsp), %ecx
	jb	.LBB0_241
# BB#215:
	cmpl	%r15d, %eax
	ja	.LBB0_241
# BB#216:
	movl	16(%rsp), %esi          # 4-byte Reload
	shll	$10, %esi
	orl	$112, %esi
	cmpl	12(%rsp), %esi          # 4-byte Folded Reload
	jbe	.LBB0_242
# BB#217:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_218:
	leaq	12(%r13), %rcx
	cmpq	%r14, %rcx
	ja	.LBB0_229
# BB#219:
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	jbe	.LBB0_229
# BB#220:
	cmpb	$-66, (%rax)
	jne	.LBB0_229
# BB#221:
	cmpq	(%rsp), %r12            # 8-byte Folded Reload
	jb	.LBB0_229
# BB#222:
	movl	16(%rsp), %eax          # 4-byte Reload
	shll	$10, %eax
	orl	$180, %eax
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	ja	.LBB0_229
# BB#223:
	movl	%eax, %eax
	addq	%r12, %rax
	cmpq	%r14, %rax
	jbe	.LBB0_230
.LBB0_224:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_225:
	addq	$4, %rcx
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	ja	.LBB0_229
# BB#226:
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	jbe	.LBB0_229
# BB#227:                               # %.preheader921.preheader
	movq	24(%rsp), %rcx          # 8-byte Reload
	negq	%rcx
	subl	208(%rsp), %ebp
	movq	(%rsp), %rbx            # 8-byte Reload
	addq	%rbx, %rbp
	movslq	-4(%r15), %r12
	addq	%rbx, %r12
	addq	%rcx, %r12
	movslq	(%rax), %r13
	addq	%rbx, %r13
	addq	%rcx, %r13
	movl	(%r15), %ecx
	movl	4(%r15), %eax
	movl	%eax, (%r15)
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	movl	%ecx, 4(%r15)
	jmp	.LBB0_87
.LBB0_229:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_230:
	cmpl	$212, 12(%rsp)          # 4-byte Folded Reload
	jb	.LBB0_243
# BB#231:
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jbe	.LBB0_243
# BB#232:
	movl	8(%r13), %ecx
	subl	%r15d, %ecx
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	leaq	212(%rax), %rdx
	cmpq	%r14, %rdx
	ja	.LBB0_249
# BB#233:
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	jbe	.LBB0_249
# BB#234:                               # %.preheader919.preheader
	addq	$156, %rcx
	movl	(%rax), %edx
	movl	%edx, (%r12)
	movl	4(%rax), %edx
	movl	%edx, 4(%r12)
	movl	8(%rax), %edx
	movl	%edx, 8(%r12)
	movl	12(%rax), %edx
	movl	%edx, 12(%r12)
	movl	16(%rax), %edx
	movl	%edx, 16(%r12)
	movl	20(%rax), %edx
	movl	%edx, 20(%r12)
	movl	24(%rax), %edx
	movl	%edx, 24(%r12)
	movl	28(%rax), %edx
	movl	%edx, 28(%r12)
	movl	32(%rax), %edx
	movl	%edx, 32(%r12)
	movl	36(%rax), %edx
	movl	%edx, 36(%r12)
	movl	40(%rax), %edx
	movl	%edx, 40(%r12)
	movl	44(%rax), %edx
	movl	%edx, 44(%r12)
	movl	48(%rax), %edx
	movl	%edx, 48(%r12)
	movl	52(%rax), %edx
	movl	%edx, 52(%r12)
	movl	56(%rax), %edx
	movl	%edx, 56(%r12)
	movl	60(%rax), %edx
	movl	%edx, 60(%r12)
	movl	64(%rax), %edx
	movl	%edx, 64(%r12)
	movl	68(%rax), %edx
	movl	%edx, 68(%r12)
	movl	72(%rax), %edx
	movl	%edx, 72(%r12)
	movl	76(%rax), %edx
	movl	%edx, 76(%r12)
	movl	80(%rax), %edx
	movl	%edx, 80(%r12)
	movl	84(%rax), %edx
	movl	%edx, 84(%r12)
	movl	88(%rax), %edx
	movl	%edx, 88(%r12)
	movl	92(%rax), %edx
	movl	%edx, 92(%r12)
	movl	96(%rax), %edx
	movl	%edx, 96(%r12)
	movl	100(%rax), %edx
	movl	%edx, 100(%r12)
	movl	104(%rax), %edx
	movl	%edx, 104(%r12)
	movl	108(%rax), %edx
	movl	%edx, 108(%r12)
	movl	112(%rax), %edx
	movl	%edx, 112(%r12)
	movl	116(%rax), %edx
	movl	%edx, 116(%r12)
	movl	120(%rax), %edx
	movl	%edx, 120(%r12)
	movl	124(%rax), %edx
	movl	%edx, 124(%r12)
	movl	128(%rax), %edx
	movl	%edx, 128(%r12)
	movl	132(%rax), %edx
	movl	%edx, 132(%r12)
	movl	136(%rax), %edx
	movl	%edx, 136(%r12)
	movl	140(%rax), %edx
	movl	%edx, 140(%r12)
	movl	144(%rax), %edx
	movl	%edx, 144(%r12)
	movl	148(%rax), %edx
	movl	%edx, 148(%r12)
	movl	152(%rax), %eax
	movl	%eax, 152(%r12)
	addq	$156, %r12
.LBB0_235:                              # %.loopexit
	movq	(%rsp), %rdx            # 8-byte Reload
	leaq	(%rdx,%rcx), %r10
	movl	4(%rdx,%rcx), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	-4(%r12), %r14d
	movl	%r15d, %r9d
	movq	%r9, %r15
	negq	%r9
	movdqa	.LCPI0_1(%rip), %xmm0   # xmm0 = [4294967295,0,1,1]
	movdqu	%xmm0, (%r12)
	movabsq	$4294967297, %rcx       # imm = 0x100000001
	movq	%rcx, 16(%r12)
	addq	%rdx, %r14
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB0_257
# BB#236:                               # %.lr.ph947.preheader
	leaq	24(%r12), %rdx
	movl	48(%rsp), %eax          # 4-byte Reload
	cmpl	$1, %eax
	movl	$1, %esi
	cmoval	%eax, %esi
	decl	%esi
	incq	%rsi
	xorl	%edi, %edi
	cmpq	$8, %rsi
	jb	.LBB0_256
# BB#237:                               # %min.iters.checked1114
	andq	%rsi, 32(%rsp)          # 8-byte Folded Spill
	je	.LBB0_256
# BB#238:                               # %vector.body1109.preheader
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	-8(%rax), %r8
	movl	%r8d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB0_250
# BB#239:                               # %vector.body1109.prol.preheader
	negq	%rdi
	xorl	%eax, %eax
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [1024,1024,1024,1024]
.LBB0_240:                              # %vector.body1109.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, 24(%r12,%rax,4)
	movdqu	%xmm0, 40(%r12,%rax,4)
	addq	$8, %rax
	incq	%rdi
	jne	.LBB0_240
	jmp	.LBB0_251
.LBB0_241:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_242:
	movl	12(%r13), %eax
	subl	%r15d, %eax
	movq	(%rsp), %rdx            # 8-byte Reload
	leaq	(%rdx,%rax), %rdx
	movl	%esi, %esi
	addq	%rdx, %rsi
	cmpq	%r14, %rsi
	jbe	.LBB0_244
.LBB0_243:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_244:
	cmpl	$192, 12(%rsp)          # 4-byte Folded Reload
	jb	.LBB0_249
# BB#245:
	cmpq	(%rsp), %rsi            # 8-byte Folded Reload
	jbe	.LBB0_249
# BB#246:
	movq	(%rsp), %rsi            # 8-byte Reload
	leaq	(%rsi,%rcx), %rsi
	leaq	192(%rsi), %rdi
	cmpq	%r14, %rdi
	ja	.LBB0_249
# BB#247:
	cmpq	(%rsp), %rdi            # 8-byte Folded Reload
	jbe	.LBB0_249
# BB#248:                               # %.preheader918.preheader
	movq	(%rsp), %r8             # 8-byte Reload
	movl	(%r8,%rcx), %edi
	movl	%edi, (%r8,%rax)
	movl	4(%r8,%rcx), %edi
	movl	%edi, 4(%r8,%rax)
	movl	8(%r8,%rcx), %edi
	movl	%edi, 8(%r8,%rax)
	movl	12(%r8,%rcx), %edi
	movl	%edi, 12(%r8,%rax)
	movl	16(%r8,%rcx), %edi
	leaq	88(%rcx), %rcx
	movl	%edi, 16(%r8,%rax)
	movl	20(%rsi), %edi
	movl	%edi, 20(%rdx)
	movl	24(%rsi), %edi
	movl	%edi, 24(%rdx)
	movl	28(%rsi), %edi
	movl	%edi, 28(%rdx)
	movl	32(%rsi), %edi
	movl	%edi, 32(%rdx)
	movl	36(%rsi), %edi
	movl	%edi, 36(%rdx)
	movl	40(%rsi), %edi
	movl	%edi, 40(%rdx)
	movl	44(%rsi), %edi
	movl	%edi, 44(%rdx)
	movl	48(%rsi), %edi
	movl	%edi, 48(%rdx)
	movl	52(%rsi), %edi
	movl	%edi, 52(%rdx)
	movl	56(%rsi), %edi
	movl	%edi, 56(%rdx)
	movl	60(%rsi), %edi
	movl	%edi, 60(%rdx)
	movl	64(%rsi), %edi
	movl	%edi, 64(%rdx)
	movl	68(%rsi), %edi
	movl	%edi, 68(%rdx)
	movl	72(%rsi), %edi
	movl	%edi, 72(%rdx)
	movl	76(%rsi), %edi
	movl	%edi, 76(%rdx)
	movl	80(%rsi), %edi
	movl	%edi, 80(%rdx)
	movl	84(%rsi), %esi
	movl	%esi, 84(%rdx)
	leaq	88(%r8,%rax), %r12
	jmp	.LBB0_235
.LBB0_249:
	movl	$-1, %r9d
	jmp	.LBB0_37
.LBB0_250:
	xorl	%eax, %eax
.LBB0_251:                              # %vector.body1109.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB0_254
# BB#252:                               # %vector.body1109.preheader.new
	leaq	136(%r12,%rax,4), %rdi
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rax, %rcx
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [1024,1024,1024,1024]
.LBB0_253:                              # %vector.body1109
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -112(%rdi)
	movdqu	%xmm0, -96(%rdi)
	movdqu	%xmm0, -80(%rdi)
	movdqu	%xmm0, -64(%rdi)
	movdqu	%xmm0, -48(%rdi)
	movdqu	%xmm0, -32(%rdi)
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	subq	$-128, %rdi
	addq	$-32, %rcx
	jne	.LBB0_253
.LBB0_254:                              # %middle.block1110
	cmpq	32(%rsp), %rsi          # 8-byte Folded Reload
	je	.LBB0_257
# BB#255:
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	movl	%eax, %edi
.LBB0_256:                              # %.lr.ph947
                                        # =>This Inner Loop Header: Depth=1
	movl	$1024, (%rdx)           # imm = 0x400
	incl	%edi
	addq	$4, %rdx
	cmpl	48(%rsp), %edi          # 4-byte Folded Reload
	jb	.LBB0_256
.LBB0_257:                              # %._crit_edge
	subq	%r15, %r14
	movslq	12(%r10), %rcx
	movq	(%rsp), %rdx            # 8-byte Reload
	addq	%rdx, %rcx
	addq	%r9, %rcx
	addq	%rdx, %r15
	movl	208(%rsp), %edx
	subq	%rdx, %r15
	cmpl	$3, %ebp
	cmovneq	%rcx, %r15
	jne	.LBB0_259
# BB#258:
	movl	64(%r10), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	addq	$100, %r10
	jmp	.LBB0_260
.LBB0_259:
	addq	$52, %r10
.LBB0_260:
	movslq	(%r10), %r13
	movq	(%rsp), %rbp            # 8-byte Reload
	addq	%rbp, %r13
	addq	%r9, %r13
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	subq	$8, %rsp
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movl	20(%rsp), %esi          # 4-byte Reload
	movq	%r12, %rcx
	movq	%r15, %r9
	pushq	%r14
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	callq	unupack399
	addq	$32, %rsp
.Lcfi25:
	.cfi_adjust_cfa_offset -32
	cmpl	$-1, %eax
	movl	$-1, %r9d
	jne	.LBB0_95
	jmp	.LBB0_37
.Lfunc_end0:
	.size	unupack, .Lfunc_end0-unupack
	.cfi_endproc

	.globl	unupack399
	.p2align	4, 0x90
	.type	unupack399,@function
unupack399:                             # @unupack399
	.cfi_startproc
# BB#0:                                 # %.preheader287306
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 224
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movq	%rcx, %rbp
	movl	%edx, %r14d
	movl	%esi, %r12d
	movq	%rdi, %rbx
	movq	240(%rsp), %rsi
	movl	%r8d, 12(%rsp)
	movq	%rsi, 48(%rsp)
	movl	(%rbp), %edx
	movl	%edx, 56(%rsp)
	movl	4(%rbp), %ecx
	movl	%ecx, 60(%rsp)
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	(%rbp), %edx
	movl	%edx, 80(%rsp)
	movl	$.L.str.19, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	4(%rbp), %edx
	movl	%edx, 84(%rsp)
	movl	$.L.str.19, %edi
	movl	$1, %esi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	8(%rbp), %edx
	movl	%edx, 88(%rsp)
	movl	$.L.str.19, %edi
	movl	$2, %esi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	12(%rbp), %edx
	movl	%edx, 92(%rsp)
	movl	$.L.str.19, %edi
	movl	$3, %esi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	16(%rbp), %edx
	movl	%edx, 96(%rsp)
	movl	$.L.str.19, %edi
	movl	$4, %esi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	20(%rbp), %edx
	movl	%edx, 100(%rsp)
	movl	$.L.str.19, %edi
	movl	$5, %esi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r12d, %r8d
	addq	%rbx, %r8
	leaq	3008(%rbp), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	leaq	24(%rbp), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	testl	%r12d, %r12d
	sete	31(%rsp)                # 1-byte Folded Spill
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	leaq	1912(%rbp), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	leaq	48(%rsp), %rbp
	movq	%r13, 160(%rsp)         # 8-byte Spill
	movq	%rbx, %rdx
	movq	%r12, 72(%rsp)          # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%r8, 40(%rsp)           # 8-byte Spill
	jmp	.LBB1_17
.LBB1_1:                                #   in Loop: Header=BB1_17 Depth=1
	movq	%r9, 120(%rsp)          # 8-byte Spill
.LBB1_2:                                #   in Loop: Header=BB1_17 Depth=1
	movl	$1, %edx
	movl	%r15d, %ecx
	shll	%cl, %edx
	movl	%edx, 12(%rsp)
	movl	%r15d, 8(%rsp)
	movl	$1, %esi
	movq	%rbp, %rdi
	leaq	16(%rsp), %rcx
	leaq	8(%rsp), %r9
	pushq	%r12
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	callq	lzma_upack_esi_50
	movq	%rbx, %rdx
	addq	$16, %rsp
.Lcfi41:
	.cfi_adjust_cfa_offset -16
	cmpl	$-1, %eax
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	$-1, %edi
	movq	128(%rsp), %r9          # 8-byte Reload
	je	.LBB1_94
# BB#3:                                 #   in Loop: Header=BB1_17 Depth=1
	movl	%r15d, 12(%rsp)
	movl	8(%rsp), %eax
	movl	%eax, %ebp
	sarl	$31, %ebp
	leal	-1(%r15), %ecx
	movl	%r15d, %edi
	andl	$3, %edi
	je	.LBB1_13
# BB#4:                                 # %.prol.preheader
                                        #   in Loop: Header=BB1_17 Depth=1
	negl	%edi
	movq	72(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_5:                                #   Parent Loop BB1_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %esi
	andl	$1, %esi
	leal	(%rsi,%rbp,2), %ebp
	shrl	%eax
	decl	%r15d
	incl	%edi
	jne	.LBB1_5
	jmp	.LBB1_14
.LBB1_6:                                #   in Loop: Header=BB1_17 Depth=1
	movl	8(%rsp), %r14d
	orl	$1, %r14d
	movl	%r14d, 8(%rsp)
	movq	%r13, %rax
	subq	%r15, %rax
	movb	$-128, 12(%rsp)
	cmpq	%rbx, %rax
	setb	%cl
	movl	$-1, %edi
	orb	31(%rsp), %cl           # 1-byte Folded Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	jne	.LBB1_94
# BB#7:                                 #   in Loop: Header=BB1_17 Depth=1
	testl	%r12d, %r12d
	sete	%cl
	leaq	1(%rax), %rsi
	cmpq	%rsi, %r8
	setb	%dl
	cmpq	%rbx, %rsi
	movq	%rbx, %rsi
	setbe	%bl
	cmpq	%rsi, %r13
	jb	.LBB1_94
# BB#8:                                 #   in Loop: Header=BB1_17 Depth=1
	orb	%bl, %cl
	orb	%cl, %dl
	jne	.LBB1_94
# BB#9:                                 #   in Loop: Header=BB1_17 Depth=1
	leaq	1(%r13), %rcx
	cmpq	%r8, %rcx
	ja	.LBB1_94
# BB#10:                                #   in Loop: Header=BB1_17 Depth=1
	movq	32(%rsp), %rdx          # 8-byte Reload
	cmpq	%rdx, %rcx
	jbe	.LBB1_94
# BB#11:                                #   in Loop: Header=BB1_17 Depth=1
	movb	(%rax), %al
	jmp	.LBB1_85
.LBB1_12:                               #   in Loop: Header=BB1_17 Depth=1
	testl	%ecx, %ecx
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	$-1, %edi
	je	.LBB1_80
	jmp	.LBB1_94
.LBB1_13:                               #   in Loop: Header=BB1_17 Depth=1
	movq	72(%rsp), %r12          # 8-byte Reload
.LBB1_14:                               # %.prol.loopexit
                                        #   in Loop: Header=BB1_17 Depth=1
	cmpl	$3, %ecx
	jb	.LBB1_16
	.p2align	4, 0x90
.LBB1_15:                               #   Parent Loop BB1_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ecx
	andl	$1, %ecx
	leal	(%rcx,%rbp,2), %ecx
	movl	%eax, %esi
	andl	$2, %esi
	leal	(%rsi,%rcx,4), %ecx
	movl	%eax, %esi
	shrl	$2, %esi
	andl	$1, %esi
	orl	%ecx, %esi
	movl	%eax, %ecx
	shrl	$3, %ecx
	andl	$1, %ecx
	leal	(%rcx,%rsi,2), %ebp
	shrl	$4, %eax
	addl	$-4, %r15d
	jne	.LBB1_15
.LBB1_16:                               #   in Loop: Header=BB1_17 Depth=1
	movl	%eax, 8(%rsp)
	movl	$0, 12(%rsp)
	addl	120(%rsp), %ebp         # 4-byte Folded Reload
	incl	%ebp
	jmp	.LBB1_65
	.p2align	4, 0x90
.LBB1_17:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_46 Depth 2
                                        #     Child Loop BB1_5 Depth 2
                                        #     Child Loop BB1_15 Depth 2
                                        #     Child Loop BB1_77 Depth 2
                                        #     Child Loop BB1_24 Depth 2
                                        #     Child Loop BB1_34 Depth 2
	movl	%r14d, 8(%rsp)
	shll	$2, %r14d
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	88(%rax,%r14), %rsi
	movq	%rsi, 16(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %ecx
	movq	%rdx, %rbx
	callq	lzma_upack_esi_00
	movl	8(%rsp), %r14d
	testl	%eax, %eax
	je	.LBB1_23
# BB#18:                                #   in Loop: Header=BB1_17 Depth=1
	movzbl	%r14b, %eax
	addl	$249, %eax
	xorl	%ecx, %ecx
	cmpl	$255, %eax
	seta	%cl
	leal	(%rcx,%rcx,2), %eax
	andl	$-256, %r14d
	leal	8(%r14,%rax), %eax
	movl	%eax, 8(%rsp)
	movl	88(%rsp), %r15d
	movl	12(%rsp), %esi
	movl	$-256, %eax
	andl	%eax, %esi
	orl	$48, %esi
	movl	%esi, 12(%rsp)
	addq	16(%rsp), %rsi
	movq	%rsi, 16(%rsp)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	movl	%r12d, %ecx
	callq	lzma_upack_esi_00
	testl	%eax, %eax
	je	.LBB1_40
# BB#19:                                #   in Loop: Header=BB1_17 Depth=1
	movl	12(%rsp), %esi
	addq	16(%rsp), %rsi
	movq	%rsi, 16(%rsp)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	movl	%r12d, %ecx
	callq	lzma_upack_esi_00
	testl	%eax, %eax
	je	.LBB1_55
# BB#20:                                #   in Loop: Header=BB1_17 Depth=1
	movq	16(%rsp), %rsi
	addq	$96, %rsi
	movq	%rsi, 16(%rsp)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	movl	%r12d, %ecx
	callq	lzma_upack_esi_00
	testl	%eax, %eax
	je	.LBB1_60
# BB#21:                                #   in Loop: Header=BB1_17 Depth=1
	movl	12(%rsp), %esi
	addq	16(%rsp), %rsi
	movq	%rsi, 16(%rsp)
	movq	%rbp, %rdi
	movq	%rbx, %r14
	movq	%rbx, %rdx
	movl	%r12d, %ecx
	callq	lzma_upack_esi_00
	movl	92(%rsp), %ecx
	movl	96(%rsp), %ebp
	movl	%ecx, 96(%rsp)
	movl	%r15d, 92(%rsp)
	testl	%eax, %eax
	je	.LBB1_62
# BB#22:                                #   in Loop: Header=BB1_17 Depth=1
	movl	$5, %eax
	movl	%ebp, %r15d
	leaq	16(%rsp), %rcx
	movq	%r14, %r9
	jmp	.LBB1_61
	.p2align	4, 0x90
.LBB1_23:                               #   in Loop: Header=BB1_17 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_24:                               # %.preheader286
                                        #   Parent Loop BB1_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%r14b, %eax
	leal	253(%rax), %ecx
	addl	$-3, %eax
	cmpl	$255, %ecx
	cmovbel	%edx, %eax
	andl	$-256, %r14d
	orl	%eax, %r14d
	cmpl	$6, %eax
	ja	.LBB1_24
# BB#25:                                #   in Loop: Header=BB1_17 Depth=1
	movl	%r14d, 8(%rsp)
	xorl	%eax, %eax
	cmpq	160(%rsp), %r13         # 8-byte Folded Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	jbe	.LBB1_28
# BB#26:                                #   in Loop: Header=BB1_17 Depth=1
	cmpq	%rdx, %r13
	jae	.LBB1_28
# BB#27:                                #   in Loop: Header=BB1_17 Depth=1
	movzbl	-1(%r13), %eax
	movl	232(%rsp), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
.LBB1_28:                               #   in Loop: Header=BB1_17 Depth=1
	leal	(%rax,%rax,2), %ebp
	shll	$10, %ebp
	addl	$4104, %ebp             # imm = 0x1008
	addq	112(%rsp), %rbp         # 8-byte Folded Reload
	movl	%r14d, %esi
	andl	$-256, %esi
	orl	$1, %esi
	movl	%esi, 8(%rsp)
	movl	12(%rsp), %ebx
	testl	%ebx, %ebx
	leaq	16(%rsp), %rax
	je	.LBB1_57
# BB#29:                                #   in Loop: Header=BB1_17 Depth=1
	testl	%r12d, %r12d
	movl	$-1, %edi
	je	.LBB1_94
# BB#30:                                #   in Loop: Header=BB1_17 Depth=1
	movl	88(%rsp), %eax
	movq	%r13, %r15
	subq	%rax, %r15
	cmpq	32(%rsp), %r15          # 8-byte Folded Reload
	jb	.LBB1_94
# BB#31:                                #   in Loop: Header=BB1_17 Depth=1
	leaq	1(%r15), %rax
	cmpq	%rdx, %rax
	ja	.LBB1_94
# BB#32:                                #   in Loop: Header=BB1_17 Depth=1
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB1_34
	jmp	.LBB1_94
	.p2align	4, 0x90
.LBB1_33:                               # %..preheader_crit_edge
                                        #   in Loop: Header=BB1_34 Depth=2
	movl	8(%rsp), %esi
	movq	72(%rsp), %r12          # 8-byte Reload
.LBB1_34:                               # %.preheader
                                        #   Parent Loop BB1_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	andl	$-65281, %esi           # imm = 0xFFFF00FF
	movzbl	(%r15), %eax
	movzbl	%bl, %ebx
	testl	%ebx, %eax
	movl	$256, %eax              # imm = 0x100
	movl	$512, %ecx              # imm = 0x200
	cmovnel	%ecx, %eax
	orl	%esi, %eax
	movl	%eax, 8(%rsp)
	shll	$2, %eax
	addq	%rbp, %rax
	movq	%rax, 16(%rsp)
	leaq	48(%rsp), %rdi
	movq	%rax, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	%r12d, %ecx
	callq	lzma_upack_esi_00
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	8(%rsp), %esi
	leal	(%rax,%rsi,2), %eax
	movb	%al, 8(%rsp)
	shrl	%ebx
	je	.LBB1_58
# BB#35:                                #   in Loop: Header=BB1_34 Depth=2
	movl	%esi, %ecx
	shrl	$8, %ecx
	subl	%eax, %ecx
	xorl	%r12d, %r12d
	testb	$1, %cl
	movl	$0, %ecx
	jne	.LBB1_37
# BB#36:                                #   in Loop: Header=BB1_34 Depth=2
	movzbl	%al, %eax
	andl	$-65536, %esi           # imm = 0xFFFF0000
	orl	%eax, %esi
	movl	%esi, 8(%rsp)
	movl	$256, %edx              # imm = 0x100
	leaq	48(%rsp), %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	leaq	16(%rsp), %rcx
	movq	%rbp, %r8
	leaq	8(%rsp), %r9
	pushq	72(%rsp)                # 8-byte Folded Reload
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	callq	lzma_upack_esi_50
	addq	$16, %rsp
.Lcfi44:
	.cfi_adjust_cfa_offset -16
	cmpl	$-1, %eax
	movl	$16, %ecx
	movl	$1, %eax
	cmovel	%eax, %ecx
.LBB1_37:                               #   in Loop: Header=BB1_34 Depth=2
	movl	%ecx, %eax
	andb	$31, %al
	je	.LBB1_33
# BB#38:                                #   in Loop: Header=BB1_17 Depth=1
	cmpb	$16, %al
	movq	40(%rsp), %r8           # 8-byte Reload
	jne	.LBB1_12
# BB#39:                                #   in Loop: Header=BB1_17 Depth=1
	movq	32(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB1_80
	.p2align	4, 0x90
.LBB1_40:                               #   in Loop: Header=BB1_17 Depth=1
	movl	8(%rsp), %r14d
	decl	%r14d
	movl	%r14d, 8(%rsp)
	movl	92(%rsp), %eax
	movl	96(%rsp), %ecx
	movl	%eax, 96(%rsp)
	movl	%r15d, 92(%rsp)
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rax, 16(%rsp)
	movl	%ecx, 100(%rsp)
	subq	$8, %rsp
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	movq	%rbp, %rdi
	movl	%r14d, %esi
	leaq	20(%rsp), %rdx
	leaq	24(%rsp), %r15
	movq	%r15, %rcx
	leaq	76(%rsp), %r8
	movq	%rbx, %r9
	pushq	%r12
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	callq	lzma_upack_esi_54
	addq	$16, %rsp
.Lcfi47:
	.cfi_adjust_cfa_offset -16
	movl	$-1, %edi
	cmpl	$-1, %eax
	je	.LBB1_94
# BB#41:                                #   in Loop: Header=BB1_17 Depth=1
	movl	68(%rsp), %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leal	-1(%rax), %eax
	cmpl	$3, %eax
	movl	$3, %ecx
	cmovael	%ecx, %eax
	movl	$64, 12(%rsp)
	movl	%eax, %ecx
	shll	$6, %ecx
	movl	%ecx, 8(%rsp)
	shll	$8, %eax
	movq	112(%rsp), %rcx         # 8-byte Reload
	leaq	888(%rcx,%rax), %r8
	movl	$1, %esi
	movl	$64, %edx
	movq	%rbp, %rdi
	movq	%r15, %rcx
	leaq	8(%rsp), %r9
	pushq	%r12
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	callq	lzma_upack_esi_50
	addq	$16, %rsp
.Lcfi50:
	.cfi_adjust_cfa_offset -16
	cmpl	$-1, %eax
	je	.LBB1_93
# BB#42:                                #   in Loop: Header=BB1_17 Depth=1
	movl	8(%rsp), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	cmpl	$4, %ecx
	jb	.LBB1_59
# BB#43:                                #   in Loop: Header=BB1_17 Depth=1
	movl	%eax, %r9d
	andl	$1, %r9d
	orl	$2, %r9d
	shrl	%eax
	leal	-1(%rax), %r15d
	movl	12(%rsp), %ecx
	movl	%ecx, 8(%rsp)
	movl	%r15d, 12(%rsp)
	movl	%r15d, %ecx
	shll	%cl, %r9d
	leal	(,%r9,4), %ecx
	movq	112(%rsp), %rdx         # 8-byte Reload
	leaq	376(%rdx,%rcx), %r8
	movq	%r8, 16(%rsp)
	movl	%r15d, %ecx
	andl	$254, %ecx
	cmpl	$6, %ecx
	movq	32(%rsp), %rbx          # 8-byte Reload
	jb	.LBB1_1
# BB#44:                                #   in Loop: Header=BB1_17 Depth=1
	andl	$-256, %r15d
	addl	$251, %eax
	movzbl	%al, %ecx
	orl	%r15d, %ecx
	movl	%ecx, 12(%rsp)
	movl	$0, 8(%rsp)
	cmpl	$4, %r12d
	jb	.LBB1_93
# BB#45:                                # %.split.preheader
                                        #   in Loop: Header=BB1_17 Depth=1
	movq	48(%rsp), %rdx
	decl	%ecx
	xorl	%eax, %eax
	movq	40(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_46:                               # %.split
                                        #   Parent Loop BB1_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, %rdx
	jb	.LBB1_93
# BB#47:                                #   in Loop: Header=BB1_46 Depth=2
	leaq	4(%rdx), %rsi
	cmpq	%r8, %rsi
	ja	.LBB1_93
# BB#48:                                #   in Loop: Header=BB1_46 Depth=2
	cmpq	%rbx, %rsi
	jbe	.LBB1_93
# BB#49:                                #   in Loop: Header=BB1_46 Depth=2
	movl	(%rdx), %ebx
	bswapl	%ebx
	movl	56(%rsp), %ebp
	movl	%ebp, %esi
	shrl	%esi
	movl	%esi, 56(%rsp)
	movl	60(%rsp), %edi
	subl	%edi, %ebx
	addl	%eax, %eax
	movl	%eax, 8(%rsp)
	cmpl	%esi, %ebx
	jb	.LBB1_51
# BB#50:                                #   in Loop: Header=BB1_46 Depth=2
	orl	$1, %eax
	movl	%eax, 8(%rsp)
	addl	%esi, %edi
	movl	%edi, 60(%rsp)
.LBB1_51:                               #   in Loop: Header=BB1_46 Depth=2
	movq	32(%rsp), %rbx          # 8-byte Reload
	cmpl	$33554431, %ebp         # imm = 0x1FFFFFF
	ja	.LBB1_53
# BB#52:                                #   in Loop: Header=BB1_46 Depth=2
	shll	$8, %edi
	movl	%edi, 60(%rsp)
	shll	$8, %esi
	movl	%esi, 56(%rsp)
	incq	%rdx
	movq	%rdx, 48(%rsp)
.LBB1_53:                               #   in Loop: Header=BB1_46 Depth=2
	leaq	48(%rsp), %rbp
	movl	%ecx, 12(%rsp)
	decl	%ecx
	cmpl	$-1, %ecx
	jne	.LBB1_46
# BB#54:                                #   in Loop: Header=BB1_17 Depth=1
	movl	$4, 12(%rsp)
	shll	$4, %eax
	movl	%eax, 8(%rsp)
	addl	%r9d, %eax
	movq	136(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 16(%rsp)
	movl	$4, %r15d
	movq	%rcx, %r8
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 120(%rsp)         # 8-byte Spill
	jmp	.LBB1_2
.LBB1_55:                               #   in Loop: Header=BB1_17 Depth=1
	movl	12(%rsp), %esi
	addq	16(%rsp), %rsi
	movq	%rsi, 16(%rsp)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	movl	%r12d, %ecx
	callq	lzma_upack_esi_00
	testl	%eax, %eax
	je	.LBB1_6
# BB#56:                                #   in Loop: Header=BB1_17 Depth=1
	movl	%r15d, %ebp
	leaq	16(%rsp), %rcx
	movq	%rbx, %r9
	jmp	.LBB1_63
.LBB1_57:                               #   in Loop: Header=BB1_17 Depth=1
	movl	$256, 12(%rsp)          # imm = 0x100
	movl	$256, %edx              # imm = 0x100
	leaq	48(%rsp), %rdi
	movq	%rax, %rcx
	movq	%rbp, %r8
	leaq	8(%rsp), %r9
	pushq	%r12
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rbx          # 8-byte Reload
	pushq	%rbx
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	callq	lzma_upack_esi_50
	movq	%rbx, %rdx
	movq	56(%rsp), %r8           # 8-byte Reload
	addq	$16, %rsp
.Lcfi53:
	.cfi_adjust_cfa_offset -16
	xorl	%r12d, %r12d
	cmpl	$-1, %eax
	jne	.LBB1_80
	jmp	.LBB1_93
.LBB1_58:                               #   in Loop: Header=BB1_17 Depth=1
	xorl	%r12d, %r12d
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	jmp	.LBB1_80
.LBB1_59:                               #   in Loop: Header=BB1_17 Depth=1
	movl	%eax, %ebp
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	128(%rsp), %r9          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	incl	%ebp
	jmp	.LBB1_65
.LBB1_60:                               #   in Loop: Header=BB1_17 Depth=1
	movl	$3, %eax
	leaq	16(%rsp), %rcx
	movq	%rbx, %r9
.LBB1_61:                               # %.sink.split
                                        #   in Loop: Header=BB1_17 Depth=1
	movl	80(%rsp,%rax,4), %ebp
	movl	%r15d, 80(%rsp,%rax,4)
	jmp	.LBB1_63
.LBB1_62:                               #   in Loop: Header=BB1_17 Depth=1
	leaq	16(%rsp), %rcx
	movq	%r14, %r9
	.p2align	4, 0x90
.LBB1_63:                               #   in Loop: Header=BB1_17 Depth=1
	movl	8(%rsp), %r14d
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	%rax, 16(%rsp)
	subq	$8, %rsp
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	leaq	56(%rsp), %rdi
	movl	%r14d, %esi
	leaq	20(%rsp), %rdx
	leaq	76(%rsp), %r8
	movq	%r9, %rbx
	pushq	%r12
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	callq	lzma_upack_esi_54
	addq	$16, %rsp
.Lcfi56:
	.cfi_adjust_cfa_offset -16
	cmpl	$-1, %eax
	je	.LBB1_93
# BB#64:                                #   in Loop: Header=BB1_17 Depth=1
	movl	12(%rsp), %eax
	movl	%eax, 8(%rsp)
	movl	68(%rsp), %r9d
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %rdx
.LBB1_65:                               #   in Loop: Header=BB1_17 Depth=1
	testl	%r12d, %r12d
	sete	%cl
	movl	%r9d, 12(%rsp)
	testl	%r9d, %r9d
	sete	%bl
	cmpq	%rdx, %r13
	movl	$-1, %edi
	jb	.LBB1_94
# BB#66:                                #   in Loop: Header=BB1_17 Depth=1
	cmpl	%r12d, %r9d
	ja	.LBB1_94
# BB#67:                                #   in Loop: Header=BB1_17 Depth=1
	orb	%bl, %cl
	jne	.LBB1_94
# BB#68:                                #   in Loop: Header=BB1_17 Depth=1
	cmpl	%r12d, %r9d
	jae	.LBB1_94
# BB#69:                                #   in Loop: Header=BB1_17 Depth=1
	movl	%r9d, %ebx
	leaq	(%r13,%rbx), %rcx
	cmpq	%r8, %rcx
	ja	.LBB1_94
# BB#70:                                #   in Loop: Header=BB1_17 Depth=1
	testl	%r12d, %r12d
	je	.LBB1_94
# BB#71:                                #   in Loop: Header=BB1_17 Depth=1
	cmpq	%rdx, %rcx
	jbe	.LBB1_94
# BB#72:                                #   in Loop: Header=BB1_17 Depth=1
	movl	%ebp, %ecx
	movq	%r13, %rsi
	subq	%rcx, %rsi
	cmpq	%rdx, %rsi
	jb	.LBB1_94
# BB#73:                                #   in Loop: Header=BB1_17 Depth=1
	leaq	1(%rsi,%rbx), %rsi
	cmpq	%r8, %rsi
	ja	.LBB1_94
# BB#74:                                #   in Loop: Header=BB1_17 Depth=1
	cmpq	%rdx, %rsi
	jbe	.LBB1_94
# BB#75:                                #   in Loop: Header=BB1_17 Depth=1
	negq	%rcx
	movl	%ebp, 88(%rsp)
	testl	%r9d, %r9d
	je	.LBB1_79
# BB#76:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_17 Depth=1
	leaq	(%r13,%rcx), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_77:                               # %.lr.ph
                                        #   Parent Loop BB1_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax,%rdx), %ebx
	movb	%bl, (%r13,%rdx)
	incq	%rdx
	cmpl	12(%rsp), %edx
	jb	.LBB1_77
# BB#78:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB1_17 Depth=1
	movl	8(%rsp), %eax
	addq	%rdx, %r13
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB1_79:                               # %._crit_edge
                                        #   in Loop: Header=BB1_17 Depth=1
	andl	$-256, %eax
	movzbl	(%r13,%rcx), %ecx
	orl	%eax, %ecx
	movl	%ecx, 8(%rsp)
	movl	$128, %r12d
.LBB1_80:                               # %.thread283
                                        #   in Loop: Header=BB1_17 Depth=1
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	movl	%r12d, 12(%rsp)
	movl	$-1, %edi
	je	.LBB1_94
# BB#81:                                # %.thread283
                                        #   in Loop: Header=BB1_17 Depth=1
	cmpq	%rdx, %r13
	jb	.LBB1_94
# BB#82:                                #   in Loop: Header=BB1_17 Depth=1
	leaq	1(%r13), %rax
	cmpq	%r8, %rax
	ja	.LBB1_94
# BB#83:                                #   in Loop: Header=BB1_17 Depth=1
	cmpq	%rdx, %rax
	jbe	.LBB1_94
# BB#84:                                #   in Loop: Header=BB1_17 Depth=1
	movb	8(%rsp), %al
.LBB1_85:                               #   in Loop: Header=BB1_17 Depth=1
	movb	%al, (%r13)
	incq	%r13
	cmpq	224(%rsp), %r13
	movq	72(%rsp), %r12          # 8-byte Reload
	leaq	48(%rsp), %rbp
	jb	.LBB1_17
# BB#86:
	movl	$1, %edi
	jmp	.LBB1_94
.LBB1_93:
	movl	$-1, %edi
.LBB1_94:                               # %.critedge
	movl	%edi, %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	unupack399, .Lfunc_end1-unupack399
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Upack: EP: %08x original:  %08X || %08x\n"
	.size	.L.str, 41

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Upack: context bits out of bounds\n"
	.size	.L.str.1, 35

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Upack: Context Bits parameter used with lzma: %02x, %02x\n"
	.size	.L.str.2, 58

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Upack: data initialized, before upack lzma call!\n"
	.size	.L.str.3, 50

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Upack: EP: %08x original %08x\n"
	.size	.L.str.4, 31

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Upack: something's wrong, report back\n"
	.size	.L.str.5, 39

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Upack: %08x %08x %08x %08x\n"
	.size	.L.str.6, 28

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Upack: EBX: %08x\n"
	.size	.L.str.7, 18

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Upack: DEST: %08x, %08x\n"
	.size	.L.str.8, 25

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Upack: probably hand-crafted data, report back\n"
	.size	.L.str.9, 48

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Upack: ecx counter: %08x\n"
	.size	.L.str.10, 26

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Upack: before_fixing\n"
	.size	.L.str.11, 22

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Upack v 1.1/1.2\n"
	.size	.L.str.12, 17

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Upack: alvalue out of bounds\n"
	.size	.L.str.13, 30

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Upack: loops: %08x search value: %02x\n"
	.size	.L.str.14, 39

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Upack: callfixerr %08x %08x = %08x, %08x\n"
	.size	.L.str.15, 42

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Upack: callfixerr\n"
	.size	.L.str.16, 19

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Upack: Rebuilding failed\n"
	.size	.L.str.17, 26

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\n\tp0: %08x\n\tp1: %08x\n\tp2: %08x\n"
	.size	.L.str.18, 32

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"state[%d] = %08x\n"
	.size	.L.str.19, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
