	.text
	.file	"mbuffer.bc"
	.globl	dump_dpb
	.p2align	4, 0x90
	.type	dump_dpb,@function
dump_dpb:                               # @dump_dpb
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	dump_dpb, .Lfunc_end0-dump_dpb
	.cfi_endproc

	.globl	getDpbSize
	.p2align	4, 0x90
	.type	getDpbSize,@function
getDpbSize:                             # @getDpbSize
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	active_sps(%rip), %rsi
	movl	2068(%rsi), %eax
	movl	2072(%rsi), %ecx
	incl	%ecx
	cmpl	$1, 2076(%rsi)
	movl	$1, %ebx
	adcl	$0, %ebx
	leal	(%rax,%rax,2), %eax
	shll	$7, %eax
	addl	$384, %eax              # imm = 0x180
	imull	%ecx, %ebx
	imull	%eax, %ebx
	movl	24(%rsi), %ecx
	addl	$-9, %ecx
	cmpl	$42, %ecx
	ja	.LBB1_16
# BB#1:
	movl	$152064, %eax           # imm = 0x25200
	jmpq	*.LJTI1_0(,%rcx,8)
.LBB1_5:
	movl	$912384, %eax           # imm = 0xDEC00
	jmp	.LBB1_17
.LBB1_7:
	movl	$3110400, %eax          # imm = 0x2F7600
	jmp	.LBB1_17
.LBB1_10:
	movl	$12582912, %eax         # imm = 0xC00000
	jmp	.LBB1_17
.LBB1_2:
	cmpl	$99, 4(%rsi)
	ja	.LBB1_17
# BB#3:
	cmpl	$0, 20(%rsi)
	jne	.LBB1_17
# BB#4:
	movl	$345600, %eax           # imm = 0x54600
	jmp	.LBB1_17
.LBB1_16:
	movl	$.L.str, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	xorl	%eax, %eax
	movq	active_sps(%rip), %rsi
	jmp	.LBB1_17
.LBB1_6:
	movl	$1824768, %eax          # imm = 0x1BD800
	jmp	.LBB1_17
.LBB1_8:
	movl	$6912000, %eax          # imm = 0x697800
	jmp	.LBB1_17
.LBB1_9:
	movl	$7864320, %eax          # imm = 0x780000
	jmp	.LBB1_17
.LBB1_11:
	movl	4(%rsi), %ecx
	addl	$-100, %ecx
	cmpl	$44, %ecx
	ja	.LBB1_13
# BB#12:
	movl	$13369344, %eax         # imm = 0xCC0000
	movabsq	$17592190239745, %rdx   # imm = 0x100000400401
	btq	%rcx, %rdx
	jb	.LBB1_17
.LBB1_13:
	movl	$12582912, %eax         # imm = 0xC00000
	jmp	.LBB1_17
.LBB1_14:
	movl	$42393600, %eax         # imm = 0x286E000
	jmp	.LBB1_17
.LBB1_15:
	movl	$70778880, %eax         # imm = 0x4380000
.LBB1_17:
	cltd
	idivl	%ebx
	movl	%eax, %ecx
	cmpl	$17, %ecx
	movl	$16, %eax
	cmovll	%ecx, %eax
	cmpl	$0, 2108(%rsi)
	je	.LBB1_22
# BB#18:
	cmpl	$0, 3032(%rsi)
	je	.LBB1_22
# BB#19:
	movl	3060(%rsi), %ecx
	cmpl	%eax, %ecx
	jle	.LBB1_21
# BB#20:
	movl	$.L.str.1, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	active_sps(%rip), %rax
	movl	3060(%rax), %ecx
.LBB1_21:
	testl	%ecx, %ecx
	movl	$1, %eax
	cmovgl	%ecx, %eax
.LBB1_22:
	popq	%rbx
	retq
.Lfunc_end1:
	.size	getDpbSize, .Lfunc_end1-getDpbSize
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_17
	.quad	.LBB1_17
	.quad	.LBB1_2
	.quad	.LBB1_5
	.quad	.LBB1_5
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_5
	.quad	.LBB1_6
	.quad	.LBB1_7
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_7
	.quad	.LBB1_8
	.quad	.LBB1_9
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_10
	.quad	.LBB1_10
	.quad	.LBB1_11
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_14
	.quad	.LBB1_15

	.text
	.globl	check_num_ref
	.p2align	4, 0x90
	.type	check_num_ref,@function
check_num_ref:                          # @check_num_ref
	.cfi_startproc
# BB#0:
	movl	dpb+32(%rip), %eax
	addl	dpb+36(%rip), %eax
	movl	dpb+52(%rip), %ecx
	testl	%ecx, %ecx
	movl	$1, %edx
	cmovgl	%ecx, %edx
	cmpl	%edx, %eax
	jle	.LBB2_1
# BB#2:
	movl	$.L.str.2, %edi
	movl	$500, %esi              # imm = 0x1F4
	jmp	error                   # TAILCALL
.LBB2_1:
	retq
.Lfunc_end2:
	.size	check_num_ref, .Lfunc_end2-check_num_ref
	.cfi_endproc

	.globl	init_dpb
	.p2align	4, 0x90
	.type	init_dpb,@function
init_dpb:                               # @init_dpb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %rbp, -16
	cmpl	$0, dpb+48(%rip)
	je	.LBB3_2
# BB#1:
	callq	free_dpb
.LBB3_2:
	callq	getDpbSize
	movl	%eax, %r14d
	movl	%r14d, dpb+24(%rip)
	movq	active_sps(%rip), %rax
	movl	2060(%rax), %eax
	movl	%eax, dpb+52(%rip)
	cmpl	%eax, %r14d
	jae	.LBB3_4
# BB#3:
	movl	$.L.str.3, %edi
	movl	$1000, %esi             # imm = 0x3E8
	callq	error
	movl	dpb+24(%rip), %r14d
.LBB3_4:
	movl	$0, dpb+28(%rip)
	movq	$0, dpb+56(%rip)
	movq	$0, dpb+32(%rip)
	movl	%r14d, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, dpb(%rip)
	testq	%rax, %rax
	jne	.LBB3_6
# BB#5:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
	movl	dpb+24(%rip), %r14d
.LBB3_6:
	movl	%r14d, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, dpb+8(%rip)
	testq	%rax, %rax
	jne	.LBB3_8
# BB#7:
	movl	$.L.str.5, %edi
	callq	no_mem_exit
	movl	dpb+24(%rip), %r14d
.LBB3_8:
	movl	%r14d, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, dpb+16(%rip)
	testq	%rax, %rax
	jne	.LBB3_10
# BB#9:
	movl	$.L.str.6, %edi
	callq	no_mem_exit
	movl	dpb+24(%rip), %r14d
.LBB3_10:                               # %.preheader21
	testl	%r14d, %r14d
	je	.LBB3_15
# BB#11:                                # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_12:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %edi
	movl	$72, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB3_14
# BB#13:                                #   in Loop: Header=BB3_12 Depth=1
	movl	$.L.str.8, %edi
	callq	no_mem_exit
	movl	dpb+24(%rip), %r14d
.LBB3_14:                               # %alloc_frame_store.exit
                                        #   in Loop: Header=BB3_12 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movups	%xmm0, 48(%rbx)
	movq	$0, 64(%rbx)
	movq	dpb(%rip), %rax
	movl	%ebp, %ecx
	movq	%rbx, (%rax,%rcx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, (%rax,%rcx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, (%rax,%rcx,8)
	incl	%ebp
	cmpl	%r14d, %ebp
	jb	.LBB3_12
.LBB3_15:                               # %.preheader20.preheader
	movl	$33, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, listX(%rip)
	testq	%rax, %rax
	jne	.LBB3_17
# BB#16:
	movl	$.L.str.7, %edi
	callq	no_mem_exit
.LBB3_17:                               # %.preheader20.129
	movl	$33, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, listX+8(%rip)
	testq	%rax, %rax
	jne	.LBB3_19
# BB#18:
	movl	$.L.str.7, %edi
	callq	no_mem_exit
.LBB3_19:                               # %.preheader20.230
	movl	$33, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, listX+16(%rip)
	testq	%rax, %rax
	jne	.LBB3_21
# BB#20:
	movl	$.L.str.7, %edi
	callq	no_mem_exit
.LBB3_21:                               # %.preheader20.331
	movl	$33, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, listX+24(%rip)
	testq	%rax, %rax
	jne	.LBB3_23
# BB#22:
	movl	$.L.str.7, %edi
	callq	no_mem_exit
.LBB3_23:                               # %.preheader20.432
	movl	$33, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, listX+32(%rip)
	testq	%rax, %rax
	jne	.LBB3_25
# BB#24:
	movl	$.L.str.7, %edi
	callq	no_mem_exit
.LBB3_25:                               # %.preheader20.533
	movl	$33, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, listX+40(%rip)
	testq	%rax, %rax
	jne	.LBB3_27
# BB#26:
	movl	$.L.str.7, %edi
	callq	no_mem_exit
.LBB3_27:
	movq	img(%rip), %rax
	movl	48(%rax), %esi
	movl	52(%rax), %edx
	movl	56(%rax), %ecx
	movl	64(%rax), %r8d
	xorl	%edi, %edi
	callq	alloc_storable_picture
	movq	%rax, no_reference_picture(%rip)
	movq	%rax, 317000(%rax)
	movq	%rax, 317008(%rax)
	movq	%rax, 317016(%rax)
	movq	$-24, %rax
	.p2align	4, 0x90
.LBB3_28:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, (%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 8(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 16(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 24(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 32(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 40(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 48(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 56(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 64(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 72(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 80(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 88(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 96(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 104(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 112(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 120(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 128(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 136(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 144(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 152(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 160(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 168(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 176(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 184(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 192(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 200(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 208(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 216(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 224(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 232(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 240(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 248(%rcx)
	movq	listX+48(%rax,%rax), %rcx
	movq	$0, 256(%rcx)
	movl	$0, listXsize+24(%rax)
	addq	$4, %rax
	jne	.LBB3_28
# BB#29:
	movl	$-2147483648, dpb+40(%rip) # imm = 0x80000000
	movq	img(%rip), %rax
	movl	$0, 5860(%rax)
	movl	$1, dpb+48(%rip)
	cmpl	$0, 6068(%rax)
	je	.LBB3_33
# BB#30:
	movl	$1, %edi
	movl	$72, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB3_32
# BB#31:
	movl	$.L.str.8, %edi
	callq	no_mem_exit
.LBB3_32:                               # %alloc_frame_store.exit19
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movups	%xmm0, 48(%rbx)
	movq	$0, 64(%rbx)
	movq	%rbx, last_out_fs(%rip)
.LBB3_33:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	init_dpb, .Lfunc_end3-init_dpb
	.cfi_endproc

	.globl	free_dpb
	.p2align	4, 0x90
	.type	free_dpb,@function
free_dpb:                               # @free_dpb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	dpb(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_13
# BB#1:                                 # %.preheader
	movl	dpb+24(%rip), %eax
	testl	%eax, %eax
	je	.LBB4_12
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ecx
	movq	(%rdi,%rcx,8), %rbx
	testq	%rbx, %rbx
	je	.LBB4_11
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_3 Depth=1
	callq	free_storable_picture
	movq	$0, 48(%rbx)
.LBB4_6:                                #   in Loop: Header=BB4_3 Depth=1
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_8
# BB#7:                                 #   in Loop: Header=BB4_3 Depth=1
	callq	free_storable_picture
	movq	$0, 56(%rbx)
.LBB4_8:                                #   in Loop: Header=BB4_3 Depth=1
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_10
# BB#9:                                 #   in Loop: Header=BB4_3 Depth=1
	callq	free_storable_picture
.LBB4_10:                               #   in Loop: Header=BB4_3 Depth=1
	movq	%rbx, %rdi
	callq	free
	movl	dpb+24(%rip), %eax
	movq	dpb(%rip), %rdi
.LBB4_11:                               # %free_frame_store.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jb	.LBB4_3
.LBB4_12:                               # %._crit_edge
	callq	free
	movq	$0, dpb(%rip)
.LBB4_13:
	movq	dpb+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_15
# BB#14:
	callq	free
.LBB4_15:
	movq	dpb+16(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_17
# BB#16:
	callq	free
.LBB4_17:
	movl	$-2147483648, dpb+40(%rip) # imm = 0x80000000
	movq	listX(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_19
# BB#18:
	callq	free
	movq	$0, listX(%rip)
.LBB4_19:
	movq	listX+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_21
# BB#20:
	callq	free
	movq	$0, listX+8(%rip)
.LBB4_21:
	movq	listX+16(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_23
# BB#22:
	callq	free
	movq	$0, listX+16(%rip)
.LBB4_23:
	movq	listX+24(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_25
# BB#24:
	callq	free
	movq	$0, listX+24(%rip)
.LBB4_25:
	movq	listX+32(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_27
# BB#26:
	callq	free
	movq	$0, listX+32(%rip)
.LBB4_27:
	movq	listX+40(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_29
# BB#28:
	callq	free
	movq	$0, listX+40(%rip)
.LBB4_29:
	movl	$0, dpb+48(%rip)
	movq	img(%rip), %rax
	cmpl	$0, 6068(%rax)
	je	.LBB4_38
# BB#30:
	movq	last_out_fs(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB4_38
# BB#31:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_33
# BB#32:
	callq	free_storable_picture
	movq	$0, 48(%rbx)
.LBB4_33:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_35
# BB#34:
	callq	free_storable_picture
	movq	$0, 56(%rbx)
.LBB4_35:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_37
# BB#36:
	callq	free_storable_picture
.LBB4_37:
	movq	%rbx, %rdi
	callq	free
.LBB4_38:                               # %free_frame_store.exit9
	movq	no_reference_picture(%rip), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	free_storable_picture   # TAILCALL
.Lfunc_end4:
	.size	free_dpb, .Lfunc_end4-free_dpb
	.cfi_endproc

	.globl	alloc_frame_store
	.p2align	4, 0x90
	.type	alloc_frame_store,@function
alloc_frame_store:                      # @alloc_frame_store
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	movl	$72, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB5_2
# BB#1:
	movl	$.L.str.8, %edi
	callq	no_mem_exit
.LBB5_2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movups	%xmm0, 48(%rbx)
	movq	$0, 64(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	alloc_frame_store, .Lfunc_end5-alloc_frame_store
	.cfi_endproc

	.globl	alloc_storable_picture
	.p2align	4, 0x90
	.type	alloc_storable_picture,@function
alloc_storable_picture:                 # @alloc_storable_picture
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 64
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	movl	%ecx, %r14d
	movl	%edx, %ebp
	movl	%esi, %r13d
	movl	%edi, %r12d
	movl	$1, %edi
	movl	$317104, %esi           # imm = 0x4D6B0
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB6_2
# BB#1:
	movl	$.L.str.9, %edi
	callq	no_mem_exit
.LBB6_2:
	testl	%r12d, %r12d
	je	.LBB6_4
# BB#3:
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	sarl	%eax
	movl	%r15d, %ecx
	shrl	$31, %ecx
	addl	%r15d, %ecx
	sarl	%ecx
	movl	%ecx, %r15d
	movl	%eax, %ebp
.LBB6_4:
	movl	%ebp, %eax
	imull	%r13d, %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$24, %ecx
	addl	%eax, %ecx
	sarl	$8, %ecx
	movl	%ecx, 316912(%rbx)
	movq	$0, 316928(%rbx)
	leaq	316920(%rbx), %rdi
	movl	%ebp, %esi
	movl	%r13d, %edx
	callq	get_mem2Dpel
	movq	active_sps(%rip), %rax
	cmpl	$0, 32(%rax)
	je	.LBB6_6
# BB#5:
	leaq	316928(%rbx), %rdi
	movl	$2, %esi
	movl	%r15d, %edx
	movl	%r14d, %ecx
	callq	get_mem3Dpel
.LBB6_6:
	movl	%r12d, (%rsp)           # 4-byte Spill
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	movl	316912(%rbx), %edi
	movl	$4, %esi
	callq	calloc
	movq	%rax, 316936(%rbx)
	testq	%rax, %rax
	jne	.LBB6_8
# BB#7:
	movl	$.L.str.10, %edi
	callq	no_mem_exit
.LBB6_8:
	leaq	316944(%rbx), %rdi
	movl	%ebp, %r12d
	sarl	$31, %r12d
	movl	%r12d, %esi
	shrl	$28, %esi
	addl	%ebp, %esi
	sarl	$4, %esi
	movl	%r13d, %r14d
	sarl	$31, %r14d
	movl	%r14d, %edx
	shrl	$28, %edx
	addl	%r13d, %edx
	sarl	$4, %edx
	callq	get_mem2Dshort
	leaq	316952(%rbx), %rdi
	shrl	$30, %r12d
	addl	%ebp, %r12d
	sarl	$2, %r12d
	shrl	$30, %r14d
	addl	%r13d, %r14d
	sarl	$2, %r14d
	movl	$2, %esi
	movl	%r12d, %edx
	movl	%r14d, %ecx
	callq	get_mem3D
	leaq	316960(%rbx), %rdi
	movl	$6, %esi
	movl	%r12d, %edx
	movl	%r14d, %ecx
	callq	get_mem3Dint64
	leaq	316968(%rbx), %rdi
	movl	$6, %esi
	movl	%r12d, %edx
	movl	%r14d, %ecx
	callq	get_mem3Dint64
	leaq	316976(%rbx), %rdi
	movl	$2, %esi
	movl	$2, %r8d
	movl	%r12d, %edx
	movl	%r14d, %ecx
	callq	get_mem4Dshort
	leaq	316984(%rbx), %rdi
	movl	%r12d, %esi
	movl	%r14d, %edx
	callq	get_mem2D
	movq	%rbx, %rdi
	addq	$316992, %rdi           # imm = 0x4D640
	movl	%r12d, %esi
	movl	%r14d, %edx
	callq	get_mem2D
	movl	$0, 316824(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 316846(%rbx)
	movups	%xmm0, 316832(%rbx)
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, (%rbx)
	movl	%r13d, 316864(%rbx)
	movl	%ebp, 316868(%rbx)
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 316872(%rbx)
	movl	%r15d, 316876(%rbx)
	movd	%r15d, %xmm0
	movd	%ebp, %xmm1
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movd	%eax, %xmm0
	movd	%r13d, %xmm2
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	pcmpeqd	%xmm0, %xmm0
	paddd	%xmm2, %xmm0
	movdqu	%xmm0, 316880(%rbx)
	movq	no_reference_picture(%rip), %rax
	movq	%rax, 317000(%rbx)
	movq	%rax, 317008(%rbx)
	movq	%rax, 317016(%rbx)
	movq	$0, 317088(%rbx)
	movl	$0, 316900(%rbx)
	movl	$0, 316904(%rbx)
	movl	$0, 12(%rbx)
	movq	$0, 4(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	alloc_storable_picture, .Lfunc_end6-alloc_storable_picture
	.cfi_endproc

	.globl	free_frame_store
	.p2align	4, 0x90
	.type	free_frame_store,@function
free_frame_store:                       # @free_frame_store
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB7_8
# BB#1:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_3
# BB#2:
	callq	free_storable_picture
	movq	$0, 48(%rbx)
.LBB7_3:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_5
# BB#4:
	callq	free_storable_picture
	movq	$0, 56(%rbx)
.LBB7_5:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_7
# BB#6:
	callq	free_storable_picture
.LBB7_7:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB7_8:
	popq	%rbx
	retq
.Lfunc_end7:
	.size	free_frame_store, .Lfunc_end7-free_frame_store
	.cfi_endproc

	.globl	free_storable_picture
	.p2align	4, 0x90
	.type	free_storable_picture,@function
free_storable_picture:                  # @free_storable_picture
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 16
.Lcfi31:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB8_22
# BB#1:
	movq	316952(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_3
# BB#2:
	movl	$2, %esi
	callq	free_mem3D
	movq	$0, 316952(%rbx)
.LBB8_3:
	movq	316960(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_5
# BB#4:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	$0, 316960(%rbx)
.LBB8_5:
	movq	316968(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_7
# BB#6:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	$0, 316968(%rbx)
.LBB8_7:
	movq	316976(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_9
# BB#8:
	movl	316868(%rbx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	addl	%eax, %edx
	sarl	$2, %edx
	movl	$2, %esi
	callq	free_mem4Dshort
	movq	$0, 316976(%rbx)
.LBB8_9:
	movq	316984(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_11
# BB#10:
	callq	free_mem2D
	movq	$0, 316984(%rbx)
.LBB8_11:
	movq	316992(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_13
# BB#12:
	callq	free_mem2D
	movq	$0, 316992(%rbx)
.LBB8_13:
	movq	316920(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_15
# BB#14:
	callq	free_mem2Dpel
	movq	$0, 316920(%rbx)
.LBB8_15:
	movq	316928(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_17
# BB#16:
	movl	$2, %esi
	callq	free_mem3Dpel
	movq	$0, 316928(%rbx)
.LBB8_17:
	movq	316936(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_19
# BB#18:
	callq	free
	movq	$0, 316936(%rbx)
.LBB8_19:
	movq	316944(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_21
# BB#20:
	callq	free_mem2Dshort
.LBB8_21:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB8_22:
	popq	%rbx
	retq
.Lfunc_end8:
	.size	free_storable_picture, .Lfunc_end8-free_storable_picture
	.cfi_endproc

	.globl	is_short_ref
	.p2align	4, 0x90
	.type	is_short_ref,@function
is_short_ref:                           # @is_short_ref
	.cfi_startproc
# BB#0:
	cmpl	$0, 316848(%rdi)
	je	.LBB9_1
# BB#2:
	cmpl	$0, 316844(%rdi)
	sete	%al
	movzbl	%al, %eax
	retq
.LBB9_1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end9:
	.size	is_short_ref, .Lfunc_end9-is_short_ref
	.cfi_endproc

	.globl	is_long_ref
	.p2align	4, 0x90
	.type	is_long_ref,@function
is_long_ref:                            # @is_long_ref
	.cfi_startproc
# BB#0:
	cmpl	$0, 316848(%rdi)
	je	.LBB10_1
# BB#2:
	cmpl	$0, 316844(%rdi)
	setne	%al
	movzbl	%al, %eax
	retq
.LBB10_1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end10:
	.size	is_long_ref, .Lfunc_end10-is_long_ref
	.cfi_endproc

	.globl	init_lists
	.p2align	4, 0x90
	.type	init_lists,@function
init_lists:                             # @init_lists
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 80
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movl	%esi, %r12d
	movq	active_sps(%rip), %rax
	movl	1008(%rax), %ecx
	addl	$4, %ecx
	movl	$1, %r9d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r9d
	testl	%r12d, %r12d
	je	.LBB11_1
# BB#23:
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	cmpl	$1, %r12d
	sete	%r10b
	setne	%r11b
	movl	dpb+32(%rip), %r8d
	testq	%r8, %r8
	je	.LBB11_31
# BB#24:                                # %.lr.ph380
	movq	dpb+8(%rip), %r15
	movq	img(%rip), %r14
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_25:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rax,8), %rdx
	movl	4(%rdx), %esi
	testl	%esi, %esi
	je	.LBB11_30
# BB#26:                                #   in Loop: Header=BB11_25 Depth=1
	movl	20(%rdx), %ecx
	cmpl	5676(%r14), %ecx
	movl	$0, %ebx
	cmoval	%r9d, %ebx
	subl	%ebx, %ecx
	movl	%ecx, 28(%rdx)
	testb	$1, %sil
	je	.LBB11_28
# BB#27:                                #   in Loop: Header=BB11_25 Depth=1
	leal	(%r10,%rcx,2), %ebx
	movq	56(%rdx), %rbp
	movl	%ebx, 316832(%rbp)
.LBB11_28:                              #   in Loop: Header=BB11_25 Depth=1
	testb	$2, %sil
	je	.LBB11_30
# BB#29:                                #   in Loop: Header=BB11_25 Depth=1
	leal	(%r11,%rcx,2), %ecx
	movq	64(%rdx), %rdx
	movl	%ecx, 316832(%rdx)
.LBB11_30:                              #   in Loop: Header=BB11_25 Depth=1
	incq	%rax
	cmpq	%r8, %rax
	jb	.LBB11_25
.LBB11_31:                              # %.preheader294
	movl	dpb+36(%rip), %r9d
	testq	%r9, %r9
	je	.LBB11_38
# BB#32:                                # %.lr.ph377
	movq	dpb+16(%rip), %rdx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_33:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rbp,8), %rcx
	movl	8(%rcx), %esi
	testb	$1, %sil
	je	.LBB11_35
# BB#34:                                #   in Loop: Header=BB11_33 Depth=1
	movq	56(%rcx), %rbx
	movl	316840(%rbx), %eax
	leal	(%r10,%rax,2), %eax
	movl	%eax, 316836(%rbx)
.LBB11_35:                              #   in Loop: Header=BB11_33 Depth=1
	testb	$2, %sil
	je	.LBB11_37
# BB#36:                                #   in Loop: Header=BB11_33 Depth=1
	movq	64(%rcx), %rax
	movl	316840(%rax), %ecx
	leal	(%r11,%rcx,2), %ecx
	movl	%ecx, 316836(%rax)
.LBB11_37:                              #   in Loop: Header=BB11_33 Depth=1
	incq	%rbp
	cmpq	%r9, %rbp
	jb	.LBB11_33
	jmp	.LBB11_38
.LBB11_1:                               # %.preheader293
	movl	dpb+32(%rip), %r8d
	testq	%r8, %r8
	je	.LBB11_8
# BB#2:                                 # %.lr.ph375
	movq	dpb+8(%rip), %rax
	movq	img(%rip), %r10
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB11_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rsi,8), %rdx
	cmpl	$3, (%rdx)
	jne	.LBB11_7
# BB#4:                                 #   in Loop: Header=BB11_3 Depth=1
	movq	48(%rdx), %rbp
	cmpl	$0, 316848(%rbp)
	je	.LBB11_7
# BB#5:                                 #   in Loop: Header=BB11_3 Depth=1
	cmpl	$0, 316844(%rbp)
	jne	.LBB11_7
# BB#6:                                 #   in Loop: Header=BB11_3 Depth=1
	movl	20(%rdx), %ebx
	cmpl	5676(%r10), %ebx
	movl	$0, %ecx
	cmoval	%r9d, %ecx
	subl	%ecx, %ebx
	movl	%ebx, 28(%rdx)
	movl	%ebx, 316832(%rbp)
	.p2align	4, 0x90
.LBB11_7:                               #   in Loop: Header=BB11_3 Depth=1
	incq	%rsi
	cmpq	%r8, %rsi
	jb	.LBB11_3
.LBB11_8:                               # %.preheader291
	movl	dpb+36(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB11_38
# BB#9:                                 # %.lr.ph373
	movq	dpb+16(%rip), %rdx
	testb	$1, %cl
	jne	.LBB11_11
# BB#10:
	xorl	%esi, %esi
	cmpl	$1, %ecx
	jne	.LBB11_16
	jmp	.LBB11_38
.LBB11_11:
	movq	(%rdx), %rax
	cmpl	$3, (%rax)
	jne	.LBB11_14
# BB#12:
	movq	48(%rax), %rax
	cmpl	$0, 316844(%rax)
	je	.LBB11_14
# BB#13:
	movl	316840(%rax), %esi
	movl	%esi, 316836(%rax)
.LBB11_14:                              # %.prol.loopexit514
	movl	$1, %esi
	cmpl	$1, %ecx
	je	.LBB11_38
	.p2align	4, 0x90
.LBB11_16:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rax
	cmpl	$3, (%rax)
	jne	.LBB11_19
# BB#17:                                #   in Loop: Header=BB11_16 Depth=1
	movq	48(%rax), %rax
	cmpl	$0, 316844(%rax)
	je	.LBB11_19
# BB#18:                                #   in Loop: Header=BB11_16 Depth=1
	movl	316840(%rax), %ebp
	movl	%ebp, 316836(%rax)
.LBB11_19:                              #   in Loop: Header=BB11_16 Depth=1
	movq	8(%rdx,%rsi,8), %rax
	cmpl	$3, (%rax)
	jne	.LBB11_22
# BB#20:                                #   in Loop: Header=BB11_16 Depth=1
	movq	48(%rax), %rax
	cmpl	$0, 316844(%rax)
	je	.LBB11_22
# BB#21:                                #   in Loop: Header=BB11_16 Depth=1
	movl	316840(%rax), %ebp
	movl	%ebp, 316836(%rax)
.LBB11_22:                              #   in Loop: Header=BB11_16 Depth=1
	addq	$2, %rsi
	cmpq	%rcx, %rsi
	jb	.LBB11_16
.LBB11_38:                              # %.loopexit292
	movabsq	$17179869184, %r13      # imm = 0x400000000
	cmpl	$4, %edi
	ja	.LBB11_87
# BB#39:                                # %.loopexit292
	movl	%edi, %eax
	jmpq	*.LJTI11_0(,%rax,8)
.LBB11_41:
	testl	%r12d, %r12d
	je	.LBB11_42
# BB#57:
	movl	dpb+24(%rip), %ebp
	movl	$8, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB11_59
# BB#58:
	movl	$.L.str.11, %edi
	callq	no_mem_exit
	movl	dpb+24(%rip), %ebp
.LBB11_59:
	movl	%ebp, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB11_61
# BB#60:
	movl	$.L.str.12, %edi
	callq	no_mem_exit
.LBB11_61:                              # %.preheader290
	movl	dpb+32(%rip), %eax
	xorl	%ebx, %ebx
	testq	%rax, %rax
	movl	$0, %edx
	je	.LBB11_74
# BB#62:                                # %.lr.ph369
	movq	dpb+8(%rip), %rcx
	testb	$1, %al
	jne	.LBB11_64
# BB#63:
	xorl	%esi, %esi
	xorl	%edx, %edx
	cmpl	$1, %eax
	jne	.LBB11_69
	jmp	.LBB11_74
.LBB11_40:
	movq	$0, listXsize(%rip)
	jmp	.LBB11_187
.LBB11_87:
	testl	%r12d, %r12d
	je	.LBB11_88
# BB#128:
	movl	dpb+24(%rip), %ebx
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB11_130
# BB#129:
	movl	$.L.str.11, %edi
	callq	no_mem_exit
	movl	dpb+24(%rip), %ebx
.LBB11_130:
	movl	%ebx, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB11_132
# BB#131:
	movl	$.L.str.13, %edi
	callq	no_mem_exit
	movl	dpb+24(%rip), %ebx
.LBB11_132:
	movl	%ebx, %edi
	movl	$8, %esi
	callq	calloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB11_134
# BB#133:
	movl	$.L.str.12, %edi
	callq	no_mem_exit
.LBB11_134:
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	movabsq	$4294967296, %rax       # imm = 0x100000000
	movq	%rax, listXsize(%rip)
	movl	dpb+32(%rip), %eax
	testq	%rax, %rax
	je	.LBB11_135
# BB#136:                               # %.lr.ph347
	xorl	%ecx, %ecx
	movq	dpb+8(%rip), %rdx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB11_137:                             # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rcx,8), %rsi
	cmpl	$0, (%rsi)
	je	.LBB11_140
# BB#138:                               #   in Loop: Header=BB11_137 Depth=1
	movq	img(%rip), %rdi
	movl	5752(%rdi), %edi
	cmpl	40(%rsi), %edi
	jl	.LBB11_140
# BB#139:                               #   in Loop: Header=BB11_137 Depth=1
	movslq	%r12d, %rdi
	incl	%r12d
	movq	%rsi, (%r13,%rdi,8)
.LBB11_140:                             #   in Loop: Header=BB11_137 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB11_137
	jmp	.LBB11_141
.LBB11_42:                              # %.preheader289
	testl	%r8d, %r8d
	je	.LBB11_43
# BB#44:                                # %.lr.ph358
	movl	%r8d, %eax
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_45:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	cmpl	$3, (%rdx)
	jne	.LBB11_49
# BB#46:                                #   in Loop: Header=BB11_45 Depth=1
	movq	48(%rdx), %rdx
	cmpl	$0, 316848(%rdx)
	je	.LBB11_49
# BB#47:                                #   in Loop: Header=BB11_45 Depth=1
	cmpl	$0, 316844(%rdx)
	jne	.LBB11_49
# BB#48:                                #   in Loop: Header=BB11_45 Depth=1
	movq	listX(%rip), %rsi
	movslq	%ebp, %rdi
	incl	%ebp
	movq	%rdx, (%rsi,%rdi,8)
	.p2align	4, 0x90
.LBB11_49:                              #   in Loop: Header=BB11_45 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB11_45
	jmp	.LBB11_50
.LBB11_64:
	movq	(%rcx), %rdx
	cmpl	$0, 4(%rdx)
	je	.LBB11_65
# BB#66:
	movq	%rdx, (%r15)
	movl	$1, %edx
	jmp	.LBB11_67
.LBB11_88:                              # %.preheader287
	testl	%r8d, %r8d
	je	.LBB11_89
# BB#90:                                # %.lr.ph323
	movl	%r8d, %eax
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB11_91:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	cmpl	$3, (%rdx)
	jne	.LBB11_96
# BB#92:                                #   in Loop: Header=BB11_91 Depth=1
	movq	48(%rdx), %rdx
	cmpl	$0, 316848(%rdx)
	je	.LBB11_96
# BB#93:                                #   in Loop: Header=BB11_91 Depth=1
	cmpl	$0, 316844(%rdx)
	jne	.LBB11_96
# BB#94:                                #   in Loop: Header=BB11_91 Depth=1
	movq	img(%rip), %rsi
	movl	5672(%rsi), %esi
	cmpl	4(%rdx), %esi
	jl	.LBB11_96
# BB#95:                                #   in Loop: Header=BB11_91 Depth=1
	movq	listX(%rip), %rsi
	movslq	%r15d, %rdi
	incl	%r15d
	movq	%rdx, (%rsi,%rdi,8)
	.p2align	4, 0x90
.LBB11_96:                              #   in Loop: Header=BB11_91 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB11_91
	jmp	.LBB11_97
.LBB11_43:
	xorl	%ebp, %ebp
.LBB11_50:                              # %._crit_edge359
	movq	listX(%rip), %rdi
	movslq	%ebp, %rbx
	movl	$8, %edx
	movl	$compare_pic_by_pic_num_desc, %ecx
	movq	%rbx, %rsi
	callq	qsort
	movl	%ebx, listXsize(%rip)
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	je	.LBB11_56
# BB#51:                                # %.lr.ph353
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_52:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+16(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	cmpl	$3, (%rdx)
	jne	.LBB11_55
# BB#53:                                #   in Loop: Header=BB11_52 Depth=1
	movq	48(%rdx), %rdx
	cmpl	$0, 316844(%rdx)
	je	.LBB11_55
# BB#54:                                #   in Loop: Header=BB11_52 Depth=1
	movq	listX(%rip), %rsi
	movslq	%ebp, %rdi
	incl	%ebp
	movq	%rdx, (%rsi,%rdi,8)
.LBB11_55:                              #   in Loop: Header=BB11_52 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB11_52
.LBB11_56:                              # %._crit_edge354
	movq	listX(%rip), %rax
	leaq	(%rax,%rbx,8), %rdi
	movl	%ebp, %eax
	subl	%ebx, %eax
	movslq	%eax, %rsi
	movl	$8, %edx
	movl	$compare_pic_by_lt_pic_num_asc, %ecx
	callq	qsort
	movl	%ebp, listXsize(%rip)
	jmp	.LBB11_86
.LBB11_135:
	xorl	%r12d, %r12d
.LBB11_141:                             # %._crit_edge348
	movslq	%r12d, %rbx
	movl	$8, %edx
	movl	$compare_fs_by_poc_desc, %ecx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	qsort
	movl	dpb+32(%rip), %eax
	testq	%rax, %rax
	movl	%r12d, %ebp
	je	.LBB11_147
# BB#142:                               # %.lr.ph341
	movq	dpb+8(%rip), %rcx
	xorl	%edx, %edx
	movl	%r12d, %ebp
	.p2align	4, 0x90
.LBB11_143:                             # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$0, (%rsi)
	je	.LBB11_146
# BB#144:                               #   in Loop: Header=BB11_143 Depth=1
	movq	img(%rip), %rdi
	movl	5752(%rdi), %edi
	cmpl	40(%rsi), %edi
	jge	.LBB11_146
# BB#145:                               #   in Loop: Header=BB11_143 Depth=1
	movslq	%ebp, %rdi
	incl	%ebp
	movq	%rsi, (%r13,%rdi,8)
.LBB11_146:                             #   in Loop: Header=BB11_143 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jb	.LBB11_143
.LBB11_147:                             # %._crit_edge342
	leaq	(%r13,%rbx,8), %rdi
	movl	%ebp, %eax
	subl	%ebx, %eax
	movslq	%eax, %r15
	movl	$8, %edx
	movl	$compare_fs_by_poc_asc, %ecx
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%r15, %rsi
	callq	qsort
	testl	%ebx, %ebx
	jle	.LBB11_149
# BB#148:                               # %.lr.ph337.preheader
	leaq	(%r14,%r15,8), %rdi
	leal	-1(%r12), %eax
	leaq	8(,%rax,8), %rdx
	movq	%r13, %rsi
	callq	memcpy
.LBB11_149:                             # %.preheader288
	cmpl	%r12d, %ebp
	movq	%r14, %r15
	jle	.LBB11_151
# BB#150:                               # %.lr.ph333.preheader
	leal	-1(%rbp), %eax
	subl	%r12d, %eax
	leaq	8(,%rax,8), %rdx
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	memcpy
.LBB11_151:                             # %._crit_edge334
	movq	$0, listXsize(%rip)
	movq	listX(%rip), %rcx
	xorl	%ebx, %ebx
	movl	$listXsize, %r8d
	xorl	%r9d, %r9d
	movl	4(%rsp), %r14d          # 4-byte Reload
	movl	%r14d, %edi
	movq	%r13, %rsi
	movl	%ebp, %edx
	callq	gen_pic_list_from_frame_list
	movq	listX+8(%rip), %rcx
	movl	$listXsize+4, %r8d
	xorl	%r9d, %r9d
	movl	%r14d, %edi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	gen_pic_list_from_frame_list
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	movq	16(%rsp), %r12          # 8-byte Reload
	je	.LBB11_162
# BB#152:                               # %.lr.ph329
	movq	dpb+16(%rip), %rcx
	cmpl	$4, %eax
	jb	.LBB11_153
# BB#154:                               # %min.iters.checked459
	movl	%eax, %r8d
	andl	$3, %r8d
	movq	%rax, %rbx
	subq	%r8, %rbx
	je	.LBB11_153
# BB#155:                               # %vector.memcheck472
	leaq	(%rcx,%rax,8), %rsi
	cmpq	%rsi, %r12
	jae	.LBB11_157
# BB#156:                               # %vector.memcheck472
	leaq	(%r12,%rax,8), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB11_157
.LBB11_153:
	xorl	%ebx, %ebx
	xorl	%edi, %edi
.LBB11_160:                             # %scalar.ph457.preheader
	movslq	%edi, %rdx
	leaq	(%r12,%rdx,8), %rdx
	movq	%rbx, %rsi
	movl	%edi, %ebx
	.p2align	4, 0x90
.LBB11_161:                             # %scalar.ph457
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rdi
	incl	%ebx
	movq	%rdi, (%rdx)
	incq	%rsi
	addq	$8, %rdx
	cmpq	%rax, %rsi
	jb	.LBB11_161
.LBB11_162:                             # %._crit_edge330
	movslq	%ebx, %rbx
	movl	$8, %edx
	movl	$compare_fs_by_lt_pic_idx_asc, %ecx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	qsort
	movq	listX(%rip), %rcx
	movl	$listXsize, %r8d
	movl	$1, %r9d
	movl	%r14d, %edi
	movq	%r12, %rsi
	movl	%ebx, %edx
	callq	gen_pic_list_from_frame_list
	movq	listX+8(%rip), %rcx
	movl	$listXsize+4, %r8d
	movl	$1, %r9d
	movl	%r14d, %edi
	movq	%r12, %rsi
	movl	%ebx, %edx
	callq	gen_pic_list_from_frame_list
	movq	%r13, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movl	listXsize+4(%rip), %r15d
	jmp	.LBB11_163
.LBB11_65:
	xorl	%edx, %edx
.LBB11_67:                              # %.prol.loopexit510
	movl	$1, %esi
	cmpl	$1, %eax
	je	.LBB11_74
	.p2align	4, 0x90
.LBB11_69:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rdi
	cmpl	$0, 4(%rdi)
	je	.LBB11_71
# BB#70:                                #   in Loop: Header=BB11_69 Depth=1
	movslq	%edx, %rbp
	incl	%edx
	movq	%rdi, (%r15,%rbp,8)
.LBB11_71:                              #   in Loop: Header=BB11_69 Depth=1
	movq	8(%rcx,%rsi,8), %rdi
	cmpl	$0, 4(%rdi)
	je	.LBB11_73
# BB#72:                                #   in Loop: Header=BB11_69 Depth=1
	movslq	%edx, %rbp
	incl	%edx
	movq	%rdi, (%r15,%rbp,8)
.LBB11_73:                              #   in Loop: Header=BB11_69 Depth=1
	addq	$2, %rsi
	cmpq	%rax, %rsi
	jb	.LBB11_69
.LBB11_74:                              # %._crit_edge370
	movslq	%edx, %rbp
	movl	$8, %edx
	movl	$compare_fs_by_frame_num_desc, %ecx
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	qsort
	movl	$0, listXsize(%rip)
	movq	listX(%rip), %rcx
	movl	$listXsize, %r8d
	xorl	%r9d, %r9d
	movl	%r12d, %edi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	gen_pic_list_from_frame_list
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	je	.LBB11_85
# BB#75:                                # %.lr.ph364
	movq	dpb+16(%rip), %rcx
	cmpl	$4, %eax
	jb	.LBB11_76
# BB#77:                                # %min.iters.checked
	movl	%eax, %r8d
	andl	$3, %r8d
	movq	%rax, %rbx
	subq	%r8, %rbx
	je	.LBB11_76
# BB#78:                                # %vector.memcheck
	leaq	(%rcx,%rax,8), %rsi
	cmpq	%rsi, %r14
	jae	.LBB11_80
# BB#79:                                # %vector.memcheck
	leaq	(%r14,%rax,8), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB11_80
.LBB11_76:
	xorl	%ebx, %ebx
	xorl	%edi, %edi
.LBB11_83:                              # %scalar.ph.preheader
	movslq	%edi, %rdx
	leaq	(%r14,%rdx,8), %rdx
	movq	%rbx, %rsi
	movl	%edi, %ebx
	.p2align	4, 0x90
.LBB11_84:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rdi
	incl	%ebx
	movq	%rdi, (%rdx)
	incq	%rsi
	addq	$8, %rdx
	cmpq	%rax, %rsi
	jb	.LBB11_84
.LBB11_85:                              # %._crit_edge365
	movslq	%ebx, %rbp
	movl	$8, %edx
	movl	$compare_fs_by_lt_pic_idx_asc, %ecx
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	qsort
	movq	listX(%rip), %rcx
	movl	$listXsize, %r8d
	movl	$1, %r9d
	movl	%r12d, %edi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	gen_pic_list_from_frame_list
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
.LBB11_86:
	movl	$0, listXsize+4(%rip)
	xorl	%r15d, %r15d
.LBB11_163:
	movl	listXsize(%rip), %r9d
	cmpl	$2, %r9d
	jl	.LBB11_175
# BB#164:
	cmpl	%r15d, %r9d
	jne	.LBB11_175
# BB#165:                               # %.preheader
	testl	%r15d, %r15d
	jle	.LBB11_174
# BB#166:                               # %.lr.ph301
	movq	listX(%rip), %rcx
	movq	listX+8(%rip), %rdx
	movslq	%r15d, %r10
	leaq	-1(%r10), %r8
	movq	%r10, %rsi
	xorl	%edi, %edi
	andq	$3, %rsi
	je	.LBB11_167
# BB#168:                               # %.prol.preheader
	movl	$1, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_169:                             # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rax
	cmpq	(%rdx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	incq	%rdi
	cmpq	%rdi, %rsi
	jne	.LBB11_169
	jmp	.LBB11_170
.LBB11_167:
	xorl	%ebp, %ebp
.LBB11_170:                             # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB11_173
# BB#171:                               # %.lr.ph301.new
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB11_172:                             # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rax
	cmpq	(%rdx,%rdi,8), %rax
	movq	8(%rcx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	cmpq	8(%rdx,%rdi,8), %rax
	movq	16(%rcx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	cmpq	16(%rdx,%rdi,8), %rax
	movq	24(%rcx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	cmpq	24(%rdx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	addq	$4, %rdi
	cmpq	%r10, %rdi
	jl	.LBB11_172
.LBB11_173:                             # %._crit_edge302
	testl	%ebp, %ebp
	jne	.LBB11_175
.LBB11_174:                             # %._crit_edge302.thread
	movq	listX+8(%rip), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	%rdx, (%rax)
	movq	listX+8(%rip), %rax
	movq	%rcx, 8(%rax)
.LBB11_175:
	movq	img(%rip), %rax
	movl	5640(%rax), %edx
	cmpl	%edx, %r9d
	cmovlel	%r9d, %edx
	movl	%edx, listXsize(%rip)
	movl	5644(%rax), %eax
	cmpl	%eax, %r15d
	cmovlel	%r15d, %eax
	movl	%eax, listXsize+4(%rip)
	cmpl	$32, %edx
	ja	.LBB11_181
# BB#176:                               # %.lr.ph298.preheader
	movl	%edx, %ecx
	movl	$33, %esi
	subl	%edx, %esi
	movl	$32, %edx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB11_179
# BB#177:                               # %.lr.ph298.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB11_178:                             # %.lr.ph298.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	no_reference_picture(%rip), %rdi
	movq	listX(%rip), %rbp
	movq	%rdi, (%rbp,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB11_178
.LBB11_179:                             # %.lr.ph298.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB11_181
	.p2align	4, 0x90
.LBB11_180:                             # %.lr.ph298
                                        # =>This Inner Loop Header: Depth=1
	movq	no_reference_picture(%rip), %rdx
	movq	listX(%rip), %rsi
	movq	%rdx, (%rsi,%rcx,8)
	movq	no_reference_picture(%rip), %rdx
	movq	listX(%rip), %rsi
	movq	%rdx, 8(%rsi,%rcx,8)
	movq	no_reference_picture(%rip), %rdx
	movq	listX(%rip), %rsi
	movq	%rdx, 16(%rsi,%rcx,8)
	movq	no_reference_picture(%rip), %rdx
	movq	listX(%rip), %rsi
	movq	%rdx, 24(%rsi,%rcx,8)
	addq	$4, %rcx
	cmpq	$33, %rcx
	jne	.LBB11_180
.LBB11_181:                             # %._crit_edge
	cmpl	$32, %eax
	ja	.LBB11_187
# BB#182:                               # %.lr.ph.preheader
	movl	%eax, %ecx
	movl	$33, %edx
	subl	%eax, %edx
	movl	$32, %eax
	subq	%rcx, %rax
	andq	$3, %rdx
	je	.LBB11_185
# BB#183:                               # %.lr.ph.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB11_184:                             # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	no_reference_picture(%rip), %rsi
	movq	listX+8(%rip), %rdi
	movq	%rsi, (%rdi,%rcx,8)
	incq	%rcx
	incq	%rdx
	jne	.LBB11_184
.LBB11_185:                             # %.lr.ph.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB11_187
	.p2align	4, 0x90
.LBB11_186:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	no_reference_picture(%rip), %rax
	movq	listX+8(%rip), %rdx
	movq	%rax, (%rdx,%rcx,8)
	movq	no_reference_picture(%rip), %rax
	movq	listX+8(%rip), %rdx
	movq	%rax, 8(%rdx,%rcx,8)
	movq	no_reference_picture(%rip), %rax
	movq	listX+8(%rip), %rdx
	movq	%rax, 16(%rdx,%rcx,8)
	movq	no_reference_picture(%rip), %rax
	movq	listX+8(%rip), %rdx
	movq	%rax, 24(%rdx,%rcx,8)
	addq	$4, %rcx
	cmpq	$33, %rcx
	jne	.LBB11_186
.LBB11_187:                             # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_89:
	xorl	%r15d, %r15d
.LBB11_97:                              # %._crit_edge324
	movq	listX(%rip), %rdi
	movslq	%r15d, %rbx
	movl	$8, %edx
	movl	$compare_pic_by_poc_desc, %ecx
	movq	%rbx, %rsi
	callq	qsort
	movl	dpb+32(%rip), %eax
	testq	%rax, %rax
	movl	%r15d, %r14d
	je	.LBB11_105
# BB#98:                                # %.lr.ph318
	xorl	%ecx, %ecx
	movl	%r15d, %r14d
	.p2align	4, 0x90
.LBB11_99:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	cmpl	$3, (%rdx)
	jne	.LBB11_104
# BB#100:                               #   in Loop: Header=BB11_99 Depth=1
	movq	48(%rdx), %rdx
	cmpl	$0, 316848(%rdx)
	je	.LBB11_104
# BB#101:                               #   in Loop: Header=BB11_99 Depth=1
	cmpl	$0, 316844(%rdx)
	jne	.LBB11_104
# BB#102:                               #   in Loop: Header=BB11_99 Depth=1
	movq	img(%rip), %rsi
	movl	5672(%rsi), %esi
	cmpl	4(%rdx), %esi
	jge	.LBB11_104
# BB#103:                               #   in Loop: Header=BB11_99 Depth=1
	movq	listX(%rip), %rsi
	movslq	%r14d, %rdi
	incl	%r14d
	movq	%rdx, (%rsi,%rdi,8)
	.p2align	4, 0x90
.LBB11_104:                             #   in Loop: Header=BB11_99 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB11_99
.LBB11_105:                             # %._crit_edge319
	movq	listX(%rip), %rax
	leaq	(%rax,%rbx,8), %rdi
	movl	%r14d, %eax
	subl	%ebx, %eax
	movslq	%eax, %rbp
	movl	$8, %edx
	movl	$compare_pic_by_poc_asc, %ecx
	movq	%rbp, %rsi
	callq	qsort
	testl	%ebx, %ebx
	jle	.LBB11_113
# BB#106:                               # %.lr.ph314.preheader
	movl	%r15d, %r9d
	leaq	-1(%r9), %r8
	movq	%r9, %rsi
	andq	$3, %rsi
	je	.LBB11_107
# BB#108:                               # %.lr.ph314.prol.preheader
	leaq	(,%rbp,8), %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_109:                             # %.lr.ph314.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	listX+8(%rip), %rax
	addq	%rdi, %rax
	movq	%rdx, (%rax,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB11_109
	jmp	.LBB11_110
.LBB11_80:                              # %vector.body.preheader
	leaq	16(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB11_81:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movq	%rdx, %rdi
	sarq	$29, %rdi
	movups	%xmm0, (%r14,%rdi)
	movups	%xmm1, 16(%r14,%rdi)
	addq	%r13, %rdx
	addq	$32, %rsi
	addq	$-4, %rbp
	jne	.LBB11_81
# BB#82:                                # %middle.block
	testl	%r8d, %r8d
	movl	%ebx, %edi
	jne	.LBB11_83
	jmp	.LBB11_85
.LBB11_107:
	xorl	%ecx, %ecx
.LBB11_110:                             # %.lr.ph314.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB11_113
# BB#111:                               # %.lr.ph314.preheader.new
	shlq	$3, %rbp
	.p2align	4, 0x90
.LBB11_112:                             # %.lr.ph314
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rax
	movq	(%rax,%rcx,8), %rax
	movq	listX+8(%rip), %rdx
	addq	%rbp, %rdx
	movq	%rax, (%rdx,%rcx,8)
	movq	listX(%rip), %rax
	movq	8(%rax,%rcx,8), %rax
	movq	listX+8(%rip), %rdx
	addq	%rbp, %rdx
	movq	%rax, 8(%rdx,%rcx,8)
	movq	listX(%rip), %rax
	movq	16(%rax,%rcx,8), %rax
	movq	listX+8(%rip), %rdx
	addq	%rbp, %rdx
	movq	%rax, 16(%rdx,%rcx,8)
	movq	listX(%rip), %rax
	movq	24(%rax,%rcx,8), %rax
	movq	listX+8(%rip), %rdx
	addq	%rbp, %rdx
	movq	%rax, 24(%rdx,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %r9
	jne	.LBB11_112
.LBB11_113:                             # %.preheader286
	movl	%r14d, %edx
	subl	%r15d, %edx
	jle	.LBB11_121
# BB#114:                               # %.lr.ph310.preheader
	movslq	%r14d, %rax
	leaq	-1(%rax), %r8
	subq	%rbx, %r8
	andq	$3, %rdx
	movq	%rbx, %rcx
	je	.LBB11_118
# BB#115:                               # %.lr.ph310.prol.preheader
	leaq	(,%rbx,8), %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_116:                             # %.lr.ph310.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rbp
	addq	%rdi, %rbp
	movq	(%rbp,%rcx,8), %rbp
	movq	listX+8(%rip), %rsi
	movq	%rbp, (%rsi,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rdx
	jne	.LBB11_116
# BB#117:                               # %.lr.ph310.prol.loopexit.unr-lcssa
	addq	%rbx, %rcx
.LBB11_118:                             # %.lr.ph310.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB11_121
# BB#119:                               # %.lr.ph310.preheader.new
	shlq	$3, %rbx
	negq	%rbx
	.p2align	4, 0x90
.LBB11_120:                             # %.lr.ph310
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	listX+8(%rip), %rsi
	addq	%rbx, %rsi
	movq	%rdx, (%rsi,%rcx,8)
	movq	listX(%rip), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	listX+8(%rip), %rsi
	addq	%rbx, %rsi
	movq	%rdx, 8(%rsi,%rcx,8)
	movq	listX(%rip), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	listX+8(%rip), %rsi
	addq	%rbx, %rsi
	movq	%rdx, 16(%rsi,%rcx,8)
	movq	listX(%rip), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	listX+8(%rip), %rsi
	addq	%rbx, %rsi
	movq	%rdx, 24(%rsi,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB11_120
.LBB11_121:                             # %._crit_edge311
	movl	%r14d, listXsize+4(%rip)
	movl	%r14d, listXsize(%rip)
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	movl	%r14d, %r15d
	je	.LBB11_127
# BB#122:                               # %.lr.ph306
	xorl	%ecx, %ecx
	movl	%r14d, %r15d
	.p2align	4, 0x90
.LBB11_123:                             # =>This Inner Loop Header: Depth=1
	movq	dpb+16(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	cmpl	$3, (%rdx)
	jne	.LBB11_126
# BB#124:                               #   in Loop: Header=BB11_123 Depth=1
	movq	48(%rdx), %rdx
	cmpl	$0, 316844(%rdx)
	je	.LBB11_126
# BB#125:                               #   in Loop: Header=BB11_123 Depth=1
	movq	listX(%rip), %rsi
	movslq	%r15d, %rdi
	movq	%rdx, (%rsi,%rdi,8)
	movq	dpb+16(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	48(%rdx), %rdx
	movq	listX+8(%rip), %rsi
	leal	1(%rdi), %r15d
	movq	%rdx, (%rsi,%rdi,8)
.LBB11_126:                             #   in Loop: Header=BB11_123 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB11_123
.LBB11_127:                             # %._crit_edge307
	movq	listX(%rip), %rax
	movslq	%r14d, %rcx
	leaq	(%rax,%rcx,8), %rdi
	movl	%r15d, %eax
	subl	%r14d, %eax
	movslq	%eax, %rsi
	movl	$8, %edx
	movl	$compare_pic_by_lt_pic_num_asc, %ecx
	callq	qsort
	movslq	listXsize(%rip), %rax
	leaq	(,%rax,8), %rdi
	addq	listX+8(%rip), %rdi
	movslq	%r15d, %rbx
	movq	%rbx, %rsi
	subq	%rax, %rsi
	movl	$8, %edx
	movl	$compare_pic_by_lt_pic_num_asc, %ecx
	callq	qsort
	movl	%ebx, listXsize+4(%rip)
	movl	%ebx, listXsize(%rip)
	jmp	.LBB11_163
.LBB11_157:                             # %vector.body455.preheader
	leaq	16(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rbp
	movabsq	$17179869184, %r9       # imm = 0x400000000
	.p2align	4, 0x90
.LBB11_158:                             # %vector.body455
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movq	%rdx, %rdi
	sarq	$29, %rdi
	movups	%xmm0, (%r12,%rdi)
	movups	%xmm1, 16(%r12,%rdi)
	addq	%r9, %rdx
	addq	$32, %rsi
	addq	$-4, %rbp
	jne	.LBB11_158
# BB#159:                               # %middle.block456
	testl	%r8d, %r8d
	movl	%ebx, %edi
	jne	.LBB11_160
	jmp	.LBB11_162
.Lfunc_end11:
	.size	init_lists, .Lfunc_end11-init_lists
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI11_0:
	.quad	.LBB11_41
	.quad	.LBB11_87
	.quad	.LBB11_40
	.quad	.LBB11_41
	.quad	.LBB11_40

	.text
	.p2align	4, 0x90
	.type	compare_pic_by_pic_num_desc,@function
compare_pic_by_pic_num_desc:            # @compare_pic_by_pic_num_desc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	316832(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	316832(%rcx), %eax
	movl	$-1, %ecx
	cmovlel	%edx, %ecx
	movl	$1, %eax
	cmovgel	%ecx, %eax
	retq
.Lfunc_end12:
	.size	compare_pic_by_pic_num_desc, .Lfunc_end12-compare_pic_by_pic_num_desc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_pic_by_lt_pic_num_asc,@function
compare_pic_by_lt_pic_num_asc:          # @compare_pic_by_lt_pic_num_asc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	316836(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	316836(%rcx), %eax
	setg	%dl
	movl	$-1, %eax
	cmovgel	%edx, %eax
	retq
.Lfunc_end13:
	.size	compare_pic_by_lt_pic_num_asc, .Lfunc_end13-compare_pic_by_lt_pic_num_asc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_fs_by_frame_num_desc,@function
compare_fs_by_frame_num_desc:           # @compare_fs_by_frame_num_desc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	28(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	28(%rcx), %eax
	movl	$-1, %ecx
	cmovlel	%edx, %ecx
	movl	$1, %eax
	cmovgel	%ecx, %eax
	retq
.Lfunc_end14:
	.size	compare_fs_by_frame_num_desc, .Lfunc_end14-compare_fs_by_frame_num_desc
	.cfi_endproc

	.p2align	4, 0x90
	.type	gen_pic_list_from_frame_list,@function
gen_pic_list_from_frame_list:           # @gen_pic_list_from_frame_list
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 96
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	testl	%r9d, %r9d
	movl	$is_short_ref, %eax
	movl	$is_long_ref, %r13d
	cmoveq	%rax, %r13
	movl	%edx, %eax
	xorl	%r12d, %r12d
	movl	%edi, 36(%rsp)          # 4-byte Spill
	cmpl	$1, %edi
	movl	$0, %r14d
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jne	.LBB15_17
# BB#1:                                 # %.preheader84
	testl	%eax, %eax
	jle	.LBB15_35
# BB#2:                                 # %.critedge.preheader.preheader
	movslq	%eax, %r15
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	movq	24(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB15_3:                               # %.critedge.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_5 Depth 2
                                        #     Child Loop BB15_11 Depth 2
	cmpl	%eax, %r12d
	jge	.LBB15_9
# BB#4:                                 # %.lr.ph102.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movslq	%r12d, %r12
	.p2align	4, 0x90
.LBB15_5:                               # %.lr.ph102
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r12,8), %rax
	testb	$1, (%rax)
	je	.LBB15_36
# BB#6:                                 #   in Loop: Header=BB15_5 Depth=2
	movq	56(%rax), %rdi
	callq	*%r13
	testl	%eax, %eax
	jne	.LBB15_7
.LBB15_36:                              # %.critedge
                                        #   in Loop: Header=BB15_5 Depth=2
	incq	%r12
	cmpq	%r15, %r12
	jl	.LBB15_5
	jmp	.LBB15_8
.LBB15_7:                               #   in Loop: Header=BB15_3 Depth=1
	movq	(%rbx,%r12,8), %rax
	movq	56(%rax), %rax
	movslq	(%rbp), %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rax, (%rdx,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, (%rbp)
	leal	1(%r12), %r12d
.LBB15_8:                               # %.preheader82
                                        #   in Loop: Header=BB15_3 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB15_9:                               # %.preheader82
                                        #   in Loop: Header=BB15_3 Depth=1
	cmpl	%eax, %r14d
	jge	.LBB15_15
# BB#10:                                # %.lr.ph105.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movslq	%r14d, %r14
	.p2align	4, 0x90
.LBB15_11:                              # %.lr.ph105
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r14,8), %rax
	testb	$2, (%rax)
	je	.LBB15_37
# BB#12:                                #   in Loop: Header=BB15_11 Depth=2
	movq	64(%rax), %rdi
	callq	*%r13
	testl	%eax, %eax
	jne	.LBB15_13
.LBB15_37:                              #   in Loop: Header=BB15_11 Depth=2
	incq	%r14
	cmpq	%r15, %r14
	jl	.LBB15_11
	jmp	.LBB15_14
.LBB15_13:                              #   in Loop: Header=BB15_3 Depth=1
	movq	(%rbx,%r14,8), %rax
	movq	64(%rax), %rax
	movslq	(%rbp), %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rax, (%rdx,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, (%rbp)
	leal	1(%r14), %r14d
.LBB15_14:                              # %.backedge86
                                        #   in Loop: Header=BB15_3 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB15_15:                              # %.backedge86
                                        #   in Loop: Header=BB15_3 Depth=1
	cmpl	%eax, %r12d
	jl	.LBB15_3
# BB#16:                                # %.backedge86
                                        #   in Loop: Header=BB15_3 Depth=1
	cmpl	%eax, %r14d
	jl	.LBB15_3
.LBB15_17:                              # %.loopexit85
	cmpl	$2, 36(%rsp)            # 4-byte Folded Reload
	jne	.LBB15_35
# BB#18:                                # %.preheader80
	cmpl	%eax, %r12d
	jl	.LBB15_20
# BB#19:                                # %.preheader80
	cmpl	%eax, %r14d
	jge	.LBB15_35
.LBB15_20:                              # %.critedge1.preheader.preheader
	movslq	%eax, %rbp
	.p2align	4, 0x90
.LBB15_21:                              # %.critedge1.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_23 Depth 2
                                        #     Child Loop BB15_29 Depth 2
	cmpl	%eax, %r14d
	jge	.LBB15_27
# BB#22:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB15_21 Depth=1
	movslq	%r14d, %r14
	.p2align	4, 0x90
.LBB15_23:                              # %.lr.ph
                                        #   Parent Loop BB15_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r14,8), %rax
	testb	$2, (%rax)
	je	.LBB15_38
# BB#24:                                #   in Loop: Header=BB15_23 Depth=2
	movq	64(%rax), %rdi
	callq	*%r13
	testl	%eax, %eax
	jne	.LBB15_25
.LBB15_38:                              # %.critedge1
                                        #   in Loop: Header=BB15_23 Depth=2
	incq	%r14
	cmpq	%rbp, %r14
	jl	.LBB15_23
	jmp	.LBB15_26
.LBB15_25:                              #   in Loop: Header=BB15_21 Depth=1
	movq	(%rbx,%r14,8), %rax
	movq	64(%rax), %rax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movslq	(%rdx), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rax, (%rsi,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, (%rdx)
	leal	1(%r14), %r14d
.LBB15_26:                              # %.preheader
                                        #   in Loop: Header=BB15_21 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB15_27:                              # %.preheader
                                        #   in Loop: Header=BB15_21 Depth=1
	cmpl	%eax, %r12d
	jge	.LBB15_33
# BB#28:                                # %.lr.ph96.preheader
                                        #   in Loop: Header=BB15_21 Depth=1
	movslq	%r12d, %r12
	.p2align	4, 0x90
.LBB15_29:                              # %.lr.ph96
                                        #   Parent Loop BB15_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r12,8), %rax
	testb	$1, (%rax)
	je	.LBB15_39
# BB#30:                                #   in Loop: Header=BB15_29 Depth=2
	movq	56(%rax), %rdi
	callq	*%r13
	testl	%eax, %eax
	jne	.LBB15_31
.LBB15_39:                              #   in Loop: Header=BB15_29 Depth=2
	incq	%r12
	cmpq	%rbp, %r12
	jl	.LBB15_29
	jmp	.LBB15_32
.LBB15_31:                              #   in Loop: Header=BB15_21 Depth=1
	movq	(%rbx,%r12,8), %rax
	movq	56(%rax), %rax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movslq	(%rdx), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rax, (%rsi,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, (%rdx)
	leal	1(%r12), %r12d
.LBB15_32:                              # %.backedge
                                        #   in Loop: Header=BB15_21 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB15_33:                              # %.backedge
                                        #   in Loop: Header=BB15_21 Depth=1
	cmpl	%eax, %r12d
	jl	.LBB15_21
# BB#34:                                # %.backedge
                                        #   in Loop: Header=BB15_21 Depth=1
	cmpl	%eax, %r14d
	jl	.LBB15_21
.LBB15_35:                              # %.loopexit81
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	gen_pic_list_from_frame_list, .Lfunc_end15-gen_pic_list_from_frame_list
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_fs_by_lt_pic_idx_asc,@function
compare_fs_by_lt_pic_idx_asc:           # @compare_fs_by_lt_pic_idx_asc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	32(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	32(%rcx), %eax
	setg	%dl
	movl	$-1, %eax
	cmovgel	%edx, %eax
	retq
.Lfunc_end16:
	.size	compare_fs_by_lt_pic_idx_asc, .Lfunc_end16-compare_fs_by_lt_pic_idx_asc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_pic_by_poc_desc,@function
compare_pic_by_poc_desc:                # @compare_pic_by_poc_desc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	4(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	4(%rcx), %eax
	movl	$-1, %ecx
	cmovlel	%edx, %ecx
	movl	$1, %eax
	cmovgel	%ecx, %eax
	retq
.Lfunc_end17:
	.size	compare_pic_by_poc_desc, .Lfunc_end17-compare_pic_by_poc_desc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_pic_by_poc_asc,@function
compare_pic_by_poc_asc:                 # @compare_pic_by_poc_asc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	4(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	4(%rcx), %eax
	setg	%dl
	movl	$-1, %eax
	cmovgel	%edx, %eax
	retq
.Lfunc_end18:
	.size	compare_pic_by_poc_asc, .Lfunc_end18-compare_pic_by_poc_asc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_fs_by_poc_desc,@function
compare_fs_by_poc_desc:                 # @compare_fs_by_poc_desc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	40(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	40(%rcx), %eax
	movl	$-1, %ecx
	cmovlel	%edx, %ecx
	movl	$1, %eax
	cmovgel	%ecx, %eax
	retq
.Lfunc_end19:
	.size	compare_fs_by_poc_desc, .Lfunc_end19-compare_fs_by_poc_desc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_fs_by_poc_asc,@function
compare_fs_by_poc_asc:                  # @compare_fs_by_poc_asc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	40(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	40(%rcx), %eax
	setg	%dl
	movl	$-1, %eax
	cmovgel	%edx, %eax
	retq
.Lfunc_end20:
	.size	compare_fs_by_poc_asc, .Lfunc_end20-compare_fs_by_poc_asc
	.cfi_endproc

	.globl	init_mbaff_lists
	.p2align	4, 0x90
	.type	init_mbaff_lists,@function
init_mbaff_lists:                       # @init_mbaff_lists
	.cfi_startproc
# BB#0:
	movq	$-16, %rax
	movq	no_reference_picture(%rip), %rcx
	.p2align	4, 0x90
.LBB21_1:                               # %.preheader31
                                        # =>This Inner Loop Header: Depth=1
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, (%rdx)
	movq	no_reference_picture(%rip), %rcx
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 8(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 16(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 32(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 40(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 48(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 56(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 64(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 72(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 80(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 88(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 96(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 104(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 112(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 120(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 128(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 136(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 144(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 152(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 160(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 168(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 176(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 184(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 192(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 200(%rdx)
	movq	no_reference_picture(%rip), %rcx
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 208(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 216(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 224(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 232(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 240(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 248(%rdx)
	movq	listX+48(%rax,%rax), %rdx
	movq	%rcx, 256(%rdx)
	movl	$0, listXsize+24(%rax)
	addq	$4, %rax
	jne	.LBB21_1
# BB#2:                                 # %.preheader
	movslq	listXsize(%rip), %rax
	testq	%rax, %rax
	jle	.LBB21_5
# BB#3:                                 # %.lr.ph35
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB21_4:                               # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	317000(%rsi), %rsi
	movq	listX+16(%rip), %rdi
	movq	%rsi, (%rdi,%rcx,2)
	movq	listX(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	317008(%rsi), %rsi
	movq	listX+16(%rip), %rdi
	movq	%rsi, 8(%rdi,%rcx,2)
	movq	listX(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	317008(%rsi), %rsi
	movq	listX+32(%rip), %rdi
	movq	%rsi, (%rdi,%rcx,2)
	movq	listX(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	317000(%rsi), %rsi
	movq	listX+32(%rip), %rdi
	movq	%rsi, 8(%rdi,%rcx,2)
	incq	%rdx
	addq	$8, %rcx
	cmpq	%rax, %rdx
	jl	.LBB21_4
.LBB21_5:                               # %._crit_edge36
	addl	%eax, %eax
	movl	%eax, listXsize+16(%rip)
	movl	%eax, listXsize+8(%rip)
	movslq	listXsize+4(%rip), %rax
	testq	%rax, %rax
	jle	.LBB21_8
# BB#6:                                 # %.lr.ph
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB21_7:                               # =>This Inner Loop Header: Depth=1
	movq	listX+8(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	317000(%rsi), %rsi
	movq	listX+24(%rip), %rdi
	movq	%rsi, (%rdi,%rcx,2)
	movq	listX+8(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	317008(%rsi), %rsi
	movq	listX+24(%rip), %rdi
	movq	%rsi, 8(%rdi,%rcx,2)
	movq	listX+8(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	317008(%rsi), %rsi
	movq	listX+40(%rip), %rdi
	movq	%rsi, (%rdi,%rcx,2)
	movq	listX+8(%rip), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	317000(%rsi), %rsi
	movq	listX+40(%rip), %rdi
	movq	%rsi, 8(%rdi,%rcx,2)
	incq	%rdx
	addq	$8, %rcx
	cmpq	%rax, %rdx
	jl	.LBB21_7
.LBB21_8:                               # %._crit_edge
	addl	%eax, %eax
	movl	%eax, listXsize+20(%rip)
	movl	%eax, listXsize+12(%rip)
	retq
.Lfunc_end21:
	.size	init_mbaff_lists, .Lfunc_end21-init_mbaff_lists
	.cfi_endproc

	.globl	reorder_ref_pic_list
	.p2align	4, 0x90
	.type	reorder_ref_pic_list,@function
reorder_ref_pic_list:                   # @reorder_ref_pic_list
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi64:
	.cfi_def_cfa_offset 112
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%rcx, %r9
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	movq	img(%rip), %rax
	cmpl	$0, 5584(%rax)
	movl	5676(%rax), %r11d
	movl	5816(%rax), %r14d
	je	.LBB22_2
# BB#1:
	addl	%r14d, %r14d
	leal	1(%r11,%r11), %r11d
.LBB22_2:
	movl	(%r9), %eax
	leal	1(%rdx), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	cmpl	$3, %eax
	je	.LBB22_62
# BB#3:                                 # %.lr.ph
	movslq	8(%rsp), %r15           # 4-byte Folded Reload
	movslq	%edx, %r12
	xorl	%r13d, %r13d
	movq	%r9, %rbx
	movl	%r11d, %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	movq	%r15, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB22_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_38 Depth 2
                                        #     Child Loop BB22_46 Depth 2
                                        #     Child Loop BB22_53 Depth 2
                                        #     Child Loop BB22_55 Depth 2
                                        #     Child Loop BB22_13 Depth 2
                                        #     Child Loop BB22_21 Depth 2
                                        #     Child Loop BB22_28 Depth 2
                                        #     Child Loop BB22_30 Depth 2
	cmpl	$4, %eax
	jl	.LBB22_6
# BB#5:                                 #   in Loop: Header=BB22_4 Depth=1
	movl	$.L.str.14, %edi
	movl	$500, %esi              # imm = 0x1F4
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r9, %r13
	movq	%rdx, %r14
	movq	%r11, %r15
	callq	error
	movq	%r15, %r11
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	%r14, %rdx
	movl	12(%rsp), %r14d         # 4-byte Reload
	movq	%r13, %r9
	movq	48(%rsp), %r13          # 8-byte Reload
	movl	(%rbx), %eax
.LBB22_6:                               #   in Loop: Header=BB22_4 Depth=1
	cmpl	$1, %eax
	jg	.LBB22_35
# BB#7:                                 #   in Loop: Header=BB22_4 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%r13,4), %ecx
	incl	%ecx
	testl	%eax, %eax
	je	.LBB22_8
# BB#9:                                 #   in Loop: Header=BB22_4 Depth=1
	addl	4(%rsp), %ecx           # 4-byte Folded Reload
	cmpl	%r14d, %ecx
	movl	%r14d, %eax
	movl	$0, %esi
	cmovll	%esi, %eax
	subl	%eax, %ecx
	movl	%ecx, %esi
	jmp	.LBB22_10
	.p2align	4, 0x90
.LBB22_35:                              #   in Loop: Header=BB22_4 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r13,4), %r8d
	movl	dpb+36(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB22_50
# BB#36:                                # %.lr.ph.i.i52
                                        #   in Loop: Header=BB22_4 Depth=1
	movq	img(%rip), %rax
	cmpl	$0, 5584(%rax)
	movq	dpb+16(%rip), %r10
	je	.LBB22_45
# BB#37:                                # %.lr.ph.split.i.i56.preheader
                                        #   in Loop: Header=BB22_4 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB22_38:                              # %.lr.ph.split.i.i56
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r10,%rdi,8), %rbx
	movl	4(%rbx), %eax
	testb	$1, %al
	je	.LBB22_41
# BB#39:                                #   in Loop: Header=BB22_38 Depth=2
	movq	56(%rbx), %rsi
	cmpl	$0, 316844(%rsi)
	je	.LBB22_41
# BB#40:                                #   in Loop: Header=BB22_38 Depth=2
	cmpl	%r8d, 316836(%rsi)
	je	.LBB22_51
.LBB22_41:                              #   in Loop: Header=BB22_38 Depth=2
	testb	$2, %al
	je	.LBB22_44
# BB#42:                                #   in Loop: Header=BB22_38 Depth=2
	movq	64(%rbx), %rsi
	cmpl	$0, 316844(%rsi)
	je	.LBB22_44
# BB#43:                                #   in Loop: Header=BB22_38 Depth=2
	cmpl	%r8d, 316836(%rsi)
	je	.LBB22_51
.LBB22_44:                              #   in Loop: Header=BB22_38 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jb	.LBB22_38
	jmp	.LBB22_50
.LBB22_8:                               #   in Loop: Header=BB22_4 Depth=1
	movl	4(%rsp), %esi           # 4-byte Reload
	subl	%ecx, %esi
	movl	%esi, %eax
	sarl	$31, %eax
	andl	%r14d, %eax
	addl	%eax, %esi
.LBB22_10:                              #   in Loop: Header=BB22_4 Depth=1
	cmpl	%r11d, %esi
	movl	$0, %eax
	cmovgl	%r14d, %eax
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movl	%esi, %r8d
	subl	%eax, %r8d
	movl	dpb+32(%rip), %r10d
	testq	%r10, %r10
	je	.LBB22_25
# BB#11:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB22_4 Depth=1
	movq	img(%rip), %rax
	cmpl	$0, 5584(%rax)
	movq	dpb+8(%rip), %rcx
	je	.LBB22_20
# BB#12:                                # %.lr.ph.split.i.i.preheader
                                        #   in Loop: Header=BB22_4 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB22_13:                              # %.lr.ph.split.i.i
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdi,8), %rbx
	movl	4(%rbx), %eax
	testb	$1, %al
	je	.LBB22_16
# BB#14:                                #   in Loop: Header=BB22_13 Depth=2
	movq	56(%rbx), %rsi
	cmpl	$0, 316844(%rsi)
	jne	.LBB22_16
# BB#15:                                #   in Loop: Header=BB22_13 Depth=2
	cmpl	%r8d, 316832(%rsi)
	je	.LBB22_26
	.p2align	4, 0x90
.LBB22_16:                              #   in Loop: Header=BB22_13 Depth=2
	testb	$2, %al
	je	.LBB22_19
# BB#17:                                #   in Loop: Header=BB22_13 Depth=2
	movq	64(%rbx), %rsi
	cmpl	$0, 316844(%rsi)
	jne	.LBB22_19
# BB#18:                                #   in Loop: Header=BB22_13 Depth=2
	cmpl	%r8d, 316832(%rsi)
	je	.LBB22_26
	.p2align	4, 0x90
.LBB22_19:                              #   in Loop: Header=BB22_13 Depth=2
	incq	%rdi
	cmpq	%r10, %rdi
	jb	.LBB22_13
	jmp	.LBB22_25
.LBB22_45:                              # %.lr.ph.split.us.i.i54.preheader
                                        #   in Loop: Header=BB22_4 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB22_46:                              # %.lr.ph.split.us.i.i54
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r10,%rdi,8), %rax
	cmpl	$3, 4(%rax)
	jne	.LBB22_49
# BB#47:                                #   in Loop: Header=BB22_46 Depth=2
	movq	48(%rax), %rsi
	cmpl	$0, 316844(%rsi)
	je	.LBB22_49
# BB#48:                                #   in Loop: Header=BB22_46 Depth=2
	cmpl	%r8d, 316836(%rsi)
	je	.LBB22_51
.LBB22_49:                              #   in Loop: Header=BB22_46 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jb	.LBB22_46
.LBB22_50:                              #   in Loop: Header=BB22_4 Depth=1
	xorl	%esi, %esi
.LBB22_51:                              # %get_long_term_pic.exit.i
                                        #   in Loop: Header=BB22_4 Depth=1
	movl	(%rsp), %eax            # 4-byte Reload
	cmpl	%edx, %eax
	movslq	%eax, %rcx
	movq	%r15, %rax
	jle	.LBB22_53
# BB#52:                                # %._crit_edge40.i.thread
                                        #   in Loop: Header=BB22_4 Depth=1
	leal	1(%rcx), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	%rsi, (%rbp,%rcx,8)
	jmp	.LBB22_61
	.p2align	4, 0x90
.LBB22_53:                              # %.lr.ph39.i
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbp,%rax,8), %rdi
	movq	%rdi, (%rbp,%rax,8)
	decq	%rax
	cmpq	%rcx, %rax
	jg	.LBB22_53
# BB#54:                                # %.lr.ph.preheader.i60
                                        #   in Loop: Header=BB22_4 Depth=1
	leal	1(%rcx), %ebx
	movq	%rsi, (%rbp,%rcx,8)
	movl	%ebx, %edi
	.p2align	4, 0x90
.LBB22_55:                              # %.lr.ph.i62
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp,%rcx,8), %rax
	testq	%rax, %rax
	je	.LBB22_59
# BB#56:                                #   in Loop: Header=BB22_55 Depth=2
	cmpl	$0, 316844(%rax)
	je	.LBB22_58
# BB#57:                                #   in Loop: Header=BB22_55 Depth=2
	cmpl	%r8d, 316836(%rax)
	je	.LBB22_59
.LBB22_58:                              #   in Loop: Header=BB22_55 Depth=2
	movslq	%edi, %rsi
	incl	%edi
	movq	%rax, (%rbp,%rsi,8)
.LBB22_59:                              #   in Loop: Header=BB22_55 Depth=2
	incq	%rcx
	cmpq	%r12, %rcx
	jle	.LBB22_55
	jmp	.LBB22_60
.LBB22_20:                              # %.lr.ph.split.us.i.i.preheader
                                        #   in Loop: Header=BB22_4 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB22_21:                              # %.lr.ph.split.us.i.i
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdi,8), %rax
	cmpl	$3, 4(%rax)
	jne	.LBB22_24
# BB#22:                                #   in Loop: Header=BB22_21 Depth=2
	movq	48(%rax), %rsi
	cmpl	$0, 316844(%rsi)
	jne	.LBB22_24
# BB#23:                                #   in Loop: Header=BB22_21 Depth=2
	cmpl	%r8d, 316832(%rsi)
	je	.LBB22_26
	.p2align	4, 0x90
.LBB22_24:                              #   in Loop: Header=BB22_21 Depth=2
	incq	%rdi
	cmpq	%r10, %rdi
	jb	.LBB22_21
.LBB22_25:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB22_4 Depth=1
	movq	no_reference_picture(%rip), %rsi
.LBB22_26:                              # %get_short_term_pic.exit.i
                                        #   in Loop: Header=BB22_4 Depth=1
	movl	(%rsp), %ebx            # 4-byte Reload
	cmpl	%edx, %ebx
	movslq	%ebx, %rcx
	movq	%r15, %rax
	jle	.LBB22_28
# BB#27:                                # %._crit_edge41.i.thread
                                        #   in Loop: Header=BB22_4 Depth=1
	incl	%ebx
	movl	%ebx, (%rsp)            # 4-byte Spill
	movq	%rsi, (%rbp,%rcx,8)
	jmp	.LBB22_61
	.p2align	4, 0x90
.LBB22_28:                              # %.lr.ph40.i
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbp,%rax,8), %rdi
	movq	%rdi, (%rbp,%rax,8)
	decq	%rax
	cmpq	%rcx, %rax
	jg	.LBB22_28
# BB#29:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB22_4 Depth=1
	incl	%ebx
	movq	%rsi, (%rbp,%rcx,8)
	movl	%ebx, %edi
	.p2align	4, 0x90
.LBB22_30:                              # %.lr.ph.i
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp,%rcx,8), %rax
	testq	%rax, %rax
	je	.LBB22_34
# BB#31:                                #   in Loop: Header=BB22_30 Depth=2
	cmpl	$0, 316844(%rax)
	jne	.LBB22_33
# BB#32:                                #   in Loop: Header=BB22_30 Depth=2
	cmpl	%r8d, 316832(%rax)
	je	.LBB22_34
.LBB22_33:                              #   in Loop: Header=BB22_30 Depth=2
	movslq	%edi, %rsi
	incl	%edi
	movq	%rax, (%rbp,%rsi,8)
.LBB22_34:                              #   in Loop: Header=BB22_30 Depth=2
	incq	%rcx
	cmpq	%r12, %rcx
	jle	.LBB22_30
.LBB22_60:                              #   in Loop: Header=BB22_4 Depth=1
	movl	%ebx, (%rsp)            # 4-byte Spill
.LBB22_61:                              # %reorder_short_term.exit
                                        #   in Loop: Header=BB22_4 Depth=1
	leaq	4(%r9,%r13,4), %rbx
	movl	4(%r9,%r13,4), %eax
	incq	%r13
	cmpl	$3, %eax
	jne	.LBB22_4
.LBB22_62:                              # %._crit_edge
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	8(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	reorder_ref_pic_list, .Lfunc_end22-reorder_ref_pic_list
	.cfi_endproc

	.globl	update_ref_list
	.p2align	4, 0x90
	.type	update_ref_list,@function
update_ref_list:                        # @update_ref_list
	.cfi_startproc
# BB#0:
	movl	dpb+28(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB23_1
# BB#2:                                 # %.lr.ph17
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB23_3:                               # =>This Inner Loop Header: Depth=1
	movq	dpb(%rip), %rax
	movq	(%rax,%rdx,8), %rsi
	movl	(%rsi), %edi
	cmpl	$3, %edi
	jne	.LBB23_6
# BB#4:                                 #   in Loop: Header=BB23_3 Depth=1
	movq	48(%rsi), %rax
	cmpl	$0, 316848(%rax)
	je	.LBB23_7
# BB#5:                                 #   in Loop: Header=BB23_3 Depth=1
	cmpl	$0, 316844(%rax)
	jne	.LBB23_7
	jmp	.LBB23_14
	.p2align	4, 0x90
.LBB23_6:                               #   in Loop: Header=BB23_3 Depth=1
	testb	$1, %dil
	je	.LBB23_10
.LBB23_7:                               # %.thread.i
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	56(%rsi), %rax
	testq	%rax, %rax
	je	.LBB23_10
# BB#8:                                 #   in Loop: Header=BB23_3 Depth=1
	cmpl	$0, 316848(%rax)
	je	.LBB23_10
# BB#9:                                 #   in Loop: Header=BB23_3 Depth=1
	cmpl	$0, 316844(%rax)
	je	.LBB23_14
	.p2align	4, 0x90
.LBB23_10:                              #   in Loop: Header=BB23_3 Depth=1
	testb	$2, %dil
	je	.LBB23_15
# BB#11:                                #   in Loop: Header=BB23_3 Depth=1
	movq	64(%rsi), %rax
	testq	%rax, %rax
	je	.LBB23_15
# BB#12:                                #   in Loop: Header=BB23_3 Depth=1
	cmpl	$0, 316848(%rax)
	je	.LBB23_15
# BB#13:                                #   in Loop: Header=BB23_3 Depth=1
	cmpl	$0, 316844(%rax)
	jne	.LBB23_15
.LBB23_14:                              #   in Loop: Header=BB23_3 Depth=1
	movq	dpb+8(%rip), %rax
	movl	%r8d, %edi
	incl	%r8d
	movq	%rsi, (%rax,%rdi,8)
.LBB23_15:                              # %is_short_term_reference.exit
                                        #   in Loop: Header=BB23_3 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jb	.LBB23_3
	jmp	.LBB23_16
.LBB23_1:
	xorl	%r8d, %r8d
.LBB23_16:                              # %._crit_edge18
	movl	%r8d, dpb+32(%rip)
	movl	dpb+24(%rip), %ecx
	cmpl	%ecx, %r8d
	jae	.LBB23_22
# BB#17:                                # %.lr.ph
	movl	%r8d, %edx
	movl	%ecx, %esi
	subl	%r8d, %esi
	leaq	-1(%rcx), %rax
	subq	%rdx, %rax
	andq	$7, %rsi
	je	.LBB23_20
# BB#18:                                # %.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB23_19:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rdi
	movq	$0, (%rdi,%rdx,8)
	incq	%rdx
	incq	%rsi
	jne	.LBB23_19
.LBB23_20:                              # %.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB23_22
	.p2align	4, 0x90
.LBB23_21:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rax
	movq	$0, (%rax,%rdx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, 8(%rax,%rdx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, 16(%rax,%rdx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, 24(%rax,%rdx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, 32(%rax,%rdx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, 40(%rax,%rdx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, 48(%rax,%rdx,8)
	movq	dpb+8(%rip), %rax
	movq	$0, 56(%rax,%rdx,8)
	addq	$8, %rdx
	cmpq	%rcx, %rdx
	jb	.LBB23_21
.LBB23_22:                              # %._crit_edge
	retq
.Lfunc_end23:
	.size	update_ref_list, .Lfunc_end23-update_ref_list
	.cfi_endproc

	.globl	update_ltref_list
	.p2align	4, 0x90
	.type	update_ltref_list,@function
update_ltref_list:                      # @update_ltref_list
	.cfi_startproc
# BB#0:
	movl	dpb+28(%rip), %r8d
	testq	%r8, %r8
	je	.LBB24_1
# BB#2:                                 # %.lr.ph17
	xorl	%edx, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB24_3:                               # =>This Inner Loop Header: Depth=1
	movq	dpb(%rip), %rcx
	movq	(%rcx,%rdx,8), %rsi
	movl	(%rsi), %edi
	cmpl	$3, %edi
	jne	.LBB24_6
# BB#4:                                 #   in Loop: Header=BB24_3 Depth=1
	movq	48(%rsi), %rcx
	cmpl	$0, 316848(%rcx)
	je	.LBB24_7
# BB#5:                                 #   in Loop: Header=BB24_3 Depth=1
	cmpl	$0, 316844(%rcx)
	jne	.LBB24_14
	jmp	.LBB24_7
	.p2align	4, 0x90
.LBB24_6:                               #   in Loop: Header=BB24_3 Depth=1
	testb	$1, %dil
	je	.LBB24_10
.LBB24_7:                               # %.thread.i
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	56(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB24_10
# BB#8:                                 #   in Loop: Header=BB24_3 Depth=1
	cmpl	$0, 316848(%rcx)
	je	.LBB24_10
# BB#9:                                 #   in Loop: Header=BB24_3 Depth=1
	cmpl	$0, 316844(%rcx)
	jne	.LBB24_14
	.p2align	4, 0x90
.LBB24_10:                              #   in Loop: Header=BB24_3 Depth=1
	testb	$2, %dil
	je	.LBB24_15
# BB#11:                                #   in Loop: Header=BB24_3 Depth=1
	movq	64(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB24_15
# BB#12:                                #   in Loop: Header=BB24_3 Depth=1
	cmpl	$0, 316848(%rcx)
	je	.LBB24_15
# BB#13:                                #   in Loop: Header=BB24_3 Depth=1
	cmpl	$0, 316844(%rcx)
	je	.LBB24_15
	.p2align	4, 0x90
.LBB24_14:                              #   in Loop: Header=BB24_3 Depth=1
	movq	dpb+16(%rip), %rcx
	movl	%eax, %edi
	incl	%eax
	movq	%rsi, (%rcx,%rdi,8)
.LBB24_15:                              # %is_long_term_reference.exit
                                        #   in Loop: Header=BB24_3 Depth=1
	incq	%rdx
	cmpq	%r8, %rdx
	jb	.LBB24_3
	jmp	.LBB24_16
.LBB24_1:
	xorl	%eax, %eax
.LBB24_16:                              # %._crit_edge18
	movl	%eax, dpb+36(%rip)
	movl	dpb+24(%rip), %ecx
	cmpl	%ecx, %eax
	jae	.LBB24_22
# BB#17:                                # %.lr.ph
	movl	%eax, %edx
	movl	%ecx, %esi
	subl	%eax, %esi
	leaq	-1(%rcx), %rax
	subq	%rdx, %rax
	andq	$7, %rsi
	je	.LBB24_20
# BB#18:                                # %.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB24_19:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+16(%rip), %rdi
	movq	$0, (%rdi,%rdx,8)
	incq	%rdx
	incq	%rsi
	jne	.LBB24_19
.LBB24_20:                              # %.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB24_22
	.p2align	4, 0x90
.LBB24_21:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+16(%rip), %rax
	movq	$0, (%rax,%rdx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, 8(%rax,%rdx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, 16(%rax,%rdx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, 24(%rax,%rdx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, 32(%rax,%rdx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, 40(%rax,%rdx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, 48(%rax,%rdx,8)
	movq	dpb+16(%rip), %rax
	movq	$0, 56(%rax,%rdx,8)
	addq	$8, %rdx
	cmpq	%rcx, %rdx
	jb	.LBB24_21
.LBB24_22:                              # %._crit_edge
	retq
.Lfunc_end24:
	.size	update_ltref_list, .Lfunc_end24-update_ltref_list
	.cfi_endproc

	.globl	mm_update_max_long_term_frame_idx
	.p2align	4, 0x90
	.type	mm_update_max_long_term_frame_idx,@function
mm_update_max_long_term_frame_idx:      # @mm_update_max_long_term_frame_idx
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	-1(%rdi), %eax
	movl	%eax, dpb+44(%rip)
	movl	dpb+36(%rip), %r8d
	testq	%r8, %r8
	je	.LBB25_16
# BB#1:                                 # %.lr.ph
	movq	dpb+16(%rip), %r9
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB25_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rdx,8), %rsi
	cmpl	%edi, 32(%rsi)
	jl	.LBB25_15
# BB#3:                                 #   in Loop: Header=BB25_2 Depth=1
	movl	(%rsi), %eax
	testb	$1, %al
	je	.LBB25_6
# BB#4:                                 #   in Loop: Header=BB25_2 Depth=1
	movq	56(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB25_6
# BB#5:                                 #   in Loop: Header=BB25_2 Depth=1
	movq	$0, 316844(%rcx)
.LBB25_6:                               #   in Loop: Header=BB25_2 Depth=1
	testb	$2, %al
	je	.LBB25_9
# BB#7:                                 #   in Loop: Header=BB25_2 Depth=1
	movq	64(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB25_9
# BB#8:                                 #   in Loop: Header=BB25_2 Depth=1
	movq	$0, 316844(%rcx)
.LBB25_9:                               # %thread-pre-split.i
                                        #   in Loop: Header=BB25_2 Depth=1
	cmpl	$3, %eax
	jne	.LBB25_14
# BB#10:                                #   in Loop: Header=BB25_2 Depth=1
	movq	56(%rsi), %rax
	testq	%rax, %rax
	je	.LBB25_13
# BB#11:                                #   in Loop: Header=BB25_2 Depth=1
	movq	64(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB25_13
# BB#12:                                #   in Loop: Header=BB25_2 Depth=1
	movl	$0, 316848(%rax)
	movl	$0, 316844(%rax)
	movq	$0, 316844(%rcx)
.LBB25_13:                              #   in Loop: Header=BB25_2 Depth=1
	movq	48(%rsi), %rax
	movl	$0, 316848(%rax)
	movl	$0, 316844(%rax)
.LBB25_14:                              # %unmark_for_long_term_reference.exit
                                        #   in Loop: Header=BB25_2 Depth=1
	movq	$0, 4(%rsi)
.LBB25_15:                              #   in Loop: Header=BB25_2 Depth=1
	incq	%rdx
	cmpq	%r8, %rdx
	jb	.LBB25_2
.LBB25_16:                              # %._crit_edge
	retq
.Lfunc_end25:
	.size	mm_update_max_long_term_frame_idx, .Lfunc_end25-mm_update_max_long_term_frame_idx
	.cfi_endproc

	.globl	store_picture_in_dpb
	.p2align	4, 0x90
	.type	store_picture_in_dpb,@function
store_picture_in_dpb:                   # @store_picture_in_dpb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi77:
	.cfi_def_cfa_offset 64
.Lcfi78:
	.cfi_offset %rbx, -56
.Lcfi79:
	.cfi_offset %r12, -48
.Lcfi80:
	.cfi_offset %r13, -40
.Lcfi81:
	.cfi_offset %r14, -32
.Lcfi82:
	.cfi_offset %r15, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	img(%rip), %rax
	movl	$0, 5860(%rax)
	xorl	%ecx, %ecx
	cmpl	$2, (%r14)
	sete	%cl
	movl	%ecx, 5864(%rax)
	cmpl	$0, 317028(%r14)
	je	.LBB26_26
# BB#1:
	cmpl	$0, 317032(%r14)
	je	.LBB26_30
# BB#2:                                 # %.preheader16.i
	cmpl	$0, dpb+28(%rip)
	je	.LBB26_15
# BB#3:                                 # %.lr.ph21.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB26_4:                               # %.lr.ph21.i
                                        # =>This Inner Loop Header: Depth=1
	movq	dpb(%rip), %rcx
	movl	%eax, %ebp
	movq	(%rcx,%rbp,8), %rbx
	testq	%rbx, %rbx
	je	.LBB26_12
# BB#5:                                 #   in Loop: Header=BB26_4 Depth=1
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_7
# BB#6:                                 #   in Loop: Header=BB26_4 Depth=1
	callq	free_storable_picture
	movq	$0, 48(%rbx)
.LBB26_7:                               #   in Loop: Header=BB26_4 Depth=1
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_9
# BB#8:                                 #   in Loop: Header=BB26_4 Depth=1
	callq	free_storable_picture
	movq	$0, 56(%rbx)
.LBB26_9:                               #   in Loop: Header=BB26_4 Depth=1
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_11
# BB#10:                                #   in Loop: Header=BB26_4 Depth=1
	callq	free_storable_picture
.LBB26_11:                              #   in Loop: Header=BB26_4 Depth=1
	movq	%rbx, %rdi
	callq	free
.LBB26_12:                              # %free_frame_store.exit.i
                                        #   in Loop: Header=BB26_4 Depth=1
	movl	$1, %edi
	movl	$72, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB26_14
# BB#13:                                #   in Loop: Header=BB26_4 Depth=1
	movl	$.L.str.8, %edi
	callq	no_mem_exit
.LBB26_14:                              # %alloc_frame_store.exit.i
                                        #   in Loop: Header=BB26_4 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movups	%xmm0, 48(%rbx)
	movq	$0, 64(%rbx)
	movq	dpb(%rip), %rax
	movq	%rbx, (%rax,%rbp,8)
	leal	1(%rbp), %eax
	cmpl	dpb+28(%rip), %eax
	jb	.LBB26_4
.LBB26_15:                              # %.preheader15.i
	movl	dpb+32(%rip), %eax
	testq	%rax, %rax
	je	.LBB26_20
# BB#16:                                # %.lr.ph19.i
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$7, %rsi
	je	.LBB26_18
	.p2align	4, 0x90
.LBB26_17:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rdi
	movq	$0, (%rdi,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB26_17
.LBB26_18:                              # %.prol.loopexit341
	cmpq	$7, %rdx
	jb	.LBB26_20
	.p2align	4, 0x90
.LBB26_19:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rdx
	movq	$0, (%rdx,%rcx,8)
	movq	dpb+8(%rip), %rdx
	movq	$0, 8(%rdx,%rcx,8)
	movq	dpb+8(%rip), %rdx
	movq	$0, 16(%rdx,%rcx,8)
	movq	dpb+8(%rip), %rdx
	movq	$0, 24(%rdx,%rcx,8)
	movq	dpb+8(%rip), %rdx
	movq	$0, 32(%rdx,%rcx,8)
	movq	dpb+8(%rip), %rdx
	movq	$0, 40(%rdx,%rcx,8)
	movq	dpb+8(%rip), %rdx
	movq	$0, 48(%rdx,%rcx,8)
	movq	dpb+8(%rip), %rdx
	movq	$0, 56(%rdx,%rcx,8)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB26_19
.LBB26_20:                              # %.preheader.i
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	je	.LBB26_25
# BB#21:                                # %.lr.ph.i
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$7, %rsi
	je	.LBB26_23
	.p2align	4, 0x90
.LBB26_22:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+16(%rip), %rdi
	movq	$0, (%rdi,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB26_22
.LBB26_23:                              # %.prol.loopexit337
	cmpq	$7, %rdx
	jb	.LBB26_25
	.p2align	4, 0x90
.LBB26_24:                              # =>This Inner Loop Header: Depth=1
	movq	dpb+16(%rip), %rdx
	movq	$0, (%rdx,%rcx,8)
	movq	dpb+16(%rip), %rdx
	movq	$0, 8(%rdx,%rcx,8)
	movq	dpb+16(%rip), %rdx
	movq	$0, 16(%rdx,%rcx,8)
	movq	dpb+16(%rip), %rdx
	movq	$0, 24(%rdx,%rcx,8)
	movq	dpb+16(%rip), %rdx
	movq	$0, 32(%rdx,%rcx,8)
	movq	dpb+16(%rip), %rdx
	movq	$0, 40(%rdx,%rcx,8)
	movq	dpb+16(%rip), %rdx
	movq	$0, 48(%rdx,%rcx,8)
	movq	dpb+16(%rip), %rdx
	movq	$0, 56(%rdx,%rcx,8)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB26_24
.LBB26_25:                              # %._crit_edge.i
	movl	$0, dpb+28(%rip)
	jmp	.LBB26_31
.LBB26_26:
	cmpl	$0, 316848(%r14)
	je	.LBB26_35
# BB#27:
	cmpl	$0, 317040(%r14)
	je	.LBB26_35
# BB#28:
	movl	$0, 5860(%rax)
	movq	317088(%r14), %r13
	testq	%r13, %r13
	je	.LBB26_35
# BB#29:                                # %.lr.ph.i35
	movl	$1, %r12d
	jmp	.LBB26_103
.LBB26_30:
	callq	flush_dpb
.LBB26_31:
	movq	$0, dpb+56(%rip)
	callq	update_ref_list
	callq	update_ltref_list
	movl	$-2147483648, dpb+40(%rip) # imm = 0x80000000
	cmpl	$0, 317036(%r14)
	je	.LBB26_33
# BB#32:
	movl	$0, dpb+44(%rip)
	movl	$0, 316840(%r14)
	movl	$1, %eax
	jmp	.LBB26_34
.LBB26_33:
	movl	$-1, dpb+44(%rip)
	xorl	%eax, %eax
.LBB26_34:                              # %idr_memory_management.exit
	movl	%eax, 316844(%r14)
	movl	$pocs_in_dpb, %edi
	xorl	%esi, %esi
	movl	$400, %edx              # imm = 0x190
	callq	memset
.LBB26_35:                              # %adaptive_memory_management.exit
	movl	(%r14), %eax
	cmpl	$1, %eax
	je	.LBB26_37
# BB#36:
	cmpl	$2, %eax
	jne	.LBB26_45
.LBB26_37:
	movq	dpb+56(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB26_45
# BB#38:
	movl	20(%rdi), %ecx
	cmpl	316832(%r14), %ecx
	jne	.LBB26_45
# BB#39:
	movl	(%rdi), %ecx
	cmpl	$1, %eax
	jne	.LBB26_41
# BB#40:
	cmpl	$2, %ecx
	je	.LBB26_42
	jmp	.LBB26_45
.LBB26_41:
	cmpl	$1, %ecx
	jne	.LBB26_45
.LBB26_42:
	cmpl	$0, 316848(%r14)
	movl	12(%rdi), %eax
	je	.LBB26_44
# BB#43:
	testl	%eax, %eax
	jne	.LBB26_85
	jmp	.LBB26_45
.LBB26_44:                              # %.thread
	testl	%eax, %eax
	je	.LBB26_85
.LBB26_45:                              # %thread-pre-split
	cmpl	$0, 317028(%r14)
	je	.LBB26_50
.LBB26_46:
	movq	img(%rip), %rax
	movl	6068(%rax), %ecx
	movl	dpb+24(%rip), %r8d
	testl	%ecx, %ecx
	je	.LBB26_66
# BB#47:
	testl	%r8d, %r8d
	je	.LBB26_66
# BB#48:                                # %.lr.ph115
	movq	dpb(%rip), %rdx
	testb	$1, %r8b
	jne	.LBB26_58
# BB#49:
	xorl	%esi, %esi
	cmpl	$1, %r8d
	jne	.LBB26_61
	jmp	.LBB26_66
.LBB26_50:
	cmpl	$0, 316848(%r14)
	je	.LBB26_46
# BB#51:
	cmpl	$0, 317040(%r14)
	jne	.LBB26_46
# BB#52:
	movl	dpb+52(%rip), %eax
	subl	dpb+36(%rip), %eax
	cmpl	%eax, dpb+32(%rip)
	jne	.LBB26_272
# BB#53:
	movl	dpb+28(%rip), %eax
	testl	%eax, %eax
	je	.LBB26_272
# BB#54:                                # %.lr.ph.i37
	movq	dpb(%rip), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB26_55:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rdi
	cmpl	$0, 4(%rdi)
	je	.LBB26_57
# BB#56:                                #   in Loop: Header=BB26_55 Depth=1
	cmpl	$0, 8(%rdi)
	je	.LBB26_271
.LBB26_57:                              #   in Loop: Header=BB26_55 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jb	.LBB26_55
	jmp	.LBB26_272
.LBB26_58:
	movq	(%rdx), %rax
	cmpl	$0, 4(%rax)
	je	.LBB26_60
# BB#59:
	movl	$1, 44(%rax)
.LBB26_60:                              # %.prol.loopexit
	movl	$1, %esi
	cmpl	$1, %r8d
	je	.LBB26_66
	.p2align	4, 0x90
.LBB26_61:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rdi
	cmpl	$0, 4(%rdi)
	je	.LBB26_63
# BB#62:                                #   in Loop: Header=BB26_61 Depth=1
	movl	$1, 44(%rdi)
.LBB26_63:                              #   in Loop: Header=BB26_61 Depth=1
	movq	8(%rdx,%rsi,8), %rdi
	cmpl	$0, 4(%rdi)
	je	.LBB26_65
# BB#64:                                #   in Loop: Header=BB26_61 Depth=1
	movl	$1, 44(%rdi)
.LBB26_65:                              #   in Loop: Header=BB26_61 Depth=1
	addq	$2, %rsi
	cmpq	%r8, %rsi
	jb	.LBB26_61
.LBB26_66:                              # %.loopexit61
	movl	dpb+28(%rip), %edx
	cmpl	%r8d, %edx
	jne	.LBB26_278
# BB#67:
	testl	%ecx, %ecx
	je	.LBB26_69
# BB#68:
	movl	$2, %edi
	callq	conceal_non_ref_pics
	movl	dpb+28(%rip), %r8d
.LBB26_69:
	testl	%r8d, %r8d
	je	.LBB26_83
# BB#70:                                # %.lr.ph.i40
	movq	dpb(%rip), %rcx
	movl	%r8d, %eax
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB26_71:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rdx
	cmpl	$0, 36(%rdx)
	je	.LBB26_81
# BB#72:                                #   in Loop: Header=BB26_71 Depth=1
	cmpl	$0, 4(%rdx)
	jne	.LBB26_81
# BB#73:                                #   in Loop: Header=BB26_71 Depth=1
	movl	(%rdx), %esi
	cmpl	$3, %esi
	jne	.LBB26_75
# BB#74:                                #   in Loop: Header=BB26_71 Depth=1
	movq	48(%rdx), %rbp
	cmpl	$0, 316848(%rbp)
	jne	.LBB26_81
	jmp	.LBB26_76
.LBB26_75:                              #   in Loop: Header=BB26_71 Depth=1
	testb	$1, %sil
	je	.LBB26_78
.LBB26_76:                              # %.thread.i.i
                                        #   in Loop: Header=BB26_71 Depth=1
	movq	56(%rdx), %rbp
	testq	%rbp, %rbp
	je	.LBB26_78
# BB#77:                                #   in Loop: Header=BB26_71 Depth=1
	cmpl	$0, 316848(%rbp)
	jne	.LBB26_81
.LBB26_78:                              #   in Loop: Header=BB26_71 Depth=1
	testb	$2, %sil
	je	.LBB26_82
# BB#79:                                #   in Loop: Header=BB26_71 Depth=1
	movq	64(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB26_82
# BB#80:                                #   in Loop: Header=BB26_71 Depth=1
	cmpl	$0, 316848(%rdx)
	je	.LBB26_82
	.p2align	4, 0x90
.LBB26_81:                              # %is_used_for_reference.exit.thread.i
                                        #   in Loop: Header=BB26_71 Depth=1
	incq	%rdi
	cmpq	%rax, %rdi
	jb	.LBB26_71
	jmp	.LBB26_83
.LBB26_82:                              # %is_used_for_reference.exit.i
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	remove_frame_from_dpb
.LBB26_83:                              # %remove_unused_frame_from_dpb.exit
	movq	img(%rip), %rax
	cmpl	$0, 6068(%rax)
	je	.LBB26_277
# BB#84:
	movq	%r14, %rdi
	callq	sliding_window_poc_management
	jmp	.LBB26_277
.LBB26_85:
	movq	%r14, %rsi
	callq	insert_picture_in_dpb
	callq	update_ref_list
	callq	update_ltref_list
	movq	$0, dpb+56(%rip)
	jmp	.LBB26_307
.LBB26_262:                             # %.us-lcssa33.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movl	$0, 316848(%rbx)
	movl	$0, 316844(%rbx)
	movl	%ecx, 4(%rdi)
	andl	$1, %ebp
	movl	%ebp, 8(%rdi)
	cmpl	$3, (%rdi)
	jne	.LBB26_264
.LBB26_263:                             #   in Loop: Header=BB26_103 Depth=1
	movq	48(%rdi), %rax
	movl	$0, 316848(%rax)
	movq	dpb+16(%rip), %rax
	movq	(%rax,%rsi,8), %rax
	movq	48(%rax), %rax
	movl	$0, 316844(%rax)
	jmp	.LBB26_264
.LBB26_86:                              #   in Loop: Header=BB26_103 Depth=1
	movl	%r15d, 316840(%rdi)
	movl	%r15d, 32(%rax)
	movl	%r15d, 316836(%rdi)
	movl	$1, 316844(%rdi)
	movq	56(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB26_89
# BB#87:                                #   in Loop: Header=BB26_103 Depth=1
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB26_89
# BB#88:                                #   in Loop: Header=BB26_103 Depth=1
	movl	%r15d, 316840(%rdx)
	movl	%r15d, 316840(%rcx)
	movl	%r15d, 316836(%rcx)
	movl	%r15d, 316836(%rdx)
	movl	$1, 316844(%rdx)
	movl	$1, 316844(%rcx)
.LBB26_89:                              #   in Loop: Header=BB26_103 Depth=1
	movl	$3, 8(%rax)
	jmp	.LBB26_250
.LBB26_90:                              # %.us-lcssa10.us.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	callq	unmark_for_reference
	callq	update_ref_list
	jmp	.LBB26_265
.LBB26_91:                              #   in Loop: Header=BB26_103 Depth=1
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB26_97
# BB#92:                                #   in Loop: Header=BB26_103 Depth=1
	cmpl	%r15d, 32(%rax)
	je	.LBB26_98
# BB#93:                                #   in Loop: Header=BB26_103 Depth=1
	movl	$.Lstr.2, %edi
	callq	puts
	movq	dpb+8(%rip), %rax
	movq	(%rax,%r12,8), %rax
	movq	56(%rax), %rsi
	movl	8(%rax), %ecx
	jmp	.LBB26_98
.LBB26_94:                              #   in Loop: Header=BB26_103 Depth=1
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB26_99
# BB#95:                                #   in Loop: Header=BB26_103 Depth=1
	cmpl	%r15d, 32(%rax)
	je	.LBB26_100
# BB#96:                                #   in Loop: Header=BB26_103 Depth=1
	movl	$.Lstr.2, %edi
	callq	puts
	movq	dpb+8(%rip), %rax
	movq	(%rax,%r12,8), %rax
	movq	64(%rax), %rsi
	movl	8(%rax), %ecx
	jmp	.LBB26_100
.LBB26_97:                              #   in Loop: Header=BB26_103 Depth=1
	xorl	%ecx, %ecx
.LBB26_98:                              #   in Loop: Header=BB26_103 Depth=1
	movl	%r15d, 316840(%rsi)
	movl	%r15d, 32(%rax)
	leal	(%rbx,%r15,2), %edx
	movl	%edx, 316836(%rsi)
	movl	$1, 316844(%rsi)
	orl	$1, %ecx
	jmp	.LBB26_101
.LBB26_99:                              #   in Loop: Header=BB26_103 Depth=1
	xorl	%ecx, %ecx
.LBB26_100:                             #   in Loop: Header=BB26_103 Depth=1
	movl	%r15d, 316840(%rsi)
	movl	%r15d, 32(%rax)
	leal	(%rbx,%r15,2), %edx
	movl	%edx, 316836(%rsi)
	movl	$1, 316844(%rsi)
	orl	$2, %ecx
.LBB26_101:                             #   in Loop: Header=BB26_103 Depth=1
	movl	%ecx, 8(%rax)
	cmpl	$3, %ecx
	movl	$1, %r12d
	jne	.LBB26_250
# BB#102:                               #   in Loop: Header=BB26_103 Depth=1
	movq	48(%rax), %rax
	movl	$1, 316844(%rax)
	movl	%r15d, 316836(%rax)
	movl	%r15d, 316840(%rax)
	jmp	.LBB26_250
	.p2align	4, 0x90
.LBB26_103:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_220 Depth 2
                                        #     Child Loop BB26_175 Depth 2
                                        #     Child Loop BB26_178 Depth 2
                                        #     Child Loop BB26_158 Depth 2
                                        #     Child Loop BB26_147 Depth 2
                                        #     Child Loop BB26_242 Depth 2
                                        #     Child Loop BB26_197 Depth 2
                                        #     Child Loop BB26_213 Depth 2
                                        #     Child Loop BB26_121 Depth 2
                                        #     Child Loop BB26_128 Depth 2
                                        #     Child Loop BB26_111 Depth 2
                                        #     Child Loop BB26_235 Depth 2
	movl	(%r13), %eax
	cmpq	$6, %rax
	ja	.LBB26_107
# BB#104:                               #   in Loop: Header=BB26_103 Depth=1
	jmpq	*.LJTI26_0(,%rax,8)
.LBB26_105:                             #   in Loop: Header=BB26_103 Depth=1
	cmpq	$0, 24(%r13)
	je	.LBB26_265
# BB#106:                               #   in Loop: Header=BB26_103 Depth=1
	movl	$.L.str.23, %edi
	jmp	.LBB26_254
.LBB26_107:                             #   in Loop: Header=BB26_103 Depth=1
	movl	$.L.str.24, %edi
	jmp	.LBB26_254
.LBB26_108:                             #   in Loop: Header=BB26_103 Depth=1
	movl	(%r14), %edx
	movl	316824(%r14), %eax
	testl	%edx, %edx
	leal	1(%rax,%rax), %r8d
	cmovel	%eax, %r8d
	movl	dpb+32(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB26_261
# BB#109:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movl	4(%r13), %eax
	notl	%eax
	addl	%eax, %r8d
	testl	%edx, %edx
	movq	dpb+8(%rip), %rdx
	je	.LBB26_234
# BB#110:                               # %.lr.ph.split.i.i.preheader
                                        #   in Loop: Header=BB26_103 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB26_111:                             # %.lr.ph.split.i.i
                                        #   Parent Loop BB26_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rdi,8), %rsi
	movl	4(%rsi), %ebp
	movl	%ebp, %ebx
	andl	$1, %ebx
	je	.LBB26_114
# BB#112:                               #   in Loop: Header=BB26_111 Depth=2
	testb	$1, 8(%rsi)
	jne	.LBB26_114
# BB#113:                               #   in Loop: Header=BB26_111 Depth=2
	movq	56(%rsi), %rax
	cmpl	%r8d, 316832(%rax)
	je	.LBB26_257
.LBB26_114:                             #   in Loop: Header=BB26_111 Depth=2
	testb	$2, %bpl
	je	.LBB26_117
# BB#115:                               #   in Loop: Header=BB26_111 Depth=2
	testb	$2, 8(%rsi)
	jne	.LBB26_117
# BB#116:                               #   in Loop: Header=BB26_111 Depth=2
	movq	64(%rsi), %rax
	cmpl	%r8d, 316832(%rax)
	je	.LBB26_259
.LBB26_117:                             #   in Loop: Header=BB26_111 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jb	.LBB26_111
	jmp	.LBB26_261
.LBB26_118:                             #   in Loop: Header=BB26_103 Depth=1
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	je	.LBB26_264
# BB#119:                               # %.lr.ph.i37.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movl	8(%r13), %r8d
	cmpl	$0, (%r14)
	movq	dpb+16(%rip), %rdx
	je	.LBB26_128
# BB#120:                               # %.lr.ph.split.i41.i.preheader
                                        #   in Loop: Header=BB26_103 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB26_121:                             # %.lr.ph.split.i41.i
                                        #   Parent Loop BB26_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rsi,8), %rdi
	movl	4(%rdi), %ebp
	movl	%ebp, %ecx
	andl	$1, %ecx
	je	.LBB26_124
# BB#122:                               #   in Loop: Header=BB26_121 Depth=2
	movl	8(%rdi), %r9d
	testb	$1, %r9b
	je	.LBB26_124
# BB#123:                               #   in Loop: Header=BB26_121 Depth=2
	movq	56(%rdi), %rbx
	cmpl	%r8d, 316836(%rbx)
	je	.LBB26_258
.LBB26_124:                             #   in Loop: Header=BB26_121 Depth=2
	testb	$2, %bpl
	je	.LBB26_127
# BB#125:                               #   in Loop: Header=BB26_121 Depth=2
	movl	8(%rdi), %ebp
	testb	$2, %bpl
	je	.LBB26_127
# BB#126:                               #   in Loop: Header=BB26_121 Depth=2
	movq	64(%rdi), %rbx
	cmpl	%r8d, 316836(%rbx)
	je	.LBB26_262
.LBB26_127:                             #   in Loop: Header=BB26_121 Depth=2
	incq	%rsi
	cmpq	%rax, %rsi
	jb	.LBB26_121
	jmp	.LBB26_264
	.p2align	4, 0x90
.LBB26_128:                             # %.lr.ph.split.us.i39.i
                                        #   Parent Loop BB26_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rsi
	cmpl	$3, 4(%rsi)
	jne	.LBB26_143
# BB#129:                               #   in Loop: Header=BB26_128 Depth=2
	cmpl	$3, 8(%rsi)
	jne	.LBB26_143
# BB#130:                               #   in Loop: Header=BB26_128 Depth=2
	movq	48(%rsi), %rdi
	cmpl	%r8d, 316836(%rdi)
	jne	.LBB26_143
# BB#131:                               #   in Loop: Header=BB26_128 Depth=2
	movl	(%rsi), %ebp
	testb	$1, %bpl
	je	.LBB26_134
# BB#132:                               #   in Loop: Header=BB26_128 Depth=2
	movq	56(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB26_134
# BB#133:                               #   in Loop: Header=BB26_128 Depth=2
	movq	$0, 316844(%rcx)
.LBB26_134:                             #   in Loop: Header=BB26_128 Depth=2
	testb	$2, %bpl
	je	.LBB26_137
# BB#135:                               #   in Loop: Header=BB26_128 Depth=2
	movq	64(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB26_137
# BB#136:                               #   in Loop: Header=BB26_128 Depth=2
	movq	$0, 316844(%rcx)
.LBB26_137:                             # %thread-pre-split.i.us.i.i
                                        #   in Loop: Header=BB26_128 Depth=2
	cmpl	$3, %ebp
	jne	.LBB26_142
# BB#138:                               #   in Loop: Header=BB26_128 Depth=2
	movq	56(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB26_141
# BB#139:                               #   in Loop: Header=BB26_128 Depth=2
	movq	64(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB26_141
# BB#140:                               #   in Loop: Header=BB26_128 Depth=2
	movl	$0, 316848(%rcx)
	movl	$0, 316844(%rcx)
	movq	$0, 316844(%rbp)
.LBB26_141:                             #   in Loop: Header=BB26_128 Depth=2
	movq	$0, 316844(%rdi)
.LBB26_142:                             # %unmark_for_long_term_reference.exit.us.i.i
                                        #   in Loop: Header=BB26_128 Depth=2
	movq	$0, 4(%rsi)
	.p2align	4, 0x90
.LBB26_143:                             #   in Loop: Header=BB26_128 Depth=2
	addq	$8, %rdx
	decq	%rax
	jne	.LBB26_128
	jmp	.LBB26_264
.LBB26_144:                             #   in Loop: Header=BB26_103 Depth=1
	movl	4(%r13), %ebp
	movl	12(%r13), %r15d
	movl	316824(%r14), %eax
	movl	(%r14), %ecx
	cmpl	$0, %ecx
	leal	1(%rax,%rax), %edx
	cmovel	%eax, %edx
	notl	%ebp
	addl	%edx, %ebp
	cmpl	$0, %ecx
	je	.LBB26_195
# BB#145:                               # %.preheader.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movl	dpb+32(%rip), %eax
	testq	%rax, %rax
	je	.LBB26_152
# BB#146:                               # %.lr.ph.i45.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movq	dpb+8(%rip), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB26_147:                             #   Parent Loop BB26_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdx,8), %rsi
	movl	4(%rsi), %edi
	testb	$1, %dil
	je	.LBB26_149
# BB#148:                               #   in Loop: Header=BB26_147 Depth=2
	movq	56(%rsi), %rbx
	cmpl	%ebp, 316832(%rbx)
	je	.LBB26_255
.LBB26_149:                             #   in Loop: Header=BB26_147 Depth=2
	testb	$2, %dil
	je	.LBB26_151
# BB#150:                               #   in Loop: Header=BB26_147 Depth=2
	movq	64(%rsi), %rsi
	cmpl	%ebp, 316832(%rsi)
	je	.LBB26_256
.LBB26_151:                             #   in Loop: Header=BB26_147 Depth=2
	incq	%rdx
	cmpq	%rax, %rdx
	jb	.LBB26_147
.LBB26_152:                             # %._crit_edge.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movl	$.L.str.25, %edi
	movl	$200, %esi
	callq	error
	xorl	%edi, %edi
.LBB26_153:                             # %unmark_long_term_frame_for_reference_by_frame_idx.exit.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	%r15d, %esi
	movl	%ebp, %r8d
	callq	unmark_long_term_field_for_reference_by_frame_idx
	movl	(%r14), %eax
	cmpl	$1, %eax
	je	.LBB26_239
# BB#154:                               # %unmark_long_term_frame_for_reference_by_frame_idx.exit.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	testl	%eax, %eax
	je	.LBB26_211
# BB#155:                               #   in Loop: Header=BB26_103 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB26_240
.LBB26_156:                             #   in Loop: Header=BB26_103 Depth=1
	movl	16(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, dpb+44(%rip)
	movl	dpb+36(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB26_264
# BB#157:                               # %.lr.ph.i48.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movq	dpb+16(%rip), %rdx
	.p2align	4, 0x90
.LBB26_158:                             #   Parent Loop BB26_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rsi
	cmpl	%eax, 32(%rsi)
	jl	.LBB26_171
# BB#159:                               #   in Loop: Header=BB26_158 Depth=2
	movl	(%rsi), %edi
	testb	$1, %dil
	je	.LBB26_162
# BB#160:                               #   in Loop: Header=BB26_158 Depth=2
	movq	56(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB26_162
# BB#161:                               #   in Loop: Header=BB26_158 Depth=2
	movq	$0, 316844(%rbp)
.LBB26_162:                             #   in Loop: Header=BB26_158 Depth=2
	testb	$2, %dil
	je	.LBB26_165
# BB#163:                               #   in Loop: Header=BB26_158 Depth=2
	movq	64(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB26_165
# BB#164:                               #   in Loop: Header=BB26_158 Depth=2
	movq	$0, 316844(%rbp)
.LBB26_165:                             # %thread-pre-split.i.i.i
                                        #   in Loop: Header=BB26_158 Depth=2
	cmpl	$3, %edi
	jne	.LBB26_170
# BB#166:                               #   in Loop: Header=BB26_158 Depth=2
	movq	56(%rsi), %rdi
	testq	%rdi, %rdi
	je	.LBB26_169
# BB#167:                               #   in Loop: Header=BB26_158 Depth=2
	movq	64(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB26_169
# BB#168:                               #   in Loop: Header=BB26_158 Depth=2
	movl	$0, 316848(%rdi)
	movl	$0, 316844(%rdi)
	movq	$0, 316844(%rbp)
.LBB26_169:                             #   in Loop: Header=BB26_158 Depth=2
	movq	48(%rsi), %rdi
	movl	$0, 316848(%rdi)
	movl	$0, 316844(%rdi)
.LBB26_170:                             # %unmark_for_long_term_reference.exit.i.i
                                        #   in Loop: Header=BB26_158 Depth=2
	movq	$0, 4(%rsi)
.LBB26_171:                             #   in Loop: Header=BB26_158 Depth=2
	addq	$8, %rdx
	decq	%rcx
	jne	.LBB26_158
	jmp	.LBB26_264
.LBB26_173:                             #   in Loop: Header=BB26_103 Depth=1
	cmpl	$0, dpb+32(%rip)
	je	.LBB26_176
# BB#174:                               # %.lr.ph.i52.i.preheader
                                        #   in Loop: Header=BB26_103 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB26_175:                             # %.lr.ph.i52.i
                                        #   Parent Loop BB26_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	dpb+8(%rip), %rax
	movl	%ebx, %ecx
	movq	(%rax,%rcx,8), %rdi
	callq	unmark_for_reference
	incl	%ebx
	cmpl	dpb+32(%rip), %ebx
	jb	.LBB26_175
.LBB26_176:                             # %mm_unmark_all_short_term_for_reference.exit.i
                                        #   in Loop: Header=BB26_103 Depth=1
	callq	update_ref_list
	movl	$-1, dpb+44(%rip)
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	je	.LBB26_192
# BB#177:                               # %.lr.ph.i.i54.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movq	dpb+16(%rip), %rcx
	.p2align	4, 0x90
.LBB26_178:                             #   Parent Loop BB26_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	cmpl	$0, 32(%rdx)
	js	.LBB26_191
# BB#179:                               #   in Loop: Header=BB26_178 Depth=2
	movl	(%rdx), %esi
	testb	$1, %sil
	je	.LBB26_182
# BB#180:                               #   in Loop: Header=BB26_178 Depth=2
	movq	56(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_182
# BB#181:                               #   in Loop: Header=BB26_178 Depth=2
	movq	$0, 316844(%rdi)
.LBB26_182:                             #   in Loop: Header=BB26_178 Depth=2
	testb	$2, %sil
	je	.LBB26_185
# BB#183:                               #   in Loop: Header=BB26_178 Depth=2
	movq	64(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_185
# BB#184:                               #   in Loop: Header=BB26_178 Depth=2
	movq	$0, 316844(%rdi)
.LBB26_185:                             # %thread-pre-split.i.i.i56.i
                                        #   in Loop: Header=BB26_178 Depth=2
	cmpl	$3, %esi
	jne	.LBB26_190
# BB#186:                               #   in Loop: Header=BB26_178 Depth=2
	movq	56(%rdx), %rsi
	testq	%rsi, %rsi
	je	.LBB26_189
# BB#187:                               #   in Loop: Header=BB26_178 Depth=2
	movq	64(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_189
# BB#188:                               #   in Loop: Header=BB26_178 Depth=2
	movl	$0, 316848(%rsi)
	movl	$0, 316844(%rsi)
	movq	$0, 316844(%rdi)
.LBB26_189:                             #   in Loop: Header=BB26_178 Depth=2
	movq	48(%rdx), %rsi
	movl	$0, 316848(%rsi)
	movl	$0, 316844(%rsi)
.LBB26_190:                             # %unmark_for_long_term_reference.exit.i.i57.i
                                        #   in Loop: Header=BB26_178 Depth=2
	movq	$0, 4(%rdx)
.LBB26_191:                             #   in Loop: Header=BB26_178 Depth=2
	addq	$8, %rcx
	decq	%rax
	jne	.LBB26_178
.LBB26_192:                             # %mm_unmark_all_long_term_for_reference.exit.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movq	img(%rip), %rax
	movl	$1, 5860(%rax)
	jmp	.LBB26_265
.LBB26_193:                             #   in Loop: Header=BB26_103 Depth=1
	movl	12(%r13), %ebp
	movl	(%r14), %edi
	testl	%edi, %edi
	je	.LBB26_218
# BB#194:                               #   in Loop: Header=BB26_103 Depth=1
	movl	316832(%r14), %ecx
	movl	$1, %edx
	xorl	%r8d, %r8d
	movl	%ebp, %esi
	callq	unmark_long_term_field_for_reference_by_frame_idx
	movl	dpb+36(%rip), %eax
	jmp	.LBB26_252
.LBB26_195:                             #   in Loop: Header=BB26_103 Depth=1
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	je	.LBB26_211
# BB#196:                               # %.lr.ph.i27.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movq	dpb+16(%rip), %rcx
	.p2align	4, 0x90
.LBB26_197:                             #   Parent Loop BB26_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	cmpl	%r15d, 32(%rdx)
	jne	.LBB26_210
# BB#198:                               #   in Loop: Header=BB26_197 Depth=2
	movl	(%rdx), %esi
	testb	$1, %sil
	je	.LBB26_201
# BB#199:                               #   in Loop: Header=BB26_197 Depth=2
	movq	56(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_201
# BB#200:                               #   in Loop: Header=BB26_197 Depth=2
	movq	$0, 316844(%rdi)
.LBB26_201:                             #   in Loop: Header=BB26_197 Depth=2
	testb	$2, %sil
	je	.LBB26_204
# BB#202:                               #   in Loop: Header=BB26_197 Depth=2
	movq	64(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_204
# BB#203:                               #   in Loop: Header=BB26_197 Depth=2
	movq	$0, 316844(%rdi)
.LBB26_204:                             # %thread-pre-split.i.i.i.i
                                        #   in Loop: Header=BB26_197 Depth=2
	cmpl	$3, %esi
	jne	.LBB26_209
# BB#205:                               #   in Loop: Header=BB26_197 Depth=2
	movq	56(%rdx), %rsi
	testq	%rsi, %rsi
	je	.LBB26_208
# BB#206:                               #   in Loop: Header=BB26_197 Depth=2
	movq	64(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_208
# BB#207:                               #   in Loop: Header=BB26_197 Depth=2
	movl	$0, 316848(%rsi)
	movl	$0, 316844(%rsi)
	movq	$0, 316844(%rdi)
.LBB26_208:                             #   in Loop: Header=BB26_197 Depth=2
	movq	48(%rdx), %rsi
	movl	$0, 316848(%rsi)
	movl	$0, 316844(%rsi)
.LBB26_209:                             # %unmark_for_long_term_reference.exit.i.i.i
                                        #   in Loop: Header=BB26_197 Depth=2
	movq	$0, 4(%rdx)
.LBB26_210:                             #   in Loop: Header=BB26_197 Depth=2
	addq	$8, %rcx
	decq	%rax
	jne	.LBB26_197
.LBB26_211:                             # %.preheader.i.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movl	dpb+32(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB26_217
# BB#212:                               # %.lr.ph9.i.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movq	dpb+8(%rip), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB26_213:                             #   Parent Loop BB26_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rsi,8), %rax
	cmpl	$3, 4(%rax)
	jne	.LBB26_216
# BB#214:                               #   in Loop: Header=BB26_213 Depth=2
	movq	48(%rax), %rdi
	cmpl	$0, 316844(%rdi)
	jne	.LBB26_216
# BB#215:                               #   in Loop: Header=BB26_213 Depth=2
	cmpl	%ebp, 316832(%rdi)
	je	.LBB26_86
	.p2align	4, 0x90
.LBB26_216:                             #   in Loop: Header=BB26_213 Depth=2
	incq	%rsi
	cmpq	%rcx, %rsi
	jb	.LBB26_213
.LBB26_217:                             # %._crit_edge10.i.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movl	$.Lstr.3, %edi
	callq	puts
	jmp	.LBB26_250
.LBB26_218:                             #   in Loop: Header=BB26_103 Depth=1
	movl	dpb+36(%rip), %eax
	testq	%rax, %rax
	je	.LBB26_251
# BB#219:                               # %.lr.ph.i.i60.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movq	dpb+16(%rip), %rcx
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB26_220:                             #   Parent Loop BB26_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rsi
	cmpl	%ebp, 32(%rsi)
	jne	.LBB26_233
# BB#221:                               #   in Loop: Header=BB26_220 Depth=2
	movl	(%rsi), %edi
	testb	$1, %dil
	je	.LBB26_224
# BB#222:                               #   in Loop: Header=BB26_220 Depth=2
	movq	56(%rsi), %rbx
	testq	%rbx, %rbx
	je	.LBB26_224
# BB#223:                               #   in Loop: Header=BB26_220 Depth=2
	movq	$0, 316844(%rbx)
.LBB26_224:                             #   in Loop: Header=BB26_220 Depth=2
	testb	$2, %dil
	je	.LBB26_227
# BB#225:                               #   in Loop: Header=BB26_220 Depth=2
	movq	64(%rsi), %rbx
	testq	%rbx, %rbx
	je	.LBB26_227
# BB#226:                               #   in Loop: Header=BB26_220 Depth=2
	movq	$0, 316844(%rbx)
.LBB26_227:                             # %thread-pre-split.i.i.i62.i
                                        #   in Loop: Header=BB26_220 Depth=2
	cmpl	$3, %edi
	jne	.LBB26_232
# BB#228:                               #   in Loop: Header=BB26_220 Depth=2
	movq	56(%rsi), %rdi
	testq	%rdi, %rdi
	je	.LBB26_231
# BB#229:                               #   in Loop: Header=BB26_220 Depth=2
	movq	64(%rsi), %rbx
	testq	%rbx, %rbx
	je	.LBB26_231
# BB#230:                               #   in Loop: Header=BB26_220 Depth=2
	movl	$0, 316848(%rdi)
	movl	$0, 316844(%rdi)
	movq	$0, 316844(%rbx)
.LBB26_231:                             #   in Loop: Header=BB26_220 Depth=2
	movq	48(%rsi), %rdi
	movl	$0, 316848(%rdi)
	movl	$0, 316844(%rdi)
.LBB26_232:                             # %unmark_for_long_term_reference.exit.i.i63.i
                                        #   in Loop: Header=BB26_220 Depth=2
	movq	$0, 4(%rsi)
.LBB26_233:                             #   in Loop: Header=BB26_220 Depth=2
	addq	$8, %rcx
	decq	%rdx
	jne	.LBB26_220
	jmp	.LBB26_252
.LBB26_234:                             # %.lr.ph.split.us.i.i.preheader
                                        #   in Loop: Header=BB26_103 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB26_235:                             # %.lr.ph.split.us.i.i
                                        #   Parent Loop BB26_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rsi,8), %rdi
	cmpl	$3, 4(%rdi)
	jne	.LBB26_238
# BB#236:                               #   in Loop: Header=BB26_235 Depth=2
	cmpl	$0, 8(%rdi)
	jne	.LBB26_238
# BB#237:                               #   in Loop: Header=BB26_235 Depth=2
	movq	48(%rdi), %rax
	cmpl	%r8d, 316832(%rax)
	je	.LBB26_90
	.p2align	4, 0x90
.LBB26_238:                             #   in Loop: Header=BB26_235 Depth=2
	incq	%rsi
	cmpq	%rcx, %rsi
	jb	.LBB26_235
	jmp	.LBB26_261
.LBB26_239:                             #   in Loop: Header=BB26_103 Depth=1
	movl	$1, %ebx
.LBB26_240:                             #   in Loop: Header=BB26_103 Depth=1
	movl	dpb+32(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB26_249
# BB#241:                               # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movq	dpb+8(%rip), %rdx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB26_242:                             #   Parent Loop BB26_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%r12,8), %rax
	movl	4(%rax), %edi
	testb	$1, %dil
	je	.LBB26_245
# BB#243:                               #   in Loop: Header=BB26_242 Depth=2
	movq	56(%rax), %rsi
	cmpl	$0, 316844(%rsi)
	jne	.LBB26_245
# BB#244:                               #   in Loop: Header=BB26_242 Depth=2
	cmpl	%ebp, 316832(%rsi)
	je	.LBB26_91
	.p2align	4, 0x90
.LBB26_245:                             #   in Loop: Header=BB26_242 Depth=2
	testb	$2, %dil
	je	.LBB26_248
# BB#246:                               #   in Loop: Header=BB26_242 Depth=2
	movq	64(%rax), %rsi
	cmpl	$0, 316844(%rsi)
	jne	.LBB26_248
# BB#247:                               #   in Loop: Header=BB26_242 Depth=2
	cmpl	%ebp, 316832(%rsi)
	je	.LBB26_94
	.p2align	4, 0x90
.LBB26_248:                             #   in Loop: Header=BB26_242 Depth=2
	incq	%r12
	cmpq	%rcx, %r12
	jb	.LBB26_242
.LBB26_249:                             # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movl	$.Lstr, %edi
	callq	puts
	movl	$1, %r12d
.LBB26_250:                             # %mm_assign_long_term_frame_idx.exit.i
                                        #   in Loop: Header=BB26_103 Depth=1
	callq	update_ref_list
.LBB26_264:                             # %mm_unmark_long_term_for_reference.exit.i
                                        #   in Loop: Header=BB26_103 Depth=1
	callq	update_ltref_list
	jmp	.LBB26_265
.LBB26_251:                             #   in Loop: Header=BB26_103 Depth=1
	xorl	%eax, %eax
.LBB26_252:                             # %mm_mark_current_picture_long_term.exit.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movl	$1, 316844(%r14)
	movl	%ebp, 316840(%r14)
	addl	dpb+32(%rip), %eax
	movl	dpb+52(%rip), %ecx
	testl	%ecx, %ecx
	cmovlel	%r12d, %ecx
	cmpl	%ecx, %eax
	jle	.LBB26_265
# BB#253:                               #   in Loop: Header=BB26_103 Depth=1
	movl	$.L.str.2, %edi
.LBB26_254:                             # %check_num_ref.exit.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	jmp	.LBB26_265
.LBB26_255:                             #   in Loop: Header=BB26_103 Depth=1
	movl	$1, %edi
	jmp	.LBB26_153
.LBB26_256:                             #   in Loop: Header=BB26_103 Depth=1
	movl	$2, %edi
	jmp	.LBB26_153
.LBB26_257:                             # %.us-lcssa.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movl	$0, 316848(%rax)
	andl	$2, %ebp
	movl	%ebp, 4(%rsi)
	cmpl	$3, (%rsi)
	je	.LBB26_260
	jmp	.LBB26_261
.LBB26_258:                             # %.us-lcssa.i42.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movl	$0, 316848(%rbx)
	movl	$0, 316844(%rbx)
	andl	$2, %ebp
	movl	%ebp, 4(%rdi)
	andl	$2, %r9d
	movl	%r9d, 8(%rdi)
	cmpl	$3, (%rdi)
	je	.LBB26_263
	jmp	.LBB26_264
.LBB26_259:                             # %.us-lcssa9.i.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movl	$0, 316848(%rax)
	movl	%ebx, 4(%rsi)
	cmpl	$3, (%rsi)
	jne	.LBB26_261
.LBB26_260:                             #   in Loop: Header=BB26_103 Depth=1
	movq	48(%rsi), %rax
	movl	$0, 316848(%rax)
.LBB26_261:                             # %mm_unmark_short_term_for_reference.exit.i
                                        #   in Loop: Header=BB26_103 Depth=1
	callq	update_ref_list
	.p2align	4, 0x90
.LBB26_265:                             # %check_num_ref.exit.i
                                        #   in Loop: Header=BB26_103 Depth=1
	movq	24(%r13), %rax
	movq	%rax, 317088(%r14)
	movq	%r13, %rdi
	callq	free
	movq	317088(%r14), %r13
	testq	%r13, %r13
	jne	.LBB26_103
# BB#266:                               # %._crit_edge.i36
	movq	img(%rip), %rax
	cmpl	$0, 5860(%rax)
	je	.LBB26_35
# BB#267:
	movl	$0, 316824(%r14)
	movl	$0, 316832(%r14)
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	je	.LBB26_273
# BB#268:
	cmpl	$1, %ecx
	je	.LBB26_274
# BB#269:
	cmpl	$2, %ecx
	jne	.LBB26_275
# BB#270:
	movl	$0, 5668(%rax)
	movl	$0, 12(%r14)
	movl	$0, 4(%r14)
	xorl	%ecx, %ecx
	jmp	.LBB26_276
.LBB26_271:
	callq	unmark_for_reference
	callq	update_ref_list
.LBB26_272:                             # %sliding_window_memory_management.exit
	movl	$0, 316844(%r14)
	jmp	.LBB26_46
.LBB26_273:
	movl	4(%r14), %edx
	movl	8(%r14), %esi
	subl	%edx, %esi
	movl	%esi, 8(%r14)
	movl	12(%r14), %ecx
	subl	%edx, %ecx
	movl	%ecx, 12(%r14)
	movl	%esi, 5664(%rax)
	movl	%ecx, 5668(%rax)
	cmpl	%ecx, %esi
	cmovlel	%esi, %ecx
	movl	%ecx, 4(%r14)
	movl	%ecx, 5672(%rax)
	jmp	.LBB26_276
.LBB26_274:
	movl	$0, 5664(%rax)
	movq	$0, 4(%r14)
	xorl	%ecx, %ecx
	jmp	.LBB26_276
.LBB26_275:                             # %._crit_edge166.i
	movl	4(%r14), %ecx
.LBB26_276:
	movl	%ecx, 5752(%rax)
	callq	flush_dpb
	jmp	.LBB26_35
.LBB26_277:                             # %thread-pre-split58
	movl	dpb+28(%rip), %edx
	movl	dpb+24(%rip), %r8d
.LBB26_278:
	movl	316848(%r14), %ecx
	cmpl	%r8d, %edx
	jne	.LBB26_281
# BB#279:
	testl	%ecx, %ecx
	je	.LBB26_293
.LBB26_280:
	callq	output_one_frame_from_dpb
	jmp	.LBB26_277
.LBB26_281:
	testl	%ecx, %ecx
	je	.LBB26_289
# BB#282:
	cmpl	$0, 316844(%r14)
	jne	.LBB26_289
# BB#283:
	movl	dpb+32(%rip), %eax
	testl	%eax, %eax
	je	.LBB26_289
# BB#284:                               # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB26_285:                             # =>This Inner Loop Header: Depth=1
	movq	dpb+8(%rip), %rcx
	movl	%ebx, %edx
	movq	(%rcx,%rdx,8), %rcx
	movl	20(%rcx), %ecx
	cmpl	316824(%r14), %ecx
	jne	.LBB26_287
# BB#286:                               #   in Loop: Header=BB26_285 Depth=1
	movl	$.L.str.15, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movl	dpb+32(%rip), %eax
.LBB26_287:                             #   in Loop: Header=BB26_285 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jb	.LBB26_285
# BB#288:                               # %.loopexit.loopexit
	movl	dpb+28(%rip), %edx
.LBB26_289:                             # %.loopexit
	movq	dpb(%rip), %rax
	movl	%edx, %ecx
	movq	(%rax,%rcx,8), %rdi
	movq	%r14, %rsi
	callq	insert_picture_in_dpb
	cmpl	$0, 317028(%r14)
	je	.LBB26_291
# BB#290:
	movq	img(%rip), %rax
	movl	$0, 6072(%rax)
.LBB26_291:
	cmpl	$0, (%r14)
	je	.LBB26_302
# BB#292:
	movq	dpb(%rip), %rcx
	movl	dpb+28(%rip), %eax
	movq	(%rcx,%rax,8), %rcx
	jmp	.LBB26_303
.LBB26_293:
	testl	%r8d, %r8d
	jne	.LBB26_295
# BB#294:
	movl	$.L.str.29, %edi
	movl	$150, %esi
	callq	error
	movl	dpb+28(%rip), %r8d
	testl	%r8d, %r8d
	je	.LBB26_301
.LBB26_295:                             # %.lr.ph.i43
	xorl	%esi, %esi
	movl	$-1, %edx
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	movq	dpb(%rip), %rdi
	movl	%r8d, %ebp
	.p2align	4, 0x90
.LBB26_296:                             # %._crit_edge12.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, %eax
	movq	(%rdi,%rax,8), %rax
	movl	40(%rax), %ebx
	cmpl	%ebx, %ecx
	jle	.LBB26_298
# BB#297:                               #   in Loop: Header=BB26_296 Depth=1
	cmpl	$0, 36(%rax)
	cmovel	%ebx, %ecx
	cmovel	%esi, %edx
	cmovel	%r8d, %ebp
.LBB26_298:                             #   in Loop: Header=BB26_296 Depth=1
	incl	%esi
	cmpl	%ebp, %esi
	jb	.LBB26_296
# BB#299:                               # %get_smallest_poc.exit
	cmpl	$-1, %edx
	je	.LBB26_301
# BB#300:
	cmpl	%ecx, 4(%r14)
	jge	.LBB26_280
.LBB26_301:                             # %get_smallest_poc.exit.thread
	movl	p_out(%rip), %esi
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	direct_output           # TAILCALL
.LBB26_302:                             # %._crit_edge
	movl	dpb+28(%rip), %eax
	xorl	%ecx, %ecx
.LBB26_303:
	movq	%rcx, dpb+56(%rip)
	leal	1(%rax), %ecx
	movl	%ecx, dpb+28(%rip)
	movq	img(%rip), %rcx
	cmpl	$0, 6068(%rcx)
	je	.LBB26_305
# BB#304:
	movl	4(%r14), %ecx
	movl	%eax, %eax
	movl	%ecx, pocs_in_dpb(,%rax,4)
.LBB26_305:
	callq	update_ref_list
	callq	update_ltref_list
	movl	dpb+32(%rip), %eax
	addl	dpb+36(%rip), %eax
	movl	dpb+52(%rip), %ecx
	testl	%ecx, %ecx
	movl	$1, %edx
	cmovgl	%ecx, %edx
	cmpl	%edx, %eax
	jle	.LBB26_307
# BB#306:
	movl	$.L.str.2, %edi
	movl	$500, %esi              # imm = 0x1F4
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.LBB26_307:                             # %check_num_ref.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	store_picture_in_dpb, .Lfunc_end26-store_picture_in_dpb
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI26_0:
	.quad	.LBB26_105
	.quad	.LBB26_108
	.quad	.LBB26_118
	.quad	.LBB26_144
	.quad	.LBB26_156
	.quad	.LBB26_173
	.quad	.LBB26_193

	.text
	.p2align	4, 0x90
	.type	insert_picture_in_dpb,@function
insert_picture_in_dpb:                  # @insert_picture_in_dpb
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi86:
	.cfi_def_cfa_offset 32
.Lcfi87:
	.cfi_offset %rbx, -24
.Lcfi88:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	(%r14), %eax
	testl	%eax, %eax
	je	.LBB27_9
# BB#1:
	cmpl	$1, %eax
	je	.LBB27_13
# BB#2:
	cmpl	$2, %eax
	jne	.LBB27_17
# BB#3:
	movq	%r14, 64(%rbx)
	movl	(%rbx), %eax
	orl	$2, %eax
	movl	%eax, (%rbx)
	cmpl	$0, 316848(%r14)
	je	.LBB27_7
# BB#4:
	orb	$2, 4(%rbx)
	orb	$2, 12(%rbx)
	cmpl	$0, 316844(%r14)
	je	.LBB27_7
# BB#5:
	orb	$2, 8(%rbx)
	jmp	.LBB27_6
.LBB27_9:
	movq	%r14, 48(%rbx)
	movl	$3, (%rbx)
	cmpl	$0, 316848(%r14)
	je	.LBB27_12
# BB#10:
	movl	$3, 4(%rbx)
	movl	$3, 12(%rbx)
	cmpl	$0, 316844(%r14)
	je	.LBB27_12
# BB#11:
	movl	$3, 8(%rbx)
	movl	316840(%r14), %eax
	movl	%eax, 32(%rbx)
.LBB27_12:
	movq	%rbx, %rdi
	callq	dpb_split_field
	jmp	.LBB27_17
.LBB27_13:
	movq	%r14, 56(%rbx)
	movl	(%rbx), %eax
	orl	$1, %eax
	movl	%eax, (%rbx)
	cmpl	$0, 316848(%r14)
	je	.LBB27_7
# BB#14:
	orb	$1, 4(%rbx)
	orb	$1, 12(%rbx)
	cmpl	$0, 316844(%r14)
	je	.LBB27_7
# BB#15:
	orb	$1, 8(%rbx)
.LBB27_6:                               # %thread-pre-split51
	movl	316840(%r14), %ecx
	movl	%ecx, 32(%rbx)
.LBB27_7:                               # %thread-pre-split51
	cmpl	$3, %eax
	jne	.LBB27_8
# BB#16:
	movq	%rbx, %rdi
	callq	dpb_combine_field
	jmp	.LBB27_17
.LBB27_8:
	movl	4(%r14), %eax
	movl	%eax, 40(%rbx)
	movq	%r14, %rdi
	callq	gen_field_ref_ids
.LBB27_17:
	movl	316832(%r14), %eax
	movl	%eax, 20(%rbx)
	movl	316828(%r14), %eax
	movl	%eax, 24(%rbx)
	movl	316852(%r14), %eax
	movl	%eax, 36(%rbx)
	cmpl	$3, (%rbx)
	jne	.LBB27_20
# BB#18:
	movl	p_ref(%rip), %edx
	cmpl	$-1, %edx
	je	.LBB27_20
# BB#19:
	movq	input(%rip), %rax
	cmpl	$0, 3016(%rax)
	je	.LBB27_21
.LBB27_20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB27_21:
	movq	snr(%rip), %rdi
	movq	48(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	find_snr                # TAILCALL
.Lfunc_end27:
	.size	insert_picture_in_dpb, .Lfunc_end27-insert_picture_in_dpb
	.cfi_endproc

	.globl	flush_dpb
	.p2align	4, 0x90
	.type	flush_dpb,@function
flush_dpb:                              # @flush_dpb
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 16
.Lcfi90:
	.cfi_offset %rbx, -16
	movq	img(%rip), %rax
	cmpl	$0, 6068(%rax)
	je	.LBB28_2
# BB#1:
	xorl	%edi, %edi
	callq	conceal_non_ref_pics
.LBB28_2:                               # %.preheader5
	cmpl	$0, dpb+28(%rip)
	je	.LBB28_22
# BB#3:                                 # %.lr.ph8.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB28_4:                               # %.lr.ph8
                                        # =>This Inner Loop Header: Depth=1
	movq	dpb(%rip), %rax
	movl	%ebx, %ecx
	movq	(%rax,%rcx,8), %rdi
	callq	unmark_for_reference
	incl	%ebx
	movl	dpb+28(%rip), %eax
	cmpl	%eax, %ebx
	jb	.LBB28_4
# BB#5:                                 # %.preheader
	testl	%eax, %eax
	je	.LBB28_6
.LBB28_8:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_9 Depth 2
	movq	dpb(%rip), %r8
	movl	%eax, %edx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB28_9:                               #   Parent Loop BB28_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r8,%rdi,8), %rsi
	cmpl	$0, 36(%rsi)
	je	.LBB28_19
# BB#10:                                #   in Loop: Header=BB28_9 Depth=2
	cmpl	$0, 4(%rsi)
	jne	.LBB28_19
# BB#11:                                #   in Loop: Header=BB28_9 Depth=2
	movl	(%rsi), %ecx
	cmpl	$3, %ecx
	jne	.LBB28_13
# BB#12:                                #   in Loop: Header=BB28_9 Depth=2
	movq	48(%rsi), %rbx
	cmpl	$0, 316848(%rbx)
	jne	.LBB28_19
	jmp	.LBB28_14
.LBB28_13:                              #   in Loop: Header=BB28_9 Depth=2
	testb	$1, %cl
	je	.LBB28_16
.LBB28_14:                              # %.thread.i.i
                                        #   in Loop: Header=BB28_9 Depth=2
	movq	56(%rsi), %rbx
	testq	%rbx, %rbx
	je	.LBB28_16
# BB#15:                                #   in Loop: Header=BB28_9 Depth=2
	cmpl	$0, 316848(%rbx)
	jne	.LBB28_19
.LBB28_16:                              #   in Loop: Header=BB28_9 Depth=2
	testb	$2, %cl
	je	.LBB28_7
# BB#17:                                #   in Loop: Header=BB28_9 Depth=2
	movq	64(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB28_7
# BB#18:                                #   in Loop: Header=BB28_9 Depth=2
	cmpl	$0, 316848(%rcx)
	je	.LBB28_7
	.p2align	4, 0x90
.LBB28_19:                              # %is_used_for_reference.exit.thread.i
                                        #   in Loop: Header=BB28_9 Depth=2
	incq	%rdi
	cmpq	%rdx, %rdi
	jb	.LBB28_9
	jmp	.LBB28_20
.LBB28_7:                               # %thread-pre-split
                                        #   in Loop: Header=BB28_8 Depth=1
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	remove_frame_from_dpb
	movl	dpb+28(%rip), %eax
	testl	%eax, %eax
	jne	.LBB28_8
	jmp	.LBB28_22
.LBB28_6:
	xorl	%eax, %eax
.LBB28_20:                              # %remove_unused_frame_from_dpb.exit.thread.preheader
	testl	%eax, %eax
	je	.LBB28_22
	.p2align	4, 0x90
.LBB28_21:                              # %remove_unused_frame_from_dpb.exit.thread
                                        # =>This Inner Loop Header: Depth=1
	callq	output_one_frame_from_dpb
	cmpl	$0, dpb+28(%rip)
	jne	.LBB28_21
.LBB28_22:                              # %remove_unused_frame_from_dpb.exit.thread._crit_edge
	movl	$-2147483648, dpb+40(%rip) # imm = 0x80000000
	popq	%rbx
	retq
.Lfunc_end28:
	.size	flush_dpb, .Lfunc_end28-flush_dpb
	.cfi_endproc

	.p2align	4, 0x90
	.type	unmark_for_reference,@function
unmark_for_reference:                   # @unmark_for_reference
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 32
.Lcfi94:
	.cfi_offset %rbx, -24
.Lcfi95:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	testb	$1, %al
	je	.LBB29_3
# BB#1:
	movq	56(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB29_3
# BB#2:
	movl	$0, 316848(%rcx)
.LBB29_3:
	testb	$2, %al
	je	.LBB29_6
# BB#4:
	movq	64(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB29_6
# BB#5:
	movl	$0, 316848(%rcx)
.LBB29_6:                               # %thread-pre-split
	cmpl	$3, %eax
	jne	.LBB29_7
# BB#8:
	movq	56(%rbx), %rax
	testq	%rax, %rax
	je	.LBB29_11
# BB#9:
	movq	64(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB29_11
# BB#10:
	movl	$0, 316848(%rax)
	movl	$0, 316848(%rcx)
.LBB29_11:
	leaq	48(%rbx), %r14
	movq	48(%rbx), %rax
	movl	$0, 316848(%rax)
	jmp	.LBB29_12
.LBB29_7:                               # %._crit_edge
	leaq	48(%rbx), %r14
	movq	48(%rbx), %rax
.LBB29_12:
	movl	$0, 4(%rbx)
	testq	%rax, %rax
	je	.LBB29_17
# BB#13:
	movq	316960(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB29_15
# BB#14:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	(%r14), %rax
	movq	$0, 316960(%rax)
.LBB29_15:
	movq	316968(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB29_17
# BB#16:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	(%r14), %rax
	movq	$0, 316968(%rax)
.LBB29_17:
	movq	56(%rbx), %rax
	testq	%rax, %rax
	je	.LBB29_22
# BB#18:
	movq	316960(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB29_20
# BB#19:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	56(%rbx), %rax
	movq	$0, 316960(%rax)
.LBB29_20:
	movq	316968(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB29_22
# BB#21:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	56(%rbx), %rax
	movq	$0, 316968(%rax)
.LBB29_22:
	movq	64(%rbx), %rax
	testq	%rax, %rax
	je	.LBB29_27
# BB#23:
	movq	316960(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB29_25
# BB#24:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	64(%rbx), %rax
	movq	$0, 316960(%rax)
.LBB29_25:
	movq	316968(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB29_27
# BB#26:
	movl	$6, %esi
	callq	free_mem3Dint64
	movq	64(%rbx), %rax
	movq	$0, 316968(%rax)
.LBB29_27:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end29:
	.size	unmark_for_reference, .Lfunc_end29-unmark_for_reference
	.cfi_endproc

	.globl	gen_field_ref_ids
	.p2align	4, 0x90
	.type	gen_field_ref_ids,@function
gen_field_ref_ids:                      # @gen_field_ref_ids
	.cfi_startproc
# BB#0:
	movl	316864(%rdi), %ecx
	cmpl	$4, %ecx
	jl	.LBB30_13
# BB#1:                                 # %.preheader.lr.ph
	movl	316868(%rdi), %esi
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB30_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_4 Depth 2
	cmpl	$4, %esi
	jl	.LBB30_12
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB30_2 Depth=1
	movl	%r9d, %eax
	sarl	$2, %eax
	movslq	%eax, %r8
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB30_4:                               #   Parent Loop BB30_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	316952(%rdi), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	(%rcx,%rdx,8), %rcx
	movsbq	(%rcx,%r9), %rcx
	testq	%rcx, %rcx
	movq	(%rax,%rdx,8), %rax
	movsbq	(%rax,%r9), %r10
	js	.LBB30_5
# BB#6:                                 #   in Loop: Header=BB30_4 Depth=2
	movq	316944(%rdi), %rax
	movl	%edx, %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	movswq	(%rax,%r8,2), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%rdi, %rax
	movq	24(%rax,%rcx,8), %rax
	jmp	.LBB30_7
	.p2align	4, 0x90
.LBB30_5:                               #   in Loop: Header=BB30_4 Depth=2
	xorl	%eax, %eax
.LBB30_7:                               #   in Loop: Header=BB30_4 Depth=2
	movq	316968(%rdi), %rcx
	movq	(%rcx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rax, (%rsi,%r9,8)
	testb	%r10b, %r10b
	js	.LBB30_8
# BB#9:                                 #   in Loop: Header=BB30_4 Depth=2
	movq	316944(%rdi), %rax
	movl	%edx, %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	movswq	(%rax,%r8,2), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%rdi, %rax
	movq	288(%rax,%r10,8), %rax
	jmp	.LBB30_10
	.p2align	4, 0x90
.LBB30_8:                               #   in Loop: Header=BB30_4 Depth=2
	xorl	%eax, %eax
.LBB30_10:                              #   in Loop: Header=BB30_4 Depth=2
	movq	8(%rcx), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movq	%rax, (%rcx,%r9,8)
	movq	316992(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movb	$1, (%rax,%r9)
	incq	%rdx
	movl	316868(%rdi), %esi
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%esi, %eax
	sarl	$2, %eax
	cltq
	cmpq	%rax, %rdx
	jl	.LBB30_4
# BB#11:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB30_2 Depth=1
	movl	316864(%rdi), %ecx
.LBB30_12:                              # %._crit_edge
                                        #   in Loop: Header=BB30_2 Depth=1
	incq	%r9
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%ecx, %eax
	sarl	$2, %eax
	cltq
	cmpq	%rax, %r9
	jl	.LBB30_2
.LBB30_13:                              # %._crit_edge42
	retq
.Lfunc_end30:
	.size	gen_field_ref_ids, .Lfunc_end30-gen_field_ref_ids
	.cfi_endproc

	.globl	dpb_split_field
	.p2align	4, 0x90
	.type	dpb_split_field,@function
dpb_split_field:                        # @dpb_split_field
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 112
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	48(%r14), %r15
	movl	316864(%r15), %esi
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$28, %eax
	addl	%esi, %eax
	sarl	$4, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	4(%r15), %eax
	movl	%eax, 40(%r14)
	cmpl	$0, 317048(%r15)
	movq	%r14, 32(%rsp)          # 8-byte Spill
	je	.LBB31_1
# BB#18:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%r14)
	movups	%xmm0, 317000(%r15)
	xorl	%r10d, %r10d
	jmp	.LBB31_19
.LBB31_1:
	movl	316868(%r15), %edx
	movl	316872(%r15), %ecx
	movl	316876(%r15), %r8d
	movl	$1, %edi
	callq	alloc_storable_picture
	movq	%rax, 56(%r14)
	movq	48(%r14), %rax
	movl	316864(%rax), %esi
	movl	316868(%rax), %edx
	movl	316872(%rax), %ecx
	movl	316876(%rax), %r8d
	movl	$2, %edi
	callq	alloc_storable_picture
	movq	%rax, 64(%r14)
	movq	48(%r14), %r15
	cmpl	$2, 316868(%r15)
	jl	.LBB31_4
# BB#2:                                 # %.lr.ph592.preheader
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB31_3:                               # %.lr.ph592
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	316920(%rax), %rax
	movq	(%rax,%rbx), %rdi
	movq	316920(%r15), %rax
	movq	(%rax,%rbx,2), %rsi
	movslq	316864(%r15), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%rbp
	movq	48(%r14), %r15
	movl	316868(%r15), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	addq	$8, %rbx
	cmpq	%rax, %rbp
	jl	.LBB31_3
.LBB31_4:                               # %.preheader560
	cmpl	$2, 316876(%r15)
	jl	.LBB31_7
# BB#5:                                 # %.lr.ph589.preheader
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB31_6:                               # %.lr.ph589
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	316928(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx), %rdi
	movq	316928(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx,2), %rsi
	movslq	316872(%r15), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	48(%r14), %rax
	movq	56(%r14), %rcx
	movq	316928(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbx), %rdi
	movq	316928(%rax), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbx,2), %rsi
	movslq	316872(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%rbp
	movq	48(%r14), %r15
	movl	316876(%r15), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	addq	$8, %rbx
	cmpq	%rax, %rbp
	jl	.LBB31_6
.LBB31_7:                               # %.preheader559
	cmpl	$2, 316868(%r15)
	jl	.LBB31_10
# BB#8:                                 # %.lr.ph587.preheader
	movl	$1, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB31_9:                               # %.lr.ph587
                                        # =>This Inner Loop Header: Depth=1
	movq	64(%r14), %rax
	movq	316920(%rax), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	316920(%r15), %rax
	movslq	%ebx, %rbx
	movq	(%rax,%rbx,8), %rsi
	movslq	316864(%r15), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%rbp
	movq	48(%r14), %r15
	movl	316868(%r15), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	addl	$2, %ebx
	cmpq	%rax, %rbp
	jl	.LBB31_9
.LBB31_10:                              # %.preheader558
	cmpl	$2, 316876(%r15)
	jl	.LBB31_13
# BB#11:                                # %.lr.ph583.preheader
	movl	$1, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB31_12:                              # %.lr.ph583
                                        # =>This Inner Loop Header: Depth=1
	movq	64(%r14), %rax
	movq	316928(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	316928(%r15), %rax
	movq	(%rax), %rax
	movslq	%ebx, %rbx
	movq	(%rax,%rbx,8), %rsi
	movslq	316872(%r15), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	48(%r14), %rax
	movq	64(%r14), %rcx
	movq	316928(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rdi
	movq	316928(%rax), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movslq	316872(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%rbp
	movq	48(%r14), %r15
	movl	316876(%r15), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	addl	$2, %ebx
	cmpq	%rax, %rbp
	jl	.LBB31_12
.LBB31_13:                              # %._crit_edge584
	movl	8(%r15), %eax
	movq	56(%r14), %r10
	movl	%eax, 4(%r10)
	movl	12(%r15), %ecx
	movq	64(%r14), %rdx
	movl	%ecx, 4(%rdx)
	movl	16(%r15), %esi
	movl	%esi, 16(%r10)
	movl	%ecx, 12(%rdx)
	movl	%ecx, 12(%r10)
	movl	%eax, 8(%rdx)
	movl	%eax, 8(%r10)
	movl	16(%r15), %eax
	movl	%eax, 16(%rdx)
	movl	316848(%r15), %eax
	movl	%eax, 316848(%rdx)
	movl	%eax, 316848(%r10)
	movl	316844(%r15), %eax
	movl	%eax, 316844(%rdx)
	movl	%eax, 316844(%r10)
	movl	316840(%r15), %eax
	movl	%eax, 316840(%rdx)
	movl	%eax, 316840(%r10)
	movl	%eax, 32(%r14)
	movl	$1, 316900(%rdx)
	movl	$1, 316900(%r10)
	movl	316904(%r15), %eax
	movl	%eax, 316904(%rdx)
	movl	%eax, 316904(%r10)
	movq	%r10, 317000(%r15)
	movq	%rdx, 317008(%r15)
	movq	%rdx, 317008(%r10)
	movq	%r15, 317016(%r10)
	movq	%r10, 317000(%rdx)
	movq	%r15, 317016(%rdx)
	movl	317044(%r15), %eax
	movl	%eax, 317044(%rdx)
	movl	%eax, 317044(%r10)
	movswq	316860(%r15), %rsi
	testq	%rsi, %rsi
	js	.LBB31_19
# BB#14:                                # %.preheader557.lr.ph
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB31_15:                              # %.preheader557
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_16 Depth 2
	movl	$16, %ecx
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB31_16:                              #   Parent Loop BB31_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	816(%r15,%rdi), %rax
	movq	%rax, 288(%r10,%rdi)
	movq	824(%r15,%rdi), %rax
	movq	%rax, 296(%r10,%rdi)
	movq	1344(%r15,%rdi), %rax
	movq	%rax, 288(%rdx,%rdi)
	movq	1352(%r15,%rdi), %rax
	movq	%rax, 296(%rdx,%rdi)
	movq	552(%r15,%rdi), %rax
	movq	%rax, 24(%r10,%rdi)
	movq	560(%r15,%rdi), %rax
	movq	%rax, 32(%r10,%rdi)
	movq	1080(%r15,%rdi), %rax
	movq	%rax, 24(%rdx,%rdi)
	movq	1088(%r15,%rdi), %rax
	movq	%rax, 32(%rdx,%rdi)
	addq	$16, %rdi
	decq	%rcx
	jne	.LBB31_16
# BB#17:                                #   in Loop: Header=BB31_15 Depth=1
	addq	$1584, %rbx             # imm = 0x630
	cmpq	%rsi, %rbp
	leaq	1(%rbp), %rbp
	jl	.LBB31_15
.LBB31_19:                              # %.preheader556
	movl	12(%rsp), %eax          # 4-byte Reload
	addl	%eax, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	316868(%r15), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	cmpl	$4, %eax
	jl	.LBB31_43
# BB#20:                                # %.lr.ph578.preheader
	movl	316864(%r15), %ecx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	addl	%ecx, %eax
	sarl	$2, %eax
	movslq	%eax, %r8
	movl	28(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%ecx, %eax
	sarl	$2, %eax
	cltq
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	movq	%r8, 16(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB31_21:                              # %.lr.ph578
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_23 Depth 2
	cmpl	$4, 48(%rsp)            # 4-byte Folded Reload
	jl	.LBB31_42
# BB#22:                                # %.lr.ph575
                                        #   in Loop: Header=BB31_21 Depth=1
	movl	%r11d, %eax
	sarl	$31, %eax
	movl	%eax, %r12d
	shrl	$30, %r12d
	addl	%r11d, %r12d
	sarl	$2, %r12d
	shrl	$29, %eax
	addl	%r11d, %eax
	sarl	$3, %eax
	imull	12(%rsp), %eax          # 4-byte Folded Reload
	movl	%r12d, %ecx
	shrl	$31, %ecx
	addl	%r12d, %ecx
	andl	$-2, %ecx
	movslq	%r12d, %r13
	subl	%ecx, %r12d
	addl	%eax, %r12d
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB31_23:                              #   Parent Loop BB31_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %ebx
	sarl	$31, %ebx
	shrl	$30, %ebx
	addl	%edi, %ebx
	sarl	$2, %ebx
	cmpl	$0, 316904(%r15)
	je	.LBB31_37
# BB#24:                                #   in Loop: Header=BB31_23 Depth=2
	leal	(%r12,%rbx,2), %esi
	movq	316936(%r15), %rax
	movslq	%esi, %rcx
	cmpb	$0, (%rax,%rcx)
	je	.LBB31_37
# BB#25:                                #   in Loop: Header=BB31_23 Depth=2
	addl	%esi, %esi
	andl	$2, %esi
	leal	2(%rsi), %ecx
	movq	316952(%r15), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	(%rdx,%r11,8), %rdx
	movsbq	(%rdx,%rdi), %r8
	testq	%r8, %r8
	movq	(%rax,%r11,8), %rax
	movsbq	(%rax,%rdi), %r14
	js	.LBB31_26
# BB#27:                                #   in Loop: Header=BB31_23 Depth=2
	movq	316944(%r15), %rax
	movq	(%rax,%r13,8), %rax
	movslq	%ebx, %rdx
	movswq	(%rax,%rdx,2), %rax
	movl	%ecx, %r9d
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%r15, %rax
	imulq	$264, %r9, %rcx         # imm = 0x108
	addq	%rax, %rcx
	movq	24(%rcx,%r8,8), %rcx
	jmp	.LBB31_28
	.p2align	4, 0x90
.LBB31_37:                              #   in Loop: Header=BB31_23 Depth=2
	movq	316952(%r15), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	(%rcx,%r11,8), %rcx
	movsbq	(%rcx,%rdi), %rdx
	testq	%rdx, %rdx
	movq	(%rax,%r11,8), %rax
	movsbq	(%rax,%rdi), %rcx
	movq	$-1, %rsi
	movq	$-1, %rax
	js	.LBB31_39
# BB#38:                                #   in Loop: Header=BB31_23 Depth=2
	movq	316944(%r15), %rax
	movq	(%rax,%r13,8), %rax
	movslq	%ebx, %rbp
	movswq	(%rax,%rbp,2), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%r15, %rax
	movq	24(%rax,%rdx,8), %rax
.LBB31_39:                              #   in Loop: Header=BB31_23 Depth=2
	movq	316968(%r15), %rdx
	movq	(%rdx), %rbp
	movq	(%rbp,%r11,8), %rbp
	movq	%rax, (%rbp,%rdi,8)
	testb	%cl, %cl
	js	.LBB31_41
# BB#40:                                #   in Loop: Header=BB31_23 Depth=2
	movq	316944(%r15), %rax
	movq	(%rax,%r13,8), %rax
	movslq	%ebx, %rsi
	movswq	(%rax,%rsi,2), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%r15, %rax
	movq	288(%rax,%rcx,8), %rsi
	jmp	.LBB31_41
.LBB31_26:                              # %._crit_edge621
                                        #   in Loop: Header=BB31_23 Depth=2
	movl	%ecx, %r9d
	xorl	%ecx, %ecx
.LBB31_28:                              #   in Loop: Header=BB31_23 Depth=2
	movq	316968(%r15), %rdx
	movq	(%rdx,%r9,8), %rax
	movq	(%rax,%r11,8), %rax
	movq	%rcx, (%rax,%rdi,8)
	testb	%r14b, %r14b
	js	.LBB31_29
# BB#30:                                #   in Loop: Header=BB31_23 Depth=2
	movq	316944(%r15), %rax
	movq	(%rax,%r13,8), %rax
	movslq	%ebx, %rcx
	movswq	(%rax,%rcx,2), %rax
	addl	$3, %esi
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%r15, %rax
	imulq	$264, %rsi, %rcx        # imm = 0x108
	addq	%rax, %rcx
	movq	24(%rcx,%r14,8), %rcx
	jmp	.LBB31_31
.LBB31_29:                              # %._crit_edge620
                                        #   in Loop: Header=BB31_23 Depth=2
	addl	$3, %esi
	xorl	%ecx, %ecx
.LBB31_31:                              #   in Loop: Header=BB31_23 Depth=2
	movq	(%rdx,%rsi,8), %rax
	movq	(%rax,%r11,8), %rax
	movq	%rcx, (%rax,%rdi,8)
	testb	%r8b, %r8b
	js	.LBB31_32
# BB#33:                                #   in Loop: Header=BB31_23 Depth=2
	movq	316944(%r15), %rax
	movq	(%rax,%r13,8), %rax
	movslq	%ebx, %rcx
	movswq	(%rax,%rcx,2), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%r15, %rax
	imulq	$264, %r9, %rcx         # imm = 0x108
	addq	%rax, %rcx
	movq	79224(%rcx,%r8,8), %rax
	jmp	.LBB31_34
.LBB31_32:                              #   in Loop: Header=BB31_23 Depth=2
	xorl	%eax, %eax
.LBB31_34:                              #   in Loop: Header=BB31_23 Depth=2
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	(%rdx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	%rax, (%rcx,%rdi,8)
	testb	%r14b, %r14b
	js	.LBB31_35
# BB#36:                                #   in Loop: Header=BB31_23 Depth=2
	movq	316944(%r15), %rax
	movq	(%rax,%r13,8), %rax
	movslq	%ebx, %rcx
	movswq	(%rax,%rcx,2), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%r15, %rax
	imulq	$264, %rsi, %rcx        # imm = 0x108
	addq	%rax, %rcx
	movq	79224(%rcx,%r14,8), %rsi
	jmp	.LBB31_41
.LBB31_35:                              #   in Loop: Header=BB31_23 Depth=2
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB31_41:                              #   in Loop: Header=BB31_23 Depth=2
	movq	8(%rdx), %rax
	movq	(%rax,%r11,8), %rax
	movq	%rsi, (%rax,%rdi,8)
	incq	%rdi
	cmpq	%r8, %rdi
	jl	.LBB31_23
.LBB31_42:                              # %._crit_edge576
                                        #   in Loop: Header=BB31_21 Depth=1
	incq	%r11
	cmpq	40(%rsp), %r11          # 8-byte Folded Reload
	jl	.LBB31_21
.LBB31_43:                              # %._crit_edge579
	cmpl	$0, 317048(%r15)
	je	.LBB31_46
# BB#44:
	movl	28(%rsp), %ecx          # 4-byte Reload
.LBB31_45:                              # %.loopexit555.thread628
	movq	316992(%r15), %rax
	movq	(%rax), %rdi
	imull	316864(%r15), %ecx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$28, %eax
	addl	%ecx, %eax
	sarl	$4, %eax
	movslq	%eax, %rdx
	xorl	%esi, %esi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	memset                  # TAILCALL
.LBB31_46:
	cmpl	$0, 316904(%r15)
	movl	28(%rsp), %ecx          # 4-byte Reload
	je	.LBB31_47
# BB#48:                                # %.preheader554
	cmpl	$8, %ecx
	jl	.LBB31_78
# BB#49:                                # %.lr.ph571
	xorl	%r13d, %r13d
	movq	32(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB31_50:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_52 Depth 2
	cmpl	$4, 316864(%r15)
	jl	.LBB31_55
# BB#51:                                # %.lr.ph568
                                        #   in Loop: Header=BB31_50 Depth=1
	movl	%r13d, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%r13d, %eax
	movl	%eax, %ecx
	sarl	$2, %ecx
	andl	$-4, %eax
	movl	%r13d, %edx
	subl	%eax, %edx
	leal	(%rdx,%rcx,8), %eax
	movl	%r13d, %edi
	shrl	$31, %edi
	addl	%r13d, %edi
	movl	%edi, %r11d
	sarl	%r11d
	leal	4(%rax), %ebp
	imull	12(%rsp), %ecx          # 4-byte Folded Reload
	shrl	$31, %edi
	addl	%r11d, %edi
	andl	$-2, %edi
	subl	%edi, %r11d
	addl	%ecx, %r11d
	leal	(%r13,%r13), %ecx
	leal	1(%r13,%r13), %edi
	movslq	%edi, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movslq	%ecx, %r9
	movslq	%ebp, %r14
	movslq	%eax, %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB31_52:                              #   Parent Loop BB31_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%ebp, %eax
	sarl	$2, %eax
	leal	(%r11,%rax,2), %eax
	movq	316936(%r15), %rcx
	cltq
	cmpb	$0, (%rcx,%rax)
	je	.LBB31_54
# BB#53:                                #   in Loop: Header=BB31_52 Depth=2
	movq	316992(%r10), %rax
	movq	(%rax,%r13,8), %rax
	movb	$1, (%rax,%rbp)
	movq	64(%r8), %rax
	movq	316992(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movb	$1, (%rax,%rbp)
	movq	48(%r8), %rax
	movq	316992(%rax), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movb	$1, (%rax,%rbp)
	movq	48(%r8), %rax
	movq	316992(%rax), %rax
	movq	(%rax,%r9,8), %rax
	movb	$1, (%rax,%rbp)
	movq	48(%r8), %rax
	movq	64(%r8), %r10
	movq	316976(%rax), %rdx
	movq	(%rdx), %rbx
	movq	(%rbx,%r14,8), %rbx
	movq	(%rbx,%rbp,8), %rbx
	movzwl	(%rbx), %edi
	movq	316976(%r10), %rsi
	movq	(%rsi), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movw	%di, (%rcx)
	movzwl	2(%rbx), %edi
	movw	%di, 2(%rcx)
	movq	8(%rdx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movzwl	(%rcx), %edx
	movq	8(%rsi), %rsi
	movq	(%rsi,%r13,8), %rsi
	movq	(%rsi,%rbp,8), %rsi
	movw	%dx, (%rsi)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rsi)
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movzbl	(%rax,%rbp), %eax
	movq	316952(%r10), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movb	%al, (%rcx,%rbp)
	movq	48(%r8), %rax
	movq	64(%r8), %rcx
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movzbl	(%rax,%rbp), %eax
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movb	%al, (%rcx,%rbp)
	movq	48(%r8), %r10
	movq	64(%r8), %rcx
	movq	316968(%r10), %rdx
	movq	32(%rdx), %rsi
	movq	(%rsi,%r14,8), %rsi
	movq	(%rsi,%rbp,8), %rsi
	movq	316968(%rcx), %rcx
	movq	(%rcx), %rdi
	movq	(%rdi,%r13,8), %rdi
	movq	%rsi, (%rdi,%rbp,8)
	movq	40(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	%rdx, (%rcx,%rbp,8)
	movq	316976(%r10), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r12,8), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movzwl	(%rdx), %esi
	movq	56(%r8), %rdi
	movq	316976(%rdi), %rbx
	movq	(%rbx), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%rbp,8), %rax
	movw	%si, (%rax)
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rax)
	movq	8(%rcx), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%rbp,8), %rax
	movzwl	(%rax), %ecx
	movq	8(%rbx), %rdx
	movq	(%rdx,%r13,8), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movw	%cx, (%rdx)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rdx)
	movq	316952(%r10), %rax
	movq	(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movzbl	(%rax,%rbp), %eax
	movq	316952(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movb	%al, (%rcx,%rbp)
	movq	48(%r8), %rax
	movq	56(%r8), %rcx
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movzbl	(%rax,%rbp), %eax
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movb	%al, (%rcx,%rbp)
	movq	48(%r8), %r15
	movq	56(%r8), %r10
	movq	316968(%r15), %rax
	movq	16(%rax), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	316968(%r10), %rdx
	movq	(%rdx), %rsi
	movq	(%rsi,%r13,8), %rsi
	movq	%rcx, (%rsi,%rbp,8)
	movq	24(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%rbp,8), %rax
	movq	8(%rdx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	%rax, (%rcx,%rbp,8)
.LBB31_54:                              #   in Loop: Header=BB31_52 Depth=2
	incq	%rbp
	movl	316864(%r15), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rax
	cmpq	%rax, %rbp
	jl	.LBB31_52
.LBB31_55:                              # %._crit_edge569
                                        #   in Loop: Header=BB31_50 Depth=1
	incq	%r13
	movl	316868(%r15), %ecx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$29, %eax
	addl	%ecx, %eax
	sarl	$3, %eax
	cltq
	cmpq	%rax, %r13
	jl	.LBB31_50
# BB#56:                                # %.loopexit555
	cmpl	$0, 317048(%r15)
	movq	32(%rsp), %r14          # 8-byte Reload
	jne	.LBB31_45
	jmp	.LBB31_57
.LBB31_47:
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB31_57:                              # %.preheader
	cmpl	$8, %ecx
	jl	.LBB31_78
# BB#58:                                # %.lr.ph565
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB31_59:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_61 Depth 2
	movl	%r12d, %eax
	orl	$1, %eax
	movl	%r12d, %esi
	andl	$-2, %esi
	testb	$2, %r12b
	cmovnel	%eax, %esi
	cmpl	$4, 316864(%r15)
	jl	.LBB31_77
# BB#60:                                # %.lr.ph
                                        #   in Loop: Header=BB31_59 Depth=1
	movl	%r12d, %edx
	shrl	$31, %edx
	addl	%r12d, %edx
	movl	%edx, %edi
	sarl	%edi
	leal	(%rsi,%rsi), %eax
	leal	(%r12,%r12), %ecx
	movslq	%ecx, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leal	1(%r12,%r12), %ecx
	movslq	%ecx, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movslq	%eax, %r8
	sarl	%esi
	movslq	%esi, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%r12d, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%r12d, %eax
	sarl	$2, %eax
	imull	12(%rsp), %eax          # 4-byte Folded Reload
	shrl	$31, %edx
	addl	%edi, %edx
	andl	$-2, %edx
	subl	%edx, %edi
	addl	%eax, %edi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB31_61:                              #   Parent Loop BB31_59 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %eax
	orl	$1, %eax
	movl	%edx, %r10d
	andl	$-2, %r10d
	testb	$2, %dl
	cmovnel	%eax, %r10d
	cmpl	$0, 316904(%r15)
	je	.LBB31_63
# BB#62:                                #   in Loop: Header=BB31_61 Depth=2
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%edx, %eax
	sarl	$2, %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rax,2), %eax
	movq	316936(%r15), %rcx
	cltq
	movzbl	(%rcx,%rax), %ecx
	testb	%cl, %cl
	je	.LBB31_63
# BB#75:                                #   in Loop: Header=BB31_61 Depth=2
	movq	316992(%r15), %rax
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movb	%cl, (%rax,%rdx)
	movq	48(%r14), %rax
	movq	316992(%rax), %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movb	%cl, (%rax,%rdx)
	jmp	.LBB31_76
	.p2align	4, 0x90
.LBB31_63:                              #   in Loop: Header=BB31_61 Depth=2
	movq	316992(%r15), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movb	$0, (%rax,%rdx)
	movq	48(%r14), %rax
	movq	316992(%rax), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movb	$0, (%rax,%rdx)
	movq	64(%r14), %rax
	movq	316992(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movb	$0, (%rax,%rdx)
	movq	56(%r14), %rax
	movq	316992(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movb	$0, (%rax,%rdx)
	movq	48(%r14), %r9
	movq	64(%r14), %r13
	movq	316976(%r9), %r11
	movq	(%r11), %rax
	movq	(%rax,%r8,8), %rax
	movslq	%r10d, %r15
	movq	(%rax,%r15,8), %rax
	movzwl	(%rax), %ebp
	movq	316976(%r13), %rsi
	movq	%r8, %rcx
	movq	(%rsi), %r8
	movq	(%r8,%r12,8), %rdi
	movq	%rcx, %r8
	movq	(%rdi,%rdx,8), %rdi
	movw	%bp, (%rdi)
	movq	56(%r14), %rbx
	movq	316976(%rbx), %rbx
	movq	(%rbx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movw	%bp, (%rcx)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rdi)
	movw	%ax, 2(%rcx)
	movq	8(%r11), %rax
	movq	(%rax,%r8,8), %rax
	movq	(%rax,%r15,8), %rax
	movzwl	(%rax), %ecx
	movq	8(%rsi), %rsi
	movq	(%rsi,%r12,8), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movw	%cx, (%rsi)
	movq	8(%rbx), %rdi
	movq	(%rdi,%r12,8), %rdi
	movq	(%rdi,%rdx,8), %rdi
	movw	%cx, (%rdi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rsi)
	movw	%ax, 2(%rdi)
	movq	316952(%r9), %rax
	movq	(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movsbq	(%rax,%r15), %rdi
	cmpq	$-1, %rdi
	movq	316952(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax,%r12,8), %rcx
	je	.LBB31_64
# BB#65:                                #   in Loop: Header=BB31_61 Depth=2
	movb	%dil, (%rcx,%rdx)
	movq	56(%r14), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movb	%dil, (%rax,%rdx)
	testb	%dil, %dil
	js	.LBB31_67
# BB#66:                                #   in Loop: Header=BB31_61 Depth=2
	movq	48(%r14), %rax
	movq	56(%r14), %rcx
	movq	316944(%rax), %rsi
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	(%rsi,%rbp,8), %rsi
	movl	%r10d, %ebp
	sarl	$2, %ebp
	movslq	%ebp, %rbp
	movswq	(%rsi,%rbp,2), %rsi
	imulq	$1584, %rsi, %rsi       # imm = 0x630
	addq	%rax, %rsi
	movq	158424(%rsi,%rdi,8), %rax
	movq	316968(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	%rax, (%rcx,%rdx,8)
	movq	237624(%rsi,%rdi,8), %rcx
	jmp	.LBB31_68
	.p2align	4, 0x90
.LBB31_64:                              #   in Loop: Header=BB31_61 Depth=2
	movb	$-1, (%rcx,%rdx)
	movq	56(%r14), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movb	$-1, (%rax,%rdx)
	movq	64(%r14), %rsi
	jmp	.LBB31_69
.LBB31_67:                              # %.critedge
                                        #   in Loop: Header=BB31_61 Depth=2
	movq	56(%r14), %rax
	movq	316968(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	$0, (%rax,%rdx,8)
	xorl	%ecx, %ecx
.LBB31_68:                              #   in Loop: Header=BB31_61 Depth=2
	movq	64(%r14), %rsi
	movq	316968(%rsi), %rax
	movq	(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	%rcx, (%rax,%rdx,8)
.LBB31_69:                              #   in Loop: Header=BB31_61 Depth=2
	movq	48(%r14), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movsbq	(%rax,%r15), %rax
	cmpq	$-1, %rax
	movq	316952(%rsi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r12,8), %rcx
	je	.LBB31_70
# BB#71:                                #   in Loop: Header=BB31_61 Depth=2
	movb	%al, (%rcx,%rdx)
	movq	56(%r14), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movb	%al, (%rcx,%rdx)
	testb	%al, %al
	js	.LBB31_73
# BB#72:                                #   in Loop: Header=BB31_61 Depth=2
	movq	48(%r14), %rcx
	movq	56(%r14), %rsi
	movq	316944(%rcx), %rdi
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	(%rdi,%rbp,8), %rdi
	sarl	$2, %r10d
	movslq	%r10d, %rbp
	movswq	(%rdi,%rbp,2), %rdi
	imulq	$1584, %rdi, %rdi       # imm = 0x630
	addq	%rcx, %rdi
	movq	158688(%rdi,%rax,8), %rcx
	movq	316968(%rsi), %rsi
	movq	8(%rsi), %rsi
	movq	(%rsi,%r12,8), %rsi
	movq	%rcx, (%rsi,%rdx,8)
	movq	237888(%rdi,%rax,8), %rax
	jmp	.LBB31_74
	.p2align	4, 0x90
.LBB31_70:                              #   in Loop: Header=BB31_61 Depth=2
	movb	$-1, (%rcx,%rdx)
	movq	56(%r14), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movb	$-1, (%rax,%rdx)
	jmp	.LBB31_76
.LBB31_73:                              # %.critedge553
                                        #   in Loop: Header=BB31_61 Depth=2
	movq	56(%r14), %rax
	movq	316968(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	$0, (%rax,%rdx,8)
	xorl	%eax, %eax
.LBB31_74:                              #   in Loop: Header=BB31_61 Depth=2
	movq	64(%r14), %rcx
	movq	316968(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	%rax, (%rcx,%rdx,8)
.LBB31_76:                              #   in Loop: Header=BB31_61 Depth=2
	incq	%rdx
	movq	48(%r14), %r15
	movl	316864(%r15), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rax
	cmpq	%rax, %rdx
	jl	.LBB31_61
.LBB31_77:                              # %._crit_edge
                                        #   in Loop: Header=BB31_59 Depth=1
	incq	%r12
	movl	316868(%r15), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	sarl	$3, %ecx
	movslq	%ecx, %rax
	cmpq	%rax, %r12
	jl	.LBB31_59
.LBB31_78:                              # %.loopexit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	dpb_split_field, .Lfunc_end31-dpb_split_field
	.cfi_endproc

	.globl	dpb_combine_field_yuv
	.p2align	4, 0x90
	.type	dpb_combine_field_yuv,@function
dpb_combine_field_yuv:                  # @dpb_combine_field_yuv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 32
.Lcfi112:
	.cfi_offset %rbx, -32
.Lcfi113:
	.cfi_offset %r14, -24
.Lcfi114:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	56(%r14), %rax
	movl	316864(%rax), %esi
	movl	316868(%rax), %edx
	addl	%edx, %edx
	movl	316872(%rax), %ecx
	movl	316876(%rax), %r8d
	addl	%r8d, %r8d
	xorl	%edi, %edi
	callq	alloc_storable_picture
	movq	%rax, 48(%r14)
	movq	56(%r14), %rcx
	cmpl	$0, 316868(%rcx)
	jle	.LBB32_3
# BB#1:                                 # %.lr.ph87
	xorl	%ebx, %ebx
	movl	$1, %r15d
	jmp	.LBB32_2
	.p2align	4, 0x90
.LBB32_16:                              # %._crit_edge91
                                        #   in Loop: Header=BB32_2 Depth=1
	movq	48(%r14), %rax
	addq	$8, %rbx
	incq	%r15
.LBB32_2:                               # =>This Inner Loop Header: Depth=1
	movq	316920(%rax), %rax
	movq	(%rax,%rbx,2), %rdi
	movq	316920(%rcx), %rax
	movq	(%rax,%rbx), %rsi
	movslq	316864(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	48(%r14), %rax
	movq	64(%r14), %rcx
	movq	316920(%rax), %rax
	movq	8(%rax,%rbx,2), %rdi
	movq	316920(%rcx), %rax
	movq	(%rax,%rbx), %rsi
	movslq	316864(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	56(%r14), %rcx
	movslq	316868(%rcx), %rax
	cmpq	%rax, %r15
	jl	.LBB32_16
.LBB32_3:                               # %.preheader
	cmpl	$0, 316876(%rcx)
	jle	.LBB32_6
# BB#4:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB32_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%r14), %rax
	movq	316928(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx,2), %rdi
	movq	316928(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx), %rsi
	movslq	316872(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	48(%r14), %rax
	movq	64(%r14), %rcx
	movq	316928(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax,%rbx,2), %rdi
	movq	316928(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx), %rsi
	movslq	316872(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	48(%r14), %rax
	movq	56(%r14), %rcx
	movq	316928(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx,2), %rdi
	movq	316928(%rcx), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx), %rsi
	movslq	316872(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	48(%r14), %rax
	movq	64(%r14), %rcx
	movq	316928(%rax), %rax
	movq	8(%rax), %rax
	movq	8(%rax,%rbx,2), %rdi
	movq	316928(%rcx), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx), %rsi
	movslq	316872(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%r15
	movq	56(%r14), %rcx
	movslq	316876(%rcx), %rax
	addq	$8, %rbx
	cmpq	%rax, %r15
	jl	.LBB32_5
.LBB32_6:                               # %._crit_edge
	movl	4(%rcx), %esi
	movq	48(%r14), %rax
	movq	64(%r14), %rdx
	movl	4(%rdx), %edi
	cmpl	%edi, %esi
	cmovlel	%esi, %edi
	movl	%edi, 16(%rax)
	movl	%edi, 4(%rax)
	movl	%edi, 40(%r14)
	movl	%edi, 16(%rcx)
	movl	%edi, 16(%rdx)
	movl	4(%rcx), %esi
	movl	%esi, 8(%rax)
	movl	%esi, 8(%rdx)
	movl	4(%rdx), %esi
	movl	%esi, 12(%rax)
	movl	%esi, 12(%rcx)
	cmpl	$0, 316848(%rcx)
	je	.LBB32_7
# BB#8:
	cmpl	$0, 316848(%rdx)
	setne	%sil
	jmp	.LBB32_9
.LBB32_7:
	xorl	%esi, %esi
.LBB32_9:
	movzbl	%sil, %esi
	movl	%esi, 316848(%rax)
	cmpl	$0, 316844(%rcx)
	je	.LBB32_10
# BB#11:
	xorl	%esi, %esi
	cmpl	$0, 316844(%rdx)
	setne	%sil
	movl	%esi, 316844(%rax)
	je	.LBB32_13
# BB#12:
	movl	32(%r14), %esi
	movl	%esi, 316840(%rax)
	jmp	.LBB32_13
.LBB32_10:                              # %.thread
	movl	$0, 316844(%rax)
.LBB32_13:
	movq	%rcx, 317000(%rax)
	movq	%rdx, 317008(%rax)
	movl	$0, 316900(%rax)
	movl	317044(%rcx), %esi
	movl	%esi, 317044(%rax)
	movl	317052(%rcx), %esi
	movl	%esi, 317052(%rax)
	testl	%esi, %esi
	je	.LBB32_15
# BB#14:
	movups	317056(%rcx), %xmm0
	movups	%xmm0, 317056(%rax)
.LBB32_15:
	movq	%rax, 317016(%rdx)
	movq	%rax, 317016(%rcx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end32:
	.size	dpb_combine_field_yuv, .Lfunc_end32-dpb_combine_field_yuv
	.cfi_endproc

	.globl	dpb_combine_field
	.p2align	4, 0x90
	.type	dpb_combine_field,@function
dpb_combine_field:                      # @dpb_combine_field
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi118:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi119:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi121:
	.cfi_def_cfa_offset 80
.Lcfi122:
	.cfi_offset %rbx, -56
.Lcfi123:
	.cfi_offset %r12, -48
.Lcfi124:
	.cfi_offset %r13, -40
.Lcfi125:
	.cfi_offset %r14, -32
.Lcfi126:
	.cfi_offset %r15, -24
.Lcfi127:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	dpb_combine_field_yuv
	movq	56(%r14), %r15
	movq	64(%r14), %r8
	movswl	316860(%r15), %ecx
	movswl	316860(%r8), %eax
	cmpl	%eax, %ecx
	cmovgel	%ecx, %eax
	testl	%eax, %eax
	js	.LBB33_7
# BB#1:                                 # %.preheader205.lr.ph
	movq	48(%r14), %r11
	cltq
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	288(%r11), %rcx
	addq	$24, %r11
	leaq	288(%r15), %r10
	leaq	24(%r15), %r12
	leaq	288(%r8), %r13
	addq	$24, %r8
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB33_2:                               # %.preheader205
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_3 Depth 2
                                        #     Child Loop BB33_5 Depth 2
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	$16, %ebx
	movq	%r13, %rdi
	movq	%r10, %rax
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB33_3:                               #   Parent Loop BB33_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %r8
	movq	%r8, %rbp
	shrq	$63, %rbp
	addq	%r8, %rbp
	andq	$-2, %rbp
	movq	(%rdi), %rsi
	movq	%rsi, %rcx
	shrq	$63, %rcx
	addq	%rsi, %rcx
	andq	$-2, %rcx
	cmpq	%rcx, %rbp
	cmovleq	%rbp, %rcx
	movq	%rcx, (%rdx)
	addq	$8, %rdx
	addq	$16, %rax
	addq	$16, %rdi
	decq	%rbx
	jne	.LBB33_3
# BB#4:                                 # %.preheader204.preheader
                                        #   in Loop: Header=BB33_2 Depth=1
	movl	$16, %edi
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%r8, %rax
	movq	%r12, %rdx
	movq	%r11, %rbx
	.p2align	4, 0x90
.LBB33_5:                               # %.preheader204
                                        #   Parent Loop BB33_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rcx
	movq	%rcx, %rsi
	shrq	$63, %rsi
	addq	%rcx, %rsi
	andq	$-2, %rsi
	movq	(%rax), %rbp
	movq	%rbp, %rcx
	shrq	$63, %rcx
	addq	%rbp, %rcx
	andq	$-2, %rcx
	cmpq	%rcx, %rsi
	cmovleq	%rsi, %rcx
	movq	%rcx, (%rbx)
	addq	$8, %rbx
	addq	$16, %rdx
	addq	$16, %rax
	decq	%rdi
	jne	.LBB33_5
# BB#6:                                 #   in Loop: Header=BB33_2 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	addq	$1584, %rcx             # imm = 0x630
	addq	$1584, %r10             # imm = 0x630
	addq	$1584, %r13             # imm = 0x630
	addq	$1584, %r11             # imm = 0x630
	addq	$1584, %r12             # imm = 0x630
	addq	$1584, %r8              # imm = 0x630
	cmpq	(%rsp), %r9             # 8-byte Folded Reload
	leaq	1(%r9), %r9
	jl	.LBB33_2
.LBB33_7:                               # %.preheader
	cmpl	$4, 316868(%r15)
	jl	.LBB33_35
# BB#8:                                 # %.lr.ph209
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB33_9:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_11 Depth 2
	cmpl	$4, 316864(%r15)
	jl	.LBB33_34
# BB#10:                                # %.lr.ph
                                        #   in Loop: Header=BB33_9 Depth=1
	movl	%r11d, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%r11d, %eax
	movl	%eax, %ecx
	andl	$2147483644, %ecx       # imm = 0x7FFFFFFC
	andl	$-4, %eax
	movl	%r11d, %edx
	subl	%eax, %edx
	leal	(%rdx,%rcx,2), %eax
	leal	4(%rax), %ecx
	movslq	%ecx, %r9
	movslq	%eax, %r10
	movl	%r11d, %eax
	sarl	$2, %eax
	movslq	%eax, %r8
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB33_11:                              #   Parent Loop BB33_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	48(%r14), %rax
	movq	316992(%rax), %rax
	movq	(%rax,%r9,8), %rax
	movb	$1, (%rax,%rdi)
	movq	48(%r14), %rax
	movq	316992(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movb	$1, (%rax,%rdi)
	movq	48(%r14), %r15
	movq	56(%r14), %rcx
	movq	316976(%rcx), %rdx
	movq	(%rdx), %rsi
	movq	(%rsi,%r11,8), %rsi
	movq	(%rsi,%rdi,8), %rsi
	movzwl	(%rsi), %ebx
	movq	316976(%r15), %rbp
	movq	(%rbp), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%rdi,8), %rax
	movw	%bx, (%rax)
	movzwl	2(%rsi), %esi
	movw	%si, 2(%rax)
	movq	8(%rdx), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%rdi,8), %rax
	movzwl	(%rax), %edx
	movq	8(%rbp), %rsi
	movq	(%rsi,%r10,8), %rsi
	movq	(%rsi,%rdi,8), %rsi
	movw	%dx, (%rsi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rsi)
	movq	316952(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movsbq	(%rax,%rdi), %rdx
	testq	%rdx, %rdx
	movq	316952(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movb	%dl, (%rax,%rdi)
	movq	48(%r14), %rax
	movq	56(%r14), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movsbq	(%rcx,%rdi), %rsi
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movb	%sil, (%rax,%rdi)
	movq	56(%r14), %rcx
	js	.LBB33_12
# BB#13:                                #   in Loop: Header=BB33_11 Depth=2
	movq	316944(%rcx), %rax
	movq	(%rax,%r8,8), %rax
	movl	%edi, %ebx
	sarl	$2, %ebx
	movslq	%ebx, %rbx
	movswq	(%rax,%rbx,2), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%rcx, %rax
	movq	24(%rax,%rdx,8), %rax
	jmp	.LBB33_14
	.p2align	4, 0x90
.LBB33_12:                              #   in Loop: Header=BB33_11 Depth=2
	xorl	%eax, %eax
.LBB33_14:                              # %._crit_edge224
                                        #   in Loop: Header=BB33_11 Depth=2
	movq	316968(%rcx), %rbx
	movq	(%rbx), %rbp
	movq	(%rbp,%r11,8), %rbp
	movq	%rax, (%rbp,%rdi,8)
	testb	%sil, %sil
	js	.LBB33_15
# BB#16:                                #   in Loop: Header=BB33_11 Depth=2
	movq	316944(%rcx), %rax
	movq	(%rax,%r8,8), %rax
	movl	%edi, %ebp
	sarl	$2, %ebp
	movslq	%ebp, %rbp
	movswq	(%rax,%rbp,2), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%rcx, %rax
	movq	288(%rax,%rsi,8), %rax
	jmp	.LBB33_17
	.p2align	4, 0x90
.LBB33_15:                              #   in Loop: Header=BB33_11 Depth=2
	xorl	%eax, %eax
.LBB33_17:                              #   in Loop: Header=BB33_11 Depth=2
	movq	8(%rbx), %rbx
	movq	(%rbx,%r11,8), %rbx
	movq	%rax, (%rbx,%rdi,8)
	testb	%dl, %dl
	js	.LBB33_18
# BB#19:                                #   in Loop: Header=BB33_11 Depth=2
	movq	316944(%rcx), %rax
	movq	(%rax,%r8,8), %rax
	movl	%edi, %ebx
	sarl	$2, %ebx
	movslq	%ebx, %rbx
	movswq	(%rax,%rbx,2), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%rcx, %rax
	movq	79224(%rax,%rdx,8), %rax
	jmp	.LBB33_20
	.p2align	4, 0x90
.LBB33_18:                              #   in Loop: Header=BB33_11 Depth=2
	xorl	%eax, %eax
.LBB33_20:                              #   in Loop: Header=BB33_11 Depth=2
	movq	48(%r14), %r15
	movq	316968(%r15), %rdx
	movq	(%rdx), %rbp
	movq	(%rbp,%r10,8), %rbp
	movq	%rax, (%rbp,%rdi,8)
	testb	%sil, %sil
	js	.LBB33_21
# BB#22:                                #   in Loop: Header=BB33_11 Depth=2
	movq	316944(%rcx), %rax
	movq	(%rax,%r8,8), %rax
	movl	%edi, %ebp
	sarl	$2, %ebp
	movslq	%ebp, %rbp
	movswq	(%rax,%rbp,2), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%rcx, %rax
	movq	79488(%rax,%rsi,8), %rax
	jmp	.LBB33_23
	.p2align	4, 0x90
.LBB33_21:                              #   in Loop: Header=BB33_11 Depth=2
	xorl	%eax, %eax
.LBB33_23:                              #   in Loop: Header=BB33_11 Depth=2
	movq	8(%rdx), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	%rax, (%rcx,%rdi,8)
	movq	64(%r14), %rax
	movq	316976(%rax), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movzwl	(%rdx), %esi
	movq	316976(%r15), %rbp
	movq	(%rbp), %rbx
	movq	(%rbx,%r9,8), %rbx
	movq	(%rbx,%rdi,8), %rbx
	movw	%si, (%rbx)
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rbx)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movzwl	(%rcx), %edx
	movq	8(%rbp), %rsi
	movq	(%rsi,%r9,8), %rsi
	movq	(%rsi,%rdi,8), %rsi
	movw	%dx, (%rsi)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rsi)
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movsbq	(%rax,%rdi), %rdx
	testq	%rdx, %rdx
	movq	316952(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax,%r9,8), %rax
	movb	%dl, (%rax,%rdi)
	movq	48(%r14), %rax
	movq	64(%r14), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movsbq	(%rcx,%rdi), %rsi
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r9,8), %rax
	movb	%sil, (%rax,%rdi)
	movq	64(%r14), %rcx
	js	.LBB33_24
# BB#25:                                #   in Loop: Header=BB33_11 Depth=2
	movq	316944(%rcx), %rax
	movq	(%rax,%r8,8), %rax
	movl	%edi, %ebx
	sarl	$2, %ebx
	movslq	%ebx, %rbx
	movswq	(%rax,%rbx,2), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%rcx, %rax
	movq	24(%rax,%rdx,8), %rax
	jmp	.LBB33_26
	.p2align	4, 0x90
.LBB33_24:                              #   in Loop: Header=BB33_11 Depth=2
	xorl	%eax, %eax
.LBB33_26:                              # %._crit_edge225
                                        #   in Loop: Header=BB33_11 Depth=2
	movq	316968(%rcx), %rbx
	movq	(%rbx), %rbp
	movq	(%rbp,%r11,8), %rbp
	movq	%rax, (%rbp,%rdi,8)
	testb	%sil, %sil
	js	.LBB33_27
# BB#28:                                #   in Loop: Header=BB33_11 Depth=2
	movq	316944(%rcx), %rax
	movq	(%rax,%r8,8), %rax
	movl	%edi, %ebp
	sarl	$2, %ebp
	movslq	%ebp, %rbp
	movswq	(%rax,%rbp,2), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%rcx, %rax
	movq	288(%rax,%rsi,8), %rax
	jmp	.LBB33_29
	.p2align	4, 0x90
.LBB33_27:                              #   in Loop: Header=BB33_11 Depth=2
	xorl	%eax, %eax
.LBB33_29:                              #   in Loop: Header=BB33_11 Depth=2
	movq	8(%rbx), %rbx
	movq	(%rbx,%r11,8), %rbx
	movq	%rax, (%rbx,%rdi,8)
	movq	$-1, %rbx
	testb	%dl, %dl
	movq	$-1, %rax
	js	.LBB33_31
# BB#30:                                #   in Loop: Header=BB33_11 Depth=2
	movq	316944(%rcx), %rax
	movq	(%rax,%r8,8), %rax
	movl	%edi, %ebp
	sarl	$2, %ebp
	movslq	%ebp, %rbp
	movswq	(%rax,%rbp,2), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%rcx, %rax
	movq	79224(%rax,%rdx,8), %rax
.LBB33_31:                              #   in Loop: Header=BB33_11 Depth=2
	movq	48(%r14), %rdx
	movq	316968(%rdx), %rdx
	movq	(%rdx), %rbp
	movq	(%rbp,%r9,8), %rbp
	movq	%rax, (%rbp,%rdi,8)
	testb	%sil, %sil
	js	.LBB33_33
# BB#32:                                #   in Loop: Header=BB33_11 Depth=2
	movq	316944(%rcx), %rax
	movq	(%rax,%r8,8), %rax
	movl	%edi, %ebx
	sarl	$2, %ebx
	movslq	%ebx, %rbx
	movswq	(%rax,%rbx,2), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%rcx, %rax
	movq	79488(%rax,%rsi,8), %rbx
.LBB33_33:                              #   in Loop: Header=BB33_11 Depth=2
	movq	8(%rdx), %rax
	movq	(%rax,%r9,8), %rax
	movq	%rbx, (%rax,%rdi,8)
	movq	56(%r14), %rax
	movq	316992(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movb	$1, (%rax,%rdi)
	movq	64(%r14), %rax
	movq	316992(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movb	$1, (%rax,%rdi)
	incq	%rdi
	movq	56(%r14), %r15
	movl	316864(%r15), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rax
	cmpq	%rax, %rdi
	jl	.LBB33_11
.LBB33_34:                              # %._crit_edge
                                        #   in Loop: Header=BB33_9 Depth=1
	incq	%r11
	movl	316868(%r15), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rax
	cmpq	%rax, %r11
	jl	.LBB33_9
.LBB33_35:                              # %._crit_edge210
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end33:
	.size	dpb_combine_field, .Lfunc_end33-dpb_combine_field
	.cfi_endproc

	.globl	alloc_ref_pic_list_reordering_buffer
	.p2align	4, 0x90
	.type	alloc_ref_pic_list_reordering_buffer,@function
alloc_ref_pic_list_reordering_buffer:   # @alloc_ref_pic_list_reordering_buffer
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 32
.Lcfi131:
	.cfi_offset %rbx, -32
.Lcfi132:
	.cfi_offset %r14, -24
.Lcfi133:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	img(%rip), %rax
	movl	44(%rax), %ecx
	cmpl	$2, %ecx
	je	.LBB34_8
# BB#1:
	cmpl	$4, %ecx
	jne	.LBB34_2
.LBB34_8:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 72(%r15)
	movq	$0, 88(%r15)
	jmp	.LBB34_9
.LBB34_2:
	movslq	5640(%rax), %r14
	incq	%r14
	movl	$4, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 72(%r15)
	testq	%rax, %rax
	jne	.LBB34_4
# BB#3:
	movl	$.L.str.16, %edi
	callq	no_mem_exit
.LBB34_4:
	movl	$4, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 80(%r15)
	testq	%rax, %rax
	jne	.LBB34_6
# BB#5:
	movl	$.L.str.17, %edi
	callq	no_mem_exit
.LBB34_6:
	movl	$4, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 88(%r15)
	testq	%rax, %rax
	jne	.LBB34_9
# BB#7:
	movl	$.L.str.18, %edi
	callq	no_mem_exit
.LBB34_9:
	movq	img(%rip), %rax
	cmpl	$1, 44(%rax)
	jne	.LBB34_16
# BB#10:
	movslq	5644(%rax), %rbx
	incq	%rbx
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, 104(%r15)
	testq	%rax, %rax
	jne	.LBB34_12
# BB#11:
	movl	$.L.str.19, %edi
	callq	no_mem_exit
.LBB34_12:
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, 112(%r15)
	testq	%rax, %rax
	jne	.LBB34_14
# BB#13:
	movl	$.L.str.20, %edi
	callq	no_mem_exit
.LBB34_14:
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, 120(%r15)
	testq	%rax, %rax
	jne	.LBB34_17
# BB#15:
	movl	$.L.str.21, %edi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	no_mem_exit             # TAILCALL
.LBB34_16:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%r15)
	movq	$0, 120(%r15)
.LBB34_17:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end34:
	.size	alloc_ref_pic_list_reordering_buffer, .Lfunc_end34-alloc_ref_pic_list_reordering_buffer
	.cfi_endproc

	.globl	free_ref_pic_list_reordering_buffer
	.p2align	4, 0x90
	.type	free_ref_pic_list_reordering_buffer,@function
free_ref_pic_list_reordering_buffer:    # @free_ref_pic_list_reordering_buffer
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi136:
	.cfi_def_cfa_offset 32
.Lcfi137:
	.cfi_offset %rbx, -24
.Lcfi138:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_2
# BB#1:
	callq	free
.LBB35_2:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_4
# BB#3:
	callq	free
.LBB35_4:
	leaq	72(%rbx), %r14
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_6
# BB#5:
	callq	free
.LBB35_6:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movq	$0, 16(%r14)
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_8
# BB#7:
	callq	free
.LBB35_8:
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_10
# BB#9:
	callq	free
.LBB35_10:
	movq	120(%rbx), %rdi
	addq	$104, %rbx
	testq	%rdi, %rdi
	je	.LBB35_12
# BB#11:
	callq	free
.LBB35_12:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end35:
	.size	free_ref_pic_list_reordering_buffer, .Lfunc_end35-free_ref_pic_list_reordering_buffer
	.cfi_endproc

	.globl	fill_frame_num_gap
	.p2align	4, 0x90
	.type	fill_frame_num_gap,@function
fill_frame_num_gap:                     # @fill_frame_num_gap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi139:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi140:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi142:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi143:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi145:
	.cfi_def_cfa_offset 64
.Lcfi146:
	.cfi_offset %rbx, -56
.Lcfi147:
	.cfi_offset %r12, -48
.Lcfi148:
	.cfi_offset %r13, -40
.Lcfi149:
	.cfi_offset %r14, -32
.Lcfi150:
	.cfi_offset %r15, -24
.Lcfi151:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	5696(%r14), %r15d
	movl	5700(%r14), %r12d
	movq	$0, 5696(%r14)
	movl	5660(%r14), %eax
	movl	5676(%r14), %r13d
	incl	%eax
	xorl	%edx, %edx
	divl	5816(%r14)
	jmp	.LBB36_1
	.p2align	4, 0x90
.LBB36_4:                               #   in Loop: Header=BB36_1 Depth=1
	movl	5664(%r14), %eax
	movl	%eax, 8(%rbx)
	movl	5668(%r14), %eax
	movl	%eax, 12(%rbx)
	movl	5672(%r14), %eax
	movl	%eax, 16(%rbx)
	movl	%eax, 4(%rbx)
	movq	%rbx, %rdi
	callq	store_picture_in_dpb
	movl	%ebp, 5660(%r14)
	incl	%ebp
	movl	%ebp, %eax
	cltd
	idivl	5816(%r14)
.LBB36_1:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %ebp
	cmpl	%ebp, %r13d
	je	.LBB36_5
# BB#2:                                 #   in Loop: Header=BB36_1 Depth=1
	movl	48(%r14), %esi
	movl	52(%r14), %edx
	movl	56(%r14), %ecx
	movl	64(%r14), %r8d
	xorl	%edi, %edi
	callq	alloc_storable_picture
	movq	%rax, %rbx
	movl	$1, 316900(%rbx)
	movl	%ebp, 316832(%rbx)
	movl	%ebp, 316824(%rbx)
	movl	$1, 316856(%rbx)
	movl	$1, 316852(%rbx)
	movl	$1, 316848(%rbx)
	movl	$0, 317040(%rbx)
	movl	%ebp, 5676(%r14)
	movq	active_sps(%rip), %rax
	cmpl	$0, 1012(%rax)
	je	.LBB36_4
# BB#3:                                 #   in Loop: Header=BB36_1 Depth=1
	movq	%r14, %rdi
	callq	decode_poc
	jmp	.LBB36_4
.LBB36_5:                               # %._crit_edge
	movl	%r15d, 5696(%r14)
	movl	%r12d, 5700(%r14)
	movl	%r13d, 5676(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end36:
	.size	fill_frame_num_gap, .Lfunc_end36-fill_frame_num_gap
	.cfi_endproc

	.globl	alloc_colocated
	.p2align	4, 0x90
	.type	alloc_colocated,@function
alloc_colocated:                        # @alloc_colocated
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi155:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi156:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi158:
	.cfi_def_cfa_offset 64
.Lcfi159:
	.cfi_offset %rbx, -56
.Lcfi160:
	.cfi_offset %r12, -48
.Lcfi161:
	.cfi_offset %r13, -40
.Lcfi162:
	.cfi_offset %r14, -32
.Lcfi163:
	.cfi_offset %r15, -24
.Lcfi164:
	.cfi_offset %rbp, -16
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%esi, %r14d
	movl	%edi, %r13d
	movl	$1, %edi
	movl	$4880, %esi             # imm = 0x1310
	callq	calloc
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB37_2
# BB#1:
	movl	$.L.str.22, %edi
	callq	no_mem_exit
.LBB37_2:
	movl	%r13d, 4(%r12)
	movl	%r14d, 8(%r12)
	leaq	1600(%r12), %rdi
	movl	%r14d, %r15d
	sarl	$31, %r15d
	movl	%r15d, %ebx
	shrl	$30, %ebx
	addl	%r14d, %ebx
	sarl	$2, %ebx
	movl	%r13d, %ebp
	sarl	$31, %ebp
	shrl	$30, %ebp
	addl	%r13d, %ebp
	sarl	$2, %ebp
	movl	$2, %esi
	movl	%ebx, %edx
	movl	%ebp, %ecx
	callq	get_mem3D
	leaq	1608(%r12), %rdi
	movl	$2, %esi
	movl	%ebx, %edx
	movl	%ebp, %ecx
	callq	get_mem3Dint64
	leaq	1616(%r12), %rdi
	movl	$2, %esi
	movl	$2, %r8d
	movl	%ebx, %edx
	movl	%ebp, %ecx
	callq	get_mem4Dshort
	leaq	1624(%r12), %rdi
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	get_mem2D
	leaq	4872(%r12), %rdi
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	get_mem2D
	movl	4(%rsp), %eax           # 4-byte Reload
	testl	%eax, %eax
	movl	%eax, %ebx
	je	.LBB37_4
# BB#3:
	leaq	3216(%r12), %rdi
	shrl	$29, %r15d
	addl	%r15d, %r14d
	sarl	$3, %r14d
	movl	$2, %esi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	get_mem3D
	leaq	3224(%r12), %rdi
	movl	$2, %esi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	get_mem3Dint64
	leaq	3232(%r12), %rdi
	movl	$2, %esi
	movl	$2, %r8d
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	get_mem4Dshort
	leaq	3240(%r12), %rdi
	movl	%r14d, %esi
	movl	%ebp, %edx
	callq	get_mem2D
	leaq	4832(%r12), %rdi
	movl	$2, %esi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	get_mem3D
	leaq	4840(%r12), %rdi
	movl	$2, %esi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	get_mem3Dint64
	leaq	4848(%r12), %rdi
	movl	$2, %esi
	movl	$2, %r8d
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	get_mem4Dshort
	movq	%r12, %rdi
	addq	$4856, %rdi             # imm = 0x12F8
	movl	%r14d, %esi
	movl	%ebp, %edx
	callq	get_mem2D
.LBB37_4:
	movl	%ebx, (%r12)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end37:
	.size	alloc_colocated, .Lfunc_end37-alloc_colocated
	.cfi_endproc

	.globl	free_colocated
	.p2align	4, 0x90
	.type	free_colocated,@function
free_colocated:                         # @free_colocated
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 16
.Lcfi166:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB38_11
# BB#1:
	movq	1600(%rbx), %rdi
	movl	$2, %esi
	callq	free_mem3D
	movq	1608(%rbx), %rdi
	movl	$2, %esi
	callq	free_mem3Dint64
	movq	1616(%rbx), %rdi
	movl	8(%rbx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	addl	%eax, %edx
	sarl	$2, %edx
	movl	$2, %esi
	callq	free_mem4Dshort
	movq	1624(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB38_3
# BB#2:
	callq	free_mem2D
	movq	$0, 1624(%rbx)
.LBB38_3:
	movq	4872(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB38_5
# BB#4:
	callq	free_mem2D
	movq	$0, 4872(%rbx)
.LBB38_5:
	cmpl	$0, (%rbx)
	je	.LBB38_10
# BB#6:
	movq	3216(%rbx), %rdi
	movl	$2, %esi
	callq	free_mem3D
	movq	3224(%rbx), %rdi
	movl	$2, %esi
	callq	free_mem3Dint64
	movq	3232(%rbx), %rdi
	movl	8(%rbx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%eax, %edx
	sarl	$3, %edx
	movl	$2, %esi
	callq	free_mem4Dshort
	movq	3240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB38_8
# BB#7:
	callq	free_mem2D
	movq	$0, 3240(%rbx)
.LBB38_8:
	movq	4832(%rbx), %rdi
	movl	$2, %esi
	callq	free_mem3D
	movq	4840(%rbx), %rdi
	movl	$2, %esi
	callq	free_mem3Dint64
	movq	4848(%rbx), %rdi
	movl	8(%rbx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%eax, %edx
	sarl	$3, %edx
	movl	$2, %esi
	callq	free_mem4Dshort
	movq	4856(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB38_10
# BB#9:
	callq	free_mem2D
.LBB38_10:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB38_11:
	popq	%rbx
	retq
.Lfunc_end38:
	.size	free_colocated, .Lfunc_end38-free_colocated
	.cfi_endproc

	.globl	compute_colocated
	.p2align	4, 0x90
	.type	compute_colocated,@function
compute_colocated:                      # @compute_colocated
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi167:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi168:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi169:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi170:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi171:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi172:
	.cfi_def_cfa_offset 56
.Lcfi173:
	.cfi_offset %rbx, -56
.Lcfi174:
	.cfi_offset %r12, -48
.Lcfi175:
	.cfi_offset %r13, -40
.Lcfi176:
	.cfi_offset %r14, -32
.Lcfi177:
	.cfi_offset %r15, -24
.Lcfi178:
	.cfi_offset %rbp, -16
	movq	8(%rsi), %rax
	movq	(%rax), %r13
	movq	img(%rip), %rax
	cmpl	$0, 5624(%rax)
	movq	%rsi, -56(%rsp)         # 8-byte Spill
	je	.LBB39_2
# BB#1:
	movq	24(%rsi), %rcx
	movq	40(%rsi), %rdx
	movq	(%rcx), %r12
	movq	(%rdx), %r14
	jmp	.LBB39_8
.LBB39_2:
	cmpl	$0, 5680(%rax)
	je	.LBB39_7
# BB#3:
	movl	5584(%rax), %ecx
	cmpl	(%r13), %ecx
	je	.LBB39_7
# BB#4:
	cmpl	$0, 316900(%r13)
	je	.LBB39_7
# BB#5:
	cmpl	$1, %ecx
	jne	.LBB39_172
# BB#6:
	movq	317000(%r13), %r14
	jmp	.LBB39_173
.LBB39_7:
	movq	%r13, %r14
	movq	%r13, %r12
.LBB39_8:
	movq	active_sps(%rip), %rcx
	cmpl	$0, 2076(%rcx)
	je	.LBB39_10
# BB#9:
	cmpl	$0, 2084(%rcx)
	je	.LBB39_21
.LBB39_10:                              # %.preheader895
	movl	316868(%r13), %edx
	cmpl	$4, %edx
	jl	.LBB39_21
# BB#11:                                # %.lr.ph937
	leaq	316976(%r13), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	leaq	316952(%r13), %r15
	leaq	316976(%r12), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movq	%r12, -72(%rsp)         # 8-byte Spill
	leaq	316952(%r12), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	leaq	316976(%r14), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movq	%r14, -64(%rsp)         # 8-byte Spill
	leaq	316952(%r14), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movq	%r13, -80(%rsp)         # 8-byte Spill
	movl	316864(%r13), %ecx
	xorl	%r12d, %r12d
	movq	-80(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB39_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_14 Depth 2
	cmpl	$4, %ecx
	jl	.LBB39_19
# BB#13:                                # %.lr.ph934
                                        #   in Loop: Header=BB39_12 Depth=1
	movl	%r12d, %eax
	shrl	$31, %eax
	addl	%r12d, %eax
	sarl	%eax
	movl	%r12d, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%r12d, %ecx
	sarl	$3, %ecx
	leal	(%rax,%rcx,4), %ecx
	cltq
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	leal	4(%rcx), %eax
	movl	%eax, -84(%rsp)         # 4-byte Spill
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB39_14:                              #   Parent Loop BB39_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	img(%rip), %rax
	cmpl	$0, 5624(%rax)
	movq	-96(%rsp), %rax         # 8-byte Reload
	movq	%r12, %rbp
	movq	%r15, %rcx
	movq	%r12, %r14
	movq	%r13, %r11
	je	.LBB39_17
# BB#15:                                #   in Loop: Header=BB39_14 Depth=2
	movq	316992(%r13), %rax
	movq	(%rax,%r12,8), %rax
	cmpb	$0, (%rax,%r10)
	movq	-96(%rsp), %rax         # 8-byte Reload
	movq	%r12, %rbp
	movq	%r15, %rcx
	movq	%r12, %r14
	movq	%r13, %r11
	je	.LBB39_17
# BB#16:                                #   in Loop: Header=BB39_14 Depth=2
	movq	dec_picture(%rip), %rax
	movl	4(%rax), %eax
	movl	%eax, %ecx
	movq	-64(%rsp), %r11         # 8-byte Reload
	subl	4(%r11), %ecx
	movl	%ecx, %esi
	negl	%esi
	cmovll	%ecx, %esi
	movq	-72(%rsp), %rbp         # 8-byte Reload
	subl	4(%rbp), %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%ecx, %esi
	movq	-40(%rsp), %rax         # 8-byte Reload
	cmovgq	-24(%rsp), %rax         # 8-byte Folded Reload
	movq	-48(%rsp), %rcx         # 8-byte Reload
	cmovgq	-32(%rsp), %rcx         # 8-byte Folded Reload
	movl	-84(%rsp), %esi         # 4-byte Reload
	cmovgl	-8(%rsp), %esi          # 4-byte Folded Reload
	movslq	%esi, %r14
	cmovgq	%rbp, %r11
	movq	-16(%rsp), %rbp         # 8-byte Reload
.LBB39_17:                              #   in Loop: Header=BB39_14 Depth=2
	movq	(%rax), %rax
	movq	(%rax), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movq	(%rdx,%r10,8), %rbx
	movzwl	(%rbx), %ebx
	movq	1616(%rdi), %r8
	movq	(%r8), %r9
	movq	(%r9,%r12,8), %rsi
	movq	(%rsi,%r10,8), %rsi
	movw	%bx, (%rsi)
	movq	(%rdx,%r10,8), %rdx
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rsi)
	movq	8(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%r10,8), %rax
	movzwl	(%rax), %edx
	movq	8(%r8), %rsi
	movq	(%rsi,%r12,8), %rsi
	movq	(%rsi,%r10,8), %rsi
	movw	%dx, (%rsi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rsi)
	movq	(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movzbl	(%rax,%r10), %eax
	movq	1600(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r12,8), %rdx
	movb	%al, (%rdx,%r10)
	movq	(%rcx), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movzbl	(%rax,%r10), %eax
	movq	1600(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movb	%al, (%rcx,%r10)
	movq	316968(%r13), %rax
	movq	(%rax), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	1608(%rdi), %rdx
	movq	(%rdx), %rsi
	movq	(%rsi,%r12,8), %rsi
	movq	%rcx, (%rsi,%r10,8)
	movq	8(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r10,8), %rax
	movq	8(%rdx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	%rax, (%rcx,%r10,8)
	movzbl	316844(%r11), %eax
	movb	%al, 4864(%rdi)
	incq	%r10
	movl	316864(%r13), %ecx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%ecx, %eax
	sarl	$2, %eax
	cltq
	cmpq	%rax, %r10
	jl	.LBB39_14
# BB#18:                                # %._crit_edge935.loopexit
                                        #   in Loop: Header=BB39_12 Depth=1
	movl	316868(%r13), %edx
.LBB39_19:                              # %._crit_edge935
                                        #   in Loop: Header=BB39_12 Depth=1
	incq	%r12
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%edx, %eax
	sarl	$2, %eax
	cltq
	cmpq	%rax, %r12
	jl	.LBB39_12
# BB#20:                                # %.loopexit896.loopexit
	movq	img(%rip), %rax
	movq	-80(%rsp), %r13         # 8-byte Reload
	movq	-64(%rsp), %r14         # 8-byte Reload
	movq	-72(%rsp), %r12         # 8-byte Reload
.LBB39_21:                              # %.loopexit896
	cmpl	$0, 5584(%rax)
	jne	.LBB39_23
# BB#22:
	cmpl	$0, 5624(%rax)
	je	.LBB39_81
.LBB39_23:                              # %.preheader893
	movl	316868(%r13), %eax
	cmpl	$8, %eax
	jl	.LBB39_81
# BB#24:                                # %.lr.ph930
	movl	316864(%r13), %esi
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB39_25:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_27 Depth 2
	cmpl	$4, %esi
	jl	.LBB39_80
# BB#26:                                # %.lr.ph927
                                        #   in Loop: Header=BB39_25 Depth=1
	movl	%r8d, %eax
	orl	$1, %eax
	movl	%r8d, %edx
	andl	$-2, %edx
	testb	$2, %r8b
	cmovneq	%rax, %rdx
	movslq	%edx, %r11
	leal	(%r8,%r8), %eax
	movslq	%eax, %r10
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB39_27:                              #   Parent Loop BB39_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %edx
	orl	$1, %edx
	movl	%eax, %esi
	andl	$-2, %esi
	testb	$2, %al
	cmovnel	%edx, %esi
	movq	img(%rip), %rdx
	cmpl	$0, 5624(%rdx)
	je	.LBB39_34
# BB#28:                                #   in Loop: Header=BB39_27 Depth=2
	movq	316976(%r14), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r11,8), %rdx
	movslq	%esi, %r9
	movq	(%rdx,%r9,8), %rdx
	movzwl	(%rdx), %esi
	movq	4848(%rdi), %rbp
	movq	(%rbp), %rbx
	movq	(%rbx,%r8,8), %rbx
	movq	(%rbx,%rax,8), %rbx
	movw	%si, (%rbx)
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rbx)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %edx
	movq	8(%rbp), %rsi
	movq	(%rsi,%r8,8), %rsi
	movq	(%rsi,%rax,8), %rsi
	movw	%dx, (%rsi)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rsi)
	movq	316952(%r14), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	4832(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movb	%cl, (%rdx,%rax)
	movq	316952(%r14), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	4832(%rdi), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movb	%cl, (%rdx,%rax)
	movq	316968(%r14), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%r9,8), %rdx
	movq	4840(%rdi), %rsi
	movq	(%rsi), %rbx
	movq	(%rbx,%r8,8), %rbx
	movq	%rdx, (%rbx,%rax,8)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movq	8(%rsi), %rdx
	movq	(%rdx,%r8,8), %rdx
	movq	%rcx, (%rdx,%rax,8)
	movq	img(%rip), %rcx
	cmpl	$1, 40(%rcx)
	jne	.LBB39_59
# BB#29:                                #   in Loop: Header=BB39_27 Depth=2
	movq	4832(%rdi), %rsi
	movq	(%rsi), %rcx
	movq	(%rcx,%r8,8), %rcx
	movzbl	(%rcx,%rax), %edx
	cmpl	$0, 316844(%r14)
	jne	.LBB39_36
# BB#30:                                #   in Loop: Header=BB39_27 Depth=2
	testb	%dl, %dl
	jne	.LBB39_36
# BB#31:                                #   in Loop: Header=BB39_27 Depth=2
	movq	4848(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rsi
	movswl	(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$1, %edx
	ja	.LBB39_57
# BB#32:                                #   in Loop: Header=BB39_27 Depth=2
	movswl	2(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movb	$1, %sil
	cmpl	$2, %edx
	jb	.LBB39_58
	jmp	.LBB39_57
	.p2align	4, 0x90
.LBB39_34:                              #   in Loop: Header=BB39_27 Depth=2
	movq	316976(%r13), %rdx
	movq	(%rdx), %rbp
	movq	(%rbp,%r11,8), %rbp
	movslq	%esi, %rsi
	movq	(%rbp,%rsi,8), %rbp
	movzwl	(%rbp), %r9d
	movq	1616(%rdi), %rcx
	movq	(%rcx), %rbx
	movq	(%rbx,%r8,8), %rbx
	movq	(%rbx,%rax,8), %rbx
	movw	%r9w, (%rbx)
	movzwl	2(%rbp), %ebp
	movw	%bp, 2(%rbx)
	movq	8(%rdx), %rdx
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movzwl	(%rdx), %ebp
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movw	%bp, (%rcx)
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rcx)
	movq	316952(%r13), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movzbl	(%rcx,%rsi), %edx
	movq	1600(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r8,8), %rbx
	cmpb	$-1, %dl
	je	.LBB39_38
# BB#35:                                #   in Loop: Header=BB39_27 Depth=2
	movb	%dl, (%rbx,%rax)
	movq	316968(%r13), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%rsi,8), %rbp
	jmp	.LBB39_39
.LBB39_36:                              # %._crit_edge1203
                                        #   in Loop: Header=BB39_27 Depth=2
	cmpb	$-1, %dl
	jne	.LBB39_57
# BB#49:                                #   in Loop: Header=BB39_27 Depth=2
	movq	8(%rsi), %rcx
	movq	(%rcx,%r8,8), %rcx
	cmpb	$0, (%rcx,%rax)
	jne	.LBB39_57
# BB#53:                                #   in Loop: Header=BB39_27 Depth=2
	movq	4848(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rdx
	movswl	(%rdx), %ecx
	movl	%ecx, %esi
	negl	%esi
	cmovll	%ecx, %esi
	cmpl	$1, %esi
	ja	.LBB39_57
# BB#54:                                #   in Loop: Header=BB39_27 Depth=2
	movswl	2(%rdx), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$2, %edx
	setb	%sil
	jmp	.LBB39_58
	.p2align	4, 0x90
.LBB39_57:                              #   in Loop: Header=BB39_27 Depth=2
	xorl	%esi, %esi
.LBB39_58:                              #   in Loop: Header=BB39_27 Depth=2
	xorb	$1, %sil
	movq	4856(%rdi), %rcx
	movq	(%rcx,%r8,8), %rcx
	movb	%sil, (%rcx,%rax)
.LBB39_59:                              #   in Loop: Header=BB39_27 Depth=2
	movq	316976(%r12), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%r9,8), %rdx
	movzwl	(%rdx), %esi
	movq	3232(%rdi), %rbx
	movq	(%rbx), %rbp
	movq	(%rbp,%r8,8), %rbp
	movq	(%rbp,%rax,8), %rbp
	movw	%si, (%rbp)
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rbp)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %edx
	movq	8(%rbx), %rsi
	movq	(%rsi,%r8,8), %rsi
	movq	(%rsi,%rax,8), %rsi
	movw	%dx, (%rsi)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rsi)
	movq	316952(%r12), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	3216(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movb	%cl, (%rdx,%rax)
	movq	316952(%r12), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	3216(%rdi), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movb	%cl, (%rdx,%rax)
	movq	316968(%r12), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%r9,8), %rdx
	movq	3224(%rdi), %rsi
	movq	(%rsi), %rbx
	movq	(%rbx,%r8,8), %rbx
	movq	%rdx, (%rbx,%rax,8)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movq	8(%rsi), %rdx
	movq	(%rdx,%r8,8), %rdx
	movq	%rcx, (%rdx,%rax,8)
	movq	img(%rip), %rcx
	movl	40(%rcx), %edx
	cmpl	$1, %edx
	jne	.LBB39_73
# BB#60:                                #   in Loop: Header=BB39_27 Depth=2
	movq	3216(%rdi), %rsi
	movq	(%rsi), %rcx
	movq	(%rcx,%r8,8), %rcx
	movzbl	(%rcx,%rax), %edx
	cmpl	$0, 316844(%r12)
	jne	.LBB39_65
# BB#61:                                #   in Loop: Header=BB39_27 Depth=2
	testb	%dl, %dl
	jne	.LBB39_65
# BB#62:                                #   in Loop: Header=BB39_27 Depth=2
	movq	3232(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rsi
	movswl	(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$1, %edx
	ja	.LBB39_71
# BB#63:                                #   in Loop: Header=BB39_27 Depth=2
	movswl	2(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movb	$1, %sil
	cmpl	$2, %edx
	jb	.LBB39_72
	jmp	.LBB39_71
.LBB39_65:                              # %._crit_edge1209
                                        #   in Loop: Header=BB39_27 Depth=2
	cmpb	$-1, %dl
	jne	.LBB39_71
# BB#67:                                #   in Loop: Header=BB39_27 Depth=2
	movq	8(%rsi), %rcx
	movq	(%rcx,%r8,8), %rcx
	cmpb	$0, (%rcx,%rax)
	jne	.LBB39_71
# BB#69:                                #   in Loop: Header=BB39_27 Depth=2
	movq	3232(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rdx
	movswl	(%rdx), %ecx
	movl	%ecx, %esi
	negl	%esi
	cmovll	%ecx, %esi
	cmpl	$1, %esi
	ja	.LBB39_71
# BB#70:                                #   in Loop: Header=BB39_27 Depth=2
	movswl	2(%rdx), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$2, %edx
	setb	%sil
	jmp	.LBB39_72
	.p2align	4, 0x90
.LBB39_71:                              #   in Loop: Header=BB39_27 Depth=2
	xorl	%esi, %esi
.LBB39_72:                              #   in Loop: Header=BB39_27 Depth=2
	xorb	$1, %sil
	movq	3240(%rdi), %rcx
	movq	(%rcx,%r8,8), %rcx
	movb	%sil, (%rcx,%rax)
	movq	img(%rip), %rcx
	movl	40(%rcx), %edx
.LBB39_73:                              #   in Loop: Header=BB39_27 Depth=2
	testl	%edx, %edx
	jne	.LBB39_78
# BB#74:                                #   in Loop: Header=BB39_27 Depth=2
	movq	316992(%r13), %rcx
	movq	(%rcx,%r10,8), %rcx
	cmpb	$0, (%rcx,%rax)
	jne	.LBB39_78
# BB#75:                                #   in Loop: Header=BB39_27 Depth=2
	movq	3232(%rdi), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movq	(%rdx,%rax,8), %rdx
	movzwl	2(%rdx), %ebp
	movl	%ebp, %esi
	shrl	$15, %esi
	addl	%ebp, %esi
	sarw	%si
	movw	%si, 2(%rdx)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movzwl	2(%rcx), %edx
	movl	%edx, %esi
	shrl	$15, %esi
	addl	%edx, %esi
	sarw	%si
	movw	%si, 2(%rcx)
	movq	4848(%rdi), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movq	(%rdx,%rax,8), %rdx
	movzwl	2(%rdx), %ebp
	movl	%ebp, %esi
	shrl	$15, %esi
	addl	%ebp, %esi
	sarw	%si
	movw	%si, 2(%rdx)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movzwl	2(%rcx), %edx
	movl	%edx, %esi
	shrl	$15, %esi
	addl	%edx, %esi
	sarw	%si
	movw	%si, 2(%rcx)
	jmp	.LBB39_78
.LBB39_38:                              #   in Loop: Header=BB39_27 Depth=2
	movb	$-1, (%rbx,%rax)
	movq	$-1, %rbp
.LBB39_39:                              #   in Loop: Header=BB39_27 Depth=2
	movq	1608(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	%rbp, (%rcx,%rax,8)
	movq	316952(%r13), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movzbl	(%rcx,%rsi), %edx
	movq	1600(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rbx
	cmpb	$-1, %dl
	je	.LBB39_41
# BB#40:                                #   in Loop: Header=BB39_27 Depth=2
	movb	%dl, (%rbx,%rax)
	movq	316968(%r13), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%rsi,8), %rsi
	jmp	.LBB39_42
.LBB39_41:                              #   in Loop: Header=BB39_27 Depth=2
	movb	$-1, (%rbx,%rax)
	movq	$-1, %rsi
.LBB39_42:                              #   in Loop: Header=BB39_27 Depth=2
	movq	1608(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	%rsi, (%rcx,%rax,8)
	movzbl	316844(%r13), %ebx
	movb	%bl, 4864(%rdi)
	movq	img(%rip), %rcx
	cmpl	$1, 40(%rcx)
	jne	.LBB39_78
# BB#43:                                #   in Loop: Header=BB39_27 Depth=2
	movq	1600(%rdi), %rsi
	movq	(%rsi), %rcx
	movq	(%rcx,%r8,8), %rcx
	movzbl	(%rcx,%rax), %edx
	orb	%dl, %bl
	je	.LBB39_46
# BB#44:                                # %._crit_edge1219
                                        #   in Loop: Header=BB39_27 Depth=2
	cmpb	$-1, %dl
	jne	.LBB39_76
# BB#51:                                #   in Loop: Header=BB39_27 Depth=2
	movq	8(%rsi), %rcx
	movq	(%rcx,%r8,8), %rcx
	cmpb	$0, (%rcx,%rax)
	jne	.LBB39_76
# BB#55:                                #   in Loop: Header=BB39_27 Depth=2
	movq	1616(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rsi
	movswl	(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$1, %edx
	ja	.LBB39_76
# BB#56:                                #   in Loop: Header=BB39_27 Depth=2
	movswl	2(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$2, %edx
	setb	%sil
	jmp	.LBB39_77
.LBB39_46:                              #   in Loop: Header=BB39_27 Depth=2
	movq	1616(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rax,8), %rsi
	movswl	(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$1, %edx
	ja	.LBB39_76
# BB#47:                                #   in Loop: Header=BB39_27 Depth=2
	movswl	2(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movb	$1, %sil
	cmpl	$2, %edx
	jb	.LBB39_77
.LBB39_76:                              #   in Loop: Header=BB39_27 Depth=2
	xorl	%esi, %esi
.LBB39_77:                              #   in Loop: Header=BB39_27 Depth=2
	xorb	$1, %sil
	movq	1624(%rdi), %rcx
	movq	(%rcx,%r8,8), %rcx
	movb	%sil, (%rcx,%rax)
	.p2align	4, 0x90
.LBB39_78:                              #   in Loop: Header=BB39_27 Depth=2
	incq	%rax
	movl	316864(%r13), %esi
	movl	%esi, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%esi, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rax
	jl	.LBB39_27
# BB#79:                                # %._crit_edge928.loopexit
                                        #   in Loop: Header=BB39_25 Depth=1
	movl	316868(%r13), %eax
.LBB39_80:                              # %._crit_edge928
                                        #   in Loop: Header=BB39_25 Depth=1
	incq	%r8
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	sarl	$3, %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %r8
	jl	.LBB39_25
.LBB39_81:                              # %.loopexit894
	movq	active_sps(%rip), %rcx
	movl	2076(%rcx), %edx
	testl	%edx, %edx
	je	.LBB39_83
# BB#82:
	cmpl	$0, 2084(%rcx)
	je	.LBB39_94
.LBB39_83:
	movq	img(%rip), %rax
	cmpl	$0, 5584(%rax)
	jne	.LBB39_93
# BB#84:                                # %.preheader891
	cmpl	$4, 316868(%r13)
	jl	.LBB39_93
# BB#85:                                # %.lr.ph924
	leaq	317008(%r13), %r9
	leaq	317000(%r13), %r8
	movl	316864(%r13), %eax
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB39_86:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_88 Depth 2
	cmpl	$4, %eax
	jl	.LBB39_91
# BB#87:                                # %.lr.ph921
                                        #   in Loop: Header=BB39_86 Depth=1
	movl	%r15d, %ecx
	shrl	$31, %ecx
	addl	%r15d, %ecx
	sarl	%ecx
	movl	%r15d, %esi
	sarl	$31, %esi
	shrl	$29, %esi
	addl	%r15d, %esi
	sarl	$3, %esi
	leal	(%rcx,%rsi,4), %r11d
	leal	4(%r11), %r10d
	movslq	%ecx, %r14
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB39_88:                              #   Parent Loop BB39_86 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	316992(%r13), %rbp
	movq	(%rbp,%r15,8), %rbp
	cmpb	$0, (%rbp,%rcx)
	je	.LBB39_90
# BB#89:                                #   in Loop: Header=BB39_88 Depth=2
	movq	dec_picture(%rip), %rax
	movl	4(%rax), %edx
	movq	317000(%r13), %rbp
	movq	317008(%r13), %rsi
	movl	%edx, %ebx
	subl	4(%rsi), %ebx
	movl	%ebx, %eax
	negl	%eax
	cmovll	%ebx, %eax
	subl	4(%rbp), %edx
	movl	%edx, %ebx
	negl	%ebx
	cmovll	%edx, %ebx
	cmpl	%ebx, %eax
	cmovgq	%rbp, %rsi
	movq	%r9, %r12
	cmovgq	%r8, %r12
	movq	316976(%rsi), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movzwl	(%rdx), %edx
	movq	1616(%rdi), %rsi
	movq	(%rsi), %rbx
	movq	(%rbx,%r15,8), %rbx
	movq	(%rbx,%rcx,8), %rbx
	movw	%dx, (%rbx)
	movq	(%r12), %rdx
	movq	316976(%rdx), %rbp
	movq	(%rbp), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%rcx,8), %rax
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rbx)
	movl	%r10d, %eax
	cmovgl	%r11d, %eax
	movslq	%eax, %rbx
	movq	8(%rbp), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%rcx,8), %rax
	movzwl	(%rax), %ebp
	movq	8(%rsi), %rsi
	movq	(%rsi,%r15,8), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movw	%bp, (%rsi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rsi)
	movq	316952(%rdx), %rax
	movq	(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movzbl	(%rax,%rcx), %eax
	movq	1600(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r15,8), %rdx
	movb	%al, (%rdx,%rcx)
	movq	(%r12), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movzbl	(%rax,%rcx), %eax
	movq	1600(%rdi), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r15,8), %rdx
	movb	%al, (%rdx,%rcx)
	movq	316968(%r13), %rax
	movq	(%rax), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	1608(%rdi), %rsi
	movq	(%rsi), %rbp
	movq	(%rbp,%r15,8), %rbp
	movq	%rdx, (%rbp,%rcx,8)
	movq	8(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax,%rcx,8), %rax
	movq	8(%rsi), %rdx
	movq	(%rdx,%r15,8), %rdx
	movq	%rax, (%rdx,%rcx,8)
	movq	(%r12), %rax
	movzbl	316844(%rax), %eax
	movb	%al, 4864(%rdi)
	movl	316864(%r13), %eax
.LBB39_90:                              #   in Loop: Header=BB39_88 Depth=2
	incq	%rcx
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	addl	%eax, %edx
	sarl	$2, %edx
	movslq	%edx, %rdx
	cmpq	%rdx, %rcx
	jl	.LBB39_88
.LBB39_91:                              # %._crit_edge922
                                        #   in Loop: Header=BB39_86 Depth=1
	incq	%r15
	movl	316868(%r13), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	addl	%ecx, %edx
	sarl	$2, %edx
	movslq	%edx, %rcx
	cmpq	%rcx, %r15
	jl	.LBB39_86
# BB#92:                                # %.loopexit892.loopexit
	movq	active_sps(%rip), %rcx
	movl	2076(%rcx), %edx
.LBB39_93:                              # %.loopexit892
	movb	316844(%r13), %bl
	leaq	4864(%rdi), %r8
	movb	%bl, 4864(%rdi)
	testl	%edx, %edx
	jne	.LBB39_95
	jmp	.LBB39_96
.LBB39_94:                              # %.loopexit892.thread
	movb	316844(%r13), %dl
	leaq	4864(%rdi), %r8
	movb	%dl, 4864(%rdi)
.LBB39_95:
	cmpl	$0, 2084(%rcx)
	je	.LBB39_116
.LBB39_96:                              # %.preheader886
	movl	316868(%r13), %edx
	cmpl	$4, %edx
	jl	.LBB39_136
# BB#97:                                # %.lr.ph914
	movl	316864(%r13), %esi
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB39_98:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_100 Depth 2
	cmpl	$4, %esi
	jl	.LBB39_115
# BB#99:                                # %.lr.ph911
                                        #   in Loop: Header=BB39_98 Depth=1
	movl	%r10d, %edx
	orl	$1, %edx
	movl	%r10d, %esi
	andl	$-2, %esi
	testb	$2, %r10b
	cmovneq	%rdx, %rsi
	movslq	%esi, %r9
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB39_100:                             #   Parent Loop BB39_98 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rsi
	orq	$1, %rsi
	movl	%eax, %ebp
	andl	$-2, %ebp
	testb	$2, %al
	cmovneq	%rsi, %rbp
	movq	1616(%rdi), %rbx
	movq	(%rbx), %rdx
	movq	(%rdx,%r9,8), %rcx
	movslq	%ebp, %rsi
	movq	(%rcx,%rsi,8), %rcx
	movzwl	(%rcx), %ebp
	movq	(%rdx,%r10,8), %rdx
	movq	(%rdx,%rax,8), %rdx
	movw	%bp, (%rdx)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rdx)
	movq	8(%rbx), %rcx
	movq	(%rcx,%r9,8), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movzwl	(%rdx), %ebp
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movw	%bp, (%rcx)
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rcx)
	movq	1600(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r9,8), %rdx
	movzbl	(%rdx,%rsi), %edx
	movq	(%rcx,%r10,8), %rcx
	movb	%dl, (%rcx,%rax)
	movq	1600(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r9,8), %rdx
	movzbl	(%rdx,%rsi), %edx
	movq	(%rcx,%r10,8), %rcx
	movb	%dl, (%rcx,%rax)
	movq	1608(%rdi), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%r9,8), %rbp
	movq	(%rbp,%rsi,8), %rbp
	movq	(%rdx,%r10,8), %rdx
	movq	%rbp, (%rdx,%rax,8)
	movq	8(%rcx), %rcx
	movq	(%rcx,%r9,8), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movq	(%rcx,%r10,8), %rcx
	movq	%rdx, (%rcx,%rax,8)
	movq	img(%rip), %rcx
	cmpl	$1, 40(%rcx)
	jne	.LBB39_113
# BB#101:                               #   in Loop: Header=BB39_100 Depth=2
	movq	1600(%rdi), %rsi
	movq	(%rsi), %rcx
	movq	(%rcx,%r10,8), %rcx
	movzbl	(%rcx,%rax), %ebx
	movzbl	(%r8), %ecx
	orb	%bl, %cl
	je	.LBB39_104
# BB#102:                               # %._crit_edge1242
                                        #   in Loop: Header=BB39_100 Depth=2
	cmpb	$-1, %bl
	jne	.LBB39_111
# BB#107:                               #   in Loop: Header=BB39_100 Depth=2
	movq	8(%rsi), %rcx
	movq	(%rcx,%r10,8), %rcx
	cmpb	$0, (%rcx,%rax)
	jne	.LBB39_111
# BB#109:                               #   in Loop: Header=BB39_100 Depth=2
	movq	1616(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%rax,8), %rsi
	movswl	(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$1, %edx
	ja	.LBB39_111
# BB#110:                               #   in Loop: Header=BB39_100 Depth=2
	movswl	2(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$2, %edx
	setb	%sil
	jmp	.LBB39_112
.LBB39_104:                             #   in Loop: Header=BB39_100 Depth=2
	movq	1616(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%rax,8), %rsi
	movswl	(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$1, %edx
	ja	.LBB39_111
# BB#105:                               #   in Loop: Header=BB39_100 Depth=2
	movswl	2(%rsi), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movb	$1, %sil
	cmpl	$2, %edx
	jb	.LBB39_112
	.p2align	4, 0x90
.LBB39_111:                             #   in Loop: Header=BB39_100 Depth=2
	xorl	%esi, %esi
.LBB39_112:                             #   in Loop: Header=BB39_100 Depth=2
	xorb	$1, %sil
	movq	1624(%rdi), %rcx
	movq	(%rcx,%r10,8), %rcx
	movb	%sil, (%rcx,%rax)
.LBB39_113:                             #   in Loop: Header=BB39_100 Depth=2
	incq	%rax
	movl	316864(%r13), %esi
	movl	%esi, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%esi, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rax
	jl	.LBB39_100
# BB#114:                               # %._crit_edge912.loopexit
                                        #   in Loop: Header=BB39_98 Depth=1
	movl	316868(%r13), %edx
.LBB39_115:                             # %._crit_edge912
                                        #   in Loop: Header=BB39_98 Depth=1
	incq	%r10
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%edx, %eax
	sarl	$2, %eax
	cltq
	cmpq	%rax, %r10
	jl	.LBB39_98
	jmp	.LBB39_136
.LBB39_116:                             # %.preheader889
	movl	316868(%r13), %edx
	cmpl	$4, %edx
	jl	.LBB39_136
# BB#117:                               # %.preheader888.lr.ph
	movl	316864(%r13), %esi
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB39_118:                             # %.preheader888
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_120 Depth 2
	cmpl	$4, %esi
	jl	.LBB39_135
# BB#119:                               # %.lr.ph916.preheader
                                        #   in Loop: Header=BB39_118 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB39_120:                             # %.lr.ph916
                                        #   Parent Loop BB39_118 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	316976(%r13), %rdx
	movq	(%rdx), %rsi
	movq	(%rsi,%r8,8), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movzwl	(%rsi), %ebp
	movq	1616(%rdi), %rbx
	movq	(%rbx), %rax
	movq	(%rax,%r8,8), %rax
	movq	(%rax,%rcx,8), %rax
	movw	%bp, (%rax)
	movzwl	2(%rsi), %esi
	movw	%si, 2(%rax)
	movq	8(%rdx), %rax
	movq	(%rax,%r8,8), %rax
	movq	(%rax,%rcx,8), %rax
	movzwl	(%rax), %edx
	movq	8(%rbx), %rsi
	movq	(%rsi,%r8,8), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movw	%dx, (%rsi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rsi)
	movq	316952(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movzbl	(%rax,%rcx), %eax
	movq	1600(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movb	%al, (%rdx,%rcx)
	movq	316952(%r13), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movzbl	(%rax,%rcx), %eax
	movq	1600(%rdi), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r8,8), %rdx
	movb	%al, (%rdx,%rcx)
	movq	316968(%r13), %rax
	movq	(%rax), %rdx
	movq	(%rdx,%r8,8), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	1608(%rdi), %rsi
	movq	(%rsi), %rbp
	movq	(%rbp,%r8,8), %rbp
	movq	%rdx, (%rbp,%rcx,8)
	movq	8(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movq	(%rax,%rcx,8), %rax
	movq	8(%rsi), %rdx
	movq	(%rdx,%r8,8), %rdx
	movq	%rax, (%rdx,%rcx,8)
	movq	img(%rip), %rax
	cmpl	$1, 40(%rax)
	jne	.LBB39_133
# BB#121:                               #   in Loop: Header=BB39_120 Depth=2
	movq	1600(%rdi), %rdx
	movq	(%rdx), %rax
	movq	(%rax,%r8,8), %rax
	movzbl	(%rax,%rcx), %ebx
	movzbl	4864(%rdi), %eax
	orb	%bl, %al
	je	.LBB39_124
# BB#122:                               # %._crit_edge1233
                                        #   in Loop: Header=BB39_120 Depth=2
	cmpb	$-1, %bl
	jne	.LBB39_131
# BB#127:                               #   in Loop: Header=BB39_120 Depth=2
	movq	8(%rdx), %rax
	movq	(%rax,%r8,8), %rax
	cmpb	$0, (%rax,%rcx)
	jne	.LBB39_131
# BB#129:                               #   in Loop: Header=BB39_120 Depth=2
	movq	1616(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movq	(%rax,%rcx,8), %rdx
	movswl	(%rdx), %eax
	movl	%eax, %esi
	negl	%esi
	cmovll	%eax, %esi
	cmpl	$1, %esi
	ja	.LBB39_131
# BB#130:                               #   in Loop: Header=BB39_120 Depth=2
	movswl	2(%rdx), %eax
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	cmpl	$2, %edx
	setb	%dl
	jmp	.LBB39_132
.LBB39_124:                             #   in Loop: Header=BB39_120 Depth=2
	movq	1616(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movq	(%rax,%rcx,8), %rdx
	movswl	(%rdx), %eax
	movl	%eax, %esi
	negl	%esi
	cmovll	%eax, %esi
	cmpl	$1, %esi
	ja	.LBB39_131
# BB#125:                               #   in Loop: Header=BB39_120 Depth=2
	movswl	2(%rdx), %eax
	movl	%eax, %esi
	negl	%esi
	cmovll	%eax, %esi
	movb	$1, %dl
	cmpl	$2, %esi
	jb	.LBB39_132
	.p2align	4, 0x90
.LBB39_131:                             #   in Loop: Header=BB39_120 Depth=2
	xorl	%edx, %edx
.LBB39_132:                             #   in Loop: Header=BB39_120 Depth=2
	xorb	$1, %dl
	movq	1624(%rdi), %rax
	movq	(%rax,%r8,8), %rax
	movb	%dl, (%rax,%rcx)
.LBB39_133:                             #   in Loop: Header=BB39_120 Depth=2
	incq	%rcx
	movl	316864(%r13), %esi
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%esi, %eax
	sarl	$2, %eax
	cltq
	cmpq	%rax, %rcx
	jl	.LBB39_120
# BB#134:                               # %._crit_edge917.loopexit
                                        #   in Loop: Header=BB39_118 Depth=1
	movl	316868(%r13), %edx
.LBB39_135:                             # %._crit_edge917
                                        #   in Loop: Header=BB39_118 Depth=1
	incq	%r8
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%edx, %eax
	sarl	$2, %eax
	cltq
	cmpq	%rax, %r8
	jl	.LBB39_118
.LBB39_136:                             # %.loopexit887
	movq	img(%rip), %r10
	cmpl	$0, 40(%r10)
	jne	.LBB39_171
# BB#137:                               # %.preheader885
	cmpl	$4, %edx
	jl	.LBB39_150
# BB#138:                               # %.preheader884.lr.ph
	movl	316864(%r13), %r8d
	movl	%r8d, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%r8d, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %r11
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%edx, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %r9
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB39_139:                             # %.preheader884
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_141 Depth 2
	cmpl	$4, %r8d
	jl	.LBB39_149
# BB#140:                               # %.lr.ph906
                                        #   in Loop: Header=BB39_139 Depth=1
	movl	5624(%r10), %ebx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB39_141:                             #   Parent Loop BB39_139 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebx, %ebx
	je	.LBB39_144
.LBB39_142:                             #   in Loop: Header=BB39_141 Depth=2
	movq	316992(%r13), %rdx
	movq	(%rdx,%rbp,8), %rdx
	cmpb	$0, (%rdx,%rcx)
	je	.LBB39_145
# BB#143:                               #   in Loop: Header=BB39_141 Depth=2
	movq	1616(%rdi), %rdx
	movq	(%rdx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%rcx,8), %rax
	shlw	2(%rax)
	movq	8(%rdx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%rcx,8), %rax
	shlw	2(%rax)
	jmp	.LBB39_148
	.p2align	4, 0x90
.LBB39_144:                             #   in Loop: Header=BB39_141 Depth=2
	cmpl	$0, 5584(%r10)
	je	.LBB39_142
.LBB39_145:                             # %.thread1264
                                        #   in Loop: Header=BB39_141 Depth=2
	cmpl	$0, 5584(%r10)
	je	.LBB39_148
# BB#146:                               #   in Loop: Header=BB39_141 Depth=2
	movq	316992(%r13), %rax
	movq	(%rax,%rbp,8), %rax
	cmpb	$0, (%rax,%rcx)
	jne	.LBB39_148
# BB#147:                               #   in Loop: Header=BB39_141 Depth=2
	movq	1616(%rdi), %r14
	movq	(%r14), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movzwl	2(%rdx), %eax
	movl	%eax, %esi
	shrl	$15, %esi
	addl	%eax, %esi
	sarw	%si
	movw	%si, 2(%rdx)
	movq	8(%r14), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%rcx,8), %rax
	movzwl	2(%rax), %edx
	movl	%edx, %esi
	shrl	$15, %esi
	addl	%edx, %esi
	sarw	%si
	movw	%si, 2(%rax)
	.p2align	4, 0x90
.LBB39_148:                             #   in Loop: Header=BB39_141 Depth=2
	incq	%rcx
	cmpq	%r11, %rcx
	jl	.LBB39_141
.LBB39_149:                             # %._crit_edge907
                                        #   in Loop: Header=BB39_139 Depth=1
	incq	%rbp
	cmpq	%r9, %rbp
	jl	.LBB39_139
.LBB39_150:                             # %.preheader883
	movl	5624(%r10), %eax
	leal	2(,%rax,4), %eax
	testl	%eax, %eax
	jle	.LBB39_171
# BB#151:                               # %.preheader.lr.ph
	movq	dec_picture(%rip), %r10
	movq	img(%rip), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	leaq	616(%rax), %r9
	xorl	%edi, %edi
	movl	$-128, %r12d
	movl	$127, %r11d
	movl	$-1024, %r14d           # imm = 0xFC00
	movl	$1023, %r15d            # imm = 0x3FF
	.p2align	4, 0x90
.LBB39_152:                             # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_166 Depth 2
                                        #     Child Loop BB39_156 Depth 2
                                        #     Child Loop BB39_161 Depth 2
	cmpl	$0, listXsize(,%rdi,4)
	jle	.LBB39_170
# BB#153:                               # %.lr.ph
                                        #   in Loop: Header=BB39_152 Depth=1
	movq	-56(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rdi,8), %r13
	movq	%rdi, %rax
	orq	$1, %rax
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %rbp
	testl	%edi, %edi
	je	.LBB39_160
# BB#154:                               # %.lr.ph
                                        #   in Loop: Header=BB39_152 Depth=1
	cmpl	$2, %edi
	jne	.LBB39_165
# BB#155:                               # %.lr.ph.split.split.us.preheader
                                        #   in Loop: Header=BB39_152 Depth=1
	movq	%r9, %rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB39_156:                             # %.lr.ph.split.split.us
                                        #   Parent Loop BB39_152 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%r10), %ebx
	movq	(%r13,%rcx,8), %rax
	movl	4(%rax), %eax
	subl	%eax, %ebx
	cmpl	$-129, %ebx
	cmovlel	%r12d, %ebx
	movl	4(%rbp), %r8d
	subl	%eax, %r8d
	cmpl	$-129, %r8d
	cmovlel	%r12d, %r8d
	cmpl	$128, %r8d
	cmovgel	%r11d, %r8d
	testl	%r8d, %r8d
	je	.LBB39_158
# BB#157:                               #   in Loop: Header=BB39_156 Depth=2
	cmpl	$128, %ebx
	cmovgel	%r11d, %ebx
	movl	%r8d, %edx
	shrl	$31, %edx
	addl	%r8d, %edx
	sarl	%edx
	movl	%edx, %eax
	negl	%eax
	cmpl	$-1, %r8d
	cmovgel	%edx, %eax
	addl	$16384, %eax            # imm = 0x4000
	cltd
	idivl	%r8d
	imull	%ebx, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-1025, %eax            # imm = 0xFBFF
	cmovlel	%r14d, %eax
	cmpl	$1024, %eax             # imm = 0x400
	cmovgel	%r15d, %eax
	jmp	.LBB39_159
	.p2align	4, 0x90
.LBB39_158:                             #   in Loop: Header=BB39_156 Depth=2
	movl	$9999, %eax             # imm = 0x270F
.LBB39_159:                             #   in Loop: Header=BB39_156 Depth=2
	movl	%eax, (%rsi)
	incq	%rcx
	movslq	listXsize(,%rdi,4), %rax
	addq	$4, %rsi
	cmpq	%rax, %rcx
	jl	.LBB39_156
	jmp	.LBB39_170
	.p2align	4, 0x90
.LBB39_160:                             # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB39_152 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB39_161:                             # %.lr.ph.split.us
                                        #   Parent Loop BB39_152 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%r10), %esi
	movq	(%r13,%rcx,8), %rax
	movl	4(%rax), %eax
	subl	%eax, %esi
	cmpl	$-129, %esi
	cmovlel	%r12d, %esi
	movl	4(%rbp), %ebx
	subl	%eax, %ebx
	cmpl	$-129, %ebx
	cmovlel	%r12d, %ebx
	cmpl	$128, %ebx
	cmovgel	%r11d, %ebx
	testl	%ebx, %ebx
	je	.LBB39_163
# BB#162:                               #   in Loop: Header=BB39_161 Depth=2
	cmpl	$128, %esi
	cmovgel	%r11d, %esi
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
	movl	%edx, %eax
	negl	%eax
	cmpl	$-1, %ebx
	cmovgel	%edx, %eax
	addl	$16384, %eax            # imm = 0x4000
	cltd
	idivl	%ebx
	imull	%esi, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-1025, %eax            # imm = 0xFBFF
	cmovlel	%r14d, %eax
	cmpl	$1024, %eax             # imm = 0x400
	cmovgel	%r15d, %eax
	jmp	.LBB39_164
	.p2align	4, 0x90
.LBB39_163:                             #   in Loop: Header=BB39_161 Depth=2
	movl	$9999, %eax             # imm = 0x270F
.LBB39_164:                             #   in Loop: Header=BB39_161 Depth=2
	movl	%eax, (%r9,%rcx,4)
	incq	%rcx
	movslq	listXsize(,%rdi,4), %rax
	cmpq	%rax, %rcx
	jl	.LBB39_161
	jmp	.LBB39_170
	.p2align	4, 0x90
.LBB39_165:                             # %.lr.ph.split.split.preheader
                                        #   in Loop: Header=BB39_152 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB39_166:                             # %.lr.ph.split.split
                                        #   Parent Loop BB39_152 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	12(%r10), %esi
	movq	(%r13,%rcx,8), %rax
	movl	4(%rax), %eax
	subl	%eax, %esi
	cmpl	$-129, %esi
	cmovlel	%r12d, %esi
	movl	4(%rbp), %ebx
	subl	%eax, %ebx
	cmpl	$-129, %ebx
	cmovlel	%r12d, %ebx
	cmpl	$128, %ebx
	cmovgel	%r11d, %ebx
	testl	%ebx, %ebx
	je	.LBB39_168
# BB#167:                               #   in Loop: Header=BB39_166 Depth=2
	cmpl	$128, %esi
	cmovgel	%r11d, %esi
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
	movl	%edx, %eax
	negl	%eax
	cmpl	$-1, %ebx
	cmovgel	%edx, %eax
	addl	$16384, %eax            # imm = 0x4000
	cltd
	idivl	%ebx
	imull	%esi, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-1025, %eax            # imm = 0xFBFF
	cmovlel	%r14d, %eax
	cmpl	$1024, %eax             # imm = 0x400
	cmovgel	%r15d, %eax
	jmp	.LBB39_169
	.p2align	4, 0x90
.LBB39_168:                             #   in Loop: Header=BB39_166 Depth=2
	movl	$9999, %eax             # imm = 0x270F
.LBB39_169:                             #   in Loop: Header=BB39_166 Depth=2
	movl	%eax, (%r9,%rcx,4)
	incq	%rcx
	movslq	listXsize(,%rdi,4), %rax
	cmpq	%rax, %rcx
	jl	.LBB39_166
.LBB39_170:                             # %._crit_edge
                                        #   in Loop: Header=BB39_152 Depth=1
	addq	$2, %rdi
	movq	-96(%rsp), %rax         # 8-byte Reload
	movl	5624(%rax), %eax
	leal	2(,%rax,4), %eax
	cltq
	addq	$256, %r9               # imm = 0x100
	cmpq	%rax, %rdi
	jl	.LBB39_152
.LBB39_171:                             # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB39_172:
	movq	317008(%r13), %r14
.LBB39_173:
	movq	%r14, %r12
	movq	%r14, %r13
	jmp	.LBB39_8
.Lfunc_end39:
	.size	compute_colocated, .Lfunc_end39-compute_colocated
	.cfi_endproc

	.p2align	4, 0x90
	.type	unmark_long_term_field_for_reference_by_frame_idx,@function
unmark_long_term_field_for_reference_by_frame_idx: # @unmark_long_term_field_for_reference_by_frame_idx
	.cfi_startproc
# BB#0:
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	testl	%r8d, %r8d
	jns	.LBB40_2
# BB#1:
	movq	img(%rip), %rax
	movl	5816(%rax), %eax
	leal	(%r8,%rax,2), %r8d
.LBB40_2:
	pushq	%rbp
.Lcfi179:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi180:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi181:
	.cfi_def_cfa_offset 32
.Lcfi182:
	.cfi_offset %rbx, -32
.Lcfi183:
	.cfi_offset %r14, -24
.Lcfi184:
	.cfi_offset %rbp, -16
	movl	dpb+36(%rip), %r11d
	testq	%r11, %r11
	je	.LBB40_46
# BB#3:                                 # %.lr.ph
	movq	dpb+16(%rip), %r9
	movl	%r8d, %r10d
	shrl	$31, %r10d
	addl	%r8d, %r10d
	sarl	%r10d
	movq	dpb+56(%rip), %r8
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB40_4:                               # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rax,8), %rbx
	cmpl	%esi, 32(%rbx)
	jne	.LBB40_45
# BB#5:                                 #   in Loop: Header=BB40_4 Depth=1
	cmpl	$2, %edi
	je	.LBB40_30
# BB#6:                                 #   in Loop: Header=BB40_4 Depth=1
	cmpl	$1, %edi
	jne	.LBB40_45
# BB#7:                                 #   in Loop: Header=BB40_4 Depth=1
	movl	8(%rbx), %ebp
	cmpl	$1, %ebp
	je	.LBB40_14
# BB#8:                                 #   in Loop: Header=BB40_4 Depth=1
	cmpl	$3, %ebp
	je	.LBB40_14
# BB#9:                                 #   in Loop: Header=BB40_4 Depth=1
	testl	%edx, %edx
	je	.LBB40_13
# BB#10:                                #   in Loop: Header=BB40_4 Depth=1
	testq	%r8, %r8
	je	.LBB40_14
# BB#11:                                #   in Loop: Header=BB40_4 Depth=1
	cmpq	%rbx, %r8
	jne	.LBB40_14
# BB#12:                                #   in Loop: Header=BB40_4 Depth=1
	cmpl	%ecx, 20(%r8)
	jne	.LBB40_14
	jmp	.LBB40_45
.LBB40_30:                              #   in Loop: Header=BB40_4 Depth=1
	movl	8(%rbx), %ebp
	cmpl	$2, %ebp
	je	.LBB40_33
# BB#31:                                #   in Loop: Header=BB40_4 Depth=1
	cmpl	$3, %ebp
	je	.LBB40_33
	jmp	.LBB40_26
.LBB40_13:                              #   in Loop: Header=BB40_4 Depth=1
	cmpl	%r10d, 20(%rbx)
	je	.LBB40_45
.LBB40_14:                              #   in Loop: Header=BB40_4 Depth=1
	movl	(%rbx), %ebp
	testb	$1, %bpl
	je	.LBB40_17
# BB#15:                                #   in Loop: Header=BB40_4 Depth=1
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.LBB40_17
# BB#16:                                #   in Loop: Header=BB40_4 Depth=1
	movq	$0, 316844(%r14)
.LBB40_17:                              #   in Loop: Header=BB40_4 Depth=1
	testb	$2, %bpl
	je	.LBB40_20
# BB#18:                                #   in Loop: Header=BB40_4 Depth=1
	movq	64(%rbx), %r14
	testq	%r14, %r14
	je	.LBB40_20
# BB#19:                                #   in Loop: Header=BB40_4 Depth=1
	movq	$0, 316844(%r14)
.LBB40_20:                              # %thread-pre-split.i42
                                        #   in Loop: Header=BB40_4 Depth=1
	cmpl	$3, %ebp
	jne	.LBB40_25
# BB#21:                                #   in Loop: Header=BB40_4 Depth=1
	movq	56(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB40_24
# BB#22:                                #   in Loop: Header=BB40_4 Depth=1
	movq	64(%rbx), %r14
	testq	%r14, %r14
	je	.LBB40_24
# BB#23:                                #   in Loop: Header=BB40_4 Depth=1
	movl	$0, 316848(%rbp)
	movl	$0, 316844(%rbp)
	movq	$0, 316844(%r14)
.LBB40_24:                              #   in Loop: Header=BB40_4 Depth=1
	movq	48(%rbx), %rbp
	movl	$0, 316848(%rbp)
	movl	$0, 316844(%rbp)
.LBB40_25:                              # %unmark_for_long_term_reference.exit49
                                        #   in Loop: Header=BB40_4 Depth=1
	cmpl	$2, %edi
	movq	$0, 4(%rbx)
	jne	.LBB40_45
.LBB40_26:                              # %.thread52
                                        #   in Loop: Header=BB40_4 Depth=1
	testl	%edx, %edx
	je	.LBB40_32
# BB#27:                                #   in Loop: Header=BB40_4 Depth=1
	testq	%r8, %r8
	je	.LBB40_33
# BB#28:                                #   in Loop: Header=BB40_4 Depth=1
	cmpq	%rbx, %r8
	jne	.LBB40_33
# BB#29:                                #   in Loop: Header=BB40_4 Depth=1
	cmpl	%ecx, 20(%r8)
	jne	.LBB40_33
	jmp	.LBB40_45
.LBB40_32:                              #   in Loop: Header=BB40_4 Depth=1
	cmpl	%r10d, 20(%rbx)
	je	.LBB40_45
	.p2align	4, 0x90
.LBB40_33:                              #   in Loop: Header=BB40_4 Depth=1
	movl	(%rbx), %ebp
	testb	$1, %bpl
	je	.LBB40_36
# BB#34:                                #   in Loop: Header=BB40_4 Depth=1
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.LBB40_36
# BB#35:                                #   in Loop: Header=BB40_4 Depth=1
	movq	$0, 316844(%r14)
.LBB40_36:                              #   in Loop: Header=BB40_4 Depth=1
	testb	$2, %bpl
	je	.LBB40_39
# BB#37:                                #   in Loop: Header=BB40_4 Depth=1
	movq	64(%rbx), %r14
	testq	%r14, %r14
	je	.LBB40_39
# BB#38:                                #   in Loop: Header=BB40_4 Depth=1
	movq	$0, 316844(%r14)
.LBB40_39:                              # %thread-pre-split.i32
                                        #   in Loop: Header=BB40_4 Depth=1
	cmpl	$3, %ebp
	jne	.LBB40_44
# BB#40:                                #   in Loop: Header=BB40_4 Depth=1
	movq	56(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB40_43
# BB#41:                                #   in Loop: Header=BB40_4 Depth=1
	movq	64(%rbx), %r14
	testq	%r14, %r14
	je	.LBB40_43
# BB#42:                                #   in Loop: Header=BB40_4 Depth=1
	movl	$0, 316848(%rbp)
	movl	$0, 316844(%rbp)
	movq	$0, 316844(%r14)
.LBB40_43:                              #   in Loop: Header=BB40_4 Depth=1
	movq	48(%rbx), %rbp
	movl	$0, 316848(%rbp)
	movl	$0, 316844(%rbp)
.LBB40_44:                              # %unmark_for_long_term_reference.exit33
                                        #   in Loop: Header=BB40_4 Depth=1
	movq	$0, 4(%rbx)
.LBB40_45:                              # %.thread
                                        #   in Loop: Header=BB40_4 Depth=1
	incq	%rax
	cmpq	%r11, %rax
	jb	.LBB40_4
.LBB40_46:                              # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end40:
	.size	unmark_long_term_field_for_reference_by_frame_idx, .Lfunc_end40-unmark_long_term_field_for_reference_by_frame_idx
	.cfi_endproc

	.p2align	4, 0x90
	.type	remove_frame_from_dpb,@function
remove_frame_from_dpb:                  # @remove_frame_from_dpb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi185:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi186:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi187:
	.cfi_def_cfa_offset 32
.Lcfi188:
	.cfi_offset %rbx, -32
.Lcfi189:
	.cfi_offset %r14, -24
.Lcfi190:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movq	dpb(%rip), %rax
	movslq	%r14d, %rbx
	movq	(%rax,%rbx,8), %rbp
	movl	(%rbp), %eax
	cmpq	$3, %rax
	ja	.LBB41_3
# BB#1:
	jmpq	*.LJTI41_0(,%rax,8)
.LBB41_2:
	movq	56(%rbp), %rdi
	callq	free_storable_picture
	movq	$0, 56(%rbp)
	jmp	.LBB41_7
.LBB41_3:
	movl	$.L.str.30, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	jmp	.LBB41_7
.LBB41_4:
	movq	64(%rbp), %rdi
	callq	free_storable_picture
	jmp	.LBB41_6
.LBB41_5:
	movq	48(%rbp), %rdi
	callq	free_storable_picture
	movq	56(%rbp), %rdi
	callq	free_storable_picture
	movq	64(%rbp), %rdi
	callq	free_storable_picture
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rbp)
.LBB41_6:
	movq	$0, 64(%rbp)
.LBB41_7:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movq	dpb(%rip), %rdx
	movq	(%rdx,%rbx,8), %r8
	movl	dpb+28(%rip), %ecx
	decl	%ecx
	movl	%ecx, %ebp
	subl	%ebx, %ebp
	jbe	.LBB41_13
# BB#8:                                 # %.lr.ph
	movl	%r14d, %edi
	movl	%ecx, %esi
	leaq	-1(%rsi), %rbx
	subq	%rdi, %rbx
	andq	$3, %rbp
	je	.LBB41_11
# BB#9:                                 # %.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB41_10:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rdx,%rdi,8), %rax
	movq	%rax, (%rdx,%rdi,8)
	incq	%rdi
	movq	dpb(%rip), %rdx
	incq	%rbp
	jne	.LBB41_10
.LBB41_11:                              # %.prol.loopexit
	cmpq	$3, %rbx
	jb	.LBB41_14
	.p2align	4, 0x90
.LBB41_12:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rdx,%rdi,8), %rax
	movq	%rax, (%rdx,%rdi,8)
	movq	dpb(%rip), %rax
	movq	16(%rax,%rdi,8), %rdx
	movq	%rdx, 8(%rax,%rdi,8)
	movq	dpb(%rip), %rax
	movq	24(%rax,%rdi,8), %rdx
	movq	%rdx, 16(%rax,%rdi,8)
	movq	dpb(%rip), %rax
	movq	32(%rax,%rdi,8), %rdx
	movq	%rdx, 24(%rax,%rdi,8)
	leaq	4(%rdi), %rdi
	movq	dpb(%rip), %rdx
	cmpq	%rsi, %rdi
	jb	.LBB41_12
	jmp	.LBB41_14
.LBB41_13:                              # %.._crit_edge_crit_edge
	movl	%ecx, %esi
.LBB41_14:                              # %._crit_edge
	movq	%r8, (%rdx,%rsi,8)
	movl	%ecx, dpb+28(%rip)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end41:
	.size	remove_frame_from_dpb, .Lfunc_end41-remove_frame_from_dpb
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI41_0:
	.quad	.LBB41_7
	.quad	.LBB41_2
	.quad	.LBB41_4
	.quad	.LBB41_5

	.text
	.p2align	4, 0x90
	.type	output_one_frame_from_dpb,@function
output_one_frame_from_dpb:              # @output_one_frame_from_dpb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi191:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi192:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi193:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi194:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi195:
	.cfi_def_cfa_offset 48
.Lcfi196:
	.cfi_offset %rbx, -40
.Lcfi197:
	.cfi_offset %r14, -32
.Lcfi198:
	.cfi_offset %r15, -24
.Lcfi199:
	.cfi_offset %rbp, -16
	movl	dpb+28(%rip), %eax
	testl	%eax, %eax
	jne	.LBB42_4
# BB#1:
	movl	$.L.str.31, %edi
	movl	$150, %esi
	callq	error
	movl	dpb+28(%rip), %eax
	testl	%eax, %eax
	jne	.LBB42_4
# BB#2:                                 # %.thread
	movl	$.L.str.29, %edi
	movl	$150, %esi
	callq	error
	movl	dpb+28(%rip), %eax
	testl	%eax, %eax
	je	.LBB42_3
.LBB42_4:                               # %.lr.ph.i
	xorl	%ecx, %ecx
	movl	$-1, %r15d
	movl	$2147483647, %ebp       # imm = 0x7FFFFFFF
	movq	dpb(%rip), %rdx
	movl	%eax, %esi
	.p2align	4, 0x90
.LBB42_5:                               # %._crit_edge12.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edi
	movq	(%rdx,%rdi,8), %rbx
	movl	40(%rbx), %edi
	cmpl	%edi, %ebp
	jle	.LBB42_7
# BB#6:                                 #   in Loop: Header=BB42_5 Depth=1
	cmpl	$0, 36(%rbx)
	cmovel	%edi, %ebp
	cmovel	%ecx, %r15d
	cmovel	%eax, %esi
.LBB42_7:                               #   in Loop: Header=BB42_5 Depth=1
	incl	%ecx
	cmpl	%esi, %ecx
	jb	.LBB42_5
# BB#8:                                 # %get_smallest_poc.exit
	cmpl	$-1, %r15d
	jne	.LBB42_10
.LBB42_9:                               # %get_smallest_poc.exit.thread
	movl	$.L.str.32, %edi
	movl	$150, %esi
	callq	error
	movl	$-1, %r15d
.LBB42_10:
	movq	img(%rip), %rax
	cmpl	$0, 6068(%rax)
	je	.LBB42_14
# BB#11:
	cmpl	$0, dpb+40(%rip)
	jne	.LBB42_13
# BB#12:
	movl	%r15d, %edi
	callq	write_lost_ref_after_idr
.LBB42_13:
	movl	p_out(%rip), %esi
	movl	%ebp, %edi
	callq	write_lost_non_ref_pic
.LBB42_14:
	movq	dpb(%rip), %rax
	movslq	%r15d, %r14
	movq	(%rax,%r14,8), %rdi
	movl	p_out(%rip), %esi
	callq	write_stored_frame
	movq	img(%rip), %rax
	cmpl	$0, 6068(%rax)
	jne	.LBB42_17
# BB#15:
	cmpl	%ebp, dpb+40(%rip)
	jl	.LBB42_17
# BB#16:
	movl	$.L.str.33, %edi
	movl	$150, %esi
	callq	error
.LBB42_17:
	movl	%ebp, dpb+40(%rip)
	movq	dpb(%rip), %rax
	movq	(%rax,%r14,8), %rax
	cmpl	$0, 4(%rax)
	je	.LBB42_18
.LBB42_26:                              # %is_used_for_reference.exit.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB42_18:
	movl	(%rax), %ecx
	cmpl	$3, %ecx
	jne	.LBB42_20
# BB#19:
	movq	48(%rax), %rdx
	cmpl	$0, 316848(%rdx)
	jne	.LBB42_26
	jmp	.LBB42_21
.LBB42_20:
	testb	$1, %cl
	je	.LBB42_23
.LBB42_21:                              # %.thread.i
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB42_23
# BB#22:
	cmpl	$0, 316848(%rdx)
	jne	.LBB42_26
.LBB42_23:
	testb	$2, %cl
	je	.LBB42_27
# BB#24:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.LBB42_27
# BB#25:
	cmpl	$0, 316848(%rax)
	jne	.LBB42_26
.LBB42_27:                              # %is_used_for_reference.exit
	movl	%r15d, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	remove_frame_from_dpb   # TAILCALL
.LBB42_3:
	movl	$2147483647, %ebp       # imm = 0x7FFFFFFF
	jmp	.LBB42_9
.Lfunc_end42:
	.size	output_one_frame_from_dpb, .Lfunc_end42-output_one_frame_from_dpb
	.cfi_endproc

	.type	Co_located,@object      # @Co_located
	.bss
	.globl	Co_located
	.p2align	3
Co_located:
	.quad	0
	.size	Co_located, 8

	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"undefined level"
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"max_dec_frame_buffering larger than MaxDpbSize"
	.size	.L.str.1, 47

	.type	dpb,@object             # @dpb
	.comm	dpb,64,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Max. number of reference frames exceeded. Invalid stream."
	.size	.L.str.2, 58

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"DPB size at specified level is smaller than the specified number of reference frames. This is not allowed.\n"
	.size	.L.str.3, 108

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"init_dpb: dpb->fs"
	.size	.L.str.4, 18

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"init_dpb: dpb->fs_ref"
	.size	.L.str.5, 22

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"init_dpb: dpb->fs_ltref"
	.size	.L.str.6, 24

	.type	listX,@object           # @listX
	.comm	listX,48,16
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"init_dpb: listX[i]"
	.size	.L.str.7, 19

	.type	no_reference_picture,@object # @no_reference_picture
	.comm	no_reference_picture,8,8
	.type	listXsize,@object       # @listXsize
	.comm	listXsize,24,16
	.type	last_out_fs,@object     # @last_out_fs
	.comm	last_out_fs,8,8
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"alloc_frame_store: f"
	.size	.L.str.8, 21

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"alloc_storable_picture: s"
	.size	.L.str.9, 26

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"alloc_storable_picture: s->mb_field"
	.size	.L.str.10, 36

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"init_lists: fs_list0"
	.size	.L.str.11, 21

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"init_lists: fs_listlt"
	.size	.L.str.12, 22

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"init_lists: fs_list1"
	.size	.L.str.13, 21

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Invalid remapping_of_pic_nums_idc command"
	.size	.L.str.14, 42

	.type	pocs_in_dpb,@object     # @pocs_in_dpb
	.comm	pocs_in_dpb,400,16
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"duplicate frame_num in short-term reference picture buffer"
	.size	.L.str.15, 59

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"alloc_ref_pic_list_reordering_buffer: reordering_of_pic_nums_idc_l0"
	.size	.L.str.16, 68

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"alloc_ref_pic_list_reordering_buffer: abs_diff_pic_num_minus1_l0"
	.size	.L.str.17, 65

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"alloc_ref_pic_list_reordering_buffer: long_term_pic_idx_l0"
	.size	.L.str.18, 59

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"alloc_ref_pic_list_reordering_buffer: reordering_of_pic_nums_idc_l1"
	.size	.L.str.19, 68

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"alloc_ref_pic_list_reordering_buffer: abs_diff_pic_num_minus1_l1"
	.size	.L.str.20, 65

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"alloc_ref_pic_list_reordering_buffer: long_term_pic_idx_l1"
	.size	.L.str.21, 59

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"alloc_colocated: s"
	.size	.L.str.22, 19

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"memory_management_control_operation = 0 not last operation in buffer"
	.size	.L.str.23, 69

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"invalid memory_management_control_operation in buffer"
	.size	.L.str.24, 54

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"field for long term marking not found"
	.size	.L.str.25, 38

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"Cannot determine smallest POC, DPB empty."
	.size	.L.str.29, 42

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"invalid frame store type"
	.size	.L.str.30, 25

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Cannot output frame, DPB empty."
	.size	.L.str.31, 32

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"no frames for output available"
	.size	.L.str.32, 31

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"output POC must be in ascending order"
	.size	.L.str.33, 38

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Warning: reference field for long term marking not found"
	.size	.Lstr, 57

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"Warning: assigning long_term_frame_idx different from other field"
	.size	.Lstr.2, 66

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"Warning: reference frame for long term marking not found"
	.size	.Lstr.3, 57


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
