	.text
	.file	"parse.bc"
	.globl	print_paren
	.p2align	4, 0x90
	.type	print_paren,@function
print_paren:                            # @print_paren
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpb	$0, 85(%r14)
	je	.LBB0_1
.LBB0_14:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_1:
	movl	40(%r14), %eax
	testl	%eax, %eax
	je	.LBB0_9
# BB#2:
	xorl	%ebx, %ebx
	cmpl	$1, %eax
	je	.LBB0_5
# BB#3:
	movl	$40, %edi
	callq	putchar
	xorl	%ebx, %ebx
	jmp	.LBB0_4
.LBB0_9:
	movq	160(%r14), %rax
	cmpq	200(%r14), %rax
	je	.LBB0_14
# BB#10:
	movl	$32, %edi
	callq	putchar
	movq	160(%r14), %rbx
	cmpq	200(%r14), %rbx
	jae	.LBB0_13
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rbx), %edi
	callq	putchar
	incq	%rbx
	cmpq	200(%r14), %rbx
	jb	.LBB0_11
.LBB0_13:                               # %._crit_edge
	movl	$32, %edi
	jmp	.LBB0_8
.LBB0_4:
	movl	40(%r14), %eax
	cmpl	%eax, %ebx
	jae	.LBB0_6
.LBB0_5:                                # %.thread
	movq	48(%r14), %rax
	movslq	%ebx, %rbx
	movq	(%rax,%rbx,8), %rdi
	callq	print_paren
	incl	%ebx
	jmp	.LBB0_4
.LBB0_6:
	cmpl	$2, %eax
	jb	.LBB0_14
# BB#7:
	movl	$41, %edi
.LBB0_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	putchar                 # TAILCALL
.Lfunc_end0:
	.size	print_paren, .Lfunc_end0-print_paren
	.cfi_endproc

	.globl	xprint_paren
	.p2align	4, 0x90
	.type	xprint_paren,@function
xprint_paren:                           # @xprint_paren
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	cmpb	$0, 85(%r15)
	je	.LBB1_1
.LBB1_12:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB1_1:
	movq	144(%r14), %rax
	movq	32(%rax), %rax
	movslq	152(%r15), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movq	8(%rax,%rcx,8), %rsi
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, 40(%r15)
	je	.LBB1_7
# BB#2:
	movl	$40, %edi
	callq	putchar
	cmpl	$0, 40(%r15)
	je	.LBB1_5
# BB#3:                                 # %.lr.ph27
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_4:                                # =>This Inner Loop Header: Depth=1
	movq	48(%r15), %rax
	movq	(%rax,%rbx,8), %rsi
	movq	%r14, %rdi
	callq	xprint_paren
	incq	%rbx
	cmpl	40(%r15), %ebx
	jb	.LBB1_4
.LBB1_5:                                # %._crit_edge28
	movl	$41, %edi
	jmp	.LBB1_6
.LBB1_7:
	movq	160(%r15), %rax
	cmpq	200(%r15), %rax
	je	.LBB1_12
# BB#8:
	movl	$32, %edi
	callq	putchar
	movq	160(%r15), %rbx
	cmpq	200(%r15), %rbx
	jae	.LBB1_11
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rbx), %edi
	callq	putchar
	incq	%rbx
	cmpq	200(%r15), %rbx
	jb	.LBB1_9
.LBB1_11:                               # %._crit_edge
	movl	$32, %edi
.LBB1_6:                                # %._crit_edge28
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	putchar                 # TAILCALL
.Lfunc_end1:
	.size	xprint_paren, .Lfunc_end1-xprint_paren
	.cfi_endproc

	.globl	xpp
	.p2align	4, 0x90
	.type	xpp,@function
xpp:                                    # @xpp
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 16
	callq	xprint_paren
	movl	$10, %edi
	popq	%rax
	jmp	putchar                 # TAILCALL
.Lfunc_end2:
	.size	xpp, .Lfunc_end2-xpp
	.cfi_endproc

	.globl	pp
	.p2align	4, 0x90
	.type	pp,@function
pp:                                     # @pp
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 16
	callq	print_paren
	movl	$10, %edi
	popq	%rax
	jmp	putchar                 # TAILCALL
.Lfunc_end3:
	.size	pp, .Lfunc_end3-pp
	.cfi_endproc

	.globl	d_get_child
	.p2align	4, 0x90
	.type	d_get_child,@function
d_get_child:                            # @d_get_child
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	js	.LBB4_1
# BB#2:
	cmpl	%esi, -112(%rdi)
	jbe	.LBB4_3
# BB#4:
	movq	-104(%rdi), %rcx
	movslq	%esi, %rdx
	movl	$152, %eax
	addq	(%rcx,%rdx,8), %rax
	retq
.LBB4_1:
	xorl	%eax, %eax
	retq
.LBB4_3:
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	d_get_child, .Lfunc_end4-d_get_child
	.cfi_endproc

	.globl	d_get_number_of_children
	.p2align	4, 0x90
	.type	d_get_number_of_children,@function
d_get_number_of_children:               # @d_get_number_of_children
	.cfi_startproc
# BB#0:
	movl	-112(%rdi), %eax
	retq
.Lfunc_end5:
	.size	d_get_number_of_children, .Lfunc_end5-d_get_number_of_children
	.cfi_endproc

	.globl	d_find_in_tree
	.p2align	4, 0x90
	.type	d_find_in_tree,@function
d_find_in_tree:                         # @d_find_in_tree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	cmpl	%ebp, (%r14)
	je	.LBB6_7
# BB#1:                                 # %.preheader
	movl	-112(%r14), %r12d
	testl	%r12d, %r12d
	je	.LBB6_2
# BB#4:                                 # %.lr.ph
	movq	-104(%r14), %r15
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbx,8), %rdi
	addq	$152, %rdi
	movl	%ebp, %esi
	callq	d_find_in_tree
	testq	%rax, %rax
	jne	.LBB6_6
# BB#3:                                 #   in Loop: Header=BB6_5 Depth=1
	incq	%rbx
	cmpl	%r12d, %ebx
	jb	.LBB6_5
	jmp	.LBB6_7
.LBB6_2:
	xorl	%r14d, %r14d
	jmp	.LBB6_7
.LBB6_6:
	movq	%rax, %r14
.LBB6_7:                                # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	d_find_in_tree, .Lfunc_end6-d_find_in_tree
	.cfi_endproc

	.globl	d_ws_before
	.p2align	4, 0x90
	.type	d_ws_before,@function
d_ws_before:                            # @d_ws_before
	.cfi_startproc
# BB#0:
	movq	-32(%rsi), %rax
	retq
.Lfunc_end7:
	.size	d_ws_before, .Lfunc_end7-d_ws_before
	.cfi_endproc

	.globl	d_ws_after
	.p2align	4, 0x90
	.type	d_ws_after,@function
d_ws_after:                             # @d_ws_after
	.cfi_startproc
# BB#0:
	movq	-24(%rsi), %rax
	retq
.Lfunc_end8:
	.size	d_ws_after, .Lfunc_end8-d_ws_after
	.cfi_endproc

	.globl	find_SNode
	.p2align	4, 0x90
	.type	find_SNode,@function
find_SNode:                             # @find_SNode
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
	movq	216(%rdi), %r9
	testq	%r9, %r9
	je	.LBB9_7
# BB#1:
	movl	%esi, %eax
	shll	$12, %eax
	addl	%r8d, %eax
	leal	(%rcx,%rax), %eax
	xorl	%edx, %edx
	divl	228(%rdi)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	(%r9,%rdx,8), %rax
	testq	%rax, %rax
	je	.LBB9_7
# BB#2:                                 # %.lr.ph
	movq	144(%rdi), %rdx
	movq	8(%rdx), %rdx
	movl	%esi, %esi
	movabsq	$-1229782938247303441, %r9 # imm = 0xEEEEEEEEEEEEEEEF
	.p2align	4, 0x90
.LBB9_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	subq	%rdx, %rdi
	sarq	$3, %rdi
	imulq	%r9, %rdi
	cmpq	%rsi, %rdi
	jne	.LBB9_6
# BB#4:                                 #   in Loop: Header=BB9_3 Depth=1
	cmpq	%r8, 8(%rax)
	jne	.LBB9_6
# BB#5:                                 #   in Loop: Header=BB9_3 Depth=1
	cmpq	%rcx, 16(%rax)
	je	.LBB9_8
	.p2align	4, 0x90
.LBB9_6:                                #   in Loop: Header=BB9_3 Depth=1
	movq	120(%rax), %rax
	testq	%rax, %rax
	jne	.LBB9_3
.LBB9_7:
	xorl	%eax, %eax
.LBB9_8:                                # %.loopexit
	retq
.Lfunc_end9:
	.size	find_SNode, .Lfunc_end9-find_SNode
	.cfi_endproc

	.globl	insert_SNode_internal
	.p2align	4, 0x90
	.type	insert_SNode_internal,@function
insert_SNode_internal:                  # @insert_SNode_internal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 64
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	144(%r14), %rax
	movl	(%rsi), %ecx
	movl	16(%rsi), %r15d
	subl	8(%rax), %ecx
	shrl	$3, %ecx
	imull	$-286330880, %ecx, %eax # imm = 0xEEEEF000
	addl	8(%rsi), %r15d
	addl	%eax, %r15d
	movl	232(%r14), %ecx
	leal	1(%rcx), %eax
	movl	228(%r14), %r13d
	cmpl	%r13d, %eax
	jbe	.LBB10_7
# BB#1:
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	216(%r14), %rbp
	movl	224(%r14), %eax
	incl	%eax
	movl	%eax, 224(%r14)
	movl	prime2(,%rax,4), %r12d
	movl	%r12d, 228(%r14)
	shlq	$3, %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, 216(%r14)
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r12, %rdx
	callq	memset
	testl	%r13d, %r13d
	je	.LBB10_6
# BB#2:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	jmp	.LBB10_4
	.p2align	4, 0x90
.LBB10_3:                               #   in Loop: Header=BB10_4 Depth=1
	movq	120(%rsi), %rax
	movq	%rax, (%rbp,%rbx,8)
	movq	%r14, %rdi
	callq	insert_SNode_internal
.LBB10_4:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rbx,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB10_3
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB10_4 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jne	.LBB10_4
.LBB10_6:                               # %._crit_edge48
	movq	%rbp, %rdi
	callq	free
	movl	228(%r14), %r13d
	movl	232(%r14), %ecx
	movq	(%rsp), %rsi            # 8-byte Reload
.LBB10_7:                               # %._crit_edge50
	movq	216(%r14), %rdi
	xorl	%edx, %edx
	movl	%r15d, %eax
	divl	%r13d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	(%rdi,%rdx,8), %rax
	movq	%rax, 120(%rsi)
	movq	%rsi, (%rdi,%rdx,8)
	incl	%ecx
	movl	%ecx, 232(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	insert_SNode_internal, .Lfunc_end10-insert_SNode_internal
	.cfi_endproc

	.globl	find_PNode
	.p2align	4, 0x90
	.type	find_PNode,@function
find_PNode:                             # @find_PNode
	.cfi_startproc
# BB#0:
	movq	%rdx, %r10
	movq	184(%rdi), %r11
	testq	%r11, %r11
	je	.LBB11_9
# BB#1:
	movl	%esi, %eax
	shll	$8, %eax
	movl	%r10d, %edx
	shll	$16, %edx
	addl	%ecx, %eax
	addl	%edx, %eax
	addl	%r8d, %eax
	addl	%r9d, %eax
	xorl	%edx, %edx
	divl	196(%rdi)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	(%r11,%rdx,8), %rax
	testq	%rax, %rax
	jne	.LBB11_3
	jmp	.LBB11_9
	.p2align	4, 0x90
.LBB11_8:                               #   in Loop: Header=BB11_3 Depth=1
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.LBB11_9
.LBB11_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%ecx, 152(%rax)
	jne	.LBB11_8
# BB#4:                                 #   in Loop: Header=BB11_3 Depth=1
	cmpq	%rsi, 160(%rax)
	jne	.LBB11_8
# BB#5:                                 #   in Loop: Header=BB11_3 Depth=1
	cmpq	%r10, 200(%rax)
	jne	.LBB11_8
# BB#6:                                 #   in Loop: Header=BB11_3 Depth=1
	cmpq	%r8, 136(%rax)
	jne	.LBB11_8
# BB#7:                                 #   in Loop: Header=BB11_3 Depth=1
	cmpq	%r9, 144(%rax)
	jne	.LBB11_8
	jmp	.LBB11_10
.LBB11_9:
	xorl	%eax, %eax
.LBB11_10:                              # %.loopexit
	retq
.Lfunc_end11:
	.size	find_PNode, .Lfunc_end11-find_PNode
	.cfi_endproc

	.globl	insert_PNode_internal
	.p2align	4, 0x90
	.type	insert_PNode_internal,@function
insert_PNode_internal:                  # @insert_PNode_internal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 64
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	160(%rsi), %r15d
	movl	200(%rsi), %eax
	shll	$8, %r15d
	shll	$16, %eax
	addl	152(%rsi), %r15d
	addl	%eax, %r15d
	addl	136(%rsi), %r15d
	addl	144(%rsi), %r15d
	movl	196(%r14), %r13d
	movl	200(%r14), %ecx
	leal	1(%rcx), %eax
	cmpl	%r13d, %eax
	jbe	.LBB12_7
# BB#1:
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	184(%r14), %rbp
	movl	192(%r14), %eax
	incl	%eax
	movl	%eax, 192(%r14)
	movl	prime2(,%rax,4), %r12d
	movl	%r12d, 196(%r14)
	shlq	$3, %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, 184(%r14)
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r12, %rdx
	callq	memset
	testl	%r13d, %r13d
	je	.LBB12_6
# BB#2:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	jmp	.LBB12_4
	.p2align	4, 0x90
.LBB12_3:                               #   in Loop: Header=BB12_4 Depth=1
	movq	96(%rsi), %rax
	movq	%rax, (%rbp,%rbx,8)
	movq	%r14, %rdi
	callq	insert_PNode_internal
.LBB12_4:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rbx,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB12_3
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB12_4 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jne	.LBB12_4
.LBB12_6:                               # %._crit_edge49
	movq	%rbp, %rdi
	callq	free
	movl	196(%r14), %r13d
	movl	200(%r14), %ecx
	movq	(%rsp), %rsi            # 8-byte Reload
.LBB12_7:                               # %._crit_edge51
	movq	184(%r14), %rdi
	xorl	%edx, %edx
	movl	%r15d, %eax
	divl	%r13d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	(%rdi,%rdx,8), %rax
	movq	%rax, 96(%rsi)
	movq	%rsi, (%rdi,%rdx,8)
	incl	%ecx
	movl	%ecx, 200(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	insert_PNode_internal, .Lfunc_end12-insert_PNode_internal
	.cfi_endproc

	.globl	free_D_ParseTreeBelow
	.p2align	4, 0x90
	.type	free_D_ParseTreeBelow,@function
free_D_ParseTreeBelow:                  # @free_D_ParseTreeBelow
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -32
.Lcfi53:
	.cfi_offset %r14, -24
.Lcfi54:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	-112(%rsi), %eax
	testl	%eax, %eax
	movq	-104(%rsi), %rdi
	leaq	-152(%rsi), %r15
	je	.LBB13_5
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rsi
	decl	32(%rsi)
	jne	.LBB13_4
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	movq	%r14, %rdi
	callq	free_PNode
	movl	40(%r15), %eax
	movq	48(%r15), %rdi
.LBB13_4:                               #   in Loop: Header=BB13_2 Depth=1
	incq	%rbx
	cmpl	%eax, %ebx
	jb	.LBB13_2
.LBB13_5:                               # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB13_8
# BB#6:                                 # %._crit_edge.i
	leaq	56(%r15), %rax
	cmpq	%rax, %rdi
	je	.LBB13_8
# BB#7:
	callq	free
.LBB13_8:
	movl	$0, 40(%r15)
	movq	$0, 48(%r15)
	movq	104(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB13_9
# BB#10:
	movq	$0, 104(%r15)
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free_PNode              # TAILCALL
.LBB13_9:                               # %free_ParseTreeBelow.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	free_D_ParseTreeBelow, .Lfunc_end13-free_D_ParseTreeBelow
	.cfi_endproc

	.globl	ambiguity_count_fn
	.p2align	4, 0x90
	.type	ambiguity_count_fn,@function
ambiguity_count_fn:                     # @ambiguity_count_fn
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	176(%rdi), %eax
	leal	-1(%rsi,%rax), %eax
	movl	%eax, 176(%rdi)
	movq	(%rdx), %rax
	retq
.Lfunc_end14:
	.size	ambiguity_count_fn, .Lfunc_end14-ambiguity_count_fn
	.cfi_endproc

	.globl	ambiguity_abort_fn
	.p2align	4, 0x90
	.type	ambiguity_abort_fn,@function
ambiguity_abort_fn:                     # @ambiguity_abort_fn
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 32
.Lcfi58:
	.cfi_offset %rbx, -32
.Lcfi59:
	.cfi_offset %r14, -24
.Lcfi60:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	cmpl	$0, verbose_level(%rip)
	je	.LBB15_4
# BB#1:
	testl	%esi, %esi
	jle	.LBB15_4
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %r15d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB15_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	addq	$-152, %rdi
	callq	print_paren
	movl	$10, %edi
	callq	putchar
	addq	$8, %rbx
	decq	%r15
	jne	.LBB15_3
.LBB15_4:                               # %.loopexit
	movq	(%r14), %rax
	movl	32(%rax), %esi
	movq	16(%rax), %rdx
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	d_fail
	movq	(%r14), %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	ambiguity_abort_fn, .Lfunc_end15-ambiguity_abort_fn
	.cfi_endproc

	.globl	d_pass
	.p2align	4, 0x90
	.type	d_pass,@function
d_pass:                                 # @d_pass
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 32
.Lcfi64:
	.cfi_offset %rbx, -32
.Lcfi65:
	.cfi_offset %r14, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	144(%r14), %rax
	cmpl	%ebp, 48(%rax)
	ja	.LBB16_2
# BB#1:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	d_fail
	movq	144(%r14), %rax
.LBB16_2:
	addq	$-152, %rbx
	movq	56(%rax), %rax
	movslq	%ebp, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	12(%rax,%rcx,8), %edx
	testb	$4, %dl
	jne	.LBB16_3
# BB#7:
	leaq	(%rax,%rcx,8), %rsi
	testb	$1, %dl
	jne	.LBB16_10
# BB#8:
	testb	$2, %dl
	je	.LBB16_9
# BB#11:
	movq	%r14, %rdi
	movq	%rbx, %rdx
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	pass_postorder          # TAILCALL
.LBB16_3:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.LBB16_9
# BB#4:
	movl	16(%rax,%rcx,8), %eax
	cmpl	%eax, 40(%rdx)
	jbe	.LBB16_9
# BB#5:
	movq	48(%rdx), %rcx
	movq	(%rcx,%rax,8), %rax
	testq	%rax, %rax
	je	.LBB16_9
# BB#6:
	movq	48(%rbx), %rsi
	movl	40(%rbx), %edx
	movl	$152, %ecx
	movq	%rbx, %rdi
	movq	%r14, %r8
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB16_9:                               # %pass_call.exit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB16_10:
	movq	%r14, %rdi
	movq	%rbx, %rdx
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	pass_preorder           # TAILCALL
.Lfunc_end16:
	.size	d_pass, .Lfunc_end16-d_pass
	.cfi_endproc

	.p2align	4, 0x90
	.type	pass_preorder,@function
pass_preorder:                          # @pass_preorder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi71:
	.cfi_def_cfa_offset 48
.Lcfi72:
	.cfi_offset %rbx, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB17_5
# BB#1:
	movl	16(%r14), %ecx
	cmpl	%ecx, 40(%rax)
	jbe	.LBB17_5
# BB#2:
	movq	48(%rax), %rax
	movq	(%rax,%rcx,8), %rax
	testq	%rax, %rax
	setne	%bpl
	je	.LBB17_6
# BB#3:
	movq	48(%rbx), %rsi
	movl	40(%rbx), %edx
	movl	$152, %ecx
	movq	%rbx, %rdi
	movq	%r15, %r8
	callq	*%rax
	jmp	.LBB17_6
.LBB17_5:
	xorl	%ebp, %ebp
.LBB17_6:                               # %pass_call.exit
	movl	12(%r14), %eax
	testb	$8, %al
	jne	.LBB17_8
# BB#7:
	testb	$16, %al
	sete	%al
	orb	%al, %bpl
	jne	.LBB17_11
.LBB17_8:                               # %.preheader
	cmpl	$0, 40(%rbx)
	je	.LBB17_11
# BB#9:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_10:                              # =>This Inner Loop Header: Depth=1
	movq	48(%rbx), %rax
	movq	(%rax,%rbp,8), %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	pass_preorder
	incq	%rbp
	cmpl	40(%rbx), %ebp
	jb	.LBB17_10
.LBB17_11:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	pass_preorder, .Lfunc_end17-pass_preorder
	.cfi_endproc

	.p2align	4, 0x90
	.type	pass_postorder,@function
pass_postorder:                         # @pass_postorder
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi78:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi80:
	.cfi_def_cfa_offset 48
.Lcfi81:
	.cfi_offset %rbx, -40
.Lcfi82:
	.cfi_offset %r12, -32
.Lcfi83:
	.cfi_offset %r14, -24
.Lcfi84:
	.cfi_offset %r15, -16
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.LBB18_4
# BB#1:
	movl	16(%r15), %ecx
	cmpl	%ecx, 40(%rax)
	jbe	.LBB18_4
# BB#2:
	movq	48(%rax), %rdx
	cmpq	$0, (%rdx,%rcx,8)
	setne	%cl
	jmp	.LBB18_5
.LBB18_4:
	xorl	%ecx, %ecx
.LBB18_5:
	movl	12(%r15), %edx
	testb	$8, %dl
	jne	.LBB18_7
# BB#6:
	testb	$16, %dl
	sete	%dl
	orb	%dl, %cl
	jne	.LBB18_11
.LBB18_7:                               # %.preheader
	cmpl	$0, 40(%r12)
	je	.LBB18_11
# BB#8:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB18_9:                               # =>This Inner Loop Header: Depth=1
	movq	48(%r12), %rax
	movq	(%rax,%rbx,8), %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	pass_postorder
	incq	%rbx
	cmpl	40(%r12), %ebx
	jb	.LBB18_9
# BB#10:                                # %.loopexit.loopexit
	movq	16(%r12), %rax
.LBB18_11:                              # %.loopexit
	testq	%rax, %rax
	je	.LBB18_15
# BB#12:
	movl	16(%r15), %ecx
	cmpl	%ecx, 40(%rax)
	jbe	.LBB18_15
# BB#13:
	movq	48(%rax), %rax
	movq	(%rax,%rcx,8), %rax
	testq	%rax, %rax
	je	.LBB18_15
# BB#14:
	movq	48(%r12), %rsi
	movl	40(%r12), %edx
	movl	$152, %ecx
	movq	%r12, %rdi
	movq	%r14, %r8
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB18_15:                              # %pass_call.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end18:
	.size	pass_postorder, .Lfunc_end18-pass_postorder
	.cfi_endproc

	.globl	null_white_space
	.p2align	4, 0x90
	.type	null_white_space,@function
null_white_space:                       # @null_white_space
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end19:
	.size	null_white_space, .Lfunc_end19-null_white_space
	.cfi_endproc

	.globl	new_D_Parser
	.p2align	4, 0x90
	.type	new_D_Parser,@function
new_D_Parser:                           # @new_D_Parser
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 32
.Lcfi88:
	.cfi_offset %rbx, -32
.Lcfi89:
	.cfi_offset %r14, -24
.Lcfi90:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$432, %edi              # imm = 0x1B0
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movl	$432, %edx              # imm = 0x1B0
	movq	%rbx, %rdi
	callq	memset
	movq	%r14, 144(%rbx)
	movl	$1, 72(%rbx)
	movl	%ebp, 84(%rbx)
	movl	$100, 112(%rbx)
	movq	$syntax_error_report_fn, 24(%rbx)
	movq	$ambiguity_abort_fn, 32(%rbx)
	movl	$1, 116(%rbx)
	movl	64(%r14), %eax
	movl	%eax, 88(%rbx)
	movq	40(%r14), %rax
	testq	%rax, %rax
	je	.LBB20_2
# BB#1:
	movq	%rax, 8(%rbx)
	jmp	.LBB20_3
.LBB20_2:
	cmpl	$0, 24(%r14)
	movl	$parse_whitespace, %eax
	movl	$white_space, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 8(%rbx)
.LBB20_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end20:
	.size	new_D_Parser, .Lfunc_end20-new_D_Parser
	.cfi_endproc

	.p2align	4, 0x90
	.type	syntax_error_report_fn,@function
syntax_error_report_fn:                 # @syntax_error_report_fn
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 32
.Lcfi94:
	.cfi_offset %rbx, -24
.Lcfi95:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	56(%rbx), %rdi
	callq	d_dup_pathname_str
	movq	%rax, %r14
	movq	stderr(%rip), %rdi
	movl	72(%rbx), %ecx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	fprintf
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end21:
	.size	syntax_error_report_fn, .Lfunc_end21-syntax_error_report_fn
	.cfi_endproc

	.p2align	4, 0x90
	.type	parse_whitespace,@function
parse_whitespace:                       # @parse_whitespace
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi98:
	.cfi_def_cfa_offset 32
.Lcfi99:
	.cfi_offset %rbx, -24
.Lcfi100:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	416(%rdi), %rbx
	movq	(%r14), %rax
	movq	%rax, 128(%rbx)
	movq	144(%rdi), %rax
	movl	24(%rax), %esi
	movq	%rbx, %rdi
	callq	exhaustive_parse
	testl	%eax, %eax
	jne	.LBB22_5
# BB#1:
	movq	280(%rbx), %rax
	testq	%rax, %rax
	je	.LBB22_5
# BB#2:
	movups	24(%rax), %xmm0
	movups	40(%rax), %xmm1
	movups	%xmm1, 16(%r14)
	movups	%xmm0, (%r14)
	movq	280(%rbx), %rsi
	decl	112(%rsi)
	jne	.LBB22_4
# BB#3:
	movq	%rbx, %rdi
	callq	free_SNode
.LBB22_4:
	movq	$0, 280(%rbx)
.LBB22_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end22:
	.size	parse_whitespace, .Lfunc_end22-parse_whitespace
	.cfi_endproc

	.p2align	4, 0x90
	.type	white_space,@function
white_space:                            # @white_space
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi107:
	.cfi_def_cfa_offset 64
.Lcfi108:
	.cfi_offset %rbx, -56
.Lcfi109:
	.cfi_offset %r12, -48
.Lcfi110:
	.cfi_offset %r13, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	(%r13), %rbx
	xorl	%esi, %esi
	cmpq	%rbx, 48(%rdi)
	cmoveq	%rbx, %rsi
	cmpb	$35, (%rbx)
	jne	.LBB23_20
# BB#1:
	cmpl	$0, 16(%r13)
	jne	.LBB23_20
.LBB23_2:                               # %.loopexit89
	movq	%rsi, (%rsp)            # 8-byte Spill
	leaq	4(%rbx), %rbp
	.p2align	4, 0x90
.LBB23_3:                               # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rbp), %r12d
	incq	%rbp
	cmpb	$0, _wspace(%r12)
	jne	.LBB23_3
# BB#4:
	leaq	-4(%rbp), %r14
	movl	$.L.str.11, %edi
	movl	$4, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB23_8
# BB#5:
	movzbl	(%rbp), %eax
	cmpb	$0, _wspace(%rax)
	je	.LBB23_8
	.p2align	4, 0x90
.LBB23_6:                               # =>This Inner Loop Header: Depth=1
	movzbl	1(%rbp), %r12d
	incq	%rbp
	cmpb	$0, _wspace(%r12)
	jne	.LBB23_6
# BB#7:
	movq	%rbp, %r14
.LBB23_8:                               # %.loopexit93
	callq	__ctype_b_loc
	movq	%rax, %r15
	movq	(%r15), %rax
	movsbq	%r12b, %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB23_10
# BB#9:
	movq	(%rsp), %rsi            # 8-byte Reload
	jmp	.LBB23_47
.LBB23_10:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	strtol
	decl	%eax
	movl	%eax, 24(%r13)
	movq	(%r15), %rcx
	decq	%r14
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB23_11:                              # =>This Inner Loop Header: Depth=1
	movsbq	1(%rbx), %rax
	incq	%rbx
	testb	$8, 1(%rcx,%rax,2)
	jne	.LBB23_11
# BB#12:                                # %.preheader92.preheader
	movzbl	%al, %ecx
	cmpb	$0, _wspace(%rcx)
	je	.LBB23_14
	.p2align	4, 0x90
.LBB23_13:                              # %.preheader92..preheader92_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%rbx), %eax
	incq	%rbx
	cmpb	$0, _wspace(%rax)
	jne	.LBB23_13
.LBB23_14:
	movq	(%rsp), %rsi            # 8-byte Reload
	cmpb	$34, %al
	jne	.LBB23_16
# BB#15:
	movq	%rbx, 8(%r13)
	jmp	.LBB23_16
	.p2align	4, 0x90
.LBB23_18:                              #   in Loop: Header=BB23_16 Depth=1
	incq	%rbx
.LBB23_16:                              # %.thread
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %eax
	cmpb	$10, %al
	je	.LBB23_20
# BB#17:                                # %.thread
                                        #   in Loop: Header=BB23_16 Depth=1
	testb	%al, %al
	jne	.LBB23_18
	jmp	.LBB23_20
.LBB23_19:                              # %.critedge.outer.loopexit
                                        #   in Loop: Header=BB23_20 Depth=1
	incq	%rbx
	.p2align	4, 0x90
.LBB23_20:                              # %.critedge.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_21 Depth 2
                                        #       Child Loop BB23_22 Depth 3
                                        #       Child Loop BB23_31 Depth 3
                                        #     Child Loop BB23_35 Depth 2
                                        #       Child Loop BB23_36 Depth 3
                                        #         Child Loop BB23_37 Depth 4
	movq	%rbx, %rax
	jmp	.LBB23_21
	.p2align	4, 0x90
.LBB23_33:                              # %.critedge1
                                        #   in Loop: Header=BB23_21 Depth=2
	incl	24(%r13)
.LBB23_21:                              # %.critedge
                                        #   Parent Loop BB23_20 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB23_22 Depth 3
                                        #       Child Loop BB23_31 Depth 3
	leaq	1(%rax), %rdx
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB23_22:                              #   Parent Loop BB23_20 Depth=1
                                        #     Parent Loop BB23_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rax
	movzbl	(%rbx), %ecx
	incq	%rbx
	leaq	1(%rax), %rdx
	cmpb	$0, _wspace(%rcx)
	jne	.LBB23_22
# BB#23:                                #   in Loop: Header=BB23_21 Depth=2
	cmpb	$47, %cl
	jne	.LBB23_24
# BB#26:                                #   in Loop: Header=BB23_21 Depth=2
	movb	(%rbx), %cl
	cmpb	$47, %cl
	jne	.LBB23_27
# BB#29:                                # %.preheader.preheader
                                        #   in Loop: Header=BB23_21 Depth=2
	movb	$47, %cl
	testb	%cl, %cl
	jne	.LBB23_31
	jmp	.LBB23_33
	.p2align	4, 0x90
.LBB23_32:                              #   in Loop: Header=BB23_31 Depth=3
	movzbl	(%rax), %ecx
	incq	%rax
	testb	%cl, %cl
	je	.LBB23_33
.LBB23_31:                              # %.preheader
                                        #   Parent Loop BB23_20 Depth=1
                                        #     Parent Loop BB23_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$10, %cl
	jne	.LBB23_32
	jmp	.LBB23_33
	.p2align	4, 0x90
.LBB23_24:                              #   in Loop: Header=BB23_20 Depth=1
	cmpb	$10, %cl
	jne	.LBB23_46
# BB#25:                                #   in Loop: Header=BB23_20 Depth=1
	incl	24(%r13)
	cmpb	$35, (%rbx)
	movq	%rbx, %rsi
	jne	.LBB23_20
	jmp	.LBB23_2
	.p2align	4, 0x90
.LBB23_27:                              #   in Loop: Header=BB23_20 Depth=1
	cmpb	$42, %cl
	jne	.LBB23_46
# BB#28:                                # %.preheader87.preheader
                                        #   in Loop: Header=BB23_20 Depth=1
	decq	%rbx
	xorl	%eax, %eax
	jmp	.LBB23_35
	.p2align	4, 0x90
.LBB23_34:                              # %.preheader87.loopexit
                                        #   in Loop: Header=BB23_35 Depth=2
	decq	%rbx
.LBB23_35:                              # %.preheader87
                                        #   Parent Loop BB23_20 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB23_36 Depth 3
                                        #         Child Loop BB23_37 Depth 4
	movq	%rbx, %rcx
	addq	$2, %rcx
	incl	%eax
.LBB23_36:                              #   Parent Loop BB23_20 Depth=1
                                        #     Parent Loop BB23_35 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB23_37 Depth 4
	movq	%rcx, %rbx
	jmp	.LBB23_37
.LBB23_41:                              #   in Loop: Header=BB23_37 Depth=4
	incl	24(%r13)
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB23_37:                              # %.outer
                                        #   Parent Loop BB23_20 Depth=1
                                        #     Parent Loop BB23_35 Depth=2
                                        #       Parent Loop BB23_36 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incq	%rbx
	movzbl	-1(%rbx), %ecx
	cmpb	$46, %cl
	jg	.LBB23_44
# BB#38:                                #   in Loop: Header=BB23_37 Depth=4
	cmpb	$42, %cl
	jne	.LBB23_39
# BB#42:                                #   in Loop: Header=BB23_37 Depth=4
	cmpb	$47, (%rbx)
	jne	.LBB23_37
	jmp	.LBB23_43
	.p2align	4, 0x90
.LBB23_44:                              #   in Loop: Header=BB23_37 Depth=4
	cmpb	$47, %cl
	jne	.LBB23_37
# BB#45:                                #   in Loop: Header=BB23_37 Depth=4
	cmpb	$42, (%rbx)
	jne	.LBB23_37
	jmp	.LBB23_34
.LBB23_39:                              #   in Loop: Header=BB23_37 Depth=4
	testb	%cl, %cl
	je	.LBB23_46
# BB#40:                                #   in Loop: Header=BB23_37 Depth=4
	cmpb	$10, %cl
	jne	.LBB23_37
	jmp	.LBB23_41
	.p2align	4, 0x90
.LBB23_43:                              #   in Loop: Header=BB23_36 Depth=3
	leaq	1(%rbx), %rcx
	decl	%eax
	jne	.LBB23_36
	jmp	.LBB23_19
.LBB23_46:                              # %.loopexit.loopexit165
	decq	%rbx
.LBB23_47:                              # %.loopexit
	movl	%ebx, %eax
	subl	%esi, %eax
	cmpq	$1, %rsi
	sbbl	%ecx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 20(%r13)
	movq	%rbx, (%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	white_space, .Lfunc_end23-white_space
	.cfi_endproc

	.globl	free_D_Parser
	.p2align	4, 0x90
	.type	free_D_Parser,@function
free_D_Parser:                          # @free_D_Parser
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 16
.Lcfi115:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	272(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB24_3
# BB#1:
	cmpq	$0, 16(%rbx)
	jne	.LBB24_3
# BB#2:
	xorl	%esi, %esi
	callq	free_D_Scope
.LBB24_3:
	movq	416(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB24_5
# BB#4:
	callq	free_D_Parser
.LBB24_5:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end24:
	.size	free_D_Parser, .Lfunc_end24-free_D_Parser
	.cfi_endproc

	.globl	free_D_ParseNode
	.p2align	4, 0x90
	.type	free_D_ParseNode,@function
free_D_ParseNode:                       # @free_D_ParseNode
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 16
.Lcfi117:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpq	$1, %rsi
	je	.LBB25_4
# BB#1:
	decl	-120(%rsi)
	jne	.LBB25_3
# BB#2:
	addq	$-152, %rsi
	movq	%rbx, %rdi
	callq	free_PNode
.LBB25_3:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free_parser_working_data # TAILCALL
.LBB25_4:
	popq	%rbx
	retq
.Lfunc_end25:
	.size	free_D_ParseNode, .Lfunc_end25-free_D_ParseNode
	.cfi_endproc

	.p2align	4, 0x90
	.type	free_PNode,@function
free_PNode:                             # @free_PNode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 32
.Lcfi121:
	.cfi_offset %rbx, -32
.Lcfi122:
	.cfi_offset %r14, -24
.Lcfi123:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	40(%r14), %rax
	testq	%rax, %rax
	je	.LBB26_2
# BB#1:
	leaq	152(%r15), %rdi
	callq	*%rax
.LBB26_2:                               # %.preheader
	movl	40(%r15), %eax
	testl	%eax, %eax
	movq	48(%r15), %rdi
	je	.LBB26_7
# BB#3:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB26_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rcx
	decl	32(%rcx)
	jne	.LBB26_6
# BB#5:                                 #   in Loop: Header=BB26_4 Depth=1
	movq	48(%r15), %rax
	movq	(%rax,%rbx,8), %rsi
	movq	%r14, %rdi
	callq	free_PNode
	movl	40(%r15), %eax
.LBB26_6:                               #   in Loop: Header=BB26_4 Depth=1
	incq	%rbx
	movq	48(%r15), %rdi
	cmpl	%eax, %ebx
	jb	.LBB26_4
.LBB26_7:                               # %._crit_edge
	testq	%rdi, %rdi
	je	.LBB26_10
# BB#8:                                 # %._crit_edge
	leaq	56(%r15), %rax
	cmpq	%rax, %rdi
	je	.LBB26_10
# BB#9:
	callq	free
.LBB26_10:
	movl	$0, 40(%r15)
	movq	$0, 48(%r15)
	movq	104(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB26_12
# BB#11:
	movq	$0, 104(%r15)
	movq	%r14, %rdi
	callq	free_PNode
.LBB26_12:
	movq	112(%r15), %rsi
	cmpq	%r15, %rsi
	je	.LBB26_15
# BB#13:
	decl	32(%rsi)
	jne	.LBB26_15
# BB#14:
	movq	%r14, %rdi
	callq	free_PNode
.LBB26_15:
	movq	320(%r14), %rax
	movq	%rax, 88(%r15)
	movq	%r15, 320(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end26:
	.size	free_PNode, .Lfunc_end26-free_PNode
	.cfi_endproc

	.p2align	4, 0x90
	.type	free_parser_working_data,@function
free_parser_working_data:               # @free_parser_working_data
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi126:
	.cfi_def_cfa_offset 32
.Lcfi127:
	.cfi_offset %rbx, -24
.Lcfi128:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	callq	free_old_nodes
	movq	%r14, %rdi
	callq	free_old_nodes
	movq	184(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB27_2
# BB#1:
	callq	free
.LBB27_2:
	leaq	184(%r14), %rbx
	movq	216(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB27_4
# BB#3:
	callq	free
.LBB27_4:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 64(%rbx)
	movq	256(%r14), %rax
	testq	%rax, %rax
	je	.LBB27_9
# BB#5:                                 # %.lr.ph96
	movq	296(%r14), %rdi
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB27_6:                               # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rbx
	movq	8(%rax), %rsi
	decl	112(%rsi)
	jne	.LBB27_8
# BB#7:                                 #   in Loop: Header=BB27_6 Depth=1
	movq	%r14, %rdi
	callq	free_SNode
	movq	296(%r14), %rdi
.LBB27_8:                               #   in Loop: Header=BB27_6 Depth=1
	callq	free
	movq	%rbx, 296(%r14)
	movq	256(%r14), %rax
	testq	%rax, %rax
	movq	%rbx, %rdi
	jne	.LBB27_6
.LBB27_9:                               # %.preheader83
	movq	264(%r14), %rax
	testq	%rax, %rax
	je	.LBB27_14
# BB#10:                                # %.lr.ph94
	movq	304(%r14), %rdi
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB27_11:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rbx
	movq	(%rax), %rsi
	decl	112(%rsi)
	jne	.LBB27_13
# BB#12:                                #   in Loop: Header=BB27_11 Depth=1
	movq	%r14, %rdi
	callq	free_SNode
	movq	304(%r14), %rdi
.LBB27_13:                              #   in Loop: Header=BB27_11 Depth=1
	callq	free
	movq	%rbx, 304(%r14)
	movq	264(%r14), %rax
	testq	%rax, %rax
	movq	%rbx, %rdi
	jne	.LBB27_11
.LBB27_14:                              # %.preheader82
	movq	296(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB27_15
	.p2align	4, 0x90
.LBB27_28:                              # =>This Inner Loop Header: Depth=1
	movq	40(%rdi), %rbx
	callq	free
	movq	%rbx, 296(%r14)
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB27_28
.LBB27_15:                              # %.preheader81
	movq	304(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB27_16
	.p2align	4, 0x90
.LBB27_29:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rdi), %rbx
	callq	free
	movq	%rbx, 304(%r14)
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB27_29
.LBB27_16:                              # %.preheader80
	movq	320(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB27_17
	.p2align	4, 0x90
.LBB27_30:                              # =>This Inner Loop Header: Depth=1
	movq	88(%rdi), %rbx
	callq	free
	movq	%rbx, 320(%r14)
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB27_30
.LBB27_17:                              # %.preheader79
	movq	336(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB27_18
	.p2align	4, 0x90
.LBB27_31:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	free
	movq	%rbx, 336(%r14)
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB27_31
.LBB27_18:                              # %.preheader78
	movq	328(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB27_19
	.p2align	4, 0x90
.LBB27_32:                              # =>This Inner Loop Header: Depth=1
	movq	128(%rdi), %rbx
	callq	free
	movq	%rbx, 328(%r14)
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB27_32
.LBB27_19:                              # %.preheader
	cmpl	$0, 344(%r14)
	movq	352(%r14), %rdi
	je	.LBB27_22
# BB#20:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB27_21:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rdi
	callq	free
	incq	%rbx
	movq	352(%r14), %rdi
	cmpl	344(%r14), %ebx
	jb	.LBB27_21
.LBB27_22:                              # %._crit_edge
	testq	%rdi, %rdi
	je	.LBB27_25
# BB#23:                                # %._crit_edge
	leaq	360(%r14), %rax
	cmpq	%rax, %rdi
	je	.LBB27_25
# BB#24:
	callq	free
.LBB27_25:
	movl	$0, 344(%r14)
	movq	$0, 352(%r14)
	movq	416(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB27_27
# BB#26:
	callq	free_parser_working_data
.LBB27_27:
	movq	384(%r14), %rdi
	callq	free
	movq	$0, 384(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end27:
	.size	free_parser_working_data, .Lfunc_end27-free_parser_working_data
	.cfi_endproc

	.globl	new_subparser
	.p2align	4, 0x90
	.type	new_subparser,@function
new_subparser:                          # @new_subparser
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi132:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi133:
	.cfi_def_cfa_offset 48
.Lcfi134:
	.cfi_offset %rbx, -40
.Lcfi135:
	.cfi_offset %r14, -32
.Lcfi136:
	.cfi_offset %r15, -24
.Lcfi137:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	144(%r14), %r15
	movl	84(%r14), %ebp
	movl	$432, %edi              # imm = 0x1B0
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movl	$432, %edx              # imm = 0x1B0
	movq	%rbx, %rdi
	callq	memset
	movq	%r15, 144(%rbx)
	movl	$1, 72(%rbx)
	movl	%ebp, 84(%rbx)
	movl	$100, 112(%rbx)
	movq	$syntax_error_report_fn, 24(%rbx)
	movq	$ambiguity_abort_fn, 32(%rbx)
	movl	$1, 116(%rbx)
	movl	64(%r15), %eax
	movl	%eax, 88(%rbx)
	movq	40(%r15), %rax
	testq	%rax, %rax
	je	.LBB28_2
# BB#1:
	movq	%rax, 8(%rbx)
	jmp	.LBB28_3
.LBB28_2:
	cmpl	$0, 24(%r15)
	movl	$parse_whitespace, %eax
	movl	$white_space, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 8(%rbx)
.LBB28_3:                               # %new_D_Parser.exit
	movq	136(%r14), %rax
	movq	%rax, 136(%rbx)
	movq	424(%r14), %rax
	movq	%rax, 424(%rbx)
	movl	$10, 192(%rbx)
	movl	prime2+40(%rip), %r14d
	movl	%r14d, 196(%rbx)
	shlq	$3, %r14
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 184(%rbx)
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r14, %rdx
	callq	memset
	movl	$8, 224(%rbx)
	movl	prime2+32(%rip), %r14d
	movl	%r14d, 228(%rbx)
	shlq	$3, %r14
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 216(%rbx)
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r14, %rdx
	callq	memset
	movl	28(%r15), %eax
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rdi
	callq	malloc
	movq	%rax, 384(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	new_subparser, .Lfunc_end28-new_subparser
	.cfi_endproc

	.globl	dparse
	.p2align	4, 0x90
	.type	dparse,@function
dparse:                                 # @dparse
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi138:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi139:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi140:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi141:
	.cfi_def_cfa_offset 48
.Lcfi142:
	.cfi_offset %rbx, -32
.Lcfi143:
	.cfi_offset %r14, -24
.Lcfi144:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 160(%r15)
	movl	$0, 152(%r15)
	movq	%rsi, 128(%r15)
	movslq	%edx, %rax
	addq	%rsi, %rax
	movq	%rax, 136(%r15)
	movq	144(%r15), %r14
	cmpl	$0, 24(%r14)
	je	.LBB29_2
# BB#1:
	movq	%r15, %rdi
	callq	new_subparser
	movq	%rax, 416(%r15)
	movq	$null_white_space, 8(%rax)
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	movq	%rcx, 116(%rax)
	movq	144(%r15), %r14
.LBB29_2:                               # %initialize_whitespace_parser.exit
	movl	$10, 192(%r15)
	movl	prime2+40(%rip), %ebx
	movl	%ebx, 196(%r15)
	shlq	$3, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 184(%r15)
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rbx, %rdx
	callq	memset
	movl	$8, 224(%r15)
	movl	prime2+32(%rip), %ebx
	movl	%ebx, 228(%r15)
	shlq	$3, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 216(%r15)
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rbx, %rdx
	callq	memset
	movl	28(%r14), %eax
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rdi
	callq	malloc
	movq	%rax, 384(%r15)
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.LBB29_4
# BB#3:
	movq	%rax, 272(%r15)
	jmp	.LBB29_7
.LBB29_4:
	movq	272(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB29_6
# BB#5:
	xorl	%esi, %esi
	callq	free_D_Scope
.LBB29_6:
	xorl	%edi, %edi
	callq	new_D_Scope
	movq	%rax, 272(%r15)
	orb	$6, (%rax)
.LBB29_7:
	movl	80(%r15), %esi
	movq	%r15, %rdi
	callq	exhaustive_parse
	testl	%eax, %eax
	je	.LBB29_9
# BB#8:
	xorl	%r14d, %r14d
	jmp	.LBB29_24
.LBB29_9:
	movq	280(%r15), %rbx
	cmpl	$1, 72(%rbx)
	jne	.LBB29_11
# BB#10:
	movq	80(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	cmpl	$1, 40(%rax)
	je	.LBB29_12
.LBB29_11:                              # %._crit_edge
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	d_fail
	movq	80(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
.LBB29_12:
	movq	112(%rax), %rax
	movq	48(%rax), %rax
	movq	(%rax), %rsi
	movq	%r15, %rdi
	callq	commit_tree
	movq	%rax, %r14
	cmpl	$0, verbose_level(%rip)
	je	.LBB29_13
# BB#14:
	movl	152(%r15), %esi
	movl	160(%r15), %edx
	movl	164(%r15), %ecx
	movl	168(%r15), %r8d
	movl	172(%r15), %r9d
	movl	176(%r15), %eax
	movl	%eax, (%rsp)
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	leaq	88(%r15), %rbx
	cmpl	$0, 88(%r15)
	je	.LBB29_19
# BB#15:
	cmpl	$2, verbose_level(%rip)
	jl	.LBB29_17
# BB#16:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	xprint_paren
	jmp	.LBB29_18
.LBB29_13:                              # %._crit_edge59
	leaq	88(%r15), %rbx
	cmpl	$0, (%rbx)
	jne	.LBB29_21
	jmp	.LBB29_20
.LBB29_17:
	movq	%r14, %rdi
	callq	print_paren
.LBB29_18:
	movl	$10, %edi
	callq	putchar
.LBB29_19:
	cmpl	$0, (%rbx)
	je	.LBB29_20
.LBB29_21:
	incl	32(%r14)
	addq	$152, %r14
	jmp	.LBB29_22
.LBB29_20:
	movl	$1, %r14d
.LBB29_22:
	movq	280(%r15), %rsi
	decl	112(%rsi)
	jne	.LBB29_24
# BB#23:
	movq	%r15, %rdi
	callq	free_SNode
.LBB29_24:
	movq	$0, 280(%r15)
	movq	%r15, %rdi
	callq	free_parser_working_data
	movq	%r14, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end29:
	.size	dparse, .Lfunc_end29-dparse
	.cfi_endproc

	.p2align	4, 0x90
	.type	exhaustive_parse,@function
exhaustive_parse:                       # @exhaustive_parse
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi148:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi149:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi151:
	.cfi_def_cfa_offset 592
.Lcfi152:
	.cfi_offset %rbx, -56
.Lcfi153:
	.cfi_offset %r12, -48
.Lcfi154:
	.cfi_offset %r13, -40
.Lcfi155:
	.cfi_offset %r14, -32
.Lcfi156:
	.cfi_offset %r15, -24
.Lcfi157:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %rbp
	movq	128(%rbp), %rax
	movq	%rax, 48(%rbp)
	movups	48(%rbp), %xmm0
	movups	64(%rbp), %xmm1
	movaps	%xmm1, 272(%rsp)
	movaps	%xmm0, 256(%rsp)
	leaq	256(%rsp), %r15
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	*8(%rbp)
	movq	(%rbp), %r8
	movq	144(%rbp), %rax
	movslq	%ebx, %rcx
	imulq	$120, %rcx, %rsi
	addq	8(%rax), %rsi
	movq	272(%rbp), %rcx
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	add_SNode
	movq	%rax, %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 512(%rsp)
	movaps	%xmm0, 496(%rsp)
	movaps	%xmm0, 480(%rsp)
	movaps	%xmm0, 464(%rsp)
	movaps	%xmm0, 448(%rsp)
	movaps	%xmm0, 432(%rsp)
	movaps	%xmm0, 416(%rsp)
	movaps	%xmm0, 400(%rsp)
	movaps	%xmm0, 384(%rsp)
	movaps	%xmm0, 368(%rsp)
	movaps	%xmm0, 352(%rsp)
	movaps	%xmm0, 336(%rsp)
	movaps	%xmm0, 320(%rsp)
	movaps	%xmm0, 304(%rsp)
	movaps	%xmm0, 288(%rsp)
	movq	8(%rbp), %rax
	movq	%rax, 504(%rsp)
	movq	(%rbp), %rax
	movq	%rax, 512(%rsp)
	movq	272(%rbp), %rax
	movq	%rax, 496(%rsp)
	movq	%rax, 424(%rsp)
	movq	256(%rsp), %rcx
	movq	%rcx, 480(%rsp)
	leaq	288(%rsp), %r8
	xorl	%esi, %esi
	movl	$0, %r9d
	movq	%rbp, %rdi
	movq	%r15, %rdx
	pushq	$0
.Lcfi158:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi159:
	.cfi_adjust_cfa_offset 8
	callq	add_PNode
	addq	$16, %rsp
.Lcfi160:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %rbx
	movq	%rbx, 64(%r14)
	addq	$72, %r14
	leaq	336(%rbp), %rcx
	movq	336(%rbp), %rsi
	testq	%rsi, %rsi
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	je	.LBB30_1
# BB#2:
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
	jmp	.LBB30_3
.LBB30_1:
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rsi
.LBB30_3:                               # %new_ZNode.exit
	leaq	48(%rbp), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%rbx, (%rsi)
	movl	$0, 8(%rsi)
	movq	$0, 16(%rsi)
	movq	%r14, %rdi
	callq	set_add_znode
	incl	32(%rbx)
	leaq	264(%rbp), %r15
	leaq	344(%rbp), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	360(%rbp), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	leaq	392(%rbp), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	leaq	400(%rbp), %rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	leaq	395(%rbp), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	leaq	396(%rbp), %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%r15, 48(%rsp)          # 8-byte Spill
	jmp	.LBB30_4
	.p2align	4, 0x90
.LBB30_122:                             # %._crit_edge230.i
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	48(%rsp), %r15          # 8-byte Reload
	cmpq	$0, (%r15)
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB30_124
# BB#123:                               #   in Loop: Header=BB30_4 Depth=1
	cmpq	$0, 256(%rbp)
	jne	.LBB30_124
	jmp	.LBB30_209
.LBB30_125:                             #   in Loop: Header=BB30_4 Depth=1
	cmpl	$0, 92(%rbp)
	jne	.LBB30_167
# BB#126:                               #   in Loop: Header=BB30_4 Depth=1
	cmpq	$0, 8(%r15)
	je	.LBB30_167
# BB#127:                               # %.lr.ph83.i
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	(%r15), %r13
	movq	24(%r13), %rbp
	movq	48(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB30_128
.LBB30_156:                             #   in Loop: Header=BB30_128 Depth=2
	movq	8(%r14), %rax
	movq	%rax, (%rdi)
	movq	(%r14), %rsi
	decl	112(%rsi)
	jne	.LBB30_158
# BB#157:                               #   in Loop: Header=BB30_128 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free_SNode
.LBB30_158:                             #   in Loop: Header=BB30_128 Depth=2
	movq	%r14, %rdi
	callq	free
	jmp	.LBB30_164
.LBB30_160:                             #   in Loop: Header=BB30_128 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbx)
	movq	(%r15), %rsi
	decl	112(%rsi)
	jne	.LBB30_162
# BB#161:                               #   in Loop: Header=BB30_128 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free_SNode
.LBB30_162:                             #   in Loop: Header=BB30_128 Depth=2
	movq	%r15, %rdi
	callq	free
	movq	(%rbx), %r15
	jmp	.LBB30_164
	.p2align	4, 0x90
.LBB30_128:                             #   Parent Loop BB30_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB30_137 Depth 3
                                        #         Child Loop BB30_148 Depth 4
                                        #           Child Loop BB30_149 Depth 5
	cmpl	$1, 72(%r13)
	jne	.LBB30_164
# BB#129:                               #   in Loop: Header=BB30_128 Depth=2
	movq	80(%r13), %rax
	movq	(%rax), %r10
	movq	(%r10), %rcx
	movl	8(%rcx), %eax
	cmpl	$10, %eax
	jne	.LBB30_133
# BB#130:                               #   in Loop: Header=BB30_128 Depth=2
	cmpl	$1, 8(%r10)
	jne	.LBB30_164
# BB#131:                               #   in Loop: Header=BB30_128 Depth=2
	movq	16(%r10), %rax
	movq	(%rax), %rax
	cmpl	$1, 72(%rax)
	jne	.LBB30_164
# BB#132:                               #   in Loop: Header=BB30_128 Depth=2
	movq	80(%rax), %rax
	movq	(%rax), %r10
	movq	(%r10), %rcx
	movl	8(%rcx), %eax
	.p2align	4, 0x90
.LBB30_133:                             #   in Loop: Header=BB30_128 Depth=2
	testb	$16, %al
	je	.LBB30_164
# BB#134:                               #   in Loop: Header=BB30_128 Depth=2
	testq	%r10, %r10
	je	.LBB30_164
# BB#135:                               #   in Loop: Header=BB30_128 Depth=2
	movq	8(%r15), %r14
	testq	%r14, %r14
	je	.LBB30_164
# BB#136:                               # %.lr.ph.i94
                                        #   in Loop: Header=BB30_128 Depth=2
	movq	%rcx, (%rsp)            # 8-byte Spill
	leaq	8(%r15), %rdi
	movq	%r14, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB30_137:                             #   Parent Loop BB30_4 Depth=1
                                        #     Parent Loop BB30_128 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB30_148 Depth 4
                                        #           Child Loop BB30_149 Depth 5
	movq	(%r14), %rdx
	cmpq	%rbp, 24(%rdx)
	jne	.LBB30_164
# BB#138:                               #   in Loop: Header=BB30_137 Depth=3
	cmpl	$1, 72(%rdx)
	jne	.LBB30_163
# BB#139:                               #   in Loop: Header=BB30_137 Depth=3
	movq	80(%rdx), %rax
	movq	(%rax), %rsi
	movq	(%rsi), %r11
	movl	8(%r11), %eax
	cmpl	$10, %eax
	jne	.LBB30_143
# BB#140:                               #   in Loop: Header=BB30_137 Depth=3
	cmpl	$1, 8(%rsi)
	jne	.LBB30_163
# BB#141:                               #   in Loop: Header=BB30_137 Depth=3
	movq	16(%rsi), %rax
	movq	(%rax), %rax
	cmpl	$1, 72(%rax)
	jne	.LBB30_163
# BB#142:                               #   in Loop: Header=BB30_137 Depth=3
	movq	80(%rax), %rax
	movq	(%rax), %rsi
	movq	(%rsi), %r11
	movl	8(%r11), %eax
	.p2align	4, 0x90
.LBB30_143:                             #   in Loop: Header=BB30_137 Depth=3
	testb	$16, %al
	je	.LBB30_163
# BB#144:                               #   in Loop: Header=BB30_137 Depth=3
	testq	%rsi, %rsi
	je	.LBB30_163
# BB#145:                               #   in Loop: Header=BB30_137 Depth=3
	movl	8(%r10), %ecx
	cmpl	8(%rsi), %ecx
	jne	.LBB30_163
# BB#146:                               # %.preheader19.i.i
                                        #   in Loop: Header=BB30_137 Depth=3
	testl	%ecx, %ecx
	je	.LBB30_153
# BB#147:                               # %.preheader.us.preheader.i.i
                                        #   in Loop: Header=BB30_137 Depth=3
	movq	16(%r10), %r12
	movq	16(%rsi), %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB30_148:                             # %.preheader.us.i.i
                                        #   Parent Loop BB30_4 Depth=1
                                        #     Parent Loop BB30_128 Depth=2
                                        #       Parent Loop BB30_137 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB30_149 Depth 5
	movq	(%r12,%rax,8), %r9
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB30_149:                             #   Parent Loop BB30_4 Depth=1
                                        #     Parent Loop BB30_128 Depth=2
                                        #       Parent Loop BB30_137 Depth=3
                                        #         Parent Loop BB30_148 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpq	(%rsi,%r8,8), %r9
	je	.LBB30_151
# BB#150:                               #   in Loop: Header=BB30_149 Depth=5
	incq	%r8
	cmpl	%ecx, %r8d
	jb	.LBB30_149
.LBB30_151:                             # %._crit_edge.us.i.i
                                        #   in Loop: Header=BB30_148 Depth=4
	cmpl	%ecx, %r8d
	jae	.LBB30_163
# BB#152:                               #   in Loop: Header=BB30_148 Depth=4
	incq	%rax
	cmpl	%ecx, %eax
	jb	.LBB30_148
.LBB30_153:                             # %VecSNode_equal.exit.i
                                        #   in Loop: Header=BB30_137 Depth=3
	movq	(%r13), %rax
	movslq	112(%rax), %r8
	movq	(%rdx), %rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	144(%rcx), %rdx
	movq	8(%rdx), %r9
	movq	%rsi, %rcx
	subq	%r9, %rcx
	sarq	$3, %rcx
	movabsq	$-1229782938247303441, %rdx # imm = 0xEEEEEEEEEEEEEEEF
	imulq	%rdx, %rcx
	cmpq	%rcx, %r8
	je	.LBB30_155
# BB#154:                               #   in Loop: Header=BB30_137 Depth=3
	movslq	112(%rsi), %rcx
	subq	%r9, %rax
	sarq	$3, %rax
	movabsq	$-1229782938247303441, %rdx # imm = 0xEEEEEEEEEEEEEEEF
	imulq	%rdx, %rax
	cmpq	%rax, %rcx
	jne	.LBB30_163
.LBB30_155:                             #   in Loop: Header=BB30_137 Depth=3
	movl	12(%r11), %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	cmpl	%eax, 12(%rcx)
	jg	.LBB30_156
# BB#159:                               #   in Loop: Header=BB30_137 Depth=3
	jl	.LBB30_160
	.p2align	4, 0x90
.LBB30_163:                             # %binary_op_ZNode.exit67.thread.i
                                        #   in Loop: Header=BB30_137 Depth=3
	movq	%r14, %rdi
	movq	8(%r14), %r14
	addq	$8, %rdi
	testq	%r14, %r14
	jne	.LBB30_137
	.p2align	4, 0x90
.LBB30_164:                             # %.critedge1.i
                                        #   in Loop: Header=BB30_128 Depth=2
	movq	8(%r15), %rax
	testq	%rax, %rax
	je	.LBB30_166
# BB#165:                               # %.critedge1._crit_edge.i
                                        #   in Loop: Header=BB30_128 Depth=2
	movq	%r15, %rbx
	addq	$8, %rbx
	movq	(%rax), %r13
	cmpq	%rbp, 24(%r13)
	movq	%rax, %r15
	je	.LBB30_128
.LBB30_166:                             # %cmp_stacks.exit.loopexit
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r15
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB30_167:                             # %cmp_stacks.exit
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	(%r15), %rax
	movq	24(%rax), %rbx
	cmpq	88(%rsp), %rbx          # 8-byte Folded Reload
	jbe	.LBB30_169
# BB#168:                               #   in Loop: Header=BB30_4 Depth=1
	movq	%rbp, %rdi
	callq	free_old_nodes
	movq	%rbx, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
.LBB30_169:                             #   in Loop: Header=BB30_4 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
	leal	1(%rcx), %eax
	xorl	%edx, %edx
	cmpl	112(%rbp), %ecx
	setge	%dl
	movq	48(%rsp), %r15          # 8-byte Reload
	jl	.LBB30_170
# BB#171:                               #   in Loop: Header=BB30_4 Depth=1
	movq	(%r15), %r13
	cmpq	$0, 8(%r13)
	je	.LBB30_173
# BB#172:                               #   in Loop: Header=BB30_4 Depth=1
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testq	%r13, %r13
	jne	.LBB30_176
	jmp	.LBB30_205
.LBB30_170:                             #   in Loop: Header=BB30_4 Depth=1
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jmp	.LBB30_174
.LBB30_173:                             #   in Loop: Header=BB30_4 Depth=1
	movq	(%r13), %rsi
	movq	%rbp, %rdi
	callq	commit_stack
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
.LBB30_174:                             # %thread-pre-split
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.LBB30_205
.LBB30_176:                             #   in Loop: Header=BB30_4 Depth=1
	movq	(%r13), %rax
	cmpq	%rbx, 24(%rax)
	je	.LBB30_177
.LBB30_205:                             # %.critedge1
                                        #   in Loop: Header=BB30_4 Depth=1
	testl	%edx, %edx
	je	.LBB30_4
# BB#206:                               #   in Loop: Header=BB30_4 Depth=1
	movq	256(%rbp), %rax
	testq	%rax, %rax
	je	.LBB30_4
# BB#207:                               #   in Loop: Header=BB30_4 Depth=1
	cmpq	$0, 40(%rax)
	jne	.LBB30_4
# BB#208:                               #   in Loop: Header=BB30_4 Depth=1
	movq	8(%rax), %rsi
	movq	%rbp, %rdi
	callq	commit_stack
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jmp	.LBB30_4
.LBB30_177:                             #   in Loop: Header=BB30_4 Depth=1
	movl	%edx, 32(%rsp)          # 4-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	8(%r13), %rcx
	movq	%rcx, 264(%rbp)
	movq	(%rax), %r14
	movups	24(%rax), %xmm0
	movups	40(%rax), %xmm1
	movaps	%xmm1, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movq	$0, 176(%rsp)
	incl	160(%rbp)
	xorl	%ebx, %ebx
	cmpq	$0, 72(%r14)
	je	.LBB30_180
# BB#178:                               #   in Loop: Header=BB30_4 Depth=1
	movl	$0, 400(%rbp)
	movb	$0, 395(%rbp)
	movq	384(%rbp), %rax
	movaps	112(%rsp), %xmm0
	movaps	128(%rsp), %xmm1
	movups	%xmm1, 24(%rax)
	movups	%xmm0, 8(%rax)
	movq	384(%rbp), %rax
	leaq	8(%rax), %rdi
	leaq	28(%rax), %rsi
	leaq	32(%rax), %rdx
	subq	$8, %rsp
.Lcfi161:
	.cfi_adjust_cfa_offset 8
	movq	152(%rsp), %rcx         # 8-byte Reload
	movq	240(%rsp), %r8          # 8-byte Reload
	movq	232(%rsp), %r9          # 8-byte Reload
	pushq	224(%rsp)               # 8-byte Folded Reload
.Lcfi162:
	.cfi_adjust_cfa_offset 8
	callq	*72(%r14)
	addq	$16, %rsp
.Lcfi163:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB30_180
# BB#179:                               #   in Loop: Header=BB30_4 Depth=1
	movq	384(%rbp), %rax
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$1, %ebx
.LBB30_180:                             #   in Loop: Header=BB30_4 Depth=1
	cmpq	$0, 80(%r14)
	je	.LBB30_182
# BB#181:                               #   in Loop: Header=BB30_4 Depth=1
	movq	384(%rbp), %rax
	leaq	(%rbx,%rbx,4), %rcx
	leaq	(%rax,%rcx,8), %rdx
	leaq	112(%rsp), %rdi
	movq	%r14, %rsi
	callq	scan_buffer
	addl	%ebx, %eax
	movl	%eax, %ebx
.LBB30_182:                             #   in Loop: Header=BB30_4 Depth=1
	testl	%ebx, %ebx
	jle	.LBB30_202
# BB#183:                               # %.lr.ph.i97
                                        #   in Loop: Header=BB30_4 Depth=1
	movl	%ebx, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB30_184
.LBB30_189:                             # %._crit_edge81.i
                                        #   in Loop: Header=BB30_184 Depth=2
	movq	(%r13), %rcx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	jmp	.LBB30_200
	.p2align	4, 0x90
.LBB30_184:                             #   Parent Loop BB30_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB30_193 Depth 3
	movq	384(%rbp), %r15
	incl	164(%rbp)
	leaq	(%rbx,%rbx,4), %r12
	movq	(%r15,%r12,8), %rax
	movzwl	(%rax), %esi
	movq	(%r13), %rdx
	movq	8(%r15,%r12,8), %rcx
	movq	64(%rdx), %r8
	addq	$24, %rdx
	movl	$0, %r9d
	movq	%rbp, %rdi
	pushq	%rax
.Lcfi164:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi165:
	.cfi_adjust_cfa_offset 8
	callq	add_PNode
	addq	$16, %rsp
.Lcfi166:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB30_185
# BB#186:                               #   in Loop: Header=BB30_184 Depth=2
	leaq	8(%r15,%r12,8), %rax
	movq	176(%rsp), %rcx
	testq	%rcx, %rcx
	je	.LBB30_190
# BB#187:                               #   in Loop: Header=BB30_184 Depth=2
	cmpq	(%rax), %rcx
	jne	.LBB30_190
# BB#188:                               #   in Loop: Header=BB30_184 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpq	216(%r14), %rcx
	je	.LBB30_189
	.p2align	4, 0x90
.LBB30_190:                             #   in Loop: Header=BB30_184 Depth=2
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movaps	%xmm1, 192(%rsp)
	movaps	%xmm0, 176(%rsp)
	movq	216(%r14), %rcx
	movq	%r14, %rdx
	addq	$224, %rdx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	leaq	176(%rsp), %rsi
	movq	%rcx, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	callq	*%rcx
	movq	(%r13), %rcx
	movl	44(%rcx), %eax
	testl	%eax, %eax
	movq	48(%rsp), %r15          # 8-byte Reload
	jns	.LBB30_192
# BB#191:                               #   in Loop: Header=BB30_184 Depth=2
	movl	40(%rcx), %eax
.LBB30_192:                             #   in Loop: Header=BB30_184 Depth=2
	movl	%eax, 192(%rsp)
	movq	80(%rcx), %rax
	jmp	.LBB30_193
	.p2align	4, 0x90
.LBB30_197:                             # %.backedge.i.i
                                        #   in Loop: Header=BB30_193 Depth=3
	movq	80(%rax), %rax
.LBB30_193:                             #   Parent Loop BB30_4 Depth=1
                                        #     Parent Loop BB30_184 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB30_198
# BB#194:                               # %.lr.ph.i.i100
                                        #   in Loop: Header=BB30_193 Depth=3
	movq	(%rax), %rsi
	movq	192(%rsi), %rdx
	cmpq	%rdx, 160(%rsi)
	jne	.LBB30_199
# BB#195:                               #   in Loop: Header=BB30_193 Depth=3
	cmpl	$0, 8(%rax)
	je	.LBB30_198
# BB#196:                               #   in Loop: Header=BB30_193 Depth=3
	movq	16(%rax), %rax
	movq	(%rax), %rax
	cmpl	$0, 72(%rax)
	jne	.LBB30_197
	.p2align	4, 0x90
.LBB30_198:                             # %.critedge10.i.i
                                        #   in Loop: Header=BB30_184 Depth=2
	movq	128(%rbp), %rdx
.LBB30_199:                             # %find_ws_before.exit.i
                                        #   in Loop: Header=BB30_184 Depth=2
	movq	%rdx, 120(%r14)
	movq	176(%rsp), %rax
	movq	%rax, 128(%r14)
.LBB30_200:                             #   in Loop: Header=BB30_184 Depth=2
	movq	%rbp, %rdi
	leaq	176(%rsp), %rsi
	movq	%r14, %rdx
	callq	goto_PNode
	jmp	.LBB30_201
	.p2align	4, 0x90
.LBB30_185:                             #   in Loop: Header=BB30_184 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
.LBB30_201:                             #   in Loop: Header=BB30_184 Depth=2
	incq	%rbx
	cmpq	(%rsp), %rbx            # 8-byte Folded Reload
	jne	.LBB30_184
.LBB30_202:                             # %._crit_edge.i103
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	(%r13), %rsi
	decl	112(%rsi)
	jne	.LBB30_204
# BB#203:                               #   in Loop: Header=BB30_4 Depth=1
	movq	%rbp, %rdi
	callq	free_SNode
.LBB30_204:                             # %shift_one.exit
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	304(%rbp), %rax
	movq	%rax, 8(%r13)
	movq	%r13, 304(%rbp)
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	32(%rsp), %edx          # 4-byte Reload
	jmp	.LBB30_174
	.p2align	4, 0x90
.LBB30_124:                             # %.critedge106
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	%r13, %rdi
	callq	free
.LBB30_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_5 Depth 2
                                        #     Child Loop BB30_128 Depth 2
                                        #       Child Loop BB30_137 Depth 3
                                        #         Child Loop BB30_148 Depth 4
                                        #           Child Loop BB30_149 Depth 5
                                        #     Child Loop BB30_184 Depth 2
                                        #       Child Loop BB30_193 Depth 3
                                        #     Child Loop BB30_21 Depth 2
                                        #     Child Loop BB30_27 Depth 2
                                        #       Child Loop BB30_30 Depth 3
                                        #         Child Loop BB30_32 Depth 4
                                        #         Child Loop BB30_47 Depth 4
                                        #       Child Loop BB30_35 Depth 3
                                        #       Child Loop BB30_62 Depth 3
                                        #         Child Loop BB30_70 Depth 4
                                        #     Child Loop BB30_87 Depth 2
                                        #     Child Loop BB30_102 Depth 2
                                        #       Child Loop BB30_104 Depth 3
                                        #         Child Loop BB30_113 Depth 4
	movq	256(%rbp), %rsi
	testq	%rsi, %rsi
	jne	.LBB30_5
	jmp	.LBB30_12
	.p2align	4, 0x90
.LBB30_11:                              #   in Loop: Header=BB30_5 Depth=2
	movq	40(%rsi), %rax
	movq	%rax, 256(%rbp)
	movq	%rbp, %rdi
	callq	reduce_one
	movq	256(%rbp), %rsi
.LBB30_9:                               # %.preheader
                                        #   in Loop: Header=BB30_5 Depth=2
	testq	%rsi, %rsi
	je	.LBB30_12
.LBB30_10:                              # %.lr.ph
                                        #   in Loop: Header=BB30_5 Depth=2
	movq	8(%rsi), %rax
	cmpq	%rbx, 24(%rax)
	je	.LBB30_11
.LBB30_5:                               # %.lr.ph141
                                        #   Parent Loop BB30_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsi), %rax
	movq	24(%rax), %rbx
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.LBB30_7
# BB#6:                                 #   in Loop: Header=BB30_5 Depth=2
	movq	(%rax), %rax
	cmpq	%rbx, 24(%rax)
	jb	.LBB30_12
.LBB30_7:                               #   in Loop: Header=BB30_5 Depth=2
	cmpq	88(%rsp), %rbx          # 8-byte Folded Reload
	jbe	.LBB30_9
# BB#8:                                 #   in Loop: Header=BB30_5 Depth=2
	movq	%rbp, %rdi
	callq	free_old_nodes
	movq	256(%rbp), %rsi
	movq	%rbx, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	testq	%rsi, %rsi
	jne	.LBB30_10
	.p2align	4, 0x90
.LBB30_12:                              # %.critedge._crit_edge
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB30_125
# BB#13:                                #   in Loop: Header=BB30_4 Depth=1
	movq	280(%rbp), %rax
	testq	%rax, %rax
	je	.LBB30_16
# BB#14:                                #   in Loop: Header=BB30_4 Depth=1
	movq	24(%rax), %rcx
	xorl	%eax, %eax
	cmpq	136(%rbp), %rcx
	je	.LBB30_210
# BB#15:                                #   in Loop: Header=BB30_4 Depth=1
	cmpl	$0, 120(%rbp)
	jne	.LBB30_210
.LBB30_16:                              #   in Loop: Header=BB30_4 Depth=1
	movl	$80000, %edi            # imm = 0x13880
	callq	malloc
	movq	%rax, %r13
	movq	248(%rbp), %rsi
	movl	$1, %eax
	testq	%rsi, %rsi
	je	.LBB30_210
# BB#17:                                #   in Loop: Header=BB30_4 Depth=1
	movups	24(%rsi), %xmm0
	movups	40(%rsi), %xmm1
	movq	104(%rsp), %rcx         # 8-byte Reload
	movups	%xmm1, 16(%rcx)
	movups	%xmm0, (%rcx)
	cmpl	$0, 116(%rbp)
	je	.LBB30_210
# BB#18:                                #   in Loop: Header=BB30_4 Depth=1
	movl	72(%rbp), %ecx
	cmpl	288(%rbp), %ecx
	jle	.LBB30_20
# BB#19:                                # %.preheader219.i
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	%ecx, 288(%rbx)
	incl	124(%rbx)
	movq	%rbx, %rdi
	callq	*24(%rbx)
	movq	248(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB30_209
.LBB30_20:                              # %.lr.ph264.preheader.i
                                        #   in Loop: Header=BB30_4 Depth=1
	xorl	%r15d, %r15d
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB30_21:                              # %.lr.ph264.i
                                        #   Parent Loop BB30_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$9998, %r15d            # imm = 0x270E
	jg	.LBB30_23
# BB#22:                                #   in Loop: Header=BB30_21 Depth=2
	movslq	%r15d, %rdx
	incl	%r15d
	movq	%rcx, (%r13,%rdx,8)
.LBB30_23:                              #   in Loop: Header=BB30_21 Depth=2
	movq	128(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB30_21
# BB#24:                                # %._crit_edge265.i
                                        #   in Loop: Header=BB30_4 Depth=1
	testl	%r15d, %r15d
	jle	.LBB30_209
# BB#25:                                # %.lr.ph256.preheader.i
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	24(%rsi), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	incq	%rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%r13, 160(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB30_27:                              # %.lr.ph256.i
                                        #   Parent Loop BB30_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB30_30 Depth 3
                                        #         Child Loop BB30_32 Depth 4
                                        #         Child Loop BB30_47 Depth 4
                                        #       Child Loop BB30_35 Depth 3
                                        #       Child Loop BB30_62 Depth 3
                                        #         Child Loop BB30_70 Depth 4
	movq	(%r13,%r9,8), %rcx
	movq	(%rcx), %rax
	movl	48(%rax), %r14d
	testq	%r14, %r14
	movq	%r9, 248(%rsp)          # 8-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	je	.LBB30_59
# BB#28:                                # %.lr.ph238.i
                                        #   in Loop: Header=BB30_27 Depth=2
	movq	168(%rsp), %rcx         # 8-byte Reload
	movb	(%rcx), %cl
	movb	%cl, 15(%rsp)           # 1-byte Spill
	testb	%cl, %cl
	movq	56(%rax), %rbp
	je	.LBB30_35
# BB#29:                                # %.lr.ph238.split.i.preheader
                                        #   in Loop: Header=BB30_27 Depth=2
	xorl	%r13d, %r13d
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	jmp	.LBB30_30
.LBB30_55:                              #   in Loop: Header=BB30_30 Depth=3
	jne	.LBB30_58
# BB#56:                                #   in Loop: Header=BB30_30 Depth=3
	movq	64(%rsp), %rax          # 8-byte Reload
	movzwl	(%rax), %eax
	cmpw	(%r12), %ax
	jb	.LBB30_57
	jmp	.LBB30_58
	.p2align	4, 0x90
.LBB30_30:                              # %.lr.ph238.split.i
                                        #   Parent Loop BB30_4 Depth=1
                                        #     Parent Loop BB30_27 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB30_32 Depth 4
                                        #         Child Loop BB30_47 Depth 4
	movq	%r13, %r12
	shlq	$4, %r12
	movq	8(%rbp,%r12), %r14
	movq	%r14, %rdi
	callq	strlen
	cmpl	$1, %eax
	jne	.LBB30_31
# BB#46:                                # %.preheader.i.i
                                        #   in Loop: Header=BB30_30 Depth=3
	movb	(%r14), %al
	movq	80(%rsp), %rbx          # 8-byte Reload
	movb	15(%rsp), %cl           # 1-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB30_47:                              # %.lr.ph.i.i
                                        #   Parent Loop BB30_4 Depth=1
                                        #     Parent Loop BB30_27 Depth=2
                                        #       Parent Loop BB30_30 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	%al, %cl
	je	.LBB30_51
# BB#48:                                #   in Loop: Header=BB30_47 Depth=4
	movzbl	(%rbx), %ecx
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB30_47
# BB#49:                                # %.critedge.i.i
                                        #   in Loop: Header=BB30_30 Depth=3
	testb	%al, %al
	jne	.LBB30_58
	jmp	.LBB30_51
	.p2align	4, 0x90
.LBB30_31:                              # %.lr.ph22.i.i
                                        #   in Loop: Header=BB30_30 Depth=3
	movslq	%eax, %rbp
	movq	168(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB30_32:                              #   Parent Loop BB30_4 Depth=1
                                        #     Parent Loop BB30_27 Depth=2
                                        #       Parent Loop BB30_30 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB30_50
# BB#33:                                #   in Loop: Header=BB30_32 Depth=4
	cmpb	$0, 1(%rbx)
	leaq	1(%rbx), %rbx
	jne	.LBB30_32
# BB#34:                                #   in Loop: Header=BB30_30 Depth=3
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB30_58
	.p2align	4, 0x90
.LBB30_50:                              # %find_substr.exit.i
                                        #   in Loop: Header=BB30_30 Depth=3
	addq	%rbp, %rbx
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	je	.LBB30_58
.LBB30_51:                              # %find_substr.exit.thread214.i
                                        #   in Loop: Header=BB30_30 Depth=3
	addq	%rbp, %r12
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB30_57
# BB#52:                                # %find_substr.exit.thread214.i
                                        #   in Loop: Header=BB30_30 Depth=3
	cmpq	32(%rsp), %rbx          # 8-byte Folded Reload
	jb	.LBB30_57
# BB#53:                                #   in Loop: Header=BB30_30 Depth=3
	jne	.LBB30_58
# BB#54:                                #   in Loop: Header=BB30_30 Depth=3
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	56(%rax), %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	cmpl	%eax, 56(%rcx)
	jae	.LBB30_55
	.p2align	4, 0x90
.LBB30_57:                              #   in Loop: Header=BB30_30 Depth=3
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB30_58:                              # %find_substr.exit.thread.i
                                        #   in Loop: Header=BB30_30 Depth=3
	incq	%r13
	cmpq	%r14, %r13
	jne	.LBB30_30
	jmp	.LBB30_59
.LBB30_42:                              #   in Loop: Header=BB30_35 Depth=3
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB30_45
	.p2align	4, 0x90
.LBB30_35:                              # %.lr.ph238.split.us.i
                                        #   Parent Loop BB30_4 Depth=1
                                        #     Parent Loop BB30_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	cmpl	$1, %eax
	jne	.LBB30_45
# BB#36:                                # %.preheader.i.us.i
                                        #   in Loop: Header=BB30_35 Depth=3
	cmpb	$0, (%rbx)
	jne	.LBB30_45
# BB#37:                                # %find_substr.exit.thread214.us.i
                                        #   in Loop: Header=BB30_35 Depth=3
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB30_44
# BB#38:                                # %find_substr.exit.thread214.us.i
                                        #   in Loop: Header=BB30_35 Depth=3
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jb	.LBB30_44
# BB#39:                                #   in Loop: Header=BB30_35 Depth=3
	jne	.LBB30_45
# BB#40:                                #   in Loop: Header=BB30_35 Depth=3
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	56(%rax), %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	cmpl	%eax, 56(%rcx)
	jb	.LBB30_44
# BB#41:                                #   in Loop: Header=BB30_35 Depth=3
	jne	.LBB30_42
# BB#43:                                #   in Loop: Header=BB30_35 Depth=3
	movq	64(%rsp), %rax          # 8-byte Reload
	movzwl	(%rax), %eax
	cmpw	(%rbp), %ax
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jae	.LBB30_45
.LBB30_44:                              #   in Loop: Header=BB30_35 Depth=3
	movq	%rbp, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB30_45:                              # %find_substr.exit.thread.us.i
                                        #   in Loop: Header=BB30_35 Depth=3
	addq	$16, %rbp
	decq	%r14
	jne	.LBB30_35
.LBB30_59:                              # %.loopexit218.i
                                        #   in Loop: Header=BB30_27 Depth=2
	movq	248(%rsp), %r9          # 8-byte Reload
	incq	%r9
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	72(%rcx), %eax
	testq	%rax, %rax
	je	.LBB30_60
# BB#61:                                # %.lr.ph248.i
                                        #   in Loop: Header=BB30_27 Depth=2
	movq	80(%rcx), %r8
	xorl	%edx, %edx
	movq	160(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB30_62:                              #   Parent Loop BB30_4 Depth=1
                                        #     Parent Loop BB30_27 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB30_70 Depth 4
	movq	(%r8,%rdx,8), %rsi
	testq	%rsi, %rsi
	je	.LBB30_75
# BB#63:                                # %.preheader215.i
                                        #   in Loop: Header=BB30_62 Depth=3
	movl	8(%rsi), %edi
	testq	%rdi, %rdi
	je	.LBB30_75
# BB#64:                                # %.lr.ph243.i
                                        #   in Loop: Header=BB30_62 Depth=3
	testb	$1, %dil
	jne	.LBB30_66
# BB#65:                                #   in Loop: Header=BB30_62 Depth=3
	xorl	%ebp, %ebp
	cmpl	$1, %edi
	jne	.LBB30_70
	jmp	.LBB30_75
.LBB30_66:                              #   in Loop: Header=BB30_62 Depth=3
	cmpl	$9998, %r15d            # imm = 0x270E
	jg	.LBB30_68
# BB#67:                                #   in Loop: Header=BB30_62 Depth=3
	movq	16(%rsi), %rbp
	movq	(%rbp), %rbp
	movslq	%r15d, %rbx
	incl	%r15d
	movq	%rbp, (%r13,%rbx,8)
.LBB30_68:                              # %.prol.loopexit
                                        #   in Loop: Header=BB30_62 Depth=3
	movl	$1, %ebp
	cmpl	$1, %edi
	je	.LBB30_75
	.p2align	4, 0x90
.LBB30_70:                              #   Parent Loop BB30_4 Depth=1
                                        #     Parent Loop BB30_27 Depth=2
                                        #       Parent Loop BB30_62 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	$9998, %r15d            # imm = 0x270E
	jg	.LBB30_72
# BB#71:                                #   in Loop: Header=BB30_70 Depth=4
	movq	16(%rsi), %rbx
	movq	(%rbx,%rbp,8), %rbx
	movslq	%r15d, %rcx
	incl	%r15d
	movq	%rbx, (%r13,%rcx,8)
.LBB30_72:                              #   in Loop: Header=BB30_70 Depth=4
	cmpl	$9999, %r15d            # imm = 0x270F
	jge	.LBB30_74
# BB#73:                                #   in Loop: Header=BB30_70 Depth=4
	movq	16(%rsi), %rcx
	movq	8(%rcx,%rbp,8), %rcx
	movslq	%r15d, %rbx
	incl	%r15d
	movq	%rcx, (%r13,%rbx,8)
.LBB30_74:                              #   in Loop: Header=BB30_70 Depth=4
	addq	$2, %rbp
	cmpq	%rbp, %rdi
	jne	.LBB30_70
	.p2align	4, 0x90
.LBB30_75:                              # %.loopexit.i
                                        #   in Loop: Header=BB30_62 Depth=3
	incq	%rdx
	cmpq	%rax, %rdx
	jne	.LBB30_62
	jmp	.LBB30_26
	.p2align	4, 0x90
.LBB30_60:                              #   in Loop: Header=BB30_27 Depth=2
	movq	160(%rsp), %r13         # 8-byte Reload
.LBB30_26:                              # %.loopexit216.i
                                        #   in Loop: Header=BB30_27 Depth=2
	movslq	%r15d, %rax
	cmpq	%r9, %rax
	jg	.LBB30_27
# BB#76:                                # %._crit_edge257.i
                                        #   in Loop: Header=BB30_4 Depth=1
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB30_209
# BB#77:                                #   in Loop: Header=BB30_4 Depth=1
	movl	$56, %edi
	callq	malloc
	movq	%rax, %r15
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	104(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movaps	%xmm1, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r15)
	movups	%xmm0, 16(%r15)
	movups	%xmm0, (%r15)
	movq	$0, 48(%r15)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	352(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB30_78
# BB#79:                                #   in Loop: Header=BB30_4 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpq	152(%rsp), %rcx         # 8-byte Folded Reload
	je	.LBB30_80
# BB#83:                                #   in Loop: Header=BB30_4 Depth=1
	testb	$7, %al
	je	.LBB30_85
# BB#84:                                #   in Loop: Header=BB30_4 Depth=1
	leal	1(%rax), %edx
	movq	96(%rsp), %rsi          # 8-byte Reload
	movl	%edx, (%rsi)
	movq	%r15, (%rcx,%rax,8)
	jmp	.LBB30_86
	.p2align	4, 0x90
.LBB30_78:                              #   in Loop: Header=BB30_4 Depth=1
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	%rax, 352(%rdx)
	movl	344(%rdx), %eax
	leal	1(%rax), %ecx
	jmp	.LBB30_82
.LBB30_80:                              #   in Loop: Header=BB30_4 Depth=1
	cmpl	$2, %eax
	ja	.LBB30_85
# BB#81:                                #   in Loop: Header=BB30_4 Depth=1
	leal	1(%rax), %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB30_82:                              #   in Loop: Header=BB30_4 Depth=1
	movl	%ecx, 344(%rdx)
	movq	%r15, 360(%rdx,%rax,8)
	jmp	.LBB30_86
	.p2align	4, 0x90
.LBB30_85:                              #   in Loop: Header=BB30_4 Depth=1
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rsi
	callq	vec_add_internal
.LBB30_86:                              #   in Loop: Header=BB30_4 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	movzwl	(%rcx), %eax
	incw	%ax
	movw	%ax, (%r15)
	movzwl	2(%rcx), %eax
	movw	%ax, 2(%r15)
	movq	112(%rsp), %rax
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jae	.LBB30_90
	.p2align	4, 0x90
.LBB30_87:                              # %.lr.ph.i212.i
                                        #   Parent Loop BB30_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, (%rax)
	jne	.LBB30_89
# BB#88:                                #   in Loop: Header=BB30_87 Depth=2
	incl	136(%rsp)
.LBB30_89:                              #   in Loop: Header=BB30_87 Depth=2
	incq	%rax
	cmpq	%rax, 32(%rsp)          # 8-byte Folded Reload
	jne	.LBB30_87
.LBB30_90:                              # %update_line.exit.i
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, 112(%rsp)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, %r12
	movq	80(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rbx
	leaq	224(%rbx), %rdx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	leaq	112(%rsp), %r14
	movq	%r14, %rsi
	callq	*216(%rbx)
	incl	112(%r12)
	movq	112(%rsp), %rcx
	xorl	%esi, %esi
	movl	$0, %r9d
	movq	%rbp, %rdi
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	%rbx, %r8
	pushq	$0
.Lcfi167:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi168:
	.cfi_adjust_cfa_offset 8
	callq	add_PNode
	addq	$16, %rsp
.Lcfi169:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %rbx
	movq	(%r12), %rsi
	movq	136(%rbx), %rcx
	movq	144(%rbx), %r8
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	new_SNode
	movq	%rbx, 64(%rax)
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	72(%rax), %r12
	movq	336(%rbp), %r14
	testq	%r14, %r14
	je	.LBB30_91
# BB#92:                                #   in Loop: Header=BB30_4 Depth=1
	movq	(%r14), %rax
	movq	240(%rsp), %rcx         # 8-byte Reload
	movq	%rax, (%rcx)
	jmp	.LBB30_93
	.p2align	4, 0x90
.LBB30_91:                              #   in Loop: Header=BB30_4 Depth=1
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r14
.LBB30_93:                              # %new_ZNode.exit.i
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	%rbx, (%r14)
	movl	$0, 8(%r14)
	movq	$0, 16(%r14)
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	set_add_znode
	incl	32(%rbx)
	movq	16(%r14), %rax
	leaq	24(%r14), %rdx
	testq	%rax, %rax
	je	.LBB30_94
# BB#95:                                #   in Loop: Header=BB30_4 Depth=1
	leaq	8(%r14), %rdi
	movl	(%rdi), %ecx
	cmpq	%rdx, %rax
	je	.LBB30_96
# BB#98:                                #   in Loop: Header=BB30_4 Depth=1
	testb	$7, %cl
	movq	24(%rsp), %rbp          # 8-byte Reload
	jne	.LBB30_97
	jmp	.LBB30_99
	.p2align	4, 0x90
.LBB30_94:                              #   in Loop: Header=BB30_4 Depth=1
	movq	%rdx, 16(%r14)
	movl	8(%r14), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 8(%r14)
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, 24(%r14,%rax,8)
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB30_100
.LBB30_96:                              #   in Loop: Header=BB30_4 Depth=1
	cmpl	$2, %ecx
	movq	24(%rsp), %rbp          # 8-byte Reload
	ja	.LBB30_99
.LBB30_97:                              #   in Loop: Header=BB30_4 Depth=1
	leal	1(%rcx), %edx
	movl	%edx, (%rdi)
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, (%rax,%rcx,8)
	jmp	.LBB30_100
	.p2align	4, 0x90
.LBB30_99:                              #   in Loop: Header=BB30_4 Depth=1
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rsi
	callq	vec_add_internal
.LBB30_100:                             #   in Loop: Header=BB30_4 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%r14, (%rbp)
	movq	%rax, 8(%rbp)
	incl	112(%rbx)
	movq	%r15, 16(%rbp)
	movq	$0, 24(%rbp)
	movq	$0, 40(%rbp)
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	free_old_nodes
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	reduce_one
	movq	144(%rbx), %rax
	movl	(%rax), %eax
	testq	%rax, %rax
	je	.LBB30_122
# BB#101:                               # %.lr.ph229.i
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	216(%rcx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB30_102:                             #   Parent Loop BB30_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB30_104 Depth 3
                                        #         Child Loop BB30_113 Depth 4
	movq	(%rcx,%rdx,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB30_104
	jmp	.LBB30_121
	.p2align	4, 0x90
.LBB30_120:                             # %._crit_edge.i
                                        #   in Loop: Header=BB30_104 Depth=3
	movq	120(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB30_121
.LBB30_104:                             # %.preheader.i
                                        #   Parent Loop BB30_4 Depth=1
                                        #     Parent Loop BB30_102 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB30_113 Depth 4
	movl	72(%rsi), %edi
	testq	%rdi, %rdi
	je	.LBB30_120
# BB#105:                               # %.lr.ph.i
                                        #   in Loop: Header=BB30_104 Depth=3
	movq	80(%rsi), %rbp
	testb	$1, %dil
	jne	.LBB30_107
# BB#106:                               #   in Loop: Header=BB30_104 Depth=3
	xorl	%ebx, %ebx
	cmpl	$1, %edi
	je	.LBB30_120
	jmp	.LBB30_112
	.p2align	4, 0x90
.LBB30_107:                             #   in Loop: Header=BB30_104 Depth=3
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB30_110
# BB#108:                               #   in Loop: Header=BB30_104 Depth=3
	movq	(%rbx), %rbx
	cmpq	%r15, 16(%rbx)
	jne	.LBB30_110
# BB#109:                               #   in Loop: Header=BB30_104 Depth=3
	movw	$257, 84(%rbx)          # imm = 0x101
.LBB30_110:                             # %.prol.loopexit252
                                        #   in Loop: Header=BB30_104 Depth=3
	movl	$1, %ebx
	cmpl	$1, %edi
	je	.LBB30_120
.LBB30_112:                             # %.lr.ph.i.new
                                        #   in Loop: Header=BB30_104 Depth=3
	subq	%rbx, %rdi
	leaq	8(%rbp,%rbx,8), %rbp
	.p2align	4, 0x90
.LBB30_113:                             #   Parent Loop BB30_4 Depth=1
                                        #     Parent Loop BB30_102 Depth=2
                                        #       Parent Loop BB30_104 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	-8(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB30_116
# BB#114:                               #   in Loop: Header=BB30_113 Depth=4
	movq	(%rbx), %rbx
	cmpq	%r15, 16(%rbx)
	jne	.LBB30_116
# BB#115:                               #   in Loop: Header=BB30_113 Depth=4
	movw	$257, 84(%rbx)          # imm = 0x101
.LBB30_116:                             #   in Loop: Header=BB30_113 Depth=4
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB30_119
# BB#117:                               #   in Loop: Header=BB30_113 Depth=4
	movq	(%rbx), %rbx
	cmpq	%r15, 16(%rbx)
	jne	.LBB30_119
# BB#118:                               #   in Loop: Header=BB30_113 Depth=4
	movw	$257, 84(%rbx)          # imm = 0x101
.LBB30_119:                             #   in Loop: Header=BB30_113 Depth=4
	addq	$16, %rbp
	addq	$-2, %rdi
	jne	.LBB30_113
	jmp	.LBB30_120
	.p2align	4, 0x90
.LBB30_121:                             # %._crit_edge226.i
                                        #   in Loop: Header=BB30_102 Depth=2
	incq	%rdx
	cmpq	%rax, %rdx
	jne	.LBB30_102
	jmp	.LBB30_122
.LBB30_209:                             # %error_recovery.exit
	movq	%r13, %rdi
	callq	free
	movl	$1, %eax
.LBB30_210:                             # %error_recovery.exit.thread
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	exhaustive_parse, .Lfunc_end30-exhaustive_parse
	.cfi_endproc

	.p2align	4, 0x90
	.type	commit_tree,@function
commit_tree:                            # @commit_tree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi170:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi171:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi172:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi173:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi174:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi175:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi176:
	.cfi_def_cfa_offset 144
.Lcfi177:
	.cfi_offset %rbx, -56
.Lcfi178:
	.cfi_offset %r12, -48
.Lcfi179:
	.cfi_offset %r13, -40
.Lcfi180:
	.cfi_offset %r14, -32
.Lcfi181:
	.cfi_offset %r15, -24
.Lcfi182:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r15
	cmpb	$0, 84(%r13)
	je	.LBB31_2
.LBB31_1:                               # %free_ParseTreeBelow.exit
	movq	%r13, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB31_2:
	movq	160(%r13), %rax
	movq	192(%r13), %rcx
	cmpq	%rcx, %rax
	je	.LBB31_18
.LBB31_3:
	movb	$1, 84(%r13)
.LBB31_4:
	movq	104(%r13), %rbx
	testq	%rbx, %rbx
	movq	%r15, 56(%rsp)          # 8-byte Spill
	je	.LBB31_40
# BB#5:
	leaq	152(%r13), %r14
	cmpq	%rcx, %rax
	movl	$0, 8(%rsp)
	movq	$0, 16(%rsp)
	je	.LBB31_20
# BB#6:                                 # %.lr.ph.i.thread
	leaq	24(%rsp), %rbp
	movq	%rbp, 16(%rsp)
	movl	$1, 8(%rsp)
	movq	%r14, 24(%rsp)
.LBB31_7:                               # %.lr.ph.split.i.preheader
	leaq	8(%rsp), %r15
	movq	%rbp, %rax
	testq	%rax, %rax
	jne	.LBB31_9
	jmp	.LBB31_12
	.p2align	4, 0x90
.LBB31_8:                               # %.backedge..lr.ph.split_crit_edge.i
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.LBB31_12
.LBB31_9:
	movl	8(%rsp), %ecx
	cmpq	%rbp, %rax
	je	.LBB31_13
# BB#10:
	testb	$7, %cl
	je	.LBB31_16
# BB#11:
	leaq	152(%rbx), %rdx
	leal	1(%rcx), %esi
	movl	%esi, 8(%rsp)
	movq	%rdx, (%rax,%rcx,8)
	jmp	.LBB31_17
	.p2align	4, 0x90
.LBB31_13:
	cmpl	$2, %ecx
	ja	.LBB31_16
# BB#14:
	leaq	152(%rbx), %rax
	jmp	.LBB31_15
	.p2align	4, 0x90
.LBB31_16:
	leaq	152(%rbx), %rsi
	movq	%r15, %rdi
	callq	vec_add_internal
	jmp	.LBB31_17
	.p2align	4, 0x90
.LBB31_12:
	leaq	152(%rbx), %rax
	movq	%rbp, 16(%rsp)
	movl	8(%rsp), %ecx
.LBB31_15:                              # %.backedge.i
	leal	1(%rcx), %edx
	movl	%edx, 8(%rsp)
	movq	%rax, 24(%rsp,%rcx,8)
.LBB31_17:                              # %.backedge.i
	movq	104(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB31_8
	jmp	.LBB31_34
.LBB31_18:
	movq	16(%r13), %rdx
	testq	%rdx, %rdx
	je	.LBB31_3
# BB#19:
	cmpq	$0, 16(%rdx)
	jne	.LBB31_4
	jmp	.LBB31_3
.LBB31_20:                              # %.lr.ph.i
	movq	%r13, %rdi
	callq	final_actionless
	testl	%eax, %eax
	leaq	24(%rsp), %rbp
	movq	%rbp, 16(%rsp)
	movl	$1, 8(%rsp)
	movq	%r14, 24(%rsp)
	je	.LBB31_7
# BB#21:
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB31_22:                              # %.lr.ph.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 100(%rax)
	jne	.LBB31_24
# BB#23:                                #   in Loop: Header=BB31_22 Depth=1
	movq	160(%rbx), %rax
	cmpq	192(%rbx), %rax
	je	.LBB31_32
.LBB31_24:                              #   in Loop: Header=BB31_22 Depth=1
	movq	16(%rsp), %rcx
	testq	%rcx, %rcx
	je	.LBB31_28
# BB#25:                                #   in Loop: Header=BB31_22 Depth=1
	cmpq	%rbp, %rcx
	movl	8(%rsp), %eax
	je	.LBB31_29
# BB#26:                                #   in Loop: Header=BB31_22 Depth=1
	testb	$7, %al
	je	.LBB31_31
# BB#27:                                #   in Loop: Header=BB31_22 Depth=1
	leaq	152(%rbx), %rdx
	leal	1(%rax), %esi
	movl	%esi, 8(%rsp)
	movq	%rdx, (%rcx,%rax,8)
	jmp	.LBB31_33
	.p2align	4, 0x90
.LBB31_28:                              #   in Loop: Header=BB31_22 Depth=1
	leaq	152(%rbx), %rax
	movq	%rbp, 16(%rsp)
	movl	8(%rsp), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 8(%rsp)
	movq	%rax, 24(%rsp,%rcx,8)
	jmp	.LBB31_33
	.p2align	4, 0x90
.LBB31_29:                              #   in Loop: Header=BB31_22 Depth=1
	cmpl	$3, %eax
	jae	.LBB31_31
# BB#30:                                #   in Loop: Header=BB31_22 Depth=1
	leaq	152(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 8(%rsp)
	movq	%rcx, 24(%rsp,%rax,8)
	jmp	.LBB31_33
	.p2align	4, 0x90
.LBB31_31:                              #   in Loop: Header=BB31_22 Depth=1
	leaq	152(%rbx), %rsi
	movq	%r15, %rdi
	callq	vec_add_internal
	jmp	.LBB31_33
.LBB31_32:                              #   in Loop: Header=BB31_22 Depth=1
	movq	%rbx, %rdi
	callq	final_actionless
	testl	%eax, %eax
	je	.LBB31_24
	.p2align	4, 0x90
.LBB31_33:                              # %.backedge.us.i
                                        #   in Loop: Header=BB31_22 Depth=1
	movq	104(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB31_22
.LBB31_34:                              # %._crit_edge.i
	movl	8(%rsp), %esi
	cmpl	$1, %esi
	movq	56(%rsp), %r15          # 8-byte Reload
	jne	.LBB31_36
# BB#35:
	movq	16(%rsp), %rdi
	movq	(%rdi), %r13
	testq	%rdi, %rdi
	jne	.LBB31_37
	jmp	.LBB31_39
.LBB31_36:
	movq	16(%rsp), %rdx
	movq	%r15, %rdi
	callq	*32(%r15)
	movq	%rax, %r13
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB31_39
.LBB31_37:
	cmpq	%rbp, %rdi
	je	.LBB31_39
# BB#38:
	callq	free
.LBB31_39:                              # %resolve_ambiguities.exit
	addq	$-152, %r13
.LBB31_40:
	movq	144(%r15), %rax
	movq	32(%rax), %rax
	movslq	152(%r13), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	(%rax,%rcx,8), %ecx
	cmpl	$2, %ecx
	sete	%bpl
	cmpl	$0, 96(%r15)
	sete	%al
	cmpl	$0, 40(%r13)
	movl	%ecx, 68(%rsp)          # 4-byte Spill
	je	.LBB31_85
# BB#41:                                # %.lr.ph
	andb	%al, %bpl
	leaq	40(%r13), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	56(%r13), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movb	%bpl, 7(%rsp)           # 1-byte Spill
	.p2align	4, 0x90
.LBB31_42:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_59 Depth 2
                                        #     Child Loop BB31_71 Depth 2
                                        #     Child Loop BB31_74 Depth 2
                                        #     Child Loop BB31_80 Depth 2
                                        #     Child Loop BB31_51 Depth 2
                                        #     Child Loop BB31_53 Depth 2
	movq	48(%r13), %rax
	movslq	%ebx, %r14
	movq	(%rax,%r14,8), %rsi
	movq	%r15, %rdi
	callq	commit_tree
	movq	48(%r13), %rcx
	movq	%rax, (%rcx,%r14,8)
	testb	%bpl, %bpl
	je	.LBB31_84
# BB#43:                                #   in Loop: Header=BB31_42 Depth=1
	movq	48(%r13), %rax
	movq	(%rax,%r14,8), %r12
	movq	144(%r15), %rax
	movq	32(%rax), %rax
	movslq	152(%r12), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	cmpl	$2, (%rax,%rcx,8)
	jne	.LBB31_84
# BB#44:                                #   in Loop: Header=BB31_42 Depth=1
	movl	40(%r12), %r10d
	movslq	40(%r13), %rbp
	cmpq	%r13, %r12
	movq	%r10, 72(%rsp)          # 8-byte Spill
	jne	.LBB31_46
# BB#45:                                #   in Loop: Header=BB31_42 Depth=1
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	d_fail
	movq	72(%rsp), %r10          # 8-byte Reload
.LBB31_46:                              #   in Loop: Header=BB31_42 Depth=1
	cmpl	$1, %r10d
	je	.LBB31_56
# BB#47:                                #   in Loop: Header=BB31_42 Depth=1
	testl	%r10d, %r10d
	jne	.LBB31_57
# BB#48:                                # %.preheader64.i
                                        #   in Loop: Header=BB31_42 Depth=1
	decq	%rbp
	cmpl	%ebx, %ebp
	jle	.LBB31_54
# BB#49:                                # %.lr.ph73.preheader.i
                                        #   in Loop: Header=BB31_42 Depth=1
	movl	%ebp, %ecx
	subl	%ebx, %ecx
	leaq	-1(%rbp), %rax
	subq	%r14, %rax
	andq	$3, %rcx
	je	.LBB31_52
# BB#50:                                # %.lr.ph73.i.prol.preheader
                                        #   in Loop: Header=BB31_42 Depth=1
	negq	%rcx
	.p2align	4, 0x90
.LBB31_51:                              # %.lr.ph73.i.prol
                                        #   Parent Loop BB31_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	48(%r13), %rdx
	movq	8(%rdx,%r14,8), %rsi
	movq	%rsi, (%rdx,%r14,8)
	incq	%r14
	incq	%rcx
	jne	.LBB31_51
.LBB31_52:                              # %.lr.ph73.i.prol.loopexit
                                        #   in Loop: Header=BB31_42 Depth=1
	cmpq	$3, %rax
	jb	.LBB31_54
	.p2align	4, 0x90
.LBB31_53:                              # %.lr.ph73.i
                                        #   Parent Loop BB31_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	48(%r13), %rax
	movq	8(%rax,%r14,8), %rcx
	movq	%rcx, (%rax,%r14,8)
	movq	48(%r13), %rax
	movq	16(%rax,%r14,8), %rcx
	movq	%rcx, 8(%rax,%r14,8)
	movq	48(%r13), %rax
	movq	24(%rax,%r14,8), %rcx
	movq	%rcx, 16(%rax,%r14,8)
	movq	48(%r13), %rax
	movq	32(%rax,%r14,8), %rcx
	movq	%rcx, 24(%rax,%r14,8)
	leaq	4(%r14), %r14
	cmpq	%r14, %rbp
	jne	.LBB31_53
.LBB31_54:                              # %._crit_edge.i57
                                        #   in Loop: Header=BB31_42 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	decl	(%rax)
	jmp	.LBB31_55
.LBB31_56:                              #   in Loop: Header=BB31_42 Depth=1
	movq	48(%r12), %rax
	movq	(%rax), %rax
	incl	32(%rax)
	movq	48(%r13), %rcx
	movq	%rax, (%rcx,%r14,8)
.LBB31_55:                              # %._crit_edge.i57
                                        #   in Loop: Header=BB31_42 Depth=1
	movb	7(%rsp), %bpl           # 1-byte Reload
	decl	32(%r12)
	jne	.LBB31_83
	jmp	.LBB31_82
.LBB31_57:                              # %.preheader63.i
                                        #   in Loop: Header=BB31_42 Depth=1
	cmpl	$2, %r10d
	jl	.LBB31_68
# BB#58:                                #   in Loop: Header=BB31_42 Depth=1
	leal	-1(%r10), %r15d
	.p2align	4, 0x90
.LBB31_59:                              # %.lr.ph71.i
                                        #   Parent Loop BB31_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	48(%r13), %rcx
	testq	%rcx, %rcx
	je	.LBB31_63
# BB#60:                                #   in Loop: Header=BB31_59 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpq	80(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB31_64
# BB#61:                                #   in Loop: Header=BB31_59 Depth=2
	testb	$7, %al
	je	.LBB31_66
# BB#62:                                #   in Loop: Header=BB31_59 Depth=2
	leal	1(%rax), %edx
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	%edx, (%rsi)
	movq	$0, (%rcx,%rax,8)
	decl	%r15d
	jne	.LBB31_59
	jmp	.LBB31_68
	.p2align	4, 0x90
.LBB31_63:                              #   in Loop: Header=BB31_59 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, 48(%r13)
	movl	40(%r13), %eax
	jmp	.LBB31_65
	.p2align	4, 0x90
.LBB31_64:                              #   in Loop: Header=BB31_59 Depth=2
	cmpl	$2, %eax
	ja	.LBB31_66
.LBB31_65:                              #   in Loop: Header=BB31_59 Depth=2
	leal	1(%rax), %ecx
	movl	%ecx, 40(%r13)
	movq	$0, 56(%r13,%rax,8)
	decl	%r15d
	jne	.LBB31_59
	jmp	.LBB31_68
	.p2align	4, 0x90
.LBB31_66:                              #   in Loop: Header=BB31_59 Depth=2
	xorl	%esi, %esi
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	vec_add_internal
	movq	72(%rsp), %r10          # 8-byte Reload
	decl	%r15d
	jne	.LBB31_59
.LBB31_68:                              # %.preheader62.i
                                        #   in Loop: Header=BB31_42 Depth=1
	leal	1(%rbx), %ecx
	cmpl	%ecx, %ebp
	movq	56(%rsp), %r15          # 8-byte Reload
	jle	.LBB31_75
# BB#69:                                # %.lr.ph69.i
                                        #   in Loop: Header=BB31_42 Depth=1
	movslq	%ecx, %r9
	movl	%ebp, %edx
	subl	%ecx, %edx
	leaq	-1(%rbp), %r8
	subq	%r9, %r8
	andq	$3, %rdx
	je	.LBB31_72
# BB#70:                                # %.prol.preheader
                                        #   in Loop: Header=BB31_42 Depth=1
	leal	-2(%r10,%rbp), %esi
	negq	%rdx
	.p2align	4, 0x90
.LBB31_71:                              #   Parent Loop BB31_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	48(%r13), %rdi
	movq	-8(%rdi,%rbp,8), %rcx
	decq	%rbp
	movslq	%esi, %rsi
	movq	%rcx, (%rdi,%rsi,8)
	decl	%esi
	incq	%rdx
	jne	.LBB31_71
.LBB31_72:                              # %.prol.loopexit
                                        #   in Loop: Header=BB31_42 Depth=1
	cmpq	$3, %r8
	jb	.LBB31_75
# BB#73:                                # %.lr.ph69.i.new
                                        #   in Loop: Header=BB31_42 Depth=1
	leal	-2(%r10), %ecx
	.p2align	4, 0x90
.LBB31_74:                              #   Parent Loop BB31_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	48(%r13), %rdx
	movq	-8(%rdx,%rbp,8), %rsi
	leaq	(%rcx,%rbp), %rdi
	movslq	%edi, %rdi
	movq	%rsi, (%rdx,%rdi,8)
	movq	48(%r13), %rdx
	movq	-16(%rdx,%rbp,8), %rsi
	leal	-1(%rdi), %eax
	cltq
	movq	%rsi, (%rdx,%rax,8)
	movq	48(%r13), %rax
	movq	-24(%rax,%rbp,8), %rdx
	leal	-2(%rdi), %esi
	movslq	%esi, %rsi
	movq	%rdx, (%rax,%rsi,8)
	movq	48(%r13), %rax
	movq	-32(%rax,%rbp,8), %rdx
	addq	$-4, %rbp
	addl	$-3, %edi
	movslq	%edi, %rsi
	movq	%rdx, (%rax,%rsi,8)
	cmpq	%r9, %rbp
	jg	.LBB31_74
.LBB31_75:                              # %.preheader.i58
                                        #   in Loop: Header=BB31_42 Depth=1
	testl	%r10d, %r10d
	movb	7(%rsp), %bpl           # 1-byte Reload
	jle	.LBB31_81
# BB#76:                                # %.lr.ph.i59
                                        #   in Loop: Header=BB31_42 Depth=1
	testb	$1, %r10b
	jne	.LBB31_78
# BB#77:                                #   in Loop: Header=BB31_42 Depth=1
	xorl	%eax, %eax
	cmpl	$1, %r10d
	jne	.LBB31_79
	jmp	.LBB31_81
.LBB31_78:                              #   in Loop: Header=BB31_42 Depth=1
	movq	48(%r12), %rax
	movq	(%rax), %rax
	incl	32(%rax)
	movq	48(%r13), %rcx
	movq	%rax, (%rcx,%r14,8)
	movl	$1, %eax
	cmpl	$1, %r10d
	je	.LBB31_81
.LBB31_79:                              # %.lr.ph.i59.new
                                        #   in Loop: Header=BB31_42 Depth=1
	shlq	$3, %r14
	.p2align	4, 0x90
.LBB31_80:                              #   Parent Loop BB31_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	48(%r12), %rcx
	movq	(%rcx,%rax,8), %rcx
	incl	32(%rcx)
	movq	48(%r13), %rdx
	addq	%r14, %rdx
	movq	%rcx, (%rdx,%rax,8)
	movq	48(%r12), %rcx
	movq	8(%rcx,%rax,8), %rcx
	incl	32(%rcx)
	movq	48(%r13), %rdx
	addq	%r14, %rdx
	movq	%rcx, 8(%rdx,%rax,8)
	addq	$2, %rax
	cmpq	%rax, %r10
	jne	.LBB31_80
	.p2align	4, 0x90
.LBB31_81:                              # %.loopexit.i
                                        #   in Loop: Header=BB31_42 Depth=1
	decl	32(%r12)
	jne	.LBB31_83
.LBB31_82:                              #   in Loop: Header=BB31_42 Depth=1
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	free_PNode
.LBB31_83:                              # %fixup_internal_symbol.exit
                                        #   in Loop: Header=BB31_42 Depth=1
	decl	%ebx
.LBB31_84:                              #   in Loop: Header=BB31_42 Depth=1
	incl	%ebx
	movl	40(%r13), %edx
	cmpl	%edx, %ebx
	jb	.LBB31_42
	jmp	.LBB31_86
.LBB31_85:
	xorl	%edx, %edx
.LBB31_86:                              # %._crit_edge
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.LBB31_89
# BB#87:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB31_89
# BB#88:
	movq	48(%r13), %rsi
	movl	$152, %ecx
	movq	%r13, %rdi
	movq	%r15, %r8
	callq	*%rax
.LBB31_89:
	cmpb	$0, 84(%r13)
	je	.LBB31_1
# BB#90:
	cmpl	$2, 68(%rsp)            # 4-byte Folded Reload
	je	.LBB31_1
# BB#91:
	movl	88(%r15), %eax
	testl	%eax, %eax
	jne	.LBB31_1
# BB#92:
	movl	40(%r13), %eax
	testl	%eax, %eax
	movq	48(%r13), %rdi
	je	.LBB31_97
# BB#93:                                # %.lr.ph.i54.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB31_94:                              # %.lr.ph.i54
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rsi
	decl	32(%rsi)
	jne	.LBB31_96
# BB#95:                                #   in Loop: Header=BB31_94 Depth=1
	movq	%r15, %rdi
	callq	free_PNode
	movl	40(%r13), %eax
	movq	48(%r13), %rdi
.LBB31_96:                              #   in Loop: Header=BB31_94 Depth=1
	incq	%rbx
	cmpl	%eax, %ebx
	jb	.LBB31_94
.LBB31_97:                              # %._crit_edge.i56
	testq	%rdi, %rdi
	je	.LBB31_100
# BB#98:                                # %._crit_edge.i56
	leaq	56(%r13), %rax
	cmpq	%rax, %rdi
	je	.LBB31_100
# BB#99:
	callq	free
.LBB31_100:
	movl	$0, 40(%r13)
	movq	$0, 48(%r13)
	movq	104(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB31_1
# BB#101:
	movq	$0, 104(%r13)
	movq	%r15, %rdi
	callq	free_PNode
	jmp	.LBB31_1
.Lfunc_end31:
	.size	commit_tree, .Lfunc_end31-commit_tree
	.cfi_endproc

	.p2align	4, 0x90
	.type	free_SNode,@function
free_SNode:                             # @free_SNode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi183:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi184:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi185:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi186:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi187:
	.cfi_def_cfa_offset 48
.Lcfi188:
	.cfi_offset %rbx, -48
.Lcfi189:
	.cfi_offset %r12, -40
.Lcfi190:
	.cfi_offset %r13, -32
.Lcfi191:
	.cfi_offset %r14, -24
.Lcfi192:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	movl	72(%r12), %eax
	testl	%eax, %eax
	movq	80(%r12), %rdi
	je	.LBB32_16
# BB#1:                                 # %.lr.ph29
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB32_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_7 Depth 2
	movq	(%rdi,%r15,8), %r13
	testq	%r13, %r13
	je	.LBB32_15
# BB#3:                                 #   in Loop: Header=BB32_2 Depth=1
	movq	(%r13), %rsi
	decl	32(%rsi)
	jne	.LBB32_5
# BB#4:                                 #   in Loop: Header=BB32_2 Depth=1
	movq	%r14, %rdi
	callq	free_PNode
.LBB32_5:                               # %.preheader
                                        #   in Loop: Header=BB32_2 Depth=1
	movl	8(%r13), %eax
	testl	%eax, %eax
	movq	16(%r13), %rdi
	je	.LBB32_11
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB32_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB32_7:                               # %.lr.ph
                                        #   Parent Loop BB32_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rbx,8), %rcx
	cmpq	%r12, %rcx
	je	.LBB32_10
# BB#8:                                 #   in Loop: Header=BB32_7 Depth=2
	decl	112(%rcx)
	jne	.LBB32_10
# BB#9:                                 #   in Loop: Header=BB32_7 Depth=2
	movq	16(%r13), %rax
	movq	(%rax,%rbx,8), %rsi
	movq	%r14, %rdi
	callq	free_SNode
	movl	8(%r13), %eax
.LBB32_10:                              #   in Loop: Header=BB32_7 Depth=2
	incq	%rbx
	movq	16(%r13), %rdi
	cmpl	%eax, %ebx
	jb	.LBB32_7
.LBB32_11:                              # %._crit_edge
                                        #   in Loop: Header=BB32_2 Depth=1
	testq	%rdi, %rdi
	je	.LBB32_14
# BB#12:                                # %._crit_edge
                                        #   in Loop: Header=BB32_2 Depth=1
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.LBB32_14
# BB#13:                                #   in Loop: Header=BB32_2 Depth=1
	callq	free
.LBB32_14:                              # %free_ZNode.exit
                                        #   in Loop: Header=BB32_2 Depth=1
	movl	$0, 8(%r13)
	movq	$0, 16(%r13)
	movq	336(%r14), %rax
	movq	%rax, (%r13)
	movq	%r13, 336(%r14)
	movl	72(%r12), %eax
	movq	80(%r12), %rdi
.LBB32_15:                              #   in Loop: Header=BB32_2 Depth=1
	incq	%r15
	cmpl	%eax, %r15d
	jb	.LBB32_2
.LBB32_16:                              # %._crit_edge30
	testq	%rdi, %rdi
	je	.LBB32_19
# BB#17:                                # %._crit_edge30
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.LBB32_19
# BB#18:
	callq	free
.LBB32_19:
	movl	$0, 72(%r12)
	movq	$0, 80(%r12)
	movq	328(%r14), %rax
	movq	%rax, 128(%r12)
	movq	%r12, 328(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end32:
	.size	free_SNode, .Lfunc_end32-free_SNode
	.cfi_endproc

	.p2align	4, 0x90
	.type	free_old_nodes,@function
free_old_nodes:                         # @free_old_nodes
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi193:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi194:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi195:
	.cfi_def_cfa_offset 32
.Lcfi196:
	.cfi_offset %rbx, -32
.Lcfi197:
	.cfi_offset %r14, -24
.Lcfi198:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	208(%r14), %r15
	movq	240(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB33_5
# BB#1:                                 # %.lr.ph93
	movl	228(%r14), %esi
	.p2align	4, 0x90
.LBB33_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_3 Depth 2
	movq	144(%r14), %rdx
	movl	(%rcx), %edi
	movl	16(%rcx), %eax
	subl	8(%rdx), %edi
	shrl	$3, %edi
	imull	$-286330880, %edi, %edx # imm = 0xEEEEF000
	addl	8(%rcx), %eax
	addl	%edx, %eax
	xorl	%edx, %edx
	divl	%esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	shlq	$3, %rdx
	addq	216(%r14), %rdx
	movq	128(%rcx), %rax
	.p2align	4, 0x90
.LBB33_3:                               #   Parent Loop BB33_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rdi
	movq	(%rdi), %rdx
	cmpq	%rcx, %rdx
	leaq	120(%rdx), %rdx
	jne	.LBB33_3
# BB#4:                                 #   in Loop: Header=BB33_2 Depth=1
	movq	(%rdx), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.LBB33_2
.LBB33_5:                               # %._crit_edge94
	movq	248(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB33_9
	.p2align	4, 0x90
.LBB33_6:                               # %.lr.ph89
                                        # =>This Inner Loop Header: Depth=1
	movq	128(%rsi), %rbx
	movl	112(%rsi), %eax
	decl	%eax
	movl	%eax, 112(%rsi)
	jne	.LBB33_8
# BB#7:                                 #   in Loop: Header=BB33_6 Depth=1
	movq	%r14, %rdi
	callq	free_SNode
.LBB33_8:                               # %.backedge82
                                        #   in Loop: Header=BB33_6 Depth=1
	testq	%rbx, %rbx
	movq	%rbx, %rsi
	jne	.LBB33_6
.LBB33_9:                               # %._crit_edge90
	movq	240(%r14), %rax
	movq	%rax, 248(%r14)
	movq	$0, 240(%r14)
	testq	%r15, %r15
	je	.LBB33_22
	.p2align	4, 0x90
.LBB33_10:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_12 Depth 2
                                        #     Child Loop BB33_18 Depth 2
	movl	40(%r15), %eax
	testl	%eax, %eax
	je	.LBB33_17
# BB#11:                                # %.lr.ph
                                        #   in Loop: Header=BB33_10 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB33_12:                              #   Parent Loop BB33_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	48(%r15), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movq	112(%rsi), %rdx
	cmpq	%rdx, %rsi
	je	.LBB33_16
# BB#13:                                #   in Loop: Header=BB33_12 Depth=2
	incl	32(%rdx)
	decl	32(%rsi)
	jne	.LBB33_15
# BB#14:                                #   in Loop: Header=BB33_12 Depth=2
	movq	%r14, %rdi
	callq	free_PNode
	movq	48(%r15), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movl	40(%r15), %eax
.LBB33_15:                              #   in Loop: Header=BB33_12 Depth=2
	movq	112(%rsi), %rdx
	movq	%rdx, (%rcx,%rbx,8)
.LBB33_16:                              #   in Loop: Header=BB33_12 Depth=2
	incq	%rbx
	cmpl	%eax, %ebx
	jb	.LBB33_12
.LBB33_17:                              # %._crit_edge
                                        #   in Loop: Header=BB33_10 Depth=1
	movl	160(%r15), %eax
	shll	$8, %eax
	movl	200(%r15), %ecx
	shll	$16, %ecx
	addl	152(%r15), %eax
	addl	%ecx, %eax
	addl	136(%r15), %eax
	addl	144(%r15), %eax
	xorl	%edx, %edx
	divl	196(%r14)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	shlq	$3, %rdx
	addq	184(%r14), %rdx
	movq	88(%r15), %rbx
	.p2align	4, 0x90
.LBB33_18:                              #   Parent Loop BB33_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rax
	movq	(%rax), %rcx
	cmpq	%r15, %rcx
	leaq	96(%rcx), %rdx
	jne	.LBB33_18
# BB#19:                                #   in Loop: Header=BB33_10 Depth=1
	movq	(%rdx), %rcx
	movq	%rcx, (%rax)
	decl	32(%r15)
	jne	.LBB33_21
# BB#20:                                #   in Loop: Header=BB33_10 Depth=1
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	free_PNode
.LBB33_21:                              # %.backedge
                                        #   in Loop: Header=BB33_10 Depth=1
	testq	%rbx, %rbx
	movq	%rbx, %r15
	jne	.LBB33_10
.LBB33_22:                              # %._crit_edge86
	movl	$0, 200(%r14)
	movq	$0, 208(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end33:
	.size	free_old_nodes, .Lfunc_end33-free_old_nodes
	.cfi_endproc

	.p2align	4, 0x90
	.type	add_SNode,@function
add_SNode:                              # @add_SNode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi199:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi200:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi201:
	.cfi_def_cfa_offset 32
.Lcfi202:
	.cfi_offset %rbx, -32
.Lcfi203:
	.cfi_offset %r14, -24
.Lcfi204:
	.cfi_offset %r15, -16
	movq	%rdx, %r9
	movq	%rdi, %r14
	movq	216(%r14), %r10
	testq	%r10, %r10
	je	.LBB34_7
# BB#1:
	movq	144(%r14), %rax
	movq	8(%rax), %rdi
	movq	%rsi, %rbx
	subq	%rdi, %rbx
	sarq	$3, %rbx
	movabsq	$-1229782938247303441, %r11 # imm = 0xEEEEEEEEEEEEEEEF
	imulq	%r11, %rbx
	movl	%ebx, %edx
	shll	$12, %edx
	leal	(%r8,%rcx), %eax
	addl	%edx, %eax
	xorl	%edx, %edx
	divl	228(%r14)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	(%r10,%rdx,8), %r15
	testq	%r15, %r15
	je	.LBB34_7
# BB#2:                                 # %.lr.ph.i
	movl	%ebx, %eax
	.p2align	4, 0x90
.LBB34_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	imulq	%r11, %rdx
	cmpq	%rax, %rdx
	jne	.LBB34_6
# BB#4:                                 #   in Loop: Header=BB34_3 Depth=1
	cmpq	%rcx, 8(%r15)
	jne	.LBB34_6
# BB#5:                                 #   in Loop: Header=BB34_3 Depth=1
	cmpq	%r8, 16(%r15)
	je	.LBB34_21
	.p2align	4, 0x90
.LBB34_6:                               #   in Loop: Header=BB34_3 Depth=1
	movq	120(%r15), %r15
	testq	%r15, %r15
	jne	.LBB34_3
.LBB34_7:                               # %.loopexit
	movq	%r14, %rdi
	movq	%r9, %rdx
	callq	new_SNode
	movq	%rax, %r15
	movq	(%r15), %rax
	cmpq	$0, 64(%rax)
	je	.LBB34_16
# BB#8:
	leaq	264(%r14), %rbx
	movq	304(%r14), %rax
	testq	%rax, %rax
	je	.LBB34_9
# BB#10:
	movq	8(%rax), %rcx
	movq	%rcx, 304(%r14)
	jmp	.LBB34_11
.LBB34_9:
	movl	$16, %edi
	callq	malloc
.LBB34_11:
	movq	%r15, (%rax)
	incl	112(%r15)
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB34_15
# BB#12:                                # %.lr.ph.i29
	movq	24(%r15), %rdx
	.p2align	4, 0x90
.LBB34_14:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rsi
	cmpq	24(%rsi), %rdx
	jbe	.LBB34_15
# BB#13:                                #   in Loop: Header=BB34_14 Depth=1
	movq	%rcx, %rbx
	addq	$8, %rbx
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	jne	.LBB34_14
.LBB34_15:                              # %add_Shift.exit
	movq	(%rbx), %rcx
	movq	%rcx, 8(%rax)
	movq	%rax, (%rbx)
	movq	(%r15), %rax
.LBB34_16:                              # %.preheader
	cmpl	$0, 16(%rax)
	je	.LBB34_21
# BB#17:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB34_18:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rax), %rcx
	movq	(%rcx,%rbx,8), %rcx
	cmpw	$0, (%rcx)
	jne	.LBB34_20
# BB#19:                                #   in Loop: Header=BB34_18 Depth=1
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	add_Reduction
	movq	(%r15), %rax
.LBB34_20:                              #   in Loop: Header=BB34_18 Depth=1
	incq	%rbx
	cmpl	16(%rax), %ebx
	jb	.LBB34_18
.LBB34_21:                              # %find_SNode.exit
	movq	%r15, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end34:
	.size	add_SNode, .Lfunc_end34-add_SNode
	.cfi_endproc

	.p2align	4, 0x90
	.type	add_PNode,@function
add_PNode:                              # @add_PNode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi205:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi206:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi207:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi208:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi209:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi210:
	.cfi_def_cfa_offset 56
	subq	$328, %rsp              # imm = 0x148
.Lcfi211:
	.cfi_def_cfa_offset 384
.Lcfi212:
	.cfi_offset %rbx, -56
.Lcfi213:
	.cfi_offset %r12, -48
.Lcfi214:
	.cfi_offset %r13, -40
.Lcfi215:
	.cfi_offset %r14, -32
.Lcfi216:
	.cfi_offset %r15, -24
.Lcfi217:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %r12d
	movq	392(%rsp), %r14
	movq	384(%rsp), %r11
	movq	184(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB35_11
# BB#1:
	movq	(%r15), %rbp
	movq	136(%r8), %rsi
	movq	144(%r8), %r10
	movl	%ebp, %edx
	shll	$8, %edx
	movl	%ecx, %eax
	shll	$16, %eax
	addl	%r12d, %eax
	addl	%edx, %eax
	addl	%esi, %eax
	addl	%r10d, %eax
	xorl	%edx, %edx
	divl	196(%rdi)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	(%rbx,%rdx,8), %r13
	testq	%r13, %r13
	jne	.LBB35_4
	jmp	.LBB35_11
	.p2align	4, 0x90
.LBB35_2:                               #   in Loop: Header=BB35_4 Depth=1
	movq	96(%r13), %r13
	testq	%r13, %r13
	je	.LBB35_11
.LBB35_4:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r12d, 152(%r13)
	jne	.LBB35_2
# BB#5:                                 #   in Loop: Header=BB35_4 Depth=1
	cmpq	%rbp, 160(%r13)
	jne	.LBB35_2
# BB#6:                                 #   in Loop: Header=BB35_4 Depth=1
	cmpq	%rcx, 200(%r13)
	jne	.LBB35_2
# BB#7:                                 #   in Loop: Header=BB35_4 Depth=1
	cmpq	%rsi, 136(%r13)
	jne	.LBB35_2
# BB#8:                                 #   in Loop: Header=BB35_4 Depth=1
	cmpq	%r10, 144(%r13)
	jne	.LBB35_2
# BB#9:                                 # %find_PNode.exit
	movb	$1, %al
	testq	%r14, %r14
	je	.LBB35_169
# BB#10:                                # %PNode_equal.exit
	cmpq	%r14, 24(%r13)
	jne	.LBB35_12
	jmp	.LBB35_166
.LBB35_11:
	xorl	%eax, %eax
	xorl	%r13d, %r13d
.LBB35_12:                              # %find_PNode.exit.thread
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%r15, %rbp
	movq	%r8, %rbx
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	$232, %eax
	addl	84(%rdi), %eax
	movq	320(%rdi), %r15
	testq	%r15, %r15
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	je	.LBB35_14
# BB#13:
	movq	88(%r15), %rcx
	movq	%rcx, 320(%rdi)
	movslq	%eax, %r14
	jmp	.LBB35_15
.LBB35_14:
	movslq	%eax, %r14
	movq	%r14, %rdi
	callq	malloc
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, %r15
.LBB35_15:
	incl	156(%rdi)
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	memset
	movl	%r12d, 152(%r15)
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	%xmm1, 176(%r15)
	movups	%xmm0, 160(%r15)
	movq	16(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	movq	384(%rsp), %r9
	je	.LBB35_23
# BB#16:
	testq	%r9, %r9
	je	.LBB35_23
# BB#17:
	movq	%rbx, %rdi
	movq	192(%rdi), %rax
	movq	%rax, 192(%r15)
	movq	8(%r9), %rax
	movl	(%r9), %ecx
	decl	%ecx
	movq	(%rax,%rcx,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	80(%rax), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	392(%rsp), %rbx
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB35_22
	.p2align	4, 0x90
.LBB35_18:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdx
	movq	192(%rdx), %rcx
	cmpq	%rcx, 160(%rdx)
	jne	.LBB35_24
# BB#19:                                #   in Loop: Header=BB35_18 Depth=1
	cmpl	$0, 8(%rax)
	je	.LBB35_22
# BB#20:                                #   in Loop: Header=BB35_18 Depth=1
	movq	16(%rax), %rax
	movq	(%rax), %rax
	cmpl	$0, 72(%rax)
	je	.LBB35_22
# BB#21:                                # %.backedge.i.i
                                        #   in Loop: Header=BB35_18 Depth=1
	movq	80(%rax), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB35_18
.LBB35_22:                              # %.critedge10.i.i
	movq	128(%r8), %rcx
	jmp	.LBB35_24
.LBB35_23:
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, 192(%r15)
	movq	%rbp, %rcx
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%rbx, %rdi
	movq	392(%rsp), %rbx
.LBB35_24:                              # %find_ws_before.exit.i
	movq	%rcx, 120(%r15)
	movq	%rbp, 200(%r15)
	movq	%rbx, 24(%r15)
	movq	%rsi, 16(%r15)
	movups	208(%rdi), %xmm0
	movlps	%xmm0, 136(%r15)
	movq	224(%rdi), %rax
	movq	%rax, 224(%r15)
	movq	%rax, 144(%r15)
	movups	%xmm0, 208(%r15)
	movq	%r15, 112(%r15)
	movq	%rbp, 128(%r15)
	testq	%rbx, %rbx
	je	.LBB35_28
# BB#25:
	movzbl	3(%rbx), %eax
	movl	%eax, 8(%r15)
	movl	4(%rbx), %eax
	movl	%eax, 12(%r15)
	movq	16(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB35_74
# BB#26:
	movq	48(%rdi), %rsi
	movl	40(%rdi), %edx
	movl	$152, %ecx
	movq	%r8, %rbx
	callq	*%rbp
	testl	%eax, %eax
	je	.LBB35_44
# BB#27:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	free_PNode
	xorl	%r15d, %r15d
.LBB35_44:
	movq	%rbx, %r8
	cmpb	$0, 144(%rsp)           # 1-byte Folded Reload
	jne	.LBB35_75
	jmp	.LBB35_106
.LBB35_28:
	testq	%rsi, %rsi
	je	.LBB35_74
# BB#29:
	testq	%r9, %r9
	je	.LBB35_41
# BB#30:
	movl	(%r9), %eax
	decl	%eax
	js	.LBB35_41
# BB#31:                                # %.lr.ph136.i
	leaq	40(%r15), %r14
	leaq	56(%r15), %r12
	movslq	%eax, %rbx
	.p2align	4, 0x90
.LBB35_32:                              # =>This Inner Loop Header: Depth=1
	movq	8(%r9), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rax
	movq	112(%rax), %rbp
	movq	48(%r15), %rcx
	testq	%rcx, %rcx
	je	.LBB35_36
# BB#33:                                #   in Loop: Header=BB35_32 Depth=1
	movl	(%r14), %eax
	cmpq	%r12, %rcx
	je	.LBB35_37
# BB#34:                                #   in Loop: Header=BB35_32 Depth=1
	testb	$7, %al
	je	.LBB35_39
# BB#35:                                #   in Loop: Header=BB35_32 Depth=1
	leal	1(%rax), %edx
	movl	%edx, (%r14)
	movq	%rbp, (%rcx,%rax,8)
	jmp	.LBB35_40
	.p2align	4, 0x90
.LBB35_36:                              #   in Loop: Header=BB35_32 Depth=1
	movq	%r12, 48(%r15)
	movl	40(%r15), %eax
	jmp	.LBB35_38
	.p2align	4, 0x90
.LBB35_37:                              #   in Loop: Header=BB35_32 Depth=1
	cmpl	$2, %eax
	ja	.LBB35_39
.LBB35_38:                              #   in Loop: Header=BB35_32 Depth=1
	leal	1(%rax), %ecx
	movl	%ecx, 40(%r15)
	movq	%rbp, 56(%r15,%rax,8)
	jmp	.LBB35_40
	.p2align	4, 0x90
.LBB35_39:                              #   in Loop: Header=BB35_32 Depth=1
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	vec_add_internal
	movq	384(%rsp), %r9
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB35_40:                              #   in Loop: Header=BB35_32 Depth=1
	incl	32(%rbp)
	decq	%rbx
	testl	%ebx, %ebx
	jns	.LBB35_32
.LBB35_41:                              # %.loopexit.i
	movl	40(%r15), %edx
	testq	%rdx, %rdx
	je	.LBB35_45
# BB#42:                                # %.lr.ph.i129.i
	movq	48(%r15), %rsi
	testb	$1, %dl
	jne	.LBB35_46
# BB#43:
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.LBB35_49
.LBB35_45:
	xorl	%eax, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB35_56
.LBB35_46:
	movq	(%rsi), %rax
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB35_48
# BB#47:
	movl	%ecx, (%r15)
	movl	12(%rax), %ecx
	movl	%ecx, 4(%r15)
.LBB35_48:
	movl	80(%rax), %eax
	incl	%eax
	movl	$1, %edi
.LBB35_49:                              # %.prol.loopexit
	movq	16(%rsp), %rbx          # 8-byte Reload
	cmpl	$1, %edx
	je	.LBB35_56
# BB#50:                                # %.lr.ph.i129.i.new
	movq	%rdx, %rcx
	subq	%rdi, %rcx
	leaq	8(%rsi,%rdi,8), %rsi
	.p2align	4, 0x90
.LBB35_51:                              # =>This Inner Loop Header: Depth=1
	movq	-8(%rsi), %rdi
	movl	8(%rdi), %ebp
	testl	%ebp, %ebp
	je	.LBB35_53
# BB#52:                                #   in Loop: Header=BB35_51 Depth=1
	movl	%ebp, (%r15)
	movl	12(%rdi), %ebp
	movl	%ebp, 4(%r15)
.LBB35_53:                              #   in Loop: Header=BB35_51 Depth=1
	movl	80(%rdi), %ebp
	leal	1(%rbp), %edi
	cmpl	%eax, %ebp
	cmovbl	%eax, %edi
	movq	(%rsi), %rax
	movl	8(%rax), %ebp
	testl	%ebp, %ebp
	je	.LBB35_55
# BB#54:                                #   in Loop: Header=BB35_51 Depth=1
	movl	%ebp, (%r15)
	movl	12(%rax), %ebp
	movl	%ebp, 4(%r15)
.LBB35_55:                              #   in Loop: Header=BB35_51 Depth=1
	movl	80(%rax), %ebp
	leal	1(%rbp), %eax
	cmpl	%edi, %ebp
	cmovbl	%edi, %eax
	addq	$16, %rsi
	addq	$-2, %rcx
	jne	.LBB35_51
.LBB35_56:                              # %._crit_edge.i.i
	movzwl	24(%rbx), %ecx
	movl	%ecx, 8(%r15)
	movl	28(%rbx), %ecx
	movl	%ecx, 12(%r15)
	movl	%eax, 80(%r15)
	movzwl	26(%rbx), %eax
	testl	%eax, %eax
	je	.LBB35_58
# BB#57:
	movl	%eax, (%r15)
	movl	32(%rbx), %eax
	movl	%eax, 4(%r15)
.LBB35_58:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB35_62
# BB#59:                                # %reduce_actions.exit.i
	movq	48(%r15), %rsi
	movl	$152, %ecx
	movq	%r15, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*%rax
	movq	384(%rsp), %r9
	movq	8(%rsp), %r8            # 8-byte Reload
	testl	%eax, %eax
	je	.LBB35_62
# BB#60:
	movq	%r8, %rdi
	movq	%r15, %rsi
	callq	free_PNode
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB35_61:                              # %make_PNode.exit
	xorl	%r15d, %r15d
	cmpb	$0, 144(%rsp)           # 1-byte Folded Reload
	je	.LBB35_106
	jmp	.LBB35_75
.LBB35_62:                              # %reduce_actions.exit.thread.i
	testq	%r9, %r9
	je	.LBB35_74
# BB#63:
	movslq	(%r9), %r14
	cmpq	$2, %r14
	jb	.LBB35_74
# BB#64:
	testl	%r14d, %r14d
	jle	.LBB35_74
# BB#65:                                # %.lr.ph.i51
	movq	48(%r15), %r9
	leaq	-1(%r14), %r10
	movl	$1, %r11d
	subl	%r14d, %r11d
	xorl	%r8d, %r8d
	movl	%r10d, %ebp
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB35_66:                              # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rdi,8), %rsi
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	je	.LBB35_72
# BB#67:                                #   in Loop: Header=BB35_66 Depth=1
	movl	(%r15), %edx
	movl	4(%r15), %r12d
	xorl	%ebx, %ebx
	testl	%ebp, %ebp
	sete	%bl
	xorl	%eax, %eax
	cmpl	$9, %edx
	setne	%al
	orq	$2, %rax
	testb	$20, %dl
	cmoveq	%rax, %rbx
	xorl	%eax, %eax
	cmpl	$9, %ecx
	setne	%al
	incq	%rax
	testb	$20, %cl
	cmovneq	%r8, %rax
	cmpl	%r12d, 4(%rsi)
	movl	$0, %esi
	jg	.LBB35_71
# BB#68:                                #   in Loop: Header=BB35_66 Depth=1
	jge	.LBB35_70
# BB#69:                                #   in Loop: Header=BB35_66 Depth=1
	movl	$1, %esi
	jmp	.LBB35_71
.LBB35_70:                              #   in Loop: Header=BB35_66 Depth=1
	andl	$2, %ecx
	shrl	%edx
	andl	$1, %edx
	leal	2(%rdx,%rcx), %esi
.LBB35_71:                              # %check_child.exit.i
                                        #   in Loop: Header=BB35_66 Depth=1
	leaq	(%rbx,%rbx,8), %rcx
	leaq	(%rax,%rax,2), %rax
	shlq	$3, %rax
	leaq	(%rax,%rcx,8), %rax
	cmpl	$0, child_table(%rax,%rsi,4)
	je	.LBB35_137
.LBB35_72:                              #   in Loop: Header=BB35_66 Depth=1
	addq	%r10, %rdi
	addl	%r11d, %ebp
	cmpq	%r14, %rdi
	jl	.LBB35_66
# BB#73:
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB35_74:                              # %make_PNode.exit
	cmpb	$0, 144(%rsp)           # 1-byte Folded Reload
	je	.LBB35_106
.LBB35_75:
	testq	%r15, %r15
	je	.LBB35_166
# BB#76:
	incl	172(%r8)
	movl	(%r15), %eax
	testl	%eax, %eax
	je	.LBB35_151
# BB#77:
	movl	(%r13), %ecx
	testl	%ecx, %ecx
	je	.LBB35_151
# BB#78:
	orl	%eax, %ecx
	testb	$4, %cl
	jne	.LBB35_81
# BB#79:
	movl	4(%r13), %eax
	cmpl	%eax, 4(%r15)
	jg	.LBB35_164
# BB#80:
	jl	.LBB35_168
.LBB35_81:
	leaq	176(%rsp), %rax
	movq	%rax, 168(%rsp)
	movq	%rax, 152(%rsp)
	leaq	240(%rsp), %rax
	movq	%rax, 160(%rsp)
	leaq	264(%rsp), %rax
	movq	%rax, 256(%rsp)
	movq	%rax, 240(%rsp)
	leaq	328(%rsp), %rax
	movq	%rax, 248(%rsp)
	leaq	56(%rsp), %rax
	movq	%rax, 48(%rsp)
	movq	%rax, 32(%rsp)
	leaq	88(%rsp), %rax
	movq	%rax, 40(%rsp)
	leaq	112(%rsp), %rax
	movq	%rax, 104(%rsp)
	movq	%rax, 88(%rsp)
	leaq	144(%rsp), %rax
	movq	%rax, 96(%rsp)
	leaq	152(%rsp), %rsi
	leaq	32(%rsp), %rdx
	movq	%r15, %rdi
	callq	get_exp_one
	leaq	240(%rsp), %r12
	leaq	88(%rsp), %rdx
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	get_exp_one
	movq	168(%rsp), %rax
	cmpq	152(%rsp), %rax
	je	.LBB35_110
# BB#82:                                # %.lr.ph43.i.i
	leaq	240(%rsp), %r12
	.p2align	4, 0x90
.LBB35_83:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_102 Depth 2
                                        #     Child Loop BB35_94 Depth 2
	movq	256(%rsp), %rcx
	cmpq	240(%rsp), %rcx
	je	.LBB35_109
# BB#84:                                #   in Loop: Header=BB35_83 Depth=1
	movq	-8(%rax), %rdx
	addq	$-8, %rax
	movq	-8(%rcx), %rsi
	addq	$-8, %rcx
	movl	80(%rsi), %edi
	cmpl	%edi, 80(%rdx)
	ja	.LBB35_89
# BB#85:                                #   in Loop: Header=BB35_83 Depth=1
	jae	.LBB35_88
.LBB35_86:                              #   in Loop: Header=BB35_83 Depth=1
	movq	%rcx, 256(%rsp)
	movq	(%rcx), %rbx
	movq	104(%rsp), %rax
	cmpq	96(%rsp), %rax
	movl	4(%rbx), %ecx
	je	.LBB35_99
# BB#87:                                #   in Loop: Header=BB35_83 Depth=1
	leaq	4(%rax), %rdx
	movq	%rdx, 104(%rsp)
	movl	%ecx, (%rax)
	jmp	.LBB35_100
.LBB35_88:                              #   in Loop: Header=BB35_83 Depth=1
	cmpq	%rsi, %rdx
	jbe	.LBB35_97
	.p2align	4, 0x90
.LBB35_89:                              # %.thread.i.i
                                        #   in Loop: Header=BB35_83 Depth=1
	movq	%rax, 168(%rsp)
	movq	(%rax), %rbx
	movq	48(%rsp), %rax
	cmpq	40(%rsp), %rax
	movl	4(%rbx), %ecx
	je	.LBB35_91
# BB#90:                                #   in Loop: Header=BB35_83 Depth=1
	leaq	4(%rax), %rdx
	movq	%rdx, 48(%rsp)
	movl	%ecx, (%rax)
	jmp	.LBB35_92
.LBB35_91:                              #   in Loop: Header=BB35_83 Depth=1
	movslq	%ecx, %rsi
	leaq	32(%rsp), %rdi
	callq	stack_push_internal
.LBB35_92:                              # %.preheader.i.i.i
                                        #   in Loop: Header=BB35_83 Depth=1
	movl	40(%rbx), %eax
	testl	%eax, %eax
	je	.LBB35_105
# BB#93:                                # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB35_83 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB35_94:                              #   Parent Loop BB35_83 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	48(%rbx), %rcx
	movq	(%rcx,%r14,8), %rdi
	cmpl	$0, (%rdi)
	je	.LBB35_96
# BB#95:                                #   in Loop: Header=BB35_94 Depth=2
	leaq	152(%rsp), %rsi
	leaq	32(%rsp), %rdx
	callq	get_exp_one
	movl	40(%rbx), %eax
.LBB35_96:                              #   in Loop: Header=BB35_94 Depth=2
	incq	%r14
	cmpl	%eax, %r14d
	jb	.LBB35_94
	jmp	.LBB35_105
.LBB35_97:                              #   in Loop: Header=BB35_83 Depth=1
	jb	.LBB35_86
# BB#98:                                #   in Loop: Header=BB35_83 Depth=1
	movq	%rax, 168(%rsp)
	movq	%rcx, 256(%rsp)
	jmp	.LBB35_105
.LBB35_99:                              #   in Loop: Header=BB35_83 Depth=1
	movslq	%ecx, %rsi
	leaq	88(%rsp), %rdi
	callq	stack_push_internal
.LBB35_100:                             # %.preheader.i35.i.i
                                        #   in Loop: Header=BB35_83 Depth=1
	movl	40(%rbx), %eax
	testl	%eax, %eax
	je	.LBB35_105
# BB#101:                               # %.lr.ph.i36.i.i
                                        #   in Loop: Header=BB35_83 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB35_102:                             #   Parent Loop BB35_83 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	48(%rbx), %rcx
	movq	(%rcx,%rbp,8), %rdi
	cmpl	$0, (%rdi)
	je	.LBB35_104
# BB#103:                               #   in Loop: Header=BB35_102 Depth=2
	movq	%r12, %rsi
	leaq	88(%rsp), %rdx
	callq	get_exp_one
	movl	40(%rbx), %eax
.LBB35_104:                             #   in Loop: Header=BB35_102 Depth=2
	incq	%rbp
	cmpl	%eax, %ebp
	jb	.LBB35_102
	.p2align	4, 0x90
.LBB35_105:                             # %get_exp_one_down.exit.backedge.i.i
                                        #   in Loop: Header=BB35_83 Depth=1
	movq	168(%rsp), %rax
	cmpq	152(%rsp), %rax
	jne	.LBB35_83
	jmp	.LBB35_110
.LBB35_106:
	testq	%r15, %r15
	je	.LBB35_108
# BB#107:
	movq	%r8, %rdi
	movq	%r15, %rsi
	movq	%r8, %rbx
	callq	insert_PNode_internal
	incl	32(%r15)
	movq	208(%rbx), %rax
	movq	%rax, 88(%r15)
	movq	%r15, 208(%rbx)
	jmp	.LBB35_165
.LBB35_108:
	xorl	%r13d, %r13d
	jmp	.LBB35_166
.LBB35_109:
	leaq	152(%rsp), %r12
.LBB35_110:                             # %.preheader.i.i
	movq	16(%r12), %rax
	cmpq	(%r12), %rax
	je	.LBB35_116
# BB#111:                               # %.lr.ph.i.i52
	leaq	152(%rsp), %rcx
	cmpq	%rcx, %r12
	je	.LBB35_114
# BB#112:
	leaq	88(%rsp), %r14
	.p2align	4, 0x90
.LBB35_113:                             # %.lr.ph.split.i.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	-8(%rax), %rcx
	movq	%rcx, 16(%r12)
	movq	-8(%rax), %rdi
	movq	%r14, %rsi
	callq	get_exp_all
	movq	16(%r12), %rax
	cmpq	(%r12), %rax
	jne	.LBB35_113
	jmp	.LBB35_116
.LBB35_114:                             # %.lr.ph.split.us.i.i.preheader
	leaq	32(%rsp), %r14
	.p2align	4, 0x90
.LBB35_115:                             # %.lr.ph.split.us.i.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	-8(%rax), %rcx
	movq	%rcx, 16(%r12)
	movq	-8(%rax), %rdi
	movq	%r14, %rsi
	callq	get_exp_all
	movq	16(%r12), %rax
	cmpq	(%r12), %rax
	jne	.LBB35_115
.LBB35_116:                             # %get_unshared_priorities.exit.i
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	movq	32(%rsp), %rax
	movq	48(%rsp), %r8
	subq	%rax, %r8
	movq	%r8, %r9
	shlq	$30, %r9
	leaq	(%r9,%rsi), %rdi
	sarq	$32, %rdi
	leaq	112(%rsp), %r12
	.p2align	4, 0x90
.LBB35_117:                             # %.preheader.i25.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_119 Depth 2
                                        #       Child Loop BB35_120 Depth 3
	xorl	%edx, %edx
	xorl	%r10d, %r10d
	jmp	.LBB35_119
	.p2align	4, 0x90
.LBB35_118:                             #   in Loop: Header=BB35_119 Depth=2
	movl	%ebp, (%rax,%rcx,4)
	movl	%ebx, 4(%rax,%rcx,4)
	movl	$1, %r10d
.LBB35_119:                             # %.outer.i.i
                                        #   Parent Loop BB35_117 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB35_120 Depth 3
	movslq	%edx, %rdx
	.p2align	4, 0x90
.LBB35_120:                             #   Parent Loop BB35_117 Depth=1
                                        #     Parent Loop BB35_119 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	cmpq	%rdi, %rcx
	jge	.LBB35_122
# BB#121:                               #   in Loop: Header=BB35_120 Depth=3
	movl	(%rax,%rcx,4), %ebx
	leaq	1(%rcx), %rdx
	movl	4(%rax,%rcx,4), %ebp
	cmpl	%ebp, %ebx
	jle	.LBB35_120
	jmp	.LBB35_118
	.p2align	4, 0x90
.LBB35_122:                             # %.loopexit.i.i
                                        #   in Loop: Header=BB35_117 Depth=1
	testl	%r10d, %r10d
	jne	.LBB35_117
# BB#123:                               # %intsort.exit.i
	movq	88(%rsp), %rdi
	movq	104(%rsp), %r10
	subq	%rdi, %r10
	movq	%r10, %r11
	shlq	$30, %r11
	addq	%r11, %rsi
	sarq	$32, %rsi
	.p2align	4, 0x90
.LBB35_124:                             # %.preheader.i28.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_126 Depth 2
                                        #       Child Loop BB35_127 Depth 3
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	jmp	.LBB35_126
	.p2align	4, 0x90
.LBB35_125:                             #   in Loop: Header=BB35_126 Depth=2
	movl	%edx, (%rdi,%rcx,4)
	movl	%ebp, 4(%rdi,%rcx,4)
	movl	$1, %r14d
.LBB35_126:                             # %.outer.i32.i
                                        #   Parent Loop BB35_124 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB35_127 Depth 3
	movslq	%ebx, %rbx
	.p2align	4, 0x90
.LBB35_127:                             #   Parent Loop BB35_124 Depth=1
                                        #     Parent Loop BB35_126 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbx, %rcx
	cmpq	%rsi, %rcx
	jge	.LBB35_129
# BB#128:                               #   in Loop: Header=BB35_127 Depth=3
	movl	(%rdi,%rcx,4), %ebp
	leaq	1(%rcx), %rbx
	movl	4(%rdi,%rcx,4), %edx
	cmpl	%edx, %ebp
	jle	.LBB35_127
	jmp	.LBB35_125
	.p2align	4, 0x90
.LBB35_129:                             # %.loopexit.i27.i
                                        #   in Loop: Header=BB35_124 Depth=1
	testl	%r14d, %r14d
	jne	.LBB35_124
# BB#130:                               # %intsort.exit35.i
	shrq	$2, %r8
	xorl	%r14d, %r14d
	testl	%r8d, %r8d
	jle	.LBB35_140
# BB#131:                               # %intsort.exit35.i
	shrq	$2, %r10
	testl	%r10d, %r10d
	jle	.LBB35_140
# BB#132:                               # %.lr.ph.preheader.i.i
	sarq	$32, %r11
	sarq	$32, %r9
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB35_133:                             # %.lr.ph.i37.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdi,%rcx,4), %edx
	cmpl	%edx, -4(%rax,%rcx,4)
	jg	.LBB35_138
# BB#134:                               #   in Loop: Header=BB35_133 Depth=1
	jl	.LBB35_139
# BB#135:                               #   in Loop: Header=BB35_133 Depth=1
	cmpq	%r9, %rcx
	jge	.LBB35_140
# BB#136:                               #   in Loop: Header=BB35_133 Depth=1
	cmpq	%r11, %rcx
	leaq	1(%rcx), %rcx
	jl	.LBB35_133
	jmp	.LBB35_140
.LBB35_137:
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	free_PNode
	movq	%rbx, %r8
	jmp	.LBB35_61
.LBB35_138:
	movl	$-1, %r14d
	jmp	.LBB35_140
.LBB35_139:
	movl	$1, %r14d
.LBB35_140:                             # %cmp_priorities.exit.i
	movq	152(%rsp), %rdi
	leaq	176(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB35_142
# BB#141:
	callq	free
.LBB35_142:
	leaq	176(%rsp), %rax
	movq	%rax, 168(%rsp)
	movq	%rax, 152(%rsp)
	leaq	240(%rsp), %rax
	movq	%rax, 160(%rsp)
	movq	240(%rsp), %rdi
	leaq	264(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB35_144
# BB#143:
	callq	free
.LBB35_144:
	leaq	264(%rsp), %rax
	movq	%rax, 256(%rsp)
	movq	%rax, 240(%rsp)
	leaq	328(%rsp), %rax
	movq	%rax, 248(%rsp)
	movq	32(%rsp), %rdi
	leaq	56(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB35_146
# BB#145:
	callq	free
.LBB35_146:
	leaq	56(%rsp), %rax
	movq	%rax, 48(%rsp)
	movq	%rax, 32(%rsp)
	leaq	88(%rsp), %rax
	movq	%rax, 40(%rsp)
	movq	88(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB35_148
# BB#147:
	callq	free
.LBB35_148:
	movq	%r12, 104(%rsp)
	movq	%r12, 88(%rsp)
	leaq	144(%rsp), %rax
	movq	%rax, 96(%rsp)
	testl	%r14d, %r14d
	movq	8(%rsp), %r8            # 8-byte Reload
	je	.LBB35_151
# BB#149:                               # %cmp_pnodes.exit
	cmpl	$1, %r14d
	je	.LBB35_168
# BB#150:                               # %cmp_pnodes.exit
	cmpl	$-1, %r14d
	je	.LBB35_164
	jmp	.LBB35_166
.LBB35_151:
	cmpl	$0, 104(%r8)
	je	.LBB35_154
.LBB35_152:                             # %cmp_eagerness.exit.thread.i
	cmpl	$0, 108(%r8)
	je	.LBB35_163
.LBB35_153:                             # %cmp_pnodes.exit.thread60
	movq	104(%r13), %rax
	movq	%rax, 104(%r15)
	movq	%r15, 104(%r13)
	jmp	.LBB35_166
.LBB35_154:
	movl	40(%r15), %eax
	movl	40(%r13), %ecx
	cmpl	%ecx, %eax
	movq	%r13, %rdx
	cmovbq	%r15, %rdx
	movslq	40(%rdx), %rdx
	testq	%rdx, %rdx
	jle	.LBB35_152
# BB#155:                               # %.lr.ph.i39.i
	movq	48(%r13), %rsi
	decl	%eax
	decl	%ecx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB35_156:                             # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rdi,8), %rbp
	movq	200(%rbp), %rbp
	cmpq	%rdi, %rax
	movq	%r15, %rbx
	je	.LBB35_158
# BB#157:                               #   in Loop: Header=BB35_156 Depth=1
	movq	48(%r15), %rbx
	movq	(%rbx,%rdi,8), %rbx
.LBB35_158:                             #   in Loop: Header=BB35_156 Depth=1
	movq	200(%rbx), %rbx
	cmpq	%rdi, %rcx
	jne	.LBB35_160
# BB#159:                               #   in Loop: Header=BB35_156 Depth=1
	movq	200(%r13), %rbp
.LBB35_160:                             #   in Loop: Header=BB35_156 Depth=1
	cmpq	%rbp, %rbx
	ja	.LBB35_164
# BB#161:                               #   in Loop: Header=BB35_156 Depth=1
	cmpq	%rbp, %rbx
	jb	.LBB35_168
# BB#162:                               #   in Loop: Header=BB35_156 Depth=1
	incq	%rdi
	cmpq	%rdx, %rdi
	jl	.LBB35_156
	jmp	.LBB35_152
.LBB35_163:
	movl	80(%r13), %eax
	cmpl	%eax, 80(%r15)
	jae	.LBB35_167
.LBB35_164:
	movq	%r8, %rdi
	movq	%r15, %rsi
	movq	%r8, %rbx
	callq	insert_PNode_internal
	movl	32(%r15), %eax
	movq	208(%rbx), %rcx
	movq	%rcx, 88(%r15)
	movq	%r15, 208(%rbx)
	movq	%r15, 112(%r13)
	addl	$2, %eax
	movl	%eax, 32(%r15)
.LBB35_165:                             # %PNode_equal.exit.thread57
	movq	%r15, %r13
.LBB35_166:                             # %PNode_equal.exit.thread57
	movq	%r13, %rax
	addq	$328, %rsp              # imm = 0x148
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB35_167:
	jbe	.LBB35_153
.LBB35_168:
	movq	%r8, %rdi
	movq	%r15, %rsi
	callq	free_PNode
	jmp	.LBB35_166
.LBB35_169:
	cmpq	%r9, 16(%r13)
	jne	.LBB35_12
# BB#170:
	movslq	40(%r13), %r10
	testq	%r11, %r11
	jne	.LBB35_172
# BB#171:
	testl	%r10d, %r10d
	je	.LBB35_166
.LBB35_172:
	cmpl	(%r11), %r10d
	jne	.LBB35_12
# BB#173:                               # %.preheader.i
	testl	%r10d, %r10d
	jle	.LBB35_166
# BB#174:                               # %.lr.ph.i46
	movq	48(%r13), %r14
	movq	8(%r11), %r11
	leal	-1(%r10), %esi
	xorl	%edx, %edx
.LBB35_175:                             # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rdx,8), %rbx
	movq	112(%rbx), %rbx
	movslq	%esi, %rbp
	movq	(%r11,%rbp,8), %rbp
	movq	(%rbp), %rbp
	cmpq	112(%rbp), %rbx
	jne	.LBB35_12
# BB#176:                               #   in Loop: Header=BB35_175 Depth=1
	incq	%rdx
	decl	%esi
	cmpq	%rdx, %r10
	jg	.LBB35_175
	jmp	.LBB35_166
.Lfunc_end35:
	.size	add_PNode, .Lfunc_end35-add_PNode
	.cfi_endproc

	.p2align	4, 0x90
	.type	set_add_znode,@function
set_add_znode:                          # @set_add_znode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi218:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi219:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi220:
	.cfi_def_cfa_offset 32
.Lcfi221:
	.cfi_offset %rbx, -32
.Lcfi222:
	.cfi_offset %r14, -24
.Lcfi223:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	cmpl	$2, %eax
	jg	.LBB36_11
# BB#1:
	movq	8(%rbx), %rcx
	leaq	16(%rbx), %rdx
	testq	%rcx, %rcx
	je	.LBB36_2
# BB#4:
	cmpq	%rdx, %rcx
	je	.LBB36_5
# BB#7:
	testb	$7, %al
	je	.LBB36_10
# BB#8:
	leal	1(%rax), %edx
	jmp	.LBB36_9
.LBB36_11:
	cmpl	$3, %eax
	jne	.LBB36_13
# BB#12:                                # %.lr.ph
	movq	8(%rbx), %r15
	movl	$0, (%rbx)
	movq	$0, 8(%rbx)
	movq	(%r15), %rsi
	movq	%rbx, %rdi
	callq	set_add_znode_hash
	movq	8(%r15), %rsi
	movq	%rbx, %rdi
	callq	set_add_znode_hash
	movq	16(%r15), %rsi
	movq	%rbx, %rdi
	callq	set_add_znode_hash
.LBB36_13:                              # %.loopexit
	movq	%rbx, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	set_add_znode_hash      # TAILCALL
.LBB36_2:
	movq	%rdx, 8(%rbx)
	leal	1(%rax), %ecx
	movl	%ecx, (%rbx)
	movq	%r14, 16(%rbx,%rax,8)
	jmp	.LBB36_3
.LBB36_5:
	cmpl	$2, %eax
	ja	.LBB36_10
# BB#6:
	movl	%eax, %edx
	incl	%edx
.LBB36_9:
	movl	%edx, (%rbx)
	movq	%r14, (%rcx,%rax,8)
.LBB36_3:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB36_10:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	vec_add_internal        # TAILCALL
.Lfunc_end36:
	.size	set_add_znode, .Lfunc_end36-set_add_znode
	.cfi_endproc

	.p2align	4, 0x90
	.type	reduce_one,@function
reduce_one:                             # @reduce_one
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi224:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi225:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi226:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi227:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi228:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi229:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi230:
	.cfi_def_cfa_offset 224
.Lcfi231:
	.cfi_offset %rbx, -56
.Lcfi232:
	.cfi_offset %r12, -48
.Lcfi233:
	.cfi_offset %r13, -40
.Lcfi234:
	.cfi_offset %r14, -32
.Lcfi235:
	.cfi_offset %r15, -24
.Lcfi236:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movq	8(%rsi), %r14
	movq	16(%rsi), %r9
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	(%rsi), %rdi
	testq	%rdi, %rdi
	je	.LBB37_4
# BB#1:
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movzwl	(%r9), %ecx
	movl	$0, 88(%rsp)
	movq	$0, 96(%rsp)
	testw	%cx, %cx
	je	.LBB37_6
# BB#2:                                 # %build_paths.exit.preheader
	movl	$0, path1(%rip)
	movq	$0, path1+8(%rip)
	leaq	104(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$1, 88(%rsp)
	movq	$path1, 104(%rsp)
	leaq	88(%rsp), %rsi
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	movq	%rcx, %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	%rcx, %r8
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	build_paths_internal
	cmpl	$0, 88(%rsp)
	je	.LBB37_60
# BB#3:                                 # %.lr.ph99
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	24(%rax), %r12
	xorl	%ecx, %ecx
	jmp	.LBB37_9
.LBB37_4:
	movzwl	2(%r9), %esi
	leaq	24(%r14), %rbx
	movq	24(%r14), %rcx
	movq	64(%r14), %r8
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	pushq	$0
.Lcfi237:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi238:
	.cfi_adjust_cfa_offset 8
	callq	add_PNode
	addq	$16, %rsp
.Lcfi239:
	.cfi_adjust_cfa_offset -16
	testq	%rax, %rax
	je	.LBB37_74
# BB#5:
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%r14, %rcx
	callq	goto_PNode
	decl	112(%r14)
	jne	.LBB37_76
	jmp	.LBB37_75
.LBB37_6:
	xorl	%ebx, %ebx
	jmp	.LBB37_60
.LBB37_7:                               #   in Loop: Header=BB37_9 Depth=1
	andl	$2, %r9d
	shrl	%esi
	andl	$1, %esi
	leal	2(%r9,%rsi), %r8d
.LBB37_8:                               # %check_child.exit.i
                                        #   in Loop: Header=BB37_9 Depth=1
	leaq	(%rdi,%rdi,8), %rsi
	leaq	(%rbx,%rbx,2), %rdi
	shlq	$3, %rdi
	leaq	(%rdi,%rsi,8), %rsi
	cmpl	$0, child_table(%rsi,%r8,4)
	jne	.LBB37_18
	jmp	.LBB37_59
	.p2align	4, 0x90
.LBB37_9:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_12 Depth 2
                                        #     Child Loop BB37_29 Depth 2
                                        #       Child Loop BB37_31 Depth 3
                                        #         Child Loop BB37_34 Depth 4
                                        #           Child Loop BB37_36 Depth 5
                                        #     Child Loop BB37_44 Depth 2
                                        #       Child Loop BB37_46 Depth 3
                                        #     Child Loop BB37_58 Depth 2
	movq	96(%rsp), %rax
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB37_16
# BB#10:                                # %.preheader86
                                        #   in Loop: Header=BB37_9 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	movslq	32(%rdx), %rdx
	movq	(%rax,%rdx,8), %rdx
	movl	8(%rdx), %eax
	testl	%eax, %eax
	je	.LBB37_14
# BB#11:                                # %.lr.ph
                                        #   in Loop: Header=BB37_9 Depth=1
	movq	16(%rdx), %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB37_12:                              #   Parent Loop BB37_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, (%rsi,%rdx,8)
	je	.LBB37_15
# BB#13:                                #   in Loop: Header=BB37_12 Depth=2
	incq	%rdx
	cmpl	%eax, %edx
	jb	.LBB37_12
	jmp	.LBB37_15
	.p2align	4, 0x90
.LBB37_14:                              #   in Loop: Header=BB37_9 Depth=1
	xorl	%edx, %edx
.LBB37_15:                              # %._crit_edge
                                        #   in Loop: Header=BB37_9 Depth=1
	cmpl	%eax, %edx
	jae	.LBB37_59
.LBB37_16:                              #   in Loop: Header=BB37_9 Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %eax
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	(%rcx), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	cmpl	$2, %eax
	jb	.LBB37_55
# BB#17:                                #   in Loop: Header=BB37_9 Depth=1
	xorl	%edx, %edx
	movq	64(%rsp), %rsi          # 8-byte Reload
	cmpl	$0, 8(%rsi)
	movq	%rsi, (%rsp)            # 8-byte Spill
	je	.LBB37_21
.LBB37_18:                              #   in Loop: Header=BB37_9 Depth=1
	leal	1(%rdx), %esi
	cmpl	%esi, %eax
	jbe	.LBB37_27
# BB#19:                                #   in Loop: Header=BB37_9 Depth=1
	movl	%esi, %ecx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsi,%rcx,8), %rcx
	movq	(%rcx), %r13
	orl	$2, %edx
	cmpl	%edx, %eax
	jbe	.LBB37_42
# BB#20:                                #   in Loop: Header=BB37_9 Depth=1
	movl	%edx, %eax
	movq	(%rsi,%rax,8), %rax
	movq	(%rax), %rdx
	jmp	.LBB37_54
.LBB37_21:                              #   in Loop: Header=BB37_9 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	8(%rcx), %rcx
	movq	(%rcx), %rcx
	cmpl	$0, 8(%rcx)
	je	.LBB37_55
# BB#22:                                #   in Loop: Header=BB37_9 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	8(%rcx), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	movl	8(%rdx), %esi
	testl	%esi, %esi
	je	.LBB37_55
# BB#23:                                #   in Loop: Header=BB37_9 Depth=1
	movq	64(%rsp), %r10          # 8-byte Reload
	movl	(%r10), %r9d
	movl	$1, %edx
	testl	%r9d, %r9d
	je	.LBB37_18
# BB#24:                                #   in Loop: Header=BB37_9 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	12(%rdi), %r8d
	xorl	%edi, %edi
	cmpl	$9, %esi
	setne	%dil
	orq	$2, %rdi
	testb	$20, %sil
	movl	$1, %ebp
	cmovneq	%rbp, %rdi
	xorl	%ebx, %ebx
	cmpl	$9, %r9d
	setne	%bl
	incq	%rbx
	testb	$20, %r9b
	movl	$0, %ebp
	cmovneq	%rbp, %rbx
	cmpl	%r8d, 4(%r10)
	movl	$0, %r8d
	jg	.LBB37_8
# BB#25:                                #   in Loop: Header=BB37_9 Depth=1
	jge	.LBB37_7
# BB#26:                                #   in Loop: Header=BB37_9 Depth=1
	movl	$1, %r8d
	jmp	.LBB37_8
.LBB37_27:                              #   in Loop: Header=BB37_9 Depth=1
	movl	8(%rcx), %eax
	movl	%eax, 80(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	je	.LBB37_55
# BB#28:                                # %.preheader105.lr.ph.i
                                        #   in Loop: Header=BB37_9 Depth=1
	movq	16(%rcx), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB37_29:                              # %.preheader105.i
                                        #   Parent Loop BB37_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB37_31 Depth 3
                                        #         Child Loop BB37_34 Depth 4
                                        #           Child Loop BB37_36 Depth 5
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movl	72(%rax), %ecx
	movl	%ecx, 84(%rsp)          # 4-byte Spill
	testl	%ecx, %ecx
	je	.LBB37_41
# BB#30:                                # %.lr.ph119.preheader.i
                                        #   in Loop: Header=BB37_29 Depth=2
	movq	80(%rax), %rax
	xorl	%ecx, %ecx
	movq	%rax, 136(%rsp)         # 8-byte Spill
.LBB37_31:                              # %.lr.ph119.i
                                        #   Parent Loop BB37_9 Depth=1
                                        #     Parent Loop BB37_29 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB37_34 Depth 4
                                        #           Child Loop BB37_36 Depth 5
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %rbp
	testq	%rbp, %rbp
	je	.LBB37_40
# BB#32:                                # %.preheader103.i
                                        #   in Loop: Header=BB37_31 Depth=3
	movl	8(%rbp), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	je	.LBB37_40
# BB#33:                                # %.preheader101.lr.ph.i
                                        #   in Loop: Header=BB37_31 Depth=3
	movq	16(%rbp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
.LBB37_34:                              # %.preheader101.i
                                        #   Parent Loop BB37_9 Depth=1
                                        #     Parent Loop BB37_29 Depth=2
                                        #       Parent Loop BB37_31 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB37_36 Depth 5
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rax
	movl	72(%rax), %r15d
	testl	%r15d, %r15d
	je	.LBB37_39
# BB#35:                                # %.lr.ph115.preheader.i
                                        #   in Loop: Header=BB37_34 Depth=4
	movq	80(%rax), %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB37_36:                              # %.lr.ph115.i
                                        #   Parent Loop BB37_9 Depth=1
                                        #     Parent Loop BB37_29 Depth=2
                                        #       Parent Loop BB37_31 Depth=3
                                        #         Parent Loop BB37_34 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%r13,%r14,8), %rax
	testq	%rax, %rax
	je	.LBB37_38
# BB#37:                                #   in Loop: Header=BB37_36 Depth=5
	movq	(%rbp), %rsi
	movq	(%rax), %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	check_assoc_priority
	testl	%eax, %eax
	je	.LBB37_55
.LBB37_38:                              #   in Loop: Header=BB37_36 Depth=5
	incq	%r14
	cmpl	%r15d, %r14d
	jb	.LBB37_36
.LBB37_39:                              # %._crit_edge116.i
                                        #   in Loop: Header=BB37_34 Depth=4
	incq	%rbx
	cmpl	48(%rsp), %ebx          # 4-byte Folded Reload
	jb	.LBB37_34
	.p2align	4, 0x90
.LBB37_40:                              # %.loopexit104.i
                                        #   in Loop: Header=BB37_31 Depth=3
	movq	144(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	cmpl	84(%rsp), %ecx          # 4-byte Folded Reload
	movq	136(%rsp), %rax         # 8-byte Reload
	jb	.LBB37_31
	.p2align	4, 0x90
.LBB37_41:                              # %._crit_edge120.i
                                        #   in Loop: Header=BB37_29 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpl	80(%rsp), %ecx          # 4-byte Folded Reload
	jb	.LBB37_29
	jmp	.LBB37_55
.LBB37_42:                              # %.preheader100.i
                                        #   in Loop: Header=BB37_9 Depth=1
	movl	8(%rcx), %edx
	testl	%edx, %edx
	je	.LBB37_53
# BB#43:                                # %.preheader.lr.ph.i
                                        #   in Loop: Header=BB37_9 Depth=1
	movq	16(%rcx), %rsi
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movl	%edx, 56(%rsp)          # 4-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
.LBB37_44:                              # %.preheader.i
                                        #   Parent Loop BB37_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB37_46 Depth 3
	movq	(%rsi,%r15,8), %rcx
	movl	72(%rcx), %r14d
	testl	%r14d, %r14d
	je	.LBB37_50
# BB#45:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB37_44 Depth=2
	movq	80(%rcx), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB37_46:                              # %.lr.ph.i
                                        #   Parent Loop BB37_9 Depth=1
                                        #     Parent Loop BB37_44 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx,%rbp,8), %rax
	testq	%rax, %rax
	je	.LBB37_48
# BB#47:                                #   in Loop: Header=BB37_46 Depth=3
	movq	(%rax), %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r13, %rsi
	callq	check_assoc_priority
	testl	%eax, %eax
	je	.LBB37_55
.LBB37_48:                              #   in Loop: Header=BB37_46 Depth=3
	incq	%rbp
	cmpl	%r14d, %ebp
	jb	.LBB37_46
# BB#49:                                #   in Loop: Header=BB37_44 Depth=2
	movl	$1, %eax
	movl	56(%rsp), %edx          # 4-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
.LBB37_50:                              # %._crit_edge.i
                                        #   in Loop: Header=BB37_44 Depth=2
	incq	%r15
	cmpl	%edx, %r15d
	jb	.LBB37_44
# BB#51:                                # %._crit_edge112.i
                                        #   in Loop: Header=BB37_9 Depth=1
	testl	%eax, %eax
	jne	.LBB37_59
.LBB37_53:                              #   in Loop: Header=BB37_9 Depth=1
	xorl	%edx, %edx
.LBB37_54:                              # %check_path_priorities_internal.exit
                                        #   in Loop: Header=BB37_9 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r13, %rsi
	callq	check_assoc_priority
	testl	%eax, %eax
	jne	.LBB37_59
	.p2align	4, 0x90
.LBB37_55:                              # %check_path_priorities_internal.exit.thread
                                        #   in Loop: Header=BB37_9 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	incl	168(%rdi)
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	-8(%rcx,%rax,8), %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %r9
	movzwl	2(%r9), %esi
	movq	(%rbx), %rdx
	movl	$160, %eax
	addq	%rax, %rdx
	movq	(%r12), %rcx
	movq	64(%rsp), %r8           # 8-byte Reload
	pushq	$0
.Lcfi240:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi241:
	.cfi_adjust_cfa_offset 8
	callq	add_PNode
	addq	$16, %rsp
.Lcfi242:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB37_59
# BB#56:                                # %.preheader
                                        #   in Loop: Header=BB37_9 Depth=1
	cmpl	$0, 8(%rbx)
	je	.LBB37_59
# BB#57:                                # %.lr.ph97
                                        #   in Loop: Header=BB37_9 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB37_58:                              #   Parent Loop BB37_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rax
	movq	(%rax,%rbp,8), %rcx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	goto_PNode
	incq	%rbp
	cmpl	8(%rbx), %ebp
	jb	.LBB37_58
.LBB37_59:                              # %check_path_priorities_internal.exit.thread83
                                        #   in Loop: Header=BB37_9 Depth=1
	movq	160(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movl	88(%rsp), %ebx
	cmpl	%ebx, %ecx
	jb	.LBB37_9
.LBB37_60:                              # %build_paths.exit._crit_edge
	movq	path1+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB37_63
# BB#61:                                # %build_paths.exit._crit_edge
	movl	$path1+16, %eax
	cmpq	%rax, %rdi
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	je	.LBB37_64
# BB#62:
	callq	free
	movl	88(%rsp), %ebx
	jmp	.LBB37_64
.LBB37_63:
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB37_64:
	movl	$0, path1(%rip)
	movq	$0, path1+8(%rip)
	movq	96(%rsp), %rdi
	cmpl	$2, %ebx
	jb	.LBB37_70
# BB#65:                                # %.lr.ph.i77.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB37_66:                              # %.lr.ph.i77
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB37_69
# BB#67:                                # %.lr.ph.i77
                                        #   in Loop: Header=BB37_66 Depth=1
	leaq	16(%rax), %rcx
	cmpq	%rcx, %rdi
	je	.LBB37_69
# BB#68:                                #   in Loop: Header=BB37_66 Depth=1
	callq	free
	movq	96(%rsp), %rax
	movq	(%rax,%rbx,8), %rax
.LBB37_69:                              #   in Loop: Header=BB37_66 Depth=1
	movl	$0, (%rax)
	movq	$0, 8(%rax)
	movq	96(%rsp), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	free
	incq	%rbx
	movq	96(%rsp), %rdi
	cmpl	88(%rsp), %ebx
	jb	.LBB37_66
.LBB37_70:                              # %._crit_edge.i81
	testq	%rdi, %rdi
	je	.LBB37_73
# BB#71:                                # %._crit_edge.i81
	leaq	104(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB37_73
# BB#72:
	callq	free
.LBB37_73:                              # %free_paths.exit
	movl	$0, 88(%rsp)
	movq	$0, 96(%rsp)
.LBB37_74:
	decl	112(%r14)
	jne	.LBB37_76
.LBB37_75:
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	free_SNode
.LBB37_76:
	movq	296(%rbp), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 40(%rcx)
	movq	%rcx, 296(%rbp)
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end37:
	.size	reduce_one, .Lfunc_end37-reduce_one
	.cfi_endproc

	.p2align	4, 0x90
	.type	commit_stack,@function
commit_stack:                           # @commit_stack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi243:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi244:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi245:
	.cfi_def_cfa_offset 32
.Lcfi246:
	.cfi_offset %rbx, -32
.Lcfi247:
	.cfi_offset %r14, -24
.Lcfi248:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$-1, %ebp
	cmpl	$1, 72(%rbx)
	jne	.LBB38_10
# BB#1:
	movq	80(%rbx), %rax
	movq	(%rax), %rax
	movl	8(%rax), %ecx
	movl	$-2, %ebp
	cmpl	$1, %ecx
	ja	.LBB38_10
# BB#2:
	movq	(%rax), %rsi
	movq	160(%rsi), %rdx
	cmpq	192(%rsi), %rdx
	je	.LBB38_3
.LBB38_5:
	testl	%ecx, %ecx
	je	.LBB38_6
# BB#7:
	movq	16(%rax), %rax
	movq	(%rax), %rsi
	movq	%r14, %rdi
	callq	commit_stack
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB38_10
# BB#8:                                 # %._crit_edge
	movq	80(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	jmp	.LBB38_9
.LBB38_3:
	movq	16(%rsi), %rdx
	testq	%rdx, %rdx
	je	.LBB38_5
# BB#4:
	movl	$-3, %ebp
	cmpq	$0, 16(%rdx)
	jne	.LBB38_10
	jmp	.LBB38_5
.LBB38_6:
	xorl	%ebp, %ebp
.LBB38_9:
	movq	%r14, %rdi
	callq	commit_tree
	movq	80(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	%rax, (%rcx)
.LBB38_10:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end38:
	.size	commit_stack, .Lfunc_end38-commit_stack
	.cfi_endproc

	.p2align	4, 0x90
	.type	new_SNode,@function
new_SNode:                              # @new_SNode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi249:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi250:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi251:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi252:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi253:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi254:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi255:
	.cfi_def_cfa_offset 64
.Lcfi256:
	.cfi_offset %rbx, -56
.Lcfi257:
	.cfi_offset %r12, -48
.Lcfi258:
	.cfi_offset %r13, -40
.Lcfi259:
	.cfi_offset %r14, -32
.Lcfi260:
	.cfi_offset %r15, -24
.Lcfi261:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rcx, %r13
	movq	%rdx, %r15
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movq	328(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB39_1
# BB#2:
	movq	128(%rbx), %rax
	movq	%rax, 328(%r14)
	jmp	.LBB39_3
.LBB39_1:
	movl	$136, %edi
	callq	malloc
	movq	%rax, %rbx
.LBB39_3:
	movl	$0, 56(%rbx)
	movl	$0, 72(%rbx)
	movq	$0, 80(%rbx)
	movl	$0, 112(%rbx)
	movq	$0, 128(%rbx)
	incl	152(%r14)
	movq	%rbp, (%rbx)
	movq	%r13, 8(%rbx)
	movq	%r12, 16(%rbx)
	movups	(%r15), %xmm0
	movups	16(%r15), %xmm1
	movups	%xmm1, 40(%rbx)
	movups	%xmm0, 24(%rbx)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	insert_SNode_internal
	movl	112(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 112(%rbx)
	movq	240(%r14), %rcx
	movq	%rcx, 128(%rbx)
	movq	%rbx, 240(%r14)
	movq	(%rbx), %rcx
	cmpb	$0, 89(%rcx)
	je	.LBB39_10
# BB#4:
	movq	280(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB39_5
# BB#6:
	leaq	24(%rbx), %rcx
	movq	(%rcx), %rcx
	cmpq	24(%rsi), %rcx
	jbe	.LBB39_10
# BB#7:
	addl	$2, %eax
	movl	%eax, 112(%rbx)
	decl	112(%rsi)
	jne	.LBB39_9
# BB#8:
	movq	%r14, %rdi
	callq	free_SNode
	jmp	.LBB39_9
.LBB39_5:
	addl	$2, %eax
	movl	%eax, 112(%rbx)
.LBB39_9:
	movq	%rbx, 280(%r14)
.LBB39_10:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end39:
	.size	new_SNode, .Lfunc_end39-new_SNode
	.cfi_endproc

	.p2align	4, 0x90
	.type	add_Reduction,@function
add_Reduction:                          # @add_Reduction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi262:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi263:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi264:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi265:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi266:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi267:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi268:
	.cfi_def_cfa_offset 80
.Lcfi269:
	.cfi_offset %rbx, -56
.Lcfi270:
	.cfi_offset %r12, -48
.Lcfi271:
	.cfi_offset %r13, -40
.Lcfi272:
	.cfi_offset %r14, -32
.Lcfi273:
	.cfi_offset %r15, -24
.Lcfi274:
	.cfi_offset %rbp, -16
	leaq	256(%rdi), %r13
	testq	%rsi, %rsi
	movq	%rsi, (%rsp)            # 8-byte Spill
	je	.LBB40_1
# BB#2:                                 # %.preheader.i
	movl	8(%rsi), %r9d
	testq	%r9, %r9
	je	.LBB40_3
# BB#4:                                 # %.lr.ph.i
	movq	16(%rsi), %r10
	leaq	-1(%r9), %r8
	movq	%r9, %rbp
	xorl	%esi, %esi
	xorl	%eax, %eax
	andq	$3, %rbp
	je	.LBB40_6
	.p2align	4, 0x90
.LBB40_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%r10,%rsi,8), %rbx
	movl	56(%rbx), %ebx
	cmpl	%ebx, %eax
	cmovbl	%ebx, %eax
	incq	%rsi
	cmpq	%rsi, %rbp
	jne	.LBB40_5
.LBB40_6:                               # %.prol.loopexit88
	cmpq	$3, %r8
	jb	.LBB40_9
# BB#7:                                 # %.lr.ph.i.new
	subq	%rsi, %r9
	leaq	24(%r10,%rsi,8), %rbx
	.p2align	4, 0x90
.LBB40_8:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rbx), %rsi
	movq	-16(%rbx), %rbp
	movl	56(%rsi), %esi
	cmpl	%esi, %eax
	cmovbl	%esi, %eax
	movl	56(%rbp), %esi
	cmpl	%esi, %eax
	cmovbl	%esi, %eax
	movq	-8(%rbx), %rsi
	movl	56(%rsi), %esi
	cmpl	%esi, %eax
	cmovbl	%esi, %eax
	movq	(%rbx), %rsi
	movl	56(%rsi), %esi
	cmpl	%esi, %eax
	cmovbl	%esi, %eax
	addq	$32, %rbx
	addq	$-4, %r9
	jne	.LBB40_8
	jmp	.LBB40_9
.LBB40_1:
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	jmp	.LBB40_9
.LBB40_3:
	xorl	%eax, %eax
.LBB40_9:                               # %znode_depth.exit
	movq	(%r13), %r11
	testq	%r11, %r11
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	je	.LBB40_31
# BB#10:                                # %.lr.ph
	movq	24(%rdx), %r8
	.p2align	4, 0x90
.LBB40_11:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_17 Depth 2
                                        #     Child Loop BB40_20 Depth 2
	movq	8(%r11), %r9
	movq	24(%r9), %r10
	cmpq	%r10, %r8
	jb	.LBB40_31
# BB#12:                                #   in Loop: Header=BB40_11 Depth=1
	movq	(%r11), %rcx
	testq	%rcx, %rcx
	je	.LBB40_13
# BB#14:                                # %.preheader.i53
                                        #   in Loop: Header=BB40_11 Depth=1
	movl	8(%rcx), %ebx
	testq	%rbx, %rbx
	je	.LBB40_15
# BB#16:                                # %.lr.ph.i54
                                        #   in Loop: Header=BB40_11 Depth=1
	movq	16(%rcx), %rdx
	leaq	-1(%rbx), %r12
	movq	%rbx, %r14
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	andq	$3, %r14
	je	.LBB40_18
	.p2align	4, 0x90
.LBB40_17:                              #   Parent Loop BB40_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rcx,8), %r15
	movl	56(%r15), %ebp
	cmpl	%ebp, %esi
	cmovbl	%ebp, %esi
	incq	%rcx
	cmpq	%rcx, %r14
	jne	.LBB40_17
.LBB40_18:                              # %.prol.loopexit
                                        #   in Loop: Header=BB40_11 Depth=1
	cmpq	$3, %r12
	jb	.LBB40_21
# BB#19:                                # %.lr.ph.i54.new
                                        #   in Loop: Header=BB40_11 Depth=1
	subq	%rcx, %rbx
	leaq	24(%rdx,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB40_20:                              #   Parent Loop BB40_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rcx), %rdx
	movq	-16(%rcx), %rbp
	movl	56(%rdx), %edx
	cmpl	%edx, %esi
	cmovbl	%edx, %esi
	movl	56(%rbp), %edx
	cmpl	%edx, %esi
	cmovbl	%edx, %esi
	movq	-8(%rcx), %rdx
	movl	56(%rdx), %edx
	cmpl	%edx, %esi
	cmovbl	%edx, %esi
	movq	(%rcx), %rdx
	movl	56(%rdx), %edx
	cmpl	%edx, %esi
	cmovbl	%edx, %esi
	addq	$32, %rcx
	addq	$-4, %rbx
	jne	.LBB40_20
	jmp	.LBB40_21
	.p2align	4, 0x90
.LBB40_13:                              #   in Loop: Header=BB40_11 Depth=1
	movl	$2147483647, %esi       # imm = 0x7FFFFFFF
	cmpl	%esi, %eax
	jge	.LBB40_22
	jmp	.LBB40_30
	.p2align	4, 0x90
.LBB40_15:                              #   in Loop: Header=BB40_11 Depth=1
	xorl	%esi, %esi
.LBB40_21:                              # %znode_depth.exit60
                                        #   in Loop: Header=BB40_11 Depth=1
	cmpl	%esi, %eax
	jl	.LBB40_30
.LBB40_22:                              # %znode_depth.exit60
                                        #   in Loop: Header=BB40_11 Depth=1
	cmpq	%r10, %r8
	je	.LBB40_23
.LBB40_30:                              #   in Loop: Header=BB40_11 Depth=1
	movq	%r11, %r13
	movq	40(%r11), %r11
	addq	$40, %r13
	testq	%r11, %r11
	jne	.LBB40_11
	jmp	.LBB40_31
.LBB40_23:
	cmpl	%esi, %eax
	jne	.LBB40_31
# BB#24:                                # %.preheader.preheader
	xorl	%eax, %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	cmpq	%rdx, %r9
	jne	.LBB40_28
	jmp	.LBB40_26
	.p2align	4, 0x90
.LBB40_29:                              # %..preheader_crit_edge
                                        #   in Loop: Header=BB40_28 Depth=1
	movq	8(%r11), %r9
	cmpq	%rdx, %r9
	jne	.LBB40_28
.LBB40_26:
	cmpq	%rsi, (%r11)
	jne	.LBB40_28
# BB#27:
	cmpq	%rcx, 16(%r11)
	je	.LBB40_35
	.p2align	4, 0x90
.LBB40_28:                              # =>This Inner Loop Header: Depth=1
	movq	40(%r11), %r11
	testq	%r11, %r11
	jne	.LBB40_29
.LBB40_31:                              # %.loopexit61
	movq	296(%rdi), %rax
	testq	%rax, %rax
	je	.LBB40_32
# BB#33:
	movq	40(%rax), %rcx
	movq	%rcx, 296(%rdi)
	jmp	.LBB40_34
.LBB40_32:
	movl	$48, %edi
	callq	malloc
.LBB40_34:
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rsi, (%rax)
	movq	%rdx, 8(%rax)
	movq	$0, 24(%rax)
	incl	112(%rdx)
	movq	%rcx, 16(%rax)
	movq	(%r13), %rcx
	movq	%rcx, 40(%rax)
	movq	%rax, (%r13)
.LBB40_35:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end40:
	.size	add_Reduction, .Lfunc_end40-add_Reduction
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_exp_one,@function
get_exp_one:                            # @get_exp_one
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi275:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi276:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi277:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi278:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi279:
	.cfi_def_cfa_offset 48
.Lcfi280:
	.cfi_offset %rbx, -40
.Lcfi281:
	.cfi_offset %r12, -32
.Lcfi282:
	.cfi_offset %r14, -24
.Lcfi283:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	testb	$4, (%r12)
	jne	.LBB41_10
# BB#1:
	movq	16(%r14), %rax
	cmpq	8(%r14), %rax
	je	.LBB41_2
# BB#3:
	leaq	8(%rax), %rcx
	movq	%rcx, 16(%r14)
	movq	%r12, (%rax)
	jmp	.LBB41_4
.LBB41_10:
	movq	16(%r15), %rcx
	movl	4(%r12), %eax
	cmpq	8(%r15), %rcx
	je	.LBB41_11
# BB#12:
	leaq	4(%rcx), %rdx
	movq	%rdx, 16(%r15)
	movl	%eax, (%rcx)
	jmp	.LBB41_13
.LBB41_2:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	stack_push_internal
.LBB41_4:
	movq	(%r14), %r8
	movq	16(%r14), %rcx
	addq	$8, %r8
	cmpq	%r8, %rcx
	ja	.LBB41_6
	jmp	.LBB41_18
	.p2align	4, 0x90
.LBB41_9:                               #   in Loop: Header=BB41_6 Depth=1
	movq	%rdi, -8(%rdx)
	movq	%rsi, -16(%rdx)
.LBB41_5:                               #   in Loop: Header=BB41_6 Depth=1
	cmpq	%r8, %rcx
	jbe	.LBB41_18
.LBB41_6:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	leaq	-8(%rdx), %rcx
	movq	-16(%rdx), %rdi
	movq	-8(%rdx), %rsi
	movl	80(%rsi), %ebx
	movl	80(%rdi), %eax
	cmpl	%eax, %ebx
	ja	.LBB41_5
# BB#7:                                 #   in Loop: Header=BB41_6 Depth=1
	cmpq	%rdi, %rsi
	jbe	.LBB41_9
# BB#8:                                 #   in Loop: Header=BB41_6 Depth=1
	cmpl	%eax, %ebx
	je	.LBB41_5
	jmp	.LBB41_9
.LBB41_11:
	movslq	%eax, %rsi
	movq	%r15, %rdi
	callq	stack_push_internal
.LBB41_13:                              # %.preheader
	movl	40(%r12), %eax
	testl	%eax, %eax
	je	.LBB41_18
# BB#14:                                # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB41_15:                              # =>This Inner Loop Header: Depth=1
	movq	48(%r12), %rcx
	movq	(%rcx,%rbx,8), %rdi
	cmpl	$0, (%rdi)
	je	.LBB41_17
# BB#16:                                #   in Loop: Header=BB41_15 Depth=1
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	get_exp_one
	movl	40(%r12), %eax
.LBB41_17:                              #   in Loop: Header=BB41_15 Depth=1
	incq	%rbx
	cmpl	%eax, %ebx
	jb	.LBB41_15
.LBB41_18:                              # %priority_insert.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end41:
	.size	get_exp_one, .Lfunc_end41-get_exp_one
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_exp_all,@function
get_exp_all:                            # @get_exp_all
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi284:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi285:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi286:
	.cfi_def_cfa_offset 32
.Lcfi287:
	.cfi_offset %rbx, -32
.Lcfi288:
	.cfi_offset %r14, -24
.Lcfi289:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpl	$0, (%r15)
	je	.LBB42_4
# BB#1:
	movq	16(%r14), %rcx
	movl	4(%r15), %eax
	cmpq	8(%r14), %rcx
	je	.LBB42_2
# BB#3:
	leaq	4(%rcx), %rdx
	movq	%rdx, 16(%r14)
	movl	%eax, (%rcx)
	cmpl	$0, 40(%r15)
	jne	.LBB42_5
	jmp	.LBB42_7
.LBB42_2:
	movslq	%eax, %rsi
	movq	%r14, %rdi
	callq	stack_push_internal
.LBB42_4:                               # %.preheader
	cmpl	$0, 40(%r15)
	je	.LBB42_7
.LBB42_5:                               # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB42_6:                               # =>This Inner Loop Header: Depth=1
	movq	48(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	112(%rax), %rdi
	movq	%r14, %rsi
	callq	get_exp_all
	incq	%rbx
	cmpl	40(%r15), %ebx
	jb	.LBB42_6
.LBB42_7:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end42:
	.size	get_exp_all, .Lfunc_end42-get_exp_all
	.cfi_endproc

	.p2align	4, 0x90
	.type	set_add_znode_hash,@function
set_add_znode_hash:                     # @set_add_znode_hash
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi290:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi291:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi292:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi293:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi294:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi295:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi296:
	.cfi_def_cfa_offset 64
.Lcfi297:
	.cfi_offset %rbx, -56
.Lcfi298:
	.cfi_offset %r12, -48
.Lcfi299:
	.cfi_offset %r13, -40
.Lcfi300:
	.cfi_offset %r14, -32
.Lcfi301:
	.cfi_offset %r15, -24
.Lcfi302:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	(%r12), %ebx
	testq	%rbx, %rbx
	je	.LBB43_1
# BB#2:
	movl	(%r14), %eax
	xorl	%edx, %edx
	divl	%ebx
	movq	8(%r12), %r15
	movslq	%edx, %rax
	cmpq	$0, (%r15,%rax,8)
	je	.LBB43_20
# BB#3:
	incl	%edx
	movl	%edx, %eax
	cltd
	idivl	%ebx
	cmpl	%ebx, %edx
	jae	.LBB43_4
# BB#13:
	movslq	%edx, %rax
	cmpq	$0, (%r15,%rax,8)
	je	.LBB43_20
# BB#14:
	incl	%edx
	movl	%edx, %eax
	cltd
	idivl	%ebx
	cmpl	%ebx, %edx
	jae	.LBB43_4
# BB#15:
	movslq	%edx, %rax
	cmpq	$0, (%r15,%rax,8)
	je	.LBB43_20
# BB#16:
	incl	%edx
	movl	%edx, %eax
	cltd
	idivl	%ebx
	cmpl	%ebx, %edx
	jae	.LBB43_4
# BB#17:
	movslq	%edx, %rax
	cmpq	$0, (%r15,%rax,8)
	je	.LBB43_20
# BB#18:
	incl	%edx
	movl	%edx, %eax
	cltd
	idivl	%ebx
	cmpl	%ebx, %edx
	jae	.LBB43_4
# BB#19:
	movslq	%edx, %rax
	cmpq	$0, (%r15,%rax,8)
	je	.LBB43_20
.LBB43_4:                               # %._crit_edge
	leaq	8(%r12), %rbp
	movl	4(%r12), %eax
	addl	$2, %eax
	jmp	.LBB43_5
.LBB43_1:                               # %..critedge42_crit_edge
	leaq	8(%r12), %rbp
	movl	$2, %eax
	xorl	%r15d, %r15d
.LBB43_5:                               # %.critedge42
	movl	%eax, 4(%r12)
	movl	%eax, %eax
	movl	prime2(,%rax,4), %r13d
	movl	%r13d, (%r12)
	shlq	$3, %r13
	movq	%r13, %rdi
	callq	malloc
	movq	%rax, (%rbp)
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r13, %rdx
	callq	memset
	testq	%r15, %r15
	je	.LBB43_12
# BB#6:                                 # %.preheader
	testl	%ebx, %ebx
	je	.LBB43_11
# BB#7:                                 # %.lr.ph.preheader
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB43_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rsi
	testq	%rsi, %rsi
	je	.LBB43_10
# BB#9:                                 #   in Loop: Header=BB43_8 Depth=1
	movq	%r12, %rdi
	callq	set_add_znode
.LBB43_10:                              #   in Loop: Header=BB43_8 Depth=1
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB43_8
.LBB43_11:                              # %set_union_znode.exit
	movq	%r15, %rdi
	callq	free
.LBB43_12:
	movq	%r12, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	set_add_znode           # TAILCALL
.LBB43_20:
	leaq	(%r15,%rax,8), %rax
	movq	%r14, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end43:
	.size	set_add_znode_hash, .Lfunc_end43-set_add_znode_hash
	.cfi_endproc

	.p2align	4, 0x90
	.type	goto_PNode,@function
goto_PNode:                             # @goto_PNode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi303:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi304:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi305:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi306:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi307:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi308:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi309:
	.cfi_def_cfa_offset 96
.Lcfi310:
	.cfi_offset %rbx, -56
.Lcfi311:
	.cfi_offset %r12, -48
.Lcfi312:
	.cfi_offset %r13, -40
.Lcfi313:
	.cfi_offset %r14, -32
.Lcfi314:
	.cfi_offset %r15, -24
.Lcfi315:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movq	%rdx, %r13
	movq	%rsi, %r9
	movq	(%rbp), %rcx
	movq	(%rcx), %rsi
	movl	152(%r13), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%eax, %edx
	sarl	$3, %edx
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %esi
	movl	%eax, %ebx
	andb	$7, %bl
	movzbl	%bl, %edx
	btl	%edx, %esi
	jae	.LBB44_56
# BB#1:
	movq	144(%rdi), %rsi
	movq	8(%rsi), %rdx
	movq	16(%rsi), %rsi
	subl	8(%rcx), %eax
	movslq	%eax, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	imulq	$120, %rcx, %rcx
	leaq	-120(%rdx,%rcx), %rsi
	movq	136(%r13), %rcx
	movq	144(%r13), %r8
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%r9, %rdx
	callq	add_SNode
	movq	%rax, %r12
	movq	%r13, 64(%r12)
	cmpq	%rbp, %r12
	je	.LBB44_4
# BB#2:
	movl	56(%rbp), %eax
	incl	%eax
	cmpl	%eax, 56(%r12)
	jae	.LBB44_4
# BB#3:
	movl	%eax, 56(%r12)
.LBB44_4:
	movl	72(%r12), %ecx
	cmpl	$3, %ecx
	ja	.LBB44_10
# BB#5:                                 # %.preheader.i
	testl	%ecx, %ecx
	movq	8(%rsp), %rdi           # 8-byte Reload
	je	.LBB44_13
# BB#6:                                 # %.lr.ph.i
	movq	80(%r12), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB44_8:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	movq	(%rax,%rsi,8), %r14
	cmpq	%r13, (%r14)
	je	.LBB44_9
# BB#7:                                 #   in Loop: Header=BB44_8 Depth=1
	incl	%edx
	cmpl	%ecx, %edx
	jb	.LBB44_8
	jmp	.LBB44_13
.LBB44_10:                              # %.lr.ph43.i
	movq	80(%r12), %rsi
	xorl	%edx, %edx
	movl	%r13d, %eax
	divl	%ecx
	movl	%edx, %eax
	movq	(%rsi,%rax,8), %r14
	testq	%r14, %r14
	movq	8(%rsp), %rdi           # 8-byte Reload
	je	.LBB44_13
# BB#11:
	cmpq	%r13, (%r14)
	je	.LBB44_41
# BB#12:
	incl	%eax
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%ecx
	movl	%edx, %eax
	movq	(%rsi,%rax,8), %r14
	testq	%r14, %r14
	je	.LBB44_13
# BB#57:
	cmpq	%r13, (%r14)
	je	.LBB44_41
# BB#58:
	incl	%eax
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%ecx
	movl	%edx, %eax
	movq	(%rsi,%rax,8), %r14
	testq	%r14, %r14
	je	.LBB44_13
# BB#59:
	cmpq	%r13, (%r14)
	je	.LBB44_41
# BB#60:
	incl	%eax
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%ecx
	movl	%edx, %eax
	movq	(%rsi,%rax,8), %r14
	testq	%r14, %r14
	je	.LBB44_13
# BB#61:
	cmpq	%r13, (%r14)
	je	.LBB44_41
# BB#62:
	incl	%eax
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%ecx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	(%rsi,%rdx,8), %r14
	testq	%r14, %r14
	je	.LBB44_13
# BB#63:
	cmpq	%r13, (%r14)
	jne	.LBB44_13
	jmp	.LBB44_41
.LBB44_9:                               # %set_find_znode.exit
	testq	%r14, %r14
	jne	.LBB44_41
.LBB44_13:                              # %set_find_znode.exit.thread
	leaq	72(%r12), %rbx
	movq	336(%rdi), %r14
	testq	%r14, %r14
	je	.LBB44_14
# BB#15:
	movq	(%r14), %rax
	movq	%rax, 336(%rdi)
	jmp	.LBB44_16
.LBB44_14:
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r14
.LBB44_16:                              # %new_ZNode.exit
	movq	%r13, (%r14)
	movl	$0, 8(%r14)
	movq	$0, 16(%r14)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	set_add_znode
	incl	32(%r13)
	movq	(%r12), %rsi
	cmpl	$0, 16(%rsi)
	je	.LBB44_17
# BB#18:                                # %.lr.ph30.preheader
	xorl	%ebx, %ebx
	movq	8(%rsp), %rdi           # 8-byte Reload
	.p2align	4, 0x90
.LBB44_19:                              # %.lr.ph30
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsi), %rax
	movq	(%rax,%rbx,8), %rcx
	cmpw	$0, (%rcx)
	je	.LBB44_21
# BB#20:                                #   in Loop: Header=BB44_19 Depth=1
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	add_Reduction
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%r12), %rsi
.LBB44_21:                              #   in Loop: Header=BB44_19 Depth=1
	incq	%rbx
	cmpl	16(%rsi), %ebx
	jb	.LBB44_19
	jmp	.LBB44_22
.LBB44_17:
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB44_22:                              # %._crit_edge
	cmpq	$0, 24(%r13)
	jne	.LBB44_41
# BB#23:                                # %.preheader
	cmpl	$0, 32(%rsi)
	je	.LBB44_41
# BB#24:                                # %.lr.ph28
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movabsq	$-1229782938247303441, %r13 # imm = 0xEEEEEEEEEEEEEEEF
	movq	%r12, 16(%rsp)          # 8-byte Spill
	jmp	.LBB44_25
.LBB44_31:                              # %find_SNode.exit.preheader
                                        #   in Loop: Header=BB44_25 Depth=1
	cmpl	$0, 72(%rbx)
	je	.LBB44_39
# BB#32:                                # %.lr.ph26
                                        #   in Loop: Header=BB44_25 Depth=1
	movq	%r10, %rbp
	leaq	8(%rbp,%r8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	addq	%r8, %rbp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB44_33:                              #   Parent Loop BB44_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	80(%rbx), %rax
	movq	(%rax,%r12,8), %rsi
	testq	%rsi, %rsi
	je	.LBB44_36
# BB#34:                                #   in Loop: Header=BB44_33 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rcx
	movq	%rbx, %rdx
	callq	add_Reduction
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rax, %rax
	je	.LBB44_36
# BB#35:                                #   in Loop: Header=BB44_33 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 24(%rax)
	movzwl	(%rbp), %ecx
	movl	%ecx, 32(%rax)
.LBB44_36:                              # %find_SNode.exit
                                        #   in Loop: Header=BB44_33 Depth=2
	incq	%r12
	cmpl	72(%rbx), %r12d
	jb	.LBB44_33
# BB#37:                                # %find_SNode.exit.thread.loopexit
                                        #   in Loop: Header=BB44_25 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	(%r12), %rsi
	jmp	.LBB44_39
	.p2align	4, 0x90
.LBB44_25:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB44_28 Depth 2
                                        #     Child Loop BB44_33 Depth 2
	movq	216(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB44_39
# BB#26:                                #   in Loop: Header=BB44_25 Depth=1
	movq	40(%rsi), %rax
	movq	%r15, %r8
	shlq	$4, %r8
	movq	%rax, %r10
	movzwl	2(%rax,%r8), %ebp
	movq	8(%r12), %rcx
	movq	16(%r12), %r9
	movl	%ebp, %eax
	shll	$12, %eax
	addl	%ecx, %eax
	leal	(%r9,%rax), %eax
	xorl	%edx, %edx
	divl	228(%rdi)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	(%rbx,%rdx,8), %rbx
	testq	%rbx, %rbx
	je	.LBB44_39
# BB#27:                                # %.lr.ph.i2
                                        #   in Loop: Header=BB44_25 Depth=1
	movq	144(%rdi), %rax
	movq	8(%rax), %rax
	.p2align	4, 0x90
.LBB44_28:                              #   Parent Loop BB44_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	imulq	%r13, %rdx
	cmpq	%rbp, %rdx
	jne	.LBB44_38
# BB#29:                                #   in Loop: Header=BB44_28 Depth=2
	cmpq	%rcx, 8(%rbx)
	jne	.LBB44_38
# BB#30:                                #   in Loop: Header=BB44_28 Depth=2
	cmpq	%r9, 16(%rbx)
	je	.LBB44_31
	.p2align	4, 0x90
.LBB44_38:                              #   in Loop: Header=BB44_28 Depth=2
	movq	120(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB44_28
.LBB44_39:                              # %find_SNode.exit.thread
                                        #   in Loop: Header=BB44_25 Depth=1
	incq	%r15
	cmpl	32(%rsi), %r15d
	jb	.LBB44_25
# BB#40:
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB44_41:                              # %set_find_znode.exit.thread4
	movl	8(%r14), %eax
	testq	%rax, %rax
	je	.LBB44_42
# BB#43:                                # %.lr.ph
	movq	16(%r14), %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB44_44:                              # =>This Inner Loop Header: Depth=1
	cmpq	%rbp, (%rdx,%rcx,8)
	je	.LBB44_46
# BB#45:                                #   in Loop: Header=BB44_44 Depth=1
	incq	%rcx
	cmpl	%eax, %ecx
	jb	.LBB44_44
.LBB44_46:                              # %.loopexit
	cmpl	%eax, %ecx
	movl	%eax, %ecx
	jae	.LBB44_47
	jmp	.LBB44_56
.LBB44_42:
	xorl	%ecx, %ecx
.LBB44_47:                              # %.loopexit.thread
	movq	16(%r14), %rdx
	leaq	24(%r14), %rsi
	testq	%rdx, %rdx
	je	.LBB44_48
# BB#49:
	addq	$8, %r14
	cmpq	%rsi, %rdx
	je	.LBB44_50
# BB#52:
	testb	$7, %cl
	jne	.LBB44_51
	jmp	.LBB44_53
.LBB44_48:
	movq	%rsi, 16(%r14)
	leal	1(%rax), %ecx
	movl	%ecx, 8(%r14)
	movq	%rbp, 24(%r14,%rax,8)
	cmpq	%rbp, %r12
	jne	.LBB44_55
	jmp	.LBB44_56
.LBB44_50:
	cmpl	$2, %ecx
	ja	.LBB44_53
.LBB44_51:
	leal	1(%rcx), %eax
	movl	%eax, (%r14)
	movl	%ecx, %eax
	movq	%rbp, (%rdx,%rax,8)
	cmpq	%rbp, %r12
	jne	.LBB44_55
	jmp	.LBB44_56
.LBB44_53:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	vec_add_internal
	cmpq	%rbp, %r12
	je	.LBB44_56
.LBB44_55:
	incl	112(%rbp)
.LBB44_56:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end44:
	.size	goto_PNode, .Lfunc_end44-goto_PNode
	.cfi_endproc

	.p2align	4, 0x90
	.type	new_VecZNode,@function
new_VecZNode:                           # @new_VecZNode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi316:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi317:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi318:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi319:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi320:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi321:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi322:
	.cfi_def_cfa_offset 64
.Lcfi323:
	.cfi_offset %rbx, -56
.Lcfi324:
	.cfi_offset %r12, -48
.Lcfi325:
	.cfi_offset %r13, -40
.Lcfi326:
	.cfi_offset %r14, -32
.Lcfi327:
	.cfi_offset %r15, -24
.Lcfi328:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %r15d
	movq	%rdi, %r14
	cmpl	$0, (%r14)
	je	.LBB45_1
# BB#2:
	movl	$40, %edi
	callq	malloc
	movq	%rax, %rbx
	jmp	.LBB45_3
.LBB45_1:
	movl	$path1, %ebx
.LBB45_3:
	movl	$0, (%rbx)
	movq	$0, 8(%rbx)
	testl	%ebp, %ebp
	js	.LBB45_17
# BB#4:
	testl	%r15d, %r15d
	jle	.LBB45_17
# BB#5:                                 # %.lr.ph
	movslq	%ebp, %r12
	leaq	16(%rbx), %r13
	movl	%r15d, %r15d
	decq	%r15
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	testq	%rax, %rax
	jne	.LBB45_8
	jmp	.LBB45_7
	.p2align	4, 0x90
.LBB45_16:                              # %._crit_edge
	incq	%rbp
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB45_7
.LBB45_8:
	movl	(%rbx), %ecx
	cmpq	%r13, %rax
	je	.LBB45_9
# BB#12:
	testb	$7, %cl
	je	.LBB45_14
# BB#13:
	movq	8(%r14), %rdx
	movq	(%rdx,%r12,8), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rbp,8), %rdx
	leal	1(%rcx), %esi
	movl	%esi, (%rbx)
	movq	%rdx, (%rax,%rcx,8)
	cmpq	%rbp, %r15
	jne	.LBB45_16
	jmp	.LBB45_17
	.p2align	4, 0x90
.LBB45_7:
	movq	8(%r14), %rax
	movq	(%rax,%r12,8), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	%r13, 8(%rbx)
	movl	(%rbx), %ecx
	leal	1(%rcx), %edx
	jmp	.LBB45_11
	.p2align	4, 0x90
.LBB45_9:
	cmpl	$2, %ecx
	ja	.LBB45_14
# BB#10:
	movq	8(%r14), %rax
	movq	(%rax,%r12,8), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movl	%ecx, %edx
	incl	%edx
.LBB45_11:
	movl	%edx, (%rbx)
	movq	%rax, 16(%rbx,%rcx,8)
	cmpq	%rbp, %r15
	jne	.LBB45_16
	jmp	.LBB45_17
	.p2align	4, 0x90
.LBB45_14:
	movq	8(%r14), %rax
	movq	(%rax,%r12,8), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbp,8), %rsi
	movq	%rbx, %rdi
	callq	vec_add_internal
	cmpq	%rbp, %r15
	jne	.LBB45_16
.LBB45_17:                              # %.loopexit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end45:
	.size	new_VecZNode, .Lfunc_end45-new_VecZNode
	.cfi_endproc

	.p2align	4, 0x90
	.type	build_paths_internal,@function
build_paths_internal:                   # @build_paths_internal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi329:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi330:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi331:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi332:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi333:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi334:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi335:
	.cfi_def_cfa_offset 80
.Lcfi336:
	.cfi_offset %rbx, -56
.Lcfi337:
	.cfi_offset %r12, -48
.Lcfi338:
	.cfi_offset %r13, -40
.Lcfi339:
	.cfi_offset %r14, -32
.Lcfi340:
	.cfi_offset %r15, -24
.Lcfi341:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	8(%rbx), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rdi
	movq	8(%rdi), %rax
	leaq	16(%rdi), %rdx
	testq	%rax, %rax
	je	.LBB46_1
# BB#2:
	movl	(%rdi), %ecx
	cmpq	%rdx, %rax
	je	.LBB46_3
# BB#5:
	testb	$7, %cl
	je	.LBB46_8
# BB#6:
	leal	1(%rcx), %edx
	jmp	.LBB46_7
.LBB46_1:
	movq	%rdx, 8(%rdi)
	movq	8(%rbx), %rax
	movq	(%rax,%rcx,8), %rax
	movl	(%rax), %ecx
	leal	1(%rcx), %edx
	movl	%edx, (%rax)
	movq	%r12, 16(%rdi,%rcx,8)
	cmpl	$2, %r14d
	jge	.LBB46_10
	jmp	.LBB46_27
.LBB46_3:
	cmpl	$2, %ecx
	ja	.LBB46_8
# BB#4:
	movl	%ecx, %edx
	incl	%edx
.LBB46_7:
	movl	%edx, (%rdi)
	movq	%r12, (%rax,%rcx,8)
	cmpl	$2, %r14d
	jge	.LBB46_10
	jmp	.LBB46_27
.LBB46_8:
	movq	%r12, %rsi
	callq	vec_add_internal
	cmpl	$2, %r14d
	jl	.LBB46_27
.LBB46_10:                              # %.preheader89
	cmpl	$0, 8(%r12)
	je	.LBB46_27
# BB#11:                                # %.preheader.lr.ph
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	1(%rax), %eax
	subl	%r14d, %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	decl	%r14d
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	leaq	16(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%r12), %rax
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB46_12:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB46_14 Depth 2
	movq	(%rax,%r13,8), %rcx
	cmpl	$0, 72(%rcx)
	je	.LBB46_26
# BB#13:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB46_12 Depth=1
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB46_14:                              # %.lr.ph
                                        #   Parent Loop BB46_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	80(%rcx), %rcx
	cmpq	$0, (%rcx,%r14,8)
	je	.LBB46_25
# BB#15:                                #   in Loop: Header=BB46_14 Depth=2
	movl	%r15d, %ecx
	negl	%ecx
	cmpq	%rcx, %r13
	je	.LBB46_24
# BB#16:                                #   in Loop: Header=BB46_14 Depth=2
	movq	8(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB46_17
# BB#18:                                #   in Loop: Header=BB46_14 Depth=2
	movl	(%rbx), %eax
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	je	.LBB46_19
# BB#21:                                #   in Loop: Header=BB46_14 Depth=2
	testb	$7, %al
	jne	.LBB46_20
	jmp	.LBB46_22
.LBB46_17:                              #   in Loop: Header=BB46_14 Depth=2
	movq	%rbx, %rdi
	movl	(%rsp), %esi            # 4-byte Reload
	movl	%ebp, %edx
	callq	new_VecZNode
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 8(%rbx)
	movl	(%rbx), %ecx
	leal	1(%rcx), %ebp
	movl	%ebp, (%rbx)
	movq	%rax, 16(%rbx,%rcx,8)
	jmp	.LBB46_23
.LBB46_19:                              #   in Loop: Header=BB46_14 Depth=2
	cmpl	$2, %eax
	ja	.LBB46_22
.LBB46_20:                              #   in Loop: Header=BB46_14 Depth=2
	movq	%rbx, %rdi
	movl	(%rsp), %esi            # 4-byte Reload
	movl	%ebp, %edx
	callq	new_VecZNode
	movq	8(%rbx), %rcx
	movl	(%rbx), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, (%rbx)
	movq	%rax, (%rcx,%rdx,8)
	jmp	.LBB46_23
.LBB46_22:                              #   in Loop: Header=BB46_14 Depth=2
	movq	%rbx, %rdi
	movl	(%rsp), %esi            # 4-byte Reload
	movl	%ebp, %edx
	callq	new_VecZNode
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	vec_add_internal
	movl	(%rbx), %ebp
.LBB46_23:                              #   in Loop: Header=BB46_14 Depth=2
	decl	%ebp
	movq	16(%r12), %rax
.LBB46_24:                              #   in Loop: Header=BB46_14 Depth=2
	movq	(%rax,%r13,8), %rax
	movq	80(%rax), %rax
	movq	(%rax,%r14,8), %rdi
	movq	%rbx, %rsi
	movl	%ebp, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	4(%rsp), %r8d           # 4-byte Reload
	callq	build_paths_internal
	incl	%r15d
	movq	16(%r12), %rax
.LBB46_25:                              #   in Loop: Header=BB46_14 Depth=2
	incq	%r14
	movq	(%rax,%r13,8), %rcx
	cmpl	72(%rcx), %r14d
	jb	.LBB46_14
.LBB46_26:                              # %._crit_edge
                                        #   in Loop: Header=BB46_12 Depth=1
	incq	%r13
	cmpl	8(%r12), %r13d
	jb	.LBB46_12
.LBB46_27:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end46:
	.size	build_paths_internal, .Lfunc_end46-build_paths_internal
	.cfi_endproc

	.p2align	4, 0x90
	.type	check_assoc_priority,@function
check_assoc_priority:                   # @check_assoc_priority
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %r8d
	movl	8(%rsi), %ecx
	testb	$24, %r8b
	je	.LBB47_1
# BB#9:
	testl	%ecx, %ecx
	je	.LBB47_16
# BB#10:
	movl	12(%rsi), %r10d
	xorl	%eax, %eax
	cmpl	$9, %ecx
	setne	%al
	orq	$2, %rax
	testb	$20, %cl
	movl	$1, %r9d
	cmoveq	%rax, %r9
	xorl	%eax, %eax
	cmpl	$9, %r8d
	setne	%al
	incq	%rax
	xorl	%edx, %edx
	testb	$20, %r8b
	cmovneq	%rdx, %rax
	cmpl	%r10d, 12(%rdi)
	jg	.LBB47_14
# BB#11:
	jge	.LBB47_13
# BB#12:
	movl	$1, %edx
	jmp	.LBB47_14
.LBB47_1:
	testb	$24, %cl
	je	.LBB47_31
# BB#2:
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.LBB47_31
# BB#3:
	movl	12(%rsi), %r9d
	xorl	%edx, %edx
	cmpl	$9, %ecx
	setne	%dl
	orq	$2, %rdx
	testb	$20, %cl
	movl	$1, %r8d
	cmoveq	%rdx, %r8
	xorl	%esi, %esi
	cmpl	$9, %eax
	setne	%sil
	incq	%rsi
	xorl	%edx, %edx
	testb	$20, %al
	cmovneq	%rdx, %rsi
	cmpl	%r9d, 4(%rdi)
	jg	.LBB47_7
# BB#4:
	jge	.LBB47_6
# BB#5:
	movl	$1, %edx
	jmp	.LBB47_7
.LBB47_16:
	testq	%rdx, %rdx
	je	.LBB47_24
# BB#17:
	movl	8(%rdx), %ecx
	testl	%ecx, %ecx
	je	.LBB47_24
# BB#18:
	movl	12(%rdx), %r10d
	xorl	%eax, %eax
	cmpl	$9, %ecx
	setne	%al
	orq	$2, %rax
	testb	$20, %cl
	movl	$1, %r9d
	cmoveq	%rax, %r9
	xorl	%eax, %eax
	cmpl	$9, %r8d
	setne	%al
	incq	%rax
	xorl	%edx, %edx
	testb	$20, %r8b
	cmovneq	%rdx, %rax
	cmpl	%r10d, 12(%rdi)
	jg	.LBB47_22
# BB#19:
	jge	.LBB47_21
# BB#20:
	movl	$1, %edx
	jmp	.LBB47_22
.LBB47_13:
	movl	%r8d, %edx
	andl	$2, %edx
	shrl	%ecx
	andl	$1, %ecx
	leal	2(%rcx,%rdx), %edx
.LBB47_14:                              # %check_child.exit39
	leaq	(%r9,%r9,8), %rcx
	leaq	(%rax,%rax,2), %rax
	shlq	$3, %rax
	leaq	(%rax,%rcx,8), %rax
	cmpl	$0, child_table(%rax,%rdx,4)
	je	.LBB47_15
.LBB47_24:
	movl	(%rsi), %edx
	testl	%edx, %edx
	je	.LBB47_31
# BB#25:
	movl	12(%rdi), %r9d
	xorl	%ecx, %ecx
	cmpl	$9, %r8d
	setne	%cl
	orq	$2, %rcx
	xorl	%edi, %edi
	testb	$20, %r8b
	cmovneq	%rdi, %rcx
	xorl	%eax, %eax
	cmpl	$9, %edx
	setne	%al
	incq	%rax
	testb	$20, %dl
	cmovneq	%rdi, %rax
	cmpl	%r9d, 4(%rsi)
	jg	.LBB47_29
# BB#26:
	jge	.LBB47_28
# BB#27:
	movl	$1, %edi
	jmp	.LBB47_29
.LBB47_15:
	movl	$-1, %eax
	retq
.LBB47_28:
	andl	$2, %edx
	shrl	%r8d
	andl	$1, %r8d
	leal	2(%r8,%rdx), %edi
.LBB47_29:                              # %check_child.exit31
	leaq	(%rcx,%rcx,8), %rcx
	leaq	(%rax,%rax,2), %rax
	shlq	$3, %rax
	leaq	(%rax,%rcx,8), %rax
	cmpl	$0, child_table(%rax,%rdi,4)
	jne	.LBB47_31
# BB#30:
	movl	$-1, %eax
	retq
.LBB47_6:
	andl	$2, %eax
	shrl	%ecx
	andl	$1, %ecx
	leal	2(%rax,%rcx), %edx
.LBB47_7:                               # %check_child.exit
	leaq	(%r8,%r8,8), %rax
	leaq	(%rsi,%rsi,2), %rcx
	shlq	$3, %rcx
	leaq	(%rcx,%rax,8), %rax
	cmpl	$0, child_table(%rax,%rdx,4)
	je	.LBB47_8
.LBB47_31:
	xorl	%eax, %eax
	retq
.LBB47_8:
	movl	$-1, %eax
	retq
.LBB47_21:
	movl	%r8d, %edx
	andl	$2, %edx
	shrl	%ecx
	andl	$1, %ecx
	leal	2(%rcx,%rdx), %edx
.LBB47_22:                              # %check_child.exit35
	leaq	(%r9,%r9,8), %rcx
	leaq	(%rax,%rax,2), %rax
	shlq	$3, %rax
	leaq	(%rax,%rcx,8), %rax
	cmpl	$0, child_table(%rax,%rdx,4)
	jne	.LBB47_24
# BB#23:
	movl	$-1, %eax
	retq
.Lfunc_end47:
	.size	check_assoc_priority, .Lfunc_end47-check_assoc_priority
	.cfi_endproc

	.p2align	4, 0x90
	.type	final_actionless,@function
final_actionless:                       # @final_actionless
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi342:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi343:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi344:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi345:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi346:
	.cfi_def_cfa_offset 48
.Lcfi347:
	.cfi_offset %rbx, -40
.Lcfi348:
	.cfi_offset %r14, -32
.Lcfi349:
	.cfi_offset %r15, -24
.Lcfi350:
	.cfi_offset %rbp, -16
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB48_2
# BB#1:
	xorl	%r14d, %r14d
	cmpq	$0, 16(%rax)
	jne	.LBB48_7
.LBB48_2:                               # %.preheader
	movl	40(%rdi), %r15d
	movl	$1, %r14d
	testl	%r15d, %r15d
	je	.LBB48_7
# BB#3:                                 # %.lr.ph
	movq	48(%rdi), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB48_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rbp,8), %rdi
	callq	final_actionless
	testl	%eax, %eax
	je	.LBB48_6
# BB#4:                                 #   in Loop: Header=BB48_5 Depth=1
	incq	%rbp
	cmpl	%r15d, %ebp
	jb	.LBB48_5
	jmp	.LBB48_7
.LBB48_6:
	xorl	%r14d, %r14d
.LBB48_7:                               # %.loopexit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end48:
	.size	final_actionless, .Lfunc_end48-final_actionless
	.cfi_endproc

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"[%s]"
	.size	.L.str.4, 5

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"unresolved ambiguity line %d file %s"
	.size	.L.str.6, 37

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"bad pass number: %d\n"
	.size	.L.str.7, 21

	.type	_wspace,@object         # @_wspace
	.data
	.globl	_wspace
	.p2align	4
_wspace:
	.asciz	"\000\000\000\000\000\000\000\000\000\001\000\001\001\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.size	_wspace, 256

	.type	.L.str.8,@object        # @.str.8
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.8:
	.asciz	"internal error: bad final reduction"
	.size	.L.str.8, 36

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%d states %d scans %d shifts %d reductions %d compares %d ambiguities\n"
	.size	.L.str.9, 71

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"syntax error, '%s' line %d\n"
	.size	.L.str.10, 28

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"line"
	.size	.L.str.11, 5

	.type	child_table,@object     # @child_table
	.section	.rodata,"a",@progbits
	.p2align	4
child_table:
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	666                     # 0x29a
	.long	666                     # 0x29a
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	666                     # 0x29a
	.long	666                     # 0x29a
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	666                     # 0x29a
	.long	666                     # 0x29a
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	666                     # 0x29a
	.long	666                     # 0x29a
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	666                     # 0x29a
	.long	0                       # 0x0
	.long	666                     # 0x29a
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	666                     # 0x29a
	.long	666                     # 0x29a
	.long	666                     # 0x29a
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	666                     # 0x29a
	.long	666                     # 0x29a
	.long	1                       # 0x1
	.long	666                     # 0x29a
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	666                     # 0x29a
	.long	0                       # 0x0
	.long	666                     # 0x29a
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	666                     # 0x29a
	.long	1                       # 0x1
	.long	666                     # 0x29a
	.long	666                     # 0x29a
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	666                     # 0x29a
	.long	666                     # 0x29a
	.long	666                     # 0x29a
	.long	1                       # 0x1
	.size	child_table, 288

	.type	path1,@object           # @path1
	.local	path1
	.comm	path1,40,8
	.type	.L.str.12,@object       # @.str.12
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.12:
	.asciz	"circular parse: unable to fixup internal symbol"
	.size	.L.str.12, 48


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
