	.text
	.file	"btDbvtBroadphase.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	10                      # 0xa
	.text
	.globl	_ZN16btDbvtBroadphaseC2EP22btOverlappingPairCache
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphaseC2EP22btOverlappingPairCache,@function
_ZN16btDbvtBroadphaseC2EP22btOverlappingPairCache: # @_ZN16btDbvtBroadphaseC2EP22btOverlappingPairCache
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	$_ZTV16btDbvtBroadphase+16, (%rbx)
	leaq	8(%rbx), %r14
.Ltmp0:
	movq	%r14, %rdi
	callq	_ZN6btDbvtC1Ev
.Ltmp1:
# BB#1:
	leaq	72(%rbx), %r12
.Ltmp3:
	movq	%r12, %rdi
	callq	_ZN6btDbvtC1Ev
.Ltmp4:
# BB#2:
	movb	$0, 221(%rbx)
	movb	$1, 222(%rbx)
	testq	%r15, %r15
	sete	220(%rbx)
	movl	$0, 168(%rbx)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,0,10]
	movups	%xmm0, 172(%rbx)
	movl	$1, %eax
	movd	%eax, %xmm0
	movdqu	%xmm0, 188(%rbx)
	movl	$0, 204(%rbx)
	jne	.LBB0_5
# BB#3:
.Ltmp9:
	movl	$128, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
.Ltmp10:
# BB#4:
.Ltmp11:
	movq	%r15, %rdi
	callq	_ZN28btHashedOverlappingPairCacheC1Ev
.Ltmp12:
.LBB0_5:
	movq	%r15, 160(%rbx)
	movl	$0, 216(%rbx)
	movq	$0, 208(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 136(%rbx)
	movq	$0, 152(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB0_7:
.Ltmp13:
	movq	%rax, %rbx
.Ltmp14:
	movq	%r12, %rdi
	callq	_ZN6btDbvtD1Ev
.Ltmp15:
# BB#8:
.Ltmp16:
	movq	%r14, %rdi
	callq	_ZN6btDbvtD1Ev
.Ltmp17:
	jmp	.LBB0_9
.LBB0_10:                               # %.loopexit
.Ltmp18:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_6:                                # %.preheader
.Ltmp5:
	movq	%rax, %rbx
.Ltmp6:
	movq	%r14, %rdi
	callq	_ZN6btDbvtD1Ev
.Ltmp7:
.LBB0_9:                                # %.loopexit12
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_11:                               # %.loopexit.split-lp
.Ltmp8:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_13:                               # %.thread
.Ltmp2:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZN16btDbvtBroadphaseC2EP22btOverlappingPairCache, .Lfunc_end0-_ZN16btDbvtBroadphaseC2EP22btOverlappingPairCache
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp12-.Ltmp9          #   Call between .Ltmp9 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp17-.Ltmp14         #   Call between .Ltmp14 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin0   #     jumps to .Ltmp18
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Lfunc_end0-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN16btDbvtBroadphaseD2Ev
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphaseD2Ev,@function
_ZN16btDbvtBroadphaseD2Ev:              # @_ZN16btDbvtBroadphaseD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r12, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTV16btDbvtBroadphase+16, (%r15)
	cmpb	$0, 220(%r15)
	je	.LBB2_3
# BB#1:
	movq	160(%r15), %rdi
	movq	(%rdi), %rax
.Ltmp19:
	callq	*(%rax)
.Ltmp20:
# BB#2:
	movq	160(%r15), %rdi
.Ltmp21:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp22:
.LBB2_3:
	leaq	8(%r15), %r12
	leaq	72(%r15), %rbx
.Ltmp29:
	movq	%rbx, %rdi
	callq	_ZN6btDbvtD1Ev
.Ltmp30:
# BB#4:
.Ltmp31:
	movq	%r12, %rbx
	movq	%r12, %rdi
	callq	_ZN6btDbvtD1Ev
.Ltmp32:
# BB#5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB2_10:
.Ltmp23:
	movq	%rax, %r14
	leaq	72(%r15), %rdi
.Ltmp24:
	callq	_ZN6btDbvtD1Ev
.Ltmp25:
# BB#11:
	addq	$8, %r15
.Ltmp26:
	movq	%r15, %rdi
	callq	_ZN6btDbvtD1Ev
.Ltmp27:
	jmp	.LBB2_12
.LBB2_14:                               # %.loopexit.split-lp
.Ltmp28:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_6:
.Ltmp33:
	movq	%rax, %r14
	cmpq	%rbx, %r12
	je	.LBB2_12
# BB#7:                                 # %.preheader.preheader
	addq	$-8, %rbx
	.p2align	4, 0x90
.LBB2_8:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leaq	-56(%rbx), %rdi
.Ltmp34:
	callq	_ZN6btDbvtD1Ev
.Ltmp35:
# BB#9:                                 #   in Loop: Header=BB2_8 Depth=1
	addq	$-64, %rbx
	cmpq	%rbx, %r15
	jne	.LBB2_8
.LBB2_12:                               # %.loopexit3
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_13:                               # %.loopexit
.Ltmp36:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN16btDbvtBroadphaseD2Ev, .Lfunc_end2-_ZN16btDbvtBroadphaseD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp22-.Ltmp19         #   Call between .Ltmp19 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin1   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp32-.Ltmp29         #   Call between .Ltmp29 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin1   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp27-.Ltmp24         #   Call between .Ltmp24 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin1   #     jumps to .Ltmp28
	.byte	1                       #   On action: 1
	.long	.Ltmp34-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin1   #     jumps to .Ltmp36
	.byte	1                       #   On action: 1
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end2-.Ltmp35     #   Call between .Ltmp35 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN16btDbvtBroadphaseD0Ev
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphaseD0Ev,@function
_ZN16btDbvtBroadphaseD0Ev:              # @_ZN16btDbvtBroadphaseD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp37:
	callq	_ZN16btDbvtBroadphaseD2Ev
.Ltmp38:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_2:
.Ltmp39:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN16btDbvtBroadphaseD0Ev, .Lfunc_end3-_ZN16btDbvtBroadphaseD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp37-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin2   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end3-.Ltmp38     #   Call between .Ltmp38 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN16btDbvtBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_,@function
_ZN16btDbvtBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_: # @_ZN16btDbvtBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 128
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	%r9d, %r15d
	movq	%r8, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movl	$96, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movq	%r12, (%rbx)
	movw	%r15w, 8(%rbx)
	movzwl	128(%rsp), %eax
	movw	%ax, 10(%rbx)
	movups	(%rbp), %xmm0
	movups	%xmm0, 28(%rbx)
	movups	(%r13), %xmm0
	movups	%xmm0, 44(%rbx)
	movq	$0, 16(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 72(%rbx)
	movups	(%rbp), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	(%r13), %xmm0
	movaps	%xmm0, 48(%rsp)
	movl	172(%r14), %eax
	movl	%eax, 88(%rbx)
	movl	216(%r14), %eax
	incl	%eax
	movl	%eax, 216(%r14)
	movl	%eax, 24(%rbx)
	leaq	8(%r14), %r15
	leaq	32(%rsp), %rsi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	_ZN6btDbvt6insertERK12btDbvtAabbMmPv
	movq	%rax, 64(%rbx)
	movslq	172(%r14), %rax
	movq	$0, 72(%rbx)
	movq	136(%r14,%rax,8), %rcx
	movq	%rcx, 80(%rbx)
	movq	136(%r14,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.LBB4_2
# BB#1:
	movq	%rbx, 72(%rcx)
.LBB4_2:                                # %_ZL10listappendI11btDbvtProxyEvPT_RS2_.exit
	movq	%rbx, 136(%r14,%rax,8)
	cmpb	$0, 221(%r14)
	jne	.LBB4_5
# BB#3:
	movq	$_ZTV18btDbvtTreeCollider+16, 8(%rsp)
	movq	%r14, 16(%rsp)
	movq	%rbx, 24(%rsp)
	movq	8(%r14), %rsi
.Ltmp40:
	leaq	32(%rsp), %rdx
	leaq	8(%rsp), %rcx
	movq	%r15, %rdi
	callq	_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE
.Ltmp41:
# BB#4:
	movq	72(%r14), %rsi
	addq	$72, %r14
.Ltmp42:
	leaq	32(%rsp), %rdx
	leaq	8(%rsp), %rcx
	movq	%r14, %rdi
	callq	_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE
.Ltmp43:
.LBB4_5:
	movq	%rbx, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_6:
.Ltmp44:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN16btDbvtBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_, .Lfunc_end4-_ZN16btDbvtBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp40-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp43-.Ltmp40         #   Call between .Ltmp40 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin3   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Lfunc_end4-.Ltmp43     #   Call between .Ltmp43 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE,"axG",@progbits,_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE,comdat
	.weak	_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE
	.p2align	4, 0x90
	.type	_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE,@function
_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE: # @_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 144
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	testq	%rbp, %rbp
	je	.LBB5_79
# BB#1:
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
	movss	4(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	movss	8(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 76(%rsp)         # 4-byte Spill
	movss	16(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	movss	20(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movss	24(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
.Ltmp45:
	movl	$512, %edi              # imm = 0x200
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r13
.Ltmp46:
# BB#2:                                 # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_.exit
	movq	%rbp, (%r13)
	movl	$64, %esi
	movl	$1, %ecx
	movl	$1, %r14d
	movq	%r13, %r10
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r13, %r9
	movq	%r13, %r11
	movq	%r13, %r8
	movq	%r13, %rbx
	movss	28(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	jmp	.LBB5_3
.LBB5_60:                               # %vector.body.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	leaq	-4(%rax), %rcx
	movl	%ecx, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB5_63
# BB#61:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	negq	%rdx
	xorl	%esi, %esi
.LBB5_62:                               # %vector.body.prol
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rsi,8), %xmm0
	movups	16(%r12,%rsi,8), %xmm1
	movups	%xmm0, (%r13,%rsi,8)
	movups	%xmm1, 16(%r13,%rsi,8)
	addq	$4, %rsi
	incq	%rdx
	jne	.LBB5_62
	jmp	.LBB5_64
.LBB5_28:                               # %vector.body191.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	leaq	-4(%rax), %rcx
	movl	%ecx, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB5_31
# BB#29:                                # %vector.body191.prol.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	negq	%rdx
	xorl	%esi, %esi
.LBB5_30:                               # %vector.body191.prol
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbx,%rsi,8), %xmm0
	movups	16(%rbx,%rsi,8), %xmm1
	movups	%xmm0, (%r13,%rsi,8)
	movups	%xmm1, 16(%r13,%rsi,8)
	addq	$4, %rsi
	incq	%rdx
	jne	.LBB5_30
	jmp	.LBB5_32
.LBB5_63:                               #   in Loop: Header=BB5_3 Depth=1
	xorl	%esi, %esi
.LBB5_64:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpq	$12, %rcx
	jb	.LBB5_67
# BB#65:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%r12,%rsi,8), %rdx
	leaq	112(%r13,%rsi,8), %rsi
.LBB5_66:                               # %vector.body
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-16, %rcx
	jne	.LBB5_66
.LBB5_67:                               # %middle.block
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpq	%rax, %r15
	je	.LBB5_74
	jmp	.LBB5_68
.LBB5_31:                               #   in Loop: Header=BB5_3 Depth=1
	xorl	%esi, %esi
.LBB5_32:                               # %vector.body191.prol.loopexit
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpq	$12, %rcx
	jb	.LBB5_35
# BB#33:                                # %vector.body191.preheader.new
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%rbx,%rsi,8), %rdx
	leaq	112(%r13,%rsi,8), %rsi
.LBB5_34:                               # %vector.body191
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-16, %rcx
	jne	.LBB5_34
.LBB5_35:                               # %middle.block192
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpq	%rax, 32(%rsp)          # 8-byte Folded Reload
	je	.LBB5_42
	jmp	.LBB5_36
	.p2align	4, 0x90
.LBB5_3:                                # %_Z9IntersectRK12btDbvtAabbMmS1_.exit.thread._crit_edge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_30 Depth 2
                                        #     Child Loop BB5_34 Depth 2
                                        #     Child Loop BB5_38 Depth 2
                                        #     Child Loop BB5_41 Depth 2
                                        #     Child Loop BB5_62 Depth 2
                                        #     Child Loop BB5_66 Depth 2
                                        #     Child Loop BB5_70 Depth 2
                                        #     Child Loop BB5_73 Depth 2
	leal	-1(%r14), %r12d
	movslq	%r14d, %r15
	movq	-8(%rbx,%r15,8), %rbp
	ucomiss	(%rbp), %xmm3
	jb	.LBB5_76
# BB#4:                                 #   in Loop: Header=BB5_3 Depth=1
	movss	16(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jb	.LBB5_76
# BB#5:                                 #   in Loop: Header=BB5_3 Depth=1
	ucomiss	4(%rbp), %xmm4
	jb	.LBB5_76
# BB#6:                                 #   in Loop: Header=BB5_3 Depth=1
	movss	20(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jb	.LBB5_76
# BB#7:                                 #   in Loop: Header=BB5_3 Depth=1
	ucomiss	8(%rbp), %xmm5
	jb	.LBB5_76
# BB#8:                                 # %_Z9IntersectRK12btDbvtAabbMmS1_.exit
                                        #   in Loop: Header=BB5_3 Depth=1
	movss	24(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	76(%rsp), %xmm0         # 4-byte Folded Reload
	jb	.LBB5_76
# BB#9:                                 #   in Loop: Header=BB5_3 Depth=1
	cmpq	$0, 48(%rbp)
	je	.LBB5_13
# BB#10:                                #   in Loop: Header=BB5_3 Depth=1
	leaq	-1(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	40(%rbp), %rdx
	cmpl	%esi, %r12d
	jne	.LBB5_12
# BB#11:                                #   in Loop: Header=BB5_3 Depth=1
	leal	(%rsi,%rsi), %edi
	testl	%esi, %esi
	cmovel	%ecx, %edi
	cmpl	%edi, %r14d
	jle	.LBB5_15
.LBB5_12:                               #   in Loop: Header=BB5_3 Depth=1
	movl	%esi, %edi
	movq	%rbx, %r12
	jmp	.LBB5_43
.LBB5_13:                               #   in Loop: Header=BB5_3 Depth=1
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%r8, %r14
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r11, %r13
	movq	%r10, 40(%rsp)          # 8-byte Spill
	movq	%r9, %r15
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp58:
	movq	%rbp, %rsi
	callq	*24(%rax)
.Ltmp59:
# BB#14:                                #   in Loop: Header=BB5_3 Depth=1
	movss	28(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movl	$1, %ecx
	movq	%r15, %r9
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	%r13, %r11
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	%r14, %r8
	movq	56(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB5_76
.LBB5_15:                               #   in Loop: Header=BB5_3 Depth=1
	testl	%edi, %edi
	movq	%r10, 40(%rsp)          # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	je	.LBB5_17
# BB#16:                                #   in Loop: Header=BB5_3 Depth=1
	movl	%edi, %r13d
	movslq	%r13d, %rdi
	shlq	$3, %rdi
.Ltmp48:
	movl	$16, %esi
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%r11, 64(%rsp)          # 8-byte Spill
	callq	_Z22btAlignedAllocInternalmi
	movq	64(%rsp), %r11          # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	movl	%r13d, %edi
	movl	$1, %ecx
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movq	%rax, %r13
.Ltmp49:
	jmp	.LBB5_18
.LBB5_17:                               #   in Loop: Header=BB5_3 Depth=1
	xorl	%r13d, %r13d
.LBB5_18:                               # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi.exit.i.i30
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpl	$2, %r14d
	jl	.LBB5_21
# BB#19:                                # %.lr.ph.i.i.i32.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpl	$4, %r12d
	jae	.LBB5_23
# BB#20:                                #   in Loop: Header=BB5_3 Depth=1
	xorl	%eax, %eax
	jmp	.LBB5_36
.LBB5_21:                               # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_.exit.i.i37
                                        #   in Loop: Header=BB5_3 Depth=1
	testq	%rbx, %rbx
	jne	.LBB5_42
# BB#22:                                #   in Loop: Header=BB5_3 Depth=1
	movq	%r13, %r10
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r13, %r9
	movq	%r13, %r11
	movq	%r13, %r8
	movq	%r13, %r12
	movq	56(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB5_43
.LBB5_23:                               # %min.iters.checked195
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	andq	$-4, %rax
	je	.LBB5_27
# BB#24:                                # %vector.memcheck208
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rbx,%rcx,8), %rcx
	cmpq	%rcx, %r13
	jae	.LBB5_28
# BB#25:                                # %vector.memcheck208
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%r13,%rcx,8), %rcx
	cmpq	%rcx, %r11
	jae	.LBB5_28
.LBB5_27:                               #   in Loop: Header=BB5_3 Depth=1
	xorl	%eax, %eax
.LBB5_36:                               # %.lr.ph.i.i.i32.preheader220
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %edx
	subl	%eax, %edx
	decq	%rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB5_39
# BB#37:                                # %.lr.ph.i.i.i32.prol.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB5_38:                               # %.lr.ph.i.i.i32.prol
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rax,8), %rsi
	movq	%rsi, (%r13,%rax,8)
	incq	%rax
	incq	%rdx
	jne	.LBB5_38
.LBB5_39:                               # %.lr.ph.i.i.i32.prol.loopexit
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpq	$7, %rcx
	jb	.LBB5_42
# BB#40:                                # %.lr.ph.i.i.i32.preheader220.new
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rax, %rcx
	leaq	56(%rbx,%rax,8), %rdx
	leaq	56(%r13,%rax,8), %rax
	.p2align	4, 0x90
.LBB5_41:                               # %.lr.ph.i.i.i32
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-56(%rdx), %rsi
	movq	%rsi, -56(%rax)
	movq	-48(%rdx), %rsi
	movq	%rsi, -48(%rax)
	movq	-40(%rdx), %rsi
	movq	%rsi, -40(%rax)
	movq	-32(%rdx), %rsi
	movq	%rsi, -32(%rax)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rax)
	movq	-16(%rdx), %rsi
	movq	%rsi, -16(%rax)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rax)
	movq	(%rdx), %rsi
	movq	%rsi, (%rax)
	addq	$64, %rdx
	addq	$64, %rax
	addq	$-8, %rcx
	jne	.LBB5_41
.LBB5_42:                               # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_.exit.i.i37.thread
                                        #   in Loop: Header=BB5_3 Depth=1
.Ltmp50:
	movl	%edi, 64(%rsp)          # 4-byte Spill
	movq	%r9, %rdi
	callq	_Z21btAlignedFreeInternalPv
	movl	64(%rsp), %edi          # 4-byte Reload
	movl	$1, %ecx
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
.Ltmp51:
	movq	%r13, %r10
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r13, %r9
	movq	%r13, %r11
	movq	%r13, %r8
	movq	%r13, %r12
	movq	56(%rsp), %rdx          # 8-byte Reload
.LBB5_43:                               #   in Loop: Header=BB5_3 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rdx, (%r12,%rax,8)
	movq	48(%rbp), %rdx
	cmpl	%edi, %r14d
	jne	.LBB5_48
# BB#44:                                #   in Loop: Header=BB5_3 Depth=1
	leal	(%r14,%r14), %edi
	testl	%r14d, %r14d
	cmovel	%ecx, %edi
	cmpl	%edi, %r14d
	jge	.LBB5_49
# BB#45:                                #   in Loop: Header=BB5_3 Depth=1
	testl	%edi, %edi
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	je	.LBB5_50
# BB#46:                                #   in Loop: Header=BB5_3 Depth=1
	movq	%r10, %rbp
	movl	%edi, %ebx
	movslq	%ebx, %rdi
	shlq	$3, %rdi
.Ltmp53:
	movl	$16, %esi
	movq	%r8, %r13
	callq	_Z22btAlignedAllocInternalmi
	movq	%r13, %r8
	movl	%ebx, %edi
	movl	$1, %ecx
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movq	%rax, %r13
.Ltmp54:
# BB#47:                                #   in Loop: Header=BB5_3 Depth=1
	movq	%rbp, %r10
	testl	%r14d, %r14d
	jg	.LBB5_51
	jmp	.LBB5_53
.LBB5_48:                               #   in Loop: Header=BB5_3 Depth=1
	movq	%r12, %rbx
	jmp	.LBB5_75
.LBB5_49:                               #   in Loop: Header=BB5_3 Depth=1
	movl	%r14d, %edi
	movq	%r12, %rbx
	jmp	.LBB5_75
.LBB5_50:                               #   in Loop: Header=BB5_3 Depth=1
	xorl	%r13d, %r13d
	testl	%r14d, %r14d
	jle	.LBB5_53
.LBB5_51:                               # %.lr.ph.i.i.i50
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpl	$4, %r14d
	jae	.LBB5_55
# BB#52:                                #   in Loop: Header=BB5_3 Depth=1
	xorl	%eax, %eax
	jmp	.LBB5_68
.LBB5_53:                               # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_.exit.i.i55
                                        #   in Loop: Header=BB5_3 Depth=1
	testq	%r12, %r12
	jne	.LBB5_74
# BB#54:                                #   in Loop: Header=BB5_3 Depth=1
	movq	%r13, %r10
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r13, %r9
	movq	%r13, %r11
	movq	%r13, %r8
	movq	%r13, %rbx
	movq	40(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB5_75
.LBB5_55:                               # %min.iters.checked
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	%r15, %rax
	andq	$-4, %rax
	je	.LBB5_59
# BB#56:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_3 Depth=1
	leaq	(%r12,%r15,8), %rcx
	cmpq	%rcx, %r13
	jae	.LBB5_60
# BB#57:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_3 Depth=1
	leaq	(%r13,%r15,8), %rcx
	cmpq	%rcx, %r8
	jae	.LBB5_60
.LBB5_59:                               #   in Loop: Header=BB5_3 Depth=1
	xorl	%eax, %eax
.LBB5_68:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	movl	%r14d, %ecx
	subl	%eax, %ecx
	subq	%rax, 32(%rsp)          # 8-byte Folded Spill
	andq	$7, %rcx
	je	.LBB5_71
# BB#69:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	negq	%rcx
	.p2align	4, 0x90
.LBB5_70:                               # %scalar.ph.prol
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rax,8), %rdx
	movq	%rdx, (%r13,%rax,8)
	incq	%rax
	incq	%rcx
	jne	.LBB5_70
.LBB5_71:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpq	$7, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB5_74
# BB#72:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	%r15, %rcx
	subq	%rax, %rcx
	leaq	56(%r12,%rax,8), %rdx
	leaq	56(%r13,%rax,8), %rax
	.p2align	4, 0x90
.LBB5_73:                               # %scalar.ph
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-56(%rdx), %rsi
	movq	%rsi, -56(%rax)
	movq	-48(%rdx), %rsi
	movq	%rsi, -48(%rax)
	movq	-40(%rdx), %rsi
	movq	%rsi, -40(%rax)
	movq	-32(%rdx), %rsi
	movq	%rsi, -32(%rax)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rax)
	movq	-16(%rdx), %rsi
	movq	%rsi, -16(%rax)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rax)
	movq	(%rdx), %rsi
	movq	%rsi, (%rax)
	addq	$64, %rdx
	addq	$64, %rax
	addq	$-8, %rcx
	jne	.LBB5_73
.LBB5_74:                               # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_.exit.i.i55.thread
                                        #   in Loop: Header=BB5_3 Depth=1
.Ltmp55:
	movq	%r10, %rbp
	movl	%edi, %ebx
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
	movl	%ebx, %edi
	movl	$1, %ecx
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
.Ltmp56:
	movq	%r13, %r10
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r13, %r9
	movq	%r13, %r11
	movq	%r13, %r8
	movq	%r13, %rbx
	movq	40(%rsp), %rdx          # 8-byte Reload
.LBB5_75:                               #   in Loop: Header=BB5_3 Depth=1
	movq	%rdx, (%rbx,%r15,8)
	incl	%r14d
	movl	%r14d, %r12d
	movl	%edi, %esi
	.p2align	4, 0x90
.LBB5_76:                               # %_Z9IntersectRK12btDbvtAabbMmS1_.exit.thread
                                        #   in Loop: Header=BB5_3 Depth=1
	testl	%r12d, %r12d
	movl	%r12d, %r14d
	jg	.LBB5_3
# BB#77:
	testq	%rbx, %rbx
	je	.LBB5_79
# BB#78:
	movq	%r13, %rdi
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB5_79:                               # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev.exit27
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_80:
.Ltmp52:
	jmp	.LBB5_83
.LBB5_81:
.Ltmp57:
	movq	%rax, %r14
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%r12, %rbx
	testq	%rbx, %rbx
	jne	.LBB5_84
	jmp	.LBB5_85
.LBB5_82:
.Ltmp60:
.LBB5_83:
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB5_85
.LBB5_84:
.Ltmp61:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
.Ltmp62:
.LBB5_85:                               # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_86:
.Ltmp63:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_87:                               # %.thread
.Ltmp47:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE, .Lfunc_end5-_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp45-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin4   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin4   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp51-.Ltmp48         #   Call between .Ltmp48 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin4   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp56-.Ltmp53         #   Call between .Ltmp53 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin4   #     jumps to .Ltmp57
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Ltmp61-.Ltmp56         #   Call between .Ltmp56 and .Ltmp61
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin4   # >> Call Site 6 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin4   #     jumps to .Ltmp63
	.byte	1                       #   On action: 1
	.long	.Ltmp62-.Lfunc_begin4   # >> Call Site 7 <<
	.long	.Lfunc_end5-.Ltmp62     #   Call between .Ltmp62 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN16btDbvtBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher,@function
_ZN16btDbvtBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher: # @_ZN16btDbvtBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -32
.Lcfi53:
	.cfi_offset %r14, -24
.Lcfi54:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cmpl	$2, 88(%rbx)
	jne	.LBB6_2
# BB#1:
	leaq	72(%r14), %rdi
	jmp	.LBB6_3
.LBB6_2:
	leaq	8(%r14), %rdi
.LBB6_3:
	movq	64(%rbx), %rsi
	callq	_ZN6btDbvt6removeEP10btDbvtNode
	movslq	88(%rbx), %rax
	leaq	136(%r14,%rax,8), %rax
	movq	72(%rbx), %rcx
	movq	80(%rbx), %rdx
	testq	%rcx, %rcx
	leaq	80(%rcx), %rcx
	cmoveq	%rax, %rcx
	movq	%rdx, (%rcx)
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.LBB6_5
# BB#4:
	movq	72(%rbx), %rcx
	movq	%rcx, 72(%rax)
.LBB6_5:                                # %_ZL10listremoveI11btDbvtProxyEvPT_RS2_.exit
	movq	160(%r14), %rdi
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	*32(%rax)
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
	movb	$1, 222(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	_ZN16btDbvtBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher, .Lfunc_end6-_ZN16btDbvtBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.cfi_endproc

	.globl	_ZNK16btDbvtBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_
	.p2align	4, 0x90
	.type	_ZNK16btDbvtBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_,@function
_ZNK16btDbvtBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_: # @_ZNK16btDbvtBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_
	.cfi_startproc
# BB#0:
	movups	28(%rsi), %xmm0
	movups	%xmm0, (%rdx)
	movups	44(%rsi), %xmm0
	movups	%xmm0, (%rcx)
	retq
.Lfunc_end7:
	.size	_ZNK16btDbvtBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_, .Lfunc_end7-_ZNK16btDbvtBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_
	.cfi_endproc

	.globl	_ZN16btDbvtBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_,@function
_ZN16btDbvtBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_: # @_ZN16btDbvtBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi61:
	.cfi_def_cfa_offset 96
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movq	%r9, %rax
	movq	%r8, %r10
	movq	%rcx, %rbx
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	$_ZTV19BroadphaseRayTester+16, 24(%rsp)
	movq	%rbx, 32(%rsp)
	leaq	8(%rbp), %rdi
	movq	8(%rbp), %rsi
	leaq	8(%rbx), %r14
	leaq	24(%rbx), %r15
	movss	36(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
.Ltmp64:
.Lcfi68:
	.cfi_escape 0x2e, 0x20
	subq	$8, %rsp
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	leaq	32(%rsp), %r11
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r14, %r8
	movq	%r15, %r9
	pushq	%r11
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	movq	%rax, 24(%rsp)          # 8-byte Spill
	pushq	%rax
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	movq	%r10, 40(%rsp)          # 8-byte Spill
	pushq	%r10
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	callq	_ZNK6btDbvt15rayTestInternalEPK10btDbvtNodeRK9btVector3S5_S5_PjfS5_S5_RNS_8ICollideE
	addq	$32, %rsp
.Lcfi73:
	.cfi_adjust_cfa_offset -32
.Ltmp65:
# BB#1:
	movq	72(%rbp), %rsi
	addq	$72, %rbp
	movss	36(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
.Ltmp66:
.Lcfi74:
	.cfi_escape 0x2e, 0x20
	subq	$8, %rsp
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	movq	%rbp, %rdi
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r14, %r8
	movq	%r15, %r9
	leaq	32(%rsp), %rax
	pushq	%rax
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	callq	_ZNK6btDbvt15rayTestInternalEPK10btDbvtNodeRK9btVector3S5_S5_PjfS5_S5_RNS_8ICollideE
	addq	$32, %rsp
.Lcfi79:
	.cfi_adjust_cfa_offset -32
.Ltmp67:
# BB#2:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_3:
.Ltmp68:
.Lcfi80:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN16btDbvtBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_, .Lfunc_end8-_ZN16btDbvtBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp64-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp67-.Ltmp64         #   Call between .Ltmp64 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin5   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end8-.Ltmp67     #   Call between .Ltmp67 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK6btDbvt15rayTestInternalEPK10btDbvtNodeRK9btVector3S5_S5_PjfS5_S5_RNS_8ICollideE,"axG",@progbits,_ZNK6btDbvt15rayTestInternalEPK10btDbvtNodeRK9btVector3S5_S5_PjfS5_S5_RNS_8ICollideE,comdat
	.weak	_ZNK6btDbvt15rayTestInternalEPK10btDbvtNodeRK9btVector3S5_S5_PjfS5_S5_RNS_8ICollideE
	.p2align	4, 0x90
	.type	_ZNK6btDbvt15rayTestInternalEPK10btDbvtNodeRK9btVector3S5_S5_PjfS5_S5_RNS_8ICollideE,@function
_ZNK6btDbvt15rayTestInternalEPK10btDbvtNodeRK9btVector3S5_S5_PjfS5_S5_RNS_8ICollideE: # @_ZNK6btDbvt15rayTestInternalEPK10btDbvtNodeRK9btVector3S5_S5_PjfS5_S5_RNS_8ICollideE
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi85:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi87:
	.cfi_def_cfa_offset 176
.Lcfi88:
	.cfi_offset %rbx, -56
.Lcfi89:
	.cfi_offset %r12, -48
.Lcfi90:
	.cfi_offset %r13, -40
.Lcfi91:
	.cfi_offset %r14, -32
.Lcfi92:
	.cfi_offset %r15, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%rsi, %rbp
	testq	%rbp, %rbp
	je	.LBB9_52
# BB#1:
.Ltmp69:
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%r8, 72(%rsp)           # 8-byte Spill
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movl	$1024, %edi             # imm = 0x400
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp70:
# BB#2:                                 # %.lr.ph.i
	movq	184(%rsp), %r12
	movq	176(%rsp), %r13
	movq	%rbx, %rdi
	addq	$8, %rdi
	xorl	%esi, %esi
	movl	$1016, %edx             # imm = 0x3F8
	callq	memset
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rbp, (%rbx)
	movl	$1, %r12d
	movl	$126, %r15d
	movl	$128, %r9d
	xorps	%xmm7, %xmm7
	movl	$128, 8(%rsp)           # 4-byte Folded Spill
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %r8
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movq	72(%rsp), %r10          # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%r14, 112(%rsp)         # 8-byte Spill
	jmp	.LBB9_3
.LBB9_30:                               # %vector.body.preheader
                                        #   in Loop: Header=BB9_3 Depth=1
	leaq	-4(%rcx), %rdx
	movl	%edx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB9_33
# BB#31:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB9_3 Depth=1
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB9_32:                               # %vector.body.prol
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbx,%rdi,8), %xmm0
	movups	16(%rbx,%rdi,8), %xmm1
	movups	%xmm0, (%r8,%rdi,8)
	movups	%xmm1, 16(%r8,%rdi,8)
	addq	$4, %rdi
	incq	%rsi
	jne	.LBB9_32
	jmp	.LBB9_34
.LBB9_33:                               #   in Loop: Header=BB9_3 Depth=1
	xorl	%edi, %edi
.LBB9_34:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB9_3 Depth=1
	cmpq	$12, %rdx
	jb	.LBB9_37
# BB#35:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%rbx,%rdi,8), %rsi
	leaq	112(%r8,%rdi,8), %rdi
.LBB9_36:                               # %vector.body
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-16, %rdx
	jne	.LBB9_36
.LBB9_37:                               # %middle.block
                                        #   in Loop: Header=BB9_3 Depth=1
	cmpq	%rcx, %rax
	je	.LBB9_44
	jmp	.LBB9_38
	.p2align	4, 0x90
.LBB9_3:                                # %_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff.exit.thread._crit_edge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_32 Depth 2
                                        #     Child Loop BB9_36 Depth 2
                                        #     Child Loop BB9_40 Depth 2
                                        #     Child Loop BB9_43 Depth 2
	movslq	%r12d, %r13
	decl	%r12d
	movq	-8(%r8,%r13,8), %rbp
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rdi), %xmm1           # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movss	8(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	8(%rdi), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 80(%rsp)
	movlps	%xmm2, 88(%rsp)
	movsd	16(%rbp), %xmm0         # xmm0 = mem[0],zero
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movss	24(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	8(%rsi), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 96(%rsp)
	movlps	%xmm2, 104(%rsp)
	movl	(%r14), %ecx
	movl	4(%r14), %eax
	movl	$1, %edx
	subl	%ecx, %edx
	shlq	$4, %rcx
	shlq	$4, %rdx
	movss	80(%rsp,%rdx), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movsd	(%rbx), %xmm2           # xmm2 = mem[0],zero
	subss	%xmm2, %xmm3
	movsd	(%r10), %xmm4           # xmm4 = mem[0],zero
	mulss	%xmm4, %xmm3
	movq	%rax, %rdx
	shlq	$4, %rdx
	movss	84(%rsp,%rdx), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movss	80(%rsp,%rcx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	subps	%xmm2, %xmm1
	mulps	%xmm4, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	ucomiss	%xmm3, %xmm0
	ja	.LBB9_49
# BB#4:                                 # %_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff.exit.thread._crit_edge
                                        #   in Loop: Header=BB9_3 Depth=1
	movl	$1, %ecx
	subl	%eax, %ecx
	shlq	$4, %rcx
	movss	84(%rsp,%rcx), %xmm5    # xmm5 = mem[0],zero,zero,zero
	pshufd	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	subss	%xmm2, %xmm5
	pshufd	$229, %xmm4, %xmm2      # xmm2 = xmm4[1,1,2,3]
	mulss	%xmm5, %xmm2
	ucomiss	%xmm2, %xmm1
	ja	.LBB9_49
# BB#5:                                 #   in Loop: Header=BB9_3 Depth=1
	minss	%xmm3, %xmm2
	movl	8(%r14), %eax
	movq	%rax, %rcx
	shlq	$4, %rcx
	movss	88(%rsp,%rcx), %xmm4    # xmm4 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm4
	movss	8(%r10), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	ucomiss	%xmm2, %xmm4
	ja	.LBB9_49
# BB#6:                                 #   in Loop: Header=BB9_3 Depth=1
	maxss	%xmm1, %xmm0
	movl	$1, %ecx
	subl	%eax, %ecx
	shlq	$4, %rcx
	movss	88(%rsp,%rcx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm1
	mulss	%xmm1, %xmm3
	ucomiss	%xmm3, %xmm0
	ja	.LBB9_49
# BB#7:                                 # %_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	maxss	%xmm0, %xmm4
	ucomiss	%xmm4, %xmm6
	jbe	.LBB9_49
# BB#8:                                 # %_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	minss	%xmm2, %xmm3
	ucomiss	%xmm7, %xmm3
	jbe	.LBB9_49
# BB#9:                                 #   in Loop: Header=BB9_3 Depth=1
	cmpq	$0, 48(%rbp)
	je	.LBB9_18
# BB#10:                                #   in Loop: Header=BB9_3 Depth=1
	cmpl	%r15d, %r12d
	jle	.LBB9_20
# BB#11:                                #   in Loop: Header=BB9_3 Depth=1
	leal	(%r9,%r9), %r11d
	cmpl	%r11d, %r9d
	jge	.LBB9_47
# BB#12:                                #   in Loop: Header=BB9_3 Depth=1
	cmpl	%r11d, 8(%rsp)          # 4-byte Folded Reload
	movq	%r11, 40(%rsp)          # 8-byte Spill
	jge	.LBB9_21
# BB#13:                                #   in Loop: Header=BB9_3 Depth=1
	testl	%r9d, %r9d
	movq	%r8, 24(%rsp)           # 8-byte Spill
	je	.LBB9_22
# BB#14:                                #   in Loop: Header=BB9_3 Depth=1
	movslq	%r11d, %rdi
	shlq	$3, %rdi
.Ltmp72:
	movl	$16, %esi
	movq	%r9, %rbx
	callq	_Z22btAlignedAllocInternalmi
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	%rbx, %r9
	movq	%rax, 16(%rsp)          # 8-byte Spill
.Ltmp73:
# BB#15:                                # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi.exit.i.i
                                        #   in Loop: Header=BB9_3 Depth=1
	testl	%r9d, %r9d
	jle	.LBB9_23
# BB#16:                                # %.lr.ph.i.i.i65
                                        #   in Loop: Header=BB9_3 Depth=1
	movslq	%r9d, %rax
	cmpl	$4, %r9d
	movq	16(%rsp), %r8           # 8-byte Reload
	jae	.LBB9_25
# BB#17:                                #   in Loop: Header=BB9_3 Depth=1
	xorl	%ecx, %ecx
	movq	24(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB9_38
.LBB9_18:                               #   in Loop: Header=BB9_3 Depth=1
	movl	%r15d, %r14d
	movq	%r9, %r15
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	192(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp77:
	movq	%rbp, %rsi
	callq	*24(%rax)
.Ltmp78:
# BB#19:                                #   in Loop: Header=BB9_3 Depth=1
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movq	%rbx, %rsi
	movq	72(%rsp), %r10          # 8-byte Reload
	movq	%r13, %rdi
	xorps	%xmm7, %xmm7
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%r15, %r9
	movl	%r14d, %r15d
	movq	112(%rsp), %r14         # 8-byte Reload
	testl	%r12d, %r12d
	jne	.LBB9_3
	jmp	.LBB9_50
.LBB9_20:                               #   in Loop: Header=BB9_3 Depth=1
	movl	%r9d, %r11d
	jmp	.LBB9_48
.LBB9_21:                               #   in Loop: Header=BB9_3 Depth=1
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	%r8, %rbx
	jmp	.LBB9_46
.LBB9_22:                               #   in Loop: Header=BB9_3 Depth=1
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	jmp	.LBB9_24
.LBB9_23:                               #   in Loop: Header=BB9_3 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rbx
	movq	24(%rsp), %r8           # 8-byte Reload
.LBB9_24:                               # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_.exit.i.i70
                                        #   in Loop: Header=BB9_3 Depth=1
	testq	%r8, %r8
	movl	%r11d, %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movq	%rax, %r15
	movq	%rax, %r12
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jne	.LBB9_45
	jmp	.LBB9_46
.LBB9_25:                               # %min.iters.checked
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	%rax, %rcx
	andq	$-4, %rcx
	movq	24(%rsp), %rbx          # 8-byte Reload
	je	.LBB9_29
# BB#26:                                # %vector.memcheck
                                        #   in Loop: Header=BB9_3 Depth=1
	leaq	(%rbx,%rax,8), %rdx
	cmpq	%rdx, %r8
	jae	.LBB9_30
# BB#27:                                # %vector.memcheck
                                        #   in Loop: Header=BB9_3 Depth=1
	leaq	(%r8,%rax,8), %rdx
	cmpq	%rdx, 32(%rsp)          # 8-byte Folded Reload
	jae	.LBB9_30
.LBB9_29:                               #   in Loop: Header=BB9_3 Depth=1
	xorl	%ecx, %ecx
.LBB9_38:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB9_3 Depth=1
	movl	%r9d, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB9_41
# BB#39:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB9_3 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB9_40:                               # %scalar.ph.prol
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rcx,8), %rdi
	movq	%rdi, (%r8,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB9_40
.LBB9_41:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB9_3 Depth=1
	cmpq	$7, %rdx
	jb	.LBB9_44
# BB#42:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB9_3 Depth=1
	subq	%rcx, %rax
	leaq	56(%rbx,%rcx,8), %rdx
	leaq	56(%r8,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB9_43:                               # %scalar.ph
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-56(%rdx), %rsi
	movq	%rsi, -56(%rcx)
	movq	-48(%rdx), %rsi
	movq	%rsi, -48(%rcx)
	movq	-40(%rdx), %rsi
	movq	%rsi, -40(%rcx)
	movq	-32(%rdx), %rsi
	movq	%rsi, -32(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	movq	-16(%rdx), %rsi
	movq	%rsi, -16(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	movq	(%rdx), %rsi
	movq	%rsi, (%rcx)
	addq	$64, %rdx
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB9_43
.LBB9_44:                               #   in Loop: Header=BB9_3 Depth=1
	movq	%r8, %rbx
.LBB9_45:                               # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_.exit.i.i70.thread
                                        #   in Loop: Header=BB9_3 Depth=1
.Ltmp74:
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %r12
	movq	%r9, %rbx
	callq	_Z21btAlignedFreeInternalPv
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	%rbx, %r9
	movq	%r12, %rbx
.Ltmp75:
	movl	%r11d, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %r15
	movq	%rax, %r12
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB9_46:                               # %.lr.ph.i72
                                        #   in Loop: Header=BB9_3 Depth=1
	movslq	%r9d, %rax
	movslq	%r11d, %rdx
	leaq	(%rbx,%rax,8), %rdi
	subq	%rax, %rdx
	shlq	$3, %rdx
	xorl	%esi, %esi
	callq	memset
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%rbx, %r8
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movq	184(%rsp), %rsi
	movq	176(%rsp), %rdi
	xorps	%xmm7, %xmm7
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	72(%rsp), %r10          # 8-byte Reload
.LBB9_47:                               # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeE6resizeEiRKS2_.exit79
                                        #   in Loop: Header=BB9_3 Depth=1
	leal	-2(%r11), %r15d
.LBB9_48:                               #   in Loop: Header=BB9_3 Depth=1
	leaq	-1(%r13), %rax
	movq	40(%rbp), %rcx
	movq	%rcx, (%r8,%rax,8)
	movq	48(%rbp), %rax
	leal	1(%r13), %r12d
	movq	%rax, (%r8,%r13,8)
	movl	%r11d, %r9d
	.p2align	4, 0x90
.LBB9_49:                               # %_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff.exit.thread
                                        #   in Loop: Header=BB9_3 Depth=1
	testl	%r12d, %r12d
	jne	.LBB9_3
.LBB9_50:
	testq	%r8, %r8
	je	.LBB9_52
# BB#51:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
.LBB9_52:                               # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev.exit62
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_53:
.Ltmp76:
	jmp	.LBB9_56
.LBB9_54:                               # %.thread
.Ltmp71:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB9_55:
.Ltmp79:
.LBB9_56:
	movq	%rax, %rbx
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB9_58
# BB#57:
.Ltmp80:
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
.Ltmp81:
.LBB9_58:                               # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB9_59:
.Ltmp82:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZNK6btDbvt15rayTestInternalEPK10btDbvtNodeRK9btVector3S5_S5_PjfS5_S5_RNS_8ICollideE, .Lfunc_end9-_ZNK6btDbvt15rayTestInternalEPK10btDbvtNodeRK9btVector3S5_S5_PjfS5_S5_RNS_8ICollideE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp69-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin6   #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp72-.Ltmp70         #   Call between .Ltmp70 and .Ltmp72
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp76-.Lfunc_begin6   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin6   # >> Call Site 4 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin6   #     jumps to .Ltmp79
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin6   # >> Call Site 5 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin6   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin6   # >> Call Site 6 <<
	.long	.Ltmp80-.Ltmp75         #   Call between .Ltmp75 and .Ltmp80
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp80-.Lfunc_begin6   # >> Call Site 7 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp82-.Lfunc_begin6   #     jumps to .Ltmp82
	.byte	1                       #   On action: 1
	.long	.Ltmp81-.Lfunc_begin6   # >> Call Site 8 <<
	.long	.Lfunc_end9-.Ltmp81     #   Call between .Ltmp81 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN6btDbvt8ICollideD2Ev,"axG",@progbits,_ZN6btDbvt8ICollideD2Ev,comdat
	.weak	_ZN6btDbvt8ICollideD2Ev
	.p2align	4, 0x90
	.type	_ZN6btDbvt8ICollideD2Ev,@function
_ZN6btDbvt8ICollideD2Ev:                # @_ZN6btDbvt8ICollideD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end10:
	.size	_ZN6btDbvt8ICollideD2Ev, .Lfunc_end10-_ZN6btDbvt8ICollideD2Ev
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
.LCPI11_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_1:
	.long	1056964608              # float 0.5
.LCPI11_3:
	.long	1028443341              # float 0.0500000007
	.text
	.globl	_ZN16btDbvtBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher,@function
_ZN16btDbvtBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher: # @_ZN16btDbvtBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi96:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi97:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 48
	subq	$64, %rsp
.Lcfi99:
	.cfi_def_cfa_offset 112
.Lcfi100:
	.cfi_offset %rbx, -48
.Lcfi101:
	.cfi_offset %r12, -40
.Lcfi102:
	.cfi_offset %r13, -32
.Lcfi103:
	.cfi_offset %r14, -24
.Lcfi104:
	.cfi_offset %r15, -16
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movups	(%r12), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	(%r15), %xmm0
	movaps	%xmm0, 48(%rsp)
	cmpl	$2, 88(%rbx)
	jne	.LBB11_2
# BB#1:
	leaq	8(%r14), %r13
	leaq	72(%r14), %rdi
	movq	64(%rbx), %rsi
	callq	_ZN6btDbvt6removeEP10btDbvtNode
	leaq	32(%rsp), %rsi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	_ZN6btDbvt6insertERK12btDbvtAabbMmPv
	movq	%rax, 64(%rbx)
	jmp	.LBB11_18
.LBB11_2:
	incl	196(%r14)
	movq	64(%rbx), %rsi
	movss	48(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rsi), %xmm0
	jb	.LBB11_16
# BB#3:
	movss	16(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	32(%rsp), %xmm0
	jb	.LBB11_16
# BB#4:
	movss	52(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%rsi), %xmm0
	jb	.LBB11_16
# BB#5:
	movss	20(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	36(%rsp), %xmm0
	jb	.LBB11_16
# BB#6:
	movss	56(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%rsi), %xmm0
	jb	.LBB11_16
# BB#7:                                 # %_Z9IntersectRK12btDbvtAabbMmS1_.exit
	movss	24(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	40(%rsp), %xmm0
	jae	.LBB11_8
.LBB11_16:                              # %_Z9IntersectRK12btDbvtAabbMmS1_.exit.thread
	leaq	8(%r14), %rdi
	leaq	32(%rsp), %rdx
	callq	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm
.LBB11_17:
	incl	200(%r14)
.LBB11_18:
	movb	$1, %al
.LBB11_19:
	movslq	88(%rbx), %rcx
	leaq	136(%r14,%rcx,8), %rcx
	movq	72(%rbx), %rdx
	movq	80(%rbx), %rsi
	testq	%rdx, %rdx
	leaq	80(%rdx), %rdx
	cmoveq	%rcx, %rdx
	movq	%rsi, (%rdx)
	movq	80(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB11_21
# BB#20:
	movq	72(%rbx), %rdx
	movq	%rdx, 72(%rcx)
.LBB11_21:                              # %_ZL10listremoveI11btDbvtProxyEvPT_RS2_.exit
	movups	(%r12), %xmm0
	movups	%xmm0, 28(%rbx)
	movups	(%r15), %xmm0
	movups	%xmm0, 44(%rbx)
	movslq	172(%r14), %rcx
	movl	%ecx, 88(%rbx)
	movq	$0, 72(%rbx)
	movq	136(%r14,%rcx,8), %rdx
	movq	%rdx, 80(%rbx)
	movq	136(%r14,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.LBB11_23
# BB#22:
	movq	%rbx, 72(%rdx)
.LBB11_23:                              # %_ZL10listappendI11btDbvtProxyEvPT_RS2_.exit
	movq	%rbx, 136(%r14,%rcx,8)
	testb	%al, %al
	je	.LBB11_27
# BB#24:
	movb	$1, 222(%r14)
	cmpb	$0, 221(%r14)
	jne	.LBB11_27
# BB#25:
	movq	$_ZTV18btDbvtTreeCollider+16, 8(%rsp)
	movq	%r14, 16(%rsp)
	leaq	72(%r14), %rdi
	movq	72(%r14), %rsi
	movq	64(%rbx), %rdx
.Ltmp83:
	leaq	8(%rsp), %rcx
	callq	_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE
.Ltmp84:
# BB#26:
	movq	8(%r14), %rsi
	addq	$8, %r14
	movq	64(%rbx), %rdx
.Ltmp85:
	leaq	8(%rsp), %rcx
	movq	%r14, %rdi
	callq	_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE
.Ltmp86:
.LBB11_27:
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB11_8:
	movsd	28(%rbx), %xmm0         # xmm0 = mem[0],zero
	movss	(%r12), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm6
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	subss	%xmm1, %xmm2
	movss	8(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	36(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movsd	44(%rbx), %xmm5         # xmm5 = mem[0],zero
	subps	%xmm0, %xmm5
	movss	52(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	mulps	.LCPI11_0(%rip), %xmm5
	mulss	.LCPI11_1(%rip), %xmm0
	movss	168(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	xorps	%xmm5, %xmm5
	movss	%xmm0, %xmm5            # xmm5 = xmm0[0],xmm5[1,2,3]
	movlps	%xmm4, 8(%rsp)
	movlps	%xmm5, 16(%rsp)
	xorps	%xmm5, %xmm5
	ucomiss	%xmm6, %xmm5
	jbe	.LBB11_10
# BB#9:
	movaps	.LCPI11_2(%rip), %xmm6  # xmm6 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm4, %xmm6
	movss	%xmm6, 8(%rsp)
.LBB11_10:
	subss	%xmm3, %xmm1
	ucomiss	%xmm2, %xmm5
	jbe	.LBB11_12
# BB#11:
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	xorps	.LCPI11_2(%rip), %xmm4
	movss	%xmm4, 12(%rsp)
.LBB11_12:
	xorps	%xmm2, %xmm2
	ucomiss	%xmm1, %xmm2
	jbe	.LBB11_14
# BB#13:
	xorps	.LCPI11_2(%rip), %xmm0
	movss	%xmm0, 16(%rsp)
.LBB11_14:
	leaq	8(%r14), %rdi
	leaq	32(%rsp), %rdx
	leaq	8(%rsp), %rcx
	movss	.LCPI11_3(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	callq	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3f
	testb	%al, %al
	jne	.LBB11_17
# BB#15:
	xorl	%eax, %eax
	jmp	.LBB11_19
.LBB11_28:
.Ltmp87:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN16btDbvtBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher, .Lfunc_end11-_ZN16btDbvtBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin7-.Lfunc_begin7 # >> Call Site 1 <<
	.long	.Ltmp83-.Lfunc_begin7   #   Call between .Lfunc_begin7 and .Ltmp83
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Ltmp86-.Ltmp83         #   Call between .Ltmp83 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin7   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp86-.Lfunc_begin7   # >> Call Site 3 <<
	.long	.Lfunc_end11-.Ltmp86    #   Call between .Ltmp86 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE,"axG",@progbits,_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE,comdat
	.weak	_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE
	.p2align	4, 0x90
	.type	_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE,@function
_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE: # @_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi108:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi109:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi111:
	.cfi_def_cfa_offset 128
.Lcfi112:
	.cfi_offset %rbx, -56
.Lcfi113:
	.cfi_offset %r12, -48
.Lcfi114:
	.cfi_offset %r13, -40
.Lcfi115:
	.cfi_offset %r14, -32
.Lcfi116:
	.cfi_offset %r15, -24
.Lcfi117:
	.cfi_offset %rbp, -16
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	testq	%r13, %r13
	je	.LBB12_71
# BB#1:
	testq	%r12, %r12
	je	.LBB12_71
# BB#2:
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	36(%rax), %rbx
	cmpq	$128, %rbx
	jge	.LBB12_3
# BB#4:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$128, 40(%rax)
	jge	.LBB12_5
# BB#6:
	movl	$2048, %edi             # imm = 0x800
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, %r14
	movslq	36(%rcx), %rax
	leaq	48(%rcx), %rbp
	testq	%rax, %rax
	jle	.LBB12_14
# BB#7:                                 # %.lr.ph.i.i.i
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB12_8
# BB#9:                                 # %.prol.preheader152
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB12_10:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%r14,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB12_10
	jmp	.LBB12_11
.LBB12_3:                               # %._ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE6resizeEiRKS1_.exit_crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	48(%rax), %rbp
	leaq	40(%rax), %r9
	jmp	.LBB12_25
.LBB12_5:                               # %..lr.ph.i_crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	48(%rax), %rbp
	jmp	.LBB12_19
.LBB12_8:
	xorl	%ecx, %ecx
.LBB12_11:                              # %.prol.loopexit153
	cmpq	$3, %r8
	jb	.LBB12_14
# BB#12:                                # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB12_13:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%r14,%rcx)
	movq	(%rbp), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%r14,%rcx)
	movq	(%rbp), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%r14,%rcx)
	movq	(%rbp), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%r14,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB12_13
.LBB12_14:                              # %_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4copyEiiPS1_.exit.i.i
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB12_18
# BB#15:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpb	$0, 56(%rax)
	je	.LBB12_17
# BB#16:
	callq	_Z21btAlignedFreeInternalPv
.LBB12_17:
	movq	$0, (%rbp)
.LBB12_18:                              # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.preheader.i
	movq	8(%rsp), %rax           # 8-byte Reload
	movb	$1, 56(%rax)
	movq	%r14, 48(%rax)
	movl	$128, 40(%rax)
.LBB12_19:                              # %.lr.ph.i
	movl	$127, %eax
	subq	%rbx, %rax
	movl	%ebx, %ecx
	negl	%ecx
	andq	$3, %rcx
	je	.LBB12_22
# BB#20:                                # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i.prol.preheader
	movq	%rbx, %rdx
	shlq	$4, %rdx
	negq	%rcx
	.p2align	4, 0x90
.LBB12_21:                              # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rsi
	movups	24(%rsp), %xmm0
	movups	%xmm0, (%rsi,%rdx)
	incq	%rbx
	addq	$16, %rdx
	incq	%rcx
	jne	.LBB12_21
.LBB12_22:                              # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i.prol.loopexit
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	40(%rcx), %r9
	cmpq	$3, %rax
	jb	.LBB12_25
# BB#23:                                # %.lr.ph.i.new
	movl	$128, %eax
	subq	%rbx, %rax
	shlq	$4, %rbx
	addq	$48, %rbx
	.p2align	4, 0x90
.LBB12_24:                              # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rcx
	movups	24(%rsp), %xmm0
	movups	%xmm0, -48(%rcx,%rbx)
	movq	(%rbp), %rcx
	movups	24(%rsp), %xmm0
	movups	%xmm0, -32(%rcx,%rbx)
	movq	(%rbp), %rcx
	movups	24(%rsp), %xmm0
	movups	%xmm0, -16(%rcx,%rbx)
	movq	(%rbp), %rcx
	movups	24(%rsp), %xmm0
	movups	%xmm0, (%rcx,%rbx)
	addq	$64, %rbx
	addq	$-4, %rax
	jne	.LBB12_24
.LBB12_25:                              # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE6resizeEiRKS1_.exit
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$128, 36(%rax)
	movq	(%rbp), %rax
	movq	%r13, (%rax)
	movq	%r12, 8(%rax)
	movl	$1, %r15d
	movl	$124, %r12d
	movq	%r9, 16(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB12_26:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_36 Depth 2
                                        #     Child Loop BB12_40 Depth 2
                                        #     Child Loop BB12_49 Depth 2
                                        #     Child Loop BB12_52 Depth 2
	movslq	%r15d, %r11
	decl	%r15d
	leaq	-1(%r11), %r14
	movq	(%rbp), %rbx
	shlq	$4, %r14
	movq	(%rbx,%r14), %r13
	movq	8(%rbx,%r14), %r8
	cmpl	%r12d, %r15d
	jle	.LBB12_54
# BB#27:                                #   in Loop: Header=BB12_26 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	36(%rax), %r12
	leaq	(%r12,%r12), %rsi
	cmpl	%esi, %r12d
	jge	.LBB12_53
# BB#28:                                #   in Loop: Header=BB12_26 Depth=1
	cmpl	%esi, (%r9)
	jge	.LBB12_46
# BB#29:                                #   in Loop: Header=BB12_26 Depth=1
	testl	%r12d, %r12d
	movq	%r11, 40(%rsp)          # 8-byte Spill
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	je	.LBB12_30
# BB#31:                                # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE8allocateEi.exit.i.i
                                        #   in Loop: Header=BB12_26 Depth=1
	movq	%rsi, %rdi
	shlq	$4, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	36(%rax), %rax
	testq	%rax, %rax
	jle	.LBB12_32
# BB#33:                                # %.lr.ph.i.i.i59
                                        #   in Loop: Header=BB12_26 Depth=1
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB12_34
# BB#35:                                # %.prol.preheader
                                        #   in Loop: Header=BB12_26 Depth=1
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB12_36:                              #   Parent Loop BB12_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%rbx,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB12_36
	jmp	.LBB12_37
.LBB12_30:                              #   in Loop: Header=BB12_26 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB12_41
.LBB12_32:                              #   in Loop: Header=BB12_26 Depth=1
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	jmp	.LBB12_41
.LBB12_34:                              #   in Loop: Header=BB12_26 Depth=1
	xorl	%ecx, %ecx
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
.LBB12_37:                              # %.prol.loopexit
                                        #   in Loop: Header=BB12_26 Depth=1
	cmpq	$3, %r8
	jae	.LBB12_39
# BB#38:                                #   in Loop: Header=BB12_26 Depth=1
	movq	48(%rsp), %r8           # 8-byte Reload
	jmp	.LBB12_41
.LBB12_39:                              # %.lr.ph.i.i.i59.new
                                        #   in Loop: Header=BB12_26 Depth=1
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	movq	48(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB12_40:                              #   Parent Loop BB12_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbx,%rcx)
	movq	(%rbp), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbx,%rcx)
	movq	(%rbp), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbx,%rcx)
	movq	(%rbp), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbx,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB12_40
.LBB12_41:                              # %_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4copyEiiPS1_.exit.i.i64
                                        #   in Loop: Header=BB12_26 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_45
# BB#42:                                #   in Loop: Header=BB12_26 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpb	$0, 56(%rax)
	je	.LBB12_44
# BB#43:                                #   in Loop: Header=BB12_26 Depth=1
	callq	_Z21btAlignedFreeInternalPv
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
.LBB12_44:                              #   in Loop: Header=BB12_26 Depth=1
	movq	$0, (%rbp)
.LBB12_45:                              # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.preheader.i65
                                        #   in Loop: Header=BB12_26 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movb	$1, 56(%rax)
	movq	%rbx, (%rbp)
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%esi, (%r9)
.LBB12_46:                              # %.lr.ph.i66
                                        #   in Loop: Header=BB12_26 Depth=1
	movslq	%esi, %rax
	movq	%r12, %rdx
	shlq	$4, %rdx
	movups	24(%rsp), %xmm0
	movups	%xmm0, (%rbx,%rdx)
	leaq	1(%r12), %rcx
	cmpq	%rax, %rcx
	je	.LBB12_53
# BB#47:                                # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i70._ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i70_crit_edge.preheader
                                        #   in Loop: Header=BB12_26 Depth=1
	movq	%rsi, %r10
	leal	3(%rsi), %edi
	subl	%r12d, %edi
	leaq	-2(%rax), %rsi
	subq	%r12, %rsi
	andq	$3, %rdi
	je	.LBB12_50
# BB#48:                                # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i70._ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i70_crit_edge.prol.preheader
                                        #   in Loop: Header=BB12_26 Depth=1
	addq	$16, %rdx
	negq	%rdi
	.p2align	4, 0x90
.LBB12_49:                              # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i70._ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i70_crit_edge.prol
                                        #   Parent Loop BB12_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rbx
	movups	24(%rsp), %xmm0
	movups	%xmm0, (%rbx,%rdx)
	incq	%rcx
	addq	$16, %rdx
	incq	%rdi
	jne	.LBB12_49
.LBB12_50:                              # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i70._ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i70_crit_edge.prol.loopexit
                                        #   in Loop: Header=BB12_26 Depth=1
	cmpq	$3, %rsi
	movq	%r10, %rsi
	jb	.LBB12_53
# BB#51:                                # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i70._ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i70_crit_edge.preheader.new
                                        #   in Loop: Header=BB12_26 Depth=1
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB12_52:                              # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i70._ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi.exit.i70_crit_edge
                                        #   Parent Loop BB12_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rdx
	movups	24(%rsp), %xmm0
	movups	%xmm0, -48(%rdx,%rcx)
	movq	(%rbp), %rdx
	movups	24(%rsp), %xmm0
	movups	%xmm0, -32(%rdx,%rcx)
	movq	(%rbp), %rdx
	movups	24(%rsp), %xmm0
	movups	%xmm0, -16(%rdx,%rcx)
	movq	(%rbp), %rdx
	movups	24(%rsp), %xmm0
	movups	%xmm0, (%rdx,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB12_52
	.p2align	4, 0x90
.LBB12_53:                              # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE6resizeEiRKS1_.exit71
                                        #   in Loop: Header=BB12_26 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%esi, 36(%rax)
	leal	-4(%rsi), %r12d
.LBB12_54:                              #   in Loop: Header=BB12_26 Depth=1
	cmpq	%r8, %r13
	je	.LBB12_55
# BB#57:                                #   in Loop: Header=BB12_26 Depth=1
	movss	16(%r8), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%r13), %xmm0
	jb	.LBB12_70
# BB#58:                                #   in Loop: Header=BB12_26 Depth=1
	movss	16(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%r8), %xmm0
	jb	.LBB12_70
# BB#59:                                #   in Loop: Header=BB12_26 Depth=1
	movss	20(%r8), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%r13), %xmm0
	jb	.LBB12_70
# BB#60:                                #   in Loop: Header=BB12_26 Depth=1
	movss	20(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%r8), %xmm0
	jb	.LBB12_70
# BB#61:                                #   in Loop: Header=BB12_26 Depth=1
	movss	24(%r8), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%r13), %xmm0
	jb	.LBB12_70
# BB#62:                                # %_Z9IntersectRK12btDbvtAabbMmS1_.exit
                                        #   in Loop: Header=BB12_26 Depth=1
	movss	24(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%r8), %xmm0
	jb	.LBB12_70
# BB#63:                                #   in Loop: Header=BB12_26 Depth=1
	cmpq	$0, 48(%r13)
	movq	48(%r8), %rcx
	je	.LBB12_67
# BB#64:                                #   in Loop: Header=BB12_26 Depth=1
	movq	40(%r13), %rax
	testq	%rcx, %rcx
	je	.LBB12_66
# BB#65:                                #   in Loop: Header=BB12_26 Depth=1
	movq	40(%r8), %rcx
	movq	(%rbp), %rdx
	movq	%rax, (%rdx,%r14)
	movq	%rcx, 8(%rdx,%r14)
	movq	48(%r13), %rax
	movq	40(%r8), %rcx
	movq	(%rbp), %rdx
	movq	%r11, %rsi
	shlq	$4, %rsi
	movq	%rax, (%rdx,%rsi)
	movq	%rcx, 8(%rdx,%rsi)
	movq	40(%r13), %rax
	movq	48(%r8), %rcx
	movq	(%rbp), %rdx
	movq	%rax, 16(%rdx,%rsi)
	movq	%rcx, 24(%rdx,%rsi)
	movq	48(%r13), %rax
	movq	48(%r8), %rcx
	addl	$3, %r11d
	movq	(%rbp), %rdx
	movq	%rax, 32(%rdx,%rsi)
	movq	%rcx, 40(%rdx,%rsi)
	movl	%r11d, %r15d
	testl	%r15d, %r15d
	jne	.LBB12_26
	jmp	.LBB12_71
	.p2align	4, 0x90
.LBB12_55:                              #   in Loop: Header=BB12_26 Depth=1
	cmpq	$0, 48(%r13)
	je	.LBB12_70
# BB#56:                                #   in Loop: Header=BB12_26 Depth=1
	movq	40(%r13), %rax
	movq	(%rbp), %rcx
	movq	%rax, (%rcx,%r14)
	movq	%rax, 8(%rcx,%r14)
	movq	48(%r13), %rax
	movq	(%rbp), %rcx
	movq	%r11, %rdx
	shlq	$4, %rdx
	movq	%rax, (%rcx,%rdx)
	movq	%rax, 8(%rcx,%rdx)
	movups	40(%r13), %xmm0
	addl	$2, %r11d
	movq	(%rbp), %rax
	movups	%xmm0, 16(%rax,%rdx)
	movl	%r11d, %r15d
.LBB12_70:                              # %_Z9IntersectRK12btDbvtAabbMmS1_.exit.thread
                                        #   in Loop: Header=BB12_26 Depth=1
	testl	%r15d, %r15d
	jne	.LBB12_26
	jmp	.LBB12_71
.LBB12_67:                              #   in Loop: Header=BB12_26 Depth=1
	testq	%rcx, %rcx
	je	.LBB12_69
# BB#68:                                #   in Loop: Header=BB12_26 Depth=1
	movq	40(%r8), %rax
	movq	(%rbp), %rcx
	movq	%r13, (%rcx,%r14)
	movq	%rax, 8(%rcx,%r14)
	movq	48(%r8), %rax
	leal	1(%r11), %r15d
	movq	(%rbp), %rcx
	shlq	$4, %r11
	movq	%r13, (%rcx,%r11)
	movq	%rax, 8(%rcx,%r11)
	testl	%r15d, %r15d
	jne	.LBB12_26
	jmp	.LBB12_71
.LBB12_66:                              #   in Loop: Header=BB12_26 Depth=1
	movq	(%rbp), %rcx
	movq	%rax, (%rcx,%r14)
	movq	%r8, 8(%rcx,%r14)
	movq	48(%r13), %rax
	leal	1(%r11), %r15d
	movq	(%rbp), %rcx
	shlq	$4, %r11
	movq	%rax, (%rcx,%r11)
	movq	%r8, 8(%rcx,%r11)
	testl	%r15d, %r15d
	jne	.LBB12_26
	jmp	.LBB12_71
.LBB12_69:                              #   in Loop: Header=BB12_26 Depth=1
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	%r13, %rsi
	movq	%r8, %rdx
	callq	*16(%rax)
	movq	16(%rsp), %r9           # 8-byte Reload
	testl	%r15d, %r15d
	jne	.LBB12_26
.LBB12_71:                              # %.loopexit
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE, .Lfunc_end12-_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE
	.cfi_endproc

	.text
	.globl	_ZN16btDbvtBroadphase25calculateOverlappingPairsEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphase25calculateOverlappingPairsEP12btDispatcher,@function
_ZN16btDbvtBroadphase25calculateOverlappingPairsEP12btDispatcher: # @_ZN16btDbvtBroadphase25calculateOverlappingPairsEP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi120:
	.cfi_def_cfa_offset 32
.Lcfi121:
	.cfi_offset %rbx, -24
.Lcfi122:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN16btDbvtBroadphase7collideEP12btDispatcher
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN16btDbvtBroadphase22performDeferredRemovalEP12btDispatcher # TAILCALL
.Lfunc_end13:
	.size	_ZN16btDbvtBroadphase25calculateOverlappingPairsEP12btDispatcher, .Lfunc_end13-_ZN16btDbvtBroadphase25calculateOverlappingPairsEP12btDispatcher
	.cfi_endproc

	.globl	_ZN16btDbvtBroadphase7collideEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphase7collideEP12btDispatcher,@function
_ZN16btDbvtBroadphase7collideEP12btDispatcher: # @_ZN16btDbvtBroadphase7collideEP12btDispatcher
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%rbp
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi126:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi127:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi129:
	.cfi_def_cfa_offset 112
.Lcfi130:
	.cfi_offset %rbx, -56
.Lcfi131:
	.cfi_offset %r12, -48
.Lcfi132:
	.cfi_offset %r13, -40
.Lcfi133:
	.cfi_offset %r14, -32
.Lcfi134:
	.cfi_offset %r15, -24
.Lcfi135:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r13
	leaq	8(%r13), %r15
	movl	180(%r13), %eax
	imull	28(%r13), %eax
	cltq
	imulq	$1374389535, %rax, %rax # imm = 0x51EB851F
	movq	%rax, %rcx
	sarq	$37, %rcx
	shrq	$63, %rax
	leal	1(%rcx,%rax), %esi
	movq	%r15, %rdi
	callq	_ZN6btDbvt19optimizeIncrementalEi
	cmpl	$0, 192(%r13)
	je	.LBB14_2
# BB#1:
	leaq	72(%r13), %rdi
	movl	176(%r13), %eax
	imull	92(%r13), %eax
	cltq
	imulq	$1374389535, %rax, %rax # imm = 0x51EB851F
	movq	%rax, %rcx
	sarq	$37, %rcx
	shrq	$63, %rax
	leal	1(%rcx,%rax), %ebx
	movl	%ebx, %esi
	callq	_ZN6btDbvt19optimizeIncrementalEi
	movl	192(%r13), %eax
	xorl	%ecx, %ecx
	subl	%ebx, %eax
	cmovsl	%ecx, %eax
	movl	%eax, 192(%r13)
.LBB14_2:
	movl	172(%r13), %eax
	leal	1(%rax), %ecx
	shrl	$31, %ecx
	leal	1(%rax,%rcx), %ecx
	andl	$-2, %ecx
	negl	%ecx
	leal	1(%rax,%rcx), %eax
	movl	%eax, 172(%r13)
	cltq
	movq	136(%r13,%rax,8), %rbx
	testq	%rbx, %rbx
	je	.LBB14_12
# BB#3:                                 # %.preheader
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	leaq	72(%r13), %r12
	leaq	16(%rsp), %rbp
	.p2align	4, 0x90
.LBB14_4:                               # =>This Inner Loop Header: Depth=1
	movq	72(%rbx), %rax
	movq	80(%rbx), %r14
	movslq	88(%rbx), %rcx
	leaq	136(%r13,%rcx,8), %rcx
	testq	%rax, %rax
	leaq	80(%rax), %rax
	cmoveq	%rcx, %rax
	movq	%r14, (%rax)
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.LBB14_6
# BB#5:                                 #   in Loop: Header=BB14_4 Depth=1
	movq	72(%rbx), %rcx
	movq	%rcx, 72(%rax)
.LBB14_6:                               # %_ZL10listremoveI11btDbvtProxyEvPT_RS2_.exit
                                        #   in Loop: Header=BB14_4 Depth=1
	movq	$0, 72(%rbx)
	movq	152(%r13), %rax
	movq	%rax, 80(%rbx)
	movq	152(%r13), %rax
	testq	%rax, %rax
	je	.LBB14_8
# BB#7:                                 #   in Loop: Header=BB14_4 Depth=1
	movq	%rbx, 72(%rax)
.LBB14_8:                               # %_ZL10listappendI11btDbvtProxyEvPT_RS2_.exit
                                        #   in Loop: Header=BB14_4 Depth=1
	movq	%rbx, 152(%r13)
	movq	64(%rbx), %rsi
.Ltmp88:
	movq	%r15, %rdi
	callq	_ZN6btDbvt6removeEP10btDbvtNode
.Ltmp89:
# BB#9:                                 #   in Loop: Header=BB14_4 Depth=1
	movups	28(%rbx), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	44(%rbx), %xmm0
	leaq	32(%rsp), %rax
	movups	%xmm0, (%rax)
.Ltmp91:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	_ZN6btDbvt6insertERK12btDbvtAabbMmPv
.Ltmp92:
# BB#10:                                #   in Loop: Header=BB14_4 Depth=1
	movq	%rax, 64(%rbx)
	movl	$2, 88(%rbx)
	testq	%r14, %r14
	movq	%r14, %rbx
	jne	.LBB14_4
# BB#11:
	movl	92(%r13), %eax
	movl	%eax, 192(%r13)
	movb	$1, 222(%r13)
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB14_12:
	movq	$_ZTV18btDbvtTreeCollider+16, 16(%rsp)
	movq	%r13, 24(%rsp)
	cmpb	$0, 221(%r13)
	je	.LBB14_16
# BB#13:
	movq	8(%r13), %rsi
	movq	72(%r13), %rdx
.Ltmp94:
	leaq	16(%rsp), %rcx
	movq	%r15, %rdi
	callq	_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE
.Ltmp95:
# BB#14:
	cmpb	$0, 221(%r13)
	je	.LBB14_16
# BB#15:
	movq	(%r15), %rsi
.Ltmp96:
	leaq	16(%rsp), %rcx
	movq	%r15, %rdi
	movq	%rsi, %rdx
	callq	_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE
.Ltmp97:
.LBB14_16:                              # %.thread
	cmpb	$0, 222(%r13)
	je	.LBB14_32
# BB#17:
	movq	160(%r13), %rdi
	movq	(%rdi), %rax
	callq	*56(%rax)
	movq	%rax, %r12
	movl	4(%r12), %ecx
	testl	%ecx, %ecx
	jle	.LBB14_32
# BB#18:
	movl	184(%r13), %eax
	imull	%ecx, %eax
	cltq
	imulq	$1374389535, %rax, %r15 # imm = 0x51EB851F
	movq	%r15, %rax
	shrq	$63, %rax
	sarq	$37, %r15
	addl	%eax, %r15d
	movl	188(%r13), %eax
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	cmpl	%r15d, %ecx
	cmovlel	%ecx, %r15d
	testl	%r15d, %r15d
	jle	.LBB14_30
# BB#19:                                # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_20:                              # %._crit_edge96
                                        # =>This Inner Loop Header: Depth=1
	movl	212(%r13), %eax
	addl	%ebx, %eax
	cltd
	idivl	%ecx
	movq	16(%r12), %rax
	movslq	%edx, %rcx
	shlq	$5, %rcx
	movq	(%rax,%rcx), %rsi
	movq	8(%rax,%rcx), %rdx
	movq	64(%rsi), %rax
	movq	64(%rdx), %rcx
	movss	16(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rax), %xmm0
	jb	.LBB14_26
# BB#21:                                #   in Loop: Header=BB14_20 Depth=1
	movss	16(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rcx), %xmm0
	jb	.LBB14_26
# BB#22:                                #   in Loop: Header=BB14_20 Depth=1
	movss	20(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%rax), %xmm0
	jb	.LBB14_26
# BB#23:                                #   in Loop: Header=BB14_20 Depth=1
	movss	20(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%rcx), %xmm0
	jb	.LBB14_26
# BB#24:                                #   in Loop: Header=BB14_20 Depth=1
	movss	24(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%rax), %xmm0
	jb	.LBB14_26
# BB#25:                                # %_Z9IntersectRK12btDbvtAabbMmS1_.exit
                                        #   in Loop: Header=BB14_20 Depth=1
	movss	24(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%rcx), %xmm0
	jae	.LBB14_27
	.p2align	4, 0x90
.LBB14_26:                              # %_Z9IntersectRK12btDbvtAabbMmS1_.exit.thread
                                        #   in Loop: Header=BB14_20 Depth=1
	movq	160(%r13), %rdi
	movq	(%rdi), %rax
	movq	%rbp, %rcx
	callq	*24(%rax)
	decl	%r15d
	decl	%ebx
.LBB14_27:                              #   in Loop: Header=BB14_20 Depth=1
	incl	%ebx
	cmpl	%r15d, %ebx
	movl	4(%r12), %ecx
	jl	.LBB14_20
# BB#28:                                # %._crit_edge
	testl	%ecx, %ecx
	jle	.LBB14_29
.LBB14_30:                              # %._crit_edge.thread
	leaq	212(%r13), %rsi
	addl	212(%r13), %r15d
	movl	%r15d, %eax
	cltd
	idivl	%ecx
	jmp	.LBB14_31
.LBB14_29:                              # %._crit_edge._crit_edge
	leaq	212(%r13), %rsi
	xorl	%edx, %edx
.LBB14_31:
	movl	%edx, (%rsi)
.LBB14_32:
	incl	208(%r13)
	movl	$1, 188(%r13)
	movb	$0, 222(%r13)
	movl	196(%r13), %eax
	movl	200(%r13), %ecx
	testl	%eax, %eax
	je	.LBB14_33
# BB#34:
	movl	%ecx, %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rdx, %xmm0
	movl	%eax, %edx
	cvtsi2ssq	%rdx, %xmm1
	divss	%xmm1, %xmm0
	jmp	.LBB14_35
.LBB14_33:
	xorps	%xmm0, %xmm0
.LBB14_35:                              # %._crit_edge98
	movss	%xmm0, 204(%r13)
	shrl	%ecx
	movl	%ecx, 200(%r13)
	shrl	%eax
	movl	%eax, 196(%r13)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_39:
.Ltmp98:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB14_38:
.Ltmp93:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB14_36:
.Ltmp90:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN16btDbvtBroadphase7collideEP12btDispatcher, .Lfunc_end14-_ZN16btDbvtBroadphase7collideEP12btDispatcher
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin8-.Lfunc_begin8 # >> Call Site 1 <<
	.long	.Ltmp88-.Lfunc_begin8   #   Call between .Lfunc_begin8 and .Ltmp88
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin8   # >> Call Site 2 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin8   #     jumps to .Ltmp90
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin8   # >> Call Site 3 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin8   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin8   # >> Call Site 4 <<
	.long	.Ltmp97-.Ltmp94         #   Call between .Ltmp94 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin8   #     jumps to .Ltmp98
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin8   # >> Call Site 5 <<
	.long	.Lfunc_end14-.Ltmp97    #   Call between .Ltmp97 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN16btDbvtBroadphase22performDeferredRemovalEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphase22performDeferredRemovalEP12btDispatcher,@function
_ZN16btDbvtBroadphase22performDeferredRemovalEP12btDispatcher: # @_ZN16btDbvtBroadphase22performDeferredRemovalEP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi136:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi137:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi138:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi139:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi140:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi142:
	.cfi_def_cfa_offset 112
.Lcfi143:
	.cfi_offset %rbx, -56
.Lcfi144:
	.cfi_offset %r12, -48
.Lcfi145:
	.cfi_offset %r13, -40
.Lcfi146:
	.cfi_offset %r14, -32
.Lcfi147:
	.cfi_offset %r15, -24
.Lcfi148:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	160(%r15), %rdi
	movq	(%rdi), %rax
	callq	*112(%rax)
	testb	%al, %al
	je	.LBB15_21
# BB#1:
	movq	160(%r15), %rdi
	movq	(%rdi), %rax
	callq	*56(%rax)
	movq	%rax, %r12
	movl	4(%r12), %eax
	cmpl	$2, %eax
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	jl	.LBB15_2
# BB#3:
	decl	%eax
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	jmp	.LBB15_4
.LBB15_2:
	xorl	%r13d, %r13d
	xorl	%edx, %edx
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	cmpl	%eax, %ebp
	jl	.LBB15_6
	jmp	.LBB15_18
.LBB15_4:                               # %_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvT_.exitthread-pre-split
	movl	4(%r12), %eax
	movq	%r14, %rdx
	cmpl	%eax, %ebp
	jge	.LBB15_18
.LBB15_6:
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movq	16(%r12), %rbx
	movslq	%ebp, %rax
	shlq	$5, %rax
	movq	(%rbx,%rax), %r14
	movq	8(%rbx,%rax), %rcx
	cmpq	%rdx, %r14
	jne	.LBB15_8
# BB#7:
	cmpq	%r13, %rcx
	je	.LBB15_10
.LBB15_8:                               # %_ZeqRK16btBroadphasePairS1_.exit.thread
	leaq	8(%rbx,%rax), %rdx
	movq	(%rdx), %rsi
	movq	64(%r14), %rdx
	movq	64(%rsi), %rsi
	movss	16(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rdx), %xmm0
	jb	.LBB15_9
# BB#11:
	movss	16(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rsi), %xmm0
	jb	.LBB15_9
# BB#12:
	movss	20(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%rdx), %xmm0
	jb	.LBB15_9
# BB#13:
	movss	20(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%rsi), %xmm0
	jb	.LBB15_9
# BB#14:
	movss	24(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%rdx), %xmm0
	jae	.LBB15_15
.LBB15_9:
	movq	%rcx, %r13
.LBB15_10:                              # %_Z9IntersectRK12btDbvtAabbMmS1_.exit.thread
	addq	%rax, %rbx
	movq	160(%r15), %rdi
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	*64(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movl	4(%rsp), %ebx           # 4-byte Reload
	incl	%ebx
	movq	%r13, %rcx
.LBB15_17:                              # %.thread62
	incl	%ebp
	movq	%rcx, %r13
	jmp	.LBB15_4
.LBB15_18:
	cmpl	$2, %eax
	jl	.LBB15_20
# BB#19:
	decl	%eax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	movl	4(%r12), %eax
.LBB15_20:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvT_.exit38
	subl	%ebx, %eax
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	leaq	16(%rsp), %rdx
	movq	%r12, %rdi
	movl	%eax, %esi
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
.LBB15_21:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_15:                              # %_Z9IntersectRK12btDbvtAabbMmS1_.exit
	movss	24(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%rsi), %xmm0
	movq	%rcx, %r13
	jb	.LBB15_10
# BB#16:
	movl	4(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB15_17
.Lfunc_end15:
	.size	_ZN16btDbvtBroadphase22performDeferredRemovalEP12btDispatcher, .Lfunc_end15-_ZN16btDbvtBroadphase22performDeferredRemovalEP12btDispatcher
	.cfi_endproc

	.section	.text._ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_,"axG",@progbits,_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_,comdat
	.weak	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_,@function
_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_: # @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi151:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi152:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 48
.Lcfi154:
	.cfi_offset %rbx, -48
.Lcfi155:
	.cfi_offset %r12, -40
.Lcfi156:
	.cfi_offset %r14, -32
.Lcfi157:
	.cfi_offset %r15, -24
.Lcfi158:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %r14d
	movq	%rdi, %r15
	movl	4(%r15), %r12d
	cmpl	%r14d, %r12d
	jg	.LBB16_19
# BB#1:
	jge	.LBB16_19
# BB#2:
	cmpl	%r14d, 8(%r15)
	jge	.LBB16_14
# BB#3:
	testl	%r14d, %r14d
	je	.LBB16_4
# BB#5:
	movslq	%r14d, %rdi
	shlq	$5, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
	movl	4(%r15), %eax
	testl	%eax, %eax
	jg	.LBB16_7
	jmp	.LBB16_9
.LBB16_4:
	xorl	%ebp, %ebp
	movl	%r12d, %eax
	testl	%eax, %eax
	jle	.LBB16_9
.LBB16_7:                               # %.lr.ph.i.i
	cltq
	movl	$24, %ecx
	.p2align	4, 0x90
.LBB16_8:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rdx
	movups	-24(%rdx,%rcx), %xmm0
	movups	%xmm0, -24(%rbp,%rcx)
	movq	-8(%rdx,%rcx), %rsi
	movq	%rsi, -8(%rbp,%rcx)
	movq	(%rdx,%rcx), %rdx
	movq	%rdx, (%rbp,%rcx)
	addq	$32, %rcx
	decq	%rax
	jne	.LBB16_8
.LBB16_9:                               # %_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_.exit.i
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB16_13
# BB#10:
	cmpb	$0, 24(%r15)
	je	.LBB16_12
# BB#11:
	callq	_Z21btAlignedFreeInternalPv
.LBB16_12:
	movq	$0, 16(%r15)
.LBB16_13:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit.preheader
	movb	$1, 24(%r15)
	movq	%rbp, 16(%r15)
	movl	%r14d, 8(%r15)
	cmpl	%r14d, %r12d
	jge	.LBB16_19
.LBB16_14:                              # %.lr.ph
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	movl	%r14d, %ecx
	subl	%r12d, %ecx
	leaq	-1(%rax), %rsi
	testb	$1, %cl
	movq	%rdx, %rcx
	je	.LBB16_16
# BB#15:                                # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit.prol
	movq	16(%r15), %rcx
	movq	%rdx, %rdi
	shlq	$5, %rdi
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rcx,%rdi)
	movq	16(%rbx), %rbp
	movq	%rbp, 16(%rcx,%rdi)
	movq	24(%rbx), %rbp
	movq	%rbp, 24(%rcx,%rdi)
	leaq	1(%rdx), %rcx
.LBB16_16:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit.prol.loopexit
	cmpq	%rdx, %rsi
	je	.LBB16_19
# BB#17:                                # %.lr.ph.new
	subq	%rcx, %rax
	shlq	$5, %rcx
	.p2align	4, 0x90
.LBB16_18:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rdx
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rdx,%rcx)
	movq	16(%rbx), %rsi
	movq	%rsi, 16(%rdx,%rcx)
	movq	24(%rbx), %rsi
	movq	%rsi, 24(%rdx,%rcx)
	movq	16(%r15), %rdx
	movups	(%rbx), %xmm0
	movups	%xmm0, 32(%rdx,%rcx)
	movq	16(%rbx), %rsi
	movq	%rsi, 48(%rdx,%rcx)
	movq	24(%rbx), %rsi
	movq	%rsi, 56(%rdx,%rcx)
	addq	$64, %rcx
	addq	$-2, %rax
	jne	.LBB16_18
.LBB16_19:                              # %.loopexit
	movl	%r14d, 4(%r15)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_, .Lfunc_end16-_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	.cfi_endproc

	.text
	.globl	_ZN16btDbvtBroadphase8optimizeEv
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphase8optimizeEv,@function
_ZN16btDbvtBroadphase8optimizeEv:       # @_ZN16btDbvtBroadphase8optimizeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi159:
	.cfi_def_cfa_offset 16
.Lcfi160:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	8(%rbx), %rdi
	movl	$128, %esi
	callq	_ZN6btDbvt15optimizeTopDownEi
	addq	$72, %rbx
	movl	$128, %esi
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZN6btDbvt15optimizeTopDownEi # TAILCALL
.Lfunc_end17:
	.size	_ZN16btDbvtBroadphase8optimizeEv, .Lfunc_end17-_ZN16btDbvtBroadphase8optimizeEv
	.cfi_endproc

	.globl	_ZN16btDbvtBroadphase23getOverlappingPairCacheEv
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphase23getOverlappingPairCacheEv,@function
_ZN16btDbvtBroadphase23getOverlappingPairCacheEv: # @_ZN16btDbvtBroadphase23getOverlappingPairCacheEv
	.cfi_startproc
# BB#0:
	movq	160(%rdi), %rax
	retq
.Lfunc_end18:
	.size	_ZN16btDbvtBroadphase23getOverlappingPairCacheEv, .Lfunc_end18-_ZN16btDbvtBroadphase23getOverlappingPairCacheEv
	.cfi_endproc

	.globl	_ZNK16btDbvtBroadphase23getOverlappingPairCacheEv
	.p2align	4, 0x90
	.type	_ZNK16btDbvtBroadphase23getOverlappingPairCacheEv,@function
_ZNK16btDbvtBroadphase23getOverlappingPairCacheEv: # @_ZNK16btDbvtBroadphase23getOverlappingPairCacheEv
	.cfi_startproc
# BB#0:
	movq	160(%rdi), %rax
	retq
.Lfunc_end19:
	.size	_ZNK16btDbvtBroadphase23getOverlappingPairCacheEv, .Lfunc_end19-_ZNK16btDbvtBroadphase23getOverlappingPairCacheEv
	.cfi_endproc

	.globl	_ZNK16btDbvtBroadphase17getBroadphaseAabbER9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK16btDbvtBroadphase17getBroadphaseAabbER9btVector3S1_,@function
_ZNK16btDbvtBroadphase17getBroadphaseAabbER9btVector3S1_: # @_ZNK16btDbvtBroadphase17getBroadphaseAabbER9btVector3S1_
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rcx
	movq	72(%rdi), %rdi
	testq	%rcx, %rcx
	je	.LBB20_4
# BB#1:
	testq	%rdi, %rdi
	je	.LBB20_3
# BB#2:
	leaq	16(%rcx), %r8
	leaq	16(%rdi), %r9
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	(%rcx), %xmm0
	movq	%rdi, %r11
	cmovaq	%rcx, %r11
	movss	16(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	ucomiss	16(%rdi), %xmm0
	movq	%r9, %r10
	cmovaq	%r8, %r10
	ucomiss	4(%rcx), %xmm1
	movq	%rdi, %rax
	cmovaq	%rcx, %rax
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	(%r11), %xmm0           # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	ucomiss	20(%rdi), %xmm2
	movq	%r9, %rax
	cmovaq	%r8, %rax
	movss	4(%rax), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	(%r10), %xmm1           # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movss	8(%rdi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	ucomiss	8(%rcx), %xmm2
	movq	%rdi, %rax
	cmovaq	%rcx, %rax
	movss	8(%rax), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	24(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	ucomiss	24(%rdi), %xmm3
	cmovaq	%r8, %r9
	movss	8(%r9), %xmm3           # xmm3 = mem[0],zero,zero,zero
	jmp	.LBB20_7
.LBB20_4:
	testq	%rdi, %rdi
	je	.LBB20_5
# BB#6:
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rdi), %xmm2          # xmm2 = mem[0],zero
	movsd	16(%rdi), %xmm1         # xmm1 = mem[0],zero
	movsd	24(%rdi), %xmm3         # xmm3 = mem[0],zero
	jmp	.LBB20_7
.LBB20_3:
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rcx), %xmm2          # xmm2 = mem[0],zero
	movsd	16(%rcx), %xmm1         # xmm1 = mem[0],zero
	movsd	24(%rcx), %xmm3         # xmm3 = mem[0],zero
	jmp	.LBB20_7
.LBB20_5:
	xorps	%xmm0, %xmm0
	xorps	%xmm2, %xmm2
	xorps	%xmm1, %xmm1
	xorps	%xmm3, %xmm3
.LBB20_7:
	movlps	%xmm0, (%rsi)
	movlps	%xmm2, 8(%rsi)
	movlps	%xmm1, (%rdx)
	movlps	%xmm3, 8(%rdx)
	retq
.Lfunc_end20:
	.size	_ZNK16btDbvtBroadphase17getBroadphaseAabbER9btVector3S1_, .Lfunc_end20-_ZNK16btDbvtBroadphase17getBroadphaseAabbER9btVector3S1_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI21_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	10                      # 0xa
	.text
	.globl	_ZN16btDbvtBroadphase9resetPoolEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphase9resetPoolEP12btDispatcher,@function
_ZN16btDbvtBroadphase9resetPoolEP12btDispatcher: # @_ZN16btDbvtBroadphase9resetPoolEP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi163:
	.cfi_def_cfa_offset 32
.Lcfi164:
	.cfi_offset %rbx, -24
.Lcfi165:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	addl	92(%rbx), %eax
	jne	.LBB21_2
# BB#1:                                 # %.loopexit.loopexit
	leaq	72(%rbx), %r14
	leaq	8(%rbx), %rdi
	callq	_ZN6btDbvt5clearEv
	movq	%r14, %rdi
	callq	_ZN6btDbvt5clearEv
	movb	$0, 221(%rbx)
	movb	$1, 222(%rbx)
	movl	$0, 192(%rbx)
	movaps	.LCPI21_0(%rip), %xmm0  # xmm0 = [0,1,0,10]
	movups	%xmm0, 172(%rbx)
	movl	$1, 188(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 136(%rbx)
	movq	$0, 152(%rbx)
	movups	%xmm0, 196(%rbx)
	movq	$0, 212(%rbx)
.LBB21_2:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end21:
	.size	_ZN16btDbvtBroadphase9resetPoolEP12btDispatcher, .Lfunc_end21-_ZN16btDbvtBroadphase9resetPoolEP12btDispatcher
	.cfi_endproc

	.globl	_ZN16btDbvtBroadphase10printStatsEv
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphase10printStatsEv,@function
_ZN16btDbvtBroadphase10printStatsEv:    # @_ZN16btDbvtBroadphase10printStatsEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end22:
	.size	_ZN16btDbvtBroadphase10printStatsEv, .Lfunc_end22-_ZN16btDbvtBroadphase10printStatsEv
	.cfi_endproc

	.globl	_ZN16btDbvtBroadphase9benchmarkEP21btBroadphaseInterface
	.p2align	4, 0x90
	.type	_ZN16btDbvtBroadphase9benchmarkEP21btBroadphaseInterface,@function
_ZN16btDbvtBroadphase9benchmarkEP21btBroadphaseInterface: # @_ZN16btDbvtBroadphase9benchmarkEP21btBroadphaseInterface
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end23:
	.size	_ZN16btDbvtBroadphase9benchmarkEP21btBroadphaseInterface, .Lfunc_end23-_ZN16btDbvtBroadphase9benchmarkEP21btBroadphaseInterface
	.cfi_endproc

	.section	.text._ZN18btDbvtTreeColliderD0Ev,"axG",@progbits,_ZN18btDbvtTreeColliderD0Ev,comdat
	.weak	_ZN18btDbvtTreeColliderD0Ev
	.p2align	4, 0x90
	.type	_ZN18btDbvtTreeColliderD0Ev,@function
_ZN18btDbvtTreeColliderD0Ev:            # @_ZN18btDbvtTreeColliderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end24:
	.size	_ZN18btDbvtTreeColliderD0Ev, .Lfunc_end24-_ZN18btDbvtTreeColliderD0Ev
	.cfi_endproc

	.section	.text._ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNodeS2_,"axG",@progbits,_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNodeS2_,comdat
	.weak	_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNodeS2_
	.p2align	4, 0x90
	.type	_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNodeS2_,@function
_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNodeS2_: # @_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNodeS2_
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi166:
	.cfi_def_cfa_offset 16
.Lcfi167:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpq	%rdx, %rsi
	je	.LBB25_2
# BB#1:
	movq	40(%rsi), %rsi
	movq	40(%rdx), %rdx
	movq	8(%rbx), %rax
	movq	160(%rax), %rdi
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	8(%rbx), %rax
	incl	188(%rax)
.LBB25_2:
	popq	%rbx
	retq
.Lfunc_end25:
	.size	_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNodeS2_, .Lfunc_end25-_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNodeS2_
	.cfi_endproc

	.section	.text._ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNode,"axG",@progbits,_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNode,comdat
	.weak	_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNode
	.p2align	4, 0x90
	.type	_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNode,@function
_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNode: # @_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNode
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	16(%rdi), %rcx
	movq	16(%rax), %rax
	movq	64(%rcx), %rdx
	jmpq	*%rax                   # TAILCALL
.Lfunc_end26:
	.size	_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNode, .Lfunc_end26-_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNode
	.cfi_endproc

	.section	.text._ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef,"axG",@progbits,_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef,comdat
	.weak	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef
	.p2align	4, 0x90
	.type	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef,@function
_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef: # @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end27:
	.size	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef, .Lfunc_end27-_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef
	.cfi_endproc

	.section	.text._ZN6btDbvt8ICollide7DescentEPK10btDbvtNode,"axG",@progbits,_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode,comdat
	.weak	_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode
	.p2align	4, 0x90
	.type	_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode,@function
_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode: # @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode
	.cfi_startproc
# BB#0:
	movb	$1, %al
	retq
.Lfunc_end28:
	.size	_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode, .Lfunc_end28-_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode
	.cfi_endproc

	.section	.text._ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode,"axG",@progbits,_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode,comdat
	.weak	_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode
	.p2align	4, 0x90
	.type	_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode,@function
_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode: # @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode
	.cfi_startproc
# BB#0:
	movb	$1, %al
	retq
.Lfunc_end29:
	.size	_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode, .Lfunc_end29-_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode
	.cfi_endproc

	.section	.text._ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_,"axG",@progbits,_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_,comdat
	.weak	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_
	.p2align	4, 0x90
	.type	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_,@function
_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_: # @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end30:
	.size	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_, .Lfunc_end30-_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_
	.cfi_endproc

	.section	.text._ZN19BroadphaseRayTesterD0Ev,"axG",@progbits,_ZN19BroadphaseRayTesterD0Ev,comdat
	.weak	_ZN19BroadphaseRayTesterD0Ev
	.p2align	4, 0x90
	.type	_ZN19BroadphaseRayTesterD0Ev,@function
_ZN19BroadphaseRayTesterD0Ev:           # @_ZN19BroadphaseRayTesterD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end31:
	.size	_ZN19BroadphaseRayTesterD0Ev, .Lfunc_end31-_ZN19BroadphaseRayTesterD0Ev
	.cfi_endproc

	.section	.text._ZN19BroadphaseRayTester7ProcessEPK10btDbvtNode,"axG",@progbits,_ZN19BroadphaseRayTester7ProcessEPK10btDbvtNode,comdat
	.weak	_ZN19BroadphaseRayTester7ProcessEPK10btDbvtNode
	.p2align	4, 0x90
	.type	_ZN19BroadphaseRayTester7ProcessEPK10btDbvtNode,@function
_ZN19BroadphaseRayTester7ProcessEPK10btDbvtNode: # @_ZN19BroadphaseRayTester7ProcessEPK10btDbvtNode
	.cfi_startproc
# BB#0:
	movq	40(%rsi), %rsi
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end32:
	.size	_ZN19BroadphaseRayTester7ProcessEPK10btDbvtNode, .Lfunc_end32-_ZN19BroadphaseRayTester7ProcessEPK10btDbvtNode
	.cfi_endproc

	.section	.text._ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii,"axG",@progbits,_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii,comdat
	.weak	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii,@function
_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii: # @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi168:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi169:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi170:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi171:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi172:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi173:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi174:
	.cfi_def_cfa_offset 64
.Lcfi175:
	.cfi_offset %rbx, -56
.Lcfi176:
	.cfi_offset %r12, -48
.Lcfi177:
	.cfi_offset %r13, -40
.Lcfi178:
	.cfi_offset %r14, -32
.Lcfi179:
	.cfi_offset %r15, -24
.Lcfi180:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r12d
	movq	%rdi, %r15
	movq	%rdx, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB33_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_2 Depth 2
                                        #       Child Loop BB33_20 Depth 3
                                        #       Child Loop BB33_4 Depth 3
                                        #       Child Loop BB33_57 Depth 3
                                        #       Child Loop BB33_33 Depth 3
	movl	%r12d, %esi
	movq	16(%r15), %r8
	leal	(%rsi,%rdx), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	shlq	$5, %rax
	movq	(%r8,%rax), %r10
	movq	8(%r8,%rax), %r13
	movq	16(%r8,%rax), %r9
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	jmp	.LBB33_2
	.p2align	4, 0x90
.LBB33_48:                              # %._crit_edge
                                        #   in Loop: Header=BB33_2 Depth=2
	movq	16(%r15), %r8
.LBB33_2:                               #   Parent Loop BB33_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB33_20 Depth 3
                                        #       Child Loop BB33_4 Depth 3
                                        #       Child Loop BB33_57 Depth 3
                                        #       Child Loop BB33_33 Depth 3
	movslq	%r12d, %r12
	testq	%r10, %r10
	je	.LBB33_3
# BB#19:                                # %.split.preheader
                                        #   in Loop: Header=BB33_2 Depth=2
	movl	24(%r10), %r11d
	movq	%r12, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rax
	jmp	.LBB33_20
	.p2align	4, 0x90
.LBB33_55:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread
                                        #   in Loop: Header=BB33_20 Depth=3
	incq	%r12
	addq	$32, %rax
.LBB33_20:                              # %.split
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rax), %rdi
	movl	$-1, %ebx
	testq	%rdi, %rdi
	movl	$-1, %ebp
	je	.LBB33_22
# BB#21:                                #   in Loop: Header=BB33_20 Depth=3
	movl	24(%rdi), %ebp
.LBB33_22:                              #   in Loop: Header=BB33_20 Depth=3
	movq	-8(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB33_24
# BB#23:                                #   in Loop: Header=BB33_20 Depth=3
	movl	24(%rcx), %ebx
.LBB33_24:                              #   in Loop: Header=BB33_20 Depth=3
	testq	%r13, %r13
	je	.LBB33_25
# BB#26:                                #   in Loop: Header=BB33_20 Depth=3
	movl	24(%r13), %r14d
	cmpl	%r11d, %ebp
	jg	.LBB33_55
	jmp	.LBB33_28
	.p2align	4, 0x90
.LBB33_25:                              #   in Loop: Header=BB33_20 Depth=3
	movl	$-1, %r14d
	cmpl	%r11d, %ebp
	jg	.LBB33_55
.LBB33_28:                              #   in Loop: Header=BB33_20 Depth=3
	cmpq	%r10, %rdi
	setne	%bpl
	cmpl	%r14d, %ebx
	jg	.LBB33_53
# BB#29:                                #   in Loop: Header=BB33_20 Depth=3
	testb	%bpl, %bpl
	jne	.LBB33_53
# BB#30:                                #   in Loop: Header=BB33_20 Depth=3
	cmpq	%r13, %rcx
	jne	.LBB33_31
# BB#52:                                #   in Loop: Header=BB33_20 Depth=3
	cmpq	%r9, (%rax)
	ja	.LBB33_55
	jmp	.LBB33_31
	.p2align	4, 0x90
.LBB33_53:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit
                                        #   in Loop: Header=BB33_20 Depth=3
	cmpq	%r10, %rdi
	jne	.LBB33_31
# BB#54:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit
                                        #   in Loop: Header=BB33_20 Depth=3
	cmpl	%r14d, %ebx
	jg	.LBB33_55
	jmp	.LBB33_31
	.p2align	4, 0x90
.LBB33_3:                               # %.split.us.preheader
                                        #   in Loop: Header=BB33_2 Depth=2
	movq	%r12, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rdi
	jmp	.LBB33_4
	.p2align	4, 0x90
.LBB33_18:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread.us
                                        #   in Loop: Header=BB33_4 Depth=3
	incq	%r12
	addq	$32, %rdi
.LBB33_4:                               # %.split.us
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rdi), %rbx
	movl	$-1, %r11d
	testq	%rbx, %rbx
	movl	$-1, %eax
	je	.LBB33_6
# BB#5:                                 #   in Loop: Header=BB33_4 Depth=3
	movl	24(%rbx), %eax
.LBB33_6:                               #   in Loop: Header=BB33_4 Depth=3
	movq	-8(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB33_8
# BB#7:                                 #   in Loop: Header=BB33_4 Depth=3
	movl	24(%rcx), %r11d
.LBB33_8:                               #   in Loop: Header=BB33_4 Depth=3
	testq	%r13, %r13
	je	.LBB33_9
# BB#10:                                #   in Loop: Header=BB33_4 Depth=3
	movl	24(%r13), %ebp
	testl	%eax, %eax
	jns	.LBB33_18
	jmp	.LBB33_12
	.p2align	4, 0x90
.LBB33_9:                               #   in Loop: Header=BB33_4 Depth=3
	movl	$-1, %ebp
	testl	%eax, %eax
	jns	.LBB33_18
.LBB33_12:                              #   in Loop: Header=BB33_4 Depth=3
	testq	%rbx, %rbx
	setne	%al
	cmpl	%ebp, %r11d
	jg	.LBB33_16
# BB#13:                                #   in Loop: Header=BB33_4 Depth=3
	testb	%al, %al
	jne	.LBB33_16
# BB#14:                                #   in Loop: Header=BB33_4 Depth=3
	cmpq	%r13, %rcx
	jne	.LBB33_31
# BB#15:                                #   in Loop: Header=BB33_4 Depth=3
	cmpq	%r9, (%rdi)
	ja	.LBB33_18
	jmp	.LBB33_31
.LBB33_16:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.us
                                        #   in Loop: Header=BB33_4 Depth=3
	testq	%rbx, %rbx
	jne	.LBB33_31
# BB#17:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.us
                                        #   in Loop: Header=BB33_4 Depth=3
	cmpl	%ebp, %r11d
	jg	.LBB33_18
	.p2align	4, 0x90
.LBB33_31:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.preheader
                                        #   in Loop: Header=BB33_2 Depth=2
	movslq	%edx, %rdx
	testq	%r10, %r10
	je	.LBB33_32
# BB#56:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.preheader47
                                        #   in Loop: Header=BB33_2 Depth=2
	movl	24(%r10), %r11d
	movq	%rdx, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rax
	jmp	.LBB33_57
	.p2align	4, 0x90
.LBB33_71:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread
                                        #   in Loop: Header=BB33_57 Depth=3
	decq	%rdx
	addq	$-32, %rax
.LBB33_57:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rax), %rdi
	movl	$-1, %r14d
	testq	%rdi, %rdi
	movl	$-1, %ecx
	je	.LBB33_59
# BB#58:                                #   in Loop: Header=BB33_57 Depth=3
	movl	24(%rdi), %ecx
.LBB33_59:                              #   in Loop: Header=BB33_57 Depth=3
	testq	%r13, %r13
	je	.LBB33_61
# BB#60:                                #   in Loop: Header=BB33_57 Depth=3
	movl	24(%r13), %r14d
.LBB33_61:                              #   in Loop: Header=BB33_57 Depth=3
	movq	-8(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB33_62
# BB#63:                                #   in Loop: Header=BB33_57 Depth=3
	movl	24(%rbp), %ebx
	cmpl	%ecx, %r11d
	jg	.LBB33_71
	jmp	.LBB33_65
	.p2align	4, 0x90
.LBB33_62:                              #   in Loop: Header=BB33_57 Depth=3
	movl	$-1, %ebx
	cmpl	%ecx, %r11d
	jg	.LBB33_71
.LBB33_65:                              #   in Loop: Header=BB33_57 Depth=3
	cmpq	%rdi, %r10
	setne	%cl
	cmpl	%ebx, %r14d
	jg	.LBB33_69
# BB#66:                                #   in Loop: Header=BB33_57 Depth=3
	testb	%cl, %cl
	jne	.LBB33_69
# BB#67:                                #   in Loop: Header=BB33_57 Depth=3
	cmpq	%rbp, %r13
	jne	.LBB33_45
# BB#68:                                #   in Loop: Header=BB33_57 Depth=3
	cmpq	(%rax), %r9
	ja	.LBB33_71
	jmp	.LBB33_45
	.p2align	4, 0x90
.LBB33_69:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32
                                        #   in Loop: Header=BB33_57 Depth=3
	cmpq	%rdi, %r10
	jne	.LBB33_45
# BB#70:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32
                                        #   in Loop: Header=BB33_57 Depth=3
	cmpl	%ebx, %r14d
	jg	.LBB33_71
	jmp	.LBB33_45
	.p2align	4, 0x90
.LBB33_32:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.us.preheader
                                        #   in Loop: Header=BB33_2 Depth=2
	movq	%rdx, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rax
	jmp	.LBB33_33
	.p2align	4, 0x90
.LBB33_51:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread.us
                                        #   in Loop: Header=BB33_33 Depth=3
	decq	%rdx
	addq	$-32, %rax
.LBB33_33:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.us
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rax), %rbx
	movl	$-1, %r11d
	testq	%rbx, %rbx
	movl	$-1, %ecx
	je	.LBB33_35
# BB#34:                                #   in Loop: Header=BB33_33 Depth=3
	movl	24(%rbx), %ecx
.LBB33_35:                              #   in Loop: Header=BB33_33 Depth=3
	testq	%r13, %r13
	je	.LBB33_37
# BB#36:                                #   in Loop: Header=BB33_33 Depth=3
	movl	24(%r13), %r11d
.LBB33_37:                              #   in Loop: Header=BB33_33 Depth=3
	movq	-8(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB33_38
# BB#39:                                #   in Loop: Header=BB33_33 Depth=3
	movl	24(%rbp), %edi
	cmpl	$-1, %ecx
	jl	.LBB33_51
	jmp	.LBB33_41
	.p2align	4, 0x90
.LBB33_38:                              #   in Loop: Header=BB33_33 Depth=3
	movl	$-1, %edi
	cmpl	$-1, %ecx
	jl	.LBB33_51
.LBB33_41:                              #   in Loop: Header=BB33_33 Depth=3
	testq	%rbx, %rbx
	setne	%cl
	cmpl	%edi, %r11d
	jg	.LBB33_49
# BB#42:                                #   in Loop: Header=BB33_33 Depth=3
	testb	%cl, %cl
	jne	.LBB33_49
# BB#43:                                #   in Loop: Header=BB33_33 Depth=3
	cmpq	%rbp, %r13
	jne	.LBB33_45
# BB#44:                                #   in Loop: Header=BB33_33 Depth=3
	cmpq	(%rax), %r9
	ja	.LBB33_51
	jmp	.LBB33_45
	.p2align	4, 0x90
.LBB33_49:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.us
                                        #   in Loop: Header=BB33_33 Depth=3
	testq	%rbx, %rbx
	jne	.LBB33_45
# BB#50:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.us
                                        #   in Loop: Header=BB33_33 Depth=3
	cmpl	%edi, %r11d
	jg	.LBB33_51
	.p2align	4, 0x90
.LBB33_45:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread41
                                        #   in Loop: Header=BB33_2 Depth=2
	cmpl	%edx, %r12d
	jg	.LBB33_47
# BB#46:                                #   in Loop: Header=BB33_2 Depth=2
	movq	%r12, %rax
	shlq	$5, %rax
	movups	(%r8,%rax), %xmm0
	movups	16(%r8,%rax), %xmm1
	movq	%rdx, %rcx
	shlq	$5, %rcx
	movups	(%r8,%rcx), %xmm2
	movups	16(%r8,%rcx), %xmm3
	movups	%xmm3, 16(%r8,%rax)
	movups	%xmm2, (%r8,%rax)
	movq	16(%r15), %rax
	movups	%xmm0, (%rax,%rcx)
	movups	%xmm1, 16(%rax,%rcx)
	leal	1(%r12), %r12d
	leal	-1(%rdx), %edx
.LBB33_47:                              #   in Loop: Header=BB33_2 Depth=2
	cmpl	%edx, %r12d
	jle	.LBB33_48
# BB#72:                                #   in Loop: Header=BB33_1 Depth=1
	cmpl	%esi, %edx
	jle	.LBB33_74
# BB#73:                                #   in Loop: Header=BB33_1 Depth=1
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
.LBB33_74:                              #   in Loop: Header=BB33_1 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	cmpl	%edx, %r12d
	jl	.LBB33_1
# BB#75:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end33:
	.size	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii, .Lfunc_end33-_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	.cfi_endproc

	.type	_ZTV16btDbvtBroadphase,@object # @_ZTV16btDbvtBroadphase
	.section	.rodata,"a",@progbits
	.globl	_ZTV16btDbvtBroadphase
	.p2align	3
_ZTV16btDbvtBroadphase:
	.quad	0
	.quad	_ZTI16btDbvtBroadphase
	.quad	_ZN16btDbvtBroadphaseD2Ev
	.quad	_ZN16btDbvtBroadphaseD0Ev
	.quad	_ZN16btDbvtBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_
	.quad	_ZN16btDbvtBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.quad	_ZN16btDbvtBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher
	.quad	_ZNK16btDbvtBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_
	.quad	_ZN16btDbvtBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_
	.quad	_ZN16btDbvtBroadphase25calculateOverlappingPairsEP12btDispatcher
	.quad	_ZN16btDbvtBroadphase23getOverlappingPairCacheEv
	.quad	_ZNK16btDbvtBroadphase23getOverlappingPairCacheEv
	.quad	_ZNK16btDbvtBroadphase17getBroadphaseAabbER9btVector3S1_
	.quad	_ZN16btDbvtBroadphase9resetPoolEP12btDispatcher
	.quad	_ZN16btDbvtBroadphase10printStatsEv
	.size	_ZTV16btDbvtBroadphase, 120

	.type	_ZTS16btDbvtBroadphase,@object # @_ZTS16btDbvtBroadphase
	.globl	_ZTS16btDbvtBroadphase
	.p2align	4
_ZTS16btDbvtBroadphase:
	.asciz	"16btDbvtBroadphase"
	.size	_ZTS16btDbvtBroadphase, 19

	.type	_ZTS21btBroadphaseInterface,@object # @_ZTS21btBroadphaseInterface
	.section	.rodata._ZTS21btBroadphaseInterface,"aG",@progbits,_ZTS21btBroadphaseInterface,comdat
	.weak	_ZTS21btBroadphaseInterface
	.p2align	4
_ZTS21btBroadphaseInterface:
	.asciz	"21btBroadphaseInterface"
	.size	_ZTS21btBroadphaseInterface, 24

	.type	_ZTI21btBroadphaseInterface,@object # @_ZTI21btBroadphaseInterface
	.section	.rodata._ZTI21btBroadphaseInterface,"aG",@progbits,_ZTI21btBroadphaseInterface,comdat
	.weak	_ZTI21btBroadphaseInterface
	.p2align	3
_ZTI21btBroadphaseInterface:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS21btBroadphaseInterface
	.size	_ZTI21btBroadphaseInterface, 16

	.type	_ZTI16btDbvtBroadphase,@object # @_ZTI16btDbvtBroadphase
	.section	.rodata,"a",@progbits
	.globl	_ZTI16btDbvtBroadphase
	.p2align	4
_ZTI16btDbvtBroadphase:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16btDbvtBroadphase
	.quad	_ZTI21btBroadphaseInterface
	.size	_ZTI16btDbvtBroadphase, 24

	.type	_ZTV18btDbvtTreeCollider,@object # @_ZTV18btDbvtTreeCollider
	.section	.rodata._ZTV18btDbvtTreeCollider,"aG",@progbits,_ZTV18btDbvtTreeCollider,comdat
	.weak	_ZTV18btDbvtTreeCollider
	.p2align	3
_ZTV18btDbvtTreeCollider:
	.quad	0
	.quad	_ZTI18btDbvtTreeCollider
	.quad	_ZN6btDbvt8ICollideD2Ev
	.quad	_ZN18btDbvtTreeColliderD0Ev
	.quad	_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNodeS2_
	.quad	_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNode
	.quad	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef
	.quad	_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode
	.quad	_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode
	.size	_ZTV18btDbvtTreeCollider, 72

	.type	_ZTS18btDbvtTreeCollider,@object # @_ZTS18btDbvtTreeCollider
	.section	.rodata._ZTS18btDbvtTreeCollider,"aG",@progbits,_ZTS18btDbvtTreeCollider,comdat
	.weak	_ZTS18btDbvtTreeCollider
	.p2align	4
_ZTS18btDbvtTreeCollider:
	.asciz	"18btDbvtTreeCollider"
	.size	_ZTS18btDbvtTreeCollider, 21

	.type	_ZTSN6btDbvt8ICollideE,@object # @_ZTSN6btDbvt8ICollideE
	.section	.rodata._ZTSN6btDbvt8ICollideE,"aG",@progbits,_ZTSN6btDbvt8ICollideE,comdat
	.weak	_ZTSN6btDbvt8ICollideE
	.p2align	4
_ZTSN6btDbvt8ICollideE:
	.asciz	"N6btDbvt8ICollideE"
	.size	_ZTSN6btDbvt8ICollideE, 19

	.type	_ZTIN6btDbvt8ICollideE,@object # @_ZTIN6btDbvt8ICollideE
	.section	.rodata._ZTIN6btDbvt8ICollideE,"aG",@progbits,_ZTIN6btDbvt8ICollideE,comdat
	.weak	_ZTIN6btDbvt8ICollideE
	.p2align	3
_ZTIN6btDbvt8ICollideE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6btDbvt8ICollideE
	.size	_ZTIN6btDbvt8ICollideE, 16

	.type	_ZTI18btDbvtTreeCollider,@object # @_ZTI18btDbvtTreeCollider
	.section	.rodata._ZTI18btDbvtTreeCollider,"aG",@progbits,_ZTI18btDbvtTreeCollider,comdat
	.weak	_ZTI18btDbvtTreeCollider
	.p2align	4
_ZTI18btDbvtTreeCollider:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18btDbvtTreeCollider
	.quad	_ZTIN6btDbvt8ICollideE
	.size	_ZTI18btDbvtTreeCollider, 24

	.type	_ZTV19BroadphaseRayTester,@object # @_ZTV19BroadphaseRayTester
	.section	.rodata._ZTV19BroadphaseRayTester,"aG",@progbits,_ZTV19BroadphaseRayTester,comdat
	.weak	_ZTV19BroadphaseRayTester
	.p2align	3
_ZTV19BroadphaseRayTester:
	.quad	0
	.quad	_ZTI19BroadphaseRayTester
	.quad	_ZN6btDbvt8ICollideD2Ev
	.quad	_ZN19BroadphaseRayTesterD0Ev
	.quad	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_
	.quad	_ZN19BroadphaseRayTester7ProcessEPK10btDbvtNode
	.quad	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef
	.quad	_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode
	.quad	_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode
	.size	_ZTV19BroadphaseRayTester, 72

	.type	_ZTS19BroadphaseRayTester,@object # @_ZTS19BroadphaseRayTester
	.section	.rodata._ZTS19BroadphaseRayTester,"aG",@progbits,_ZTS19BroadphaseRayTester,comdat
	.weak	_ZTS19BroadphaseRayTester
	.p2align	4
_ZTS19BroadphaseRayTester:
	.asciz	"19BroadphaseRayTester"
	.size	_ZTS19BroadphaseRayTester, 22

	.type	_ZTI19BroadphaseRayTester,@object # @_ZTI19BroadphaseRayTester
	.section	.rodata._ZTI19BroadphaseRayTester,"aG",@progbits,_ZTI19BroadphaseRayTester,comdat
	.weak	_ZTI19BroadphaseRayTester
	.p2align	4
_ZTI19BroadphaseRayTester:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19BroadphaseRayTester
	.quad	_ZTIN6btDbvt8ICollideE
	.size	_ZTI19BroadphaseRayTester, 24


	.globl	_ZN16btDbvtBroadphaseC1EP22btOverlappingPairCache
	.type	_ZN16btDbvtBroadphaseC1EP22btOverlappingPairCache,@function
_ZN16btDbvtBroadphaseC1EP22btOverlappingPairCache = _ZN16btDbvtBroadphaseC2EP22btOverlappingPairCache
	.globl	_ZN16btDbvtBroadphaseD1Ev
	.type	_ZN16btDbvtBroadphaseD1Ev,@function
_ZN16btDbvtBroadphaseD1Ev = _ZN16btDbvtBroadphaseD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
