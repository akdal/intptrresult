	.text
	.file	"ffbench.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4756540486875873280     # double 1.0E+10
.LCPI0_1:
	.quad	-4466831549978902528    # double -1.0E+10
.LCPI0_2:
	.quad	4643176031446892544     # double 255
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movabsq	$1099511628032, %rax    # imm = 0x10000000100
	movq	%rax, main.nsize+4(%rip)
	movl	$1048592, %edi          # imm = 0x100010
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_27
# BB#1:
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movl	$1048592, %edx          # imm = 0x100010
	movq	%r15, %rdi
	callq	memset
	leaq	8(%r15), %rax
	movabsq	$4638707616191610880, %rcx # imm = 0x4060000000000000
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader130
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
                                        #     Child Loop BB0_8 Depth 2
	movl	%ebx, %edx
	andl	$15, %edx
	cmpq	$8, %rdx
	jne	.LBB0_3
# BB#7:                                 # %.preheader130.split.us.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_8:                                # %.preheader130.split.us
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, (%rax,%rdx)
	movq	%rcx, 16(%rax,%rdx)
	movq	%rcx, 32(%rax,%rdx)
	movq	%rcx, 48(%rax,%rdx)
	addq	$64, %rdx
	cmpq	$4096, %rdx             # imm = 0x1000
	jne	.LBB0_8
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader130.split.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_4:                                # %.preheader130.split
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edi
	andl	$14, %edi
	cmpq	$8, %rdi
	jne	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=2
	movq	%rcx, (%rdx)
.LBB0_6:                                #   in Loop: Header=BB0_4 Depth=2
	addq	$2, %rsi
	addq	$32, %rdx
	cmpq	$256, %rsi              # imm = 0x100
	jne	.LBB0_4
.LBB0_9:                                # %.us-lcssa143.us
                                        #   in Loop: Header=BB0_2 Depth=1
	incq	%rbx
	addq	$4096, %rax             # imm = 0x1000
	cmpq	$256, %rbx              # imm = 0x100
	jne	.LBB0_2
# BB#10:                                # %.preheader129.preheader
	movl	$63, %ebx
	.p2align	4, 0x90
.LBB0_11:                               # %.preheader129
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movq	%r15, %rdi
	callq	fourn
	movl	$-1, %esi
	movq	%r15, %rdi
	callq	fourn
	decl	%ebx
	jne	.LBB0_11
# BB#12:                                # %.preheader128.preheader
	movsd	.LCPI0_0(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movl	$1, %eax
	.p2align	4, 0x90
.LBB0_13:                               # %.preheader128
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r15,%rax,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	cmplesd	%xmm4, %xmm2
	movapd	%xmm2, %xmm3
	andpd	%xmm1, %xmm3
	andnpd	%xmm4, %xmm2
	orpd	%xmm3, %xmm2
	maxsd	%xmm0, %xmm1
	movsd	16(%r15,%rax,8), %xmm0  # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm3
	cmplesd	%xmm2, %xmm3
	movapd	%xmm3, %xmm4
	andnpd	%xmm2, %xmm4
	andpd	%xmm0, %xmm3
	orpd	%xmm4, %xmm3
	maxsd	%xmm1, %xmm0
	movsd	32(%r15,%rax,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	cmplesd	%xmm3, %xmm2
	movapd	%xmm2, %xmm4
	andnpd	%xmm3, %xmm4
	andpd	%xmm1, %xmm2
	orpd	%xmm4, %xmm2
	maxsd	%xmm0, %xmm1
	movsd	48(%r15,%rax,8), %xmm0  # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm4
	cmplesd	%xmm2, %xmm4
	movapd	%xmm4, %xmm3
	andnpd	%xmm2, %xmm3
	andpd	%xmm0, %xmm4
	orpd	%xmm3, %xmm4
	maxsd	%xmm1, %xmm0
	addq	$8, %rax
	cmpq	$65537, %rax            # imm = 0x10001
	jl	.LBB0_13
# BB#14:
	subsd	%xmm4, %xmm0
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	addq	$8, %r15
	movl	$255, %r12d
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_15:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_16 Depth 2
                                        #     Child Loop BB0_19 Depth 2
	movl	%r13d, %eax
	andl	$15, %eax
	movq	%r15, %rbp
	xorl	%ebx, %ebx
	cmpl	$8, %eax
	je	.LBB0_19
	jmp	.LBB0_16
.LBB0_20:                               #   in Loop: Header=BB0_19 Depth=2
	incl	%r14d
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	movl	$255, %r8d
	xorl	%eax, %eax
	movl	%r13d, %edx
	movl	%ebx, %ecx
	callq	fprintf
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	jmp	.LBB0_21
	.p2align	4, 0x90
.LBB0_19:                               # %.preheader.split.us
                                        #   Parent Loop BB0_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	subsd	%xmm4, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %r9d
	cmpl	$255, %r9d
	jne	.LBB0_20
.LBB0_21:                               #   in Loop: Header=BB0_19 Depth=2
	incq	%rbx
	addq	$16, %rbp
	cmpq	$256, %rbx              # imm = 0x100
	jne	.LBB0_19
	jmp	.LBB0_22
.LBB0_17:                               #   in Loop: Header=BB0_16 Depth=2
	incl	%r14d
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	movl	%ebx, %ecx
	callq	fprintf
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	jmp	.LBB0_18
	.p2align	4, 0x90
.LBB0_16:                               # %.preheader.split
                                        #   Parent Loop BB0_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	subsd	%xmm4, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %r9d
	movl	%ebx, %eax
	andl	$15, %eax
	cmpl	$8, %eax
	movl	$0, %r8d
	cmovel	%r12d, %r8d
	cmpl	%r8d, %r9d
	jne	.LBB0_17
.LBB0_18:                               #   in Loop: Header=BB0_16 Depth=2
	incq	%rbx
	addq	$16, %rbp
	cmpq	$256, %rbx              # imm = 0x100
	jne	.LBB0_16
.LBB0_22:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB0_15 Depth=1
	incq	%r13
	addq	$4096, %r15             # imm = 0x1000
	cmpq	$256, %r13              # imm = 0x100
	jne	.LBB0_15
# BB#23:
	movq	stderr(%rip), %rdi
	testl	%r14d, %r14d
	jne	.LBB0_25
# BB#24:
	movl	$.L.str.2, %esi
	movl	$63, %edx
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB0_26
.LBB0_25:
	movl	$.L.str.3, %esi
	movl	$63, %edx
	xorl	%eax, %eax
	movl	%r14d, %ecx
	callq	fprintf
.LBB0_26:
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_27:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4618760256179416348     # double 6.2831853071795898
.LCPI1_1:
	.quad	4602678819172646912     # double 0.5
.LCPI1_2:
	.quad	-4611686018427387904    # double -2
.LCPI1_3:
	.quad	4607182418800017408     # double 1
	.text
	.p2align	4, 0x90
	.type	fourn,@function
fourn:                                  # @fourn
	.cfi_startproc
# BB#0:                                 # %.preheader642
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 128
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	main.nsize+8(%rip), %r12d
	movl	main.nsize+4(%rip), %eax
	imull	%r12d, %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	cvtsi2sdl	%esi, %xmm0
	mulsd	.LCPI1_0(%rip), %xmm0
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	movl	$1, %ecx
	movl	$2, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_26:                               # %._crit_edge23
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpq	$2, %rax
	jl	.LBB1_28
# BB#27:                                # %._crit_edge23._crit_edge
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	main.nsize-4(,%rax,4), %r12d
	decq	%rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	36(%rsp), %ecx          # 4-byte Reload
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
                                        #       Child Loop BB1_6 Depth 3
                                        #         Child Loop BB1_8 Depth 4
                                        #       Child Loop BB1_11 Depth 3
                                        #     Child Loop BB1_17 Depth 2
                                        #       Child Loop BB1_19 Depth 3
                                        #         Child Loop BB1_21 Depth 4
                                        #           Child Loop BB1_23 Depth 5
	movl	%ecx, %ebp
	movl	%r12d, %ecx
	imull	%ebp, %ecx
	movl	32(%rsp), %eax          # 4-byte Reload
	cltd
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	idivl	%ecx
	movl	%eax, %r13d
	addl	%ebp, %ebp
	imull	%ebp, %r12d
	imull	%r12d, %r13d
	testl	%r12d, %r12d
	jle	.LBB1_14
# BB#2:                                 # %.lr.ph12
                                        #   in Loop: Header=BB1_1 Depth=1
	leal	-2(%rbp), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movslq	%ebp, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	%r12d, %r10
	movslq	%r13d, %rsi
	movl	$1, %eax
	movl	$1, %edx
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_3:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_6 Depth 3
                                        #         Child Loop BB1_8 Depth 4
                                        #       Child Loop BB1_11 Depth 3
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	cmpl	%edx, %eax
	jle	.LBB1_10
# BB#4:                                 # %.preheader4
                                        #   in Loop: Header=BB1_3 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	leal	(%rcx,%rdx), %r11d
	cmpl	%r11d, %edx
	jg	.LBB1_10
# BB#5:                                 # %.preheader2.lr.ph
                                        #   in Loop: Header=BB1_3 Depth=2
	leal	1(%rax), %r14d
	movq	8(%rsp), %r15           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader2
                                        #   Parent Loop BB1_1 Depth=1
                                        #     Parent Loop BB1_3 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_8 Depth 4
	cmpl	%r13d, %r15d
	jg	.LBB1_9
# BB#7:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_6 Depth=3
	movl	%r14d, %edi
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph
                                        #   Parent Loop BB1_1 Depth=1
                                        #     Parent Loop BB1_3 Depth=2
                                        #       Parent Loop BB1_6 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	-1(%rdi), %ebp
	movq	(%rbx,%rcx,8), %r8
	movslq	%ebp, %rbp
	movq	(%rbx,%rbp,8), %r9
	movq	%r9, (%rbx,%rcx,8)
	movq	%r8, (%rbx,%rbp,8)
	movq	8(%rbx,%rcx,8), %rbp
	movslq	%edi, %rdi
	movq	(%rbx,%rdi,8), %rdx
	movq	%rdx, 8(%rbx,%rcx,8)
	movq	%rbp, (%rbx,%rdi,8)
	addq	%r10, %rcx
	addl	%r12d, %edi
	cmpq	%rsi, %rcx
	jle	.LBB1_8
.LBB1_9:                                # %._crit_edge
                                        #   in Loop: Header=BB1_6 Depth=3
	addq	$2, %r15
	addl	$2, %r14d
	cmpl	%r11d, %r15d
	jle	.LBB1_6
.LBB1_10:                               # %.preheader3.preheader
                                        #   in Loop: Header=BB1_3 Depth=2
	movl	%r12d, %edi
	movq	64(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_11:                               # %.preheader3
                                        #   Parent Loop BB1_1 Depth=1
                                        #     Parent Loop BB1_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, %ecx
	sarl	%edi
	subl	%edi, %eax
	jle	.LBB1_13
# BB#12:                                # %.preheader3
                                        #   in Loop: Header=BB1_11 Depth=3
	cmpl	%ebp, %edi
	jge	.LBB1_11
.LBB1_13:                               #   in Loop: Header=BB1_3 Depth=2
	addl	%edi, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	addq	24(%rsp), %rdx          # 8-byte Folded Reload
	cmpl	%r12d, %edx
	movl	%ecx, %eax
	jle	.LBB1_3
.LBB1_14:                               # %.preheader5
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpl	%r12d, %ebp
	jge	.LBB1_26
# BB#15:                                # %.lr.ph22.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	movslq	%ebp, %r9
	movslq	%r13d, %r15
	movl	%ebp, %r10d
	movq	%r9, 56(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph22
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_19 Depth 3
                                        #         Child Loop BB1_21 Depth 4
                                        #           Child Loop BB1_23 Depth 5
	movl	%r10d, %r14d
	leal	(%r14,%r14), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	cltd
	idivl	%ebp
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	48(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	movapd	%xmm1, %xmm0
	mulsd	.LCPI1_1(%rip), %xmm0
	callq	sin
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movl	8(%rsp), %r10d          # 4-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	testl	%r14d, %r14d
	jle	.LBB1_16
# BB#18:                                # %.preheader1.preheader
                                        #   in Loop: Header=BB1_17 Depth=2
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm1
	mulsd	.LCPI1_2(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	movslq	%r10d, %rax
	movslq	%r14d, %rcx
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	leaq	8(%rbx,%rcx,8), %rcx
	movl	$1, %edx
	movsd	.LCPI1_3(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB1_19:                               # %.preheader1
                                        #   Parent Loop BB1_1 Depth=1
                                        #     Parent Loop BB1_17 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_21 Depth 4
                                        #           Child Loop BB1_23 Depth 5
	leaq	(%rdx,%r9), %r8
	leal	-2(%r8), %edi
	cmpl	%edi, %edx
	jg	.LBB1_25
# BB#20:                                # %.preheader.preheader
                                        #   in Loop: Header=BB1_19 Depth=3
	movapd	%xmm2, %xmm3
	shufpd	$1, %xmm3, %xmm3        # xmm3 = xmm3[1,0]
	.p2align	4, 0x90
.LBB1_21:                               # %.preheader
                                        #   Parent Loop BB1_1 Depth=1
                                        #     Parent Loop BB1_17 Depth=2
                                        #       Parent Loop BB1_19 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB1_23 Depth 5
	cmpl	%r13d, %edx
	jg	.LBB1_24
# BB#22:                                # %.lr.ph14.preheader
                                        #   in Loop: Header=BB1_21 Depth=4
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph14
                                        #   Parent Loop BB1_1 Depth=1
                                        #     Parent Loop BB1_17 Depth=2
                                        #       Parent Loop BB1_19 Depth=3
                                        #         Parent Loop BB1_21 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movsd	(%rcx,%rsi,8), %xmm4    # xmm4 = mem[0],zero
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm3, %xmm4
	movsd	-8(%rcx,%rsi,8), %xmm5  # xmm5 = mem[0],zero
	movlhps	%xmm5, %xmm5            # xmm5 = xmm5[0,0]
	mulpd	%xmm2, %xmm5
	movapd	%xmm5, %xmm6
	subpd	%xmm4, %xmm6
	addpd	%xmm4, %xmm5
	movapd	%xmm5, %xmm4
	movsd	%xmm6, %xmm4            # xmm4 = xmm6[0],xmm4[1]
	movsd	(%rbx,%rsi,8), %xmm7    # xmm7 = mem[0],zero
	subsd	%xmm6, %xmm7
	movsd	%xmm7, -8(%rcx,%rsi,8)
	movsd	8(%rbx,%rsi,8), %xmm6   # xmm6 = mem[0],zero
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	subsd	%xmm5, %xmm6
	movsd	%xmm6, (%rcx,%rsi,8)
	movupd	(%rbx,%rsi,8), %xmm5
	addpd	%xmm4, %xmm5
	movupd	%xmm5, (%rbx,%rsi,8)
	addq	%rax, %rsi
	cmpq	%r15, %rsi
	jle	.LBB1_23
.LBB1_24:                               # %._crit_edge15
                                        #   in Loop: Header=BB1_21 Depth=4
	addq	$2, %rdx
	cmpl	%edi, %edx
	jle	.LBB1_21
.LBB1_25:                               # %._crit_edge17
                                        #   in Loop: Header=BB1_19 Depth=3
	movaps	%xmm1, %xmm3
	mulpd	%xmm2, %xmm3
	movapd	%xmm2, %xmm4
	shufpd	$1, %xmm4, %xmm4        # xmm4 = xmm4[1,0]
	mulpd	%xmm0, %xmm4
	movapd	%xmm3, %xmm5
	subpd	%xmm4, %xmm5
	addpd	%xmm3, %xmm4
	movsd	%xmm5, %xmm4            # xmm4 = xmm5[0],xmm4[1]
	addpd	%xmm4, %xmm2
	cmpl	%r14d, %r8d
	movq	%r8, %rdx
	jle	.LBB1_19
.LBB1_16:                               # %.loopexit
                                        #   in Loop: Header=BB1_17 Depth=2
	cmpl	%r12d, %r10d
	jl	.LBB1_17
	jmp	.LBB1_26
.LBB1_28:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	fourn, .Lfunc_end1-fourn
	.cfi_endproc

	.type	main.nsize,@object      # @main.nsize
	.local	main.nsize
	.comm	main.nsize,12,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Can't allocate data array.\n"
	.size	.L.str, 28

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Wrong answer at (%d,%d)!  Expected %d, got %d.\n"
	.size	.L.str.1, 48

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%d passes.  No errors in results.\n"
	.size	.L.str.2, 35

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%d passes.  %d errors in results.\n"
	.size	.L.str.3, 35


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
