	.text
	.file	"point_relax.bc"
	.globl	hypre_PointRelaxCreate
	.p2align	4, 0x90
	.type	hypre_PointRelaxCreate,@function
hypre_PointRelaxCreate:                 # @hypre_PointRelaxCreate
	.cfi_startproc
# BB#0:                                 # %hypre_PointRelaxSetPointset.exit20
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	$1, %edi
	movl	$144, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movl	%ebp, (%rbx)
	movl	$.L.str, %edi
	callq	hypre_InitializeTiming
	movl	%eax, 132(%rbx)
	movabsq	$4517329193108106637, %rax # imm = 0x3EB0C6F7A0B5ED8D
	movq	%rax, 8(%rbx)
	movl	$1000, 16(%rbx)         # imm = 0x3E8
	movl	$0, 20(%rbx)
	movl	$0, 24(%rbx)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 32(%rbx)
	movl	$0, 40(%rbx)
	movq	$0, 104(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	hypre_PointRelaxSetNumPointsets
	movq	72(%rbx), %rax
	movq	(%rax), %rdi
	callq	hypre_Free
	movq	72(%rbx), %rax
	movq	$0, (%rax)
	movl	$12, %edi
	callq	hypre_MAlloc
	movq	72(%rbx), %rcx
	movq	%rax, (%rcx)
	movq	48(%rbx), %rax
	movl	$1, (%rax)
	movq	64(%rbx), %rax
	movl	$1, (%rax)
	movl	$1, 4(%rax)
	movl	$1, 8(%rax)
	movq	72(%rbx), %rax
	movq	(%rax), %rax
	movq	$0, (%rax)
	movl	$0, 8(%rax)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_PointRelaxCreate, .Lfunc_end0-hypre_PointRelaxCreate
	.cfi_endproc

	.globl	hypre_PointRelaxSetNumPointsets
	.p2align	4, 0x90
	.type	hypre_PointRelaxSetNumPointsets,@function
hypre_PointRelaxSetNumPointsets:        # @hypre_PointRelaxSetNumPointsets
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r12, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %r15, -16
	movl	%esi, %r14d
	movq	%rdi, %r12
	cmpl	$0, 40(%r12)
	jle	.LBB1_3
# BB#1:                                 # %.lr.ph43.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph43
                                        # =>This Inner Loop Header: Depth=1
	movq	72(%r12), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_Free
	movq	72(%r12), %rax
	movq	$0, (%rax,%rbx,8)
	incq	%rbx
	movslq	40(%r12), %rax
	cmpq	%rax, %rbx
	jl	.LBB1_2
.LBB1_3:                                # %._crit_edge44
	movq	48(%r12), %rdi
	callq	hypre_Free
	movq	$0, 48(%r12)
	movq	56(%r12), %rdi
	callq	hypre_Free
	movq	$0, 56(%r12)
	movq	64(%r12), %rdi
	callq	hypre_Free
	movq	$0, 64(%r12)
	movq	72(%r12), %rdi
	callq	hypre_Free
	movq	$0, 72(%r12)
	movl	%r14d, 40(%r12)
	leal	(,%r14,4), %r15d
	movl	%r15d, %edi
	callq	hypre_MAlloc
	movq	%rax, 48(%r12)
	movl	%r15d, %edi
	callq	hypre_MAlloc
	movq	%rax, 56(%r12)
	leal	(%r15,%r15,2), %edi
	callq	hypre_MAlloc
	movq	%rax, 64(%r12)
	leal	(,%r14,8), %edi
	callq	hypre_MAlloc
	movq	%rax, 72(%r12)
	testl	%r14d, %r14d
	jle	.LBB1_10
# BB#4:                                 # %.lr.ph.preheader
	movq	48(%r12), %rcx
	movl	$0, (%rcx)
	movq	56(%r12), %rcx
	movl	$0, (%rcx)
	movq	$0, (%rax)
	cmpl	$1, %r14d
	je	.LBB1_10
# BB#5:                                 # %.lr.ph..lr.ph_crit_edge.preheader
	movl	%r14d, %eax
	testb	$1, %al
	jne	.LBB1_6
# BB#7:                                 # %.lr.ph..lr.ph_crit_edge.prol
	movq	48(%r12), %rcx
	movq	72(%r12), %rdx
	movl	$0, 4(%rcx)
	movq	56(%r12), %rcx
	movl	$1, 4(%rcx)
	movq	$0, 8(%rdx)
	movl	$2, %ecx
	cmpl	$2, %r14d
	jne	.LBB1_9
	jmp	.LBB1_10
.LBB1_6:
	movl	$1, %ecx
	cmpl	$2, %r14d
	je	.LBB1_10
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%r12), %rdx
	movq	72(%r12), %rsi
	movl	$0, (%rdx,%rcx,4)
	movq	56(%r12), %rdx
	movl	%ecx, (%rdx,%rcx,4)
	movq	$0, (%rsi,%rcx,8)
	movq	48(%r12), %rdx
	movq	72(%r12), %rsi
	movl	$0, 4(%rdx,%rcx,4)
	movq	56(%r12), %rdx
	leal	1(%rcx), %edi
	movl	%edi, 4(%rdx,%rcx,4)
	movq	$0, 8(%rsi,%rcx,8)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jne	.LBB1_9
.LBB1_10:                               # %._crit_edge
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	hypre_PointRelaxSetNumPointsets, .Lfunc_end1-hypre_PointRelaxSetNumPointsets
	.cfi_endproc

	.globl	hypre_PointRelaxSetPointset
	.p2align	4, 0x90
	.type	hypre_PointRelaxSetPointset,@function
hypre_PointRelaxSetPointset:            # @hypre_PointRelaxSetPointset
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -48
.Lcfi20:
	.cfi_offset %r12, -40
.Lcfi21:
	.cfi_offset %r13, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rcx, %r12
	movl	%edx, %r15d
	movq	%rdi, %r13
	movq	72(%r13), %rax
	movslq	%esi, %rbx
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_Free
	movq	72(%r13), %rax
	movq	$0, (%rax,%rbx,8)
	leal	(,%r15,4), %eax
	leal	(%rax,%rax,2), %edi
	callq	hypre_MAlloc
	movq	72(%r13), %rcx
	movq	%rax, (%rcx,%rbx,8)
	movq	48(%r13), %rax
	movl	%r15d, (%rax,%rbx,4)
	movl	(%r12), %eax
	movq	64(%r13), %rcx
	leaq	(%rbx,%rbx,2), %rdx
	movl	%eax, (%rcx,%rdx,4)
	movl	4(%r12), %eax
	movl	%eax, 4(%rcx,%rdx,4)
	movl	8(%r12), %eax
	movl	%eax, 8(%rcx,%rdx,4)
	testl	%r15d, %r15d
	jle	.LBB2_7
# BB#1:                                 # %.lr.ph
	movq	72(%r13), %rax
	movq	(%rax,%rbx,8), %rcx
	movl	%r15d, %eax
	testb	$1, %al
	jne	.LBB2_3
# BB#2:
	xorl	%edx, %edx
	cmpl	$1, %r15d
	jne	.LBB2_5
	jmp	.LBB2_7
.LBB2_3:
	movl	(%r14), %edx
	movl	%edx, (%rcx)
	movl	4(%r14), %edx
	movl	%edx, 4(%rcx)
	movl	8(%r14), %edx
	movl	%edx, 8(%rcx)
	movl	$1, %edx
	cmpl	$1, %r15d
	je	.LBB2_7
.LBB2_5:                                # %.lr.ph.new
	subq	%rdx, %rax
	leaq	(%rdx,%rdx,2), %rdx
	leaq	20(%rcx,%rdx,4), %rcx
	leaq	20(%r14,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB2_6:                                # =>This Inner Loop Header: Depth=1
	movl	-20(%rdx), %esi
	movl	%esi, -20(%rcx)
	movl	-16(%rdx), %esi
	movl	%esi, -16(%rcx)
	movl	-12(%rdx), %esi
	movl	%esi, -12(%rcx)
	movl	-8(%rdx), %esi
	movl	%esi, -8(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	movl	(%rdx), %esi
	movl	%esi, (%rcx)
	addq	$24, %rcx
	addq	$24, %rdx
	addq	$-2, %rax
	jne	.LBB2_6
.LBB2_7:                                # %._crit_edge
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	hypre_PointRelaxSetPointset, .Lfunc_end2-hypre_PointRelaxSetPointset
	.cfi_endproc

	.globl	hypre_PointRelaxDestroy
	.p2align	4, 0x90
	.type	hypre_PointRelaxDestroy,@function
hypre_PointRelaxDestroy:                # @hypre_PointRelaxDestroy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB3_5
# BB#1:                                 # %.preheader
	cmpl	$0, 40(%r14)
	jle	.LBB3_4
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	72(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_Free
	movq	72(%r14), %rax
	movq	$0, (%rax,%rbx,8)
	movq	120(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_ComputePkgDestroy
	incq	%rbx
	movslq	40(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB3_3
.LBB3_4:                                # %._crit_edge
	movq	48(%r14), %rdi
	callq	hypre_Free
	movq	$0, 48(%r14)
	movq	56(%r14), %rdi
	callq	hypre_Free
	movq	$0, 56(%r14)
	movq	64(%r14), %rdi
	callq	hypre_Free
	movq	$0, 64(%r14)
	movq	72(%r14), %rdi
	callq	hypre_Free
	movq	$0, 72(%r14)
	movq	80(%r14), %rdi
	callq	hypre_StructMatrixDestroy
	movq	88(%r14), %rdi
	callq	hypre_StructVectorDestroy
	movq	96(%r14), %rdi
	callq	hypre_StructVectorDestroy
	movq	120(%r14), %rdi
	callq	hypre_Free
	movq	$0, 120(%r14)
	movq	104(%r14), %rdi
	callq	hypre_StructVectorDestroy
	movl	132(%r14), %edi
	callq	hypre_FinalizeTiming
	movq	%r14, %rdi
	callq	hypre_Free
.LBB3_5:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	hypre_PointRelaxDestroy, .Lfunc_end3-hypre_PointRelaxDestroy
	.cfi_endproc

	.globl	hypre_PointRelaxSetup
	.p2align	4, 0x90
	.type	hypre_PointRelaxSetup,@function
hypre_PointRelaxSetup:                  # @hypre_PointRelaxSetup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi35:
	.cfi_def_cfa_offset 352
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movl	40(%rdi), %r13d
	movq	48(%rdi), %r12
	movq	64(%rdi), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	72(%rdi), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	cmpq	$0, 104(%rdi)
	jne	.LBB4_2
# BB#1:
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	(%rbx), %edi
	movq	%rsi, %r15
	movq	8(%rbx), %rsi
	callq	hypre_StructVectorCreate
	movq	%rax, %r14
	leaq	48(%rbx), %rsi
	movq	%r14, %rdi
	callq	hypre_StructVectorSetNumGhost
	movq	%r14, %rdi
	callq	hypre_StructVectorInitialize
	movq	%r14, %rdi
	callq	hypre_StructVectorAssemble
	movq	%r15, %rsi
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%r14, 104(%rax)
.LBB4_2:
	movq	8(%rsi), %r15
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	24(%rsi), %rdi
	movq	$0, 272(%rsp)
	movl	$0, 280(%rsp)
	leaq	272(%rsp), %rsi
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	callq	hypre_StructStencilElementRank
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 284(%rsp)
	movl	$1, 292(%rsp)
	movl	$8, %esi
	movl	%r13d, %edi
	callq	hypre_CAlloc
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%r13, 72(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	jle	.LBB4_29
# BB#3:                                 # %.lr.ph212
	xorl	%ebx, %ebx
                                        # implicit-def: %RAX
	movq	%rax, 80(%rsp)          # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 88(%rsp)          # 8-byte Spill
                                        # implicit-def: %R14
	movq	%r15, 128(%rsp)         # 8-byte Spill
	movq	%r12, 160(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_6 Depth 2
                                        #       Child Loop BB4_12 Depth 3
                                        #         Child Loop BB4_15 Depth 4
                                        #         Child Loop BB4_17 Depth 4
                                        #         Child Loop BB4_19 Depth 4
                                        #           Child Loop BB4_20 Depth 5
	movq	%r15, %rdi
	movq	120(%rsp), %rsi         # 8-byte Reload
	leaq	264(%rsp), %rdx
	leaq	256(%rsp), %rcx
	leaq	248(%rsp), %r8
	leaq	240(%rsp), %r9
	leaq	104(%rsp), %rax
	pushq	%rax
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	leaq	120(%rsp), %rax
	pushq	%rax
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CreateComputeInfo
	addq	$16, %rsp
.Lcfi44:
	.cfi_adjust_cfa_offset -16
	leaq	(%rbx,%rbx,2), %rax
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rbx, 176(%rsp)         # 8-byte Spill
	jmp	.LBB4_6
	.p2align	4, 0x90
.LBB4_24:                               # %._crit_edge202
                                        #   in Loop: Header=BB4_6 Depth=2
	movl	36(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movq	128(%rsp), %r15         # 8-byte Reload
	movq	96(%rsp), %r11          # 8-byte Reload
	je	.LBB4_5
# BB#25:                                # %._crit_edge202
                                        #   in Loop: Header=BB4_6 Depth=2
	cmpl	$1, %eax
	je	.LBB4_28
# BB#26:                                #   in Loop: Header=BB4_6 Depth=2
	incl	%eax
	cmpl	$2, %eax
	jne	.LBB4_6
	jmp	.LBB4_27
.LBB4_5:                                # %.outer.loopexit
                                        #   in Loop: Header=BB4_6 Depth=2
	movl	$1, %eax
	movq	%r13, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_6:                                #   Parent Loop BB4_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_12 Depth 3
                                        #         Child Loop BB4_15 Depth 4
                                        #         Child Loop BB4_17 Depth 4
                                        #         Child Loop BB4_19 Depth 4
                                        #           Child Loop BB4_20 Depth 5
	cmpl	$1, %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	je	.LBB4_9
# BB#7:                                 #   in Loop: Header=BB4_6 Depth=2
	testl	%eax, %eax
	jne	.LBB4_10
# BB#8:                                 #   in Loop: Header=BB4_6 Depth=2
	movq	112(%rsp), %r14
	jmp	.LBB4_10
	.p2align	4, 0x90
.LBB4_9:                                #   in Loop: Header=BB4_6 Depth=2
	movq	104(%rsp), %r14
.LBB4_10:                               #   in Loop: Header=BB4_6 Depth=2
	movl	8(%r14), %r15d
	movl	%r15d, %edi
	callq	hypre_BoxArrayArrayCreate
	movq	%rax, %r13
	testl	%r15d, %r15d
	jle	.LBB4_24
# BB#11:                                # %.lr.ph201
                                        #   in Loop: Header=BB4_6 Depth=2
	movl	(%r12,%rbx,4), %eax
	xorl	%edx, %edx
	movq	%r14, 152(%rsp)         # 8-byte Spill
	movq	%r13, 144(%rsp)         # 8-byte Spill
	movq	%r15, 136(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_12:                               #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_15 Depth 4
                                        #         Child Loop BB4_17 Depth 4
                                        #         Child Loop BB4_19 Depth 4
                                        #           Child Loop BB4_20 Depth 5
	movq	(%r14), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movl	8(%rcx), %ebp
	movq	(%r13), %rcx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	(%rcx,%rdx,8), %rdi
	movl	%ebp, %esi
	imull	%eax, %esi
	movq	%rdi, 200(%rsp)         # 8-byte Spill
	callq	hypre_BoxArraySetSize
	movl	(%r12,%rbx,4), %eax
	testl	%eax, %eax
	jle	.LBB4_23
# BB#13:                                # %.lr.ph197
                                        #   in Loop: Header=BB4_12 Depth=3
	testl	%ebp, %ebp
	jle	.LBB4_14
# BB#18:                                # %.lr.ph197.split.us.preheader
                                        #   in Loop: Header=BB4_12 Depth=3
	leal	-1(%rbp), %eax
	incq	%rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbp, 192(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_19:                               # %.lr.ph197.split.us
                                        #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_6 Depth=2
                                        #       Parent Loop BB4_12 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB4_20 Depth 5
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %r13
	shlq	$2, %r13
	movq	168(%rsp), %rax         # 8-byte Reload
	addq	(%rax,%rbx,8), %r13
	movslq	%edx, %rdx
	movq	%rdx, 216(%rsp)         # 8-byte Spill
	leaq	(,%rdx,8), %rax
	leaq	(%rax,%rax,2), %rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movq	%rbp, %r14
	xorl	%r12d, %r12d
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	208(%rsp), %r15         # 8-byte Reload
	movq	200(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_20:                               #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_6 Depth=2
                                        #       Parent Loop BB4_12 Depth=3
                                        #         Parent Loop BB4_19 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%r15), %rax
	movq	(%rbx), %rcx
	addq	232(%rsp), %rcx         # 8-byte Folded Reload
	movl	(%rax,%r12), %edx
	movl	%edx, (%r12,%rcx)
	movl	4(%rax,%r12), %edx
	movl	%edx, 4(%r12,%rcx)
	movl	8(%rax,%r12), %edx
	movl	%edx, 8(%r12,%rcx)
	movl	12(%rax,%r12), %edx
	movl	%edx, 12(%r12,%rcx)
	movl	16(%rax,%r12), %edx
	movl	%edx, 16(%r12,%rcx)
	movl	20(%rax,%r12), %eax
	movl	%eax, 20(%r12,%rcx)
	leaq	(%rcx,%r12), %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	hypre_ProjectBox
	addq	$24, %r12
	decq	%r14
	jne	.LBB4_20
# BB#21:                                # %._crit_edge192.us
                                        #   in Loop: Header=BB4_19 Depth=4
	movq	216(%rsp), %rdx         # 8-byte Reload
	addq	184(%rsp), %rdx         # 8-byte Folded Reload
	movq	224(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movq	160(%rsp), %r12         # 8-byte Reload
	movq	176(%rsp), %rbx         # 8-byte Reload
	movslq	(%r12,%rbx,4), %rax
	cmpq	%rax, %rcx
	movq	192(%rsp), %rbp         # 8-byte Reload
	jl	.LBB4_19
# BB#22:                                #   in Loop: Header=BB4_12 Depth=3
	movq	152(%rsp), %r14         # 8-byte Reload
	movq	144(%rsp), %r13         # 8-byte Reload
	movq	136(%rsp), %r15         # 8-byte Reload
	jmp	.LBB4_23
	.p2align	4, 0x90
.LBB4_14:                               # %.lr.ph197.split.preheader
                                        #   in Loop: Header=BB4_12 Depth=3
	leal	-1(%rax), %edx
	movl	%eax, %esi
	xorl	%ecx, %ecx
	andl	$7, %esi
	je	.LBB4_16
	.p2align	4, 0x90
.LBB4_15:                               # %.lr.ph197.split.prol
                                        #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_6 Depth=2
                                        #       Parent Loop BB4_12 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%ecx
	cmpl	%ecx, %esi
	jne	.LBB4_15
.LBB4_16:                               # %.lr.ph197.split.prol.loopexit
                                        #   in Loop: Header=BB4_12 Depth=3
	cmpl	$7, %edx
	jb	.LBB4_23
	.p2align	4, 0x90
.LBB4_17:                               # %.lr.ph197.split
                                        #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_6 Depth=2
                                        #       Parent Loop BB4_12 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	addl	$8, %ecx
	cmpl	%eax, %ecx
	jl	.LBB4_17
	.p2align	4, 0x90
.LBB4_23:                               #   in Loop: Header=BB4_12 Depth=3
	movq	8(%rsp), %rdx           # 8-byte Reload
	incq	%rdx
	cmpq	%r15, %rdx
	jne	.LBB4_12
	jmp	.LBB4_24
	.p2align	4, 0x90
.LBB4_27:                               #   in Loop: Header=BB4_4 Depth=1
	movq	88(%rsp), %r13          # 8-byte Reload
.LBB4_28:                               # %.thread
                                        #   in Loop: Header=BB4_4 Depth=1
	movq	264(%rsp), %rdi
	movq	256(%rsp), %rsi
	movq	248(%rsp), %r8
	movq	240(%rsp), %r9
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	subq	$8, %rsp
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	leaq	292(%rsp), %rdx
	movq	%rdx, %rcx
	pushq	%rax
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	movq	48(%rsp), %rax          # 8-byte Reload
	pushq	16(%rax)
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)               # 8-byte Folded Reload
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	callq	hypre_ComputePkgCreate
	addq	$64, %rsp
.Lcfi53:
	.cfi_adjust_cfa_offset -64
	movq	112(%rsp), %rdi
	callq	hypre_BoxArrayArrayDestroy
	movq	104(%rsp), %rdi
	callq	hypre_BoxArrayArrayDestroy
	incq	%rbx
	cmpq	72(%rsp), %rbx          # 8-byte Folded Reload
	movq	%r13, 88(%rsp)          # 8-byte Spill
	jne	.LBB4_4
.LBB4_29:                               # %._crit_edge213
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	hypre_StructMatrixRef
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rax, 80(%rbp)
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	hypre_StructVectorRef
	movq	%rax, 96(%rbp)
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	hypre_StructVectorRef
	movq	%rax, 88(%rbp)
	movl	32(%rsp), %eax          # 4-byte Reload
	movl	%eax, 112(%rbp)
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, 120(%rbp)
	movq	72(%rsp), %rsi          # 8-byte Reload
	testl	%esi, %esi
	jle	.LBB4_30
# BB#31:                                # %.lr.ph.preheader
	movq	56(%rsp), %rdi          # 8-byte Reload
	addq	$8, %rdi
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_32:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdi), %ecx
	imull	-8(%rdi), %ecx
	imull	(%rdi), %ecx
	movl	(%r12), %eax
	cltd
	idivl	%ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	addsd	%xmm1, %xmm0
	addq	$4, %r12
	addq	$12, %rdi
	decq	%rsi
	jne	.LBB4_32
	jmp	.LBB4_33
.LBB4_30:
	xorpd	%xmm0, %xmm0
.LBB4_33:                               # %._crit_edge
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	72(%rax), %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	addl	112(%rcx), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 136(%rcx)
	xorl	%eax, %eax
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	hypre_PointRelaxSetup, .Lfunc_end4-hypre_PointRelaxSetup
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	hypre_PointRelax
	.p2align	4, 0x90
	.type	hypre_PointRelax,@function
hypre_PointRelax:                       # @hypre_PointRelax
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	subq	$600, %rsp              # imm = 0x258
.Lcfi60:
	.cfi_def_cfa_offset 656
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movl	16(%rbp), %r13d
	movl	24(%rbp), %r14d
	movsd	32(%rbp), %xmm0         # xmm0 = mem[0],zero
	movsd	%xmm0, 104(%rsp)        # 8-byte Spill
	movl	40(%rbp), %eax
	movl	%eax, 300(%rsp)         # 4-byte Spill
	movq	56(%rbp), %rax
	movq	%rax, 456(%rsp)         # 8-byte Spill
	movq	64(%rbp), %rax
	movq	%rax, 448(%rsp)         # 8-byte Spill
	movq	104(%rbp), %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movslq	112(%rbp), %rax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	movq	120(%rbp), %rax
	movq	%rax, 440(%rsp)         # 8-byte Spill
	movl	132(%rbp), %edi
	callq	hypre_BeginTiming
	movq	80(%rbp), %rdi
	callq	hypre_StructMatrixDestroy
	movq	88(%rbp), %rdi
	callq	hypre_StructVectorDestroy
	movq	96(%rbp), %rdi
	callq	hypre_StructVectorDestroy
	movq	%r12, %rdi
	callq	hypre_StructMatrixRef
	movq	%rax, 80(%rbp)
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movq	%rbx, %rdi
	callq	hypre_StructVectorRef
	movq	%rax, 96(%rbp)
	movq	%r15, 464(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	callq	hypre_StructVectorRef
	movq	%rax, 88(%rbp)
	movq	%rbp, 432(%rsp)         # 8-byte Spill
	movl	$0, 128(%rbp)
	testl	%r13d, %r13d
	je	.LBB5_1
# BB#4:
	movl	%r13d, 296(%rsp)        # 4-byte Spill
	movq	24(%r12), %rax
	movq	(%rax), %rcx
	movq	%rcx, 576(%rsp)         # 8-byte Spill
	movl	8(%rax), %eax
	movq	%rax, 480(%rsp)         # 8-byte Spill
	testl	%r14d, %r14d
	movq	%r12, 320(%rsp)         # 8-byte Spill
	je	.LBB5_5
# BB#6:
	movq	456(%rsp), %rax         # 8-byte Reload
	movslq	(%rax), %rax
	movq	440(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %rdx
	leaq	(%rax,%rax,2), %rax
	movq	448(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,4), %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	leaq	8(%rdx), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	addq	$16, %rdx
	movq	%rdx, 384(%rsp)         # 8-byte Spill
                                        # implicit-def: %RBX
	xorl	%ecx, %ecx
	movsd	104(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB5_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_13 Depth 2
                                        #       Child Loop BB5_15 Depth 3
                                        #         Child Loop BB5_19 Depth 4
                                        #           Child Loop BB5_21 Depth 5
                                        #             Child Loop BB5_33 Depth 6
                                        #             Child Loop BB5_28 Depth 6
	testl	%ecx, %ecx
	movq	224(%rsp), %rax         # 8-byte Reload
	je	.LBB5_10
# BB#8:                                 #   in Loop: Header=BB5_7 Depth=1
	cmpl	$1, %ecx
	jne	.LBB5_11
# BB#9:                                 #   in Loop: Header=BB5_7 Depth=1
	movq	384(%rsp), %rax         # 8-byte Reload
.LBB5_10:                               # %.sink.split
                                        #   in Loop: Header=BB5_7 Depth=1
	movq	(%rax), %rbx
.LBB5_11:                               #   in Loop: Header=BB5_7 Depth=1
	movl	%ecx, 232(%rsp)         # 4-byte Spill
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB5_41
# BB#12:                                # %.lr.ph1614
                                        #   in Loop: Header=BB5_7 Depth=1
	xorl	%edx, %edx
	movq	%rbx, 360(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB5_13:                               #   Parent Loop BB5_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_15 Depth 3
                                        #         Child Loop BB5_19 Depth 4
                                        #           Child Loop BB5_21 Depth 5
                                        #             Child Loop BB5_33 Depth 6
                                        #             Child Loop BB5_28 Depth 6
	movq	(%rbx), %rcx
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$0, 8(%rsi)
	jle	.LBB5_40
# BB#14:                                # %.lr.ph1606
                                        #   in Loop: Header=BB5_13 Depth=2
	movq	40(%r12), %rax
	movq	48(%r12), %r8
	movq	(%rax), %rax
	movq	464(%rsp), %rbx         # 8-byte Reload
	movq	16(%rbx), %rcx
	movq	24(%rbx), %r9
	movq	(%rcx), %r14
	movq	112(%rsp), %rbp         # 8-byte Reload
	movq	16(%rbp), %rcx
	movq	24(%rbp), %r10
	movq	(%rcx), %r11
	movq	64(%r12), %rdi
	movq	(%rdi,%rdx,8), %rdi
	movq	368(%rsp), %rcx         # 8-byte Reload
	movslq	(%rdi,%rcx,4), %rdi
	leaq	(%r8,%rdi,8), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	40(%rbx), %rcx
	movslq	(%rcx,%rdx,4), %rcx
	leaq	(%r9,%rcx,8), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	40(%rbp), %rcx
	movslq	(%rcx,%rdx,4), %rcx
	leaq	(%r10,%rcx,8), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 240(%rsp)         # 8-byte Spill
	leaq	(,%rdx,8), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	4(%rax,%rcx), %rdx
	movq	%rdx, 256(%rsp)         # 8-byte Spill
	addq	%rcx, %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	leaq	4(%r14,%rcx), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	addq	%rcx, %r14
	movq	%r14, 392(%rsp)         # 8-byte Spill
	leaq	(%r11,%rcx), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	leaq	4(%r11,%rcx), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rsi, 400(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB5_15:                               #   Parent Loop BB5_7 Depth=1
                                        #     Parent Loop BB5_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB5_19 Depth 4
                                        #           Child Loop BB5_21 Depth 5
                                        #             Child Loop BB5_33 Depth 6
                                        #             Child Loop BB5_28 Depth 6
	movq	(%rsi), %rax
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %r14
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	(%rax,%r14,8), %rdi
	movq	328(%rsp), %rsi         # 8-byte Reload
	leaq	148(%rsp), %rdx
	callq	hypre_BoxGetStrideSize
	movq	184(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r8d
	movq	248(%rsp), %rcx         # 8-byte Reload
	movl	8(%rcx), %eax
	movl	%eax, %r11d
	subl	%r8d, %r11d
	incl	%r11d
	cmpl	%r8d, %eax
	movl	(%rcx), %r10d
	movl	12(%rcx), %eax
	movl	$0, %esi
	cmovsl	%esi, %r11d
	movl	%eax, %ecx
	subl	%r10d, %ecx
	incl	%ecx
	cmpl	%r10d, %eax
	movq	392(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %ebp
	movq	160(%rsp), %rdi         # 8-byte Reload
	movl	8(%rdi), %eax
	cmovsl	%esi, %ecx
	movl	%eax, %edx
	subl	%ebp, %edx
	incl	%edx
	cmpl	%ebp, %eax
	movl	(%rdi), %ebx
	movl	12(%rdi), %eax
	cmovsl	%esi, %edx
	movl	%eax, %r9d
	subl	%ebx, %r9d
	incl	%r9d
	movl	%ebx, 96(%rsp)          # 4-byte Spill
	cmpl	%ebx, %eax
	movq	192(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r13d
	movq	256(%rsp), %rbx         # 8-byte Reload
	movl	8(%rbx), %eax
	cmovsl	%esi, %r9d
	movl	%eax, %edi
	subl	%r13d, %edi
	incl	%edi
	cmpl	%r13d, %eax
	movl	(%rbx), %r15d
	movl	12(%rbx), %eax
	cmovsl	%esi, %edi
	movl	%eax, %r12d
	subl	%r15d, %r12d
	incl	%r12d
	movl	%r15d, 24(%rsp)         # 4-byte Spill
	cmpl	%r15d, %eax
	cmovsl	%esi, %r12d
	movl	148(%rsp), %eax
	movl	152(%rsp), %esi
	cmpl	%eax, %esi
	movq	%rax, 8(%rsp)           # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	cmovgel	%esi, %eax
	movl	156(%rsp), %esi
	cmpl	%eax, %esi
	movl	%esi, 144(%rsp)         # 4-byte Spill
	cmovgel	%esi, %eax
	testl	%eax, %eax
	jle	.LBB5_38
# BB#16:                                # %.lr.ph1600
                                        #   in Loop: Header=BB5_15 Depth=3
	cmpl	$0, 144(%rsp)           # 4-byte Folded Reload
	jle	.LBB5_38
# BB#17:                                # %.lr.ph1600
                                        #   in Loop: Header=BB5_15 Depth=3
	cmpl	$0, 80(%rsp)            # 4-byte Folded Reload
	jle	.LBB5_38
# BB#18:                                # %.preheader1375.us.preheader
                                        #   in Loop: Header=BB5_15 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r14,8), %esi
	movl	%esi, 88(%rsp)          # 4-byte Spill
	movl	4(%rax,%r14,8), %esi
	movl	%esi, 120(%rsp)         # 4-byte Spill
	movl	%r8d, (%rsp)            # 4-byte Spill
	movl	%ebp, 72(%rsp)          # 4-byte Spill
	movl	8(%rax,%r14,8), %r8d
	movl	%r11d, %ebx
	imull	%ecx, %ebx
	movq	328(%rsp), %rsi         # 8-byte Reload
	movl	8(%rsi), %eax
	imull	%eax, %ebx
	movl	%ebx, 140(%rsp)         # 4-byte Spill
	movl	%edx, %ebx
	imull	%r9d, %ebx
	imull	%eax, %ebx
	movl	%ebx, 352(%rsp)         # 4-byte Spill
	movl	%r13d, 64(%rsp)         # 4-byte Spill
	movl	%edi, %r13d
	imull	%r12d, %r13d
	imull	%eax, %r13d
	movl	%r10d, 4(%rsp)          # 4-byte Spill
	movl	4(%rsi), %r15d
	movl	%r15d, %ebp
	imull	%edi, %ebp
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movl	%r11d, %eax
	imull	%r15d, %eax
	imull	%edx, %r15d
	movslq	(%rsi), %rsi
	movq	%rsi, 216(%rsp)         # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	negl	%esi
	imull	8(%rsp), %esi           # 4-byte Folded Reload
	leal	(%rsi,%rbp), %ebx
	leal	(%rsi,%r15), %r14d
	addl	%eax, %esi
	movl	%eax, %r10d
	movl	%r10d, 128(%rsp)        # 4-byte Spill
	movq	80(%rsp), %rbp          # 8-byte Reload
	leal	-1(%rbp), %eax
	imull	%eax, %ebx
	imull	%eax, %r14d
	imull	%eax, %esi
	addl	32(%rsp), %ebx          # 4-byte Folded Reload
	movq	216(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	8(%rsp), %eax           # 4-byte Folded Reload
	subl	%eax, %ebx
	movl	%ebx, 176(%rsp)         # 4-byte Spill
	addl	%r15d, %r14d
	subl	%eax, %r14d
	movl	%r14d, 336(%rsp)        # 4-byte Spill
	addl	%r10d, %esi
	subl	%eax, %esi
	movq	%rsi, 344(%rsp)         # 8-byte Spill
	movl	88(%rsp), %r10d         # 4-byte Reload
	movl	%r10d, %r14d
	subl	(%rsp), %r14d           # 4-byte Folded Reload
	movl	120(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %esi
	subl	4(%rsp), %esi           # 4-byte Folded Reload
	movl	%r8d, %eax
	movq	248(%rsp), %rbx         # 8-byte Reload
	subl	4(%rbx), %eax
	imull	%ecx, %eax
	addl	%esi, %eax
	imull	%r11d, %eax
	addl	%r14d, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	%r10d, %r11d
	subl	72(%rsp), %r11d         # 4-byte Folded Reload
	movl	%ebp, %ecx
	subl	96(%rsp), %ecx          # 4-byte Folded Reload
	movl	%r8d, %ebx
	movq	160(%rsp), %rsi         # 8-byte Reload
	subl	4(%rsi), %ebx
	imull	%r9d, %ebx
	addl	%ecx, %ebx
	imull	%edx, %ebx
	addl	%r11d, %ebx
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	subl	64(%rsp), %r10d         # 4-byte Folded Reload
	subl	24(%rsp), %ebp          # 4-byte Folded Reload
	movq	256(%rsp), %rax         # 8-byte Reload
	subl	4(%rax), %r8d
	imull	%r12d, %r8d
	addl	%ebp, %r8d
	imull	%edi, %r8d
	movq	80(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %eax
	imull	32(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 16(%rsp)          # 4-byte Spill
	subl	%eax, %r13d
	movl	%r13d, 136(%rsp)        # 4-byte Spill
	addl	%r10d, %r8d
	movl	%r8d, (%rsp)            # 4-byte Spill
	movl	%edx, %eax
	movq	%r15, 96(%rsp)          # 8-byte Spill
	imull	%r15d, %eax
	movl	%eax, 408(%rsp)         # 4-byte Spill
	subl	%eax, 352(%rsp)         # 4-byte Folded Spill
	movl	%edx, %eax
	imull	128(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 168(%rsp)         # 4-byte Spill
	subl	%eax, 140(%rsp)         # 4-byte Folded Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	-1(%rax), %ecx
	leaq	1(%rcx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rax, %rdx
	movabsq	$8589934590, %rax       # imm = 0x1FFFFFFFE
	andq	%rax, %rdx
	setne	%al
	movq	216(%rsp), %rsi         # 8-byte Reload
	cmpl	$1, %esi
	sete	%bl
	andb	%al, %bl
	movb	%bl, 64(%rsp)           # 1-byte Spill
	movq	%rsi, %rax
	movq	%rdx, 208(%rsp)         # 8-byte Spill
	imulq	%rdx, %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	%rsi, %r10
	shlq	$4, %r10
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax,%rcx,8), %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax,%rcx,8), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leaq	8(%rax,%rcx,8), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	leaq	(,%rsi,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_19:                               # %.preheader1375.us
                                        #   Parent Loop BB5_7 Depth=1
                                        #     Parent Loop BB5_13 Depth=2
                                        #       Parent Loop BB5_15 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB5_21 Depth 5
                                        #             Child Loop BB5_33 Depth 6
                                        #             Child Loop BB5_28 Depth 6
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	344(%rsp), %rax         # 8-byte Reload
	movl	%eax, %ecx
	movl	336(%rsp), %eax         # 4-byte Reload
	movl	176(%rsp), %edx         # 4-byte Reload
	jle	.LBB5_37
# BB#20:                                # %.preheader1374.us.us.preheader
                                        #   in Loop: Header=BB5_19 Depth=4
	movl	%esi, 180(%rsp)         # 4-byte Spill
	xorl	%r11d, %r11d
	movl	4(%rsp), %r15d          # 4-byte Reload
	movl	20(%rsp), %r9d          # 4-byte Reload
	movl	(%rsp), %r12d           # 4-byte Reload
	.p2align	4, 0x90
.LBB5_21:                               # %.preheader1374.us.us
                                        #   Parent Loop BB5_7 Depth=1
                                        #     Parent Loop BB5_13 Depth=2
                                        #       Parent Loop BB5_15 Depth=3
                                        #         Parent Loop BB5_19 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB5_33 Depth 6
                                        #             Child Loop BB5_28 Depth 6
	movslq	%r12d, %r13
	movslq	%r9d, %rsi
	movslq	%r15d, %r8
	cmpq	$1, 24(%rsp)            # 8-byte Folded Reload
	jbe	.LBB5_22
# BB#29:                                # %min.iters.checked
                                        #   in Loop: Header=BB5_21 Depth=5
	cmpb	$0, 64(%rsp)            # 1-byte Folded Reload
	je	.LBB5_22
# BB#30:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_21 Depth=5
	movq	32(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r11d, %eax
	addl	(%rsp), %eax            # 4-byte Folded Reload
	movslq	%eax, %rcx
	movq	96(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r11d, %eax
	addl	20(%rsp), %eax          # 4-byte Folded Reload
	movslq	%eax, %r14
	movl	128(%rsp), %eax         # 4-byte Reload
	imull	%r11d, %eax
	addl	4(%rsp), %eax           # 4-byte Folded Reload
	movslq	%eax, %rbp
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp,8), %rax
	movq	288(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rbp,8), %rdx
	movq	280(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%r14,8), %rdi
	cmpq	%rdi, %rax
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%r14,8), %rdi
	sbbb	%bl, %bl
	movb	%bl, 120(%rsp)          # 1-byte Spill
	cmpq	%rdx, %rdi
	sbbb	%bl, %bl
	andb	120(%rsp), %bl          # 1-byte Folded Reload
	movq	272(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rcx,8), %rdi
	cmpq	%rdi, %rax
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rdi
	sbbb	%al, %al
	cmpq	%rdx, %rdi
	sbbb	%dl, %dl
	testb	$1, %bl
	jne	.LBB5_22
# BB#31:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_21 Depth=5
	andb	%dl, %al
	andb	$1, %al
	jne	.LBB5_22
# BB#32:                                # %vector.body.preheader
                                        #   in Loop: Header=BB5_21 Depth=5
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r13,8), %rbx
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r8,8), %rsi
	movq	264(%rsp), %rdx         # 8-byte Reload
	addq	%rdx, %rbp
	addq	%rdx, %r14
	addq	%rdx, %rcx
	movq	208(%rsp), %rdx         # 8-byte Reload
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_33:                               # %vector.body
                                        #   Parent Loop BB5_7 Depth=1
                                        #     Parent Loop BB5_13 Depth=2
                                        #       Parent Loop BB5_15 Depth=3
                                        #         Parent Loop BB5_19 Depth=4
                                        #           Parent Loop BB5_21 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movupd	(%rax,%rdi), %xmm0
	movupd	(%rbx,%rdi), %xmm1
	divpd	%xmm1, %xmm0
	movupd	%xmm0, (%rsi,%rdi)
	addq	%r10, %rdi
	addq	$-2, %rdx
	jne	.LBB5_33
# BB#34:                                # %middle.block
                                        #   in Loop: Header=BB5_21 Depth=5
	movq	208(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, 24(%rsp)          # 8-byte Folded Reload
	movl	%eax, %esi
	jne	.LBB5_23
	jmp	.LBB5_35
	.p2align	4, 0x90
.LBB5_22:                               #   in Loop: Header=BB5_21 Depth=5
	movq	%r8, %rbp
	movq	%rsi, %r14
	movq	%r13, %rcx
	xorl	%esi, %esi
.LBB5_23:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB5_21 Depth=5
	movq	8(%rsp), %rax           # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%esi, %eax
	testb	$1, %al
	jne	.LBB5_25
# BB#24:                                #   in Loop: Header=BB5_21 Depth=5
	movl	%esi, %r8d
	cmpl	%esi, 72(%rsp)          # 4-byte Folded Reload
	jne	.LBB5_27
	jmp	.LBB5_35
	.p2align	4, 0x90
.LBB5_25:                               # %scalar.ph.prol
                                        #   in Loop: Header=BB5_21 Depth=5
	movq	40(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%r14,8), %xmm0    # xmm0 = mem[0],zero
	movq	56(%rsp), %rax          # 8-byte Reload
	divsd	(%rax,%rcx,8), %xmm0
	movq	48(%rsp), %rax          # 8-byte Reload
	movsd	%xmm0, (%rax,%rbp,8)
	movq	216(%rsp), %rax         # 8-byte Reload
	addq	%rax, %rcx
	addq	%rax, %r14
	addq	%rax, %rbp
	leal	1(%rsi), %r8d
	cmpl	%esi, 72(%rsp)          # 4-byte Folded Reload
	je	.LBB5_35
.LBB5_27:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB5_21 Depth=5
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp,8), %rsi
	movq	88(%rsp), %rbp          # 8-byte Reload
	leaq	(%rsi,%rbp), %r13
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,8), %rbx
	leaq	(%rbx,%rbp), %rax
	movq	56(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdi
	leaq	(%rdi,%rbp), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	subl	%r8d, %edx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_28:                               # %scalar.ph
                                        #   Parent Loop BB5_7 Depth=1
                                        #     Parent Loop BB5_13 Depth=2
                                        #       Parent Loop BB5_15 Depth=3
                                        #         Parent Loop BB5_19 Depth=4
                                        #           Parent Loop BB5_21 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movsd	(%rbx,%rbp), %xmm0      # xmm0 = mem[0],zero
	divsd	(%rdi,%rbp), %xmm0
	movsd	%xmm0, (%rsi,%rbp)
	movsd	(%rax,%rbp), %xmm0      # xmm0 = mem[0],zero
	divsd	(%rcx,%rbp), %xmm0
	movsd	%xmm0, (%r13,%rbp)
	addq	%r10, %rbp
	addl	$-2, %edx
	jne	.LBB5_28
.LBB5_35:                               # %._crit_edge1559.us.us
                                        #   in Loop: Header=BB5_21 Depth=5
	addl	32(%rsp), %r12d         # 4-byte Folded Reload
	addl	96(%rsp), %r9d          # 4-byte Folded Reload
	addl	128(%rsp), %r15d        # 4-byte Folded Reload
	incl	%r11d
	cmpl	80(%rsp), %r11d         # 4-byte Folded Reload
	jne	.LBB5_21
# BB#36:                                #   in Loop: Header=BB5_19 Depth=4
	movl	168(%rsp), %ecx         # 4-byte Reload
	movl	408(%rsp), %eax         # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	180(%rsp), %esi         # 4-byte Reload
.LBB5_37:                               # %._crit_edge1567.us
                                        #   in Loop: Header=BB5_19 Depth=4
	addl	(%rsp), %edx            # 4-byte Folded Reload
	addl	20(%rsp), %eax          # 4-byte Folded Reload
	addl	4(%rsp), %ecx           # 4-byte Folded Reload
	addl	136(%rsp), %edx         # 4-byte Folded Reload
	addl	352(%rsp), %eax         # 4-byte Folded Reload
	addl	140(%rsp), %ecx         # 4-byte Folded Reload
	incl	%esi
	cmpl	144(%rsp), %esi         # 4-byte Folded Reload
	movl	%edx, (%rsp)            # 4-byte Spill
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	jne	.LBB5_19
.LBB5_38:                               # %._crit_edge1601
                                        #   in Loop: Header=BB5_15 Depth=3
	movq	200(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movq	400(%rsp), %rsi         # 8-byte Reload
	movslq	8(%rsi), %rax
	cmpq	%rax, %rcx
	jl	.LBB5_15
# BB#39:                                # %._crit_edge1607.loopexit
                                        #   in Loop: Header=BB5_13 Depth=2
	movq	360(%rsp), %rbx         # 8-byte Reload
	movl	8(%rbx), %eax
	movq	320(%rsp), %r12         # 8-byte Reload
	movsd	104(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	240(%rsp), %rdx         # 8-byte Reload
.LBB5_40:                               # %._crit_edge1607
                                        #   in Loop: Header=BB5_13 Depth=2
	incq	%rdx
	movslq	%eax, %rcx
	cmpq	%rcx, %rdx
	jl	.LBB5_13
.LBB5_41:                               # %._crit_edge1615
                                        #   in Loop: Header=BB5_7 Depth=1
	movl	232(%rsp), %ecx         # 4-byte Reload
	incl	%ecx
	cmpl	$2, %ecx
	jne	.LBB5_7
# BB#42:
	ucomisd	.LCPI5_0(%rip), %xmm0
	jne	.LBB5_43
	jnp	.LBB5_44
.LBB5_43:
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	hypre_StructScale
	movsd	104(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB5_44:
	movl	$1, %eax
	xorl	%edx, %edx
	idivl	300(%rsp)               # 4-byte Folded Reload
	xorl	%ecx, %ecx
	testl	%edx, %edx
	sete	%cl
	cmpl	296(%rsp), %ecx         # 4-byte Folded Reload
	jl	.LBB5_46
	jmp	.LBB5_114
.LBB5_1:
	testl	%r14d, %r14d
	je	.LBB5_3
# BB#2:
	xorpd	%xmm0, %xmm0
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	hypre_StructVectorSetConstantValues
.LBB5_3:
	movq	432(%rsp), %rax         # 8-byte Reload
	movl	132(%rax), %edi
	jmp	.LBB5_115
.LBB5_5:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
                                        # implicit-def: %RBX
	movsd	104(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cmpl	296(%rsp), %ecx         # 4-byte Folded Reload
	jge	.LBB5_114
.LBB5_46:                               # %.lr.ph1551
	movq	%rbx, %r14
	movsd	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 488(%rsp)        # 8-byte Spill
	movl	368(%rsp), %eax         # 4-byte Reload
	movq	%rax, 408(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB5_47:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_48 Depth 2
                                        #       Child Loop BB5_55 Depth 3
                                        #         Child Loop BB5_57 Depth 4
                                        #           Child Loop BB5_61 Depth 5
                                        #             Child Loop BB5_63 Depth 6
                                        #               Child Loop BB5_65 Depth 7
                                        #               Child Loop BB5_69 Depth 7
                                        #           Child Loop BB5_75 Depth 5
                                        #             Child Loop BB5_79 Depth 6
                                        #               Child Loop BB5_81 Depth 7
                                        #                 Child Loop BB5_86 Depth 8
                                        #           Child Loop BB5_95 Depth 5
                                        #             Child Loop BB5_97 Depth 6
                                        #               Child Loop BB5_102 Depth 7
	movl	%ecx, 308(%rsp)         # 4-byte Spill
	movl	%edx, 304(%rsp)         # 4-byte Spill
	movslq	%edx, %rax
	movq	456(%rsp), %rcx         # 8-byte Reload
	movslq	(%rcx,%rax,4), %rax
	movq	440(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %rbx
	leaq	(%rax,%rax,2), %rax
	movq	448(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,4), %rax
	movq	%rax, 528(%rsp)         # 8-byte Spill
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	312(%rsp), %rsi         # 8-byte Reload
	callq	hypre_StructCopy
	movsd	104(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
	leaq	8(%rbx), %rax
	movq	%rax, 504(%rsp)         # 8-byte Spill
	movq	%rbx, 512(%rsp)         # 8-byte Spill
	leaq	16(%rbx), %rax
	movq	%rax, 496(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB5_48:                               #   Parent Loop BB5_47 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_55 Depth 3
                                        #         Child Loop BB5_57 Depth 4
                                        #           Child Loop BB5_61 Depth 5
                                        #             Child Loop BB5_63 Depth 6
                                        #               Child Loop BB5_65 Depth 7
                                        #               Child Loop BB5_69 Depth 7
                                        #           Child Loop BB5_75 Depth 5
                                        #             Child Loop BB5_79 Depth 6
                                        #               Child Loop BB5_81 Depth 7
                                        #                 Child Loop BB5_86 Depth 8
                                        #           Child Loop BB5_95 Depth 5
                                        #             Child Loop BB5_97 Depth 6
                                        #               Child Loop BB5_102 Depth 7
	cmpl	$1, %eax
	movl	%eax, 420(%rsp)         # 4-byte Spill
	je	.LBB5_51
# BB#49:                                #   in Loop: Header=BB5_48 Depth=2
	testl	%eax, %eax
	jne	.LBB5_53
# BB#50:                                #   in Loop: Header=BB5_48 Depth=2
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	24(%rax), %rsi
	movq	512(%rsp), %rdi         # 8-byte Reload
	leaq	592(%rsp), %rdx
	callq	hypre_InitializeIndtComputations
	movsd	104(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movq	504(%rsp), %rax         # 8-byte Reload
	jmp	.LBB5_52
	.p2align	4, 0x90
.LBB5_51:                               #   in Loop: Header=BB5_48 Depth=2
	movq	592(%rsp), %rdi
	callq	hypre_FinalizeIndtComputations
	movsd	104(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movq	496(%rsp), %rax         # 8-byte Reload
.LBB5_52:                               # %.sink.split1309
                                        #   in Loop: Header=BB5_48 Depth=2
	movq	(%rax), %rdx
.LBB5_53:                               #   in Loop: Header=BB5_48 Depth=2
	movl	8(%rdx), %eax
	testl	%eax, %eax
	jle	.LBB5_109
# BB#54:                                # %.lr.ph1526
                                        #   in Loop: Header=BB5_48 Depth=2
	xorl	%esi, %esi
	movq	%rdx, 360(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB5_55:                               #   Parent Loop BB5_47 Depth=1
                                        #     Parent Loop BB5_48 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB5_57 Depth 4
                                        #           Child Loop BB5_61 Depth 5
                                        #             Child Loop BB5_63 Depth 6
                                        #               Child Loop BB5_65 Depth 7
                                        #               Child Loop BB5_69 Depth 7
                                        #           Child Loop BB5_75 Depth 5
                                        #             Child Loop BB5_79 Depth 6
                                        #               Child Loop BB5_81 Depth 7
                                        #                 Child Loop BB5_86 Depth 8
                                        #           Child Loop BB5_95 Depth 5
                                        #             Child Loop BB5_97 Depth 6
                                        #               Child Loop BB5_102 Depth 7
	movq	(%rdx), %rcx
	movq	(%rcx,%rsi,8), %rbx
	cmpl	$0, 8(%rbx)
	jle	.LBB5_108
# BB#56:                                # %.lr.ph1510
                                        #   in Loop: Header=BB5_55 Depth=3
	movq	40(%r12), %rax
	movq	(%rax), %r9
	movq	%rsi, %rbp
	movq	464(%rsp), %rsi         # 8-byte Reload
	movq	16(%rsi), %rcx
	movq	24(%rsi), %r8
	movq	(%rcx), %rdx
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	16(%rcx), %rcx
	movq	(%rcx), %r11
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	16(%rdi), %rcx
	movq	24(%rdi), %r10
	movq	(%rcx), %rax
	movq	40(%rsi), %rcx
	movslq	(%rcx,%rbp,4), %rcx
	movq	40(%rdi), %rsi
	movslq	(%rsi,%rbp,4), %rdi
	movq	%rbp, 184(%rsp)         # 8-byte Spill
	leaq	(,%rbp,8), %rsi
	leaq	(%rsi,%rsi,2), %rsi
	leaq	4(%rdx,%rsi), %rbp
	movq	%rbp, 472(%rsp)         # 8-byte Spill
	addq	%rsi, %rdx
	movq	%rdx, 552(%rsp)         # 8-byte Spill
	leaq	4(%rax,%rsi), %rdx
	movq	%rdx, 536(%rsp)         # 8-byte Spill
	addq	%rsi, %rax
	movq	%rax, 544(%rsp)         # 8-byte Spill
	leaq	16(%r11,%rsi), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	addq	%rsi, %r11
	movq	%r11, 400(%rsp)         # 8-byte Spill
	leaq	(%r9,%rsi), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	leaq	4(%r9,%rsi), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	%r10, 520(%rsp)         # 8-byte Spill
	leaq	(%r10,%rdi,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	(%r8,%rcx,8), %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	shlq	$3, %rdi
	movq	%rdi, 352(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rbx, 560(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB5_57:                               #   Parent Loop BB5_47 Depth=1
                                        #     Parent Loop BB5_48 Depth=2
                                        #       Parent Loop BB5_55 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB5_61 Depth 5
                                        #             Child Loop BB5_63 Depth 6
                                        #               Child Loop BB5_65 Depth 7
                                        #               Child Loop BB5_69 Depth 7
                                        #           Child Loop BB5_75 Depth 5
                                        #             Child Loop BB5_79 Depth 6
                                        #               Child Loop BB5_81 Depth 7
                                        #                 Child Loop BB5_86 Depth 8
                                        #           Child Loop BB5_95 Depth 5
                                        #             Child Loop BB5_97 Depth 6
                                        #               Child Loop BB5_102 Depth 7
	movq	(%rbx), %r12
	movq	%rcx, 568(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rbx
	leaq	(%r12,%rbx,8), %rdi
	movq	528(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rsi
	leaq	148(%rsp), %rdx
	callq	hypre_BoxGetStrideSize
	movq	544(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r9d
	movq	536(%rsp), %rdi         # 8-byte Reload
	movl	8(%rdi), %ecx
	movl	%ecx, %eax
	subl	%r9d, %eax
	incl	%eax
	cmpl	%r9d, %ecx
	movl	(%rdi), %r15d
	movl	12(%rdi), %ecx
	movl	$0, %r11d
	cmovsl	%r11d, %eax
	movl	%ecx, %ebp
	subl	%r15d, %ebp
	incl	%ebp
	cmpl	%r15d, %ecx
	movq	552(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %edx
	movq	472(%rsp), %rcx         # 8-byte Reload
	movl	8(%rcx), %esi
	cmovsl	%r11d, %ebp
	movl	%esi, %r8d
	subl	%edx, %r8d
	incl	%r8d
	movl	%edx, 8(%rsp)           # 4-byte Spill
	cmpl	%edx, %esi
	movl	(%rcx), %r10d
	movl	12(%rcx), %esi
	cmovsl	%r11d, %r8d
	movl	%esi, %r13d
	subl	%r10d, %r13d
	incl	%r13d
	cmpl	%r10d, %esi
	movl	(%r12,%rbx,8), %ecx
	movl	4(%r12,%rbx,8), %edx
	movl	8(%r12,%rbx,8), %esi
	cmovsl	%r11d, %r13d
	movl	%ecx, 240(%rsp)         # 4-byte Spill
	movl	%ecx, %ebx
	subl	%r9d, %ebx
	movq	%rbx, 168(%rsp)         # 8-byte Spill
	movl	%edx, 192(%rsp)         # 4-byte Spill
	subl	%r15d, %edx
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movl	%esi, %ecx
	subl	4(%rdi), %ecx
	imull	%ebp, %ecx
	addl	%edx, %ecx
	imull	%eax, %ecx
	movq	%rcx, 376(%rsp)         # 8-byte Spill
	movl	4(%r14), %ecx
	movl	8(%r14), %edx
	imull	%eax, %ebp
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	imull	%ecx, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	%edx, 224(%rsp)         # 4-byte Spill
	imull	%edx, %ebp
	movl	%ebp, 160(%rsp)         # 4-byte Spill
	movl	148(%rsp), %eax
	movl	152(%rsp), %ecx
	cmpl	%eax, %ecx
	movq	%rax, 24(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	cmovgel	%ecx, %eax
	movl	156(%rsp), %ecx
	cmpl	%eax, %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	cmovgel	%ecx, %eax
	testl	%eax, %eax
	movslq	(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	jle	.LBB5_73
# BB#58:                                # %.lr.ph1409
                                        #   in Loop: Header=BB5_57 Depth=4
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB5_73
# BB#59:                                # %.lr.ph1409
                                        #   in Loop: Header=BB5_57 Depth=4
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB5_73
# BB#60:                                # %.preheader1371.us.preheader
                                        #   in Loop: Header=BB5_57 Depth=4
	movq	232(%rsp), %rax         # 8-byte Reload
	movl	%eax, %edx
	imull	%r8d, %edx
	movl	%r8d, %esi
	imull	%r13d, %esi
	imull	224(%rsp), %esi         # 4-byte Folded Reload
	movq	80(%rsp), %r12          # 8-byte Reload
	movl	%r12d, %ebx
	negl	%ebx
	movq	24(%rsp), %r9           # 8-byte Reload
	imull	%r9d, %ebx
	leal	(%rbx,%rdx), %ebp
	movq	88(%rsp), %rcx          # 8-byte Reload
	addl	%ecx, %ebx
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, %edi
	imull	%edx, %edi
	movl	%edi, 208(%rsp)         # 4-byte Spill
	subl	%edi, %esi
	movl	%esi, 20(%rsp)          # 4-byte Spill
	movl	%eax, %esi
	imull	%ecx, %esi
	movl	160(%rsp), %edi         # 4-byte Reload
	movl	%esi, 264(%rsp)         # 4-byte Spill
	subl	%esi, %edi
	movl	%edi, 272(%rsp)         # 4-byte Spill
	leal	-1(%rax), %eax
	imull	%eax, %ebp
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	addl	%edx, %ebp
	movl	%r12d, %edx
	imull	%r9d, %edx
	subl	%edx, %ebp
	movl	%ebp, 280(%rsp)         # 4-byte Spill
	imull	%eax, %ebx
	addl	%ecx, %ebx
	subl	%edx, %ebx
	movq	%rbx, 288(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	376(%rsp), %rcx         # 8-byte Reload
	leal	(%rax,%rcx), %ecx
	movl	240(%rsp), %eax         # 4-byte Reload
	subl	8(%rsp), %eax           # 4-byte Folded Reload
	movl	192(%rsp), %edx         # 4-byte Reload
	subl	%r10d, %edx
	movl	16(%rsp), %ebx          # 4-byte Reload
	movq	472(%rsp), %rsi         # 8-byte Reload
	subl	4(%rsi), %ebx
	imull	%r13d, %ebx
	addl	%edx, %ebx
	imull	%r8d, %ebx
	addl	%eax, %ebx
	leal	-1(%r9), %eax
	movl	%eax, 96(%rsp)          # 4-byte Spill
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	andl	$3, %r9d
	movq	%r12, %rdi
	shlq	$5, %rdi
	leaq	(,%r12,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	(%rax,%rax,2), %rbp
	movq	%r12, %r13
	shlq	$4, %r13
	movq	520(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp), %r10
	leaq	(%rax,%r13), %r15
	leaq	(%rax,%r12,8), %r12
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_61:                               # %.preheader1371.us
                                        #   Parent Loop BB5_47 Depth=1
                                        #     Parent Loop BB5_48 Depth=2
                                        #       Parent Loop BB5_55 Depth=3
                                        #         Parent Loop BB5_57 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB5_63 Depth 6
                                        #               Child Loop BB5_65 Depth 7
                                        #               Child Loop BB5_69 Depth 7
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	288(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	280(%rsp), %edx         # 4-byte Reload
	jle	.LBB5_72
# BB#62:                                # %.preheader1369.us.us.preheader
                                        #   in Loop: Header=BB5_61 Depth=5
	movl	%esi, (%rsp)            # 4-byte Spill
	xorl	%eax, %eax
	movl	%ebx, 120(%rsp)         # 4-byte Spill
	movl	%ebx, %r11d
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	movl	%ecx, %r14d
	.p2align	4, 0x90
.LBB5_63:                               # %.preheader1369.us.us
                                        #   Parent Loop BB5_47 Depth=1
                                        #     Parent Loop BB5_48 Depth=2
                                        #       Parent Loop BB5_55 Depth=3
                                        #         Parent Loop BB5_57 Depth=4
                                        #           Parent Loop BB5_61 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB5_65 Depth 7
                                        #               Child Loop BB5_69 Depth 7
	movl	%eax, 8(%rsp)           # 4-byte Spill
	testl	%r9d, %r9d
	movslq	%r14d, %r14
	movslq	%r11d, %r11
	movq	%r14, %rcx
	movq	%r11, %rsi
	movl	$0, %edx
	je	.LBB5_67
# BB#64:                                # %.prol.preheader
                                        #   in Loop: Header=BB5_63 Depth=6
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,8), %r8
	movq	216(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_65:                               #   Parent Loop BB5_47 Depth=1
                                        #     Parent Loop BB5_48 Depth=2
                                        #       Parent Loop BB5_55 Depth=3
                                        #         Parent Loop BB5_57 Depth=4
                                        #           Parent Loop BB5_61 Depth=5
                                        #             Parent Loop BB5_63 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movq	(%rcx,%rsi,8), %rbx
	movq	%rbx, (%r8,%rsi,8)
	incl	%edx
	addq	%rax, %rsi
	cmpl	%edx, %r9d
	jne	.LBB5_65
# BB#66:                                # %.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB5_63 Depth=6
	leaq	(%r14,%rsi), %rcx
	addq	%r11, %rsi
.LBB5_67:                               # %.prol.loopexit
                                        #   in Loop: Header=BB5_63 Depth=6
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movq	%r11, 40(%rsp)          # 8-byte Spill
	cmpl	$3, 96(%rsp)            # 4-byte Folded Reload
	jb	.LBB5_70
# BB#68:                                # %.preheader1369.us.us.new
                                        #   in Loop: Header=BB5_63 Depth=6
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,8), %r14
	movq	352(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rbx
	movq	216(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	(%rsi,%rax), %r11
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r8d
	subl	%edx, %r8d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_69:                               #   Parent Loop BB5_47 Depth=1
                                        #     Parent Loop BB5_48 Depth=2
                                        #       Parent Loop BB5_55 Depth=3
                                        #         Parent Loop BB5_57 Depth=4
                                        #           Parent Loop BB5_61 Depth=5
                                        #             Parent Loop BB5_63 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movq	%r10, %rax
	movq	(%rsi,%rdx), %r10
	movq	%r10, (%r14,%rdx)
	movq	%rax, %r10
	movq	(%r11,%rdx), %rax
	leaq	(%rbx,%rdx), %rcx
	movq	%rax, (%r12,%rcx)
	leaq	(%r13,%rdx), %rax
	movq	(%rsi,%rax), %rax
	movq	%rax, (%r15,%rcx)
	leaq	(%rbp,%rdx), %rax
	movq	(%rsi,%rax), %rax
	movq	%rax, (%r10,%rcx)
	addq	%rdi, %rdx
	addl	$-4, %r8d
	jne	.LBB5_69
.LBB5_70:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB5_63 Depth=6
	movq	40(%rsp), %r11          # 8-byte Reload
	addl	128(%rsp), %r11d        # 4-byte Folded Reload
	movq	48(%rsp), %r14          # 8-byte Reload
	addl	88(%rsp), %r14d         # 4-byte Folded Reload
	movl	8(%rsp), %eax           # 4-byte Reload
	incl	%eax
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB5_63
# BB#71:                                #   in Loop: Header=BB5_61 Depth=5
	movl	264(%rsp), %eax         # 4-byte Reload
	movl	208(%rsp), %edx         # 4-byte Reload
	movl	64(%rsp), %ecx          # 4-byte Reload
	movl	120(%rsp), %ebx         # 4-byte Reload
	movl	(%rsp), %esi            # 4-byte Reload
.LBB5_72:                               # %._crit_edge1383.us
                                        #   in Loop: Header=BB5_61 Depth=5
	addl	%ebx, %edx
	addl	%ecx, %eax
	addl	20(%rsp), %edx          # 4-byte Folded Reload
	addl	272(%rsp), %eax         # 4-byte Folded Reload
	incl	%esi
	cmpl	4(%rsp), %esi           # 4-byte Folded Reload
	movl	%edx, %ebx
	movl	%eax, %ecx
	jne	.LBB5_61
.LBB5_73:                               # %.preheader1372
                                        #   in Loop: Header=BB5_57 Depth=4
	cmpl	$0, 480(%rsp)           # 4-byte Folded Reload
	movsd	104(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
	jle	.LBB5_91
# BB#74:                                # %.lr.ph1460
                                        #   in Loop: Header=BB5_57 Depth=4
	movl	148(%rsp), %edx
	movl	152(%rsp), %r11d
	movl	156(%rsp), %r9d
	cmpl	%edx, %r11d
	movl	%edx, %eax
	cmovgel	%r11d, %eax
	cmpl	%eax, %r9d
	cmovgel	%r9d, %eax
	movl	%eax, 328(%rsp)         # 4-byte Spill
	leal	-1(%r11), %r8d
	leal	-1(%rdx), %esi
	incq	%rsi
	movl	%edx, %ebx
	movq	80(%rsp), %rbp          # 8-byte Reload
	imull	%ebp, %ebx
	movl	%ebp, %ecx
	negl	%ecx
	imull	%edx, %ecx
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 584(%rsp)         # 8-byte Spill
	leal	(%rcx,%rax), %r10d
	movl	%r11d, %edi
	imull	%eax, %edi
	movl	160(%rsp), %ecx         # 4-byte Reload
	movl	%edi, 176(%rsp)         # 4-byte Spill
	subl	%edi, %ecx
	movl	%ecx, 144(%rsp)         # 4-byte Spill
	movl	%r8d, 384(%rsp)         # 4-byte Spill
	movl	%r10d, 128(%rsp)        # 4-byte Spill
	imull	%r10d, %r8d
	addl	%eax, %r8d
	movl	%ebx, 428(%rsp)         # 4-byte Spill
	subl	%ebx, %r8d
	movl	%r8d, 140(%rsp)         # 4-byte Spill
	imulq	%rbp, %rsi
	movl	%r9d, 180(%rsp)         # 4-byte Spill
	testl	%r9d, %r9d
	setle	%al
	movq	%r11, 40(%rsp)          # 8-byte Spill
	testl	%r11d, %r11d
	setle	%cl
	orb	%al, %cl
	movb	%cl, 392(%rsp)          # 1-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%edx, %eax
	andl	$1, %eax
	movl	%eax, 96(%rsp)          # 4-byte Spill
	movq	%rbp, %r11
	shlq	$4, %r11
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	376(%rsp), %rdx         # 8-byte Reload
	leal	(%rax,%rdx), %eax
	movl	%eax, 424(%rsp)         # 4-byte Spill
	leaq	(,%rbp,8), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_75:                               #   Parent Loop BB5_47 Depth=1
                                        #     Parent Loop BB5_48 Depth=2
                                        #       Parent Loop BB5_55 Depth=3
                                        #         Parent Loop BB5_57 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB5_79 Depth 6
                                        #               Child Loop BB5_81 Depth 7
                                        #                 Child Loop BB5_86 Depth 8
	movq	%rax, 336(%rsp)         # 8-byte Spill
	cmpq	408(%rsp), %rax         # 8-byte Folded Reload
	je	.LBB5_90
# BB#76:                                #   in Loop: Header=BB5_75 Depth=5
	movq	248(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r8d
	movq	200(%rsp), %rdx         # 8-byte Reload
	movl	8(%rdx), %eax
	movl	%eax, %ecx
	subl	%r8d, %ecx
	incl	%ecx
	cmpl	%r8d, %eax
	movl	(%rdx), %r9d
	movl	12(%rdx), %eax
	movl	$0, %edx
	cmovsl	%edx, %ecx
	movl	%eax, %r15d
	subl	%r9d, %r15d
	incl	%r15d
	cmpl	%r9d, %eax
	movq	256(%rsp), %rbp         # 8-byte Reload
	movl	-4(%rbp), %eax
	movq	400(%rsp), %rdi         # 8-byte Reload
	movl	(%rdi), %ebx
	cmovsl	%edx, %r15d
	movl	%eax, %edi
	subl	%ebx, %edi
	incl	%edi
	cmpl	%ebx, %eax
	movl	(%rbp), %eax
	movl	-12(%rbp), %r13d
	cmovsl	%edx, %edi
	movl	%eax, %ebp
	subl	%r13d, %ebp
	incl	%ebp
	cmpl	%r13d, %eax
	cmovsl	%edx, %ebp
	cmpl	$0, 328(%rsp)           # 4-byte Folded Reload
	jle	.LBB5_90
# BB#77:                                # %.lr.ph1456
                                        #   in Loop: Header=BB5_75 Depth=5
	cmpb	$0, 392(%rsp)           # 1-byte Folded Reload
	jne	.LBB5_90
# BB#78:                                # %.preheader1368.us.preheader
                                        #   in Loop: Header=BB5_75 Depth=5
	movq	320(%rsp), %rdx         # 8-byte Reload
	movq	48(%rdx), %r10
	movq	64(%rdx), %rdx
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	(%rdx,%rax,8), %rdx
	movl	%r8d, 72(%rsp)          # 4-byte Spill
	movl	%r9d, 24(%rsp)          # 4-byte Spill
	movq	336(%rsp), %r8          # 8-byte Reload
	movslq	(%rdx,%r8,4), %rdx
	leaq	(%r10,%rdx,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%ebx, (%rsp)            # 4-byte Spill
	leaq	(%r8,%r8,2), %r9
	movq	576(%rsp), %rax         # 8-byte Reload
	movslq	(%rax,%r9,4), %r8
	movl	8(%rax,%r9,4), %edx
	imull	%ebp, %edx
	addl	4(%rax,%r9,4), %edx
	imull	%edi, %edx
	movslq	%edx, %rax
	addq	%r8, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	232(%rsp), %rax         # 8-byte Reload
	movl	%eax, %edx
	imull	%ecx, %edx
	movl	%eax, %r10d
	imull	%edi, %r10d
	movq	584(%rsp), %r8          # 8-byte Reload
	leal	(%r8,%rdx), %r12d
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rbx
	imull	%edx, %eax
	movl	%eax, 136(%rsp)         # 4-byte Spill
	movl	384(%rsp), %eax         # 4-byte Reload
	movl	%eax, %r14d
	imull	%r12d, %r14d
	addl	%edx, %r14d
	movl	%ebx, %edx
	imull	%r10d, %edx
	movl	%edx, 344(%rsp)         # 4-byte Spill
	leal	(%r8,%r10), %edx
	movl	%eax, %r8d
	movl	%edx, 32(%rsp)          # 4-byte Spill
	imull	%edx, %r8d
	addl	%r10d, %r8d
	movl	240(%rsp), %ebx         # 4-byte Reload
	movl	%ebx, %r10d
	subl	(%rsp), %r10d           # 4-byte Folded Reload
	movl	192(%rsp), %r9d         # 4-byte Reload
	subl	%r13d, %r9d
	movl	%edi, %edx
	imull	%ebp, %edx
	movl	16(%rsp), %r13d         # 4-byte Reload
	movq	256(%rsp), %rax         # 8-byte Reload
	subl	-8(%rax), %r13d
	imull	%ebp, %r13d
	addl	%r9d, %r13d
	imull	%edi, %r13d
	addl	%r10d, %r13d
	movl	%r13d, %r9d
	movl	%ebx, %ebp
	subl	72(%rsp), %ebp          # 4-byte Folded Reload
	movl	192(%rsp), %r10d        # 4-byte Reload
	subl	24(%rsp), %r10d         # 4-byte Folded Reload
	movl	%ecx, %ebx
	imull	%r15d, %ebx
	movl	16(%rsp), %eax          # 4-byte Reload
	movq	200(%rsp), %rdi         # 8-byte Reload
	subl	4(%rdi), %eax
	imull	%r15d, %eax
	addl	%r10d, %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	24(%rdi), %r10
	imull	%ecx, %eax
	movq	40(%rdi), %rcx
	movl	%eax, %edi
	movq	184(%rsp), %rax         # 8-byte Reload
	movslq	(%rcx,%rax,4), %rcx
	addl	%ebp, %edi
	leaq	(%r10,%rcx,8), %rax
	movq	64(%rsp), %rbp          # 8-byte Reload
	leaq	(%rax,%rbp,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	224(%rsp), %eax         # 4-byte Reload
	imull	%eax, %edx
	imull	%eax, %ebx
	subl	136(%rsp), %ebx         # 4-byte Folded Reload
	movl	%ebx, 264(%rsp)         # 4-byte Spill
	subl	344(%rsp), %edx         # 4-byte Folded Reload
	movl	%edx, 208(%rsp)         # 4-byte Spill
	movl	428(%rsp), %eax         # 4-byte Reload
	subl	%eax, %r14d
	movl	%r14d, 280(%rsp)        # 4-byte Spill
	subl	%eax, %r8d
	movl	%r8d, 272(%rsp)         # 4-byte Spill
	movq	%rbp, %rax
	addq	%rcx, %rax
	leaq	(%r10,%rax,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movl	424(%rsp), %eax         # 4-byte Reload
	.p2align	4, 0x90
.LBB5_79:                               # %.preheader1368.us
                                        #   Parent Loop BB5_47 Depth=1
                                        #     Parent Loop BB5_48 Depth=2
                                        #       Parent Loop BB5_55 Depth=3
                                        #         Parent Loop BB5_57 Depth=4
                                        #           Parent Loop BB5_75 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB5_81 Depth 7
                                        #                 Child Loop BB5_86 Depth 8
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movl	140(%rsp), %ebp         # 4-byte Reload
	movl	272(%rsp), %ecx         # 4-byte Reload
	movl	280(%rsp), %edx         # 4-byte Reload
	jle	.LBB5_89
# BB#80:                                # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB5_79 Depth=6
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	xorl	%r13d, %r13d
	movl	%edi, 4(%rsp)           # 4-byte Spill
	movl	%edi, %ecx
	movl	%r9d, (%rsp)            # 4-byte Spill
	movl	%r9d, %edi
	movl	%eax, 288(%rsp)         # 4-byte Spill
	movl	%eax, %ebp
	.p2align	4, 0x90
.LBB5_81:                               # %.preheader.us.us
                                        #   Parent Loop BB5_47 Depth=1
                                        #     Parent Loop BB5_48 Depth=2
                                        #       Parent Loop BB5_55 Depth=3
                                        #         Parent Loop BB5_57 Depth=4
                                        #           Parent Loop BB5_75 Depth=5
                                        #             Parent Loop BB5_79 Depth=6
                                        # =>            This Loop Header: Depth=7
                                        #                 Child Loop BB5_86 Depth 8
	movslq	%edi, %rdi
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	movslq	%ecx, %r10
	movslq	%ebp, %rax
	jne	.LBB5_83
# BB#82:                                #   in Loop: Header=BB5_81 Depth=7
	movq	%rdi, %r8
	xorl	%edx, %edx
	jmp	.LBB5_84
	.p2align	4, 0x90
.LBB5_83:                               #   in Loop: Header=BB5_81 Depth=7
	movq	48(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%r10,8), %xmm0    # xmm0 = mem[0],zero
	movq	72(%rsp), %rdx          # 8-byte Reload
	mulsd	(%rdx,%rdi,8), %xmm0
	movq	56(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdx,%rax,8)
	movq	80(%rsp), %rdx          # 8-byte Reload
	addq	%rdx, %r10
	leaq	(%rdi,%rdx), %r8
	addq	%rdx, %rax
	movl	$1, %edx
.LBB5_84:                               # %.prol.loopexit1772
                                        #   in Loop: Header=BB5_81 Depth=7
	addq	%rsi, %rdi
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB5_87
# BB#85:                                # %.preheader.us.us.new
                                        #   in Loop: Header=BB5_81 Depth=7
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movq	56(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %rax
	movq	%rsi, %r15
	movq	120(%rsp), %rsi         # 8-byte Reload
	leaq	(%rax,%rsi), %rdx
	movq	64(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%r8,8), %r14
	leaq	(%r14,%rsi), %r9
	movq	48(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%r10,8), %rbx
	leaq	(%rbx,%rsi), %r10
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%esi, %r8d
	movq	%r15, %rsi
	subl	24(%rsp), %r8d          # 4-byte Folded Reload
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB5_86:                               #   Parent Loop BB5_47 Depth=1
                                        #     Parent Loop BB5_48 Depth=2
                                        #       Parent Loop BB5_55 Depth=3
                                        #         Parent Loop BB5_57 Depth=4
                                        #           Parent Loop BB5_75 Depth=5
                                        #             Parent Loop BB5_79 Depth=6
                                        #               Parent Loop BB5_81 Depth=7
                                        # =>              This Inner Loop Header: Depth=8
	movsd	(%rbx,%r15), %xmm0      # xmm0 = mem[0],zero
	mulsd	(%r14,%r15), %xmm0
	movsd	(%rax,%r15), %xmm1      # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax,%r15)
	movsd	(%r10,%r15), %xmm0      # xmm0 = mem[0],zero
	mulsd	(%r9,%r15), %xmm0
	movsd	(%rdx,%r15), %xmm1      # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdx,%r15)
	addq	%r11, %r15
	addl	$-2, %r8d
	jne	.LBB5_86
.LBB5_87:                               # %._crit_edge1415.us.us
                                        #   in Loop: Header=BB5_81 Depth=7
	addl	%esi, %ecx
	addl	%esi, %ebp
	addl	%r12d, %ecx
	addl	32(%rsp), %edi          # 4-byte Folded Reload
	addl	128(%rsp), %ebp         # 4-byte Folded Reload
	incl	%r13d
	cmpl	40(%rsp), %r13d         # 4-byte Folded Reload
	jne	.LBB5_81
# BB#88:                                #   in Loop: Header=BB5_79 Depth=6
	movl	176(%rsp), %ebp         # 4-byte Reload
	movl	344(%rsp), %ecx         # 4-byte Reload
	movl	136(%rsp), %edx         # 4-byte Reload
	movl	(%rsp), %r9d            # 4-byte Reload
	movl	4(%rsp), %edi           # 4-byte Reload
	movl	20(%rsp), %ebx          # 4-byte Reload
	movl	288(%rsp), %eax         # 4-byte Reload
.LBB5_89:                               # %._crit_edge1423.us
                                        #   in Loop: Header=BB5_79 Depth=6
	addl	%edi, %edx
	addl	%r9d, %ecx
	addl	%eax, %ebp
	addl	264(%rsp), %edx         # 4-byte Folded Reload
	addl	208(%rsp), %ecx         # 4-byte Folded Reload
	addl	144(%rsp), %ebp         # 4-byte Folded Reload
	incl	%ebx
	cmpl	180(%rsp), %ebx         # 4-byte Folded Reload
	movl	%edx, %edi
	movl	%ecx, %r9d
	movl	%ebp, %eax
	jne	.LBB5_79
	.p2align	4, 0x90
.LBB5_90:                               # %.loopexit
                                        #   in Loop: Header=BB5_75 Depth=5
	movq	336(%rsp), %rax         # 8-byte Reload
	incq	%rax
	cmpq	480(%rsp), %rax         # 8-byte Folded Reload
	jne	.LBB5_75
.LBB5_91:                               # %._crit_edge1461
                                        #   in Loop: Header=BB5_57 Depth=4
	movq	248(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r9d
	movq	200(%rsp), %rcx         # 8-byte Reload
	movl	8(%rcx), %eax
	movl	%eax, %edx
	subl	%r9d, %edx
	incl	%edx
	cmpl	%r9d, %eax
	movl	(%rcx), %r14d
	movl	12(%rcx), %eax
	movl	$0, %ecx
	cmovsl	%ecx, %edx
	movl	%eax, %edi
	subl	%r14d, %edi
	incl	%edi
	cmpl	%r14d, %eax
	cmovsl	%ecx, %edi
	movl	148(%rsp), %eax
	movl	152(%rsp), %esi
	movl	156(%rsp), %ebx
	cmpl	%eax, %esi
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%eax, %ecx
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	cmovgel	%esi, %ecx
	cmpl	%ecx, %ebx
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	cmovgel	%ebx, %ecx
	testl	%ecx, %ecx
	movl	16(%rsp), %eax          # 4-byte Reload
	movq	168(%rsp), %r8          # 8-byte Reload
	jle	.LBB5_106
# BB#92:                                # %.lr.ph1500
                                        #   in Loop: Header=BB5_57 Depth=4
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB5_106
# BB#93:                                # %.lr.ph1500
                                        #   in Loop: Header=BB5_57 Depth=4
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB5_106
# BB#94:                                # %.preheader1370.us.preheader
                                        #   in Loop: Header=BB5_57 Depth=4
	movq	320(%rsp), %rsi         # 8-byte Reload
	movq	48(%rsi), %rcx
	movq	64(%rsi), %rsi
	movq	184(%rsp), %rbx         # 8-byte Reload
	movq	(%rsi,%rbx,8), %rsi
	movq	368(%rsp), %rbx         # 8-byte Reload
	movslq	(%rsi,%rbx,4), %rsi
	leaq	(%rcx,%rsi,8), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	232(%rsp), %rcx         # 8-byte Reload
	imull	%edx, %ecx
	movl	%edx, %ebp
	imull	%edi, %ebp
	imull	224(%rsp), %ebp         # 4-byte Folded Reload
	movq	80(%rsp), %r10          # 8-byte Reload
	movl	%r10d, %ebx
	negl	%ebx
	movq	8(%rsp), %r11           # 8-byte Reload
	imull	%r11d, %ebx
	leal	(%rbx,%rcx), %r15d
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, %r13
	movl	%r13d, %r12d
	imull	%ecx, %r12d
	leal	-1(%r13), %esi
	movl	%r15d, 128(%rsp)        # 4-byte Spill
	imull	%esi, %r15d
	addl	%ecx, %r15d
	movq	88(%rsp), %rcx          # 8-byte Reload
	addl	%ecx, %ebx
                                        # kill: %R13D<def> %R13D<kill> %R13<kill>
	imull	%ecx, %r13d
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	imull	%ebx, %esi
	addl	%ecx, %esi
	movl	%r10d, %ecx
	imull	%r11d, %ecx
	subl	%ecx, %r15d
	movl	%r15d, 120(%rsp)        # 4-byte Spill
	subl	%ecx, %esi
	movl	%esi, 64(%rsp)          # 4-byte Spill
	addl	376(%rsp), %r8d         # 4-byte Folded Reload
	movl	240(%rsp), %ebx         # 4-byte Reload
	subl	%r9d, %ebx
	movl	%r12d, 88(%rsp)         # 4-byte Spill
	subl	%r12d, %ebp
	movl	%ebp, 72(%rsp)          # 4-byte Spill
	movl	%r13d, (%rsp)           # 4-byte Spill
	subl	%r13d, 160(%rsp)        # 4-byte Folded Spill
	movl	192(%rsp), %esi         # 4-byte Reload
	subl	%r14d, %esi
	leal	-1(%r11), %ebp
	incq	%rbp
	imulq	%r10, %rbp
	movq	200(%rsp), %rcx         # 8-byte Reload
	subl	4(%rcx), %eax
	imull	%edi, %eax
	addl	%esi, %eax
	imull	%edx, %eax
	addl	%ebx, %eax
	movl	%r11d, %edx
	andl	$1, %edx
	movq	%r10, %rdi
	shlq	$4, %rdi
	leaq	(,%r10,8), %r9
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_95:                               # %.preheader1370.us
                                        #   Parent Loop BB5_47 Depth=1
                                        #     Parent Loop BB5_48 Depth=2
                                        #       Parent Loop BB5_55 Depth=3
                                        #         Parent Loop BB5_57 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB5_97 Depth 6
                                        #               Child Loop BB5_102 Depth 7
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movl	64(%rsp), %ecx          # 4-byte Reload
	movl	120(%rsp), %esi         # 4-byte Reload
	jle	.LBB5_105
# BB#96:                                # %.preheader1367.us.us.preheader
                                        #   in Loop: Header=BB5_95 Depth=5
	movl	%ebx, 96(%rsp)          # 4-byte Spill
	xorl	%r13d, %r13d
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	%eax, %r10d
	movq	%r8, 168(%rsp)          # 8-byte Spill
	movl	%r8d, %r11d
	.p2align	4, 0x90
.LBB5_97:                               # %.preheader1367.us.us
                                        #   Parent Loop BB5_47 Depth=1
                                        #     Parent Loop BB5_48 Depth=2
                                        #       Parent Loop BB5_55 Depth=3
                                        #         Parent Loop BB5_57 Depth=4
                                        #           Parent Loop BB5_95 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB5_102 Depth 7
	movslq	%r10d, %r10
	testl	%edx, %edx
	movslq	%r11d, %rcx
	jne	.LBB5_99
# BB#98:                                #   in Loop: Header=BB5_97 Depth=6
	movq	%r10, %rbx
	xorl	%esi, %esi
	jmp	.LBB5_100
	.p2align	4, 0x90
.LBB5_99:                               #   in Loop: Header=BB5_97 Depth=6
	movq	56(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	48(%rsp), %rsi          # 8-byte Reload
	divsd	(%rsi,%r10,8), %xmm0
	movsd	%xmm0, (%rax,%rcx,8)
	movq	80(%rsp), %rax          # 8-byte Reload
	leaq	(%r10,%rax), %rbx
	addq	%rax, %rcx
	movl	$1, %esi
.LBB5_100:                              # %.prol.loopexit1776
                                        #   in Loop: Header=BB5_97 Depth=6
	addq	%rbp, %r10
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB5_103
# BB#101:                               # %.preheader1367.us.us.new
                                        #   in Loop: Header=BB5_97 Depth=6
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rcx
	leaq	(%rcx,%r9), %r15
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,8), %r14
	leaq	(%r14,%r9), %r12
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %r8d
	subl	%esi, %r8d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_102:                              #   Parent Loop BB5_47 Depth=1
                                        #     Parent Loop BB5_48 Depth=2
                                        #       Parent Loop BB5_55 Depth=3
                                        #         Parent Loop BB5_57 Depth=4
                                        #           Parent Loop BB5_95 Depth=5
                                        #             Parent Loop BB5_97 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movsd	(%rcx,%rsi), %xmm0      # xmm0 = mem[0],zero
	divsd	(%r14,%rsi), %xmm0
	movsd	%xmm0, (%rcx,%rsi)
	movsd	(%r15,%rsi), %xmm0      # xmm0 = mem[0],zero
	divsd	(%r12,%rsi), %xmm0
	movsd	%xmm0, (%r15,%rsi)
	addq	%rdi, %rsi
	addl	$-2, %r8d
	jne	.LBB5_102
.LBB5_103:                              # %._crit_edge1468.us.us
                                        #   in Loop: Header=BB5_97 Depth=6
	addl	%ebp, %r11d
	addl	128(%rsp), %r10d        # 4-byte Folded Reload
	addl	32(%rsp), %r11d         # 4-byte Folded Reload
	incl	%r13d
	cmpl	40(%rsp), %r13d         # 4-byte Folded Reload
	jne	.LBB5_97
# BB#104:                               #   in Loop: Header=BB5_95 Depth=5
	movl	(%rsp), %ecx            # 4-byte Reload
	movl	88(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %eax          # 4-byte Reload
	movq	168(%rsp), %r8          # 8-byte Reload
	movl	96(%rsp), %ebx          # 4-byte Reload
.LBB5_105:                              # %._crit_edge1474.us
                                        #   in Loop: Header=BB5_95 Depth=5
	addl	%eax, %esi
	addl	%r8d, %ecx
	addl	72(%rsp), %esi          # 4-byte Folded Reload
	addl	160(%rsp), %ecx         # 4-byte Folded Reload
	incl	%ebx
	cmpl	24(%rsp), %ebx          # 4-byte Folded Reload
	movl	%esi, %eax
	movl	%ecx, %r8d
	jne	.LBB5_95
.LBB5_106:                              # %._crit_edge1501
                                        #   in Loop: Header=BB5_57 Depth=4
	movq	568(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movq	560(%rsp), %rbx         # 8-byte Reload
	movslq	8(%rbx), %rax
	cmpq	%rax, %rcx
	jl	.LBB5_57
# BB#107:                               # %._crit_edge1511.loopexit
                                        #   in Loop: Header=BB5_55 Depth=3
	movq	360(%rsp), %rdx         # 8-byte Reload
	movl	8(%rdx), %eax
	movq	320(%rsp), %r12         # 8-byte Reload
	movq	184(%rsp), %rsi         # 8-byte Reload
.LBB5_108:                              # %._crit_edge1511
                                        #   in Loop: Header=BB5_55 Depth=3
	incq	%rsi
	movslq	%eax, %rcx
	cmpq	%rcx, %rsi
	jl	.LBB5_55
.LBB5_109:                              # %._crit_edge1527
                                        #   in Loop: Header=BB5_48 Depth=2
	movl	420(%rsp), %eax         # 4-byte Reload
	incl	%eax
	cmpl	$2, %eax
	jne	.LBB5_48
# BB#110:                               #   in Loop: Header=BB5_47 Depth=1
	movq	%rdx, %r14
	ucomisd	.LCPI5_0(%rip), %xmm2
	jne	.LBB5_111
	jnp	.LBB5_112
.LBB5_111:                              #   in Loop: Header=BB5_47 Depth=1
	movsd	488(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdi
	callq	hypre_StructScale
	movsd	104(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	%rbx, %rsi
	callq	hypre_StructAxpy
	jmp	.LBB5_113
	.p2align	4, 0x90
.LBB5_112:                              #   in Loop: Header=BB5_47 Depth=1
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
	callq	hypre_StructCopy
.LBB5_113:                              #   in Loop: Header=BB5_47 Depth=1
	movl	308(%rsp), %ecx         # 4-byte Reload
	movl	304(%rsp), %eax         # 4-byte Reload
	incl	%eax
	cltd
	idivl	300(%rsp)               # 4-byte Folded Reload
	cmpl	$1, %edx
	adcl	$0, %ecx
	cmpl	296(%rsp), %ecx         # 4-byte Folded Reload
	jl	.LBB5_47
.LBB5_114:                              # %._crit_edge1552
	movq	432(%rsp), %rbx         # 8-byte Reload
	movl	%ecx, 128(%rbx)
	movl	136(%rbx), %edi
	callq	hypre_IncFLOPCount
	movl	132(%rbx), %edi
.LBB5_115:
	callq	hypre_EndTiming
	xorl	%eax, %eax
	addq	$600, %rsp              # imm = 0x258
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	hypre_PointRelax, .Lfunc_end5-hypre_PointRelax
	.cfi_endproc

	.globl	hypre_PointRelaxSetTol
	.p2align	4, 0x90
	.type	hypre_PointRelaxSetTol,@function
hypre_PointRelaxSetTol:                 # @hypre_PointRelaxSetTol
	.cfi_startproc
# BB#0:
	movsd	%xmm0, 8(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	hypre_PointRelaxSetTol, .Lfunc_end6-hypre_PointRelaxSetTol
	.cfi_endproc

	.globl	hypre_PointRelaxSetMaxIter
	.p2align	4, 0x90
	.type	hypre_PointRelaxSetMaxIter,@function
hypre_PointRelaxSetMaxIter:             # @hypre_PointRelaxSetMaxIter
	.cfi_startproc
# BB#0:
	movl	%esi, 16(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	hypre_PointRelaxSetMaxIter, .Lfunc_end7-hypre_PointRelaxSetMaxIter
	.cfi_endproc

	.globl	hypre_PointRelaxSetZeroGuess
	.p2align	4, 0x90
	.type	hypre_PointRelaxSetZeroGuess,@function
hypre_PointRelaxSetZeroGuess:           # @hypre_PointRelaxSetZeroGuess
	.cfi_startproc
# BB#0:
	movl	%esi, 24(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	hypre_PointRelaxSetZeroGuess, .Lfunc_end8-hypre_PointRelaxSetZeroGuess
	.cfi_endproc

	.globl	hypre_PointRelaxSetWeight
	.p2align	4, 0x90
	.type	hypre_PointRelaxSetWeight,@function
hypre_PointRelaxSetWeight:              # @hypre_PointRelaxSetWeight
	.cfi_startproc
# BB#0:
	movsd	%xmm0, 32(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	hypre_PointRelaxSetWeight, .Lfunc_end9-hypre_PointRelaxSetWeight
	.cfi_endproc

	.globl	hypre_PointRelaxSetPointsetRank
	.p2align	4, 0x90
	.type	hypre_PointRelaxSetPointsetRank,@function
hypre_PointRelaxSetPointsetRank:        # @hypre_PointRelaxSetPointsetRank
	.cfi_startproc
# BB#0:
	movq	56(%rdi), %rax
	movslq	%esi, %rcx
	movl	%edx, (%rax,%rcx,4)
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	hypre_PointRelaxSetPointsetRank, .Lfunc_end10-hypre_PointRelaxSetPointsetRank
	.cfi_endproc

	.globl	hypre_PointRelaxSetTempVec
	.p2align	4, 0x90
	.type	hypre_PointRelaxSetTempVec,@function
hypre_PointRelaxSetTempVec:             # @hypre_PointRelaxSetTempVec
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 32
.Lcfi70:
	.cfi_offset %rbx, -24
.Lcfi71:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	104(%rbx), %rdi
	callq	hypre_StructVectorDestroy
	movq	%r14, %rdi
	callq	hypre_StructVectorRef
	movq	%rax, 104(%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	hypre_PointRelaxSetTempVec, .Lfunc_end11-hypre_PointRelaxSetTempVec
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"PointRelax"
	.size	.L.str, 11


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
