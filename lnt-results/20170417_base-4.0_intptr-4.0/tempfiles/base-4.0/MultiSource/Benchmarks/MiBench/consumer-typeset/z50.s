	.text
	.file	"z50.bc"
	.globl	PDF_PrintAfterLastPage
	.p2align	4, 0x90
	.type	PDF_PrintAfterLastPage,@function
PDF_PrintAfterLastPage:                 # @PDF_PrintAfterLastPage
	.cfi_startproc
# BB#0:
	cmpb	$1, prologue_done(%rip)
	jne	.LBB0_1
# BB#2:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	out_fp(%rip), %rdi
	callq	PDFPage_Cleanup
	movq	out_fp(%rip), %rdi
	popq	%rax
	jmp	PDFFile_Cleanup         # TAILCALL
.LBB0_1:
	retq
.Lfunc_end0:
	.size	PDF_PrintAfterLastPage, .Lfunc_end0-PDF_PrintAfterLastPage
	.cfi_endproc

	.globl	PDF_SaveGraphicState
	.p2align	4, 0x90
	.type	PDF_SaveGraphicState,@function
PDF_SaveGraphicState:                   # @PDF_SaveGraphicState
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	out_fp(%rip), %rdi
	callq	PDFPage_Push
	movl	gs_stack_top(%rip), %ecx
	leal	1(%rcx), %eax
	movl	%eax, gs_stack_top(%rip)
	cmpl	$49, %ecx
	jl	.LBB1_2
# BB#1:
	addq	$32, %rbx
	movl	$50, %edi
	movl	$1, %esi
	movl	$.L.str, %edx
	movl	$1, %ecx
	movl	$50, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movl	gs_stack_top(%rip), %eax
.LBB1_2:
	movl	currentfont(%rip), %ecx
	cltq
	shlq	$2, %rax
	movl	%ecx, gs_stack(%rax,%rax,4)
	movl	currentcolour(%rip), %ecx
	movl	%ecx, gs_stack+4(%rax,%rax,4)
	movl	cpexists(%rip), %ecx
	movl	%ecx, gs_stack+8(%rax,%rax,4)
	movl	currenty(%rip), %ecx
	movl	%ecx, gs_stack+12(%rax,%rax,4)
	movzwl	currentxheight2(%rip), %ecx
	movw	%cx, gs_stack+16(%rax,%rax,4)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	PDF_SaveGraphicState, .Lfunc_end1-PDF_SaveGraphicState
	.cfi_endproc

	.globl	PDF_RestoreGraphicState
	.p2align	4, 0x90
	.type	PDF_RestoreGraphicState,@function
PDF_RestoreGraphicState:                # @PDF_RestoreGraphicState
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 16
	movq	out_fp(%rip), %rdi
	callq	PDFPage_Pop
	movslq	gs_stack_top(%rip), %rax
	leal	-1(%rax), %ecx
	shlq	$2, %rax
	movl	gs_stack(%rax,%rax,4), %edx
	movl	%edx, currentfont(%rip)
	movl	gs_stack+4(%rax,%rax,4), %edx
	movl	%edx, currentcolour(%rip)
	movl	gs_stack+8(%rax,%rax,4), %edx
	movl	%edx, cpexists(%rip)
	movl	gs_stack+12(%rax,%rax,4), %edx
	movl	%edx, currenty(%rip)
	movzwl	gs_stack+16(%rax,%rax,4), %eax
	movw	%ax, currentxheight2(%rip)
	movl	%ecx, gs_stack_top(%rip)
	popq	%rax
	retq
.Lfunc_end2:
	.size	PDF_RestoreGraphicState, .Lfunc_end2-PDF_RestoreGraphicState
	.cfi_endproc

	.globl	PDF_PrintGraphicObject
	.p2align	4, 0x90
	.type	PDF_PrintGraphicObject,@function
PDF_PrintGraphicObject:                 # @PDF_PrintGraphicObject
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 32
.Lcfi7:
	.cfi_offset %rbx, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movb	32(%r15), %al
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jae	.LBB3_1
# BB#17:
	movq	out_fp(%rip), %rdi
	addq	$64, %r15
	movq	%r15, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	PDFPage_WriteGraphic    # TAILCALL
.LBB3_1:
	leaq	32(%r15), %r14
	cmpb	$17, %al
	jne	.LBB3_16
# BB#2:                                 # %.preheader23
	movq	8(%r15), %rbx
	cmpq	%r15, %rbx
	jne	.LBB3_4
	jmp	.LBB3_14
.LBB3_7:                                #   in Loop: Header=BB3_4 Depth=1
	cmpb	$0, 42(%rdi)
	je	.LBB3_9
# BB#8:                                 #   in Loop: Header=BB3_4 Depth=1
	movq	out_fp(%rip), %rdi
	movl	$.L.str.1, %esi
	callq	PDFPage_Write
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB3_4
	jmp	.LBB3_14
.LBB3_12:                               #   in Loop: Header=BB3_4 Depth=1
	addb	$-119, %al
	cmpb	$19, %al
	jbe	.LBB3_13
# BB#15:                                #   in Loop: Header=BB3_4 Depth=1
	movl	$50, %edi
	movl	$2, %esi
	movl	$.L.str.3, %edx
	movl	$2, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB3_4
	jmp	.LBB3_14
.LBB3_9:                                #   in Loop: Header=BB3_4 Depth=1
	cmpb	$0, 41(%rdi)
	je	.LBB3_13
# BB#10:                                #   in Loop: Header=BB3_4 Depth=1
	movq	out_fp(%rip), %rdi
	movl	$.L.str.2, %esi
	callq	PDFPage_Write
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB3_4
	jmp	.LBB3_14
	.p2align	4, 0x90
.LBB3_13:                               # %.backedge
                                        #   in Loop: Header=BB3_4 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB3_4
	jmp	.LBB3_14
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_4 Depth=1
	callq	PDF_PrintGraphicObject
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	je	.LBB3_14
.LBB3_4:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_5 Depth 2
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB3_5:                                #   Parent Loop BB3_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %eax
	cmpq	$26, %rax
	ja	.LBB3_12
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=2
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_16:
	movl	$50, %edi
	movl	$3, %esi
	movl	$.L.str.3, %edx
	movl	$2, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	movq	%r14, %r8
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	Error                   # TAILCALL
.LBB3_14:                               # %.loopexit24
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	PDF_PrintGraphicObject, .Lfunc_end3-PDF_PrintGraphicObject
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_5
	.quad	.LBB3_7
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_11
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_12
	.quad	.LBB3_13

	.text
	.globl	PDF_DefineGraphicNames
	.p2align	4, 0x90
	.type	PDF_DefineGraphicNames,@function
PDF_DefineGraphicNames:                 # @PDF_DefineGraphicNames
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 48
	subq	$272, %rsp              # imm = 0x110
.Lcfi15:
	.cfi_def_cfa_offset 320
.Lcfi16:
	.cfi_offset %rbx, -48
.Lcfi17:
	.cfi_offset %r12, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	cmpb	$97, 32(%r12)
	je	.LBB4_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.5, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_2:
	movl	76(%r12), %edi
	movl	%edi, %eax
	andl	$4095, %eax             # imm = 0xFFF
	cmpl	currentfont(%rip), %eax
	je	.LBB4_5
# BB#3:
	movl	%eax, currentfont(%rip)
	testl	%eax, %eax
	je	.LBB4_5
# BB#4:
	movl	%eax, %edi
	callq	FontHalfXHeight
	movw	%ax, currentxheight2(%rip)
	movq	out_fp(%rip), %rbx
	movl	currentfont(%rip), %edi
	movq	%r12, %rsi
	callq	FontSize
	movl	%eax, %ebp
	movl	currentfont(%rip), %edi
	callq	FontName
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%rax, %rdx
	callq	PDFFont_Set
	movl	76(%r12), %edi
.LBB4_5:
	shrl	$12, %edi
	andl	$1023, %edi             # imm = 0x3FF
	cmpl	currentcolour(%rip), %edi
	je	.LBB4_8
# BB#6:
	movl	%edi, currentcolour(%rip)
	testl	%edi, %edi
	je	.LBB4_8
# BB#7:
	callq	ColourCommand
	movq	%rax, %rcx
	leaq	16(%rsp), %rbx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	sprintf
	movq	out_fp(%rip), %rdi
	movq	%rbx, %rsi
	callq	PDFPage_Write
.LBB4_8:
	movl	48(%r12), %r15d
	movl	60(%r12), %r14d
	movl	56(%r12), %ebp
	addl	%r15d, %ebp
	movl	52(%r12), %ebx
	addl	%r14d, %ebx
	movl	currentfont(%rip), %edi
	testl	%edi, %edi
	je	.LBB4_9
# BB#10:
	movq	%r12, %rsi
	callq	FontSize
	movl	%eax, %r8d
	jmp	.LBB4_11
.LBB4_9:
	movl	$240, %r8d
.LBB4_11:
	movswl	66(%r12), %r9d
	movswl	70(%r12), %eax
	movl	%eax, (%rsp)
	movl	%ebp, %edi
	movl	%ebx, %esi
	movl	%r15d, %edx
	movl	%r14d, %ecx
	callq	PDFPage_SetVars
	addq	$272, %rsp              # imm = 0x110
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	PDF_DefineGraphicNames, .Lfunc_end4-PDF_DefineGraphicNames
	.cfi_endproc

	.globl	PDF_SaveTranslateDefineSave
	.p2align	4, 0x90
	.type	PDF_SaveTranslateDefineSave,@function
PDF_SaveTranslateDefineSave:            # @PDF_SaveTranslateDefineSave
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	out_fp(%rip), %rdi
	callq	PDFPage_Push
	movl	gs_stack_top(%rip), %ecx
	leal	1(%rcx), %eax
	movl	%eax, gs_stack_top(%rip)
	cmpl	$49, %ecx
	jl	.LBB5_2
# BB#1:
	leaq	32(%rbx), %r8
	movl	$50, %edi
	movl	$1, %esi
	movl	$.L.str, %edx
	movl	$1, %ecx
	movl	$50, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	gs_stack_top(%rip), %eax
.LBB5_2:                                # %PDF_SaveGraphicState.exit
	movl	currentfont(%rip), %ecx
	cltq
	shlq	$2, %rax
	movl	%ecx, gs_stack(%rax,%rax,4)
	movl	currentcolour(%rip), %ecx
	movl	%ecx, gs_stack+4(%rax,%rax,4)
	movl	cpexists(%rip), %ecx
	movl	%ecx, gs_stack+8(%rax,%rax,4)
	movl	currenty(%rip), %ecx
	movl	%ecx, gs_stack+12(%rax,%rax,4)
	movzwl	currentxheight2(%rip), %ecx
	movw	%cx, gs_stack+16(%rax,%rax,4)
	movl	%r14d, %eax
	orl	%ebp, %eax
	je	.LBB5_4
# BB#3:
	movq	out_fp(%rip), %rdi
	cvtsi2ssl	%ebp, %xmm0
	cvtsi2ssl	%r14d, %xmm1
	callq	PDFPage_Translate
.LBB5_4:                                # %PDF_CoordTranslate.exit
	movl	$0, cpexists(%rip)
	movq	%rbx, %rdi
	callq	PDF_DefineGraphicNames
	movq	out_fp(%rip), %rdi
	callq	PDFPage_Push
	movl	gs_stack_top(%rip), %ecx
	leal	1(%rcx), %eax
	movl	%eax, gs_stack_top(%rip)
	cmpl	$49, %ecx
	jl	.LBB5_6
# BB#5:
	addq	$32, %rbx
	movl	$50, %edi
	movl	$1, %esi
	movl	$.L.str, %edx
	movl	$1, %ecx
	movl	$50, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movl	gs_stack_top(%rip), %eax
.LBB5_6:                                # %PDF_SaveGraphicState.exit5
	movl	currentfont(%rip), %ecx
	cltq
	shlq	$2, %rax
	movl	%ecx, gs_stack(%rax,%rax,4)
	movl	currentcolour(%rip), %ecx
	movl	%ecx, gs_stack+4(%rax,%rax,4)
	movl	cpexists(%rip), %ecx
	movl	%ecx, gs_stack+8(%rax,%rax,4)
	movl	currenty(%rip), %ecx
	movl	%ecx, gs_stack+12(%rax,%rax,4)
	movzwl	currentxheight2(%rip), %ecx
	movw	%cx, gs_stack+16(%rax,%rax,4)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	PDF_SaveTranslateDefineSave, .Lfunc_end5-PDF_SaveTranslateDefineSave
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDF_CoordTranslate,@function
PDF_CoordTranslate:                     # @PDF_CoordTranslate
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	orl	%edi, %eax
	je	.LBB6_2
# BB#1:
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 16
	movq	out_fp(%rip), %rax
	cvtsi2ssl	%edi, %xmm0
	cvtsi2ssl	%esi, %xmm1
	movq	%rax, %rdi
	callq	PDFPage_Translate
	addq	$8, %rsp
.LBB6_2:
	movl	$0, cpexists(%rip)
	retq
.Lfunc_end6:
	.size	PDF_CoordTranslate, .Lfunc_end6-PDF_CoordTranslate
	.cfi_endproc

	.globl	PDF_PrintGraphicInclude
	.p2align	4, 0x90
	.type	PDF_PrintGraphicInclude,@function
PDF_PrintGraphicInclude:                # @PDF_PrintGraphicInclude
	.cfi_startproc
# BB#0:
	movq	%rdi, %r8
	movq	8(%r8), %r9
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r9), %r9
	cmpb	$0, 32(%r9)
	je	.LBB7_1
# BB#2:
	addq	$32, %r8
	addq	$64, %r9
	movl	$50, %edi
	movl	$4, %esi
	movl	$.L.str.8, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	jmp	Error                   # TAILCALL
.Lfunc_end7:
	.size	PDF_PrintGraphicInclude, .Lfunc_end7-PDF_PrintGraphicInclude
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDF_PrintInitialize,@function
PDF_PrintInitialize:                    # @PDF_PrintInitialize
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 16
	movq	%rdi, out_fp(%rip)
	movb	$0, prologue_done(%rip)
	movl	$-1, gs_stack_top(%rip)
	movl	$0, currentfont(%rip)
	movl	$0, currentcolour(%rip)
	movl	$0, cpexists(%rip)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB8_1
# BB#2:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB8_3
.LBB8_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB8_3:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, needs(%rip)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB8_4
# BB#5:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB8_6
.LBB8_4:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB8_6:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, supplied(%rip)
	popq	%rax
	retq
.Lfunc_end8:
	.size	PDF_PrintInitialize, .Lfunc_end8-PDF_PrintInitialize
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	1141751808              # float 567
	.text
	.p2align	4, 0x90
	.type	PDF_PrintLength,@function
PDF_PrintLength:                        # @PDF_PrintLength
	.cfi_startproc
# BB#0:
	cvtsi2ssl	%esi, %xmm0
	divss	.LCPI9_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.11, %esi
	movb	$1, %al
	jmp	sprintf                 # TAILCALL
.Lfunc_end9:
	.size	PDF_PrintLength, .Lfunc_end9-PDF_PrintLength
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDF_PrintPageSetupForFont,@function
PDF_PrintPageSetupForFont:              # @PDF_PrintPageSetupForFont
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 48
.Lcfi34:
	.cfi_offset %rbx, -40
.Lcfi35:
	.cfi_offset %r12, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movq	out_fp(%rip), %rdi
	xorl	%r12d, %r12d
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
	movb	60(%rbx), %al
	testb	%al, %al
	jns	.LBB10_2
# BB#1:
	andb	$127, %al
	movzbl	%al, %ebx
	movl	$1, %esi
	movl	%ebx, %edi
	callq	MapEnsurePrinted
	movl	%ebx, %edi
	callq	MapEncodingName
	movq	%rax, %r12
.LBB10_2:
	movq	out_fp(%rip), %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	PDFFont_AddFont         # TAILCALL
.Lfunc_end10:
	.size	PDF_PrintPageSetupForFont, .Lfunc_end10-PDF_PrintPageSetupForFont
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDF_PrintPageResourceForFont,@function
PDF_PrintPageResourceForFont:           # @PDF_PrintPageResourceForFont
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	PDF_PrintPageResourceForFont, .Lfunc_end11-PDF_PrintPageResourceForFont
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDF_PrintMapping,@function
PDF_PrintMapping:                       # @PDF_PrintMapping
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 32
.Lcfi41:
	.cfi_offset %rbx, -32
.Lcfi42:
	.cfi_offset %r14, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movl	%edi, %eax
	movq	MapTable(,%rax,8), %rbx
	movq	out_fp(%rip), %rdi
	movq	24(%rbx), %rsi
	addq	$64, %rsi
	callq	PDFFile_BeginFontEncoding
	movq	out_fp(%rip), %rdi
	movl	$32, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_1:                               # =>This Inner Loop Header: Depth=1
	movq	32(%rbx,%rbp,8), %rdx
	addq	$64, %rdx
	incq	%rbp
	testb	$7, %bpl
	movl	$10, %ecx
	cmovnel	%r14d, %ecx
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %rdi
	cmpq	$256, %rbp              # imm = 0x100
	jne	.LBB12_1
# BB#2:
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	PDFFile_EndFontEncoding # TAILCALL
.Lfunc_end12:
	.size	PDF_PrintMapping, .Lfunc_end12-PDF_PrintMapping
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI13_0:
	.long	1028443341              # float 0.0500000007
	.text
	.p2align	4, 0x90
	.type	PDF_PrintBeforeFirstPage,@function
PDF_PrintBeforeFirstPage:               # @PDF_PrintBeforeFirstPage
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 16
	movq	out_fp(%rip), %r10
	movslq	%edi, %rax
	imulq	$1717986919, %rax, %rax # imm = 0x66666667
	movq	%rax, %rcx
	shrq	$63, %rcx
	sarq	$35, %rax
	addl	%ecx, %eax
	movslq	%esi, %rcx
	imulq	$1717986919, %rcx, %rdx # imm = 0x66666667
	movq	%rdx, %rcx
	shrq	$63, %rcx
	sarq	$35, %rdx
	addl	%ecx, %edx
	movl	$120, (%rsp)
	movl	$1440, %ecx             # imm = 0x5A0
	movl	$567, %r8d              # imm = 0x237
	movl	$20, %r9d
	movq	%r10, %rdi
	movl	%eax, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	PDFFile_Init
	movq	out_fp(%rip), %rdi
	callq	FontPrintPageSetup
	movq	out_fp(%rip), %rdi
	movss	.LCPI13_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movl	$10, %esi
	callq	PDFPage_Init
	movq	out_fp(%rip), %rdi
	callq	FontPrintPageResources
	callq	FontAdvanceCurrentPage
	movb	$1, prologue_done(%rip)
	popq	%rax
	retq
.Lfunc_end13:
	.size	PDF_PrintBeforeFirstPage, .Lfunc_end13-PDF_PrintBeforeFirstPage
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_0:
	.long	1028443341              # float 0.0500000007
	.text
	.p2align	4, 0x90
	.type	PDF_PrintBetweenPages,@function
PDF_PrintBetweenPages:                  # @PDF_PrintBetweenPages
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 16
	movq	out_fp(%rip), %rdi
	callq	PDFPage_Cleanup
	movq	out_fp(%rip), %rdi
	movss	.LCPI14_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movl	$10, %esi
	callq	PDFPage_Init
	movq	out_fp(%rip), %rdi
	callq	FontPrintPageResources
	movq	out_fp(%rip), %rdi
	callq	FontPrintPageSetup
	popq	%rax
	jmp	FontAdvanceCurrentPage  # TAILCALL
.Lfunc_end14:
	.size	PDF_PrintBetweenPages, .Lfunc_end14-PDF_PrintBetweenPages
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDF_PrintWord,@function
PDF_PrintWord:                          # @PDF_PrintWord
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi52:
	.cfi_def_cfa_offset 320
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	incl	TotalWordCount(%rip)
	movl	%edx, %r15d
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	40(%r13), %edi
	movl	%edi, %eax
	andl	$4095, %eax             # imm = 0xFFF
	cmpl	currentfont(%rip), %eax
	je	.LBB15_2
# BB#1:
	movl	%eax, currentfont(%rip)
	movl	%eax, %edi
	callq	FontHalfXHeight
	movw	%ax, currentxheight2(%rip)
	movq	out_fp(%rip), %r12
	movl	currentfont(%rip), %edi
	movq	%r13, %rsi
	callq	FontSize
	movl	%eax, %ebp
	movl	currentfont(%rip), %edi
	callq	FontName
	movq	%r12, %rdi
	movl	%ebp, %esi
	movq	%rax, %rdx
	callq	PDFFont_Set
	movl	40(%r13), %edi
.LBB15_2:
	shrl	$12, %edi
	andl	$1023, %edi             # imm = 0x3FF
	cmpl	currentcolour(%rip), %edi
	je	.LBB15_5
# BB#3:
	movl	%edi, currentcolour(%rip)
	testl	%edi, %edi
	je	.LBB15_5
# BB#4:
	callq	ColourCommand
	movq	%rax, %rcx
	movq	%rsp, %rbp
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	sprintf
	movq	out_fp(%rip), %rdi
	movq	%rbp, %rsi
	callq	PDFPage_Write
.LBB15_5:
	movswl	currentxheight2(%rip), %eax
	subl	%eax, %r15d
	cmpl	$0, cpexists(%rip)
	je	.LBB15_9
# BB#6:
	cmpl	%r15d, currenty(%rip)
	jne	.LBB15_9
# BB#7:
	callq	PDFHasValidTextMatrix
	testl	%eax, %eax
	je	.LBB15_9
# BB#8:
	movl	$.L.str.14, %r8d
	jmp	.LBB15_10
.LBB15_9:
	movl	%r15d, currenty(%rip)
	movl	$1, cpexists(%rip)
	movl	$.L.str.15, %r8d
.LBB15_10:
	movq	finfo(%rip), %rax
	movl	40(%r13), %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	8(%rax,%rcx), %r9
	leaq	64(%r13), %r12
	movb	64(%r13), %al
	movq	%r12, %r11
	movq	%r12, %rsi
	jmp	.LBB15_11
.LBB15_43:                              #   in Loop: Header=BB15_11 Depth=1
	movb	%al, (%r11)
	jmp	.LBB15_26
	.p2align	4, 0x90
.LBB15_11:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_14 Depth 2
                                        #       Child Loop BB15_17 Depth 3
                                        #       Child Loop BB15_23 Depth 3
	leaq	1(%rsi), %rbx
	movb	%al, (%r11)
	movzbl	%al, %eax
	cmpb	$2, (%r9,%rax)
	jb	.LBB15_25
# BB#12:                                #   in Loop: Header=BB15_11 Depth=1
	movzbl	(%rsi), %r10d
	movzbl	(%r9,%r10), %eax
	cmpb	%r10b, 256(%r9,%rax)
	jne	.LBB15_25
# BB#13:                                # %.preheader120.lr.ph
                                        #   in Loop: Header=BB15_11 Depth=1
	leaq	256(%r9,%rax), %rbp
	movb	(%rbx), %cl
	.p2align	4, 0x90
.LBB15_14:                              # %.preheader120
                                        #   Parent Loop BB15_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_17 Depth 3
                                        #       Child Loop BB15_23 Depth 3
	leaq	1(%rbp), %rdi
	movb	1(%rbp), %al
	cmpb	%cl, %al
	jne	.LBB15_15
# BB#16:                                # %.lr.ph130.preheader
                                        #   in Loop: Header=BB15_14 Depth=2
	movl	%ecx, %eax
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB15_17:                              # %.lr.ph130
                                        #   Parent Loop BB15_11 Depth=1
                                        #     Parent Loop BB15_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	%al, %al
	je	.LBB15_20
# BB#18:                                # %.lr.ph130
                                        #   in Loop: Header=BB15_17 Depth=3
	movzbl	1(%rdi), %edx
	testb	%dl, %dl
	je	.LBB15_20
# BB#19:                                #   in Loop: Header=BB15_17 Depth=3
	movzbl	1(%rdi), %eax
	incq	%rdi
	cmpb	1(%rsi), %al
	leaq	1(%rsi), %rsi
	je	.LBB15_17
.LBB15_20:                              # %.lr.ph130..critedge.loopexit_crit_edge
                                        #   in Loop: Header=BB15_14 Depth=2
	leaq	-1(%rdi), %rbp
	cmpb	$0, 2(%rbp)
	jne	.LBB15_22
	jmp	.LBB15_43
	.p2align	4, 0x90
.LBB15_15:                              #   in Loop: Header=BB15_14 Depth=2
	movq	%rbx, %rsi
	cmpb	$0, 2(%rbp)
	je	.LBB15_43
.LBB15_22:                              # %.preheader.preheader
                                        #   in Loop: Header=BB15_14 Depth=2
	incq	%rdi
	movq	%rdi, %rbp
	.p2align	4, 0x90
.LBB15_23:                              # %.preheader
                                        #   Parent Loop BB15_11 Depth=1
                                        #     Parent Loop BB15_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$0, (%rbp)
	leaq	1(%rbp), %rbp
	jne	.LBB15_23
# BB#24:                                #   in Loop: Header=BB15_14 Depth=2
	cmpb	%r10b, (%rbp)
	je	.LBB15_14
	.p2align	4, 0x90
.LBB15_25:                              #   in Loop: Header=BB15_11 Depth=1
	movq	%rbx, %rsi
.LBB15_26:                              # %.loopexit
                                        #   in Loop: Header=BB15_11 Depth=1
	incq	%r11
	movb	(%rsi), %al
	testb	%al, %al
	jne	.LBB15_11
# BB#27:
	movb	$0, (%r11)
	movsbl	(%r8), %eax
	cmpl	$115, %eax
	je	.LBB15_30
# BB#28:
	cmpl	$109, %eax
	jne	.LBB15_32
# BB#29:
	movq	out_fp(%rip), %rdi
	movl	%r14d, %esi
	movl	%r15d, %edx
	callq	PDFText_OpenXY
	jmp	.LBB15_31
.LBB15_30:
	movq	out_fp(%rip), %rdi
	movl	%r14d, %esi
	subl	PDF_PrintWord.last_hpos(%rip), %esi
	callq	PDFText_OpenX
.LBB15_31:                              # %.sink.split
	movl	%r14d, PDF_PrintWord.last_hpos(%rip)
.LBB15_32:
	movq	out_fp(%rip), %rdi
	movzbl	64(%r13), %eax
	movq	EightBitToPrintForm(,%rax,8), %rsi
	callq	PDFPage_Write
	movb	65(%r13), %al
	testb	%al, %al
	je	.LBB15_42
# BB#33:                                # %.lr.ph.preheader
	movq	finfo(%rip), %rcx
	movl	40(%r13), %edx
	movl	%edx, %esi
	andl	$4095, %esi             # imm = 0xFFF
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$5, %rsi
	movq	40(%rcx,%rsi), %rsi
	movzbl	60(%rsi), %esi
	andl	$127, %esi
	movq	MapTable(,%rsi,8), %rbp
	movq	%r12, %rsi
	incq	%rsi
	jmp	.LBB15_34
	.p2align	4, 0x90
.LBB15_41:                              # %.thread..lr.ph_crit_edge
                                        #   in Loop: Header=BB15_34 Depth=1
	leaq	1(%r12), %rsi
	movq	finfo(%rip), %rcx
	movl	40(%r13), %edx
.LBB15_34:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_36 Depth 2
	movzbl	(%r12), %edi
	movq	%rsi, %r12
	andl	$4095, %edx             # imm = 0xFFF
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$5, %rdx
	movq	48(%rcx,%rdx), %rdx
	movzwl	40(%rdx), %edx
	andl	$4095, %edx             # imm = 0xFFF
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$5, %rdx
	movq	64(%rcx,%rdx), %rsi
	movzbl	2945(%rbp,%rdi), %edi
	movzwl	(%rsi,%rdi,2), %esi
	testq	%rsi, %rsi
	je	.LBB15_40
# BB#35:                                #   in Loop: Header=BB15_34 Depth=1
	movzbl	%al, %edi
	movb	2945(%rbp,%rdi), %bl
	movq	72(%rcx,%rdx), %rdi
	.p2align	4, 0x90
.LBB15_36:                              #   Parent Loop BB15_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	%bl, (%rdi,%rsi)
	leaq	1(%rsi), %rsi
	ja	.LBB15_36
# BB#37:                                #   in Loop: Header=BB15_34 Depth=1
	jne	.LBB15_40
# BB#38:                                #   in Loop: Header=BB15_34 Depth=1
	movq	80(%rcx,%rdx), %rdi
	movq	88(%rcx,%rdx), %rcx
	movzbl	-1(%rdi,%rsi), %edx
	movswl	(%rcx,%rdx,2), %esi
	testl	%esi, %esi
	je	.LBB15_40
# BB#39:                                #   in Loop: Header=BB15_34 Depth=1
	movq	out_fp(%rip), %rdi
	callq	PDFText_Kern
	movb	(%r12), %al
	.p2align	4, 0x90
.LBB15_40:                              # %.thread
                                        #   in Loop: Header=BB15_34 Depth=1
	movq	out_fp(%rip), %rdi
	movzbl	%al, %eax
	movq	EightBitToPrintForm(,%rax,8), %rsi
	callq	PDFPage_Write
	movb	1(%r12), %al
	testb	%al, %al
	jne	.LBB15_41
.LBB15_42:                              # %._crit_edge
	movq	out_fp(%rip), %rdi
	callq	PDFText_Close
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	PDF_PrintWord, .Lfunc_end15-PDF_PrintWord
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDF_PrintPlainGraphic,@function
PDF_PrintPlainGraphic:                  # @PDF_PrintPlainGraphic
	.cfi_startproc
# BB#0:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.5, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.16, %r9d
	xorl	%eax, %eax
	jmp	Error                   # TAILCALL
.Lfunc_end16:
	.size	PDF_PrintPlainGraphic, .Lfunc_end16-PDF_PrintPlainGraphic
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDF_PrintUnderline,@function
PDF_PrintUnderline:                     # @PDF_PrintUnderline
	.cfi_startproc
# BB#0:
	movq	out_fp(%rip), %r9
	movq	finfo(%rip), %rax
	movl	%edi, %edi
	leaq	(%rdi,%rdi,2), %rdi
	shlq	$5, %rdi
	movswl	56(%rax,%rdi), %esi
	subl	%esi, %r8d
	movswl	58(%rax,%rdi), %eax
	movq	%r9, %rdi
	movl	%edx, %esi
	movl	%ecx, %edx
	movl	%r8d, %ecx
	movl	%eax, %r8d
	jmp	PDFPage_PrintUnderline  # TAILCALL
.Lfunc_end17:
	.size	PDF_PrintUnderline, .Lfunc_end17-PDF_PrintUnderline
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI18_0:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI18_1:
	.quad	4640537203540230144     # double 180
	.text
	.p2align	4, 0x90
	.type	PDF_CoordRotate,@function
PDF_CoordRotate:                        # @PDF_CoordRotate
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	sarl	$31, %eax
	shrl	$25, %eax
	addl	%edi, %eax
	sarl	$7, %eax
	cltq
	imulq	$-1240768329, %rax, %rcx # imm = 0xB60B60B7
	shrq	$32, %rcx
	addl	%eax, %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	sarl	$8, %ecx
	addl	%edx, %ecx
	imull	$360, %ecx, %ecx        # imm = 0x168
	subl	%ecx, %eax
	je	.LBB18_2
# BB#1:
	pushq	%rax
.Lcfi59:
	.cfi_def_cfa_offset 16
	movq	out_fp(%rip), %rdi
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI18_0(%rip), %xmm0
	divsd	.LCPI18_1(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	callq	PDFPage_Rotate
	addq	$8, %rsp
.LBB18_2:
	movl	$0, cpexists(%rip)
	retq
.Lfunc_end18:
	.size	PDF_CoordRotate, .Lfunc_end18-PDF_CoordRotate
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI19_0:
	.quad	-4616189618054758400    # double -1
.LCPI19_2:
	.quad	4576918229304087675     # double 0.01
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI19_1:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.p2align	4, 0x90
	.type	PDF_CoordScale,@function
PDF_CoordScale:                         # @PDF_CoordScale
	.cfi_startproc
# BB#0:
	cvtss2sd	%xmm0, %xmm2
	addsd	.LCPI19_0(%rip), %xmm2
	andpd	.LCPI19_1(%rip), %xmm2
	ucomisd	.LCPI19_2(%rip), %xmm2
	ja	.LBB19_2
# BB#1:
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm1, %xmm2
	addsd	.LCPI19_0(%rip), %xmm2
	andpd	.LCPI19_1(%rip), %xmm2
	ucomisd	.LCPI19_2(%rip), %xmm2
	jbe	.LBB19_3
.LBB19_2:
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 16
	movq	out_fp(%rip), %rdi
	callq	PDFPage_Scale
	addq	$8, %rsp
.LBB19_3:
	movl	$0, cpexists(%rip)
	retq
.Lfunc_end19:
	.size	PDF_CoordScale, .Lfunc_end19-PDF_CoordScale
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDF_LinkSource,@function
PDF_LinkSource:                         # @PDF_LinkSource
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end20:
	.size	PDF_LinkSource, .Lfunc_end20-PDF_LinkSource
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDF_LinkDest,@function
PDF_LinkDest:                           # @PDF_LinkDest
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end21:
	.size	PDF_LinkDest, .Lfunc_end21-PDF_LinkDest
	.cfi_endproc

	.p2align	4, 0x90
	.type	PDF_LinkCheck,@function
PDF_LinkCheck:                          # @PDF_LinkCheck
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end22:
	.size	PDF_LinkCheck, .Lfunc_end22-PDF_LinkCheck
	.cfi_endproc

	.type	prologue_done,@object   # @prologue_done
	.local	prologue_done
	.comm	prologue_done,1,4
	.type	out_fp,@object          # @out_fp
	.local	out_fp
	.comm	out_fp,8,8
	.type	gs_stack_top,@object    # @gs_stack_top
	.local	gs_stack_top
	.comm	gs_stack_top,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"rotations, graphics etc. too deeply nested (max is %d)"
	.size	.L.str, 55

	.type	currentfont,@object     # @currentfont
	.local	currentfont
	.comm	currentfont,4,4
	.type	gs_stack,@object        # @gs_stack
	.local	gs_stack
	.comm	gs_stack,1000,16
	.type	currentcolour,@object   # @currentcolour
	.local	currentcolour
	.comm	currentcolour,4,4
	.type	cpexists,@object        # @cpexists
	.local	cpexists
	.comm	cpexists,4,4
	.type	currenty,@object        # @currenty
	.local	currenty
	.comm	currenty,4,4
	.type	currentxheight2,@object # @currentxheight2
	.local	currentxheight2
	.comm	currentxheight2,2,2
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n"
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" "
	.size	.L.str.2, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"error in left parameter of %s"
	.size	.L.str.3, 30

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"@Graphic"
	.size	.L.str.4, 9

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"assert failed in %s"
	.size	.L.str.5, 20

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"PrintGraphic: type(x) != GRAPHIC!"
	.size	.L.str.6, 34

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%s "
	.size	.L.str.7, 4

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"cannot include EPS file in PDF output; EPS file %s ignored"
	.size	.L.str.8, 59

	.type	pdf_back,@object        # @pdf_back
	.data
	.p2align	3
pdf_back:
	.long	1                       # 0x1
	.zero	4
	.quad	.L.str.9
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	PDF_PrintInitialize
	.quad	PDF_PrintLength
	.quad	PDF_PrintPageSetupForFont
	.quad	PDF_PrintPageResourceForFont
	.quad	PDF_PrintMapping
	.quad	PDF_PrintBeforeFirstPage
	.quad	PDF_PrintBetweenPages
	.quad	PDF_PrintAfterLastPage
	.quad	PDF_PrintWord
	.quad	PDF_PrintPlainGraphic
	.quad	PDF_PrintUnderline
	.quad	PDF_CoordTranslate
	.quad	PDF_CoordRotate
	.quad	PDF_CoordScale
	.quad	PDF_SaveGraphicState
	.quad	PDF_RestoreGraphicState
	.quad	PDF_PrintGraphicObject
	.quad	PDF_DefineGraphicNames
	.quad	PDF_SaveTranslateDefineSave
	.quad	PDF_PrintGraphicInclude
	.quad	PDF_LinkSource
	.quad	PDF_LinkDest
	.quad	PDF_LinkCheck
	.size	pdf_back, 232

	.type	PDF_BackEnd,@object     # @PDF_BackEnd
	.globl	PDF_BackEnd
	.p2align	3
PDF_BackEnd:
	.quad	pdf_back
	.size	PDF_BackEnd, 8

	.type	.L.str.9,@object        # @.str.9
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.9:
	.asciz	"PDF"
	.size	.L.str.9, 4

	.type	needs,@object           # @needs
	.local	needs
	.comm	needs,8,8
	.type	supplied,@object        # @supplied
	.local	supplied
	.comm	supplied,8,8
	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%.3fc"
	.size	.L.str.11, 6

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"%%%%IncludeResource: font %s\n"
	.size	.L.str.12, 30

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"/%s%c"
	.size	.L.str.13, 6

	.type	PDF_PrintWord.last_hpos,@object # @PDF_PrintWord.last_hpos
	.local	PDF_PrintWord.last_hpos
	.comm	PDF_PrintWord.last_hpos,4,4
	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"s"
	.size	.L.str.14, 2

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"m"
	.size	.L.str.15, 2

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"PDF_PrintPlainGraphic: this routine should never be called!"
	.size	.L.str.16, 60


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
