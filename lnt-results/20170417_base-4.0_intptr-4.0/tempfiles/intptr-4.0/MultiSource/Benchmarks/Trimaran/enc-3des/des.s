	.text
	.file	"des.bc"
	.globl	des_main_ks
	.p2align	4, 0x90
	.type	des_main_ks,@function
des_main_ks:                            # @des_main_ks
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movzbl	(%rsi), %r8d
	movq	%r8, %rax
	shlq	$24, %rax
	movzbl	1(%rsi), %edx
	shlq	$16, %rdx
	orq	%rax, %rdx
	movzbl	2(%rsi), %eax
	shlq	$8, %rax
	orq	%rdx, %rax
	movzbl	3(%rsi), %ebx
	orq	%rax, %rbx
	movzbl	4(%rsi), %eax
	shlq	$24, %rax
	movzbl	5(%rsi), %edx
	shlq	$16, %rdx
	orq	%rax, %rdx
	movzbl	6(%rsi), %eax
	shlq	$8, %rax
	orq	%rdx, %rax
	movzbl	7(%rsi), %ecx
	orq	%rax, %rcx
	movl	%ecx, %esi
	shrl	$4, %esi
	xorl	%ebx, %esi
	andl	$252645135, %esi        # imm = 0xF0F0F0F
	movq	%rbx, %rax
	xorq	%rsi, %rax
	shlq	$4, %rsi
	xorq	%rcx, %rsi
	movl	%esi, %r9d
	xorl	%ebx, %r9d
	andl	$269488144, %r9d        # imm = 0x10101010
	xorq	%rsi, %r9
	movl	%eax, %ecx
	andl	$15, %ecx
	movl	LHs(,%rcx,8), %ecx
	movq	%rax, %rsi
	shrq	$5, %rsi
	andl	$120, %esi
	movl	LHs(%rsi), %ebx
	shll	$3, %ecx
	shll	$2, %ebx
	orl	%ecx, %ebx
	movq	%rax, %rcx
	shrq	$13, %rcx
	andl	$120, %ecx
	movl	LHs(%rcx), %esi
	addl	%esi, %esi
	orl	%ebx, %esi
	movq	%rax, %rcx
	shrq	$21, %rcx
	andl	$120, %ecx
	orl	LHs(%rcx), %esi
	movq	%rax, %rcx
	shrq	$2, %rcx
	andl	$120, %ecx
	movl	LHs(%rcx), %ecx
	shll	$7, %ecx
	movq	%rax, %rdx
	shrq	$10, %rdx
	andl	$120, %edx
	movl	LHs(%rdx), %edx
	shll	$6, %edx
	orl	%ecx, %edx
	shrq	$18, %rax
	andl	$120, %eax
	movl	LHs(%rax), %eax
	shll	$5, %eax
	orl	%edx, %eax
	shrq	$5, %r8
	movl	LHs(,%r8,8), %r13d
	shll	$4, %r13d
	orl	%eax, %r13d
	orl	%esi, %r13d
	movl	%r9d, %eax
	andl	$30, %eax
	movl	RHs(,%rax,4), %eax
	movq	%r9, %rcx
	shrq	$6, %rcx
	andl	$120, %ecx
	movl	RHs(%rcx), %esi
	shll	$3, %eax
	shll	$2, %esi
	orl	%eax, %esi
	movq	%r9, %rax
	shrq	$14, %rax
	andl	$120, %eax
	movl	RHs(%rax), %ecx
	addl	%ecx, %ecx
	orl	%esi, %ecx
	movq	%r9, %rax
	shrq	$22, %rax
	andl	$120, %eax
	orl	RHs(%rax), %ecx
	movq	%r9, %rax
	shrq	%rax
	andl	$120, %eax
	movl	RHs(%rax), %eax
	shll	$7, %eax
	movq	%r9, %rsi
	shrq	$9, %rsi
	andl	$120, %esi
	movl	RHs(%rsi), %esi
	shll	$6, %esi
	orl	%eax, %esi
	movq	%r9, %rax
	shrq	$17, %rax
	andl	$120, %eax
	movl	RHs(%rax), %eax
	shll	$5, %eax
	orl	%esi, %eax
	shrq	$25, %r9
	andl	$120, %r9d
	movl	RHs(%r9), %r10d
	shll	$4, %r10d
	orl	%eax, %r10d
	orl	%ecx, %r10d
	andl	$268435455, %r13d       # imm = 0xFFFFFFF
	andl	$268435455, %r10d       # imm = 0xFFFFFFF
	xorl	%eax, %eax
	jmp	.LBB0_1
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	movl	$2, %eax
	movl	$26, %r8d
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	cmpl	$2, %eax
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	movl	%eax, -12(%rsp)         # 4-byte Spill
	jl	.LBB0_5
# BB#2:                                 # %switch.early.test
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$15, %eax
	je	.LBB0_5
# BB#3:                                 # %switch.early.test
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$8, %eax
	jne	.LBB0_4
	.p2align	4, 0x90
.LBB0_5:                                #   in Loop: Header=BB0_1 Depth=1
	movl	$1, %eax
	movl	$27, %r8d
.LBB0_6:                                #   in Loop: Header=BB0_1 Depth=1
	movq	%r13, %r9
	movl	%eax, %ecx
	shlq	%cl, %r9
	movl	%r8d, %ecx
	shrq	%cl, %r13
	movq	%r10, %rbp
	movl	%eax, %ecx
	shlq	%cl, %rbp
	movl	%r9d, %r11d
	andl	$268435454, %r11d       # imm = 0xFFFFFFE
	orq	%r13, %r11
	andq	$1, %r13
	shlq	$28, %r13
	movq	%r11, %rbx
	movq	%r11, %r14
	movq	%r11, %r15
	movq	%r9, %r12
	shrq	%r12
	andl	$1048576, %r12d         # imm = 0x100000
	movq	%r9, %rcx
	shrq	$10, %rcx
	andl	$65536, %ecx            # imm = 0x10000
	movl	%ebp, %eax
	andl	$256, %eax              # imm = 0x100
	orq	%r12, %rax
	movq	%rbp, %rsi
	shrq	$13, %rsi
	andl	$8192, %esi             # imm = 0x2000
	orq	%rcx, %rax
	movq	%rbp, %rcx
	shrq	$4, %rcx
	andl	$4096, %ecx             # imm = 0x1000
	orq	%rsi, %rax
	movq	%rbp, %rsi
	shrq	%rsi
	andl	$1024, %esi             # imm = 0x400
	orq	%rcx, %rax
	movq	%rbp, %rcx
	shrq	$5, %rcx
	andl	$32, %ecx
	orq	%rsi, %rax
	movq	%rbp, %rsi
	shrq	$10, %rsi
	andl	$16, %esi
	orq	%rcx, %rsi
	movq	%rbp, %rcx
	shrq	$18, %rcx
	andl	$4, %ecx
	orq	%rcx, %rsi
	movq	%rbp, %rcx
	shrq	$26, %rcx
	andl	$2, %ecx
	orq	%rcx, %rsi
	movl	%ebp, %ecx
	shrl	$24, %ecx
	andl	$1, %ecx
	orq	%rcx, %rsi
	movq	%r11, %rdx
	orq	%r13, %rsi
	movq	%r11, %r12
	orq	%rax, %rsi
	movq	%r11, %rax
	shlq	$4, %rax
	andl	$603979776, %eax        # imm = 0x24000000
	shlq	$14, %rbx
	andl	$134217728, %ebx        # imm = 0x8000000
	orq	%rax, %rbx
	leaq	(,%r11,4), %rax
	movl	%r8d, %ecx
	shrq	%cl, %r10
	shlq	$18, %r14
	andl	$34078720, %r14d        # imm = 0x2080000
	orq	%r14, %rbx
	movl	%ebp, %ecx
	andl	$268435454, %ecx        # imm = 0xFFFFFFE
	orq	%r10, %rcx
	shlq	$6, %r15
	andl	$16777216, %r15d        # imm = 0x1000000
	shlq	$9, %rdx
	andl	$2097152, %edx          # imm = 0x200000
	shlq	$10, %r12
	orq	%r15, %rbx
	movl	%r12d, %edi
	andl	$262144, %edi           # imm = 0x40000
	andl	$131072, %eax           # imm = 0x20000
	orq	%rdx, %rbx
	movq	%rcx, %r14
	orq	%rsi, %rbx
	movq	%rcx, %r8
	orq	%rdi, %rax
	movq	%rcx, %rdx
	andq	$32, %rdx
	shlq	$6, %rdx
	shrq	$14, %r14
	orq	%rdx, %rax
	movl	%r14d, %edx
	andl	$512, %edx              # imm = 0x200
	shrq	$3, %r8
	orq	%rdx, %rax
	movl	%r8d, %edx
	andl	$8, %edx
	orq	%rdx, %rax
	orq	%rbx, %rax
	movq	-8(%rsp), %rdi          # 8-byte Reload
	movq	%rax, (%rdi)
	movq	%r9, %rbx
	shrq	$2, %rbx
	andl	$33554432, %ebx         # imm = 0x2000000
	movl	%ebp, %eax
	andl	$512, %eax              # imm = 0x200
	orq	%rbx, %rax
	movq	%r9, %rdx
	shrq	$6, %rdx
	andl	$262144, %edx           # imm = 0x40000
	orq	%rdx, %rax
	shrq	$4, %r9
	andl	$65536, %r9d            # imm = 0x10000
	orq	%r9, %rax
	movq	%rbp, %rdx
	shrq	$2, %rdx
	andl	$8192, %edx             # imm = 0x2000
	orq	%rdx, %rax
	movq	%rbp, %rdx
	shrq	$9, %rdx
	andl	$1024, %edx             # imm = 0x400
	orq	%rdx, %rax
	movq	%rbp, %rdx
	shrq	$7, %rdx
	andl	$32, %edx
	shrq	$21, %rbp
	andl	$2, %ebp
	orq	%rdx, %rbp
	movq	%r11, %rdx
	shlq	$2, %r10
	andl	$4, %r10d
	orq	%r10, %rbp
	movq	%r11, %rsi
	shlq	$15, %rdx
	shlq	$17, %rsi
	andl	$268435456, %esi        # imm = 0x10000000
	orq	%rsi, %rbp
	movl	%edx, %esi
	andl	$536870912, %esi        # imm = 0x20000000
	orq	%rsi, %rbp
	andl	$134217728, %r12d       # imm = 0x8000000
	orq	%r12, %rbp
	movq	%r11, %rsi
	orq	%rax, %rbp
	leaq	(%r11,%r11), %rax
	andq	$16, %rsi
	shlq	$22, %rsi
	andl	$16777216, %eax         # imm = 0x1000000
	orq	%rsi, %rax
	movq	%r11, %rsi
	andq	$32, %rsi
	shlq	$16, %rsi
	orq	%rsi, %rax
	movq	%r11, %rsi
	shlq	$11, %rsi
	andl	$1048576, %esi          # imm = 0x100000
	orq	%rsi, %rax
	leaq	(,%r11,8), %rsi
	andl	$524288, %esi           # imm = 0x80000
	orq	%rsi, %rax
	orq	%rbp, %rax
	movq	%rcx, %rsi
	andl	$131072, %edx           # imm = 0x20000
	andq	$16, %rsi
	shlq	$8, %rsi
	orq	%rdx, %rsi
	andl	$2056, %r14d            # imm = 0x808
	orq	%r14, %rsi
	movq	%rcx, %rdx
	andq	$2, %rdx
	shlq	$7, %rdx
	orq	%rdx, %rsi
	andl	$17, %r8d
	orq	%r8, %rsi
	orq	%rax, %rsi
	movq	%rsi, 8(%rdi)
	movq	%rcx, %r10
	movq	%r11, %r13
	movl	-12(%rsp), %eax         # 4-byte Reload
	incl	%eax
	addq	$16, %rdi
	cmpl	$16, %eax
	jne	.LBB0_1
# BB#7:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	des_main_ks, .Lfunc_end0-des_main_ks
	.cfi_endproc

	.globl	des_set_key
	.p2align	4, 0x90
	.type	des_set_key,@function
des_set_key:                            # @des_set_key
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	des_main_ks
	movups	240(%rbx), %xmm0
	movups	%xmm0, 256(%rbx)
	movups	224(%rbx), %xmm0
	movups	%xmm0, 272(%rbx)
	movups	208(%rbx), %xmm0
	movups	%xmm0, 288(%rbx)
	movups	192(%rbx), %xmm0
	movups	%xmm0, 304(%rbx)
	movups	176(%rbx), %xmm0
	movups	%xmm0, 320(%rbx)
	movups	160(%rbx), %xmm0
	movups	%xmm0, 336(%rbx)
	movups	144(%rbx), %xmm0
	movups	%xmm0, 352(%rbx)
	movups	128(%rbx), %xmm0
	movups	%xmm0, 368(%rbx)
	movups	112(%rbx), %xmm0
	movups	%xmm0, 384(%rbx)
	movups	96(%rbx), %xmm0
	movups	%xmm0, 400(%rbx)
	movups	80(%rbx), %xmm0
	movups	%xmm0, 416(%rbx)
	movups	64(%rbx), %xmm0
	movups	%xmm0, 432(%rbx)
	movups	48(%rbx), %xmm0
	movups	%xmm0, 448(%rbx)
	movups	32(%rbx), %xmm0
	movups	%xmm0, 464(%rbx)
	movups	16(%rbx), %xmm0
	movups	%xmm0, 480(%rbx)
	movups	(%rbx), %xmm0
	movups	%xmm0, 496(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	des_set_key, .Lfunc_end1-des_set_key
	.cfi_endproc

	.globl	des_crypt
	.p2align	4, 0x90
	.type	des_crypt,@function
des_crypt:                              # @des_crypt
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movzbl	(%rsi), %eax
	shlq	$24, %rax
	movzbl	1(%rsi), %ecx
	shlq	$16, %rcx
	orq	%rax, %rcx
	movzbl	2(%rsi), %eax
	shlq	$8, %rax
	orq	%rcx, %rax
	movzbl	3(%rsi), %r8d
	orq	%rax, %r8
	movzbl	4(%rsi), %eax
	shlq	$24, %rax
	movzbl	5(%rsi), %ecx
	shlq	$16, %rcx
	orq	%rax, %rcx
	movzbl	6(%rsi), %eax
	shlq	$8, %rax
	orq	%rcx, %rax
	movzbl	7(%rsi), %ecx
	orq	%rax, %rcx
	movl	%r8d, %esi
	shrl	$4, %esi
	xorl	%ecx, %esi
	andl	$252645135, %esi        # imm = 0xF0F0F0F
	xorq	%rsi, %rcx
	shlq	$4, %rsi
	xorq	%r8, %rsi
	movl	%esi, %eax
	shrl	$16, %eax
	xorl	%ecx, %eax
	movzwl	%ax, %r11d
	xorq	%r11, %rcx
	shlq	$16, %r11
	xorq	%rsi, %r11
	movl	%ecx, %esi
	shrl	$2, %esi
	xorl	%r11d, %esi
	andl	$858993459, %esi        # imm = 0x33333333
	xorq	%rsi, %r11
	shlq	$2, %rsi
	xorq	%rcx, %rsi
	movl	%esi, %ecx
	shrl	$8, %ecx
	xorl	%r11d, %ecx
	andl	$16711935, %ecx         # imm = 0xFF00FF
	xorq	%rcx, %r11
	shlq	$8, %rcx
	xorq	%rsi, %rcx
	leaq	(%rcx,%rcx), %rsi
	shrq	$31, %rcx
	orq	%rcx, %rsi
	movl	%esi, %r9d
	movl	%r11d, %ecx
	xorl	%esi, %ecx
	andl	$-1431655766, %ecx      # imm = 0xAAAAAAAA
	xorq	%rcx, %r9
	xorq	%rcx, %r11
	movq	%r11, %rcx
	shrq	$31, %rcx
	addl	%r11d, %r11d
	orl	%ecx, %r11d
	movq	(%rdi), %rsi
	xorq	%r9, %rsi
	movl	%esi, %r10d
	andl	$63, %r10d
	movq	%rsi, %rcx
	shrq	$5, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	movq	SB6(%rcx), %r8
	xorq	SB8(,%r10,8), %r8
	movq	%rsi, %rcx
	shrq	$13, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB4(%rcx), %r8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB2(%rsi), %r8
	movq	%r9, %rsi
	shlq	$28, %rsi
	movq	%r9, %rcx
	shrq	$4, %rcx
	orq	%rsi, %rcx
	xorq	8(%rdi), %rcx
	movl	%ecx, %r10d
	andl	$63, %r10d
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB7(,%r10,8), %r8
	xorq	SB5(%rsi), %r8
	xorq	SB3(%rax), %r8
	xorq	SB1(%rcx), %r8
	xorq	%r11, %r8
	movq	16(%rdi), %rax
	xorq	%r8, %rax
	movl	%eax, %esi
	andl	$63, %esi
	movq	%rax, %rcx
	shrq	$5, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	movq	SB6(%rcx), %rcx
	xorq	SB8(,%rsi,8), %rcx
	movq	%rax, %rsi
	shrq	$13, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB4(%rsi), %rcx
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB2(%rax), %rcx
	movq	%r8, %rsi
	shlq	$28, %rsi
	movq	%r8, %rax
	shrq	$4, %rax
	orq	%rsi, %rax
	xorq	24(%rdi), %rax
	movl	%eax, %r10d
	andl	$63, %r10d
	movq	%rax, %r11
	shrq	$5, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%rax, %rsi
	shrq	$13, %rsi
	andl	$504, %esi              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB7(,%r10,8), %rcx
	xorq	SB5(%r11), %rcx
	xorq	SB3(%rsi), %rcx
	xorq	SB1(%rax), %rcx
	xorq	%r9, %rcx
	movq	32(%rdi), %rsi
	xorq	%rcx, %rsi
	movl	%esi, %r9d
	andl	$63, %r9d
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	movq	SB6(%rax), %r11
	movq	%rsi, %r10
	shrq	$13, %r10
	andl	$504, %r10d             # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%rcx, %rbx
	shlq	$28, %rbx
	movq	%rcx, %rax
	shrq	$4, %rax
	orq	%rbx, %rax
	xorq	40(%rdi), %rax
	movl	%eax, %r14d
	andl	$63, %r14d
	movq	%rax, %r15
	shrq	$5, %r15
	andl	$504, %r15d             # imm = 0x1F8
	movq	%rax, %rbx
	shrq	$13, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB8(,%r9,8), %r11
	xorq	SB4(%r10), %r11
	xorq	SB2(%rsi), %r11
	xorq	SB7(,%r14,8), %r11
	xorq	SB5(%r15), %r11
	xorq	SB3(%rbx), %r11
	xorq	%r8, %r11
	xorq	SB1(%rax), %r11
	movq	48(%rdi), %rsi
	xorq	%r11, %rsi
	movl	%esi, %r8d
	andl	$63, %r8d
	movq	%rsi, %r9
	shrq	$5, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movq	%rsi, %r10
	shrq	$13, %r10
	andl	$504, %r10d             # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%r11, %rbx
	shlq	$28, %rbx
	movq	%r11, %rax
	shrq	$4, %rax
	orq	%rbx, %rax
	xorq	56(%rdi), %rax
	movl	%eax, %r14d
	andl	$63, %r14d
	movq	%rax, %r15
	shrq	$5, %r15
	andl	$504, %r15d             # imm = 0x1F8
	movq	%rax, %rbx
	shrq	$13, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB8(,%r8,8), %rcx
	xorq	SB6(%r9), %rcx
	xorq	SB4(%r10), %rcx
	xorq	SB2(%rsi), %rcx
	xorq	SB7(,%r14,8), %rcx
	xorq	SB5(%r15), %rcx
	xorq	SB3(%rbx), %rcx
	xorq	SB1(%rax), %rcx
	movq	64(%rdi), %rsi
	xorq	%rcx, %rsi
	movl	%esi, %r8d
	andl	$63, %r8d
	movq	%rsi, %r9
	shrq	$5, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movq	%rsi, %r10
	shrq	$13, %r10
	andl	$504, %r10d             # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%rcx, %rbx
	shlq	$28, %rbx
	movq	%rcx, %rax
	shrq	$4, %rax
	orq	%rbx, %rax
	xorq	72(%rdi), %rax
	movl	%eax, %r14d
	andl	$63, %r14d
	movq	%rax, %r15
	shrq	$5, %r15
	andl	$504, %r15d             # imm = 0x1F8
	movq	%rax, %rbx
	shrq	$13, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB8(,%r8,8), %r11
	xorq	SB6(%r9), %r11
	xorq	SB4(%r10), %r11
	xorq	SB2(%rsi), %r11
	xorq	SB7(,%r14,8), %r11
	xorq	SB5(%r15), %r11
	xorq	SB3(%rbx), %r11
	xorq	SB1(%rax), %r11
	movq	80(%rdi), %rsi
	xorq	%r11, %rsi
	movl	%esi, %r8d
	andl	$63, %r8d
	movq	%rsi, %r9
	shrq	$5, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movq	%rsi, %r10
	shrq	$13, %r10
	andl	$504, %r10d             # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%r11, %rbx
	shlq	$28, %rbx
	movq	%r11, %rax
	shrq	$4, %rax
	orq	%rbx, %rax
	xorq	88(%rdi), %rax
	movl	%eax, %r14d
	andl	$63, %r14d
	movq	%rax, %r15
	shrq	$5, %r15
	andl	$504, %r15d             # imm = 0x1F8
	movq	%rax, %rbx
	shrq	$13, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB8(,%r8,8), %rcx
	xorq	SB6(%r9), %rcx
	xorq	SB4(%r10), %rcx
	xorq	SB2(%rsi), %rcx
	xorq	SB7(,%r14,8), %rcx
	xorq	SB5(%r15), %rcx
	xorq	SB3(%rbx), %rcx
	xorq	SB1(%rax), %rcx
	movq	96(%rdi), %rsi
	xorq	%rcx, %rsi
	movl	%esi, %r8d
	andl	$63, %r8d
	movq	%rsi, %r9
	shrq	$5, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movq	%rsi, %r10
	shrq	$13, %r10
	andl	$504, %r10d             # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%rcx, %rbx
	shlq	$28, %rbx
	movq	%rcx, %rax
	shrq	$4, %rax
	orq	%rbx, %rax
	xorq	104(%rdi), %rax
	movl	%eax, %r14d
	andl	$63, %r14d
	movq	%rax, %r15
	shrq	$5, %r15
	andl	$504, %r15d             # imm = 0x1F8
	movq	%rax, %rbx
	shrq	$13, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB8(,%r8,8), %r11
	xorq	SB6(%r9), %r11
	xorq	SB4(%r10), %r11
	xorq	SB2(%rsi), %r11
	xorq	SB7(,%r14,8), %r11
	xorq	SB5(%r15), %r11
	xorq	SB3(%rbx), %r11
	xorq	SB1(%rax), %r11
	movq	112(%rdi), %rsi
	xorq	%r11, %rsi
	movl	%esi, %r8d
	andl	$63, %r8d
	movq	%rsi, %r9
	shrq	$5, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movq	%rsi, %r10
	shrq	$13, %r10
	andl	$504, %r10d             # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%r11, %rbx
	shlq	$28, %rbx
	movq	%r11, %rax
	shrq	$4, %rax
	orq	%rbx, %rax
	xorq	120(%rdi), %rax
	movl	%eax, %r14d
	andl	$63, %r14d
	movq	%rax, %r15
	shrq	$5, %r15
	andl	$504, %r15d             # imm = 0x1F8
	movq	%rax, %rbx
	shrq	$13, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB8(,%r8,8), %rcx
	xorq	SB6(%r9), %rcx
	xorq	SB4(%r10), %rcx
	xorq	SB2(%rsi), %rcx
	xorq	SB7(,%r14,8), %rcx
	xorq	SB5(%r15), %rcx
	xorq	SB3(%rbx), %rcx
	xorq	SB1(%rax), %rcx
	movq	128(%rdi), %rsi
	xorq	%rcx, %rsi
	movl	%esi, %r8d
	andl	$63, %r8d
	movq	%rsi, %r9
	shrq	$5, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movq	%rsi, %r10
	shrq	$13, %r10
	andl	$504, %r10d             # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%rcx, %rbx
	shlq	$28, %rbx
	movq	%rcx, %rax
	shrq	$4, %rax
	orq	%rbx, %rax
	xorq	136(%rdi), %rax
	movl	%eax, %r14d
	andl	$63, %r14d
	movq	%rax, %r15
	shrq	$5, %r15
	andl	$504, %r15d             # imm = 0x1F8
	movq	%rax, %rbx
	shrq	$13, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB8(,%r8,8), %r11
	xorq	SB6(%r9), %r11
	xorq	SB4(%r10), %r11
	xorq	SB2(%rsi), %r11
	xorq	SB7(,%r14,8), %r11
	xorq	SB5(%r15), %r11
	xorq	SB3(%rbx), %r11
	xorq	SB1(%rax), %r11
	movq	144(%rdi), %rsi
	xorq	%r11, %rsi
	movl	%esi, %r8d
	andl	$63, %r8d
	movq	%rsi, %r9
	shrq	$5, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movq	%rsi, %r10
	shrq	$13, %r10
	andl	$504, %r10d             # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%r11, %rbx
	shlq	$28, %rbx
	movq	%r11, %rax
	shrq	$4, %rax
	orq	%rbx, %rax
	xorq	152(%rdi), %rax
	movl	%eax, %r14d
	andl	$63, %r14d
	movq	%rax, %r15
	shrq	$5, %r15
	andl	$504, %r15d             # imm = 0x1F8
	movq	%rax, %rbx
	shrq	$13, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB8(,%r8,8), %rcx
	xorq	SB6(%r9), %rcx
	xorq	SB4(%r10), %rcx
	xorq	SB2(%rsi), %rcx
	xorq	SB7(,%r14,8), %rcx
	xorq	SB5(%r15), %rcx
	xorq	SB3(%rbx), %rcx
	xorq	SB1(%rax), %rcx
	movq	160(%rdi), %rsi
	xorq	%rcx, %rsi
	movl	%esi, %r8d
	andl	$63, %r8d
	movq	%rsi, %r9
	shrq	$5, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movq	%rsi, %r10
	shrq	$13, %r10
	andl	$504, %r10d             # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%rcx, %rbx
	shlq	$28, %rbx
	movq	%rcx, %rax
	shrq	$4, %rax
	orq	%rbx, %rax
	xorq	168(%rdi), %rax
	movl	%eax, %r14d
	andl	$63, %r14d
	movq	%rax, %r15
	shrq	$5, %r15
	andl	$504, %r15d             # imm = 0x1F8
	movq	%rax, %rbx
	shrq	$13, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB8(,%r8,8), %r11
	xorq	SB6(%r9), %r11
	xorq	SB4(%r10), %r11
	xorq	SB2(%rsi), %r11
	xorq	SB7(,%r14,8), %r11
	xorq	SB5(%r15), %r11
	xorq	SB3(%rbx), %r11
	xorq	SB1(%rax), %r11
	movq	176(%rdi), %rsi
	xorq	%r11, %rsi
	movl	%esi, %r8d
	andl	$63, %r8d
	movq	%rsi, %r9
	shrq	$5, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movq	%rsi, %r10
	shrq	$13, %r10
	andl	$504, %r10d             # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%r11, %rbx
	shlq	$28, %rbx
	movq	%r11, %rax
	shrq	$4, %rax
	orq	%rbx, %rax
	xorq	184(%rdi), %rax
	movl	%eax, %r14d
	andl	$63, %r14d
	movq	%rax, %r15
	shrq	$5, %r15
	andl	$504, %r15d             # imm = 0x1F8
	movq	%rax, %rbx
	shrq	$13, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB8(,%r8,8), %rcx
	xorq	SB6(%r9), %rcx
	xorq	SB4(%r10), %rcx
	xorq	SB2(%rsi), %rcx
	xorq	SB7(,%r14,8), %rcx
	xorq	SB5(%r15), %rcx
	xorq	SB3(%rbx), %rcx
	xorq	SB1(%rax), %rcx
	movq	192(%rdi), %rsi
	xorq	%rcx, %rsi
	movl	%esi, %r8d
	andl	$63, %r8d
	movq	%rsi, %r9
	shrq	$5, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movq	%rsi, %r10
	shrq	$13, %r10
	andl	$504, %r10d             # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%rcx, %rbx
	shlq	$28, %rbx
	movq	%rcx, %rax
	shrq	$4, %rax
	orq	%rbx, %rax
	xorq	200(%rdi), %rax
	movl	%eax, %r14d
	andl	$63, %r14d
	movq	%rax, %r15
	shrq	$5, %r15
	andl	$504, %r15d             # imm = 0x1F8
	movq	%rax, %rbx
	shrq	$13, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB8(,%r8,8), %r11
	xorq	SB6(%r9), %r11
	xorq	SB4(%r10), %r11
	xorq	SB2(%rsi), %r11
	xorq	SB7(,%r14,8), %r11
	xorq	SB5(%r15), %r11
	xorq	SB3(%rbx), %r11
	xorq	SB1(%rax), %r11
	movq	208(%rdi), %rsi
	xorq	%r11, %rsi
	movl	%esi, %r8d
	andl	$63, %r8d
	movq	%rsi, %r9
	shrq	$5, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movq	%rsi, %r10
	shrq	$13, %r10
	andl	$504, %r10d             # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%r11, %rbx
	shlq	$28, %rbx
	movq	%r11, %rax
	shrq	$4, %rax
	orq	%rbx, %rax
	xorq	216(%rdi), %rax
	movl	%eax, %r14d
	andl	$63, %r14d
	movq	%rax, %r15
	shrq	$5, %r15
	andl	$504, %r15d             # imm = 0x1F8
	movq	%rax, %rbx
	shrq	$13, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB8(,%r8,8), %rcx
	xorq	SB6(%r9), %rcx
	xorq	SB4(%r10), %rcx
	xorq	SB2(%rsi), %rcx
	xorq	SB7(,%r14,8), %rcx
	xorq	SB5(%r15), %rcx
	xorq	SB3(%rbx), %rcx
	xorq	SB1(%rax), %rcx
	movq	224(%rdi), %rsi
	xorq	%rcx, %rsi
	movl	%esi, %r8d
	andl	$63, %r8d
	movq	%rsi, %r9
	shrq	$5, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movq	%rsi, %r10
	shrq	$13, %r10
	andl	$504, %r10d             # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%rcx, %rbx
	shlq	$28, %rbx
	movq	%rcx, %rax
	shrq	$4, %rax
	orq	%rbx, %rax
	xorq	232(%rdi), %rax
	movl	%eax, %r14d
	andl	$63, %r14d
	movq	%rax, %r15
	shrq	$5, %r15
	andl	$504, %r15d             # imm = 0x1F8
	movq	%rax, %rbx
	shrq	$13, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB8(,%r8,8), %r11
	xorq	SB6(%r9), %r11
	xorq	SB4(%r10), %r11
	xorq	SB2(%rsi), %r11
	xorq	SB7(,%r14,8), %r11
	xorq	SB5(%r15), %r11
	xorq	SB3(%rbx), %r11
	xorq	SB1(%rax), %r11
	movq	240(%rdi), %rsi
	xorq	%r11, %rsi
	movl	%esi, %r8d
	andl	$63, %r8d
	movq	%rsi, %r9
	shrq	$5, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movq	%rsi, %r10
	shrq	$13, %r10
	andl	$504, %r10d             # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	%r11, %rbx
	shlq	$28, %rbx
	movq	%r11, %rax
	shrq	$4, %rax
	orq	%rbx, %rax
	xorq	248(%rdi), %rax
	movl	%eax, %r14d
	andl	$63, %r14d
	movq	%rax, %rbx
	shrq	$5, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	movq	%rax, %rdi
	shrq	$13, %rdi
	andl	$504, %edi              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB8(,%r8,8), %rcx
	xorq	SB6(%r9), %rcx
	xorq	SB4(%r10), %rcx
	xorq	SB2(%rsi), %rcx
	xorq	SB7(,%r14,8), %rcx
	xorq	SB5(%rbx), %rcx
	xorq	SB3(%rdi), %rcx
	xorq	SB1(%rax), %rcx
	movq	%rcx, %rax
	shlq	$31, %rax
	shrq	%rcx
	orq	%rax, %rcx
	movl	%ecx, %esi
	movl	%r11d, %edi
	xorl	%ecx, %edi
	andl	$-1431655766, %edi      # imm = 0xAAAAAAAA
	xorq	%rdi, %rsi
	xorq	%r11, %rdi
	shlq	$31, %r11
	shrq	%rdi
	orq	%r11, %rdi
	movl	%edi, %eax
	shrl	$8, %edi
	xorl	%esi, %edi
	andl	$16711935, %edi         # imm = 0xFF00FF
	xorq	%rdi, %rsi
	shlq	$8, %rdi
	xorq	%rax, %rdi
	movl	%edi, %eax
	shrl	$2, %eax
	xorl	%esi, %eax
	andl	$858993459, %eax        # imm = 0x33333333
	xorq	%rax, %rsi
	shlq	$2, %rax
	xorq	%rdi, %rax
	movq	%rsi, %rcx
	shrq	$16, %rcx
	movzwl	%ax, %edi
	xorq	%rcx, %rdi
	xorq	%rdi, %rax
	shlq	$16, %rdi
	xorq	%rsi, %rdi
	movl	%edi, %esi
	shrl	$4, %esi
	movl	%eax, %ecx
	xorl	%esi, %ecx
	andl	$252645135, %ecx        # imm = 0xF0F0F0F
	xorq	%rcx, %rax
	shlq	$4, %rcx
	xorq	%rdi, %rcx
	movq	%rcx, %rsi
	shrq	$24, %rsi
	movb	%sil, (%rdx)
	movq	%rcx, %rsi
	shrq	$16, %rsi
	movb	%sil, 1(%rdx)
	movb	%ch, 2(%rdx)  # NOREX
	movb	%cl, 3(%rdx)
	movq	%rax, %rcx
	shrq	$24, %rcx
	movb	%cl, 4(%rdx)
	movq	%rax, %rcx
	shrq	$16, %rcx
	movb	%cl, 5(%rdx)
	movb	%ah, 6(%rdx)  # NOREX
	movb	%al, 7(%rdx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	des_crypt, .Lfunc_end2-des_crypt
	.cfi_endproc

	.globl	des_encrypt
	.p2align	4, 0x90
	.type	des_encrypt,@function
des_encrypt:                            # @des_encrypt
	.cfi_startproc
# BB#0:
	jmp	des_crypt               # TAILCALL
.Lfunc_end3:
	.size	des_encrypt, .Lfunc_end3-des_encrypt
	.cfi_endproc

	.globl	des_decrypt
	.p2align	4, 0x90
	.type	des_decrypt,@function
des_decrypt:                            # @des_decrypt
	.cfi_startproc
# BB#0:
	addq	$256, %rdi              # imm = 0x100
	jmp	des_crypt               # TAILCALL
.Lfunc_end4:
	.size	des_decrypt, .Lfunc_end4-des_decrypt
	.cfi_endproc

	.globl	des3_set_2keys
	.p2align	4, 0x90
	.type	des3_set_2keys,@function
des3_set_2keys:                         # @des3_set_2keys
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	callq	des_main_ks
	leaq	1024(%rbx), %rdi
	movq	%r14, %rsi
	callq	des_main_ks
	leaq	1272(%rbx), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	movq	-1032(%rax), %rdx
	movq	%rdx, 768(%rbx,%rcx,8)
	movq	-1024(%rax), %rsi
	movq	%rsi, 776(%rbx,%rcx,8)
	movq	-8(%rax), %rdi
	movq	%rdi, 256(%rbx,%rcx,8)
	movq	(%rax), %rdi
	movq	%rdi, 264(%rbx,%rcx,8)
	movups	(%rbx,%rcx,8), %xmm0
	movups	%xmm0, 512(%rbx,%rcx,8)
	movq	%rdx, 1280(%rbx,%rcx,8)
	movq	%rsi, 1288(%rbx,%rcx,8)
	addq	$2, %rcx
	addq	$-16, %rax
	cmpq	$32, %rcx
	jl	.LBB5_1
# BB#2:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	des3_set_2keys, .Lfunc_end5-des3_set_2keys
	.cfi_endproc

	.globl	des3_set_3keys
	.p2align	4, 0x90
	.type	des3_set_3keys,@function
des3_set_3keys:                         # @des3_set_3keys
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 32
.Lcfi28:
	.cfi_offset %rbx, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	callq	des_main_ks
	leaq	1024(%rbx), %rdi
	movq	%r15, %rsi
	callq	des_main_ks
	leaq	512(%rbx), %rdi
	movq	%r14, %rsi
	callq	des_main_ks
	leaq	1272(%rbx), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	movq	-520(%rax), %rdx
	movq	%rdx, 768(%rbx,%rcx,8)
	movq	-512(%rax), %rdx
	movq	%rdx, 776(%rbx,%rcx,8)
	movq	-8(%rax), %rdx
	movq	%rdx, 256(%rbx,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, 264(%rbx,%rcx,8)
	movq	-1032(%rax), %rdx
	movq	%rdx, 1280(%rbx,%rcx,8)
	movq	-1024(%rax), %rdx
	movq	%rdx, 1288(%rbx,%rcx,8)
	addq	$2, %rcx
	addq	$-16, %rax
	cmpq	$32, %rcx
	jl	.LBB6_1
# BB#2:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	des3_set_3keys, .Lfunc_end6-des3_set_3keys
	.cfi_endproc

	.globl	des3_crypt
	.p2align	4, 0x90
	.type	des3_crypt,@function
des3_crypt:                             # @des3_crypt
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 16
.Lcfi32:
	.cfi_offset %rbx, -16
	movzbl	(%rsi), %eax
	shlq	$24, %rax
	movzbl	1(%rsi), %ecx
	shlq	$16, %rcx
	orq	%rax, %rcx
	movzbl	2(%rsi), %eax
	shlq	$8, %rax
	orq	%rcx, %rax
	movzbl	3(%rsi), %r8d
	orq	%rax, %r8
	movzbl	4(%rsi), %eax
	shlq	$24, %rax
	movzbl	5(%rsi), %ecx
	shlq	$16, %rcx
	orq	%rax, %rcx
	movzbl	6(%rsi), %eax
	shlq	$8, %rax
	orq	%rcx, %rax
	movzbl	7(%rsi), %ecx
	orq	%rax, %rcx
	movl	%r8d, %esi
	shrl	$4, %esi
	xorl	%ecx, %esi
	andl	$252645135, %esi        # imm = 0xF0F0F0F
	xorq	%rsi, %rcx
	shlq	$4, %rsi
	xorq	%r8, %rsi
	movl	%esi, %eax
	shrl	$16, %eax
	xorl	%ecx, %eax
	movzwl	%ax, %r9d
	xorq	%r9, %rcx
	shlq	$16, %r9
	xorq	%rsi, %r9
	movl	%ecx, %esi
	shrl	$2, %esi
	xorl	%r9d, %esi
	andl	$858993459, %esi        # imm = 0x33333333
	xorq	%rsi, %r9
	shlq	$2, %rsi
	xorq	%rcx, %rsi
	movl	%esi, %ecx
	shrl	$8, %ecx
	xorl	%r9d, %ecx
	andl	$16711935, %ecx         # imm = 0xFF00FF
	xorq	%rcx, %r9
	shlq	$8, %rcx
	xorq	%rsi, %rcx
	leaq	(%rcx,%rcx), %rsi
	shrq	$31, %rcx
	orq	%rcx, %rsi
	movl	%esi, %r10d
	movl	%r9d, %eax
	xorl	%esi, %eax
	andl	$-1431655766, %eax      # imm = 0xAAAAAAAA
	xorq	%rax, %r10
	xorq	%rax, %r9
	movq	%r9, %rax
	shrq	$31, %rax
	addl	%r9d, %r9d
	orl	%eax, %r9d
	movq	(%rdi), %rcx
	xorq	%r10, %rcx
	movl	%ecx, %esi
	andl	$63, %esi
	movq	%rcx, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	movq	SB6(%rax), %r8
	xorq	SB8(,%rsi,8), %r8
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB4(%rax), %r8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB2(%rcx), %r8
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rsi
	shrq	$4, %rsi
	orq	%rax, %rsi
	xorq	8(%rdi), %rsi
	movl	%esi, %eax
	andl	$63, %eax
	movq	%rsi, %rcx
	shrq	$5, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r8
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB5(%rcx), %r8
	xorq	SB3(%rax), %r8
	xorq	SB1(%rsi), %r8
	xorq	%r9, %r8
	movq	16(%rdi), %rcx
	xorq	%r8, %rcx
	movl	%ecx, %esi
	andl	$63, %esi
	movq	%rcx, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	movq	SB6(%rax), %rbx
	xorq	SB8(,%rsi,8), %rbx
	movq	%rcx, %rsi
	shrq	$13, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB4(%rsi), %rbx
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB2(%rcx), %rbx
	movq	%r8, %rcx
	shlq	$28, %rcx
	movq	%r8, %rsi
	shrq	$4, %rsi
	orq	%rcx, %rsi
	xorq	24(%rdi), %rsi
	movl	%esi, %r9d
	andl	$63, %r9d
	movq	%rsi, %r11
	shrq	$5, %r11
	andl	$504, %r11d             # imm = 0x1F8
	xorq	SB7(,%r9,8), %rbx
	movq	%rsi, %rcx
	shrq	$13, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB5(%r11), %rbx
	xorq	SB3(%rcx), %rbx
	xorq	SB1(%rsi), %rbx
	xorq	%r10, %rbx
	movq	32(%rdi), %r11
	xorq	%rbx, %r11
	movl	%r11d, %r9d
	andl	$63, %r9d
	movq	%rbx, %rsi
	shlq	$28, %rsi
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rsi, %rcx
	movq	%r11, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	movq	SB6(%rsi), %r10
	xorq	SB8(,%r9,8), %r10
	movq	%r11, %rsi
	shrq	$13, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB4(%rsi), %r10
	shrq	$21, %r11
	andl	$504, %r11d             # imm = 0x1F8
	xorq	40(%rdi), %rcx
	xorq	SB2(%r11), %r10
	movl	%ecx, %esi
	andl	$63, %esi
	xorq	SB7(,%rsi,8), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$13, %rsi
	andl	$504, %esi              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB3(%rsi), %r10
	xorq	%r8, %r10
	xorq	SB1(%rcx), %r10
	movq	48(%rdi), %r8
	xorq	%r10, %r8
	movq	%r10, %rsi
	shlq	$28, %rsi
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rsi, %rcx
	movl	%r8d, %esi
	andl	$63, %esi
	xorq	SB8(,%rsi,8), %rbx
	movq	%r8, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB6(%rsi), %rbx
	movq	%r8, %rsi
	shrq	$13, %rsi
	andl	$504, %esi              # imm = 0x1F8
	shrq	$21, %r8
	andl	$504, %r8d              # imm = 0x1F8
	xorq	56(%rdi), %rcx
	xorq	SB4(%rsi), %rbx
	movl	%ecx, %esi
	andl	$63, %esi
	xorq	SB2(%r8), %rbx
	movq	%rcx, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB7(,%rsi,8), %rbx
	movq	%rcx, %rsi
	shrq	$13, %rsi
	andl	$504, %esi              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rax), %rbx
	xorq	SB3(%rsi), %rbx
	xorq	SB1(%rcx), %rbx
	movq	64(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	72(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	80(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	88(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	96(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	104(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	112(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	120(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	128(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	136(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	144(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	152(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	160(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	168(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	176(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	184(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	192(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	200(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	208(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	216(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	224(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	232(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	240(%rdi), %r8
	xorq	%r10, %r8
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %r9
	shrq	$4, %r9
	orq	%rax, %r9
	movl	%r8d, %esi
	andl	$63, %esi
	xorq	SB8(,%rsi,8), %rbx
	movq	%r8, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%r8, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %r8
	andl	$504, %r8d              # imm = 0x1F8
	xorq	SB4(%rax), %rbx
	movq	248(%rdi), %rax
	xorq	%r9, %rax
	xorq	SB2(%r8), %rbx
	movl	%eax, %ecx
	andl	$63, %ecx
	xorq	SB7(,%rcx,8), %rbx
	movq	%rax, %rcx
	shrq	$5, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rcx), %rbx
	movq	%rax, %rcx
	shrq	$13, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB3(%rcx), %rbx
	movq	256(%rdi), %rcx
	xorq	%r10, %rcx
	xorq	SB1(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	264(%rdi), %r9
	xorq	SB4(%rax), %rbx
	movl	%r9d, %eax
	andl	$63, %eax
	xorq	SB2(%rcx), %rbx
	movq	%r9, %rcx
	shrq	$5, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%r9, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %r9
	andl	$504, %r9d              # imm = 0x1F8
	xorq	SB5(%rcx), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%r9), %rbx
	movq	272(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	280(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	288(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	296(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	304(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	312(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	320(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	328(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	336(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	344(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	352(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	360(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	368(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	376(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	384(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	392(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	400(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	408(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	416(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	424(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	432(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	440(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	448(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	456(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	464(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	472(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	480(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	488(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	496(%rdi), %r8
	xorq	%rbx, %r8
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %r9
	shrq	$4, %r9
	orq	%rax, %r9
	movl	%r8d, %esi
	andl	$63, %esi
	xorq	SB8(,%rsi,8), %r10
	movq	%r8, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%r8, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %r8
	andl	$504, %r8d              # imm = 0x1F8
	xorq	SB4(%rax), %r10
	movq	504(%rdi), %rax
	xorq	%r9, %rax
	xorq	SB2(%r8), %r10
	movl	%eax, %ecx
	andl	$63, %ecx
	xorq	SB7(,%rcx,8), %r10
	movq	%rax, %rcx
	shrq	$5, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rcx), %r10
	movq	%rax, %rcx
	shrq	$13, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB3(%rcx), %r10
	movq	512(%rdi), %rcx
	xorq	%rbx, %rcx
	xorq	SB1(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	520(%rdi), %r9
	xorq	SB4(%rax), %r10
	movl	%r9d, %eax
	andl	$63, %eax
	xorq	SB2(%rcx), %r10
	movq	%r9, %rcx
	shrq	$5, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%r9, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %r9
	andl	$504, %r9d              # imm = 0x1F8
	xorq	SB5(%rcx), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%r9), %r10
	movq	528(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	536(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	544(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	552(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	560(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	568(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	576(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	584(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	592(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	600(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	608(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	616(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	624(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	632(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	640(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	648(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	656(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	664(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	672(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	680(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	688(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	696(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	704(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	712(%rdi), %rcx
	xorq	SB4(%rax), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %r10
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %r10
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	720(%rdi), %rsi
	xorq	%r10, %rsi
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %rbx
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %rbx
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	728(%rdi), %rcx
	xorq	SB4(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB2(%rsi), %rbx
	movq	%rcx, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB5(%rsi), %rbx
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	736(%rdi), %rsi
	xorq	%rbx, %rsi
	movq	%rbx, %rax
	shlq	$28, %rax
	movq	%rbx, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movl	%esi, %eax
	andl	$63, %eax
	xorq	SB8(,%rax,8), %r10
	movq	%rsi, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB6(%rax), %r10
	movq	%rsi, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB4(%rax), %r10
	shrq	$21, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	744(%rdi), %rcx
	xorq	SB2(%rsi), %r10
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB7(,%rax,8), %r10
	movq	%rcx, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB5(%rax), %r10
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB3(%rax), %r10
	xorq	SB1(%rcx), %r10
	movq	%r10, %rax
	shlq	$28, %rax
	movq	%r10, %rcx
	shrq	$4, %rcx
	orq	%rax, %rcx
	movq	752(%rdi), %rax
	xorq	%r10, %rax
	xorq	760(%rdi), %rcx
	movl	%eax, %esi
	andl	$63, %esi
	xorq	SB8(,%rsi,8), %rbx
	movq	%rax, %rsi
	shrq	$5, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB6(%rsi), %rbx
	movq	%rax, %rsi
	shrq	$13, %rsi
	andl	$504, %esi              # imm = 0x1F8
	xorq	SB4(%rsi), %rbx
	shrq	$21, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB2(%rax), %rbx
	movl	%ecx, %eax
	andl	$63, %eax
	xorq	SB7(,%rax,8), %rbx
	movq	%rcx, %rax
	shrq	$5, %rax
	andl	$504, %eax              # imm = 0x1F8
	xorq	SB5(%rax), %rbx
	movq	%rcx, %rax
	shrq	$13, %rax
	andl	$504, %eax              # imm = 0x1F8
	shrq	$21, %rcx
	andl	$504, %ecx              # imm = 0x1F8
	xorq	SB3(%rax), %rbx
	xorq	SB1(%rcx), %rbx
	movq	%rbx, %rax
	shlq	$31, %rax
	shrq	%rbx
	orq	%rax, %rbx
	movl	%ebx, %esi
	movl	%r10d, %edi
	xorl	%ebx, %edi
	andl	$-1431655766, %edi      # imm = 0xAAAAAAAA
	xorq	%rdi, %rsi
	xorq	%r10, %rdi
	shlq	$31, %r10
	shrq	%rdi
	orq	%r10, %rdi
	movl	%edi, %eax
	shrl	$8, %edi
	xorl	%esi, %edi
	andl	$16711935, %edi         # imm = 0xFF00FF
	xorq	%rdi, %rsi
	shlq	$8, %rdi
	xorq	%rax, %rdi
	movl	%edi, %eax
	shrl	$2, %eax
	xorl	%esi, %eax
	andl	$858993459, %eax        # imm = 0x33333333
	xorq	%rax, %rsi
	shlq	$2, %rax
	xorq	%rdi, %rax
	movq	%rsi, %rcx
	shrq	$16, %rcx
	movzwl	%ax, %edi
	xorq	%rcx, %rdi
	xorq	%rdi, %rax
	shlq	$16, %rdi
	xorq	%rsi, %rdi
	movl	%edi, %esi
	shrl	$4, %esi
	movl	%eax, %ecx
	xorl	%esi, %ecx
	andl	$252645135, %ecx        # imm = 0xF0F0F0F
	xorq	%rcx, %rax
	shlq	$4, %rcx
	xorq	%rdi, %rcx
	movq	%rcx, %rsi
	shrq	$24, %rsi
	movb	%sil, (%rdx)
	movq	%rcx, %rsi
	shrq	$16, %rsi
	movb	%sil, 1(%rdx)
	movb	%ch, 2(%rdx)  # NOREX
	movb	%cl, 3(%rdx)
	movq	%rax, %rcx
	shrq	$24, %rcx
	movb	%cl, 4(%rdx)
	movq	%rax, %rcx
	shrq	$16, %rcx
	movb	%cl, 5(%rdx)
	movb	%ah, 6(%rdx)  # NOREX
	movb	%al, 7(%rdx)
	popq	%rbx
	retq
.Lfunc_end7:
	.size	des3_crypt, .Lfunc_end7-des3_crypt
	.cfi_endproc

	.globl	des3_encrypt
	.p2align	4, 0x90
	.type	des3_encrypt,@function
des3_encrypt:                           # @des3_encrypt
	.cfi_startproc
# BB#0:
	jmp	des3_crypt              # TAILCALL
.Lfunc_end8:
	.size	des3_encrypt, .Lfunc_end8-des3_encrypt
	.cfi_endproc

	.globl	des3_decrypt
	.p2align	4, 0x90
	.type	des3_decrypt,@function
des3_decrypt:                           # @des3_decrypt
	.cfi_startproc
# BB#0:
	addq	$768, %rdi              # imm = 0x300
	jmp	des3_crypt              # TAILCALL
.Lfunc_end9:
	.size	des3_decrypt, .Lfunc_end9-des3_decrypt
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$2104, %rsp             # imm = 0x838
.Lcfi39:
	.cfi_def_cfa_offset 2160
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movl	$100, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpl	$2, %edi
	jne	.LBB10_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB10_2:
	leaq	1336(%rsp), %r14
	leaq	16(%rsp), %r12
	xorl	%r13d, %r13d
.LBB10_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_9 Depth 2
                                        #       Child Loop BB10_11 Depth 3
                                        #         Child Loop BB10_17 Depth 4
                                        #         Child Loop BB10_20 Depth 4
                                        #         Child Loop BB10_23 Depth 4
                                        #         Child Loop BB10_28 Depth 4
                                        #         Child Loop BB10_32 Depth 4
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	testl	%r13d, %r13d
	je	.LBB10_6
# BB#4:                                 #   in Loop: Header=BB10_3 Depth=1
	cmpl	$1, %r13d
	jne	.LBB10_7
# BB#5:                                 #   in Loop: Header=BB10_3 Depth=1
	movl	$.Lstr.2, %edi
	callq	puts
	movb	$1, %bpl
	jmp	.LBB10_8
	.p2align	4, 0x90
.LBB10_6:                               # %.thread
                                        #   in Loop: Header=BB10_3 Depth=1
	movl	$.Lstr.3, %edi
	callq	puts
.LBB10_7:                               #   in Loop: Header=BB10_3 Depth=1
	xorl	%ebp, %ebp
.LBB10_8:                               # %.preheader
                                        #   in Loop: Header=BB10_3 Depth=1
	xorl	%ebx, %ebx
	movl	%r13d, 28(%rsp)         # 4-byte Spill
.LBB10_9:                               #   Parent Loop BB10_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_11 Depth 3
                                        #         Child Loop BB10_17 Depth 4
                                        #         Child Loop BB10_20 Depth 4
                                        #         Child Loop BB10_23 Depth 4
                                        #         Child Loop BB10_28 Depth 4
                                        #         Child Loop BB10_32 Depth 4
	leaq	1(%rbx), %rsi
	movl	%ebx, %edx
	shll	$6, %edx
	addl	$64, %edx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movq	%rsi, 32(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB10_40
# BB#10:                                # %.lr.ph
                                        #   in Loop: Header=BB10_9 Depth=2
	xorl	%r15d, %r15d
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB10_11:                              #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_9 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_17 Depth 4
                                        #         Child Loop BB10_20 Depth 4
                                        #         Child Loop BB10_23 Depth 4
                                        #         Child Loop BB10_28 Depth 4
                                        #         Child Loop BB10_32 Depth 4
	movabsq	$8367815003007840078, %rax # imm = 0x7420736920776F4E
	movq	%rax, 16(%rsp)
	cmpl	$2, %ebx
	je	.LBB10_15
# BB#12:                                #   in Loop: Header=BB10_11 Depth=3
	cmpl	$1, %ebx
	je	.LBB10_16
# BB#13:                                #   in Loop: Header=BB10_11 Depth=3
	testl	%ebx, %ebx
	jne	.LBB10_19
# BB#14:                                #   in Loop: Header=BB10_11 Depth=3
	movl	$DES3_keys, %esi
	leaq	48(%rsp), %rdi
	callq	des_main_ks
	movaps	288(%rsp), %xmm0
	movaps	%xmm0, 304(%rsp)
	movaps	272(%rsp), %xmm0
	movaps	%xmm0, 320(%rsp)
	movaps	256(%rsp), %xmm0
	movaps	%xmm0, 336(%rsp)
	movaps	240(%rsp), %xmm0
	movaps	%xmm0, 352(%rsp)
	movaps	224(%rsp), %xmm0
	movaps	%xmm0, 368(%rsp)
	movaps	208(%rsp), %xmm0
	movaps	%xmm0, 384(%rsp)
	movaps	192(%rsp), %xmm0
	movaps	%xmm0, 400(%rsp)
	movaps	176(%rsp), %xmm0
	movaps	%xmm0, 416(%rsp)
	movaps	160(%rsp), %xmm0
	movaps	%xmm0, 432(%rsp)
	movaps	144(%rsp), %xmm0
	movaps	%xmm0, 448(%rsp)
	movaps	128(%rsp), %xmm0
	movaps	%xmm0, 464(%rsp)
	movaps	112(%rsp), %xmm0
	movaps	%xmm0, 480(%rsp)
	movaps	96(%rsp), %xmm0
	movaps	%xmm0, 496(%rsp)
	movaps	80(%rsp), %xmm0
	movaps	%xmm0, 512(%rsp)
	movaps	64(%rsp), %xmm0
	movaps	%xmm0, 528(%rsp)
	movaps	48(%rsp), %xmm0
	movaps	%xmm0, 544(%rsp)
	testq	%rbx, %rbx
	jne	.LBB10_19
	jmp	.LBB10_26
	.p2align	4, 0x90
.LBB10_15:                              #   in Loop: Header=BB10_11 Depth=3
	movl	$DES3_keys, %esi
	movl	$DES3_keys+8, %edx
	movl	$DES3_keys+16, %ecx
	leaq	568(%rsp), %rdi
	callq	des3_set_3keys
	testq	%rbx, %rbx
	jne	.LBB10_19
	jmp	.LBB10_26
	.p2align	4, 0x90
.LBB10_16:                              #   in Loop: Header=BB10_11 Depth=3
	movl	$DES3_keys, %esi
	leaq	568(%rsp), %rdi
	callq	des_main_ks
	movl	$DES3_keys+8, %esi
	leaq	1592(%rsp), %rdi
	callq	des_main_ks
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_17:                              #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_9 Depth=2
                                        #       Parent Loop BB10_11 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	808(%rsp,%rax,8), %rdx
	movq	%rdx, 1336(%rsp,%rcx,8)
	movq	816(%rsp,%rax,8), %rsi
	movq	%rsi, 1344(%rsp,%rcx,8)
	movq	1832(%rsp,%rax,8), %rdi
	movq	%rdi, 824(%rsp,%rcx,8)
	movq	1840(%rsp,%rax,8), %rdi
	movq	%rdi, 832(%rsp,%rcx,8)
	movups	568(%rsp,%rcx,8), %xmm0
	movups	%xmm0, 1080(%rsp,%rcx,8)
	movq	%rdx, 1848(%rsp,%rcx,8)
	movq	%rsi, 1856(%rsp,%rcx,8)
	addq	$2, %rcx
	addq	$-2, %rax
	cmpq	$32, %rcx
	jl	.LBB10_17
# BB#18:                                # %des3_set_2keys.exit.preheader
                                        #   in Loop: Header=BB10_11 Depth=3
	testq	%rbx, %rbx
	je	.LBB10_26
.LBB10_19:                              # %des3_set_2keys.exit.preheader.split
                                        #   in Loop: Header=BB10_11 Depth=3
	movl	$10000, %ebx            # imm = 0x2710
	testl	%r13d, %r13d
	je	.LBB10_23
	.p2align	4, 0x90
.LBB10_20:                              # %des3_set_2keys.exit.preheader.split.split
                                        #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_9 Depth=2
                                        #       Parent Loop BB10_11 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testb	%bpl, %bpl
	je	.LBB10_22
# BB#21:                                #   in Loop: Header=BB10_20 Depth=4
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%r12, %rdx
	callq	des3_crypt
.LBB10_22:                              # %des3_set_2keys.exit
                                        #   in Loop: Header=BB10_20 Depth=4
	decl	%ebx
	jne	.LBB10_20
	jmp	.LBB10_35
	.p2align	4, 0x90
.LBB10_23:                              # %des3_set_2keys.exit.preheader.split.split.us
                                        #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_9 Depth=2
                                        #       Parent Loop BB10_11 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	568(%rsp), %rdi
	movq	%r12, %rsi
	movq	%r12, %rdx
	callq	des3_crypt
	testb	%bpl, %bpl
	je	.LBB10_25
# BB#24:                                #   in Loop: Header=BB10_23 Depth=4
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%r12, %rdx
	callq	des3_crypt
.LBB10_25:                              # %des3_set_2keys.exit.us47
                                        #   in Loop: Header=BB10_23 Depth=4
	decl	%ebx
	jne	.LBB10_23
	jmp	.LBB10_35
	.p2align	4, 0x90
.LBB10_26:                              # %des3_set_2keys.exit.preheader.split.us
                                        #   in Loop: Header=BB10_11 Depth=3
	testl	%r13d, %r13d
	je	.LBB10_31
# BB#27:                                # %des3_set_2keys.exit.preheader.split.us.split.preheader
                                        #   in Loop: Header=BB10_11 Depth=3
	movl	$10000, %ebx            # imm = 0x2710
	.p2align	4, 0x90
.LBB10_28:                              # %des3_set_2keys.exit.preheader.split.us.split
                                        #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_9 Depth=2
                                        #       Parent Loop BB10_11 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testb	%bpl, %bpl
	je	.LBB10_30
# BB#29:                                #   in Loop: Header=BB10_28 Depth=4
	leaq	304(%rsp), %rdi
	movq	%r12, %rsi
	movq	%r12, %rdx
	callq	des_crypt
.LBB10_30:                              # %des3_set_2keys.exit.us
                                        #   in Loop: Header=BB10_28 Depth=4
	decl	%ebx
	jne	.LBB10_28
	jmp	.LBB10_35
.LBB10_31:                              # %des3_set_2keys.exit.preheader.split.us.split.us.preheader
                                        #   in Loop: Header=BB10_11 Depth=3
	movl	$10000, %r13d           # imm = 0x2710
	.p2align	4, 0x90
.LBB10_32:                              # %des3_set_2keys.exit.preheader.split.us.split.us
                                        #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_9 Depth=2
                                        #       Parent Loop BB10_11 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	48(%rsp), %rdi
	movq	%r12, %rsi
	movq	%r12, %rdx
	callq	des_crypt
	testb	%bpl, %bpl
	je	.LBB10_34
# BB#33:                                #   in Loop: Header=BB10_32 Depth=4
	leaq	304(%rsp), %rdi
	movq	%r12, %rsi
	movq	%r12, %rdx
	callq	des_crypt
.LBB10_34:                              # %des3_set_2keys.exit.us.us
                                        #   in Loop: Header=BB10_32 Depth=4
	decl	%r13d
	jne	.LBB10_32
	.p2align	4, 0x90
.LBB10_35:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB10_11 Depth=3
	movl	28(%rsp), %r13d         # 4-byte Reload
	testl	%r13d, %r13d
	movq	40(%rsp), %rbx          # 8-byte Reload
	jne	.LBB10_37
# BB#36:                                #   in Loop: Header=BB10_11 Depth=3
	movq	16(%rsp), %rax
	cmpq	DES3_enc_test(,%rbx,8), %rax
	jne	.LBB10_43
.LBB10_37:                              #   in Loop: Header=BB10_11 Depth=3
	testb	%bpl, %bpl
	je	.LBB10_39
# BB#38:                                #   in Loop: Header=BB10_11 Depth=3
	movq	16(%rsp), %rax
	cmpq	DES3_dec_test(,%rbx,8), %rax
	jne	.LBB10_43
.LBB10_39:                              #   in Loop: Header=BB10_11 Depth=3
	incl	%r15d
	cmpl	8(%rsp), %r15d          # 4-byte Folded Reload
	jl	.LBB10_11
.LBB10_40:                              # %._crit_edge
                                        #   in Loop: Header=BB10_9 Depth=2
	movl	$.Lstr, %edi
	callq	puts
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpq	$3, %rax
	movq	%rax, %rbx
	jl	.LBB10_9
# BB#41:                                #   in Loop: Header=BB10_3 Depth=1
	incl	%r13d
	cmpl	$2, %r13d
	jl	.LBB10_3
# BB#42:
	movl	$10, %edi
	callq	putchar
	xorl	%eax, %eax
	jmp	.LBB10_44
.LBB10_43:
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$1, %eax
.LBB10_44:
	addq	$2104, %rsp             # imm = 0x838
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	main, .Lfunc_end10-main
	.cfi_endproc

	.type	SB1,@object             # @SB1
	.data
	.globl	SB1
	.p2align	4
SB1:
	.quad	16843776                # 0x1010400
	.quad	0                       # 0x0
	.quad	65536                   # 0x10000
	.quad	16843780                # 0x1010404
	.quad	16842756                # 0x1010004
	.quad	66564                   # 0x10404
	.quad	4                       # 0x4
	.quad	65536                   # 0x10000
	.quad	1024                    # 0x400
	.quad	16843776                # 0x1010400
	.quad	16843780                # 0x1010404
	.quad	1024                    # 0x400
	.quad	16778244                # 0x1000404
	.quad	16842756                # 0x1010004
	.quad	16777216                # 0x1000000
	.quad	4                       # 0x4
	.quad	1028                    # 0x404
	.quad	16778240                # 0x1000400
	.quad	16778240                # 0x1000400
	.quad	66560                   # 0x10400
	.quad	66560                   # 0x10400
	.quad	16842752                # 0x1010000
	.quad	16842752                # 0x1010000
	.quad	16778244                # 0x1000404
	.quad	65540                   # 0x10004
	.quad	16777220                # 0x1000004
	.quad	16777220                # 0x1000004
	.quad	65540                   # 0x10004
	.quad	0                       # 0x0
	.quad	1028                    # 0x404
	.quad	66564                   # 0x10404
	.quad	16777216                # 0x1000000
	.quad	65536                   # 0x10000
	.quad	16843780                # 0x1010404
	.quad	4                       # 0x4
	.quad	16842752                # 0x1010000
	.quad	16843776                # 0x1010400
	.quad	16777216                # 0x1000000
	.quad	16777216                # 0x1000000
	.quad	1024                    # 0x400
	.quad	16842756                # 0x1010004
	.quad	65536                   # 0x10000
	.quad	66560                   # 0x10400
	.quad	16777220                # 0x1000004
	.quad	1024                    # 0x400
	.quad	4                       # 0x4
	.quad	16778244                # 0x1000404
	.quad	66564                   # 0x10404
	.quad	16843780                # 0x1010404
	.quad	65540                   # 0x10004
	.quad	16842752                # 0x1010000
	.quad	16778244                # 0x1000404
	.quad	16777220                # 0x1000004
	.quad	1028                    # 0x404
	.quad	66564                   # 0x10404
	.quad	16843776                # 0x1010400
	.quad	1028                    # 0x404
	.quad	16778240                # 0x1000400
	.quad	16778240                # 0x1000400
	.quad	0                       # 0x0
	.quad	65540                   # 0x10004
	.quad	66560                   # 0x10400
	.quad	0                       # 0x0
	.quad	16842756                # 0x1010004
	.size	SB1, 512

	.type	LHs,@object             # @LHs
	.section	.rodata,"a",@progbits
	.p2align	4
LHs:
	.quad	0                       # 0x0
	.quad	1                       # 0x1
	.quad	256                     # 0x100
	.quad	257                     # 0x101
	.quad	65536                   # 0x10000
	.quad	65537                   # 0x10001
	.quad	65792                   # 0x10100
	.quad	65793                   # 0x10101
	.quad	16777216                # 0x1000000
	.quad	16777217                # 0x1000001
	.quad	16777472                # 0x1000100
	.quad	16777473                # 0x1000101
	.quad	16842752                # 0x1010000
	.quad	16842753                # 0x1010001
	.quad	16843008                # 0x1010100
	.quad	16843009                # 0x1010101
	.size	LHs, 128

	.type	RHs,@object             # @RHs
	.p2align	4
RHs:
	.quad	0                       # 0x0
	.quad	16777216                # 0x1000000
	.quad	65536                   # 0x10000
	.quad	16842752                # 0x1010000
	.quad	256                     # 0x100
	.quad	16777472                # 0x1000100
	.quad	65792                   # 0x10100
	.quad	16843008                # 0x1010100
	.quad	1                       # 0x1
	.quad	16777217                # 0x1000001
	.quad	65537                   # 0x10001
	.quad	16842753                # 0x1010001
	.quad	257                     # 0x101
	.quad	16777473                # 0x1000101
	.quad	65793                   # 0x10101
	.quad	16843009                # 0x1010101
	.size	RHs, 128

	.type	SB8,@object             # @SB8
	.p2align	4
SB8:
	.quad	268439616               # 0x10001040
	.quad	4096                    # 0x1000
	.quad	262144                  # 0x40000
	.quad	268701760               # 0x10041040
	.quad	268435456               # 0x10000000
	.quad	268439616               # 0x10001040
	.quad	64                      # 0x40
	.quad	268435456               # 0x10000000
	.quad	262208                  # 0x40040
	.quad	268697600               # 0x10040000
	.quad	268701760               # 0x10041040
	.quad	266240                  # 0x41000
	.quad	268701696               # 0x10041000
	.quad	266304                  # 0x41040
	.quad	4096                    # 0x1000
	.quad	64                      # 0x40
	.quad	268697600               # 0x10040000
	.quad	268435520               # 0x10000040
	.quad	268439552               # 0x10001000
	.quad	4160                    # 0x1040
	.quad	266240                  # 0x41000
	.quad	262208                  # 0x40040
	.quad	268697664               # 0x10040040
	.quad	268701696               # 0x10041000
	.quad	4160                    # 0x1040
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	268697664               # 0x10040040
	.quad	268435520               # 0x10000040
	.quad	268439552               # 0x10001000
	.quad	266304                  # 0x41040
	.quad	262144                  # 0x40000
	.quad	266304                  # 0x41040
	.quad	262144                  # 0x40000
	.quad	268701696               # 0x10041000
	.quad	4096                    # 0x1000
	.quad	64                      # 0x40
	.quad	268697664               # 0x10040040
	.quad	4096                    # 0x1000
	.quad	266304                  # 0x41040
	.quad	268439552               # 0x10001000
	.quad	64                      # 0x40
	.quad	268435520               # 0x10000040
	.quad	268697600               # 0x10040000
	.quad	268697664               # 0x10040040
	.quad	268435456               # 0x10000000
	.quad	262144                  # 0x40000
	.quad	268439616               # 0x10001040
	.quad	0                       # 0x0
	.quad	268701760               # 0x10041040
	.quad	262208                  # 0x40040
	.quad	268435520               # 0x10000040
	.quad	268697600               # 0x10040000
	.quad	268439552               # 0x10001000
	.quad	268439616               # 0x10001040
	.quad	0                       # 0x0
	.quad	268701760               # 0x10041040
	.quad	266240                  # 0x41000
	.quad	266240                  # 0x41000
	.quad	4160                    # 0x1040
	.quad	4160                    # 0x1040
	.quad	262208                  # 0x40040
	.quad	268435456               # 0x10000000
	.quad	268701696               # 0x10041000
	.size	SB8, 512

	.type	SB6,@object             # @SB6
	.p2align	4
SB6:
	.quad	536870928               # 0x20000010
	.quad	541065216               # 0x20400000
	.quad	16384                   # 0x4000
	.quad	541081616               # 0x20404010
	.quad	541065216               # 0x20400000
	.quad	16                      # 0x10
	.quad	541081616               # 0x20404010
	.quad	4194304                 # 0x400000
	.quad	536887296               # 0x20004000
	.quad	4210704                 # 0x404010
	.quad	4194304                 # 0x400000
	.quad	536870928               # 0x20000010
	.quad	4194320                 # 0x400010
	.quad	536887296               # 0x20004000
	.quad	536870912               # 0x20000000
	.quad	16400                   # 0x4010
	.quad	0                       # 0x0
	.quad	4194320                 # 0x400010
	.quad	536887312               # 0x20004010
	.quad	16384                   # 0x4000
	.quad	4210688                 # 0x404000
	.quad	536887312               # 0x20004010
	.quad	16                      # 0x10
	.quad	541065232               # 0x20400010
	.quad	541065232               # 0x20400010
	.quad	0                       # 0x0
	.quad	4210704                 # 0x404010
	.quad	541081600               # 0x20404000
	.quad	16400                   # 0x4010
	.quad	4210688                 # 0x404000
	.quad	541081600               # 0x20404000
	.quad	536870912               # 0x20000000
	.quad	536887296               # 0x20004000
	.quad	16                      # 0x10
	.quad	541065232               # 0x20400010
	.quad	4210688                 # 0x404000
	.quad	541081616               # 0x20404010
	.quad	4194304                 # 0x400000
	.quad	16400                   # 0x4010
	.quad	536870928               # 0x20000010
	.quad	4194304                 # 0x400000
	.quad	536887296               # 0x20004000
	.quad	536870912               # 0x20000000
	.quad	16400                   # 0x4010
	.quad	536870928               # 0x20000010
	.quad	541081616               # 0x20404010
	.quad	4210688                 # 0x404000
	.quad	541065216               # 0x20400000
	.quad	4210704                 # 0x404010
	.quad	541081600               # 0x20404000
	.quad	0                       # 0x0
	.quad	541065232               # 0x20400010
	.quad	16                      # 0x10
	.quad	16384                   # 0x4000
	.quad	541065216               # 0x20400000
	.quad	4210704                 # 0x404010
	.quad	16384                   # 0x4000
	.quad	4194320                 # 0x400010
	.quad	536887312               # 0x20004010
	.quad	0                       # 0x0
	.quad	541081600               # 0x20404000
	.quad	536870912               # 0x20000000
	.quad	4194320                 # 0x400010
	.quad	536887312               # 0x20004010
	.size	SB6, 512

	.type	SB4,@object             # @SB4
	.p2align	4
SB4:
	.quad	8396801                 # 0x802001
	.quad	8321                    # 0x2081
	.quad	8321                    # 0x2081
	.quad	128                     # 0x80
	.quad	8396928                 # 0x802080
	.quad	8388737                 # 0x800081
	.quad	8388609                 # 0x800001
	.quad	8193                    # 0x2001
	.quad	0                       # 0x0
	.quad	8396800                 # 0x802000
	.quad	8396800                 # 0x802000
	.quad	8396929                 # 0x802081
	.quad	129                     # 0x81
	.quad	0                       # 0x0
	.quad	8388736                 # 0x800080
	.quad	8388609                 # 0x800001
	.quad	1                       # 0x1
	.quad	8192                    # 0x2000
	.quad	8388608                 # 0x800000
	.quad	8396801                 # 0x802001
	.quad	128                     # 0x80
	.quad	8388608                 # 0x800000
	.quad	8193                    # 0x2001
	.quad	8320                    # 0x2080
	.quad	8388737                 # 0x800081
	.quad	1                       # 0x1
	.quad	8320                    # 0x2080
	.quad	8388736                 # 0x800080
	.quad	8192                    # 0x2000
	.quad	8396928                 # 0x802080
	.quad	8396929                 # 0x802081
	.quad	129                     # 0x81
	.quad	8388736                 # 0x800080
	.quad	8388609                 # 0x800001
	.quad	8396800                 # 0x802000
	.quad	8396929                 # 0x802081
	.quad	129                     # 0x81
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	8396800                 # 0x802000
	.quad	8320                    # 0x2080
	.quad	8388736                 # 0x800080
	.quad	8388737                 # 0x800081
	.quad	1                       # 0x1
	.quad	8396801                 # 0x802001
	.quad	8321                    # 0x2081
	.quad	8321                    # 0x2081
	.quad	128                     # 0x80
	.quad	8396929                 # 0x802081
	.quad	129                     # 0x81
	.quad	1                       # 0x1
	.quad	8192                    # 0x2000
	.quad	8388609                 # 0x800001
	.quad	8193                    # 0x2001
	.quad	8396928                 # 0x802080
	.quad	8388737                 # 0x800081
	.quad	8193                    # 0x2001
	.quad	8320                    # 0x2080
	.quad	8388608                 # 0x800000
	.quad	8396801                 # 0x802001
	.quad	128                     # 0x80
	.quad	8388608                 # 0x800000
	.quad	8192                    # 0x2000
	.quad	8396928                 # 0x802080
	.size	SB4, 512

	.type	SB2,@object             # @SB2
	.p2align	4
SB2:
	.quad	2148565024              # 0x80108020
	.quad	2147516416              # 0x80008000
	.quad	32768                   # 0x8000
	.quad	1081376                 # 0x108020
	.quad	1048576                 # 0x100000
	.quad	32                      # 0x20
	.quad	2148532256              # 0x80100020
	.quad	2147516448              # 0x80008020
	.quad	2147483680              # 0x80000020
	.quad	2148565024              # 0x80108020
	.quad	2148564992              # 0x80108000
	.quad	2147483648              # 0x80000000
	.quad	2147516416              # 0x80008000
	.quad	1048576                 # 0x100000
	.quad	32                      # 0x20
	.quad	2148532256              # 0x80100020
	.quad	1081344                 # 0x108000
	.quad	1048608                 # 0x100020
	.quad	2147516448              # 0x80008020
	.quad	0                       # 0x0
	.quad	2147483648              # 0x80000000
	.quad	32768                   # 0x8000
	.quad	1081376                 # 0x108020
	.quad	2148532224              # 0x80100000
	.quad	1048608                 # 0x100020
	.quad	2147483680              # 0x80000020
	.quad	0                       # 0x0
	.quad	1081344                 # 0x108000
	.quad	32800                   # 0x8020
	.quad	2148564992              # 0x80108000
	.quad	2148532224              # 0x80100000
	.quad	32800                   # 0x8020
	.quad	0                       # 0x0
	.quad	1081376                 # 0x108020
	.quad	2148532256              # 0x80100020
	.quad	1048576                 # 0x100000
	.quad	2147516448              # 0x80008020
	.quad	2148532224              # 0x80100000
	.quad	2148564992              # 0x80108000
	.quad	32768                   # 0x8000
	.quad	2148532224              # 0x80100000
	.quad	2147516416              # 0x80008000
	.quad	32                      # 0x20
	.quad	2148565024              # 0x80108020
	.quad	1081376                 # 0x108020
	.quad	32                      # 0x20
	.quad	32768                   # 0x8000
	.quad	2147483648              # 0x80000000
	.quad	32800                   # 0x8020
	.quad	2148564992              # 0x80108000
	.quad	1048576                 # 0x100000
	.quad	2147483680              # 0x80000020
	.quad	1048608                 # 0x100020
	.quad	2147516448              # 0x80008020
	.quad	2147483680              # 0x80000020
	.quad	1048608                 # 0x100020
	.quad	1081344                 # 0x108000
	.quad	0                       # 0x0
	.quad	2147516416              # 0x80008000
	.quad	32800                   # 0x8020
	.quad	2147483648              # 0x80000000
	.quad	2148532256              # 0x80100020
	.quad	2148565024              # 0x80108020
	.quad	1081344                 # 0x108000
	.size	SB2, 512

	.type	SB7,@object             # @SB7
	.p2align	4
SB7:
	.quad	2097152                 # 0x200000
	.quad	69206018                # 0x4200002
	.quad	67110914                # 0x4000802
	.quad	0                       # 0x0
	.quad	2048                    # 0x800
	.quad	67110914                # 0x4000802
	.quad	2099202                 # 0x200802
	.quad	69208064                # 0x4200800
	.quad	69208066                # 0x4200802
	.quad	2097152                 # 0x200000
	.quad	0                       # 0x0
	.quad	67108866                # 0x4000002
	.quad	2                       # 0x2
	.quad	67108864                # 0x4000000
	.quad	69206018                # 0x4200002
	.quad	2050                    # 0x802
	.quad	67110912                # 0x4000800
	.quad	2099202                 # 0x200802
	.quad	2097154                 # 0x200002
	.quad	67110912                # 0x4000800
	.quad	67108866                # 0x4000002
	.quad	69206016                # 0x4200000
	.quad	69208064                # 0x4200800
	.quad	2097154                 # 0x200002
	.quad	69206016                # 0x4200000
	.quad	2048                    # 0x800
	.quad	2050                    # 0x802
	.quad	69208066                # 0x4200802
	.quad	2099200                 # 0x200800
	.quad	2                       # 0x2
	.quad	67108864                # 0x4000000
	.quad	2099200                 # 0x200800
	.quad	67108864                # 0x4000000
	.quad	2099200                 # 0x200800
	.quad	2097152                 # 0x200000
	.quad	67110914                # 0x4000802
	.quad	67110914                # 0x4000802
	.quad	69206018                # 0x4200002
	.quad	69206018                # 0x4200002
	.quad	2                       # 0x2
	.quad	2097154                 # 0x200002
	.quad	67108864                # 0x4000000
	.quad	67110912                # 0x4000800
	.quad	2097152                 # 0x200000
	.quad	69208064                # 0x4200800
	.quad	2050                    # 0x802
	.quad	2099202                 # 0x200802
	.quad	69208064                # 0x4200800
	.quad	2050                    # 0x802
	.quad	67108866                # 0x4000002
	.quad	69208066                # 0x4200802
	.quad	69206016                # 0x4200000
	.quad	2099200                 # 0x200800
	.quad	0                       # 0x0
	.quad	2                       # 0x2
	.quad	69208066                # 0x4200802
	.quad	0                       # 0x0
	.quad	2099202                 # 0x200802
	.quad	69206016                # 0x4200000
	.quad	2048                    # 0x800
	.quad	67108866                # 0x4000002
	.quad	67110912                # 0x4000800
	.quad	2048                    # 0x800
	.quad	2097154                 # 0x200002
	.size	SB7, 512

	.type	SB5,@object             # @SB5
	.p2align	4
SB5:
	.quad	256                     # 0x100
	.quad	34078976                # 0x2080100
	.quad	34078720                # 0x2080000
	.quad	1107296512              # 0x42000100
	.quad	524288                  # 0x80000
	.quad	256                     # 0x100
	.quad	1073741824              # 0x40000000
	.quad	34078720                # 0x2080000
	.quad	1074266368              # 0x40080100
	.quad	524288                  # 0x80000
	.quad	33554688                # 0x2000100
	.quad	1074266368              # 0x40080100
	.quad	1107296512              # 0x42000100
	.quad	1107820544              # 0x42080000
	.quad	524544                  # 0x80100
	.quad	1073741824              # 0x40000000
	.quad	33554432                # 0x2000000
	.quad	1074266112              # 0x40080000
	.quad	1074266112              # 0x40080000
	.quad	0                       # 0x0
	.quad	1073742080              # 0x40000100
	.quad	1107820800              # 0x42080100
	.quad	1107820800              # 0x42080100
	.quad	33554688                # 0x2000100
	.quad	1107820544              # 0x42080000
	.quad	1073742080              # 0x40000100
	.quad	0                       # 0x0
	.quad	1107296256              # 0x42000000
	.quad	34078976                # 0x2080100
	.quad	33554432                # 0x2000000
	.quad	1107296256              # 0x42000000
	.quad	524544                  # 0x80100
	.quad	524288                  # 0x80000
	.quad	1107296512              # 0x42000100
	.quad	256                     # 0x100
	.quad	33554432                # 0x2000000
	.quad	1073741824              # 0x40000000
	.quad	34078720                # 0x2080000
	.quad	1107296512              # 0x42000100
	.quad	1074266368              # 0x40080100
	.quad	33554688                # 0x2000100
	.quad	1073741824              # 0x40000000
	.quad	1107820544              # 0x42080000
	.quad	34078976                # 0x2080100
	.quad	1074266368              # 0x40080100
	.quad	256                     # 0x100
	.quad	33554432                # 0x2000000
	.quad	1107820544              # 0x42080000
	.quad	1107820800              # 0x42080100
	.quad	524544                  # 0x80100
	.quad	1107296256              # 0x42000000
	.quad	1107820800              # 0x42080100
	.quad	34078720                # 0x2080000
	.quad	0                       # 0x0
	.quad	1074266112              # 0x40080000
	.quad	1107296256              # 0x42000000
	.quad	524544                  # 0x80100
	.quad	33554688                # 0x2000100
	.quad	1073742080              # 0x40000100
	.quad	524288                  # 0x80000
	.quad	0                       # 0x0
	.quad	1074266112              # 0x40080000
	.quad	34078976                # 0x2080100
	.quad	1073742080              # 0x40000100
	.size	SB5, 512

	.type	SB3,@object             # @SB3
	.p2align	4
SB3:
	.quad	520                     # 0x208
	.quad	134349312               # 0x8020200
	.quad	0                       # 0x0
	.quad	134348808               # 0x8020008
	.quad	134218240               # 0x8000200
	.quad	0                       # 0x0
	.quad	131592                  # 0x20208
	.quad	134218240               # 0x8000200
	.quad	131080                  # 0x20008
	.quad	134217736               # 0x8000008
	.quad	134217736               # 0x8000008
	.quad	131072                  # 0x20000
	.quad	134349320               # 0x8020208
	.quad	131080                  # 0x20008
	.quad	134348800               # 0x8020000
	.quad	520                     # 0x208
	.quad	134217728               # 0x8000000
	.quad	8                       # 0x8
	.quad	134349312               # 0x8020200
	.quad	512                     # 0x200
	.quad	131584                  # 0x20200
	.quad	134348800               # 0x8020000
	.quad	134348808               # 0x8020008
	.quad	131592                  # 0x20208
	.quad	134218248               # 0x8000208
	.quad	131584                  # 0x20200
	.quad	131072                  # 0x20000
	.quad	134218248               # 0x8000208
	.quad	8                       # 0x8
	.quad	134349320               # 0x8020208
	.quad	512                     # 0x200
	.quad	134217728               # 0x8000000
	.quad	134349312               # 0x8020200
	.quad	134217728               # 0x8000000
	.quad	131080                  # 0x20008
	.quad	520                     # 0x208
	.quad	131072                  # 0x20000
	.quad	134349312               # 0x8020200
	.quad	134218240               # 0x8000200
	.quad	0                       # 0x0
	.quad	512                     # 0x200
	.quad	131080                  # 0x20008
	.quad	134349320               # 0x8020208
	.quad	134218240               # 0x8000200
	.quad	134217736               # 0x8000008
	.quad	512                     # 0x200
	.quad	0                       # 0x0
	.quad	134348808               # 0x8020008
	.quad	134218248               # 0x8000208
	.quad	131072                  # 0x20000
	.quad	134217728               # 0x8000000
	.quad	134349320               # 0x8020208
	.quad	8                       # 0x8
	.quad	131592                  # 0x20208
	.quad	131584                  # 0x20200
	.quad	134217736               # 0x8000008
	.quad	134348800               # 0x8020000
	.quad	134218248               # 0x8000208
	.quad	520                     # 0x208
	.quad	134348800               # 0x8020000
	.quad	131592                  # 0x20208
	.quad	8                       # 0x8
	.quad	134348808               # 0x8020008
	.quad	131584                  # 0x20200
	.size	SB3, 512

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n Triple-DES Monte Carlo Test (ECB mode) - "
	.size	.L.str, 44

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" Test %d, key size = %3d bits: "
	.size	.L.str.3, 32

	.type	DES3_keys,@object       # @DES3_keys
	.data
	.p2align	4
DES3_keys:
	.ascii	"\001#Eg\211\253\315\357"
	.ascii	"#Eg\211\253\315\357\001"
	.ascii	"Eg\211\253\315\357\001#"
	.size	DES3_keys, 24

	.type	DES3_enc_test,@object   # @DES3_enc_test
	.p2align	4
DES3_enc_test:
	.ascii	"j*\031\364\036\312\205K"
	.ascii	"\003\346\237[\372X\353B"
	.ascii	"\335\027\350\270\2647\3222"
	.size	DES3_enc_test, 24

	.type	DES3_dec_test,@object   # @DES3_dec_test
	.p2align	4
DES3_dec_test:
	.ascii	"\315\326O/\224'\301]"
	.ascii	"i\226\310\372G\242\253\353"
	.ascii	"\203%9vD\t\032\n"
	.size	DES3_dec_test, 24

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr:
	.asciz	"passed."
	.size	.Lstr, 8

	.type	.Lstr.1,@object         # @str.1
.Lstr.1:
	.asciz	"failed!"
	.size	.Lstr.1, 8

	.type	.Lstr.2,@object         # @str.2
.Lstr.2:
	.asciz	"decryption\n"
	.size	.Lstr.2, 12

	.type	.Lstr.3,@object         # @str.3
.Lstr.3:
	.asciz	"encryption\n"
	.size	.Lstr.3, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
