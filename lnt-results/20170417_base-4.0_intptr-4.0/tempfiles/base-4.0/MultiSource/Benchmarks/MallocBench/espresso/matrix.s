	.text
	.file	"matrix.bc"
	.globl	sm_alloc
	.p2align	4, 0x90
	.type	sm_alloc,@function
sm_alloc:                               # @sm_alloc
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$88, %edi
	callq	malloc
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	movl	$0, 24(%rax)
	movl	$0, 8(%rax)
	movq	$0, 80(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rax)
	movl	$0, 48(%rax)
	movups	%xmm0, 56(%rax)
	movl	$0, 72(%rax)
	popq	%rcx
	retq
.Lfunc_end0:
	.size	sm_alloc, .Lfunc_end0-sm_alloc
	.cfi_endproc

	.globl	sm_alloc_size
	.p2align	4, 0x90
	.type	sm_alloc_size,@function
sm_alloc_size:                          # @sm_alloc_size
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 32
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movl	%edi, %ebp
	movl	$88, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	$0, (%rbx)
	movq	$0, 16(%rbx)
	movl	$0, 24(%rbx)
	movl	$0, 8(%rbx)
	movq	$0, 80(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movl	$0, 48(%rbx)
	movups	%xmm0, 56(%rbx)
	movl	$0, 72(%rbx)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%r14d, %edx
	callq	sm_resize
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	sm_alloc_size, .Lfunc_end1-sm_alloc_size
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.zero	16
	.text
	.globl	sm_free
	.p2align	4, 0x90
	.type	sm_free,@function
sm_free:                                # @sm_free
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph31
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rdi), %rbx
	xorl	%eax, %eax
	callq	sm_row_free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB2_1
.LBB2_2:                                # %._crit_edge32
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rdi), %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rdi)
	xorl	%eax, %eax
	callq	sm_col_free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB2_3
.LBB2_4:                                # %._crit_edge
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_6
# BB#5:
	callq	free
	movq	$0, (%r14)
.LBB2_6:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_8
# BB#7:
	callq	free
.LBB2_8:
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end2:
	.size	sm_free, .Lfunc_end2-sm_free
	.cfi_endproc

	.globl	sm_dup
	.p2align	4, 0x90
	.type	sm_dup,@function
sm_dup:                                 # @sm_dup
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	$88, %edi
	callq	malloc
	movq	%rax, %r15
	movq	$0, (%r15)
	movq	$0, 16(%r15)
	movl	$0, 24(%r15)
	movl	$0, 8(%r15)
	movq	$0, 80(%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r15)
	movl	$0, 48(%r15)
	movups	%xmm0, 56(%r15)
	movl	$0, 72(%r15)
	movq	40(%r14), %rax
	testq	%rax, %rax
	je	.LBB3_6
# BB#1:
	movl	(%rax), %esi
	movq	64(%r14), %rax
	movl	(%rax), %edx
	movq	%r15, %rdi
	callq	sm_resize
	jmp	.LBB3_2
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph25
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	16(%r14), %rbx
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph
                                        #   in Loop: Header=BB3_5 Depth=2
	movl	(%rbx), %esi
	movl	4(%rbx), %edx
	movq	%r15, %rdi
	callq	sm_insert
	movq	24(%rbx), %rbx
.LBB3_5:                                # %.lr.ph
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbx, %rbx
	jne	.LBB3_4
.LBB3_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_5 Depth 2
	movq	32(%r14), %r14
	testq	%r14, %r14
	jne	.LBB3_3
.LBB3_6:                                # %.loopexit
	movq	%r15, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	sm_dup, .Lfunc_end3-sm_dup
	.cfi_endproc

	.globl	sm_resize
	.p2align	4, 0x90
	.type	sm_resize,@function
sm_resize:                              # @sm_resize
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 48
.Lcfi23:
	.cfi_offset %rbx, -40
.Lcfi24:
	.cfi_offset %r12, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movl	8(%rbx), %r12d
	cmpl	%r15d, %r12d
	jg	.LBB4_12
# BB#1:
	leal	(%r12,%r12), %eax
	incl	%r15d
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_3
# BB#2:
	movslq	%r15d, %rsi
	shlq	$3, %rsi
	callq	realloc
	movl	8(%rbx), %r12d
	jmp	.LBB4_4
.LBB4_3:
	movslq	%r15d, %rdi
	shlq	$3, %rdi
	callq	malloc
.LBB4_4:
	movq	%rax, (%rbx)
	cmpl	%r15d, %r12d
	jge	.LBB4_11
# BB#5:                                 # %.lr.ph45.preheader
	movslq	%r12d, %rdi
	movslq	%r15d, %rcx
	movq	$0, (%rax,%rdi,8)
	leaq	1(%rdi), %rax
	cmpq	%rcx, %rax
	jge	.LBB4_11
# BB#6:                                 # %.lr.ph45..lr.ph45_crit_edge.preheader
	leal	7(%r15), %esi
	subl	%r12d, %esi
	leaq	-2(%rcx), %rdx
	subq	%rdi, %rdx
	andq	$7, %rsi
	je	.LBB4_9
# BB#7:                                 # %.lr.ph45..lr.ph45_crit_edge.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB4_8:                                # %.lr.ph45..lr.ph45_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	$0, (%rdi,%rax,8)
	incq	%rax
	incq	%rsi
	jne	.LBB4_8
.LBB4_9:                                # %.lr.ph45..lr.ph45_crit_edge.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB4_11
	.p2align	4, 0x90
.LBB4_10:                               # %.lr.ph45..lr.ph45_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movq	$0, (%rdx,%rax,8)
	movq	(%rbx), %rdx
	movq	$0, 8(%rdx,%rax,8)
	movq	(%rbx), %rdx
	movq	$0, 16(%rdx,%rax,8)
	movq	(%rbx), %rdx
	movq	$0, 24(%rdx,%rax,8)
	movq	(%rbx), %rdx
	movq	$0, 32(%rdx,%rax,8)
	movq	(%rbx), %rdx
	movq	$0, 40(%rdx,%rax,8)
	movq	(%rbx), %rdx
	movq	$0, 48(%rdx,%rax,8)
	movq	(%rbx), %rdx
	movq	$0, 56(%rdx,%rax,8)
	addq	$8, %rax
	cmpq	%rcx, %rax
	jl	.LBB4_10
.LBB4_11:                               # %._crit_edge46
	movl	%r15d, 8(%rbx)
.LBB4_12:
	movl	24(%rbx), %r15d
	cmpl	%r14d, %r15d
	jg	.LBB4_24
# BB#13:
	leal	(%r15,%r15), %eax
	incl	%r14d
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_15
# BB#14:
	movslq	%r14d, %rsi
	shlq	$3, %rsi
	callq	realloc
	movl	24(%rbx), %r15d
	jmp	.LBB4_16
.LBB4_15:
	movslq	%r14d, %rdi
	shlq	$3, %rdi
	callq	malloc
.LBB4_16:
	movq	%rax, 16(%rbx)
	cmpl	%r14d, %r15d
	jge	.LBB4_23
# BB#17:                                # %.lr.ph.preheader
	movslq	%r15d, %rdi
	movslq	%r14d, %rcx
	movq	$0, (%rax,%rdi,8)
	leaq	1(%rdi), %rax
	cmpq	%rcx, %rax
	jge	.LBB4_23
# BB#18:                                # %.lr.ph..lr.ph_crit_edge.preheader
	leal	7(%r14), %esi
	subl	%r15d, %esi
	leaq	-2(%rcx), %rdx
	subq	%rdi, %rdx
	andq	$7, %rsi
	je	.LBB4_21
# BB#19:                                # %.lr.ph..lr.ph_crit_edge.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB4_20:                               # %.lr.ph..lr.ph_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdi
	movq	$0, (%rdi,%rax,8)
	incq	%rax
	incq	%rsi
	jne	.LBB4_20
.LBB4_21:                               # %.lr.ph..lr.ph_crit_edge.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB4_23
	.p2align	4, 0x90
.LBB4_22:                               # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdx
	movq	$0, (%rdx,%rax,8)
	movq	16(%rbx), %rdx
	movq	$0, 8(%rdx,%rax,8)
	movq	16(%rbx), %rdx
	movq	$0, 16(%rdx,%rax,8)
	movq	16(%rbx), %rdx
	movq	$0, 24(%rdx,%rax,8)
	movq	16(%rbx), %rdx
	movq	$0, 32(%rdx,%rax,8)
	movq	16(%rbx), %rdx
	movq	$0, 40(%rdx,%rax,8)
	movq	16(%rbx), %rdx
	movq	$0, 48(%rdx,%rax,8)
	movq	16(%rbx), %rdx
	movq	$0, 56(%rdx,%rax,8)
	addq	$8, %rax
	cmpq	%rcx, %rax
	jl	.LBB4_22
.LBB4_23:                               # %._crit_edge
	movl	%r14d, 24(%rbx)
.LBB4_24:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	sm_resize, .Lfunc_end4-sm_resize
	.cfi_endproc

	.globl	sm_insert
	.p2align	4, 0x90
	.type	sm_insert,@function
sm_insert:                              # @sm_insert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 64
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movl	%esi, %r14d
	movq	%rdi, %rbx
	cmpl	%r14d, 8(%rbx)
	jle	.LBB5_2
# BB#1:
	cmpl	%r13d, 24(%rbx)
	jg	.LBB5_3
.LBB5_2:
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movl	%r13d, %edx
	callq	sm_resize
.LBB5_3:
	movq	(%rbx), %rax
	movslq	%r14d, %rbp
	movq	(%rax,%rbp,8), %r15
	testq	%r15, %r15
	jne	.LBB5_15
# BB#4:
	xorl	%eax, %eax
	callq	sm_row_alloc
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	%r15, (%rax,%rbp,8)
	movl	%r14d, (%r15)
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.LBB5_5
# BB#6:
	cmpl	%r14d, (%rax)
	jge	.LBB5_8
# BB#7:
	movl	%r14d, (%r15)
	movq	%r15, 32(%rax)
	movq	%rax, 40(%r15)
	movq	%r15, 40(%rbx)
	movq	$0, 32(%r15)
	jmp	.LBB5_14
.LBB5_5:
	movl	%r14d, (%r15)
	movq	%r15, 32(%rbx)
	movq	%r15, 40(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r15)
	jmp	.LBB5_14
.LBB5_8:
	movq	32(%rbx), %rax
	movl	(%rax), %ecx
	cmpl	%r14d, %ecx
	jle	.LBB5_9
# BB#53:
	movl	%r14d, (%r15)
	movq	%r15, 40(%rax)
	movq	%rax, 32(%r15)
	movq	%r15, 32(%rbx)
	movq	$0, 40(%r15)
	jmp	.LBB5_14
.LBB5_9:                                # %.preheader266
	jge	.LBB5_11
	.p2align	4, 0x90
.LBB5_10:                               # %.lr.ph283
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rax), %rax
	movl	(%rax), %ecx
	cmpl	%r14d, %ecx
	jl	.LBB5_10
.LBB5_11:                               # %._crit_edge284
	cmpl	%r14d, %ecx
	jle	.LBB5_12
# BB#13:
	movl	%r14d, (%r15)
	movq	40(%rax), %rax
	movq	32(%rax), %rcx
	movq	%r15, 40(%rcx)
	movq	%rcx, 32(%r15)
	movq	%r15, 32(%rax)
	movq	%rax, 40(%r15)
.LBB5_14:
	incl	48(%rbx)
.LBB5_15:
	movq	16(%rbx), %rax
	movslq	%r13d, %rbp
	movq	(%rax,%rbp,8), %r12
	testq	%r12, %r12
	jne	.LBB5_27
# BB#16:
	xorl	%eax, %eax
	callq	sm_col_alloc
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	%r12, (%rax,%rbp,8)
	movl	%r13d, (%r12)
	movq	64(%rbx), %rax
	testq	%rax, %rax
	je	.LBB5_17
# BB#18:
	cmpl	%r13d, (%rax)
	jge	.LBB5_20
# BB#19:
	movl	%r13d, (%r12)
	movq	%r12, 32(%rax)
	movq	%rax, 40(%r12)
	movq	%r12, 64(%rbx)
	movq	$0, 32(%r12)
	jmp	.LBB5_26
.LBB5_17:
	movl	%r13d, (%r12)
	movq	%r12, 56(%rbx)
	movq	%r12, 64(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	jmp	.LBB5_26
.LBB5_20:
	movq	56(%rbx), %rax
	movl	(%rax), %ecx
	cmpl	%r13d, %ecx
	jle	.LBB5_21
# BB#54:
	movl	%r13d, (%r12)
	movq	%r12, 40(%rax)
	movq	%rax, 32(%r12)
	movq	%r12, 56(%rbx)
	movq	$0, 40(%r12)
	jmp	.LBB5_26
.LBB5_21:                               # %.preheader265
	jge	.LBB5_23
	.p2align	4, 0x90
.LBB5_22:                               # %.lr.ph278
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rax), %rax
	movl	(%rax), %ecx
	cmpl	%r13d, %ecx
	jl	.LBB5_22
.LBB5_23:                               # %._crit_edge279
	cmpl	%r13d, %ecx
	jle	.LBB5_24
# BB#25:
	movl	%r13d, (%r12)
	movq	40(%rax), %rax
	movq	32(%rax), %rcx
	movq	%r12, 40(%rcx)
	movq	%rcx, 32(%r12)
	movq	%r12, 32(%rax)
	movq	%rax, 40(%r12)
.LBB5_26:
	incl	72(%rbx)
.LBB5_27:
	movl	$48, %edi
	callq	malloc
	movq	$0, 40(%rax)
	movq	24(%r15), %rcx
	testq	%rcx, %rcx
	je	.LBB5_28
# BB#30:
	cmpl	%r13d, 4(%rcx)
	jge	.LBB5_32
# BB#31:
	movl	%r13d, 4(%rax)
	movq	%rax, 24(%rcx)
	movq	%rcx, 32(%rax)
	movq	%rax, 24(%r15)
	xorl	%ecx, %ecx
	movl	$24, %edx
	jmp	.LBB5_38
.LBB5_28:
	movl	%r13d, 4(%rax)
	movq	%rax, 16(%r15)
	movq	%rax, 24(%r15)
	movq	$0, 24(%rax)
	xorl	%ecx, %ecx
	jmp	.LBB5_37
.LBB5_32:
	movq	16(%r15), %rbx
	movl	4(%rbx), %ecx
	cmpl	%r13d, %ecx
	jle	.LBB5_33
# BB#55:
	movl	%r13d, 4(%rax)
	movq	%rax, 32(%rbx)
	movq	%rbx, 24(%rax)
	movq	%rax, 16(%r15)
	xorl	%ecx, %ecx
	jmp	.LBB5_37
.LBB5_33:                               # %.preheader264
	jge	.LBB5_35
	.p2align	4, 0x90
.LBB5_34:                               # %.lr.ph273
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rbx), %rbx
	movl	4(%rbx), %ecx
	cmpl	%r13d, %ecx
	jl	.LBB5_34
.LBB5_35:                               # %._crit_edge274
	cmpl	%r13d, %ecx
	jle	.LBB5_39
# BB#36:
	movl	%r13d, 4(%rax)
	movq	32(%rbx), %rcx
	movq	24(%rcx), %rdx
	movq	%rax, 32(%rdx)
	movq	%rdx, 24(%rax)
	movq	%rax, 24(%rcx)
.LBB5_37:                               # %.thread
	movl	$32, %edx
.LBB5_38:                               # %.thread
	movq	%rcx, (%rax,%rdx)
	incl	4(%r15)
.LBB5_40:
	movq	24(%r12), %rcx
	testq	%rcx, %rcx
	je	.LBB5_41
# BB#43:
	cmpl	%r14d, (%rcx)
	jge	.LBB5_45
# BB#44:
	movl	%r14d, (%rax)
	movq	%rax, 8(%rcx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%r12)
	movq	$0, 8(%rax)
	jmp	.LBB5_42
.LBB5_41:
	movl	%r14d, (%rax)
	movq	%rax, 16(%r12)
	movq	%rax, 24(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	jmp	.LBB5_42
.LBB5_45:
	movq	16(%r12), %rbx
	movl	(%rbx), %ecx
	cmpl	%r14d, %ecx
	jle	.LBB5_46
# BB#56:
	movl	%r14d, (%rax)
	movq	%rax, 16(%rbx)
	movq	%rbx, 8(%rax)
	movq	%rax, 16(%r12)
	movq	$0, 16(%rax)
	jmp	.LBB5_42
.LBB5_46:                               # %.preheader
	jge	.LBB5_48
	.p2align	4, 0x90
.LBB5_47:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rbx
	movl	(%rbx), %ecx
	cmpl	%r14d, %ecx
	jl	.LBB5_47
.LBB5_48:                               # %._crit_edge
	cmpl	%r14d, %ecx
	jle	.LBB5_52
# BB#49:
	movl	%r14d, (%rax)
	movq	16(%rbx), %rcx
	movq	8(%rcx), %rdx
	movq	%rax, 16(%rdx)
	movq	%rdx, 8(%rax)
	movq	%rax, 8(%rcx)
	movq	%rcx, 16(%rax)
.LBB5_42:
	incl	4(%r12)
	movq	%rax, %rbx
.LBB5_52:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_39:
	cmpq	%rax, %rbx
	je	.LBB5_40
# BB#50:
	testq	%rax, %rax
	je	.LBB5_52
# BB#51:
	movq	%rax, %rdi
	callq	free
	jmp	.LBB5_52
.LBB5_12:
	movq	%rax, %r15
	jmp	.LBB5_15
.LBB5_24:
	movq	%rax, %r12
	jmp	.LBB5_27
.Lfunc_end5:
	.size	sm_insert, .Lfunc_end5-sm_insert
	.cfi_endproc

	.globl	sm_find
	.p2align	4, 0x90
	.type	sm_find,@function
sm_find:                                # @sm_find
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	testl	%esi, %esi
	js	.LBB6_8
# BB#1:
	cmpl	%esi, 8(%rax)
	jle	.LBB6_8
# BB#2:
	testl	%edx, %edx
	js	.LBB6_8
# BB#3:
	movq	(%rax), %rcx
	movslq	%esi, %rdi
	movq	(%rcx,%rdi,8), %rdi
	testq	%rdi, %rdi
	je	.LBB6_8
# BB#4:
	cmpl	%edx, 24(%rax)
	jle	.LBB6_8
# BB#5:
	movq	16(%rax), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rcx
	testq	%rcx, %rcx
	je	.LBB6_8
# BB#6:
	movl	4(%rdi), %eax
	cmpl	4(%rcx), %eax
	jge	.LBB6_7
# BB#9:
	xorl	%eax, %eax
	movl	%edx, %esi
	jmp	sm_row_find             # TAILCALL
.LBB6_8:                                # %.thread
	xorl	%eax, %eax
	retq
.LBB6_7:
	xorl	%eax, %eax
	movq	%rcx, %rdi
	jmp	sm_col_find             # TAILCALL
.Lfunc_end6:
	.size	sm_find, .Lfunc_end6-sm_find
	.cfi_endproc

	.globl	sm_remove
	.p2align	4, 0x90
	.type	sm_remove,@function
sm_remove:                              # @sm_remove
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 16
.Lcfi41:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testl	%esi, %esi
	js	.LBB7_9
# BB#1:
	cmpl	%esi, 8(%rbx)
	jle	.LBB7_9
# BB#2:
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.LBB7_10
# BB#3:
	movq	(%rbx), %rcx
	movslq	%esi, %rdi
	movq	(%rcx,%rdi,8), %rdi
	testq	%rdi, %rdi
	je	.LBB7_10
# BB#4:
	cmpl	%edx, 24(%rbx)
	jle	.LBB7_9
# BB#5:
	movq	16(%rbx), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rcx
	testq	%rcx, %rcx
	je	.LBB7_9
# BB#6:
	movl	4(%rdi), %eax
	cmpl	4(%rcx), %eax
	jge	.LBB7_13
# BB#7:
	xorl	%eax, %eax
	movl	%edx, %esi
	callq	sm_row_find
	jmp	.LBB7_10
.LBB7_9:
	xorl	%eax, %eax
.LBB7_10:                               # %sm_find.exit
	movq	%rbx, %rdi
	movq	%rax, %rsi
	popq	%rbx
	jmp	sm_remove_element       # TAILCALL
.LBB7_13:
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sm_col_find
	jmp	.LBB7_10
.Lfunc_end7:
	.size	sm_remove, .Lfunc_end7-sm_remove
	.cfi_endproc

	.globl	sm_remove_element
	.p2align	4, 0x90
	.type	sm_remove_element,@function
sm_remove_element:                      # @sm_remove_element
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -24
.Lcfi46:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB8_4
# BB#1:
	movslq	(%rbx), %rsi
	testq	%rsi, %rsi
	js	.LBB8_6
# BB#2:
	cmpl	8(%r14), %esi
	jge	.LBB8_6
# BB#3:
	movq	(%r14), %rax
	movq	(%rax,%rsi,8), %rax
	jmp	.LBB8_7
.LBB8_6:
	xorl	%eax, %eax
.LBB8_7:
	movq	24(%rbx), %rcx
	movq	32(%rbx), %rdx
	leaq	16(%rax), %rdi
	testq	%rdx, %rdx
	leaq	24(%rdx), %rdx
	cmoveq	%rdi, %rdx
	movq	%rcx, (%rdx)
	movq	24(%rbx), %rcx
	movq	32(%rbx), %rdx
	leaq	24(%rax), %rdi
	testq	%rcx, %rcx
	leaq	32(%rcx), %rcx
	cmoveq	%rdi, %rcx
	movq	%rdx, (%rcx)
	decl	4(%rax)
	cmpq	$0, 16(%rax)
	jne	.LBB8_9
# BB#8:
	movq	%r14, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	sm_delrow
.LBB8_9:
	movslq	4(%rbx), %rsi
	testq	%rsi, %rsi
	js	.LBB8_13
# BB#10:
	cmpl	24(%r14), %esi
	jge	.LBB8_13
# BB#11:
	movq	16(%r14), %rax
	movq	(%rax,%rsi,8), %rax
	jmp	.LBB8_14
.LBB8_13:
	xorl	%eax, %eax
.LBB8_14:
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	16(%rax), %rdi
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	cmoveq	%rdi, %rdx
	movq	%rcx, (%rdx)
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	24(%rax), %rdi
	testq	%rcx, %rcx
	leaq	16(%rcx), %rcx
	cmoveq	%rdi, %rcx
	movq	%rdx, (%rcx)
	decl	4(%rax)
	cmpq	$0, 16(%rax)
	jne	.LBB8_16
# BB#15:
	movq	%r14, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	sm_delcol
.LBB8_16:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.LBB8_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	sm_remove_element, .Lfunc_end8-sm_remove_element
	.cfi_endproc

	.globl	sm_delrow
	.p2align	4, 0x90
	.type	sm_delrow,@function
sm_delrow:                              # @sm_delrow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 64
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	testl	%esi, %esi
	js	.LBB9_14
# BB#1:
	cmpl	%esi, 8(%r15)
	jle	.LBB9_14
# BB#2:
	movq	(%r15), %rax
	movslq	%esi, %r13
	movq	(%rax,%r13,8), %r14
	testq	%r14, %r14
	je	.LBB9_14
# BB#3:
	leaq	16(%r14), %r12
	movq	16(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB9_13
	.p2align	4, 0x90
.LBB9_4:                                # =>This Inner Loop Header: Depth=1
	movq	24(%rsi), %rbp
	movslq	4(%rsi), %rax
	testq	%rax, %rax
	js	.LBB9_8
# BB#5:                                 #   in Loop: Header=BB9_4 Depth=1
	cmpl	24(%r15), %eax
	jge	.LBB9_8
# BB#6:                                 #   in Loop: Header=BB9_4 Depth=1
	movq	16(%r15), %rcx
	movq	(%rcx,%rax,8), %rbx
	jmp	.LBB9_9
	.p2align	4, 0x90
.LBB9_8:                                #   in Loop: Header=BB9_4 Depth=1
	xorl	%ebx, %ebx
.LBB9_9:                                #   in Loop: Header=BB9_4 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sm_col_remove_element
	cmpq	$0, 16(%rbx)
	jne	.LBB9_11
# BB#10:                                #   in Loop: Header=BB9_4 Depth=1
	movl	(%rbx), %esi
	movq	%r15, %rdi
	callq	sm_delcol
.LBB9_11:                               # %.backedge
                                        #   in Loop: Header=BB9_4 Depth=1
	testq	%rbp, %rbp
	movq	%rbp, %rsi
	jne	.LBB9_4
# BB#12:                                # %._crit_edge.loopexit
	movq	(%r15), %rax
.LBB9_13:                               # %._crit_edge
	movq	$0, (%rax,%r13,8)
	movq	32(%r14), %rax
	movq	40(%r14), %rcx
	leaq	32(%r15), %rdx
	testq	%rcx, %rcx
	leaq	32(%rcx), %rcx
	cmoveq	%rdx, %rcx
	movq	%rax, (%rcx)
	movq	32(%r14), %rax
	movq	40(%r14), %rcx
	leaq	40(%r15), %rdx
	testq	%rax, %rax
	leaq	40(%rax), %rax
	cmoveq	%rdx, %rax
	movq	%rcx, (%rax)
	decl	48(%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	xorl	%eax, %eax
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sm_row_free             # TAILCALL
.LBB9_14:                               # %.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	sm_delrow, .Lfunc_end9-sm_delrow
	.cfi_endproc

	.globl	sm_delcol
	.p2align	4, 0x90
	.type	sm_delcol,@function
sm_delcol:                              # @sm_delcol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi64:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 64
.Lcfi67:
	.cfi_offset %rbx, -56
.Lcfi68:
	.cfi_offset %r12, -48
.Lcfi69:
	.cfi_offset %r13, -40
.Lcfi70:
	.cfi_offset %r14, -32
.Lcfi71:
	.cfi_offset %r15, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	testl	%esi, %esi
	js	.LBB10_14
# BB#1:
	cmpl	%esi, 24(%r15)
	jle	.LBB10_14
# BB#2:
	movq	16(%r15), %rax
	movslq	%esi, %r13
	movq	(%rax,%r13,8), %r14
	testq	%r14, %r14
	je	.LBB10_14
# BB#3:
	leaq	16(%r14), %r12
	movq	16(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB10_13
	.p2align	4, 0x90
.LBB10_4:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rsi), %rbp
	movslq	(%rsi), %rax
	testq	%rax, %rax
	js	.LBB10_8
# BB#5:                                 #   in Loop: Header=BB10_4 Depth=1
	cmpl	8(%r15), %eax
	jge	.LBB10_8
# BB#6:                                 #   in Loop: Header=BB10_4 Depth=1
	movq	(%r15), %rcx
	movq	(%rcx,%rax,8), %rbx
	jmp	.LBB10_9
	.p2align	4, 0x90
.LBB10_8:                               #   in Loop: Header=BB10_4 Depth=1
	xorl	%ebx, %ebx
.LBB10_9:                               #   in Loop: Header=BB10_4 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sm_row_remove_element
	cmpq	$0, 16(%rbx)
	jne	.LBB10_11
# BB#10:                                #   in Loop: Header=BB10_4 Depth=1
	movl	(%rbx), %esi
	movq	%r15, %rdi
	callq	sm_delrow
.LBB10_11:                              # %.backedge
                                        #   in Loop: Header=BB10_4 Depth=1
	testq	%rbp, %rbp
	movq	%rbp, %rsi
	jne	.LBB10_4
# BB#12:                                # %._crit_edge.loopexit
	movq	16(%r15), %rax
.LBB10_13:                              # %._crit_edge
	movq	$0, (%rax,%r13,8)
	movq	32(%r14), %rax
	movq	40(%r14), %rcx
	leaq	56(%r15), %rdx
	testq	%rcx, %rcx
	leaq	32(%rcx), %rcx
	cmoveq	%rdx, %rcx
	movq	%rax, (%rcx)
	movq	32(%r14), %rax
	movq	40(%r14), %rcx
	leaq	64(%r15), %rdx
	testq	%rax, %rax
	leaq	40(%rax), %rax
	cmoveq	%rdx, %rax
	movq	%rcx, (%rax)
	decl	72(%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	xorl	%eax, %eax
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sm_col_free             # TAILCALL
.LBB10_14:                              # %.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	sm_delcol, .Lfunc_end10-sm_delcol
	.cfi_endproc

	.globl	sm_copy_row
	.p2align	4, 0x90
	.type	sm_copy_row,@function
sm_copy_row:                            # @sm_copy_row
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 32
.Lcfi76:
	.cfi_offset %rbx, -32
.Lcfi77:
	.cfi_offset %r14, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	16(%rdx), %rbp
	testq	%rbp, %rbp
	je	.LBB11_3
	.p2align	4, 0x90
.LBB11_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%rbp), %edx
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	sm_insert
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB11_1
.LBB11_3:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end11:
	.size	sm_copy_row, .Lfunc_end11-sm_copy_row
	.cfi_endproc

	.globl	sm_copy_col
	.p2align	4, 0x90
	.type	sm_copy_col,@function
sm_copy_col:                            # @sm_copy_col
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 32
.Lcfi82:
	.cfi_offset %rbx, -32
.Lcfi83:
	.cfi_offset %r14, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	16(%rdx), %rbp
	testq	%rbp, %rbp
	je	.LBB12_3
	.p2align	4, 0x90
.LBB12_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %edx
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	sm_insert
	movq	8(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB12_1
.LBB12_3:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end12:
	.size	sm_copy_col, .Lfunc_end12-sm_copy_col
	.cfi_endproc

	.globl	sm_longest_row
	.p2align	4, 0x90
	.type	sm_longest_row,@function
sm_longest_row:                         # @sm_longest_row
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rcx
	xorl	%edx, %edx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB13_3
	.p2align	4, 0x90
.LBB13_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%rcx), %esi
	cmpl	%edx, %esi
	cmovgq	%rcx, %rax
	cmovgel	%esi, %edx
	movq	32(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB13_1
.LBB13_3:                               # %._crit_edge
	retq
.Lfunc_end13:
	.size	sm_longest_row, .Lfunc_end13-sm_longest_row
	.cfi_endproc

	.globl	sm_longest_col
	.p2align	4, 0x90
	.type	sm_longest_col,@function
sm_longest_col:                         # @sm_longest_col
	.cfi_startproc
# BB#0:
	movq	56(%rdi), %rcx
	xorl	%edx, %edx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB14_3
	.p2align	4, 0x90
.LBB14_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%rcx), %esi
	cmpl	%edx, %esi
	cmovgq	%rcx, %rax
	cmovgel	%esi, %edx
	movq	32(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB14_1
.LBB14_3:                               # %._crit_edge
	retq
.Lfunc_end14:
	.size	sm_longest_col, .Lfunc_end14-sm_longest_col
	.cfi_endproc

	.globl	sm_num_elements
	.p2align	4, 0x90
	.type	sm_num_elements,@function
sm_num_elements:                        # @sm_num_elements
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB15_3
	.p2align	4, 0x90
.LBB15_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addl	4(%rcx), %eax
	movq	32(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB15_1
.LBB15_3:                               # %._crit_edge
	retq
.Lfunc_end15:
	.size	sm_num_elements, .Lfunc_end15-sm_num_elements
	.cfi_endproc

	.globl	sm_read
	.p2align	4, 0x90
	.type	sm_read,@function
sm_read:                                # @sm_read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi90:
	.cfi_def_cfa_offset 64
.Lcfi91:
	.cfi_offset %rbx, -48
.Lcfi92:
	.cfi_offset %r12, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$88, %edi
	callq	malloc
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	movl	$0, 24(%rax)
	movl	$0, 8(%rax)
	movq	$0, 80(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rax)
	movl	$0, 48(%rax)
	movups	%xmm0, 56(%rax)
	movl	$0, 72(%rax)
	movq	%rax, (%r14)
	movq	%rbx, %rdi
	callq	feof
	movl	$1, %r15d
	testl	%eax, %eax
	jne	.LBB16_6
# BB#1:                                 # %.lr.ph.preheader
	leaq	12(%rsp), %r12
	leaq	8(%rsp), %rbp
	.p2align	4, 0x90
.LBB16_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%rbp, %rcx
	callq	fscanf
	cmpl	$2, %eax
	jne	.LBB16_3
# BB#5:                                 #   in Loop: Header=BB16_2 Depth=1
	movq	(%r14), %rdi
	movl	12(%rsp), %esi
	movl	8(%rsp), %edx
	callq	sm_insert
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB16_2
	jmp	.LBB16_6
.LBB16_3:                               # %.lr.ph
	cmpl	$-1, %eax
	je	.LBB16_6
# BB#4:                                 # %.loopexit.loopexit9
	xorl	%r15d, %r15d
.LBB16_6:                               # %.loopexit
	movl	%r15d, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	sm_read, .Lfunc_end16-sm_read
	.cfi_endproc

	.globl	sm_read_compressed
	.p2align	4, 0x90
	.type	sm_read_compressed,@function
sm_read_compressed:                     # @sm_read_compressed
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 80
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	movl	$88, %edi
	callq	malloc
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	movl	$0, 24(%rax)
	movl	$0, 8(%rax)
	movq	$0, 80(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rax)
	movl	$0, 48(%rax)
	movups	%xmm0, 56(%rax)
	movl	$0, 72(%rax)
	movq	%rax, (%r13)
	xorl	%ebx, %ebx
	leaq	12(%rsp), %rdx
	leaq	8(%rsp), %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	cmpl	$2, %eax
	jne	.LBB17_16
# BB#1:
	movq	(%r13), %rdi
	movl	12(%rsp), %esi
	movl	8(%rsp), %edx
	callq	sm_resize
	movl	$1, %ebx
	cmpl	$0, 12(%rsp)
	jle	.LBB17_16
# BB#2:                                 # %.lr.ph29.preheader
	leaq	16(%rsp), %r12
	xorl	%ebp, %ebp
.LBB17_3:                               # %.lr.ph29
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_6 Depth 2
                                        #       Child Loop BB17_9 Depth 3
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	fscanf
	cmpl	$1, %eax
	jne	.LBB17_15
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB17_3 Depth=1
	cmpl	$0, 8(%rsp)
	jle	.LBB17_13
# BB#5:                                 # %.lr.ph25.preheader
                                        #   in Loop: Header=BB17_3 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB17_6:                               # %.lr.ph25
                                        #   Parent Loop BB17_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_9 Depth 3
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	fscanf
	cmpl	$1, %eax
	jne	.LBB17_15
# BB#7:                                 # %thread-pre-split
                                        #   in Loop: Header=BB17_6 Depth=2
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.LBB17_12
# BB#8:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB17_6 Depth=2
	movl	%r15d, %ebx
	.p2align	4, 0x90
.LBB17_9:                               # %.lr.ph
                                        #   Parent Loop BB17_3 Depth=1
                                        #     Parent Loop BB17_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	$1, %al
	je	.LBB17_11
# BB#10:                                #   in Loop: Header=BB17_9 Depth=3
	movq	(%r13), %rdi
	movl	%ebp, %esi
	movl	%ebx, %edx
	callq	sm_insert
	movq	16(%rsp), %rax
.LBB17_11:                              #   in Loop: Header=BB17_9 Depth=3
	shrq	%rax
	movq	%rax, 16(%rsp)
	incl	%ebx
	testq	%rax, %rax
	jne	.LBB17_9
.LBB17_12:                              # %._crit_edge
                                        #   in Loop: Header=BB17_6 Depth=2
	addl	$32, %r15d
	cmpl	8(%rsp), %r15d
	jl	.LBB17_6
.LBB17_13:                              # %._crit_edge26
                                        #   in Loop: Header=BB17_3 Depth=1
	incl	%ebp
	cmpl	12(%rsp), %ebp
	jl	.LBB17_3
# BB#14:
	movl	$1, %ebx
	jmp	.LBB17_16
.LBB17_15:
	xorl	%ebx, %ebx
.LBB17_16:                              # %.loopexit
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	sm_read_compressed, .Lfunc_end17-sm_read_compressed
	.cfi_endproc

	.globl	sm_write
	.p2align	4, 0x90
	.type	sm_write,@function
sm_write:                               # @sm_write
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 32
.Lcfi112:
	.cfi_offset %rbx, -32
.Lcfi113:
	.cfi_offset %r14, -24
.Lcfi114:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	32(%rsi), %r14
	testq	%r14, %r14
	jne	.LBB18_2
	jmp	.LBB18_6
	.p2align	4, 0x90
.LBB18_5:                               # %._crit_edge
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	32(%r14), %r14
	testq	%r14, %r14
	je	.LBB18_6
.LBB18_2:                               # %.lr.ph18
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_3 Depth 2
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB18_5
	.p2align	4, 0x90
.LBB18_3:                               # %.lr.ph
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %edx
	movl	4(%rbx), %ecx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB18_3
	jmp	.LBB18_5
.LBB18_6:                               # %._crit_edge19
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end18:
	.size	sm_write, .Lfunc_end18-sm_write
	.cfi_endproc

	.globl	sm_print
	.p2align	4, 0x90
	.type	sm_print,@function
sm_print:                               # @sm_print
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi117:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi119:
	.cfi_def_cfa_offset 48
.Lcfi120:
	.cfi_offset %rbx, -40
.Lcfi121:
	.cfi_offset %r12, -32
.Lcfi122:
	.cfi_offset %r14, -24
.Lcfi123:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	64(%r14), %rax
	movl	(%rax), %eax
	cmpl	$100, %eax
	jl	.LBB19_5
# BB#1:
	movl	$.L.str.3, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	56(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB19_4
	.p2align	4, 0x90
.LBB19_2:                               # %.lr.ph69
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rbx), %rax
	imulq	$1374389535, %rax, %rax # imm = 0x51EB851F
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	sarl	$5, %eax
	addl	%ecx, %eax
	movslq	%eax, %rdx
	imulq	$1717986919, %rdx, %rax # imm = 0x66666667
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	sarl	$2, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%rax,%rax,4), %eax
	subl	%eax, %edx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB19_2
.LBB19_4:                               # %._crit_edge70
	movl	$10, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	64(%r14), %rax
	movl	(%rax), %eax
.LBB19_5:
	cmpl	$9, %eax
	jle	.LBB19_6
# BB#7:
	movl	$.L.str.3, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	56(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB19_10
	.p2align	4, 0x90
.LBB19_8:                               # %.lr.ph64
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rbx), %rax
	imulq	$1717986919, %rax, %rax # imm = 0x66666667
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	sarl	$2, %eax
	addl	%ecx, %eax
	movslq	%eax, %rdx
	imulq	$1717986919, %rdx, %rax # imm = 0x66666667
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	sarl	$2, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%rax,%rax,4), %eax
	subl	%eax, %edx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB19_8
.LBB19_10:                              # %._crit_edge65
	leaq	56(%r14), %r12
	movl	$10, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	jmp	.LBB19_11
.LBB19_6:                               # %._crit_edge72
	leaq	56(%r14), %r12
.LBB19_11:
	movl	$.L.str.3, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB19_14
	.p2align	4, 0x90
.LBB19_12:                              # %.lr.ph59
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rbx), %rdx
	imulq	$1717986919, %rdx, %rax # imm = 0x66666667
	movq	%rax, %rcx
	shrq	$63, %rcx
	sarq	$34, %rax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%rax,%rax,4), %eax
	subl	%eax, %edx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB19_12
.LBB19_14:                              # %._crit_edge60
	movl	$10, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movl	$.L.str.3, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB19_17
	.p2align	4, 0x90
.LBB19_15:                              # %.lr.ph54
                                        # =>This Inner Loop Header: Depth=1
	movl	$45, %edi
	movq	%r15, %rsi
	callq	fputc
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB19_15
	jmp	.LBB19_17
	.p2align	4, 0x90
.LBB19_18:                              # %.lr.ph49
                                        #   in Loop: Header=BB19_17 Depth=1
	movl	(%r14), %edx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movq	(%r12), %rbx
	jmp	.LBB19_20
	.p2align	4, 0x90
.LBB19_19:                              # %.lr.ph
                                        #   in Loop: Header=BB19_20 Depth=2
	movl	(%rbx), %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sm_row_find
	xorl	%ecx, %ecx
	testq	%rax, %rax
	setne	%cl
	leal	46(%rcx,%rcx,2), %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	32(%rbx), %rbx
.LBB19_20:                              # %.lr.ph
                                        #   Parent Loop BB19_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbx, %rbx
	jne	.LBB19_19
.LBB19_17:                              # %._crit_edge55
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_20 Depth 2
	movl	$10, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	32(%r14), %r14
	testq	%r14, %r14
	jne	.LBB19_18
# BB#21:                                # %._crit_edge50
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end19:
	.size	sm_print, .Lfunc_end19-sm_print
	.cfi_endproc

	.globl	sm_dump
	.p2align	4, 0x90
	.type	sm_dump,@function
sm_dump:                                # @sm_dump
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 32
.Lcfi127:
	.cfi_offset %rbx, -32
.Lcfi128:
	.cfi_offset %r14, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rdx
	movq	%rdi, %rbx
	movq	stdout(%rip), %r14
	movl	48(%rbx), %ecx
	movl	72(%rbx), %r8d
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	cmpl	%ebp, 48(%rbx)
	jge	.LBB20_1
# BB#2:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	sm_print                # TAILCALL
.LBB20_1:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end20:
	.size	sm_dump, .Lfunc_end20-sm_dump
	.cfi_endproc

	.globl	sm_cleanup
	.p2align	4, 0x90
	.type	sm_cleanup,@function
sm_cleanup:                             # @sm_cleanup
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end21:
	.size	sm_cleanup, .Lfunc_end21-sm_cleanup
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d %d"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%lx"
	.size	.L.str.1, 4

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%d %d\n"
	.size	.L.str.2, 7

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"    "
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%d"
	.size	.L.str.4, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%3d:"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%s %d rows by %d cols\n"
	.size	.L.str.7, 23


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
