	.text
	.file	"args.bc"
	.globl	mylog
	.p2align	4, 0x90
	.type	mylog,@function
mylog:                                  # @mylog
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$2, %edi
	jl	.LBB0_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addl	%ecx, %ecx
	incl	%eax
	cmpl	%edi, %ecx
	jl	.LBB0_2
.LBB0_3:                                # %._crit_edge
	retq
.Lfunc_end0:
	.size	mylog, .Lfunc_end0-mylog
	.cfi_endproc

	.globl	dealwithargs
	.p2align	4, 0x90
	.type	dealwithargs,@function
dealwithargs:                           # @dealwithargs
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	cmpl	$4, %edi
	jl	.LBB1_2
# BB#1:                                 # %.thread
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, flag(%rip)
	jmp	.LBB1_3
.LBB1_2:
	movl	$1, flag(%rip)
	cmpl	$3, %edi
	jne	.LBB1_7
.LBB1_3:                                # %.thread9
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, NumNodes(%rip)
.LBB1_4:
	movq	8(%rbx), %rdi
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
	movl	NumNodes(%rip), %edx
	cmpl	$2, %edx
	jl	.LBB1_9
# BB#5:                                 # %.lr.ph.i.preheader
	xorl	%ebx, %ebx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	addl	%eax, %eax
	incl	%ebx
	cmpl	%edx, %eax
	jl	.LBB1_6
	jmp	.LBB1_9
.LBB1_7:
	movl	$1, NumNodes(%rip)
	cmpl	$2, %edi
	jge	.LBB1_4
# BB#8:
	xorl	%ebx, %ebx
	movl	$32, %r14d
	movl	$1, %edx
.LBB1_9:                                # %mylog.exit
	movl	%ebx, NDim(%rip)
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movl	%ebx, %ecx
	callq	printf
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	dealwithargs, .Lfunc_end1-dealwithargs
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Voronoi with %d size on %d procs of dim %d\n"
	.size	.L.str, 44


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
