	.text
	.file	"errorMessage.bc"
	.globl	errorMessage
	.p2align	4, 0x90
	.type	errorMessage,@function
errorMessage:                           # @errorMessage
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$1032, %rsp             # imm = 0x408
.Lcfi2:
	.cfi_def_cfa_offset 1056
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testb	%sil, %sil
	je	.LBB0_1
# BB#3:
	movl	$errorBuffer, %edi
	callq	strlen
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	strlen
	addq	%rbx, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB0_6
# BB#4:
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$36, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	movl	$errorBuffer, %ecx
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	fprintf
	movabsq	$7162252202996887157, %rax # imm = 0x63656E6E6F636E75
	movq	%rax, errorBuffer(%rip)
	movl	$6579572, errorBuffer+8(%rip) # imm = 0x646574
	jmp	.LBB0_5
.LBB0_1:
	movq	%r14, %rdi
	callq	strlen
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB0_2
# BB#7:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$45, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	addq	$1032, %rsp             # imm = 0x408
	popq	%rbx
	popq	%r14
	jmp	fprintf                 # TAILCALL
.LBB0_6:                                # %errorMessage.exit
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	movb	$0, 2(%rsp,%rax)
	movw	$8250, (%rsp,%rax)      # imm = 0x203A
	movl	$errorBuffer, %esi
	movq	%rbx, %rdi
	callq	strcat
	movl	$errorBuffer, %edi
	movq	%rbx, %rsi
	callq	strcpy
.LBB0_5:
	addq	$1032, %rsp             # imm = 0x408
	popq	%rbx
	popq	%r14
	retq
.LBB0_2:
	movl	$errorBuffer, %edi
	movq	%r14, %rsi
	addq	$1032, %rsp             # imm = 0x408
	popq	%rbx
	popq	%r14
	jmp	strcpy                  # TAILCALL
.Lfunc_end0:
	.size	errorMessage, .Lfunc_end0-errorMessage
	.cfi_endproc

	.globl	flushErrorMessage
	.p2align	4, 0x90
	.type	flushErrorMessage,@function
flushErrorMessage:                      # @flushErrorMessage
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movl	$errorBuffer, %edi
	callq	strlen
	testq	%rax, %rax
	je	.LBB1_1
# BB#2:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	movl	$errorBuffer, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	popq	%rax
	jmp	fflush                  # TAILCALL
.LBB1_1:
	popq	%rax
	retq
.Lfunc_end1:
	.size	flushErrorMessage, .Lfunc_end1-flushErrorMessage
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Error Message Too Large for Buffer: flushing\n"
	.size	.L.str, 46

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"unconnected: %s\n"
	.size	.L.str.1, 17

	.type	errorBuffer,@object     # @errorBuffer
	.local	errorBuffer
	.comm	errorBuffer,1024,16
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Error Message Buffer full: flushing\n"
	.size	.L.str.2, 37

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"unconnected: %s: %s\n"
	.size	.L.str.3, 21

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"unconnected"
	.size	.L.str.4, 12

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	": "
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%s\n"
	.size	.L.str.6, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
