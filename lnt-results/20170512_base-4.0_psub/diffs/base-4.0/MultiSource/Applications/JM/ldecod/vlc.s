	.text
	.file	"vlc.bc"
	.globl	ue_v
	.p2align	4, 0x90
	.type	ue_v,@function
ue_v:                                   # @ue_v
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Lcfi0:
	.cfi_def_cfa_offset 64
	movl	$0, 8(%rsp)
	movq	$linfo_ue, 40(%rsp)
	leaq	8(%rsp), %rdi
	callq	readSyntaxElement_VLC
	movl	20(%rsp), %eax
	addl	%eax, UsedBits(%rip)
	movl	12(%rsp), %eax
	addq	$56, %rsp
	retq
.Lfunc_end0:
	.size	ue_v, .Lfunc_end0-ue_v
	.cfi_endproc

	.globl	linfo_ue
	.p2align	4, 0x90
	.type	linfo_ue,@function
linfo_ue:                               # @linfo_ue
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	shrl	%edi
	movl	$1, %eax
	movl	%edi, %ecx
	shll	%cl, %eax
	leal	-1(%rsi,%rax), %eax
	movl	%eax, (%rdx)
	retq
.Lfunc_end1:
	.size	linfo_ue, .Lfunc_end1-linfo_ue
	.cfi_endproc

	.globl	readSyntaxElement_VLC
	.p2align	4, 0x90
	.type	readSyntaxElement_VLC,@function
readSyntaxElement_VLC:                  # @readSyntaxElement_VLC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 48
.Lcfi6:
	.cfi_offset %rbx, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rdi, %r8
	movl	8(%rsi), %eax
	movl	%eax, %ecx
	notl	%ecx
	movq	16(%rsi), %r9
	sarl	$3, %eax
	movslq	%eax, %rdx
	andl	$7, %ecx
	movzbl	(%r9,%rdx), %eax
	btl	%ecx, %eax
	jae	.LBB2_2
# BB#1:                                 # %GetVLCSymbol.exit.thread21
	movq	$1, 12(%r8)
	movl	$1, %edi
	xorl	%eax, %eax
	jmp	.LBB2_11
.LBB2_2:                                # %.lr.ph73.i.preheader
	movslq	12(%rsi), %r10
	movl	$1, %edi
	movl	$7, %eax
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph73.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebp
	incl	%edi
	xorl	%ebx, %ebx
	testl	%ebp, %ebp
	setle	%bl
	movl	$-1, %ecx
	cmovlel	%eax, %ecx
	addq	%rbx, %rdx
	addl	%ebp, %ecx
	movzbl	(%r9,%rdx), %ebp
	btl	%ecx, %ebp
	jae	.LBB2_3
# BB#4:                                 # %.preheader.i
	leal	-1(%rdi), %r11d
	testl	%r11d, %r11d
	jle	.LBB2_9
# BB#5:                                 # %.lr.ph.i
	xorl	%ebx, %ebx
	movl	$7, %r14d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_6:                                # =>This Inner Loop Header: Depth=1
	movq	%rdx, %r15
	xorl	%edx, %edx
	testl	%ecx, %ecx
	setle	%dl
	movl	$-1, %ebp
	cmovlel	%r14d, %ebp
	addq	%r15, %rdx
	cmpq	%r10, %rdx
	jg	.LBB2_13
# BB#7:                                 #   in Loop: Header=BB2_6 Depth=1
	addl	%ebp, %ecx
	movzbl	(%r9,%rdx), %ebp
	btl	%ecx, %ebp
	sbbl	%ebp, %ebp
	andl	$1, %ebp
	leal	(%rbp,%rax,2), %eax
	incl	%ebx
	cmpl	%r11d, %ebx
	jl	.LBB2_6
# BB#8:                                 # %GetVLCSymbol.exit.loopexit
	addl	%ebx, %edi
	jmp	.LBB2_10
.LBB2_9:
	xorl	%eax, %eax
.LBB2_10:                               # %GetVLCSymbol.exit
	movl	%eax, 16(%r8)
	movl	%edi, 12(%r8)
	cmpl	$-1, %edi
	je	.LBB2_14
.LBB2_11:
	addl	%edi, 8(%rsi)
	leaq	4(%r8), %rdx
	leaq	8(%r8), %rcx
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%eax, %esi
	callq	*32(%r8)
	movl	$1, %eax
	jmp	.LBB2_12
.LBB2_13:                               # %GetVLCSymbol.exit.thread
	movl	$-1, 12(%r8)
.LBB2_14:
	movl	$-1, %eax
.LBB2_12:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	readSyntaxElement_VLC, .Lfunc_end2-readSyntaxElement_VLC
	.cfi_endproc

	.globl	se_v
	.p2align	4, 0x90
	.type	se_v,@function
se_v:                                   # @se_v
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 64
	movl	$0, 8(%rsp)
	movq	$linfo_se, 40(%rsp)
	leaq	8(%rsp), %rdi
	callq	readSyntaxElement_VLC
	movl	20(%rsp), %eax
	addl	%eax, UsedBits(%rip)
	movl	12(%rsp), %eax
	addq	$56, %rsp
	retq
.Lfunc_end3:
	.size	se_v, .Lfunc_end3-se_v
	.cfi_endproc

	.globl	linfo_se
	.p2align	4, 0x90
	.type	linfo_se,@function
linfo_se:                               # @linfo_se
	.cfi_startproc
# BB#0:
	shrl	%edi
	movl	$1, %eax
	movl	%edi, %ecx
	shll	%cl, %eax
	addl	%esi, %eax
	movl	%eax, %ecx
	sarl	%ecx
	movl	%ecx, %esi
	negl	%esi
	testb	$1, %al
	cmovel	%ecx, %esi
	movl	%esi, (%rdx)
	retq
.Lfunc_end4:
	.size	linfo_se, .Lfunc_end4-linfo_se
	.cfi_endproc

	.globl	u_v
	.p2align	4, 0x90
	.type	u_v,@function
u_v:                                    # @u_v
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	8(%rdx), %r8d
	movl	%r8d, %ecx
	notl	%ecx
	movq	16(%rdx), %r9
	movl	%r8d, %eax
	sarl	$3, %eax
	movslq	%eax, %r11
	andl	$7, %ecx
	movslq	12(%rdx), %r10
	xorl	%eax, %eax
	movl	%edi, %esi
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	testl	%esi, %esi
	je	.LBB5_5
# BB#2:                                 #   in Loop: Header=BB5_1 Depth=1
	movl	%eax, %ebx
	addl	%ebx, %ebx
	movzbl	(%r9,%r11), %ebp
	movl	$1, %eax
	shll	%cl, %eax
	andl	%ebp, %eax
	shrl	%cl, %eax
	orl	%ebx, %eax
	decl	%esi
	leal	-1(%rcx), %ebx
	testl	%ecx, %ecx
	movl	%ebx, %ecx
	jg	.LBB5_1
# BB#3:                                 #   in Loop: Header=BB5_1 Depth=1
	addl	$8, %ebx
	cmpq	%r10, %r11
	leaq	1(%r11), %r11
	movl	%ebx, %ecx
	jl	.LBB5_1
# BB#4:
	xorl	%eax, %eax
	jmp	.LBB5_7
.LBB5_5:                                # %GetBits.exit.i
	testl	%edi, %edi
	js	.LBB5_7
# BB#6:
	addl	%edi, %r8d
	movl	%r8d, 8(%rdx)
.LBB5_7:                                # %readSyntaxElement_FLC.exit
	addl	%edi, UsedBits(%rip)
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end5:
	.size	u_v, .Lfunc_end5-u_v
	.cfi_endproc

	.globl	readSyntaxElement_FLC
	.p2align	4, 0x90
	.type	readSyntaxElement_FLC,@function
readSyntaxElement_FLC:                  # @readSyntaxElement_FLC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	8(%rsi), %eax
	movl	%eax, %ecx
	notl	%ecx
	movq	16(%rsi), %r9
	movl	12(%rdi), %r8d
	sarl	$3, %eax
	movslq	%eax, %r11
	andl	$7, %ecx
	movslq	12(%rsi), %r10
	xorl	%edx, %edx
	movl	%r8d, %eax
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	testl	%eax, %eax
	je	.LBB6_5
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	movl	%edx, %ebx
	addl	%ebx, %ebx
	movzbl	(%r9,%r11), %ebp
	movl	$1, %edx
	shll	%cl, %edx
	andl	%ebp, %edx
	shrl	%cl, %edx
	orl	%ebx, %edx
	decl	%eax
	leal	-1(%rcx), %ebx
	testl	%ecx, %ecx
	movl	%ebx, %ecx
	jg	.LBB6_1
# BB#3:                                 #   in Loop: Header=BB6_1 Depth=1
	addl	$8, %ebx
	cmpq	%r10, %r11
	leaq	1(%r11), %r11
	movl	%ebx, %ecx
	jl	.LBB6_1
	jmp	.LBB6_7
.LBB6_5:                                # %GetBits.exit
	movl	%edx, 16(%rdi)
	testl	%r8d, %r8d
	js	.LBB6_7
# BB#6:
	addl	%r8d, 8(%rsi)
	movl	%edx, 4(%rdi)
	movl	$1, %eax
	jmp	.LBB6_8
.LBB6_7:
	movl	$-1, %eax
.LBB6_8:                                # %GetBits.exit.thread
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end6:
	.size	readSyntaxElement_FLC, .Lfunc_end6-readSyntaxElement_FLC
	.cfi_endproc

	.globl	u_1
	.p2align	4, 0x90
	.type	u_1,@function
u_1:                                    # @u_1
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
.Lcfi20:
	.cfi_offset %rbx, -16
	movl	8(%rsi), %r8d
	movl	%r8d, %ecx
	notl	%ecx
	movq	16(%rsi), %r10
	movl	%r8d, %eax
	sarl	$3, %eax
	movslq	%eax, %r11
	andl	$7, %ecx
	movslq	12(%rsi), %r9
	xorl	%eax, %eax
	movl	$1, %edi
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	testl	%edi, %edi
	je	.LBB7_5
# BB#2:                                 #   in Loop: Header=BB7_1 Depth=1
	movl	%eax, %edx
	addl	%edx, %edx
	movzbl	(%r10,%r11), %ebx
	movl	$1, %eax
	shll	%cl, %eax
	andl	%ebx, %eax
	shrl	%cl, %eax
	orl	%edx, %eax
	decl	%edi
	leal	-1(%rcx), %edx
	testl	%ecx, %ecx
	movl	%edx, %ecx
	jg	.LBB7_1
# BB#3:                                 #   in Loop: Header=BB7_1 Depth=1
	addl	$8, %edx
	cmpq	%r9, %r11
	leaq	1(%r11), %r11
	movl	%edx, %ecx
	jl	.LBB7_1
# BB#4:
	xorl	%eax, %eax
	jmp	.LBB7_6
.LBB7_5:                                # %GetBits.exit.i.i
	incl	%r8d
	movl	%r8d, 8(%rsi)
.LBB7_6:                                # %u_v.exit
	incl	UsedBits(%rip)
	popq	%rbx
	retq
.Lfunc_end7:
	.size	u_1, .Lfunc_end7-u_1
	.cfi_endproc

	.globl	linfo_cbp_intra
	.p2align	4, 0x90
	.type	linfo_cbp_intra,@function
linfo_cbp_intra:                        # @linfo_cbp_intra
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	shrl	%edi
	movl	$1, %eax
	movl	%edi, %ecx
	shll	%cl, %eax
	leal	-1(%rsi,%rax), %eax
	movq	active_sps(%rip), %rcx
	xorl	%esi, %esi
	cmpl	$0, 32(%rcx)
	setne	%sil
	cltq
	leaq	(%rsi,%rsi,2), %rcx
	shlq	$5, %rcx
	movzbl	NCBP(%rcx,%rax,2), %eax
	movl	%eax, (%rdx)
	retq
.Lfunc_end8:
	.size	linfo_cbp_intra, .Lfunc_end8-linfo_cbp_intra
	.cfi_endproc

	.globl	linfo_cbp_inter
	.p2align	4, 0x90
	.type	linfo_cbp_inter,@function
linfo_cbp_inter:                        # @linfo_cbp_inter
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	shrl	%edi
	movl	$1, %eax
	movl	%edi, %ecx
	shll	%cl, %eax
	leal	-1(%rsi,%rax), %eax
	movq	active_sps(%rip), %rcx
	xorl	%esi, %esi
	cmpl	$0, 32(%rcx)
	setne	%sil
	cltq
	leaq	(%rsi,%rsi,2), %rcx
	shlq	$5, %rcx
	movzbl	NCBP+1(%rcx,%rax,2), %eax
	movl	%eax, (%rdx)
	retq
.Lfunc_end9:
	.size	linfo_cbp_inter, .Lfunc_end9-linfo_cbp_inter
	.cfi_endproc

	.globl	linfo_levrun_inter
	.p2align	4, 0x90
	.type	linfo_levrun_inter,@function
linfo_levrun_inter:                     # @linfo_levrun_inter
	.cfi_startproc
# BB#0:
	cmpl	$9, %edi
	jg	.LBB10_5
# BB#1:
	movl	%edi, %r9d
	sarl	%r9d
	xorl	%r8d, %r8d
	decl	%r9d
	cmovsl	%r8d, %r9d
	movl	%esi, %eax
	sarl	%eax
	movslq	%eax, %r8
	shlq	$4, %r9
	movzbl	NTAB1(%r9,%r8,2), %eax
	movl	%eax, (%rdx)
	movzbl	NTAB1+1(%r9,%r8,2), %eax
	movl	%eax, (%rcx)
	testb	$1, %sil
	je	.LBB10_3
# BB#2:
	negl	(%rdx)
.LBB10_3:
	cmpl	$1, %edi
	jne	.LBB10_7
# BB#4:
	movl	$0, (%rdx)
	retq
.LBB10_5:
	movl	%esi, %eax
	shrl	%eax
	andl	$15, %eax
	movl	%eax, (%rcx)
	movzbl	LEVRUN1(%rax), %r8d
	movl	%esi, %r9d
	sarl	$5, %r9d
	shrl	%edi
	addl	$-5, %edi
	movl	$1, %eax
	movl	%edi, %ecx
	shll	%cl, %eax
	addl	%r9d, %eax
	addl	%r8d, %eax
	movl	%eax, (%rdx)
	testb	$1, %sil
	je	.LBB10_7
# BB#6:
	negl	%eax
	movl	%eax, (%rdx)
.LBB10_7:                               # %.thread
	retq
.Lfunc_end10:
	.size	linfo_levrun_inter, .Lfunc_end10-linfo_levrun_inter
	.cfi_endproc

	.globl	linfo_levrun_c2x2
	.p2align	4, 0x90
	.type	linfo_levrun_c2x2,@function
linfo_levrun_c2x2:                      # @linfo_levrun_c2x2
	.cfi_startproc
# BB#0:
	cmpl	$5, %edi
	jg	.LBB11_5
# BB#1:
	movl	%edi, %r9d
	sarl	%r9d
	xorl	%r8d, %r8d
	decl	%r9d
	cmovsl	%r8d, %r9d
	movl	%esi, %eax
	sarl	%eax
	movslq	%eax, %r8
	addq	%r8, %r8
	movzbl	NTAB3(%r8,%r9,4), %eax
	movl	%eax, (%rdx)
	movzbl	NTAB3+1(%r8,%r9,4), %eax
	movl	%eax, (%rcx)
	testb	$1, %sil
	je	.LBB11_3
# BB#2:
	negl	(%rdx)
.LBB11_3:
	cmpl	$1, %edi
	jne	.LBB11_7
# BB#4:
	movl	$0, (%rdx)
	retq
.LBB11_5:
	movl	%esi, %eax
	shrl	%eax
	andl	$3, %eax
	movl	%eax, (%rcx)
	movzbl	LEVRUN3(%rax), %r8d
	movl	%esi, %r9d
	sarl	$3, %r9d
	shrl	%edi
	addl	$-3, %edi
	movl	$1, %eax
	movl	%edi, %ecx
	shll	%cl, %eax
	addl	%r9d, %eax
	addl	%r8d, %eax
	movl	%eax, (%rdx)
	testb	$1, %sil
	je	.LBB11_7
# BB#6:
	negl	%eax
	movl	%eax, (%rdx)
.LBB11_7:                               # %.thread
	retq
.Lfunc_end11:
	.size	linfo_levrun_c2x2, .Lfunc_end11-linfo_levrun_c2x2
	.cfi_endproc

	.globl	GetVLCSymbol
	.p2align	4, 0x90
	.type	GetVLCSymbol,@function
GetVLCSymbol:                           # @GetVLCSymbol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 40
.Lcfi25:
	.cfi_offset %rbx, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movl	%esi, %r10d
	notl	%r10d
	sarl	$3, %esi
	movslq	%esi, %rsi
	andl	$7, %r10d
	movzbl	(%rdi,%rsi), %eax
	movl	$1, %r9d
	xorl	%r8d, %r8d
	btl	%r10d, %eax
	jae	.LBB12_1
.LBB12_8:                               # %._crit_edge
	movl	%r8d, (%rdx)
	movl	%r9d, %eax
.LBB12_9:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_1:                               # %.lr.ph73.preheader
	movl	$1, %r9d
	movl	$7, %eax
	.p2align	4, 0x90
.LBB12_2:                               # %.lr.ph73
                                        # =>This Inner Loop Header: Depth=1
	movl	%r10d, %ebp
	incl	%r9d
	xorl	%ebx, %ebx
	testl	%ebp, %ebp
	setle	%bl
	movl	$-1, %r10d
	cmovlel	%eax, %r10d
	addq	%rbx, %rsi
	addl	%ebp, %r10d
	movzbl	(%rdi,%rsi), %ebp
	btl	%r10d, %ebp
	jae	.LBB12_2
# BB#3:                                 # %.preheader
	leal	-1(%r9), %r11d
	testl	%r11d, %r11d
	jle	.LBB12_8
# BB#4:                                 # %.lr.ph
	movslq	%ecx, %r15
	xorl	%ecx, %ecx
	movl	$7, %r14d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB12_5:                               # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rbx
	xorl	%esi, %esi
	testl	%r10d, %r10d
	setle	%sil
	movl	$-1, %eax
	movl	$-1, %ebp
	cmovlel	%r14d, %ebp
	addq	%rbx, %rsi
	cmpq	%r15, %rsi
	jg	.LBB12_9
# BB#6:                                 #   in Loop: Header=BB12_5 Depth=1
	addl	%ebp, %r10d
	movzbl	(%rdi,%rsi), %eax
	btl	%r10d, %eax
	sbbl	%eax, %eax
	andl	$1, %eax
	leal	(%rax,%r8,2), %r8d
	incl	%ecx
	cmpl	%r11d, %ecx
	jl	.LBB12_5
# BB#7:                                 # %._crit_edge.loopexit
	addl	%ecx, %r9d
	jmp	.LBB12_8
.Lfunc_end12:
	.size	GetVLCSymbol, .Lfunc_end12-GetVLCSymbol
	.cfi_endproc

	.globl	readSyntaxElement_UVLC
	.p2align	4, 0x90
	.type	readSyntaxElement_UVLC,@function
readSyntaxElement_UVLC:                 # @readSyntaxElement_UVLC
	.cfi_startproc
# BB#0:
	movq	(%rdx), %rsi
	jmp	readSyntaxElement_VLC   # TAILCALL
.Lfunc_end13:
	.size	readSyntaxElement_UVLC, .Lfunc_end13-readSyntaxElement_UVLC
	.cfi_endproc

	.globl	readSyntaxElement_Intra4x4PredictionMode
	.p2align	4, 0x90
	.type	readSyntaxElement_Intra4x4PredictionMode,@function
readSyntaxElement_Intra4x4PredictionMode: # @readSyntaxElement_Intra4x4PredictionMode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	(%rdx), %r14
	movl	8(%r14), %eax
	movl	%eax, %edx
	notl	%edx
	movq	16(%r14), %r8
	sarl	$3, %eax
	movslq	%eax, %rsi
	andl	$7, %edx
	movzbl	(%r8,%rsi), %ecx
	movl	$1, %eax
	xorl	%ebx, %ebx
	btl	%edx, %ecx
	movl	$1, %ecx
	jae	.LBB14_1
.LBB14_5:
	movl	%ebx, 16(%rdi)
	movl	%ecx, 12(%rdi)
	addl	%ecx, 8(%r14)
	cmpl	$1, %ecx
	movl	$-1, %ecx
	cmovnel	%ebx, %ecx
	movl	%ecx, 4(%rdi)
	jmp	.LBB14_6
.LBB14_1:                               # %.preheader.i
	cmpl	$1, %edx
	adcq	$0, %rsi
	cmpl	$1, %edx
	movslq	12(%r14), %r9
	sbbl	%r11d, %r11d
	cmpq	%r9, %rsi
	jg	.LBB14_7
# BB#2:
	notl	%r11d
	orl	$7, %r11d
	addl	%edx, %r11d
	xorl	%r10d, %r10d
	testl	%r11d, %r11d
	setle	%r10b
	movl	$7, %ecx
	movl	$-1, %edx
	cmovlel	%ecx, %edx
	addq	%rsi, %r10
	cmpq	%r9, %r10
	jg	.LBB14_7
# BB#3:
	addl	%r11d, %edx
	xorl	%ebx, %ebx
	testl	%edx, %edx
	setle	%bl
	addq	%r10, %rbx
	cmpq	%r9, %rbx
	jle	.LBB14_4
.LBB14_7:                               # %GetVLCSymbol_IntraMode.exit.thread
	movl	$-1, 12(%rdi)
	movl	$-1, %eax
.LBB14_6:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB14_4:                               # %.sink.split.loopexit53.i
	testl	%edx, %edx
	movl	$7, %r9d
	movl	$-1, %ebp
	cmovlel	%r9d, %ebp
	addl	%edx, %ebp
	movzbl	(%r8,%r10), %ecx
	btl	%edx, %ecx
	sbbl	%ecx, %ecx
	andl	$1, %ecx
	movzbl	(%r8,%rsi), %edx
	btl	%r11d, %edx
	sbbl	%edx, %edx
	andl	$2, %edx
	orl	%ecx, %edx
	movzbl	(%r8,%rbx), %ecx
	btl	%ebp, %ecx
	sbbl	%ecx, %ecx
	andl	$1, %ecx
	leal	(%rcx,%rdx,2), %ebx
	movl	$4, %ecx
	jmp	.LBB14_5
.Lfunc_end14:
	.size	readSyntaxElement_Intra4x4PredictionMode, .Lfunc_end14-readSyntaxElement_Intra4x4PredictionMode
	.cfi_endproc

	.globl	GetVLCSymbol_IntraMode
	.p2align	4, 0x90
	.type	GetVLCSymbol_IntraMode,@function
GetVLCSymbol_IntraMode:                 # @GetVLCSymbol_IntraMode
	.cfi_startproc
# BB#0:
	movl	%esi, %r9d
	notl	%r9d
	sarl	$3, %esi
	movslq	%esi, %rsi
	andl	$7, %r9d
	movzbl	(%rdi,%rsi), %eax
	btl	%r9d, %eax
	jae	.LBB15_4
# BB#1:
	movl	$1, %eax
	xorl	%ecx, %ecx
.LBB15_2:                               # %.sink.split
	movl	%ecx, (%rdx)
	retq
.LBB15_4:                               # %.preheader
	cmpl	$1, %r9d
	adcq	$0, %rsi
	cmpl	$1, %r9d
	movslq	%ecx, %r8
	sbbl	%ecx, %ecx
	movl	$-1, %eax
	cmpq	%r8, %rsi
	jg	.LBB15_3
# BB#5:
	notl	%ecx
	orl	$7, %ecx
	addl	%r9d, %ecx
	xorl	%r9d, %r9d
	testl	%ecx, %ecx
	setle	%r9b
	movl	$7, %r11d
	movl	$-1, %eax
	cmovgl	%eax, %r11d
	addq	%rsi, %r9
	cmpq	%r8, %r9
	jg	.LBB15_3
# BB#6:
	addl	%ecx, %r11d
	xorl	%r10d, %r10d
	testl	%r11d, %r11d
	setle	%r10b
	addq	%r9, %r10
	cmpq	%r8, %r10
	jle	.LBB15_7
.LBB15_3:                               # %.loopexit
	retq
.LBB15_7:                               # %.sink.split.loopexit53
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 16
.Lcfi36:
	.cfi_offset %rbx, -16
	testl	%r11d, %r11d
	movl	$7, %r8d
	movl	$-1, %ebx
	cmovlel	%r8d, %ebx
	addl	%r11d, %ebx
	movzbl	(%rdi,%r9), %eax
	btl	%r11d, %eax
	sbbl	%eax, %eax
	andl	$1, %eax
	movzbl	(%rdi,%rsi), %esi
	btl	%ecx, %esi
	sbbl	%ecx, %ecx
	andl	$2, %ecx
	orl	%eax, %ecx
	movzbl	(%rdi,%r10), %eax
	btl	%ebx, %eax
	sbbl	%eax, %eax
	andl	$1, %eax
	leal	(%rax,%rcx,2), %ecx
	movl	$4, %eax
	popq	%rbx
	jmp	.LBB15_2
.Lfunc_end15:
	.size	GetVLCSymbol_IntraMode, .Lfunc_end15-GetVLCSymbol_IntraMode
	.cfi_endproc

	.globl	more_rbsp_data
	.p2align	4, 0x90
	.type	more_rbsp_data,@function
more_rbsp_data:                         # @more_rbsp_data
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	%esi, %ecx
	sarl	$3, %ecx
	decl	%edx
	movl	$1, %eax
	cmpl	%edx, %ecx
	jl	.LBB16_7
# BB#1:
	notl	%esi
	andl	$7, %esi
	movslq	%ecx, %rcx
	movzbl	(%rdi,%rcx), %ecx
	btl	%esi, %ecx
	jae	.LBB16_7
# BB#2:                                 # %.preheader
	testl	%esi, %esi
	je	.LBB16_3
# BB#4:                                 # %.lr.ph
	leal	-1(%rsi), %edx
	btl	%edx, %ecx
	sbbl	%eax, %eax
	andl	$1, %eax
	cmpl	$1, %esi
	jbe	.LBB16_5
# BB#8:                                 # %.lr.ph.1
	leal	-2(%rsi), %edi
	btl	%edi, %ecx
	adcl	$0, %eax
	cmpl	$2, %edx
	jl	.LBB16_5
# BB#9:                                 # %.lr.ph.2
	leal	-3(%rsi), %edx
	btl	%edx, %ecx
	adcl	$0, %eax
	cmpl	$2, %edi
	jl	.LBB16_5
# BB#10:                                # %.lr.ph.3
	leal	-4(%rsi), %edi
	btl	%edi, %ecx
	adcl	$0, %eax
	cmpl	$2, %edx
	jl	.LBB16_5
# BB#11:                                # %.lr.ph.4
	leal	-5(%rsi), %edx
	btl	%edx, %ecx
	adcl	$0, %eax
	cmpl	$2, %edi
	jl	.LBB16_5
# BB#12:                                # %.lr.ph.5
	leal	-6(%rsi), %edi
	btl	%edi, %ecx
	adcl	$0, %eax
	cmpl	$2, %edx
	jl	.LBB16_5
# BB#13:                                # %.lr.ph.6
	addl	$-7, %esi
	btl	%esi, %ecx
	adcl	$0, %eax
.LBB16_5:                               # %._crit_edge.loopexit
	testl	%eax, %eax
	setne	%al
	jmp	.LBB16_6
.LBB16_3:
	xorl	%eax, %eax
.LBB16_6:                               # %._crit_edge
	movzbl	%al, %eax
.LBB16_7:
	retq
.Lfunc_end16:
	.size	more_rbsp_data, .Lfunc_end16-more_rbsp_data
	.cfi_endproc

	.globl	uvlc_startcode_follows
	.p2align	4, 0x90
	.type	uvlc_startcode_follows,@function
uvlc_startcode_follows:                 # @uvlc_startcode_follows
	.cfi_startproc
# BB#0:
	movq	5592(%rdi), %rax
	movslq	28(%rax), %rcx
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	movq	40(%rax), %rax
	movslq	assignSE2partition+8(%rcx), %rcx
	imulq	$56, %rcx, %rcx
	movq	(%rax,%rcx), %rcx
	movl	8(%rcx), %eax
	movl	12(%rcx), %esi
	movl	%eax, %edx
	sarl	$3, %edx
	decl	%esi
	cmpl	%esi, %edx
	jge	.LBB17_2
# BB#1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB17_2:
	notl	%eax
	andl	$7, %eax
	movq	16(%rcx), %rcx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	btl	%eax, %edx
	jae	.LBB17_3
# BB#4:                                 # %.preheader.i
	testl	%eax, %eax
	je	.LBB17_5
# BB#6:                                 # %.lr.ph.i
	leal	-1(%rax), %esi
	btl	%esi, %edx
	sbbl	%ecx, %ecx
	andl	$1, %ecx
	cmpl	$1, %eax
	jbe	.LBB17_7
# BB#9:                                 # %.lr.ph.i.1
	leal	-2(%rax), %edi
	btl	%edi, %edx
	adcl	$0, %ecx
	cmpl	$2, %esi
	jl	.LBB17_7
# BB#10:                                # %.lr.ph.i.2
	leal	-3(%rax), %esi
	btl	%esi, %edx
	adcl	$0, %ecx
	cmpl	$2, %edi
	jl	.LBB17_7
# BB#11:                                # %.lr.ph.i.3
	leal	-4(%rax), %edi
	btl	%edi, %edx
	adcl	$0, %ecx
	cmpl	$2, %esi
	jl	.LBB17_7
# BB#12:                                # %.lr.ph.i.4
	leal	-5(%rax), %esi
	btl	%esi, %edx
	adcl	$0, %ecx
	cmpl	$2, %edi
	jl	.LBB17_7
# BB#13:                                # %.lr.ph.i.5
	leal	-6(%rax), %edi
	btl	%edi, %edx
	adcl	$0, %ecx
	cmpl	$2, %esi
	jl	.LBB17_7
# BB#14:                                # %.lr.ph.i.6
	addl	$-7, %eax
	btl	%eax, %edx
	adcl	$0, %ecx
.LBB17_7:                               # %._crit_edge.loopexit.i
	testl	%ecx, %ecx
	sete	%al
	movzbl	%al, %eax
	retq
.LBB17_3:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB17_5:
	movb	$1, %al
	movzbl	%al, %eax
	retq
.Lfunc_end17:
	.size	uvlc_startcode_follows, .Lfunc_end17-uvlc_startcode_follows
	.cfi_endproc

	.globl	code_from_bitstream_2d
	.p2align	4, 0x90
	.type	code_from_bitstream_2d,@function
code_from_bitstream_2d:                 # @code_from_bitstream_2d
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rcx, %r10
	movl	$-1, %eax
	testl	%r9d, %r9d
	jle	.LBB18_15
# BB#1:                                 # %.preheader.lr.ph
	testl	%r8d, %r8d
	jle	.LBB18_15
# BB#2:                                 # %.preheader.us.preheader
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	movq	(%rsi), %rcx
	movl	8(%rcx), %eax
	movq	16(%rcx), %rsi
	movslq	%r8d, %r12
	movl	%eax, %r13d
	notl	%r13d
	movl	%eax, -48(%rsp)         # 4-byte Spill
	sarl	$3, %eax
	cltq
	movq	%rax, -8(%rsp)          # 8-byte Spill
	andl	$7, %r13d
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movslq	12(%rcx), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movl	%r9d, -44(%rsp)         # 4-byte Spill
.LBB18_3:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_4 Depth 2
                                        #       Child Loop BB18_6 Depth 3
	movq	%rax, -40(%rsp)         # 8-byte Spill
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB18_4:                               #   Parent Loop BB18_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB18_6 Depth 3
	movl	(%rdx,%r11,4), %eax
	testl	%eax, %eax
	je	.LBB18_11
# BB#5:                                 #   in Loop: Header=BB18_4 Depth=2
	movl	(%r10,%r11,4), %r15d
	xorl	%edi, %edi
	movl	%eax, %r14d
	movq	-8(%rsp), %r8           # 8-byte Reload
	movl	%r13d, %ecx
	.p2align	4, 0x90
.LBB18_6:                               #   Parent Loop BB18_3 Depth=1
                                        #     Parent Loop BB18_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%r14d, %r14d
	je	.LBB18_10
# BB#7:                                 #   in Loop: Header=BB18_6 Depth=3
	movl	%edi, %ebp
	addl	%ebp, %ebp
	movzbl	(%rsi,%r8), %ebx
	movl	$1, %edi
	shll	%cl, %edi
	andl	%ebx, %edi
	shrl	%cl, %edi
	orl	%ebp, %edi
	decl	%r14d
	leal	-1(%rcx), %r9d
	testl	%ecx, %ecx
	movl	%r9d, %ecx
	jg	.LBB18_6
# BB#8:                                 #   in Loop: Header=BB18_6 Depth=3
	addl	$8, %r9d
	cmpq	-16(%rsp), %r8          # 8-byte Folded Reload
	leaq	1(%r8), %r8
	movl	%r9d, %ecx
	jl	.LBB18_6
# BB#9:                                 #   in Loop: Header=BB18_4 Depth=2
	movl	$-1, %edi
	.p2align	4, 0x90
.LBB18_10:                              # %ShowBits.exit.us
                                        #   in Loop: Header=BB18_4 Depth=2
	cmpl	%r15d, %edi
	je	.LBB18_14
.LBB18_11:                              #   in Loop: Header=BB18_4 Depth=2
	incq	%r11
	cmpq	%r12, %r11
	jl	.LBB18_4
# BB#12:                                # %._crit_edge.us
                                        #   in Loop: Header=BB18_3 Depth=1
	leaq	(%rdx,%r12,4), %rdx
	leaq	(%r10,%r12,4), %r10
	movq	-40(%rsp), %rax         # 8-byte Reload
	incl	%eax
	movl	-44(%rsp), %r9d         # 4-byte Reload
	cmpl	%r9d, %eax
	jl	.LBB18_3
# BB#13:
	movl	$-1, %eax
	jmp	.LBB18_15
.LBB18_14:                              # %.us-lcssa.us
	movq	-24(%rsp), %rcx         # 8-byte Reload
	movl	%r11d, 4(%rcx)
	movq	-40(%rsp), %rdx         # 8-byte Reload
	movl	%edx, 8(%rcx)
	movl	-48(%rsp), %esi         # 4-byte Reload
	addl	%eax, %esi
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movl	%esi, 8(%rdx)
	movl	%eax, 12(%rcx)
	movq	56(%rsp), %rax
	movl	%r15d, (%rax)
	xorl	%eax, %eax
.LBB18_15:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	code_from_bitstream_2d, .Lfunc_end18-code_from_bitstream_2d
	.cfi_endproc

	.globl	ShowBits
	.p2align	4, 0x90
	.type	ShowBits,@function
ShowBits:                               # @ShowBits
	.cfi_startproc
# BB#0:
	movl	%ecx, %r8d
	movl	%esi, %ecx
	notl	%ecx
	sarl	$3, %esi
	movslq	%esi, %r10
	andl	$7, %ecx
	movslq	%edx, %r9
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB19_1:                               # =>This Inner Loop Header: Depth=1
	testl	%r8d, %r8d
	je	.LBB19_5
# BB#2:                                 #   in Loop: Header=BB19_1 Depth=1
	movl	%eax, %edx
	addl	%edx, %edx
	movzbl	(%rdi,%r10), %esi
	movl	$1, %eax
	shll	%cl, %eax
	andl	%esi, %eax
	shrl	%cl, %eax
	orl	%edx, %eax
	decl	%r8d
	leal	-1(%rcx), %edx
	testl	%ecx, %ecx
	movl	%edx, %ecx
	jg	.LBB19_1
# BB#3:                                 #   in Loop: Header=BB19_1 Depth=1
	addl	$8, %edx
	cmpq	%r9, %r10
	leaq	1(%r10), %r10
	movl	%edx, %ecx
	jl	.LBB19_1
# BB#4:
	movl	$-1, %eax
.LBB19_5:                               # %.loopexit
	retq
.Lfunc_end19:
	.size	ShowBits, .Lfunc_end19-ShowBits
	.cfi_endproc

	.globl	GetBits
	.p2align	4, 0x90
	.type	GetBits,@function
GetBits:                                # @GetBits
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 16
.Lcfi50:
	.cfi_offset %rbx, -16
	movl	%ecx, %eax
	movl	%esi, %ecx
	notl	%ecx
	sarl	$3, %esi
	movslq	%esi, %r10
	andl	$7, %ecx
	movslq	%eax, %r9
	xorl	%esi, %esi
	movl	%r8d, %r11d
	.p2align	4, 0x90
.LBB20_1:                               # =>This Inner Loop Header: Depth=1
	testl	%r11d, %r11d
	je	.LBB20_5
# BB#2:                                 #   in Loop: Header=BB20_1 Depth=1
	movl	%esi, %eax
	addl	%eax, %eax
	movzbl	(%rdi,%r10), %ebx
	movl	$1, %esi
	shll	%cl, %esi
	andl	%ebx, %esi
	shrl	%cl, %esi
	orl	%eax, %esi
	decl	%r11d
	leal	-1(%rcx), %eax
	testl	%ecx, %ecx
	movl	%eax, %ecx
	jg	.LBB20_1
# BB#3:                                 #   in Loop: Header=BB20_1 Depth=1
	addl	$8, %eax
	cmpq	%r9, %r10
	leaq	1(%r10), %r10
	movl	%eax, %ecx
	jl	.LBB20_1
# BB#4:
	movl	$-1, %r8d
	jmp	.LBB20_6
.LBB20_5:
	movl	%esi, (%rdx)
.LBB20_6:                               # %.loopexit
	movl	%r8d, %eax
	popq	%rbx
	retq
.Lfunc_end20:
	.size	GetBits, .Lfunc_end20-GetBits
	.cfi_endproc

	.globl	readSyntaxElement_NumCoeffTrailingOnes
	.p2align	4, 0x90
	.type	readSyntaxElement_NumCoeffTrailingOnes,@function
readSyntaxElement_NumCoeffTrailingOnes: # @readSyntaxElement_NumCoeffTrailingOnes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 96
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	(%rsi), %r11
	movl	8(%r11), %r12d
	movq	16(%r11), %r9
	movslq	12(%r11), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movslq	4(%rdi), %rax
	cmpq	$3, %rax
	jne	.LBB21_10
# BB#1:
	movl	%r12d, %ecx
	notl	%ecx
	movl	%r12d, %eax
	sarl	$3, %eax
	movslq	%eax, %rsi
	andl	$7, %ecx
	xorl	%eax, %eax
	movl	$6, %ebp
	.p2align	4, 0x90
.LBB21_2:                               # =>This Inner Loop Header: Depth=1
	testl	%ebp, %ebp
	je	.LBB21_6
# BB#3:                                 #   in Loop: Header=BB21_2 Depth=1
	movl	%eax, %edx
	addl	%edx, %edx
	movzbl	(%r9,%rsi), %ebx
	movl	$1, %eax
	shll	%cl, %eax
	andl	%ebx, %eax
	shrl	%cl, %eax
	orl	%edx, %eax
	decl	%ebp
	leal	-1(%rcx), %edx
	testl	%ecx, %ecx
	movl	%edx, %ecx
	jg	.LBB21_2
# BB#4:                                 #   in Loop: Header=BB21_2 Depth=1
	addl	$8, %edx
	cmpq	16(%rsp), %rsi          # 8-byte Folded Reload
	leaq	1(%rsi), %rsi
	movl	%edx, %ecx
	jl	.LBB21_2
# BB#5:
	movl	$-1, %eax
.LBB21_6:                               # %ShowBits.exit
	addl	$6, %r12d
	movl	%r12d, 8(%r11)
	movl	%eax, %ecx
	andl	$3, %ecx
	movl	%ecx, 8(%rdi)
	sarl	$2, %eax
	movl	%eax, 4(%rdi)
	jne	.LBB21_9
# BB#7:                                 # %ShowBits.exit
	cmpl	$3, %ecx
	jne	.LBB21_9
# BB#8:
	movl	$0, 8(%rdi)
	movl	$6, %r8d
	jmp	.LBB21_20
.LBB21_10:                              # %.preheader.us.i
	movl	%r12d, %r14d
	notl	%r14d
	movl	%r12d, %ecx
	sarl	$3, %ecx
	movslq	%ecx, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	andl	$7, %r14d
	xorl	%r10d, %r10d
	imulq	$272, %rax, %r13        # imm = 0x110
	.p2align	4, 0x90
.LBB21_11:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_13 Depth 2
	movl	readSyntaxElement_NumCoeffTrailingOnes.lentab(%r13,%r10,4), %r8d
	testl	%r8d, %r8d
	je	.LBB21_21
# BB#12:                                #   in Loop: Header=BB21_11 Depth=1
	movl	readSyntaxElement_NumCoeffTrailingOnes.codtab(%r13,%r10,4), %r15d
	xorl	%eax, %eax
	movl	%r8d, %ebx
	movq	24(%rsp), %rbp          # 8-byte Reload
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB21_13:                              #   Parent Loop BB21_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebx, %ebx
	je	.LBB21_17
# BB#14:                                #   in Loop: Header=BB21_13 Depth=2
	movl	%eax, %esi
	addl	%esi, %esi
	movzbl	(%r9,%rbp), %edx
	movl	$1, %eax
	shll	%cl, %eax
	andl	%edx, %eax
	shrl	%cl, %eax
	orl	%esi, %eax
	decl	%ebx
	leal	-1(%rcx), %edx
	testl	%ecx, %ecx
	movl	%edx, %ecx
	jg	.LBB21_13
# BB#15:                                #   in Loop: Header=BB21_13 Depth=2
	addl	$8, %edx
	cmpq	16(%rsp), %rbp          # 8-byte Folded Reload
	leaq	1(%rbp), %rbp
	movl	%edx, %ecx
	jl	.LBB21_13
# BB#16:                                #   in Loop: Header=BB21_11 Depth=1
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB21_17:                              # %ShowBits.exit.us.i
                                        #   in Loop: Header=BB21_11 Depth=1
	cmpl	%r15d, %eax
	je	.LBB21_18
.LBB21_21:                              #   in Loop: Header=BB21_11 Depth=1
	incq	%r10
	cmpq	$17, %r10
	jl	.LBB21_11
# BB#22:                                # %._crit_edge.us.i
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	movq	%r11, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %rsi
	leaq	readSyntaxElement_NumCoeffTrailingOnes.lentab+68(%r13), %r12
	leaq	readSyntaxElement_NumCoeffTrailingOnes.codtab+68(%r13), %r15
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB21_23:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_25 Depth 2
	movl	(%r12,%r10,4), %r8d
	testl	%r8d, %r8d
	je	.LBB21_32
# BB#24:                                #   in Loop: Header=BB21_23 Depth=1
	movl	(%r15,%r10,4), %ebp
	xorl	%eax, %eax
	movl	%r8d, %ebx
	movq	24(%rsp), %r11          # 8-byte Reload
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB21_25:                              #   Parent Loop BB21_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebx, %ebx
	je	.LBB21_29
# BB#26:                                #   in Loop: Header=BB21_25 Depth=2
	movl	%eax, %edx
	addl	%edx, %edx
	movzbl	(%r9,%r11), %edi
	movl	$1, %eax
	shll	%cl, %eax
	andl	%edi, %eax
	shrl	%cl, %eax
	orl	%edx, %eax
	decl	%ebx
	leal	-1(%rcx), %edi
	testl	%ecx, %ecx
	movl	%edi, %ecx
	jg	.LBB21_25
# BB#27:                                #   in Loop: Header=BB21_25 Depth=2
	addl	$8, %edi
	cmpq	16(%rsp), %r11          # 8-byte Folded Reload
	leaq	1(%r11), %r11
	movl	%edi, %ecx
	jl	.LBB21_25
# BB#28:                                #   in Loop: Header=BB21_23 Depth=1
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB21_29:                              # %ShowBits.exit.us.i.1
                                        #   in Loop: Header=BB21_23 Depth=1
	cmpl	%ebp, %eax
	je	.LBB21_30
.LBB21_32:                              #   in Loop: Header=BB21_23 Depth=1
	incq	%r10
	cmpq	$17, %r10
	jl	.LBB21_23
# BB#33:                                # %._crit_edge.us.i.1
	leaq	readSyntaxElement_NumCoeffTrailingOnes.lentab+136(%r13), %r12
	leaq	readSyntaxElement_NumCoeffTrailingOnes.codtab+136(%r13), %r15
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB21_34:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_36 Depth 2
	movl	(%r12,%r10,4), %r8d
	testl	%r8d, %r8d
	je	.LBB21_42
# BB#35:                                #   in Loop: Header=BB21_34 Depth=1
	movl	(%r15,%r10,4), %r11d
	xorl	%eax, %eax
	movl	%r8d, %ebx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB21_36:                              #   Parent Loop BB21_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebx, %ebx
	je	.LBB21_40
# BB#37:                                #   in Loop: Header=BB21_36 Depth=2
	movl	%eax, %edx
	addl	%edx, %edx
	movzbl	(%r9,%rdi), %ebp
	movl	$1, %eax
	shll	%cl, %eax
	andl	%ebp, %eax
	shrl	%cl, %eax
	orl	%edx, %eax
	decl	%ebx
	leal	-1(%rcx), %ebp
	testl	%ecx, %ecx
	movl	%ebp, %ecx
	jg	.LBB21_36
# BB#38:                                #   in Loop: Header=BB21_36 Depth=2
	addl	$8, %ebp
	cmpq	16(%rsp), %rdi          # 8-byte Folded Reload
	leaq	1(%rdi), %rdi
	movl	%ebp, %ecx
	jl	.LBB21_36
# BB#39:                                #   in Loop: Header=BB21_34 Depth=1
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB21_40:                              # %ShowBits.exit.us.i.2
                                        #   in Loop: Header=BB21_34 Depth=1
	cmpl	%r11d, %eax
	je	.LBB21_41
.LBB21_42:                              #   in Loop: Header=BB21_34 Depth=1
	incq	%r10
	cmpq	$17, %r10
	jl	.LBB21_34
# BB#43:                                # %._crit_edge.us.i.2
	leaq	readSyntaxElement_NumCoeffTrailingOnes.lentab+204(%r13), %r12
	leaq	readSyntaxElement_NumCoeffTrailingOnes.codtab+204(%r13), %r15
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB21_44:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_46 Depth 2
	movl	(%r12,%r10,4), %r8d
	testl	%r8d, %r8d
	je	.LBB21_52
# BB#45:                                #   in Loop: Header=BB21_44 Depth=1
	movl	(%r15,%r10,4), %r11d
	xorl	%ebp, %ebp
	movl	%r8d, %ebx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB21_46:                              #   Parent Loop BB21_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebx, %ebx
	je	.LBB21_50
# BB#47:                                #   in Loop: Header=BB21_46 Depth=2
	movl	%ebp, %eax
	addl	%eax, %eax
	movzbl	(%r9,%rdi), %edx
	movl	$1, %ebp
	shll	%cl, %ebp
	andl	%edx, %ebp
	shrl	%cl, %ebp
	orl	%eax, %ebp
	decl	%ebx
	leal	-1(%rcx), %eax
	testl	%ecx, %ecx
	movl	%eax, %ecx
	jg	.LBB21_46
# BB#48:                                #   in Loop: Header=BB21_46 Depth=2
	addl	$8, %eax
	cmpq	16(%rsp), %rdi          # 8-byte Folded Reload
	leaq	1(%rdi), %rdi
	movl	%eax, %ecx
	jl	.LBB21_46
# BB#49:                                #   in Loop: Header=BB21_44 Depth=1
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB21_50:                              # %ShowBits.exit.us.i.3
                                        #   in Loop: Header=BB21_44 Depth=1
	cmpl	%r11d, %ebp
	je	.LBB21_51
.LBB21_52:                              #   in Loop: Header=BB21_44 Depth=1
	incq	%r10
	cmpq	$17, %r10
	jl	.LBB21_44
# BB#53:                                # %._crit_edge.us.i.3
	movl	$.Lstr, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.LBB21_9:
	incl	%eax
	movl	%eax, 4(%rdi)
	movl	$6, %r8d
	jmp	.LBB21_20
.LBB21_18:
	xorl	%eax, %eax
	jmp	.LBB21_19
.LBB21_30:
	movl	$1, %eax
	jmp	.LBB21_31
.LBB21_51:
	movl	$3, %eax
	jmp	.LBB21_31
.LBB21_41:
	movl	$2, %eax
.LBB21_31:                              # %.us-lcssa.us.i
	movq	%rsi, %rdi
	movq	32(%rsp), %r11          # 8-byte Reload
	movl	12(%rsp), %r12d         # 4-byte Reload
.LBB21_19:                              # %.us-lcssa.us.i
	movl	%r10d, 4(%rdi)
	movl	%eax, 8(%rdi)
	addl	%r8d, %r12d
	movl	%r12d, 8(%r11)
.LBB21_20:
	movl	%r8d, 12(%rdi)
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	readSyntaxElement_NumCoeffTrailingOnes, .Lfunc_end21-readSyntaxElement_NumCoeffTrailingOnes
	.cfi_endproc

	.globl	readSyntaxElement_NumCoeffTrailingOnesChromaDC
	.p2align	4, 0x90
	.type	readSyntaxElement_NumCoeffTrailingOnesChromaDC,@function
readSyntaxElement_NumCoeffTrailingOnesChromaDC: # @readSyntaxElement_NumCoeffTrailingOnesChromaDC
	.cfi_startproc
# BB#0:                                 # %.preheader.us.i
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi70:
	.cfi_def_cfa_offset 96
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	active_sps(%rip), %rax
	movl	32(%rax), %eax
	decl	%eax
	movslq	%eax, %rcx
	movq	(%rsi), %rdi
	movl	8(%rdi), %r12d
	movl	%r12d, %r14d
	notl	%r14d
	movq	16(%rdi), %r10
	movl	%r12d, %edx
	sarl	$3, %edx
	movslq	%edx, %rax
	andl	$7, %r14d
	movslq	12(%rdi), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	imulq	$272, %rcx, %r13        # imm = 0x110
	.p2align	4, 0x90
.LBB22_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_3 Depth 2
	movl	readSyntaxElement_NumCoeffTrailingOnesChromaDC.lentab(%r13,%r11,4), %r8d
	testl	%r8d, %r8d
	je	.LBB22_10
# BB#2:                                 #   in Loop: Header=BB22_1 Depth=1
	movl	readSyntaxElement_NumCoeffTrailingOnesChromaDC.codtab(%r13,%r11,4), %r15d
	xorl	%edx, %edx
	movl	%r8d, %ebx
	movq	%rax, %r9
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB22_3:                               #   Parent Loop BB22_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebx, %ebx
	je	.LBB22_7
# BB#4:                                 #   in Loop: Header=BB22_3 Depth=2
	movl	%edx, %ebp
	addl	%ebp, %ebp
	movzbl	(%r10,%rax), %esi
	movl	$1, %edx
	shll	%cl, %edx
	andl	%esi, %edx
	shrl	%cl, %edx
	orl	%ebp, %edx
	decl	%ebx
	leal	-1(%rcx), %ebp
	testl	%ecx, %ecx
	movl	%ebp, %ecx
	jg	.LBB22_3
# BB#5:                                 #   in Loop: Header=BB22_3 Depth=2
	addl	$8, %ebp
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	leaq	1(%rax), %rax
	movl	%ebp, %ecx
	jl	.LBB22_3
# BB#6:                                 #   in Loop: Header=BB22_1 Depth=1
	movl	$-1, %edx
	.p2align	4, 0x90
.LBB22_7:                               # %ShowBits.exit.us.i
                                        #   in Loop: Header=BB22_1 Depth=1
	cmpl	%r15d, %edx
	movq	%r9, %rax
	je	.LBB22_8
.LBB22_10:                              #   in Loop: Header=BB22_1 Depth=1
	incq	%r11
	cmpq	$17, %r11
	jl	.LBB22_1
# BB#11:                                # %._crit_edge.us.i
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	leaq	readSyntaxElement_NumCoeffTrailingOnesChromaDC.lentab+68(%r13), %r12
	leaq	readSyntaxElement_NumCoeffTrailingOnesChromaDC.codtab+68(%r13), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB22_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_14 Depth 2
	movl	(%r12,%r11,4), %r8d
	testl	%r8d, %r8d
	je	.LBB22_21
# BB#13:                                #   in Loop: Header=BB22_12 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%r11,4), %r15d
	xorl	%edx, %edx
	movl	%r8d, %ebx
	movq	%rax, %rdi
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB22_14:                              #   Parent Loop BB22_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebx, %ebx
	je	.LBB22_18
# BB#15:                                #   in Loop: Header=BB22_14 Depth=2
	movl	%edx, %esi
	addl	%esi, %esi
	movzbl	(%r10,%rdi), %ebp
	movl	$1, %edx
	shll	%cl, %edx
	andl	%ebp, %edx
	shrl	%cl, %edx
	orl	%esi, %edx
	decl	%ebx
	leal	-1(%rcx), %r9d
	testl	%ecx, %ecx
	movl	%r9d, %ecx
	jg	.LBB22_14
# BB#16:                                #   in Loop: Header=BB22_14 Depth=2
	addl	$8, %r9d
	cmpq	16(%rsp), %rdi          # 8-byte Folded Reload
	leaq	1(%rdi), %rdi
	movl	%r9d, %ecx
	jl	.LBB22_14
# BB#17:                                #   in Loop: Header=BB22_12 Depth=1
	movl	$-1, %edx
	.p2align	4, 0x90
.LBB22_18:                              # %ShowBits.exit.us.i.1
                                        #   in Loop: Header=BB22_12 Depth=1
	cmpl	%r15d, %edx
	je	.LBB22_19
.LBB22_21:                              #   in Loop: Header=BB22_12 Depth=1
	incq	%r11
	cmpq	$17, %r11
	jl	.LBB22_12
# BB#22:                                # %._crit_edge.us.i.1
	leaq	readSyntaxElement_NumCoeffTrailingOnesChromaDC.lentab+136(%r13), %r12
	leaq	readSyntaxElement_NumCoeffTrailingOnesChromaDC.codtab+136(%r13), %r15
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB22_23:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_25 Depth 2
	movl	(%r12,%r11,4), %r8d
	testl	%r8d, %r8d
	je	.LBB22_31
# BB#24:                                #   in Loop: Header=BB22_23 Depth=1
	movl	(%r15,%r11,4), %r9d
	xorl	%edx, %edx
	movl	%r8d, %ebx
	movq	%rax, %rdi
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB22_25:                              #   Parent Loop BB22_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebx, %ebx
	je	.LBB22_29
# BB#26:                                #   in Loop: Header=BB22_25 Depth=2
	movl	%edx, %esi
	addl	%esi, %esi
	movzbl	(%r10,%rdi), %ebp
	movl	$1, %edx
	shll	%cl, %edx
	andl	%ebp, %edx
	shrl	%cl, %edx
	orl	%esi, %edx
	decl	%ebx
	leal	-1(%rcx), %ebp
	testl	%ecx, %ecx
	movl	%ebp, %ecx
	jg	.LBB22_25
# BB#27:                                #   in Loop: Header=BB22_25 Depth=2
	addl	$8, %ebp
	cmpq	16(%rsp), %rdi          # 8-byte Folded Reload
	leaq	1(%rdi), %rdi
	movl	%ebp, %ecx
	jl	.LBB22_25
# BB#28:                                #   in Loop: Header=BB22_23 Depth=1
	movl	$-1, %edx
	.p2align	4, 0x90
.LBB22_29:                              # %ShowBits.exit.us.i.2
                                        #   in Loop: Header=BB22_23 Depth=1
	cmpl	%r9d, %edx
	je	.LBB22_30
.LBB22_31:                              #   in Loop: Header=BB22_23 Depth=1
	incq	%r11
	cmpq	$17, %r11
	jl	.LBB22_23
# BB#32:                                # %._crit_edge.us.i.2
	leaq	readSyntaxElement_NumCoeffTrailingOnesChromaDC.lentab+204(%r13), %r12
	leaq	readSyntaxElement_NumCoeffTrailingOnesChromaDC.codtab+204(%r13), %r15
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB22_33:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_35 Depth 2
	movl	(%r12,%r11,4), %r8d
	testl	%r8d, %r8d
	je	.LBB22_41
# BB#34:                                #   in Loop: Header=BB22_33 Depth=1
	movl	(%r15,%r11,4), %r9d
	xorl	%ebp, %ebp
	movl	%r8d, %ebx
	movq	%rax, %rdi
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB22_35:                              #   Parent Loop BB22_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebx, %ebx
	je	.LBB22_39
# BB#36:                                #   in Loop: Header=BB22_35 Depth=2
	movl	%ebp, %edx
	addl	%edx, %edx
	movzbl	(%r10,%rdi), %esi
	movl	$1, %ebp
	shll	%cl, %ebp
	andl	%esi, %ebp
	shrl	%cl, %ebp
	orl	%edx, %ebp
	decl	%ebx
	leal	-1(%rcx), %edx
	testl	%ecx, %ecx
	movl	%edx, %ecx
	jg	.LBB22_35
# BB#37:                                #   in Loop: Header=BB22_35 Depth=2
	addl	$8, %edx
	cmpq	16(%rsp), %rdi          # 8-byte Folded Reload
	leaq	1(%rdi), %rdi
	movl	%edx, %ecx
	jl	.LBB22_35
# BB#38:                                #   in Loop: Header=BB22_33 Depth=1
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB22_39:                              # %ShowBits.exit.us.i.3
                                        #   in Loop: Header=BB22_33 Depth=1
	cmpl	%r9d, %ebp
	je	.LBB22_40
.LBB22_41:                              #   in Loop: Header=BB22_33 Depth=1
	incq	%r11
	cmpq	$17, %r11
	jl	.LBB22_33
# BB#42:                                # %._crit_edge.us.i.3
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.LBB22_8:
	xorl	%eax, %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	jmp	.LBB22_9
.LBB22_19:
	movl	$1, %eax
	jmp	.LBB22_20
.LBB22_40:
	movl	$3, %eax
	jmp	.LBB22_20
.LBB22_30:
	movl	$2, %eax
.LBB22_20:                              # %.loopexit
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	4(%rsp), %r12d          # 4-byte Reload
.LBB22_9:                               # %.loopexit
	movl	%r11d, 4(%rcx)
	movl	%eax, 8(%rcx)
	addl	%r8d, %r12d
	movl	%r12d, 8(%rdi)
	movl	%r8d, 12(%rcx)
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	readSyntaxElement_NumCoeffTrailingOnesChromaDC, .Lfunc_end22-readSyntaxElement_NumCoeffTrailingOnesChromaDC
	.cfi_endproc

	.globl	readSyntaxElement_Level_VLC0
	.p2align	4, 0x90
	.type	readSyntaxElement_Level_VLC0,@function
readSyntaxElement_Level_VLC0:           # @readSyntaxElement_Level_VLC0
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 48
.Lcfi82:
	.cfi_offset %rbx, -48
.Lcfi83:
	.cfi_offset %r12, -40
.Lcfi84:
	.cfi_offset %r14, -32
.Lcfi85:
	.cfi_offset %r15, -24
.Lcfi86:
	.cfi_offset %rbp, -16
	movq	(%rsi), %r9
	movl	8(%r9), %r10d
	movq	16(%r9), %r12
	movslq	12(%r9), %r11
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB23_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_2 Depth 2
	movl	%esi, %r15d
	leal	(%r15,%r10), %eax
	movl	%eax, %ecx
	notl	%ecx
	sarl	$3, %eax
	cltq
	andl	$7, %ecx
	movl	$1, %esi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB23_2:                               #   Parent Loop BB23_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%esi, %esi
	je	.LBB23_6
# BB#3:                                 #   in Loop: Header=BB23_2 Depth=2
	movl	%ebp, %ebx
	addl	%ebx, %ebx
	movzbl	(%r12,%rax), %edx
	movl	$1, %ebp
	shll	%cl, %ebp
	andl	%edx, %ebp
	shrl	%cl, %ebp
	orl	%ebx, %ebp
	decl	%esi
	leal	-1(%rcx), %ebx
	testl	%ecx, %ecx
	movl	%ebx, %ecx
	jg	.LBB23_2
# BB#4:                                 #   in Loop: Header=BB23_2 Depth=2
	addl	$8, %ebx
	cmpq	%r11, %rax
	leaq	1(%rax), %rax
	movl	%ebx, %ecx
	jl	.LBB23_2
	jmp	.LBB23_5
	.p2align	4, 0x90
.LBB23_6:                               # %ShowBits.exit
                                        #   in Loop: Header=BB23_1 Depth=1
	leal	1(%r15), %esi
	testl	%ebp, %ebp
	je	.LBB23_1
	jmp	.LBB23_7
.LBB23_5:                               # %ShowBits.exit.thread
	leal	1(%r15), %esi
.LBB23_7:                               # %.loopexit
	addl	%esi, %r10d
	cmpl	$14, %esi
	jg	.LBB23_9
# BB#8:
	movl	%r15d, %ebp
	andl	$1, %ebp
	movl	%r15d, %eax
	shrl	$31, %eax
	addl	%r15d, %eax
	sarl	%eax
	incl	%eax
	jmp	.LBB23_24
.LBB23_9:
	cmpl	$15, %esi
	jne	.LBB23_16
# BB#10:
	movl	%r10d, %ecx
	notl	%ecx
	movl	%r10d, %eax
	sarl	$3, %eax
	movslq	%eax, %rsi
	andl	$7, %ecx
	xorl	%eax, %eax
	movl	$4, %ebp
	.p2align	4, 0x90
.LBB23_11:                              # =>This Inner Loop Header: Depth=1
	testl	%ebp, %ebp
	je	.LBB23_15
# BB#12:                                #   in Loop: Header=BB23_11 Depth=1
	movl	%eax, %edx
	addl	%edx, %edx
	movzbl	(%r12,%rsi), %ebx
	movl	$1, %eax
	shll	%cl, %eax
	andl	%ebx, %eax
	shrl	%cl, %eax
	orl	%edx, %eax
	decl	%ebp
	leal	-1(%rcx), %ebx
	testl	%ecx, %ecx
	movl	%ebx, %ecx
	jg	.LBB23_11
# BB#13:                                #   in Loop: Header=BB23_11 Depth=1
	addl	$8, %ebx
	cmpq	%r11, %rsi
	leaq	1(%rsi), %rsi
	movl	%ebx, %ecx
	jl	.LBB23_11
# BB#14:
	movl	$-1, %eax
.LBB23_15:                              # %ShowBits.exit88
	addl	$5, %r15d
	addl	$4, %r10d
	movl	%eax, %ebp
	andl	$1, %ebp
	shrl	%eax
	andl	$7, %eax
	orl	$8, %eax
	movl	%r15d, %esi
	jmp	.LBB23_24
.LBB23_16:
	xorl	%ebp, %ebp
	cmpl	$15, %r15d
	jl	.LBB23_17
# BB#18:
	leal	-15(%r15), %r8d
	addl	$-3, %r15d
	movl	%r10d, %ecx
	notl	%ecx
	movl	%r10d, %eax
	sarl	$3, %eax
	movslq	%eax, %r14
	andl	$7, %ecx
	xorl	%eax, %eax
	movl	%r15d, %ebp
	.p2align	4, 0x90
.LBB23_19:                              # =>This Inner Loop Header: Depth=1
	testl	%ebp, %ebp
	je	.LBB23_23
# BB#20:                                #   in Loop: Header=BB23_19 Depth=1
	movl	%eax, %edx
	addl	%edx, %edx
	movzbl	(%r12,%r14), %ebx
	movl	$1, %eax
	shll	%cl, %eax
	andl	%ebx, %eax
	shrl	%cl, %eax
	orl	%edx, %eax
	decl	%ebp
	leal	-1(%rcx), %ebx
	testl	%ecx, %ecx
	movl	%ebx, %ecx
	jg	.LBB23_19
# BB#21:                                #   in Loop: Header=BB23_19 Depth=1
	addl	$8, %ebx
	cmpq	%r11, %r14
	leaq	1(%r14), %r14
	movl	%ebx, %ecx
	jl	.LBB23_19
# BB#22:
	movl	$-1, %eax
.LBB23_23:                              # %ShowBits.exit78
	addl	%r15d, %r10d
	movl	%eax, %ebp
	andl	$1, %ebp
	movl	$2048, %edx             # imm = 0x800
	movl	%r8d, %ecx
	shll	%cl, %edx
	sarl	%eax
	leal	-2032(%rdx,%rax), %eax
	addl	%r15d, %esi
	jmp	.LBB23_24
.LBB23_17:
	xorl	%eax, %eax
.LBB23_24:
	movl	%eax, %ecx
	negl	%ecx
	testl	%ebp, %ebp
	cmovel	%eax, %ecx
	movl	%ecx, 16(%rdi)
	movl	%esi, 12(%rdi)
	movl	%r10d, 8(%r9)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	readSyntaxElement_Level_VLC0, .Lfunc_end23-readSyntaxElement_Level_VLC0
	.cfi_endproc

	.globl	readSyntaxElement_Level_VLCN
	.p2align	4, 0x90
	.type	readSyntaxElement_Level_VLCN,@function
readSyntaxElement_Level_VLCN:           # @readSyntaxElement_Level_VLCN
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi89:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi90:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi91:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 56
.Lcfi93:
	.cfi_offset %rbx, -56
.Lcfi94:
	.cfi_offset %r12, -48
.Lcfi95:
	.cfi_offset %r13, -40
.Lcfi96:
	.cfi_offset %r14, -32
.Lcfi97:
	.cfi_offset %r15, -24
.Lcfi98:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	movq	(%rdx), %rdi
	movl	8(%rdi), %r9d
	movq	16(%rdi), %r12
	leal	-1(%rsi), %r15d
	movl	$15, %r14d
	movl	%r15d, %ecx
	shll	%cl, %r14d
	movslq	12(%rdi), %r10
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB24_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_2 Depth 2
	movl	%r11d, %r8d
	leal	(%r8,%r9), %eax
	movl	%eax, %ecx
	notl	%ecx
	sarl	$3, %eax
	movslq	%eax, %r13
	andl	$7, %ecx
	movl	$1, %ebp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB24_2:                               #   Parent Loop BB24_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebp, %ebp
	je	.LBB24_6
# BB#3:                                 #   in Loop: Header=BB24_2 Depth=2
	movl	%eax, %edx
	addl	%edx, %edx
	movzbl	(%r12,%r13), %ebx
	movl	$1, %eax
	shll	%cl, %eax
	andl	%ebx, %eax
	shrl	%cl, %eax
	orl	%edx, %eax
	decl	%ebp
	leal	-1(%rcx), %edx
	testl	%ecx, %ecx
	movl	%edx, %ecx
	jg	.LBB24_2
# BB#4:                                 #   in Loop: Header=BB24_2 Depth=2
	addl	$8, %edx
	cmpq	%r10, %r13
	leaq	1(%r13), %r13
	movl	%edx, %ecx
	jl	.LBB24_2
	jmp	.LBB24_5
	.p2align	4, 0x90
.LBB24_6:                               # %ShowBits.exit
                                        #   in Loop: Header=BB24_1 Depth=1
	leal	1(%r8), %r11d
	testl	%eax, %eax
	je	.LBB24_1
	jmp	.LBB24_7
.LBB24_5:                               # %ShowBits.exit.thread
	leal	1(%r8), %r11d
.LBB24_7:                               # %.loopexit
	cmpl	$14, %r8d
	jg	.LBB24_16
# BB#8:
	movq	%rdi, %r13
	movl	%r8d, %r14d
	movl	%r15d, %ecx
	shll	%cl, %r14d
	incl	%r14d
	testl	%r15d, %r15d
	je	.LBB24_9
# BB#10:
	addl	%r9d, %r11d
	movl	%r11d, %ecx
	notl	%ecx
	sarl	$3, %r11d
	movslq	%r11d, %rbx
	andl	$7, %ecx
	xorl	%ebp, %ebp
	movq	-8(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB24_11:                              # =>This Inner Loop Header: Depth=1
	testl	%r15d, %r15d
	je	.LBB24_15
# BB#12:                                #   in Loop: Header=BB24_11 Depth=1
	movl	%ebp, %eax
	addl	%eax, %eax
	movzbl	(%r12,%rbx), %edx
	movl	$1, %ebp
	shll	%cl, %ebp
	andl	%edx, %ebp
	shrl	%cl, %ebp
	orl	%eax, %ebp
	decl	%r15d
	leal	-1(%rcx), %eax
	testl	%ecx, %ecx
	movl	%eax, %ecx
	jg	.LBB24_11
# BB#13:                                #   in Loop: Header=BB24_11 Depth=1
	addl	$8, %eax
	cmpq	%r10, %rbx
	leaq	1(%rbx), %rbx
	movl	%eax, %ecx
	jl	.LBB24_11
# BB#14:
	movl	$-1, %ebp
.LBB24_15:                              # %ShowBits.exit124
	addl	%ebp, %r14d
	addl	%esi, %r8d
	movl	%r8d, %r11d
	jmp	.LBB24_23
.LBB24_16:
	leal	-15(%r8), %r15d
	leal	(%r11,%r9), %eax
	movl	%eax, %ecx
	notl	%ecx
	addl	$-4, %r8d
	sarl	$3, %eax
	movslq	%eax, %r13
	andl	$7, %ecx
	xorl	%esi, %esi
	movl	%r8d, %eax
	.p2align	4, 0x90
.LBB24_17:                              # =>This Inner Loop Header: Depth=1
	testl	%eax, %eax
	je	.LBB24_18
# BB#19:                                #   in Loop: Header=BB24_17 Depth=1
	movl	%esi, %edx
	addl	%edx, %edx
	movzbl	(%r12,%r13), %ebp
	movl	$1, %esi
	shll	%cl, %esi
	andl	%ebp, %esi
	shrl	%cl, %esi
	orl	%edx, %esi
	decl	%eax
	leal	-1(%rcx), %edx
	testl	%ecx, %ecx
	movl	%edx, %ecx
	jg	.LBB24_17
# BB#20:                                #   in Loop: Header=BB24_17 Depth=1
	addl	$8, %edx
	cmpq	%r10, %r13
	leaq	1(%r13), %r13
	movl	%edx, %ecx
	jl	.LBB24_17
# BB#21:
	movq	%rdi, %r13
	movl	$-1, %esi
	jmp	.LBB24_22
.LBB24_18:
	movq	%rdi, %r13
.LBB24_22:                              # %ShowBits.exit114
	movq	-8(%rsp), %rdi          # 8-byte Reload
	addl	%r8d, %r11d
	movl	$2048, %eax             # imm = 0x800
	movl	%r15d, %ecx
	shll	%cl, %eax
	addl	%eax, %r14d
	leal	-2047(%rsi,%r14), %r14d
	jmp	.LBB24_23
.LBB24_9:
	movq	-8(%rsp), %rdi          # 8-byte Reload
.LBB24_23:
	leal	(%r11,%r9), %eax
	movl	%eax, %ecx
	notl	%ecx
	sarl	$3, %eax
	movslq	%eax, %rsi
	andl	$7, %ecx
	xorl	%eax, %eax
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB24_24:                              # =>This Inner Loop Header: Depth=1
	testl	%ebp, %ebp
	je	.LBB24_28
# BB#25:                                #   in Loop: Header=BB24_24 Depth=1
	movl	%eax, %edx
	addl	%edx, %edx
	movzbl	(%r12,%rsi), %ebx
	movl	$1, %eax
	shll	%cl, %eax
	andl	%ebx, %eax
	shrl	%cl, %eax
	orl	%edx, %eax
	decl	%ebp
	leal	-1(%rcx), %edx
	testl	%ecx, %ecx
	movl	%edx, %ecx
	jg	.LBB24_24
# BB#26:                                #   in Loop: Header=BB24_24 Depth=1
	addl	$8, %edx
	cmpq	%r10, %rsi
	leaq	1(%rsi), %rsi
	movl	%edx, %ecx
	jl	.LBB24_24
# BB#27:
	movl	$-1, %eax
.LBB24_28:                              # %ShowBits.exit104
	leal	1(%r11), %ecx
	movl	%r14d, %edx
	negl	%edx
	testl	%eax, %eax
	cmovel	%r14d, %edx
	movl	%edx, 16(%rdi)
	movl	%ecx, 12(%rdi)
	leal	1(%r11,%r9), %eax
	movl	%eax, 8(%r13)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	readSyntaxElement_Level_VLCN, .Lfunc_end24-readSyntaxElement_Level_VLCN
	.cfi_endproc

	.globl	readSyntaxElement_TotalZeros
	.p2align	4, 0x90
	.type	readSyntaxElement_TotalZeros,@function
readSyntaxElement_TotalZeros:           # @readSyntaxElement_TotalZeros
	.cfi_startproc
# BB#0:                                 # %.preheader.us.i
	pushq	%rbp
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi102:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi103:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi105:
	.cfi_def_cfa_offset 80
.Lcfi106:
	.cfi_offset %rbx, -56
.Lcfi107:
	.cfi_offset %r12, -48
.Lcfi108:
	.cfi_offset %r13, -40
.Lcfi109:
	.cfi_offset %r14, -32
.Lcfi110:
	.cfi_offset %r15, -24
.Lcfi111:
	.cfi_offset %rbp, -16
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movslq	4(%rdi), %r15
	movq	(%rsi), %rcx
	movl	8(%rcx), %eax
	movl	%eax, %r11d
	notl	%r11d
	movq	16(%rcx), %rsi
	movl	%eax, 4(%rsp)           # 4-byte Spill
	sarl	$3, %eax
	movslq	%eax, %r10
	andl	$7, %r11d
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movslq	12(%rcx), %r14
	xorl	%r9d, %r9d
	shlq	$6, %r15
	.p2align	4, 0x90
.LBB25_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_3 Depth 2
	movl	readSyntaxElement_TotalZeros.lentab(%r15,%r9,4), %r13d
	testl	%r13d, %r13d
	je	.LBB25_8
# BB#2:                                 #   in Loop: Header=BB25_1 Depth=1
	movl	readSyntaxElement_TotalZeros.codtab(%r15,%r9,4), %r12d
	xorl	%eax, %eax
	movl	%r13d, %ebp
	movq	%r10, %rbx
	movl	%r11d, %ecx
	.p2align	4, 0x90
.LBB25_3:                               #   Parent Loop BB25_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebp, %ebp
	je	.LBB25_7
# BB#4:                                 #   in Loop: Header=BB25_3 Depth=2
	movl	%eax, %edi
	addl	%edi, %edi
	movzbl	(%rsi,%rbx), %edx
	movl	$1, %eax
	shll	%cl, %eax
	andl	%edx, %eax
	shrl	%cl, %eax
	orl	%edi, %eax
	decl	%ebp
	leal	-1(%rcx), %r8d
	testl	%ecx, %ecx
	movl	%r8d, %ecx
	jg	.LBB25_3
# BB#5:                                 #   in Loop: Header=BB25_3 Depth=2
	addl	$8, %r8d
	cmpq	%r14, %rbx
	leaq	1(%rbx), %rbx
	movl	%r8d, %ecx
	jl	.LBB25_3
# BB#6:                                 #   in Loop: Header=BB25_1 Depth=1
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB25_7:                               # %ShowBits.exit.us.i
                                        #   in Loop: Header=BB25_1 Depth=1
	cmpl	%r12d, %eax
	je	.LBB25_10
.LBB25_8:                               #   in Loop: Header=BB25_1 Depth=1
	incq	%r9
	cmpq	$16, %r9
	jl	.LBB25_1
# BB#9:                                 # %._crit_edge.us.i
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.LBB25_10:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r9d, 4(%rax)
	movl	$0, 8(%rax)
	movl	4(%rsp), %edx           # 4-byte Reload
	addl	%r13d, %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%edx, 8(%rcx)
	movl	%r13d, 12(%rax)
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	readSyntaxElement_TotalZeros, .Lfunc_end25-readSyntaxElement_TotalZeros
	.cfi_endproc

	.globl	readSyntaxElement_TotalZerosChromaDC
	.p2align	4, 0x90
	.type	readSyntaxElement_TotalZerosChromaDC,@function
readSyntaxElement_TotalZerosChromaDC:   # @readSyntaxElement_TotalZerosChromaDC
	.cfi_startproc
# BB#0:                                 # %.preheader.us.i
	pushq	%rbp
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi114:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi115:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi116:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi118:
	.cfi_def_cfa_offset 80
.Lcfi119:
	.cfi_offset %rbx, -56
.Lcfi120:
	.cfi_offset %r12, -48
.Lcfi121:
	.cfi_offset %r13, -40
.Lcfi122:
	.cfi_offset %r14, -32
.Lcfi123:
	.cfi_offset %r15, -24
.Lcfi124:
	.cfi_offset %rbp, -16
	movq	active_sps(%rip), %rax
	movl	32(%rax), %eax
	decl	%eax
	movslq	%eax, %rcx
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movslq	4(%rdi), %rbp
	movq	(%rsi), %rsi
	movl	8(%rsi), %edx
	movl	%edx, %r14d
	notl	%r14d
	movq	16(%rsi), %rax
	movl	%edx, 4(%rsp)           # 4-byte Spill
	sarl	$3, %edx
	movslq	%edx, %r10
	andl	$7, %r14d
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movslq	12(%rsi), %r11
	xorl	%r9d, %r9d
	shlq	$6, %rbp
	imulq	$960, %rcx, %r12        # imm = 0x3C0
	addq	%rbp, %r12
	.p2align	4, 0x90
.LBB26_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_3 Depth 2
	movl	readSyntaxElement_TotalZerosChromaDC.lentab(%r12,%r9,4), %r13d
	testl	%r13d, %r13d
	je	.LBB26_8
# BB#2:                                 #   in Loop: Header=BB26_1 Depth=1
	movl	readSyntaxElement_TotalZerosChromaDC.codtab(%r12,%r9,4), %r15d
	xorl	%esi, %esi
	movl	%r13d, %ebp
	movq	%r10, %rbx
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB26_3:                               #   Parent Loop BB26_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebp, %ebp
	je	.LBB26_7
# BB#4:                                 #   in Loop: Header=BB26_3 Depth=2
	movl	%esi, %edi
	addl	%edi, %edi
	movzbl	(%rax,%rbx), %edx
	movl	$1, %esi
	shll	%cl, %esi
	andl	%edx, %esi
	shrl	%cl, %esi
	orl	%edi, %esi
	decl	%ebp
	leal	-1(%rcx), %r8d
	testl	%ecx, %ecx
	movl	%r8d, %ecx
	jg	.LBB26_3
# BB#5:                                 #   in Loop: Header=BB26_3 Depth=2
	addl	$8, %r8d
	cmpq	%r11, %rbx
	leaq	1(%rbx), %rbx
	movl	%r8d, %ecx
	jl	.LBB26_3
# BB#6:                                 #   in Loop: Header=BB26_1 Depth=1
	movl	$-1, %esi
	.p2align	4, 0x90
.LBB26_7:                               # %ShowBits.exit.us.i
                                        #   in Loop: Header=BB26_1 Depth=1
	cmpl	%r15d, %esi
	je	.LBB26_10
.LBB26_8:                               #   in Loop: Header=BB26_1 Depth=1
	incq	%r9
	cmpq	$16, %r9
	jl	.LBB26_1
# BB#9:                                 # %._crit_edge.us.i
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.LBB26_10:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r9d, 4(%rax)
	movl	$0, 8(%rax)
	movl	4(%rsp), %edx           # 4-byte Reload
	addl	%r13d, %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%edx, 8(%rcx)
	movl	%r13d, 12(%rax)
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	readSyntaxElement_TotalZerosChromaDC, .Lfunc_end26-readSyntaxElement_TotalZerosChromaDC
	.cfi_endproc

	.globl	readSyntaxElement_Run
	.p2align	4, 0x90
	.type	readSyntaxElement_Run,@function
readSyntaxElement_Run:                  # @readSyntaxElement_Run
	.cfi_startproc
# BB#0:                                 # %.preheader.us.i
	pushq	%rbp
.Lcfi125:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi126:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi128:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi129:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi131:
	.cfi_def_cfa_offset 80
.Lcfi132:
	.cfi_offset %rbx, -56
.Lcfi133:
	.cfi_offset %r12, -48
.Lcfi134:
	.cfi_offset %r13, -40
.Lcfi135:
	.cfi_offset %r14, -32
.Lcfi136:
	.cfi_offset %r15, -24
.Lcfi137:
	.cfi_offset %rbp, -16
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movslq	4(%rdi), %r15
	movq	(%rsi), %rcx
	movl	8(%rcx), %eax
	movl	%eax, %r11d
	notl	%r11d
	movq	16(%rcx), %rsi
	movl	%eax, 4(%rsp)           # 4-byte Spill
	sarl	$3, %eax
	movslq	%eax, %r10
	andl	$7, %r11d
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movslq	12(%rcx), %r14
	xorl	%r9d, %r9d
	shlq	$6, %r15
	.p2align	4, 0x90
.LBB27_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_3 Depth 2
	movl	readSyntaxElement_Run.lentab(%r15,%r9,4), %r13d
	testl	%r13d, %r13d
	je	.LBB27_8
# BB#2:                                 #   in Loop: Header=BB27_1 Depth=1
	movl	readSyntaxElement_Run.codtab(%r15,%r9,4), %r12d
	xorl	%eax, %eax
	movl	%r13d, %ebp
	movq	%r10, %rbx
	movl	%r11d, %ecx
	.p2align	4, 0x90
.LBB27_3:                               #   Parent Loop BB27_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebp, %ebp
	je	.LBB27_7
# BB#4:                                 #   in Loop: Header=BB27_3 Depth=2
	movl	%eax, %edi
	addl	%edi, %edi
	movzbl	(%rsi,%rbx), %edx
	movl	$1, %eax
	shll	%cl, %eax
	andl	%edx, %eax
	shrl	%cl, %eax
	orl	%edi, %eax
	decl	%ebp
	leal	-1(%rcx), %r8d
	testl	%ecx, %ecx
	movl	%r8d, %ecx
	jg	.LBB27_3
# BB#5:                                 #   in Loop: Header=BB27_3 Depth=2
	addl	$8, %r8d
	cmpq	%r14, %rbx
	leaq	1(%rbx), %rbx
	movl	%r8d, %ecx
	jl	.LBB27_3
# BB#6:                                 #   in Loop: Header=BB27_1 Depth=1
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB27_7:                               # %ShowBits.exit.us.i
                                        #   in Loop: Header=BB27_1 Depth=1
	cmpl	%r12d, %eax
	je	.LBB27_10
.LBB27_8:                               #   in Loop: Header=BB27_1 Depth=1
	incq	%r9
	cmpq	$16, %r9
	jl	.LBB27_1
# BB#9:                                 # %._crit_edge.us.i
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.LBB27_10:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r9d, 4(%rax)
	movl	$0, 8(%rax)
	movl	4(%rsp), %edx           # 4-byte Reload
	addl	%r13d, %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%edx, 8(%rcx)
	movl	%r13d, 12(%rax)
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	readSyntaxElement_Run, .Lfunc_end27-readSyntaxElement_Run
	.cfi_endproc

	.globl	peekSyntaxElement_UVLC
	.p2align	4, 0x90
	.type	peekSyntaxElement_UVLC,@function
peekSyntaxElement_UVLC:                 # @peekSyntaxElement_UVLC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi138:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi139:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi140:
	.cfi_def_cfa_offset 32
.Lcfi141:
	.cfi_offset %rbx, -32
.Lcfi142:
	.cfi_offset %r14, -24
.Lcfi143:
	.cfi_offset %rbp, -16
	movq	%rdi, %r8
	movq	(%rdx), %rcx
	movl	8(%rcx), %edx
	movl	%edx, %eax
	notl	%eax
	movq	16(%rcx), %r11
	sarl	$3, %edx
	movslq	%edx, %rdx
	andl	$7, %eax
	movzbl	(%r11,%rdx), %esi
	btl	%eax, %esi
	jae	.LBB28_2
# BB#1:                                 # %GetVLCSymbol.exit.thread24
	movq	$1, 12(%r8)
	movl	$1, %edi
	xorl	%esi, %esi
	jmp	.LBB28_11
.LBB28_2:                               # %.lr.ph73.i.preheader
	movslq	12(%rcx), %r9
	movl	$1, %edi
	movl	$7, %ecx
	.p2align	4, 0x90
.LBB28_3:                               # %.lr.ph73.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %esi
	incl	%edi
	xorl	%ebp, %ebp
	testl	%esi, %esi
	setle	%bpl
	movl	$-1, %eax
	cmovlel	%ecx, %eax
	addq	%rbp, %rdx
	addl	%esi, %eax
	movzbl	(%r11,%rdx), %esi
	btl	%eax, %esi
	jae	.LBB28_3
# BB#4:                                 # %.preheader.i
	leal	-1(%rdi), %r10d
	testl	%r10d, %r10d
	jle	.LBB28_9
# BB#5:                                 # %.lr.ph.i
	xorl	%ecx, %ecx
	movl	$7, %r14d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB28_6:                               # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rbx
	xorl	%edx, %edx
	testl	%eax, %eax
	setle	%dl
	movl	$-1, %ebp
	cmovlel	%r14d, %ebp
	addq	%rbx, %rdx
	cmpq	%r9, %rdx
	jg	.LBB28_13
# BB#7:                                 #   in Loop: Header=BB28_6 Depth=1
	addl	%ebp, %eax
	movzbl	(%r11,%rdx), %ebx
	btl	%eax, %ebx
	sbbl	%ebx, %ebx
	andl	$1, %ebx
	leal	(%rbx,%rsi,2), %esi
	incl	%ecx
	cmpl	%r10d, %ecx
	jl	.LBB28_6
# BB#8:                                 # %GetVLCSymbol.exit.loopexit
	addl	%ecx, %edi
	jmp	.LBB28_10
.LBB28_9:
	xorl	%esi, %esi
.LBB28_10:                              # %GetVLCSymbol.exit
	movl	%esi, 16(%r8)
	movl	%edi, 12(%r8)
	cmpl	$-1, %edi
	je	.LBB28_14
.LBB28_11:
	leaq	4(%r8), %rdx
	leaq	8(%r8), %rcx
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*32(%r8)
	movl	$1, %eax
	jmp	.LBB28_12
.LBB28_13:                              # %GetVLCSymbol.exit.thread
	movl	$-1, 12(%r8)
.LBB28_14:
	movl	$-1, %eax
.LBB28_12:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end28:
	.size	peekSyntaxElement_UVLC, .Lfunc_end28-peekSyntaxElement_UVLC
	.cfi_endproc

	.type	NTAB1,@object           # @NTAB1
	.section	.rodata,"a",@progbits
	.globl	NTAB1
	.p2align	4
NTAB1:
	.asciz	"\001"
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2,1
	.ascii	"\001\002"
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.asciz	"\002"
	.ascii	"\001\003"
	.ascii	"\001\004"
	.ascii	"\001\005"
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.asciz	"\003"
	.ascii	"\002\001"
	.zero	2,2
	.ascii	"\001\006"
	.ascii	"\001\007"
	.ascii	"\001\b"
	.ascii	"\001\t"
	.asciz	"\004"
	.size	NTAB1, 64

	.type	LEVRUN1,@object         # @LEVRUN1
	.globl	LEVRUN1
	.p2align	4
LEVRUN1:
	.asciz	"\004\002\002\001\001\001\001\001\001\001\000\000\000\000\000"
	.size	LEVRUN1, 16

	.type	NTAB2,@object           # @NTAB2
	.globl	NTAB2
	.p2align	4
NTAB2:
	.asciz	"\001"
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2,1
	.asciz	"\002"
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.ascii	"\001\002"
	.asciz	"\003"
	.asciz	"\004"
	.asciz	"\005"
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.ascii	"\001\003"
	.ascii	"\001\004"
	.ascii	"\002\001"
	.ascii	"\003\001"
	.asciz	"\006"
	.asciz	"\007"
	.asciz	"\b"
	.asciz	"\t"
	.size	NTAB2, 64

	.type	LEVRUN3,@object         # @LEVRUN3
	.globl	LEVRUN3
LEVRUN3:
	.asciz	"\002\001\000"
	.size	LEVRUN3, 4

	.type	NTAB3,@object           # @NTAB3
	.globl	NTAB3
NTAB3:
	.asciz	"\001"
	.zero	2
	.asciz	"\002"
	.zero	2,1
	.size	NTAB3, 8

	.type	UsedBits,@object        # @UsedBits
	.comm	UsedBits,4,4
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	readSyntaxElement_NumCoeffTrailingOnes.lentab,@object # @readSyntaxElement_NumCoeffTrailingOnes.lentab
	.p2align	4
readSyntaxElement_NumCoeffTrailingOnes.lentab:
	.long	1                       # 0x1
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	2                       # 0x2
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.size	readSyntaxElement_NumCoeffTrailingOnes.lentab, 816

	.type	readSyntaxElement_NumCoeffTrailingOnes.codtab,@object # @readSyntaxElement_NumCoeffTrailingOnes.codtab
	.p2align	4
readSyntaxElement_NumCoeffTrailingOnes.codtab:
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	8                       # 0x8
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	7                       # 0x7
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	1                       # 0x1
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	3                       # 0x3
	.long	11                      # 0xb
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	4                       # 0x4
	.long	7                       # 0x7
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	8                       # 0x8
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	7                       # 0x7
	.long	9                       # 0x9
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	7                       # 0x7
	.long	10                      # 0xa
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	8                       # 0x8
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	9                       # 0x9
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	6                       # 0x6
	.long	10                      # 0xa
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	8                       # 0x8
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	8                       # 0x8
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	5                       # 0x5
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	12                      # 0xc
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	7                       # 0x7
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	10                      # 0xa
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	7                       # 0x7
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	12                      # 0xc
	.long	11                      # 0xb
	.long	10                      # 0xa
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	13                      # 0xd
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	10                      # 0xa
	.long	6                       # 0x6
	.long	2                       # 0x2
	.size	readSyntaxElement_NumCoeffTrailingOnes.codtab, 816

	.type	readSyntaxElement_NumCoeffTrailingOnesChromaDC.lentab,@object # @readSyntaxElement_NumCoeffTrailingOnesChromaDC.lentab
	.p2align	4
readSyntaxElement_NumCoeffTrailingOnesChromaDC.lentab:
	.long	2                       # 0x2
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.size	readSyntaxElement_NumCoeffTrailingOnesChromaDC.lentab, 816

	.type	readSyntaxElement_NumCoeffTrailingOnesChromaDC.codtab,@object # @readSyntaxElement_NumCoeffTrailingOnesChromaDC.codtab
	.p2align	4
readSyntaxElement_NumCoeffTrailingOnesChromaDC.codtab:
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	6                       # 0x6
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	15                      # 0xf
	.long	14                      # 0xe
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	13                      # 0xd
	.long	12                      # 0xc
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	11                      # 0xb
	.long	10                      # 0xa
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	8                       # 0x8
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	7                       # 0x7
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	1                       # 0x1
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	8                       # 0x8
	.size	readSyntaxElement_NumCoeffTrailingOnesChromaDC.codtab, 816

	.type	readSyntaxElement_TotalZeros.lentab,@object # @readSyntaxElement_TotalZeros.lentab
	.p2align	4
readSyntaxElement_TotalZeros.lentab:
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	readSyntaxElement_TotalZeros.lentab, 960

	.type	readSyntaxElement_TotalZeros.codtab,@object # @readSyntaxElement_TotalZeros.codtab
	.p2align	4
readSyntaxElement_TotalZeros.codtab:
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	readSyntaxElement_TotalZeros.codtab, 960

	.type	readSyntaxElement_TotalZerosChromaDC.lentab,@object # @readSyntaxElement_TotalZerosChromaDC.lentab
	.p2align	4
readSyntaxElement_TotalZerosChromaDC.lentab:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	readSyntaxElement_TotalZerosChromaDC.lentab, 2880

	.type	readSyntaxElement_TotalZerosChromaDC.codtab,@object # @readSyntaxElement_TotalZerosChromaDC.codtab
	.p2align	4
readSyntaxElement_TotalZerosChromaDC.codtab:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	readSyntaxElement_TotalZerosChromaDC.codtab, 2880

	.type	readSyntaxElement_Run.lentab,@object # @readSyntaxElement_Run.lentab
	.p2align	4
readSyntaxElement_Run.lentab:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	0                       # 0x0
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.size	readSyntaxElement_Run.lentab, 960

	.type	readSyntaxElement_Run.codtab,@object # @readSyntaxElement_Run.codtab
	.p2align	4
readSyntaxElement_Run.codtab:
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.size	readSyntaxElement_Run.codtab, 960

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"ERROR: failed to find NumCoeff/TrailingOnes"
	.size	.Lstr, 44

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"ERROR: failed to find NumCoeff/TrailingOnes ChromaDC"
	.size	.Lstr.1, 53

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"ERROR: failed to find Total Zeros"
	.size	.Lstr.3, 34

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"ERROR: failed to find Run"
	.size	.Lstr.4, 26


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
