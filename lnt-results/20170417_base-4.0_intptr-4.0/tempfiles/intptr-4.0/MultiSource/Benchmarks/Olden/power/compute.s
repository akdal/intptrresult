	.text
	.file	"compute.bc"
	.globl	Compute_Tree
	.p2align	4, 0x90
	.type	Compute_Tree,@function
Compute_Tree:                           # @Compute_Tree
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 48
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movq	64(%r14,%rbx,8), %rdi
	movsd	16(%r14), %xmm0         # xmm0 = mem[0],zero
	movsd	24(%r14), %xmm1         # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	movapd	%xmm1, %xmm3
	callq	Compute_Lateral
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movapd	(%rsp), %xmm1           # 16-byte Reload
	addpd	%xmm0, %xmm1
	movapd	%xmm1, (%rsp)           # 16-byte Spill
	movaps	(%rsp), %xmm0           # 16-byte Reload
	incq	%rbx
	cmpq	$11, %rbx
	jne	.LBB0_1
# BB#2:
	movups	%xmm0, (%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	Compute_Tree, .Lfunc_end0-Compute_Tree
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_1:
	.quad	-4607182418800017408    # double -4
.LCPI1_2:
	.quad	4607182418800017408     # double 1
	.text
	.globl	Compute_Lateral
	.p2align	4, 0x90
	.type	Compute_Lateral,@function
Compute_Lateral:                        # @Compute_Lateral
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 80
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movapd	%xmm0, %xmm5
	movq	%rdi, %rbx
	movsd	32(%rbx), %xmm4         # xmm4 = mem[0],zero
	movsd	40(%rbx), %xmm0         # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm6
	mulsd	%xmm1, %xmm6
	divsd	%xmm4, %xmm6
	addsd	%xmm5, %xmm6
	mulsd	16(%rbx), %xmm6
	addsd	%xmm2, %xmm6
	mulsd	%xmm5, %xmm4
	divsd	%xmm0, %xmm4
	addsd	%xmm1, %xmm4
	mulsd	24(%rbx), %xmm4
	addsd	%xmm3, %xmm4
	movq	48(%rbx), %r14
	testq	%r14, %r14
	je	.LBB1_1
# BB#2:
	movq	%r14, %rdi
	movapd	%xmm5, %xmm0
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm6, %xmm2
	movapd	%xmm4, %xmm3
	movsd	%xmm4, 48(%rsp)         # 8-byte Spill
	movsd	%xmm5, 40(%rsp)         # 8-byte Spill
	movsd	%xmm6, 32(%rsp)         # 8-byte Spill
	callq	Compute_Lateral
	movsd	32(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movsd	40(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movsd	48(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	jmp	.LBB1_3
.LBB1_1:
                                        # implicit-def: %XMM0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
                                        # implicit-def: %XMM0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
.LBB1_3:
	movq	56(%rbx), %rdi
	movapd	%xmm5, %xmm0
	movapd	%xmm6, %xmm2
	movapd	%xmm4, %xmm3
	callq	Compute_Branch
	testq	%r14, %r14
	je	.LBB1_5
# BB#4:
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	8(%rsp), %xmm1          # 8-byte Folded Reload
.LBB1_5:
	movsd	%xmm0, (%rbx)
	movsd	%xmm1, 8(%rbx)
	movsd	32(%rbx), %xmm2         # xmm2 = mem[0],zero
	movsd	40(%rbx), %xmm4         # xmm4 = mem[0],zero
	movapd	%xmm2, %xmm3
	mulsd	%xmm3, %xmm3
	movapd	%xmm4, %xmm6
	mulsd	%xmm6, %xmm6
	addsd	%xmm3, %xmm6
	movapd	%xmm2, %xmm3
	addsd	%xmm3, %xmm3
	mulsd	%xmm4, %xmm3
	mulsd	%xmm1, %xmm3
	movapd	%xmm4, %xmm5
	addsd	%xmm5, %xmm5
	mulsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm3
	subsd	%xmm2, %xmm3
	mulsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm4
	subsd	%xmm4, %xmm1
	mulsd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movapd	.LCPI1_0(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm3, %xmm4
	mulsd	%xmm3, %xmm3
	movsd	.LCPI1_1(%rip), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm6, %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB1_7
# BB#6:                                 # %call.sqrt
	movsd	%xmm6, (%rsp)           # 8-byte Spill
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	movsd	(%rsp), %xmm6           # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB1_7:                                # %.split
	subsd	%xmm1, %xmm4
	addsd	%xmm6, %xmm6
	divsd	%xmm6, %xmm4
	movapd	%xmm4, %xmm0
	subsd	(%rbx), %xmm0
	movups	32(%rbx), %xmm2
	movaps	%xmm2, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulsd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	addsd	8(%rbx), %xmm1
	movsd	%xmm1, 8(%rbx)
	movsd	%xmm4, (%rbx)
	addpd	%xmm2, %xmm2
	movapd	%xmm4, %xmm0
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	mulpd	%xmm2, %xmm0
	movsd	.LCPI1_2(%rip), %xmm2   # xmm2 = mem[0],zero
	subsd	%xmm0, %xmm2
	movapd	%xmm0, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	subsd	%xmm3, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	divpd	%xmm2, %xmm0
	movupd	%xmm0, 16(%rbx)
	movapd	%xmm4, %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	Compute_Lateral, .Lfunc_end1-Compute_Lateral
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_1:
	.quad	-4607182418800017408    # double -4
.LCPI2_2:
	.quad	4607182418800017408     # double 1
.LCPI2_3:
	.quad	0                       # double 0
	.text
	.globl	Compute_Branch
	.p2align	4, 0x90
	.type	Compute_Branch,@function
Compute_Branch:                         # @Compute_Branch
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	subq	$72, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 112
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r12, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movsd	32(%r15), %xmm5         # xmm5 = mem[0],zero
	movsd	40(%r15), %xmm4         # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm6
	mulsd	%xmm1, %xmm6
	divsd	%xmm5, %xmm6
	addsd	%xmm0, %xmm6
	mulsd	16(%r15), %xmm6
	addsd	%xmm2, %xmm6
	mulsd	%xmm0, %xmm5
	divsd	%xmm4, %xmm5
	addsd	%xmm1, %xmm5
	mulsd	24(%r15), %xmm5
	addsd	%xmm3, %xmm5
	movq	48(%r15), %r14
	testq	%r14, %r14
	movsd	%xmm5, 16(%rsp)         # 8-byte Spill
	movsd	%xmm6, 8(%rsp)          # 8-byte Spill
	je	.LBB2_1
# BB#2:
	movq	%r14, %rdi
	movapd	%xmm6, %xmm2
	movapd	%xmm5, %xmm3
	callq	Compute_Branch
	movsd	8(%rsp), %xmm6          # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movsd	16(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	jmp	.LBB2_3
.LBB2_1:
                                        # implicit-def: %XMM0
.LBB2_3:
	movapd	%xmm0, 48(%rsp)         # 16-byte Spill
	xorpd	%xmm3, %xmm3
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	movapd	%xmm3, 32(%rsp)         # 16-byte Spill
	movq	56(%r15,%rbx,8), %r12
	movq	(%r12), %rax
	movq	%rax, P(%rip)
	movq	8(%r12), %rax
	movq	%rax, Q(%rip)
	movapd	%xmm6, %xmm0
	movapd	%xmm5, %xmm1
	callq	optimize_node
	movsd	P(%rip), %xmm0          # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jbe	.LBB2_5
# BB#6:                                 #   in Loop: Header=BB2_4 Depth=1
	movq	$0, P(%rip)
	movq	$0, Q(%rip)
	xorpd	%xmm0, %xmm0
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_5:                                # %._crit_edge.i
                                        #   in Loop: Header=BB2_4 Depth=1
	movhpd	Q(%rip), %xmm0          # xmm0 = xmm0[0],mem[0]
.LBB2_7:                                # %Compute_Leaf.exit
                                        #   in Loop: Header=BB2_4 Depth=1
	movapd	32(%rsp), %xmm3         # 16-byte Reload
	movupd	%xmm0, (%r12)
	addpd	%xmm0, %xmm3
	incq	%rbx
	cmpq	$12, %rbx
	movsd	16(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movsd	8(%rsp), %xmm6          # 8-byte Reload
                                        # xmm6 = mem[0],zero
	jne	.LBB2_4
# BB#8:
	xorl	%eax, %eax
	testq	%r14, %r14
	setne	%al
	movapd	48(%rsp), %xmm2         # 16-byte Reload
	addpd	%xmm3, %xmm2
	movd	%eax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	psllq	$63, %xmm0
	psrad	$31, %xmm0
	pshufd	$245, %xmm0, %xmm1      # xmm1 = xmm0[1,1,3,3]
	andpd	%xmm1, %xmm2
	andnpd	%xmm3, %xmm1
	orpd	%xmm2, %xmm1
	movupd	%xmm1, (%r15)
	movsd	32(%r15), %xmm3         # xmm3 = mem[0],zero
	movsd	40(%r15), %xmm0         # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm6
	movapd	%xmm3, %xmm2
	addsd	%xmm2, %xmm2
	mulsd	%xmm0, %xmm2
	movapd	%xmm1, %xmm4
	psrldq	$8, %xmm4               # xmm4 = xmm4[8,9,10,11,12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero
	mulsd	%xmm4, %xmm2
	movapd	%xmm0, %xmm5
	addsd	%xmm5, %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm1, %xmm5
	subsd	%xmm5, %xmm2
	subsd	%xmm3, %xmm2
	mulsd	%xmm3, %xmm4
	mulsd	%xmm1, %xmm0
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm3
	mulsd	%xmm6, %xmm6
	addsd	%xmm3, %xmm6
	subsd	%xmm0, %xmm4
	mulsd	%xmm4, %xmm4
	addsd	%xmm4, %xmm1
	movapd	.LCPI2_0(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm2, %xmm4
	mulsd	%xmm2, %xmm2
	movsd	.LCPI2_1(%rip), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm6, %xmm0
	mulsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB2_10
# BB#9:                                 # %call.sqrt
	movsd	%xmm6, 32(%rsp)         # 8-byte Spill
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	movsd	32(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB2_10:                               # %.split
	subsd	%xmm1, %xmm4
	addsd	%xmm6, %xmm6
	divsd	%xmm6, %xmm4
	movapd	%xmm4, %xmm0
	subsd	(%r15), %xmm0
	movups	32(%r15), %xmm2
	movaps	%xmm2, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulsd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	addsd	8(%r15), %xmm1
	movsd	%xmm1, 8(%r15)
	movsd	%xmm4, (%r15)
	addpd	%xmm2, %xmm2
	movapd	%xmm4, %xmm0
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	mulpd	%xmm2, %xmm0
	movsd	.LCPI2_2(%rip), %xmm2   # xmm2 = mem[0],zero
	subsd	%xmm0, %xmm2
	movapd	%xmm0, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	subsd	%xmm3, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	divpd	%xmm2, %xmm0
	movupd	%xmm0, 16(%r15)
	movapd	%xmm4, %xmm0
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	Compute_Branch, .Lfunc_end2-Compute_Branch
	.cfi_endproc

	.globl	Compute_Leaf
	.p2align	4, 0x90
	.type	Compute_Leaf,@function
Compute_Leaf:                           # @Compute_Leaf
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movq	%rax, P(%rip)
	movq	8(%rbx), %rax
	movq	%rax, Q(%rip)
	callq	optimize_node
	movsd	P(%rip), %xmm0          # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jbe	.LBB3_1
# BB#2:
	movq	$0, P(%rip)
	movq	$0, Q(%rip)
	xorpd	%xmm0, %xmm0
	jmp	.LBB3_3
.LBB3_1:                                # %._crit_edge
	movsd	Q(%rip), %xmm1          # xmm1 = mem[0],zero
.LBB3_3:
	movsd	%xmm0, (%rbx)
	movsd	%xmm1, 8(%rbx)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	Compute_Leaf, .Lfunc_end3-Compute_Leaf
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	-4606056518893174784    # double -5
.LCPI4_2:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
.LCPI4_3:
	.quad	4617427004022730742     # double 5.0990195135927845
.LCPI4_4:
	.quad	4596233848715572764     # double 0.19611613513818404
.LCPI4_5:
	.quad	4607007505076573091     # double 0.98058067569092022
.LCPI4_6:
	.quad	-4617991057905706598    # double -0.80000000000000004
.LCPI4_8:
	.quad	4607182418800017408     # double 1
.LCPI4_10:
	.quad	-4616364531778202717    # double -0.98058067569092022
.LCPI4_11:
	.quad	-4627138188139203044    # double -0.19611613513818404
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_1:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
.LCPI4_7:
	.quad	4607007505076573091     # double 0.98058067569092022
	.quad	4596233848715572764     # double 0.19611613513818404
.LCPI4_9:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	optimize_node
	.p2align	4, 0x90
	.type	optimize_node,@function
optimize_node:                          # @optimize_node
	.cfi_startproc
# BB#0:
	subq	$120, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 128
	movapd	%xmm1, %xmm9
	movapd	%xmm0, %xmm8
	movapd	.LCPI4_1(%rip), %xmm12  # xmm12 = [nan,nan]
	movsd	.LCPI4_2(%rip), %xmm7   # xmm7 = mem[0],zero
	xorpd	%xmm10, %xmm10
	movsd	.LCPI4_8(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	%xmm9, 8(%rsp)          # 8-byte Spill
	movsd	%xmm8, (%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB4_1:                                # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movsd	P(%rip), %xmm1          # xmm1 = mem[0],zero
	movsd	Q(%rip), %xmm3          # xmm3 = mem[0],zero
	movapd	%xmm3, %xmm0
	mulsd	.LCPI4_0(%rip), %xmm0
	addsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm2
	andpd	%xmm12, %xmm2
	ucomisd	%xmm7, %xmm2
	jbe	.LBB4_2
# BB#3:                                 #   in Loop: Header=BB4_1 Depth=1
	divsd	.LCPI4_3(%rip), %xmm0
	movapd	%xmm0, %xmm2
	mulsd	.LCPI4_4(%rip), %xmm2
	mulsd	.LCPI4_5(%rip), %xmm0
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	movapd	%xmm0, %xmm1
	addpd	%xmm3, %xmm1
	subpd	%xmm3, %xmm0
	movhpd	%xmm0, P(%rip)
	movsd	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1]
	movlpd	%xmm1, Q(%rip)
	movapd	%xmm0, %xmm3
	jmp	.LBB4_4
	.p2align	4, 0x90
.LBB4_2:                                #   in Loop: Header=BB4_1 Depth=1
	unpcklpd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0]
.LBB4_4:                                #   in Loop: Header=BB4_1 Depth=1
	movapd	%xmm3, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	mulsd	%xmm0, %xmm0
	movapd	%xmm3, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm2
	addsd	.LCPI4_6(%rip), %xmm2
	ucomisd	%xmm7, %xmm2
	jbe	.LBB4_13
# BB#5:                                 #   in Loop: Header=BB4_1 Depth=1
	addpd	%xmm3, %xmm3
	movapd	%xmm3, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulsd	%xmm1, %xmm1
	addsd	%xmm10, %xmm1
	movapd	%xmm3, %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	sqrtsd	%xmm0, %xmm11
	ucomisd	%xmm11, %xmm11
	movapd	%xmm2, 32(%rsp)         # 16-byte Spill
	jnp	.LBB4_7
# BB#6:                                 # %call.sqrt
                                        #   in Loop: Header=BB4_1 Depth=1
	movapd	%xmm3, 64(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	32(%rsp), %xmm2         # 16-byte Reload
	movapd	64(%rsp), %xmm3         # 16-byte Reload
	movsd	.LCPI4_8(%rip), %xmm4   # xmm4 = mem[0],zero
	xorpd	%xmm10, %xmm10
	movsd	.LCPI4_2(%rip), %xmm7   # xmm7 = mem[0],zero
	movapd	.LCPI4_1(%rip), %xmm6   # xmm6 = [nan,nan]
	movapd	%xmm6, %xmm12
	movsd	(%rsp), %xmm8           # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movsd	8(%rsp), %xmm9          # 8-byte Reload
                                        # xmm9 = mem[0],zero
	movapd	%xmm0, %xmm11
.LBB4_7:                                # %.split
                                        #   in Loop: Header=BB4_1 Depth=1
	movapd	%xmm11, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	divpd	%xmm0, %xmm3
	movapd	%xmm3, %xmm5
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	mulsd	.LCPI4_4(%rip), %xmm5
	addsd	%xmm10, %xmm5
	movapd	%xmm3, %xmm0
	mulsd	.LCPI4_5(%rip), %xmm0
	subsd	%xmm0, %xmm5
	movapd	%xmm5, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	.LCPI4_7(%rip), %xmm1
	movapd	%xmm3, %xmm0
	addpd	%xmm1, %xmm0
	subpd	%xmm1, %xmm3
	movapd	%xmm3, %xmm6
	movsd	%xmm0, %xmm6            # xmm6 = xmm0[0],xmm6[1]
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	mulsd	%xmm3, %xmm3
	addsd	%xmm10, %xmm3
	mulsd	%xmm0, %xmm0
	addsd	%xmm3, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB4_9
# BB#8:                                 # %call.sqrt73
                                        #   in Loop: Header=BB4_1 Depth=1
	movapd	%xmm11, 64(%rsp)        # 16-byte Spill
	movapd	%xmm6, 16(%rsp)         # 16-byte Spill
	movapd	%xmm5, 80(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	80(%rsp), %xmm5         # 16-byte Reload
	movapd	64(%rsp), %xmm11        # 16-byte Reload
	movapd	32(%rsp), %xmm2         # 16-byte Reload
	movsd	.LCPI4_8(%rip), %xmm4   # xmm4 = mem[0],zero
	xorpd	%xmm10, %xmm10
	movsd	.LCPI4_2(%rip), %xmm7   # xmm7 = mem[0],zero
	movapd	.LCPI4_1(%rip), %xmm6   # xmm6 = [nan,nan]
	movapd	%xmm6, %xmm12
	movapd	16(%rsp), %xmm6         # 16-byte Reload
	movsd	(%rsp), %xmm8           # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movsd	8(%rsp), %xmm9          # 8-byte Reload
                                        # xmm9 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB4_9:                                # %.split.split
                                        #   in Loop: Header=BB4_1 Depth=1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	divpd	%xmm1, %xmm6
	mulsd	%xmm5, %xmm5
	movapd	%xmm4, %xmm1
	subsd	%xmm5, %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm1, %xmm0
	ja	.LBB4_12
# BB#10:                                #   in Loop: Header=BB4_1 Depth=1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB4_12
# BB#11:                                # %call.sqrt75
                                        #   in Loop: Header=BB4_1 Depth=1
	movapd	%xmm1, %xmm0
	movapd	%xmm11, 64(%rsp)        # 16-byte Spill
	movapd	%xmm6, 16(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	64(%rsp), %xmm11        # 16-byte Reload
	movapd	32(%rsp), %xmm2         # 16-byte Reload
	movsd	.LCPI4_8(%rip), %xmm4   # xmm4 = mem[0],zero
	xorpd	%xmm10, %xmm10
	movsd	.LCPI4_2(%rip), %xmm7   # xmm7 = mem[0],zero
	movapd	.LCPI4_1(%rip), %xmm6   # xmm6 = [nan,nan]
	movapd	%xmm6, %xmm12
	movapd	16(%rsp), %xmm6         # 16-byte Reload
	movsd	(%rsp), %xmm8           # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movsd	8(%rsp), %xmm9          # 8-byte Reload
                                        # xmm9 = mem[0],zero
	.p2align	4, 0x90
.LBB4_12:                               # %make_orthogonal.exit56
                                        #   in Loop: Header=BB4_1 Depth=1
	mulsd	%xmm0, %xmm11
	divsd	%xmm11, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm6, %xmm2
	movsd	Q(%rip), %xmm3          # xmm3 = mem[0],zero
	movhpd	P(%rip), %xmm3          # xmm3 = xmm3[0],mem[0]
	subpd	%xmm2, %xmm3
	movhpd	%xmm3, P(%rip)
	movlpd	%xmm3, Q(%rip)
.LBB4_13:                               #   in Loop: Header=BB4_1 Depth=1
	movapd	%xmm3, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	addsd	%xmm4, %xmm0
	movapd	%xmm4, %xmm13
	divsd	%xmm0, %xmm13
	subsd	%xmm8, %xmm13
	addsd	%xmm4, %xmm3
	movapd	%xmm4, %xmm6
	divsd	%xmm3, %xmm6
	subsd	%xmm9, %xmm6
	movapd	%xmm13, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm10, %xmm1
	movapd	%xmm6, %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm11, %xmm11
	sqrtsd	%xmm0, %xmm11
	ucomisd	%xmm11, %xmm11
	jnp	.LBB4_15
# BB#14:                                # %call.sqrt77
                                        #   in Loop: Header=BB4_1 Depth=1
	movsd	%xmm13, 32(%rsp)        # 8-byte Spill
	movsd	%xmm6, 16(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	32(%rsp), %xmm13        # 8-byte Reload
                                        # xmm13 = mem[0],zero
	movsd	.LCPI4_8(%rip), %xmm4   # xmm4 = mem[0],zero
	xorpd	%xmm10, %xmm10
	movsd	.LCPI4_2(%rip), %xmm7   # xmm7 = mem[0],zero
	movapd	.LCPI4_1(%rip), %xmm6   # xmm6 = [nan,nan]
	movapd	%xmm6, %xmm12
	movsd	16(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movsd	(%rsp), %xmm8           # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movsd	8(%rsp), %xmm9          # 8-byte Reload
                                        # xmm9 = mem[0],zero
	movapd	%xmm0, %xmm11
.LBB4_15:                               # %.split76
                                        #   in Loop: Header=BB4_1 Depth=1
	divsd	%xmm11, %xmm13
	divsd	%xmm11, %xmm6
	movsd	P(%rip), %xmm0          # xmm0 = mem[0],zero
	addsd	%xmm4, %xmm0
	movapd	%xmm4, %xmm2
	divsd	%xmm0, %xmm2
	movsd	Q(%rip), %xmm0          # xmm0 = mem[0],zero
	addsd	%xmm4, %xmm0
	movapd	%xmm4, %xmm3
	divsd	%xmm0, %xmm3
	movapd	%xmm2, %xmm14
	subsd	%xmm8, %xmm14
	movapd	%xmm3, %xmm5
	subsd	%xmm9, %xmm5
	movapd	%xmm14, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm5, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	movsd	%xmm11, 64(%rsp)        # 8-byte Spill
	jnp	.LBB4_17
# BB#16:                                # %call.sqrt78
                                        #   in Loop: Header=BB4_1 Depth=1
	movapd	%xmm1, %xmm0
	movsd	%xmm13, 32(%rsp)        # 8-byte Spill
	movsd	%xmm6, 16(%rsp)         # 8-byte Spill
	movsd	%xmm2, 80(%rsp)         # 8-byte Spill
	movsd	%xmm3, 56(%rsp)         # 8-byte Spill
	movsd	%xmm5, 104(%rsp)        # 8-byte Spill
	movsd	%xmm14, 112(%rsp)       # 8-byte Spill
	callq	sqrt
	movsd	112(%rsp), %xmm14       # 8-byte Reload
                                        # xmm14 = mem[0],zero
	movsd	104(%rsp), %xmm5        # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movsd	56(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	80(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	64(%rsp), %xmm11        # 8-byte Reload
                                        # xmm11 = mem[0],zero
	movsd	32(%rsp), %xmm13        # 8-byte Reload
                                        # xmm13 = mem[0],zero
	movsd	.LCPI4_8(%rip), %xmm4   # xmm4 = mem[0],zero
	xorpd	%xmm10, %xmm10
	movsd	.LCPI4_2(%rip), %xmm7   # xmm7 = mem[0],zero
	movapd	.LCPI4_1(%rip), %xmm6   # xmm6 = [nan,nan]
	movapd	%xmm6, %xmm12
	movsd	16(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movsd	(%rsp), %xmm8           # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movsd	8(%rsp), %xmm9          # 8-byte Reload
                                        # xmm9 = mem[0],zero
.LBB4_17:                               # %.split76.split
                                        #   in Loop: Header=BB4_1 Depth=1
	mulsd	%xmm2, %xmm2
	mulsd	%xmm14, %xmm2
	divsd	%xmm0, %xmm2
	mulsd	%xmm3, %xmm3
	mulsd	%xmm5, %xmm3
	divsd	%xmm0, %xmm3
	mulsd	%xmm13, %xmm2
	xorpd	%xmm5, %xmm5
	subsd	%xmm2, %xmm5
	mulsd	%xmm6, %xmm3
	subsd	%xmm3, %xmm5
	movapd	%xmm13, %xmm3
	movsd	.LCPI4_4(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm3
	addsd	%xmm10, %xmm3
	movapd	%xmm6, %xmm0
	movsd	.LCPI4_5(%rip), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm0
	mulsd	%xmm1, %xmm0
	subsd	%xmm0, %xmm13
	movapd	%xmm13, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm10, %xmm1
	movapd	%xmm3, %xmm14
	mulsd	%xmm2, %xmm14
	addsd	%xmm6, %xmm14
	movapd	%xmm14, %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB4_19
# BB#18:                                # %call.sqrt79
                                        #   in Loop: Header=BB4_1 Depth=1
	movsd	%xmm13, 32(%rsp)        # 8-byte Spill
	movsd	%xmm14, 16(%rsp)        # 8-byte Spill
	movapd	%xmm5, 80(%rsp)         # 16-byte Spill
	movsd	%xmm3, 56(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	56(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movapd	80(%rsp), %xmm5         # 16-byte Reload
	movsd	16(%rsp), %xmm14        # 8-byte Reload
                                        # xmm14 = mem[0],zero
	movsd	64(%rsp), %xmm11        # 8-byte Reload
                                        # xmm11 = mem[0],zero
	movsd	32(%rsp), %xmm13        # 8-byte Reload
                                        # xmm13 = mem[0],zero
	movsd	.LCPI4_8(%rip), %xmm4   # xmm4 = mem[0],zero
	xorpd	%xmm10, %xmm10
	movsd	.LCPI4_2(%rip), %xmm7   # xmm7 = mem[0],zero
	movapd	.LCPI4_1(%rip), %xmm6   # xmm6 = [nan,nan]
	movapd	%xmm6, %xmm12
	movsd	(%rsp), %xmm8           # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movsd	8(%rsp), %xmm9          # 8-byte Reload
                                        # xmm9 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB4_19:                               # %.split76.split.split
                                        #   in Loop: Header=BB4_1 Depth=1
	andpd	%xmm12, %xmm5
	mulsd	%xmm3, %xmm3
	movapd	%xmm4, %xmm0
	subsd	%xmm3, %xmm0
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	ja	.LBB4_22
# BB#20:                                #   in Loop: Header=BB4_1 Depth=1
	xorps	%xmm2, %xmm2
	sqrtsd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm2
	jnp	.LBB4_22
# BB#21:                                # %call.sqrt81
                                        #   in Loop: Header=BB4_1 Depth=1
	movsd	%xmm13, 32(%rsp)        # 8-byte Spill
	movsd	%xmm14, 16(%rsp)        # 8-byte Spill
	movapd	%xmm5, 80(%rsp)         # 16-byte Spill
	movsd	%xmm1, 56(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	56(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	80(%rsp), %xmm5         # 16-byte Reload
	movsd	16(%rsp), %xmm14        # 8-byte Reload
                                        # xmm14 = mem[0],zero
	movsd	64(%rsp), %xmm11        # 8-byte Reload
                                        # xmm11 = mem[0],zero
	movsd	32(%rsp), %xmm13        # 8-byte Reload
                                        # xmm13 = mem[0],zero
	movsd	.LCPI4_8(%rip), %xmm4   # xmm4 = mem[0],zero
	xorpd	%xmm10, %xmm10
	movsd	.LCPI4_2(%rip), %xmm7   # xmm7 = mem[0],zero
	movapd	.LCPI4_1(%rip), %xmm6   # xmm6 = [nan,nan]
	movapd	%xmm6, %xmm12
	movsd	(%rsp), %xmm8           # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movsd	8(%rsp), %xmm9          # 8-byte Reload
                                        # xmm9 = mem[0],zero
	movapd	%xmm0, %xmm2
	.p2align	4, 0x90
.LBB4_22:                               # %make_orthogonal.exit
                                        #   in Loop: Header=BB4_1 Depth=1
	divsd	%xmm5, %xmm11
	divsd	%xmm1, %xmm13
	divsd	%xmm1, %xmm14
	movsd	P(%rip), %xmm3          # xmm3 = mem[0],zero
	addsd	%xmm3, %xmm3
	movsd	Q(%rip), %xmm5          # xmm5 = mem[0],zero
	addsd	%xmm5, %xmm5
	movapd	%xmm3, %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm10, %xmm0
	movapd	%xmm5, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB4_24
# BB#23:                                # %call.sqrt82
                                        #   in Loop: Header=BB4_1 Depth=1
	movapd	%xmm1, %xmm0
	movsd	%xmm13, 32(%rsp)        # 8-byte Spill
	movsd	%xmm11, 64(%rsp)        # 8-byte Spill
	movsd	%xmm14, 16(%rsp)        # 8-byte Spill
	movsd	%xmm2, 80(%rsp)         # 8-byte Spill
	movsd	%xmm5, 56(%rsp)         # 8-byte Spill
	movsd	%xmm3, 104(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	104(%rsp), %xmm3        # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	56(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movsd	80(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	16(%rsp), %xmm14        # 8-byte Reload
                                        # xmm14 = mem[0],zero
	movsd	64(%rsp), %xmm11        # 8-byte Reload
                                        # xmm11 = mem[0],zero
	movsd	32(%rsp), %xmm13        # 8-byte Reload
                                        # xmm13 = mem[0],zero
	movsd	.LCPI4_8(%rip), %xmm4   # xmm4 = mem[0],zero
	xorpd	%xmm10, %xmm10
	movsd	.LCPI4_2(%rip), %xmm7   # xmm7 = mem[0],zero
	movapd	.LCPI4_1(%rip), %xmm6   # xmm6 = [nan,nan]
	movapd	%xmm6, %xmm12
	movsd	(%rsp), %xmm8           # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movsd	8(%rsp), %xmm9          # 8-byte Reload
                                        # xmm9 = mem[0],zero
.LBB4_24:                               # %make_orthogonal.exit.split
                                        #   in Loop: Header=BB4_1 Depth=1
	divsd	%xmm0, %xmm3
	divsd	%xmm0, %xmm5
	mulsd	%xmm13, %xmm3
	addsd	%xmm10, %xmm3
	mulsd	%xmm14, %xmm5
	addsd	%xmm3, %xmm5
	mulsd	%xmm2, %xmm11
	ucomisd	%xmm10, %xmm5
	movsd	P(%rip), %xmm0          # xmm0 = mem[0],zero
	jbe	.LBB4_25
# BB#26:                                #   in Loop: Header=BB4_1 Depth=1
	movapd	%xmm0, %xmm6
	mulsd	%xmm6, %xmm6
	movsd	Q(%rip), %xmm1          # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm6, %xmm2
	addsd	.LCPI4_6(%rip), %xmm2
	xorpd	.LCPI4_9(%rip), %xmm2
	divsd	%xmm5, %xmm2
	ucomisd	%xmm2, %xmm11
	jbe	.LBB4_28
# BB#27:                                #   in Loop: Header=BB4_1 Depth=1
	movapd	%xmm2, %xmm11
	jmp	.LBB4_28
	.p2align	4, 0x90
.LBB4_25:                               # %make_orthogonal.exit._crit_edge
                                        #   in Loop: Header=BB4_1 Depth=1
	movsd	Q(%rip), %xmm1          # xmm1 = mem[0],zero
.LBB4_28:                               #   in Loop: Header=BB4_1 Depth=1
	mulsd	%xmm11, %xmm13
	addsd	%xmm13, %xmm0
	movsd	%xmm0, P(%rip)
	mulsd	%xmm11, %xmm14
	addsd	%xmm1, %xmm14
	movsd	%xmm14, Q(%rip)
	movapd	%xmm14, %xmm6
	mulsd	.LCPI4_0(%rip), %xmm6
	addsd	%xmm0, %xmm6
	movapd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm1
	movapd	%xmm14, %xmm3
	mulsd	%xmm3, %xmm3
	addsd	%xmm1, %xmm3
	addsd	.LCPI4_6(%rip), %xmm3
	addsd	%xmm4, %xmm0
	movapd	%xmm4, %xmm2
	divsd	%xmm0, %xmm2
	subsd	%xmm8, %xmm2
	addsd	%xmm4, %xmm14
	movapd	%xmm4, %xmm5
	divsd	%xmm14, %xmm5
	subsd	%xmm9, %xmm5
	movapd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm10, %xmm0
	movapd	%xmm5, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB4_30
# BB#29:                                # %call.sqrt84
                                        #   in Loop: Header=BB4_1 Depth=1
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 32(%rsp)         # 8-byte Spill
	movapd	%xmm5, 64(%rsp)         # 16-byte Spill
	movapd	%xmm6, 16(%rsp)         # 16-byte Spill
	movapd	%xmm3, 80(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	80(%rsp), %xmm3         # 16-byte Reload
	movapd	64(%rsp), %xmm5         # 16-byte Reload
	movsd	32(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	.LCPI4_8(%rip), %xmm4   # xmm4 = mem[0],zero
	xorpd	%xmm10, %xmm10
	movsd	.LCPI4_2(%rip), %xmm7   # xmm7 = mem[0],zero
	movapd	.LCPI4_1(%rip), %xmm6   # xmm6 = [nan,nan]
	movapd	%xmm6, %xmm12
	movapd	16(%rsp), %xmm6         # 16-byte Reload
	movsd	(%rsp), %xmm8           # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movsd	8(%rsp), %xmm9          # 8-byte Reload
                                        # xmm9 = mem[0],zero
.LBB4_30:                               # %.split83
                                        #   in Loop: Header=BB4_1 Depth=1
	andpd	%xmm12, %xmm6
	ucomisd	%xmm7, %xmm3
	ja	.LBB4_1
# BB#31:                                # %.split83
                                        #   in Loop: Header=BB4_1 Depth=1
	ucomisd	%xmm7, %xmm6
	ja	.LBB4_1
# BB#32:                                #   in Loop: Header=BB4_1 Depth=1
	andpd	%xmm12, %xmm3
	ucomisd	%xmm7, %xmm3
	jbe	.LBB4_34
# BB#33:                                #   in Loop: Header=BB4_1 Depth=1
	divsd	%xmm0, %xmm2
	divsd	%xmm0, %xmm5
	mulsd	.LCPI4_10(%rip), %xmm2
	mulsd	.LCPI4_11(%rip), %xmm5
	addsd	%xmm2, %xmm5
	andpd	%xmm12, %xmm5
	ucomisd	%xmm7, %xmm5
	ja	.LBB4_1
.LBB4_34:                               # %.critedge2
	addq	$120, %rsp
	retq
.Lfunc_end4:
	.size	optimize_node, .Lfunc_end4-optimize_node
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.quad	4596233848715572764     # double 0.19611613513818404
	.quad	-4616364531778202717    # double -0.98058067569092022
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_1:
	.quad	4617427004022730742     # double 5.0990195135927845
	.text
	.globl	find_gradient_h
	.p2align	4, 0x90
	.type	find_gradient_h,@function
find_gradient_h:                        # @find_gradient_h
	.cfi_startproc
# BB#0:
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [1.961161e-01,-9.805807e-01]
	movups	%xmm0, (%rdi)
	movsd	.LCPI5_1(%rip), %xmm0   # xmm0 = mem[0],zero
	retq
.Lfunc_end5:
	.size	find_gradient_h, .Lfunc_end5-find_gradient_h
	.cfi_endproc

	.globl	find_gradient_g
	.p2align	4, 0x90
	.type	find_gradient_g,@function
find_gradient_g:                        # @find_gradient_g
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
.Lcfi23:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movsd	P(%rip), %xmm0          # xmm0 = mem[0],zero
	movhpd	Q(%rip), %xmm0          # xmm0 = xmm0[0],mem[0]
	addpd	%xmm0, %xmm0
	movupd	%xmm0, (%rbx)
	movapd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm1
	xorpd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	mulsd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_2
# BB#1:                                 # %call.sqrt
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB6_2:                                # %.split
	movupd	(%rbx), %xmm0
	movapd	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	divpd	%xmm2, %xmm0
	movupd	%xmm0, (%rbx)
	movapd	%xmm1, %xmm0
	popq	%rbx
	retq
.Lfunc_end6:
	.size	find_gradient_g, .Lfunc_end6-find_gradient_g
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	make_orthogonal
	.p2align	4, 0x90
	.type	make_orthogonal,@function
make_orthogonal:                        # @make_orthogonal
	.cfi_startproc
# BB#0:                                 # %.preheader43
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rbx), %xmm2           # xmm2 = mem[0],zero
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	movapd	%xmm2, %xmm3
	mulsd	%xmm1, %xmm3
	xorpd	%xmm4, %xmm4
	addsd	%xmm4, %xmm3
	movsd	8(%rsi), %xmm5          # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	addsd	%xmm3, %xmm5
	mulsd	%xmm5, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbx)
	mulsd	%xmm2, %xmm2
	addsd	%xmm4, %xmm2
	movsd	8(%rsi), %xmm1          # xmm1 = mem[0],zero
	mulsd	%xmm5, %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rbx)
	mulsd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB7_2
# BB#1:                                 # %call.sqrt
	movsd	%xmm5, 8(%rsp)          # 8-byte Spill
	callq	sqrt
	movsd	8(%rsp), %xmm5          # 8-byte Reload
                                        # xmm5 = mem[0],zero
	xorpd	%xmm4, %xmm4
	movapd	%xmm0, %xmm1
.LBB7_2:                                # %.preheader43.split
	movupd	(%rbx), %xmm0
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	divpd	%xmm1, %xmm0
	movupd	%xmm0, (%rbx)
	mulsd	%xmm5, %xmm5
	movsd	.LCPI7_0(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	%xmm5, %xmm0
	ucomisd	%xmm0, %xmm4
	ja	.LBB7_4
# BB#3:
	xorps	%xmm4, %xmm4
	sqrtsd	%xmm0, %xmm4
	ucomisd	%xmm4, %xmm4
	jp	.LBB7_5
.LBB7_4:
	movapd	%xmm4, %xmm0
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB7_5:                                # %call.sqrt44
	addq	$16, %rsp
	popq	%rbx
	jmp	sqrt                    # TAILCALL
.Lfunc_end7:
	.size	make_orthogonal, .Lfunc_end7-make_orthogonal
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.text
	.globl	find_gradient_f
	.p2align	4, 0x90
	.type	find_gradient_f,@function
find_gradient_f:                        # @find_gradient_f
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 16
.Lcfi28:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movapd	%xmm0, %xmm2
	movsd	P(%rip), %xmm3          # xmm3 = mem[0],zero
	movhpd	Q(%rip), %xmm3          # xmm3 = xmm3[0],mem[0]
	movapd	.LCPI8_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	addpd	%xmm0, %xmm3
	divpd	%xmm3, %xmm0
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	subpd	%xmm2, %xmm0
	movupd	%xmm0, (%rbx)
	movapd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm1
	xorpd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	mulsd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB8_2
# BB#1:                                 # %call.sqrt
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB8_2:                                # %.split
	movupd	(%rbx), %xmm0
	movapd	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	divpd	%xmm2, %xmm0
	movupd	%xmm0, (%rbx)
	movapd	%xmm1, %xmm0
	popq	%rbx
	retq
.Lfunc_end8:
	.size	find_gradient_f, .Lfunc_end8-find_gradient_f
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI9_1:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	find_dd_grad_f
	.p2align	4, 0x90
	.type	find_dd_grad_f,@function
find_dd_grad_f:                         # @find_dd_grad_f
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movsd	P(%rip), %xmm2          # xmm2 = mem[0],zero
	movhpd	Q(%rip), %xmm2          # xmm2 = xmm2[0],mem[0]
	movapd	.LCPI9_0(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00]
	addpd	%xmm3, %xmm2
	divpd	%xmm2, %xmm3
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movapd	%xmm3, %xmm2
	subpd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm2, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB9_2
# BB#1:                                 # %call.sqrt
	movapd	%xmm1, %xmm0
	movapd	%xmm3, 16(%rsp)         # 16-byte Spill
	movapd	%xmm2, (%rsp)           # 16-byte Spill
	callq	sqrt
	movapd	(%rsp), %xmm2           # 16-byte Reload
	movapd	16(%rsp), %xmm3         # 16-byte Reload
.LBB9_2:                                # %.split
	mulpd	%xmm3, %xmm3
	mulpd	%xmm2, %xmm3
	xorpd	.LCPI9_1(%rip), %xmm3
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	divpd	%xmm0, %xmm3
	movupd	%xmm3, (%rbx)
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end9:
	.size	find_dd_grad_f, .Lfunc_end9-find_dd_grad_f
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI10_0:
	.quad	-4617991057905706598    # double -0.80000000000000004
	.text
	.globl	find_g
	.p2align	4, 0x90
	.type	find_g,@function
find_g:                                 # @find_g
	.cfi_startproc
# BB#0:
	movsd	P(%rip), %xmm1          # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	movsd	Q(%rip), %xmm0          # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	addsd	.LCPI10_0(%rip), %xmm0
	retq
.Lfunc_end10:
	.size	find_g, .Lfunc_end10-find_g
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI11_0:
	.quad	-4606056518893174784    # double -5
	.text
	.globl	find_h
	.p2align	4, 0x90
	.type	find_h,@function
find_h:                                 # @find_h
	.cfi_startproc
# BB#0:
	movsd	Q(%rip), %xmm0          # xmm0 = mem[0],zero
	mulsd	.LCPI11_0(%rip), %xmm0
	addsd	P(%rip), %xmm0
	retq
.Lfunc_end11:
	.size	find_h, .Lfunc_end11-find_h
	.cfi_endproc

	.type	P,@object               # @P
	.data
	.p2align	3
P:
	.quad	4607182418800017408     # double 1
	.size	P, 8

	.type	Q,@object               # @Q
	.p2align	3
Q:
	.quad	4607182418800017408     # double 1
	.size	Q, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
