	.text
	.file	"walksub.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4598175219545276416     # double 0.25
	.text
	.globl	walksub
	.p2align	4, 0x90
	.type	walksub,@function
walksub:                                # @walksub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$216, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 256
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %rbx
	movq	%rdi, %r14
	leaq	256(%rsp), %rbp
	movaps	256(%rsp), %xmm5
	movapd	272(%rsp), %xmm4
	movaps	288(%rsp), %xmm2
	movaps	304(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movupd	%xmm4, 16(%rsp)
	movups	%xmm5, (%rsp)
	movq	%rbx, %rdi
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	movsd	%xmm1, 80(%rsp)         # 8-byte Spill
	callq	subdivp
	testw	%ax, %ax
	je	.LBB0_17
# BB#1:                                 # %.preheader
	movsd	72(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	mulsd	.LCPI0_0(%rip), %xmm4
	incl	%r15d
	movq	48(%rbx), %rsi
	testq	%rsi, %rsi
	movsd	%xmm4, 72(%rsp)         # 8-byte Spill
	je	.LBB0_3
# BB#2:
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	32(%rbp), %xmm2
	movups	48(%rbp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	88(%rsp), %rdi
	movapd	%xmm4, %xmm0
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	%r15d, %edx
	callq	walksub
	movsd	72(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movupd	88(%rsp), %xmm0
	movupd	104(%rsp), %xmm1
	movups	120(%rsp), %xmm2
	movups	136(%rsp), %xmm3
	movups	%xmm3, 48(%rbp)
	movups	%xmm2, 32(%rbp)
	movupd	%xmm1, 16(%rbp)
	movupd	%xmm0, (%rbp)
.LBB0_3:
	movq	56(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB0_5
# BB#4:
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	32(%rbp), %xmm2
	movups	48(%rbp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	88(%rsp), %rdi
	movapd	%xmm4, %xmm0
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	%r15d, %edx
	callq	walksub
	movsd	72(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movupd	88(%rsp), %xmm0
	movupd	104(%rsp), %xmm1
	movups	120(%rsp), %xmm2
	movups	136(%rsp), %xmm3
	movups	%xmm3, 48(%rbp)
	movups	%xmm2, 32(%rbp)
	movupd	%xmm1, 16(%rbp)
	movupd	%xmm0, (%rbp)
.LBB0_5:
	movq	64(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB0_7
# BB#6:
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	32(%rbp), %xmm2
	movups	48(%rbp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	88(%rsp), %rdi
	movapd	%xmm4, %xmm0
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	%r15d, %edx
	callq	walksub
	movsd	72(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movupd	88(%rsp), %xmm0
	movupd	104(%rsp), %xmm1
	movups	120(%rsp), %xmm2
	movups	136(%rsp), %xmm3
	movups	%xmm3, 48(%rbp)
	movups	%xmm2, 32(%rbp)
	movupd	%xmm1, 16(%rbp)
	movupd	%xmm0, (%rbp)
.LBB0_7:
	movq	72(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB0_9
# BB#8:
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	32(%rbp), %xmm2
	movups	48(%rbp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	88(%rsp), %rdi
	movapd	%xmm4, %xmm0
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	%r15d, %edx
	callq	walksub
	movsd	72(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movupd	88(%rsp), %xmm0
	movupd	104(%rsp), %xmm1
	movups	120(%rsp), %xmm2
	movups	136(%rsp), %xmm3
	movups	%xmm3, 48(%rbp)
	movups	%xmm2, 32(%rbp)
	movupd	%xmm1, 16(%rbp)
	movupd	%xmm0, (%rbp)
.LBB0_9:
	movq	80(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB0_11
# BB#10:
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	32(%rbp), %xmm2
	movups	48(%rbp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	88(%rsp), %rdi
	movapd	%xmm4, %xmm0
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	%r15d, %edx
	callq	walksub
	movsd	72(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movupd	88(%rsp), %xmm0
	movupd	104(%rsp), %xmm1
	movups	120(%rsp), %xmm2
	movups	136(%rsp), %xmm3
	movups	%xmm3, 48(%rbp)
	movups	%xmm2, 32(%rbp)
	movupd	%xmm1, 16(%rbp)
	movupd	%xmm0, (%rbp)
.LBB0_11:
	movq	88(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB0_13
# BB#12:
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	32(%rbp), %xmm2
	movups	48(%rbp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	88(%rsp), %rdi
	movapd	%xmm4, %xmm0
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	%r15d, %edx
	callq	walksub
	movsd	72(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movupd	88(%rsp), %xmm0
	movupd	104(%rsp), %xmm1
	movups	120(%rsp), %xmm2
	movups	136(%rsp), %xmm3
	movups	%xmm3, 48(%rbp)
	movups	%xmm2, 32(%rbp)
	movupd	%xmm1, 16(%rbp)
	movupd	%xmm0, (%rbp)
.LBB0_13:
	movq	96(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB0_15
# BB#14:
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	32(%rbp), %xmm2
	movups	48(%rbp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	88(%rsp), %rdi
	movapd	%xmm4, %xmm0
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	%r15d, %edx
	callq	walksub
	movsd	72(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movupd	88(%rsp), %xmm0
	movupd	104(%rsp), %xmm1
	movups	120(%rsp), %xmm2
	movups	136(%rsp), %xmm3
	movups	%xmm3, 48(%rbp)
	movups	%xmm2, 32(%rbp)
	movupd	%xmm1, 16(%rbp)
	movupd	%xmm0, (%rbp)
.LBB0_15:
	movq	104(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB0_20
# BB#16:
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	32(%rbp), %xmm2
	movups	48(%rbp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	88(%rsp), %rdi
	movapd	%xmm4, %xmm0
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	%r15d, %edx
	callq	walksub
	movupd	88(%rsp), %xmm0
	movupd	104(%rsp), %xmm1
	movups	120(%rsp), %xmm2
	movups	136(%rsp), %xmm3
	jmp	.LBB0_19
.LBB0_17:
	cmpq	%rbx, (%rbp)
	je	.LBB0_20
# BB#18:
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	32(%rbp), %xmm2
	movups	48(%rbp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	152(%rsp), %rdi
	movq	%rbx, %rsi
	callq	gravsub
	movupd	152(%rsp), %xmm0
	movupd	168(%rsp), %xmm1
	movups	184(%rsp), %xmm2
	movups	200(%rsp), %xmm3
.LBB0_19:                               # %.loopexit
	movups	%xmm3, 48(%rbp)
	movups	%xmm2, 32(%rbp)
	movupd	%xmm1, 16(%rbp)
	movupd	%xmm0, (%rbp)
.LBB0_20:                               # %.loopexit
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	32(%rbp), %xmm2
	movups	48(%rbp), %xmm3
	movups	%xmm3, 48(%r14)
	movups	%xmm2, 32(%r14)
	movups	%xmm1, 16(%r14)
	movups	%xmm0, (%r14)
	movq	%r14, %rax
	addq	$216, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	walksub, .Lfunc_end0-walksub
	.cfi_endproc

	.type	NumNodes,@object        # @NumNodes
	.comm	NumNodes,4,4
	.type	root,@object            # @root
	.comm	root,8,8
	.type	rmin,@object            # @rmin
	.comm	rmin,24,16
	.type	xxxrsize,@object        # @xxxrsize
	.comm	xxxrsize,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
