	.text
	.file	"toast_audio.bc"
	.globl	audio_init_input
	.p2align	4, 0x90
	.type	audio_init_input,@function
audio_init_input:                       # @audio_init_input
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	in(%rip), %rdi
	callq	fgetc
	cmpl	$46, %eax
	jne	.LBB0_17
# BB#1:
	movq	in(%rip), %rdi
	callq	fgetc
	cmpl	$115, %eax
	jne	.LBB0_17
# BB#2:
	movq	in(%rip), %rdi
	callq	fgetc
	cmpl	$110, %eax
	jne	.LBB0_17
# BB#3:
	movq	in(%rip), %rdi
	callq	fgetc
	cmpl	$100, %eax
	jne	.LBB0_17
# BB#4:
	movq	in(%rip), %rbp
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB0_17
# BB#5:
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	cmpl	$-1, %r12d
	je	.LBB0_17
# BB#6:
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %r14d
	cmpl	$-1, %r14d
	je	.LBB0_17
# BB#7:
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, 4(%rsp)           # 4-byte Spill
	cmpl	$-1, %eax
	je	.LBB0_17
# BB#8:
	movq	in(%rip), %rbp
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB0_17
# BB#9:
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB0_17
# BB#10:
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB0_17
# BB#11:
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB0_17
# BB#12:
	movq	in(%rip), %r15
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB0_17
# BB#13:
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %r13d
	cmpl	$-1, %r13d
	je	.LBB0_17
# BB#14:
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, (%rsp)            # 4-byte Spill
	cmpl	$-1, %eax
	je	.LBB0_17
# BB#15:
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %r15d
	cmpl	$-1, %r15d
	je	.LBB0_17
# BB#16:
	shll	$8, %ebx
	movzbl	%r12b, %eax
	orl	%ebx, %eax
	shll	$8, %eax
	movzbl	%r14b, %ecx
	orl	%eax, %ecx
	shll	$8, %ecx
	movzbl	4(%rsp), %esi           # 1-byte Folded Reload
	orl	%ecx, %esi
	movq	in(%rip), %rdi
	addq	$-16, %rsi
	movl	$1, %edx
	callq	fseek
	testl	%eax, %eax
	js	.LBB0_17
# BB#18:
	shll	$8, %ebp
	movzbl	%r13b, %eax
	orl	%ebp, %eax
	shll	$8, %eax
	movzbl	(%rsp), %edx            # 1-byte Folded Reload
	orl	%eax, %edx
	shll	$8, %edx
	movzbl	%r15b, %ecx
	orl	%edx, %ecx
	leaq	-1(%rcx), %rax
	cmpq	$3, %rax
	jae	.LBB0_19
# BB#20:                                # %switch.lookup
	movq	.Lswitch.table-8(,%rcx,8), %rax
.LBB0_21:
	movq	%rax, input(%rip)
	xorl	%eax, %eax
	jmp	.LBB0_22
.LBB0_17:                               # %get_u32.exit.thread
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	inname(%rip), %rax
	testq	%rax, %rax
	movl	$.L.str.1, %ecx
	cmovneq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rdx, %r8
	callq	fprintf
	movl	$-1, %eax
.LBB0_22:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_19:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	inname(%rip), %r8
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$ulaw_input, %eax
	jmp	.LBB0_21
.Lfunc_end0:
	.size	audio_init_input, .Lfunc_end0-audio_init_input
	.cfi_endproc

	.globl	audio_init_output
	.p2align	4, 0x90
	.type	audio_init_output,@function
audio_init_output:                      # @audio_init_output
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	out(%rip), %rsi
	movl	$.L.str.3, %edi
	callq	fputs
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#1:
	movq	out(%rip), %rbx
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#2:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#3:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#4:                                 # %put_u32.exit
	movl	$32, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#5:
	movq	out(%rip), %rbx
	movl	$-1, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#6:
	movl	$-1, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#7:
	movl	$-1, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#8:                                 # %put_u32.exit2
	movl	$-1, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#9:
	movq	out(%rip), %rbx
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#10:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#11:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#12:                                # %put_u32.exit4
	movl	$1, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#13:
	movq	out(%rip), %rbx
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#14:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#15:
	movl	$31, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#16:                                # %put_u32.exit6
	movl	$64, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#17:
	movq	out(%rip), %rbx
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#18:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#19:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#20:                                # %put_u32.exit8
	movl	$1, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#21:
	movq	out(%rip), %rbx
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#22:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#23:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#24:                                # %put_u32.exit10
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#25:
	movq	out(%rip), %rbx
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movb	$1, %bpl
	cmpl	$-1, %eax
	je	.LBB1_29
# BB#26:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_29
# BB#27:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_29
# BB#28:
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	sete	%bpl
.LBB1_29:                               # %put_u32.exit12
	movzbl	%bpl, %eax
	negl	%eax
	jmp	.LBB1_31
.LBB1_30:                               # %put_u32.exit.thread
	movl	$-1, %eax
.LBB1_31:                               # %put_u32.exit.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	audio_init_output, .Lfunc_end1-audio_init_output
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s: bad (missing?) header in Sun audio file \"%s\";\n\tTry one of -u, -a, -l instead (%s -h for help).\n"
	.size	.L.str, 100

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"stdin"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s: warning: file format #%lu for %s not implemented, defaulting to u-law.\n"
	.size	.L.str.2, 76

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	".snd"
	.size	.L.str.3, 5

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	ulaw_input
	.quad	alaw_input
	.quad	linear_input
	.size	.Lswitch.table, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
