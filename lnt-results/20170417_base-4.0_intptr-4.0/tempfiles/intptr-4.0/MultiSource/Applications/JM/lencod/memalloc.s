	.text
	.file	"memalloc.bc"
	.globl	init_top_bot_planes
	.p2align	4, 0x90
	.type	init_top_bot_planes,@function
init_top_bot_planes:                    # @init_top_bot_planes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r12
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	%r14d, %eax
	shrl	$31, %eax
	addl	%r14d, %eax
	sarl	%eax
	movslq	%eax, %rbp
	movl	$8, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.LBB0_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB0_2:
	movl	$8, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	%rax, (%r15)
	testq	%rax, %rax
	jne	.LBB0_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.1, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB0_4:                                # %.preheader
	cmpl	$2, %r14d
	jl	.LBB0_7
# BB#5:                                 # %.lr.ph.preheader
	addq	$8, %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rbx), %rcx
	movq	(%r12), %rdx
	movq	%rcx, (%rdx,%rax,8)
	movq	(%rbx), %rcx
	movq	(%r15), %rdx
	movq	%rcx, (%rdx,%rax,8)
	incq	%rax
	addq	$16, %rbx
	cmpq	%rbp, %rax
	jl	.LBB0_6
.LBB0_7:                                # %._crit_edge
	shll	$3, %r14d
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	init_top_bot_planes, .Lfunc_end0-init_top_bot_planes
	.cfi_endproc

	.globl	no_mem_exit
	.p2align	4, 0x90
	.type	no_mem_exit,@function
no_mem_exit:                            # @no_mem_exit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	popq	%rax
	jmp	error                   # TAILCALL
.Lfunc_end1:
	.size	no_mem_exit, .Lfunc_end1-no_mem_exit
	.cfi_endproc

	.globl	free_top_bot_planes
	.p2align	4, 0x90
	.type	free_top_bot_planes,@function
free_top_bot_planes:                    # @free_top_bot_planes
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
.Lcfi12:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	callq	free
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end2:
	.size	free_top_bot_planes, .Lfunc_end2-free_top_bot_planes
	.cfi_endproc

	.globl	get_mem2Dpel
	.p2align	4, 0x90
	.type	get_mem2Dpel,@function
get_mem2Dpel:                           # @get_mem2Dpel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movslq	%ebp, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	movq	%r12, (%rbx)
	testq	%r12, %r12
	jne	.LBB3_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.2, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %r12
.LBB3_2:
	movl	%r15d, %r14d
	imull	%ebp, %r14d
	movslq	%r14d, %rdi
	movl	$2, %esi
	callq	calloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.LBB3_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.2, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB3_4:                                # %.preheader
	cmpl	$2, %ebp
	jl	.LBB3_13
# BB#5:                                 # %.lr.ph
	movslq	%r15d, %rax
	movl	%ebp, %r9d
	addl	$3, %ebp
	leaq	-2(%r9), %r8
	andq	$3, %rbp
	je	.LBB3_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	leaq	(%rax,%rax), %rdi
	.p2align	4, 0x90
.LBB3_8:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rcx
	addq	%rdi, %rcx
	movq	%rcx, 8(%rsi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB3_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %r8
	jae	.LBB3_11
	jmp	.LBB3_13
.LBB3_6:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB3_13
.LBB3_11:                               # %.lr.ph.new
	addq	%rax, %rax
	.p2align	4, 0x90
.LBB3_12:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	-8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 8(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	16(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 24(%rcx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %r9
	jne	.LBB3_12
.LBB3_13:                               # %._crit_edge
	addl	%r14d, %r14d
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	get_mem2Dpel, .Lfunc_end3-get_mem2Dpel
	.cfi_endproc

	.globl	get_mem3Dpel
	.p2align	4, 0x90
	.type	get_mem3Dpel,@function
get_mem3Dpel:                           # @get_mem3Dpel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 64
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r12d
	movl	%esi, %r14d
	movq	%rdi, %r13
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r13)
	testq	%rax, %rax
	jne	.LBB4_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.3, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB4_2:                                # %.preheader
	testl	%r14d, %r14d
	jle	.LBB4_5
# BB#3:                                 # %.lr.ph.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	addq	%rbp, %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	callq	get_mem2Dpel
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB4_4
.LBB4_5:                                # %._crit_edge
	imull	%r12d, %r14d
	imull	%r15d, %r14d
	addl	%r14d, %r14d
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	get_mem3Dpel, .Lfunc_end4-get_mem3Dpel
	.cfi_endproc

	.globl	get_mem4Dpel
	.p2align	4, 0x90
	.type	get_mem4Dpel,@function
get_mem4Dpel:                           # @get_mem4Dpel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 96
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movl	%ecx, %ebp
	movl	%edx, (%rsp)            # 4-byte Spill
	movl	%esi, %r14d
	movq	%rdi, %r15
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%rax, (%r15)
	testq	%rax, %rax
	jne	.LBB5_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB5_2:                                # %.preheader
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	testl	%r14d, %r14d
	jle	.LBB5_14
# BB#3:                                 # %.lr.ph
	movl	(%rsp), %eax            # 4-byte Reload
	movslq	%eax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	movl	4(%rsp), %ecx           # 4-byte Reload
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jle	.LBB5_4
# BB#8:                                 # %.lr.ph.split.us.preheader
	movl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB5_9:                                # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_12 Depth 2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r14
	movl	$8, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	calloc
	movq	%rax, (%r14,%r12,8)
	testq	%rax, %rax
	jne	.LBB5_11
# BB#10:                                #   in Loop: Header=BB5_9 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.3, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB5_11:                               # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB5_9 Depth=1
	xorl	%r15d, %r15d
	movq	32(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_12:                               # %.lr.ph.i.us
                                        #   Parent Loop BB5_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r12,8), %rdi
	addq	%r15, %rdi
	movl	%ebp, %esi
	movl	%ebx, %edx
	callq	get_mem2Dpel
	addq	$8, %r15
	decq	%r13
	jne	.LBB5_12
# BB#13:                                # %get_mem3Dpel.exit.loopexit.us
                                        #   in Loop: Header=BB5_9 Depth=1
	incq	%r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB5_9
	jmp	.LBB5_14
.LBB5_4:                                # %.lr.ph.split.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r15
	movl	$8, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	calloc
	movq	%rax, (%r15,%r14,8)
	testq	%rax, %rax
	jne	.LBB5_7
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.3, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB5_7:                                # %get_mem3Dpel.exit
                                        #   in Loop: Header=BB5_5 Depth=1
	incq	%r14
	cmpq	%r14, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB5_5
.LBB5_14:                               # %._crit_edge
	movl	4(%rsp), %eax           # 4-byte Reload
	imull	(%rsp), %eax            # 4-byte Folded Reload
	imull	%ebp, %eax
	imull	%ebx, %eax
	addl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	get_mem4Dpel, .Lfunc_end5-get_mem4Dpel
	.cfi_endproc

	.globl	get_mem5Dpel
	.p2align	4, 0x90
	.type	get_mem5Dpel,@function
get_mem5Dpel:                           # @get_mem5Dpel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 64
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movl	%r8d, %r13d
	movl	%ecx, %r12d
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movslq	%r15d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%rbx)
	testq	%rax, %rax
	jne	.LBB6_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.5, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB6_2:                                # %.preheader
	movl	%r15d, (%rsp)           # 4-byte Spill
	testl	%r15d, %r15d
	movl	%r13d, %ecx
	movl	%r12d, %edx
	movl	%r14d, %esi
	jle	.LBB6_5
# BB#3:                                 # %.lr.ph.preheader
	movl	(%rsp), %r14d           # 4-byte Reload
	xorl	%r15d, %r15d
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r12
	movq	(%r12), %rdi
	addq	%r15, %rdi
	movl	%ebp, %r13d
	movl	%edx, %ebx
	movl	%esi, %ebp
	movl	%r13d, %r8d
	callq	get_mem4Dpel
	movl	%ebp, %esi
	movl	%ebx, %edx
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%r13d, %ebp
	movq	%r12, %rbx
	addq	$8, %r15
	decq	%r14
	jne	.LBB6_4
.LBB6_5:                                # %._crit_edge
	movl	(%rsp), %eax            # 4-byte Reload
	imull	%esi, %eax
	imull	%edx, %eax
	imull	%ecx, %eax
	imull	%ebp, %eax
	addl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	get_mem5Dpel, .Lfunc_end6-get_mem5Dpel
	.cfi_endproc

	.globl	free_mem2Dpel
	.p2align	4, 0x90
	.type	free_mem2Dpel,@function
free_mem2Dpel:                          # @free_mem2Dpel
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 16
.Lcfi63:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB7_5
# BB#1:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_3
# BB#2:
	callq	free
	jmp	.LBB7_4
.LBB7_5:
	movl	$.L.str.6, %edi
	movl	$100, %esi
	popq	%rbx
	jmp	error                   # TAILCALL
.LBB7_3:
	movl	$.L.str.6, %edi
	movl	$100, %esi
	callq	error
.LBB7_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end7:
	.size	free_mem2Dpel, .Lfunc_end7-free_mem2Dpel
	.cfi_endproc

	.globl	free_mem3Dpel
	.p2align	4, 0x90
	.type	free_mem3Dpel,@function
free_mem3Dpel:                          # @free_mem3Dpel
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 48
.Lcfi69:
	.cfi_offset %rbx, -40
.Lcfi70:
	.cfi_offset %r12, -32
.Lcfi71:
	.cfi_offset %r14, -24
.Lcfi72:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB8_11
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB8_10
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %r12d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB8_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.LBB8_8
# BB#4:                                 #   in Loop: Header=BB8_3 Depth=1
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB8_6
# BB#5:                                 #   in Loop: Header=BB8_3 Depth=1
	callq	free
	jmp	.LBB8_7
	.p2align	4, 0x90
.LBB8_8:                                #   in Loop: Header=BB8_3 Depth=1
	movl	$.L.str.6, %edi
	movl	$100, %esi
	callq	error
	jmp	.LBB8_9
	.p2align	4, 0x90
.LBB8_6:                                #   in Loop: Header=BB8_3 Depth=1
	movl	$.L.str.6, %edi
	movl	$100, %esi
	callq	error
.LBB8_7:                                #   in Loop: Header=BB8_3 Depth=1
	movq	%r15, %rdi
	callq	free
.LBB8_9:                                # %free_mem2Dpel.exit
                                        #   in Loop: Header=BB8_3 Depth=1
	addq	$8, %rbx
	decq	%r12
	jne	.LBB8_3
.LBB8_10:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB8_11:
	movl	$.L.str.7, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	error                   # TAILCALL
.Lfunc_end8:
	.size	free_mem3Dpel, .Lfunc_end8-free_mem3Dpel
	.cfi_endproc

	.globl	free_mem4Dpel
	.p2align	4, 0x90
	.type	free_mem4Dpel,@function
free_mem4Dpel:                          # @free_mem4Dpel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi77:
	.cfi_def_cfa_offset 48
.Lcfi78:
	.cfi_offset %rbx, -40
.Lcfi79:
	.cfi_offset %r14, -32
.Lcfi80:
	.cfi_offset %r15, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB9_5
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB9_4
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %ebx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movl	%r15d, %esi
	callq	free_mem3Dpel
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB9_3
.LBB9_4:                                # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB9_5:
	movl	$.L.str.8, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.Lfunc_end9:
	.size	free_mem4Dpel, .Lfunc_end9-free_mem4Dpel
	.cfi_endproc

	.globl	free_mem5Dpel
	.p2align	4, 0x90
	.type	free_mem5Dpel,@function
free_mem5Dpel:                          # @free_mem5Dpel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi88:
	.cfi_def_cfa_offset 80
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	testq	%rdi, %rdi
	je	.LBB10_16
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB10_15
# BB#2:                                 # %.lr.ph
	testl	%edx, %edx
	movl	%esi, %r15d
	jle	.LBB10_3
# BB#6:                                 # %.lr.ph.split.us.preheader
	movl	%edx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_7:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_9 Depth 2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbx,8), %r12
	testq	%r12, %r12
	je	.LBB10_11
# BB#8:                                 # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB10_7 Depth=1
	movq	%r12, %r13
	movq	16(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB10_9:                               # %.lr.ph.i.us
                                        #   Parent Loop BB10_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rdi
	movl	%ebp, %esi
	callq	free_mem3Dpel
	addq	$8, %r13
	decq	%r14
	jne	.LBB10_9
# BB#10:                                # %._crit_edge.i.loopexit.us
                                        #   in Loop: Header=BB10_7 Depth=1
	movq	%r12, %rdi
	callq	free
	jmp	.LBB10_12
	.p2align	4, 0x90
.LBB10_11:                              #   in Loop: Header=BB10_7 Depth=1
	movl	$.L.str.8, %edi
	movl	$100, %esi
	callq	error
.LBB10_12:                              # %free_mem4Dpel.exit.us
                                        #   in Loop: Header=BB10_7 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jne	.LBB10_7
	jmp	.LBB10_15
.LBB10_16:
	movl	$.L.str.9, %edi
	movl	$100, %esi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.LBB10_3:
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB10_4:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_13
# BB#5:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB10_4 Depth=1
	callq	free
	jmp	.LBB10_14
	.p2align	4, 0x90
.LBB10_13:                              #   in Loop: Header=BB10_4 Depth=1
	movl	$.L.str.8, %edi
	movl	$100, %esi
	callq	error
.LBB10_14:                              # %free_mem4Dpel.exit
                                        #   in Loop: Header=BB10_4 Depth=1
	addq	$8, %rbx
	decq	%r15
	jne	.LBB10_4
.LBB10_15:                              # %._crit_edge
	movq	8(%rsp), %rdi           # 8-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end10:
	.size	free_mem5Dpel, .Lfunc_end10-free_mem5Dpel
	.cfi_endproc

	.globl	get_mem2D
	.p2align	4, 0x90
	.type	get_mem2D,@function
get_mem2D:                              # @get_mem2D
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 48
.Lcfi100:
	.cfi_offset %rbx, -48
.Lcfi101:
	.cfi_offset %r12, -40
.Lcfi102:
	.cfi_offset %r14, -32
.Lcfi103:
	.cfi_offset %r15, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbp, (%rbx)
	testq	%rbp, %rbp
	jne	.LBB11_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.10, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %rbp
.LBB11_2:
	movl	%r15d, %r12d
	imull	%r14d, %r12d
	movslq	%r12d, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, (%rbp)
	testq	%rax, %rax
	jne	.LBB11_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.10, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB11_4:                               # %.preheader
	cmpl	$2, %r14d
	jl	.LBB11_12
# BB#5:                                 # %.lr.ph
	movslq	%r15d, %rax
	movl	%r14d, %ecx
	addl	$3, %r14d
	leaq	-2(%rcx), %rsi
	andq	$3, %r14
	je	.LBB11_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB11_8:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rdi,%rdx,8), %rbp
	addq	%rax, %rbp
	movq	%rbp, 8(%rdi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %r14
	jne	.LBB11_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %rsi
	jae	.LBB11_11
	jmp	.LBB11_12
.LBB11_6:
	movl	$1, %edx
	cmpq	$3, %rsi
	jb	.LBB11_12
	.p2align	4, 0x90
.LBB11_11:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	-8(%rsi,%rdx,8), %rdi
	addq	%rax, %rdi
	movq	%rdi, (%rsi,%rdx,8)
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rdi
	addq	%rax, %rdi
	movq	%rdi, 8(%rsi,%rdx,8)
	movq	(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rdi
	addq	%rax, %rdi
	movq	%rdi, 16(%rsi,%rdx,8)
	movq	(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rdi
	addq	%rax, %rdi
	movq	%rdi, 24(%rsi,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB11_11
.LBB11_12:                              # %._crit_edge
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	get_mem2D, .Lfunc_end11-get_mem2D
	.cfi_endproc

	.globl	get_mem2Dint
	.p2align	4, 0x90
	.type	get_mem2Dint,@function
get_mem2Dint:                           # @get_mem2Dint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi108:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 48
.Lcfi110:
	.cfi_offset %rbx, -48
.Lcfi111:
	.cfi_offset %r12, -40
.Lcfi112:
	.cfi_offset %r14, -32
.Lcfi113:
	.cfi_offset %r15, -24
.Lcfi114:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movslq	%ebp, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	movq	%r12, (%rbx)
	testq	%r12, %r12
	jne	.LBB12_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.11, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %r12
.LBB12_2:
	movl	%r15d, %r14d
	imull	%ebp, %r14d
	movslq	%r14d, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.LBB12_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.11, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB12_4:                               # %.preheader
	cmpl	$2, %ebp
	jl	.LBB12_13
# BB#5:                                 # %.lr.ph
	movslq	%r15d, %rax
	movl	%ebp, %r9d
	addl	$3, %ebp
	leaq	-2(%r9), %r8
	andq	$3, %rbp
	je	.LBB12_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	leaq	(,%rax,4), %rdi
	.p2align	4, 0x90
.LBB12_8:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rcx
	addq	%rdi, %rcx
	movq	%rcx, 8(%rsi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB12_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %r8
	jae	.LBB12_11
	jmp	.LBB12_13
.LBB12_6:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB12_13
.LBB12_11:                              # %.lr.ph.new
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB12_12:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	-8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 8(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	16(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 24(%rcx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %r9
	jne	.LBB12_12
.LBB12_13:                              # %._crit_edge
	shll	$2, %r14d
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	get_mem2Dint, .Lfunc_end12-get_mem2Dint
	.cfi_endproc

	.globl	get_mem2Dint64
	.p2align	4, 0x90
	.type	get_mem2Dint64,@function
get_mem2Dint64:                         # @get_mem2Dint64
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi118:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 48
.Lcfi120:
	.cfi_offset %rbx, -48
.Lcfi121:
	.cfi_offset %r12, -40
.Lcfi122:
	.cfi_offset %r14, -32
.Lcfi123:
	.cfi_offset %r15, -24
.Lcfi124:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movslq	%ebp, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	movq	%r12, (%rbx)
	testq	%r12, %r12
	jne	.LBB13_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.12, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %r12
.LBB13_2:
	movl	%r15d, %r14d
	imull	%ebp, %r14d
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.LBB13_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.12, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB13_4:                               # %.preheader
	cmpl	$2, %ebp
	jl	.LBB13_13
# BB#5:                                 # %.lr.ph
	movslq	%r15d, %rax
	movl	%ebp, %r9d
	addl	$3, %ebp
	leaq	-2(%r9), %r8
	andq	$3, %rbp
	je	.LBB13_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	leaq	(,%rax,8), %rdi
	.p2align	4, 0x90
.LBB13_8:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rcx
	addq	%rdi, %rcx
	movq	%rcx, 8(%rsi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB13_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %r8
	jae	.LBB13_11
	jmp	.LBB13_13
.LBB13_6:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB13_13
.LBB13_11:                              # %.lr.ph.new
	shlq	$3, %rax
	.p2align	4, 0x90
.LBB13_12:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	-8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 8(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	16(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 24(%rcx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %r9
	jne	.LBB13_12
.LBB13_13:                              # %._crit_edge
	shll	$3, %r14d
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	get_mem2Dint64, .Lfunc_end13-get_mem2Dint64
	.cfi_endproc

	.globl	get_mem3D
	.p2align	4, 0x90
	.type	get_mem3D,@function
get_mem3D:                              # @get_mem3D
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi125:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi126:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi128:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi129:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi131:
	.cfi_def_cfa_offset 64
.Lcfi132:
	.cfi_offset %rbx, -56
.Lcfi133:
	.cfi_offset %r12, -48
.Lcfi134:
	.cfi_offset %r13, -40
.Lcfi135:
	.cfi_offset %r14, -32
.Lcfi136:
	.cfi_offset %r15, -24
.Lcfi137:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r13d
	movl	%esi, %r14d
	movq	%rdi, %r12
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.LBB14_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.13, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB14_2:                               # %.preheader
	testl	%r14d, %r14d
	jle	.LBB14_5
# BB#3:                                 # %.lr.ph.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdi
	addq	%rbp, %rdi
	movl	%r13d, %esi
	movl	%r15d, %edx
	callq	get_mem2D
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB14_4
.LBB14_5:                               # %._crit_edge
	imull	%r14d, %r13d
	imull	%r15d, %r13d
	movl	%r13d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	get_mem3D, .Lfunc_end14-get_mem3D
	.cfi_endproc

	.globl	get_mem3Dint
	.p2align	4, 0x90
	.type	get_mem3Dint,@function
get_mem3Dint:                           # @get_mem3Dint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi138:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi139:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi140:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi141:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi142:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi144:
	.cfi_def_cfa_offset 64
.Lcfi145:
	.cfi_offset %rbx, -56
.Lcfi146:
	.cfi_offset %r12, -48
.Lcfi147:
	.cfi_offset %r13, -40
.Lcfi148:
	.cfi_offset %r14, -32
.Lcfi149:
	.cfi_offset %r15, -24
.Lcfi150:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r12d
	movl	%esi, %r14d
	movq	%rdi, %r13
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r13)
	testq	%rax, %rax
	jne	.LBB15_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.14, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB15_2:                               # %.preheader
	testl	%r14d, %r14d
	jle	.LBB15_5
# BB#3:                                 # %.lr.ph.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB15_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	addq	%rbp, %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	callq	get_mem2Dint
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB15_4
.LBB15_5:                               # %._crit_edge
	imull	%r12d, %r14d
	imull	%r15d, %r14d
	shll	$2, %r14d
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	get_mem3Dint, .Lfunc_end15-get_mem3Dint
	.cfi_endproc

	.globl	get_mem3Dint64
	.p2align	4, 0x90
	.type	get_mem3Dint64,@function
get_mem3Dint64:                         # @get_mem3Dint64
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi151:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi152:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi153:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi154:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi155:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi157:
	.cfi_def_cfa_offset 64
.Lcfi158:
	.cfi_offset %rbx, -56
.Lcfi159:
	.cfi_offset %r12, -48
.Lcfi160:
	.cfi_offset %r13, -40
.Lcfi161:
	.cfi_offset %r14, -32
.Lcfi162:
	.cfi_offset %r15, -24
.Lcfi163:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r12d
	movl	%esi, %r14d
	movq	%rdi, %r13
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r13)
	testq	%rax, %rax
	jne	.LBB16_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.15, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB16_2:                               # %.preheader
	testl	%r14d, %r14d
	jle	.LBB16_5
# BB#3:                                 # %.lr.ph.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	addq	%rbp, %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	callq	get_mem2Dint64
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB16_4
.LBB16_5:                               # %._crit_edge
	imull	%r12d, %r14d
	imull	%r15d, %r14d
	shll	$3, %r14d
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	get_mem3Dint64, .Lfunc_end16-get_mem3Dint64
	.cfi_endproc

	.globl	get_mem4Dint
	.p2align	4, 0x90
	.type	get_mem4Dint,@function
get_mem4Dint:                           # @get_mem4Dint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi166:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi167:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi168:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi169:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi170:
	.cfi_def_cfa_offset 96
.Lcfi171:
	.cfi_offset %rbx, -56
.Lcfi172:
	.cfi_offset %r12, -48
.Lcfi173:
	.cfi_offset %r13, -40
.Lcfi174:
	.cfi_offset %r14, -32
.Lcfi175:
	.cfi_offset %r15, -24
.Lcfi176:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movl	%ecx, %ebp
	movl	%edx, (%rsp)            # 4-byte Spill
	movl	%esi, %r14d
	movq	%rdi, %r15
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%rax, (%r15)
	testq	%rax, %rax
	jne	.LBB17_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.16, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB17_2:                               # %.preheader
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	testl	%r14d, %r14d
	jle	.LBB17_14
# BB#3:                                 # %.lr.ph
	movl	(%rsp), %eax            # 4-byte Reload
	movslq	%eax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	movl	4(%rsp), %ecx           # 4-byte Reload
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jle	.LBB17_4
# BB#8:                                 # %.lr.ph.split.us.preheader
	movl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB17_9:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_12 Depth 2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r14
	movl	$8, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	calloc
	movq	%rax, (%r14,%r12,8)
	testq	%rax, %rax
	jne	.LBB17_11
# BB#10:                                #   in Loop: Header=BB17_9 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.14, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB17_11:                              # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB17_9 Depth=1
	xorl	%r15d, %r15d
	movq	32(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB17_12:                              # %.lr.ph.i.us
                                        #   Parent Loop BB17_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r12,8), %rdi
	addq	%r15, %rdi
	movl	%ebp, %esi
	movl	%ebx, %edx
	callq	get_mem2Dint
	addq	$8, %r15
	decq	%r13
	jne	.LBB17_12
# BB#13:                                # %get_mem3Dint.exit.loopexit.us
                                        #   in Loop: Header=BB17_9 Depth=1
	incq	%r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB17_9
	jmp	.LBB17_14
.LBB17_4:                               # %.lr.ph.split.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB17_5:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r15
	movl	$8, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	calloc
	movq	%rax, (%r15,%r14,8)
	testq	%rax, %rax
	jne	.LBB17_7
# BB#6:                                 #   in Loop: Header=BB17_5 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.14, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB17_7:                               # %get_mem3Dint.exit
                                        #   in Loop: Header=BB17_5 Depth=1
	incq	%r14
	cmpq	%r14, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB17_5
.LBB17_14:                              # %._crit_edge
	movl	4(%rsp), %eax           # 4-byte Reload
	imull	(%rsp), %eax            # 4-byte Folded Reload
	imull	%ebp, %eax
	imull	%ebx, %eax
	shll	$2, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	get_mem4Dint, .Lfunc_end17-get_mem4Dint
	.cfi_endproc

	.globl	get_mem5Dint
	.p2align	4, 0x90
	.type	get_mem5Dint,@function
get_mem5Dint:                           # @get_mem5Dint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi177:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi178:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi179:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi180:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi181:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi182:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi183:
	.cfi_def_cfa_offset 64
.Lcfi184:
	.cfi_offset %rbx, -56
.Lcfi185:
	.cfi_offset %r12, -48
.Lcfi186:
	.cfi_offset %r13, -40
.Lcfi187:
	.cfi_offset %r14, -32
.Lcfi188:
	.cfi_offset %r15, -24
.Lcfi189:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movl	%r8d, %r13d
	movl	%ecx, %r12d
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movslq	%r15d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%rbx)
	testq	%rax, %rax
	jne	.LBB18_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.17, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB18_2:                               # %.preheader
	movl	%r15d, (%rsp)           # 4-byte Spill
	testl	%r15d, %r15d
	movl	%r13d, %ecx
	movl	%r12d, %edx
	movl	%r14d, %esi
	jle	.LBB18_5
# BB#3:                                 # %.lr.ph.preheader
	movl	(%rsp), %r14d           # 4-byte Reload
	xorl	%r15d, %r15d
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB18_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r12
	movq	(%r12), %rdi
	addq	%r15, %rdi
	movl	%ebp, %r13d
	movl	%edx, %ebx
	movl	%esi, %ebp
	movl	%r13d, %r8d
	callq	get_mem4Dint
	movl	%ebp, %esi
	movl	%ebx, %edx
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%r13d, %ebp
	movq	%r12, %rbx
	addq	$8, %r15
	decq	%r14
	jne	.LBB18_4
.LBB18_5:                               # %._crit_edge
	movl	(%rsp), %eax            # 4-byte Reload
	imull	%esi, %eax
	imull	%edx, %eax
	imull	%ecx, %eax
	imull	%ebp, %eax
	shll	$2, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	get_mem5Dint, .Lfunc_end18-get_mem5Dint
	.cfi_endproc

	.globl	free_mem2D
	.p2align	4, 0x90
	.type	free_mem2D,@function
free_mem2D:                             # @free_mem2D
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi190:
	.cfi_def_cfa_offset 16
.Lcfi191:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB19_5
# BB#1:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_3
# BB#2:
	callq	free
	jmp	.LBB19_4
.LBB19_5:
	movl	$.L.str.18, %edi
	movl	$100, %esi
	popq	%rbx
	jmp	error                   # TAILCALL
.LBB19_3:
	movl	$.L.str.18, %edi
	movl	$100, %esi
	callq	error
.LBB19_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end19:
	.size	free_mem2D, .Lfunc_end19-free_mem2D
	.cfi_endproc

	.globl	free_mem2Dint
	.p2align	4, 0x90
	.type	free_mem2Dint,@function
free_mem2Dint:                          # @free_mem2Dint
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi192:
	.cfi_def_cfa_offset 16
.Lcfi193:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB20_5
# BB#1:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB20_3
# BB#2:
	callq	free
	jmp	.LBB20_4
.LBB20_5:
	movl	$.L.str.19, %edi
	movl	$100, %esi
	popq	%rbx
	jmp	error                   # TAILCALL
.LBB20_3:
	movl	$.L.str.19, %edi
	movl	$100, %esi
	callq	error
.LBB20_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end20:
	.size	free_mem2Dint, .Lfunc_end20-free_mem2Dint
	.cfi_endproc

	.globl	free_mem2Dint64
	.p2align	4, 0x90
	.type	free_mem2Dint64,@function
free_mem2Dint64:                        # @free_mem2Dint64
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi194:
	.cfi_def_cfa_offset 16
.Lcfi195:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB21_5
# BB#1:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_3
# BB#2:
	callq	free
	jmp	.LBB21_4
.LBB21_5:
	movl	$.L.str.20, %edi
	movl	$100, %esi
	popq	%rbx
	jmp	error                   # TAILCALL
.LBB21_3:
	movl	$.L.str.20, %edi
	movl	$100, %esi
	callq	error
.LBB21_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end21:
	.size	free_mem2Dint64, .Lfunc_end21-free_mem2Dint64
	.cfi_endproc

	.globl	free_mem3D
	.p2align	4, 0x90
	.type	free_mem3D,@function
free_mem3D:                             # @free_mem3D
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi196:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi197:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi198:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi199:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi200:
	.cfi_def_cfa_offset 48
.Lcfi201:
	.cfi_offset %rbx, -40
.Lcfi202:
	.cfi_offset %r12, -32
.Lcfi203:
	.cfi_offset %r14, -24
.Lcfi204:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB22_11
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB22_10
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %r12d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB22_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.LBB22_8
# BB#4:                                 #   in Loop: Header=BB22_3 Depth=1
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB22_6
# BB#5:                                 #   in Loop: Header=BB22_3 Depth=1
	callq	free
	jmp	.LBB22_7
	.p2align	4, 0x90
.LBB22_8:                               #   in Loop: Header=BB22_3 Depth=1
	movl	$.L.str.18, %edi
	movl	$100, %esi
	callq	error
	jmp	.LBB22_9
	.p2align	4, 0x90
.LBB22_6:                               #   in Loop: Header=BB22_3 Depth=1
	movl	$.L.str.18, %edi
	movl	$100, %esi
	callq	error
.LBB22_7:                               #   in Loop: Header=BB22_3 Depth=1
	movq	%r15, %rdi
	callq	free
.LBB22_9:                               # %free_mem2D.exit
                                        #   in Loop: Header=BB22_3 Depth=1
	addq	$8, %rbx
	decq	%r12
	jne	.LBB22_3
.LBB22_10:                              # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB22_11:
	movl	$.L.str.21, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	error                   # TAILCALL
.Lfunc_end22:
	.size	free_mem3D, .Lfunc_end22-free_mem3D
	.cfi_endproc

	.globl	free_mem3Dint
	.p2align	4, 0x90
	.type	free_mem3Dint,@function
free_mem3Dint:                          # @free_mem3Dint
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi205:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi206:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi207:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi208:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi209:
	.cfi_def_cfa_offset 48
.Lcfi210:
	.cfi_offset %rbx, -40
.Lcfi211:
	.cfi_offset %r12, -32
.Lcfi212:
	.cfi_offset %r14, -24
.Lcfi213:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB23_11
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB23_10
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %r12d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB23_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.LBB23_8
# BB#4:                                 #   in Loop: Header=BB23_3 Depth=1
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB23_6
# BB#5:                                 #   in Loop: Header=BB23_3 Depth=1
	callq	free
	jmp	.LBB23_7
	.p2align	4, 0x90
.LBB23_8:                               #   in Loop: Header=BB23_3 Depth=1
	movl	$.L.str.19, %edi
	movl	$100, %esi
	callq	error
	jmp	.LBB23_9
	.p2align	4, 0x90
.LBB23_6:                               #   in Loop: Header=BB23_3 Depth=1
	movl	$.L.str.19, %edi
	movl	$100, %esi
	callq	error
.LBB23_7:                               #   in Loop: Header=BB23_3 Depth=1
	movq	%r15, %rdi
	callq	free
.LBB23_9:                               # %free_mem2Dint.exit
                                        #   in Loop: Header=BB23_3 Depth=1
	addq	$8, %rbx
	decq	%r12
	jne	.LBB23_3
.LBB23_10:                              # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB23_11:
	movl	$.L.str.22, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	error                   # TAILCALL
.Lfunc_end23:
	.size	free_mem3Dint, .Lfunc_end23-free_mem3Dint
	.cfi_endproc

	.globl	free_mem3Dint64
	.p2align	4, 0x90
	.type	free_mem3Dint64,@function
free_mem3Dint64:                        # @free_mem3Dint64
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi214:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi215:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi216:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi217:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi218:
	.cfi_def_cfa_offset 48
.Lcfi219:
	.cfi_offset %rbx, -40
.Lcfi220:
	.cfi_offset %r12, -32
.Lcfi221:
	.cfi_offset %r14, -24
.Lcfi222:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB24_11
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB24_10
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %r12d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB24_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.LBB24_8
# BB#4:                                 #   in Loop: Header=BB24_3 Depth=1
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB24_6
# BB#5:                                 #   in Loop: Header=BB24_3 Depth=1
	callq	free
	jmp	.LBB24_7
	.p2align	4, 0x90
.LBB24_8:                               #   in Loop: Header=BB24_3 Depth=1
	movl	$.L.str.20, %edi
	movl	$100, %esi
	callq	error
	jmp	.LBB24_9
	.p2align	4, 0x90
.LBB24_6:                               #   in Loop: Header=BB24_3 Depth=1
	movl	$.L.str.20, %edi
	movl	$100, %esi
	callq	error
.LBB24_7:                               #   in Loop: Header=BB24_3 Depth=1
	movq	%r15, %rdi
	callq	free
.LBB24_9:                               # %free_mem2Dint64.exit
                                        #   in Loop: Header=BB24_3 Depth=1
	addq	$8, %rbx
	decq	%r12
	jne	.LBB24_3
.LBB24_10:                              # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB24_11:
	movl	$.L.str.23, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	error                   # TAILCALL
.Lfunc_end24:
	.size	free_mem3Dint64, .Lfunc_end24-free_mem3Dint64
	.cfi_endproc

	.globl	free_mem4Dint
	.p2align	4, 0x90
	.type	free_mem4Dint,@function
free_mem4Dint:                          # @free_mem4Dint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi223:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi224:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi225:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi226:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi227:
	.cfi_def_cfa_offset 48
.Lcfi228:
	.cfi_offset %rbx, -40
.Lcfi229:
	.cfi_offset %r14, -32
.Lcfi230:
	.cfi_offset %r15, -24
.Lcfi231:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB25_5
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB25_4
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %ebx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB25_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movl	%r15d, %esi
	callq	free_mem3Dint
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB25_3
.LBB25_4:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB25_5:
	movl	$.L.str.24, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.Lfunc_end25:
	.size	free_mem4Dint, .Lfunc_end25-free_mem4Dint
	.cfi_endproc

	.globl	free_mem5Dint
	.p2align	4, 0x90
	.type	free_mem5Dint,@function
free_mem5Dint:                          # @free_mem5Dint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi232:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi233:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi234:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi235:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi236:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi237:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi238:
	.cfi_def_cfa_offset 80
.Lcfi239:
	.cfi_offset %rbx, -56
.Lcfi240:
	.cfi_offset %r12, -48
.Lcfi241:
	.cfi_offset %r13, -40
.Lcfi242:
	.cfi_offset %r14, -32
.Lcfi243:
	.cfi_offset %r15, -24
.Lcfi244:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	testq	%rdi, %rdi
	je	.LBB26_16
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB26_15
# BB#2:                                 # %.lr.ph
	testl	%edx, %edx
	movl	%esi, %r15d
	jle	.LBB26_3
# BB#6:                                 # %.lr.ph.split.us.preheader
	movl	%edx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB26_7:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_9 Depth 2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbx,8), %r12
	testq	%r12, %r12
	je	.LBB26_11
# BB#8:                                 # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB26_7 Depth=1
	movq	%r12, %r13
	movq	16(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB26_9:                               # %.lr.ph.i.us
                                        #   Parent Loop BB26_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rdi
	movl	%ebp, %esi
	callq	free_mem3Dint
	addq	$8, %r13
	decq	%r14
	jne	.LBB26_9
# BB#10:                                # %._crit_edge.i.loopexit.us
                                        #   in Loop: Header=BB26_7 Depth=1
	movq	%r12, %rdi
	callq	free
	jmp	.LBB26_12
	.p2align	4, 0x90
.LBB26_11:                              #   in Loop: Header=BB26_7 Depth=1
	movl	$.L.str.24, %edi
	movl	$100, %esi
	callq	error
.LBB26_12:                              # %free_mem4Dint.exit.us
                                        #   in Loop: Header=BB26_7 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jne	.LBB26_7
	jmp	.LBB26_15
.LBB26_16:
	movl	$.L.str.25, %edi
	movl	$100, %esi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.LBB26_3:
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB26_4:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_13
# BB#5:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB26_4 Depth=1
	callq	free
	jmp	.LBB26_14
	.p2align	4, 0x90
.LBB26_13:                              #   in Loop: Header=BB26_4 Depth=1
	movl	$.L.str.24, %edi
	movl	$100, %esi
	callq	error
.LBB26_14:                              # %free_mem4Dint.exit
                                        #   in Loop: Header=BB26_4 Depth=1
	addq	$8, %rbx
	decq	%r15
	jne	.LBB26_4
.LBB26_15:                              # %._crit_edge
	movq	8(%rsp), %rdi           # 8-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end26:
	.size	free_mem5Dint, .Lfunc_end26-free_mem5Dint
	.cfi_endproc

	.globl	get_mem2Dshort
	.p2align	4, 0x90
	.type	get_mem2Dshort,@function
get_mem2Dshort:                         # @get_mem2Dshort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi245:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi246:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi247:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi248:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi249:
	.cfi_def_cfa_offset 48
.Lcfi250:
	.cfi_offset %rbx, -48
.Lcfi251:
	.cfi_offset %r12, -40
.Lcfi252:
	.cfi_offset %r14, -32
.Lcfi253:
	.cfi_offset %r15, -24
.Lcfi254:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movslq	%ebp, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	movq	%r12, (%rbx)
	testq	%r12, %r12
	jne	.LBB27_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.27, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %r12
.LBB27_2:
	movl	%r15d, %r14d
	imull	%ebp, %r14d
	movslq	%r14d, %rdi
	movl	$2, %esi
	callq	calloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.LBB27_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.27, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB27_4:                               # %.preheader
	cmpl	$2, %ebp
	jl	.LBB27_13
# BB#5:                                 # %.lr.ph
	movslq	%r15d, %rax
	movl	%ebp, %r9d
	addl	$3, %ebp
	leaq	-2(%r9), %r8
	andq	$3, %rbp
	je	.LBB27_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	leaq	(%rax,%rax), %rdi
	.p2align	4, 0x90
.LBB27_8:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rcx
	addq	%rdi, %rcx
	movq	%rcx, 8(%rsi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB27_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %r8
	jae	.LBB27_11
	jmp	.LBB27_13
.LBB27_6:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB27_13
.LBB27_11:                              # %.lr.ph.new
	addq	%rax, %rax
	.p2align	4, 0x90
.LBB27_12:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	-8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 8(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	16(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 24(%rcx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %r9
	jne	.LBB27_12
.LBB27_13:                              # %._crit_edge
	addl	%r14d, %r14d
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	get_mem2Dshort, .Lfunc_end27-get_mem2Dshort
	.cfi_endproc

	.globl	get_mem3Dshort
	.p2align	4, 0x90
	.type	get_mem3Dshort,@function
get_mem3Dshort:                         # @get_mem3Dshort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi255:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi256:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi257:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi258:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi259:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi260:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi261:
	.cfi_def_cfa_offset 64
.Lcfi262:
	.cfi_offset %rbx, -56
.Lcfi263:
	.cfi_offset %r12, -48
.Lcfi264:
	.cfi_offset %r13, -40
.Lcfi265:
	.cfi_offset %r14, -32
.Lcfi266:
	.cfi_offset %r15, -24
.Lcfi267:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r12d
	movl	%esi, %r14d
	movq	%rdi, %r13
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r13)
	testq	%rax, %rax
	jne	.LBB28_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.28, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB28_2:                               # %.preheader
	testl	%r14d, %r14d
	jle	.LBB28_5
# BB#3:                                 # %.lr.ph.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB28_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	addq	%rbp, %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	callq	get_mem2Dshort
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB28_4
.LBB28_5:                               # %._crit_edge
	imull	%r12d, %r14d
	imull	%r15d, %r14d
	addl	%r14d, %r14d
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	get_mem3Dshort, .Lfunc_end28-get_mem3Dshort
	.cfi_endproc

	.globl	get_mem4Dshort
	.p2align	4, 0x90
	.type	get_mem4Dshort,@function
get_mem4Dshort:                         # @get_mem4Dshort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi268:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi269:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi270:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi271:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi272:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi273:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi274:
	.cfi_def_cfa_offset 96
.Lcfi275:
	.cfi_offset %rbx, -56
.Lcfi276:
	.cfi_offset %r12, -48
.Lcfi277:
	.cfi_offset %r13, -40
.Lcfi278:
	.cfi_offset %r14, -32
.Lcfi279:
	.cfi_offset %r15, -24
.Lcfi280:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movl	%ecx, %ebp
	movl	%edx, (%rsp)            # 4-byte Spill
	movl	%esi, %r14d
	movq	%rdi, %r15
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%rax, (%r15)
	testq	%rax, %rax
	jne	.LBB29_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.29, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB29_2:                               # %.preheader
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	testl	%r14d, %r14d
	jle	.LBB29_14
# BB#3:                                 # %.lr.ph
	movl	(%rsp), %eax            # 4-byte Reload
	movslq	%eax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	movl	4(%rsp), %ecx           # 4-byte Reload
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jle	.LBB29_4
# BB#8:                                 # %.lr.ph.split.us.preheader
	movl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB29_9:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_12 Depth 2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r14
	movl	$8, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	calloc
	movq	%rax, (%r14,%r12,8)
	testq	%rax, %rax
	jne	.LBB29_11
# BB#10:                                #   in Loop: Header=BB29_9 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.28, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB29_11:                              # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB29_9 Depth=1
	xorl	%r15d, %r15d
	movq	32(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB29_12:                              # %.lr.ph.i.us
                                        #   Parent Loop BB29_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r12,8), %rdi
	addq	%r15, %rdi
	movl	%ebp, %esi
	movl	%ebx, %edx
	callq	get_mem2Dshort
	addq	$8, %r15
	decq	%r13
	jne	.LBB29_12
# BB#13:                                # %get_mem3Dshort.exit.loopexit.us
                                        #   in Loop: Header=BB29_9 Depth=1
	incq	%r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB29_9
	jmp	.LBB29_14
.LBB29_4:                               # %.lr.ph.split.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB29_5:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r15
	movl	$8, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	calloc
	movq	%rax, (%r15,%r14,8)
	testq	%rax, %rax
	jne	.LBB29_7
# BB#6:                                 #   in Loop: Header=BB29_5 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.28, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB29_7:                               # %get_mem3Dshort.exit
                                        #   in Loop: Header=BB29_5 Depth=1
	incq	%r14
	cmpq	%r14, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB29_5
.LBB29_14:                              # %._crit_edge
	movl	4(%rsp), %eax           # 4-byte Reload
	imull	(%rsp), %eax            # 4-byte Folded Reload
	imull	%ebp, %eax
	imull	%ebx, %eax
	addl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end29:
	.size	get_mem4Dshort, .Lfunc_end29-get_mem4Dshort
	.cfi_endproc

	.globl	get_mem5Dshort
	.p2align	4, 0x90
	.type	get_mem5Dshort,@function
get_mem5Dshort:                         # @get_mem5Dshort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi281:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi282:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi283:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi284:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi285:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi286:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi287:
	.cfi_def_cfa_offset 64
.Lcfi288:
	.cfi_offset %rbx, -56
.Lcfi289:
	.cfi_offset %r12, -48
.Lcfi290:
	.cfi_offset %r13, -40
.Lcfi291:
	.cfi_offset %r14, -32
.Lcfi292:
	.cfi_offset %r15, -24
.Lcfi293:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movl	%r8d, %r13d
	movl	%ecx, %r12d
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movslq	%r15d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%rbx)
	testq	%rax, %rax
	jne	.LBB30_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.30, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB30_2:                               # %.preheader
	movl	%r15d, (%rsp)           # 4-byte Spill
	testl	%r15d, %r15d
	movl	%r13d, %ecx
	movl	%r12d, %edx
	movl	%r14d, %esi
	jle	.LBB30_5
# BB#3:                                 # %.lr.ph.preheader
	movl	(%rsp), %r14d           # 4-byte Reload
	xorl	%r15d, %r15d
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB30_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r12
	movq	(%r12), %rdi
	addq	%r15, %rdi
	movl	%ebp, %r13d
	movl	%edx, %ebx
	movl	%esi, %ebp
	movl	%r13d, %r8d
	callq	get_mem4Dshort
	movl	%ebp, %esi
	movl	%ebx, %edx
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%r13d, %ebp
	movq	%r12, %rbx
	addq	$8, %r15
	decq	%r14
	jne	.LBB30_4
.LBB30_5:                               # %._crit_edge
	movl	(%rsp), %eax            # 4-byte Reload
	imull	%esi, %eax
	imull	%edx, %eax
	imull	%ecx, %eax
	imull	%ebp, %eax
	addl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	get_mem5Dshort, .Lfunc_end30-get_mem5Dshort
	.cfi_endproc

	.globl	get_mem6Dshort
	.p2align	4, 0x90
	.type	get_mem6Dshort,@function
get_mem6Dshort:                         # @get_mem6Dshort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi294:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi295:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi296:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi297:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi298:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi299:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi300:
	.cfi_def_cfa_offset 128
.Lcfi301:
	.cfi_offset %rbx, -56
.Lcfi302:
	.cfi_offset %r12, -48
.Lcfi303:
	.cfi_offset %r13, -40
.Lcfi304:
	.cfi_offset %r14, -32
.Lcfi305:
	.cfi_offset %r15, -24
.Lcfi306:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movl	%r8d, 20(%rsp)          # 4-byte Spill
	movl	%ecx, %r13d
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movq	%rax, (%rbx)
	testq	%rax, %rax
	jne	.LBB31_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.31, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB31_2:                               # %.preheader
	movl	128(%rsp), %ebp
	movl	%r14d, 16(%rsp)         # 4-byte Spill
	testl	%r14d, %r14d
	jle	.LBB31_14
# BB#3:                                 # %.lr.ph
	movl	12(%rsp), %eax          # 4-byte Reload
	movslq	%eax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	movl	16(%rsp), %ecx          # 4-byte Reload
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	jle	.LBB31_4
# BB#8:                                 # %.lr.ph.split.us.preheader
	movl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movl	%r13d, 28(%rsp)         # 4-byte Spill
	movl	%r12d, 24(%rsp)         # 4-byte Spill
	movl	28(%rsp), %r14d         # 4-byte Reload
	.p2align	4, 0x90
.LBB31_9:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_12 Depth 2
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r13
	movl	$8, %esi
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	calloc
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movq	%rax, (%r13,%rbx,8)
	testq	%rax, %rax
	jne	.LBB31_11
# BB#10:                                #   in Loop: Header=BB31_9 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.30, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB31_11:                              # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB31_9 Depth=1
	xorl	%r12d, %r12d
	movq	56(%rsp), %r15          # 8-byte Reload
	movl	20(%rsp), %ebp          # 4-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB31_12:                              # %.lr.ph.i.us
                                        #   Parent Loop BB31_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13,%rbx,8), %rdi
	addq	%r12, %rdi
	movl	%r14d, %esi
	movl	%ebp, %edx
	movl	24(%rsp), %ecx          # 4-byte Reload
	movl	128(%rsp), %r8d
	callq	get_mem4Dshort
	addq	$8, %r12
	decq	%r15
	jne	.LBB31_12
# BB#13:                                # %get_mem5Dshort.exit.loopexit.us
                                        #   in Loop: Header=BB31_9 Depth=1
	incq	%rbx
	cmpq	32(%rsp), %rbx          # 8-byte Folded Reload
	movl	24(%rsp), %r12d         # 4-byte Reload
	movl	%r14d, %r13d
	movl	128(%rsp), %ebp
	jne	.LBB31_9
	jmp	.LBB31_14
.LBB31_4:                               # %.lr.ph.split.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB31_5:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r15
	movl	$8, %esi
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	calloc
	movq	%rax, (%r15,%r14,8)
	testq	%rax, %rax
	jne	.LBB31_7
# BB#6:                                 #   in Loop: Header=BB31_5 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.30, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB31_7:                               # %get_mem5Dshort.exit
                                        #   in Loop: Header=BB31_5 Depth=1
	incq	%r14
	cmpq	%r14, 32(%rsp)          # 8-byte Folded Reload
	jne	.LBB31_5
.LBB31_14:                              # %._crit_edge
	movl	16(%rsp), %eax          # 4-byte Reload
	imull	12(%rsp), %eax          # 4-byte Folded Reload
	imull	%r13d, %eax
	imull	20(%rsp), %eax          # 4-byte Folded Reload
	imull	%r12d, %eax
	imull	%ebp, %eax
	addl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	get_mem6Dshort, .Lfunc_end31-get_mem6Dshort
	.cfi_endproc

	.globl	free_mem2Dshort
	.p2align	4, 0x90
	.type	free_mem2Dshort,@function
free_mem2Dshort:                        # @free_mem2Dshort
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi307:
	.cfi_def_cfa_offset 16
.Lcfi308:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB32_5
# BB#1:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_3
# BB#2:
	callq	free
	jmp	.LBB32_4
.LBB32_5:
	movl	$.L.str.32, %edi
	movl	$100, %esi
	popq	%rbx
	jmp	error                   # TAILCALL
.LBB32_3:
	movl	$.L.str.32, %edi
	movl	$100, %esi
	callq	error
.LBB32_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end32:
	.size	free_mem2Dshort, .Lfunc_end32-free_mem2Dshort
	.cfi_endproc

	.globl	free_mem3Dshort
	.p2align	4, 0x90
	.type	free_mem3Dshort,@function
free_mem3Dshort:                        # @free_mem3Dshort
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi309:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi310:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi311:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi312:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi313:
	.cfi_def_cfa_offset 48
.Lcfi314:
	.cfi_offset %rbx, -40
.Lcfi315:
	.cfi_offset %r12, -32
.Lcfi316:
	.cfi_offset %r14, -24
.Lcfi317:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB33_11
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB33_10
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %r12d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB33_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.LBB33_8
# BB#4:                                 #   in Loop: Header=BB33_3 Depth=1
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB33_6
# BB#5:                                 #   in Loop: Header=BB33_3 Depth=1
	callq	free
	jmp	.LBB33_7
	.p2align	4, 0x90
.LBB33_8:                               #   in Loop: Header=BB33_3 Depth=1
	movl	$.L.str.32, %edi
	movl	$100, %esi
	callq	error
	jmp	.LBB33_9
	.p2align	4, 0x90
.LBB33_6:                               #   in Loop: Header=BB33_3 Depth=1
	movl	$.L.str.32, %edi
	movl	$100, %esi
	callq	error
.LBB33_7:                               #   in Loop: Header=BB33_3 Depth=1
	movq	%r15, %rdi
	callq	free
.LBB33_9:                               # %free_mem2Dshort.exit
                                        #   in Loop: Header=BB33_3 Depth=1
	addq	$8, %rbx
	decq	%r12
	jne	.LBB33_3
.LBB33_10:                              # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB33_11:
	movl	$.L.str.33, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	error                   # TAILCALL
.Lfunc_end33:
	.size	free_mem3Dshort, .Lfunc_end33-free_mem3Dshort
	.cfi_endproc

	.globl	free_mem4Dshort
	.p2align	4, 0x90
	.type	free_mem4Dshort,@function
free_mem4Dshort:                        # @free_mem4Dshort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi318:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi319:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi320:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi321:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi322:
	.cfi_def_cfa_offset 48
.Lcfi323:
	.cfi_offset %rbx, -40
.Lcfi324:
	.cfi_offset %r14, -32
.Lcfi325:
	.cfi_offset %r15, -24
.Lcfi326:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB34_5
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB34_4
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %ebx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB34_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movl	%r15d, %esi
	callq	free_mem3Dshort
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB34_3
.LBB34_4:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB34_5:
	movl	$.L.str.34, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.Lfunc_end34:
	.size	free_mem4Dshort, .Lfunc_end34-free_mem4Dshort
	.cfi_endproc

	.globl	free_mem5Dshort
	.p2align	4, 0x90
	.type	free_mem5Dshort,@function
free_mem5Dshort:                        # @free_mem5Dshort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi327:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi328:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi329:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi330:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi331:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi332:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi333:
	.cfi_def_cfa_offset 80
.Lcfi334:
	.cfi_offset %rbx, -56
.Lcfi335:
	.cfi_offset %r12, -48
.Lcfi336:
	.cfi_offset %r13, -40
.Lcfi337:
	.cfi_offset %r14, -32
.Lcfi338:
	.cfi_offset %r15, -24
.Lcfi339:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	testq	%rdi, %rdi
	je	.LBB35_16
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB35_15
# BB#2:                                 # %.lr.ph
	testl	%edx, %edx
	movl	%esi, %r15d
	jle	.LBB35_3
# BB#6:                                 # %.lr.ph.split.us.preheader
	movl	%edx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB35_7:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_9 Depth 2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbx,8), %r12
	testq	%r12, %r12
	je	.LBB35_11
# BB#8:                                 # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB35_7 Depth=1
	movq	%r12, %r13
	movq	16(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB35_9:                               # %.lr.ph.i.us
                                        #   Parent Loop BB35_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rdi
	movl	%ebp, %esi
	callq	free_mem3Dshort
	addq	$8, %r13
	decq	%r14
	jne	.LBB35_9
# BB#10:                                # %._crit_edge.i.loopexit.us
                                        #   in Loop: Header=BB35_7 Depth=1
	movq	%r12, %rdi
	callq	free
	jmp	.LBB35_12
	.p2align	4, 0x90
.LBB35_11:                              #   in Loop: Header=BB35_7 Depth=1
	movl	$.L.str.34, %edi
	movl	$100, %esi
	callq	error
.LBB35_12:                              # %free_mem4Dshort.exit.us
                                        #   in Loop: Header=BB35_7 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jne	.LBB35_7
	jmp	.LBB35_15
.LBB35_16:
	movl	$.L.str.35, %edi
	movl	$100, %esi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.LBB35_3:
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB35_4:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_13
# BB#5:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB35_4 Depth=1
	callq	free
	jmp	.LBB35_14
	.p2align	4, 0x90
.LBB35_13:                              #   in Loop: Header=BB35_4 Depth=1
	movl	$.L.str.34, %edi
	movl	$100, %esi
	callq	error
.LBB35_14:                              # %free_mem4Dshort.exit
                                        #   in Loop: Header=BB35_4 Depth=1
	addq	$8, %rbx
	decq	%r15
	jne	.LBB35_4
.LBB35_15:                              # %._crit_edge
	movq	8(%rsp), %rdi           # 8-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end35:
	.size	free_mem5Dshort, .Lfunc_end35-free_mem5Dshort
	.cfi_endproc

	.globl	free_mem6Dshort
	.p2align	4, 0x90
	.type	free_mem6Dshort,@function
free_mem6Dshort:                        # @free_mem6Dshort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi340:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi341:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi342:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi343:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi344:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi345:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi346:
	.cfi_def_cfa_offset 64
.Lcfi347:
	.cfi_offset %rbx, -56
.Lcfi348:
	.cfi_offset %r12, -48
.Lcfi349:
	.cfi_offset %r13, -40
.Lcfi350:
	.cfi_offset %r14, -32
.Lcfi351:
	.cfi_offset %r15, -24
.Lcfi352:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movl	%ecx, %r12d
	movl	%edx, %r13d
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB36_5
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB36_4
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %ebp
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB36_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movl	%r13d, %esi
	movl	%r12d, %edx
	movl	%r14d, %ecx
	callq	free_mem5Dshort
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB36_3
.LBB36_4:                               # %._crit_edge
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB36_5:
	movl	$.L.str.36, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.Lfunc_end36:
	.size	free_mem6Dshort, .Lfunc_end36-free_mem6Dshort
	.cfi_endproc

	.globl	get_mem2Ddouble
	.p2align	4, 0x90
	.type	get_mem2Ddouble,@function
get_mem2Ddouble:                        # @get_mem2Ddouble
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi353:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi354:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi355:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi356:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi357:
	.cfi_def_cfa_offset 48
.Lcfi358:
	.cfi_offset %rbx, -48
.Lcfi359:
	.cfi_offset %r12, -40
.Lcfi360:
	.cfi_offset %r14, -32
.Lcfi361:
	.cfi_offset %r15, -24
.Lcfi362:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movslq	%ebp, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	movq	%r12, (%rbx)
	testq	%r12, %r12
	jne	.LBB37_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.37, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %r12
.LBB37_2:
	movl	%r15d, %r14d
	imull	%ebp, %r14d
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.LBB37_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.37, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB37_4:                               # %.preheader
	cmpl	$2, %ebp
	jl	.LBB37_13
# BB#5:                                 # %.lr.ph
	movslq	%r15d, %rax
	movl	%ebp, %r9d
	addl	$3, %ebp
	leaq	-2(%r9), %r8
	andq	$3, %rbp
	je	.LBB37_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	leaq	(,%rax,8), %rdi
	.p2align	4, 0x90
.LBB37_8:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rcx
	addq	%rdi, %rcx
	movq	%rcx, 8(%rsi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB37_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %r8
	jae	.LBB37_11
	jmp	.LBB37_13
.LBB37_6:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB37_13
.LBB37_11:                              # %.lr.ph.new
	shlq	$3, %rax
	.p2align	4, 0x90
.LBB37_12:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	-8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 8(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	16(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 24(%rcx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %r9
	jne	.LBB37_12
.LBB37_13:                              # %._crit_edge
	shll	$3, %r14d
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end37:
	.size	get_mem2Ddouble, .Lfunc_end37-get_mem2Ddouble
	.cfi_endproc

	.globl	get_mem2Ddb_offset
	.p2align	4, 0x90
	.type	get_mem2Ddb_offset,@function
get_mem2Ddb_offset:                     # @get_mem2Ddb_offset
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi363:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi364:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi365:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi366:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi367:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi368:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi369:
	.cfi_def_cfa_offset 64
.Lcfi370:
	.cfi_offset %rbx, -56
.Lcfi371:
	.cfi_offset %r12, -48
.Lcfi372:
	.cfi_offset %r13, -40
.Lcfi373:
	.cfi_offset %r14, -32
.Lcfi374:
	.cfi_offset %r15, -24
.Lcfi375:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movslq	%ebp, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r13
	movq	%r13, (%rbx)
	testq	%r13, %r13
	jne	.LBB38_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.38, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %r13
.LBB38_2:
	movl	%r15d, %r14d
	imull	%ebp, %r14d
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r13)
	testq	%rax, %rax
	jne	.LBB38_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.38, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB38_4:
	movq	(%rbx), %rax
	movslq	%r12d, %rcx
	shlq	$3, %rcx
	addq	%rcx, (%rax)
	cmpl	$2, %ebp
	jl	.LBB38_13
# BB#5:                                 # %.lr.ph
	movslq	%r15d, %rax
	movl	%ebp, %r9d
	addl	$3, %ebp
	leaq	-2(%r9), %r8
	andq	$3, %rbp
	je	.LBB38_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	leaq	(,%rax,8), %rdi
	.p2align	4, 0x90
.LBB38_8:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rcx
	addq	%rdi, %rcx
	movq	%rcx, 8(%rsi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB38_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %r8
	jae	.LBB38_11
	jmp	.LBB38_13
.LBB38_6:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB38_13
.LBB38_11:                              # %.lr.ph.new
	shlq	$3, %rax
	.p2align	4, 0x90
.LBB38_12:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	-8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 8(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	16(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 24(%rcx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %r9
	jne	.LBB38_12
.LBB38_13:                              # %._crit_edge
	shll	$3, %r14d
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end38:
	.size	get_mem2Ddb_offset, .Lfunc_end38-get_mem2Ddb_offset
	.cfi_endproc

	.globl	get_mem3Ddb_offset
	.p2align	4, 0x90
	.type	get_mem3Ddb_offset,@function
get_mem3Ddb_offset:                     # @get_mem3Ddb_offset
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi376:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi377:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi378:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi379:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi380:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi381:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi382:
	.cfi_def_cfa_offset 80
.Lcfi383:
	.cfi_offset %rbx, -56
.Lcfi384:
	.cfi_offset %r12, -48
.Lcfi385:
	.cfi_offset %r13, -40
.Lcfi386:
	.cfi_offset %r14, -32
.Lcfi387:
	.cfi_offset %r15, -24
.Lcfi388:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbp, (%rbx)
	testq	%rbp, %rbp
	jne	.LBB39_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.39, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %rbp
.LBB39_2:
	movl	4(%rsp), %eax           # 4-byte Reload
	imull	%r14d, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%rbp)
	testq	%rax, %rax
	jne	.LBB39_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.39, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB39_4:
	movq	(%rbx), %rax
	movslq	%r15d, %rcx
	shlq	$3, %rcx
	addq	%rcx, (%rax)
	cmpl	$2, %r14d
	jl	.LBB39_13
# BB#5:                                 # %.lr.ph39
	movslq	4(%rsp), %rax           # 4-byte Folded Reload
	movl	%r14d, %r9d
	leal	3(%r14), %edi
	leaq	-2(%r9), %r8
	andq	$3, %rdi
	je	.LBB39_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	leaq	(,%rax,8), %rbp
	.p2align	4, 0x90
.LBB39_8:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rcx
	addq	%rbp, %rcx
	movq	%rcx, 8(%rsi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB39_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %r8
	jae	.LBB39_11
	jmp	.LBB39_13
.LBB39_6:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB39_13
.LBB39_11:
	shlq	$3, %rax
	.p2align	4, 0x90
.LBB39_12:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	-8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 8(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	16(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 24(%rcx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %r9
	jne	.LBB39_12
.LBB39_13:                              # %.preheader
	testl	%r14d, %r14d
	jle	.LBB39_21
# BB#14:                                # %.lr.ph35
	movl	4(%rsp), %eax           # 4-byte Reload
	subl	%r15d, %eax
	negl	%r15d
	cmpl	%r15d, %eax
	jle	.LBB39_21
# BB#15:                                # %.lr.ph35.split.us.preheader
	movslq	8(%rsp), %rbp           # 4-byte Folded Reload
	movslq	%r15d, %r12
	movl	%r14d, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	shlq	$3, %r12
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB39_16:                              # %.lr.ph35.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_17 Depth 2
	movl	4(%rsp), %r13d          # 4-byte Reload
	movq	%r12, %r15
	.p2align	4, 0x90
.LBB39_17:                              #   Parent Loop BB39_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$8, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	(%rbx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	%rax, (%rcx,%r15)
	testq	%rax, %rax
	jne	.LBB39_19
# BB#18:                                #   in Loop: Header=BB39_17 Depth=2
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.39, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB39_19:                              #   in Loop: Header=BB39_17 Depth=2
	addq	$8, %r15
	decl	%r13d
	jne	.LBB39_17
# BB#20:                                # %._crit_edge.us
                                        #   in Loop: Header=BB39_16 Depth=1
	incq	%r14
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	jne	.LBB39_16
.LBB39_21:                              # %._crit_edge36
	movl	8(%rsp), %eax           # 4-byte Reload
	imull	12(%rsp), %eax          # 4-byte Folded Reload
	shll	$3, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end39:
	.size	get_mem3Ddb_offset, .Lfunc_end39-get_mem3Ddb_offset
	.cfi_endproc

	.globl	get_mem3Dint_offset
	.p2align	4, 0x90
	.type	get_mem3Dint_offset,@function
get_mem3Dint_offset:                    # @get_mem3Dint_offset
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi389:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi390:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi391:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi392:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi393:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi394:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi395:
	.cfi_def_cfa_offset 80
.Lcfi396:
	.cfi_offset %rbx, -56
.Lcfi397:
	.cfi_offset %r12, -48
.Lcfi398:
	.cfi_offset %r13, -40
.Lcfi399:
	.cfi_offset %r14, -32
.Lcfi400:
	.cfi_offset %r15, -24
.Lcfi401:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbp, (%rbx)
	testq	%rbp, %rbp
	jne	.LBB40_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.40, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %rbp
.LBB40_2:
	movl	4(%rsp), %eax           # 4-byte Reload
	imull	%r14d, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%rbp)
	testq	%rax, %rax
	jne	.LBB40_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.40, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB40_4:
	movq	(%rbx), %rax
	movslq	%r15d, %rcx
	shlq	$3, %rcx
	addq	%rcx, (%rax)
	cmpl	$2, %r14d
	jl	.LBB40_13
# BB#5:                                 # %.lr.ph39
	movslq	4(%rsp), %rax           # 4-byte Folded Reload
	movl	%r14d, %r9d
	leal	3(%r14), %edi
	leaq	-2(%r9), %r8
	andq	$3, %rdi
	je	.LBB40_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	leaq	(,%rax,8), %rbp
	.p2align	4, 0x90
.LBB40_8:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rcx
	addq	%rbp, %rcx
	movq	%rcx, 8(%rsi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB40_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %r8
	jae	.LBB40_11
	jmp	.LBB40_13
.LBB40_6:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB40_13
.LBB40_11:
	shlq	$3, %rax
	.p2align	4, 0x90
.LBB40_12:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	-8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 8(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	16(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 24(%rcx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %r9
	jne	.LBB40_12
.LBB40_13:                              # %.preheader
	testl	%r14d, %r14d
	jle	.LBB40_21
# BB#14:                                # %.lr.ph35
	movl	4(%rsp), %eax           # 4-byte Reload
	subl	%r15d, %eax
	negl	%r15d
	cmpl	%r15d, %eax
	jle	.LBB40_21
# BB#15:                                # %.lr.ph35.split.us.preheader
	movslq	8(%rsp), %rbp           # 4-byte Folded Reload
	movslq	%r15d, %r12
	movl	%r14d, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	shlq	$3, %r12
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB40_16:                              # %.lr.ph35.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_17 Depth 2
	movl	4(%rsp), %r13d          # 4-byte Reload
	movq	%r12, %r15
	.p2align	4, 0x90
.LBB40_17:                              #   Parent Loop BB40_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$4, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	(%rbx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	%rax, (%rcx,%r15)
	testq	%rax, %rax
	jne	.LBB40_19
# BB#18:                                #   in Loop: Header=BB40_17 Depth=2
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.40, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB40_19:                              #   in Loop: Header=BB40_17 Depth=2
	addq	$8, %r15
	decl	%r13d
	jne	.LBB40_17
# BB#20:                                # %._crit_edge.us
                                        #   in Loop: Header=BB40_16 Depth=1
	incq	%r14
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	jne	.LBB40_16
.LBB40_21:                              # %._crit_edge36
	movl	8(%rsp), %eax           # 4-byte Reload
	imull	12(%rsp), %eax          # 4-byte Folded Reload
	shll	$2, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end40:
	.size	get_mem3Dint_offset, .Lfunc_end40-get_mem3Dint_offset
	.cfi_endproc

	.globl	free_mem2Ddouble
	.p2align	4, 0x90
	.type	free_mem2Ddouble,@function
free_mem2Ddouble:                       # @free_mem2Ddouble
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi402:
	.cfi_def_cfa_offset 16
.Lcfi403:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB41_5
# BB#1:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB41_3
# BB#2:
	callq	free
	jmp	.LBB41_4
.LBB41_5:
	movl	$.L.str.41, %edi
	movl	$100, %esi
	popq	%rbx
	jmp	error                   # TAILCALL
.LBB41_3:
	movl	$.L.str.41, %edi
	movl	$100, %esi
	callq	error
.LBB41_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end41:
	.size	free_mem2Ddouble, .Lfunc_end41-free_mem2Ddouble
	.cfi_endproc

	.globl	free_mem2Ddb_offset
	.p2align	4, 0x90
	.type	free_mem2Ddb_offset,@function
free_mem2Ddb_offset:                    # @free_mem2Ddb_offset
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi404:
	.cfi_def_cfa_offset 16
.Lcfi405:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB42_5
# BB#1:
	movq	(%rbx), %rdi
	movslq	%esi, %rax
	shlq	$3, %rax
	subq	%rax, %rdi
	movq	%rdi, (%rbx)
	je	.LBB42_3
# BB#2:
	callq	free
	jmp	.LBB42_4
.LBB42_5:
	movl	$.L.str.42, %edi
	movl	$100, %esi
	popq	%rbx
	jmp	error                   # TAILCALL
.LBB42_3:
	movl	$.L.str.42, %edi
	movl	$100, %esi
	callq	error
.LBB42_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end42:
	.size	free_mem2Ddb_offset, .Lfunc_end42-free_mem2Ddb_offset
	.cfi_endproc

	.globl	free_mem3Ddb_offset
	.p2align	4, 0x90
	.type	free_mem3Ddb_offset,@function
free_mem3Ddb_offset:                    # @free_mem3Ddb_offset
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi406:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi407:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi408:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi409:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi410:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi411:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi412:
	.cfi_def_cfa_offset 64
.Lcfi413:
	.cfi_offset %rbx, -56
.Lcfi414:
	.cfi_offset %r12, -48
.Lcfi415:
	.cfi_offset %r13, -40
.Lcfi416:
	.cfi_offset %r14, -32
.Lcfi417:
	.cfi_offset %r15, -24
.Lcfi418:
	.cfi_offset %rbp, -16
	movl	%ecx, (%rsp)            # 4-byte Spill
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB43_14
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB43_10
# BB#2:                                 # %.lr.ph24
	movl	(%rsp), %edx            # 4-byte Reload
	movl	%edx, %eax
	negl	%eax
	movl	4(%rsp), %ecx           # 4-byte Reload
	subl	%edx, %ecx
	cmpl	%eax, %ecx
	jle	.LBB43_10
# BB#3:                                 # %.lr.ph24.split.us.preheader
	movslq	%eax, %r14
	movl	%esi, %r12d
	shlq	$3, %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB43_4:                               # %.lr.ph24.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB43_5 Depth 2
	movl	4(%rsp), %r15d          # 4-byte Reload
	movq	%r14, %r13
	.p2align	4, 0x90
.LBB43_5:                               #   Parent Loop BB43_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rbp,8), %rax
	movq	(%rax,%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB43_7
# BB#6:                                 #   in Loop: Header=BB43_5 Depth=2
	callq	free
	jmp	.LBB43_8
	.p2align	4, 0x90
.LBB43_7:                               #   in Loop: Header=BB43_5 Depth=2
	movl	$.L.str.43, %edi
	movl	$100, %esi
	callq	error
.LBB43_8:                               #   in Loop: Header=BB43_5 Depth=2
	addq	$8, %r13
	decl	%r15d
	jne	.LBB43_5
# BB#9:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB43_4 Depth=1
	incq	%rbp
	cmpq	%r12, %rbp
	jne	.LBB43_4
.LBB43_10:                              # %._crit_edge25
	movq	(%rbx), %rdi
	movslq	(%rsp), %rax            # 4-byte Folded Reload
	shlq	$3, %rax
	subq	%rax, %rdi
	movq	%rdi, (%rbx)
	je	.LBB43_12
# BB#11:
	callq	free
	jmp	.LBB43_13
.LBB43_14:
	movl	$.L.str.43, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.LBB43_12:
	movl	$.L.str.43, %edi
	movl	$100, %esi
	callq	error
.LBB43_13:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end43:
	.size	free_mem3Ddb_offset, .Lfunc_end43-free_mem3Ddb_offset
	.cfi_endproc

	.globl	free_mem3Dint_offset
	.p2align	4, 0x90
	.type	free_mem3Dint_offset,@function
free_mem3Dint_offset:                   # @free_mem3Dint_offset
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi419:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi420:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi421:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi422:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi423:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi424:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi425:
	.cfi_def_cfa_offset 64
.Lcfi426:
	.cfi_offset %rbx, -56
.Lcfi427:
	.cfi_offset %r12, -48
.Lcfi428:
	.cfi_offset %r13, -40
.Lcfi429:
	.cfi_offset %r14, -32
.Lcfi430:
	.cfi_offset %r15, -24
.Lcfi431:
	.cfi_offset %rbp, -16
	movl	%ecx, (%rsp)            # 4-byte Spill
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB44_14
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB44_10
# BB#2:                                 # %.lr.ph24
	movl	(%rsp), %edx            # 4-byte Reload
	movl	%edx, %eax
	negl	%eax
	movl	4(%rsp), %ecx           # 4-byte Reload
	subl	%edx, %ecx
	cmpl	%eax, %ecx
	jle	.LBB44_10
# BB#3:                                 # %.lr.ph24.split.us.preheader
	movslq	%eax, %r14
	movl	%esi, %r12d
	shlq	$3, %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB44_4:                               # %.lr.ph24.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB44_5 Depth 2
	movl	4(%rsp), %r15d          # 4-byte Reload
	movq	%r14, %r13
	.p2align	4, 0x90
.LBB44_5:                               #   Parent Loop BB44_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rbp,8), %rax
	movq	(%rax,%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB44_7
# BB#6:                                 #   in Loop: Header=BB44_5 Depth=2
	callq	free
	jmp	.LBB44_8
	.p2align	4, 0x90
.LBB44_7:                               #   in Loop: Header=BB44_5 Depth=2
	movl	$.L.str.44, %edi
	movl	$100, %esi
	callq	error
.LBB44_8:                               #   in Loop: Header=BB44_5 Depth=2
	addq	$8, %r13
	decl	%r15d
	jne	.LBB44_5
# BB#9:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB44_4 Depth=1
	incq	%rbp
	cmpq	%r12, %rbp
	jne	.LBB44_4
.LBB44_10:                              # %._crit_edge25
	movq	(%rbx), %rdi
	movslq	(%rsp), %rax            # 4-byte Folded Reload
	shlq	$3, %rax
	subq	%rax, %rdi
	movq	%rdi, (%rbx)
	je	.LBB44_12
# BB#11:
	callq	free
	jmp	.LBB44_13
.LBB44_14:
	movl	$.L.str.44, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.LBB44_12:
	movl	$.L.str.44, %edi
	movl	$100, %esi
	callq	error
.LBB44_13:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end44:
	.size	free_mem3Dint_offset, .Lfunc_end44-free_mem3Dint_offset
	.cfi_endproc

	.globl	get_mem2Dint_offset
	.p2align	4, 0x90
	.type	get_mem2Dint_offset,@function
get_mem2Dint_offset:                    # @get_mem2Dint_offset
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi432:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi433:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi434:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi435:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi436:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi437:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi438:
	.cfi_def_cfa_offset 64
.Lcfi439:
	.cfi_offset %rbx, -56
.Lcfi440:
	.cfi_offset %r12, -48
.Lcfi441:
	.cfi_offset %r13, -40
.Lcfi442:
	.cfi_offset %r14, -32
.Lcfi443:
	.cfi_offset %r15, -24
.Lcfi444:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movslq	%ebp, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r13
	movq	%r13, (%rbx)
	testq	%r13, %r13
	jne	.LBB45_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.11, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %r13
.LBB45_2:
	movl	%r15d, %r14d
	imull	%ebp, %r14d
	movslq	%r14d, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, (%r13)
	testq	%rax, %rax
	jne	.LBB45_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.26, %edx
	movl	$.L.str.11, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB45_4:
	movq	(%rbx), %rax
	movslq	%r12d, %rcx
	shlq	$2, %rcx
	addq	%rcx, (%rax)
	cmpl	$2, %ebp
	jl	.LBB45_13
# BB#5:                                 # %.lr.ph
	movslq	%r15d, %rax
	movl	%ebp, %r9d
	addl	$3, %ebp
	leaq	-2(%r9), %r8
	andq	$3, %rbp
	je	.LBB45_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	leaq	(,%rax,4), %rdi
	.p2align	4, 0x90
.LBB45_8:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rcx
	addq	%rdi, %rcx
	movq	%rcx, 8(%rsi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB45_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %r8
	jae	.LBB45_11
	jmp	.LBB45_13
.LBB45_6:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB45_13
.LBB45_11:                              # %.lr.ph.new
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB45_12:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	-8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 8(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	16(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 24(%rcx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %r9
	jne	.LBB45_12
.LBB45_13:                              # %._crit_edge
	shll	$2, %r14d
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end45:
	.size	get_mem2Dint_offset, .Lfunc_end45-get_mem2Dint_offset
	.cfi_endproc

	.globl	free_mem2Dint_offset
	.p2align	4, 0x90
	.type	free_mem2Dint_offset,@function
free_mem2Dint_offset:                   # @free_mem2Dint_offset
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi445:
	.cfi_def_cfa_offset 16
.Lcfi446:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB46_5
# BB#1:
	movq	(%rbx), %rdi
	subl	%esi, (%rdi)
	testq	%rdi, %rdi
	je	.LBB46_3
# BB#2:
	callq	free
	jmp	.LBB46_4
.LBB46_5:
	movl	$.L.str.45, %edi
	movl	$100, %esi
	popq	%rbx
	jmp	error                   # TAILCALL
.LBB46_3:
	movl	$.L.str.45, %edi
	movl	$100, %esi
	callq	error
.LBB46_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end46:
	.size	free_mem2Dint_offset, .Lfunc_end46-free_mem2Dint_offset
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"init_top_bot_planes: imgTopField"
	.size	.L.str, 33

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"init_top_bot_planes: imgBotField"
	.size	.L.str.1, 33

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"get_mem2Dpel: array2D"
	.size	.L.str.2, 22

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"get_mem3Dpel: array3D"
	.size	.L.str.3, 22

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"get_mem4Dpel: array4D"
	.size	.L.str.4, 22

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"get_mem5Dpel: array5D"
	.size	.L.str.5, 22

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"free_mem2Dpel: trying to free unused memory"
	.size	.L.str.6, 44

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"free_mem3Dpel: trying to free unused memory"
	.size	.L.str.7, 44

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"free_mem4Dpel: trying to free unused memory"
	.size	.L.str.8, 44

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"free_mem5Dpel: trying to free unused memory"
	.size	.L.str.9, 44

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"get_mem2D: array2D"
	.size	.L.str.10, 19

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"get_mem2Dint: array2D"
	.size	.L.str.11, 22

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"get_mem2Dint64: array2D"
	.size	.L.str.12, 24

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"get_mem3D: array3D"
	.size	.L.str.13, 19

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"get_mem3Dint: array3D"
	.size	.L.str.14, 22

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"get_mem3Dint64: array3D"
	.size	.L.str.15, 24

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"get_mem4Dint: array4D"
	.size	.L.str.16, 22

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"get_mem5Dint: array5D"
	.size	.L.str.17, 22

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"free_mem2D: trying to free unused memory"
	.size	.L.str.18, 41

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"free_mem2Dint: trying to free unused memory"
	.size	.L.str.19, 44

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"free_mem2Dint64: trying to free unused memory"
	.size	.L.str.20, 46

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"free_mem3D: trying to free unused memory"
	.size	.L.str.21, 41

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"free_mem3Dint: trying to free unused memory"
	.size	.L.str.22, 44

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"free_mem3Dint64: trying to free unused memory"
	.size	.L.str.23, 46

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"free_mem4Dint: trying to free unused memory"
	.size	.L.str.24, 44

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"free_mem5Dint: trying to free unused memory"
	.size	.L.str.25, 44

	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Could not allocate memory: %s"
	.size	.L.str.26, 30

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"get_mem2Dshort: array2D"
	.size	.L.str.27, 24

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"get_mem3Dshort: array3D"
	.size	.L.str.28, 24

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"get_mem4Dshort: array4D"
	.size	.L.str.29, 24

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"get_mem5Dshort: array5D"
	.size	.L.str.30, 24

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"get_mem6Dshort: array6D"
	.size	.L.str.31, 24

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"free_mem2Dshort: trying to free unused memory"
	.size	.L.str.32, 46

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"free_mem3Dshort: trying to free unused memory"
	.size	.L.str.33, 46

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"free_mem4Dshort: trying to free unused memory"
	.size	.L.str.34, 46

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"free_mem5Dshort: trying to free unused memory"
	.size	.L.str.35, 46

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"free_mem6Dshort: trying to free unused memory"
	.size	.L.str.36, 46

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"get_mem2Ddouble: array2D"
	.size	.L.str.37, 25

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"get_mem2Ddb_offset: array2D"
	.size	.L.str.38, 28

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"get_mem3Ddb_offset: array3D"
	.size	.L.str.39, 28

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"get_mem3Dint_offset: array3D"
	.size	.L.str.40, 29

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"free_mem2Ddouble: trying to free unused memory"
	.size	.L.str.41, 47

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"free_mem2Ddb_offset: trying to free unused memory"
	.size	.L.str.42, 50

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"free_mem3Ddb_offset: trying to free unused memory"
	.size	.L.str.43, 50

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"free_mem3Dint_offset: trying to free unused memory"
	.size	.L.str.44, 51

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"free_mem2Dint_offset: trying to free unused memory"
	.size	.L.str.45, 51

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
