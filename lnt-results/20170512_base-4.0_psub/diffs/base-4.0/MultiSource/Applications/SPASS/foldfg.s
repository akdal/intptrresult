	.text
	.file	"foldfg.bc"
	.globl	fol_Init
	.p2align	4, 0x90
	.type	fol_Init,@function
fol_Init:                               # @fol_Init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	testl	%edi, %edi
	je	.LBB0_2
# BB#1:
	movl	$.L.str, %edi
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	callq	symbol_CreateJunctor
	movl	%eax, fol_ALL(%rip)
	movl	$.L.str.1, %edi
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	callq	symbol_CreateJunctor
	movl	%eax, fol_EXIST(%rip)
	movl	$.L.str.2, %edi
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	callq	symbol_CreateJunctor
	movl	%eax, fol_AND(%rip)
	movl	$.L.str.3, %edi
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	callq	symbol_CreateJunctor
	movl	%eax, fol_OR(%rip)
	movl	$.L.str.4, %edi
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	callq	symbol_CreateJunctor
	movl	%eax, fol_NOT(%rip)
	movl	$.L.str.5, %edi
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	callq	symbol_CreateJunctor
	movl	%eax, fol_IMPLIES(%rip)
	movl	$.L.str.6, %edi
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	callq	symbol_CreateJunctor
	movl	%eax, fol_IMPLIED(%rip)
	movl	$.L.str.7, %edi
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	callq	symbol_CreateJunctor
	movl	%eax, fol_EQUIV(%rip)
	movl	$.L.str.8, %edi
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	callq	symbol_CreateJunctor
	movl	%eax, fol_VARLIST(%rip)
	movl	$.L.str.9, %edi
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	callq	symbol_CreatePredicate
	movl	%eax, fol_EQUALITY(%rip)
	movl	$.L.str.10, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	callq	symbol_CreatePredicate
	movl	%eax, fol_TRUE(%rip)
	movl	$.L.str.11, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	callq	symbol_CreatePredicate
	movl	%eax, fol_FALSE(%rip)
	movl	fol_ALL(%rip), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	fol_EXIST(%rip), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movslq	fol_AND(%rip), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movslq	fol_OR(%rip), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movslq	fol_NOT(%rip), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movslq	fol_IMPLIES(%rip), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movslq	fol_IMPLIED(%rip), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movslq	fol_EQUIV(%rip), %r14
	movslq	fol_VARLIST(%rip), %r12
	movslq	fol_EQUALITY(%rip), %r13
	movslq	fol_TRUE(%rip), %r15
	movslq	%eax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	$0, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r15, 8(%rbp)
	movq	%rbx, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r13, 8(%rbx)
	movq	%rbp, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r12, 8(%rbp)
	movq	%rbx, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r14, 8(%rbx)
	movq	%rbp, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rbp)
	movq	%rbx, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rbx)
	movq	%rbp, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rbp)
	movq	%rbx, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rbx)
	movq	%rbp, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movl	8(%rsp), %ecx           # 4-byte Reload
	movq	%rax, %r14
	movl	12(%rsp), %eax          # 4-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, 8(%r14)
	movq	%rbx, (%r14)
	jmp	.LBB0_3
.LBB0_2:
	xorl	%r14d, %r14d
	movl	$.L.str.9, %edi
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	callq	symbol_CreatePredicate
	movl	%eax, fol_EQUALITY(%rip)
	movl	$.L.str.4, %edi
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	callq	symbol_CreateJunctor
	movl	%eax, fol_NOT(%rip)
	movl	fol_EQUALITY(%rip), %ecx
.LBB0_3:
	movslq	%eax, %r15
	movslq	%ecx, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	%r14, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, fol_SYMBOLS(%rip)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	fol_Init, .Lfunc_end0-fol_Init
	.cfi_endproc

	.globl	fol_IsStringPredefined
	.p2align	4, 0x90
	.type	fol_IsStringPredefined,@function
fol_IsStringPredefined:                 # @fol_IsStringPredefined
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	fol_SYMBOLS(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB1_5
# BB#1:                                 # %.lr.ph
	movl	symbol_TYPESTATBITS(%rip), %r14d
	movq	symbol_SIGNATURE(%rip), %r12
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %ebp
	movl	%ebp, %eax
	negl	%eax
	movl	%r14d, %ecx
	sarl	%cl, %eax
	cltq
	movq	(%r12,%rax,8), %rax
	movq	(%rax), %rsi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_6
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_2
.LBB1_5:
	xorl	%ebp, %ebp
.LBB1_6:                                # %._crit_edge
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	fol_IsStringPredefined, .Lfunc_end1-fol_IsStringPredefined
	.cfi_endproc

	.globl	fol_CreateQuantifier
	.p2align	4, 0x90
	.type	fol_CreateQuantifier,@function
fol_CreateQuantifier:                   # @fol_CreateQuantifier
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%edi, %ebp
	movl	fol_VARLIST(%rip), %edi
	callq	term_Create
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movl	%ebp, %edi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	term_Create             # TAILCALL
.Lfunc_end2:
	.size	fol_CreateQuantifier, .Lfunc_end2-fol_CreateQuantifier
	.cfi_endproc

	.globl	fol_CreateQuantifierAddFather
	.p2align	4, 0x90
	.type	fol_CreateQuantifierAddFather,@function
fol_CreateQuantifierAddFather:          # @fol_CreateQuantifierAddFather
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%edi, %ebp
	movl	fol_VARLIST(%rip), %edi
	callq	term_CreateAddFather
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movl	%ebp, %edi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	term_CreateAddFather    # TAILCALL
.Lfunc_end3:
	.size	fol_CreateQuantifierAddFather, .Lfunc_end3-fol_CreateQuantifierAddFather
	.cfi_endproc

	.globl	fol_ComplementaryTerm
	.p2align	4, 0x90
	.type	fol_ComplementaryTerm,@function
fol_ComplementaryTerm:                  # @fol_ComplementaryTerm
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movl	fol_NOT(%rip), %ebx
	cmpl	%ebx, (%rdi)
	jne	.LBB4_2
# BB#1:
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	term_Copy               # TAILCALL
.LBB4_2:
	callq	term_Copy
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	term_Create             # TAILCALL
.Lfunc_end4:
	.size	fol_ComplementaryTerm, .Lfunc_end4-fol_ComplementaryTerm
	.cfi_endproc

	.globl	fol_GetNonFOLPredicates
	.p2align	4, 0x90
	.type	fol_GetNonFOLPredicates,@function
fol_GetNonFOLPredicates:                # @fol_GetNonFOLPredicates
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 16
	callq	symbol_GetAllPredicates
	movl	$fol_IsPredefinedPred, %esi
	movq	%rax, %rdi
	popq	%rax
	jmp	list_DeleteElementIf    # TAILCALL
.Lfunc_end5:
	.size	fol_GetNonFOLPredicates, .Lfunc_end5-fol_GetNonFOLPredicates
	.cfi_endproc

	.p2align	4, 0x90
	.type	fol_IsPredefinedPred,@function
fol_IsPredefinedPred:                   # @fol_IsPredefinedPred
	.cfi_startproc
# BB#0:
	cmpl	%edi, fol_EQUALITY(%rip)
	movb	$1, %al
	je	.LBB6_3
# BB#1:
	cmpl	%edi, fol_TRUE(%rip)
	je	.LBB6_3
# BB#2:
	cmpl	%edi, fol_FALSE(%rip)
	sete	%al
.LBB6_3:
	movzbl	%al, %eax
	retq
.Lfunc_end6:
	.size	fol_IsPredefinedPred, .Lfunc_end6-fol_IsPredefinedPred
	.cfi_endproc

	.globl	fol_GetAssignments
	.p2align	4, 0x90
	.type	fol_GetAssignments,@function
fol_GetAssignments:                     # @fol_GetAssignments
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 32
.Lcfi44:
	.cfi_offset %rbx, -24
.Lcfi45:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	testl	%eax, %eax
	jns	.LBB7_9
# BB#1:                                 # %term_IsAtom.exit
	movl	%eax, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	jne	.LBB7_9
# BB#2:
	cmpl	fol_EQUALITY(%rip), %eax
	jne	.LBB7_21
# BB#3:
	movq	16(%rbx), %rax
	movq	8(%rax), %rcx
	movl	(%rcx), %esi
	testl	%esi, %esi
	jle	.LBB7_6
# BB#4:
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	term_ContainsVariable
	testl	%eax, %eax
	je	.LBB7_20
# BB#5:                                 # %._crit_edge.i
	movq	16(%rbx), %rax
.LBB7_6:
	movq	(%rax), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %esi
	testl	%esi, %esi
	jle	.LBB7_21
# BB#7:                                 # %fol_IsAssignment.exit
	movq	8(%rax), %rdi
	callq	term_ContainsVariable
	testl	%eax, %eax
	jne	.LBB7_21
.LBB7_20:                               # %fol_IsAssignment.exit.thread18
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	jmp	.LBB7_22
.LBB7_9:                                # %term_IsAtom.exit.thread
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB7_21
# BB#10:                                # %.lr.ph.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_11:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_14 Depth 2
	movq	8(%rbx), %rdi
	callq	fol_GetAssignments
	testq	%rax, %rax
	je	.LBB7_16
# BB#12:                                #   in Loop: Header=BB7_11 Depth=1
	testq	%r14, %r14
	je	.LBB7_17
# BB#13:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB7_11 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB7_14:                               # %.preheader.i
                                        #   Parent Loop BB7_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB7_14
# BB#15:                                #   in Loop: Header=BB7_11 Depth=1
	movq	%r14, (%rcx)
	jmp	.LBB7_17
	.p2align	4, 0x90
.LBB7_16:                               #   in Loop: Header=BB7_11 Depth=1
	movq	%r14, %rax
.LBB7_17:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB7_11 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r14
	jne	.LBB7_11
	jmp	.LBB7_22
.LBB7_21:
	xorl	%eax, %eax
.LBB7_22:                               # %fol_IsAssignment.exit.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	fol_GetAssignments, .Lfunc_end7-fol_GetAssignments
	.cfi_endproc

	.globl	fol_NormalizeVars
	.p2align	4, 0x90
	.type	fol_NormalizeVars,@function
fol_NormalizeVars:                      # @fol_NormalizeVars
	.cfi_startproc
# BB#0:
	movl	$0, symbol_STANDARDVARCOUNTER(%rip)
	movl	term_MARK(%rip), %eax
	cmpl	$-1, %eax
	jne	.LBB8_4
# BB#1:                                 # %.preheader.i.preheader
	movq	$-48000, %rax           # imm = 0xFFFF4480
	jmp	.LBB8_2
	.p2align	4, 0x90
.LBB8_5:                                # %.preheader.i.1
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	$0, term_BIND+48016(%rax)
	movq	$0, term_BIND+48032(%rax)
	movq	$0, term_BIND+48048(%rax)
	movq	$0, term_BIND+48064(%rax)
	movq	$0, term_BIND+48080(%rax)
	movq	$0, term_BIND+48096(%rax)
	movq	$0, term_BIND+48112(%rax)
	subq	$-128, %rax
.LBB8_2:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, term_BIND+48000(%rax)
	testq	%rax, %rax
	jne	.LBB8_5
# BB#3:
	movl	$1, term_MARK(%rip)
	movl	$1, %eax
.LBB8_4:                                # %term_NewMark.exit
	incl	%eax
	movl	%eax, term_MARK(%rip)
	jmp	fol_NormalizeVarsIntern # TAILCALL
.Lfunc_end8:
	.size	fol_NormalizeVars, .Lfunc_end8-fol_NormalizeVars
	.cfi_endproc

	.p2align	4, 0x90
	.type	fol_NormalizeVarsIntern,@function
fol_NormalizeVarsIntern:                # @fol_NormalizeVarsIntern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 64
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movslq	(%r14), %rax
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB9_18
# BB#1:
	cmpl	%eax, fol_ALL(%rip)
	je	.LBB9_3
# BB#2:
	cmpl	%eax, fol_EXIST(%rip)
	je	.LBB9_3
	.p2align	4, 0x90
.LBB9_17:                               # %.lr.ph68
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	fol_NormalizeVarsIntern
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_17
	jmp	.LBB9_20
.LBB9_18:
	testl	%eax, %eax
	jle	.LBB9_20
# BB#19:
	shlq	$4, %rax
	movl	term_BIND+8(%rax), %eax
	movl	%eax, (%r14)
	jmp	.LBB9_20
.LBB9_3:
	movq	8(%rbx), %rax
	movq	16(%rax), %rbp
	xorl	%r15d, %r15d
	testq	%rbp, %rbp
	je	.LBB9_12
	.p2align	4, 0x90
.LBB9_4:                                # %.lr.ph65
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_8 Depth 2
	movq	8(%rbp), %rax
	movslq	(%rax), %r12
	shlq	$4, %r12
	leaq	term_BIND+8(%r12), %r13
	movq	term_BIND+8(%r12), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	testq	%r15, %r15
	je	.LBB9_5
# BB#6:                                 #   in Loop: Header=BB9_4 Depth=1
	testq	%rax, %rax
	je	.LBB9_10
# BB#7:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB9_8:                                # %.preheader.i
                                        #   Parent Loop BB9_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB9_8
# BB#9:                                 #   in Loop: Header=BB9_4 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB9_10
	.p2align	4, 0x90
.LBB9_5:                                #   in Loop: Header=BB9_4 Depth=1
	movq	%rax, %r15
.LBB9_10:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB9_4 Depth=1
	movl	term_MARK(%rip), %eax
	decl	%eax
	movslq	symbol_STANDARDVARCOUNTER(%rip), %rcx
	incq	%rcx
	movl	%ecx, symbol_STANDARDVARCOUNTER(%rip)
	movq	%rax, term_BIND(%r12)
	movq	%rcx, (%r13)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB9_4
# BB#11:                                # %._crit_edge66.loopexit
	movq	16(%r14), %rbx
.LBB9_12:                               # %._crit_edge66
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	callq	fol_NormalizeVarsIntern
	movq	16(%r14), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB9_15
# BB#13:                                # %.lr.ph.preheader
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB9_14:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rdx
	movslq	(%rdx), %rsi
	shlq	$4, %rsi
	movl	term_BIND+8(%rsi), %edi
	movl	%edi, (%rdx)
	movl	term_MARK(%rip), %edx
	decl	%edx
	movq	8(%rcx), %rdi
	movq	%rdx, term_BIND(%rsi)
	movq	%rdi, term_BIND+8(%rsi)
	movq	(%rcx), %rcx
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB9_14
.LBB9_15:                               # %._crit_edge
	testq	%r15, %r15
	je	.LBB9_20
	.p2align	4, 0x90
.LBB9_16:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB9_16
.LBB9_20:                               # %list_Delete.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	fol_NormalizeVarsIntern, .Lfunc_end9-fol_NormalizeVarsIntern
	.cfi_endproc

	.globl	fol_NormalizeVarsStartingAt
	.p2align	4, 0x90
	.type	fol_NormalizeVarsStartingAt,@function
fol_NormalizeVarsStartingAt:            # @fol_NormalizeVarsStartingAt
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 16
.Lcfi60:
	.cfi_offset %rbx, -16
	movl	symbol_STANDARDVARCOUNTER(%rip), %ebx
	movl	%esi, symbol_STANDARDVARCOUNTER(%rip)
	movl	term_MARK(%rip), %eax
	cmpl	$-1, %eax
	jne	.LBB10_4
# BB#1:                                 # %.preheader.i.preheader
	movq	$-48000, %rax           # imm = 0xFFFF4480
	jmp	.LBB10_2
	.p2align	4, 0x90
.LBB10_5:                               # %.preheader.i.1
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	$0, term_BIND+48016(%rax)
	movq	$0, term_BIND+48032(%rax)
	movq	$0, term_BIND+48048(%rax)
	movq	$0, term_BIND+48064(%rax)
	movq	$0, term_BIND+48080(%rax)
	movq	$0, term_BIND+48096(%rax)
	movq	$0, term_BIND+48112(%rax)
	subq	$-128, %rax
.LBB10_2:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, term_BIND+48000(%rax)
	testq	%rax, %rax
	jne	.LBB10_5
# BB#3:
	movl	$1, term_MARK(%rip)
	movl	$1, %eax
.LBB10_4:                               # %term_NewMark.exit
	incl	%eax
	movl	%eax, term_MARK(%rip)
	callq	fol_NormalizeVarsIntern
	movl	%ebx, symbol_STANDARDVARCOUNTER(%rip)
	popq	%rbx
	retq
.Lfunc_end10:
	.size	fol_NormalizeVarsStartingAt, .Lfunc_end10-fol_NormalizeVarsStartingAt
	.cfi_endproc

	.globl	fol_RemoveImplied
	.p2align	4, 0x90
	.type	fol_RemoveImplied,@function
fol_RemoveImplied:                      # @fol_RemoveImplied
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi63:
	.cfi_def_cfa_offset 32
.Lcfi64:
	.cfi_offset %rbx, -24
.Lcfi65:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	fol_NOT(%rip), %eax
	movl	symbol_TYPEMASK(%rip), %r8d
	movl	fol_ALL(%rip), %edx
	movl	fol_EXIST(%rip), %esi
	jmp	.LBB11_1
	.p2align	4, 0x90
.LBB11_8:                               #   in Loop: Header=BB11_1 Depth=1
	movq	16(%r14), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %r14
.LBB11_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jns	.LBB11_3
# BB#2:                                 # %symbol_IsPredicate.exit6.i
                                        #   in Loop: Header=BB11_1 Depth=1
	movl	%ecx, %edi
	negl	%edi
	andl	%r8d, %edi
	cmpl	$2, %edi
	je	.LBB11_14
.LBB11_3:                               # %symbol_IsPredicate.exit6.thread.i
                                        #   in Loop: Header=BB11_1 Depth=1
	cmpl	%eax, %ecx
	jne	.LBB11_6
# BB#4:                                 #   in Loop: Header=BB11_1 Depth=1
	movq	16(%r14), %rdi
	movq	8(%rdi), %rdi
	movl	(%rdi), %ebx
	testl	%ebx, %ebx
	jns	.LBB11_6
# BB#5:                                 # %fol_IsLiteral.exit
                                        #   in Loop: Header=BB11_1 Depth=1
	negl	%ebx
	andl	%r8d, %ebx
	cmpl	$2, %ebx
	je	.LBB11_14
	.p2align	4, 0x90
.LBB11_6:                               # %fol_IsLiteral.exit.thread
                                        #   in Loop: Header=BB11_1 Depth=1
	cmpl	%ecx, %edx
	je	.LBB11_8
# BB#7:                                 # %fol_IsLiteral.exit.thread
                                        #   in Loop: Header=BB11_1 Depth=1
	cmpl	%ecx, %esi
	je	.LBB11_8
# BB#9:
	cmpl	fol_IMPLIED(%rip), %ecx
	jne	.LBB11_10
# BB#11:
	movl	fol_IMPLIES(%rip), %eax
	movl	%eax, (%r14)
	movq	16(%r14), %rdi
	callq	list_NReverse
	movq	%rax, %rbx
	movq	%rbx, 16(%r14)
	testq	%rbx, %rbx
	jne	.LBB11_12
	jmp	.LBB11_14
.LBB11_10:                              # %._crit_edge
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB11_14
	.p2align	4, 0x90
.LBB11_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	fol_RemoveImplied
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB11_12
.LBB11_14:                              # %fol_IsLiteral.exit.thread17
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	fol_RemoveImplied, .Lfunc_end11-fol_RemoveImplied
	.cfi_endproc

	.globl	fol_VarOccursFreely
	.p2align	4, 0x90
	.type	fol_VarOccursFreely,@function
fol_VarOccursFreely:                    # @fol_VarOccursFreely
	.cfi_startproc
# BB#0:
	movl	stack_POINTER(%rip), %r10d
	xorl	%r8d, %r8d
	movl	%r10d, %edx
	jmp	.LBB12_1
	.p2align	4, 0x90
.LBB12_15:                              #   in Loop: Header=BB12_14 Depth=2
	movl	%ecx, stack_POINTER(%rip)
	cmpl	%ecx, %r10d
	movl	%ecx, %edx
	jne	.LBB12_14
	jmp	.LBB12_19
	.p2align	4, 0x90
.LBB12_16:                              # %.critedge
                                        #   in Loop: Header=BB12_1 Depth=1
	cmpl	%edx, %r10d
	je	.LBB12_19
# BB#17:                                #   in Loop: Header=BB12_1 Depth=1
	leal	-1(%rdx), %eax
	movq	stack_STACK(,%rax,8), %rcx
	movq	(%rcx), %r9
	movq	8(%rcx), %rsi
	movq	%r9, stack_STACK(,%rax,8)
.LBB12_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_6 Depth 2
                                        #     Child Loop BB12_14 Depth 2
	movl	(%rsi), %eax
	movq	16(%rsi), %r9
	testq	%r9, %r9
	je	.LBB12_11
# BB#2:                                 #   in Loop: Header=BB12_1 Depth=1
	cmpl	%eax, fol_ALL(%rip)
	je	.LBB12_4
# BB#3:                                 #   in Loop: Header=BB12_1 Depth=1
	cmpl	%eax, fol_EXIST(%rip)
	je	.LBB12_4
# BB#10:                                #   in Loop: Header=BB12_1 Depth=1
	movl	%edx, %eax
	leal	1(%rdx), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%r9, stack_STACK(,%rax,8)
	movl	%ecx, %edx
	jmp	.LBB12_13
	.p2align	4, 0x90
.LBB12_11:                              #   in Loop: Header=BB12_1 Depth=1
	testl	%eax, %eax
	jle	.LBB12_13
# BB#12:                                #   in Loop: Header=BB12_1 Depth=1
	cmpl	(%rdi), %eax
	jne	.LBB12_13
	jmp	.LBB12_18
	.p2align	4, 0x90
.LBB12_4:                               #   in Loop: Header=BB12_1 Depth=1
	movq	8(%r9), %rax
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB12_9
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB12_1 Depth=1
	movl	(%rdi), %r11d
	movl	$1, %eax
	.p2align	4, 0x90
.LBB12_6:                               #   Parent Loop BB12_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsi), %rcx
	cmpl	%r11d, (%rcx)
	cmovel	%r8d, %eax
	testl	%eax, %eax
	je	.LBB12_8
# BB#7:                                 #   in Loop: Header=BB12_6 Depth=2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB12_6
.LBB12_8:                               # %._crit_edge
                                        #   in Loop: Header=BB12_1 Depth=1
	testl	%eax, %eax
	je	.LBB12_13
.LBB12_9:                               # %.critedge52
                                        #   in Loop: Header=BB12_1 Depth=1
	movq	(%r9), %rax
	movl	%edx, %ecx
	leal	1(%rdx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
.LBB12_13:                              # %.preheader
                                        #   in Loop: Header=BB12_1 Depth=1
	xorl	%eax, %eax
	cmpl	%r10d, %edx
	je	.LBB12_19
	.p2align	4, 0x90
.LBB12_14:                              # %.lr.ph50
                                        #   Parent Loop BB12_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdx), %ecx
	cmpq	$0, stack_STACK(,%rcx,8)
	je	.LBB12_15
	jmp	.LBB12_16
.LBB12_18:
	movl	%r10d, stack_POINTER(%rip)
	movl	$1, %eax
.LBB12_19:                              # %.loopexit
	retq
.Lfunc_end12:
	.size	fol_VarOccursFreely, .Lfunc_end12-fol_VarOccursFreely
	.cfi_endproc

	.globl	fol_FreeVariables
	.p2align	4, 0x90
	.type	fol_FreeVariables,@function
fol_FreeVariables:                      # @fol_FreeVariables
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi72:
	.cfi_def_cfa_offset 64
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	stack_POINTER(%rip), %ebp
	movl	term_MARK(%rip), %eax
	cmpl	$-1, %eax
	je	.LBB13_1
# BB#4:                                 # %term_ActMark.exit65
	leal	1(%rax), %ecx
	movl	%ecx, term_MARK(%rip)
	cmpl	$-1, %ecx
	jne	.LBB13_9
# BB#5:                                 # %.preheader.i.preheader
	movq	$-48000, %rcx           # imm = 0xFFFF4480
	jmp	.LBB13_6
	.p2align	4, 0x90
.LBB13_36:                              # %.preheader.i.1
                                        #   in Loop: Header=BB13_6 Depth=1
	movq	$0, term_BIND+48016(%rcx)
	movq	$0, term_BIND+48032(%rcx)
	movq	$0, term_BIND+48048(%rcx)
	movq	$0, term_BIND+48064(%rcx)
	movq	$0, term_BIND+48080(%rcx)
	movq	$0, term_BIND+48096(%rcx)
	movq	$0, term_BIND+48112(%rcx)
	subq	$-128, %rcx
.LBB13_6:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, term_BIND+48000(%rcx)
	testq	%rcx, %rcx
	jne	.LBB13_36
# BB#7:
	movl	$1, %ecx
	jmp	.LBB13_8
.LBB13_1:                               # %.preheader.i64.preheader
	movq	$-48000, %rax           # imm = 0xFFFF4480
	jmp	.LBB13_2
	.p2align	4, 0x90
.LBB13_35:                              # %.preheader.i64.1
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	$0, term_BIND+48016(%rax)
	movq	$0, term_BIND+48032(%rax)
	movq	$0, term_BIND+48048(%rax)
	movq	$0, term_BIND+48064(%rax)
	movq	$0, term_BIND+48080(%rax)
	movq	$0, term_BIND+48096(%rax)
	movq	$0, term_BIND+48112(%rax)
	subq	$-128, %rax
.LBB13_2:                               # %.preheader.i64
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, term_BIND+48000(%rax)
	testq	%rax, %rax
	jne	.LBB13_35
# BB#3:                                 # %term_ActMark.exit65.thread
	movl	$1, term_MARK(%rip)
	movl	$1, %eax
	movl	$2, %ecx
.LBB13_8:                               # %term_ActMark.exit.sink.split
	movl	%ecx, term_MARK(%rip)
.LBB13_9:                               # %term_ActMark.exit
	leal	1(%rcx), %edx
	movl	%edx, term_MARK(%rip)
	movl	%ecx, %ebx
	movl	%eax, %r12d
	xorl	%r14d, %r14d
	movl	%ebp, %ecx
	jmp	.LBB13_10
	.p2align	4, 0x90
.LBB13_31:                              #   in Loop: Header=BB13_30 Depth=2
	movl	%eax, stack_POINTER(%rip)
	cmpl	%eax, %ebp
	movl	%eax, %ecx
	jne	.LBB13_30
	jmp	.LBB13_32
	.p2align	4, 0x90
.LBB13_33:                              # %.critedge
                                        #   in Loop: Header=BB13_10 Depth=1
	cmpl	%ecx, %ebp
	je	.LBB13_32
# BB#34:                                #   in Loop: Header=BB13_10 Depth=1
	leal	-1(%rcx), %eax
	movq	stack_STACK(,%rax,8), %rdx
	movq	(%rdx), %rsi
	movq	8(%rdx), %r15
	movq	%rsi, stack_STACK(,%rax,8)
.LBB13_10:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_20 Depth 2
                                        #     Child Loop BB13_14 Depth 2
                                        #     Child Loop BB13_30 Depth 2
	movslq	(%r15), %rdx
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.LBB13_25
# BB#11:                                #   in Loop: Header=BB13_10 Depth=1
	cmpl	%edx, fol_ALL(%rip)
	je	.LBB13_13
# BB#12:                                #   in Loop: Header=BB13_10 Depth=1
	cmpl	%edx, fol_EXIST(%rip)
	je	.LBB13_13
# BB#19:                                #   in Loop: Header=BB13_10 Depth=1
	cmpl	fol_VARLIST(%rip), %edx
	jne	.LBB13_24
	.p2align	4, 0x90
.LBB13_20:                              # %.lr.ph
                                        #   Parent Loop BB13_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rdx
	movslq	(%rdx), %rdx
	shlq	$4, %rdx
	cmpl	%ebx, term_BIND(%rdx)
	jae	.LBB13_22
# BB#21:                                #   in Loop: Header=BB13_20 Depth=2
	leaq	term_BIND(%rdx), %rdx
	movq	$0, (%rdx)
.LBB13_22:                              #   in Loop: Header=BB13_20 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB13_20
# BB#23:                                # %._crit_edge
                                        #   in Loop: Header=BB13_10 Depth=1
	leal	-1(%rcx), %eax
	movq	stack_STACK(,%rax,8), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, stack_STACK(,%rax,8)
	cmpl	%ebp, %ecx
	jne	.LBB13_30
	jmp	.LBB13_32
	.p2align	4, 0x90
.LBB13_25:                              #   in Loop: Header=BB13_10 Depth=1
	testl	%edx, %edx
	jle	.LBB13_29
# BB#26:                                #   in Loop: Header=BB13_10 Depth=1
	shlq	$4, %rdx
	movl	term_BIND(%rdx), %eax
	cmpl	%ebx, %eax
	jae	.LBB13_29
# BB#27:                                #   in Loop: Header=BB13_10 Depth=1
	cmpl	%r12d, %eax
	jae	.LBB13_29
# BB#28:                                #   in Loop: Header=BB13_10 Depth=1
	leaq	term_BIND(%rdx), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r14, (%rax)
	movq	%rbx, (%r13)
	movl	stack_POINTER(%rip), %ecx
	movq	%rax, %r14
	cmpl	%ebp, %ecx
	jne	.LBB13_30
	jmp	.LBB13_32
	.p2align	4, 0x90
.LBB13_13:                              #   in Loop: Header=BB13_10 Depth=1
	movq	8(%rax), %rdx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB13_18
	.p2align	4, 0x90
.LBB13_14:                              # %.lr.ph84
                                        #   Parent Loop BB13_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdx), %rax
	movslq	(%rax), %rax
	shlq	$4, %rax
	cmpl	%ebx, term_BIND(%rax)
	jae	.LBB13_16
# BB#15:                                #   in Loop: Header=BB13_14 Depth=2
	leaq	term_BIND(%rax), %rax
	movq	%r12, (%rax)
.LBB13_16:                              #   in Loop: Header=BB13_14 Depth=2
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB13_14
# BB#17:                                # %._crit_edge85.loopexit
                                        #   in Loop: Header=BB13_10 Depth=1
	movq	16(%r15), %rax
.LBB13_18:                              # %._crit_edge85
                                        #   in Loop: Header=BB13_10 Depth=1
	leal	1(%rcx), %edx
	movl	%edx, stack_POINTER(%rip)
	movl	%ecx, %esi
	movq	%rax, stack_STACK(,%rsi,8)
	movq	16(%r15), %rax
	movq	(%rax), %rax
	addl	$2, %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rdx,8)
	cmpl	%ebp, %ecx
	jne	.LBB13_30
	jmp	.LBB13_32
.LBB13_24:                              #   in Loop: Header=BB13_10 Depth=1
	movl	%ecx, %edx
	leal	1(%rcx), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rdx,8)
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	.p2align	4, 0x90
.LBB13_29:                              # %.preheader
                                        #   in Loop: Header=BB13_10 Depth=1
	cmpl	%ebp, %ecx
	je	.LBB13_32
	.p2align	4, 0x90
.LBB13_30:                              # %.lr.ph86
                                        #   Parent Loop BB13_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rcx), %eax
	cmpq	$0, stack_STACK(,%rax,8)
	je	.LBB13_31
	jmp	.LBB13_33
.LBB13_32:                              # %.critedge.thread
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	fol_FreeVariables, .Lfunc_end13-fol_FreeVariables
	.cfi_endproc

	.globl	fol_BoundVariables
	.p2align	4, 0x90
	.type	fol_BoundVariables,@function
fol_BoundVariables:                     # @fol_BoundVariables
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 32
.Lcfi82:
	.cfi_offset %rbx, -32
.Lcfi83:
	.cfi_offset %r14, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	stack_POINTER(%rip), %ebp
	xorl	%r14d, %r14d
	movl	%ebp, %eax
	jmp	.LBB14_1
	.p2align	4, 0x90
.LBB14_17:                              #   in Loop: Header=BB14_1 Depth=1
	leal	-1(%rax), %ecx
	movq	stack_STACK(,%rcx,8), %rdx
	movq	(%rdx), %rsi
	movq	8(%rdx), %rbx
	movq	%rsi, stack_STACK(,%rcx,8)
.LBB14_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_7 Depth 2
                                        #     Child Loop BB14_13 Depth 2
	movl	(%rbx), %edx
	cmpl	%edx, fol_ALL(%rip)
	movq	16(%rbx), %rcx
	je	.LBB14_3
# BB#2:                                 #   in Loop: Header=BB14_1 Depth=1
	cmpl	%edx, fol_EXIST(%rip)
	je	.LBB14_3
# BB#10:                                #   in Loop: Header=BB14_1 Depth=1
	testq	%rcx, %rcx
	je	.LBB14_12
# BB#11:                                #   in Loop: Header=BB14_1 Depth=1
	movl	%eax, %edx
	leal	1(%rax), %eax
	movl	%eax, stack_POINTER(%rip)
	movq	%rcx, stack_STACK(,%rdx,8)
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	%ebp, %eax
	jne	.LBB14_13
	jmp	.LBB14_15
	.p2align	4, 0x90
.LBB14_3:                               #   in Loop: Header=BB14_1 Depth=1
	movq	8(%rcx), %rax
	movq	16(%rax), %rdi
	callq	list_Copy
	testq	%r14, %r14
	je	.LBB14_4
# BB#5:                                 #   in Loop: Header=BB14_1 Depth=1
	testq	%rax, %rax
	je	.LBB14_9
# BB#6:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB14_1 Depth=1
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB14_7:                               # %.preheader.i
                                        #   Parent Loop BB14_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB14_7
# BB#8:                                 #   in Loop: Header=BB14_1 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB14_9
	.p2align	4, 0x90
.LBB14_4:                               #   in Loop: Header=BB14_1 Depth=1
	movq	%rax, %r14
.LBB14_9:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB14_1 Depth=1
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movl	stack_POINTER(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, stack_POINTER(%rip)
	movq	%rcx, stack_STACK(,%rdx,8)
.LBB14_12:                              # %.preheader
                                        #   in Loop: Header=BB14_1 Depth=1
	cmpl	%ebp, %eax
	je	.LBB14_15
	.p2align	4, 0x90
.LBB14_13:                              # %.lr.ph
                                        #   Parent Loop BB14_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rax), %ecx
	cmpq	$0, stack_STACK(,%rcx,8)
	jne	.LBB14_16
# BB#14:                                #   in Loop: Header=BB14_13 Depth=2
	movl	%ecx, stack_POINTER(%rip)
	cmpl	%ecx, %ebp
	movl	%ecx, %eax
	jne	.LBB14_13
	jmp	.LBB14_15
	.p2align	4, 0x90
.LBB14_16:                              # %.critedge
                                        #   in Loop: Header=BB14_1 Depth=1
	cmpl	%eax, %ebp
	jne	.LBB14_17
.LBB14_15:                              # %.critedge.thread
	movl	$term_Equal, %esi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	list_DeleteDuplicates   # TAILCALL
.Lfunc_end14:
	.size	fol_BoundVariables, .Lfunc_end14-fol_BoundVariables
	.cfi_endproc

	.globl	fol_Free
	.p2align	4, 0x90
	.type	fol_Free,@function
fol_Free:                               # @fol_Free
	.cfi_startproc
# BB#0:
	movq	fol_SYMBOLS(%rip), %rax
	testq	%rax, %rax
	je	.LBB15_2
	.p2align	4, 0x90
.LBB15_1:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB15_1
.LBB15_2:                               # %list_Delete.exit
	retq
.Lfunc_end15:
	.size	fol_Free, .Lfunc_end15-fol_Free
	.cfi_endproc

	.globl	fol_FormulaIsClause
	.p2align	4, 0x90
	.type	fol_FormulaIsClause,@function
fol_FormulaIsClause:                    # @fol_FormulaIsClause
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	cmpl	fol_ALL(%rip), %ecx
	jne	.LBB16_2
# BB#1:
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movl	(%rdi), %ecx
.LBB16_2:
	xorl	%eax, %eax
	cmpl	fol_OR(%rip), %ecx
	jne	.LBB16_12
# BB#3:
	movq	16(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB16_11
# BB#4:                                 # %.lr.ph
	movl	fol_NOT(%rip), %r9d
	movl	symbol_TYPEMASK(%rip), %r8d
	.p2align	4, 0x90
.LBB16_5:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdi
	movl	(%rdi), %edx
	testl	%edx, %edx
	jns	.LBB16_7
# BB#6:                                 # %symbol_IsPredicate.exit6.i
                                        #   in Loop: Header=BB16_5 Depth=1
	movl	%edx, %esi
	negl	%esi
	andl	%r8d, %esi
	cmpl	$2, %esi
	je	.LBB16_10
.LBB16_7:                               # %symbol_IsPredicate.exit6.thread.i
                                        #   in Loop: Header=BB16_5 Depth=1
	cmpl	%r9d, %edx
	jne	.LBB16_12
# BB#8:                                 #   in Loop: Header=BB16_5 Depth=1
	movq	16(%rdi), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %esi
	testl	%esi, %esi
	jns	.LBB16_12
# BB#9:                                 # %fol_IsLiteral.exit
                                        #   in Loop: Header=BB16_5 Depth=1
	negl	%esi
	andl	%r8d, %esi
	cmpl	$2, %esi
	jne	.LBB16_12
.LBB16_10:                              # %fol_IsLiteral.exit.thread13
                                        #   in Loop: Header=BB16_5 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB16_5
.LBB16_11:
	movl	$1, %eax
.LBB16_12:                              # %fol_IsLiteral.exit.thread
	retq
.Lfunc_end16:
	.size	fol_FormulaIsClause, .Lfunc_end16-fol_FormulaIsClause
	.cfi_endproc

	.globl	fol_FPrintOtterOptions
	.p2align	4, 0x90
	.type	fol_FPrintOtterOptions,@function
fol_FPrintOtterOptions:                 # @fol_FPrintOtterOptions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 32
.Lcfi88:
	.cfi_offset %rbx, -32
.Lcfi89:
	.cfi_offset %r14, -24
.Lcfi90:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %r14d
	movq	%rdi, %rbx
	cmpl	$3, %ebp
	ja	.LBB17_8
# BB#1:
	movl	%ebp, %eax
	jmpq	*.LJTI17_0(,%rax,8)
.LBB17_2:
	movl	$.L.str.12, %edi
	movl	$20, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.13, %edi
	movl	$17, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.14, %edi
	movl	$13, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.15, %edi
	movl	$19, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.16, %edi
	movl	$25, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	testl	%r14d, %r14d
	je	.LBB17_4
# BB#3:
	movl	$.L.str.17, %edi
	movl	$24, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.18, %edi
	movl	$25, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.19, %edi
	movl	$23, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.20, %edi
	movl	$16, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.21, %edi
	movl	$16, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.22, %edi
	movl	$21, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.23, %edi
	movl	$17, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB17_4:
	movl	$.L.str.24, %edi
	movl	$11, %esi
	jmp	.LBB17_5
.LBB17_7:
	movl	$.L.str.25, %edi
	movl	$12, %esi
.LBB17_5:
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB17_6:
	movl	$.L.str.30, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.LBB17_8:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.26, %esi
	movl	$.L.str.27, %edx
	movl	$575, %ecx              # imm = 0x23F
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.29, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end17:
	.size	fol_FPrintOtterOptions, .Lfunc_end17-fol_FPrintOtterOptions
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI17_0:
	.quad	.LBB17_6
	.quad	.LBB17_2
	.quad	.LBB17_4
	.quad	.LBB17_7

	.text
	.p2align	4, 0x90
	.type	misc_DumpCore,@function
misc_DumpCore:                          # @misc_DumpCore
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end18:
	.size	misc_DumpCore, .Lfunc_end18-misc_DumpCore
	.cfi_endproc

	.globl	fol_FPrintOtter
	.p2align	4, 0x90
	.type	fol_FPrintOtter,@function
fol_FPrintOtter:                        # @fol_FPrintOtter
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 48
.Lcfi97:
	.cfi_offset %rbx, -48
.Lcfi98:
	.cfi_offset %r12, -40
.Lcfi99:
	.cfi_offset %r14, -32
.Lcfi100:
	.cfi_offset %r15, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r12
	movq	%rdi, %r14
	testq	%r12, %r12
	je	.LBB19_12
# BB#1:
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB19_2:                               # %.lr.ph47
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	movl	fol_EQUALITY(%rip), %esi
	callq	term_ContainsSymbol
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB19_4
# BB#3:                                 # %.lr.ph47
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB19_2
.LBB19_4:                               # %.critedge
	movq	%r14, %rdi
	movl	%ebp, %esi
	movl	%r15d, %edx
	callq	fol_FPrintOtterOptions
	testq	%r12, %r12
	je	.LBB19_11
# BB#5:
	movl	$.L.str.31, %edi
	movl	$22, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testl	%ebp, %ebp
	je	.LBB19_7
# BB#6:
	movl	$.L.str.32, %edi
	movl	$13, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	.p2align	4, 0x90
.LBB19_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rax
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB19_9
# BB#8:                                 #   in Loop: Header=BB19_7 Depth=1
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	8(%r12), %rax
.LBB19_9:                               #   in Loop: Header=BB19_7 Depth=1
	movq	(%rax), %rsi
	movq	%r14, %rdi
	callq	fol_FPrintOtterFormula
	movl	$.L.str.34, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB19_7
# BB#10:                                # %._crit_edge
	movl	$.L.str.35, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.LBB19_12:                              # %.critedge.thread
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	%r15d, %edx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fol_FPrintOtterOptions  # TAILCALL
.LBB19_11:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	fol_FPrintOtter, .Lfunc_end19-fol_FPrintOtter
	.cfi_endproc

	.p2align	4, 0x90
	.type	fol_FPrintOtterFormula,@function
fol_FPrintOtterFormula:                 # @fol_FPrintOtterFormula
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 48
.Lcfi107:
	.cfi_offset %rbx, -48
.Lcfi108:
	.cfi_offset %r12, -40
.Lcfi109:
	.cfi_offset %r14, -32
.Lcfi110:
	.cfi_offset %r15, -24
.Lcfi111:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	(%r14), %r12d
	testl	%r12d, %r12d
	jns	.LBB20_6
# BB#1:                                 # %symbol_IsPredicate.exit
	movl	%r12d, %eax
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	jne	.LBB20_6
# BB#2:
	cmpl	fol_EQUALITY(%rip), %r12d
	jne	.LBB20_4
# BB#3:
	movq	16(%r14), %rax
	movq	8(%rax), %rsi
	movq	%rbx, %rdi
	callq	term_FPrintOtterPrefix
	movl	$.L.str.59, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movq	%rbx, %rdi
	jmp	.LBB20_5
.LBB20_6:                               # %symbol_IsPredicate.exit.thread
	movl	fol_ALL(%rip), %eax
	cmpl	%r12d, %eax
	je	.LBB20_8
# BB#7:                                 # %symbol_IsPredicate.exit.thread
	cmpl	%r12d, fol_EXIST(%rip)
	je	.LBB20_8
# BB#19:
	cmpl	fol_NOT(%rip), %r12d
	jne	.LBB20_21
# BB#20:
	movl	$.L.str.64, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	16(%r14), %rax
	movq	8(%rax), %rsi
	movq	%rbx, %rdi
	callq	fol_FPrintOtterFormula
.LBB20_44:                              # %._crit_edge110
	movl	$41, %edi
	movq	%rbx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fputc                   # TAILCALL
.LBB20_8:
	movq	16(%r14), %rcx
	movq	8(%rcx), %rdx
	movq	16(%rdx), %rbp
	testq	%rbp, %rbp
	jne	.LBB20_9
	jmp	.LBB20_15
	.p2align	4, 0x90
.LBB20_13:                              # %..lr.ph105_crit_edge
                                        #   in Loop: Header=BB20_9 Depth=1
	movl	fol_ALL(%rip), %eax
.LBB20_9:                               # %.lr.ph105
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%eax, %r12d
	jne	.LBB20_11
# BB#10:                                #   in Loop: Header=BB20_9 Depth=1
	movl	$.L.str.60, %edi
	movl	$4, %esi
	jmp	.LBB20_12
	.p2align	4, 0x90
.LBB20_11:                              #   in Loop: Header=BB20_9 Depth=1
	movl	$.L.str.61, %edi
	movl	$7, %esi
.LBB20_12:                              #   in Loop: Header=BB20_9 Depth=1
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	8(%rbp), %rsi
	movq	%rbx, %rdi
	callq	term_FPrintOtterPrefix
	movl	$.L.str.62, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB20_13
# BB#14:                                # %._crit_edge.loopexit
	movq	16(%r14), %rcx
.LBB20_15:                              # %._crit_edge
	movq	(%rcx), %rax
	movq	8(%rax), %rsi
	movq	%rbx, %rdi
	callq	fol_FPrintOtterFormula
	movq	16(%r14), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB20_18
	.p2align	4, 0x90
.LBB20_16:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$41, %edi
	movq	%rbx, %rsi
	callq	fputc
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB20_16
	jmp	.LBB20_18
.LBB20_21:
	cmpl	fol_AND(%rip), %r12d
	je	.LBB20_25
# BB#22:
	cmpl	fol_OR(%rip), %r12d
	je	.LBB20_25
# BB#23:
	cmpl	fol_EQUIV(%rip), %r12d
	je	.LBB20_25
# BB#24:
	cmpl	fol_IMPLIES(%rip), %r12d
	je	.LBB20_25
.LBB20_18:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_25:
	movl	$40, %edi
	movq	%rbx, %rsi
	callq	fputc
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.LBB20_44
# BB#26:                                # %.lr.ph109
	movl	symbol_TYPEMASK(%rip), %r15d
	.p2align	4, 0x90
.LBB20_27:                              # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rsi
	movl	(%rsi), %eax
	testl	%eax, %eax
	jns	.LBB20_29
# BB#28:                                # %symbol_IsPredicate.exit6.i
                                        #   in Loop: Header=BB20_27 Depth=1
	movl	%eax, %ecx
	negl	%ecx
	andl	%r15d, %ecx
	cmpl	$2, %ecx
	je	.LBB20_32
.LBB20_29:                              # %symbol_IsPredicate.exit6.thread.i
                                        #   in Loop: Header=BB20_27 Depth=1
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB20_33
# BB#30:                                #   in Loop: Header=BB20_27 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jns	.LBB20_33
# BB#31:                                # %fol_IsLiteral.exit
                                        #   in Loop: Header=BB20_27 Depth=1
	negl	%eax
	andl	%r15d, %eax
	cmpl	$2, %eax
	jne	.LBB20_33
.LBB20_32:                              # %fol_IsLiteral.exit.thread87
                                        #   in Loop: Header=BB20_27 Depth=1
	movq	%rbx, %rdi
	callq	fol_FPrintOtterFormula
	cmpq	$0, (%r14)
	jne	.LBB20_35
	jmp	.LBB20_44
	.p2align	4, 0x90
.LBB20_33:                              # %fol_IsLiteral.exit.thread
                                        #   in Loop: Header=BB20_27 Depth=1
	movl	$40, %edi
	movq	%rbx, %rsi
	callq	fputc
	movq	8(%r14), %rsi
	movq	%rbx, %rdi
	callq	fol_FPrintOtterFormula
	movl	$41, %edi
	movq	%rbx, %rsi
	callq	fputc
	cmpq	$0, (%r14)
	je	.LBB20_44
.LBB20_35:                              #   in Loop: Header=BB20_27 Depth=1
	cmpl	fol_AND(%rip), %r12d
	jne	.LBB20_37
# BB#36:                                #   in Loop: Header=BB20_27 Depth=1
	movl	$.L.str.66, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB20_37:                              #   in Loop: Header=BB20_27 Depth=1
	cmpl	fol_OR(%rip), %r12d
	jne	.LBB20_39
# BB#38:                                #   in Loop: Header=BB20_27 Depth=1
	movl	$.L.str.67, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB20_39:                              #   in Loop: Header=BB20_27 Depth=1
	cmpl	fol_EQUIV(%rip), %r12d
	jne	.LBB20_41
# BB#40:                                #   in Loop: Header=BB20_27 Depth=1
	movl	$.L.str.68, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB20_41:                              #   in Loop: Header=BB20_27 Depth=1
	cmpl	fol_IMPLIES(%rip), %r12d
	jne	.LBB20_43
# BB#42:                                #   in Loop: Header=BB20_27 Depth=1
	movl	$.L.str.69, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB20_43:                              # %.backedge
                                        #   in Loop: Header=BB20_27 Depth=1
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB20_27
	jmp	.LBB20_44
.LBB20_4:
	movq	%rbx, %rdi
	movq	%r14, %rsi
.LBB20_5:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	term_FPrintOtterPrefix  # TAILCALL
.Lfunc_end20:
	.size	fol_FPrintOtterFormula, .Lfunc_end20-fol_FPrintOtterFormula
	.cfi_endproc

	.globl	fol_FPrintDFGSignature
	.p2align	4, 0x90
	.type	fol_FPrintDFGSignature,@function
fol_FPrintDFGSignature:                 # @fol_FPrintDFGSignature
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi114:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi115:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi116:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi118:
	.cfi_def_cfa_offset 64
.Lcfi119:
	.cfi_offset %rbx, -56
.Lcfi120:
	.cfi_offset %r12, -48
.Lcfi121:
	.cfi_offset %r13, -40
.Lcfi122:
	.cfi_offset %r14, -32
.Lcfi123:
	.cfi_offset %r15, -24
.Lcfi124:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	symbol_GetAllFunctions
	movq	%rax, %rbp
	callq	symbol_GetAllPredicates
	movl	$fol_IsPredefinedPred, %esi
	movq	%rax, %rdi
	callq	list_DeleteElementIf
	movq	%rax, %r15
	testq	%rbp, %rbp
	je	.LBB21_9
# BB#1:
	movl	$.L.str.36, %edi
	movl	$12, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	symbol_TYPESTATBITS(%rip), %r12d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB21_2:                               # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rbx
	xorl	%eax, %eax
	subl	8(%rbx), %eax
	movl	%r12d, %ecx
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %rdx
	movl	16(%rax), %ecx
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	(%rbx), %rbp
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	testq	%rbp, %rbp
	je	.LBB21_4
# BB#3:                                 #   in Loop: Header=BB21_2 Depth=1
	movl	$.L.str.38, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB21_4:                               #   in Loop: Header=BB21_2 Depth=1
	cmpl	$14, %r13d
	ja	.LBB21_6
# BB#5:                                 #   in Loop: Header=BB21_2 Depth=1
	incl	%r13d
	testq	%rbp, %rbp
	jne	.LBB21_2
	jmp	.LBB21_8
	.p2align	4, 0x90
.LBB21_6:                               #   in Loop: Header=BB21_2 Depth=1
	movl	$.L.str.39, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	xorl	%r13d, %r13d
	testq	%rbp, %rbp
	jne	.LBB21_2
.LBB21_8:
	movl	$.L.str.40, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB21_9:
	testq	%r15, %r15
	je	.LBB21_18
# BB#10:
	movl	$.L.str.41, %edi
	movl	$13, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	symbol_TYPESTATBITS(%rip), %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB21_11:                              # =>This Inner Loop Header: Depth=1
	movq	%r15, %rbx
	xorl	%eax, %eax
	subl	8(%rbx), %eax
	movl	%r12d, %ecx
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %rdx
	movl	16(%rax), %ecx
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	(%rbx), %r15
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	testq	%r15, %r15
	je	.LBB21_13
# BB#12:                                #   in Loop: Header=BB21_11 Depth=1
	movl	$.L.str.38, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB21_13:                              #   in Loop: Header=BB21_11 Depth=1
	cmpl	$14, %ebp
	ja	.LBB21_15
# BB#14:                                #   in Loop: Header=BB21_11 Depth=1
	incl	%ebp
	testq	%r15, %r15
	jne	.LBB21_11
	jmp	.LBB21_17
	.p2align	4, 0x90
.LBB21_15:                              #   in Loop: Header=BB21_11 Depth=1
	movl	$.L.str.39, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	xorl	%ebp, %ebp
	testq	%r15, %r15
	jne	.LBB21_11
.LBB21_17:
	movl	$.L.str.40, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.LBB21_18:                              # %list_Delete.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	fol_FPrintDFGSignature, .Lfunc_end21-fol_FPrintDFGSignature
	.cfi_endproc

	.globl	fol_FPrintDFG
	.p2align	4, 0x90
	.type	fol_FPrintDFG,@function
fol_FPrintDFG:                          # @fol_FPrintDFG
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi125:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi126:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi128:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 48
.Lcfi130:
	.cfi_offset %rbx, -48
.Lcfi131:
	.cfi_offset %r12, -40
.Lcfi132:
	.cfi_offset %r14, -32
.Lcfi133:
	.cfi_offset %r15, -24
.Lcfi134:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	$0, 16(%r14)
	movl	(%r14), %ebp
	je	.LBB22_13
# BB#1:
	movl	fol_ALL(%rip), %r12d
	movl	fol_EXIST(%rip), %r15d
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	symbol_FPrint
	cmpl	%ebp, %r12d
	je	.LBB22_3
# BB#2:
	cmpl	%ebp, %r15d
	je	.LBB22_3
# BB#7:
	movl	$40, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	16(%r14), %rbp
	testq	%rbp, %rbp
	jne	.LBB22_9
	jmp	.LBB22_12
	.p2align	4, 0x90
.LBB22_10:                              # %.backedge32
                                        #   in Loop: Header=BB22_9 Depth=1
	movl	$44, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB22_12
.LBB22_9:                               # %.lr.ph37
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	movq	%rbx, %rdi
	callq	fol_FPrintDFG
	cmpq	$0, (%rbp)
	jne	.LBB22_10
	jmp	.LBB22_12
.LBB22_13:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	symbol_FPrint           # TAILCALL
.LBB22_3:
	movl	$.L.str.42, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	16(%r14), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rbp
	testq	%rbp, %rbp
	jne	.LBB22_5
	jmp	.LBB22_11
	.p2align	4, 0x90
.LBB22_6:                               # %.backedge
                                        #   in Loop: Header=BB22_5 Depth=1
	movl	$44, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB22_11
.LBB22_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	movq	%rbx, %rdi
	callq	fol_FPrintDFG
	cmpq	$0, (%rbp)
	jne	.LBB22_6
.LBB22_11:                              # %fol_TermListFPrintDFG.exit
	movl	$.L.str.43, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movq	%rbx, %rdi
	callq	fol_FPrintDFG
.LBB22_12:                              # %fol_TermListFPrintDFG.exit28
	movl	$41, %edi
	movq	%rbx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.Lfunc_end22:
	.size	fol_FPrintDFG, .Lfunc_end22-fol_FPrintDFG
	.cfi_endproc

	.globl	fol_PrintDFG
	.p2align	4, 0x90
	.type	fol_PrintDFG,@function
fol_PrintDFG:                           # @fol_PrintDFG
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	stdout(%rip), %rdi
	movq	%rax, %rsi
	jmp	fol_FPrintDFG           # TAILCALL
.Lfunc_end23:
	.size	fol_PrintDFG, .Lfunc_end23-fol_PrintDFG
	.cfi_endproc

	.globl	fol_PrintPrecedence
	.p2align	4, 0x90
	.type	fol_PrintPrecedence,@function
fol_PrintPrecedence:                    # @fol_PrintPrecedence
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi135:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi136:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi138:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 48
.Lcfi140:
	.cfi_offset %rbx, -48
.Lcfi141:
	.cfi_offset %r12, -40
.Lcfi142:
	.cfi_offset %r14, -32
.Lcfi143:
	.cfi_offset %r15, -24
.Lcfi144:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	symbol_SignatureExists
	testl	%eax, %eax
	je	.LBB24_17
# BB#1:                                 # %.preheader
	movl	symbol_ACTINDEX(%rip), %ecx
	cmpl	$2, %ecx
	jl	.LBB24_2
# BB#3:                                 # %.lr.ph36
	movl	symbol_TYPEMASK(%rip), %r15d
	xorl	%r12d, %r12d
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB24_4:                               # =>This Inner Loop Header: Depth=1
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%rbp,8), %rax
	testq	%rax, %rax
	je	.LBB24_11
# BB#5:                                 #   in Loop: Header=BB24_4 Depth=1
	movslq	24(%rax), %rbx
	testq	%rbx, %rbx
	jns	.LBB24_11
# BB#6:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB24_4 Depth=1
	movl	%ebx, %eax
	negl	%eax
	andl	%r15d, %eax
	cmpl	$2, %eax
	ja	.LBB24_11
# BB#7:                                 #   in Loop: Header=BB24_4 Depth=1
	cmpl	%ebx, fol_EQUALITY(%rip)
	je	.LBB24_11
# BB#8:                                 #   in Loop: Header=BB24_4 Depth=1
	cmpl	%ebx, fol_TRUE(%rip)
	je	.LBB24_11
# BB#9:                                 #   in Loop: Header=BB24_4 Depth=1
	cmpl	%ebx, fol_FALSE(%rip)
	je	.LBB24_11
# BB#10:                                #   in Loop: Header=BB24_4 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movl	symbol_ACTINDEX(%rip), %ecx
	movq	%rax, %r12
	.p2align	4, 0x90
.LBB24_11:                              # %symbol_IsFunction.exit.thread
                                        #   in Loop: Header=BB24_4 Depth=1
	incq	%rbp
	movslq	%ecx, %rax
	cmpq	%rax, %rbp
	jl	.LBB24_4
	jmp	.LBB24_12
.LBB24_2:
	xorl	%r12d, %r12d
.LBB24_12:                              # %._crit_edge37
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	symbol_SortByPrecedence
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB24_17
# BB#13:                                # %.lr.ph
	movl	symbol_TYPESTATBITS(%rip), %r14d
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB24_14:                              # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	subl	8(%rbx), %eax
	movl	%r14d, %ecx
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %rdi
	movq	stdout(%rip), %rsi
	callq	fputs
	cmpq	$0, (%rbx)
	je	.LBB24_16
# BB#15:                                #   in Loop: Header=BB24_14 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.44, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB24_14
	.p2align	4, 0x90
.LBB24_16:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB24_16
.LBB24_17:                              # %list_Delete.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	fol_PrintPrecedence, .Lfunc_end24-fol_PrintPrecedence
	.cfi_endproc

	.globl	fol_FPrintPrecedence
	.p2align	4, 0x90
	.type	fol_FPrintPrecedence,@function
fol_FPrintPrecedence:                   # @fol_FPrintPrecedence
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi148:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi149:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi151:
	.cfi_def_cfa_offset 64
.Lcfi152:
	.cfi_offset %rbx, -56
.Lcfi153:
	.cfi_offset %r12, -48
.Lcfi154:
	.cfi_offset %r13, -40
.Lcfi155:
	.cfi_offset %r14, -32
.Lcfi156:
	.cfi_offset %r15, -24
.Lcfi157:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	callq	symbol_SignatureExists
	testl	%eax, %eax
	je	.LBB25_24
# BB#1:                                 # %.preheader
	movl	symbol_ACTINDEX(%rip), %ecx
	cmpl	$2, %ecx
	jl	.LBB25_2
# BB#3:                                 # %.lr.ph57
	movl	symbol_TYPEMASK(%rip), %r15d
	xorl	%r12d, %r12d
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB25_4:                               # =>This Inner Loop Header: Depth=1
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%rbp,8), %rax
	testq	%rax, %rax
	je	.LBB25_11
# BB#5:                                 #   in Loop: Header=BB25_4 Depth=1
	movslq	24(%rax), %rbx
	testq	%rbx, %rbx
	jns	.LBB25_11
# BB#6:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB25_4 Depth=1
	movl	%ebx, %eax
	negl	%eax
	andl	%r15d, %eax
	cmpl	$2, %eax
	ja	.LBB25_11
# BB#7:                                 #   in Loop: Header=BB25_4 Depth=1
	cmpl	%ebx, fol_EQUALITY(%rip)
	je	.LBB25_11
# BB#8:                                 #   in Loop: Header=BB25_4 Depth=1
	cmpl	%ebx, fol_TRUE(%rip)
	je	.LBB25_11
# BB#9:                                 #   in Loop: Header=BB25_4 Depth=1
	cmpl	%ebx, fol_FALSE(%rip)
	je	.LBB25_11
# BB#10:                                #   in Loop: Header=BB25_4 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movl	symbol_ACTINDEX(%rip), %ecx
	movq	%rax, %r12
	.p2align	4, 0x90
.LBB25_11:                              # %symbol_IsFunction.exit.thread
                                        #   in Loop: Header=BB25_4 Depth=1
	incq	%rbp
	movslq	%ecx, %rax
	cmpq	%rax, %rbp
	jl	.LBB25_4
	jmp	.LBB25_12
.LBB25_2:
	xorl	%r12d, %r12d
.LBB25_12:                              # %._crit_edge58
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	symbol_SortByPrecedence
	movq	%rax, %r14
	movl	$.L.str.45, %edi
	movl	$15, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	testq	%r14, %r14
	je	.LBB25_25
# BB#13:                                # %.lr.ph
	movl	symbol_TYPESTATBITS(%rip), %r15d
	xorl	%r12d, %r12d
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB25_14:                              # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	subl	8(%rbp), %eax
	movl	%r15d, %ecx
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rbx
	movl	$40, %edi
	movq	%r13, %rsi
	callq	_IO_putc
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	callq	fputs
	movl	$44, %edi
	movq	%r13, %rsi
	callq	_IO_putc
	movl	12(%rbx), %edx
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	movl	$44, %edi
	movq	%r13, %rsi
	callq	_IO_putc
	xorl	%eax, %eax
	subl	8(%rbp), %eax
	movl	%r15d, %ecx
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movl	20(%rax), %eax
	movl	$114, %edi
	testb	$8, %al
	jne	.LBB25_16
# BB#15:                                #   in Loop: Header=BB25_14 Depth=1
	shrl	$4, %eax
	andl	$1, %eax
	orl	$108, %eax
	movl	%eax, %edi
.LBB25_16:                              #   in Loop: Header=BB25_14 Depth=1
	movq	%r13, %rsi
	callq	_IO_putc
	movl	$41, %edi
	movq	%r13, %rsi
	callq	_IO_putc
	cmpq	$0, (%rbp)
	je	.LBB25_18
# BB#17:                                #   in Loop: Header=BB25_14 Depth=1
	movl	$44, %edi
	movq	%r13, %rsi
	callq	_IO_putc
.LBB25_18:                              #   in Loop: Header=BB25_14 Depth=1
	cmpl	$16, %r12d
	jl	.LBB25_20
# BB#19:                                #   in Loop: Header=BB25_14 Depth=1
	movl	$.L.str.39, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	xorl	%r12d, %r12d
	jmp	.LBB25_21
	.p2align	4, 0x90
.LBB25_20:                              #   in Loop: Header=BB25_14 Depth=1
	incl	%r12d
.LBB25_21:                              #   in Loop: Header=BB25_14 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB25_14
# BB#22:                                # %._crit_edge
	movl	$.L.str.47, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	testq	%r14, %r14
	je	.LBB25_24
	.p2align	4, 0x90
.LBB25_23:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB25_23
.LBB25_24:                              # %list_Delete.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_25:                              # %._crit_edge.thread
	movl	$.L.str.47, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end25:
	.size	fol_FPrintPrecedence, .Lfunc_end25-fol_FPrintPrecedence
	.cfi_endproc

	.globl	fol_FPrintDFGProblem
	.p2align	4, 0x90
	.type	fol_FPrintDFGProblem,@function
fol_FPrintDFGProblem:                   # @fol_FPrintDFGProblem
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi158:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi159:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi161:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi162:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi163:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi164:
	.cfi_def_cfa_offset 64
.Lcfi165:
	.cfi_offset %rbx, -56
.Lcfi166:
	.cfi_offset %r12, -48
.Lcfi167:
	.cfi_offset %r13, -40
.Lcfi168:
	.cfi_offset %r14, -32
.Lcfi169:
	.cfi_offset %r15, -24
.Lcfi170:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %r12
	movq	%rcx, %r13
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	64(%rsp), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$.L.str.48, %edi
	movl	$25, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.49, %edi
	movl	$22, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.50, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	fprintf
	movl	$.L.str.51, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fprintf
	movl	$.L.str.52, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rdx
	callq	fprintf
	movl	$.L.str.53, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r12, %rdx
	callq	fprintf
	movl	$.L.str.35, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.54, %edi
	movl	$17, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	%rbx, %rdi
	callq	fol_FPrintDFGSignature
	movl	$.L.str.35, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.55, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	fol_FPrintFormulaList
	movl	$.L.str.56, %edx
	movq	%rbx, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	fol_FPrintFormulaList
	movl	$.L.str.57, %edi
	movl	$13, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end26:
	.size	fol_FPrintDFGProblem, .Lfunc_end26-fol_FPrintDFGProblem
	.cfi_endproc

	.p2align	4, 0x90
	.type	fol_FPrintFormulaList,@function
fol_FPrintFormulaList:                  # @fol_FPrintFormulaList
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi173:
	.cfi_def_cfa_offset 32
.Lcfi174:
	.cfi_offset %rbx, -32
.Lcfi175:
	.cfi_offset %r14, -24
.Lcfi176:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$.L.str.70, %edi
	movl	$17, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	fputs
	movl	$.L.str.71, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testq	%rbx, %rbx
	je	.LBB27_3
	.p2align	4, 0x90
.LBB27_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.72, %edi
	movl	$9, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	fol_FPrintDFG
	movl	$.L.str.71, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB27_1
.LBB27_3:                               # %._crit_edge
	movl	$.L.str.35, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	fwrite                  # TAILCALL
.Lfunc_end27:
	.size	fol_FPrintFormulaList, .Lfunc_end27-fol_FPrintFormulaList
	.cfi_endproc

	.globl	fol_AssocEquation
	.p2align	4, 0x90
	.type	fol_AssocEquation,@function
fol_AssocEquation:                      # @fol_AssocEquation
	.cfi_startproc
# BB#0:
	movl	fol_EQUALITY(%rip), %ecx
	xorl	%eax, %eax
	cmpl	(%rdi), %ecx
	jne	.LBB28_16
# BB#1:
	movq	16(%rdi), %rdx
	movq	8(%rdx), %r10
	movl	(%r10), %r8d
	testl	%r8d, %r8d
	jns	.LBB28_16
# BB#2:                                 # %symbol_IsFunction.exit
	movl	%r8d, %edi
	negl	%edi
	movl	symbol_TYPEMASK(%rip), %ecx
	andl	%edi, %ecx
	orl	$1, %ecx
	cmpl	$1, %ecx
	jne	.LBB28_16
# BB#3:
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %edi
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	%edi, %rdi
	movq	(%rcx,%rdi,8), %rcx
	cmpl	$2, 16(%rcx)
	jne	.LBB28_16
# BB#4:
	movq	(%rdx), %rcx
	movq	8(%rcx), %rdi
	cmpl	(%rdi), %r8d
	jne	.LBB28_16
# BB#5:
	movq	16(%r10), %rdx
	movq	8(%rdx), %rcx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jle	.LBB28_7
# BB#6:
	movq	%rdi, %r10
	jmp	.LBB28_8
.LBB28_7:
	movq	16(%rdi), %rdx
	movq	8(%rdx), %rcx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jle	.LBB28_16
.LBB28_8:
	movq	(%rdx), %rdx
	movq	8(%rdx), %rdx
	cmpl	%r8d, (%rdx)
	jne	.LBB28_16
# BB#9:
	movq	16(%rdx), %rdi
	movq	8(%rdi), %rdx
	movl	(%rdx), %edx
	testl	%edx, %edx
	jle	.LBB28_16
# BB#10:
	movq	(%rdi), %rdi
	movq	8(%rdi), %rdi
	movl	(%rdi), %r9d
	testl	%r9d, %r9d
	jle	.LBB28_16
# BB#11:
	movq	16(%r10), %r11
	movq	8(%r11), %rdi
	cmpl	%r8d, (%rdi)
	jne	.LBB28_16
# BB#12:
	movq	16(%rdi), %r10
	movq	8(%r10), %rdi
	cmpl	(%rdi), %ecx
	jne	.LBB28_16
# BB#13:
	movq	(%r10), %rcx
	movq	8(%rcx), %rcx
	cmpl	(%rcx), %edx
	jne	.LBB28_16
# BB#14:
	movq	(%r11), %rcx
	movq	8(%rcx), %rcx
	cmpl	(%rcx), %r9d
	jne	.LBB28_16
# BB#15:
	movl	%r8d, (%rsi)
	movl	$1, %eax
.LBB28_16:                              # %.thread76
	retq
.Lfunc_end28:
	.size	fol_AssocEquation, .Lfunc_end28-fol_AssocEquation
	.cfi_endproc

	.globl	fol_DistributiveEquation
	.p2align	4, 0x90
	.type	fol_DistributiveEquation,@function
fol_DistributiveEquation:               # @fol_DistributiveEquation
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi177:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi178:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi179:
	.cfi_def_cfa_offset 32
.Lcfi180:
	.cfi_offset %rbx, -32
.Lcfi181:
	.cfi_offset %r14, -24
.Lcfi182:
	.cfi_offset %rbp, -16
	movl	fol_EQUALITY(%rip), %ecx
	xorl	%eax, %eax
	cmpl	(%rdi), %ecx
	jne	.LBB29_21
# BB#1:
	movq	16(%rdi), %rcx
	movq	8(%rcx), %r8
	movl	(%r8), %ebx
	testl	%ebx, %ebx
	jns	.LBB29_21
# BB#2:
	movq	(%rcx), %rcx
	movq	8(%rcx), %r9
	movl	(%r9), %r11d
	cmpl	%r11d, %ebx
	je	.LBB29_21
# BB#3:                                 # %symbol_IsFunction.exit96
	movl	%ebx, %edi
	negl	%edi
	movl	symbol_TYPEMASK(%rip), %ebp
	movl	%ebp, %ecx
	andl	%edi, %ecx
	orl	$1, %ecx
	cmpl	$1, %ecx
	jne	.LBB29_21
# BB#4:                                 # %symbol_IsFunction.exit96
	testl	%r11d, %r11d
	jns	.LBB29_21
# BB#5:                                 # %symbol_IsFunction.exit
	movl	%r11d, %r10d
	negl	%r10d
	andl	%r10d, %ebp
	orl	$1, %ebp
	cmpl	$1, %ebp
	jne	.LBB29_21
# BB#6:
	movl	symbol_TYPESTATBITS(%rip), %ecx
	sarl	%cl, %edi
	movq	symbol_SIGNATURE(%rip), %r14
	movslq	%edi, %rdi
	movq	(%r14,%rdi,8), %rdi
	cmpl	$2, 16(%rdi)
	jne	.LBB29_21
# BB#7:
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r10d
	movslq	%r10d, %rcx
	movq	(%r14,%rcx,8), %rcx
	cmpl	$2, 16(%rcx)
	jne	.LBB29_21
# BB#8:
	movq	16(%r8), %rdi
	movq	8(%rdi), %rcx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jle	.LBB29_10
# BB#9:
	movl	%ebx, %r11d
	movq	%r8, %r10
	movq	%r9, %r8
	jmp	.LBB29_11
.LBB29_10:
	movq	16(%r9), %rdi
	movq	8(%rdi), %rcx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	movq	%r9, %r10
	jle	.LBB29_21
.LBB29_11:                              # %._crit_edge
	movq	16(%r8), %rbp
	movq	8(%rbp), %rbx
	cmpl	(%rbx), %r11d
	jne	.LBB29_21
# BB#12:
	movq	(%rbp), %rbp
	movq	8(%rbp), %r9
	cmpl	(%r9), %r11d
	jne	.LBB29_21
# BB#13:
	movq	(%rdi), %rdi
	movq	8(%rdi), %rdi
	movl	(%rdi), %r14d
	cmpl	(%r8), %r14d
	jne	.LBB29_21
# BB#14:
	movq	16(%rdi), %rdi
	movq	8(%rdi), %rbp
	movl	(%rbp), %r11d
	testl	%r11d, %r11d
	jle	.LBB29_21
# BB#15:
	movq	(%rdi), %rdi
	movq	8(%rdi), %rdi
	movl	(%rdi), %r8d
	testl	%r8d, %r8d
	jle	.LBB29_21
# BB#16:
	movq	16(%rbx), %rdi
	movq	8(%rdi), %rbx
	cmpl	%ecx, (%rbx)
	jne	.LBB29_21
# BB#17:
	movq	(%rdi), %rdi
	movq	8(%rdi), %rdi
	cmpl	%r11d, (%rdi)
	jne	.LBB29_21
# BB#18:
	movq	16(%r9), %rdi
	movq	8(%rdi), %rbp
	cmpl	%ecx, (%rbp)
	jne	.LBB29_21
# BB#19:
	movq	(%rdi), %rcx
	movq	8(%rcx), %rcx
	cmpl	%r8d, (%rcx)
	jne	.LBB29_21
# BB#20:
	movl	%r14d, (%rsi)
	movl	(%r10), %eax
	movl	%eax, (%rdx)
	movl	$1, %eax
.LBB29_21:                              # %symbol_IsFunction.exit96.thread
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end29:
	.size	fol_DistributiveEquation, .Lfunc_end29-fol_DistributiveEquation
	.cfi_endproc

	.globl	fol_Instances
	.p2align	4, 0x90
	.type	fol_Instances,@function
fol_Instances:                          # @fol_Instances
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi183:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi184:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi185:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi186:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi187:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi188:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi189:
	.cfi_def_cfa_offset 64
.Lcfi190:
	.cfi_offset %rbx, -56
.Lcfi191:
	.cfi_offset %r12, -48
.Lcfi192:
	.cfi_offset %r13, -40
.Lcfi193:
	.cfi_offset %r14, -32
.Lcfi194:
	.cfi_offset %r15, -24
.Lcfi195:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movq	%r12, %rdi
	callq	term_ComputeSize
	movl	%eax, %r14d
	movq	%rbp, %rdi
	callq	term_InstallSize
	movl	stack_POINTER(%rip), %ebx
	movl	symbol_TYPEMASK(%rip), %r13d
	xorl	%r15d, %r15d
	cmpq	%r12, %rbp
	je	.LBB30_20
.LBB30_11:
	cmpl	%r14d, 28(%rbp)
	jb	.LBB30_20
# BB#12:
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	unify_MatchFlexible
	testl	%eax, %eax
	je	.LBB30_3
# BB#13:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
.LBB30_14:
	xorps	%xmm0, %xmm0
.LBB30_15:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB30_18
# BB#16:                                # %.lr.ph.i.i.preheader
	incl	%eax
	.p2align	4, 0x90
.LBB30_17:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB30_17
.LBB30_18:                              # %._crit_edge.i.i
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB30_20
# BB#19:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB30_20
	.p2align	4, 0x90
.LBB30_3:
	movl	(%rbp), %eax
	testl	%eax, %eax
	jns	.LBB30_6
# BB#4:                                 # %symbol_IsPredicate.exit.i
	movl	%eax, %ecx
	negl	%ecx
	andl	%r13d, %ecx
	cmpl	$2, %ecx
	je	.LBB30_14
.LBB30_6:                               # %symbol_IsPredicate.exit.thread.i
	cmpl	%eax, fol_ALL(%rip)
	movq	16(%rbp), %rcx
	xorps	%xmm0, %xmm0
	je	.LBB30_9
# BB#7:                                 # %symbol_IsPredicate.exit.thread.i
	cmpl	%eax, fol_EXIST(%rip)
	je	.LBB30_9
# BB#8:
	movl	stack_POINTER(%rip), %eax
	leal	1(%rax), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rcx, stack_STACK(,%rax,8)
	jmp	.LBB30_15
.LBB30_9:
	movq	(%rcx), %rax
	movl	stack_POINTER(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
	jmp	.LBB30_15
	.p2align	4, 0x90
.LBB30_2:                               # %.critedge.i
                                        #   in Loop: Header=BB30_20 Depth=1
	cmpl	%eax, %ebx
	je	.LBB30_22
# BB#10:                                #   in Loop: Header=BB30_20 Depth=1
	movq	(%rdx), %rax
	movq	8(%rdx), %rbp
	movq	%rax, stack_STACK(,%rcx,8)
	cmpq	%r12, %rbp
	jne	.LBB30_11
	.p2align	4, 0x90
.LBB30_20:                              # %cont_BackTrack.exit.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_21 Depth 2
	movl	stack_POINTER(%rip), %eax
	cmpl	%ebx, %eax
	je	.LBB30_22
	.p2align	4, 0x90
.LBB30_21:                              # %.lr.ph.i
                                        #   Parent Loop BB30_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rax), %ecx
	movq	stack_STACK(,%rcx,8), %rdx
	testq	%rdx, %rdx
	jne	.LBB30_2
# BB#1:                                 # %cont_BackTrack.exit.i
                                        #   in Loop: Header=BB30_21 Depth=2
	movl	%ecx, stack_POINTER(%rip)
	cmpl	%ecx, %ebx
	movl	%ecx, %eax
	jne	.LBB30_21
.LBB30_22:                              # %fol_InstancesIntern.exit
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	fol_Instances, .Lfunc_end30-fol_Instances
	.cfi_endproc

	.globl	fol_Generalizations
	.p2align	4, 0x90
	.type	fol_Generalizations,@function
fol_Generalizations:                    # @fol_Generalizations
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi196:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi197:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi198:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi199:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi200:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi201:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi202:
	.cfi_def_cfa_offset 64
.Lcfi203:
	.cfi_offset %rbx, -56
.Lcfi204:
	.cfi_offset %r12, -48
.Lcfi205:
	.cfi_offset %r13, -40
.Lcfi206:
	.cfi_offset %r14, -32
.Lcfi207:
	.cfi_offset %r15, -24
.Lcfi208:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movq	%r12, %rdi
	callq	term_ComputeSize
	movl	%eax, %r14d
	movq	%rbp, %rdi
	callq	term_InstallSize
	movl	stack_POINTER(%rip), %ebx
	movl	symbol_TYPEMASK(%rip), %r13d
	xorl	%r15d, %r15d
	movl	%ebx, %eax
	cmpq	%r12, %rbp
	je	.LBB31_26
.LBB31_11:
	cmpl	%r14d, 28(%rbp)
	jbe	.LBB31_17
# BB#12:
	movl	(%rbp), %ecx
	testl	%ecx, %ecx
	jns	.LBB31_14
# BB#13:                                # %symbol_IsPredicate.exit.i
	movl	%ecx, %edx
	negl	%edx
	andl	%r13d, %edx
	cmpl	$2, %edx
	je	.LBB31_26
.LBB31_14:                              # %symbol_IsPredicate.exit.thread.i
	cmpl	%ecx, fol_ALL(%rip)
	movq	16(%rbp), %rdx
	je	.LBB31_25
# BB#15:                                # %symbol_IsPredicate.exit.thread.i
	cmpl	%ecx, fol_EXIST(%rip)
	je	.LBB31_25
# BB#16:
	leal	1(%rax), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movl	%eax, %eax
	movq	%rdx, stack_STACK(,%rax,8)
	jmp	.LBB31_26
	.p2align	4, 0x90
.LBB31_17:
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	unify_MatchFlexible
	testl	%eax, %eax
	je	.LBB31_3
# BB#18:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
.LBB31_19:
	xorps	%xmm0, %xmm0
.LBB31_20:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB31_23
# BB#21:                                # %.lr.ph.i.i.preheader
	incl	%eax
	.p2align	4, 0x90
.LBB31_22:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB31_22
.LBB31_23:                              # %._crit_edge.i.i
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB31_26
# BB#24:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB31_26
	.p2align	4, 0x90
.LBB31_25:
	movq	(%rdx), %rcx
	leal	1(%rax), %edx
	movl	%edx, stack_POINTER(%rip)
	movl	%eax, %eax
	movq	%rcx, stack_STACK(,%rax,8)
	jmp	.LBB31_26
.LBB31_3:
	movl	(%rbp), %eax
	testl	%eax, %eax
	jns	.LBB31_6
# BB#4:                                 # %symbol_IsPredicate.exit33.i
	movl	%eax, %ecx
	negl	%ecx
	andl	%r13d, %ecx
	cmpl	$2, %ecx
	je	.LBB31_19
.LBB31_6:                               # %symbol_IsPredicate.exit33.thread.i
	cmpl	%eax, fol_ALL(%rip)
	movq	16(%rbp), %rcx
	xorps	%xmm0, %xmm0
	je	.LBB31_9
# BB#7:                                 # %symbol_IsPredicate.exit33.thread.i
	cmpl	%eax, fol_EXIST(%rip)
	je	.LBB31_9
# BB#8:
	movl	stack_POINTER(%rip), %eax
	leal	1(%rax), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rcx, stack_STACK(,%rax,8)
	jmp	.LBB31_20
.LBB31_9:
	movq	(%rcx), %rax
	movl	stack_POINTER(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
	jmp	.LBB31_20
	.p2align	4, 0x90
.LBB31_2:                               # %.critedge.i
                                        #   in Loop: Header=BB31_26 Depth=1
	cmpl	%eax, %ebx
	je	.LBB31_28
# BB#10:                                #   in Loop: Header=BB31_26 Depth=1
	movq	(%rdx), %rsi
	movq	8(%rdx), %rbp
	movq	%rsi, stack_STACK(,%rcx,8)
	cmpq	%r12, %rbp
	jne	.LBB31_11
	.p2align	4, 0x90
.LBB31_26:                              # %cont_BackTrack.exit.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_27 Depth 2
	movl	stack_POINTER(%rip), %eax
	cmpl	%ebx, %eax
	je	.LBB31_28
	.p2align	4, 0x90
.LBB31_27:                              # %.lr.ph.i
                                        #   Parent Loop BB31_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rax), %ecx
	movq	stack_STACK(,%rcx,8), %rdx
	testq	%rdx, %rdx
	jne	.LBB31_2
# BB#1:                                 # %cont_BackTrack.exit.i
                                        #   in Loop: Header=BB31_27 Depth=2
	movl	%ecx, stack_POINTER(%rip)
	cmpl	%ecx, %ebx
	movl	%ecx, %eax
	jne	.LBB31_27
.LBB31_28:                              # %fol_GeneralizationsIntern.exit
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	fol_Generalizations, .Lfunc_end31-fol_Generalizations
	.cfi_endproc

	.globl	fol_MostGeneralFormula
	.p2align	4, 0x90
	.type	fol_MostGeneralFormula,@function
fol_MostGeneralFormula:                 # @fol_MostGeneralFormula
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi209:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi210:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi211:
	.cfi_def_cfa_offset 32
.Lcfi212:
	.cfi_offset %rbx, -32
.Lcfi213:
	.cfi_offset %r14, -24
.Lcfi214:
	.cfi_offset %r15, -16
	movq	(%rdi), %rbx
	movq	8(%rdi), %r14
	testq	%rbx, %rbx
	je	.LBB32_9
# BB#1:                                 # %.lr.ph.preheader
	movl	cont_BINDINGS(%rip), %eax
	movl	cont_STACKPOINTER(%rip), %ecx
	.p2align	4, 0x90
.LBB32_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_5 Depth 2
	movq	%r14, %r15
	movq	8(%rbx), %r14
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	unify_MatchFlexible
	testl	%eax, %eax
	cmoveq	%r15, %r14
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB32_3
# BB#4:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB32_2 Depth=1
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB32_5:                               # %.lr.ph.i
                                        #   Parent Loop BB32_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB32_5
	jmp	.LBB32_6
	.p2align	4, 0x90
.LBB32_3:                               #   in Loop: Header=BB32_2 Depth=1
	movl	%ecx, %eax
.LBB32_6:                               # %._crit_edge.i
                                        #   in Loop: Header=BB32_2 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB32_7
# BB#10:                                #   in Loop: Header=BB32_2 Depth=1
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB32_8
	.p2align	4, 0x90
.LBB32_7:                               #   in Loop: Header=BB32_2 Depth=1
	xorl	%ecx, %ecx
.LBB32_8:                               # %cont_BackTrack.exit.backedge
                                        #   in Loop: Header=BB32_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB32_2
.LBB32_9:                               # %cont_BackTrack.exit._crit_edge
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end32:
	.size	fol_MostGeneralFormula, .Lfunc_end32-fol_MostGeneralFormula
	.cfi_endproc

	.globl	fol_ReplaceVariable
	.p2align	4, 0x90
	.type	fol_ReplaceVariable,@function
fol_ReplaceVariable:                    # @fol_ReplaceVariable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi215:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi216:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi217:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi218:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi219:
	.cfi_def_cfa_offset 48
.Lcfi220:
	.cfi_offset %rbx, -40
.Lcfi221:
	.cfi_offset %r14, -32
.Lcfi222:
	.cfi_offset %r15, -24
.Lcfi223:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	(%r14), %eax
	cmpl	%eax, fol_ALL(%rip)
	je	.LBB33_2
# BB#1:
	cmpl	%eax, fol_EXIST(%rip)
	jne	.LBB33_5
.LBB33_2:
	movq	16(%r14), %rax
	movq	8(%rax), %rcx
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB33_7
	jmp	.LBB33_4
	.p2align	4, 0x90
.LBB33_8:                               #   in Loop: Header=BB33_7 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB33_4
.LBB33_7:                               # %.lr.ph39
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	cmpl	%ebp, (%rdx)
	jne	.LBB33_8
	jmp	.LBB33_12
.LBB33_4:                               # %._crit_edge
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	fol_ReplaceVariable
	movl	(%r14), %eax
.LBB33_5:
	cmpl	%ebp, %eax
	jne	.LBB33_9
# BB#6:
	movl	(%r15), %eax
	movl	%eax, (%r14)
	movq	16(%r15), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 16(%r14)
	jmp	.LBB33_12
.LBB33_9:
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB33_12
	.p2align	4, 0x90
.LBB33_10:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	fol_ReplaceVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB33_10
.LBB33_12:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end33:
	.size	fol_ReplaceVariable, .Lfunc_end33-fol_ReplaceVariable
	.cfi_endproc

	.globl	fol_PrettyPrintDFG
	.p2align	4, 0x90
	.type	fol_PrettyPrintDFG,@function
fol_PrettyPrintDFG:                     # @fol_PrettyPrintDFG
	.cfi_startproc
# BB#0:
	xorl	%esi, %esi
	jmp	fol_PrettyPrintInternDFG # TAILCALL
.Lfunc_end34:
	.size	fol_PrettyPrintDFG, .Lfunc_end34-fol_PrettyPrintDFG
	.cfi_endproc

	.p2align	4, 0x90
	.type	fol_PrettyPrintInternDFG,@function
fol_PrettyPrintInternDFG:               # @fol_PrettyPrintInternDFG
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi224:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi225:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi226:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi227:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi228:
	.cfi_def_cfa_offset 48
.Lcfi229:
	.cfi_offset %rbx, -40
.Lcfi230:
	.cfi_offset %r14, -32
.Lcfi231:
	.cfi_offset %r15, -24
.Lcfi232:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	(%rbx), %r15d
	cmpl	fol_VARLIST(%rip), %r15d
	jne	.LBB35_1
# BB#24:
	movq	stdout(%rip), %rsi
	movl	$91, %edi
	callq	_IO_putc
	movq	16(%rbx), %rdi
	callq	term_TermListPrintPrefix
	movq	stdout(%rip), %rsi
	movl	$93, %edi
.LBB35_21:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.LBB35_1:                               # %.preheader34
	testl	%r14d, %r14d
	movl	%r15d, %eax
	jle	.LBB35_5
# BB#2:                                 # %.lr.ph38.preheader
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB35_3:                               # %.lr.ph38
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.73, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	decl	%ebp
	jne	.LBB35_3
# BB#4:                                 # %._crit_edge39.loopexit
	movl	(%rbx), %eax
.LBB35_5:                               # %._crit_edge39
	testl	%eax, %eax
	jns	.LBB35_7
# BB#6:                                 # %symbol_IsPredicate.exit6.i
	movl	%eax, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	je	.LBB35_25
.LBB35_7:                               # %symbol_IsPredicate.exit6.thread.i
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB35_10
# BB#8:
	movq	16(%rbx), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jns	.LBB35_10
# BB#9:                                 # %fol_IsLiteral.exit
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	je	.LBB35_25
.LBB35_10:                              # %fol_IsLiteral.exit.thread
	testl	%r15d, %r15d
	jns	.LBB35_25
# BB#11:                                # %symbol_IsJunctor.exit
	movl	%r15d, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$3, %ecx
	jne	.LBB35_25
# BB#12:
	cmpq	$0, 16(%rbx)
	je	.LBB35_22
# BB#13:
	movl	%r15d, %edi
	callq	symbol_Print
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	cmpl	%r15d, fol_ALL(%rip)
	je	.LBB35_16
# BB#14:
	cmpl	%r15d, fol_EXIST(%rip)
	je	.LBB35_16
# BB#15:
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
.LBB35_16:                              # %.preheader
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB35_20
# BB#17:                                # %.lr.ph
	incl	%r14d
	.p2align	4, 0x90
.LBB35_18:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	%r14d, %esi
	callq	fol_PrettyPrintInternDFG
	cmpq	$0, (%rbx)
	je	.LBB35_20
# BB#19:                                # %.backedge
                                        #   in Loop: Header=BB35_18 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.74, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB35_18
	jmp	.LBB35_20
.LBB35_25:                              # %fol_IsLiteral.exit.thread32
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	term_PrintPrefix        # TAILCALL
.LBB35_22:
	testl	%eax, %eax
	jle	.LBB35_23
# BB#26:
	movl	%r15d, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	symbol_Print            # TAILCALL
.LBB35_23:
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	movl	%r15d, %edi
	callq	symbol_Print
.LBB35_20:                              # %._crit_edge
	movq	stdout(%rip), %rsi
	movl	$41, %edi
	jmp	.LBB35_21
.Lfunc_end35:
	.size	fol_PrettyPrintInternDFG, .Lfunc_end35-fol_PrettyPrintInternDFG
	.cfi_endproc

	.globl	fol_CheckFatherLinksIntern
	.p2align	4, 0x90
	.type	fol_CheckFatherLinksIntern,@function
fol_CheckFatherLinksIntern:             # @fol_CheckFatherLinksIntern
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi233:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi234:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi235:
	.cfi_def_cfa_offset 32
.Lcfi236:
	.cfi_offset %rbx, -24
.Lcfi237:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	(%r14), %edx
	movl	fol_ALL(%rip), %eax
	cmpl	%edx, %eax
	movl	fol_EXIST(%rip), %ecx
	movq	16(%r14), %rbx
	jne	.LBB36_2
	.p2align	4, 0x90
.LBB36_3:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movq	8(%rdx), %r14
	movl	(%r14), %edx
	cmpl	%edx, %eax
	movq	16(%r14), %rbx
	je	.LBB36_3
.LBB36_2:
	cmpl	%edx, %ecx
	je	.LBB36_3
	jmp	.LBB36_4
	.p2align	4, 0x90
.LBB36_7:                               #   in Loop: Header=BB36_4 Depth=1
	movq	(%rbx), %rbx
.LBB36_4:                               # %tailrecurse._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	je	.LBB36_8
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB36_4 Depth=1
	movq	8(%rbx), %rax
	cmpq	%r14, 8(%rax)
	jne	.LBB36_9
# BB#6:                                 #   in Loop: Header=BB36_4 Depth=1
	movq	%rax, %rdi
	callq	fol_CheckFatherLinksIntern
	testq	%rax, %rax
	je	.LBB36_7
	jmp	.LBB36_9
.LBB36_8:
	xorl	%eax, %eax
.LBB36_9:                               # %.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end36:
	.size	fol_CheckFatherLinksIntern, .Lfunc_end36-fol_CheckFatherLinksIntern
	.cfi_endproc

	.globl	fol_CheckFatherLinks
	.p2align	4, 0x90
	.type	fol_CheckFatherLinks,@function
fol_CheckFatherLinks:                   # @fol_CheckFatherLinks
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end37:
	.size	fol_CheckFatherLinks, .Lfunc_end37-fol_CheckFatherLinks
	.cfi_endproc

	.globl	fol_PrettyPrint
	.p2align	4, 0x90
	.type	fol_PrettyPrint,@function
fol_PrettyPrint:                        # @fol_PrettyPrint
	.cfi_startproc
# BB#0:
	xorl	%esi, %esi
	jmp	fol_PrettyPrintIntern   # TAILCALL
.Lfunc_end38:
	.size	fol_PrettyPrint, .Lfunc_end38-fol_PrettyPrint
	.cfi_endproc

	.p2align	4, 0x90
	.type	fol_PrettyPrintIntern,@function
fol_PrettyPrintIntern:                  # @fol_PrettyPrintIntern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi238:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi239:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi240:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi241:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi242:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi243:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi244:
	.cfi_def_cfa_offset 64
.Lcfi245:
	.cfi_offset %rbx, -56
.Lcfi246:
	.cfi_offset %r12, -48
.Lcfi247:
	.cfi_offset %r13, -40
.Lcfi248:
	.cfi_offset %r14, -32
.Lcfi249:
	.cfi_offset %r15, -24
.Lcfi250:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	movl	symbol_TYPEMASK(%rip), %r15d
	testl	%ebx, %ebx
	jg	.LBB39_2
	jmp	.LBB39_4
	.p2align	4, 0x90
.LBB39_13:                              # %._crit_edge61
                                        #   in Loop: Header=BB39_4 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.75, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r14
	incl	%ebx
	testl	%ebx, %ebx
	jle	.LBB39_4
.LBB39_2:                               # %.lr.ph55.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB39_3:                               # %.lr.ph55
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.73, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	incl	%ebp
	cmpl	%ebp, %ebx
	jne	.LBB39_3
.LBB39_4:                               # %._crit_edge56
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_11 Depth 2
	movl	(%r14), %ebp
	testl	%ebp, %ebp
	jns	.LBB39_20
# BB#5:                                 # %symbol_IsJunctor.exit
                                        #   in Loop: Header=BB39_4 Depth=1
	movl	%ebp, %eax
	negl	%eax
	andl	%r15d, %eax
	cmpl	$3, %eax
	jne	.LBB39_20
# BB#6:                                 #   in Loop: Header=BB39_4 Depth=1
	cmpq	$0, 16(%r14)
	je	.LBB39_19
# BB#7:                                 #   in Loop: Header=BB39_4 Depth=1
	movl	fol_ALL(%rip), %r13d
	movl	fol_EXIST(%rip), %r12d
	movl	%ebp, %edi
	callq	symbol_Print
	cmpl	%ebp, %r13d
	movq	stdout(%rip), %rcx
	je	.LBB39_9
# BB#8:                                 #   in Loop: Header=BB39_4 Depth=1
	cmpl	%ebp, %r12d
	jne	.LBB39_14
.LBB39_9:                               #   in Loop: Header=BB39_4 Depth=1
	movl	$.L.str.42, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	16(%r14), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rbp
	testq	%rbp, %rbp
	jne	.LBB39_11
	jmp	.LBB39_13
	.p2align	4, 0x90
.LBB39_12:                              # %.backedge43
                                        #   in Loop: Header=BB39_11 Depth=2
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB39_13
.LBB39_11:                              # %.lr.ph60
                                        #   Parent Loop BB39_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rax
	movl	(%rax), %edi
	callq	symbol_Print
	cmpq	$0, (%rbp)
	jne	.LBB39_12
	jmp	.LBB39_13
.LBB39_20:                              # %symbol_IsJunctor.exit.thread
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	term_PrintPrefix        # TAILCALL
.LBB39_19:
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	movl	(%r14), %edi
	callq	symbol_Print
	jmp	.LBB39_18
.LBB39_14:
	movl	$.L.str.76, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	16(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB39_18
# BB#15:                                # %.lr.ph
	incl	%ebx
	.p2align	4, 0x90
.LBB39_16:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movl	%ebx, %esi
	callq	fol_PrettyPrintIntern
	cmpq	$0, (%rbp)
	je	.LBB39_18
# BB#17:                                # %.backedge
                                        #   in Loop: Header=BB39_16 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.74, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB39_16
.LBB39_18:                              # %._crit_edge
	movq	stdout(%rip), %rsi
	movl	$41, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.Lfunc_end39:
	.size	fol_PrettyPrintIntern, .Lfunc_end39-fol_PrettyPrintIntern
	.cfi_endproc

	.globl	fol_GetSubstEquations
	.p2align	4, 0x90
	.type	fol_GetSubstEquations,@function
fol_GetSubstEquations:                  # @fol_GetSubstEquations
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi251:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi252:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi253:
	.cfi_def_cfa_offset 32
.Lcfi254:
	.cfi_offset %rbx, -24
.Lcfi255:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	movl	fol_ALL(%rip), %ecx
	cmpl	%eax, %ecx
	movl	fol_EXIST(%rip), %edx
	je	.LBB40_2
.LBB40_1:
	cmpl	%eax, %edx
	jne	.LBB40_3
.LBB40_2:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbx
	movl	(%rbx), %eax
	cmpl	%eax, %ecx
	jne	.LBB40_1
	jmp	.LBB40_2
.LBB40_3:                               # %tailrecurse._crit_edge
	cmpl	%eax, fol_EQUALITY(%rip)
	jne	.LBB40_7
# BB#4:
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	8(%rcx), %rdi
	movl	(%rdi), %esi
	testl	%esi, %esi
	jle	.LBB40_8
# BB#5:
	movq	%rax, %rdi
	jmp	.LBB40_6
.LBB40_8:
	movl	(%rax), %esi
	testl	%esi, %esi
	jle	.LBB40_7
.LBB40_6:
	callq	term_ContainsSymbol
	testl	%eax, %eax
	je	.LBB40_10
.LBB40_7:
	xorl	%r14d, %r14d
.LBB40_11:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jns	.LBB40_13
# BB#12:                                # %symbol_IsPredicate.exit
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	je	.LBB40_21
.LBB40_13:                              # %symbol_IsPredicate.exit.thread
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB40_21
	.p2align	4, 0x90
.LBB40_15:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_18 Depth 2
	movq	8(%rbx), %rdi
	callq	fol_GetSubstEquations
	testq	%r14, %r14
	je	.LBB40_20
# BB#16:                                #   in Loop: Header=BB40_15 Depth=1
	testq	%rax, %rax
	je	.LBB40_14
# BB#17:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB40_15 Depth=1
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB40_18:                              # %.preheader.i
                                        #   Parent Loop BB40_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB40_18
# BB#19:                                #   in Loop: Header=BB40_15 Depth=1
	movq	%rax, (%rcx)
.LBB40_14:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB40_15 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB40_15
	jmp	.LBB40_21
	.p2align	4, 0x90
.LBB40_20:                              #   in Loop: Header=BB40_15 Depth=1
	movq	%rax, %r14
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB40_15
.LBB40_21:                              # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB40_10:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbx, 8(%r14)
	movq	$0, (%r14)
	jmp	.LBB40_11
.Lfunc_end40:
	.size	fol_GetSubstEquations, .Lfunc_end40-fol_GetSubstEquations
	.cfi_endproc

	.globl	fol_GetBindingQuantifier
	.p2align	4, 0x90
	.type	fol_GetBindingQuantifier,@function
fol_GetBindingQuantifier:               # @fol_GetBindingQuantifier
	.cfi_startproc
# BB#0:
	movl	fol_ALL(%rip), %eax
	movl	fol_EXIST(%rip), %r8d
	jmp	.LBB41_1
	.p2align	4, 0x90
.LBB41_8:                               # %.loopexit
                                        #   in Loop: Header=BB41_1 Depth=1
	movq	8(%rdi), %rdi
.LBB41_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB41_6 Depth 2
	movl	(%rdi), %edx
	cmpl	%edx, %eax
	je	.LBB41_3
# BB#2:                                 # %tailrecurse
                                        #   in Loop: Header=BB41_1 Depth=1
	cmpl	%edx, %r8d
	jne	.LBB41_8
.LBB41_3:                               #   in Loop: Header=BB41_1 Depth=1
	movq	16(%rdi), %rdx
	movq	8(%rdx), %rdx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB41_6
	jmp	.LBB41_8
	.p2align	4, 0x90
.LBB41_4:                               #   in Loop: Header=BB41_6 Depth=2
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB41_8
.LBB41_6:                               # %.lr.ph
                                        #   Parent Loop BB41_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdx), %rcx
	cmpl	%esi, (%rcx)
	jne	.LBB41_4
# BB#7:
	movq	%rdi, %rax
	retq
.Lfunc_end41:
	.size	fol_GetBindingQuantifier, .Lfunc_end41-fol_GetBindingQuantifier
	.cfi_endproc

	.globl	fol_TermPolarity
	.p2align	4, 0x90
	.type	fol_TermPolarity,@function
fol_TermPolarity:                       # @fol_TermPolarity
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi256:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi257:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi258:
	.cfi_def_cfa_offset 32
.Lcfi259:
	.cfi_offset %rbx, -24
.Lcfi260:
	.cfi_offset %r14, -16
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.LBB42_14
# BB#1:                                 # %.lr.ph
	movl	fol_AND(%rip), %edx
	movl	fol_OR(%rip), %r8d
	movl	fol_ALL(%rip), %r9d
	movl	fol_EXIST(%rip), %r10d
	.p2align	4, 0x90
.LBB42_2:                               # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB42_14
# BB#3:                                 #   in Loop: Header=BB42_2 Depth=1
	movl	(%rdi), %ecx
	cmpl	%edx, %ecx
	je	.LBB42_7
# BB#4:                                 #   in Loop: Header=BB42_2 Depth=1
	cmpl	%r8d, %ecx
	je	.LBB42_7
# BB#5:                                 #   in Loop: Header=BB42_2 Depth=1
	cmpl	%ecx, %r9d
	je	.LBB42_7
# BB#6:                                 #   in Loop: Header=BB42_2 Depth=1
	cmpl	%ecx, %r10d
	jne	.LBB42_8
.LBB42_7:                               # %tailrecurse.backedge
                                        #   in Loop: Header=BB42_2 Depth=1
	cmpq	%rsi, %rdi
	jne	.LBB42_2
	jmp	.LBB42_14
.LBB42_8:
	cmpl	fol_NOT(%rip), %ecx
	jne	.LBB42_10
# BB#9:
	callq	fol_TermPolarity
	negl	%eax
	jmp	.LBB42_14
.LBB42_10:
	xorl	%eax, %eax
	cmpl	fol_EQUIV(%rip), %ecx
	je	.LBB42_14
# BB#11:
	cmpl	fol_IMPLIES(%rip), %ecx
	jne	.LBB42_15
# BB#12:
	movq	16(%rdi), %rax
	jmp	.LBB42_13
.LBB42_15:
	cmpl	fol_IMPLIED(%rip), %ecx
	jne	.LBB42_17
# BB#16:
	movq	16(%rdi), %rax
	movq	(%rax), %rax
.LBB42_13:
	movq	8(%rax), %r14
	callq	fol_TermPolarity
	movl	%eax, %ecx
	negl	%ecx
	cmpq	%rbx, %r14
	cmovnel	%eax, %ecx
	movl	%ecx, %eax
.LBB42_14:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB42_17:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.26, %esi
	movl	$.L.str.27, %edx
	movl	$1527, %ecx             # imm = 0x5F7
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.58, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.29, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end42:
	.size	fol_TermPolarity, .Lfunc_end42-fol_TermPolarity
	.cfi_endproc

	.globl	fol_PolarCheck
	.p2align	4, 0x90
	.type	fol_PolarCheck,@function
fol_PolarCheck:                         # @fol_PolarCheck
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi261:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi262:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi263:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi264:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi265:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi266:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi267:
	.cfi_def_cfa_offset 64
.Lcfi268:
	.cfi_offset %rbx, -56
.Lcfi269:
	.cfi_offset %r12, -48
.Lcfi270:
	.cfi_offset %r13, -40
.Lcfi271:
	.cfi_offset %r14, -32
.Lcfi272:
	.cfi_offset %r15, -24
.Lcfi273:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	fol_TermPolarity
	movl	%eax, %ecx
	movl	(%r14), %r12d
	xorl	%eax, %eax
	cmpl	$1, %ecx
	je	.LBB43_24
# BB#1:
	cmpl	$-1, %ecx
	jne	.LBB43_48
# BB#2:
	cmpl	fol_ALL(%rip), %r12d
	jne	.LBB43_48
# BB#3:
	movl	$1, %eax
	cmpq	%r14, %rbx
	je	.LBB43_48
# BB#4:                                 # %.lr.ph.i16
	movq	8(%rbx), %rdx
	cmpq	%r14, %rdx
	je	.LBB43_48
# BB#5:                                 # %.lr.ph.preheader
	movl	fol_OR(%rip), %r15d
	movl	fol_AND(%rip), %esi
	movl	fol_NOT(%rip), %r13d
	movl	fol_IMPLIES(%rip), %r8d
	movl	fol_IMPLIED(%rip), %r9d
	movl	fol_EXIST(%rip), %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	movl	fol_EQUIV(%rip), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB43_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%rdx, %rbx
	movl	(%rbx), %edx
	cmpl	%esi, %edx
	je	.LBB43_16
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB43_6 Depth=1
	cmpq	%rdi, %rbx
	je	.LBB43_16
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB43_6 Depth=1
	cmpl	%r15d, %edx
	je	.LBB43_16
# BB#9:                                 #   in Loop: Header=BB43_6 Depth=1
	cmpl	%edx, %r12d
	je	.LBB43_16
# BB#10:                                #   in Loop: Header=BB43_6 Depth=1
	cmpl	%edx, (%rsp)            # 4-byte Folded Reload
	je	.LBB43_16
# BB#11:                                #   in Loop: Header=BB43_6 Depth=1
	cmpl	%r8d, %edx
	jne	.LBB43_13
# BB#12:                                #   in Loop: Header=BB43_6 Depth=1
	movq	16(%rbx), %rcx
	movq	(%rcx), %rcx
	cmpq	%rdi, 8(%rcx)
	je	.LBB43_16
.LBB43_13:                              #   in Loop: Header=BB43_6 Depth=1
	cmpl	%r9d, %edx
	jne	.LBB43_15
# BB#14:                                #   in Loop: Header=BB43_6 Depth=1
	movq	16(%rbx), %rcx
	cmpq	%rdi, 8(%rcx)
	je	.LBB43_16
.LBB43_15:                              #   in Loop: Header=BB43_6 Depth=1
	negl	%ebp
	cmpl	4(%rsp), %edx           # 4-byte Folded Reload
	movl	$0, %ecx
	cmovel	%ecx, %ebp
	.p2align	4, 0x90
.LBB43_16:                              # %fol_PolarCheckCount.exit.i36
                                        #   in Loop: Header=BB43_6 Depth=1
	cmpl	%r13d, %edx
	jne	.LBB43_19
.LBB43_17:                              #   in Loop: Header=BB43_6 Depth=1
	movl	%ebp, %edi
	jmp	.LBB43_18
	.p2align	4, 0x90
.LBB43_19:                              #   in Loop: Header=BB43_6 Depth=1
	cmpl	%r15d, %edx
	jne	.LBB43_21
# BB#20:                                #   in Loop: Header=BB43_6 Depth=1
	movl	$1, %edi
	cmpl	$1, %ebp
	jne	.LBB43_21
.LBB43_18:                              # %tailrecurse.backedge.i19.backedge
                                        #   in Loop: Header=BB43_6 Depth=1
	movq	8(%rbx), %rdx
	cmpq	%r14, %rdx
	movl	%edi, %ebp
	jne	.LBB43_6
	jmp	.LBB43_48
	.p2align	4, 0x90
.LBB43_21:                              #   in Loop: Header=BB43_6 Depth=1
	cmpl	$1, %ebp
	sete	%dil
	cmpl	%esi, %edx
	sete	%cl
	cmpl	$-1, %ebp
	sete	%r10b
	cmpl	%r8d, %edx
	sete	%r11b
	cmpl	%r9d, %edx
	sete	%dl
	testb	%dil, %dl
	jne	.LBB43_17
# BB#22:                                #   in Loop: Header=BB43_6 Depth=1
	andb	%r10b, %cl
	jne	.LBB43_17
# BB#23:                                #   in Loop: Header=BB43_6 Depth=1
	andb	%dil, %r11b
	movl	%ebp, %edi
	jne	.LBB43_18
	jmp	.LBB43_47
.LBB43_24:
	cmpl	fol_EXIST(%rip), %r12d
	jne	.LBB43_48
# BB#25:
	movl	$1, %eax
	cmpq	%r14, %rbx
	je	.LBB43_48
# BB#26:                                # %.lr.ph.i
	movq	8(%rbx), %rdx
	cmpq	%r14, %rdx
	je	.LBB43_48
# BB#27:                                # %.lr.ph51.preheader
	movl	fol_OR(%rip), %esi
	movl	fol_AND(%rip), %r11d
	movl	fol_NOT(%rip), %r13d
	movl	fol_IMPLIES(%rip), %r9d
	movl	fol_IMPLIED(%rip), %r8d
	movl	fol_ALL(%rip), %r15d
	movl	fol_EQUIV(%rip), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB43_28:                              # %.lr.ph51
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%rdx, %rbx
	movl	(%rbx), %edx
	cmpl	%r11d, %edx
	je	.LBB43_38
# BB#29:                                # %.lr.ph51
                                        #   in Loop: Header=BB43_28 Depth=1
	cmpq	%rdi, %rbx
	je	.LBB43_38
# BB#30:                                # %.lr.ph51
                                        #   in Loop: Header=BB43_28 Depth=1
	cmpl	%esi, %edx
	je	.LBB43_38
# BB#31:                                #   in Loop: Header=BB43_28 Depth=1
	cmpl	%edx, %r15d
	je	.LBB43_38
# BB#32:                                #   in Loop: Header=BB43_28 Depth=1
	cmpl	%edx, %r12d
	je	.LBB43_38
# BB#33:                                #   in Loop: Header=BB43_28 Depth=1
	cmpl	%r9d, %edx
	jne	.LBB43_35
# BB#34:                                #   in Loop: Header=BB43_28 Depth=1
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	cmpq	%rdi, 8(%rax)
	je	.LBB43_38
.LBB43_35:                              #   in Loop: Header=BB43_28 Depth=1
	cmpl	%r8d, %edx
	jne	.LBB43_37
# BB#36:                                #   in Loop: Header=BB43_28 Depth=1
	movq	16(%rbx), %rax
	cmpq	%rdi, 8(%rax)
	je	.LBB43_38
.LBB43_37:                              #   in Loop: Header=BB43_28 Depth=1
	negl	%ebp
	cmpl	(%rsp), %edx            # 4-byte Folded Reload
	movl	$0, %eax
	cmovel	%eax, %ebp
	.p2align	4, 0x90
.LBB43_38:                              # %fol_PolarCheckCount.exit.i
                                        #   in Loop: Header=BB43_28 Depth=1
	cmpl	%r13d, %edx
	jne	.LBB43_42
.LBB43_39:                              #   in Loop: Header=BB43_28 Depth=1
	movl	%ebp, %edi
	jmp	.LBB43_40
	.p2align	4, 0x90
.LBB43_42:                              #   in Loop: Header=BB43_28 Depth=1
	cmpl	%esi, %edx
	jne	.LBB43_44
# BB#43:                                #   in Loop: Header=BB43_28 Depth=1
	movl	$-1, %edi
	cmpl	$-1, %ebp
	jne	.LBB43_44
.LBB43_40:                              # %tailrecurse.backedge.i.backedge
                                        #   in Loop: Header=BB43_28 Depth=1
	movq	8(%rbx), %rdx
	cmpq	%r14, %rdx
	movl	%edi, %ebp
	jne	.LBB43_28
	jmp	.LBB43_41
	.p2align	4, 0x90
.LBB43_44:                              #   in Loop: Header=BB43_28 Depth=1
	cmpl	$-1, %ebp
	sete	%al
	cmpl	%r11d, %edx
	sete	%cl
	cmpl	$1, %ebp
	sete	%r10b
	cmpl	%r9d, %edx
	sete	%dil
	cmpl	%r8d, %edx
	sete	%dl
	testb	%al, %dl
	jne	.LBB43_39
# BB#45:                                #   in Loop: Header=BB43_28 Depth=1
	andb	%r10b, %cl
	jne	.LBB43_39
# BB#46:                                #   in Loop: Header=BB43_28 Depth=1
	andb	%al, %dil
	movl	%ebp, %edi
	jne	.LBB43_40
.LBB43_47:
	xorl	%eax, %eax
	jmp	.LBB43_48
.LBB43_41:
	movl	$1, %eax
.LBB43_48:                              # %fol_PolarCheckAllquantor.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end43:
	.size	fol_PolarCheck, .Lfunc_end43-fol_PolarCheck
	.cfi_endproc

	.globl	fol_PopQuantifier
	.p2align	4, 0x90
	.type	fol_PopQuantifier,@function
fol_PopQuantifier:                      # @fol_PopQuantifier
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi274:
	.cfi_def_cfa_offset 16
.Lcfi275:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	callq	term_Delete
	movq	16(%rbx), %rdx
	movq	(%rdx), %rcx
	movq	8(%rcx), %rax
	testq	%rdx, %rdx
	je	.LBB44_3
# BB#1:                                 # %.lr.ph.i.preheader
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	je	.LBB44_3
	.p2align	4, 0x90
.LBB44_2:                               # %.lr.ph.i..lr.ph.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB44_2
.LBB44_3:                               # %list_Delete.exit
	movl	(%rax), %ecx
	movl	%ecx, (%rbx)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rbx)
	testq	%rcx, %rcx
	jne	.LBB44_5
	jmp	.LBB44_8
	.p2align	4, 0x90
.LBB44_7:                               #   in Loop: Header=BB44_5 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB44_8
.LBB44_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	cmpq	$0, 8(%rdx)
	je	.LBB44_7
# BB#6:                                 #   in Loop: Header=BB44_5 Depth=1
	movq	%rbx, 8(%rdx)
	jmp	.LBB44_7
.LBB44_8:                               # %._crit_edge
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
	popq	%rbx
	retq
.Lfunc_end44:
	.size	fol_PopQuantifier, .Lfunc_end44-fol_PopQuantifier
	.cfi_endproc

	.globl	fol_DeleteQuantifierVariable
	.p2align	4, 0x90
	.type	fol_DeleteQuantifierVariable,@function
fol_DeleteQuantifierVariable:           # @fol_DeleteQuantifierVariable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi276:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi277:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi278:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi279:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi280:
	.cfi_def_cfa_offset 48
.Lcfi281:
	.cfi_offset %rbx, -40
.Lcfi282:
	.cfi_offset %r14, -32
.Lcfi283:
	.cfi_offset %r15, -24
.Lcfi284:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movq	16(%r14), %rax
	movq	8(%rax), %r15
	movq	16(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB45_1
	.p2align	4, 0x90
.LBB45_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	cmpl	%ebp, (%rdi)
	jne	.LBB45_4
# BB#3:                                 #   in Loop: Header=BB45_2 Depth=1
	callq	term_Delete
	movq	$0, 8(%rbx)
.LBB45_4:                               #   in Loop: Header=BB45_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB45_2
# BB#5:                                 # %._crit_edge.loopexit
	movq	16(%r14), %rax
	movq	8(%rax), %r15
	movq	16(%r15), %rdi
	jmp	.LBB45_6
.LBB45_1:
	xorl	%edi, %edi
.LBB45_6:                               # %._crit_edge
	xorl	%esi, %esi
	callq	list_PointerDeleteElement
	movq	%rax, 16(%r15)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	cmpq	$0, 16(%rdi)
	jne	.LBB45_16
# BB#7:
	callq	term_Delete
	movq	16(%r14), %rdx
	movq	(%rdx), %rcx
	movq	8(%rcx), %rax
	testq	%rdx, %rdx
	je	.LBB45_10
# BB#8:                                 # %.lr.ph.i.i.preheader
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	testq	%rcx, %rcx
	je	.LBB45_10
	.p2align	4, 0x90
.LBB45_9:                               # %.lr.ph.i..lr.ph.i_crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB45_9
.LBB45_10:                              # %list_Delete.exit.i
	movl	(%rax), %ecx
	movl	%ecx, (%r14)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r14)
	testq	%rcx, %rcx
	jne	.LBB45_12
	jmp	.LBB45_15
	.p2align	4, 0x90
.LBB45_14:                              #   in Loop: Header=BB45_12 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB45_15
.LBB45_12:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	cmpq	$0, 8(%rdx)
	je	.LBB45_14
# BB#13:                                #   in Loop: Header=BB45_12 Depth=1
	movq	%r14, 8(%rdx)
	jmp	.LBB45_14
.LBB45_15:                              # %fol_PopQuantifier.exit
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
.LBB45_16:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end45:
	.size	fol_DeleteQuantifierVariable, .Lfunc_end45-fol_DeleteQuantifierVariable
	.cfi_endproc

	.globl	fol_SetTrue
	.p2align	4, 0x90
	.type	fol_SetTrue,@function
fol_SetTrue:                            # @fol_SetTrue
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi285:
	.cfi_def_cfa_offset 16
.Lcfi286:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	$0, 16(%rbx)
	movl	fol_TRUE(%rip), %eax
	movl	%eax, (%rbx)
	popq	%rbx
	retq
.Lfunc_end46:
	.size	fol_SetTrue, .Lfunc_end46-fol_SetTrue
	.cfi_endproc

	.globl	fol_SetFalse
	.p2align	4, 0x90
	.type	fol_SetFalse,@function
fol_SetFalse:                           # @fol_SetFalse
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi287:
	.cfi_def_cfa_offset 16
.Lcfi288:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	$0, 16(%rbx)
	movl	fol_FALSE(%rip), %eax
	movl	%eax, (%rbx)
	popq	%rbx
	retq
.Lfunc_end47:
	.size	fol_SetFalse, .Lfunc_end47-fol_SetFalse
	.cfi_endproc

	.globl	fol_PropagateFreeness
	.p2align	4, 0x90
	.type	fol_PropagateFreeness,@function
fol_PropagateFreeness:                  # @fol_PropagateFreeness
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi289:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi290:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi291:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi292:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi293:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi294:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi295:
	.cfi_def_cfa_offset 80
.Lcfi296:
	.cfi_offset %rbx, -56
.Lcfi297:
	.cfi_offset %r12, -48
.Lcfi298:
	.cfi_offset %r13, -40
.Lcfi299:
	.cfi_offset %r14, -32
.Lcfi300:
	.cfi_offset %r15, -24
.Lcfi301:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	movl	fol_ALL(%rip), %ecx
	cmpl	%eax, %ecx
	movl	fol_EXIST(%rip), %edx
	je	.LBB48_2
.LBB48_1:
	cmpl	%eax, %edx
	jne	.LBB48_3
.LBB48_2:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbx
	movl	(%rbx), %eax
	cmpl	%eax, %ecx
	jne	.LBB48_1
	jmp	.LBB48_2
.LBB48_3:                               # %tailrecurse._crit_edge
	testl	%eax, %eax
	jns	.LBB48_5
# BB#4:                                 # %term_IsAtom.exit
	movl	%eax, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	jne	.LBB48_5
# BB#8:
	xorl	%ebp, %ebp
	cmpl	%eax, fol_EQUALITY(%rip)
	jne	.LBB48_20
# BB#9:
	movq	16(%rbx), %r12
	movq	(%r12), %rax
	movq	8(%r12), %rdi
	movq	8(%rax), %r14
	movl	(%rdi), %eax
	cmpl	(%r14), %eax
	jne	.LBB48_20
# BB#10:
	negl	%eax
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movl	$768, %ecx              # imm = 0x300
	andl	20(%rax), %ecx
	cmpl	$768, %ecx              # imm = 0x300
	jne	.LBB48_20
# BB#11:
	movq	$0, 16(%rbx)
	movl	fol_AND(%rip), %eax
	movl	%eax, (%rbx)
	leaq	16(%r14), %rbp
	movq	16(%rdi), %r13
	testq	%r13, %r13
	je	.LBB48_16
# BB#12:                                # %.lr.ph.i
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rbp, %r14
	.p2align	4, 0x90
.LBB48_13:                              # =>This Inner Loop Header: Depth=1
	movq	(%r14), %r14
	movq	8(%r14), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r15, 8(%rbp)
	movq	$0, (%rbp)
	movq	8(%r13), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%rbp, (%rax)
	movl	fol_EQUALITY(%rip), %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbp
	movq	16(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 16(%rbx)
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB48_13
# BB#14:                                # %._crit_edge.i
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rdi), %rax
	testq	%rax, %rax
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	je	.LBB48_16
	.p2align	4, 0x90
.LBB48_15:                              # %.lr.ph.i56.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB48_15
.LBB48_16:                              # %list_Delete.exit57.i
	movq	(%rbp), %rax
	testq	%rax, %rax
	je	.LBB48_18
	.p2align	4, 0x90
.LBB48_17:                              # %.lr.ph.i51.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB48_17
.LBB48_18:                              # %list_Delete.exit52.i
	movq	$0, 16(%rdi)
	movq	$0, 16(%r14)
	callq	term_Delete
	movq	%r14, %rdi
	callq	term_Delete
	movl	$1, %ebp
	testq	%r12, %r12
	je	.LBB48_20
	.p2align	4, 0x90
.LBB48_19:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB48_19
	jmp	.LBB48_20
.LBB48_5:                               # %term_IsAtom.exit.thread
	movq	16(%rbx), %rbx
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	je	.LBB48_20
# BB#6:                                 # %.lr.ph.preheader
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB48_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	fol_PropagateFreeness
	testl	%eax, %eax
	cmovnel	%r14d, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB48_7
.LBB48_20:                              # %fol_ReplaceByArgCon.exit
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end48:
	.size	fol_PropagateFreeness, .Lfunc_end48-fol_PropagateFreeness
	.cfi_endproc

	.globl	fol_PropagateWitness
	.p2align	4, 0x90
	.type	fol_PropagateWitness,@function
fol_PropagateWitness:                   # @fol_PropagateWitness
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi302:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi303:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi304:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi305:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi306:
	.cfi_def_cfa_offset 48
.Lcfi307:
	.cfi_offset %rbx, -40
.Lcfi308:
	.cfi_offset %r14, -32
.Lcfi309:
	.cfi_offset %r15, -24
.Lcfi310:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	movl	fol_ALL(%rip), %ecx
	cmpl	%eax, %ecx
	movl	fol_EXIST(%rip), %edx
	je	.LBB49_2
.LBB49_1:
	cmpl	%eax, %edx
	jne	.LBB49_3
.LBB49_2:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbx
	movl	(%rbx), %eax
	cmpl	%eax, %ecx
	jne	.LBB49_1
	jmp	.LBB49_2
.LBB49_3:                               # %tailrecurse._crit_edge
	xorl	%r14d, %r14d
	cmpl	%eax, fol_EQUALITY(%rip)
	jne	.LBB49_11
# BB#4:
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	8(%rcx), %rdi
	movl	(%rdi), %esi
	testl	%esi, %esi
	jle	.LBB49_7
# BB#5:
	movq	%rax, %rdi
	callq	term_ContainsSymbol
	testl	%eax, %eax
	jne	.LBB49_11
# BB#6:
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	jmp	.LBB49_10
.LBB49_7:
	movl	(%rax), %esi
	testl	%esi, %esi
	jle	.LBB49_11
# BB#8:
	callq	term_ContainsSymbol
	testl	%eax, %eax
	jne	.LBB49_11
# BB#9:
	movq	16(%rbx), %rax
.LBB49_10:
	movq	8(%rax), %rax
	movl	(%rax), %esi
	movq	%rbx, %rdi
	callq	fol_PropagateWitnessIntern
	movl	%eax, %r14d
.LBB49_11:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jns	.LBB49_13
# BB#12:                                # %symbol_IsPredicate.exit
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	xorl	%ebp, %ebp
	cmpl	$2, %eax
	je	.LBB49_17
.LBB49_13:                              # %symbol_IsPredicate.exit.thread
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB49_14
# BB#15:                                # %.lr.ph.preheader
	movl	$1, %r15d
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB49_16:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	fol_PropagateWitness
	testl	%eax, %eax
	cmovnel	%r15d, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB49_16
	jmp	.LBB49_17
.LBB49_14:
	movl	%r14d, %ebp
.LBB49_17:                              # %.loopexit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end49:
	.size	fol_PropagateWitness, .Lfunc_end49-fol_PropagateWitness
	.cfi_endproc

	.p2align	4, 0x90
	.type	fol_PropagateWitnessIntern,@function
fol_PropagateWitnessIntern:             # @fol_PropagateWitnessIntern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi311:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi312:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi313:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi314:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi315:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi316:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi317:
	.cfi_def_cfa_offset 64
.Lcfi318:
	.cfi_offset %rbx, -56
.Lcfi319:
	.cfi_offset %r12, -48
.Lcfi320:
	.cfi_offset %r13, -40
.Lcfi321:
	.cfi_offset %r14, -32
.Lcfi322:
	.cfi_offset %r15, -24
.Lcfi323:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	8(%rbx), %rbp
	xorl	%r13d, %r13d
	testq	%rbp, %rbp
	je	.LBB50_23
# BB#1:
	movq	8(%rbp), %r15
	testq	%r15, %r15
	je	.LBB50_23
# BB#2:
	movl	(%rbp), %r12d
	movl	(%r15), %eax
	cmpl	%eax, fol_ALL(%rip)
	je	.LBB50_4
# BB#3:
	cmpl	%eax, fol_EXIST(%rip)
	jne	.LBB50_23
.LBB50_4:
	movq	16(%rbp), %rdi
	callq	list_Length
	cmpl	$2, %eax
	jne	.LBB50_23
# BB#5:
	movq	16(%rbp), %r8
	movq	(%r8), %rax
	movq	8(%rax), %rdx
	cmpq	%rbx, %rdx
	movq	%rdx, %rsi
	jne	.LBB50_7
# BB#6:
	movq	8(%r8), %rsi
.LBB50_7:
	movl	(%r15), %r9d
	movl	fol_ALL(%rip), %r10d
	movl	(%rsi), %edi
	cmpl	%r10d, %r9d
	jne	.LBB50_13
# BB#8:
	cmpl	fol_OR(%rip), %r12d
	jne	.LBB50_13
# BB#9:
	cmpl	fol_NOT(%rip), %edi
	jne	.LBB50_13
# BB#10:
	movq	16(%rsi), %rcx
	movq	8(%rcx), %rbp
	xorl	%eax, %eax
	subl	(%rbp), %eax
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movl	$768, %ecx              # imm = 0x300
	andl	20(%rax), %ecx
	cmpl	$768, %ecx              # imm = 0x300
	jne	.LBB50_13
# BB#11:
	movq	16(%rbp), %rax
	movq	8(%rax), %rax
	cmpl	%r14d, (%rax)
	je	.LBB50_12
.LBB50_13:                              # %._crit_edge
	negl	%edi
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %edi
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	$768, %ecx              # imm = 0x300
	andl	20(%rax), %ecx
	cmpl	$768, %ecx              # imm = 0x300
	jne	.LBB50_23
# BB#14:
	movq	16(%rsi), %rax
	movq	8(%rax), %rax
	cmpl	%r14d, (%rax)
	jne	.LBB50_23
# BB#15:
	cmpl	%r10d, %r9d
	jne	.LBB50_20
# BB#16:
	cmpq	%rbx, %rdx
	setne	%al
	cmpl	fol_IMPLIES(%rip), %r12d
	jne	.LBB50_18
# BB#17:
	testb	%al, %al
	jne	.LBB50_18
.LBB50_12:
	movq	16(%r15), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	$0, 16(%r15)
	movl	fol_FALSE(%rip), %eax
	jmp	.LBB50_22
.LBB50_20:
	cmpl	fol_AND(%rip), %r12d
	jne	.LBB50_23
# BB#21:
	movq	16(%r15), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	$0, 16(%r15)
	movl	fol_TRUE(%rip), %eax
.LBB50_22:
	movl	%eax, (%r15)
	movl	$1, %r13d
.LBB50_23:
	movl	%r13d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB50_18:
	cmpl	fol_IMPLIED(%rip), %r12d
	jne	.LBB50_23
# BB#19:
	cmpq	%rbx, 8(%r8)
	jne	.LBB50_23
	jmp	.LBB50_12
.Lfunc_end50:
	.size	fol_PropagateWitnessIntern, .Lfunc_end50-fol_PropagateWitnessIntern
	.cfi_endproc

	.globl	fol_PropagateTautologies
	.p2align	4, 0x90
	.type	fol_PropagateTautologies,@function
fol_PropagateTautologies:               # @fol_PropagateTautologies
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi324:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi325:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi326:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi327:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi328:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi329:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi330:
	.cfi_def_cfa_offset 64
.Lcfi331:
	.cfi_offset %rbx, -56
.Lcfi332:
	.cfi_offset %r12, -48
.Lcfi333:
	.cfi_offset %r13, -40
.Lcfi334:
	.cfi_offset %r14, -32
.Lcfi335:
	.cfi_offset %r15, -24
.Lcfi336:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	(%rbx), %r13d
	movq	16(%rbx), %r14
	movl	fol_ALL(%rip), %eax
	cmpl	%r13d, %eax
	movl	fol_EXIST(%rip), %ecx
	je	.LBB51_3
# BB#1:
	cmpl	%r13d, %ecx
	je	.LBB51_3
# BB#2:
	movq	%rbx, %r15
	addq	$16, %r15
	cmpl	%r13d, fol_EQUALITY(%rip)
	je	.LBB51_7
	jmp	.LBB51_9
	.p2align	4, 0x90
.LBB51_3:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rdx
	movq	8(%rdx), %rbx
	movl	(%rbx), %r13d
	movq	16(%rbx), %r14
	cmpl	%r13d, %eax
	je	.LBB51_3
# BB#4:                                 # %tailrecurse
                                        #   in Loop: Header=BB51_3 Depth=1
	cmpl	%r13d, %ecx
	je	.LBB51_3
# BB#5:
	leaq	16(%rbx), %r15
	cmpl	%r13d, fol_EQUALITY(%rip)
	jne	.LBB51_9
.LBB51_7:
	movq	(%r14), %rax
	movq	8(%r14), %rdi
	movq	8(%rax), %rsi
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB51_9
# BB#8:
	movq	(%r15), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	$0, (%r15)
	jmp	.LBB51_21
.LBB51_9:
	cmpl	fol_OR(%rip), %r13d
	setne	%al
	cmpl	fol_AND(%rip), %r13d
	setne	%cl
	testb	%al, %cl
	jne	.LBB51_27
# BB#10:
	testq	%r14, %r14
	je	.LBB51_27
# BB#11:                                # %.lr.ph76.split.preheader
	movq	%r14, %r12
.LBB51_12:                              # %.lr.ph76.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB51_14 Depth 2
                                        #       Child Loop BB51_17 Depth 3
	movq	8(%r12), %rax
	movl	(%rax), %ecx
	cmpl	fol_NOT(%rip), %ecx
	jne	.LBB51_26
# BB#13:                                # %.lr.ph74.preheader
                                        #   in Loop: Header=BB51_12 Depth=1
	movq	%r14, %rbp
	jmp	.LBB51_14
	.p2align	4, 0x90
.LBB51_25:                              # %..lr.ph74_crit_edge
                                        #   in Loop: Header=BB51_14 Depth=2
	movq	8(%r12), %rax
.LBB51_14:                              # %.lr.ph74
                                        #   Parent Loop BB51_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB51_17 Depth 3
	movq	8(%rbp), %rsi
	cmpq	%rsi, %rax
	je	.LBB51_24
# BB#15:                                #   in Loop: Header=BB51_14 Depth=2
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movl	term_MARK(%rip), %edx
	cmpl	$-1, %edx
	jne	.LBB51_19
# BB#16:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB51_14 Depth=2
	movq	$-48000, %rax           # imm = 0xFFFF4480
	jmp	.LBB51_17
	.p2align	4, 0x90
.LBB51_35:                              # %.preheader.i.i.1
                                        #   in Loop: Header=BB51_17 Depth=3
	movq	$0, term_BIND+48016(%rax)
	movq	$0, term_BIND+48032(%rax)
	movq	$0, term_BIND+48048(%rax)
	movq	$0, term_BIND+48064(%rax)
	movq	$0, term_BIND+48080(%rax)
	movq	$0, term_BIND+48096(%rax)
	movq	$0, term_BIND+48112(%rax)
	subq	$-128, %rax
.LBB51_17:                              # %.preheader.i.i
                                        #   Parent Loop BB51_12 Depth=1
                                        #     Parent Loop BB51_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	$0, term_BIND+48000(%rax)
	testq	%rax, %rax
	jne	.LBB51_35
# BB#18:                                #   in Loop: Header=BB51_14 Depth=2
	movl	$1, term_MARK(%rip)
	movl	$1, %edx
.LBB51_19:                              # %fol_AlphaEqual.exit
                                        #   in Loop: Header=BB51_14 Depth=2
	leal	1(%rdx), %eax
	movl	%eax, term_MARK(%rip)
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fol_AlphaEqualIntern
	testl	%eax, %eax
	jne	.LBB51_20
.LBB51_24:                              #   in Loop: Header=BB51_14 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB51_25
.LBB51_26:                              # %.loopexit66
                                        #   in Loop: Header=BB51_12 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB51_12
.LBB51_27:                              # %.loopexit68
	movl	(%rbx), %eax
	testl	%eax, %eax
	js	.LBB51_28
# BB#32:                                # %term_IsAtom.exit.thread.preheader
	testq	%r14, %r14
	jne	.LBB51_30
# BB#33:
	xorl	%ebx, %ebx
	jmp	.LBB51_34
.LBB51_20:                              # %.us-lcssa
	movq	(%r15), %rdi
	movl	fol_OR(%rip), %ebp
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	cmpl	%ebp, %r13d
	movq	$0, (%r15)
	jne	.LBB51_23
.LBB51_21:
	movl	fol_TRUE(%rip), %eax
	jmp	.LBB51_22
.LBB51_23:
	movl	fol_FALSE(%rip), %eax
.LBB51_22:                              # %.loopexit
	movl	%eax, (%rbx)
	movl	$1, %ebx
	jmp	.LBB51_34
.LBB51_28:                              # %term_IsAtom.exit
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	xorl	%ebx, %ebx
	cmpl	$2, %eax
	je	.LBB51_34
# BB#29:                                # %term_IsAtom.exit
	testq	%r14, %r14
	je	.LBB51_34
.LBB51_30:                              # %term_IsAtom.exit.thread.preheader106
	xorl	%ebx, %ebx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB51_31:                              # %term_IsAtom.exit.thread
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rdi
	callq	fol_PropagateTautologies
	testl	%eax, %eax
	cmovnel	%ebp, %ebx
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB51_31
.LBB51_34:                              # %.loopexit
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end51:
	.size	fol_PropagateTautologies, .Lfunc_end51-fol_PropagateTautologies
	.cfi_endproc

	.globl	fol_AlphaEqual
	.p2align	4, 0x90
	.type	fol_AlphaEqual,@function
fol_AlphaEqual:                         # @fol_AlphaEqual
	.cfi_startproc
# BB#0:
	movl	term_MARK(%rip), %edx
	cmpl	$-1, %edx
	jne	.LBB52_4
# BB#1:                                 # %.preheader.i.preheader
	movq	$-48000, %rax           # imm = 0xFFFF4480
	jmp	.LBB52_2
	.p2align	4, 0x90
.LBB52_5:                               # %.preheader.i.1
                                        #   in Loop: Header=BB52_2 Depth=1
	movq	$0, term_BIND+48016(%rax)
	movq	$0, term_BIND+48032(%rax)
	movq	$0, term_BIND+48048(%rax)
	movq	$0, term_BIND+48064(%rax)
	movq	$0, term_BIND+48080(%rax)
	movq	$0, term_BIND+48096(%rax)
	movq	$0, term_BIND+48112(%rax)
	subq	$-128, %rax
.LBB52_2:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, term_BIND+48000(%rax)
	testq	%rax, %rax
	jne	.LBB52_5
# BB#3:
	movl	$1, term_MARK(%rip)
	movl	$1, %edx
.LBB52_4:                               # %term_ActMark.exit
	leal	1(%rdx), %eax
	movl	%eax, term_MARK(%rip)
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	jmp	fol_AlphaEqualIntern    # TAILCALL
.Lfunc_end52:
	.size	fol_AlphaEqual, .Lfunc_end52-fol_AlphaEqual
	.cfi_endproc

	.p2align	4, 0x90
	.type	fol_AlphaEqualIntern,@function
fol_AlphaEqualIntern:                   # @fol_AlphaEqualIntern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi337:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi338:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi339:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi340:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi341:
	.cfi_def_cfa_offset 48
.Lcfi342:
	.cfi_offset %rbx, -40
.Lcfi343:
	.cfi_offset %r14, -32
.Lcfi344:
	.cfi_offset %r15, -24
.Lcfi345:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movl	(%rbp), %eax
	movl	(%rbx), %ecx
	testl	%eax, %eax
	jle	.LBB53_4
# BB#1:
	testl	%ecx, %ecx
	jle	.LBB53_4
# BB#2:
	movslq	%ecx, %rdx
	shlq	$4, %rdx
	cmpl	%r14d, term_BIND(%rdx)
	jae	.LBB53_12
# BB#3:
	cmpl	%ecx, %eax
	sete	%r15b
	jmp	.LBB53_14
.LBB53_4:
	cmpl	%ecx, %eax
	jne	.LBB53_13
# BB#5:
	cmpl	%eax, fol_ALL(%rip)
	movq	16(%rbp), %rdi
	je	.LBB53_15
# BB#6:
	cmpl	%eax, fol_EXIST(%rip)
	je	.LBB53_15
# BB#7:
	callq	list_Length
	movl	%eax, %r15d
	movq	16(%rbx), %rdi
	callq	list_Length
	cmpl	%eax, %r15d
	jne	.LBB53_13
# BB#8:                                 # %.preheader
	movq	16(%rbp), %rbp
	movb	$1, %r15b
	testq	%rbp, %rbp
	je	.LBB53_14
# BB#9:                                 # %.lr.ph102.preheader
	addq	$16, %rbx
	.p2align	4, 0x90
.LBB53_10:                              # %.lr.ph102
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movq	8(%rbp), %rdi
	movq	8(%rbx), %rsi
	movl	%r14d, %edx
	callq	fol_AlphaEqualIntern
	testl	%eax, %eax
	je	.LBB53_13
# BB#11:                                #   in Loop: Header=BB53_10 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB53_10
	jmp	.LBB53_14
.LBB53_12:
	cmpl	term_BIND+8(%rdx), %eax
	sete	%r15b
	jmp	.LBB53_14
.LBB53_15:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	callq	list_Length
	movl	%eax, %r15d
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rdi
	callq	list_Length
	cmpl	%eax, %r15d
	jne	.LBB53_13
# BB#16:
	movq	16(%rbp), %rcx
	movq	8(%rcx), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB53_20
# BB#17:                                # %.lr.ph97
	movq	16(%rbx), %rcx
	movq	8(%rcx), %rcx
	addq	$16, %rcx
	movl	%r14d, %edx
	.p2align	4, 0x90
.LBB53_18:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rcx
	movq	8(%rcx), %rsi
	movq	8(%rax), %rdi
	movslq	(%rdi), %rdi
	movslq	(%rsi), %rsi
	shlq	$4, %rsi
	movq	%rdx, term_BIND(%rsi)
	movq	%rdi, term_BIND+8(%rsi)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB53_18
# BB#19:                                # %._crit_edge.loopexit
	movq	16(%rbp), %rcx
.LBB53_20:                              # %._crit_edge
	movq	(%rcx), %rax
	movq	8(%rax), %rdi
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movl	%r14d, %edx
	callq	fol_AlphaEqualIntern
	testl	%eax, %eax
	je	.LBB53_13
# BB#21:
	movq	16(%rbp), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	movb	$1, %r15b
	testq	%rax, %rax
	je	.LBB53_14
# BB#22:                                # %.lr.ph.preheader
	movq	16(%rbx), %rcx
	movq	8(%rcx), %rcx
	addq	$16, %rcx
	.p2align	4, 0x90
.LBB53_23:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rcx
	movq	8(%rcx), %rdx
	movslq	(%rdx), %rdx
	shlq	$4, %rdx
	movq	$0, term_BIND(%rdx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB53_23
	jmp	.LBB53_14
.LBB53_13:
	xorl	%r15d, %r15d
.LBB53_14:                              # %.loopexit
	movzbl	%r15b, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end53:
	.size	fol_AlphaEqualIntern, .Lfunc_end53-fol_AlphaEqualIntern
	.cfi_endproc

	.globl	fol_VarBoundTwice
	.p2align	4, 0x90
	.type	fol_VarBoundTwice,@function
fol_VarBoundTwice:                      # @fol_VarBoundTwice
	.cfi_startproc
# BB#0:
	movl	term_MARK(%rip), %esi
	cmpl	$-1, %esi
	jne	.LBB54_4
# BB#1:                                 # %.preheader.i.preheader
	movq	$-48000, %rax           # imm = 0xFFFF4480
	jmp	.LBB54_2
	.p2align	4, 0x90
.LBB54_5:                               # %.preheader.i.1
                                        #   in Loop: Header=BB54_2 Depth=1
	movq	$0, term_BIND+48016(%rax)
	movq	$0, term_BIND+48032(%rax)
	movq	$0, term_BIND+48048(%rax)
	movq	$0, term_BIND+48064(%rax)
	movq	$0, term_BIND+48080(%rax)
	movq	$0, term_BIND+48096(%rax)
	movq	$0, term_BIND+48112(%rax)
	subq	$-128, %rax
.LBB54_2:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, term_BIND+48000(%rax)
	testq	%rax, %rax
	jne	.LBB54_5
# BB#3:
	movl	$1, term_MARK(%rip)
	movl	$1, %esi
.LBB54_4:                               # %term_ActMark.exit
	leal	1(%rsi), %eax
	movl	%eax, term_MARK(%rip)
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	jmp	fol_VarBoundTwiceIntern # TAILCALL
.Lfunc_end54:
	.size	fol_VarBoundTwice, .Lfunc_end54-fol_VarBoundTwice
	.cfi_endproc

	.p2align	4, 0x90
	.type	fol_VarBoundTwiceIntern,@function
fol_VarBoundTwiceIntern:                # @fol_VarBoundTwiceIntern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi346:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi347:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi348:
	.cfi_def_cfa_offset 32
.Lcfi349:
	.cfi_offset %rbx, -32
.Lcfi350:
	.cfi_offset %r14, -24
.Lcfi351:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB55_19
# BB#1:
	movl	(%r14), %eax
	testl	%eax, %eax
	jns	.LBB55_3
# BB#2:                                 # %term_IsAtom.exit
	movl	%eax, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	je	.LBB55_19
.LBB55_3:                               # %term_IsAtom.exit.thread
	cmpl	%eax, fol_ALL(%rip)
	movq	16(%r14), %rbx
	je	.LBB55_9
# BB#4:                                 # %term_IsAtom.exit.thread
	cmpl	%eax, fol_EXIST(%rip)
	jne	.LBB55_5
.LBB55_9:
	movq	8(%rbx), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB55_15
# BB#10:                                # %.lr.ph46
	movl	%ebp, %ecx
	.p2align	4, 0x90
.LBB55_11:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rdx
	movslq	(%rdx), %rdx
	shlq	$4, %rdx
	cmpl	%ebp, term_BIND(%rdx)
	jae	.LBB55_12
# BB#13:                                #   in Loop: Header=BB55_11 Depth=1
	leaq	term_BIND(%rdx), %rdx
	movq	%rcx, (%rdx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB55_11
# BB#14:                                # %._crit_edge.loopexit
	movq	16(%r14), %rbx
.LBB55_15:                              # %._crit_edge
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movl	%ebp, %esi
	callq	fol_VarBoundTwiceIntern
	movl	%eax, %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	jne	.LBB55_20
# BB#16:
	movq	16(%r14), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB55_19
	.p2align	4, 0x90
.LBB55_17:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movslq	(%rcx), %rcx
	shlq	$4, %rcx
	movq	$0, term_BIND(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB55_17
	jmp	.LBB55_19
	.p2align	4, 0x90
.LBB55_8:                               #   in Loop: Header=BB55_5 Depth=1
	movq	(%rbx), %rbx
.LBB55_5:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	je	.LBB55_19
# BB#6:                                 # %.lr.ph48
                                        #   in Loop: Header=BB55_5 Depth=1
	movq	8(%rbx), %rdi
	movl	%ebp, %esi
	callq	fol_VarBoundTwiceIntern
	testl	%eax, %eax
	je	.LBB55_8
# BB#7:
	movl	$1, %eax
	jmp	.LBB55_20
.LBB55_19:
	xorl	%eax, %eax
.LBB55_20:                              # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB55_12:
	movl	$1, %eax
	jmp	.LBB55_20
.Lfunc_end55:
	.size	fol_VarBoundTwiceIntern, .Lfunc_end55-fol_VarBoundTwiceIntern
	.cfi_endproc

	.globl	fol_Depth
	.p2align	4, 0x90
	.type	fol_Depth,@function
fol_Depth:                              # @fol_Depth
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi352:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi353:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi354:
	.cfi_def_cfa_offset 32
.Lcfi355:
	.cfi_offset %rbx, -24
.Lcfi356:
	.cfi_offset %rbp, -16
	movl	(%rdi), %eax
	testl	%eax, %eax
	jns	.LBB56_2
# BB#1:                                 # %symbol_IsPredicate.exit
	movl	%eax, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	movl	$1, %ebp
	cmpl	$2, %ecx
	je	.LBB56_10
.LBB56_2:                               # %symbol_IsPredicate.exit.thread
	cmpl	%eax, fol_ALL(%rip)
	movq	16(%rdi), %rbx
	je	.LBB56_6
# BB#3:                                 # %symbol_IsPredicate.exit.thread
	cmpl	%eax, fol_EXIST(%rip)
	je	.LBB56_6
# BB#4:                                 # %.preheader
	testq	%rbx, %rbx
	je	.LBB56_5
# BB#7:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB56_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	fol_Depth
	cmpl	%ebp, %eax
	cmoval	%eax, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB56_8
# BB#9:                                 # %._crit_edge.loopexit
	incl	%ebp
	jmp	.LBB56_10
.LBB56_6:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	callq	fol_Depth
	incl	%eax
	jmp	.LBB56_11
.LBB56_5:
	movl	$1, %ebp
.LBB56_10:                              # %._crit_edge
	movl	%ebp, %eax
.LBB56_11:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end56:
	.size	fol_Depth, .Lfunc_end56-fol_Depth
	.cfi_endproc

	.globl	fol_ApplyContextToTerm
	.p2align	4, 0x90
	.type	fol_ApplyContextToTerm,@function
fol_ApplyContextToTerm:                 # @fol_ApplyContextToTerm
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi357:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi358:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi359:
	.cfi_def_cfa_offset 32
.Lcfi360:
	.cfi_offset %rbx, -24
.Lcfi361:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	fol_CheckApplyContextToTerm
	testl	%eax, %eax
	je	.LBB57_1
# BB#2:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	fol_ApplyContextToTermIntern
	movl	$1, %eax
	jmp	.LBB57_3
.LBB57_1:
	xorl	%eax, %eax
.LBB57_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end57:
	.size	fol_ApplyContextToTerm, .Lfunc_end57-fol_ApplyContextToTerm
	.cfi_endproc

	.p2align	4, 0x90
	.type	fol_CheckApplyContextToTerm,@function
fol_CheckApplyContextToTerm:            # @fol_CheckApplyContextToTerm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi362:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi363:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi364:
	.cfi_def_cfa_offset 32
.Lcfi365:
	.cfi_offset %rbx, -32
.Lcfi366:
	.cfi_offset %r14, -24
.Lcfi367:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	(%rsi), %eax
	cmpl	%eax, fol_ALL(%rip)
	movq	16(%rsi), %rbx
	je	.LBB58_5
# BB#1:
	cmpl	%eax, fol_EXIST(%rip)
	je	.LBB58_5
# BB#2:                                 # %.preheader
	movl	$1, %ebp
	testq	%rbx, %rbx
	je	.LBB58_13
	.p2align	4, 0x90
.LBB58_4:                               # %.lr.ph45
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	fol_CheckApplyContextToTerm
	testl	%eax, %eax
	cmovel	%eax, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB58_4
	jmp	.LBB58_13
.LBB58_5:
	movq	8(%rbx), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.LBB58_11
	jmp	.LBB58_7
	.p2align	4, 0x90
.LBB58_10:                              #   in Loop: Header=BB58_11 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB58_7
.LBB58_11:                              # %.lr.ph42
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movslq	(%rcx), %rcx
	shlq	$5, %rcx
	cmpq	$0, 8(%r14,%rcx)
	je	.LBB58_10
# BB#12:
	xorl	%ebp, %ebp
	jmp	.LBB58_13
.LBB58_7:                               # %._crit_edge
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rbx
	movl	$1, %ebp
	testq	%rbx, %rbx
	je	.LBB58_13
	.p2align	4, 0x90
.LBB58_9:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	fol_CheckApplyContextToTerm
	testl	%eax, %eax
	cmovel	%eax, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB58_9
.LBB58_13:                              # %.loopexit
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end58:
	.size	fol_CheckApplyContextToTerm, .Lfunc_end58-fol_CheckApplyContextToTerm
	.cfi_endproc

	.p2align	4, 0x90
	.type	fol_ApplyContextToTermIntern,@function
fol_ApplyContextToTermIntern:           # @fol_ApplyContextToTermIntern
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi368:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi369:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi370:
	.cfi_def_cfa_offset 32
.Lcfi371:
	.cfi_offset %rbx, -24
.Lcfi372:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	(%rsi), %eax
	movl	fol_ALL(%rip), %ecx
	cmpl	%eax, %ecx
	movl	fol_EXIST(%rip), %edx
	je	.LBB59_2
.LBB59_1:
	cmpl	%eax, %edx
	jne	.LBB59_3
.LBB59_2:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rsi), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movl	(%rsi), %eax
	cmpl	%eax, %ecx
	jne	.LBB59_1
	jmp	.LBB59_2
.LBB59_3:                               # %tailrecurse._crit_edge
	testl	%eax, %eax
	jle	.LBB59_6
# BB#4:
	cltq
	shlq	$5, %rax
	cmpq	$0, 8(%r14,%rax)
	je	.LBB59_9
# BB#5:
	movl	$1, %edx
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	cont_ApplyBindingsModuloMatching # TAILCALL
.LBB59_6:
	movq	16(%rsi), %rbx
	testq	%rbx, %rbx
	je	.LBB59_9
	.p2align	4, 0x90
.LBB59_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	fol_ApplyContextToTermIntern
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB59_7
.LBB59_9:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end59:
	.size	fol_ApplyContextToTermIntern, .Lfunc_end59-fol_ApplyContextToTermIntern
	.cfi_endproc

	.globl	fol_SignatureMatchFormula
	.p2align	4, 0x90
	.type	fol_SignatureMatchFormula,@function
fol_SignatureMatchFormula:              # @fol_SignatureMatchFormula
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi373:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi374:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi375:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi376:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi377:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi378:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi379:
	.cfi_def_cfa_offset 80
.Lcfi380:
	.cfi_offset %rbx, -56
.Lcfi381:
	.cfi_offset %r12, -48
.Lcfi382:
	.cfi_offset %r13, -40
.Lcfi383:
	.cfi_offset %r14, -32
.Lcfi384:
	.cfi_offset %r15, -24
.Lcfi385:
	.cfi_offset %rbp, -16
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r13
	movl	stack_POINTER(%rip), %ebp
	movl	term_MARK(%rip), %eax
	cmpl	$-1, %eax
	jne	.LBB60_4
# BB#1:                                 # %.preheader.i.preheader
	movq	$-48000, %rax           # imm = 0xFFFF4480
	jmp	.LBB60_2
	.p2align	4, 0x90
.LBB60_36:                              # %.preheader.i.1
                                        #   in Loop: Header=BB60_2 Depth=1
	movq	$0, term_BIND+48016(%rax)
	movq	$0, term_BIND+48032(%rax)
	movq	$0, term_BIND+48048(%rax)
	movq	$0, term_BIND+48064(%rax)
	movq	$0, term_BIND+48080(%rax)
	movq	$0, term_BIND+48096(%rax)
	movq	$0, term_BIND+48112(%rax)
	subq	$-128, %rax
.LBB60_2:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, term_BIND+48000(%rax)
	testq	%rax, %rax
	jne	.LBB60_36
# BB#3:
	movl	$1, term_MARK(%rip)
	movl	$1, %eax
.LBB60_4:                               # %term_NewMark.exit
	leal	1(%rax), %ecx
	movl	%ecx, term_MARK(%rip)
	movl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB60_5
	.p2align	4, 0x90
.LBB60_33:                              #   in Loop: Header=BB60_32 Depth=2
	addl	$-2, %ecx
	movl	%ecx, stack_POINTER(%rip)
	cmpl	%ecx, %ebp
	jne	.LBB60_32
	jmp	.LBB60_23
	.p2align	4, 0x90
.LBB60_34:                              # %.critedge
                                        #   in Loop: Header=BB60_5 Depth=1
	cmpl	%ecx, %ebp
	jne	.LBB60_35
	jmp	.LBB60_23
	.p2align	4, 0x90
.LBB60_25:                              #   in Loop: Header=BB60_5 Depth=1
	testl	%r15d, %r15d
	jle	.LBB60_31
# BB#26:                                #   in Loop: Header=BB60_5 Depth=1
	shlq	$4, %r15
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	%ecx, term_BIND(%r15)
	jae	.LBB60_30
# BB#27:                                #   in Loop: Header=BB60_5 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB60_29
# BB#28:                                #   in Loop: Header=BB60_5 Depth=1
	testl	%r14d, %r14d
	jle	.LBB60_23
.LBB60_29:                              #   in Loop: Header=BB60_5 Depth=1
	movslq	%r14d, %rax
	leaq	term_BIND(%r15), %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%rcx)
	movq	%rax, term_BIND+8(%r15)
	jmp	.LBB60_31
.LBB60_30:                              #   in Loop: Header=BB60_5 Depth=1
	cmpl	%r14d, term_BIND+8(%r15)
	je	.LBB60_31
	jmp	.LBB60_23
.LBB60_9:                               # %symbol_IsJunctor.exit58.thread
                                        #   in Loop: Header=BB60_5 Depth=1
	testl	%r14d, %r14d
	jns	.LBB60_11
# BB#10:                                # %symbol_IsJunctor.exit
                                        #   in Loop: Header=BB60_5 Depth=1
	movl	%r14d, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$3, %ecx
	je	.LBB60_21
.LBB60_11:                              # %symbol_IsJunctor.exit.thread
                                        #   in Loop: Header=BB60_5 Depth=1
	movl	fol_EQUALITY(%rip), %ecx
	cmpl	%r15d, %ecx
	je	.LBB60_21
# BB#12:                                # %symbol_IsJunctor.exit.thread
                                        #   in Loop: Header=BB60_5 Depth=1
	movl	fol_TRUE(%rip), %edx
	cmpl	%r15d, %edx
	je	.LBB60_21
# BB#13:                                # %symbol_IsJunctor.exit.thread
                                        #   in Loop: Header=BB60_5 Depth=1
	movl	fol_FALSE(%rip), %esi
	cmpl	%r15d, %esi
	je	.LBB60_21
# BB#14:                                #   in Loop: Header=BB60_5 Depth=1
	cmpl	%r14d, %ecx
	je	.LBB60_21
# BB#15:                                #   in Loop: Header=BB60_5 Depth=1
	cmpl	%r14d, %edx
	je	.LBB60_21
# BB#16:                                #   in Loop: Header=BB60_5 Depth=1
	cmpl	%r14d, %esi
	je	.LBB60_21
# BB#17:                                #   in Loop: Header=BB60_5 Depth=1
	movl	%r14d, symbol_CONTEXT(,%rax,4)
	jmp	.LBB60_18
	.p2align	4, 0x90
.LBB60_35:                              #   in Loop: Header=BB60_5 Depth=1
	movq	stack_STACK(,%rdx,8), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %r13
	addl	$-2, %ecx
	movq	stack_STACK(,%rcx,8), %rax
	movq	8(%rax), %r12
	movq	%rsi, stack_STACK(,%rdx,8)
	movq	(%rax), %rax
	movq	%rax, stack_STACK(,%rcx,8)
.LBB60_5:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB60_32 Depth 2
	movslq	(%r13), %r15
	testq	%r15, %r15
	movl	(%r12), %r14d
	jg	.LBB60_18
# BB#6:                                 #   in Loop: Header=BB60_5 Depth=1
	movl	%r15d, %edx
	negl	%edx
	movl	%edx, %eax
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	cltq
	movl	symbol_CONTEXT(,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB60_7
# BB#24:                                #   in Loop: Header=BB60_5 Depth=1
	cmpl	%r14d, %ecx
	je	.LBB60_18
	jmp	.LBB60_22
.LBB60_7:                               #   in Loop: Header=BB60_5 Depth=1
	testl	%r15d, %r15d
	jns	.LBB60_9
# BB#8:                                 #   in Loop: Header=BB60_5 Depth=1
	andl	symbol_TYPEMASK(%rip), %edx
	cmpl	$3, %edx
	jne	.LBB60_9
.LBB60_21:                              # %fol_IsPredefinedPred.exit57.thread
                                        #   in Loop: Header=BB60_5 Depth=1
	cmpl	%r14d, %r15d
	jne	.LBB60_22
	.p2align	4, 0x90
.LBB60_18:                              #   in Loop: Header=BB60_5 Depth=1
	movq	16(%r13), %rdi
	callq	list_Length
	movl	%eax, %ebx
	movq	16(%r12), %rdi
	callq	list_Length
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	%ecx, %ebx
	jne	.LBB60_23
# BB#19:                                #   in Loop: Header=BB60_5 Depth=1
	cmpq	$0, 16(%r13)
	je	.LBB60_25
# BB#20:                                #   in Loop: Header=BB60_5 Depth=1
	movq	16(%r12), %rax
	movl	stack_POINTER(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
	movq	16(%r13), %rax
	leal	2(%rcx), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rdx,8)
.LBB60_31:                              # %.preheader
                                        #   in Loop: Header=BB60_5 Depth=1
	movl	stack_POINTER(%rip), %ecx
	movl	$1, %eax
	cmpl	%ebp, %ecx
	je	.LBB60_23
	.p2align	4, 0x90
.LBB60_32:                              # %.lr.ph
                                        #   Parent Loop BB60_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rcx), %edx
	cmpq	$0, stack_STACK(,%rdx,8)
	je	.LBB60_33
	jmp	.LBB60_34
.LBB60_22:
	xorl	%eax, %eax
.LBB60_23:                              # %.thread
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end60:
	.size	fol_SignatureMatchFormula, .Lfunc_end60-fol_SignatureMatchFormula
	.cfi_endproc

	.globl	fol_SignatureMatch
	.p2align	4, 0x90
	.type	fol_SignatureMatch,@function
fol_SignatureMatch:                     # @fol_SignatureMatch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi386:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi387:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi388:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi389:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi390:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi391:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi392:
	.cfi_def_cfa_offset 96
.Lcfi393:
	.cfi_offset %rbx, -56
.Lcfi394:
	.cfi_offset %r12, -48
.Lcfi395:
	.cfi_offset %r13, -40
.Lcfi396:
	.cfi_offset %r14, -32
.Lcfi397:
	.cfi_offset %r15, -24
.Lcfi398:
	.cfi_offset %rbp, -16
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movl	stack_POINTER(%rip), %r14d
	movl	term_MARK(%rip), %eax
	decl	%eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB61_1
	.p2align	4, 0x90
.LBB61_28:                              #   in Loop: Header=BB61_27 Depth=2
	addl	$-2, %ecx
	movl	%ecx, stack_POINTER(%rip)
	cmpl	%ecx, %r14d
	jne	.LBB61_27
	jmp	.LBB61_18
	.p2align	4, 0x90
.LBB61_29:                              # %.critedge
                                        #   in Loop: Header=BB61_1 Depth=1
	cmpl	%ecx, %r14d
	jne	.LBB61_30
	jmp	.LBB61_18
	.p2align	4, 0x90
.LBB61_20:                              #   in Loop: Header=BB61_1 Depth=1
	testl	%r12d, %r12d
	jle	.LBB61_26
# BB#21:                                #   in Loop: Header=BB61_1 Depth=1
	movq	%r12, %rcx
	shlq	$4, %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	cmpl	%edx, term_BIND(%rcx)
	jae	.LBB61_25
# BB#22:                                #   in Loop: Header=BB61_1 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB61_24
# BB#23:                                #   in Loop: Header=BB61_1 Depth=1
	testl	%r15d, %r15d
	jle	.LBB61_18
.LBB61_24:                              #   in Loop: Header=BB61_1 Depth=1
	movslq	%r15d, %rax
	leaq	term_BIND(%rcx), %rdx
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, (%rdx)
	movq	%rax, term_BIND+8(%rcx)
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, (%rbp)
	jmp	.LBB61_26
.LBB61_25:                              #   in Loop: Header=BB61_1 Depth=1
	cmpl	%r15d, term_BIND+8(%rcx)
	je	.LBB61_26
	jmp	.LBB61_18
.LBB61_5:                               # %symbol_IsJunctor.exit65.thread
                                        #   in Loop: Header=BB61_1 Depth=1
	testl	%r15d, %r15d
	jns	.LBB61_7
# BB#6:                                 # %symbol_IsJunctor.exit
                                        #   in Loop: Header=BB61_1 Depth=1
	movl	%r15d, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$3, %ecx
	je	.LBB61_16
.LBB61_7:                               # %symbol_IsJunctor.exit.thread
                                        #   in Loop: Header=BB61_1 Depth=1
	movl	fol_EQUALITY(%rip), %ecx
	cmpl	%r12d, %ecx
	je	.LBB61_16
# BB#8:                                 # %symbol_IsJunctor.exit.thread
                                        #   in Loop: Header=BB61_1 Depth=1
	movl	fol_TRUE(%rip), %edx
	cmpl	%r12d, %edx
	je	.LBB61_16
# BB#9:                                 # %symbol_IsJunctor.exit.thread
                                        #   in Loop: Header=BB61_1 Depth=1
	movl	fol_FALSE(%rip), %esi
	cmpl	%r12d, %esi
	je	.LBB61_16
# BB#10:                                #   in Loop: Header=BB61_1 Depth=1
	cmpl	%r15d, %ecx
	je	.LBB61_16
# BB#11:                                #   in Loop: Header=BB61_1 Depth=1
	cmpl	%r15d, %edx
	je	.LBB61_16
# BB#12:                                #   in Loop: Header=BB61_1 Depth=1
	cmpl	%r15d, %esi
	je	.LBB61_16
# BB#13:                                # %.preheader80.preheader
                                        #   in Loop: Header=BB61_1 Depth=1
	xorl	%ecx, %ecx
.LBB61_14:                              # %.preheader80
                                        #   Parent Loop BB61_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%r15d, symbol_CONTEXT(,%rcx,4)
	je	.LBB61_16
# BB#15:                                # %.preheader80.190
                                        #   in Loop: Header=BB61_14 Depth=2
	cmpl	%r15d, symbol_CONTEXT+4(,%rcx,4)
	je	.LBB61_16
# BB#31:                                # %.preheader80.291
                                        #   in Loop: Header=BB61_14 Depth=2
	cmpl	%r15d, symbol_CONTEXT+8(,%rcx,4)
	je	.LBB61_16
# BB#32:                                # %.preheader80.392
                                        #   in Loop: Header=BB61_14 Depth=2
	cmpl	%r15d, symbol_CONTEXT+12(,%rcx,4)
	je	.LBB61_16
# BB#33:                                # %.preheader80.493
                                        #   in Loop: Header=BB61_14 Depth=2
	cmpl	%r15d, symbol_CONTEXT+16(,%rcx,4)
	je	.LBB61_16
# BB#34:                                #   in Loop: Header=BB61_14 Depth=2
	addq	$5, %rcx
	cmpq	$4000, %rcx             # imm = 0xFA0
	jl	.LBB61_14
# BB#35:                                #   in Loop: Header=BB61_1 Depth=1
	movl	%r15d, symbol_CONTEXT(,%rax,4)
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, (%rbx)
	jmp	.LBB61_36
	.p2align	4, 0x90
.LBB61_30:                              #   in Loop: Header=BB61_1 Depth=1
	movq	stack_STACK(,%rdx,8), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rbp
	addl	$-2, %ecx
	movq	stack_STACK(,%rcx,8), %rax
	movq	8(%rax), %r13
	movq	%rsi, stack_STACK(,%rdx,8)
	movq	(%rax), %rax
	movq	%rax, stack_STACK(,%rcx,8)
.LBB61_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB61_14 Depth 2
                                        #     Child Loop BB61_27 Depth 2
	movslq	(%rbp), %r12
	testq	%r12, %r12
	movl	(%r13), %r15d
	jg	.LBB61_36
# BB#2:                                 #   in Loop: Header=BB61_1 Depth=1
	movl	%r12d, %edx
	negl	%edx
	movl	%edx, %eax
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	cltq
	movl	symbol_CONTEXT(,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB61_3
# BB#19:                                #   in Loop: Header=BB61_1 Depth=1
	cmpl	%r15d, %ecx
	je	.LBB61_36
	jmp	.LBB61_17
.LBB61_3:                               #   in Loop: Header=BB61_1 Depth=1
	testl	%r12d, %r12d
	jns	.LBB61_5
# BB#4:                                 #   in Loop: Header=BB61_1 Depth=1
	andl	symbol_TYPEMASK(%rip), %edx
	cmpl	$3, %edx
	jne	.LBB61_5
.LBB61_16:                              # %symbol_ContextIsMapped.exit
                                        #   in Loop: Header=BB61_1 Depth=1
	cmpl	%r15d, %r12d
	jne	.LBB61_17
	.p2align	4, 0x90
.LBB61_36:                              #   in Loop: Header=BB61_1 Depth=1
	movq	16(%rbp), %rdi
	callq	list_Length
	movl	%eax, %ebx
	movq	16(%r13), %rdi
	callq	list_Length
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	%ecx, %ebx
	jne	.LBB61_18
# BB#37:                                #   in Loop: Header=BB61_1 Depth=1
	cmpq	$0, 16(%rbp)
	je	.LBB61_20
# BB#38:                                #   in Loop: Header=BB61_1 Depth=1
	movq	16(%r13), %rax
	movl	stack_POINTER(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
	movq	16(%rbp), %rax
	leal	2(%rcx), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rdx,8)
.LBB61_26:                              # %.preheader
                                        #   in Loop: Header=BB61_1 Depth=1
	movl	stack_POINTER(%rip), %ecx
	movl	$1, %eax
	cmpl	%r14d, %ecx
	je	.LBB61_18
	.p2align	4, 0x90
.LBB61_27:                              # %.lr.ph
                                        #   Parent Loop BB61_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rcx), %edx
	cmpq	$0, stack_STACK(,%rdx,8)
	je	.LBB61_28
	jmp	.LBB61_29
.LBB61_17:
	xorl	%eax, %eax
.LBB61_18:                              # %.thread
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end61:
	.size	fol_SignatureMatch, .Lfunc_end61-fol_SignatureMatch
	.cfi_endproc

	.globl	fol_CheckFormula
	.p2align	4, 0x90
	.type	fol_CheckFormula,@function
fol_CheckFormula:                       # @fol_CheckFormula
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi399:
	.cfi_def_cfa_offset 16
.Lcfi400:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	fol_FreeVariables
	testq	%rax, %rax
	je	.LBB62_3
	.p2align	4, 0x90
.LBB62_1:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB62_1
# BB#2:                                 # %list_Delete.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB62_3:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	term_CheckTerm          # TAILCALL
.Lfunc_end62:
	.size	fol_CheckFormula, .Lfunc_end62-fol_CheckFormula
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"forall"
	.size	.L.str, 7

	.type	fol_ALL,@object         # @fol_ALL
	.comm	fol_ALL,4,4
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"exists"
	.size	.L.str.1, 7

	.type	fol_EXIST,@object       # @fol_EXIST
	.comm	fol_EXIST,4,4
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"and"
	.size	.L.str.2, 4

	.type	fol_AND,@object         # @fol_AND
	.comm	fol_AND,4,4
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"or"
	.size	.L.str.3, 3

	.type	fol_OR,@object          # @fol_OR
	.comm	fol_OR,4,4
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"not"
	.size	.L.str.4, 4

	.type	fol_NOT,@object         # @fol_NOT
	.comm	fol_NOT,4,4
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"implies"
	.size	.L.str.5, 8

	.type	fol_IMPLIES,@object     # @fol_IMPLIES
	.comm	fol_IMPLIES,4,4
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"implied"
	.size	.L.str.6, 8

	.type	fol_IMPLIED,@object     # @fol_IMPLIED
	.comm	fol_IMPLIED,4,4
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"equiv"
	.size	.L.str.7, 6

	.type	fol_EQUIV,@object       # @fol_EQUIV
	.comm	fol_EQUIV,4,4
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.zero	1
	.size	.L.str.8, 1

	.type	fol_VARLIST,@object     # @fol_VARLIST
	.comm	fol_VARLIST,4,4
	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"equal"
	.size	.L.str.9, 6

	.type	fol_EQUALITY,@object    # @fol_EQUALITY
	.comm	fol_EQUALITY,4,4
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"true"
	.size	.L.str.10, 5

	.type	fol_TRUE,@object        # @fol_TRUE
	.comm	fol_TRUE,4,4
	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"false"
	.size	.L.str.11, 6

	.type	fol_FALSE,@object       # @fol_FALSE
	.comm	fol_FALSE,4,4
	.type	fol_SYMBOLS,@object     # @fol_SYMBOLS
	.comm	fol_SYMBOLS,8,8
	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\nset(process_input)."
	.size	.L.str.12, 21

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"\nset(binary_res)."
	.size	.L.str.13, 18

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"\nset(factor)."
	.size	.L.str.14, 14

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"\nclear(print_kept)."
	.size	.L.str.15, 20

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"\nassign(max_seconds, 20)."
	.size	.L.str.16, 26

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"\nclear(print_new_demod)."
	.size	.L.str.17, 25

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\nclear(print_back_demod)."
	.size	.L.str.18, 26

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"\nclear(print_back_sub)."
	.size	.L.str.19, 24

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"\nset(para_from)."
	.size	.L.str.20, 17

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"\nset(para_into)."
	.size	.L.str.21, 17

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"\nset(para_from_vars)."
	.size	.L.str.22, 22

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"\nset(back_demod)."
	.size	.L.str.23, 18

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"\nset(auto)."
	.size	.L.str.24, 12

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"\nset(auto2)."
	.size	.L.str.25, 13

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str.26, 31

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/SPASS/foldfg.c"
	.size	.L.str.27, 78

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"\n In fol_FPrintOtterOptions: Illegal parameter value %d."
	.size	.L.str.28, 57

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.29, 133

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"\n\n"
	.size	.L.str.30, 3

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"formula_list(usable).\n"
	.size	.L.str.31, 23

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"all x (x=x).\n"
	.size	.L.str.32, 14

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"\n%% %s \n"
	.size	.L.str.33, 9

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	".\n\n"
	.size	.L.str.34, 4

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"end_of_list.\n\n"
	.size	.L.str.35, 15

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"  functions["
	.size	.L.str.36, 13

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"(%s, %d)"
	.size	.L.str.37, 9

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	", "
	.size	.L.str.38, 3

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"\n\t"
	.size	.L.str.39, 3

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"].\n"
	.size	.L.str.40, 4

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"  predicates["
	.size	.L.str.41, 14

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"(["
	.size	.L.str.42, 3

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"],"
	.size	.L.str.43, 3

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	" > "
	.size	.L.str.44, 4

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"set_precedence("
	.size	.L.str.45, 16

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"%d"
	.size	.L.str.46, 3

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	")."
	.size	.L.str.47, 3

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"begin_problem(Unknown).\n\n"
	.size	.L.str.48, 26

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"list_of_descriptions.\n"
	.size	.L.str.49, 23

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"name(%s).\n"
	.size	.L.str.50, 11

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"author(%s).\n"
	.size	.L.str.51, 13

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"status(%s).\n"
	.size	.L.str.52, 13

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"description(%s).\n"
	.size	.L.str.53, 18

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"list_of_symbols.\n"
	.size	.L.str.54, 18

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"axioms"
	.size	.L.str.55, 7

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"conjectures"
	.size	.L.str.56, 12

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"end_problem.\n"
	.size	.L.str.57, 14

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"\n In fol_TermPolarity: Unknown first-order operator.\n"
	.size	.L.str.58, 54

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	" = "
	.size	.L.str.59, 4

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"all "
	.size	.L.str.60, 5

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"exists "
	.size	.L.str.61, 8

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	" ("
	.size	.L.str.62, 3

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"- ("
	.size	.L.str.64, 4

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	" & "
	.size	.L.str.66, 4

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	" | "
	.size	.L.str.67, 4

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	" <-> "
	.size	.L.str.68, 6

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	" -> "
	.size	.L.str.69, 5

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"list_of_formulae("
	.size	.L.str.70, 18

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	").\n"
	.size	.L.str.71, 4

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"\tformula("
	.size	.L.str.72, 10

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"  "
	.size	.L.str.73, 3

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	",\n"
	.size	.L.str.74, 3

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"],\n"
	.size	.L.str.75, 4

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"(\n"
	.size	.L.str.76, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
