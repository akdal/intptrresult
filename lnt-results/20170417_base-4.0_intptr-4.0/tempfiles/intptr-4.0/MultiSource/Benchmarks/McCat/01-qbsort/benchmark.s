	.text
	.file	"benchmark.bc"
	.globl	LessThan
	.p2align	4, 0x90
	.type	LessThan,@function
LessThan:                               # @LessThan
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	%esi, %edi
	setl	%al
	retq
.Lfunc_end0:
	.size	LessThan, .Lfunc_end0-LessThan
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	xorl	%ebx, %ebx
	cmpl	$2, %edi
	jl	.LBB1_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	strtol
	movq	%rax, %rbx
.LBB1_2:                                # %.preheader
	movl	$1, %r12d
	leaq	8(%rsp), %r15
	jmp	.LBB1_3
	.p2align	4, 0x90
.LBB1_11:                               # %._crit_edge
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	8(%rsp), %rdi
	callq	PrintList
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	8(%rsp), %rdi
	movl	$LessThan, %esi
	callq	BubbleSort
	movq	%rax, 8(%rsp)
	movq	%rax, %rdi
	callq	PrintList
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$LessThan, %esi
	movq	%rbp, %rdi
	callq	QuickSort
	movq	%rax, 16(%rsp)
	movq	%rax, %rdi
	callq	PrintLinkList
	movl	$10, %edi
	callq	putchar
	movq	16(%rsp), %rdi
	callq	FreeLinkList
	movq	%rbp, %rdi
	callq	FreeLinkList
	movq	8(%rsp), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	8(%rsp), %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	incl	%r12d
.LBB1_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_8 Depth 2
	leaq	16(%rsp), %rdi
	movq	%r15, %rsi
	callq	ReadList
	testl	%eax, %eax
	jne	.LBB1_4
# BB#7:                                 #   in Loop: Header=BB1_3 Depth=1
	movq	16(%rsp), %rbp
	movq	8(%rsp), %r14
	movq	8(%r14), %r13
	movslq	(%r14), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, 8(%r14)
	movslq	(%r14), %rdx
	shlq	$2, %rdx
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	memcpy
	testl	%ebx, %ebx
	jg	.LBB1_8
	jmp	.LBB1_11
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph..lr.ph_crit_edge
                                        #   in Loop: Header=BB1_8 Depth=2
	decl	%ebx
	movq	8(%rsp), %r14
.LBB1_8:                                # %.lr.ph
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$LessThan, %esi
	movq	%r14, %rdi
	callq	BubbleSort
	movq	%rax, 8(%rsp)
	movq	8(%rax), %rdi
	movslq	(%rax), %rdx
	shlq	$2, %rdx
	movq	%r13, %rsi
	callq	memcpy
	movl	$LessThan, %esi
	movq	%rbp, %rdi
	callq	QuickSort
	movq	%rax, 16(%rsp)
	movq	%rax, %rdi
	callq	FreeLinkList
	cmpl	$2, %ebx
	jge	.LBB1_9
# BB#10:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB1_3 Depth=1
	decl	%ebx
	jmp	.LBB1_11
.LBB1_4:
	cmpl	$7, %eax
	je	.LBB1_12
# BB#5:
	cmpl	$42, %eax
	jne	.LBB1_13
# BB#6:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	printf
	movl	$1, %edi
	callq	exit
.LBB1_12:
	movl	$.Lstr, %edi
	callq	puts
	xorl	%edi, %edi
	callq	exit
.LBB1_13:
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\nList read (reverse order): "
	.size	.L.str, 29

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\nBubbleSort: "
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\nQuickSort:  "
	.size	.L.str.2, 14

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Comma expected in list number %d\n"
	.size	.L.str.4, 34

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"Last list read"
	.size	.Lstr, 15

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.1:
	.asciz	"Program Error: Unrecognized errorcode from ReadList"
	.size	.Lstr.1, 52


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
