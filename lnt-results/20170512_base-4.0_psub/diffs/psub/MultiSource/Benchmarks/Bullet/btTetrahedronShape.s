	.text
	.file	"btTetrahedronShape.bc"
	.globl	_ZN16btBU_Simplex1to4C2Ev
	.p2align	4, 0x90
	.type	_ZN16btBU_Simplex1to4C2Ev,@function
_ZN16btBU_Simplex1to4C2Ev:              # @_ZN16btBU_Simplex1to4C2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN34btPolyhedralConvexAabbCachingShapeC2Ev
	movq	$_ZTV16btBU_Simplex1to4+16, (%rbx)
	movl	$0, 100(%rbx)
	movl	$2, 8(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN16btBU_Simplex1to4C2Ev, .Lfunc_end0-_ZN16btBU_Simplex1to4C2Ev
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN16btBU_Simplex1to4C2ERK9btVector3
	.p2align	4, 0x90
	.type	_ZN16btBU_Simplex1to4C2ERK9btVector3,@function
_ZN16btBU_Simplex1to4C2ERK9btVector3:   # @_ZN16btBU_Simplex1to4C2ERK9btVector3
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN34btPolyhedralConvexAabbCachingShapeC2Ev
	movq	$_ZTV16btBU_Simplex1to4+16, (%rbx)
	movl	$2, 8(%rbx)
	movl	$1, 100(%rbx)
	movups	(%r14), %xmm0
	movups	%xmm0, 104(%rbx)
.Ltmp0:
	movq	%rbx, %rdi
	callq	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
.Ltmp1:
# BB#1:                                 # %_ZN16btBU_Simplex1to49addVertexERK9btVector3.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp4:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN16btBU_Simplex1to4C2ERK9btVector3, .Lfunc_end2-_ZN16btBU_Simplex1to4C2ERK9btVector3
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN16btBU_Simplex1to49addVertexERK9btVector3
	.p2align	4, 0x90
	.type	_ZN16btBU_Simplex1to49addVertexERK9btVector3,@function
_ZN16btBU_Simplex1to49addVertexERK9btVector3: # @_ZN16btBU_Simplex1to49addVertexERK9btVector3
	.cfi_startproc
# BB#0:
	movslq	100(%rdi), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 100(%rdi)
	shlq	$4, %rax
	movups	(%rsi), %xmm0
	movups	%xmm0, 104(%rdi,%rax)
	jmp	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv # TAILCALL
.Lfunc_end3:
	.size	_ZN16btBU_Simplex1to49addVertexERK9btVector3, .Lfunc_end3-_ZN16btBU_Simplex1to49addVertexERK9btVector3
	.cfi_endproc

	.globl	_ZN16btBU_Simplex1to4C2ERK9btVector3S2_
	.p2align	4, 0x90
	.type	_ZN16btBU_Simplex1to4C2ERK9btVector3S2_,@function
_ZN16btBU_Simplex1to4C2ERK9btVector3S2_: # @_ZN16btBU_Simplex1to4C2ERK9btVector3S2_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -32
.Lcfi11:
	.cfi_offset %r14, -24
.Lcfi12:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	callq	_ZN34btPolyhedralConvexAabbCachingShapeC2Ev
	movq	$_ZTV16btBU_Simplex1to4+16, (%rbx)
	movl	$2, 8(%rbx)
	movl	$1, 100(%rbx)
	movups	(%r15), %xmm0
	movups	%xmm0, 104(%rbx)
.Ltmp6:
	movq	%rbx, %rdi
	callq	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
.Ltmp7:
# BB#1:                                 # %_ZN16btBU_Simplex1to49addVertexERK9btVector3.exit
	movslq	100(%rbx), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 100(%rbx)
	shlq	$4, %rax
	movups	(%r14), %xmm0
	movups	%xmm0, 104(%rbx,%rax)
.Ltmp8:
	movq	%rbx, %rdi
	callq	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
.Ltmp9:
# BB#2:                                 # %_ZN16btBU_Simplex1to49addVertexERK9btVector3.exit4
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB4_3:
.Ltmp10:
	movq	%rax, %r14
.Ltmp11:
	movq	%rbx, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp12:
# BB#4:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_5:
.Ltmp13:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN16btBU_Simplex1to4C2ERK9btVector3S2_, .Lfunc_end4-_ZN16btBU_Simplex1to4C2ERK9btVector3S2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp6-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp6           #   Call between .Ltmp6 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin1   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	1                       #   On action: 1
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_
	.p2align	4, 0x90
	.type	_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_,@function
_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_: # @_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r12, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	callq	_ZN34btPolyhedralConvexAabbCachingShapeC2Ev
	movq	$_ZTV16btBU_Simplex1to4+16, (%rbx)
	movl	$2, 8(%rbx)
	movl	$1, 100(%rbx)
	movups	(%r12), %xmm0
	movups	%xmm0, 104(%rbx)
.Ltmp14:
	movq	%rbx, %rdi
	callq	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
.Ltmp15:
# BB#1:                                 # %_ZN16btBU_Simplex1to49addVertexERK9btVector3.exit
	movslq	100(%rbx), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 100(%rbx)
	shlq	$4, %rax
	movups	(%r15), %xmm0
	movups	%xmm0, 104(%rbx,%rax)
.Ltmp16:
	movq	%rbx, %rdi
	callq	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
.Ltmp17:
# BB#2:                                 # %_ZN16btBU_Simplex1to49addVertexERK9btVector3.exit5
	movslq	100(%rbx), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 100(%rbx)
	shlq	$4, %rax
	movups	(%r14), %xmm0
	movups	%xmm0, 104(%rbx,%rax)
.Ltmp18:
	movq	%rbx, %rdi
	callq	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
.Ltmp19:
# BB#3:                                 # %_ZN16btBU_Simplex1to49addVertexERK9btVector3.exit6
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB5_4:
.Ltmp20:
	movq	%rax, %r14
.Ltmp21:
	movq	%rbx, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp22:
# BB#5:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_6:
.Ltmp23:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_, .Lfunc_end5-_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp14-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp19-.Ltmp14         #   Call between .Ltmp14 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	1                       #   On action: 1
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_S2_
	.p2align	4, 0x90
	.type	_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_S2_,@function
_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_S2_: # @_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_S2_
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -48
.Lcfi28:
	.cfi_offset %r12, -40
.Lcfi29:
	.cfi_offset %r13, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbx
	callq	_ZN34btPolyhedralConvexAabbCachingShapeC2Ev
	movq	$_ZTV16btBU_Simplex1to4+16, (%rbx)
	movl	$2, 8(%rbx)
	movl	$1, 100(%rbx)
	movups	(%r13), %xmm0
	movups	%xmm0, 104(%rbx)
.Ltmp24:
	movq	%rbx, %rdi
	callq	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
.Ltmp25:
# BB#1:                                 # %_ZN16btBU_Simplex1to49addVertexERK9btVector3.exit
	movslq	100(%rbx), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 100(%rbx)
	shlq	$4, %rax
	movups	(%r12), %xmm0
	movups	%xmm0, 104(%rbx,%rax)
.Ltmp26:
	movq	%rbx, %rdi
	callq	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
.Ltmp27:
# BB#2:                                 # %_ZN16btBU_Simplex1to49addVertexERK9btVector3.exit6
	movslq	100(%rbx), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 100(%rbx)
	shlq	$4, %rax
	movups	(%r15), %xmm0
	movups	%xmm0, 104(%rbx,%rax)
.Ltmp28:
	movq	%rbx, %rdi
	callq	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
.Ltmp29:
# BB#3:                                 # %_ZN16btBU_Simplex1to49addVertexERK9btVector3.exit7
	movslq	100(%rbx), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 100(%rbx)
	shlq	$4, %rax
	movups	(%r14), %xmm0
	movups	%xmm0, 104(%rbx,%rax)
.Ltmp30:
	movq	%rbx, %rdi
	callq	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
.Ltmp31:
# BB#4:                                 # %_ZN16btBU_Simplex1to49addVertexERK9btVector3.exit8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB6_5:
.Ltmp32:
	movq	%rax, %r14
.Ltmp33:
	movq	%rbx, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp34:
# BB#6:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_7:
.Ltmp35:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_S2_, .Lfunc_end6-_ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_S2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp24-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp31-.Ltmp24         #   Call between .Ltmp24 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin3   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin3   #     jumps to .Ltmp35
	.byte	1                       #   On action: 1
	.long	.Ltmp34-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp34     #   Call between .Ltmp34 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZNK16btBU_Simplex1to47getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK16btBU_Simplex1to47getAabbERK11btTransformR9btVector3S4_,@function
_ZNK16btBU_Simplex1to47getAabbERK11btTransformR9btVector3S4_: # @_ZNK16btBU_Simplex1to47getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	jmp	_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_ # TAILCALL
.Lfunc_end7:
	.size	_ZNK16btBU_Simplex1to47getAabbERK11btTransformR9btVector3S4_, .Lfunc_end7-_ZNK16btBU_Simplex1to47getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.globl	_ZNK16btBU_Simplex1to414getNumVerticesEv
	.p2align	4, 0x90
	.type	_ZNK16btBU_Simplex1to414getNumVerticesEv,@function
_ZNK16btBU_Simplex1to414getNumVerticesEv: # @_ZNK16btBU_Simplex1to414getNumVerticesEv
	.cfi_startproc
# BB#0:
	movl	100(%rdi), %eax
	retq
.Lfunc_end8:
	.size	_ZNK16btBU_Simplex1to414getNumVerticesEv, .Lfunc_end8-_ZNK16btBU_Simplex1to414getNumVerticesEv
	.cfi_endproc

	.globl	_ZNK16btBU_Simplex1to411getNumEdgesEv
	.p2align	4, 0x90
	.type	_ZNK16btBU_Simplex1to411getNumEdgesEv,@function
_ZNK16btBU_Simplex1to411getNumEdgesEv:  # @_ZNK16btBU_Simplex1to411getNumEdgesEv
	.cfi_startproc
# BB#0:
	movslq	100(%rdi), %rax
	cmpq	$4, %rax
	ja	.LBB9_2
# BB#1:                                 # %switch.lookup
	movl	.Lswitch.table(,%rax,4), %eax
	retq
.LBB9_2:
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	_ZNK16btBU_Simplex1to411getNumEdgesEv, .Lfunc_end9-_ZNK16btBU_Simplex1to411getNumEdgesEv
	.cfi_endproc

	.globl	_ZNK16btBU_Simplex1to47getEdgeEiR9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK16btBU_Simplex1to47getEdgeEiR9btVector3S1_,@function
_ZNK16btBU_Simplex1to47getEdgeEiR9btVector3S1_: # @_ZNK16btBU_Simplex1to47getEdgeEiR9btVector3S1_
	.cfi_startproc
# BB#0:
	movl	100(%rdi), %eax
	cmpl	$4, %eax
	je	.LBB10_8
# BB#1:
	cmpl	$3, %eax
	je	.LBB10_4
# BB#2:
	cmpl	$2, %eax
	je	.LBB10_3
	jmp	.LBB10_7
.LBB10_8:
	cmpl	$5, %esi
	ja	.LBB10_7
# BB#9:
	movl	%esi, %eax
	jmpq	*.LJTI10_0(,%rax,8)
.LBB10_12:
	movups	104(%rdi), %xmm0
	jmp	.LBB10_13
.LBB10_4:
	cmpl	$2, %esi
	je	.LBB10_11
# BB#5:
	cmpl	$1, %esi
	je	.LBB10_10
# BB#6:
	testl	%esi, %esi
	jne	.LBB10_7
.LBB10_3:
	movups	104(%rdi), %xmm0
	movups	%xmm0, (%rdx)
	movups	120(%rdi), %xmm0
	movups	%xmm0, (%rcx)
	retq
.LBB10_7:
	retq
.LBB10_11:
	movups	136(%rdi), %xmm0
	movups	%xmm0, (%rdx)
	movups	104(%rdi), %xmm0
	movups	%xmm0, (%rcx)
	retq
.LBB10_10:
	movups	120(%rdi), %xmm0
	movups	%xmm0, (%rdx)
	movups	136(%rdi), %xmm0
	movups	%xmm0, (%rcx)
	retq
.LBB10_14:
	movups	120(%rdi), %xmm0
	jmp	.LBB10_13
.LBB10_15:
	movups	136(%rdi), %xmm0
.LBB10_13:
	movups	%xmm0, (%rdx)
	movups	152(%rdi), %xmm0
	movups	%xmm0, (%rcx)
	retq
.Lfunc_end10:
	.size	_ZNK16btBU_Simplex1to47getEdgeEiR9btVector3S1_, .Lfunc_end10-_ZNK16btBU_Simplex1to47getEdgeEiR9btVector3S1_
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI10_0:
	.quad	.LBB10_3
	.quad	.LBB10_10
	.quad	.LBB10_11
	.quad	.LBB10_12
	.quad	.LBB10_14
	.quad	.LBB10_15

	.text
	.globl	_ZNK16btBU_Simplex1to49getVertexEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK16btBU_Simplex1to49getVertexEiR9btVector3,@function
_ZNK16btBU_Simplex1to49getVertexEiR9btVector3: # @_ZNK16btBU_Simplex1to49getVertexEiR9btVector3
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	shlq	$4, %rax
	movups	104(%rdi,%rax), %xmm0
	movups	%xmm0, (%rdx)
	retq
.Lfunc_end11:
	.size	_ZNK16btBU_Simplex1to49getVertexEiR9btVector3, .Lfunc_end11-_ZNK16btBU_Simplex1to49getVertexEiR9btVector3
	.cfi_endproc

	.globl	_ZNK16btBU_Simplex1to412getNumPlanesEv
	.p2align	4, 0x90
	.type	_ZNK16btBU_Simplex1to412getNumPlanesEv,@function
_ZNK16btBU_Simplex1to412getNumPlanesEv: # @_ZNK16btBU_Simplex1to412getNumPlanesEv
	.cfi_startproc
# BB#0:
	movslq	100(%rdi), %rax
	cmpq	$4, %rax
	ja	.LBB12_2
# BB#1:                                 # %switch.lookup
	movl	.Lswitch.table.1(,%rax,4), %eax
	retq
.LBB12_2:
	xorl	%eax, %eax
	retq
.Lfunc_end12:
	.size	_ZNK16btBU_Simplex1to412getNumPlanesEv, .Lfunc_end12-_ZNK16btBU_Simplex1to412getNumPlanesEv
	.cfi_endproc

	.globl	_ZNK16btBU_Simplex1to48getPlaneER9btVector3S1_i
	.p2align	4, 0x90
	.type	_ZNK16btBU_Simplex1to48getPlaneER9btVector3S1_i,@function
_ZNK16btBU_Simplex1to48getPlaneER9btVector3S1_i: # @_ZNK16btBU_Simplex1to48getPlaneER9btVector3S1_i
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end13:
	.size	_ZNK16btBU_Simplex1to48getPlaneER9btVector3S1_i, .Lfunc_end13-_ZNK16btBU_Simplex1to48getPlaneER9btVector3S1_i
	.cfi_endproc

	.globl	_ZNK16btBU_Simplex1to48getIndexEi
	.p2align	4, 0x90
	.type	_ZNK16btBU_Simplex1to48getIndexEi,@function
_ZNK16btBU_Simplex1to48getIndexEi:      # @_ZNK16btBU_Simplex1to48getIndexEi
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end14:
	.size	_ZNK16btBU_Simplex1to48getIndexEi, .Lfunc_end14-_ZNK16btBU_Simplex1to48getIndexEi
	.cfi_endproc

	.globl	_ZNK16btBU_Simplex1to48isInsideERK9btVector3f
	.p2align	4, 0x90
	.type	_ZNK16btBU_Simplex1to48isInsideERK9btVector3f,@function
_ZNK16btBU_Simplex1to48isInsideERK9btVector3f: # @_ZNK16btBU_Simplex1to48isInsideERK9btVector3f
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end15:
	.size	_ZNK16btBU_Simplex1to48isInsideERK9btVector3f, .Lfunc_end15-_ZNK16btBU_Simplex1to48isInsideERK9btVector3f
	.cfi_endproc

	.section	.text._ZN16btBU_Simplex1to4D0Ev,"axG",@progbits,_ZN16btBU_Simplex1to4D0Ev,comdat
	.weak	_ZN16btBU_Simplex1to4D0Ev
	.p2align	4, 0x90
	.type	_ZN16btBU_Simplex1to4D0Ev,@function
_ZN16btBU_Simplex1to4D0Ev:              # @_ZN16btBU_Simplex1to4D0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -24
.Lcfi36:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp36:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp37:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB16_2:
.Ltmp38:
	movq	%rax, %r14
.Ltmp39:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp40:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB16_4:
.Ltmp41:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN16btBU_Simplex1to4D0Ev, .Lfunc_end16-_ZN16btBU_Simplex1to4D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp36-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin4   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp39-.Ltmp37         #   Call between .Ltmp37 and .Ltmp39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin4   #     jumps to .Ltmp41
	.byte	1                       #   On action: 1
	.long	.Ltmp40-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Lfunc_end16-.Ltmp40    #   Call between .Ltmp40 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end17:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end17-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK16btBU_Simplex1to47getNameEv,"axG",@progbits,_ZNK16btBU_Simplex1to47getNameEv,comdat
	.weak	_ZNK16btBU_Simplex1to47getNameEv
	.p2align	4, 0x90
	.type	_ZNK16btBU_Simplex1to47getNameEv,@function
_ZNK16btBU_Simplex1to47getNameEv:       # @_ZNK16btBU_Simplex1to47getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end18:
	.size	_ZNK16btBU_Simplex1to47getNameEv, .Lfunc_end18-_ZNK16btBU_Simplex1to47getNameEv
	.cfi_endproc

	.section	.text._ZN21btConvexInternalShape9setMarginEf,"axG",@progbits,_ZN21btConvexInternalShape9setMarginEf,comdat
	.weak	_ZN21btConvexInternalShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape9setMarginEf,@function
_ZN21btConvexInternalShape9setMarginEf: # @_ZN21btConvexInternalShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end19:
	.size	_ZN21btConvexInternalShape9setMarginEf, .Lfunc_end19-_ZN21btConvexInternalShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end20:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end20-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end21:
	.size	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end21-_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end22:
	.size	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end22-_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.type	_ZTV16btBU_Simplex1to4,@object # @_ZTV16btBU_Simplex1to4
	.section	.rodata,"a",@progbits
	.globl	_ZTV16btBU_Simplex1to4
	.p2align	3
_ZTV16btBU_Simplex1to4:
	.quad	0
	.quad	_ZTI16btBU_Simplex1to4
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN16btBU_Simplex1to4D0Ev
	.quad	_ZNK16btBU_Simplex1to47getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK16btBU_Simplex1to47getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	_ZNK16btBU_Simplex1to414getNumVerticesEv
	.quad	_ZNK16btBU_Simplex1to411getNumEdgesEv
	.quad	_ZNK16btBU_Simplex1to47getEdgeEiR9btVector3S1_
	.quad	_ZNK16btBU_Simplex1to49getVertexEiR9btVector3
	.quad	_ZNK16btBU_Simplex1to412getNumPlanesEv
	.quad	_ZNK16btBU_Simplex1to48getPlaneER9btVector3S1_i
	.quad	_ZNK16btBU_Simplex1to48isInsideERK9btVector3f
	.quad	_ZNK16btBU_Simplex1to48getIndexEi
	.size	_ZTV16btBU_Simplex1to4, 224

	.type	_ZTS16btBU_Simplex1to4,@object # @_ZTS16btBU_Simplex1to4
	.globl	_ZTS16btBU_Simplex1to4
	.p2align	4
_ZTS16btBU_Simplex1to4:
	.asciz	"16btBU_Simplex1to4"
	.size	_ZTS16btBU_Simplex1to4, 19

	.type	_ZTI16btBU_Simplex1to4,@object # @_ZTI16btBU_Simplex1to4
	.globl	_ZTI16btBU_Simplex1to4
	.p2align	4
_ZTI16btBU_Simplex1to4:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16btBU_Simplex1to4
	.quad	_ZTI34btPolyhedralConvexAabbCachingShape
	.size	_ZTI16btBU_Simplex1to4, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"btBU_Simplex1to4"
	.size	.L.str, 17

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	6                       # 0x6
	.size	.Lswitch.table, 20

	.type	.Lswitch.table.1,@object # @switch.table.1
	.p2align	4
.Lswitch.table.1:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	4                       # 0x4
	.size	.Lswitch.table.1, 20


	.globl	_ZN16btBU_Simplex1to4C1Ev
	.type	_ZN16btBU_Simplex1to4C1Ev,@function
_ZN16btBU_Simplex1to4C1Ev = _ZN16btBU_Simplex1to4C2Ev
	.globl	_ZN16btBU_Simplex1to4C1ERK9btVector3
	.type	_ZN16btBU_Simplex1to4C1ERK9btVector3,@function
_ZN16btBU_Simplex1to4C1ERK9btVector3 = _ZN16btBU_Simplex1to4C2ERK9btVector3
	.globl	_ZN16btBU_Simplex1to4C1ERK9btVector3S2_
	.type	_ZN16btBU_Simplex1to4C1ERK9btVector3S2_,@function
_ZN16btBU_Simplex1to4C1ERK9btVector3S2_ = _ZN16btBU_Simplex1to4C2ERK9btVector3S2_
	.globl	_ZN16btBU_Simplex1to4C1ERK9btVector3S2_S2_
	.type	_ZN16btBU_Simplex1to4C1ERK9btVector3S2_S2_,@function
_ZN16btBU_Simplex1to4C1ERK9btVector3S2_S2_ = _ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_
	.globl	_ZN16btBU_Simplex1to4C1ERK9btVector3S2_S2_S2_
	.type	_ZN16btBU_Simplex1to4C1ERK9btVector3S2_S2_S2_,@function
_ZN16btBU_Simplex1to4C1ERK9btVector3S2_S2_S2_ = _ZN16btBU_Simplex1to4C2ERK9btVector3S2_S2_S2_
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
