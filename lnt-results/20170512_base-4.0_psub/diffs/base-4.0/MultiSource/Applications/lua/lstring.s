	.text
	.file	"lstring.bc"
	.hidden	luaS_resize
	.globl	luaS_resize
	.p2align	4, 0x90
	.type	luaS_resize,@function
luaS_resize:                            # @luaS_resize
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	movq	32(%r15), %rax
	cmpb	$2, 33(%rax)
	je	.LBB0_11
# BB#1:
	cmpl	$-2, %r14d
	jle	.LBB0_2
# BB#3:
	movslq	%r14d, %rcx
	shlq	$3, %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	luaM_realloc_
	movq	%rax, %r13
	movq	32(%r15), %r12
	testl	%r14d, %r14d
	jle	.LBB0_5
# BB#4:                                 # %.lr.ph56.preheader
	leal	-1(%r14), %eax
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	memset
	jmp	.LBB0_5
.LBB0_2:                                # %.thread
	movq	%r15, %rdi
	callq	luaM_toobig
	movq	%rax, %r13
	movq	32(%r15), %r12
.LBB0_5:                                # %.preheader
	movslq	12(%r12), %rdx
	testq	%rdx, %rdx
	jle	.LBB0_10
# BB#6:                                 # %.lr.ph51
	leal	-1(%r14), %eax
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB0_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_8 Depth 2
	movq	(%r12), %rsi
	movq	(%rsi,%r8,8), %rsi
	testq	%rsi, %rsi
	je	.LBB0_9
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph
                                        #   Parent Loop BB0_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rdi
	movl	12(%rsi), %ecx
	andl	%eax, %ecx
	movslq	%ecx, %rcx
	movq	(%r13,%rcx,8), %rbx
	movq	%rbx, (%rsi)
	movq	%rsi, (%r13,%rcx,8)
	testq	%rdi, %rdi
	movq	%rdi, %rsi
	jne	.LBB0_8
.LBB0_9:                                # %._crit_edge
                                        #   in Loop: Header=BB0_7 Depth=1
	incq	%r8
	cmpq	%rdx, %r8
	jl	.LBB0_7
.LBB0_10:                               # %._crit_edge52
	movq	(%r12), %rsi
	shlq	$3, %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	callq	luaM_realloc_
	movl	%r14d, 12(%r12)
	movq	%r13, (%r12)
.LBB0_11:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	luaS_resize, .Lfunc_end0-luaS_resize
	.cfi_endproc

	.hidden	luaS_newlstr
	.globl	luaS_newlstr
	.p2align	4, 0x90
	.type	luaS_newlstr,@function
luaS_newlstr:                           # @luaS_newlstr
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 64
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	%r12, %rax
	shrq	$5, %rax
	incq	%rax
	cmpq	%r12, %rax
	jbe	.LBB1_2
# BB#1:
	movl	%r12d, %ebp
	jmp	.LBB1_4
.LBB1_2:                                # %.lr.ph52.preheader
	movq	%r12, %rcx
	movl	%r12d, %ebp
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph52
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edx
	shll	$5, %edx
	movl	%ebp, %esi
	shrl	$2, %esi
	addl	%edx, %esi
	movzbl	-1(%r15,%rcx), %edx
	addl	%esi, %edx
	xorl	%edx, %ebp
	subq	%rax, %rcx
	cmpq	%rax, %rcx
	jae	.LBB1_3
.LBB1_4:                                # %._crit_edge53
	movq	32(%r14), %r13
	movq	(%r13), %rax
	movl	12(%r13), %ecx
	decl	%ecx
	andl	%ebp, %ecx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_6
	jmp	.LBB1_11
	.p2align	4, 0x90
.LBB1_10:                               #   in Loop: Header=BB1_6 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB1_11
.LBB1_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r12, 16(%rbx)
	jne	.LBB1_10
# BB#7:                                 #   in Loop: Header=BB1_6 Depth=1
	leaq	24(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB1_10
# BB#8:
	movb	9(%rbx), %al
	movb	32(%r13), %cl
	notb	%cl
	andb	%al, %cl
	testb	$3, %cl
	je	.LBB1_16
# BB#9:
	xorb	$3, %al
	movb	%al, 9(%rbx)
	jmp	.LBB1_16
.LBB1_11:                               # %._crit_edge
	leaq	1(%r12), %rax
	cmpq	$-26, %rax
	jb	.LBB1_13
# BB#12:
	movq	%r14, %rdi
	callq	luaM_toobig
.LBB1_13:
	leaq	25(%r12), %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	luaM_realloc_
	movq	%rax, %rbx
	movq	%r12, 16(%rbx)
	movl	%ebp, 12(%rbx)
	movq	32(%r14), %rax
	movb	32(%rax), %al
	andb	$3, %al
	movb	%al, 9(%rbx)
	movb	$4, 8(%rbx)
	movb	$0, 10(%rbx)
	leaq	24(%rbx), %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	memcpy
	movb	$0, 24(%rbx,%r12)
	movq	32(%r14), %rcx
	movl	12(%rcx), %esi
	leal	-1(%rsi), %eax
	andl	%ebp, %eax
	movq	(%rcx), %rdx
	movq	(%rdx,%rax,8), %rdi
	movq	%rdi, (%rbx)
	movq	%rbx, (%rdx,%rax,8)
	movl	8(%rcx), %eax
	incl	%eax
	movl	%eax, 8(%rcx)
	cmpl	$1073741822, %esi       # imm = 0x3FFFFFFE
	jg	.LBB1_16
# BB#14:
	cmpl	%esi, %eax
	jbe	.LBB1_16
# BB#15:
	addl	%esi, %esi
	movq	%r14, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	luaS_resize
.LBB1_16:                               # %newlstr.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	luaS_newlstr, .Lfunc_end1-luaS_newlstr
	.cfi_endproc

	.hidden	luaS_newudata
	.globl	luaS_newudata
	.p2align	4, 0x90
	.type	luaS_newudata,@function
luaS_newudata:                          # @luaS_newudata
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	cmpq	$-42, %r15
	jb	.LBB2_2
# BB#1:
	movq	%rbx, %rdi
	callq	luaM_toobig
.LBB2_2:
	leaq	40(%r15), %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	luaM_realloc_
	movq	32(%rbx), %rcx
	movb	32(%rcx), %dl
	andb	$3, %dl
	movb	%dl, 9(%rax)
	movb	$7, 8(%rax)
	movq	%r15, 32(%rax)
	movq	$0, 16(%rax)
	movq	%r14, 24(%rax)
	movq	176(%rcx), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, (%rcx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	luaS_newudata, .Lfunc_end2-luaS_newudata
	.cfi_endproc

	.hidden	luaM_realloc_
	.hidden	luaM_toobig

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
