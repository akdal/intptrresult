	.text
	.file	"SetProperties.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	_Z13SetPropertiesP8IUnknownRK13CObjectVectorI9CPropertyE
	.p2align	4, 0x90
	.type	_Z13SetPropertiesP8IUnknownRK13CObjectVectorI9CPropertyE,@function
_Z13SetPropertiesP8IUnknownRK13CObjectVectorI9CPropertyE: # @_Z13SetPropertiesP8IUnknownRK13CObjectVectorI9CPropertyE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi2:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
.Lcfi3:
	.cfi_offset %rbx, -56
.Lcfi4:
	.cfi_offset %r12, -48
.Lcfi5:
	.cfi_offset %r13, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
	movq	%rsi, %rbx
	cmpl	$0, 12(%rbx)
	je	.LBB0_1
# BB#3:
	movq	$0, -80(%rbp)
	movq	(%rdi), %rax
.Ltmp0:
	leaq	-80(%rbp), %rdx
	movl	$IID_ISetProperties, %esi
	callq	*(%rax)
.Ltmp1:
# BB#4:
	cmpq	$0, -80(%rbp)
	je	.LBB0_1
# BB#5:
	xorps	%xmm0, %xmm0
	movups	%xmm0, -120(%rbp)
	movq	$8, -104(%rbp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, -128(%rbp)
	movslq	12(%rbx), %r15
	movl	$16, %ecx
	movq	%r15, %rax
	mulq	%rcx
	pushfq
	popq	%rcx
	addq	$8, %rax
	movq	$-1, %rdi
	cmovbq	%rdi, %rax
	pushq	%rcx
	popfq
	cmovnoq	%rax, %rdi
.Ltmp3:
	callq	_Znam
	movq	%rax, %r12
.Ltmp4:
# BB#6:
	movq	%r15, (%r12)
	leaq	8(%r12), %rax
	movq	%rax, -72(%rbp)         # 8-byte Spill
	testl	%r15d, %r15d
	je	.LBB0_15
# BB#7:
	movq	%r15, %rax
	shlq	$4, %rax
	leaq	-16(%rax), %rdx
	movl	%edx, %esi
	shrl	$4, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB0_8
# BB#9:                                 # %.prol.preheader
	negq	%rsi
	movq	-72(%rbp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_10:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rcx)
	addq	$16, %rcx
	incq	%rsi
	jne	.LBB0_10
	jmp	.LBB0_11
.LBB0_1:
	xorl	%r14d, %r14d
.LBB0_2:
	movl	%r14d, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_8:
	movq	-72(%rbp), %rcx         # 8-byte Reload
.LBB0_11:                               # %.prol.loopexit
	cmpq	$112, %rdx
	jb	.LBB0_14
# BB#12:                                # %.new
	addq	-72(%rbp), %rax         # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB0_13:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 32(%rcx)
	movl	$0, 48(%rcx)
	movl	$0, 64(%rcx)
	movl	$0, 80(%rcx)
	movl	$0, 96(%rcx)
	movl	$0, 112(%rcx)
	subq	$-128, %rcx
	cmpq	%rax, %rcx
	jne	.LBB0_13
.LBB0_14:                               # %.preheader106
	testl	%r15d, %r15d
	jle	.LBB0_15
# BB#16:                                # %.lr.ph132
	xorl	%ecx, %ecx
	leaq	-128(%rbp), %rax
	movq	%rax, -168(%rbp)        # 8-byte Spill
	movq	%rbx, -184(%rbp)        # 8-byte Spill
.LBB0_17:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_22 Depth 2
                                        #     Child Loop BB0_40 Depth 2
                                        #     Child Loop BB0_58 Depth 2
	movq	16(%rbx), %rax
	movq	%rcx, -176(%rbp)        # 8-byte Spill
	movq	(%rax,%rcx,8), %r13
	movl	$0, -96(%rbp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -64(%rbp)
	movl	8(%r13), %r15d
	movslq	%r15d, %rbx
	leaq	1(%rbx), %r14
	testl	%r14d, %r14d
	je	.LBB0_18
# BB#19:                                # %._crit_edge16.i.i
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	%r14, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp6:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp7:
# BB#20:                                # %.noexc61
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	%rax, -64(%rbp)
	movl	$0, (%rax)
	movl	%r14d, -52(%rbp)
	jmp	.LBB0_21
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_17 Depth=1
	xorl	%eax, %eax
.LBB0_21:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	(%r13), %rcx
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB0_22:                               #   Parent Loop BB0_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %esi
	addq	$4, %rcx
	movl	%esi, (%rdx)
	addq	$4, %rdx
	testl	%esi, %esi
	jne	.LBB0_22
# BB#23:                                #   in Loop: Header=BB0_17 Depth=1
	movl	%r15d, -56(%rbp)
	cmpl	$0, 24(%r13)
	je	.LBB0_24
# BB#46:                                #   in Loop: Header=BB0_17 Depth=1
	movq	16(%r13), %rdi
.Ltmp9:
	leaq	-192(%rbp), %rsi
	callq	_Z21ConvertStringToUInt64PKwPS0_
.Ltmp10:
# BB#47:                                # %.noexc76
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	-192(%rbp), %rcx
	movq	16(%r13), %rsi
	subq	%rsi, %rcx
	sarq	$2, %rcx
	movslq	24(%r13), %rdx
	cmpq	%rdx, %rcx
	jne	.LBB0_48
# BB#49:                                #   in Loop: Header=BB0_17 Depth=1
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB0_51
# BB#50:                                #   in Loop: Header=BB0_17 Depth=1
.Ltmp15:
	leaq	-96(%rbp), %rdi
	movl	%eax, %esi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp16:
	jmp	.LBB0_52
	.p2align	4, 0x90
.LBB0_24:                               #   in Loop: Header=BB0_17 Depth=1
	testl	%r15d, %r15d
	je	.LBB0_52
# BB#25:                                #   in Loop: Header=BB0_17 Depth=1
	movl	-4(%rax,%rbx,4), %eax
	cmpl	$43, %eax
	je	.LBB0_29
# BB#26:                                #   in Loop: Header=BB0_17 Depth=1
	cmpl	$45, %eax
	jne	.LBB0_30
# BB#27:                                #   in Loop: Header=BB0_17 Depth=1
.Ltmp19:
	xorl	%esi, %esi
	leaq	-96(%rbp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp20:
	jmp	.LBB0_30
	.p2align	4, 0x90
.LBB0_48:                               #   in Loop: Header=BB0_17 Depth=1
.Ltmp11:
	leaq	-96(%rbp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp12:
	jmp	.LBB0_52
.LBB0_51:                               #   in Loop: Header=BB0_17 Depth=1
.Ltmp13:
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp14:
	jmp	.LBB0_52
.LBB0_29:                               #   in Loop: Header=BB0_17 Depth=1
.Ltmp17:
	movl	$1, %esi
	leaq	-96(%rbp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp18:
.LBB0_30:                               #   in Loop: Header=BB0_17 Depth=1
	cmpw	$0, -96(%rbp)
	je	.LBB0_52
# BB#31:                                #   in Loop: Header=BB0_17 Depth=1
	movl	-56(%rbp), %ecx
	decl	%ecx
.Ltmp22:
	xorl	%edx, %edx
	leaq	-160(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp23:
# BB#32:                                # %_ZNK11CStringBaseIwE4LeftEi.exit
                                        #   in Loop: Header=BB0_17 Depth=1
	movl	$0, -56(%rbp)
	movq	-64(%rbp), %r13
	movl	$0, (%r13)
	movslq	-152(%rbp), %r14
	incq	%r14
	movl	-52(%rbp), %r15d
	cmpl	%r15d, %r14d
	je	.LBB0_39
# BB#33:                                #   in Loop: Header=BB0_17 Depth=1
	movq	%r14, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp25:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp26:
# BB#34:                                # %.noexc74
                                        #   in Loop: Header=BB0_17 Depth=1
	testq	%r13, %r13
	je	.LBB0_35
# BB#36:                                # %.noexc74
                                        #   in Loop: Header=BB0_17 Depth=1
	testl	%r15d, %r15d
	movl	$0, %eax
	jle	.LBB0_38
# BB#37:                                # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	%r13, %rdi
	callq	_ZdaPv
	movslq	-56(%rbp), %rax
	jmp	.LBB0_38
.LBB0_35:                               #   in Loop: Header=BB0_17 Depth=1
	xorl	%eax, %eax
.LBB0_38:                               # %._crit_edge16.i.i70
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	%rbx, -64(%rbp)
	movl	$0, (%rbx,%rax,4)
	movl	%r14d, -52(%rbp)
	movq	%rbx, %r13
.LBB0_39:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i71
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	-160(%rbp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_40:                               #   Parent Loop BB0_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%r13,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_40
# BB#41:                                #   in Loop: Header=BB0_17 Depth=1
	movl	-152(%rbp), %eax
	movl	%eax, -56(%rbp)
	testq	%rdi, %rdi
	je	.LBB0_52
# BB#42:                                #   in Loop: Header=BB0_17 Depth=1
	callq	_ZdaPv
	.p2align	4, 0x90
.LBB0_52:                               #   in Loop: Header=BB0_17 Depth=1
.Ltmp28:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp29:
# BB#53:                                # %.noexc80
                                        #   in Loop: Header=BB0_17 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	movslq	-56(%rbp), %r15
	leaq	1(%r15), %r14
	testl	%r14d, %r14d
	je	.LBB0_54
# BB#55:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	%r14, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp30:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp31:
# BB#56:                                # %.noexc.i
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%r14d, 12(%r13)
	jmp	.LBB0_57
	.p2align	4, 0x90
.LBB0_54:                               #   in Loop: Header=BB0_17 Depth=1
	xorl	%eax, %eax
.LBB0_57:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	-64(%rbp), %rcx
	.p2align	4, 0x90
.LBB0_58:                               #   Parent Loop BB0_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB0_58
# BB#59:                                #   in Loop: Header=BB0_17 Depth=1
	movl	%r15d, 8(%r13)
.Ltmp33:
	leaq	-128(%rbp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp34:
# BB#60:                                #   in Loop: Header=BB0_17 Depth=1
	movq	-112(%rbp), %rax
	movslq	-116(%rbp), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, -116(%rbp)
	movq	-176(%rbp), %rdi        # 8-byte Reload
	shlq	$4, %rdi
	addq	-72(%rbp), %rdi         # 8-byte Folded Reload
.Ltmp35:
	leaq	-96(%rbp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp36:
# BB#61:                                #   in Loop: Header=BB0_17 Depth=1
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_63
# BB#62:                                #   in Loop: Header=BB0_17 Depth=1
	callq	_ZdaPv
.LBB0_63:                               # %_ZN11CStringBaseIwED2Ev.exit84
                                        #   in Loop: Header=BB0_17 Depth=1
.Ltmp40:
	leaq	-96(%rbp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp41:
	movq	-184(%rbp), %rbx        # 8-byte Reload
# BB#64:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit86
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	-176(%rbp), %rcx        # 8-byte Reload
	incq	%rcx
	movslq	12(%rbx), %rax
	cmpq	%rax, %rcx
	jl	.LBB0_17
# BB#65:                                # %._crit_edge133.loopexit
	movl	-116(%rbp), %eax
	jmp	.LBB0_66
.LBB0_15:                               # %.preheader106.._crit_edge133_crit_edge
	xorl	%eax, %eax
	leaq	-128(%rbp), %rcx
	movq	%rcx, -168(%rbp)        # 8-byte Spill
.LBB0_66:                               # %._crit_edge133
	xorps	%xmm0, %xmm0
	movups	%xmm0, -152(%rbp)
	movq	$8, -136(%rbp)
	movq	$_ZTV13CRecordVectorIPKwE+16, -160(%rbp)
	testl	%eax, %eax
	jle	.LBB0_67
# BB#75:                                # %.lr.ph
	xorl	%ebx, %ebx
	leaq	-160(%rbp), %r15
	.p2align	4, 0x90
.LBB0_76:                               # =>This Inner Loop Header: Depth=1
	movq	-112(%rbp), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %r14
.Ltmp43:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp44:
# BB#77:                                #   in Loop: Header=BB0_76 Depth=1
	movq	-144(%rbp), %rax
	movslq	-148(%rbp), %rcx
	movq	%r14, (%rax,%rcx,8)
	leal	1(%rcx), %ecx
	movl	%ecx, -148(%rbp)
	incq	%rbx
	movslq	-116(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_76
# BB#78:                                # %._crit_edge.loopexit
	movq	-144(%rbp), %rsi
	jmp	.LBB0_79
.LBB0_67:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
.LBB0_79:                               # %._crit_edge
	movq	-80(%rbp), %rdi
	movq	(%rdi), %rax
.Ltmp46:
	movq	-72(%rbp), %rdx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	*40(%rax)
	movl	%eax, %r14d
.Ltmp47:
# BB#80:
.Ltmp51:
	leaq	-160(%rbp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp52:
# BB#81:
	testl	%r14d, %r14d
	jne	.LBB0_86
# BB#82:
	movq	(%r12), %rbx
	shlq	$4, %rbx
	je	.LBB0_85
	.p2align	4, 0x90
.LBB0_83:                               # %.preheader100
                                        # =>This Inner Loop Header: Depth=1
	leaq	-8(%r12,%rbx), %rdi
.Ltmp65:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp66:
# BB#84:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit64
                                        #   in Loop: Header=BB0_83 Depth=1
	addq	$-16, %rbx
	jne	.LBB0_83
.LBB0_85:                               # %.loopexit101
	movq	%r12, %rdi
	callq	_ZdaPv
	xorl	%r14d, %r14d
.LBB0_86:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, -128(%rbp)
.Ltmp79:
	movq	-168(%rbp), %rbx        # 8-byte Reload
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp80:
# BB#87:
.Ltmp85:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp86:
# BB#88:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_2
# BB#89:
	movq	(%rdi), %rax
	callq	*16(%rax)
	jmp	.LBB0_2
.LBB0_113:
.Ltmp87:
	jmp	.LBB0_120
.LBB0_111:
.Ltmp81:
	movq	%rax, %r14
.Ltmp82:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp83:
	jmp	.LBB0_121
.LBB0_112:
.Ltmp84:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_93:
.Ltmp53:
	jmp	.LBB0_94
.LBB0_91:                               # %.loopexit.split-lp103
.Ltmp48:
	movq	%rax, %r14
	leaq	-160(%rbp), %r15
	jmp	.LBB0_92
.LBB0_114:
.Ltmp5:
	movq	%rax, %r14
	jmp	.LBB0_115
.LBB0_119:
.Ltmp2:
.LBB0_120:
	movq	%rax, %r14
	jmp	.LBB0_121
.LBB0_44:
.Ltmp27:
	movq	%rax, %r14
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_70
# BB#45:
	callq	_ZdaPv
	jmp	.LBB0_70
.LBB0_43:
.Ltmp24:
	jmp	.LBB0_69
.LBB0_28:
.Ltmp21:
	jmp	.LBB0_69
.LBB0_130:
.Ltmp32:
	movq	%rax, %r14
	movq	%r13, %rdi
	callq	_ZdlPv
	jmp	.LBB0_70
.LBB0_73:
.Ltmp8:
	movq	%rax, %r14
	jmp	.LBB0_74
.LBB0_72:
.Ltmp42:
.LBB0_94:
	movq	%rax, %r14
	jmp	.LBB0_95
.LBB0_106:
.Ltmp67:
	movq	%rax, %r14
	cmpq	$16, %rbx
	jne	.LBB0_108
	jmp	.LBB0_110
	.p2align	4, 0x90
.LBB0_109:                              # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
                                        #   in Loop: Header=BB0_108 Depth=1
	addq	$-16, %rbx
	cmpq	$16, %rbx
	je	.LBB0_110
.LBB0_108:                              # %.preheader95
                                        # =>This Inner Loop Header: Depth=1
	leaq	-24(%r12,%rbx), %rdi
.Ltmp68:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp69:
	jmp	.LBB0_109
.LBB0_110:                              # %.loopexit99
	movq	%r12, %rdi
	callq	_ZdaPv
	jmp	.LBB0_115
.LBB0_125:                              # %.loopexit.split-lp.loopexit
.Ltmp70:
	jmp	.LBB0_127
.LBB0_90:                               # %.loopexit102
.Ltmp45:
	movq	%rax, %r14
.LBB0_92:
.Ltmp49:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp50:
	jmp	.LBB0_95
.LBB0_68:
.Ltmp37:
.LBB0_69:
	movq	%rax, %r14
.LBB0_70:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_74
# BB#71:
	callq	_ZdaPv
.LBB0_74:
.Ltmp38:
	leaq	-96(%rbp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp39:
.LBB0_95:
	movq	%r14, %rdi
	callq	__cxa_begin_catch
	movq	(%r12), %rbx
	shlq	$4, %rbx
	je	.LBB0_98
	.p2align	4, 0x90
.LBB0_96:                               # %.preheader93
                                        # =>This Inner Loop Header: Depth=1
	leaq	-8(%r12,%rbx), %rdi
.Ltmp54:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp55:
# BB#97:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit68
                                        #   in Loop: Header=BB0_96 Depth=1
	addq	$-16, %rbx
	jne	.LBB0_96
.LBB0_98:                               # %.loopexit94
	movq	%r12, %rdi
	callq	_ZdaPv
.Ltmp60:
	callq	__cxa_rethrow
.Ltmp61:
# BB#129:
.LBB0_104:
.Ltmp62:
	movq	%rax, %r14
	jmp	.LBB0_105
.LBB0_99:
.Ltmp56:
	movq	%rax, %r14
	cmpq	$16, %rbx
	jne	.LBB0_101
	jmp	.LBB0_103
	.p2align	4, 0x90
.LBB0_102:                              # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit66
                                        #   in Loop: Header=BB0_101 Depth=1
	addq	$-16, %rbx
	cmpq	$16, %rbx
	je	.LBB0_103
.LBB0_101:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leaq	-24(%r12,%rbx), %rdi
.Ltmp57:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp58:
	jmp	.LBB0_102
.LBB0_103:                              # %.loopexit92
	movq	%r12, %rdi
	callq	_ZdaPv
.LBB0_105:
.Ltmp63:
	callq	__cxa_end_catch
.Ltmp64:
.LBB0_115:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, -128(%rbp)
.Ltmp71:
	leaq	-128(%rbp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp72:
# BB#116:
.Ltmp77:
	leaq	-128(%rbp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp78:
.LBB0_121:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_123
# BB#122:
	movq	(%rdi), %rax
.Ltmp88:
	callq	*16(%rax)
.Ltmp89:
.LBB0_123:                              # %_ZN9CMyComPtrI14ISetPropertiesED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_117:
.Ltmp73:
	movq	%rax, %rbx
.Ltmp74:
	leaq	-128(%rbp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp75:
	jmp	.LBB0_128
.LBB0_118:
.Ltmp76:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_126:                              # %.loopexit.split-lp.loopexit.split-lp
.Ltmp90:
	jmp	.LBB0_127
.LBB0_124:                              # %.loopexit
.Ltmp59:
.LBB0_127:                              # %.body
	movq	%rax, %rbx
.LBB0_128:                              # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_Z13SetPropertiesP8IUnknownRK13CObjectVectorI9CPropertyE, .Lfunc_end0-_Z13SetPropertiesP8IUnknownRK13CObjectVectorI9CPropertyE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\251\203\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\240\003"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp16-.Ltmp9          #   Call between .Ltmp9 and .Ltmp16
	.long	.Ltmp37-.Lfunc_begin0   #     jumps to .Ltmp37
	.byte	1                       #   On action: 1
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin0   #     jumps to .Ltmp21
	.byte	1                       #   On action: 1
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp14-.Ltmp11         #   Call between .Ltmp11 and .Ltmp14
	.long	.Ltmp37-.Lfunc_begin0   #     jumps to .Ltmp37
	.byte	1                       #   On action: 1
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp21-.Lfunc_begin0   #     jumps to .Ltmp21
	.byte	1                       #   On action: 1
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	1                       #   On action: 1
	.long	.Ltmp25-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin0   #     jumps to .Ltmp27
	.byte	1                       #   On action: 1
	.long	.Ltmp28-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp37-.Lfunc_begin0   #     jumps to .Ltmp37
	.byte	1                       #   On action: 1
	.long	.Ltmp30-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin0   #     jumps to .Ltmp32
	.byte	1                       #   On action: 1
	.long	.Ltmp33-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp36-.Ltmp33         #   Call between .Ltmp33 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin0   #     jumps to .Ltmp37
	.byte	1                       #   On action: 1
	.long	.Ltmp40-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin0   #     jumps to .Ltmp42
	.byte	1                       #   On action: 1
	.long	.Ltmp43-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin0   #     jumps to .Ltmp45
	.byte	1                       #   On action: 1
	.long	.Ltmp46-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin0   #     jumps to .Ltmp48
	.byte	1                       #   On action: 1
	.long	.Ltmp51-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin0   #     jumps to .Ltmp53
	.byte	1                       #   On action: 1
	.long	.Ltmp65-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin0   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin0   #     jumps to .Ltmp81
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin0   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp86-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp82-.Ltmp86         #   Call between .Ltmp86 and .Ltmp82
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin0   #     jumps to .Ltmp84
	.byte	1                       #   On action: 1
	.long	.Ltmp68-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin0   #     jumps to .Ltmp70
	.byte	1                       #   On action: 1
	.long	.Ltmp49-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp39-.Ltmp49         #   Call between .Ltmp49 and .Ltmp39
	.long	.Ltmp90-.Lfunc_begin0   #     jumps to .Ltmp90
	.byte	1                       #   On action: 1
	.long	.Ltmp39-.Lfunc_begin0   # >> Call Site 24 <<
	.long	.Ltmp54-.Ltmp39         #   Call between .Ltmp39 and .Ltmp54
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin0   # >> Call Site 25 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin0   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin0   # >> Call Site 26 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin0   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin0   # >> Call Site 27 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin0   #     jumps to .Ltmp59
	.byte	1                       #   On action: 1
	.long	.Ltmp63-.Lfunc_begin0   # >> Call Site 28 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp90-.Lfunc_begin0   #     jumps to .Ltmp90
	.byte	1                       #   On action: 1
	.long	.Ltmp71-.Lfunc_begin0   # >> Call Site 29 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin0   #     jumps to .Ltmp73
	.byte	1                       #   On action: 1
	.long	.Ltmp77-.Lfunc_begin0   # >> Call Site 30 <<
	.long	.Ltmp89-.Ltmp77         #   Call between .Ltmp77 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin0   #     jumps to .Ltmp90
	.byte	1                       #   On action: 1
	.long	.Ltmp89-.Lfunc_begin0   # >> Call Site 31 <<
	.long	.Ltmp74-.Ltmp89         #   Call between .Ltmp89 and .Ltmp74
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin0   # >> Call Site 32 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin0   #     jumps to .Ltmp76
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp91:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp92:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB2_2:
.Ltmp93:
	movq	%rax, %r14
.Ltmp94:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp95:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_4:
.Ltmp96:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end2-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp91-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin1   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp94-.Ltmp92         #   Call between .Ltmp92 and .Ltmp94
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin1   #     jumps to .Ltmp96
	.byte	1                       #   On action: 1
	.long	.Ltmp95-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp95     #   Call between .Ltmp95 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp97:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp98:
# BB#1:
.Ltmp103:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp104:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_5:
.Ltmp105:
	movq	%rax, %r14
	jmp	.LBB3_6
.LBB3_3:
.Ltmp99:
	movq	%rax, %r14
.Ltmp100:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp101:
.LBB3_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_4:
.Ltmp102:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end3-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp97-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin2   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp103-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin2  #     jumps to .Ltmp105
	.byte	0                       #   On action: cleanup
	.long	.Ltmp100-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin2  #     jumps to .Ltmp102
	.byte	1                       #   On action: 1
	.long	.Ltmp101-.Lfunc_begin2  # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp101    #   Call between .Ltmp101 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 64
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB4_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB4_6
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_5
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	callq	_ZdaPv
.LBB4_5:                                # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB4_6:                                #   in Loop: Header=BB4_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB4_2
.LBB4_7:                                # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end4:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end4-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 64
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB5_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB5_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB5_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB5_5
.LBB5_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB5_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp106:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp107:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB5_35
# BB#12:
	movq	%rbx, %r13
.LBB5_13:                               # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB5_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB5_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB5_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB5_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB5_15
.LBB5_14:
	xorl	%esi, %esi
.LBB5_15:                               # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB5_16:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB5_16
.LBB5_29:
	movq	%r13, %rbx
.LBB5_30:                               # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp108:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp109:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB5_32:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB5_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB5_8
.LBB5_3:
	xorl	%eax, %eax
.LBB5_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB5_6:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB5_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB5_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_35:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB5_30
.LBB5_21:                               # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB5_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_24:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB5_24
	jmp	.LBB5_25
.LBB5_22:
	xorl	%ecx, %ecx
.LBB5_25:                               # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB5_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB5_27:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB5_27
.LBB5_28:                               # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB5_15
	jmp	.LBB5_29
.LBB5_34:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp110:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end5-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp106-.Lfunc_begin3  #   Call between .Lfunc_begin3 and .Ltmp106
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp106-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp109-.Ltmp106       #   Call between .Ltmp106 and .Ltmp109
	.long	.Ltmp110-.Lfunc_begin3  #     jumps to .Ltmp110
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Lfunc_end5-.Ltmp109    #   Call between .Ltmp109 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIPKwED0Ev,"axG",@progbits,_ZN13CRecordVectorIPKwED0Ev,comdat
	.weak	_ZN13CRecordVectorIPKwED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIPKwED0Ev,@function
_ZN13CRecordVectorIPKwED0Ev:            # @_ZN13CRecordVectorIPKwED0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -24
.Lcfi48:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp111:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp112:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB6_2:
.Ltmp113:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN13CRecordVectorIPKwED0Ev, .Lfunc_end6-_ZN13CRecordVectorIPKwED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp111-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp112-.Ltmp111       #   Call between .Ltmp111 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin4  #     jumps to .Ltmp113
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Lfunc_end6-.Ltmp112    #   Call between .Ltmp112 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24

	.type	_ZTV13CRecordVectorIPKwE,@object # @_ZTV13CRecordVectorIPKwE
	.section	.rodata._ZTV13CRecordVectorIPKwE,"aG",@progbits,_ZTV13CRecordVectorIPKwE,comdat
	.weak	_ZTV13CRecordVectorIPKwE
	.p2align	3
_ZTV13CRecordVectorIPKwE:
	.quad	0
	.quad	_ZTI13CRecordVectorIPKwE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIPKwED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIPKwE, 40

	.type	_ZTS13CRecordVectorIPKwE,@object # @_ZTS13CRecordVectorIPKwE
	.section	.rodata._ZTS13CRecordVectorIPKwE,"aG",@progbits,_ZTS13CRecordVectorIPKwE,comdat
	.weak	_ZTS13CRecordVectorIPKwE
	.p2align	4
_ZTS13CRecordVectorIPKwE:
	.asciz	"13CRecordVectorIPKwE"
	.size	_ZTS13CRecordVectorIPKwE, 21

	.type	_ZTI13CRecordVectorIPKwE,@object # @_ZTI13CRecordVectorIPKwE
	.section	.rodata._ZTI13CRecordVectorIPKwE,"aG",@progbits,_ZTI13CRecordVectorIPKwE,comdat
	.weak	_ZTI13CRecordVectorIPKwE
	.p2align	4
_ZTI13CRecordVectorIPKwE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPKwE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPKwE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
