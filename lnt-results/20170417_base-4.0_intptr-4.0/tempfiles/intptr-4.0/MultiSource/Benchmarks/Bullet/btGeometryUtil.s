	.text
	.file	"btGeometryUtil.bc"
	.globl	btBulletMathProbe
	.p2align	4, 0x90
	.type	btBulletMathProbe,@function
btBulletMathProbe:                      # @btBulletMathProbe
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	btBulletMathProbe, .Lfunc_end0-btBulletMathProbe
	.cfi_endproc

	.globl	_ZN14btGeometryUtil19isPointInsidePlanesERK20btAlignedObjectArrayI9btVector3ERKS1_f
	.p2align	4, 0x90
	.type	_ZN14btGeometryUtil19isPointInsidePlanesERK20btAlignedObjectArrayI9btVector3ERKS1_f,@function
_ZN14btGeometryUtil19isPointInsidePlanesERK20btAlignedObjectArrayI9btVector3ERKS1_f: # @_ZN14btGeometryUtil19isPointInsidePlanesERK20btAlignedObjectArrayI9btVector3ERKS1_f
	.cfi_startproc
# BB#0:
	movslq	4(%rdi), %rcx
	testq	%rcx, %rcx
	movb	$1, %al
	jle	.LBB1_5
# BB#1:                                 # %.lr.ph
	movq	16(%rdi), %rdx
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	addq	$12, %rdx
	xorl	%esi, %esi
	xorps	%xmm4, %xmm4
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movss	-12(%rdx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	movss	-8(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	addss	%xmm5, %xmm6
	movss	-4(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm6, %xmm5
	addss	(%rdx), %xmm5
	subss	%xmm0, %xmm5
	ucomiss	%xmm4, %xmm5
	ja	.LBB1_4
# BB#2:                                 #   in Loop: Header=BB1_3 Depth=1
	incq	%rsi
	addq	$16, %rdx
	cmpq	%rcx, %rsi
	jl	.LBB1_3
	jmp	.LBB1_5
.LBB1_4:
	xorl	%eax, %eax
.LBB1_5:                                # %._crit_edge
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end1:
	.size	_ZN14btGeometryUtil19isPointInsidePlanesERK20btAlignedObjectArrayI9btVector3ERKS1_f, .Lfunc_end1-_ZN14btGeometryUtil19isPointInsidePlanesERK20btAlignedObjectArrayI9btVector3ERKS1_f
	.cfi_endproc

	.globl	_ZN14btGeometryUtil22areVerticesBehindPlaneERK9btVector3RK20btAlignedObjectArrayIS0_Ef
	.p2align	4, 0x90
	.type	_ZN14btGeometryUtil22areVerticesBehindPlaneERK9btVector3RK20btAlignedObjectArrayIS0_Ef,@function
_ZN14btGeometryUtil22areVerticesBehindPlaneERK9btVector3RK20btAlignedObjectArrayIS0_Ef: # @_ZN14btGeometryUtil22areVerticesBehindPlaneERK9btVector3RK20btAlignedObjectArrayIS0_Ef
	.cfi_startproc
# BB#0:
	movslq	4(%rsi), %rcx
	testq	%rcx, %rcx
	movb	$1, %al
	jle	.LBB2_5
# BB#1:                                 # %.lr.ph
	movq	16(%rsi), %rdx
	movss	(%rdi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	addq	$8, %rdx
	xorl	%esi, %esi
	xorps	%xmm5, %xmm5
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movss	-8(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	movss	-4(%rdx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm7
	addss	%xmm6, %xmm7
	movss	(%rdx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	addss	%xmm7, %xmm6
	addss	%xmm4, %xmm6
	subss	%xmm0, %xmm6
	ucomiss	%xmm5, %xmm6
	ja	.LBB2_4
# BB#2:                                 #   in Loop: Header=BB2_3 Depth=1
	incq	%rsi
	addq	$16, %rdx
	cmpq	%rcx, %rsi
	jl	.LBB2_3
	jmp	.LBB2_5
.LBB2_4:
	xorl	%eax, %eax
.LBB2_5:                                # %._crit_edge
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end2:
	.size	_ZN14btGeometryUtil22areVerticesBehindPlaneERK9btVector3RK20btAlignedObjectArrayIS0_Ef, .Lfunc_end2-_ZN14btGeometryUtil22areVerticesBehindPlaneERK9btVector3RK20btAlignedObjectArrayIS0_Ef
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	1065336439              # float 0.999000012
	.text
	.globl	_Z8notExistRK9btVector3RK20btAlignedObjectArrayIS_E
	.p2align	4, 0x90
	.type	_Z8notExistRK9btVector3RK20btAlignedObjectArrayIS_E,@function
_Z8notExistRK9btVector3RK20btAlignedObjectArrayIS_E: # @_Z8notExistRK9btVector3RK20btAlignedObjectArrayIS_E
	.cfi_startproc
# BB#0:
	movslq	4(%rsi), %rcx
	testq	%rcx, %rcx
	movb	$1, %al
	jle	.LBB3_5
# BB#1:                                 # %.lr.ph
	movq	16(%rsi), %rdx
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	addq	$8, %rdx
	xorl	%esi, %esi
	movss	.LCPI3_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movss	-8(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	-4(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm4, %xmm5
	movss	(%rdx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	addss	%xmm5, %xmm4
	ucomiss	%xmm3, %xmm4
	ja	.LBB3_4
# BB#2:                                 #   in Loop: Header=BB3_3 Depth=1
	incq	%rsi
	addq	$16, %rdx
	cmpq	%rcx, %rsi
	jl	.LBB3_3
	jmp	.LBB3_5
.LBB3_4:
	xorl	%eax, %eax
.LBB3_5:                                # %._crit_edge
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end3:
	.size	_Z8notExistRK9btVector3RK20btAlignedObjectArrayIS_E, .Lfunc_end3-_Z8notExistRK9btVector3RK20btAlignedObjectArrayIS_E
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1065353216              # float 1
.LCPI4_1:
	.long	953267991               # float 9.99999974E-5
.LCPI4_2:
	.long	1065336439              # float 0.999000012
.LCPI4_4:
	.long	3156465418              # float -0.00999999977
.LCPI4_5:
	.long	3212836864              # float -1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN14btGeometryUtil29getPlaneEquationsFromVerticesER20btAlignedObjectArrayI9btVector3ES3_
	.p2align	4, 0x90
	.type	_ZN14btGeometryUtil29getPlaneEquationsFromVerticesER20btAlignedObjectArrayI9btVector3ES3_,@function
_ZN14btGeometryUtil29getPlaneEquationsFromVerticesER20btAlignedObjectArrayI9btVector3ES3_: # @_ZN14btGeometryUtil29getPlaneEquationsFromVerticesER20btAlignedObjectArrayI9btVector3ES3_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 224
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movslq	4(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB4_45
# BB#1:                                 # %.lr.ph131
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%eax, %edx
	movl	$2, %esi
	movl	$1, %edi
	xorl	%ebp, %ebp
	movss	.LCPI4_0(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movss	.LCPI4_1(%rip), %xmm10  # xmm10 = mem[0],zero,zero,zero
	movss	.LCPI4_2(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	.LCPI4_4(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm13, %xmm13
	movl	$1, %r12d
	movss	.LCPI4_5(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_8 Depth 2
                                        #       Child Loop BB4_10 Depth 3
                                        #         Child Loop BB4_11 Depth 4
                                        #           Child Loop BB4_19 Depth 5
                                        #           Child Loop BB4_23 Depth 5
                                        #           Child Loop BB4_33 Depth 5
                                        #           Child Loop BB4_36 Depth 5
	movq	%rbp, %rcx
	leaq	1(%rcx), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	%rdx, %rax
	jge	.LBB4_2
# BB#4:                                 # %.lr.ph129
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	movq	16(%rbx), %rax
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movslq	%esi, %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	shlq	$4, %rcx
	leaq	(%rax,%rcx), %r14
	leaq	4(%rax,%rcx), %r13
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	%rdi, %rcx
	jmp	.LBB4_8
	.p2align	4, 0x90
.LBB4_5:                                # %.loopexit123
                                        #   in Loop: Header=BB4_8 Depth=2
	movq	136(%rsp), %rcx         # 8-byte Reload
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB4_6
# BB#7:                                 # %.loopexit123._crit_edge
                                        #   in Loop: Header=BB4_8 Depth=2
	incq	32(%rsp)                # 8-byte Folded Spill
	movq	16(%rbx), %rax
.LBB4_8:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_10 Depth 3
                                        #         Child Loop BB4_11 Depth 4
                                        #           Child Loop BB4_19 Depth 5
                                        #           Child Loop BB4_23 Depth 5
                                        #           Child Loop BB4_33 Depth 5
                                        #           Child Loop BB4_36 Depth 5
	leaq	1(%rcx), %rdx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	cmpq	8(%rsp), %rdx           # 8-byte Folded Reload
	jge	.LBB4_5
# BB#9:                                 # %.lr.ph
                                        #   in Loop: Header=BB4_8 Depth=2
	shlq	$4, %rcx
	leaq	(%rax,%rcx), %rdx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	leaq	4(%rax,%rcx), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	32(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB4_10
	.p2align	4, 0x90
.LBB4_16:                               #   in Loop: Header=BB4_10 Depth=3
	movq	160(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	cmpl	16(%rsp), %edx          # 4-byte Folded Reload
	je	.LBB4_5
# BB#17:                                # %._crit_edge147
                                        #   in Loop: Header=BB4_10 Depth=3
	movq	16(%rbx), %rax
.LBB4_10:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_11 Depth 4
                                        #           Child Loop BB4_19 Depth 5
                                        #           Child Loop BB4_23 Depth 5
                                        #           Child Loop BB4_33 Depth 5
                                        #           Child Loop BB4_36 Depth 5
	movq	152(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx), %xmm12          # xmm12 = mem[0],zero,zero,zero
	movss	(%r14), %xmm2           # xmm2 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm12
	movq	144(%rsp), %rcx         # 8-byte Reload
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	movsd	(%r13), %xmm6           # xmm6 = mem[0],zero
	subps	%xmm6, %xmm0
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movq	%rdx, %rcx
	shlq	$4, %rcx
	movss	(%rax,%rcx), %xmm9      # xmm9 = mem[0],zero,zero,zero
	movss	4(%rax,%rcx), %xmm1     # xmm1 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm1
	movss	8(%rax,%rcx), %xmm7     # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm7    # xmm7 = xmm7[0],xmm9[0],xmm7[1],xmm9[1]
	pshufd	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	shufps	$0, %xmm6, %xmm2        # xmm2 = xmm2[0,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm2      # xmm2 = xmm2[2,0],xmm6[2,3]
	subps	%xmm2, %xmm7
	movaps	%xmm0, %xmm6
	mulps	%xmm7, %xmm6
	movaps	%xmm12, %xmm2
	mulss	%xmm1, %xmm12
	unpcklps	%xmm7, %xmm1    # xmm1 = xmm1[0],xmm7[0],xmm1[1],xmm7[1]
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	mulss	%xmm0, %xmm7
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	shufps	$0, %xmm0, %xmm2        # xmm2 = xmm2[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm2      # xmm2 = xmm2[2,0],xmm0[2,3]
	mulps	%xmm2, %xmm1
	subps	%xmm1, %xmm6
	subss	%xmm7, %xmm12
	xorl	%ebp, %ebp
	movaps	%xmm8, %xmm7
	movaps	%xmm12, 96(%rsp)        # 16-byte Spill
	movaps	%xmm6, 80(%rsp)         # 16-byte Spill
	.p2align	4, 0x90
.LBB4_11:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        #       Parent Loop BB4_10 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB4_19 Depth 5
                                        #           Child Loop BB4_23 Depth 5
                                        #           Child Loop BB4_33 Depth 5
                                        #           Child Loop BB4_36 Depth 5
	movaps	%xmm12, %xmm5
	mulss	%xmm7, %xmm5
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm6, %xmm7
	movaps	%xmm7, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	%xmm10, %xmm0
	jbe	.LBB4_44
# BB#12:                                #   in Loop: Header=BB4_11 Depth=4
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB4_14
# BB#13:                                # %call.sqrt
                                        #   in Loop: Header=BB4_11 Depth=4
	movaps	%xmm7, 64(%rsp)         # 16-byte Spill
	movaps	%xmm5, 48(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movaps	96(%rsp), %xmm12        # 16-byte Reload
	movss	.LCPI4_5(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	xorps	%xmm13, %xmm13
	movss	.LCPI4_4(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI4_2(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	.LCPI4_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm10
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	movss	.LCPI4_0(%rip), %xmm6   # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm8
	movaps	80(%rsp), %xmm6         # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB4_14:                               # %.split
                                        #   in Loop: Header=BB4_11 Depth=4
	movaps	%xmm8, %xmm0
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm5
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm7
	movaps	%xmm7, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movslq	4(%r15), %rax
	testq	%rax, %rax
	jle	.LBB4_20
# BB#15:                                # %.lr.ph.i58
                                        #   in Loop: Header=BB4_11 Depth=4
	movq	16(%r15), %rcx
	addq	$8, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_19:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        #       Parent Loop BB4_10 Depth=3
                                        #         Parent Loop BB4_11 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movss	-8(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	movss	-4(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	addss	%xmm2, %xmm1
	ucomiss	%xmm4, %xmm1
	ja	.LBB4_44
# BB#18:                                #   in Loop: Header=BB4_19 Depth=5
	incq	%rdx
	addq	$16, %rcx
	cmpq	%rax, %rdx
	jl	.LBB4_19
.LBB4_20:                               # %.loopexit122
                                        #   in Loop: Header=BB4_11 Depth=4
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	movss	(%r13), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movss	4(%r13), %xmm9          # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm9
	addss	%xmm2, %xmm9
	movslq	4(%rbx), %rcx
	testq	%rcx, %rcx
	jle	.LBB4_24
# BB#21:                                # %.lr.ph.i
                                        #   in Loop: Header=BB4_11 Depth=4
	movq	16(%rbx), %rdx
	addq	$8, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_23:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        #       Parent Loop BB4_10 Depth=3
                                        #         Parent Loop BB4_11 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movss	-8(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	movss	-4(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	addss	%xmm2, %xmm1
	subss	%xmm9, %xmm1
	addss	%xmm3, %xmm1
	ucomiss	%xmm13, %xmm1
	ja	.LBB4_44
# BB#22:                                #   in Loop: Header=BB4_23 Depth=5
	incq	%rsi
	addq	$16, %rdx
	cmpq	%rcx, %rsi
	jl	.LBB4_23
.LBB4_24:                               # %.loopexit
                                        #   in Loop: Header=BB4_11 Depth=4
	xorps	.LCPI4_3(%rip), %xmm9
	shufps	$0, %xmm5, %xmm9        # xmm9 = xmm9[0,0],xmm5[0,0]
	shufps	$226, %xmm5, %xmm9      # xmm9 = xmm9[2,0],xmm5[2,3]
	cmpl	8(%r15), %eax
	jne	.LBB4_43
# BB#25:                                #   in Loop: Header=BB4_11 Depth=4
	leal	(%rax,%rax), %r12d
	testl	%eax, %eax
	movl	$1, %ecx
	cmovel	%ecx, %r12d
	cmpl	%r12d, %eax
	jge	.LBB4_42
# BB#26:                                #   in Loop: Header=BB4_11 Depth=4
	testl	%r12d, %r12d
	movaps	%xmm7, 64(%rsp)         # 16-byte Spill
	movaps	%xmm9, 48(%rsp)         # 16-byte Spill
	je	.LBB4_27
# BB#28:                                #   in Loop: Header=BB4_11 Depth=4
	movslq	%r12d, %rdi
	shlq	$4, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movaps	48(%rsp), %xmm9         # 16-byte Reload
	movaps	96(%rsp), %xmm12        # 16-byte Reload
	movss	.LCPI4_5(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	xorps	%xmm13, %xmm13
	movss	.LCPI4_4(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI4_2(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	.LCPI4_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm10
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	movss	.LCPI4_0(%rip), %xmm6   # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm8
	movaps	80(%rsp), %xmm6         # 16-byte Reload
	movq	%rax, %rbx
	movl	4(%r15), %eax
	testl	%eax, %eax
	jg	.LBB4_30
	jmp	.LBB4_37
.LBB4_27:                               #   in Loop: Header=BB4_11 Depth=4
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB4_37
.LBB4_30:                               # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB4_11 Depth=4
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB4_31
# BB#32:                                # %.prol.preheader
                                        #   in Loop: Header=BB4_11 Depth=4
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_33:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        #       Parent Loop BB4_10 Depth=3
                                        #         Parent Loop BB4_11 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	16(%r15), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%rbx,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB4_33
	jmp	.LBB4_34
.LBB4_31:                               #   in Loop: Header=BB4_11 Depth=4
	xorl	%ecx, %ecx
.LBB4_34:                               # %.prol.loopexit
                                        #   in Loop: Header=BB4_11 Depth=4
	cmpq	$3, %r8
	jb	.LBB4_37
# BB#35:                                # %.lr.ph.i.i.i.new
                                        #   in Loop: Header=BB4_11 Depth=4
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB4_36:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        #       Parent Loop BB4_10 Depth=3
                                        #         Parent Loop BB4_11 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	16(%r15), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbx,%rcx)
	movq	16(%r15), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbx,%rcx)
	movq	16(%r15), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbx,%rcx)
	movq	16(%r15), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbx,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB4_36
.LBB4_37:                               # %_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_.exit.i.i
                                        #   in Loop: Header=BB4_11 Depth=4
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_41
# BB#38:                                #   in Loop: Header=BB4_11 Depth=4
	cmpb	$0, 24(%r15)
	je	.LBB4_40
# BB#39:                                #   in Loop: Header=BB4_11 Depth=4
	callq	_Z21btAlignedFreeInternalPv
	movaps	48(%rsp), %xmm9         # 16-byte Reload
	movaps	96(%rsp), %xmm12        # 16-byte Reload
	movss	.LCPI4_5(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	xorps	%xmm13, %xmm13
	movss	.LCPI4_4(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI4_2(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	.LCPI4_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm10
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	movss	.LCPI4_0(%rip), %xmm6   # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm8
	movaps	80(%rsp), %xmm6         # 16-byte Reload
.LBB4_40:                               #   in Loop: Header=BB4_11 Depth=4
	movq	$0, 16(%r15)
.LBB4_41:                               # %_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB4_11 Depth=4
	movb	$1, 24(%r15)
	movq	%rbx, 16(%r15)
	movl	%r12d, 8(%r15)
	movl	4(%r15), %eax
.LBB4_42:                               # %_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_.exit
                                        #   in Loop: Header=BB4_11 Depth=4
	movl	$1, %r12d
	movq	40(%rsp), %rbx          # 8-byte Reload
.LBB4_43:                               # %_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_.exit
                                        #   in Loop: Header=BB4_11 Depth=4
	movq	16(%r15), %rcx
	cltq
	shlq	$4, %rax
	movlps	%xmm7, (%rcx,%rax)
	movlps	%xmm9, 8(%rcx,%rax)
	incl	4(%r15)
.LBB4_44:                               # %_Z8notExistRK9btVector3RK20btAlignedObjectArrayIS_E.exit
                                        #   in Loop: Header=BB4_11 Depth=4
	incl	%ebp
	cmpl	$2, %ebp
	movaps	%xmm11, %xmm7
	jne	.LBB4_11
	jmp	.LBB4_16
	.p2align	4, 0x90
.LBB4_6:                                #   in Loop: Header=BB4_3 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	120(%rsp), %rbp         # 8-byte Reload
.LBB4_2:                                # %.loopexit124
                                        #   in Loop: Header=BB4_3 Depth=1
	incl	%esi
	incq	%rdi
	cmpq	%rax, %rbp
	jne	.LBB4_3
.LBB4_45:                               # %._crit_edge
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN14btGeometryUtil29getPlaneEquationsFromVerticesER20btAlignedObjectArrayI9btVector3ES3_, .Lfunc_end4-_ZN14btGeometryUtil29getPlaneEquationsFromVerticesER20btAlignedObjectArrayI9btVector3ES3_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	953267991               # float 9.99999974E-5
.LCPI5_2:
	.long	897988541               # float 9.99999997E-7
.LCPI5_3:
	.long	3212836864              # float -1
.LCPI5_4:
	.long	3156465418              # float -0.00999999977
.LCPI5_5:
	.long	0                       # float 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZN14btGeometryUtil29getVerticesFromPlaneEquationsERK20btAlignedObjectArrayI9btVector3ERS2_
	.p2align	4, 0x90
	.type	_ZN14btGeometryUtil29getVerticesFromPlaneEquationsERK20btAlignedObjectArrayI9btVector3ERS2_,@function
_ZN14btGeometryUtil29getVerticesFromPlaneEquationsERK20btAlignedObjectArrayI9btVector3ERS2_: # @_ZN14btGeometryUtil29getVerticesFromPlaneEquationsERK20btAlignedObjectArrayI9btVector3ERS2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 160
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movslq	4(%rbp), %r9
	testq	%r9, %r9
	jle	.LBB5_39
# BB#1:                                 # %.lr.ph165
	movl	%r9d, %ebx
	movl	%r9d, %r8d
	movl	$2, %edx
	movl	$1, %r15d
	xorl	%edi, %edi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movq	%r8, 48(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB5_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_8 Depth 2
                                        #       Child Loop BB5_10 Depth 3
                                        #         Child Loop BB5_17 Depth 4
                                        #         Child Loop BB5_27 Depth 4
                                        #         Child Loop BB5_30 Depth 4
	movq	%rdi, %r10
	leaq	1(%r10), %rdi
	cmpq	%r9, %rdi
	movq	%r8, %rax
	jge	.LBB5_2
# BB#4:                                 # %.lr.ph163
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	16(%rbp), %rax
	movl	%edx, 28(%rsp)          # 4-byte Spill
	movslq	%edx, %rdi
	shlq	$4, %r10
	leaq	8(%rax,%r10), %r12
	addq	%rax, %r10
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movq	%r10, 40(%rsp)          # 8-byte Spill
	jmp	.LBB5_8
	.p2align	4, 0x90
.LBB5_7:                                # %.loopexit159._crit_edge
                                        #   in Loop: Header=BB5_8 Depth=2
	incq	%rdi
	movq	16(%rbp), %rax
	movq	%r11, %r15
.LBB5_8:                                #   Parent Loop BB5_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_10 Depth 3
                                        #         Child Loop BB5_17 Depth 4
                                        #         Child Loop BB5_27 Depth 4
                                        #         Child Loop BB5_30 Depth 4
	leaq	1(%r15), %r11
	cmpq	%r9, %r11
	jge	.LBB5_5
# BB#9:                                 # %.lr.ph
                                        #   in Loop: Header=BB5_8 Depth=2
	shlq	$4, %r15
	leaq	4(%rax,%r15), %r14
	addq	%rax, %r15
	movq	%rdi, %r13
	jmp	.LBB5_10
	.p2align	4, 0x90
.LBB5_38:                               # %_ZN14btGeometryUtil19isPointInsidePlanesERK20btAlignedObjectArrayI9btVector3ERKS1_f.exit._crit_edge
                                        #   in Loop: Header=BB5_10 Depth=3
	movq	16(%rbp), %rax
.LBB5_10:                               #   Parent Loop BB5_3 Depth=1
                                        #     Parent Loop BB5_8 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB5_17 Depth 4
                                        #         Child Loop BB5_27 Depth 4
                                        #         Child Loop BB5_30 Depth 4
	movss	(%r14), %xmm13          # xmm13 = mem[0],zero,zero,zero
	movq	%r13, %rcx
	shlq	$4, %rcx
	movss	8(%rax,%rcx), %xmm4     # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm11
	mulss	%xmm4, %xmm11
	movss	4(%r14), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	(%rax,%rcx), %xmm6      # xmm6 = mem[0],zero,zero,zero
	movss	4(%rax,%rcx), %xmm1     # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm11
	movaps	%xmm5, %xmm2
	mulss	%xmm6, %xmm2
	movss	(%r15), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm8
	movaps	%xmm4, %xmm0
	mulss	%xmm7, %xmm0
	subss	%xmm0, %xmm2
	movaps	%xmm1, %xmm0
	mulss	%xmm7, %xmm0
	movaps	%xmm13, %xmm4
	movaps	%xmm6, %xmm9
	mulss	%xmm6, %xmm4
	subss	%xmm4, %xmm0
	movaps	%xmm11, %xmm4
	mulss	%xmm4, %xmm4
	movaps	%xmm2, %xmm6
	mulss	%xmm6, %xmm6
	addss	%xmm4, %xmm6
	movaps	%xmm0, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm6, %xmm4
	ucomiss	.LCPI5_0(%rip), %xmm4
	jbe	.LBB5_37
# BB#11:                                #   in Loop: Header=BB5_10 Depth=3
	movss	(%r12), %xmm10          # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	mulss	%xmm10, %xmm3
	movss	-4(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm4
	mulss	%xmm6, %xmm4
	subss	%xmm4, %xmm3
	movss	(%r10), %xmm12          # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm8
	movaps	%xmm9, %xmm4
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm8
	mulss	%xmm6, %xmm9
	mulss	%xmm12, %xmm1
	subss	%xmm1, %xmm9
	movaps	%xmm3, %xmm1
	mulss	%xmm3, %xmm3
	movaps	%xmm8, %xmm14
	movaps	%xmm8, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm3, %xmm4
	movaps	%xmm9, %xmm15
	movaps	%xmm9, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm4, %xmm3
	ucomiss	.LCPI5_0(%rip), %xmm3
	jbe	.LBB5_37
# BB#12:                                #   in Loop: Header=BB5_10 Depth=3
	movaps	%xmm5, %xmm8
	mulss	%xmm6, %xmm8
	movaps	%xmm13, %xmm3
	mulss	%xmm10, %xmm3
	subss	%xmm3, %xmm8
	movaps	%xmm7, %xmm9
	mulss	%xmm10, %xmm9
	mulss	%xmm12, %xmm5
	subss	%xmm5, %xmm9
	mulss	%xmm12, %xmm13
	mulss	%xmm6, %xmm7
	subss	%xmm7, %xmm13
	movaps	%xmm8, %xmm3
	mulss	%xmm3, %xmm3
	movaps	%xmm9, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm3, %xmm4
	movaps	%xmm13, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm4, %xmm3
	ucomiss	.LCPI5_0(%rip), %xmm3
	jbe	.LBB5_37
# BB#13:                                #   in Loop: Header=BB5_10 Depth=3
	mulss	%xmm11, %xmm12
	mulss	%xmm2, %xmm6
	addss	%xmm12, %xmm6
	mulss	%xmm0, %xmm10
	addss	%xmm6, %xmm10
	movaps	%xmm10, %xmm3
	andps	.LCPI5_1(%rip), %xmm3
	ucomiss	.LCPI5_2(%rip), %xmm3
	jbe	.LBB5_37
# BB#14:                                #   in Loop: Header=BB5_10 Depth=3
	movss	.LCPI5_3(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	divss	%xmm10, %xmm3
	movss	4(%r12), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm11
	mulss	%xmm4, %xmm2
	mulss	%xmm4, %xmm0
	movss	8(%r14), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	mulss	%xmm4, %xmm14
	mulss	%xmm4, %xmm15
	movss	12(%rax,%rcx), %xmm4    # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm8
	mulss	%xmm4, %xmm9
	mulss	%xmm4, %xmm13
	addss	%xmm11, %xmm1
	addss	%xmm2, %xmm14
	addss	%xmm0, %xmm15
	addss	%xmm8, %xmm1
	addss	%xmm9, %xmm14
	addss	%xmm13, %xmm15
	mulss	%xmm3, %xmm1
	movss	%xmm1, 16(%rsp)         # 4-byte Spill
	mulss	%xmm3, %xmm14
	movss	%xmm14, 24(%rsp)        # 4-byte Spill
	mulss	%xmm3, %xmm15
	movss	%xmm15, 20(%rsp)        # 4-byte Spill
	movslq	4(%rbp), %rcx
	testq	%rcx, %rcx
	jle	.LBB5_18
# BB#15:                                # %.lr.ph.i
                                        #   in Loop: Header=BB5_10 Depth=3
	addq	$12, %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_17:                               #   Parent Loop BB5_3 Depth=1
                                        #     Parent Loop BB5_8 Depth=2
                                        #       Parent Loop BB5_10 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movss	-12(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	16(%rsp), %xmm0         # 4-byte Folded Reload
	movss	-8(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	24(%rsp), %xmm1         # 4-byte Folded Reload
	addss	%xmm0, %xmm1
	movss	-4(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	20(%rsp), %xmm0         # 4-byte Folded Reload
	addss	%xmm1, %xmm0
	addss	(%rax), %xmm0
	addss	.LCPI5_4(%rip), %xmm0
	ucomiss	.LCPI5_5, %xmm0
	ja	.LBB5_37
# BB#16:                                #   in Loop: Header=BB5_17 Depth=4
	incq	%rdx
	addq	$16, %rax
	cmpq	%rcx, %rdx
	jl	.LBB5_17
.LBB5_18:                               # %.loopexit
                                        #   in Loop: Header=BB5_10 Depth=3
	movl	4(%rsi), %eax
	cmpl	8(%rsi), %eax
	jne	.LBB5_36
# BB#19:                                #   in Loop: Header=BB5_10 Depth=3
	leal	(%rax,%rax), %edx
	testl	%eax, %eax
	movl	$1, %ecx
	cmovel	%ecx, %edx
	cmpl	%edx, %eax
	jge	.LBB5_36
# BB#20:                                #   in Loop: Header=BB5_10 Depth=3
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	testl	%edx, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	je	.LBB5_21
# BB#22:                                #   in Loop: Header=BB5_10 Depth=3
	movslq	%edx, %rdi
	shlq	$4, %rdi
	movl	$16, %esi
	movq	%r11, %rbx
	callq	_Z22btAlignedAllocInternalmi
	movq	%rbx, %r11
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rax, %rbx
	movl	4(%rsi), %eax
	testl	%eax, %eax
	jg	.LBB5_24
	jmp	.LBB5_31
.LBB5_21:                               #   in Loop: Header=BB5_10 Depth=3
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB5_31
.LBB5_24:                               # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB5_10 Depth=3
	movq	%rbp, %r9
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rbp
	andq	$3, %rbp
	je	.LBB5_25
# BB#26:                                # %.prol.preheader
                                        #   in Loop: Header=BB5_10 Depth=3
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_27:                               #   Parent Loop BB5_3 Depth=1
                                        #     Parent Loop BB5_8 Depth=2
                                        #       Parent Loop BB5_10 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%rsi), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%rbx,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rbp
	jne	.LBB5_27
	jmp	.LBB5_28
.LBB5_25:                               #   in Loop: Header=BB5_10 Depth=3
	xorl	%ecx, %ecx
.LBB5_28:                               # %.prol.loopexit
                                        #   in Loop: Header=BB5_10 Depth=3
	cmpq	$3, %r8
	movq	%r9, %rbp
	jb	.LBB5_31
# BB#29:                                # %.lr.ph.i.i.i.new
                                        #   in Loop: Header=BB5_10 Depth=3
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB5_30:                               #   Parent Loop BB5_3 Depth=1
                                        #     Parent Loop BB5_8 Depth=2
                                        #       Parent Loop BB5_10 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%rsi), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbx,%rcx)
	movq	16(%rsi), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbx,%rcx)
	movq	16(%rsi), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbx,%rcx)
	movq	16(%rsi), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbx,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB5_30
.LBB5_31:                               # %_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_.exit.i.i
                                        #   in Loop: Header=BB5_10 Depth=3
	movq	16(%rsi), %rdi
	testq	%rdi, %rdi
	movl	12(%rsp), %eax          # 4-byte Reload
	je	.LBB5_35
# BB#32:                                #   in Loop: Header=BB5_10 Depth=3
	cmpb	$0, 24(%rsi)
	je	.LBB5_34
# BB#33:                                #   in Loop: Header=BB5_10 Depth=3
	movq	%r11, 72(%rsp)          # 8-byte Spill
	callq	_Z21btAlignedFreeInternalPv
	movl	12(%rsp), %eax          # 4-byte Reload
	movq	72(%rsp), %r11          # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
.LBB5_34:                               #   in Loop: Header=BB5_10 Depth=3
	movq	$0, 16(%rsi)
.LBB5_35:                               # %_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB5_10 Depth=3
	movb	$1, 24(%rsi)
	movq	%rbx, 16(%rsi)
	movl	%eax, 8(%rsi)
	movl	4(%rsi), %eax
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	96(%rsp), %rdi          # 8-byte Reload
.LBB5_36:                               # %_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_.exit
                                        #   in Loop: Header=BB5_10 Depth=3
	movq	16(%rsi), %rcx
	cltq
	shlq	$4, %rax
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, (%rcx,%rax)
	movss	24(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 4(%rcx,%rax)
	movss	20(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rcx,%rax)
	movl	$0, 12(%rcx,%rax)
	incl	4(%rsi)
	.p2align	4, 0x90
.LBB5_37:                               # %_ZN14btGeometryUtil19isPointInsidePlanesERK20btAlignedObjectArrayI9btVector3ERKS1_f.exit
                                        #   in Loop: Header=BB5_10 Depth=3
	incq	%r13
	cmpl	%ebx, %r13d
	jne	.LBB5_38
.LBB5_5:                                # %.loopexit159
                                        #   in Loop: Header=BB5_8 Depth=2
	cmpq	%rbx, %r11
	jne	.LBB5_7
# BB#6:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	%rbx, %rax
	movq	48(%rsp), %r8           # 8-byte Reload
	movl	28(%rsp), %edx          # 4-byte Reload
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	56(%rsp), %rdi          # 8-byte Reload
.LBB5_2:                                # %.loopexit160
                                        #   in Loop: Header=BB5_3 Depth=1
	incl	%edx
	incq	%r15
	cmpq	%rax, %rdi
	jne	.LBB5_3
.LBB5_39:                               # %._crit_edge
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN14btGeometryUtil29getVerticesFromPlaneEquationsERK20btAlignedObjectArrayI9btVector3ERS2_, .Lfunc_end5-_ZN14btGeometryUtil29getVerticesFromPlaneEquationsERK20btAlignedObjectArrayI9btVector3ERS2_
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
