	.text
	.file	"Sha1.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1732584193              # 0x67452301
	.long	4023233417              # 0xefcdab89
	.long	2562383102              # 0x98badcfe
	.long	271733878               # 0x10325476
	.text
	.globl	_ZN7NCrypto5NSha112CContextBase4InitEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto5NSha112CContextBase4InitEv,@function
_ZN7NCrypto5NSha112CContextBase4InitEv: # @_ZN7NCrypto5NSha112CContextBase4InitEv
	.cfi_startproc
# BB#0:
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1732584193,4023233417,2562383102,271733878]
	movups	%xmm0, (%rdi)
	movl	$-1009589776, 16(%rdi)  # imm = 0xC3D2E1F0
	movq	$0, 24(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN7NCrypto5NSha112CContextBase4InitEv, .Lfunc_end0-_ZN7NCrypto5NSha112CContextBase4InitEv
	.cfi_endproc

	.globl	_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b
	.p2align	4, 0x90
	.type	_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b,@function
_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b: # @_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 288
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, -128(%rsp)        # 4-byte Spill
	movq	%rdx, -104(%rsp)        # 8-byte Spill
	movl	(%rdi), %r13d
	movl	4(%rdi), %r11d
	movl	8(%rdi), %r8d
	movl	12(%rdi), %ebx
	movq	%rdi, -112(%rsp)        # 8-byte Spill
	movl	16(%rdi), %ecx
	movl	%ebx, %ebp
	xorl	%r8d, %ebp
	andl	%r11d, %ebp
	xorl	%ebx, %ebp
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movl	%r13d, %eax
	roll	$5, %eax
	addl	%ecx, %eax
	addl	%ebp, %eax
	movdqu	(%rsi,%r9,4), %xmm0
	movd	%xmm0, %ecx
	leal	1518500249(%rcx,%rax), %ecx
	roll	$30, %r11d
	movl	%r11d, %eax
	xorl	%r8d, %eax
	andl	%r13d, %eax
	xorl	%r8d, %eax
	movl	%ecx, %edx
	roll	$5, %edx
	addl	%ebx, %eax
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	movd	%xmm1, %edi
	addl	%eax, %edi
	leal	1518500249(%rdx,%rdi), %ebx
	roll	$30, %r13d
	movl	%r11d, %eax
	xorl	%r13d, %eax
	andl	%ecx, %eax
	xorl	%r11d, %eax
	movl	%ebx, %edx
	roll	$5, %edx
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	movd	%xmm1, %ebp
	addl	%r8d, %ebp
	addl	%eax, %ebp
	leal	1518500249(%rdx,%rbp), %r8d
	roll	$30, %ecx
	movl	%ecx, %eax
	xorl	%r13d, %eax
	andl	%ebx, %eax
	xorl	%r13d, %eax
	movdqu	%xmm0, -96(%rsp,%r9,4)
	movl	%r8d, %edx
	roll	$5, %edx
	pshufd	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3]
	movd	%xmm0, %ebp
	addl	%r11d, %ebp
	addl	%eax, %ebp
	leal	1518500249(%rdx,%rbp), %r11d
	roll	$30, %ebx
	movl	%ebx, %eax
	xorl	%ecx, %eax
	andl	%r8d, %eax
	xorl	%ecx, %eax
	movl	16(%rsi,%r9,4), %edx
	movl	%edx, -80(%rsp,%r9,4)
	movl	%r11d, %ebp
	roll	$5, %ebp
	addl	%edx, %r13d
	addl	%eax, %r13d
	leal	1518500249(%rbp,%r13), %r13d
	roll	$30, %r8d
	addq	$5, %r9
	movl	%ebx, %ebp
	xorl	%r8d, %ebp
	andl	%r11d, %ebp
	xorl	%ebx, %ebp
	cmpq	$15, %r9
	jl	.LBB1_1
# BB#2:
	movq	%rsi, -120(%rsp)        # 8-byte Spill
	movl	60(%rsi), %eax
	movl	%eax, -36(%rsp)
	movl	%r13d, %edx
	roll	$5, %edx
	addl	%ecx, %edx
	addl	%ebp, %edx
	leal	1518500249(%rax,%rdx), %ebp
	roll	$30, %r11d
	movl	%r11d, %edx
	xorl	%r8d, %edx
	andl	%r13d, %edx
	xorl	%r8d, %edx
	movl	-88(%rsp), %r14d
	movl	-64(%rsp), %edi
	xorl	-44(%rsp), %edi
	xorl	%r14d, %edi
	xorl	-96(%rsp), %edi
	roll	%edi
	movl	%edi, -32(%rsp)
	movl	%ebp, %esi
	roll	$5, %esi
	addl	%edx, %ebx
	addl	%esi, %ebx
	leal	1518500249(%rdi,%rbx), %r9d
	roll	$30, %r13d
	movl	%r11d, %edx
	xorl	%r13d, %edx
	andl	%ebp, %edx
	xorl	%r11d, %edx
	movl	-84(%rsp), %r12d
	movl	-40(%rsp), %ecx
	movl	-60(%rsp), %r15d
	xorl	%ecx, %r15d
	xorl	%r12d, %r15d
	xorl	-92(%rsp), %r15d
	roll	%r15d
	movl	%r15d, -28(%rsp)
	movl	%r9d, %esi
	roll	$5, %esi
	addl	%r8d, %edx
	addl	%r15d, %edx
	leal	1518500249(%rsi,%rdx), %r10d
	roll	$30, %ebp
	movl	%ebp, %edx
	movl	%ebp, %ebx
	xorl	%r13d, %edx
	andl	%r9d, %edx
	xorl	%r13d, %edx
	xorl	-56(%rsp), %eax
	movl	-80(%rsp), %ebp
	xorl	%ebp, %r14d
	xorl	%eax, %r14d
	roll	%r14d
	movl	%r14d, -24(%rsp)
	movl	%r10d, %eax
	roll	$5, %eax
	addl	%r11d, %edx
	addl	%r14d, %edx
	leal	1518500249(%rax,%rdx), %r11d
	roll	$30, %r9d
	movl	%r9d, %eax
	xorl	%ebx, %eax
	andl	%r10d, %eax
	movl	%ebx, %esi
	xorl	%ebx, %eax
	xorl	-52(%rsp), %edi
	movl	-76(%rsp), %ebx
	xorl	%ebx, %r12d
	xorl	%edi, %r12d
	roll	%r12d
	movl	%r12d, -20(%rsp)
	movl	%r11d, %edx
	roll	$5, %edx
	addl	%r13d, %eax
	addl	%r12d, %eax
	leal	1518500249(%rdx,%rax), %r8d
	roll	$30, %r10d
	movl	$25, %r13d
	jmp	.LBB1_3
	.p2align	4, 0x90
.LBB1_4:                                # %._crit_edge491
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	-120(%rsp,%r13,4), %ecx
	addq	$5, %r13
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movl	%r10d, %eax
	xorl	%r11d, %eax
	xorl	%r9d, %eax
	xorl	-148(%rsp,%r13,4), %r15d
	movl	-172(%rsp,%r13,4), %edx
	movl	%edx, -124(%rsp)        # 4-byte Spill
	xorl	%edx, %ebp
	xorl	%r15d, %ebp
	roll	%ebp
	movl	%ebp, -116(%rsp,%r13,4)
	movl	%r8d, %edx
	roll	$5, %edx
	addl	%eax, %edx
	addl	%esi, %edx
	leal	1859775393(%rbp,%rdx), %edi
	roll	$30, %r11d
	movl	%r10d, %edx
	xorl	%r8d, %edx
	xorl	%r11d, %edx
	xorl	-144(%rsp,%r13,4), %r14d
	movl	-168(%rsp,%r13,4), %eax
	xorl	%eax, %ebx
	xorl	%r14d, %ebx
	roll	%ebx
	movl	%ebx, -112(%rsp,%r13,4)
	movl	%edi, %esi
	roll	$5, %esi
	addl	%r9d, %edx
	addl	%ebx, %edx
	leal	1859775393(%rsi,%rdx), %r9d
	roll	$30, %r8d
	movl	%r11d, %edx
	xorl	%r8d, %edx
	xorl	%edi, %edx
	xorl	%r12d, %ecx
	movl	-164(%rsp,%r13,4), %r12d
	xorl	%r12d, %ecx
	xorl	-124(%rsp), %ecx        # 4-byte Folded Reload
	movl	%ecx, %r15d
	roll	%r15d
	movl	%r15d, -108(%rsp,%r13,4)
	movl	%r9d, %ecx
	roll	$5, %ecx
	addl	%r10d, %edx
	addl	%r15d, %edx
	leal	1859775393(%rcx,%rdx), %r10d
	roll	$30, %edi
	movl	%edi, %ecx
	xorl	%r8d, %ecx
	xorl	%r9d, %ecx
	movl	%ebp, %edx
	xorl	-136(%rsp,%r13,4), %edx
	movl	-160(%rsp,%r13,4), %ebp
	xorl	%ebp, %eax
	xorl	%edx, %eax
	movl	%eax, %r14d
	roll	%r14d
	movl	%r14d, -104(%rsp,%r13,4)
	movl	%r10d, %eax
	roll	$5, %eax
	addl	%r11d, %ecx
	addl	%r14d, %ecx
	leal	1859775393(%rax,%rcx), %r11d
	roll	$30, %r9d
	movl	%r9d, %eax
	movl	%edi, %esi
	xorl	%edi, %eax
	xorl	%r10d, %eax
	movl	%ebx, %ecx
	xorl	-132(%rsp,%r13,4), %ecx
	movl	-156(%rsp,%r13,4), %ebx
	xorl	%ebx, %r12d
	xorl	%ecx, %r12d
	roll	%r12d
	movl	%r12d, -100(%rsp,%r13,4)
	movl	%r11d, %ecx
	roll	$5, %ecx
	addl	%r8d, %eax
	addl	%r12d, %eax
	leal	1859775393(%rcx,%rax), %r8d
	roll	$30, %r10d
	cmpq	$39, %r13
	jle	.LBB1_4
# BB#10:                                # %.lr.ph443.preheader
	movl	52(%rsp), %ecx
	movl	(%rsp), %edx
	movl	4(%rsp), %edi
	movl	56(%rsp), %eax
	movl	60(%rsp), %r12d
	movl	$44, %ebp
	movl	%esi, %r13d
	.p2align	4, 0x90
.LBB1_11:                               # %.lr.ph443
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rbx
	movl	%r10d, %ebp
	andl	%r11d, %ebp
	movl	%r10d, %esi
	orl	%r11d, %esi
	andl	%r9d, %esi
	orl	%ebp, %esi
	xorl	-144(%rsp,%rbx,4), %ecx
	movl	-168(%rsp,%rbx,4), %r14d
	xorl	%r14d, %edx
	xorl	%ecx, %edx
	roll	%edx
	movl	%edx, -112(%rsp,%rbx,4)
	movl	%r8d, %ecx
	roll	$5, %ecx
	addl	%r13d, %ecx
	addl	%esi, %ecx
	leal	-1894007588(%rdx,%rcx), %r13d
	roll	$30, %r11d
	movl	%r11d, %ecx
	andl	%r8d, %ecx
	movl	%r11d, %esi
	orl	%r8d, %esi
	andl	%r10d, %esi
	orl	%ecx, %esi
	xorl	-140(%rsp,%rbx,4), %eax
	movl	-164(%rsp,%rbx,4), %ecx
	xorl	%ecx, %edi
	xorl	%eax, %edi
	roll	%edi
	movl	%edi, -108(%rsp,%rbx,4)
	movl	%r13d, %eax
	roll	$5, %eax
	addl	%r9d, %esi
	addl	%edi, %esi
	leal	-1894007588(%rax,%rsi), %r9d
	roll	$30, %r8d
	movl	%r13d, %eax
	andl	%r8d, %eax
	movl	%r13d, %esi
	orl	%r8d, %esi
	andl	%r11d, %esi
	orl	%eax, %esi
	xorl	-136(%rsp,%rbx,4), %r12d
	movl	-160(%rsp,%rbx,4), %eax
	xorl	%eax, %r14d
	xorl	%r12d, %r14d
	roll	%r14d
	movl	%r14d, -104(%rsp,%rbx,4)
	movl	%r9d, %ebp
	roll	$5, %ebp
	addl	%r10d, %esi
	addl	%r14d, %esi
	leal	-1894007588(%rbp,%rsi), %r10d
	roll	$30, %r13d
	movl	%r9d, %esi
	andl	%r13d, %esi
	movl	%r9d, %ebp
	orl	%r13d, %ebp
	andl	%r8d, %ebp
	orl	%esi, %ebp
	xorl	-132(%rsp,%rbx,4), %edx
	movl	-156(%rsp,%rbx,4), %r15d
	xorl	%r15d, %ecx
	xorl	%edx, %ecx
	roll	%ecx
	movl	%ecx, -100(%rsp,%rbx,4)
	movl	%r10d, %edx
	roll	$5, %edx
	addl	%r11d, %ebp
	addl	%ecx, %ebp
	leal	-1894007588(%rdx,%rbp), %r11d
	roll	$30, %r9d
	movl	%r10d, %edx
	andl	%r9d, %edx
	movl	%r10d, %esi
	orl	%r9d, %esi
	andl	%r13d, %esi
	orl	%edx, %esi
	xorl	-128(%rsp,%rbx,4), %edi
	movl	-152(%rsp,%rbx,4), %edx
	xorl	%edx, %eax
	xorl	%edi, %eax
	roll	%eax
	movl	%eax, -96(%rsp,%rbx,4)
	movl	%r11d, %edi
	roll	$5, %edi
	addl	%r8d, %esi
	addl	%eax, %esi
	leal	-1894007588(%rdi,%rsi), %r8d
	roll	$30, %r10d
	leaq	5(%rbx), %rbp
	incq	%rbx
	cmpq	$60, %rbx
	movl	%eax, %r12d
	movl	%edx, %edi
	movl	%ecx, %eax
	movl	%r15d, %edx
	movl	%r14d, %ecx
	jl	.LBB1_11
# BB#5:                                 # %.preheader424
	leal	-4(%rbp), %eax
	cmpl	$79, %eax
	jg	.LBB1_7
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r10d, %ecx
	xorl	%r11d, %ecx
	xorl	%r9d, %ecx
	movl	-144(%rsp,%rbp,4), %eax
	xorl	-124(%rsp,%rbp,4), %eax
	movl	-168(%rsp,%rbp,4), %edx
	xorl	%edx, %eax
	xorl	-176(%rsp,%rbp,4), %eax
	roll	%eax
	movl	%eax, -112(%rsp,%rbp,4)
	movl	%r8d, %esi
	roll	$5, %esi
	addl	%ecx, %esi
	addl	%r13d, %esi
	leal	-899497514(%rax,%rsi), %r13d
	roll	$30, %r11d
	movl	%r10d, %edi
	xorl	%r8d, %edi
	xorl	%r11d, %edi
	movl	-140(%rsp,%rbp,4), %ecx
	xorl	-120(%rsp,%rbp,4), %ecx
	movl	-164(%rsp,%rbp,4), %r14d
	xorl	%r14d, %ecx
	xorl	-172(%rsp,%rbp,4), %ecx
	roll	%ecx
	movl	%ecx, -108(%rsp,%rbp,4)
	movl	%r13d, %ebx
	roll	$5, %ebx
	addl	%r9d, %edi
	addl	%ecx, %edi
	leal	-899497514(%rbx,%rdi), %r9d
	roll	$30, %r8d
	movl	%r11d, %ebx
	xorl	%r8d, %ebx
	xorl	%r13d, %ebx
	movl	-136(%rsp,%rbp,4), %esi
	xorl	-116(%rsp,%rbp,4), %esi
	movl	-160(%rsp,%rbp,4), %edi
	xorl	%edi, %edx
	xorl	%esi, %edx
	roll	%edx
	movl	%edx, -104(%rsp,%rbp,4)
	movl	%r9d, %esi
	roll	$5, %esi
	addl	%r10d, %ebx
	addl	%edx, %ebx
	leal	-899497514(%rsi,%rbx), %r10d
	roll	$30, %r13d
	movl	%r13d, %edx
	xorl	%r8d, %edx
	xorl	%r9d, %edx
	xorl	-132(%rsp,%rbp,4), %eax
	xorl	-156(%rsp,%rbp,4), %eax
	xorl	%r14d, %eax
	roll	%eax
	movl	%eax, -100(%rsp,%rbp,4)
	movl	%r10d, %esi
	roll	$5, %esi
	addl	%r11d, %edx
	addl	%eax, %edx
	leal	-899497514(%rsi,%rdx), %r11d
	roll	$30, %r9d
	movl	%r9d, %eax
	xorl	%r13d, %eax
	xorl	%r10d, %eax
	xorl	-128(%rsp,%rbp,4), %ecx
	xorl	-152(%rsp,%rbp,4), %ecx
	xorl	%edi, %ecx
	roll	%ecx
	movl	%ecx, -96(%rsp,%rbp,4)
	movl	%r11d, %edx
	roll	$5, %edx
	addl	%r8d, %eax
	addl	%ecx, %eax
	leal	-899497514(%rdx,%rax), %r8d
	roll	$30, %r10d
	leaq	5(%rbp), %rax
	incq	%rbp
	cmpq	$80, %rbp
	movq	%rax, %rbp
	jl	.LBB1_6
.LBB1_7:                                # %._crit_edge
	movq	-112(%rsp), %rcx        # 8-byte Reload
	addl	(%rcx), %r8d
	movq	-104(%rsp), %rax        # 8-byte Reload
	movl	%r8d, (%rax)
	addl	4(%rcx), %r11d
	movl	%r11d, 4(%rax)
	addl	8(%rcx), %r10d
	movl	%r10d, 8(%rax)
	addl	12(%rcx), %r9d
	movl	%r9d, 12(%rax)
	addl	16(%rcx), %r13d
	movl	%r13d, 16(%rax)
	cmpb	$0, -128(%rsp)          # 1-byte Folded Reload
	je	.LBB1_9
# BB#8:                                 # %.preheader.preheader
	movdqa	160(%rsp), %xmm0
	movdqa	176(%rsp), %xmm1
	movaps	192(%rsp), %xmm2
	movaps	208(%rsp), %xmm3
	movq	-120(%rsp), %rax        # 8-byte Reload
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movdqu	%xmm1, 16(%rax)
	movdqu	%xmm0, (%rax)
.LBB1_9:                                # %.loopexit
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b, .Lfunc_end1-_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b
	.cfi_endproc

	.globl	_ZNK7NCrypto5NSha112CContextBase12PrepareBlockEPjj
	.p2align	4, 0x90
	.type	_ZNK7NCrypto5NSha112CContextBase12PrepareBlockEPjj,@function
_ZNK7NCrypto5NSha112CContextBase12PrepareBlockEPjj: # @_ZNK7NCrypto5NSha112CContextBase12PrepareBlockEPjj
	.cfi_startproc
# BB#0:
	movl	%edx, %eax
	andl	$15, %eax
	movl	$14, %ecx
	subl	%edx, %ecx
	movl	$13, %r8d
	subl	%eax, %r8d
	andl	$7, %ecx
	je	.LBB2_1
# BB#2:                                 # %.prol.preheader
	movl	%edx, %eax
	andl	$15, %eax
	negl	%ecx
	movl	$-2147483648, %r9d      # imm = 0x80000000
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movl	%r9d, (%rsi,%rax,4)
	incq	%rax
	xorl	%r9d, %r9d
	incl	%ecx
	jne	.LBB2_3
# BB#4:                                 # %.prol.loopexit.unr-lcssa
	xorl	%ecx, %ecx
	cmpl	$7, %r8d
	jae	.LBB2_6
	jmp	.LBB2_8
.LBB2_1:
	movl	$-2147483648, %ecx      # imm = 0x80000000
	cmpl	$7, %r8d
	jb	.LBB2_8
.LBB2_6:                                # %.new
	addl	$7, %eax
	.p2align	4, 0x90
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	leal	-7(%rax), %r8d
	leal	-6(%rax), %r9d
	movl	%ecx, (%rsi,%r8,4)
	leal	-5(%rax), %ecx
	movl	$0, (%rsi,%r9,4)
	leal	-4(%rax), %r8d
	movl	$0, (%rsi,%rcx,4)
	leal	-3(%rax), %ecx
	movl	$0, (%rsi,%r8,4)
	leal	-2(%rax), %r8d
	movl	$0, (%rsi,%rcx,4)
	leal	-1(%rax), %ecx
	movl	$0, (%rsi,%r8,4)
	movl	$0, (%rsi,%rcx,4)
	movl	%eax, %ecx
	movl	$0, (%rsi,%rcx,4)
	addl	$8, %eax
	xorl	%ecx, %ecx
	cmpl	$21, %eax
	jne	.LBB2_7
.LBB2_8:
	movq	24(%rdi), %rax
	shlq	$9, %rax
	movl	%edx, %ecx
	shlq	$5, %rcx
	addq	%rax, %rcx
	movq	%rcx, %rax
	shrq	$32, %rax
	movl	%eax, 56(%rsi)
	movl	%ecx, 60(%rsi)
	retq
.Lfunc_end2:
	.size	_ZNK7NCrypto5NSha112CContextBase12PrepareBlockEPjj, .Lfunc_end2-_ZNK7NCrypto5NSha112CContextBase12PrepareBlockEPjj
	.cfi_endproc

	.globl	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	.p2align	4, 0x90
	.type	_ZN7NCrypto5NSha18CContext6UpdateEPKhm,@function
_ZN7NCrypto5NSha18CContext6UpdateEPKhm: # @_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	32(%r14), %ebp
	testq	%r15, %r15
	je	.LBB3_7
# BB#1:                                 # %.lr.ph
	leaq	36(%r14), %r12
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	shrl	$2, %eax
	movl	%ebp, %ecx
	andl	$3, %ecx
	jne	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	$0, 36(%r14,%rax,4)
.LBB3_4:                                # %._crit_edge16
                                        #   in Loop: Header=BB3_2 Depth=1
	decq	%r15
	movzbl	(%rbx), %edx
	shll	$3, %ecx
	xorl	$24, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, 36(%r14,%rax,4)
	incl	%ebp
	cmpl	$64, %ebp
	jne	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	xorl	%ebp, %ebp
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b
	incq	24(%r14)
.LBB3_6:                                # %.backedge
                                        #   in Loop: Header=BB3_2 Depth=1
	incq	%rbx
	testq	%r15, %r15
	jne	.LBB3_2
.LBB3_7:                                # %._crit_edge
	movl	%ebp, 32(%r14)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN7NCrypto5NSha18CContext6UpdateEPKhm, .Lfunc_end3-_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	255                     # 0xff
	.long	255                     # 0xff
	.long	255                     # 0xff
	.long	255                     # 0xff
.LCPI4_1:
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.text
	.globl	_ZN7NCrypto5NSha18CContext9UpdateRarEPhmb
	.p2align	4, 0x90
	.type	_ZN7NCrypto5NSha18CContext9UpdateRarEPhmb,@function
_ZN7NCrypto5NSha18CContext9UpdateRarEPhmb: # @_ZN7NCrypto5NSha18CContext9UpdateRarEPhmb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 80
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rdx, %r14
	movq	%rsi, %r13
	movq	%rdi, %r12
	movl	32(%r12), %eax
	leaq	36(%r12), %rsi
	leaq	100(%r12), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	xorl	%r9d, %r9d
	jmp	.LBB4_1
.LBB4_10:                               #   in Loop: Header=BB4_1 Depth=1
	movb	%r15b, %r9b
	.p2align	4, 0x90
.LBB4_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_9 Depth 2
	movq	%r13, %rbp
	testq	%r14, %r14
	je	.LBB4_11
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	movl	%eax, %edx
	shrl	$2, %edx
	movl	%eax, %ecx
	andl	$3, %ecx
	jne	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_1 Depth=1
	movl	$0, 36(%r12,%rdx,4)
.LBB4_4:                                # %._crit_edge
                                        #   in Loop: Header=BB4_1 Depth=1
	decq	%r14
	leaq	1(%rbp), %r13
	movzbl	(%rbp), %edi
	shll	$3, %ecx
	xorl	$24, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	orl	%edi, 36(%r12,%rdx,4)
	incl	%eax
	cmpl	$64, %eax
	jne	.LBB4_1
# BB#5:                                 #   in Loop: Header=BB4_1 Depth=1
	xorl	%ecx, %ecx
	testb	%r9b, %r9b
	setne	%cl
	movq	%r12, %rdi
	movq	%r12, %rdx
	movq	%rsi, %rbx
	movq	%r9, 16(%rsp)           # 8-byte Spill
	callq	_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b
	movq	%rbx, %rsi
	incq	24(%r12)
	xorl	%eax, %eax
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	movb	%r15b, %r9b
	je	.LBB4_1
# BB#6:                                 # %vector.memcheck
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	$4294967233, %ecx       # imm = 0xFFFFFFC1
	leaq	(%rbp,%rcx), %rcx
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	jae	.LBB4_12
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB4_1 Depth=1
	movabsq	$4294967297, %rcx       # imm = 0x100000001
	leaq	(%rbp,%rcx), %rcx
	cmpq	%rcx, %rsi
	jae	.LBB4_12
# BB#8:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	$4294967233, %ecx       # imm = 0xFFFFFFC1
	leaq	(%r13,%rcx), %rbp
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_9:                                # %.preheader
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	36(%r12,%rdx,4), %ecx
	movb	%cl, -1(%rbp,%rdx,4)
	movb	%ch, (%rbp,%rdx,4)  # NOREX
	movl	%ecx, %ebx
	shrl	$16, %ebx
	movb	%bl, 1(%rbp,%rdx,4)
	shrl	$24, %ecx
	movb	%cl, 2(%rbp,%rdx,4)
	incq	%rdx
	cmpq	$16, %rdx
	jne	.LBB4_9
	jmp	.LBB4_10
.LBB4_12:                               # %vector.body
                                        #   in Loop: Header=BB4_1 Depth=1
	movdqu	36(%r12), %xmm1
	movdqa	%xmm1, %xmm2
	psrld	$8, %xmm2
	movdqa	%xmm1, %xmm0
	psrld	$16, %xmm0
	movdqa	%xmm1, %xmm3
	psrld	$24, %xmm3
	movdqa	.LCPI4_0(%rip), %xmm4   # xmm4 = [255,255,255,255]
	pand	%xmm4, %xmm1
	pand	%xmm4, %xmm2
	packuswb	%xmm2, %xmm1
	packuswb	%xmm1, %xmm1
	pxor	%xmm5, %xmm5
	punpcklbw	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1],xmm1[2],xmm5[2],xmm1[3],xmm5[3],xmm1[4],xmm5[4],xmm1[5],xmm5[5],xmm1[6],xmm5[6],xmm1[7],xmm5[7]
	pshufd	$215, %xmm1, %xmm2      # xmm2 = xmm1[3,1,1,3]
	pshuflw	$226, %xmm2, %xmm2      # xmm2 = xmm2[2,0,2,3,4,5,6,7]
	pshufhw	$237, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,5,7,6,7]
	pshufd	$40, %xmm1, %xmm1       # xmm1 = xmm1[0,2,2,0]
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$231, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,7,5,6,7]
	packuswb	%xmm2, %xmm1
	movdqa	.LCPI4_1(%rip), %xmm6   # xmm6 = [65535,0,65535,0,65535,0,65535,0]
	pand	%xmm6, %xmm1
	pand	%xmm4, %xmm0
	packuswb	%xmm3, %xmm0
	packuswb	%xmm0, %xmm0
	punpcklbw	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1],xmm0[2],xmm5[2],xmm0[3],xmm5[3],xmm0[4],xmm5[4],xmm0[5],xmm5[5],xmm0[6],xmm5[6],xmm0[7],xmm5[7]
	pshufd	$215, %xmm0, %xmm2      # xmm2 = xmm0[3,1,1,3]
	pshuflw	$36, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,0,4,5,6,7]
	pshufhw	$212, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,5,5,7]
	pshufd	$40, %xmm0, %xmm0       # xmm0 = xmm0[0,2,2,0]
	pshuflw	$132, %xmm0, %xmm0      # xmm0 = xmm0[0,1,0,2,4,5,6,7]
	pshufhw	$116, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,5,7,5]
	packuswb	%xmm2, %xmm0
	movdqa	%xmm6, %xmm2
	pandn	%xmm0, %xmm2
	por	%xmm1, %xmm2
	movl	$4294967233, %ecx       # imm = 0xFFFFFFC1
	movdqu	%xmm2, (%rbp,%rcx)
	movdqu	52(%r12), %xmm1
	movdqa	%xmm1, %xmm2
	psrld	$8, %xmm2
	movdqa	%xmm1, %xmm0
	psrld	$16, %xmm0
	movdqa	%xmm1, %xmm3
	psrld	$24, %xmm3
	pand	%xmm4, %xmm1
	pand	%xmm4, %xmm2
	packuswb	%xmm2, %xmm1
	packuswb	%xmm1, %xmm1
	punpcklbw	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1],xmm1[2],xmm5[2],xmm1[3],xmm5[3],xmm1[4],xmm5[4],xmm1[5],xmm5[5],xmm1[6],xmm5[6],xmm1[7],xmm5[7]
	pshufd	$215, %xmm1, %xmm2      # xmm2 = xmm1[3,1,1,3]
	pshuflw	$226, %xmm2, %xmm2      # xmm2 = xmm2[2,0,2,3,4,5,6,7]
	pshufhw	$237, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,5,7,6,7]
	pshufd	$40, %xmm1, %xmm1       # xmm1 = xmm1[0,2,2,0]
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$231, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,7,5,6,7]
	packuswb	%xmm2, %xmm1
	pand	%xmm6, %xmm1
	pand	%xmm4, %xmm0
	packuswb	%xmm3, %xmm0
	packuswb	%xmm0, %xmm0
	punpcklbw	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1],xmm0[2],xmm5[2],xmm0[3],xmm5[3],xmm0[4],xmm5[4],xmm0[5],xmm5[5],xmm0[6],xmm5[6],xmm0[7],xmm5[7]
	pshufd	$215, %xmm0, %xmm2      # xmm2 = xmm0[3,1,1,3]
	pshuflw	$36, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,0,4,5,6,7]
	pshufhw	$212, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,5,5,7]
	pshufd	$40, %xmm0, %xmm0       # xmm0 = xmm0[0,2,2,0]
	pshuflw	$132, %xmm0, %xmm0      # xmm0 = xmm0[0,1,0,2,4,5,6,7]
	pshufhw	$116, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,5,7,5]
	packuswb	%xmm2, %xmm0
	movdqa	%xmm6, %xmm2
	pandn	%xmm0, %xmm2
	por	%xmm1, %xmm2
	movl	$4294967249, %ecx       # imm = 0xFFFFFFD1
	movdqu	%xmm2, (%rbp,%rcx)
	movdqu	68(%r12), %xmm1
	movdqa	%xmm1, %xmm2
	psrld	$8, %xmm2
	movdqa	%xmm1, %xmm0
	psrld	$16, %xmm0
	movdqa	%xmm1, %xmm3
	psrld	$24, %xmm3
	pand	%xmm4, %xmm1
	pand	%xmm4, %xmm2
	packuswb	%xmm2, %xmm1
	packuswb	%xmm1, %xmm1
	punpcklbw	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1],xmm1[2],xmm5[2],xmm1[3],xmm5[3],xmm1[4],xmm5[4],xmm1[5],xmm5[5],xmm1[6],xmm5[6],xmm1[7],xmm5[7]
	pshufd	$215, %xmm1, %xmm2      # xmm2 = xmm1[3,1,1,3]
	pshuflw	$226, %xmm2, %xmm2      # xmm2 = xmm2[2,0,2,3,4,5,6,7]
	pshufhw	$237, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,5,7,6,7]
	pshufd	$40, %xmm1, %xmm1       # xmm1 = xmm1[0,2,2,0]
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$231, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,7,5,6,7]
	packuswb	%xmm2, %xmm1
	pand	%xmm6, %xmm1
	pand	%xmm4, %xmm0
	packuswb	%xmm3, %xmm0
	packuswb	%xmm0, %xmm0
	punpcklbw	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1],xmm0[2],xmm5[2],xmm0[3],xmm5[3],xmm0[4],xmm5[4],xmm0[5],xmm5[5],xmm0[6],xmm5[6],xmm0[7],xmm5[7]
	pshufd	$215, %xmm0, %xmm2      # xmm2 = xmm0[3,1,1,3]
	pshuflw	$36, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,0,4,5,6,7]
	pshufhw	$212, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,5,5,7]
	pshufd	$40, %xmm0, %xmm0       # xmm0 = xmm0[0,2,2,0]
	pshuflw	$132, %xmm0, %xmm0      # xmm0 = xmm0[0,1,0,2,4,5,6,7]
	pshufhw	$116, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,5,7,5]
	packuswb	%xmm2, %xmm0
	movdqa	%xmm6, %xmm2
	pandn	%xmm0, %xmm2
	por	%xmm1, %xmm2
	movl	$4294967265, %ecx       # imm = 0xFFFFFFE1
	movdqu	%xmm2, (%rbp,%rcx)
	movdqu	84(%r12), %xmm1
	movdqa	%xmm1, %xmm2
	psrld	$8, %xmm2
	movdqa	%xmm1, %xmm0
	psrld	$16, %xmm0
	movdqa	%xmm1, %xmm3
	psrld	$24, %xmm3
	pand	%xmm4, %xmm1
	pand	%xmm4, %xmm2
	packuswb	%xmm2, %xmm1
	packuswb	%xmm1, %xmm1
	punpcklbw	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1],xmm1[2],xmm5[2],xmm1[3],xmm5[3],xmm1[4],xmm5[4],xmm1[5],xmm5[5],xmm1[6],xmm5[6],xmm1[7],xmm5[7]
	pshufd	$215, %xmm1, %xmm2      # xmm2 = xmm1[3,1,1,3]
	pshuflw	$226, %xmm2, %xmm2      # xmm2 = xmm2[2,0,2,3,4,5,6,7]
	pshufhw	$237, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,5,7,6,7]
	pshufd	$40, %xmm1, %xmm1       # xmm1 = xmm1[0,2,2,0]
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$231, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,7,5,6,7]
	packuswb	%xmm2, %xmm1
	pand	%xmm6, %xmm1
	pand	%xmm4, %xmm0
	packuswb	%xmm3, %xmm0
	packuswb	%xmm0, %xmm0
	punpcklbw	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1],xmm0[2],xmm5[2],xmm0[3],xmm5[3],xmm0[4],xmm5[4],xmm0[5],xmm5[5],xmm0[6],xmm5[6],xmm0[7],xmm5[7]
	pshufd	$215, %xmm0, %xmm2      # xmm2 = xmm0[3,1,1,3]
	pshuflw	$36, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,0,4,5,6,7]
	pshufhw	$212, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,5,5,7]
	pshufd	$40, %xmm0, %xmm0       # xmm0 = xmm0[0,2,2,0]
	pshuflw	$132, %xmm0, %xmm0      # xmm0 = xmm0[0,1,0,2,4,5,6,7]
	pshufhw	$116, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,5,7,5]
	packuswb	%xmm2, %xmm0
	movdqa	%xmm6, %xmm2
	pandn	%xmm0, %xmm2
	por	%xmm1, %xmm2
	movl	$4294967281, %ecx       # imm = 0xFFFFFFF1
	movdqu	%xmm2, (%rbp,%rcx)
	movb	%r15b, %r9b
	jmp	.LBB4_1
.LBB4_11:
	movl	%eax, 32(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN7NCrypto5NSha18CContext9UpdateRarEPhmb, .Lfunc_end4-_ZN7NCrypto5NSha18CContext9UpdateRarEPhmb
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	1732584193              # 0x67452301
	.long	4023233417              # 0xefcdab89
	.long	2562383102              # 0x98badcfe
	.long	271733878               # 0x10325476
	.text
	.globl	_ZN7NCrypto5NSha18CContext5FinalEPh
	.p2align	4, 0x90
	.type	_ZN7NCrypto5NSha18CContext5FinalEPh,@function
_ZN7NCrypto5NSha18CContext5FinalEPh:    # @_ZN7NCrypto5NSha18CContext5FinalEPh
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 48
.Lcfi41:
	.cfi_offset %rbx, -48
.Lcfi42:
	.cfi_offset %r12, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	24(%rbx), %rdx
	shlq	$9, %rdx
	movl	32(%rbx), %esi
	movl	%esi, %eax
	shrl	$2, %eax
	movl	%esi, %ecx
	andl	$3, %ecx
	je	.LBB5_2
# BB#1:                                 # %._crit_edge42
	movl	36(%rbx,%rax,4), %edi
	jmp	.LBB5_3
.LBB5_2:
	movl	$0, 36(%rbx,%rax,4)
	xorl	%edi, %edi
.LBB5_3:
	leaq	(%rdx,%rsi,8), %r14
	shll	$3, %ecx
	xorl	$24, %ecx
	movl	$128, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edi, %edx
	movl	%edx, 36(%rbx,%rax,4)
	leal	1(%rax), %ebp
	leaq	36(%rbx), %r15
	cmpl	$14, %ebp
	jne	.LBB5_5
	jmp	.LBB5_8
	.p2align	4, 0x90
.LBB5_7:                                #   in Loop: Header=BB5_5 Depth=1
	movl	%ebp, %eax
	movl	$0, 36(%rbx,%rax,4)
	incl	%ebp
	cmpl	$14, %ebp
	jne	.LBB5_5
	jmp	.LBB5_8
	.p2align	4, 0x90
.LBB5_6:                                # %.thread
                                        #   in Loop: Header=BB5_5 Depth=1
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b
	incq	24(%rbx)
	movl	%ebp, %eax
	movl	$0, 36(%rbx,%rax,4)
	movl	$1, %ebp
.LBB5_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	andl	$15, %ebp
	jne	.LBB5_7
	jmp	.LBB5_6
.LBB5_8:                                # %._crit_edge
	movq	%r14, %rax
	shrq	$32, %rax
	movl	%eax, 92(%rbx)
	movl	%r14d, 96(%rbx)
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b
	movl	(%rbx), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movq	%r12, %rdx
	movb	%cl, (%rdx)
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 1(%rdx)
	movb	%ah, 2(%rdx)  # NOREX
	movb	%al, 3(%rdx)
	movl	4(%rbx), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 4(%rdx)
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 5(%rdx)
	movb	%ah, 6(%rdx)  # NOREX
	movb	%al, 7(%rdx)
	movl	8(%rbx), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 8(%rdx)
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 9(%rdx)
	movb	%ah, 10(%rdx)  # NOREX
	movb	%al, 11(%rdx)
	movl	12(%rbx), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 12(%rdx)
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 13(%rdx)
	movb	%ah, 14(%rdx)  # NOREX
	movb	%al, 15(%rdx)
	movl	16(%rbx), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 16(%rdx)
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 17(%rdx)
	movb	%ah, 18(%rdx)  # NOREX
	movb	%al, 19(%rdx)
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [1732584193,4023233417,2562383102,271733878]
	movups	%xmm0, (%rbx)
	movl	$-1009589776, 16(%rbx)  # imm = 0xC3D2E1F0
	movq	$0, 24(%rbx)
	movl	$0, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN7NCrypto5NSha18CContext5FinalEPh, .Lfunc_end5-_ZN7NCrypto5NSha18CContext5FinalEPh
	.cfi_endproc

	.globl	_ZN7NCrypto5NSha110CContext326UpdateEPKjm
	.p2align	4, 0x90
	.type	_ZN7NCrypto5NSha110CContext326UpdateEPKjm,@function
_ZN7NCrypto5NSha110CContext326UpdateEPKjm: # @_ZN7NCrypto5NSha110CContext326UpdateEPKjm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 48
.Lcfi51:
	.cfi_offset %rbx, -40
.Lcfi52:
	.cfi_offset %r12, -32
.Lcfi53:
	.cfi_offset %r14, -24
.Lcfi54:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB6_5
# BB#1:                                 # %.lr.ph
	leaq	36(%r15), %r12
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	decq	%r14
	movl	(%rbx), %eax
	movl	32(%r15), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 32(%r15)
	movl	%eax, 36(%r15,%rcx,4)
	cmpl	$16, %edx
	jne	.LBB6_4
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	movl	$0, 32(%r15)
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b
	incq	24(%r15)
.LBB6_4:                                # %.backedge
                                        #   in Loop: Header=BB6_2 Depth=1
	addq	$4, %rbx
	testq	%r14, %r14
	jne	.LBB6_2
.LBB6_5:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	_ZN7NCrypto5NSha110CContext326UpdateEPKjm, .Lfunc_end6-_ZN7NCrypto5NSha110CContext326UpdateEPKjm
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.long	1732584193              # 0x67452301
	.long	4023233417              # 0xefcdab89
	.long	2562383102              # 0x98badcfe
	.long	271733878               # 0x10325476
	.text
	.globl	_ZN7NCrypto5NSha110CContext325FinalEPj
	.p2align	4, 0x90
	.type	_ZN7NCrypto5NSha110CContext325FinalEPj,@function
_ZN7NCrypto5NSha110CContext325FinalEPj: # @_ZN7NCrypto5NSha110CContext325FinalEPj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi59:
	.cfi_def_cfa_offset 48
.Lcfi60:
	.cfi_offset %rbx, -40
.Lcfi61:
	.cfi_offset %r12, -32
.Lcfi62:
	.cfi_offset %r14, -24
.Lcfi63:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	24(%rbx), %rcx
	shlq	$9, %rcx
	movl	32(%rbx), %r12d
	leal	1(%r12), %eax
	movl	$-2147483648, 36(%rbx,%r12,4) # imm = 0x80000000
	shlq	$5, %r12
	addq	%rcx, %r12
	leaq	36(%rbx), %r15
	cmpl	$14, %eax
	jne	.LBB7_2
	jmp	.LBB7_5
	.p2align	4, 0x90
.LBB7_4:                                # %.backedge
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	%eax, %ecx
	incl	%eax
	movl	$0, 36(%rbx,%rcx,4)
	cmpl	$14, %eax
	jne	.LBB7_2
	jmp	.LBB7_5
	.p2align	4, 0x90
.LBB7_3:                                # %.backedge.thread
                                        #   in Loop: Header=BB7_2 Depth=1
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b
	incq	24(%rbx)
	movl	$0, 36(%rbx)
	movl	$1, %eax
.LBB7_2:                                # %.backedge24
                                        # =>This Inner Loop Header: Depth=1
	andl	$15, %eax
	jne	.LBB7_4
	jmp	.LBB7_3
.LBB7_5:                                # %._crit_edge
	movq	%r12, %rax
	shrq	$32, %rax
	movl	%eax, 92(%rbx)
	movl	%r12d, 96(%rbx)
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	_ZN7NCrypto5NSha112CContextBase14GetBlockDigestEPjS2_b
	movaps	.LCPI7_0(%rip), %xmm0   # xmm0 = [1732584193,4023233417,2562383102,271733878]
	movups	%xmm0, (%rbx)
	movl	$-1009589776, 16(%rbx)  # imm = 0xC3D2E1F0
	movq	$0, 24(%rbx)
	movl	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	_ZN7NCrypto5NSha110CContext325FinalEPj, .Lfunc_end7-_ZN7NCrypto5NSha110CContext325FinalEPj
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
