	.text
	.file	"btHinge2Constraint.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1065353216              # float 1
.LCPI0_2:
	.long	1109256679              # float 39.4784203
.LCPI0_3:
	.long	1008981770              # float 0.00999999977
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN18btHinge2ConstraintC2ER11btRigidBodyS1_R9btVector3S3_S3_
	.p2align	4, 0x90
	.type	_ZN18btHinge2ConstraintC2ER11btRigidBodyS1_R9btVector3S3_S3_,@function
_ZN18btHinge2ConstraintC2ER11btRigidBodyS1_R9btVector3S3_S3_: # @_ZN18btHinge2ConstraintC2ER11btRigidBodyS1_R9btVector3S3_S3_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 272
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movq	%r8, %rbp
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movb	_ZGVZN11btTransform11getIdentityEvE17identityTransform(%rip), %al
	testb	%al, %al
	jne	.LBB0_6
# BB#1:
	movl	$_ZGVZN11btTransform11getIdentityEvE17identityTransform, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB0_6
# BB#2:
	movb	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip), %al
	testb	%al, %al
	jne	.LBB0_5
# BB#3:
	movl	$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB0_5
# BB#4:
	movl	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip) # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+4(%rip)
	movl	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+20(%rip) # imm = 0x3F800000
	movups	%xmm0, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+24(%rip)
	movq	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+40(%rip) # imm = 0x3F800000
	movl	$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, %edi
	callq	__cxa_guard_release
.LBB0_5:
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform(%rip)
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix+16(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+16(%rip)
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix+32(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+32(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+48(%rip)
	movl	$_ZGVZN11btTransform11getIdentityEvE17identityTransform, %edi
	callq	__cxa_guard_release
.LBB0_6:                                # %_ZN11btTransform11getIdentityEv.exit
	movb	_ZGVZN11btTransform11getIdentityEvE17identityTransform(%rip), %al
	testb	%al, %al
	jne	.LBB0_12
# BB#7:
	movl	$_ZGVZN11btTransform11getIdentityEvE17identityTransform, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB0_12
# BB#8:
	movb	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip), %al
	testb	%al, %al
	jne	.LBB0_11
# BB#9:
	movl	$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB0_11
# BB#10:
	movl	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip) # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+4(%rip)
	movl	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+20(%rip) # imm = 0x3F800000
	movups	%xmm0, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+24(%rip)
	movq	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+40(%rip) # imm = 0x3F800000
	movl	$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, %edi
	callq	__cxa_guard_release
.LBB0_11:
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform(%rip)
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix+16(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+16(%rip)
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix+32(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+32(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+48(%rip)
	movl	$_ZGVZN11btTransform11getIdentityEvE17identityTransform, %edi
	callq	__cxa_guard_release
.LBB0_12:
	movl	$_ZZN11btTransform11getIdentityEvE17identityTransform, %ecx
	movl	$_ZZN11btTransform11getIdentityEvE17identityTransform, %r8d
	movl	$1, %r9d
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	_ZN29btGeneric6DofSpringConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	movq	$_ZTV18btHinge2Constraint+16, (%rbx)
	movups	(%r15), %xmm0
	movups	%xmm0, 1344(%rbx)
	movups	(%rbp), %xmm0
	movups	%xmm0, 1360(%rbx)
	movups	(%r13), %xmm0
	movups	%xmm0, 1376(%rbx)
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB0_14
# BB#13:                                # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB0_14:                               # %.split
	movss	.LCPI0_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm14
	divss	%xmm1, %xmm14
	movss	(%rbp), %xmm11          # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm11
	movss	%xmm11, (%rbp)
	movss	4(%rbp), %xmm8          # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm8
	movss	%xmm8, 4(%rbp)
	mulss	8(%rbp), %xmm14
	movss	%xmm14, 8(%rbp)
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	movss	%xmm14, 12(%rsp)        # 4-byte Spill
	movss	%xmm11, 44(%rsp)        # 4-byte Spill
	movss	%xmm8, 40(%rsp)         # 4-byte Spill
	jnp	.LBB0_16
# BB#15:                                # %call.sqrt365
	callq	sqrtf
	movss	40(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	44(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movss	.LCPI0_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm14        # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB0_16:                               # %.split.split
	divss	%xmm1, %xmm3
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	%xmm0, (%r13)
	movaps	%xmm0, %xmm6
	movss	4(%r13), %xmm7          # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm7
	movss	%xmm7, 4(%r13)
	mulss	8(%r13), %xmm3
	movss	%xmm3, 8(%r13)
	movaps	%xmm8, %xmm1
	mulss	%xmm3, %xmm1
	movaps	%xmm14, %xmm0
	mulss	%xmm7, %xmm0
	subss	%xmm0, %xmm1
	movss	%xmm1, 16(%rsp)         # 4-byte Spill
	movss	%xmm6, 52(%rsp)         # 4-byte Spill
	mulss	%xmm6, %xmm14
	movaps	%xmm11, %xmm0
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm14
	movss	%xmm14, 48(%rsp)        # 4-byte Spill
	movaps	%xmm11, %xmm1
	mulss	%xmm7, %xmm1
	movaps	%xmm7, %xmm9
	movaps	%xmm8, %xmm0
	mulss	%xmm6, %xmm0
	subss	%xmm0, %xmm1
	movaps	%xmm1, %xmm10
	movss	%xmm10, 56(%rsp)        # 4-byte Spill
	movsd	8(%r12), %xmm7          # xmm7 = mem[0],zero
	movaps	%xmm7, 96(%rsp)         # 16-byte Spill
	movsd	24(%r12), %xmm13        # xmm13 = mem[0],zero
	movd	56(%r12), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movdqa	.LCPI0_1(%rip), %xmm6   # xmm6 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm6, %xmm15
	movd	60(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movdqa	%xmm0, %xmm1
	pxor	%xmm6, %xmm1
	pshufd	$224, %xmm15, %xmm2     # xmm2 = xmm15[0,0,2,3]
	mulps	%xmm7, %xmm2
	pshufd	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm13, %xmm1
	addps	%xmm2, %xmm1
	movd	64(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movdqa	%xmm5, %xmm2
	pxor	%xmm6, %xmm2
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movsd	40(%r12), %xmm6         # xmm6 = mem[0],zero
	mulps	%xmm6, %xmm2
	addps	%xmm1, %xmm2
	movaps	%xmm2, 80(%rsp)         # 16-byte Spill
	movss	16(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 64(%rsp)         # 4-byte Spill
	mulss	%xmm1, %xmm15
	movss	32(%r12), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	subss	%xmm0, %xmm15
	movaps	%xmm3, %xmm2
	movss	48(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	subss	%xmm5, %xmm15
	movss	52(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	mulss	%xmm7, %xmm0
	movaps	%xmm9, %xmm4
	movss	%xmm4, 60(%rsp)         # 4-byte Spill
	movaps	%xmm4, %xmm1
	mulss	%xmm13, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm2, %xmm0
	movaps	%xmm2, %xmm9
	movss	%xmm9, 8(%rsp)          # 4-byte Spill
	mulss	%xmm6, %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, 36(%rsp)         # 4-byte Spill
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	mulss	%xmm13, %xmm14
	addss	%xmm0, %xmm14
	movaps	%xmm10, %xmm0
	mulss	%xmm6, %xmm0
	addss	%xmm14, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	mulss	%xmm7, %xmm11
	movaps	%xmm8, %xmm1
	movaps	%xmm8, %xmm10
	mulss	%xmm13, %xmm1
	addss	%xmm11, %xmm1
	movss	12(%rsp), %xmm14        # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm6, %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
	pshufd	$229, %xmm7, %xmm2      # xmm2 = xmm7[1,1,2,3]
	movaps	%xmm5, %xmm7
	mulss	%xmm2, %xmm7
	pshufd	$229, %xmm13, %xmm5     # xmm5 = xmm13[1,1,2,3]
	mulss	%xmm5, %xmm4
	addss	%xmm7, %xmm4
	pshufd	$229, %xmm6, %xmm11     # xmm11 = xmm6[1,1,2,3]
	mulss	%xmm11, %xmm9
	addss	%xmm4, %xmm9
	movss	%xmm9, 24(%rsp)         # 4-byte Spill
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	movss	48(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm5, %xmm0
	addss	%xmm1, %xmm0
	movss	56(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm1
	mulss	%xmm11, %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, 20(%rsp)         # 4-byte Spill
	movss	44(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	movaps	%xmm10, %xmm4
	mulss	%xmm4, %xmm5
	addss	%xmm2, %xmm5
	mulss	%xmm14, %xmm11
	addss	%xmm5, %xmm11
	movss	52(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	64(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	movss	60(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rsp), %xmm10         # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm10
	addss	%xmm1, %xmm10
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	mulss	%xmm12, %xmm8
	addss	%xmm0, %xmm8
	mulss	%xmm3, %xmm9
	addss	%xmm8, %xmm9
	mulss	%xmm5, %xmm7
	mulss	%xmm12, %xmm4
	addss	%xmm7, %xmm4
	mulss	%xmm3, %xmm14
	addss	%xmm4, %xmm14
	movss	(%r15), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	movaps	%xmm2, 176(%rsp)        # 16-byte Spill
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movaps	%xmm4, 192(%rsp)        # 16-byte Spill
	movaps	96(%rsp), %xmm7         # 16-byte Reload
	mulps	%xmm4, %xmm7
	movss	4(%r15), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	movaps	%xmm4, 160(%rsp)        # 16-byte Spill
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movaps	%xmm1, 96(%rsp)         # 16-byte Spill
	mulps	%xmm1, %xmm13
	addps	%xmm7, %xmm13
	movss	8(%r15), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm1
	movaps	%xmm7, 144(%rsp)        # 16-byte Spill
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	mulps	%xmm1, %xmm6
	addps	%xmm13, %xmm6
	addps	80(%rsp), %xmm6         # 16-byte Folded Reload
	mulss	%xmm2, %xmm5
	mulss	%xmm4, %xmm12
	addss	%xmm5, %xmm12
	mulss	%xmm7, %xmm3
	addss	%xmm12, %xmm3
	addss	%xmm15, %xmm3
	xorps	%xmm2, %xmm2
	movss	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1,2,3]
	movss	36(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 96(%rbx)
	movss	32(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 100(%rbx)
	movss	28(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 104(%rbx)
	movl	$0, 108(%rbx)
	movss	24(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 112(%rbx)
	movss	20(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 116(%rbx)
	movss	%xmm11, 120(%rbx)
	movl	$0, 124(%rbx)
	movss	%xmm10, 128(%rbx)
	movss	%xmm9, 132(%rbx)
	movss	%xmm14, 136(%rbx)
	movl	$0, 140(%rbx)
	movlps	%xmm6, 144(%rbx)
	movlps	%xmm2, 152(%rbx)
	movd	56(%r14), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movdqa	.LCPI0_1(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm1, %xmm12
	movd	60(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movdqa	%xmm0, %xmm2
	pxor	%xmm1, %xmm2
	movsd	8(%r14), %xmm4          # xmm4 = mem[0],zero
	pshufd	$224, %xmm12, %xmm3     # xmm3 = xmm12[0,0,2,3]
	mulps	%xmm4, %xmm3
	movaps	%xmm4, %xmm5
	movaps	%xmm5, 64(%rsp)         # 16-byte Spill
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movsd	24(%r14), %xmm4         # xmm4 = mem[0],zero
	mulps	%xmm4, %xmm2
	movaps	%xmm4, %xmm7
	movaps	%xmm7, 112(%rsp)        # 16-byte Spill
	addps	%xmm3, %xmm2
	movd	64(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	pxor	%xmm3, %xmm1
	pshufd	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movsd	40(%r14), %xmm15        # xmm15 = mem[0],zero
	mulps	%xmm15, %xmm1
	addps	%xmm2, %xmm1
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	movss	16(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 24(%rsp)         # 4-byte Spill
	mulss	%xmm1, %xmm12
	movss	32(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 20(%rsp)         # 4-byte Spill
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm12
	movss	48(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm3
	subss	%xmm3, %xmm12
	movss	52(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	mulss	%xmm5, %xmm0
	movss	60(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	mulss	%xmm7, %xmm2
	addss	%xmm0, %xmm2
	movss	8(%rsp), %xmm13         # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm0
	mulss	%xmm15, %xmm0
	addss	%xmm2, %xmm0
	movss	%xmm0, 36(%rsp)         # 4-byte Spill
	movss	16(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm5, %xmm0
	movss	48(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm2
	mulss	%xmm7, %xmm2
	addss	%xmm0, %xmm2
	movss	56(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm0
	mulss	%xmm15, %xmm0
	addss	%xmm2, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movss	44(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm5, %xmm0
	movss	40(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	addss	%xmm0, %xmm2
	movss	12(%rsp), %xmm14        # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm14
	addss	%xmm2, %xmm14
	pshufd	$229, %xmm5, %xmm6      # xmm6 = xmm5[1,1,2,3]
	movaps	%xmm1, %xmm0
	mulss	%xmm6, %xmm0
	pshufd	$229, %xmm7, %xmm5      # xmm5 = xmm7[1,1,2,3]
	movaps	%xmm3, %xmm7
	movaps	%xmm3, %xmm2
	mulss	%xmm5, %xmm7
	addss	%xmm0, %xmm7
	pshufd	$229, %xmm15, %xmm3     # xmm3 = xmm15[1,1,2,3]
	mulss	%xmm3, %xmm13
	addss	%xmm7, %xmm13
	movaps	%xmm9, %xmm7
	mulss	%xmm6, %xmm7
	mulss	%xmm5, %xmm4
	addss	%xmm7, %xmm4
	movaps	%xmm10, %xmm7
	mulss	%xmm3, %xmm7
	addss	%xmm4, %xmm7
	mulss	%xmm11, %xmm6
	movaps	%xmm11, %xmm4
	movss	40(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm5
	addss	%xmm6, %xmm5
	movss	12(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm3
	addss	%xmm5, %xmm3
	movaps	%xmm1, %xmm0
	movss	24(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	movaps	%xmm2, %xmm5
	movss	20(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm0, %xmm5
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	addss	%xmm5, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movaps	%xmm9, %xmm5
	mulss	%xmm6, %xmm5
	movss	48(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm9
	addss	%xmm5, %xmm0
	movaps	%xmm0, %xmm5
	mulss	%xmm2, %xmm10
	addss	%xmm5, %xmm10
	mulss	%xmm6, %xmm4
	mulss	%xmm9, %xmm8
	addss	%xmm4, %xmm8
	movaps	%xmm11, %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm8, %xmm0
	movaps	%xmm0, %xmm5
	movaps	192(%rsp), %xmm1        # 16-byte Reload
	mulps	64(%rsp), %xmm1         # 16-byte Folded Reload
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	mulps	112(%rsp), %xmm0        # 16-byte Folded Reload
	addps	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	mulps	%xmm15, %xmm0
	addps	%xmm1, %xmm0
	addps	80(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	%xmm0, %xmm1
	movaps	176(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm6, %xmm4
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm9, %xmm0
	addss	%xmm4, %xmm0
	movaps	144(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm2, %xmm4
	addss	%xmm0, %xmm4
	addss	%xmm12, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movss	36(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 160(%rbx)
	movss	32(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 164(%rbx)
	movss	%xmm14, 168(%rbx)
	movl	$0, 172(%rbx)
	movss	%xmm13, 176(%rbx)
	movss	%xmm7, 180(%rbx)
	movss	%xmm3, 184(%rbx)
	movl	$0, 188(%rbx)
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 192(%rbx)
	movss	%xmm10, 196(%rbx)
	movss	%xmm5, 200(%rbx)
	movl	$0, 204(%rbx)
	movlps	%xmm1, 208(%rbx)
	movlps	%xmm0, 216(%rbx)
	movl	$0, 728(%rbx)
	movl	$0, 732(%rbx)
	movl	$-1082130432, 736(%rbx) # imm = 0xBF800000
	movl	$0, 740(%rbx)
	movl	$0, 744(%rbx)
	movl	$0, 748(%rbx)
	movl	$1065353216, 752(%rbx)  # imm = 0x3F800000
	movl	$0, 756(%rbx)
	movl	$1065353216, 868(%rbx)  # imm = 0x3F800000
	movl	$0, 924(%rbx)
	movl	$-1085730853, 980(%rbx) # imm = 0xBF490FDB
	movl	$-1082130432, 872(%rbx) # imm = 0xBF800000
	movl	$0, 928(%rbx)
	movl	$1061752795, 984(%rbx)  # imm = 0x3F490FDB
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_ZN29btGeneric6DofSpringConstraint12enableSpringEib
	movss	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	_ZN29btGeneric6DofSpringConstraint12setStiffnessEif
	movss	.LCPI0_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	_ZN29btGeneric6DofSpringConstraint10setDampingEif
	movq	%rbx, %rdi
	callq	_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEv
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN18btHinge2ConstraintC2ER11btRigidBodyS1_R9btVector3S3_S3_, .Lfunc_end0-_ZN18btHinge2ConstraintC2ER11btRigidBodyS1_R9btVector3S3_S3_
	.cfi_endproc

	.section	.text._ZN17btTypedConstraintD2Ev,"axG",@progbits,_ZN17btTypedConstraintD2Ev,comdat
	.weak	_ZN17btTypedConstraintD2Ev
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraintD2Ev,@function
_ZN17btTypedConstraintD2Ev:             # @_ZN17btTypedConstraintD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN17btTypedConstraintD2Ev, .Lfunc_end1-_ZN17btTypedConstraintD2Ev
	.cfi_endproc

	.section	.text._ZN18btHinge2ConstraintD0Ev,"axG",@progbits,_ZN18btHinge2ConstraintD0Ev,comdat
	.weak	_ZN18btHinge2ConstraintD0Ev
	.p2align	4, 0x90
	.type	_ZN18btHinge2ConstraintD0Ev,@function
_ZN18btHinge2ConstraintD0Ev:            # @_ZN18btHinge2ConstraintD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end2:
	.size	_ZN18btHinge2ConstraintD0Ev, .Lfunc_end2-_ZN18btHinge2ConstraintD0Ev
	.cfi_endproc

	.section	.text._ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,"axG",@progbits,_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,comdat
	.weak	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,@function
_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif: # @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end3:
	.size	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif, .Lfunc_end3-_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_endproc

	.type	_ZTV18btHinge2Constraint,@object # @_ZTV18btHinge2Constraint
	.section	.rodata._ZTV18btHinge2Constraint,"aG",@progbits,_ZTV18btHinge2Constraint,comdat
	.weak	_ZTV18btHinge2Constraint
	.p2align	3
_ZTV18btHinge2Constraint:
	.quad	0
	.quad	_ZTI18btHinge2Constraint
	.quad	_ZN17btTypedConstraintD2Ev
	.quad	_ZN18btHinge2ConstraintD0Ev
	.quad	_ZN23btGeneric6DofConstraint13buildJacobianEv
	.quad	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.quad	_ZN23btGeneric6DofConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.quad	_ZN29btGeneric6DofSpringConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.quad	_ZN23btGeneric6DofConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.quad	_ZN23btGeneric6DofConstraint13calcAnchorPosEv
	.size	_ZTV18btHinge2Constraint, 80

	.type	_ZTS18btHinge2Constraint,@object # @_ZTS18btHinge2Constraint
	.section	.rodata._ZTS18btHinge2Constraint,"aG",@progbits,_ZTS18btHinge2Constraint,comdat
	.weak	_ZTS18btHinge2Constraint
	.p2align	4
_ZTS18btHinge2Constraint:
	.asciz	"18btHinge2Constraint"
	.size	_ZTS18btHinge2Constraint, 21

	.type	_ZTI18btHinge2Constraint,@object # @_ZTI18btHinge2Constraint
	.section	.rodata._ZTI18btHinge2Constraint,"aG",@progbits,_ZTI18btHinge2Constraint,comdat
	.weak	_ZTI18btHinge2Constraint
	.p2align	4
_ZTI18btHinge2Constraint:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18btHinge2Constraint
	.quad	_ZTI29btGeneric6DofSpringConstraint
	.size	_ZTI18btHinge2Constraint, 24

	.type	_ZZN11btTransform11getIdentityEvE17identityTransform,@object # @_ZZN11btTransform11getIdentityEvE17identityTransform
	.section	.bss._ZZN11btTransform11getIdentityEvE17identityTransform,"aGw",@nobits,_ZZN11btTransform11getIdentityEvE17identityTransform,comdat
	.weak	_ZZN11btTransform11getIdentityEvE17identityTransform
	.p2align	2
_ZZN11btTransform11getIdentityEvE17identityTransform:
	.zero	64
	.size	_ZZN11btTransform11getIdentityEvE17identityTransform, 64

	.type	_ZGVZN11btTransform11getIdentityEvE17identityTransform,@object # @_ZGVZN11btTransform11getIdentityEvE17identityTransform
	.section	.bss._ZGVZN11btTransform11getIdentityEvE17identityTransform,"aGw",@nobits,_ZGVZN11btTransform11getIdentityEvE17identityTransform,comdat
	.weak	_ZGVZN11btTransform11getIdentityEvE17identityTransform
	.p2align	3
_ZGVZN11btTransform11getIdentityEvE17identityTransform:
	.quad	0                       # 0x0
	.size	_ZGVZN11btTransform11getIdentityEvE17identityTransform, 8

	.type	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix,@object # @_ZZN11btMatrix3x311getIdentityEvE14identityMatrix
	.section	.bss._ZZN11btMatrix3x311getIdentityEvE14identityMatrix,"aGw",@nobits,_ZZN11btMatrix3x311getIdentityEvE14identityMatrix,comdat
	.weak	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix
	.p2align	2
_ZZN11btMatrix3x311getIdentityEvE14identityMatrix:
	.zero	48
	.size	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix, 48

	.type	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix,@object # @_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix
	.section	.bss._ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix,"aGw",@nobits,_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix,comdat
	.weak	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix
	.p2align	3
_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix:
	.quad	0                       # 0x0
	.size	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, 8


	.globl	_ZN18btHinge2ConstraintC1ER11btRigidBodyS1_R9btVector3S3_S3_
	.type	_ZN18btHinge2ConstraintC1ER11btRigidBodyS1_R9btVector3S3_S3_,@function
_ZN18btHinge2ConstraintC1ER11btRigidBodyS1_R9btVector3S3_S3_ = _ZN18btHinge2ConstraintC2ER11btRigidBodyS1_R9btVector3S3_S3_
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
