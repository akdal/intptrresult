	.text
	.file	"zdict.bc"
	.globl	zdict
	.p2align	4, 0x90
	.type	zdict,@function
zdict:                                  # @zdict
	.cfi_startproc
# BB#0:
	movq	%rdi, %rcx
	movzwl	8(%rcx), %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$20, %edx
	jne	.LBB0_3
# BB#1:
	movq	(%rcx), %rdi
	movl	$-15, %eax
	testq	%rdi, %rdi
	js	.LBB0_3
# BB#2:
	movl	dict_max_size(%rip), %edx
	cmpq	%rdx, %rdi
	jle	.LBB0_4
.LBB0_3:
	retq
.LBB0_4:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%rcx, %rsi
	jmp	dict_create             # TAILCALL
.Lfunc_end0:
	.size	zdict, .Lfunc_end0-zdict
	.cfi_endproc

	.globl	zmaxlength
	.p2align	4, 0x90
	.type	zmaxlength,@function
zmaxlength:                             # @zmaxlength
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$8, %ecx
	jne	.LBB1_3
# BB#1:
	movq	%rbx, %rdi
	callq	dict_access_ref
	movq	%rax, %rcx
	movl	$-7, %eax
	testb	$2, 9(%rcx)
	je	.LBB1_3
# BB#2:
	movq	%rbx, %rdi
	callq	dict_maxlength
	movl	%eax, %eax
	movq	%rax, (%rbx)
	movw	$20, 8(%rbx)
	xorl	%eax, %eax
.LBB1_3:
	popq	%rbx
	retq
.Lfunc_end1:
	.size	zmaxlength, .Lfunc_end1-zmaxlength
	.cfi_endproc

	.globl	zsetmaxlength
	.p2align	4, 0x90
	.type	zsetmaxlength,@function
zsetmaxlength:                          # @zsetmaxlength
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movzwl	-8(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$8, %ecx
	jne	.LBB2_8
# BB#1:
	leaq	-16(%rbx), %r14
	movq	%r14, %rdi
	callq	dict_access_ref
	movq	%rax, %rcx
	movl	$-7, %eax
	testb	$1, 9(%rcx)
	je	.LBB2_8
# BB#2:
	movzwl	8(%rbx), %eax
	andl	$252, %eax
	cmpl	$20, %eax
	movl	$-20, %eax
	jne	.LBB2_8
# BB#3:
	movq	(%rbx), %rbx
	movl	$-15, %eax
	testq	%rbx, %rbx
	js	.LBB2_8
# BB#4:
	movl	dict_max_size(%rip), %ecx
	cmpq	%rcx, %rbx
	jg	.LBB2_8
# BB#5:
	movq	%r14, %rdi
	callq	dict_length
	movl	%eax, %ecx
	movl	$-2, %eax
	cmpl	%ebx, %ecx
	ja	.LBB2_8
# BB#6:
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	dict_resize
	testl	%eax, %eax
	js	.LBB2_8
# BB#7:
	addq	$-32, osp(%rip)
.LBB2_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	zsetmaxlength, .Lfunc_end2-zsetmaxlength
	.cfi_endproc

	.globl	zbegin
	.p2align	4, 0x90
	.type	zbegin,@function
zbegin:                                 # @zbegin
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
.Lcfi8:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$8, %ecx
	jne	.LBB3_5
# BB#1:
	movq	%rbx, %rdi
	callq	dict_access_ref
	movq	%rax, %rcx
	movl	$-7, %eax
	testb	$2, 9(%rcx)
	je	.LBB3_5
# BB#2:
	movq	dsp(%rip), %rax
	cmpq	dstop(%rip), %rax
	je	.LBB3_3
# BB#4:
	leaq	16(%rax), %rcx
	movq	%rcx, dsp(%rip)
	movups	(%rbx), %xmm0
	movups	%xmm0, 16(%rax)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB3_5:
	popq	%rbx
	retq
.LBB3_3:
	movl	$-3, %eax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	zbegin, .Lfunc_end3-zbegin
	.cfi_endproc

	.globl	zend
	.p2align	4, 0x90
	.type	zend,@function
zend:                                   # @zend
	.cfi_startproc
# BB#0:
	movq	dsp(%rip), %rax
	movl	$dstack+16, %ecx
	cmpq	%rcx, %rax
	je	.LBB4_1
# BB#2:
	addq	$-16, %rax
	movq	%rax, dsp(%rip)
	xorl	%eax, %eax
	retq
.LBB4_1:
	movl	$-4, %eax
	retq
.Lfunc_end4:
	.size	zend, .Lfunc_end4-zend
	.cfi_endproc

	.globl	zdef
	.p2align	4, 0x90
	.type	zdef,@function
zdef:                                   # @zdef
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$-17, %eax
	cmpq	%rbx, osp_nargs+8(%rip)
	ja	.LBB5_5
# BB#1:
	movzwl	-8(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$32, %ecx
	je	.LBB5_5
# BB#2:
	movq	dsp(%rip), %rdi
	callq	dict_access_ref
	movq	%rax, %rcx
	movl	$-7, %eax
	testb	$1, 9(%rcx)
	je	.LBB5_5
# BB#3:
	leaq	-16(%rbx), %rsi
	movq	dsp(%rip), %rdi
	movq	%rbx, %rdx
	callq	dict_put
	testl	%eax, %eax
	jne	.LBB5_5
# BB#4:
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
.LBB5_5:
	popq	%rbx
	retq
.Lfunc_end5:
	.size	zdef, .Lfunc_end5-zdef
	.cfi_endproc

	.globl	zload
	.p2align	4, 0x90
	.type	zload,@function
zload:                                  # @zload
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$-17, %eax
	cmpq	%rbx, osp_nargs(%rip)
	ja	.LBB6_5
# BB#1:
	movzwl	8(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$32, %ecx
	je	.LBB6_5
# BB#2:
	movq	dsp(%rip), %rsi
	leaq	8(%rsp), %rcx
	movl	$dstack, %edi
	movq	%rbx, %rdx
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB6_3
# BB#4:
	movq	8(%rsp), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, (%rbx)
	xorl	%eax, %eax
	jmp	.LBB6_5
.LBB6_3:
	movl	$-21, %eax
.LBB6_5:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end6:
	.size	zload, .Lfunc_end6-zload
	.cfi_endproc

	.globl	zstore
	.p2align	4, 0x90
	.type	zstore,@function
zstore:                                 # @zstore
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$-17, %eax
	cmpq	%rbx, osp_nargs+8(%rip)
	ja	.LBB7_6
# BB#1:
	movzwl	-8(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$32, %ecx
	je	.LBB7_6
# BB#2:
	leaq	-16(%rbx), %r14
	movq	dsp(%rip), %rsi
	movq	%rsp, %rcx
	movl	$dstack, %edi
	movq	%r14, %rdx
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB7_3
# BB#4:
	movq	(%rsp), %rax
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rax)
	jmp	.LBB7_5
.LBB7_3:
	movq	dsp(%rip), %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	dict_put
	testl	%eax, %eax
	jne	.LBB7_6
.LBB7_5:
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
.LBB7_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	zstore, .Lfunc_end7-zstore
	.cfi_endproc

	.globl	zknown
	.p2align	4, 0x90
	.type	zknown,@function
zknown:                                 # @zknown
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 48
.Lcfi23:
	.cfi_offset %rbx, -32
.Lcfi24:
	.cfi_offset %r14, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	-8(%rbx), %eax
	andl	$252, %eax
	movl	$-20, %ebp
	cmpl	$8, %eax
	jne	.LBB8_5
# BB#1:
	leaq	-16(%rbx), %r14
	movq	%r14, %rdi
	callq	dict_access_ref
	movl	$-7, %ebp
	testb	$2, 9(%rax)
	je	.LBB8_5
# BB#2:
	movzwl	8(%rbx), %eax
	andl	$252, %eax
	xorl	%ebp, %ebp
	cmpl	$32, %eax
	movl	$0, %ecx
	je	.LBB8_4
# BB#3:
	leaq	8(%rsp), %rcx
	movq	%r14, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	dict_lookup
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setg	%cl
.LBB8_4:
	movw	%cx, -16(%rbx)
	movw	$4, -8(%rbx)
	addq	$-16, osp(%rip)
.LBB8_5:
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	zknown, .Lfunc_end8-zknown
	.cfi_endproc

	.globl	zwhere
	.p2align	4, 0x90
	.type	zwhere,@function
zwhere:                                 # @zwhere
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r12, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	$-17, %eax
	cmpq	%r14, osp_nargs(%rip)
	ja	.LBB9_12
# BB#1:
	movzwl	8(%r14), %eax
	andl	$252, %eax
	cmpl	$32, %eax
	je	.LBB9_7
# BB#2:
	movq	dsp(%rip), %rbx
	movq	%rsp, %r15
	movl	$dstack, %r12d
	.p2align	4, 0x90
.LBB9_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	dict_access_ref
	testb	$2, 9(%rax)
	je	.LBB9_4
# BB#5:                                 #   in Loop: Header=BB9_3 Depth=1
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	dict_lookup
	testl	%eax, %eax
	jg	.LBB9_8
# BB#6:                                 #   in Loop: Header=BB9_3 Depth=1
	addq	$-16, %rbx
	cmpq	%r12, %rbx
	jae	.LBB9_3
.LBB9_7:
	movw	$0, (%r14)
	movw	$4, 8(%r14)
.LBB9_11:                               # %.loopexit
	xorl	%eax, %eax
.LBB9_12:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB9_4:
	movl	$-7, %eax
	jmp	.LBB9_12
.LBB9_8:
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
	leaq	16(%r14), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB9_10
# BB#9:
	movq	%r14, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB9_12
.LBB9_10:
	movw	$1, 16(%r14)
	movw	$4, 24(%r14)
	jmp	.LBB9_11
.Lfunc_end9:
	.size	zwhere, .Lfunc_end9-zwhere
	.cfi_endproc

	.globl	zcopy_dict
	.p2align	4, 0x90
	.type	zcopy_dict,@function
zcopy_dict:                             # @zcopy_dict
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 48
.Lcfi40:
	.cfi_offset %rbx, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	-8(%rbx), %eax
	andl	$252, %eax
	movl	$-20, %ebp
	cmpl	$8, %eax
	jne	.LBB10_6
# BB#1:
	leaq	-16(%rbx), %r14
	movq	%r14, %rdi
	callq	dict_access_ref
	movl	$-7, %ebp
	testb	$2, 9(%rax)
	je	.LBB10_6
# BB#2:
	movq	%rbx, %rdi
	callq	dict_access_ref
	testb	$1, 9(%rax)
	je	.LBB10_6
# BB#3:
	movq	%rbx, %rdi
	callq	dict_length
	movl	$-15, %ebp
	testl	%eax, %eax
	jne	.LBB10_6
# BB#4:
	movq	%rbx, %rdi
	callq	dict_maxlength
	movl	%eax, %r15d
	movq	%r14, %rdi
	callq	dict_maxlength
	cmpl	%eax, %r15d
	jb	.LBB10_6
# BB#5:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	dict_copy
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
	addq	$-16, osp(%rip)
	xorl	%ebp, %ebp
.LBB10_6:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	zcopy_dict, .Lfunc_end10-zcopy_dict
	.cfi_endproc

	.globl	zcurrentdict
	.p2align	4, 0x90
	.type	zcurrentdict,@function
zcurrentdict:                           # @zcurrentdict
	.cfi_startproc
# BB#0:
	leaq	16(%rdi), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB11_2
# BB#1:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB11_2:
	movq	dsp(%rip), %rcx
	movups	(%rcx), %xmm0
	movups	%xmm0, (%rax)
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	zcurrentdict, .Lfunc_end11-zcurrentdict
	.cfi_endproc

	.globl	zcountdictstack
	.p2align	4, 0x90
	.type	zcountdictstack,@function
zcountdictstack:                        # @zcountdictstack
	.cfi_startproc
# BB#0:
	leaq	16(%rdi), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB12_2
# BB#1:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB12_2:
	movq	dsp(%rip), %rax
	movl	$dstack, %ecx
	subq	%rcx, %rax
	sarq	$4, %rax
	incq	%rax
	movq	%rax, 16(%rdi)
	movw	$20, 24(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end12:
	.size	zcountdictstack, .Lfunc_end12-zcountdictstack
	.cfi_endproc

	.globl	zdictstack
	.p2align	4, 0x90
	.type	zdictstack,@function
zdictstack:                             # @zdictstack
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 16
.Lcfi45:
	.cfi_offset %rbx, -16
	movzwl	8(%rdi), %eax
	movl	$-20, %ebx
	testb	$-4, %al
	jne	.LBB13_4
# BB#1:
	movl	$-7, %ebx
	testb	$1, %ah
	je	.LBB13_4
# BB#2:
	movq	dsp(%rip), %rdx
	movl	$dstack, %ecx
	subq	%rcx, %rdx
	shrq	$4, %rdx
	incq	%rdx
	movzwl	10(%rdi), %ecx
	movl	$-15, %ebx
	cmpl	%ecx, %edx
	jg	.LBB13_4
# BB#3:
	movw	%dx, 10(%rdi)
	orl	$32768, %eax            # imm = 0x8000
	movw	%ax, 8(%rdi)
	movq	(%rdi), %rdi
	xorl	%ebx, %ebx
	movl	$dstack, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	refcpy
.LBB13_4:
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end13:
	.size	zdictstack, .Lfunc_end13-zdictstack
	.cfi_endproc

	.globl	zdict_op_init
	.p2align	4, 0x90
	.type	zdict_op_init,@function
zdict_op_init:                          # @zdict_op_init
	.cfi_startproc
# BB#0:
	movl	$zdict_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end14:
	.size	zdict_op_init, .Lfunc_end14-zdict_op_init
	.cfi_endproc

	.type	zdict_op_init.my_defs,@object # @zdict_op_init.my_defs
	.data
	.p2align	4
zdict_op_init.my_defs:
	.quad	.L.str
	.quad	zbegin
	.quad	.L.str.1
	.quad	zcountdictstack
	.quad	.L.str.2
	.quad	zcurrentdict
	.quad	.L.str.3
	.quad	zdef
	.quad	.L.str.4
	.quad	zdict
	.quad	.L.str.5
	.quad	zdictstack
	.quad	.L.str.6
	.quad	zend
	.quad	.L.str.7
	.quad	zknown
	.quad	.L.str.8
	.quad	zload
	.quad	.L.str.9
	.quad	zmaxlength
	.quad	.L.str.10
	.quad	zsetmaxlength
	.quad	.L.str.11
	.quad	zstore
	.quad	.L.str.12
	.quad	zwhere
	.zero	16
	.size	zdict_op_init.my_defs, 224

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"1begin"
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"0countdictstack"
	.size	.L.str.1, 16

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"0currentdict"
	.size	.L.str.2, 13

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"2def"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"1dict"
	.size	.L.str.4, 6

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"0dictstack"
	.size	.L.str.5, 11

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"0end"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"2known"
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"1load"
	.size	.L.str.8, 6

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"1maxlength"
	.size	.L.str.9, 11

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"2setmaxlength"
	.size	.L.str.10, 14

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"2store"
	.size	.L.str.11, 7

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"1where"
	.size	.L.str.12, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
