	.text
	.file	"matrix.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI0_1:
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.text
	.globl	mkmatrix
	.p2align	4, 0x90
	.type	mkmatrix,@function
mkmatrix:                               # @mkmatrix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movl	%edi, %r14d
	movslq	%r14d, %rbx
	leaq	(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, (%rsp)            # 8-byte Spill
	testl	%ebx, %ebx
	jle	.LBB0_14
# BB#1:                                 # %.lr.ph28
	movslq	%ebp, %rax
	leaq	(,%rax,4), %r15
	testl	%eax, %eax
	jle	.LBB0_2
# BB#4:                                 # %.lr.ph28.split.us.preheader
	movl	%ebp, %r13d
	movl	%r14d, %ebx
	movl	%ebp, %eax
	andl	$7, %eax
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	subq	%rax, %r13
	movl	$1, %r14d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph28.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_9 Depth 2
                                        #     Child Loop BB0_12 Depth 2
	movq	%r15, %rdi
	callq	malloc
	cmpl	$8, %ebp
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rax, (%rcx,%r12,8)
	jb	.LBB0_6
# BB#7:                                 # %min.iters.checked
                                        #   in Loop: Header=BB0_5 Depth=1
	testq	%r13, %r13
	je	.LBB0_6
# BB#8:                                 # %vector.body.preheader
                                        #   in Loop: Header=BB0_5 Depth=1
	leal	(%r14,%r13), %ecx
	leaq	16(%rax), %rdx
	movq	%r13, %rsi
	movl	%r14d, %edi
	movdqa	.LCPI0_0(%rip), %xmm2   # xmm2 = [0,1,2,3]
	movdqa	.LCPI0_1(%rip), %xmm3   # xmm3 = [4,5,6,7]
	.p2align	4, 0x90
.LBB0_9:                                # %vector.body
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	%edi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, %xmm1
	paddd	%xmm2, %xmm1
	paddd	%xmm3, %xmm0
	movdqu	%xmm1, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$32, %rdx
	addl	$8, %edi
	addq	$-8, %rsi
	jne	.LBB0_9
# BB#10:                                # %middle.block
                                        #   in Loop: Header=BB0_5 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	%r13, %rsi
	jne	.LBB0_11
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_6:                                #   in Loop: Header=BB0_5 Depth=1
	xorl	%esi, %esi
	movl	%r14d, %ecx
.LBB0_11:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_5 Depth=1
	leaq	(%rax,%rsi,4), %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
	.p2align	4, 0x90
.LBB0_12:                               # %scalar.ph
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, (%rax)
	incl	%ecx
	addq	$4, %rax
	decq	%rdx
	jne	.LBB0_12
.LBB0_13:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_5 Depth=1
	addl	%ebp, %r14d
	incq	%r12
	cmpq	%rbx, %r12
	jne	.LBB0_5
	jmp	.LBB0_14
.LBB0_2:                                # %.lr.ph28.split.preheader
	movl	%r14d, %ebp
	movq	(%rsp), %rbx            # 8-byte Reload
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph28.split
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, (%rbx)
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB0_3
.LBB0_14:                               # %._crit_edge29
	movq	(%rsp), %rax            # 8-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	mkmatrix, .Lfunc_end0-mkmatrix
	.cfi_endproc

	.globl	zeromatrix
	.p2align	4, 0x90
	.type	zeromatrix,@function
zeromatrix:                             # @zeromatrix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%edi, %edi
	jle	.LBB1_7
# BB#1:
	testl	%esi, %esi
	jle	.LBB1_7
# BB#2:                                 # %.preheader.us.preheader
	decl	%esi
	leaq	4(,%rsi,4), %r12
	movl	%edi, %r15d
	leaq	-1(%r15), %r13
	movq	%r15, %rbp
	xorl	%ebx, %ebx
	andq	$7, %rbp
	je	.LBB1_4
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	incq	%rbx
	cmpq	%rbx, %rbp
	jne	.LBB1_3
.LBB1_4:                                # %.preheader.us.prol.loopexit
	cmpq	$7, %r13
	jb	.LBB1_7
# BB#5:                                 # %.preheader.us.preheader.new
	subq	%rbx, %r15
	leaq	56(%r14,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	addq	$64, %rbx
	addq	$-8, %r15
	jne	.LBB1_6
.LBB1_7:                                # %._crit_edge14
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	zeromatrix, .Lfunc_end1-zeromatrix
	.cfi_endproc

	.globl	freematrix
	.p2align	4, 0x90
	.type	freematrix,@function
freematrix:                             # @freematrix
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	testl	%edi, %edi
	jle	.LBB2_3
# BB#1:                                 # %.lr.ph.preheader
	movslq	%edi, %rbx
	incq	%rbx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-16(%r14,%rbx,8), %rdi
	callq	free
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB2_2
.LBB2_3:                                # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end2:
	.size	freematrix, .Lfunc_end2-freematrix
	.cfi_endproc

	.globl	mmult
	.p2align	4, 0x90
	.type	mmult,@function
mmult:                                  # @mmult
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 48
.Lcfi36:
	.cfi_offset %rbx, -48
.Lcfi37:
	.cfi_offset %r12, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	testl	%edi, %edi
	jle	.LBB3_9
# BB#1:
	testl	%esi, %esi
	jle	.LBB3_9
# BB#2:                                 # %.preheader.us.us.preheader.preheader
	movl	%esi, %r15d
	movl	%edi, %r9d
	leaq	-1(%r15), %r11
	movl	%r15d, %r12d
	andl	$3, %r12d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader.us.us.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
                                        #       Child Loop BB3_5 Depth 3
                                        #       Child Loop BB3_10 Depth 3
	movq	(%rdx,%r10,8), %rdi
	movq	(%r8,%r10,8), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_4:                                # %.preheader.us.us
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_5 Depth 3
                                        #       Child Loop BB3_10 Depth 3
	xorl	%esi, %esi
	xorl	%ebp, %ebp
	testq	%r12, %r12
	je	.LBB3_6
	.p2align	4, 0x90
.LBB3_5:                                #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx,%rsi,8), %rax
	movl	(%rax,%rbx,4), %eax
	imull	(%rdi,%rsi,4), %eax
	addl	%eax, %ebp
	incq	%rsi
	cmpq	%rsi, %r12
	jne	.LBB3_5
.LBB3_6:                                # %.prol.loopexit
                                        #   in Loop: Header=BB3_4 Depth=2
	cmpq	$3, %r11
	jb	.LBB3_7
	.p2align	4, 0x90
.LBB3_10:                               #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx,%rsi,8), %rax
	movl	(%rax,%rbx,4), %eax
	imull	(%rdi,%rsi,4), %eax
	addl	%ebp, %eax
	movq	8(%rcx,%rsi,8), %rbp
	movl	(%rbp,%rbx,4), %ebp
	imull	4(%rdi,%rsi,4), %ebp
	addl	%eax, %ebp
	movq	16(%rcx,%rsi,8), %rax
	movl	(%rax,%rbx,4), %eax
	imull	8(%rdi,%rsi,4), %eax
	addl	%ebp, %eax
	movq	24(%rcx,%rsi,8), %rbp
	movl	(%rbp,%rbx,4), %ebp
	imull	12(%rdi,%rsi,4), %ebp
	addl	%eax, %ebp
	addq	$4, %rsi
	cmpq	%rsi, %r15
	jne	.LBB3_10
.LBB3_7:                                # %._crit_edge.us.us
                                        #   in Loop: Header=BB3_4 Depth=2
	movl	%ebp, (%r14,%rbx,4)
	incq	%rbx
	cmpq	%rsi, %rbx
	jne	.LBB3_4
# BB#8:                                 # %._crit_edge35.us
                                        #   in Loop: Header=BB3_3 Depth=1
	incq	%r10
	cmpq	%r9, %r10
	jne	.LBB3_3
.LBB3_9:                                # %._crit_edge37
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	mmult, .Lfunc_end3-mmult
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
.LCPI4_1:
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
.LCPI4_2:
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	15                      # 0xf
.LCPI4_3:
	.long	16                      # 0x10
	.long	17                      # 0x11
	.long	18                      # 0x12
	.long	19                      # 0x13
.LCPI4_4:
	.long	22                      # 0x16
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	25                      # 0x19
.LCPI4_5:
	.long	26                      # 0x1a
	.long	27                      # 0x1b
	.long	28                      # 0x1c
	.long	29                      # 0x1d
.LCPI4_6:
	.long	32                      # 0x20
	.long	33                      # 0x21
	.long	34                      # 0x22
	.long	35                      # 0x23
.LCPI4_7:
	.long	36                      # 0x24
	.long	37                      # 0x25
	.long	38                      # 0x26
	.long	39                      # 0x27
.LCPI4_8:
	.long	42                      # 0x2a
	.long	43                      # 0x2b
	.long	44                      # 0x2c
	.long	45                      # 0x2d
.LCPI4_9:
	.long	46                      # 0x2e
	.long	47                      # 0x2f
	.long	48                      # 0x30
	.long	49                      # 0x31
.LCPI4_10:
	.long	52                      # 0x34
	.long	53                      # 0x35
	.long	54                      # 0x36
	.long	55                      # 0x37
.LCPI4_11:
	.long	56                      # 0x38
	.long	57                      # 0x39
	.long	58                      # 0x3a
	.long	59                      # 0x3b
.LCPI4_12:
	.long	62                      # 0x3e
	.long	63                      # 0x3f
	.long	64                      # 0x40
	.long	65                      # 0x41
.LCPI4_13:
	.long	66                      # 0x42
	.long	67                      # 0x43
	.long	68                      # 0x44
	.long	69                      # 0x45
.LCPI4_14:
	.long	72                      # 0x48
	.long	73                      # 0x49
	.long	74                      # 0x4a
	.long	75                      # 0x4b
.LCPI4_15:
	.long	76                      # 0x4c
	.long	77                      # 0x4d
	.long	78                      # 0x4e
	.long	79                      # 0x4f
.LCPI4_16:
	.long	82                      # 0x52
	.long	83                      # 0x53
	.long	84                      # 0x54
	.long	85                      # 0x55
.LCPI4_17:
	.long	86                      # 0x56
	.long	87                      # 0x57
	.long	88                      # 0x58
	.long	89                      # 0x59
.LCPI4_18:
	.long	92                      # 0x5c
	.long	93                      # 0x5d
	.long	94                      # 0x5e
	.long	95                      # 0x5f
.LCPI4_19:
	.long	96                      # 0x60
	.long	97                      # 0x61
	.long	98                      # 0x62
	.long	99                      # 0x63
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi47:
	.cfi_def_cfa_offset 224
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movl	$3000000, %r15d         # imm = 0x2DC6C0
	cmpl	$2, %edi
	jne	.LBB4_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r15
.LBB4_2:                                # %.lr.ph28.split.us.i39
	movl	$80, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	$40, %edi
	callq	malloc
	movq	%rax, (%rbx)
	movl	$1, (%rax)
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [2,3,4,5]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_1(%rip), %xmm0   # xmm0 = [6,7,8,9]
	movups	%xmm0, 20(%rax)
	movl	$10, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 8(%rbx)
	movl	$11, (%rax)
	movaps	.LCPI4_2(%rip), %xmm0   # xmm0 = [12,13,14,15]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_3(%rip), %xmm0   # xmm0 = [16,17,18,19]
	movups	%xmm0, 20(%rax)
	movl	$20, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 16(%rbx)
	movl	$21, (%rax)
	movaps	.LCPI4_4(%rip), %xmm0   # xmm0 = [22,23,24,25]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_5(%rip), %xmm0   # xmm0 = [26,27,28,29]
	movups	%xmm0, 20(%rax)
	movl	$30, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 24(%rbx)
	movl	$31, (%rax)
	movaps	.LCPI4_6(%rip), %xmm0   # xmm0 = [32,33,34,35]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_7(%rip), %xmm0   # xmm0 = [36,37,38,39]
	movups	%xmm0, 20(%rax)
	movl	$40, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 32(%rbx)
	movl	$41, (%rax)
	movaps	.LCPI4_8(%rip), %xmm0   # xmm0 = [42,43,44,45]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_9(%rip), %xmm0   # xmm0 = [46,47,48,49]
	movups	%xmm0, 20(%rax)
	movl	$50, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 40(%rbx)
	movl	$51, (%rax)
	movaps	.LCPI4_10(%rip), %xmm0  # xmm0 = [52,53,54,55]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_11(%rip), %xmm0  # xmm0 = [56,57,58,59]
	movups	%xmm0, 20(%rax)
	movl	$60, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 48(%rbx)
	movl	$61, (%rax)
	movaps	.LCPI4_12(%rip), %xmm0  # xmm0 = [62,63,64,65]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_13(%rip), %xmm0  # xmm0 = [66,67,68,69]
	movups	%xmm0, 20(%rax)
	movl	$70, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 56(%rbx)
	movl	$71, (%rax)
	movaps	.LCPI4_14(%rip), %xmm0  # xmm0 = [72,73,74,75]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_15(%rip), %xmm0  # xmm0 = [76,77,78,79]
	movups	%xmm0, 20(%rax)
	movl	$80, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 64(%rbx)
	movl	$81, (%rax)
	movaps	.LCPI4_16(%rip), %xmm0  # xmm0 = [82,83,84,85]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_17(%rip), %xmm0  # xmm0 = [86,87,88,89]
	movups	%xmm0, 20(%rax)
	movl	$90, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	%rax, 72(%rbx)
	movl	$91, (%rax)
	movaps	.LCPI4_18(%rip), %xmm0  # xmm0 = [92,93,94,95]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_19(%rip), %xmm0  # xmm0 = [96,97,98,99]
	movups	%xmm0, 20(%rax)
	movl	$100, 36(%rax)
	movl	$80, %edi
	callq	malloc
	movq	%rax, %r14
	movl	$40, %edi
	callq	malloc
	movq	%rax, (%r14)
	movl	$1, (%rax)
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [2,3,4,5]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_1(%rip), %xmm0   # xmm0 = [6,7,8,9]
	movups	%xmm0, 20(%rax)
	movl	$10, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 8(%r14)
	movl	$11, (%rax)
	movaps	.LCPI4_2(%rip), %xmm0   # xmm0 = [12,13,14,15]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_3(%rip), %xmm0   # xmm0 = [16,17,18,19]
	movups	%xmm0, 20(%rax)
	movl	$20, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 16(%r14)
	movl	$21, (%rax)
	movaps	.LCPI4_4(%rip), %xmm0   # xmm0 = [22,23,24,25]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_5(%rip), %xmm0   # xmm0 = [26,27,28,29]
	movups	%xmm0, 20(%rax)
	movl	$30, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 24(%r14)
	movl	$31, (%rax)
	movaps	.LCPI4_6(%rip), %xmm0   # xmm0 = [32,33,34,35]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_7(%rip), %xmm0   # xmm0 = [36,37,38,39]
	movups	%xmm0, 20(%rax)
	movl	$40, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 32(%r14)
	movl	$41, (%rax)
	movaps	.LCPI4_8(%rip), %xmm0   # xmm0 = [42,43,44,45]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_9(%rip), %xmm0   # xmm0 = [46,47,48,49]
	movups	%xmm0, 20(%rax)
	movl	$50, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 40(%r14)
	movl	$51, (%rax)
	movaps	.LCPI4_10(%rip), %xmm0  # xmm0 = [52,53,54,55]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_11(%rip), %xmm0  # xmm0 = [56,57,58,59]
	movups	%xmm0, 20(%rax)
	movl	$60, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 48(%r14)
	movl	$61, (%rax)
	movaps	.LCPI4_12(%rip), %xmm0  # xmm0 = [62,63,64,65]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_13(%rip), %xmm0  # xmm0 = [66,67,68,69]
	movups	%xmm0, 20(%rax)
	movl	$70, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 56(%r14)
	movl	$71, (%rax)
	movaps	.LCPI4_14(%rip), %xmm0  # xmm0 = [72,73,74,75]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_15(%rip), %xmm0  # xmm0 = [76,77,78,79]
	movups	%xmm0, 20(%rax)
	movl	$80, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 64(%r14)
	movl	$81, (%rax)
	movaps	.LCPI4_16(%rip), %xmm0  # xmm0 = [82,83,84,85]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_17(%rip), %xmm0  # xmm0 = [86,87,88,89]
	movups	%xmm0, 20(%rax)
	movl	$90, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 72(%r14)
	movl	$91, (%rax)
	movaps	.LCPI4_18(%rip), %xmm0  # xmm0 = [92,93,94,95]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_19(%rip), %xmm0  # xmm0 = [96,97,98,99]
	movups	%xmm0, 20(%rax)
	movl	$100, 36(%rax)
	movl	$80, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	$40, %edi
	callq	malloc
	movq	%rax, (%rbx)
	movl	$1, (%rax)
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [2,3,4,5]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_1(%rip), %xmm0   # xmm0 = [6,7,8,9]
	movups	%xmm0, 20(%rax)
	movl	$10, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 8(%rbx)
	movl	$11, (%rax)
	movaps	.LCPI4_2(%rip), %xmm0   # xmm0 = [12,13,14,15]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_3(%rip), %xmm0   # xmm0 = [16,17,18,19]
	movups	%xmm0, 20(%rax)
	movl	$20, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 16(%rbx)
	movl	$21, (%rax)
	movaps	.LCPI4_4(%rip), %xmm0   # xmm0 = [22,23,24,25]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_5(%rip), %xmm0   # xmm0 = [26,27,28,29]
	movups	%xmm0, 20(%rax)
	movl	$30, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 24(%rbx)
	movl	$31, (%rax)
	movaps	.LCPI4_6(%rip), %xmm0   # xmm0 = [32,33,34,35]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_7(%rip), %xmm0   # xmm0 = [36,37,38,39]
	movups	%xmm0, 20(%rax)
	movl	$40, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 32(%rbx)
	movl	$41, (%rax)
	movaps	.LCPI4_8(%rip), %xmm0   # xmm0 = [42,43,44,45]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_9(%rip), %xmm0   # xmm0 = [46,47,48,49]
	movups	%xmm0, 20(%rax)
	movl	$50, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 40(%rbx)
	movl	$51, (%rax)
	movaps	.LCPI4_10(%rip), %xmm0  # xmm0 = [52,53,54,55]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_11(%rip), %xmm0  # xmm0 = [56,57,58,59]
	movups	%xmm0, 20(%rax)
	movl	$60, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 48(%rbx)
	movl	$61, (%rax)
	movaps	.LCPI4_12(%rip), %xmm0  # xmm0 = [62,63,64,65]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_13(%rip), %xmm0  # xmm0 = [66,67,68,69]
	movups	%xmm0, 20(%rax)
	movl	$70, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 56(%rbx)
	movl	$71, (%rax)
	movaps	.LCPI4_14(%rip), %xmm0  # xmm0 = [72,73,74,75]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_15(%rip), %xmm0  # xmm0 = [76,77,78,79]
	movups	%xmm0, 20(%rax)
	movl	$80, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rax, 64(%rbx)
	movl	$81, (%rax)
	movaps	.LCPI4_16(%rip), %xmm0  # xmm0 = [82,83,84,85]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_17(%rip), %xmm0  # xmm0 = [86,87,88,89]
	movups	%xmm0, 20(%rax)
	movl	$90, 36(%rax)
	movl	$40, %edi
	callq	malloc
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movq	%rax, 72(%rbx)
	movl	$91, (%rax)
	movaps	.LCPI4_18(%rip), %xmm0  # xmm0 = [92,93,94,95]
	movups	%xmm0, 4(%rax)
	movaps	.LCPI4_19(%rip), %xmm0  # xmm0 = [96,97,98,99]
	movups	%xmm0, 20(%rax)
	movl	$100, 36(%rax)
	testl	%r15d, %r15d
	jle	.LBB4_11
# BB#3:                                 # %.preheader.us.us.preheader.i.preheader.preheader
	movq	(%r14), %rdi
	movq	8(%r14), %rbp
	leaq	8(%r14), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	16(%r14), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	16(%r14), %rbx
	leaq	24(%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	24(%r14), %rdx
	leaq	32(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	32(%r14), %r13
	leaq	40(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	40(%r14), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	leaq	48(%r14), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	48(%r14), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	56(%r14), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r14, %rax
	addq	$72, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	56(%r14), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	64(%r14), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	64(%r14), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movq	72(%r14), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%r15, 96(%rsp)          # 8-byte Spill
	movq	144(%rsp), %r8          # 8-byte Reload
	movq	136(%rsp), %r9          # 8-byte Reload
	movq	120(%rsp), %r10         # 8-byte Reload
	movq	112(%rsp), %r12         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_4:                                # %.preheader.us.us.preheader.i.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
                                        #       Child Loop BB4_6 Depth 3
	movq	%rax, 104(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_5:                                # %.preheader.us.us.preheader.i
                                        #   Parent Loop BB4_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_6 Depth 3
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi,%rax,8), %r14
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	(%rsi,%rax,8), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	xorl	%r11d, %r11d
	movq	128(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_6:                                # %.preheader.us.us.i
                                        #   Parent Loop BB4_4 Depth=1
                                        #     Parent Loop BB4_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r13, %rax
	movl	(%rdi,%r11), %r13d
	imull	(%r14), %r13d
	movl	(%rbp,%r11), %r15d
	imull	4(%r14), %r15d
	addl	%r13d, %r15d
	movq	%rax, %r13
	movl	(%rbx,%r11), %esi
	imull	8(%r14), %esi
	addl	%r15d, %esi
	movq	%rbx, %rax
	movq	%rbp, %rbx
	movq	%rdi, %rbp
	movl	(%rdx,%r11), %edi
	imull	12(%r14), %edi
	addl	%esi, %edi
	movl	(%r13,%r11), %esi
	imull	16(%r14), %esi
	addl	%edi, %esi
	movl	(%r8,%r11), %edi
	imull	20(%r14), %edi
	addl	%esi, %edi
	movl	(%r9,%r11), %esi
	imull	24(%r14), %esi
	addl	%edi, %esi
	movl	(%rcx,%r11), %edi
	imull	28(%r14), %edi
	addl	%esi, %edi
	movl	(%r10,%r11), %esi
	imull	32(%r14), %esi
	addl	%edi, %esi
	movl	(%r12,%r11), %edi
	imull	36(%r14), %edi
	addl	%esi, %edi
	movq	160(%rsp), %rsi         # 8-byte Reload
	movl	%edi, (%rsi,%r11)
	movq	%rbp, %rdi
	movq	%rbx, %rbp
	movq	%rax, %rbx
	addq	$4, %r11
	cmpq	$40, %r11
	jne	.LBB4_6
# BB#7:                                 # %._crit_edge35.us.i
                                        #   in Loop: Header=BB4_5 Depth=2
	movq	152(%rsp), %rax         # 8-byte Reload
	incq	%rax
	cmpq	$10, %rax
	jne	.LBB4_5
# BB#8:                                 # %mmult.exit
                                        #   in Loop: Header=BB4_4 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	incl	%eax
	movq	96(%rsp), %r15          # 8-byte Reload
	cmpl	%r15d, %eax
	jne	.LBB4_4
# BB#9:
	movq	88(%rsp), %r14          # 8-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	jmp	.LBB4_10
.LBB4_11:                               # %mkmatrix.exit47.preheader.mkmatrix.exit47._crit_edge_crit_edge
	leaq	64(%r14), %r12
	leaq	56(%r14), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	48(%r14), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	40(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	32(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	24(%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	16(%r14), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	72(%r14), %r13
	movq	%r14, %r15
	addq	$8, %r15
.LBB4_10:                               # %mkmatrix.exit47._crit_edge
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rax
	movq	16(%rbp), %rcx
	movl	(%rax), %esi
	movl	12(%rcx), %edx
	movq	24(%rbp), %rax
	movl	8(%rax), %ecx
	movq	32(%rbp), %rax
	movl	16(%rax), %r8d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	72(%rbx), %rdi
	callq	free
	movq	64(%rbx), %rdi
	callq	free
	movq	56(%rbx), %rdi
	callq	free
	movq	48(%rbx), %rdi
	callq	free
	movq	40(%rbx), %rdi
	callq	free
	movq	32(%rbx), %rdi
	callq	free
	movq	24(%rbx), %rdi
	callq	free
	movq	16(%rbx), %rdi
	callq	free
	movq	8(%rbx), %rdi
	callq	free
	movq	(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	(%r13), %rdi
	callq	free
	movq	(%r12), %rdi
	callq	free
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rdi
	callq	free
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rdi
	callq	free
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	free
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	free
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	free
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	free
	movq	(%r15), %rdi
	callq	free
	movq	(%r14), %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	72(%rbp), %rdi
	callq	free
	movq	64(%rbp), %rdi
	callq	free
	movq	56(%rbp), %rdi
	callq	free
	movq	48(%rbp), %rdi
	callq	free
	movq	40(%rbp), %rdi
	callq	free
	movq	32(%rbp), %rdi
	callq	free
	movq	24(%rbp), %rdi
	callq	free
	movq	16(%rbp), %rdi
	callq	free
	movq	8(%rbp), %rdi
	callq	free
	movq	(%rbp), %rdi
	callq	free
	movq	%rbp, %rdi
	callq	free
	xorl	%eax, %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	main, .Lfunc_end4-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d %d %d %d\n"
	.size	.L.str, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
