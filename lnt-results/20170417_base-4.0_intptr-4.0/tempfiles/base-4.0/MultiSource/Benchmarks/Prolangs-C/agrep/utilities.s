	.text
	.file	"utilities.bc"
	.globl	Push
	.p2align	4, 0x90
	.type	Push,@function
Push:                                   # @Push
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$24, %edi
	callq	malloc
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB0_6
# BB#1:
	testq	%rax, %rax
	je	.LBB0_6
# BB#2:
	movq	(%rbx), %rcx
	movq	%rcx, 16(%rax)
	movq	%r14, (%rax)
	testq	%rcx, %rcx
	je	.LBB0_3
# BB#4:
	movl	8(%rcx), %ecx
	incl	%ecx
	jmp	.LBB0_5
.LBB0_3:
	movl	$1, %ecx
.LBB0_5:
	movl	%ecx, 8(%rax)
	movq	%rax, (%rbx)
	movq	%rax, %rcx
.LBB0_6:
	movq	%rcx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	Push, .Lfunc_end0-Push
	.cfi_endproc

	.globl	Pop
	.p2align	4, 0x90
	.type	Pop,@function
Pop:                                    # @Pop
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#1:
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.LBB1_4
# BB#2:
	movq	(%rax), %rbx
	movq	16(%rax), %rcx
	movq	%rcx, (%rdi)
	movq	%rax, %rdi
	callq	free
	jmp	.LBB1_5
.LBB1_4:
	xorl	%ebx, %ebx
.LBB1_5:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	Pop, .Lfunc_end1-Pop
	.cfi_endproc

	.globl	Top
	.p2align	4, 0x90
	.type	Top,@function
Top:                                    # @Top
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB2_1
# BB#2:
	movq	(%rdi), %rax
	retq
.LBB2_1:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	Top, .Lfunc_end2-Top
	.cfi_endproc

	.globl	Size
	.p2align	4, 0x90
	.type	Size,@function
Size:                                   # @Size
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB3_1
# BB#2:
	movl	8(%rdi), %eax
	retq
.LBB3_1:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	Size, .Lfunc_end3-Size
	.cfi_endproc

	.globl	occurs_in
	.p2align	4, 0x90
	.type	occurs_in,@function
occurs_in:                              # @occurs_in
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.LBB4_2
	jmp	.LBB4_4
	.p2align	4, 0x90
.LBB4_5:                                #   in Loop: Header=BB4_2 Depth=1
	movq	8(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB4_4
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%edi, (%rsi)
	jne	.LBB4_5
# BB#3:
	movl	$1, %eax
.LBB4_4:                                # %._crit_edge
	retq
.Lfunc_end4:
	.size	occurs_in, .Lfunc_end4-occurs_in
	.cfi_endproc

	.globl	pset_union
	.p2align	4, 0x90
	.type	pset_union,@function
pset_union:                             # @pset_union
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 48
.Lcfi12:
	.cfi_offset %rbx, -48
.Lcfi13:
	.cfi_offset %r12, -40
.Lcfi14:
	.cfi_offset %r13, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB5_22
# BB#1:                                 # %.lr.ph
	testq	%r14, %r14
	je	.LBB5_13
# BB#2:                                 # %.lr.ph.split.preheader
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_4 Depth 2
	movl	(%rbx), %eax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph.i
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%eax, (%rcx)
	je	.LBB5_11
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=2
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_4
# BB#6:                                 # %.loopexit
                                        #   in Loop: Header=BB5_3 Depth=1
	movl	$16, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB5_21
# BB#7:                                 #   in Loop: Header=BB5_3 Depth=1
	movl	(%rbx), %ecx
	movl	%ecx, (%rax)
	testq	%r12, %r12
	je	.LBB5_9
# BB#8:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	%rax, 8(%r15)
	jmp	.LBB5_10
.LBB5_9:                                #   in Loop: Header=BB5_3 Depth=1
	movq	%rax, %r12
.LBB5_10:                               # %occurs_in.exit
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	%rax, %r15
.LBB5_11:                               # %occurs_in.exit
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_3
# BB#12:                                # %._crit_edge
	testq	%r12, %r12
	jne	.LBB5_19
	jmp	.LBB5_22
.LBB5_13:                               # %.lr.ph.split.us.preheader
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB5_14:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB5_21
# BB#15:                                #   in Loop: Header=BB5_14 Depth=1
	movl	(%rbx), %eax
	movl	%eax, (%r15)
	testq	%r12, %r12
	je	.LBB5_17
# BB#16:                                #   in Loop: Header=BB5_14 Depth=1
	movq	%r15, 8(%r13)
	jmp	.LBB5_18
	.p2align	4, 0x90
.LBB5_17:                               #   in Loop: Header=BB5_14 Depth=1
	movq	%r15, %r12
.LBB5_18:                               # %occurs_in.exit.us
                                        #   in Loop: Header=BB5_14 Depth=1
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%r15, %r13
	jne	.LBB5_14
.LBB5_19:                               # %._crit_edge.thread38
	movq	%r14, 8(%r15)
	movq	%r12, %r14
	jmp	.LBB5_22
.LBB5_21:
	xorl	%r14d, %r14d
.LBB5_22:                               # %.loopexit26
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	pset_union, .Lfunc_end5-pset_union
	.cfi_endproc

	.globl	create_pos
	.p2align	4, 0x90
	.type	create_pos,@function
create_pos:                             # @create_pos
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 16
.Lcfi18:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movl	$16, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB6_1
# BB#2:
	movl	%ebx, (%rax)
	movq	$0, 8(%rax)
	popq	%rbx
	retq
.LBB6_1:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	create_pos, .Lfunc_end6-create_pos
	.cfi_endproc

	.globl	subset_pset
	.p2align	4, 0x90
	.type	subset_pset,@function
subset_pset:                            # @subset_pset
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB7_1
	.p2align	4, 0x90
.LBB7_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_5 Depth 2
                                        #       Child Loop BB7_6 Depth 3
	testq	%rsi, %rsi
	je	.LBB7_3
# BB#4:                                 # %.outer.split.us.preheader.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	xorl	%eax, %eax
.LBB7_5:                                # %.outer.split.us.preheader
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_6 Depth 3
	movl	%eax, %ecx
	.p2align	4, 0x90
.LBB7_6:                                # %.outer.split.us
                                        #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ecx, %eax
	cmpl	$1, %eax
	je	.LBB7_7
# BB#8:                                 #   in Loop: Header=BB7_6 Depth=3
	movl	(%rdi), %edx
	movl	$1, %ecx
	cmpl	(%rsi), %edx
	je	.LBB7_6
# BB#9:                                 # %.us-lcssa12.us
                                        #   in Loop: Header=BB7_5 Depth=2
	movq	8(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB7_5
# BB#10:                                #   in Loop: Header=BB7_2 Depth=1
	xorl	%esi, %esi
	jmp	.LBB7_11
	.p2align	4, 0x90
.LBB7_7:                                #   in Loop: Header=BB7_2 Depth=1
	movl	$1, %eax
	jmp	.LBB7_11
	.p2align	4, 0x90
.LBB7_3:                                #   in Loop: Header=BB7_2 Depth=1
	xorl	%esi, %esi
	xorl	%eax, %eax
.LBB7_11:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB7_13
# BB#12:                                # %.us-lcssa.us
                                        #   in Loop: Header=BB7_2 Depth=1
	testl	%eax, %eax
	jne	.LBB7_2
.LBB7_13:                               # %._crit_edge
	retq
.LBB7_1:
	movl	$1, %eax
	retq
.Lfunc_end7:
	.size	subset_pset, .Lfunc_end7-subset_pset
	.cfi_endproc

	.globl	eq_pset
	.p2align	4, 0x90
	.type	eq_pset,@function
eq_pset:                                # @eq_pset
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB8_13
# BB#1:                                 # %.preheader.i.preheader
	movq	%rsi, %r9
	movq	%rdi, %r8
	.p2align	4, 0x90
.LBB8_2:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_3 Depth 2
                                        #       Child Loop BB8_4 Depth 3
	xorl	%ecx, %ecx
	testq	%r9, %r9
	je	.LBB8_25
.LBB8_3:                                # %.outer.split.us.i.preheader
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_4 Depth 3
	movl	%ecx, %eax
	.p2align	4, 0x90
.LBB8_4:                                # %.outer.split.us.i
                                        #   Parent Loop BB8_2 Depth=1
                                        #     Parent Loop BB8_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, %ecx
	cmpl	$1, %ecx
	je	.LBB8_5
# BB#6:                                 #   in Loop: Header=BB8_4 Depth=3
	movl	(%r8), %edx
	movl	$1, %eax
	cmpl	(%r9), %edx
	je	.LBB8_4
# BB#7:                                 # %.us-lcssa12.us.i
                                        #   in Loop: Header=BB8_3 Depth=2
	movq	8(%r9), %r9
	testq	%r9, %r9
	jne	.LBB8_3
# BB#8:                                 #   in Loop: Header=BB8_2 Depth=1
	xorl	%r9d, %r9d
	testl	%ecx, %ecx
	jne	.LBB8_10
	jmp	.LBB8_11
	.p2align	4, 0x90
.LBB8_5:                                #   in Loop: Header=BB8_2 Depth=1
	movl	$1, %ecx
	testl	%ecx, %ecx
	je	.LBB8_11
.LBB8_10:                               # %.us-lcssa.us.i
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	8(%r8), %r8
	testq	%r8, %r8
	jne	.LBB8_2
.LBB8_11:                               # %subset_pset.exit
	testl	%ecx, %ecx
	je	.LBB8_12
.LBB8_13:                               # %subset_pset.exit.thread
	testq	%rsi, %rsi
	je	.LBB8_14
	.p2align	4, 0x90
.LBB8_15:                               # %.preheader.i6
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_16 Depth 2
                                        #       Child Loop BB8_17 Depth 3
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.LBB8_24
.LBB8_16:                               # %.outer.split.us.i11.preheader
                                        #   Parent Loop BB8_15 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_17 Depth 3
	movl	%eax, %ecx
	.p2align	4, 0x90
.LBB8_17:                               # %.outer.split.us.i11
                                        #   Parent Loop BB8_15 Depth=1
                                        #     Parent Loop BB8_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ecx, %eax
	cmpl	$1, %eax
	je	.LBB8_18
# BB#19:                                #   in Loop: Header=BB8_17 Depth=3
	movl	(%rsi), %edx
	movl	$1, %ecx
	cmpl	(%rdi), %edx
	je	.LBB8_17
# BB#20:                                # %.us-lcssa12.us.i12
                                        #   in Loop: Header=BB8_16 Depth=2
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB8_16
# BB#21:                                #   in Loop: Header=BB8_15 Depth=1
	xorl	%edi, %edi
	testl	%eax, %eax
	jne	.LBB8_23
	jmp	.LBB8_24
	.p2align	4, 0x90
.LBB8_18:                               #   in Loop: Header=BB8_15 Depth=1
	movl	$1, %eax
	testl	%eax, %eax
	je	.LBB8_24
.LBB8_23:                               # %.us-lcssa.us.i15
                                        #   in Loop: Header=BB8_15 Depth=1
	movq	8(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB8_15
	jmp	.LBB8_24
.LBB8_14:
	movl	$1, %eax
.LBB8_24:                               # %subset_pset.exit17
	testl	%eax, %eax
	setne	%cl
.LBB8_25:                               # %subset_pset.exit.thread39
	movzbl	%cl, %eax
	retq
.LBB8_12:
	xorl	%ecx, %ecx
	movzbl	%cl, %eax
	retq
.Lfunc_end8:
	.size	eq_pset, .Lfunc_end8-eq_pset
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
