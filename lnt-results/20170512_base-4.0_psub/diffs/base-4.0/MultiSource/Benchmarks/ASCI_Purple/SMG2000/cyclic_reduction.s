	.text
	.file	"cyclic_reduction.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.text
	.globl	hypre_CyclicReductionCreate
	.p2align	4, 0x90
	.type	hypre_CyclicReductionCreate,@function
hypre_CyclicReductionCreate:            # @hypre_CyclicReductionCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	$1, %edi
	movl	$112, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movl	%ebp, (%rbx)
	movl	$0, 8(%rbx)
	movl	$.L.str, %edi
	callq	hypre_InitializeTiming
	movl	%eax, 104(%rbx)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,0,0,1]
	movups	%xmm0, 12(%rbx)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 28(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_CyclicReductionCreate, .Lfunc_end0-hypre_CyclicReductionCreate
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.zero	16
	.text
	.globl	hypre_CycRedCreateCoarseOp
	.p2align	4, 0x90
	.type	hypre_CycRedCreateCoarseOp,@function
hypre_CycRedCreateCoarseOp:             # @hypre_CycRedCreateCoarseOp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 80
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, %r14
	movq	%rdi, %r13
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movq	$0, 16(%rsp)
	cmpl	$0, 72(%r13)
	je	.LBB1_1
# BB#2:                                 # %.loopexit.loopexit5961
	movl	$2, %r15d
	movl	$2, %edi
	movl	$12, %esi
	callq	hypre_CAlloc
	movl	$-1, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	jmp	.LBB1_3
.LBB1_1:                                # %.loopexit.loopexit60
	movl	$3, %r15d
	movl	$3, %edi
	movl	$12, %esi
	callq	hypre_CAlloc
	movl	$-1, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	$1, 24(%rax)
	movl	$0, 32(%rax)
.LBB1_3:                                # %.loopexit
	movl	$1, %edi
	movl	%r15d, %esi
	movq	%rax, %rdx
	callq	hypre_StructStencilCreate
	movq	%rax, %rbx
	movl	(%r13), %edi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	hypre_StructMatrixCreate
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	hypre_StructStencilDestroy
	movl	72(%r13), %eax
	movl	%eax, 72(%rbp)
	addl	%r12d, %r12d
	movslq	%r12d, %rax
	movl	$1, (%rsp,%rax,4)
	cmpl	$0, 72(%r13)
	jne	.LBB1_5
# BB#4:
	orl	$1, %r12d
	movslq	%r12d, %rax
	movl	$1, (%rsp,%rax,4)
.LBB1_5:
	movq	%rsp, %rsi
	movq	%rbp, %rdi
	callq	hypre_StructMatrixSetNumGhost
	movq	%rbp, %rdi
	callq	hypre_StructMatrixInitializeShell
	movq	%rbp, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_CycRedCreateCoarseOp, .Lfunc_end1-hypre_CycRedCreateCoarseOp
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	hypre_CycRedSetupCoarseOp
	.p2align	4, 0x90
	.type	hypre_CycRedSetupCoarseOp,@function
hypre_CycRedSetupCoarseOp:              # @hypre_CycRedSetupCoarseOp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 304
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rcx, %r8
	movq	%rdx, 232(%rsp)         # 8-byte Spill
	movq	%rsi, 128(%rsp)         # 8-byte Spill
	movq	8(%rsi), %rcx
	movq	8(%rcx), %r15
	cmpl	$0, 8(%r15)
	movq	%rdi, 168(%rsp)         # 8-byte Spill
	movq	%r15, 184(%rsp)         # 8-byte Spill
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	jle	.LBB2_36
# BB#1:                                 # %.preheader1011.lr.ph
	movq	8(%rdi), %rax
	movq	16(%rax), %rsi
	movq	16(%rcx), %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
                                        # implicit-def: %RAX
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%r8, 160(%rsp)          # 8-byte Spill
	movq	%rsi, 224(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_2:                                # %.preheader1011
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
                                        #     Child Loop BB2_10 Depth 2
                                        #       Child Loop BB2_13 Depth 3
                                        #         Child Loop BB2_14 Depth 4
                                        #     Child Loop BB2_24 Depth 2
                                        #       Child Loop BB2_27 Depth 3
                                        #         Child Loop BB2_28 Depth 4
	movq	216(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rbx,4), %eax
	movslq	%ecx, %rcx
	leaq	(%rcx,%rcx,2), %rdx
	leaq	-1(%rcx), %rbp
	leaq	-2(%rdx,%rdx), %rcx
	movq	168(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_3:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %r12
	leaq	6(%r12), %rcx
	cmpl	%eax, 4(%rsi,%rbp,4)
	leaq	1(%rbp), %rbp
	jne	.LBB2_3
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	(%r15), %rcx
	leaq	(%rbx,%rbx,2), %rax
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rax,8), %rdi
	movq	%rbx, 176(%rsp)         # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	%r8, %rdx
	leaq	196(%rsp), %rcx
	callq	hypre_StructMapCoarseToFine
	movq	40(%r14), %rax
	movq	(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	128(%rsp), %rbx         # 8-byte Reload
	movq	40(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	$0, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r14, %rdi
	movl	%ebp, %esi
	leaq	12(%rsp), %r15
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, %r13
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r14, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, %r15
	movq	$1, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%r14, %rdi
	movq	%rbp, 240(%rsp)         # 8-byte Spill
	movl	%ebp, %esi
	leaq	12(%rsp), %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, %rbp
	movq	$0, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%rbx, %rdi
	movq	176(%rsp), %r14         # 8-byte Reload
	movl	%r14d, %esi
	leaq	12(%rsp), %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%rbx, %rdi
	movq	%r14, %rbx
	movq	168(%rsp), %r14         # 8-byte Reload
	movl	%ebx, %esi
	leaq	12(%rsp), %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	72(%r14), %eax
	testl	%eax, %eax
	jne	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	$1, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	128(%rsp), %rdi         # 8-byte Reload
	movl	%ebx, %esi
	leaq	12(%rsp), %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	72(%r14), %eax
.LBB2_6:                                #   in Loop: Header=BB2_2 Depth=1
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movq	$1, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	148(%rsp), %rsi
	callq	hypre_BoxGetSize
	leaq	(,%rbx,8), %rax
	leaq	(%rax,%rax,2), %r8
	movq	64(%rsp), %r10          # 8-byte Reload
	movl	(%r10,%r8), %edx
	movl	%edx, 72(%rsp)          # 4-byte Spill
	movl	4(%r10,%r8), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	12(%r10,%r8), %ecx
	movl	%ecx, %esi
	subl	%edx, %esi
	incl	%esi
	cmpl	%edx, %ecx
	movl	16(%r10,%r8), %ecx
	movl	$0, %edi
	cmovsl	%edi, %esi
	movl	%ecx, %r9d
	subl	%eax, %r9d
	incl	%r9d
	cmpl	%eax, %ecx
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	8(%rax,%r12,4), %r11d
	movl	20(%rax,%r12,4), %ecx
	cmovsl	%edi, %r9d
	movl	%ecx, %ebx
	subl	%r11d, %ebx
	incl	%ebx
	cmpl	%r11d, %ecx
	movl	12(%rax,%r12,4), %edx
	movl	24(%rax,%r12,4), %r14d
	cmovsl	%edi, %ebx
	movl	%r14d, %ecx
	subl	%edx, %ecx
	incl	%ecx
	cmpl	%edx, %r14d
	cmovsl	%edi, %ecx
	movl	204(%rsp), %r14d
	subl	16(%rax,%r12,4), %r14d
	movl	196(%rsp), %edi
	subl	%r11d, %edi
	movl	%edi, 32(%rsp)          # 4-byte Spill
	movl	200(%rsp), %edi
	subl	%edx, %edi
	imull	%ecx, %r14d
	addl	%edi, %r14d
	imull	%ebx, %r14d
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r11d
	subl	72(%rsp), %r11d         # 4-byte Folded Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	4(%rdi,%r8), %edx
	movl	8(%rdi,%r8), %edi
	subl	4(%rsp), %edx           # 4-byte Folded Reload
	subl	8(%r10,%r8), %edi
	imull	%r9d, %edi
	addl	%edx, %edi
	imull	%esi, %edi
	movq	160(%rsp), %r8          # 8-byte Reload
	movl	4(%r8), %eax
	imull	%ebx, %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	imull	%ebx, %ecx
	imull	8(%r8), %ecx
	movl	%esi, 64(%rsp)          # 4-byte Spill
	imull	%esi, %r9d
	movl	148(%rsp), %r12d
	movl	152(%rsp), %edx
	movl	156(%rsp), %esi
	cmpl	%r12d, %edx
	movl	%r12d, %eax
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	cmovgel	%edx, %eax
	cmpl	%eax, %esi
	cmovgel	%esi, %eax
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	movslq	(%r8), %r10
	movl	%esi, 4(%rsp)           # 4-byte Spill
	je	.LBB2_21
# BB#7:                                 #   in Loop: Header=BB2_2 Depth=1
	testl	%eax, %eax
	movapd	.LCPI2_0(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00]
	jle	.LBB2_35
# BB#8:                                 # %.lr.ph1091
                                        #   in Loop: Header=BB2_2 Depth=1
	testl	%esi, %esi
	jle	.LBB2_35
# BB#9:                                 # %.preheader1008.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	%r12d, %eax
	imull	%r10d, %eax
	movl	48(%rsp), %r8d          # 4-byte Reload
	movl	%r11d, 40(%rsp)         # 4-byte Spill
	movl	%r8d, %r11d
	subl	%eax, %r11d
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rbx
	leal	-1(%rbx), %edx
	imull	%edx, %r11d
	addl	%r8d, %r11d
	subl	%eax, %r11d
	movl	%r11d, 88(%rsp)         # 4-byte Spill
	movl	64(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r11d
	subl	%r12d, %r11d
	imull	%edx, %r11d
	movl	%ebx, %edx
	imull	%r8d, %edx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	subl	%edx, %ecx
	addl	40(%rsp), %edi          # 4-byte Folded Reload
	movl	%ebx, %edx
	imull	%eax, %edx
	movl	%edx, 96(%rsp)          # 4-byte Spill
	subl	%edx, %r9d
	addl	%eax, %r11d
	subl	%r12d, %r11d
	movl	%r11d, 60(%rsp)         # 4-byte Spill
	addl	32(%rsp), %r14d         # 4-byte Folded Reload
	shlq	$3, %r10
	addq	$-8, %r13
	xorl	%eax, %eax
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	movl	%ecx, 112(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB2_10:                               # %.preheader1008
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_13 Depth 3
                                        #         Child Loop BB2_14 Depth 4
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_19
# BB#11:                                # %.preheader1006.lr.ph
                                        #   in Loop: Header=BB2_10 Depth=2
	testl	%r12d, %r12d
	jle	.LBB2_18
# BB#12:                                # %.preheader1006.us.preheader
                                        #   in Loop: Header=BB2_10 Depth=2
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	xorl	%r8d, %r8d
	movl	%edi, 32(%rsp)          # 4-byte Spill
	movl	%edi, %eax
	.p2align	4, 0x90
.LBB2_13:                               # %.preheader1006.us
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_10 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_14 Depth 4
	movslq	%eax, %rcx
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdi
	movq	136(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %r9
	movslq	%r14d, %rcx
	leaq	(%r15,%rcx,8), %rdx
	leaq	(%r13,%rcx,8), %rsi
	leaq	(%rbp,%rcx,8), %rcx
	movl	%r12d, %ebx
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB2_14:                               #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_10 Depth=2
                                        #       Parent Loop BB2_13 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%rdx,%r11), %xmm0      # xmm0 = mem[0],zero
	mulsd	-8(%rdx,%r11), %xmm0
	xorpd	%xmm2, %xmm0
	divsd	(%rsi,%r11), %xmm0
	movsd	%xmm0, (%rdi)
	movsd	8(%rsi,%r11), %xmm0     # xmm0 = mem[0],zero
	movsd	(%rdx,%r11), %xmm1      # xmm1 = mem[0],zero
	mulsd	-8(%rcx,%r11), %xmm1
	divsd	(%rsi,%r11), %xmm1
	subsd	%xmm1, %xmm0
	movsd	(%rcx,%r11), %xmm1      # xmm1 = mem[0],zero
	mulsd	8(%rdx,%r11), %xmm1
	divsd	16(%rsi,%r11), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%r9)
	addq	$8, %rdi
	addq	$8, %r9
	addq	%r10, %r11
	decl	%ebx
	jne	.LBB2_14
# BB#15:                                # %._crit_edge1076.us
                                        #   in Loop: Header=BB2_13 Depth=3
	incl	%r8d
	addl	64(%rsp), %eax          # 4-byte Folded Reload
	addl	48(%rsp), %r14d         # 4-byte Folded Reload
	cmpl	24(%rsp), %r8d          # 4-byte Folded Reload
	jne	.LBB2_13
# BB#16:                                # %._crit_edge1082.loopexit
                                        #   in Loop: Header=BB2_10 Depth=2
	movl	32(%rsp), %edi          # 4-byte Reload
	addl	96(%rsp), %edi          # 4-byte Folded Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
	movl	112(%rsp), %ecx         # 4-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	72(%rsp), %edx          # 4-byte Reload
	jmp	.LBB2_20
	.p2align	4, 0x90
.LBB2_18:                               # %.preheader1006.preheader
                                        #   in Loop: Header=BB2_10 Depth=2
	addl	88(%rsp), %r14d         # 4-byte Folded Reload
	addl	60(%rsp), %edi          # 4-byte Folded Reload
.LBB2_19:                               # %._crit_edge1082
                                        #   in Loop: Header=BB2_10 Depth=2
	movl	%r14d, %edx
.LBB2_20:                               # %._crit_edge1082
                                        #   in Loop: Header=BB2_10 Depth=2
	addl	%ecx, %edx
	addl	%r9d, %edi
	incl	%eax
	cmpl	%esi, %eax
	movl	%edx, %r14d
	jne	.LBB2_10
	jmp	.LBB2_35
	.p2align	4, 0x90
.LBB2_21:                               #   in Loop: Header=BB2_2 Depth=1
	testl	%eax, %eax
	movq	120(%rsp), %rdx         # 8-byte Reload
	movapd	.LCPI2_0(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00]
	jle	.LBB2_35
# BB#22:                                # %.lr.ph1111
                                        #   in Loop: Header=BB2_2 Depth=1
	testl	%esi, %esi
	jle	.LBB2_35
# BB#23:                                # %.preheader1007.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	%r10d, %eax
	imull	%r12d, %eax
	movl	48(%rsp), %ebx          # 4-byte Reload
	subl	%eax, %ebx
	movq	24(%rsp), %r8           # 8-byte Reload
	leal	-1(%r8), %esi
	imull	%esi, %ebx
	addl	48(%rsp), %ebx          # 4-byte Folded Reload
	subl	%eax, %ebx
	movl	%ebx, 88(%rsp)          # 4-byte Spill
	movl	64(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebx
	subl	%r12d, %ebx
	imull	%esi, %ebx
	movl	%r11d, 40(%rsp)         # 4-byte Spill
	movl	%r8d, %r11d
	imull	48(%rsp), %r11d         # 4-byte Folded Reload
	movl	4(%rsp), %esi           # 4-byte Reload
	movq	%r11, 104(%rsp)         # 8-byte Spill
	subl	%r11d, %ecx
	addl	40(%rsp), %edi          # 4-byte Folded Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	imull	%eax, %r8d
	movl	%r8d, 96(%rsp)          # 4-byte Spill
	subl	%r8d, %r9d
	addl	%eax, %ebx
	subl	%r12d, %ebx
	movl	%ebx, 60(%rsp)          # 4-byte Spill
	addl	32(%rsp), %r14d         # 4-byte Folded Reload
	shlq	$3, %r10
	xorl	%eax, %eax
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	movl	%ecx, 112(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB2_24:                               # %.preheader1007
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_27 Depth 3
                                        #         Child Loop BB2_28 Depth 4
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_33
# BB#25:                                # %.preheader1005.lr.ph
                                        #   in Loop: Header=BB2_24 Depth=2
	testl	%r12d, %r12d
	jle	.LBB2_32
# BB#26:                                # %.preheader1005.us.preheader
                                        #   in Loop: Header=BB2_24 Depth=2
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	xorl	%r9d, %r9d
	movl	%edi, 32(%rsp)          # 4-byte Spill
	movl	%edi, %r11d
	.p2align	4, 0x90
.LBB2_27:                               # %.preheader1005.us
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_24 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_28 Depth 4
	movslq	%r11d, %rcx
	movslq	%r14d, %rax
	shlq	$3, %rax
	movq	80(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rbx
	movq	136(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdi
	leaq	(%rdx,%rcx,8), %r8
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_28:                               #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_24 Depth=2
                                        #       Parent Loop BB2_27 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%r15,%rax), %xmm0      # xmm0 = mem[0],zero
	mulsd	-8(%r15,%rax), %xmm0
	xorpd	%xmm2, %xmm0
	divsd	-8(%r13,%rax), %xmm0
	movsd	%xmm0, (%rbx,%rcx,8)
	movsd	(%r13,%rax), %xmm0      # xmm0 = mem[0],zero
	movsd	(%r15,%rax), %xmm1      # xmm1 = mem[0],zero
	mulsd	-8(%rbp,%rax), %xmm1
	divsd	-8(%r13,%rax), %xmm1
	subsd	%xmm1, %xmm0
	movsd	(%rbp,%rax), %xmm1      # xmm1 = mem[0],zero
	mulsd	8(%r15,%rax), %xmm1
	divsd	8(%r13,%rax), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rdi,%rcx,8)
	movsd	(%rbp,%rax), %xmm0      # xmm0 = mem[0],zero
	mulsd	8(%rbp,%rax), %xmm0
	xorpd	%xmm2, %xmm0
	divsd	8(%r13,%rax), %xmm0
	movsd	%xmm0, (%r8,%rcx,8)
	incq	%rcx
	addq	%r10, %rax
	cmpl	%ecx, %r12d
	jne	.LBB2_28
# BB#29:                                # %._crit_edge1096.us
                                        #   in Loop: Header=BB2_27 Depth=3
	incl	%r9d
	addl	64(%rsp), %r11d         # 4-byte Folded Reload
	addl	48(%rsp), %r14d         # 4-byte Folded Reload
	cmpl	24(%rsp), %r9d          # 4-byte Folded Reload
	jne	.LBB2_27
# BB#30:                                # %._crit_edge1102.loopexit
                                        #   in Loop: Header=BB2_24 Depth=2
	movl	32(%rsp), %edi          # 4-byte Reload
	addl	96(%rsp), %edi          # 4-byte Folded Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
	movl	112(%rsp), %ecx         # 4-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	72(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB2_34
	.p2align	4, 0x90
.LBB2_32:                               # %.preheader1005.preheader
                                        #   in Loop: Header=BB2_24 Depth=2
	addl	88(%rsp), %r14d         # 4-byte Folded Reload
	addl	60(%rsp), %edi          # 4-byte Folded Reload
.LBB2_33:                               # %._crit_edge1102
                                        #   in Loop: Header=BB2_24 Depth=2
	movl	%r14d, %ebx
.LBB2_34:                               # %._crit_edge1102
                                        #   in Loop: Header=BB2_24 Depth=2
	addl	%ecx, %ebx
	addl	%r9d, %edi
	incl	%eax
	cmpl	%esi, %eax
	movl	%ebx, %r14d
	jne	.LBB2_24
	.p2align	4, 0x90
.LBB2_35:                               # %.loopexit1009
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	176(%rsp), %rbx         # 8-byte Reload
	incq	%rbx
	movq	184(%rsp), %r15         # 8-byte Reload
	movslq	8(%r15), %rax
	cmpq	%rax, %rbx
	movq	160(%rsp), %r8          # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	240(%rsp), %rcx         # 8-byte Reload
	jl	.LBB2_2
	jmp	.LBB2_37
.LBB2_36:
                                        # implicit-def: %RAX
	movq	%rax, 120(%rsp)         # 8-byte Spill
.LBB2_37:                               # %._crit_edge1119
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	hypre_StructMatrixAssemble
	movq	208(%rsp), %rax         # 8-byte Reload
	cmpl	$1, 56(%rax)
	jne	.LBB2_94
# BB#38:                                # %.preheader1003
	cmpl	$0, 8(%r15)
	jle	.LBB2_94
# BB#39:                                # %.lr.ph1071
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_40:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_47 Depth 2
                                        #       Child Loop BB2_56 Depth 3
                                        #         Child Loop BB2_53 Depth 4
                                        #         Child Loop BB2_64 Depth 4
                                        #     Child Loop BB2_72 Depth 2
                                        #       Child Loop BB2_81 Depth 3
                                        #         Child Loop BB2_78 Depth 4
                                        #         Child Loop BB2_89 Depth 4
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	%r15, %rbx
	movq	(%rbx), %r13
	leaq	(%rcx,%rcx,2), %rax
	leaq	(%r13,%rax,8), %r14
	movq	128(%rsp), %rbp         # 8-byte Reload
	movq	40(%rbp), %rax
	movq	(%rax), %rbx
	movq	$0, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%rbp, %rdi
	movq	88(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	leaq	12(%rsp), %r15
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	%rbp, %rdi
	movq	88(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, %r12
	movq	168(%rsp), %rax         # 8-byte Reload
	movl	72(%rax), %r15d
	testl	%r15d, %r15d
	jne	.LBB2_42
# BB#41:                                #   in Loop: Header=BB2_40 Depth=1
	movq	$1, 12(%rsp)
	movl	$0, 20(%rsp)
	movq	128(%rsp), %rdi         # 8-byte Reload
	movl	%ebp, %esi
	leaq	12(%rsp), %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rax         # 8-byte Reload
	movl	72(%rax), %r15d
.LBB2_42:                               #   in Loop: Header=BB2_40 Depth=1
	movq	%r14, %rdi
	leaq	148(%rsp), %rsi
	callq	hypre_BoxGetSize
	leaq	(,%rbp,8), %rax
	leaq	(%rax,%rax,2), %rax
	movl	(%rbx,%rax), %edx
	movl	4(%rbx,%rax), %esi
	movl	12(%rbx,%rax), %ecx
	movl	%ecx, %r8d
	subl	%edx, %r8d
	incl	%r8d
	cmpl	%edx, %ecx
	movl	16(%rbx,%rax), %ecx
	movl	$0, %edi
	cmovsl	%edi, %r8d
	movl	%ecx, %ebp
	subl	%esi, %ebp
	incl	%ebp
	cmpl	%esi, %ecx
	movl	(%r14), %ecx
	cmovsl	%edi, %ebp
	subl	%edx, %ecx
	movl	4(%r13,%rax), %edx
	movl	8(%r13,%rax), %edi
	subl	%esi, %edx
	subl	8(%rbx,%rax), %edi
	imull	%ebp, %edi
	addl	%edx, %edi
	imull	%r8d, %edi
	movl	%edi, %esi
	imull	%r8d, %ebp
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	movl	148(%rsp), %edx
	movl	152(%rsp), %r13d
	movl	156(%rsp), %edi
	cmpl	%edx, %r13d
	movl	%edx, %eax
	cmovgel	%r13d, %eax
	cmpl	%eax, %edi
	movl	%edi, 4(%rsp)           # 4-byte Spill
	cmovgel	%edi, %eax
	testl	%r15d, %r15d
	je	.LBB2_68
# BB#43:                                #   in Loop: Header=BB2_40 Depth=1
	testl	%eax, %eax
	xorpd	%xmm2, %xmm2
	jle	.LBB2_93
# BB#44:                                # %.lr.ph1037
                                        #   in Loop: Header=BB2_40 Depth=1
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB2_93
# BB#45:                                # %.lr.ph1037
                                        #   in Loop: Header=BB2_40 Depth=1
	testl	%r13d, %r13d
	jle	.LBB2_93
# BB#46:                                # %.preheader1001.us.preheader
                                        #   in Loop: Header=BB2_40 Depth=1
	movl	%r8d, %eax
	subl	%edx, %eax
	movl	%r8d, %edi
	imull	%r13d, %edi
	movl	%edi, 96(%rsp)          # 4-byte Spill
	subl	%edi, 8(%rsp)           # 4-byte Folded Spill
	leal	-1(%r13), %edi
	imull	%eax, %edi
	addl	%r8d, %edi
	subl	%edx, %edi
	movl	%edi, 104(%rsp)         # 4-byte Spill
	addl	%ecx, %esi
	leal	-1(%rdx), %r11d
	movq	24(%rsp), %r14          # 8-byte Reload
	leaq	8(%r14,%r11,8), %r9
	leaq	8(%r12,%r11,8), %r10
	leaq	1(%r11), %rcx
	movq	%rcx, %rbp
	movabsq	$8589934590, %rax       # imm = 0x1FFFFFFFE
	andq	%rax, %rbp
	leaq	-2(%rbp), %rdi
	shrq	%rdi
	cmpq	%r10, %r14
	sbbb	%r10b, %r10b
	cmpq	%r9, %r12
	sbbb	%al, %al
	andb	%r10b, %al
	andb	$1, %al
	movb	%al, 48(%rsp)           # 1-byte Spill
	movq	%rdi, 64(%rsp)          # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	andl	$1, %edi
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	leaq	8(%r14), %rbx
	addq	$16, %r14
	movq	%r14, 40(%rsp)          # 8-byte Spill
	leaq	8(%r12), %r9
	movq	%r12, %rax
	addq	$16, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%edi, %edi
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_47:                               # %.preheader1001.us
                                        #   Parent Loop BB2_40 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_56 Depth 3
                                        #         Child Loop BB2_53 Depth 4
                                        #         Child Loop BB2_64 Depth 4
	testl	%edx, %edx
	movl	104(%rsp), %eax         # 4-byte Reload
	jle	.LBB2_67
# BB#48:                                # %.preheader999.us.us.preheader
                                        #   in Loop: Header=BB2_47 Depth=2
	movl	%edi, 112(%rsp)         # 4-byte Spill
	xorl	%r14d, %r14d
	movl	%esi, 32(%rsp)          # 4-byte Spill
	movl	%esi, %eax
	jmp	.LBB2_56
.LBB2_49:                               # %vector.body1220.preheader
                                        #   in Loop: Header=BB2_56 Depth=3
	cmpq	$0, 136(%rsp)           # 8-byte Folded Reload
	jne	.LBB2_51
# BB#50:                                # %vector.body1220.prol
                                        #   in Loop: Header=BB2_56 Depth=3
	movupd	(%r12,%r15,8), %xmm0
	addpd	%xmm0, %xmm0
	movq	24(%rsp), %rax          # 8-byte Reload
	movupd	(%rax,%r15,8), %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm1, (%rax,%r15,8)
	movupd	%xmm2, (%r12,%r15,8)
	movl	$2, %edi
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB2_52
	jmp	.LBB2_54
.LBB2_51:                               #   in Loop: Header=BB2_56 Depth=3
	xorl	%edi, %edi
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB2_54
.LBB2_52:                               # %vector.body1220.preheader.new
                                        #   in Loop: Header=BB2_56 Depth=3
	movq	%rbp, %rsi
	subq	%rdi, %rsi
	addq	%r15, %rdi
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdi,8), %rax
	movq	72(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB2_53:                               # %vector.body1220
                                        #   Parent Loop BB2_40 Depth=1
                                        #     Parent Loop BB2_47 Depth=2
                                        #       Parent Loop BB2_56 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movupd	-16(%rdi), %xmm0
	addpd	%xmm0, %xmm0
	movupd	-16(%rax), %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm1, -16(%rax)
	movupd	%xmm2, -16(%rdi)
	movupd	(%rdi), %xmm0
	addpd	%xmm0, %xmm0
	movupd	(%rax), %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm1, (%rax)
	movupd	%xmm2, (%rdi)
	addq	$32, %rax
	addq	$32, %rdi
	addq	$-4, %rsi
	jne	.LBB2_53
.LBB2_54:                               # %middle.block1221
                                        #   in Loop: Header=BB2_56 Depth=3
	cmpq	%rbp, %rcx
	movq	80(%rsp), %rbx          # 8-byte Reload
	je	.LBB2_65
# BB#55:                                #   in Loop: Header=BB2_56 Depth=3
	movq	%r15, %rsi
	addq	%rbp, %rsi
	movl	%ebp, %eax
	jmp	.LBB2_60
	.p2align	4, 0x90
.LBB2_56:                               # %.preheader999.us.us
                                        #   Parent Loop BB2_40 Depth=1
                                        #     Parent Loop BB2_47 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_53 Depth 4
                                        #         Child Loop BB2_64 Depth 4
	movslq	%eax, %r15
	cmpq	$1, %rcx
	jbe	.LBB2_59
# BB#57:                                # %min.iters.checked1224
                                        #   in Loop: Header=BB2_56 Depth=3
	testq	%rbp, %rbp
	je	.LBB2_59
# BB#58:                                # %vector.memcheck1245
                                        #   in Loop: Header=BB2_56 Depth=3
	cmpb	$0, 48(%rsp)            # 1-byte Folded Reload
	je	.LBB2_49
	.p2align	4, 0x90
.LBB2_59:                               #   in Loop: Header=BB2_56 Depth=3
	movq	%r15, %rsi
	xorl	%eax, %eax
.LBB2_60:                               # %scalar.ph1222.preheader
                                        #   in Loop: Header=BB2_56 Depth=3
	movl	%edx, %edi
	subl	%eax, %edi
	testb	$1, %dil
	jne	.LBB2_62
# BB#61:                                #   in Loop: Header=BB2_56 Depth=3
	movl	%eax, %edi
	cmpl	%eax, %r11d
	jne	.LBB2_63
	jmp	.LBB2_65
	.p2align	4, 0x90
.LBB2_62:                               # %scalar.ph1222.prol
                                        #   in Loop: Header=BB2_56 Depth=3
	movsd	(%r12,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	movq	24(%rsp), %rdi          # 8-byte Reload
	addsd	(%rdi,%rsi,8), %xmm0
	movsd	%xmm0, (%rdi,%rsi,8)
	movq	$0, (%r12,%rsi,8)
	incq	%rsi
	leal	1(%rax), %edi
	cmpl	%eax, %r11d
	je	.LBB2_65
.LBB2_63:                               # %scalar.ph1222.preheader.new
                                        #   in Loop: Header=BB2_56 Depth=3
	leaq	(%r9,%rsi,8), %r10
	leaq	(%rbx,%rsi,8), %rax
	movl	%edx, %esi
	subl	%edi, %esi
	.p2align	4, 0x90
.LBB2_64:                               # %scalar.ph1222
                                        #   Parent Loop BB2_40 Depth=1
                                        #     Parent Loop BB2_47 Depth=2
                                        #       Parent Loop BB2_56 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	-8(%r10), %xmm0         # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	addsd	-8(%rax), %xmm0
	movsd	%xmm0, -8(%rax)
	movq	$0, -8(%r10)
	movsd	(%r10), %xmm0           # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	addsd	(%rax), %xmm0
	movsd	%xmm0, (%rax)
	movq	$0, (%r10)
	addq	$16, %r10
	addq	$16, %rax
	addl	$-2, %esi
	jne	.LBB2_64
.LBB2_65:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB2_56 Depth=3
	leal	(%r15,%r8), %eax
	incl	%r14d
	cmpl	%r13d, %r14d
	jne	.LBB2_56
# BB#66:                                #   in Loop: Header=BB2_47 Depth=2
	movl	96(%rsp), %eax          # 4-byte Reload
	movl	32(%rsp), %esi          # 4-byte Reload
	movl	112(%rsp), %edi         # 4-byte Reload
.LBB2_67:                               # %._crit_edge1018.us
                                        #   in Loop: Header=BB2_47 Depth=2
	addl	%esi, %eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	incl	%edi
	cmpl	4(%rsp), %edi           # 4-byte Folded Reload
	movl	%eax, %esi
	jne	.LBB2_47
	jmp	.LBB2_93
	.p2align	4, 0x90
.LBB2_68:                               #   in Loop: Header=BB2_40 Depth=1
	testl	%eax, %eax
	xorpd	%xmm2, %xmm2
	jle	.LBB2_93
# BB#69:                                # %.lr.ph1064
                                        #   in Loop: Header=BB2_40 Depth=1
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB2_93
# BB#70:                                # %.lr.ph1064
                                        #   in Loop: Header=BB2_40 Depth=1
	testl	%r13d, %r13d
	jle	.LBB2_93
# BB#71:                                # %.preheader1000.us.preheader
                                        #   in Loop: Header=BB2_40 Depth=1
	movl	%r8d, %eax
	subl	%edx, %eax
	movl	%r8d, %edi
	imull	%r13d, %edi
	movl	%edi, 160(%rsp)         # 4-byte Spill
	subl	%edi, 8(%rsp)           # 4-byte Folded Spill
	leal	-1(%r13), %edi
	imull	%eax, %edi
	addl	%r8d, %edi
	subl	%edx, %edi
	movl	%edi, 60(%rsp)          # 4-byte Spill
	addl	%ecx, %esi
	movl	%esi, 32(%rsp)          # 4-byte Spill
	leal	-1(%rdx), %r14d
	movq	24(%rsp), %r10          # 8-byte Reload
	leaq	8(%r10), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	8(%r10,%r14,8), %rbp
	leaq	8(%r12), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	8(%r12,%r14,8), %rbx
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movq	120(%rsp), %rsi         # 8-byte Reload
	leaq	8(%rsi), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	8(%rsi,%r14,8), %r15
	leaq	1(%r14), %rax
	movq	%rax, %r9
	movabsq	$8589934590, %rcx       # imm = 0x1FFFFFFFE
	andq	%rcx, %r9
	leaq	-2(%r9), %rdi
	shrq	%rdi
	cmpq	%rbx, %r10
	sbbb	%bl, %bl
	cmpq	%rbp, %r12
	sbbb	%cl, %cl
	andb	%bl, %cl
	cmpq	%r15, %r10
	sbbb	%r11b, %r11b
	cmpq	%rbp, %rsi
	sbbb	%bl, %bl
	andb	%r11b, %bl
	orb	%cl, %bl
	cmpq	%r15, %r12
	movl	32(%rsp), %ebp          # 4-byte Reload
	sbbb	%r11b, %r11b
	cmpq	80(%rsp), %rsi          # 8-byte Folded Reload
	sbbb	%cl, %cl
	andb	%r11b, %cl
	orb	%bl, %cl
	andb	$1, %cl
	movb	%cl, 80(%rsp)           # 1-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	andl	$1, %edi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	addq	$16, %r10
	movq	%r10, 112(%rsp)         # 8-byte Spill
	leaq	16(%rsi), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%r12, %rcx
	addq	$16, %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_72:                               # %.preheader1000.us
                                        #   Parent Loop BB2_40 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_81 Depth 3
                                        #         Child Loop BB2_78 Depth 4
                                        #         Child Loop BB2_89 Depth 4
	testl	%edx, %edx
	movl	60(%rsp), %ecx          # 4-byte Reload
	jle	.LBB2_92
# BB#73:                                # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB2_72 Depth=2
	movl	%esi, 176(%rsp)         # 4-byte Spill
	movl	%ebp, %r11d
	xorl	%r15d, %r15d
	movl	%r11d, 32(%rsp)         # 4-byte Spill
	jmp	.LBB2_81
.LBB2_74:                               # %vector.body.preheader
                                        #   in Loop: Header=BB2_81 Depth=3
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	jne	.LBB2_76
# BB#75:                                # %vector.body.prol
                                        #   in Loop: Header=BB2_81 Depth=3
	movupd	(%r12,%r10,8), %xmm0
	movq	120(%rsp), %rcx         # 8-byte Reload
	movupd	(%rcx,%r10,8), %xmm1
	addpd	%xmm0, %xmm1
	movq	24(%rsp), %rsi          # 8-byte Reload
	movupd	(%rsi,%r10,8), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rsi,%r10,8)
	movupd	%xmm2, (%r12,%r10,8)
	movupd	%xmm2, (%rcx,%r10,8)
	movl	$2, %ecx
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	jne	.LBB2_77
	jmp	.LBB2_79
.LBB2_76:                               #   in Loop: Header=BB2_81 Depth=3
	xorl	%ecx, %ecx
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB2_79
.LBB2_77:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB2_81 Depth=3
	movq	112(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r10,8), %rsi
	movq	104(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%r10,8), %rdi
	movq	96(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%r10,8), %rbx
	.p2align	4, 0x90
.LBB2_78:                               # %vector.body
                                        #   Parent Loop BB2_40 Depth=1
                                        #     Parent Loop BB2_72 Depth=2
                                        #       Parent Loop BB2_81 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movupd	-16(%rbx,%rcx,8), %xmm0
	movupd	-16(%rdi,%rcx,8), %xmm1
	addpd	%xmm0, %xmm1
	movupd	-16(%rsi,%rcx,8), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, -16(%rsi,%rcx,8)
	movupd	%xmm2, -16(%rbx,%rcx,8)
	movupd	%xmm2, -16(%rdi,%rcx,8)
	movupd	(%rbx,%rcx,8), %xmm0
	movupd	(%rdi,%rcx,8), %xmm1
	addpd	%xmm0, %xmm1
	movupd	(%rsi,%rcx,8), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rsi,%rcx,8)
	movupd	%xmm2, (%rbx,%rcx,8)
	movupd	%xmm2, (%rdi,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %r9
	jne	.LBB2_78
.LBB2_79:                               # %middle.block
                                        #   in Loop: Header=BB2_81 Depth=3
	cmpq	%r9, %rax
	je	.LBB2_90
# BB#80:                                #   in Loop: Header=BB2_81 Depth=3
	addq	%r9, %r10
	movl	%r9d, %ecx
	jmp	.LBB2_85
	.p2align	4, 0x90
.LBB2_81:                               # %.preheader.us.us
                                        #   Parent Loop BB2_40 Depth=1
                                        #     Parent Loop BB2_72 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_78 Depth 4
                                        #         Child Loop BB2_89 Depth 4
	movslq	%r11d, %r10
	cmpq	$1, %rax
	jbe	.LBB2_84
# BB#82:                                # %min.iters.checked
                                        #   in Loop: Header=BB2_81 Depth=3
	testq	%r9, %r9
	je	.LBB2_84
# BB#83:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_81 Depth=3
	cmpb	$0, 80(%rsp)            # 1-byte Folded Reload
	je	.LBB2_74
	.p2align	4, 0x90
.LBB2_84:                               #   in Loop: Header=BB2_81 Depth=3
	xorl	%ecx, %ecx
.LBB2_85:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_81 Depth=3
	movl	%edx, %esi
	subl	%ecx, %esi
	testb	$1, %sil
	jne	.LBB2_87
# BB#86:                                #   in Loop: Header=BB2_81 Depth=3
	movl	%ecx, %edi
	cmpl	%ecx, %r14d
	jne	.LBB2_88
	jmp	.LBB2_90
	.p2align	4, 0x90
.LBB2_87:                               # %scalar.ph.prol
                                        #   in Loop: Header=BB2_81 Depth=3
	movsd	(%r12,%r10,8), %xmm0    # xmm0 = mem[0],zero
	movq	120(%rsp), %rsi         # 8-byte Reload
	addsd	(%rsi,%r10,8), %xmm0
	movq	24(%rsp), %rdi          # 8-byte Reload
	addsd	(%rdi,%r10,8), %xmm0
	movsd	%xmm0, (%rdi,%r10,8)
	movq	$0, (%r12,%r10,8)
	movq	$0, (%rsi,%r10,8)
	incq	%r10
	leal	1(%rcx), %edi
	cmpl	%ecx, %r14d
	je	.LBB2_90
.LBB2_88:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB2_81 Depth=3
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r10,8), %rsi
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r10,8), %rbx
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r10,8), %rcx
	movl	%edx, %ebp
	subl	%edi, %ebp
	.p2align	4, 0x90
.LBB2_89:                               # %scalar.ph
                                        #   Parent Loop BB2_40 Depth=1
                                        #     Parent Loop BB2_72 Depth=2
                                        #       Parent Loop BB2_81 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	-8(%rsi), %xmm0         # xmm0 = mem[0],zero
	addsd	-8(%rbx), %xmm0
	addsd	-8(%rcx), %xmm0
	movsd	%xmm0, -8(%rcx)
	movq	$0, -8(%rsi)
	movq	$0, -8(%rbx)
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rbx), %xmm0
	addsd	(%rcx), %xmm0
	movsd	%xmm0, (%rcx)
	movq	$0, (%rsi)
	movq	$0, (%rbx)
	addq	$16, %rsi
	addq	$16, %rbx
	addq	$16, %rcx
	addl	$-2, %ebp
	jne	.LBB2_89
.LBB2_90:                               # %._crit_edge1041.us.us
                                        #   in Loop: Header=BB2_81 Depth=3
	addl	%r8d, %r11d
	incl	%r15d
	cmpl	%r13d, %r15d
	jne	.LBB2_81
# BB#91:                                #   in Loop: Header=BB2_72 Depth=2
	movl	160(%rsp), %ecx         # 4-byte Reload
	movl	32(%rsp), %ebp          # 4-byte Reload
	movl	176(%rsp), %esi         # 4-byte Reload
.LBB2_92:                               # %._crit_edge1045.us
                                        #   in Loop: Header=BB2_72 Depth=2
	addl	%ebp, %ecx
	addl	8(%rsp), %ecx           # 4-byte Folded Reload
	incl	%esi
	cmpl	4(%rsp), %esi           # 4-byte Folded Reload
	movl	%ecx, %ebp
	jne	.LBB2_72
	.p2align	4, 0x90
.LBB2_93:                               # %.loopexit
                                        #   in Loop: Header=BB2_40 Depth=1
	movq	88(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movq	184(%rsp), %r15         # 8-byte Reload
	movslq	8(%r15), %rax
	cmpq	%rax, %rcx
	jl	.LBB2_40
.LBB2_94:                               # %.loopexit1004
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	hypre_StructMatrixAssemble
	xorl	%eax, %eax
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	hypre_CycRedSetupCoarseOp, .Lfunc_end2-hypre_CycRedSetupCoarseOp
	.cfi_endproc

	.globl	hypre_CyclicReductionSetup
	.p2align	4, 0x90
	.type	hypre_CyclicReductionSetup,@function
hypre_CyclicReductionSetup:             # @hypre_CyclicReductionSetup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 272
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	movl	(%rbp), %r15d
	movslq	8(%rbp), %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 192(%rsp)
	movq	$0, 208(%rsp)
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	movq	8(%rsi), %r14
	movq	40(%r14), %rdi
	callq	hypre_BoxDuplicate
	movq	%rax, %rsi
	movl	12(%rsi,%rbx,4), %eax
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movl	(%rsi,%rbx,4), %ecx
	movl	%eax, %edi
	subl	%ecx, %edi
	incl	%edi
	xorl	%edx, %edx
	cmpl	%ecx, %eax
	movl	$0, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	cmovsl	%edx, %edi
	leaq	12(%rbp), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	leaq	24(%rbp), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	addq	$12, %rsi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	callq	hypre_Log2
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	16(,%rax,8), %edi
	callq	hypre_MAlloc
	movq	%r14, %rdi
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rax, %rsi
	callq	hypre_StructGridRef
	movl	$3, %edx
	xorl	%r12d, %r12d
	jmp	.LBB3_1
	.p2align	4, 0x90
.LBB3_5:                                #   in Loop: Header=BB3_1 Depth=1
	movq	%r13, %rdi
	leaq	4(%rsp), %rbx
	movq	%rbx, %rsi
	movl	%r15d, %r14d
	movq	%rdx, %r15
	leaq	16(%rsp), %rbp
	movq	%rbp, %rdx
	callq	hypre_ProjectBox
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	callq	hypre_StructMapFineToCoarse
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%rdi, %rcx
	callq	hypre_StructMapFineToCoarse
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	-24(%rax,%r15,8), %rdi
	movq	72(%rsp), %r13          # 8-byte Reload
	leaq	8(%rax,%r13), %r8
	movl	$1, %ecx
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	hypre_StructCoarsen
	movq	%r15, %rdx
	movl	%r14d, %r15d
	incq	%rdx
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%rax, %r12
	addq	$8, %r13
	movq	%r13, 72(%rsp)          # 8-byte Spill
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	leaq	-3(%rdx), %rsi
	testq	%rsi, %rsi
	jle	.LBB3_3
# BB#2:                                 #   in Loop: Header=BB3_1 Depth=1
	movl	$0, 4(%rsp)
	movl	$0, 8(%rsp)
	movl	$0, 12(%rsp)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 16(%rsp)
	movl	$1, %eax
	jmp	.LBB3_4
	.p2align	4, 0x90
.LBB3_3:                                #   in Loop: Header=BB3_1 Depth=1
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	12(%rdi), %eax
	movl	%eax, 4(%rsp)
	movl	16(%rdi), %eax
	movl	%eax, 8(%rsp)
	movl	20(%rdi), %eax
	movl	%eax, 12(%rsp)
	movl	24(%rdi), %eax
	movl	%eax, 16(%rsp)
	movl	28(%rdi), %eax
	movl	%eax, 20(%rsp)
	movl	32(%rdi), %eax
.LBB3_4:                                #   in Loop: Header=BB3_1 Depth=1
	movq	64(%rsp), %r13          # 8-byte Reload
	movl	%eax, 24(%rsp)
	movq	48(%rsp), %rdi          # 8-byte Reload
	shll	16(%rsp,%rdi,4)
	movl	(%r13,%rdi,4), %eax
	cmpl	12(%r13,%rdi,4), %eax
	jne	.LBB3_5
# BB#6:
	movq	%r12, 176(%rsp)         # 8-byte Spill
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	movq	%rsi, %r14
	leal	-2(%rdx), %ebx
	movq	%r13, %rdi
	callq	hypre_BoxDestroy
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	%ebx, 4(%rax)
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	80(%rsp), %r13          # 8-byte Reload
	movq	%r13, 40(%rbx)
	movq	(%r13), %rax
	movq	8(%rax), %rdi
	callq	hypre_BoxArrayDuplicate
	movq	%rax, %rbp
	movq	%rbp, %rdi
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	callq	hypre_ProjectBoxArray
	movq	%rbp, 48(%rbx)
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %r12
	movl	%r12d, %edi
	callq	hypre_MAlloc
	movq	%rax, %rbp
	testl	%r14d, %r14d
	movq	%r14, 40(%rsp)          # 8-byte Spill
	jle	.LBB3_7
# BB#8:                                 # %.lr.ph312
	movl	%r14d, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_9:                                # =>This Inner Loop Header: Depth=1
	testq	%r13, %r13
	jle	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_9 Depth=1
	movl	$0, 4(%rsp)
	movl	$0, 8(%rsp)
	movl	$0, 12(%rsp)
	movq	$0, 28(%rsp)
	movl	$0, 36(%rsp)
	movq	48(%rsp), %rax          # 8-byte Reload
	incl	28(%rsp,%rax,4)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 16(%rsp)
	movl	$1, %eax
	jmp	.LBB3_12
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_9 Depth=1
	movl	12(%rbx), %eax
	movl	%eax, 4(%rsp)
	movl	16(%rbx), %ecx
	movl	%ecx, 8(%rsp)
	movl	20(%rbx), %edx
	movl	%edx, 12(%rsp)
	movl	%eax, 28(%rsp)
	movl	%ecx, 32(%rsp)
	movl	%edx, 36(%rsp)
	movq	48(%rsp), %rax          # 8-byte Reload
	incl	28(%rsp,%rax,4)
	movl	24(%rbx), %eax
	movl	%eax, 16(%rsp)
	movl	28(%rbx), %eax
	movl	%eax, 20(%rsp)
	movl	32(%rbx), %eax
.LBB3_12:                               #   in Loop: Header=BB3_9 Depth=1
	movl	%eax, 24(%rsp)
	movq	48(%rsp), %rax          # 8-byte Reload
	shll	16(%rsp,%rax,4)
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %rax
	movq	8(%rax), %rdi
	callq	hypre_BoxArrayDuplicate
	movq	%rax, (%rbp,%r13,8)
	movq	%rax, %rdi
	leaq	28(%rsp), %rsi
	leaq	16(%rsp), %rdx
	callq	hypre_ProjectBoxArray
	incq	%r13
	cmpq	%r13, %r14
	jne	.LBB3_9
# BB#13:                                # %._crit_edge313.loopexit
	movq	176(%rsp), %rbx         # 8-byte Reload
	sarq	$32, %rbx
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	168(%rsp), %r13         # 8-byte Reload
	movq	160(%rsp), %r14         # 8-byte Reload
	jmp	.LBB3_14
.LBB3_7:
	xorl	%ebx, %ebx
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	160(%rsp), %r14         # 8-byte Reload
	movq	%r13, %rax
	movq	%rcx, %r13
.LBB3_14:                               # %._crit_edge313
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %rdi
	callq	hypre_BoxArrayDuplicate
	movq	%rax, (%rbp,%rbx,8)
	cmpl	$3, 184(%rsp)           # 4-byte Folded Reload
	jne	.LBB3_16
# BB#15:
	movq	%rax, %rdi
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	callq	hypre_ProjectBoxArray
.LBB3_16:
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbp, 56(%rbx)
	movl	%r12d, %edi
	callq	hypre_MAlloc
	movq	%rax, %rbp
	movl	%r12d, %edi
	callq	hypre_MAlloc
	movq	%rax, %r12
	movq	%r14, %rdi
	callq	hypre_StructMatrixRef
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%rax, (%rbp)
	movq	%r13, %rdi
	callq	hypre_StructVectorRef
	movq	48(%rsp), %rsi          # 8-byte Reload
	leal	(%rsi,%rsi), %ecx
	movq	40(%rsp), %rdx          # 8-byte Reload
	testl	%edx, %edx
	movq	%rax, (%r12)
	movslq	%ecx, %rax
	movl	$1, 192(%rsp,%rax,4)
	leal	1(%rsi,%rsi), %eax
	cltq
	movl	$1, 192(%rsp,%rax,4)
	jle	.LBB3_17
# BB#19:                                # %.lr.ph307
	movl	%edx, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	movl	%r15d, %r13d
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	80(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_20:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbx,8), %rdi
	movq	8(%r14,%rbx,8), %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	hypre_CycRedCreateCoarseOp
	movq	%rax, 8(%r15,%rbx,8)
	addl	60(%rax), %ebp
	movq	8(%r14,%rbx,8), %rsi
	movl	%r13d, %edi
	callq	hypre_StructVectorCreate
	movq	%rax, 8(%r12,%rbx,8)
	movq	%rax, %rdi
	leaq	192(%rsp), %rsi
	callq	hypre_StructVectorSetNumGhost
	movq	8(%r12,%rbx,8), %rdi
	callq	hypre_StructVectorInitializeShell
	movq	8(%r12,%rbx,8), %rax
	incq	%rbx
	addl	36(%rax), %ebp
	cmpq	%rbx, 104(%rsp)         # 8-byte Folded Reload
	jne	.LBB3_20
# BB#21:                                # %._crit_edge308
	movl	$8, %esi
	movl	%ebp, %edi
	callq	hypre_CAlloc
	movq	%rax, %rbp
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbp, 64(%rbx)
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_18
# BB#22:                                # %.lr.ph302.preheader
	movl	40(%rsp), %r14d         # 4-byte Reload
	xorl	%ebx, %ebx
	movq	64(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_23:                               # %.lr.ph302
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r15,%rbx,8), %rdi
	movq	%rbp, %rsi
	callq	hypre_StructMatrixInitializeData
	movq	8(%r15,%rbx,8), %rax
	movslq	60(%rax), %rax
	leaq	(%rbp,%rax,8), %rbp
	movq	8(%r12,%rbx,8), %rdi
	movq	%rbp, %rsi
	callq	hypre_StructVectorInitializeData
	movq	8(%r12,%rbx,8), %rdi
	callq	hypre_StructVectorAssemble
	movq	8(%r12,%rbx,8), %rax
	incq	%rbx
	movslq	36(%rax), %rax
	leaq	(%rbp,%rax,8), %rbp
	cmpq	%rbx, %r14
	jne	.LBB3_23
# BB#24:                                # %._crit_edge303
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%r15, 72(%rax)
	movq	%r12, 80(%rax)
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	movq	72(%rsp), %r15          # 8-byte Reload
	jle	.LBB3_30
# BB#25:                                # %.lr.ph297.preheader
	movl	40(%rsp), %r13d         # 4-byte Reload
	xorl	%ebp, %ebp
	leaq	4(%rsp), %rbx
	leaq	16(%rsp), %r14
	.p2align	4, 0x90
.LBB3_26:                               # %.lr.ph297
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbp, %rbp
	jle	.LBB3_28
# BB#27:                                #   in Loop: Header=BB3_26 Depth=1
	movl	$0, 4(%rsp)
	movl	$0, 8(%rsp)
	movl	$0, 12(%rsp)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 16(%rsp)
	movl	$1, %eax
	jmp	.LBB3_29
	.p2align	4, 0x90
.LBB3_28:                               #   in Loop: Header=BB3_26 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	12(%rcx), %eax
	movl	%eax, 4(%rsp)
	movl	16(%rcx), %eax
	movl	%eax, 8(%rsp)
	movl	20(%rcx), %eax
	movl	%eax, 12(%rsp)
	movl	24(%rcx), %eax
	movl	%eax, 16(%rsp)
	movl	28(%rcx), %eax
	movl	%eax, 20(%rsp)
	movl	32(%rcx), %eax
.LBB3_29:                               #   in Loop: Header=BB3_26 Depth=1
	movl	%eax, 24(%rsp)
	movq	48(%rsp), %rax          # 8-byte Reload
	shll	16(%rsp,%rax,4)
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	movq	8(%rax,%rbp,8), %rsi
	incq	%rbp
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	hypre_CycRedSetupCoarseOp
	cmpq	%rbp, %r13
	jne	.LBB3_26
	jmp	.LBB3_30
.LBB3_17:                               # %._crit_edge308.thread
	xorl	%edi, %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, 64(%rbx)
.LBB3_18:                               # %._crit_edge303.thread
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, 72(%rbx)
	movq	%r12, 80(%rbx)
	movq	72(%rsp), %r15          # 8-byte Reload
.LBB3_30:                               # %._crit_edge298
	movq	%r12, 72(%rsp)          # 8-byte Spill
	movl	%r15d, %edi
	callq	hypre_MAlloc
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	%r15d, %edi
	callq	hypre_MAlloc
	movq	%rax, 88(%rsp)          # 8-byte Spill
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_36
# BB#31:                                # %.lr.ph293
	movl	40(%rsp), %eax          # 4-byte Reload
	movq	%rax, 104(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	movq	88(%rsp), %r13          # 8-byte Reload
	movq	96(%rsp), %r14          # 8-byte Reload
	movq	80(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_32:                               # =>This Inner Loop Header: Depth=1
	testq	%rbp, %rbp
	jle	.LBB3_34
# BB#33:                                #   in Loop: Header=BB3_32 Depth=1
	movl	$0, 4(%rsp)
	movl	$0, 8(%rsp)
	movl	$0, 12(%rsp)
	movq	$0, 28(%rsp)
	movl	$0, 36(%rsp)
	movq	48(%rsp), %rax          # 8-byte Reload
	incl	28(%rsp,%rax,4)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 16(%rsp)
	movl	$1, %eax
	jmp	.LBB3_35
	.p2align	4, 0x90
.LBB3_34:                               #   in Loop: Header=BB3_32 Depth=1
	movq	56(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsi), %eax
	movl	%eax, 4(%rsp)
	movl	16(%rsi), %ecx
	movl	%ecx, 8(%rsp)
	movl	20(%rsi), %edx
	movl	%edx, 12(%rsp)
	movl	%eax, 28(%rsp)
	movl	%ecx, 32(%rsp)
	movl	%edx, 36(%rsp)
	movq	48(%rsp), %rax          # 8-byte Reload
	incl	28(%rsp,%rax,4)
	movl	24(%rsi), %eax
	movl	%eax, 16(%rsp)
	movl	28(%rsi), %eax
	movl	%eax, 20(%rsp)
	movl	32(%rsi), %eax
.LBB3_35:                               #   in Loop: Header=BB3_32 Depth=1
	movl	%eax, 24(%rsp)
	movq	48(%rsp), %rax          # 8-byte Reload
	shll	16(%rsp,%rax,4)
	movq	(%r12,%rbp,8), %rdi
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	leaq	136(%rsp), %rax
	movq	%rax, %rdx
	leaq	128(%rsp), %rax
	movq	%rax, %rcx
	leaq	152(%rsp), %rax
	movq	%rax, %r8
	leaq	144(%rsp), %rax
	movq	%rax, %r9
	leaq	112(%rsp), %rax
	pushq	%rax
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	leaq	128(%rsp), %rax
	pushq	%rax
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CreateComputeInfo
	addq	$16, %rsp
.Lcfi46:
	.cfi_adjust_cfa_offset -16
	movq	136(%rsp), %rdi
	leaq	28(%rsp), %r15
	movq	%r15, %rsi
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	128(%rsp), %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	120(%rsp), %rdi
	leaq	4(%rsp), %r15
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	112(%rsp), %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	136(%rsp), %rdi
	movq	128(%rsp), %rsi
	movq	152(%rsp), %r8
	movq	144(%rsp), %r9
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rax
	subq	$8, %rsp
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdx
	movq	%rbx, %rcx
	pushq	%r14
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	pushq	16(%rax)
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	pushq	(%r12,%rbp,8)
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	pushq	160(%rsp)
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	176(%rsp)
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	callq	hypre_ComputePkgCreate
	addq	$64, %rsp
.Lcfi55:
	.cfi_adjust_cfa_offset -64
	movq	(%r12,%rbp,8), %rdi
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	leaq	136(%rsp), %rdx
	leaq	128(%rsp), %rcx
	leaq	152(%rsp), %r8
	leaq	144(%rsp), %r9
	leaq	112(%rsp), %rax
	pushq	%rax
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	leaq	128(%rsp), %rax
	pushq	%rax
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CreateComputeInfo
	addq	$16, %rsp
.Lcfi58:
	.cfi_adjust_cfa_offset -16
	movq	136(%rsp), %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	128(%rsp), %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	120(%rsp), %rdi
	leaq	28(%rsp), %r15
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	112(%rsp), %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	136(%rsp), %rdi
	movq	128(%rsp), %rsi
	movq	152(%rsp), %r8
	movq	144(%rsp), %r9
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rax
	subq	$8, %rsp
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdx
	movq	%rbx, %rcx
	pushq	%r13
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	16(%rax)
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	(%r12,%rbp,8)
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	160(%rsp)
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	176(%rsp)
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	callq	hypre_ComputePkgCreate
	addq	$64, %rsp
.Lcfi67:
	.cfi_adjust_cfa_offset -64
	incq	%rbp
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%rbp, 104(%rsp)         # 8-byte Folded Reload
	jne	.LBB3_32
.LBB3_36:                               # %._crit_edge294
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, 88(%rbx)
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rax, 96(%rbx)
	movl	28(%rbx), %esi
	imull	24(%rbx), %esi
	imull	32(%rbx), %esi
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rax
	movl	72(%rax), %edi
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	cltd
	idivl	%esi
	movl	%eax, %ecx
	leal	(%rdi,%rdi,4), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	cltd
	idivl	%esi
	addl	%ecx, %eax
	movl	%eax, 108(%rbx)
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpl	$2, %ecx
	jl	.LBB3_37
# BB#38:                                # %.lr.ph.preheader
	movl	%ecx, %ecx
	addq	$-2, %rcx
	movq	184(%rsp), %rdx         # 8-byte Reload
	testb	$3, %dl
	je	.LBB3_39
# BB#40:                                # %.lr.ph.prol.preheader
	andb	$3, %dl
	movzbl	%dl, %esi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_41:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp,%rdx,8), %rdi
	imull	$5, 72(%rdi), %edi
	addl	%edi, %eax
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB3_41
# BB#42:                                # %.lr.ph.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %rcx
	jae	.LBB3_44
	jmp	.LBB3_46
.LBB3_37:
	movl	$1, %ecx
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	jg	.LBB3_48
	jmp	.LBB3_49
.LBB3_39:
	movl	$1, %edx
	cmpq	$3, %rcx
	jb	.LBB3_46
.LBB3_44:                               # %.lr.ph.preheader.new
	movl	40(%rsp), %ecx          # 4-byte Reload
	subq	%rdx, %rcx
	leaq	24(%rbp,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB3_45:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	movq	-16(%rdx), %rdi
	imull	$5, 72(%rsi), %esi
	addl	%eax, %esi
	imull	$5, 72(%rdi), %eax
	addl	%esi, %eax
	movq	-8(%rdx), %rsi
	imull	$5, 72(%rsi), %esi
	addl	%eax, %esi
	movq	(%rdx), %rax
	imull	$5, 72(%rax), %eax
	addl	%esi, %eax
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB3_45
.LBB3_46:                               # %._crit_edge
	movl	%eax, 108(%rbx)
	movq	176(%rsp), %rcx         # 8-byte Reload
	sarq	$32, %rcx
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	jle	.LBB3_49
.LBB3_48:
	movq	(%rbp,%rcx,8), %rcx
	movl	72(%rcx), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	addl	%edx, %eax
	movl	%eax, 108(%rbx)
.LBB3_49:
	xorl	%eax, %eax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	hypre_CyclicReductionSetup, .Lfunc_end3-hypre_CyclicReductionSetup
	.cfi_endproc

	.globl	hypre_CyclicReduction
	.p2align	4, 0x90
	.type	hypre_CyclicReduction,@function
hypre_CyclicReduction:                  # @hypre_CyclicReduction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	subq	$664, %rsp              # imm = 0x298
.Lcfi74:
	.cfi_def_cfa_offset 720
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, 272(%rsp)         # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movslq	4(%rbx), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movslq	8(%rbx), %rax
	movq	%rax, 512(%rsp)         # 8-byte Spill
	movq	48(%rbx), %r13
	movq	56(%rbx), %rax
	movq	%rax, 592(%rsp)         # 8-byte Spill
	movq	72(%rbx), %rbp
	movq	80(%rbx), %r15
	movq	88(%rbx), %rax
	movq	%rax, 528(%rsp)         # 8-byte Spill
	movq	96(%rbx), %rax
	movq	%rax, 520(%rsp)         # 8-byte Spill
	movl	104(%rbx), %edi
	callq	hypre_BeginTiming
	movq	(%rbp), %rdi
	callq	hypre_StructMatrixDestroy
	movq	(%r15), %rdi
	callq	hypre_StructVectorDestroy
	movq	%r14, %rdi
	callq	hypre_StructMatrixRef
	movq	%rbp, 360(%rsp)         # 8-byte Spill
	movq	%rax, (%rbp)
	movq	%r12, 280(%rsp)         # 8-byte Spill
	movq	%r12, %rdi
	callq	hypre_StructVectorRef
	movq	%r13, %rcx
	movq	%r15, 168(%rsp)         # 8-byte Spill
	movq	%rax, (%r15)
	cmpl	$0, 8(%rcx)
	movq	%rbx, 352(%rsp)         # 8-byte Spill
	jle	.LBB4_1
# BB#5:                                 # %.lr.ph1909
	leaq	24(%rbx), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	leaq	236(%rsp), %rax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	leaq	104(%rsp), %rax
	movq	%rax, 432(%rsp)         # 8-byte Spill
	xorl	%edi, %edi
	movq	%rcx, 264(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_10 Depth 2
                                        #       Child Loop BB4_12 Depth 3
                                        #         Child Loop BB4_14 Depth 4
                                        #         Child Loop BB4_18 Depth 4
	movq	(%rcx), %rax
	leaq	(,%rdi,8), %rcx
	leaq	(%rcx,%rcx,2), %rbx
	movq	280(%rsp), %rdx         # 8-byte Reload
	movq	16(%rdx), %rcx
	movq	24(%rdx), %rsi
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	(%rcx), %r12
	movq	272(%rsp), %rsi         # 8-byte Reload
	movq	16(%rsi), %rcx
	movq	24(%rsi), %r13
	movq	(%rcx), %rbp
	movq	40(%rdx), %rcx
	movslq	(%rcx,%rdi,4), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	40(%rsi), %rcx
	movq	%rdi, 208(%rsp)         # 8-byte Spill
	movslq	(%rcx,%rdi,4), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	(%rax,%rbx), %ecx
	movl	%ecx, 104(%rsp)
	movl	4(%rax,%rbx), %ecx
	movl	%ecx, 108(%rsp)
	movl	8(%rax,%rbx), %ecx
	leaq	(%rax,%rbx), %rdi
	movl	%ecx, 112(%rsp)
	movq	256(%rsp), %rsi         # 8-byte Reload
	leaq	236(%rsp), %rdx
	callq	hypre_BoxGetStrideSize
	movl	(%rbp,%rbx), %r9d
	movl	4(%rbp,%rbx), %edi
	movl	12(%rbp,%rbx), %eax
	movl	%eax, %r14d
	subl	%r9d, %r14d
	incl	%r14d
	cmpl	%r9d, %eax
	movl	16(%rbp,%rbx), %eax
	movl	$0, %edx
	cmovsl	%edx, %r14d
	movl	%eax, %r8d
	subl	%edi, %r8d
	incl	%r8d
	cmpl	%edi, %eax
	movl	(%r12,%rbx), %ecx
	movl	4(%r12,%rbx), %r11d
	movl	12(%r12,%rbx), %eax
	cmovsl	%edx, %r8d
	movl	%eax, %r15d
	subl	%ecx, %r15d
	incl	%r15d
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	cmpl	%ecx, %eax
	movl	16(%r12,%rbx), %ecx
	cmovsl	%edx, %r15d
	movl	%ecx, %eax
	subl	%r11d, %eax
	incl	%eax
	cmpl	%r11d, %ecx
	cmovsl	%edx, %eax
	movl	236(%rsp), %ecx
	movl	240(%rsp), %edx
	movl	244(%rsp), %esi
	cmpl	%ecx, %edx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	cmovgel	%edx, %ecx
	cmpl	%ecx, %esi
	movl	%esi, 216(%rsp)         # 4-byte Spill
	cmovgel	%esi, %ecx
	testl	%ecx, %ecx
	jle	.LBB4_22
# BB#7:                                 # %.lr.ph1905
                                        #   in Loop: Header=BB4_6 Depth=1
	cmpl	$0, 216(%rsp)           # 4-byte Folded Reload
	jle	.LBB4_22
# BB#8:                                 # %.lr.ph1905
                                        #   in Loop: Header=BB4_6 Depth=1
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_22
# BB#9:                                 # %.preheader1641.us.preheader
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movl	112(%rsp), %ecx
	movl	%ecx, %r13d
	subl	8(%rbp,%rbx), %r13d
	subl	8(%r12,%rbx), %ecx
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movq	352(%rsp), %rdx         # 8-byte Reload
	movl	32(%rdx), %ecx
	movl	%r14d, %ebx
	imull	%r8d, %ebx
	imull	%ecx, %ebx
	movl	%edi, 8(%rsp)           # 4-byte Spill
	movl	%r15d, %edi
	imull	%eax, %edi
	imull	%ecx, %edi
	movl	%r11d, 16(%rsp)         # 4-byte Spill
	movl	28(%rdx), %r11d
	movl	%r9d, 64(%rsp)          # 4-byte Spill
	movl	%r14d, %ebp
	imull	%r11d, %ebp
	imull	%r15d, %r11d
	movslq	24(%rdx), %r9
	movl	%r9d, %edx
	negl	%edx
	movq	88(%rsp), %r10          # 8-byte Reload
	imull	%r10d, %edx
	leal	(%rdx,%r11), %esi
	addl	%ebp, %edx
	movq	48(%rsp), %r12          # 8-byte Reload
	leal	-1(%r12), %ecx
	imull	%ecx, %esi
	imull	%ecx, %edx
	addl	%r11d, %esi
	movl	%r9d, %ecx
	imull	%r10d, %ecx
	subl	%ecx, %esi
	movl	%esi, 184(%rsp)         # 4-byte Spill
	addl	%ebp, %edx
	subl	%ecx, %edx
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	movl	104(%rsp), %r10d
	movl	%r10d, %edx
	subl	64(%rsp), %edx          # 4-byte Folded Reload
	movl	108(%rsp), %esi
	movl	%esi, %ecx
	subl	8(%rsp), %ecx           # 4-byte Folded Reload
	imull	%r8d, %r13d
	addl	%ecx, %r13d
	imull	%r14d, %r13d
	addl	%edx, %r13d
	movl	40(%rsp), %edx          # 4-byte Reload
	subl	24(%rsp), %r10d         # 4-byte Folded Reload
	subl	16(%rsp), %esi          # 4-byte Folded Reload
	imull	%eax, %edx
	addl	%esi, %edx
	movl	%r12d, %eax
	movq	%r11, 64(%rsp)          # 8-byte Spill
	imull	%r11d, %eax
	movl	%eax, 248(%rsp)         # 4-byte Spill
	subl	%eax, %edi
	movl	%edi, 192(%rsp)         # 4-byte Spill
	imull	%r15d, %edx
	movl	%r12d, %eax
	movl	%ebp, 136(%rsp)         # 4-byte Spill
	imull	%ebp, %eax
	movl	%eax, 288(%rsp)         # 4-byte Spill
	subl	%eax, %ebx
	movl	%ebx, 296(%rsp)         # 4-byte Spill
	addl	%r10d, %edx
	movq	88(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r15d
	andl	$3, %r15d
	movq	(%rsp), %rax            # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r14,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	shlq	$3, %r14
	movq	%r14, 72(%rsp)          # 8-byte Spill
	movq	%r9, %rdi
	shlq	$5, %rdi
	movq	%r9, %r11
	shlq	$4, %r11
	leaq	(,%r9,8), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	(%rax,%rax,2), %rbp
	leaq	(%rcx,%r9,8), %r10
	leaq	(%rcx,%rbp), %r14
	leaq	(%rcx,%r11), %r8
	leal	-1(%rbx), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	xorl	%ebx, %ebx
	movq	%r9, 96(%rsp)           # 8-byte Spill
	movl	%r15d, 80(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB4_10:                               # %.preheader1641.us
                                        #   Parent Loop BB4_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_12 Depth 3
                                        #         Child Loop BB4_14 Depth 4
                                        #         Child Loop BB4_18 Depth 4
	cmpl	$0, 88(%rsp)            # 4-byte Folded Reload
	movq	176(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	184(%rsp), %ecx         # 4-byte Reload
	jle	.LBB4_21
# BB#11:                                # %.preheader1640.us.us.preheader
                                        #   in Loop: Header=BB4_10 Depth=2
	movl	%ebx, 224(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movl	%edx, 40(%rsp)          # 4-byte Spill
	movl	%edx, %r12d
	movl	%r13d, 160(%rsp)        # 4-byte Spill
                                        # kill: %R13D<def> %R13D<kill> %R13<def>
	.p2align	4, 0x90
.LBB4_12:                               # %.preheader1640.us.us
                                        #   Parent Loop BB4_6 Depth=1
                                        #     Parent Loop BB4_10 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_14 Depth 4
                                        #         Child Loop BB4_18 Depth 4
	movl	%eax, (%rsp)            # 4-byte Spill
	testl	%r15d, %r15d
	movslq	%r13d, %r13
	movslq	%r12d, %r12
	movq	%r13, %rbx
	movq	%r12, %rcx
	movl	$0, %eax
	je	.LBB4_16
# BB#13:                                # %.prol.preheader2323
                                        #   in Loop: Header=BB4_12 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%r13,8), %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r12,8), %rdx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_14:                               #   Parent Loop BB4_6 Depth=1
                                        #     Parent Loop BB4_10 Depth=2
                                        #       Parent Loop BB4_12 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rbx,%rcx,8), %rsi
	movq	%rsi, (%rdx,%rcx,8)
	incl	%eax
	addq	%r9, %rcx
	cmpl	%eax, %r15d
	jne	.LBB4_14
# BB#15:                                # %.prol.loopexit2324.unr-lcssa
                                        #   in Loop: Header=BB4_12 Depth=3
	leaq	(%r13,%rcx), %rbx
	addq	%r12, %rcx
.LBB4_16:                               # %.prol.loopexit2324
                                        #   in Loop: Header=BB4_12 Depth=3
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	cmpl	$3, 56(%rsp)            # 4-byte Folded Reload
	movq	%r8, %r15
	jb	.LBB4_19
# BB#17:                                # %.preheader1640.us.us.new
                                        #   in Loop: Header=BB4_12 Depth=3
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	(%rdx,%rbx,8), %r13
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rbx,8), %r8
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	movq	128(%rsp), %rdx         # 8-byte Reload
	leaq	(%rcx,%rdx), %r12
	movq	88(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %ebx
	subl	%eax, %ebx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_18:                               #   Parent Loop BB4_6 Depth=1
                                        #     Parent Loop BB4_10 Depth=2
                                        #       Parent Loop BB4_12 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r13,%rax), %rdx
	movq	%rdx, (%rcx,%rax)
	leaq	(%r8,%rax), %rdx
	movq	(%r10,%rdx), %rsi
	movq	%rsi, (%r12,%rax)
	movq	(%r15,%rdx), %rsi
	movq	%r14, %r9
	movq	%r10, %r14
	movq	%r11, %r10
	leaq	(%r10,%rax), %r11
	movq	%rsi, (%rcx,%r11)
	movq	%r10, %r11
	movq	%r14, %r10
	movq	%r9, %r14
	movq	(%r14,%rdx), %rdx
	leaq	(%rbp,%rax), %rsi
	movq	%rdx, (%rcx,%rsi)
	addq	%rdi, %rax
	addl	$-4, %ebx
	jne	.LBB4_18
.LBB4_19:                               # %._crit_edge1873.us.us
                                        #   in Loop: Header=BB4_12 Depth=3
	movq	32(%rsp), %r12          # 8-byte Reload
	addl	64(%rsp), %r12d         # 4-byte Folded Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	addl	136(%rsp), %r13d        # 4-byte Folded Reload
	movl	(%rsp), %eax            # 4-byte Reload
	incl	%eax
	cmpl	48(%rsp), %eax          # 4-byte Folded Reload
	movq	96(%rsp), %r9           # 8-byte Reload
	movq	%r15, %r8
	movl	80(%rsp), %r15d         # 4-byte Reload
	jne	.LBB4_12
# BB#20:                                #   in Loop: Header=BB4_10 Depth=2
	movl	288(%rsp), %eax         # 4-byte Reload
	movl	248(%rsp), %ecx         # 4-byte Reload
	movl	40(%rsp), %edx          # 4-byte Reload
	movl	160(%rsp), %r13d        # 4-byte Reload
	movl	224(%rsp), %ebx         # 4-byte Reload
.LBB4_21:                               # %._crit_edge1879.us
                                        #   in Loop: Header=BB4_10 Depth=2
	addl	%edx, %ecx
	addl	%r13d, %eax
	addl	192(%rsp), %ecx         # 4-byte Folded Reload
	addl	296(%rsp), %eax         # 4-byte Folded Reload
	incl	%ebx
	cmpl	216(%rsp), %ebx         # 4-byte Folded Reload
	movl	%ecx, %edx
	movl	%eax, %r13d
	jne	.LBB4_10
.LBB4_22:                               # %._crit_edge1906
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	208(%rsp), %rdi         # 8-byte Reload
	incq	%rdi
	movq	264(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %rdi
	jl	.LBB4_6
	jmp	.LBB4_2
.LBB4_1:                                # %..preheader1639_crit_edge
	leaq	104(%rsp), %rax
	movq	%rax, 432(%rsp)         # 8-byte Spill
	leaq	236(%rsp), %rax
	movq	%rax, 368(%rsp)         # 8-byte Spill
.LBB4_2:                                # %.preheader1639
	movq	200(%rsp), %rax         # 8-byte Reload
	leal	-1(%rax), %eax
	movq	%rax, 584(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
                                        # implicit-def: %RDI
	jmp	.LBB4_3
	.p2align	4, 0x90
.LBB4_94:                               #   in Loop: Header=BB4_3 Depth=1
	movq	600(%rsp), %rcx         # 8-byte Reload
.LBB4_3:                                # %.loopexit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_26 Depth 2
                                        #       Child Loop BB4_30 Depth 3
                                        #         Child Loop BB4_32 Depth 4
                                        #           Child Loop BB4_57 Depth 5
                                        #           Child Loop BB4_39 Depth 5
                                        #     Child Loop BB4_46 Depth 2
                                        #       Child Loop BB4_64 Depth 3
                                        #         Child Loop BB4_65 Depth 4
                                        #         Child Loop BB4_68 Depth 4
                                        #           Child Loop BB4_72 Depth 5
                                        #             Child Loop BB4_74 Depth 6
                                        #               Child Loop BB4_86 Depth 7
                                        #               Child Loop BB4_77 Depth 7
	movq	352(%rsp), %rax         # 8-byte Reload
	movq	%rcx, %rdx
	movq	%rdx, 328(%rsp)         # 8-byte Spill
	testq	%rcx, %rcx
	movq	%rdi, 320(%rsp)         # 8-byte Spill
	jle	.LBB4_23
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	movl	$0, 308(%rsp)
	movl	$0, 312(%rsp)
	movl	$0, 316(%rsp)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 148(%rsp)
	movl	$1, %eax
	jmp	.LBB4_24
	.p2align	4, 0x90
.LBB4_23:                               #   in Loop: Header=BB4_3 Depth=1
	movq	%rax, %rcx
	movl	12(%rcx), %eax
	movl	%eax, 308(%rsp)
	movl	16(%rcx), %eax
	movl	%eax, 312(%rsp)
	movl	20(%rcx), %eax
	movl	%eax, 316(%rsp)
	movl	24(%rcx), %eax
	movl	%eax, 148(%rsp)
	movl	28(%rcx), %eax
	movl	%eax, 152(%rsp)
	movl	32(%rcx), %eax
.LBB4_24:                               #   in Loop: Header=BB4_3 Depth=1
	movl	%eax, 156(%rsp)
	movq	512(%rsp), %rax         # 8-byte Reload
	shll	148(%rsp,%rax,4)
	movq	592(%rsp), %rax         # 8-byte Reload
	movq	328(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	cmpl	$0, 8(%rcx)
	jle	.LBB4_44
# BB#25:                                # %.lr.ph1798
                                        #   in Loop: Header=BB4_3 Depth=1
	xorl	%r14d, %r14d
	movq	%rcx, 256(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_26:                               #   Parent Loop BB4_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_30 Depth 3
                                        #         Child Loop BB4_32 Depth 4
                                        #           Child Loop BB4_57 Depth 5
                                        #           Child Loop BB4_39 Depth 5
	movq	(%rcx), %rbp
	leaq	(,%r14,8), %rax
	leaq	(%rax,%rax,2), %r13
	movq	360(%rsp), %rax         # 8-byte Reload
	movq	328(%rsp), %rbx         # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	movq	40(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	168(%rsp), %r12         # 8-byte Reload
	movq	(%r12,%rbx,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %r15
	movq	$0, 116(%rsp)
	movl	$0, 124(%rsp)
	movl	%r14d, %esi
	leaq	116(%rsp), %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	(%r12,%rbx,8), %rax
	movq	24(%rax), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	40(%rax), %rax
	movq	%r14, 264(%rsp)         # 8-byte Spill
	movslq	(%rax,%r14,4), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	(%rbp,%r13), %eax
	movl	%eax, 104(%rsp)
	movl	4(%rbp,%r13), %eax
	movl	%eax, 108(%rsp)
	movl	8(%rbp,%r13), %eax
	leaq	(%rbp,%r13), %rdi
	movl	%eax, 112(%rsp)
	leaq	148(%rsp), %rsi
	movq	368(%rsp), %rdx         # 8-byte Reload
	callq	hypre_BoxGetStrideSize
	movq	%r15, %rdi
	movl	(%rdi,%r13), %r9d
	movl	4(%rdi,%r13), %r11d
	movl	12(%rdi,%r13), %ecx
	movl	%ecx, %r12d
	subl	%r9d, %r12d
	incl	%r12d
	cmpl	%r9d, %ecx
	movl	16(%rdi,%r13), %ecx
	movl	$0, %ebx
	cmovsl	%ebx, %r12d
	movl	%ecx, %r14d
	subl	%r11d, %r14d
	incl	%r14d
	cmpl	%r11d, %ecx
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	(%rcx,%r13), %r8d
	movl	4(%rcx,%r13), %esi
	movl	12(%rcx,%r13), %edx
	cmovsl	%ebx, %r14d
	movl	%edx, %r15d
	subl	%r8d, %r15d
	incl	%r15d
	cmpl	%r8d, %edx
	movl	16(%rcx,%r13), %edx
	cmovsl	%ebx, %r15d
	movl	%edx, %ebp
	subl	%esi, %ebp
	incl	%ebp
	cmpl	%esi, %edx
	cmovsl	%ebx, %ebp
	movl	236(%rsp), %r10d
	movl	240(%rsp), %ebx
	movl	244(%rsp), %eax
	cmpl	%r10d, %ebx
	movl	%r10d, %edx
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	cmovgel	%ebx, %edx
	cmpl	%edx, %eax
	movl	%eax, 176(%rsp)         # 4-byte Spill
	cmovgel	%eax, %edx
	testl	%edx, %edx
	jle	.LBB4_43
# BB#27:                                # %.lr.ph1792
                                        #   in Loop: Header=BB4_26 Depth=2
	cmpl	$0, 176(%rsp)           # 4-byte Folded Reload
	jle	.LBB4_43
# BB#28:                                # %.lr.ph1792
                                        #   in Loop: Header=BB4_26 Depth=2
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_43
# BB#29:                                # %.preheader1637.us.preheader
                                        #   in Loop: Header=BB4_26 Depth=2
	movl	112(%rsp), %eax
	movl	%r8d, 24(%rsp)          # 4-byte Spill
	movl	%esi, 32(%rsp)          # 4-byte Spill
	movl	%eax, %r8d
	subl	8(%rdi,%r13), %r8d
	movl	156(%rsp), %edx
	subl	8(%rcx,%r13), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movl	%r11d, (%rsp)           # 4-byte Spill
	movl	%r12d, %esi
	imull	%r14d, %esi
	imull	%edx, %esi
	movl	%r15d, %ecx
	imull	%ebp, %ecx
	imull	%edx, %ecx
	movl	%r9d, 48(%rsp)          # 4-byte Spill
	movl	152(%rsp), %edi
	movl	%r12d, %r9d
	imull	%edi, %r9d
	imull	%r15d, %edi
	movslq	148(%rsp), %r11
	movl	%r11d, %eax
	negl	%eax
	imull	%r10d, %eax
	leal	(%rax,%rdi), %ebx
	addl	%r9d, %eax
	movq	40(%rsp), %rdx          # 8-byte Reload
	leal	-1(%rdx), %r13d
	imull	%r13d, %ebx
	imull	%r13d, %eax
	addl	%edi, %ebx
	movl	%r11d, %edx
	imull	%r10d, %edx
	subl	%edx, %ebx
	movl	%ebx, 208(%rsp)         # 4-byte Spill
	addl	%r9d, %eax
	subl	%edx, %eax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movl	104(%rsp), %r13d
	movl	%r13d, %edx
	subl	48(%rsp), %edx          # 4-byte Folded Reload
	movl	108(%rsp), %eax
	movl	%eax, %ebx
	subl	(%rsp), %ebx            # 4-byte Folded Reload
	imull	%r14d, %r8d
	addl	%ebx, %r8d
	imull	%r12d, %r8d
	addl	%edx, %r8d
	movl	%r8d, 96(%rsp)          # 4-byte Spill
	subl	24(%rsp), %r13d         # 4-byte Folded Reload
	subl	32(%rsp), %eax          # 4-byte Folded Reload
	movl	56(%rsp), %ebx          # 4-byte Reload
	imull	%ebp, %ebx
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	(%r8,%rbp,8), %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	addl	%eax, %ebx
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %eax
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	imull	%edi, %eax
	movl	%eax, 280(%rsp)         # 4-byte Spill
	subl	%eax, %ecx
	movl	%ecx, 248(%rsp)         # 4-byte Spill
	imull	%r15d, %ebx
	movl	%edx, %ecx
	movq	%r9, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	imull	%r9d, %ecx
	movl	%ecx, 272(%rsp)         # 4-byte Spill
	subl	%ecx, %esi
	movl	%esi, 184(%rsp)         # 4-byte Spill
	addl	%r13d, %ebx
	movl	%ebx, 56(%rsp)          # 4-byte Spill
	leal	-1(%r10), %ecx
	addq	%rcx, %rbp
	leaq	8(%r8,%rbp,8), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	1(%rcx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, %rdx
	movabsq	$8589934590, %rax       # imm = 0x1FFFFFFFE
	andq	%rax, %rdx
	setne	%al
	leaq	-2(%rdx), %rsi
	shrq	%rsi
	cmpl	$1, %r11d
	sete	%bl
	andb	%al, %bl
	movb	%bl, 64(%rsp)           # 1-byte Spill
	movq	%r11, %rax
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	imulq	%rdx, %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	%rsi, 224(%rsp)         # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	andl	$1, %esi
	movq	%rsi, 216(%rsp)         # 8-byte Spill
	movq	%r11, %r9
	shlq	$5, %r9
	movq	%r11, %r13
	shlq	$4, %r13
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	leaq	8(%rax,%rcx,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%r11, 48(%rsp)          # 8-byte Spill
	leaq	(,%r11,8), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movq	%r10, 160(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_30:                               # %.preheader1637.us
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_26 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_32 Depth 4
                                        #           Child Loop BB4_57 Depth 5
                                        #           Child Loop BB4_39 Depth 5
	testl	%r10d, %r10d
	movq	288(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	208(%rsp), %ecx         # 4-byte Reload
	jle	.LBB4_42
# BB#31:                                # %.preheader1636.us.us.preheader
                                        #   in Loop: Header=BB4_30 Depth=3
	movl	%edx, 192(%rsp)         # 4-byte Spill
	xorl	%r11d, %r11d
	movl	96(%rsp), %eax          # 4-byte Reload
	movl	56(%rsp), %ecx          # 4-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
	jmp	.LBB4_32
.LBB4_52:                               # %vector.body2149.preheader
                                        #   in Loop: Header=BB4_32 Depth=4
	cmpq	$0, 216(%rsp)           # 8-byte Folded Reload
	jne	.LBB4_53
# BB#54:                                # %vector.body2149.prol
                                        #   in Loop: Header=BB4_32 Depth=4
	movupd	(%r8,%r12,8), %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movupd	(%rax,%rdi,8), %xmm1
	divpd	%xmm0, %xmm1
	movupd	%xmm1, (%rax,%rdi,8)
	movl	$2, %ebx
	cmpq	$0, 224(%rsp)           # 8-byte Folded Reload
	jne	.LBB4_56
	jmp	.LBB4_58
.LBB4_53:                               #   in Loop: Header=BB4_32 Depth=4
	xorl	%ebx, %ebx
	cmpq	$0, 224(%rsp)           # 8-byte Folded Reload
	je	.LBB4_58
.LBB4_56:                               # %vector.body2149.preheader.new
                                        #   in Loop: Header=BB4_32 Depth=4
	movq	80(%rsp), %r15          # 8-byte Reload
	subq	%rbx, %r15
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	imulq	%rbx, %rdx
	leaq	(%rdx,%rcx), %rax
	movq	72(%rsp), %r8           # 8-byte Reload
	leaq	(%r8,%rax,8), %rax
	addq	%r14, %rdx
	movq	(%rsp), %rsi            # 8-byte Reload
	leaq	(%rsi,%rdx,8), %r10
	addq	$2, %rbx
	imulq	%rbp, %rbx
	movq	%rcx, %rdx
	addq	%rbx, %rdx
	leaq	(%r8,%rdx,8), %r8
	addq	%r14, %rbx
	leaq	(%rsi,%rbx,8), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_57:                               # %vector.body2149
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_26 Depth=2
                                        #       Parent Loop BB4_30 Depth=3
                                        #         Parent Loop BB4_32 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movupd	(%rax,%rsi), %xmm0
	movupd	(%r10,%rsi), %xmm1
	divpd	%xmm0, %xmm1
	movupd	%xmm1, (%r10,%rsi)
	movupd	(%r8,%rsi), %xmm0
	movupd	(%rdx,%rsi), %xmm1
	divpd	%xmm0, %xmm1
	movupd	%xmm1, (%rdx,%rsi)
	addq	%r9, %rsi
	addq	$-4, %r15
	jne	.LBB4_57
.LBB4_58:                               # %middle.block2150
                                        #   in Loop: Header=BB4_32 Depth=4
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, 16(%rsp)          # 8-byte Folded Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	160(%rsp), %r10         # 8-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
	je	.LBB4_40
# BB#59:                                #   in Loop: Header=BB4_32 Depth=4
	movq	296(%rsp), %rax         # 8-byte Reload
	addq	%rax, %rdi
	addq	%rax, %r12
	movq	80(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	jmp	.LBB4_34
	.p2align	4, 0x90
.LBB4_32:                               # %.preheader1636.us.us
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_26 Depth=2
                                        #       Parent Loop BB4_30 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB4_57 Depth 5
                                        #           Child Loop BB4_39 Depth 5
	movslq	%ecx, %rcx
	movslq	%eax, %r14
	cmpq	$1, 16(%rsp)            # 8-byte Folded Reload
	jbe	.LBB4_33
# BB#49:                                # %min.iters.checked2153
                                        #   in Loop: Header=BB4_32 Depth=4
	cmpb	$0, 64(%rsp)            # 1-byte Folded Reload
	je	.LBB4_33
# BB#50:                                # %vector.memcheck2178
                                        #   in Loop: Header=BB4_32 Depth=4
	movq	32(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r11d, %eax
	addl	56(%rsp), %eax          # 4-byte Folded Reload
	movslq	%eax, %r12
	movq	24(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r11d, %eax
	addl	96(%rsp), %eax          # 4-byte Folded Reload
	movslq	%eax, %rdi
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%rdi,8), %rax
	movq	88(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r12,8), %rdx
	cmpq	%rdx, %rax
	jae	.LBB4_52
# BB#51:                                # %vector.memcheck2178
                                        #   in Loop: Header=BB4_32 Depth=4
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdi,8), %rax
	leaq	(%r8,%r12,8), %rdx
	cmpq	%rax, %rdx
	jae	.LBB4_52
	.p2align	4, 0x90
.LBB4_33:                               #   in Loop: Header=BB4_32 Depth=4
	movq	%r14, %rdi
	movq	%rcx, %r12
	xorl	%eax, %eax
.LBB4_34:                               # %scalar.ph2151.preheader
                                        #   in Loop: Header=BB4_32 Depth=4
	movl	%r10d, %edx
	subl	%eax, %edx
	testb	$1, %dl
	jne	.LBB4_36
# BB#35:                                #   in Loop: Header=BB4_32 Depth=4
	movl	%eax, %ebp
	cmpl	%eax, 8(%rsp)           # 4-byte Folded Reload
	jne	.LBB4_38
	jmp	.LBB4_40
	.p2align	4, 0x90
.LBB4_36:                               # %scalar.ph2151.prol
                                        #   in Loop: Header=BB4_32 Depth=4
	movq	(%rsp), %rdx            # 8-byte Reload
	movsd	(%rdx,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	divsd	(%r8,%r12,8), %xmm0
	movsd	%xmm0, (%rdx,%rdi,8)
	movq	48(%rsp), %rdx          # 8-byte Reload
	addq	%rdx, %r12
	addq	%rdx, %rdi
	leal	1(%rax), %ebp
	cmpl	%eax, 8(%rsp)           # 4-byte Folded Reload
	je	.LBB4_40
.LBB4_38:                               # %scalar.ph2151.preheader.new
                                        #   in Loop: Header=BB4_32 Depth=4
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%rdi,8), %rax
	movq	136(%rsp), %rdx         # 8-byte Reload
	leaq	(%rax,%rdx), %rdi
	leaq	(%r8,%r12,8), %rbx
	leaq	(%rbx,%rdx), %rdx
	movl	%r10d, %esi
	subl	%ebp, %esi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_39:                               # %scalar.ph2151
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_26 Depth=2
                                        #       Parent Loop BB4_30 Depth=3
                                        #         Parent Loop BB4_32 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movsd	(%rax,%rbp), %xmm0      # xmm0 = mem[0],zero
	divsd	(%rbx,%rbp), %xmm0
	movsd	%xmm0, (%rax,%rbp)
	movsd	(%rdi,%rbp), %xmm0      # xmm0 = mem[0],zero
	divsd	(%rdx,%rbp), %xmm0
	movsd	%xmm0, (%rdi,%rbp)
	addq	%r13, %rbp
	addl	$-2, %esi
	jne	.LBB4_39
.LBB4_40:                               # %._crit_edge1760.us.us
                                        #   in Loop: Header=BB4_32 Depth=4
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	(%rcx,%rax), %ecx
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%r14,%rax), %eax
	incl	%r11d
	cmpl	%r15d, %r11d
	jne	.LBB4_32
# BB#41:                                #   in Loop: Header=BB4_30 Depth=3
	movl	272(%rsp), %eax         # 4-byte Reload
	movl	280(%rsp), %ecx         # 4-byte Reload
	movl	192(%rsp), %edx         # 4-byte Reload
.LBB4_42:                               # %._crit_edge1766.us
                                        #   in Loop: Header=BB4_30 Depth=3
	addl	56(%rsp), %ecx          # 4-byte Folded Reload
	addl	96(%rsp), %eax          # 4-byte Folded Reload
	addl	248(%rsp), %ecx         # 4-byte Folded Reload
	addl	184(%rsp), %eax         # 4-byte Folded Reload
	incl	%edx
	cmpl	176(%rsp), %edx         # 4-byte Folded Reload
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	movl	%eax, 96(%rsp)          # 4-byte Spill
	jne	.LBB4_30
.LBB4_43:                               # %._crit_edge1793
                                        #   in Loop: Header=BB4_26 Depth=2
	movq	264(%rsp), %r14         # 8-byte Reload
	incq	%r14
	movq	256(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %r14
	jl	.LBB4_26
.LBB4_44:                               # %._crit_edge1799
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	328(%rsp), %rax         # 8-byte Reload
	cmpq	584(%rsp), %rax         # 8-byte Folded Reload
	je	.LBB4_95
# BB#45:                                #   in Loop: Header=BB4_3 Depth=1
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	%rax, %rdx
	movq	(%rcx,%rdx,8), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	movq	%rax, 616(%rsp)         # 8-byte Spill
	leaq	1(%rdx), %rax
	movq	%rax, 600(%rsp)         # 8-byte Spill
	movq	8(%rcx,%rdx,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rcx
	movq	%rcx, 536(%rsp)         # 8-byte Spill
	movq	16(%rax), %rax
	movq	%rax, 608(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	320(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_46:                               #   Parent Loop BB4_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_64 Depth 3
                                        #         Child Loop BB4_65 Depth 4
                                        #         Child Loop BB4_68 Depth 4
                                        #           Child Loop BB4_72 Depth 5
                                        #             Child Loop BB4_74 Depth 6
                                        #               Child Loop BB4_86 Depth 7
                                        #               Child Loop BB4_77 Depth 7
	cmpl	$1, %eax
	movl	%eax, 500(%rsp)         # 4-byte Spill
	je	.LBB4_60
# BB#47:                                #   in Loop: Header=BB4_46 Depth=2
	testl	%eax, %eax
	jne	.LBB4_62
# BB#48:                                #   in Loop: Header=BB4_46 Depth=2
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	328(%rsp), %rbp         # 8-byte Reload
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	movq	528(%rsp), %rbx         # 8-byte Reload
	movq	(%rbx,%rbp,8), %rdi
	leaq	488(%rsp), %rdx
	callq	hypre_InitializeIndtComputations
	movq	(%rbx,%rbp,8), %rax
	addq	$8, %rax
	jmp	.LBB4_61
	.p2align	4, 0x90
.LBB4_60:                               #   in Loop: Header=BB4_46 Depth=2
	movq	488(%rsp), %rdi
	callq	hypre_FinalizeIndtComputations
	movq	528(%rsp), %rax         # 8-byte Reload
	movq	328(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	addq	$16, %rax
.LBB4_61:                               # %.sink.split
                                        #   in Loop: Header=BB4_46 Depth=2
	movq	(%rax), %rdi
.LBB4_62:                               #   in Loop: Header=BB4_46 Depth=2
	movq	536(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 8(%rax)
	jle	.LBB4_93
# BB#63:                                # %.preheader1638.lr.ph
                                        #   in Loop: Header=BB4_46 Depth=2
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	movq	%rdi, 320(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_64:                               # %.preheader1638
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_46 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_65 Depth 4
                                        #         Child Loop BB4_68 Depth 4
                                        #           Child Loop BB4_72 Depth 5
                                        #             Child Loop BB4_74 Depth 6
                                        #               Child Loop BB4_86 Depth 7
                                        #               Child Loop BB4_77 Depth 7
	movq	608(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%r9,4), %ecx
	movslq	%eax, %r8
	leaq	-1(%r8,%r8,2), %rbp
	movq	616(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r8,4), %rdx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_65:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_46 Depth=2
                                        #       Parent Loop BB4_64 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rbx, %r15
	leaq	1(%r15), %rbx
	addq	$3, %rbp
	cmpl	%ecx, (%rdx,%r15,4)
	jne	.LBB4_65
# BB#66:                                #   in Loop: Header=BB4_64 Depth=3
	movq	%rbx, 624(%rsp)         # 8-byte Spill
	leal	-1(%rax,%rbx), %esi
	movl	%esi, (%rsp)            # 4-byte Spill
	leaq	(,%r8,8), %rax
	addq	(%rdi), %rax
	movq	(%rax,%r15,8), %rax
	movq	%rax, 552(%rsp)         # 8-byte Spill
	movq	328(%rsp), %rbx         # 8-byte Reload
	movq	360(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	movq	40(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, 576(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %r10
	movq	16(%r10), %rcx
	movq	24(%r10), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	(%rcx), %r13
	movq	8(%rax,%rbx,8), %rcx
	movq	16(%rcx), %rdx
	movq	24(%rcx), %rbx
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	(%rdx), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%r8, 632(%rsp)          # 8-byte Spill
	leaq	-4(,%r8,4), %rbx
	movq	40(%r10), %rax
	addq	%rbx, %rax
	movslq	4(%rax,%r15,4), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	40(%rcx), %rax
	movq	%r9, 544(%rsp)          # 8-byte Spill
	movslq	(%rax,%r9,4), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 116(%rsp)
	movl	$0, 124(%rsp)
	leaq	116(%rsp), %rax
	movq	%rax, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	-4(%r13,%rbp,8), %eax
	movl	-16(%r13,%rbp,8), %ecx
	movl	%eax, %r14d
	subl	%ecx, %r14d
	incl	%r14d
	cmpl	%ecx, %eax
	movl	(%r13,%rbp,8), %eax
	movl	-12(%r13,%rbp,8), %ecx
	movl	$0, %edx
	cmovsl	%edx, %r14d
	xorl	%esi, %esi
	movl	%eax, %r12d
	subl	%ecx, %r12d
	incl	%r12d
	cmpl	%ecx, %eax
	movq	328(%rsp), %rdx         # 8-byte Reload
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	24(%rax), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	cmovsl	%esi, %r12d
	movq	40(%rax), %rax
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	addq	%rbx, %rax
	movslq	4(%rax,%r15,4), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movslq	116(%rsp), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	imull	124(%rsp), %r12d
	addl	120(%rsp), %r12d
	movq	$1, 116(%rsp)
	movl	$0, 124(%rsp)
	movq	360(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdx,8), %rdi
	movl	(%rsp), %esi            # 4-byte Reload
	leaq	116(%rsp), %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	552(%rsp), %rbx         # 8-byte Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	-4(%r13,%rbp,8), %ecx
	movl	-16(%r13,%rbp,8), %edx
	movl	%ecx, %eax
	subl	%edx, %eax
	incl	%eax
	cmpl	%edx, %ecx
	movl	(%r13,%rbp,8), %edx
	movq	%rbp, 392(%rsp)         # 8-byte Spill
	movq	%r13, 568(%rsp)         # 8-byte Spill
	movl	-12(%r13,%rbp,8), %esi
	movl	$0, %edi
	cmovsl	%edi, %eax
	movl	%edx, %ecx
	subl	%esi, %ecx
	incl	%ecx
	cmpl	%esi, %edx
	cmovsl	%edi, %ecx
	cmpl	$0, 8(%rbx)
	jle	.LBB4_92
# BB#67:                                # %.lr.ph1854
                                        #   in Loop: Header=BB4_64 Depth=3
	imull	%r14d, %r12d
	movslq	%r12d, %rdx
	addq	88(%rsp), %rdx          # 8-byte Folded Reload
	movq	168(%rsp), %rsi         # 8-byte Reload
	movq	328(%rsp), %rdi         # 8-byte Reload
	movq	(%rsi,%rdi,8), %rsi
	movq	64(%rsp), %rdi          # 8-byte Reload
	addq	40(%rsi), %rdi
	movslq	4(%rdi,%r15,4), %rdi
	imull	124(%rsp), %ecx
	addl	120(%rsp), %ecx
	imull	%eax, %ecx
	movslq	116(%rsp), %rax
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movq	544(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rax,2), %rax
	movq	16(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rax,8), %rax
	movq	%rax, 560(%rsp)         # 8-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	leaq	(%rax,%rbp,8), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	392(%rsp), %rax         # 8-byte Reload
	addq	%rax, %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	movq	24(%rsi), %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	(%rsi,%rbp,8), %rsi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	136(%rsp), %rsi         # 8-byte Reload
	leaq	8(%rsi), %rbp
	movq	%rbp, 648(%rsp)         # 8-byte Spill
	movq	80(%rsp), %rbp          # 8-byte Reload
	addq	%rdx, %rbp
	movq	96(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rbp,8), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rdx          # 8-byte Reload
	leaq	8(%rdx), %rbp
	movq	%rbp, 640(%rsp)         # 8-byte Spill
	addq	%rdi, %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	16(%rdx), %rax
	movq	%rax, 464(%rsp)         # 8-byte Spill
	leaq	16(%rsi), %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_68:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_46 Depth=2
                                        #       Parent Loop BB4_64 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB4_72 Depth 5
                                        #             Child Loop BB4_74 Depth 6
                                        #               Child Loop BB4_86 Depth 7
                                        #               Child Loop BB4_77 Depth 7
	movq	(%rbx), %rax
	movq	%rcx, 656(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rbp
	movl	(%rax,%rcx,8), %edx
	movl	%edx, 104(%rsp)
	movl	4(%rax,%rcx,8), %edx
	movl	%edx, 108(%rsp)
	movl	8(%rax,%rcx,8), %eax
	movl	%eax, 112(%rsp)
	movq	432(%rsp), %rdi         # 8-byte Reload
	leaq	308(%rsp), %rsi
	leaq	148(%rsp), %rbx
	movq	%rbx, %rdx
	leaq	340(%rsp), %rcx
	callq	hypre_StructMapFineToCoarse
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	368(%rsp), %rdx         # 8-byte Reload
	callq	hypre_BoxGetStrideSize
	movq	560(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %r15d
	movl	12(%rcx), %eax
	movl	%eax, %edx
	subl	%r15d, %edx
	incl	%edx
	cmpl	%r15d, %eax
	movl	4(%rcx), %r13d
	movl	16(%rcx), %ecx
	movl	$0, %esi
	cmovsl	%esi, %edx
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movl	%ecx, %r9d
	subl	%r13d, %r9d
	incl	%r9d
	cmpl	%r13d, %ecx
	movq	392(%rsp), %rdi         # 8-byte Reload
	movq	568(%rsp), %rdx         # 8-byte Reload
	movl	-16(%rdx,%rdi,4), %r8d
	movl	-4(%rdx,%rdi,4), %ecx
	cmovsl	%esi, %r9d
	movl	%ecx, %r12d
	subl	%r8d, %r12d
	incl	%r12d
	cmpl	%r8d, %ecx
	movl	-12(%rdx,%rdi,4), %r14d
	movl	(%rdx,%rdi,4), %ecx
	cmovsl	%esi, %r12d
	movl	%ecx, %ebx
	subl	%r14d, %ebx
	incl	%ebx
	cmpl	%r14d, %ecx
	movq	576(%rsp), %rbp         # 8-byte Reload
	movl	-16(%rbp,%rdi,4), %eax
	movl	-4(%rbp,%rdi,4), %ecx
	cmovsl	%esi, %ebx
	movl	%ecx, %edx
	subl	%eax, %edx
	incl	%edx
	cmpl	%eax, %ecx
	movl	-12(%rbp,%rdi,4), %r11d
	movl	(%rbp,%rdi,4), %ecx
	cmovsl	%esi, %edx
	movl	%ecx, %r10d
	subl	%r11d, %r10d
	incl	%r10d
	cmpl	%r11d, %ecx
	cmovsl	%esi, %r10d
	movl	236(%rsp), %ecx
	movl	240(%rsp), %esi
	cmpl	%ecx, %esi
	movq	%rcx, 88(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	cmovgel	%esi, %ecx
	movl	244(%rsp), %esi
	cmpl	%ecx, %esi
	movl	%esi, 376(%rsp)         # 4-byte Spill
	cmovgel	%esi, %ecx
	testl	%ecx, %ecx
	jle	.LBB4_91
# BB#69:                                # %.lr.ph1848
                                        #   in Loop: Header=BB4_68 Depth=4
	cmpl	$0, 376(%rsp)           # 4-byte Folded Reload
	jle	.LBB4_91
# BB#70:                                # %.lr.ph1848
                                        #   in Loop: Header=BB4_68 Depth=4
	cmpl	$0, 80(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_91
# BB#71:                                # %.preheader1635.us.preheader
                                        #   in Loop: Header=BB4_68 Depth=4
	movl	152(%rsp), %esi
	movl	156(%rsp), %ecx
	movl	%r12d, %edi
	imull	%ebx, %edi
	imull	%ecx, %edi
	movl	%edi, 416(%rsp)         # 4-byte Spill
	movl	%edx, %edi
	imull	%r10d, %edi
	imull	%ecx, %edi
	movl	%edi, 408(%rsp)         # 4-byte Spill
	movl	%esi, %ecx
	imull	%edx, %ecx
	movq	%rcx, %rbp
	imull	%r12d, %esi
	movl	%esi, 48(%rsp)          # 4-byte Spill
	movl	%eax, 96(%rsp)          # 4-byte Spill
	movl	%r8d, 72(%rsp)          # 4-byte Spill
	movslq	148(%rsp), %r8
	movl	%r8d, %edi
	negl	%edi
	imull	88(%rsp), %edi          # 4-byte Folded Reload
	movq	%rbp, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leal	(%rdi,%rax), %ebp
	addl	%esi, %edi
	movq	80(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %ecx
	imull	%ecx, %ebp
	addl	%eax, %ebp
	movl	%r8d, %esi
	movq	88(%rsp), %rax          # 8-byte Reload
	imull	%eax, %esi
	subl	%esi, %ebp
	movl	%ebp, 448(%rsp)         # 4-byte Spill
	imull	%ecx, %edi
	addl	48(%rsp), %edi          # 4-byte Folded Reload
	subl	%esi, %edi
	movq	%rdi, 456(%rsp)         # 8-byte Spill
	movl	%r11d, 128(%rsp)        # 4-byte Spill
	movl	8(%rsp), %r11d          # 4-byte Reload
	subl	%eax, %r11d
	imull	%ecx, %r11d
	movl	340(%rsp), %ecx
	subl	%r15d, %ecx
	movl	344(%rsp), %esi
	subl	%r13d, %esi
	movl	348(%rsp), %ebp
	movq	560(%rsp), %rdi         # 8-byte Reload
	subl	8(%rdi), %ebp
	imull	%r9d, %ebp
	addl	%esi, %ebp
	imull	8(%rsp), %ebp           # 4-byte Folded Reload
	addl	%ecx, %ebp
	movl	%ebp, 192(%rsp)         # 4-byte Spill
	movl	104(%rsp), %r13d
	movl	%r13d, %esi
	subl	72(%rsp), %esi          # 4-byte Folded Reload
	movl	108(%rsp), %edi
	movl	%edi, %ebp
	subl	%r14d, %ebp
	movl	112(%rsp), %r15d
	movl	%r15d, %eax
	movq	392(%rsp), %r14         # 8-byte Reload
	movq	568(%rsp), %rcx         # 8-byte Reload
	subl	-8(%rcx,%r14,4), %eax
	imull	%ebx, %eax
	addl	%ebp, %eax
	imull	%r12d, %eax
	addl	%esi, %eax
	movl	%eax, 176(%rsp)         # 4-byte Spill
	subl	96(%rsp), %r13d         # 4-byte Folded Reload
	subl	128(%rsp), %edi         # 4-byte Folded Reload
	movq	576(%rsp), %rsi         # 8-byte Reload
	subl	-8(%rsi,%r14,4), %r15d
	imull	%r10d, %r15d
	addl	%edi, %r15d
	movq	80(%rsp), %rsi          # 8-byte Reload
	movl	%esi, %eax
	imull	64(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 336(%rsp)         # 4-byte Spill
	subl	%eax, 408(%rsp)         # 4-byte Folded Spill
	imull	%edx, %r15d
	movl	%esi, %eax
	imull	48(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 508(%rsp)         # 4-byte Spill
	subl	%eax, 416(%rsp)         # 4-byte Folded Spill
	subl	%esi, %r9d
	movl	8(%rsp), %eax           # 4-byte Reload
	imull	%eax, %r9d
	movl	%r9d, 400(%rsp)         # 4-byte Spill
	addl	%eax, %r11d
	movq	88(%rsp), %rcx          # 8-byte Reload
	subl	%ecx, %r11d
	movl	%r11d, 440(%rsp)        # 4-byte Spill
	addl	%r13d, %r15d
	movl	%r15d, 184(%rsp)        # 4-byte Spill
	imull	%esi, %eax
	movl	%eax, 504(%rsp)         # 4-byte Spill
	leal	-1(%rcx), %eax
	leaq	1(%rax), %rdx
	movabsq	$8589934590, %rcx       # imm = 0x1FFFFFFFE
	leaq	-2(%rcx), %rsi
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	andq	%rdx, %rsi
	setne	%cl
	cmpl	$1, %r8d
	sete	%dl
	andb	%cl, %dl
	movb	%dl, 72(%rsp)           # 1-byte Spill
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	8(%rcx,%rax,8), %rcx
	movq	%rcx, 288(%rsp)         # 8-byte Spill
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	8(%rcx,%rax,8), %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movq	648(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 280(%rsp)         # 8-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	8(%rcx,%rax,8), %rcx
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	movq	640(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 264(%rsp)         # 8-byte Spill
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	8(%rcx,%rax,8), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	%r8, %rax
	movq	%rsi, 248(%rsp)         # 8-byte Spill
	imulq	%rsi, %rax
	movq	%rax, 480(%rsp)         # 8-byte Spill
	movq	%r8, %rsi
	shlq	$5, %rsi
	shlq	$3, %r8
	xorl	%edi, %edi
	movq	%rsi, 472(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_72:                               # %.preheader1635.us
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_46 Depth=2
                                        #       Parent Loop BB4_64 Depth=3
                                        #         Parent Loop BB4_68 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB4_74 Depth 6
                                        #               Child Loop BB4_86 Depth 7
                                        #               Child Loop BB4_77 Depth 7
	cmpl	$0, 88(%rsp)            # 4-byte Folded Reload
	movl	440(%rsp), %eax         # 4-byte Reload
	movq	456(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	448(%rsp), %edx         # 4-byte Reload
	jle	.LBB4_90
# BB#73:                                # %.preheader1634.us.us.preheader
                                        #   in Loop: Header=BB4_72 Depth=5
	movl	%edi, 384(%rsp)         # 4-byte Spill
	xorl	%r9d, %r9d
	movl	176(%rsp), %r12d        # 4-byte Reload
	movl	184(%rsp), %r15d        # 4-byte Reload
	movl	192(%rsp), %r11d        # 4-byte Reload
	.p2align	4, 0x90
.LBB4_74:                               # %.preheader1634.us.us
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_46 Depth=2
                                        #       Parent Loop BB4_64 Depth=3
                                        #         Parent Loop BB4_68 Depth=4
                                        #           Parent Loop BB4_72 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB4_86 Depth 7
                                        #               Child Loop BB4_77 Depth 7
	movslq	%r11d, %r13
	movslq	%r15d, %rbp
	movslq	%r12d, %r14
	cmpq	$3, 96(%rsp)            # 8-byte Folded Reload
	jbe	.LBB4_75
# BB#78:                                # %min.iters.checked
                                        #   in Loop: Header=BB4_74 Depth=6
	cmpb	$0, 72(%rsp)            # 1-byte Folded Reload
	je	.LBB4_75
# BB#79:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_74 Depth=6
	movq	%r14, 160(%rsp)         # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r9d, %eax
	addl	184(%rsp), %eax         # 4-byte Folded Reload
	movslq	%eax, %rbx
	movl	48(%rsp), %eax          # 4-byte Reload
	imull	%r9d, %eax
	addl	176(%rsp), %eax         # 4-byte Folded Reload
	movslq	%eax, %r14
	movl	8(%rsp), %eax           # 4-byte Reload
	imull	%r9d, %eax
	addl	192(%rsp), %eax         # 4-byte Folded Reload
	cltq
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rcx,%rax,8), %r10
	movq	288(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rbp
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,8), %rdx
	movq	208(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14,8), %rcx
	movq	136(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	280(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	cmpq	%rcx, %r10
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r14,8), %rdi
	sbbb	%cl, %cl
	cmpq	%rbp, %rdx
	sbbb	%sil, %sil
	andb	%cl, %sil
	movq	272(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r14,8), %rdx
	cmpq	%rax, %r10
	sbbb	%cl, %cl
	cmpq	%rbp, 128(%rsp)         # 8-byte Folded Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	sbbb	%al, %al
	movb	%al, 216(%rsp)          # 1-byte Spill
	cmpq	%rdx, %r10
	sbbb	%dl, %dl
	cmpq	%rbp, %rdi
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rbx,8), %rbx
	sbbb	%al, %al
	movb	%al, 296(%rsp)          # 1-byte Spill
	cmpq	%rbx, %r10
	sbbb	%bl, %bl
	cmpq	%rbp, 224(%rsp)         # 8-byte Folded Reload
	movq	256(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14,8), %rdi
	sbbb	%al, %al
	movb	%al, 224(%rsp)          # 1-byte Spill
	cmpq	%rdi, %r10
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,8), %rdi
	sbbb	%al, %al
	cmpq	%rbp, %rdi
	sbbb	%dil, %dil
	testb	$1, %sil
	jne	.LBB4_80
# BB#81:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_74 Depth=6
	andb	216(%rsp), %cl          # 1-byte Folded Reload
	andb	$1, %cl
	jne	.LBB4_80
# BB#82:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_74 Depth=6
	andb	296(%rsp), %dl          # 1-byte Folded Reload
	andb	$1, %dl
	jne	.LBB4_80
# BB#83:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_74 Depth=6
	andb	224(%rsp), %bl          # 1-byte Folded Reload
	andb	$1, %bl
	jne	.LBB4_80
# BB#84:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_74 Depth=6
	andb	%dil, %al
	andb	$1, %al
	jne	.LBB4_80
# BB#85:                                # %vector.body.preheader
                                        #   in Loop: Header=BB4_74 Depth=6
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	16(%rax,%r13,8), %rax
	movq	464(%rsp), %rcx         # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %r10
	movq	424(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	160(%rsp), %rdi         # 8-byte Reload
	leaq	16(%rcx,%rdi,8), %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	16(%rcx,%rdi,8), %rbp
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	16(%rcx,%rdi,8), %rbx
	movq	248(%rsp), %rcx         # 8-byte Reload
	addq	%rcx, %r13
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	480(%rsp), %rdi         # 8-byte Reload
	addq	%rdi, %r14
	addq	%rdi, 128(%rsp)         # 8-byte Folded Spill
	xorl	%edi, %edi
	movq	472(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_86:                               # %vector.body
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_46 Depth=2
                                        #       Parent Loop BB4_64 Depth=3
                                        #         Parent Loop BB4_68 Depth=4
                                        #           Parent Loop BB4_72 Depth=5
                                        #             Parent Loop BB4_74 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movupd	-16(%rbx,%rdi), %xmm0
	movupd	(%rbx,%rdi), %xmm1
	movupd	-16(%rsi,%rdi), %xmm2
	movupd	(%rsi,%rdi), %xmm3
	movupd	-16(%rbp,%rdi), %xmm4
	movupd	(%rbp,%rdi), %xmm5
	mulpd	%xmm2, %xmm4
	mulpd	%xmm3, %xmm5
	subpd	%xmm4, %xmm0
	subpd	%xmm5, %xmm1
	movupd	-16(%r10,%rdi), %xmm2
	movupd	(%r10,%rdi), %xmm3
	movupd	-16(%rdx,%rdi), %xmm4
	movupd	(%rdx,%rdi), %xmm5
	mulpd	%xmm2, %xmm4
	mulpd	%xmm3, %xmm5
	subpd	%xmm4, %xmm0
	subpd	%xmm5, %xmm1
	movupd	%xmm0, -16(%rax)
	movupd	%xmm1, (%rax)
	addq	$32, %rax
	addq	%r13, %rdi
	addq	$-4, %rcx
	jne	.LBB4_86
# BB#87:                                # %middle.block
                                        #   in Loop: Header=BB4_74 Depth=6
	movq	248(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, 96(%rsp)          # 8-byte Folded Reload
	movl	%eax, %ecx
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	128(%rsp), %rbp         # 8-byte Reload
	jne	.LBB4_76
	jmp	.LBB4_88
.LBB4_80:                               #   in Loop: Header=BB4_74 Depth=6
	movq	160(%rsp), %r14         # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_75:                               #   in Loop: Header=BB4_74 Depth=6
	xorl	%ecx, %ecx
.LBB4_76:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB4_74 Depth=6
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%r13,8), %rax
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r14,8), %r10
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r14,8), %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%r14,8), %rsi
	movq	136(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rbp,8), %rdi
	movq	56(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rbp,8), %rbp
	movq	88(%rsp), %rbx          # 8-byte Reload
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill>
	subl	%ecx, %ebx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_77:                               # %scalar.ph
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_46 Depth=2
                                        #       Parent Loop BB4_64 Depth=3
                                        #         Parent Loop BB4_68 Depth=4
                                        #           Parent Loop BB4_72 Depth=5
                                        #             Parent Loop BB4_74 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movsd	(%r10,%rcx), %xmm0      # xmm0 = mem[0],zero
	movsd	(%rdi,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%rdx,%rcx), %xmm1
	subsd	%xmm1, %xmm0
	movsd	(%rbp,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%rsi,%rcx), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rax)
	addq	$8, %rax
	addq	%r8, %rcx
	decl	%ebx
	jne	.LBB4_77
.LBB4_88:                               # %._crit_edge1807.us.us
                                        #   in Loop: Header=BB4_74 Depth=6
	addl	64(%rsp), %r15d         # 4-byte Folded Reload
	addl	48(%rsp), %r12d         # 4-byte Folded Reload
	addl	8(%rsp), %r11d          # 4-byte Folded Reload
	incl	%r9d
	cmpl	80(%rsp), %r9d          # 4-byte Folded Reload
	jne	.LBB4_74
# BB#89:                                #   in Loop: Header=BB4_72 Depth=5
	movl	504(%rsp), %eax         # 4-byte Reload
	movl	508(%rsp), %ecx         # 4-byte Reload
	movl	336(%rsp), %edx         # 4-byte Reload
	movl	384(%rsp), %edi         # 4-byte Reload
.LBB4_90:                               # %._crit_edge1815.us
                                        #   in Loop: Header=BB4_72 Depth=5
	addl	184(%rsp), %edx         # 4-byte Folded Reload
	addl	176(%rsp), %ecx         # 4-byte Folded Reload
	addl	192(%rsp), %eax         # 4-byte Folded Reload
	addl	408(%rsp), %edx         # 4-byte Folded Reload
	addl	416(%rsp), %ecx         # 4-byte Folded Reload
	addl	400(%rsp), %eax         # 4-byte Folded Reload
	incl	%edi
	cmpl	376(%rsp), %edi         # 4-byte Folded Reload
	movl	%edx, 184(%rsp)         # 4-byte Spill
	movl	%ecx, 176(%rsp)         # 4-byte Spill
	movl	%eax, 192(%rsp)         # 4-byte Spill
	jne	.LBB4_72
.LBB4_91:                               # %._crit_edge1849
                                        #   in Loop: Header=BB4_68 Depth=4
	movq	656(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movq	552(%rsp), %rbx         # 8-byte Reload
	movslq	8(%rbx), %rax
	cmpq	%rax, %rcx
	jl	.LBB4_68
.LBB4_92:                               # %._crit_edge1855
                                        #   in Loop: Header=BB4_64 Depth=3
	movq	632(%rsp), %rax         # 8-byte Reload
	movq	624(%rsp), %rcx         # 8-byte Reload
	leaq	-1(%rax,%rcx), %rax
	movq	544(%rsp), %r9          # 8-byte Reload
	incq	%r9
	movq	536(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rcx
	cmpq	%rcx, %r9
	movq	320(%rsp), %rdi         # 8-byte Reload
	jl	.LBB4_64
.LBB4_93:                               # %._crit_edge1862
                                        #   in Loop: Header=BB4_46 Depth=2
	movl	500(%rsp), %eax         # 4-byte Reload
	incl	%eax
	cmpl	$2, %eax
	jne	.LBB4_46
	jmp	.LBB4_94
.LBB4_95:
	movq	200(%rsp), %rax         # 8-byte Reload
	addq	$-2, %rax
	testl	%eax, %eax
	movq	320(%rsp), %rcx         # 8-byte Reload
	js	.LBB4_153
	.p2align	4, 0x90
.LBB4_96:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_101 Depth 2
                                        #       Child Loop BB4_102 Depth 3
                                        #       Child Loop BB4_107 Depth 3
                                        #         Child Loop BB4_109 Depth 4
                                        #           Child Loop BB4_111 Depth 5
                                        #           Child Loop BB4_115 Depth 5
                                        #     Child Loop BB4_121 Depth 2
                                        #       Child Loop BB4_128 Depth 3
                                        #         Child Loop BB4_130 Depth 4
                                        #           Child Loop BB4_134 Depth 5
                                        #             Child Loop BB4_136 Depth 6
                                        #               Child Loop BB4_144 Depth 7
                                        #               Child Loop BB4_139 Depth 7
	movq	%rax, 200(%rsp)         # 8-byte Spill
	testl	%eax, %eax
	movq	%rcx, 320(%rsp)         # 8-byte Spill
	je	.LBB4_98
# BB#97:                                #   in Loop: Header=BB4_96 Depth=1
	movl	$0, 308(%rsp)
	movl	$0, 312(%rsp)
	movl	$0, 316(%rsp)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 148(%rsp)
	movl	$1, %eax
	jmp	.LBB4_99
	.p2align	4, 0x90
.LBB4_98:                               #   in Loop: Header=BB4_96 Depth=1
	movq	352(%rsp), %rcx         # 8-byte Reload
	movl	12(%rcx), %eax
	movl	%eax, 308(%rsp)
	movl	16(%rcx), %eax
	movl	%eax, 312(%rsp)
	movl	20(%rcx), %eax
	movl	%eax, 316(%rsp)
	movl	24(%rcx), %eax
	movl	%eax, 148(%rsp)
	movl	28(%rcx), %eax
	movl	%eax, 152(%rsp)
	movl	32(%rcx), %eax
.LBB4_99:                               #   in Loop: Header=BB4_96 Depth=1
	movl	%eax, 156(%rsp)
	movq	512(%rsp), %rax         # 8-byte Reload
	shll	148(%rsp,%rax,4)
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	8(%rcx,%rax,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rsi
	cmpl	$0, 8(%rsi)
	jle	.LBB4_120
# BB#100:                               # %.preheader1632.lr.ph
                                        #   in Loop: Header=BB4_96 Depth=1
	movq	200(%rsp), %rcx         # 8-byte Reload
	movq	168(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%rcx,8), %rcx
	movq	8(%rcx), %rcx
	movq	16(%rcx), %rdi
	movq	16(%rax), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	movq	%rsi, 272(%rsp)         # 8-byte Spill
	movq	%rdi, 264(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_101:                              # %.preheader1632
                                        #   Parent Loop BB4_96 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_102 Depth 3
                                        #       Child Loop BB4_107 Depth 3
                                        #         Child Loop BB4_109 Depth 4
                                        #           Child Loop BB4_111 Depth 5
                                        #           Child Loop BB4_115 Depth 5
	movq	256(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rbx,4), %eax
	movslq	%ecx, %rcx
	leaq	(%rcx,%rcx,2), %rdx
	leaq	-1(%rcx), %rbp
	leaq	-2(%rdx,%rdx), %rcx
	.p2align	4, 0x90
.LBB4_102:                              #   Parent Loop BB4_96 Depth=1
                                        #     Parent Loop BB4_101 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbp, %r12
	movq	%rcx, %r14
	leaq	1(%r12), %rbp
	leaq	6(%r14), %rcx
	cmpl	%eax, 4(%rdi,%r12,4)
	jne	.LBB4_102
# BB#103:                               #   in Loop: Header=BB4_101 Depth=2
	movq	%rbp, 280(%rsp)         # 8-byte Spill
	movq	(%rsi), %rax
	leaq	(,%rbx,8), %rcx
	leaq	(%rcx,%rcx,2), %r15
	movl	(%rax,%r15), %ecx
	movl	%ecx, 340(%rsp)
	movl	4(%rax,%r15), %ecx
	movl	%ecx, 344(%rsp)
	movl	8(%rax,%r15), %ecx
	leaq	(%rax,%r15), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	%ecx, 348(%rsp)
	leaq	340(%rsp), %rdi
	leaq	308(%rsp), %rsi
	leaq	148(%rsp), %rdx
	movq	432(%rsp), %rcx         # 8-byte Reload
	callq	hypre_StructMapCoarseToFine
	movq	200(%rsp), %rdx         # 8-byte Reload
	movq	168(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rdx,8), %rax
	movq	16(%rax), %rcx
	movq	24(%rax), %rdi
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	(%rcx), %rbp
	movq	8(%rsi,%rdx,8), %rcx
	movq	16(%rcx), %rdx
	movq	24(%rcx), %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	(%rdx), %r13
	movq	40(%rax), %rax
	movslq	4(%rax,%r12,4), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	40(%rcx), %rax
	movq	%rbx, 208(%rsp)         # 8-byte Spill
	movslq	(%rax,%rbx,4), %r12
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	368(%rsp), %rsi         # 8-byte Reload
	callq	hypre_BoxGetSize
	movq	%rbp, %rbx
	movl	(%r13,%r15), %r9d
	movl	4(%r13,%r15), %r8d
	movl	12(%r13,%r15), %ecx
	movl	%ecx, %esi
	subl	%r9d, %esi
	incl	%esi
	cmpl	%r9d, %ecx
	movl	16(%r13,%r15), %edx
	movl	$0, %ebp
	cmovsl	%ebp, %esi
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	%edx, %ecx
	subl	%r8d, %ecx
	incl	%ecx
	cmpl	%r8d, %edx
	movl	8(%rbx,%r14,4), %esi
	movl	20(%rbx,%r14,4), %edx
	cmovsl	%ebp, %ecx
	movl	%edx, %r10d
	subl	%esi, %r10d
	incl	%r10d
	cmpl	%esi, %edx
	movl	12(%rbx,%r14,4), %r11d
	movl	24(%rbx,%r14,4), %edx
	cmovsl	%ebp, %r10d
	movl	%edx, %edi
	subl	%r11d, %edi
	incl	%edi
	cmpl	%r11d, %edx
	cmovsl	%ebp, %edi
	movl	236(%rsp), %edx
	movl	240(%rsp), %eax
	movl	244(%rsp), %ebp
	cmpl	%edx, %eax
	movq	%rdx, 56(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmovgel	%eax, %edx
	cmpl	%edx, %ebp
	movl	%ebp, 216(%rsp)         # 4-byte Spill
	cmovgel	%ebp, %edx
	testl	%edx, %edx
	jle	.LBB4_119
# BB#104:                               # %.lr.ph1681
                                        #   in Loop: Header=BB4_101 Depth=2
	cmpl	$0, 216(%rsp)           # 4-byte Folded Reload
	jle	.LBB4_119
# BB#105:                               # %.lr.ph1681
                                        #   in Loop: Header=BB4_101 Depth=2
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_119
# BB#106:                               # %.preheader1631.us.preheader
                                        #   in Loop: Header=BB4_101 Depth=2
	movl	348(%rsp), %ebp
	movq	208(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdx,2), %rdx
	subl	8(%r13,%rdx,8), %ebp
	movl	%r9d, 64(%rsp)          # 4-byte Spill
	movslq	148(%rsp), %r9
	movl	%esi, 136(%rsp)         # 4-byte Spill
	movl	112(%rsp), %esi
	subl	16(%rbx,%r14,4), %esi
	movl	152(%rsp), %edx
	imull	%r10d, %edx
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	%r12, (%rsp)            # 8-byte Spill
	movl	%r13d, %ebx
	imull	%r9d, %ebx
	movl	%edx, %eax
	subl	%ebx, %eax
	movq	16(%rsp), %r14          # 8-byte Reload
	movl	%r8d, 80(%rsp)          # 4-byte Spill
	movl	%r14d, %r8d
	imull	%edx, %r8d
	leal	-1(%r14), %r15d
	movl	%eax, 48(%rsp)          # 4-byte Spill
	imull	%r15d, %eax
	addl	%edx, %eax
	subl	%ebx, %eax
	movl	%eax, 296(%rsp)         # 4-byte Spill
	movl	8(%rsp), %r12d          # 4-byte Reload
	movl	%r12d, %ebx
	subl	%r13d, %ebx
	imull	%r15d, %ebx
	movl	340(%rsp), %edx
	subl	64(%rsp), %edx          # 4-byte Folded Reload
	movl	344(%rsp), %eax
	subl	80(%rsp), %eax          # 4-byte Folded Reload
	imull	%ecx, %ebp
	addl	%eax, %ebp
	imull	%r12d, %ebp
	addl	%edx, %ebp
	movl	104(%rsp), %r15d
	subl	136(%rsp), %r15d        # 4-byte Folded Reload
	movl	108(%rsp), %edx
	subl	%r11d, %edx
	movl	%r10d, %eax
	imull	%edi, %eax
	imull	%edi, %esi
	imull	156(%rsp), %eax
	movl	%r8d, 248(%rsp)         # 4-byte Spill
	subl	%r8d, %eax
	movl	%eax, 176(%rsp)         # 4-byte Spill
	subl	%r14d, %ecx
	imull	%r12d, %ecx
	movl	%ecx, 184(%rsp)         # 4-byte Spill
	addl	%r12d, %ebx
	subl	%r13d, %ebx
	movl	%ebx, 192(%rsp)         # 4-byte Spill
	addl	%edx, %esi
	leal	-1(%r13), %eax
	imull	%r10d, %esi
	movq	%rax, 64(%rsp)          # 8-byte Spill
	incq	%rax
	imulq	%r9, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	addl	%r15d, %esi
	imull	%r14d, %r12d
	movl	%r12d, 288(%rsp)        # 4-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%r13d, %r14d
	andl	$3, %r14d
	movq	%r9, %r12
	shlq	$5, %r12
	leaq	(%r9,%r9,2), %rax
	shlq	$3, %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%r9, %rax
	shlq	$4, %rax
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,8), %r11
	leaq	(,%rcx,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	(%rax,%r9,8), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_107:                              # %.preheader1631.us
                                        #   Parent Loop BB4_96 Depth=1
                                        #     Parent Loop BB4_101 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_109 Depth 4
                                        #           Child Loop BB4_111 Depth 5
                                        #           Child Loop BB4_115 Depth 5
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	movl	192(%rsp), %eax         # 4-byte Reload
	movl	296(%rsp), %edx         # 4-byte Reload
	jle	.LBB4_118
# BB#108:                               # %.preheader1630.us.us.preheader
                                        #   in Loop: Header=BB4_107 Depth=3
	movl	%ecx, 224(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movl	%ebp, 40(%rsp)          # 4-byte Spill
	movl	%ebp, %ecx
	movl	%esi, 160(%rsp)         # 4-byte Spill
	movl	%esi, %r10d
	.p2align	4, 0x90
.LBB4_109:                              # %.preheader1630.us.us
                                        #   Parent Loop BB4_96 Depth=1
                                        #     Parent Loop BB4_101 Depth=2
                                        #       Parent Loop BB4_107 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB4_111 Depth 5
                                        #           Child Loop BB4_115 Depth 5
	movl	%eax, (%rsp)            # 4-byte Spill
	movslq	%r10d, %r10
	testl	%r14d, %r14d
	movslq	%ecx, %rcx
	movq	%rcx, %rax
	movq	%r10, %r13
	movl	$0, %r15d
	je	.LBB4_113
# BB#110:                               # %.prol.preheader
                                        #   in Loop: Header=BB4_109 Depth=4
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	xorl	%r15d, %r15d
	movq	%r10, %r13
	.p2align	4, 0x90
.LBB4_111:                              #   Parent Loop BB4_96 Depth=1
                                        #     Parent Loop BB4_101 Depth=2
                                        #       Parent Loop BB4_107 Depth=3
                                        #         Parent Loop BB4_109 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%rax,%r15,8), %rdx
	movq	%rdx, (%r11,%r13,8)
	addq	%r9, %r13
	incq	%r15
	cmpl	%r15d, %r14d
	jne	.LBB4_111
# BB#112:                               # %.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB4_109 Depth=4
	leaq	(%rcx,%r15), %rax
.LBB4_113:                              # %.prol.loopexit
                                        #   in Loop: Header=BB4_109 Depth=4
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	addq	136(%rsp), %r10         # 8-byte Folded Reload
	cmpl	$3, 64(%rsp)            # 4-byte Folded Reload
	jb	.LBB4_116
# BB#114:                               # %.preheader1630.us.us.new
                                        #   in Loop: Header=BB4_109 Depth=4
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	24(%rcx,%rax,8), %rax
	movq	72(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r13,8), %rdx
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r13,8), %rbx
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r13,8), %r8
	movq	128(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r13,8), %rbp
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %edi
	subl	%r15d, %edi
	movq	96(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_115:                              #   Parent Loop BB4_96 Depth=1
                                        #     Parent Loop BB4_101 Depth=2
                                        #       Parent Loop BB4_107 Depth=3
                                        #         Parent Loop BB4_109 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	-24(%rax), %rcx
	movq	%rcx, (%rdx,%rsi)
	movq	-16(%rax), %rcx
	movq	%rcx, (%rbp,%rsi)
	movq	-8(%rax), %rcx
	movq	%rcx, (%r8,%rsi)
	movq	(%rax), %rcx
	movq	%rcx, (%rbx,%rsi)
	addq	$32, %rax
	addq	%r12, %rsi
	addl	$-4, %edi
	jne	.LBB4_115
.LBB4_116:                              # %._crit_edge.us.us
                                        #   in Loop: Header=BB4_109 Depth=4
	addl	48(%rsp), %r10d         # 4-byte Folded Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	addl	8(%rsp), %ecx           # 4-byte Folded Reload
	movl	(%rsp), %eax            # 4-byte Reload
	incl	%eax
	cmpl	16(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB4_109
# BB#117:                               #   in Loop: Header=BB4_107 Depth=3
	movl	288(%rsp), %eax         # 4-byte Reload
	movl	248(%rsp), %edx         # 4-byte Reload
	movl	40(%rsp), %ebp          # 4-byte Reload
	movl	160(%rsp), %esi         # 4-byte Reload
	movl	224(%rsp), %ecx         # 4-byte Reload
.LBB4_118:                              # %._crit_edge1655.us
                                        #   in Loop: Header=BB4_107 Depth=3
	addl	%esi, %edx
	addl	%ebp, %eax
	addl	176(%rsp), %edx         # 4-byte Folded Reload
	addl	184(%rsp), %eax         # 4-byte Folded Reload
	incl	%ecx
	cmpl	216(%rsp), %ecx         # 4-byte Folded Reload
	movl	%edx, %esi
	movl	%eax, %ebp
	jne	.LBB4_107
.LBB4_119:                              # %._crit_edge1682
                                        #   in Loop: Header=BB4_101 Depth=2
	movq	208(%rsp), %rbx         # 8-byte Reload
	incq	%rbx
	movq	272(%rsp), %rsi         # 8-byte Reload
	movslq	8(%rsi), %rax
	cmpq	%rax, %rbx
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	jl	.LBB4_101
.LBB4_120:                              # %.preheader1633
                                        #   in Loop: Header=BB4_96 Depth=1
	xorl	%eax, %eax
	movq	320(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_121:                              #   Parent Loop BB4_96 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_128 Depth 3
                                        #         Child Loop BB4_130 Depth 4
                                        #           Child Loop BB4_134 Depth 5
                                        #             Child Loop BB4_136 Depth 6
                                        #               Child Loop BB4_144 Depth 7
                                        #               Child Loop BB4_139 Depth 7
	cmpl	$1, %eax
	movl	%eax, 336(%rsp)         # 4-byte Spill
	je	.LBB4_124
# BB#122:                               #   in Loop: Header=BB4_121 Depth=2
	testl	%eax, %eax
	jne	.LBB4_126
# BB#123:                               #   in Loop: Header=BB4_121 Depth=2
	movq	200(%rsp), %rbx         # 8-byte Reload
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rsi
	movq	520(%rsp), %rbp         # 8-byte Reload
	movq	(%rbp,%rbx,8), %rdi
	leaq	488(%rsp), %rdx
	callq	hypre_InitializeIndtComputations
	movq	(%rbp,%rbx,8), %rax
	addq	$8, %rax
	jmp	.LBB4_125
	.p2align	4, 0x90
.LBB4_124:                              #   in Loop: Header=BB4_121 Depth=2
	movq	488(%rsp), %rdi
	callq	hypre_FinalizeIndtComputations
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	520(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	addq	$16, %rax
.LBB4_125:                              # %.sink.split1581
                                        #   in Loop: Header=BB4_121 Depth=2
	movq	(%rax), %rcx
.LBB4_126:                              #   in Loop: Header=BB4_121 Depth=2
	cmpl	$0, 8(%rcx)
	jle	.LBB4_151
# BB#127:                               # %.lr.ph1739
                                        #   in Loop: Header=BB4_121 Depth=2
	xorl	%r15d, %r15d
	movq	%rcx, 320(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_128:                              #   Parent Loop BB4_96 Depth=1
                                        #     Parent Loop BB4_121 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_130 Depth 4
                                        #           Child Loop BB4_134 Depth 5
                                        #             Child Loop BB4_136 Depth 6
                                        #               Child Loop BB4_144 Depth 7
                                        #               Child Loop BB4_139 Depth 7
	movq	(%rcx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%rax, 376(%rsp)         # 8-byte Spill
	movq	200(%rsp), %rbp         # 8-byte Reload
	movq	360(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	movq	%rax, %r14
	movq	40(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rbp,8), %rax
	movq	%rcx, %rbx
	movq	16(%rax), %rax
	movq	(%rax), %r12
	movq	$0, 116(%rsp)
	movl	$0, 124(%rsp)
	movl	%r15d, %esi
	leaq	116(%rsp), %r13
	movq	%r13, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	(%rbx,%rbp,8), %rax
	movq	24(%rax), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	40(%rax), %rax
	movslq	(%rax,%r15,4), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 116(%rsp)
	movl	$0, 124(%rsp)
	movq	(%r14,%rbp,8), %rdi
	movl	%r15d, %esi
	movq	%r13, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%r15,%r15,2), %r13
	movq	%r12, %rbx
	movl	12(%rbx,%r13,8), %eax
	movl	(%rbx,%r13,8), %ecx
	movl	%eax, %r12d
	subl	%ecx, %r12d
	incl	%r12d
	cmpl	%ecx, %eax
	movl	16(%rbx,%r13,8), %eax
	movl	4(%rbx,%r13,8), %ecx
	movl	$0, %edx
	cmovsl	%edx, %r12d
	xorl	%edx, %edx
	movl	%eax, %r14d
	subl	%ecx, %r14d
	incl	%r14d
	cmpl	%ecx, %eax
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	40(%rax), %rax
	movslq	(%rax,%r15,4), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movslq	116(%rsp), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	cmovsl	%edx, %r14d
	imull	124(%rsp), %r14d
	addl	120(%rsp), %r14d
	movq	$1, 116(%rsp)
	movl	$0, 124(%rsp)
	movq	360(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	movq	%r15, 400(%rsp)         # 8-byte Spill
	movl	%r15d, %esi
	leaq	116(%rsp), %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	376(%rsp), %rbp         # 8-byte Reload
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	12(%rbx,%r13,8), %ecx
	movl	(%rbx,%r13,8), %edx
	movl	%ecx, %eax
	subl	%edx, %eax
	incl	%eax
	cmpl	%edx, %ecx
	movl	16(%rbx,%r13,8), %edx
	movl	4(%rbx,%r13,8), %esi
	movl	$0, %edi
	cmovsl	%edi, %eax
	movl	%edx, %ecx
	subl	%esi, %ecx
	incl	%ecx
	cmpl	%esi, %edx
	cmovsl	%edi, %ecx
	cmpl	$0, 8(%rbp)
	jle	.LBB4_150
# BB#129:                               # %.lr.ph1731
                                        #   in Loop: Header=BB4_128 Depth=3
	imull	%r12d, %r14d
	leaq	16(%rbx,%r13,8), %rdx
	movq	%rdx, 416(%rsp)         # 8-byte Spill
	movslq	%r14d, %r8
	addq	136(%rsp), %r8          # 8-byte Folded Reload
	movslq	116(%rsp), %rsi
	imull	124(%rsp), %ecx
	addl	120(%rsp), %ecx
	imull	%eax, %ecx
	movslq	%ecx, %rax
	addq	%rsi, %rax
	movq	400(%rsp), %rdi         # 8-byte Reload
	leaq	(,%rdi,8), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movq	384(%rsp), %rsi         # 8-byte Reload
	leaq	4(%rsi,%rcx), %rdx
	movq	%rdx, 424(%rsp)         # 8-byte Spill
	addq	%rcx, %rsi
	movq	%rsi, 384(%rsp)         # 8-byte Spill
	leaq	8(%rbx,%rcx), %rcx
	movq	%rcx, 408(%rsp)         # 8-byte Spill
	movq	200(%rsp), %rcx         # 8-byte Reload
	movq	168(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rcx,8), %rcx
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rdx,%rsi,8), %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	24(%rcx), %rsi
	movq	40(%rcx), %rcx
	movslq	(%rcx,%rdi,4), %rcx
	movq	64(%rsp), %rdi          # 8-byte Reload
	addq	%r8, %rdi
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	8(%rdx), %rdx
	movq	%rdx, 456(%rsp)         # 8-byte Spill
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rdx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	8(%rdx), %rdx
	movq	%rdx, 448(%rsp)         # 8-byte Spill
	addq	%rcx, %rax
	leaq	(%rsi,%rax,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rax
	movq	%rax, 440(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_130:                              #   Parent Loop BB4_96 Depth=1
                                        #     Parent Loop BB4_121 Depth=2
                                        #       Parent Loop BB4_128 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB4_134 Depth 5
                                        #             Child Loop BB4_136 Depth 6
                                        #               Child Loop BB4_144 Depth 7
                                        #               Child Loop BB4_139 Depth 7
	movq	(%rbp), %rax
	movq	%rcx, 464(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rdi
	movl	(%rax,%rcx,8), %edx
	movl	%edx, 104(%rsp)
	movl	4(%rax,%rcx,8), %edx
	movl	%edx, 108(%rsp)
	movl	8(%rax,%rcx,8), %eax
	movl	%eax, 112(%rsp)
	leaq	148(%rsp), %rsi
	movq	368(%rsp), %rdx         # 8-byte Reload
	callq	hypre_BoxGetStrideSize
	movq	416(%rsp), %rcx         # 8-byte Reload
	movl	-16(%rcx), %r14d
	movl	-4(%rcx), %eax
	movl	%eax, %r8d
	subl	%r14d, %r8d
	incl	%r8d
	cmpl	%r14d, %eax
	movl	-12(%rcx), %r10d
	movl	(%rcx), %eax
	movl	$0, %edx
	cmovsl	%edx, %r8d
	movl	%eax, %ebx
	subl	%r10d, %ebx
	incl	%ebx
	cmpl	%r10d, %eax
	movq	384(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %edi
	movq	424(%rsp), %rsi         # 8-byte Reload
	movl	8(%rsi), %ecx
	cmovsl	%edx, %ebx
	movl	%ecx, %ebp
	subl	%edi, %ebp
	incl	%ebp
	cmpl	%edi, %ecx
	movl	(%rsi), %r11d
	movl	12(%rsi), %ecx
	cmovsl	%edx, %ebp
	movl	%ecx, %r9d
	subl	%r11d, %r9d
	incl	%r9d
	cmpl	%r11d, %ecx
	cmovsl	%edx, %r9d
	movl	236(%rsp), %eax
	movl	240(%rsp), %edx
	movl	244(%rsp), %esi
	cmpl	%eax, %edx
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	%eax, %ecx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	cmovgel	%edx, %ecx
	cmpl	%ecx, %esi
	movl	%esi, 208(%rsp)         # 4-byte Spill
	cmovgel	%esi, %ecx
	testl	%ecx, %ecx
	jle	.LBB4_149
# BB#131:                               # %.lr.ph1725
                                        #   in Loop: Header=BB4_130 Depth=4
	cmpl	$0, 208(%rsp)           # 4-byte Folded Reload
	jle	.LBB4_149
# BB#132:                               # %.lr.ph1725
                                        #   in Loop: Header=BB4_130 Depth=4
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_149
# BB#133:                               # %.preheader1629.us.preheader
                                        #   in Loop: Header=BB4_130 Depth=4
	movl	%edi, 64(%rsp)          # 4-byte Spill
	movl	%r10d, 72(%rsp)         # 4-byte Spill
	movslq	148(%rsp), %r10
	movl	152(%rsp), %edx
	movl	156(%rsp), %ecx
	movl	%r8d, %eax
	imull	%edx, %eax
	movl	%eax, %esi
	imull	%ebp, %edx
	movq	%rdx, %rdi
	movl	%r8d, %r12d
	imull	%ebx, %r12d
	imull	%ecx, %r12d
	movl	%r11d, 88(%rsp)         # 4-byte Spill
	movl	%ebp, %r11d
	imull	%r9d, %r11d
	imull	%ecx, %r11d
	movl	%r10d, %eax
	negl	%eax
	movq	80(%rsp), %rcx          # 8-byte Reload
	imull	%ecx, %eax
	movq	%rcx, %r15
	leal	(%rax,%rdi), %edx
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	addl	%esi, %eax
	movl	%esi, 48(%rsp)          # 4-byte Spill
	movq	96(%rsp), %r13          # 8-byte Reload
	leal	-1(%r13), %ecx
	imull	%ecx, %edx
	imull	%ecx, %eax
	addl	%edi, %edx
	movl	%r10d, %ecx
	imull	%r15d, %ecx
	subl	%ecx, %edx
	movl	%edx, 256(%rsp)         # 4-byte Spill
	addl	%esi, %eax
	subl	%ecx, %eax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movl	104(%rsp), %r15d
	movl	%r15d, %edx
	subl	%r14d, %edx
	movl	108(%rsp), %esi
	movl	%esi, %edi
	subl	72(%rsp), %edi          # 4-byte Folded Reload
	movl	112(%rsp), %ecx
	movl	%ecx, %r14d
	movq	408(%rsp), %rax         # 8-byte Reload
	subl	(%rax), %r14d
	imull	%ebx, %r14d
	addl	%edi, %r14d
	imull	%r8d, %r14d
	addl	%edx, %r14d
	movl	%r14d, 72(%rsp)         # 4-byte Spill
	subl	64(%rsp), %r15d         # 4-byte Folded Reload
	movl	%r13d, %eax
	imull	8(%rsp), %eax           # 4-byte Folded Reload
	movl	%eax, 480(%rsp)         # 4-byte Spill
	subl	%eax, %r11d
	movl	%r11d, 272(%rsp)        # 4-byte Spill
	subl	88(%rsp), %esi          # 4-byte Folded Reload
	movl	%r13d, %eax
	imull	48(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 472(%rsp)         # 4-byte Spill
	subl	%eax, %r12d
	movl	%r12d, 280(%rsp)        # 4-byte Spill
	movq	424(%rsp), %rax         # 8-byte Reload
	subl	4(%rax), %ecx
	imull	%r9d, %ecx
	movq	80(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %eax
	addl	%esi, %ecx
	imull	%ebp, %ecx
	addl	%r15d, %ecx
	movl	%ecx, 128(%rsp)         # 4-byte Spill
	leaq	1(%rax), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	movabsq	$8589934590, %rcx       # imm = 0x1FFFFFFFE
	andq	%rcx, %rdi
	setne	%cl
	cmpl	$1, %r10d
	sete	%dl
	andb	%cl, %dl
	movb	%dl, 88(%rsp)           # 1-byte Spill
	movq	136(%rsp), %rsi         # 8-byte Reload
	leaq	8(%rsi,%rax,8), %rcx
	movq	(%rsp), %rdx            # 8-byte Reload
	cmpq	%rcx, %rdx
	sbbb	%cl, %cl
	leaq	8(%rdx,%rax,8), %rbp
	cmpq	%rbp, %rsi
	sbbb	%bl, %bl
	andb	%cl, %bl
	andb	$1, %bl
	movb	%bl, 224(%rsp)          # 1-byte Spill
	movq	56(%rsp), %rsi          # 8-byte Reload
	leaq	8(%rsi,%rax,8), %rcx
	cmpq	%rcx, %rdx
	sbbb	%cl, %cl
	movq	%rbp, 160(%rsp)         # 8-byte Spill
	cmpq	%rbp, %rsi
	sbbb	%dl, %dl
	andb	%cl, %dl
	movq	456(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 296(%rsp)         # 8-byte Spill
	movq	448(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	movq	440(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	andb	$1, %dl
	movb	%dl, 216(%rsp)          # 1-byte Spill
	movq	%r10, %rax
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	imulq	%rdi, %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	%r10, %r13
	shlq	$4, %r13
	shlq	$3, %r10
	xorl	%edx, %edx
	movq	%r13, 248(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_134:                              # %.preheader1629.us
                                        #   Parent Loop BB4_96 Depth=1
                                        #     Parent Loop BB4_121 Depth=2
                                        #       Parent Loop BB4_128 Depth=3
                                        #         Parent Loop BB4_130 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB4_136 Depth 6
                                        #               Child Loop BB4_144 Depth 7
                                        #               Child Loop BB4_139 Depth 7
	cmpl	$0, 80(%rsp)            # 4-byte Folded Reload
	movq	264(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	256(%rsp), %ecx         # 4-byte Reload
	jle	.LBB4_148
# BB#135:                               # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB4_134 Depth=5
	movl	%edx, 288(%rsp)         # 4-byte Spill
	xorl	%r8d, %r8d
	movl	72(%rsp), %r11d         # 4-byte Reload
	movl	128(%rsp), %r9d         # 4-byte Reload
	.p2align	4, 0x90
.LBB4_136:                              # %.preheader.us.us
                                        #   Parent Loop BB4_96 Depth=1
                                        #     Parent Loop BB4_121 Depth=2
                                        #       Parent Loop BB4_128 Depth=3
                                        #         Parent Loop BB4_130 Depth=4
                                        #           Parent Loop BB4_134 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB4_144 Depth 7
                                        #               Child Loop BB4_139 Depth 7
	movslq	%r9d, %rax
	movslq	%r11d, %r13
	cmpq	$1, 64(%rsp)            # 8-byte Folded Reload
	jbe	.LBB4_137
# BB#140:                               # %min.iters.checked2212
                                        #   in Loop: Header=BB4_136 Depth=6
	cmpb	$0, 88(%rsp)            # 1-byte Folded Reload
	je	.LBB4_137
# BB#141:                               # %vector.memcheck2282
                                        #   in Loop: Header=BB4_136 Depth=6
	movq	8(%rsp), %rcx           # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	imull	%r8d, %ecx
	addl	128(%rsp), %ecx         # 4-byte Folded Reload
	movslq	%ecx, %r12
	movl	48(%rsp), %ecx          # 4-byte Reload
	imull	%r8d, %ecx
	addl	72(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %r14
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rcx,%r14,8), %rcx
	movq	160(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r14,8), %rbp
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r12,8), %rsi
	movq	296(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r12,8), %rdi
	cmpq	%rdi, %rcx
	sbbb	%dil, %dil
	cmpq	%rbp, %rsi
	movq	192(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r12,8), %rsi
	sbbb	%bl, %bl
	andb	%dil, %bl
	andb	$1, %bl
	orb	224(%rsp), %bl          # 1-byte Folded Reload
	cmpq	%rsi, %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r12,8), %rsi
	sbbb	%dil, %dil
	cmpq	%rbp, %rsi
	sbbb	%dl, %dl
	andb	%dil, %dl
	andb	$1, %dl
	orb	%bl, %dl
	movq	176(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r12,8), %rsi
	cmpq	%rsi, %rcx
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r12,8), %rsi
	sbbb	%cl, %cl
	cmpq	%rbp, %rsi
	sbbb	%bl, %bl
	orb	216(%rsp), %dl          # 1-byte Folded Reload
	jne	.LBB4_137
# BB#142:                               # %vector.memcheck2282
                                        #   in Loop: Header=BB4_136 Depth=6
	andb	%bl, %cl
	andb	$1, %cl
	jne	.LBB4_137
# BB#143:                               # %vector.body2208.preheader
                                        #   in Loop: Header=BB4_136 Depth=6
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %r15
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %rdi
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%r13,8), %rdx
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r13,8), %rax
	movq	136(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r13,8), %rbp
	movq	184(%rsp), %rsi         # 8-byte Reload
	addq	%rsi, %r14
	addq	%rsi, %r12
	movq	40(%rsp), %rsi          # 8-byte Reload
	xorl	%ebx, %ebx
	movq	248(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_144:                              # %vector.body2208
                                        #   Parent Loop BB4_96 Depth=1
                                        #     Parent Loop BB4_121 Depth=2
                                        #       Parent Loop BB4_128 Depth=3
                                        #         Parent Loop BB4_130 Depth=4
                                        #           Parent Loop BB4_134 Depth=5
                                        #             Parent Loop BB4_136 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movupd	(%rdi,%rbx), %xmm0
	movupd	(%rbp,%rbx), %xmm1
	mulpd	%xmm0, %xmm1
	movupd	(%rcx,%rbx), %xmm0
	movupd	(%rax,%rbx), %xmm2
	mulpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	movupd	(%r15,%rbx), %xmm0
	divpd	%xmm0, %xmm2
	movupd	(%rdx,%rbx), %xmm0
	subpd	%xmm2, %xmm0
	movupd	%xmm0, (%rdx,%rbx)
	addq	%r13, %rbx
	addq	$-2, %rsi
	jne	.LBB4_144
# BB#145:                               # %middle.block2209
                                        #   in Loop: Header=BB4_136 Depth=6
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, 64(%rsp)          # 8-byte Folded Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	jne	.LBB4_138
	jmp	.LBB4_146
	.p2align	4, 0x90
.LBB4_137:                              #   in Loop: Header=BB4_136 Depth=6
	movq	%r13, %r14
	movq	%rax, %r12
	xorl	%eax, %eax
.LBB4_138:                              # %scalar.ph2210.preheader
                                        #   in Loop: Header=BB4_136 Depth=6
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r14,8), %r15
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r14,8), %rcx
	movq	(%rsp), %rdx            # 8-byte Reload
	leaq	(%rdx,%r14,8), %rdx
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%r12,8), %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%r12,8), %rbp
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%r12,8), %rbx
	movq	80(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	subl	%eax, %esi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_139:                              # %scalar.ph2210
                                        #   Parent Loop BB4_96 Depth=1
                                        #     Parent Loop BB4_121 Depth=2
                                        #       Parent Loop BB4_128 Depth=3
                                        #         Parent Loop BB4_130 Depth=4
                                        #           Parent Loop BB4_134 Depth=5
                                        #             Parent Loop BB4_136 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movsd	(%rdi,%rax), %xmm0      # xmm0 = mem[0],zero
	mulsd	(%r15,%rax), %xmm0
	movsd	(%rbp,%rax), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%rcx,%rax), %xmm1
	addsd	%xmm0, %xmm1
	divsd	(%rbx,%rax), %xmm1
	movsd	(%rdx,%rax), %xmm0      # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rdx,%rax)
	addq	%r10, %rax
	decl	%esi
	jne	.LBB4_139
.LBB4_146:                              # %._crit_edge1693.us.us
                                        #   in Loop: Header=BB4_136 Depth=6
	addl	8(%rsp), %r9d           # 4-byte Folded Reload
	addl	48(%rsp), %r11d         # 4-byte Folded Reload
	incl	%r8d
	cmpl	96(%rsp), %r8d          # 4-byte Folded Reload
	jne	.LBB4_136
# BB#147:                               #   in Loop: Header=BB4_134 Depth=5
	movl	472(%rsp), %eax         # 4-byte Reload
	movl	480(%rsp), %ecx         # 4-byte Reload
	movl	288(%rsp), %edx         # 4-byte Reload
.LBB4_148:                              # %._crit_edge1699.us
                                        #   in Loop: Header=BB4_134 Depth=5
	addl	128(%rsp), %ecx         # 4-byte Folded Reload
	addl	72(%rsp), %eax          # 4-byte Folded Reload
	addl	272(%rsp), %ecx         # 4-byte Folded Reload
	addl	280(%rsp), %eax         # 4-byte Folded Reload
	incl	%edx
	cmpl	208(%rsp), %edx         # 4-byte Folded Reload
	movl	%ecx, 128(%rsp)         # 4-byte Spill
	movl	%eax, 72(%rsp)          # 4-byte Spill
	jne	.LBB4_134
.LBB4_149:                              # %._crit_edge1726
                                        #   in Loop: Header=BB4_130 Depth=4
	movq	464(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movq	376(%rsp), %rbp         # 8-byte Reload
	movslq	8(%rbp), %rax
	cmpq	%rax, %rcx
	jl	.LBB4_130
.LBB4_150:                              # %._crit_edge1732
                                        #   in Loop: Header=BB4_128 Depth=3
	movq	400(%rsp), %r15         # 8-byte Reload
	incq	%r15
	movq	320(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %r15
	jl	.LBB4_128
.LBB4_151:                              # %._crit_edge1740
                                        #   in Loop: Header=BB4_121 Depth=2
	movl	336(%rsp), %eax         # 4-byte Reload
	incl	%eax
	cmpl	$2, %eax
	jne	.LBB4_121
# BB#152:                               #   in Loop: Header=BB4_96 Depth=1
	movq	200(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	leaq	-1(%rax), %rax
	jg	.LBB4_96
.LBB4_153:                              # %._crit_edge1755
	movq	352(%rsp), %rbx         # 8-byte Reload
	movl	108(%rbx), %edi
	callq	hypre_IncFLOPCount
	movl	104(%rbx), %edi
	callq	hypre_EndTiming
	xorl	%eax, %eax
	addq	$664, %rsp              # imm = 0x298
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	hypre_CyclicReduction, .Lfunc_end4-hypre_CyclicReduction
	.cfi_endproc

	.globl	hypre_CyclicReductionSetBase
	.p2align	4, 0x90
	.type	hypre_CyclicReductionSetBase,@function
hypre_CyclicReductionSetBase:           # @hypre_CyclicReductionSetBase
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	%eax, 12(%rdi)
	movl	(%rdx), %eax
	movl	%eax, 24(%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 16(%rdi)
	movl	4(%rdx), %eax
	movl	%eax, 28(%rdi)
	movl	8(%rsi), %eax
	movl	%eax, 20(%rdi)
	movl	8(%rdx), %eax
	movl	%eax, 32(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	hypre_CyclicReductionSetBase, .Lfunc_end5-hypre_CyclicReductionSetBase
	.cfi_endproc

	.globl	hypre_CyclicReductionDestroy
	.p2align	4, 0x90
	.type	hypre_CyclicReductionDestroy,@function
hypre_CyclicReductionDestroy:           # @hypre_CyclicReductionDestroy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 48
.Lcfi86:
	.cfi_offset %rbx, -40
.Lcfi87:
	.cfi_offset %r12, -32
.Lcfi88:
	.cfi_offset %r14, -24
.Lcfi89:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB6_7
# BB#1:
	movq	48(%r14), %rdi
	callq	hypre_BoxArrayDestroy
	movq	40(%r14), %rax
	movq	(%rax), %rdi
	callq	hypre_StructGridDestroy
	movq	72(%r14), %rax
	movq	(%rax), %rdi
	callq	hypre_StructMatrixDestroy
	movq	80(%r14), %rax
	movq	(%rax), %rdi
	callq	hypre_StructVectorDestroy
	cmpl	$2, 4(%r14)
	jl	.LBB6_2
# BB#3:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	movabsq	$4294967296, %r15       # imm = 0x100000000
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%r14), %rax
	movq	8(%rax,%rbx,8), %rdi
	callq	hypre_StructGridDestroy
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_BoxArrayDestroy
	movq	72(%r14), %rax
	movq	8(%rax,%rbx,8), %rdi
	callq	hypre_StructMatrixDestroy
	movq	80(%r14), %rax
	movq	8(%rax,%rbx,8), %rdi
	callq	hypre_StructVectorDestroy
	movq	88(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_ComputePkgDestroy
	movq	96(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	incq	%rbx
	callq	hypre_ComputePkgDestroy
	movslq	4(%r14), %rax
	decq	%rax
	addq	%r15, %r12
	cmpq	%rax, %rbx
	jl	.LBB6_4
# BB#5:                                 # %._crit_edge.loopexit
	sarq	$32, %r12
	jmp	.LBB6_6
.LBB6_2:
	xorl	%r12d, %r12d
.LBB6_6:                                # %._crit_edge
	movq	56(%r14), %rax
	movq	(%rax,%r12,8), %rdi
	callq	hypre_BoxArrayDestroy
	movq	64(%r14), %rdi
	callq	hypre_Free
	movq	$0, 64(%r14)
	movq	40(%r14), %rdi
	callq	hypre_Free
	movq	$0, 40(%r14)
	movq	56(%r14), %rdi
	callq	hypre_Free
	movq	$0, 56(%r14)
	movq	72(%r14), %rdi
	callq	hypre_Free
	movq	$0, 72(%r14)
	movq	80(%r14), %rdi
	callq	hypre_Free
	movq	$0, 80(%r14)
	movq	88(%r14), %rdi
	callq	hypre_Free
	movq	$0, 88(%r14)
	movq	96(%r14), %rdi
	callq	hypre_Free
	movq	$0, 96(%r14)
	movl	104(%r14), %edi
	callq	hypre_FinalizeTiming
	movq	%r14, %rdi
	callq	hypre_Free
.LBB6_7:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	hypre_CyclicReductionDestroy, .Lfunc_end6-hypre_CyclicReductionDestroy
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"CyclicReduction"
	.size	.L.str, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
