	.text
	.file	"btSoftBodyConcaveCollisionAlgorithm.bc"
	.globl	_ZN35btSoftBodyConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	.p2align	4, 0x90
	.type	_ZN35btSoftBodyConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b,@function
_ZN35btSoftBodyConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b: # @_ZN35btSoftBodyConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	callq	_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	movq	$_ZTV35btSoftBodyConcaveCollisionAlgorithm+16, (%rbx)
	movb	%r13b, 16(%rbx)
	leaq	24(%rbx), %r14
	movq	(%rbp), %rax
	movq	$_ZTV26btSoftBodyTriangleCallback+16, 24(%rbx)
	movq	%rax, 88(%rbx)
	movq	$0, 96(%rbx)
	movb	$1, 136(%rbx)
	movq	$0, 128(%rbx)
	movl	$0, 116(%rbx)
	movl	$0, 120(%rbx)
	movb	$1, 168(%rbx)
	movq	$0, 160(%rbx)
	movl	$0, 148(%rbx)
	movl	$0, 152(%rbx)
	movb	$1, 200(%rbx)
	movq	$0, 192(%rbx)
	movl	$0, 180(%rbx)
	movl	$0, 184(%rbx)
	movb	$1, 232(%rbx)
	movq	$0, 224(%rbx)
	movl	$0, 212(%rbx)
	movl	$0, 216(%rbx)
	testb	%r13b, %r13b
	movq	%r12, %rax
	cmovneq	%r15, %rax
	movq	%rax, 32(%rbx)
	cmovneq	%r12, %r15
	movq	%r15, 40(%rbx)
.Ltmp0:
	movq	%r14, %rdi
	callq	_ZN26btSoftBodyTriangleCallback10clearCacheEv
.Ltmp1:
# BB#1:                                 # %_ZN26btSoftBodyTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_2:
.Ltmp2:
	movq	%rax, %rbp
	addq	$112, %rbx
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev
.Ltmp4:
# BB#3:
.Ltmp5:
	movq	%r14, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp6:
# BB#4:                                 # %.body
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB0_5:
.Ltmp7:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN35btSoftBodyConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b, .Lfunc_end0-_ZN35btSoftBodyConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN35btSoftBodyConcaveCollisionAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN35btSoftBodyConcaveCollisionAlgorithmD2Ev,@function
_ZN35btSoftBodyConcaveCollisionAlgorithmD2Ev: # @_ZN35btSoftBodyConcaveCollisionAlgorithmD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	$_ZTV35btSoftBodyConcaveCollisionAlgorithm+16, (%rbx)
	leaq	24(%rbx), %r15
	movq	$_ZTV26btSoftBodyTriangleCallback+16, 24(%rbx)
.Ltmp8:
	movq	%r15, %rdi
	callq	_ZN26btSoftBodyTriangleCallback10clearCacheEv
.Ltmp9:
# BB#1:
	addq	$112, %rbx
.Ltmp13:
	movq	%rbx, %rdi
	callq	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev
.Ltmp14:
# BB#2:                                 # %_ZN26btSoftBodyTriangleCallbackD2Ev.exit
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN18btTriangleCallbackD2Ev # TAILCALL
.LBB2_4:
.Ltmp15:
	movq	%rax, %r14
	jmp	.LBB2_5
.LBB2_3:
.Ltmp10:
	movq	%rax, %r14
	addq	$112, %rbx
.Ltmp11:
	movq	%rbx, %rdi
	callq	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev
.Ltmp12:
.LBB2_5:
.Ltmp16:
	movq	%r15, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp17:
# BB#6:                                 # %.body
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_7:
.Ltmp18:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN35btSoftBodyConcaveCollisionAlgorithmD2Ev, .Lfunc_end2-_ZN35btSoftBodyConcaveCollisionAlgorithmD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin1   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin1   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp11-.Ltmp14         #   Call between .Ltmp14 and .Ltmp11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp17-.Ltmp11         #   Call between .Ltmp11 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	1                       #   On action: 1
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end2-.Ltmp17     #   Call between .Ltmp17 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN35btSoftBodyConcaveCollisionAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN35btSoftBodyConcaveCollisionAlgorithmD0Ev,@function
_ZN35btSoftBodyConcaveCollisionAlgorithmD0Ev: # @_ZN35btSoftBodyConcaveCollisionAlgorithmD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	$_ZTV35btSoftBodyConcaveCollisionAlgorithm+16, (%rbx)
	leaq	24(%rbx), %r14
	movq	$_ZTV26btSoftBodyTriangleCallback+16, 24(%rbx)
.Ltmp19:
	movq	%r14, %rdi
	callq	_ZN26btSoftBodyTriangleCallback10clearCacheEv
.Ltmp20:
# BB#1:
	leaq	112(%rbx), %rdi
.Ltmp24:
	callq	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev
.Ltmp25:
# BB#2:                                 # %_ZN26btSoftBodyTriangleCallbackD2Ev.exit.i
.Ltmp30:
	movq	%r14, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp31:
# BB#3:                                 # %_ZN35btSoftBodyConcaveCollisionAlgorithmD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdlPv                  # TAILCALL
.LBB3_8:
.Ltmp32:
	movq	%rax, %r15
	jmp	.LBB3_9
.LBB3_5:
.Ltmp26:
	movq	%rax, %r15
	jmp	.LBB3_6
.LBB3_4:
.Ltmp21:
	movq	%rax, %r15
	leaq	112(%rbx), %rdi
.Ltmp22:
	callq	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev
.Ltmp23:
.LBB3_6:
.Ltmp27:
	movq	%r14, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp28:
.LBB3_9:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB3_7:
.Ltmp29:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN35btSoftBodyConcaveCollisionAlgorithmD0Ev, .Lfunc_end3-_ZN35btSoftBodyConcaveCollisionAlgorithmD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin2   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin2   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin2   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp28-.Ltmp22         #   Call between .Ltmp22 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin2   #     jumps to .Ltmp29
	.byte	1                       #   On action: 1
	.long	.Ltmp28-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Lfunc_end3-.Ltmp28     #   Call between .Ltmp28 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN26btSoftBodyTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b
	.p2align	4, 0x90
	.type	_ZN26btSoftBodyTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b,@function
_ZN26btSoftBodyTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b: # @_ZN26btSoftBodyTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 32
.Lcfi28:
	.cfi_offset %rbx, -24
.Lcfi29:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV26btSoftBodyTriangleCallback+16, (%rbx)
	movq	%rsi, 64(%rbx)
	movq	$0, 72(%rbx)
	movb	$1, 112(%rbx)
	movq	$0, 104(%rbx)
	movl	$0, 92(%rbx)
	movl	$0, 96(%rbx)
	movb	$1, 144(%rbx)
	movq	$0, 136(%rbx)
	movl	$0, 124(%rbx)
	movl	$0, 128(%rbx)
	movb	$1, 176(%rbx)
	movq	$0, 168(%rbx)
	movl	$0, 156(%rbx)
	movl	$0, 160(%rbx)
	movb	$1, 208(%rbx)
	movq	$0, 200(%rbx)
	movl	$0, 188(%rbx)
	movl	$0, 192(%rbx)
	testb	%r8b, %r8b
	movq	%rdx, %rax
	cmovneq	%rcx, %rax
	movq	%rax, 8(%rbx)
	cmovneq	%rdx, %rcx
	movq	%rcx, 16(%rbx)
.Ltmp33:
	callq	_ZN26btSoftBodyTriangleCallback10clearCacheEv
.Ltmp34:
# BB#1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_2:
.Ltmp35:
	movq	%rax, %r14
	leaq	88(%rbx), %rdi
.Ltmp36:
	callq	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev
.Ltmp37:
# BB#3:
.Ltmp38:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp39:
# BB#4:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_5:
.Ltmp40:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN26btSoftBodyTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b, .Lfunc_end4-_ZN26btSoftBodyTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp33-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin3   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp39-.Ltmp36         #   Call between .Ltmp36 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin3   #     jumps to .Ltmp40
	.byte	1                       #   On action: 1
	.long	.Ltmp39-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Lfunc_end4-.Ltmp39     #   Call between .Ltmp39 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN26btSoftBodyTriangleCallback10clearCacheEv
	.p2align	4, 0x90
	.type	_ZN26btSoftBodyTriangleCallback10clearCacheEv,@function
_ZN26btSoftBodyTriangleCallback10clearCacheEv: # @_ZN26btSoftBodyTriangleCallback10clearCacheEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 80
.Lcfi37:
	.cfi_offset %rbx, -56
.Lcfi38:
	.cfi_offset %r12, -48
.Lcfi39:
	.cfi_offset %r13, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, 156(%r14)
	jle	.LBB5_15
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	movq	%r14, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB5_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_4 Depth 2
                                        #       Child Loop BB5_7 Depth 3
                                        #         Child Loop BB5_8 Depth 4
	movq	8(%r14), %rcx
	movq	168(%r14), %rax
	movq	776(%rcx), %r12
	movq	%rbp, %rcx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rbx
	cmpl	$0, 68(%r12)
	jle	.LBB5_12
# BB#3:                                 # %.lr.ph45.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	leaq	8(%rax,%rcx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_4:                                #   Parent Loop BB5_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_7 Depth 3
                                        #         Child Loop BB5_8 Depth 4
	movq	80(%r12), %rax
	movq	(%rax,%rbp,8), %r14
	testq	%r14, %r14
	je	.LBB5_10
# BB#5:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB5_4 Depth=2
	leaq	(%rax,%rbp,8), %r13
	xorl	%edi, %edi
.LBB5_7:                                # %.lr.ph.i
                                        #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB5_8 Depth 4
	testq	%rdi, %rdi
	leaq	288(%rdi), %r15
	cmoveq	%r13, %r15
	movq	%r14, %rdi
	.p2align	4, 0x90
.LBB5_8:                                #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_4 Depth=2
                                        #       Parent Loop BB5_7 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	288(%rdi), %r14
	cmpq	%rbx, 280(%rdi)
	jne	.LBB5_6
# BB#9:                                 #   in Loop: Header=BB5_8 Depth=4
	movq	%r14, (%r15)
	callq	_ZdlPv
	testq	%r14, %r14
	movq	%r14, %rdi
	jne	.LBB5_8
	jmp	.LBB5_10
	.p2align	4, 0x90
.LBB5_6:                                # %.outer.loopexit.i
                                        #   in Loop: Header=BB5_7 Depth=3
	testq	%r14, %r14
	jne	.LBB5_7
	.p2align	4, 0x90
.LBB5_10:                               # %.outer._crit_edge.i
                                        #   in Loop: Header=BB5_4 Depth=2
	incq	%rbp
	movslq	68(%r12), %rax
	cmpq	%rax, %rbp
	jl	.LBB5_4
# BB#11:                                # %_ZN11btSparseSdfILi3EE16RemoveReferencesEP16btCollisionShape.exitthread-pre-split
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rbx
	movq	(%rsp), %r14            # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB5_12:                               # %_ZN11btSparseSdfILi3EE16RemoveReferencesEP16btCollisionShape.exit
                                        #   in Loop: Header=BB5_2 Depth=1
	testq	%rbx, %rbx
	je	.LBB5_14
# BB#13:                                #   in Loop: Header=BB5_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB5_14:                               #   in Loop: Header=BB5_2 Depth=1
	incq	%rbp
	movslq	156(%r14), %rax
	cmpq	%rax, %rbp
	jl	.LBB5_2
.LBB5_15:                               # %._crit_edge
	addq	$88, %r14
	movq	%r14, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E5clearEv # TAILCALL
.Lfunc_end5:
	.size	_ZN26btSoftBodyTriangleCallback10clearCacheEv, .Lfunc_end5-_ZN26btSoftBodyTriangleCallback10clearCacheEv
	.cfi_endproc

	.section	.text._ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev,"axG",@progbits,_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev,comdat
	.weak	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev
	.p2align	4, 0x90
	.type	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev,@function
_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev: # @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 32
.Lcfi46:
	.cfi_offset %rbx, -24
.Lcfi47:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_4
# BB#1:
	cmpb	$0, 120(%rbx)
	je	.LBB6_3
# BB#2:
.Ltmp41:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp42:
.LBB6_3:                                # %.noexc
	movq	$0, 112(%rbx)
.LBB6_4:
	movb	$1, 120(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 100(%rbx)
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_8
# BB#5:
	cmpb	$0, 88(%rbx)
	je	.LBB6_7
# BB#6:
.Ltmp46:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp47:
.LBB6_7:                                # %.noexc5
	movq	$0, 80(%rbx)
.LBB6_8:
	movb	$1, 88(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 68(%rbx)
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_12
# BB#9:
	cmpb	$0, 56(%rbx)
	je	.LBB6_11
# BB#10:
.Ltmp51:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp52:
.LBB6_11:                               # %.noexc7
	movq	$0, 48(%rbx)
.LBB6_12:
	movb	$1, 56(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 36(%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_16
# BB#13:
	cmpb	$0, 24(%rbx)
	je	.LBB6_15
# BB#14:
	callq	_Z21btAlignedFreeInternalPv
.LBB6_15:
	movq	$0, 16(%rbx)
.LBB6_16:                               # %_ZN20btAlignedObjectArrayIiED2Ev.exit9
	movb	$1, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 4(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB6_22:
.Ltmp53:
	movq	%rax, %r14
	jmp	.LBB6_29
.LBB6_23:
.Ltmp48:
	movq	%rax, %r14
	jmp	.LBB6_24
.LBB6_17:
.Ltmp43:
	movq	%rax, %r14
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_21
# BB#18:
	cmpb	$0, 88(%rbx)
	je	.LBB6_20
# BB#19:
.Ltmp44:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp45:
.LBB6_20:                               # %.noexc11
	movq	$0, 80(%rbx)
.LBB6_21:                               # %_ZN20btAlignedObjectArrayI10btTriIndexED2Ev.exit12
	movb	$1, 88(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 68(%rbx)
.LBB6_24:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_28
# BB#25:
	cmpb	$0, 56(%rbx)
	je	.LBB6_27
# BB#26:
.Ltmp49:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp50:
.LBB6_27:                               # %.noexc14
	movq	$0, 48(%rbx)
.LBB6_28:                               # %_ZN20btAlignedObjectArrayIiED2Ev.exit15
	movb	$1, 56(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 36(%rbx)
.LBB6_29:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_33
# BB#30:
	cmpb	$0, 24(%rbx)
	je	.LBB6_32
# BB#31:
.Ltmp54:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp55:
.LBB6_32:                               # %.noexc17
	movq	$0, 16(%rbx)
.LBB6_33:
	movb	$1, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 4(%rbx)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_34:
.Ltmp56:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev, .Lfunc_end6-_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp41-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin4   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin4   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin4   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp44-.Ltmp52         #   Call between .Ltmp52 and .Ltmp44
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Ltmp55-.Ltmp44         #   Call between .Ltmp44 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin4   #     jumps to .Ltmp56
	.byte	1                       #   On action: 1
	.long	.Ltmp55-.Lfunc_begin4   # >> Call Site 6 <<
	.long	.Lfunc_end6-.Ltmp55     #   Call between .Ltmp55 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN26btSoftBodyTriangleCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN26btSoftBodyTriangleCallbackD2Ev,@function
_ZN26btSoftBodyTriangleCallbackD2Ev:    # @_ZN26btSoftBodyTriangleCallbackD2Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 32
.Lcfi51:
	.cfi_offset %rbx, -24
.Lcfi52:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV26btSoftBodyTriangleCallback+16, (%rbx)
.Ltmp57:
	callq	_ZN26btSoftBodyTriangleCallback10clearCacheEv
.Ltmp58:
# BB#1:
	leaq	88(%rbx), %rdi
.Ltmp62:
	callq	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev
.Ltmp63:
# BB#2:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN18btTriangleCallbackD2Ev # TAILCALL
.LBB7_4:
.Ltmp64:
	movq	%rax, %r14
	jmp	.LBB7_5
.LBB7_3:
.Ltmp59:
	movq	%rax, %r14
	leaq	88(%rbx), %rdi
.Ltmp60:
	callq	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev
.Ltmp61:
.LBB7_5:
.Ltmp65:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp66:
# BB#6:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_7:
.Ltmp67:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN26btSoftBodyTriangleCallbackD2Ev, .Lfunc_end7-_ZN26btSoftBodyTriangleCallbackD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp57-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin5   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin5   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp60-.Ltmp63         #   Call between .Ltmp63 and .Ltmp60
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Ltmp66-.Ltmp60         #   Call between .Ltmp60 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin5   #     jumps to .Ltmp67
	.byte	1                       #   On action: 1
	.long	.Ltmp66-.Lfunc_begin5   # >> Call Site 5 <<
	.long	.Lfunc_end7-.Ltmp66     #   Call between .Ltmp66 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN26btSoftBodyTriangleCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN26btSoftBodyTriangleCallbackD0Ev,@function
_ZN26btSoftBodyTriangleCallbackD0Ev:    # @_ZN26btSoftBodyTriangleCallbackD0Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 32
.Lcfi56:
	.cfi_offset %rbx, -24
.Lcfi57:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV26btSoftBodyTriangleCallback+16, (%rbx)
.Ltmp68:
	callq	_ZN26btSoftBodyTriangleCallback10clearCacheEv
.Ltmp69:
# BB#1:
	leaq	88(%rbx), %rdi
.Ltmp73:
	callq	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev
.Ltmp74:
# BB#2:
.Ltmp79:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp80:
# BB#3:                                 # %_ZN26btSoftBodyTriangleCallbackD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB8_8:
.Ltmp81:
	movq	%rax, %r14
	jmp	.LBB8_9
.LBB8_5:
.Ltmp75:
	movq	%rax, %r14
	jmp	.LBB8_6
.LBB8_4:
.Ltmp70:
	movq	%rax, %r14
	leaq	88(%rbx), %rdi
.Ltmp71:
	callq	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev
.Ltmp72:
.LBB8_6:
.Ltmp76:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp77:
.LBB8_9:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_7:
.Ltmp78:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN26btSoftBodyTriangleCallbackD0Ev, .Lfunc_end8-_ZN26btSoftBodyTriangleCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp68-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin6   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin6   #     jumps to .Ltmp75
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin6   #     jumps to .Ltmp81
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin6   # >> Call Site 4 <<
	.long	.Ltmp77-.Ltmp71         #   Call between .Ltmp71 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin6   #     jumps to .Ltmp78
	.byte	1                       #   On action: 1
	.long	.Ltmp77-.Lfunc_begin6   # >> Call Site 5 <<
	.long	.Lfunc_end8-.Ltmp77     #   Call between .Ltmp77 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9btHashMapI9btHashKeyI10btTriIndexES1_E5clearEv,"axG",@progbits,_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E5clearEv,comdat
	.weak	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E5clearEv
	.p2align	4, 0x90
	.type	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E5clearEv,@function
_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E5clearEv: # @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E5clearEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 16
.Lcfi59:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_4
# BB#1:
	cmpb	$0, 24(%rbx)
	je	.LBB9_3
# BB#2:
	callq	_Z21btAlignedFreeInternalPv
.LBB9_3:
	movq	$0, 16(%rbx)
.LBB9_4:                                # %_ZN20btAlignedObjectArrayIiE5clearEv.exit
	movb	$1, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 4(%rbx)
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_8
# BB#5:
	cmpb	$0, 56(%rbx)
	je	.LBB9_7
# BB#6:
	callq	_Z21btAlignedFreeInternalPv
.LBB9_7:
	movq	$0, 48(%rbx)
.LBB9_8:                                # %_ZN20btAlignedObjectArrayIiE5clearEv.exit2
	movb	$1, 56(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 36(%rbx)
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_12
# BB#9:
	cmpb	$0, 88(%rbx)
	je	.LBB9_11
# BB#10:
	callq	_Z21btAlignedFreeInternalPv
.LBB9_11:
	movq	$0, 80(%rbx)
.LBB9_12:                               # %_ZN20btAlignedObjectArrayI10btTriIndexE5clearEv.exit
	movb	$1, 88(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 68(%rbx)
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_16
# BB#13:
	cmpb	$0, 120(%rbx)
	je	.LBB9_15
# BB#14:
	callq	_Z21btAlignedFreeInternalPv
.LBB9_15:
	movq	$0, 112(%rbx)
.LBB9_16:                               # %_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE5clearEv.exit
	movb	$1, 120(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 100(%rbx)
	popq	%rbx
	retq
.Lfunc_end9:
	.size	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E5clearEv, .Lfunc_end9-_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E5clearEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_0:
	.long	1065353216              # float 1
.LCPI10_2:
	.long	1031127695              # float 0.0599999987
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_1:
	.long	1031127695              # float 0.0599999987
	.long	1031127695              # float 0.0599999987
	.zero	4
	.zero	4
	.text
	.globl	_ZN26btSoftBodyTriangleCallback15processTriangleEP9btVector3ii
	.p2align	4, 0x90
	.type	_ZN26btSoftBodyTriangleCallback15processTriangleEP9btVector3ii,@function
_ZN26btSoftBodyTriangleCallback15processTriangleEP9btVector3ii: # @_ZN26btSoftBodyTriangleCallback15processTriangleEP9btVector3ii
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi64:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi66:
	.cfi_def_cfa_offset 224
.Lcfi67:
	.cfi_offset %rbx, -56
.Lcfi68:
	.cfi_offset %r12, -48
.Lcfi69:
	.cfi_offset %r13, -40
.Lcfi70:
	.cfi_offset %r14, -32
.Lcfi71:
	.cfi_offset %r15, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movl	%edx, %ebp
	movq	%rsi, %r15
	movq	%rdi, %r13
	movq	16(%r13), %rbx
	movq	64(%r13), %r14
	movq	72(%r13), %rax
	testq	%rax, %rax
	je	.LBB10_4
# BB#1:
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB10_4
# BB#2:
	movq	(%rdi), %rax
	callq	*96(%rax)
	testb	$1, %al
	je	.LBB10_4
# BB#3:
	movabsq	$4863606123715821568, %rax # imm = 0x437F0000437F0000
	movq	%rax, 48(%rsp)
	movq	$0, 56(%rsp)
	movq	72(%r13), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movss	(%r15), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%r15), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1]
	movaps	%xmm7, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movss	28(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm2    # xmm2 = xmm2[0],xmm9[0],xmm2[1],xmm9[1]
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	addps	%xmm3, %xmm1
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm4
	addps	%xmm1, %xmm4
	movsd	56(%rbx), %xmm10        # xmm10 = mem[0],zero
	addps	%xmm10, %xmm4
	movss	40(%rbx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm7
	movss	44(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	addss	%xmm7, %xmm0
	movss	48(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm5
	addss	%xmm0, %xmm5
	movss	64(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm5
	xorps	%xmm1, %xmm1
	movss	%xmm5, %xmm1            # xmm1 = xmm5[0],xmm1[1,2,3]
	movlps	%xmm4, (%rsp)
	movlps	%xmm1, 8(%rsp)
	movss	16(%r15), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	20(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1]
	movaps	%xmm7, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	unpcklps	%xmm9, %xmm6    # xmm6 = xmm6[0],xmm9[0],xmm6[1],xmm9[1]
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm6, %xmm3
	addps	%xmm5, %xmm3
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm2, %xmm5
	addps	%xmm3, %xmm5
	addps	%xmm10, %xmm5
	mulss	%xmm11, %xmm7
	mulss	%xmm12, %xmm1
	addss	%xmm7, %xmm1
	mulss	%xmm13, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, 16(%rsp)
	movlps	%xmm0, 24(%rsp)
	movq	%rsp, %rsi
	leaq	16(%rsp), %rdx
	movq	%r14, 32(%rsp)          # 8-byte Spill
	leaq	48(%rsp), %r14
	movq	%r14, %rcx
	callq	*%rax
	movq	72(%r13), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movss	16(%r15), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	20(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1]
	movaps	%xmm7, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movss	28(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm2    # xmm2 = xmm2[0],xmm9[0],xmm2[1],xmm9[1]
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	addps	%xmm3, %xmm1
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm4
	addps	%xmm1, %xmm4
	movsd	56(%rbx), %xmm10        # xmm10 = mem[0],zero
	addps	%xmm10, %xmm4
	movss	40(%rbx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm7
	movss	44(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	addss	%xmm7, %xmm0
	movss	48(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm5
	addss	%xmm0, %xmm5
	movss	64(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm5
	xorps	%xmm1, %xmm1
	movss	%xmm5, %xmm1            # xmm1 = xmm5[0],xmm1[1,2,3]
	movlps	%xmm4, (%rsp)
	movlps	%xmm1, 8(%rsp)
	movss	32(%r15), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	36(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	40(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1]
	movaps	%xmm7, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	unpcklps	%xmm9, %xmm6    # xmm6 = xmm6[0],xmm9[0],xmm6[1],xmm9[1]
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm6, %xmm3
	addps	%xmm5, %xmm3
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm2, %xmm5
	addps	%xmm3, %xmm5
	addps	%xmm10, %xmm5
	mulss	%xmm11, %xmm7
	mulss	%xmm12, %xmm1
	addss	%xmm7, %xmm1
	mulss	%xmm13, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, 16(%rsp)
	movlps	%xmm0, 24(%rsp)
	movq	%rsp, %rsi
	leaq	16(%rsp), %rdx
	movq	%r14, %rcx
	callq	*%rax
	movq	72(%r13), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movss	32(%r15), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	36(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	40(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1]
	movaps	%xmm7, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movss	28(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm2    # xmm2 = xmm2[0],xmm9[0],xmm2[1],xmm9[1]
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	addps	%xmm3, %xmm1
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm4
	addps	%xmm1, %xmm4
	movsd	56(%rbx), %xmm10        # xmm10 = mem[0],zero
	addps	%xmm10, %xmm4
	movss	40(%rbx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm7
	movss	44(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	addss	%xmm7, %xmm0
	movss	48(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm5
	addss	%xmm0, %xmm5
	movss	64(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm5
	xorps	%xmm1, %xmm1
	movss	%xmm5, %xmm1            # xmm1 = xmm5[0],xmm1[1,2,3]
	movlps	%xmm4, (%rsp)
	movlps	%xmm1, 8(%rsp)
	movss	(%r15), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%r15), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1]
	movaps	%xmm7, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	unpcklps	%xmm9, %xmm6    # xmm6 = xmm6[0],xmm9[0],xmm6[1],xmm9[1]
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm6, %xmm3
	addps	%xmm5, %xmm3
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm2, %xmm5
	addps	%xmm3, %xmm5
	addps	%xmm10, %xmm5
	mulss	%xmm11, %xmm7
	mulss	%xmm12, %xmm1
	addss	%xmm7, %xmm1
	mulss	%xmm13, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, 16(%rsp)
	movlps	%xmm0, 24(%rsp)
	movq	%rsp, %rsi
	leaq	16(%rsp), %rdx
	movq	%r14, %rcx
	movq	32(%rsp), %r14          # 8-byte Reload
	callq	*%rax
.LBB10_4:
	shll	$21, %ebp
	orl	%r12d, %ebp
	movl	%ebp, (%rsp)
	movq	$0, 8(%rsp)
	movl	%ebp, 16(%rsp)
	shll	$15, %r12d
	notl	%r12d
	addl	%ebp, %r12d
	movl	%r12d, %eax
	sarl	$10, %eax
	xorl	%r12d, %eax
	leal	(%rax,%rax,8), %eax
	movl	%eax, %ecx
	sarl	$6, %ecx
	xorl	%eax, %ecx
	movl	%ecx, %eax
	shll	$11, %eax
	notl	%eax
	addl	%ecx, %eax
	movl	%eax, %ecx
	sarl	$16, %ecx
	xorl	%eax, %ecx
	movl	160(%r13), %eax
	decl	%eax
	andl	%ecx, %eax
	cmpl	92(%r13), %eax
	jae	.LBB10_9
# BB#5:
	movq	104(%r13), %rcx
	cltq
	movl	(%rcx,%rax,4), %eax
	cmpl	$-1, %eax
	je	.LBB10_9
# BB#6:                                 # %.lr.ph.i.i.i
	movq	200(%r13), %rcx
	.p2align	4, 0x90
.LBB10_7:                               # =>This Inner Loop Header: Depth=1
	cltq
	cmpl	(%rcx,%rax,4), %ebp
	je	.LBB10_13
# BB#8:                                 #   in Loop: Header=BB10_7 Depth=1
	movq	136(%r13), %rdx
	movl	(%rdx,%rax,4), %eax
	cmpl	$-1, %eax
	jne	.LBB10_7
	jmp	.LBB10_9
.LBB10_13:                              # %_ZN9btHashMapI9btHashKeyI10btTriIndexES1_EixERKS2_.exit
	movq	168(%r13), %rcx
	shlq	$4, %rax
	movq	%rcx, %rdx
	addq	%rax, %rdx
	je	.LBB10_9
# BB#14:
	movq	8(%rcx,%rax), %rax
	movq	208(%rbx), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, 16(%rax)
	movq	%r14, %r15
	movq	200(%rbx), %r14
	movq	%rax, 200(%rbx)
	movq	(%r15), %rax
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	callq	*16(%rax)
	movq	%rax, %rbp
	movq	(%rbp), %rax
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	movq	72(%r13), %rcx
	movq	56(%r13), %r8
	movq	%rbp, %rdi
	callq	*16(%rax)
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	*104(%rax)
	movq	%r14, 200(%rbx)
	jmp	.LBB10_15
.LBB10_9:                               # %_ZN9btHashMapI9btHashKeyI10btTriIndexES1_EixERKS2_.exit.thread
	movss	(%r15), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	16(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm6
	movsd	20(%r15), %xmm0         # xmm0 = mem[0],zero
	movsd	4(%r15), %xmm4          # xmm4 = mem[0],zero
	subps	%xmm4, %xmm0
	movss	36(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movss	32(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	40(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	pshufd	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	subps	%xmm3, %xmm2
	movaps	%xmm0, %xmm4
	mulps	%xmm2, %xmm4
	movaps	%xmm6, %xmm3
	mulss	%xmm1, %xmm6
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	mulss	%xmm0, %xmm2
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	shufps	$0, %xmm0, %xmm3        # xmm3 = xmm3[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm3      # xmm3 = xmm3[2,0],xmm0[2,3]
	mulps	%xmm3, %xmm1
	subps	%xmm1, %xmm4
	movaps	%xmm4, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm2, %xmm6
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB10_11
# BB#10:                                # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm6, 32(%rsp)         # 16-byte Spill
	movaps	%xmm4, 144(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	144(%rsp), %xmm4        # 16-byte Reload
	movaps	32(%rsp), %xmm6         # 16-byte Reload
.LBB10_11:                              # %_ZN9btHashMapI9btHashKeyI10btTriIndexES1_EixERKS2_.exit.thread.split
	movss	.LCPI10_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm4, %xmm0
	mulss	%xmm6, %xmm1
	mulps	.LCPI10_1(%rip), %xmm0
	mulss	.LCPI10_2(%rip), %xmm1
	movsd	(%r15), %xmm9           # xmm9 = mem[0],zero
	movaps	%xmm9, %xmm4
	addps	%xmm0, %xmm4
	movss	8(%r15), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm6
	addss	%xmm11, %xmm6
	xorps	%xmm8, %xmm8
	xorps	%xmm7, %xmm7
	movss	%xmm6, %xmm7            # xmm7 = xmm6[0],xmm7[1,2,3]
	movlps	%xmm4, 48(%rsp)
	movlps	%xmm7, 56(%rsp)
	movsd	16(%r15), %xmm10        # xmm10 = mem[0],zero
	movaps	%xmm0, %xmm6
	addps	%xmm10, %xmm6
	movss	24(%r15), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	addss	%xmm7, %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movlps	%xmm6, 64(%rsp)
	movlps	%xmm3, 72(%rsp)
	movsd	32(%r15), %xmm2         # xmm2 = mem[0],zero
	movaps	%xmm0, %xmm3
	addps	%xmm2, %xmm3
	movss	40(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	addss	%xmm6, %xmm4
	xorps	%xmm5, %xmm5
	movss	%xmm4, %xmm5            # xmm5 = xmm4[0],xmm5[1,2,3]
	movlps	%xmm3, 80(%rsp)
	movlps	%xmm5, 88(%rsp)
	subps	%xmm0, %xmm9
	subss	%xmm1, %xmm11
	xorps	%xmm3, %xmm3
	movss	%xmm11, %xmm3           # xmm3 = xmm11[0],xmm3[1,2,3]
	movlps	%xmm9, 96(%rsp)
	movlps	%xmm3, 104(%rsp)
	subps	%xmm0, %xmm10
	subss	%xmm1, %xmm7
	xorps	%xmm3, %xmm3
	movss	%xmm7, %xmm3            # xmm3 = xmm7[0],xmm3[1,2,3]
	movlps	%xmm10, 112(%rsp)
	movlps	%xmm3, 120(%rsp)
	subps	%xmm0, %xmm2
	subss	%xmm1, %xmm6
	movss	%xmm6, %xmm8            # xmm8 = xmm6[0],xmm8[1,2,3]
	movlps	%xmm2, 128(%rsp)
	movlps	%xmm8, 136(%rsp)
	movl	$136, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
.Ltmp82:
	leaq	48(%rsp), %rsi
	movl	$6, %edx
	movl	$16, %ecx
	movq	%rbp, %rdi
	callq	_ZN17btConvexHullShapeC1EPKfii
.Ltmp83:
# BB#12:
	leaq	88(%r13), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	208(%rbx), %rax
	movq	16(%rax), %rax
	movq	%rax, 16(%rbp)
	movq	200(%rbx), %r12
	movq	%rbp, 200(%rbx)
	movq	(%r14), %rax
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	*16(%rax)
	movq	%rbx, %r15
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	movq	72(%r13), %rcx
	movq	56(%r13), %r8
	movq	%rbx, %rdi
	callq	*16(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*104(%rax)
	movq	%r12, 200(%r15)
	movq	%rbp, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E6insertERKS2_RKS1_
.LBB10_15:
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_16:
.Ltmp84:
	movq	%rax, %rbx
.Ltmp85:
	movq	%rbp, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp86:
# BB#17:                                # %_ZN17btConvexHullShapedlEPv.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB10_18:
.Ltmp87:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_ZN26btSoftBodyTriangleCallback15processTriangleEP9btVector3ii, .Lfunc_end10-_ZN26btSoftBodyTriangleCallback15processTriangleEP9btVector3ii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin7-.Lfunc_begin7 # >> Call Site 1 <<
	.long	.Ltmp82-.Lfunc_begin7   #   Call between .Lfunc_begin7 and .Ltmp82
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin7   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin7   # >> Call Site 3 <<
	.long	.Ltmp85-.Ltmp83         #   Call between .Ltmp83 and .Ltmp85
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin7   # >> Call Site 4 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin7   #     jumps to .Ltmp87
	.byte	1                       #   On action: 1
	.long	.Ltmp86-.Lfunc_begin7   # >> Call Site 5 <<
	.long	.Lfunc_end10-.Ltmp86    #   Call between .Ltmp86 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9btHashMapI9btHashKeyI10btTriIndexES1_E6insertERKS2_RKS1_,"axG",@progbits,_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E6insertERKS2_RKS1_,comdat
	.weak	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E6insertERKS2_RKS1_
	.p2align	4, 0x90
	.type	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E6insertERKS2_RKS1_,@function
_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E6insertERKS2_RKS1_: # @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E6insertERKS2_RKS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi79:
	.cfi_def_cfa_offset 80
.Lcfi80:
	.cfi_offset %rbx, -56
.Lcfi81:
	.cfi_offset %r12, -48
.Lcfi82:
	.cfi_offset %r13, -40
.Lcfi83:
	.cfi_offset %r14, -32
.Lcfi84:
	.cfi_offset %r15, -24
.Lcfi85:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r10
	movq	%rdi, %r12
	movl	(%r10), %eax
	movl	%eax, %ecx
	shll	$15, %ecx
	notl	%ecx
	addl	%eax, %ecx
	movl	%ecx, %edx
	sarl	$10, %edx
	xorl	%ecx, %edx
	leal	(%rdx,%rdx,8), %ecx
	movl	%ecx, %edx
	sarl	$6, %edx
	xorl	%ecx, %edx
	movl	%edx, %ecx
	shll	$11, %ecx
	notl	%ecx
	addl	%edx, %ecx
	movl	%ecx, %edx
	sarl	$16, %edx
	xorl	%ecx, %edx
	movl	72(%r12), %ebp
	leal	-1(%rbp), %r14d
	andl	%edx, %r14d
	cmpl	4(%r12), %r14d
	jae	.LBB11_5
# BB#1:
	movq	16(%r12), %rcx
	movslq	%r14d, %rdx
	movl	(%rcx,%rdx,4), %edx
	cmpl	$-1, %edx
	je	.LBB11_5
# BB#2:                                 # %.lr.ph.i
	movq	112(%r12), %rcx
	.p2align	4, 0x90
.LBB11_3:                               # =>This Inner Loop Header: Depth=1
	movslq	%edx, %rdx
	cmpl	(%rcx,%rdx,4), %eax
	je	.LBB11_9
# BB#4:                                 #   in Loop: Header=BB11_3 Depth=1
	movq	48(%r12), %rsi
	movl	(%rsi,%rdx,4), %edx
	cmpl	$-1, %edx
	jne	.LBB11_3
.LBB11_5:                               # %.loopexit
	movl	68(%r12), %edi
	cmpl	%ebp, %edi
	movl	%edi, %eax
	jne	.LBB11_23
# BB#6:
	leal	(%rbp,%rbp), %eax
	testl	%ebp, %ebp
	movl	$1, %r13d
	cmovnel	%eax, %r13d
	cmpl	%r13d, %ebp
	movl	%ebp, %eax
	jge	.LBB11_23
# BB#7:
	testl	%r13d, %r13d
	movl	%edi, 12(%rsp)          # 4-byte Spill
	je	.LBB11_10
# BB#8:
	movslq	%r13d, %rdi
	shlq	$4, %rdi
	movl	$16, %esi
	movq	%r10, %rbx
	callq	_Z22btAlignedAllocInternalmi
	movq	%rbx, %r10
	movq	%rax, %rbx
	movl	68(%r12), %eax
	testl	%eax, %eax
	jg	.LBB11_11
	jmp	.LBB11_18
.LBB11_9:                               # %_ZNK9btHashMapI9btHashKeyI10btTriIndexES1_E9findIndexERKS2_.exit
	movq	80(%r12), %rax
	shlq	$4, %rdx
	movups	(%r15), %xmm0
	movups	%xmm0, (%rax,%rdx)
	jmp	.LBB11_58
.LBB11_10:
	xorl	%ebx, %ebx
	movl	%ebp, %eax
	testl	%eax, %eax
	jle	.LBB11_18
.LBB11_11:                              # %.lr.ph.i.i.i27
	movq	%rbp, %r9
	cltq
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB11_14
# BB#12:                                # %.prol.preheader
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_13:                              # =>This Inner Loop Header: Depth=1
	movq	80(%r12), %rbp
	movups	(%rbp,%rdi), %xmm0
	movups	%xmm0, (%rbx,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB11_13
	jmp	.LBB11_15
.LBB11_14:
	xorl	%ecx, %ecx
.LBB11_15:                              # %.prol.loopexit
	cmpq	$3, %rdx
	movq	%r9, %rbp
	jb	.LBB11_18
# BB#16:                                # %.lr.ph.i.i.i27.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB11_17:                              # =>This Inner Loop Header: Depth=1
	movq	80(%r12), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbx,%rcx)
	movq	80(%r12), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbx,%rcx)
	movq	80(%r12), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbx,%rcx)
	movq	80(%r12), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbx,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB11_17
.LBB11_18:                              # %_ZNK20btAlignedObjectArrayI10btTriIndexE4copyEiiPS0_.exit.i.i
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB11_22
# BB#19:
	cmpb	$0, 88(%r12)
	je	.LBB11_21
# BB#20:
	movq	%r10, 16(%rsp)          # 8-byte Spill
	callq	_Z21btAlignedFreeInternalPv
	movq	16(%rsp), %r10          # 8-byte Reload
.LBB11_21:
	movq	$0, 80(%r12)
.LBB11_22:                              # %_ZN20btAlignedObjectArrayI10btTriIndexE10deallocateEv.exit.i.i
	movb	$1, 88(%r12)
	movq	%rbx, 80(%r12)
	movl	%r13d, 72(%r12)
	movl	68(%r12), %eax
	movl	12(%rsp), %edi          # 4-byte Reload
.LBB11_23:                              # %_ZN20btAlignedObjectArrayI10btTriIndexE9push_backERKS0_.exit
	movq	80(%r12), %rcx
	cltq
	shlq	$4, %rax
	movups	(%r15), %xmm0
	movups	%xmm0, (%rcx,%rax)
	incl	68(%r12)
	movl	100(%r12), %eax
	cmpl	104(%r12), %eax
	jne	.LBB11_55
# BB#24:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %r13d
	cmovnel	%ecx, %r13d
	cmpl	%r13d, %eax
	jge	.LBB11_55
# BB#25:
	testl	%r13d, %r13d
	movl	%edi, 12(%rsp)          # 4-byte Spill
	je	.LBB11_27
# BB#26:
	movslq	%r13d, %rdi
	shlq	$2, %rdi
	movl	$16, %esi
	movq	%r10, %rbx
	callq	_Z22btAlignedAllocInternalmi
	movq	%rbx, %r10
	movq	%rax, %r15
	movl	100(%r12), %eax
	jmp	.LBB11_28
.LBB11_27:
	xorl	%r15d, %r15d
.LBB11_28:                              # %_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE8allocateEi.exit.i.i
	movq	112(%r12), %rdi
	testl	%eax, %eax
	jle	.LBB11_31
# BB#29:                                # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	cmpl	$8, %eax
	jae	.LBB11_32
# BB#30:
	xorl	%edx, %edx
	jmp	.LBB11_45
.LBB11_31:                              # %_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4copyEiiPS2_.exit.i.i
	testq	%rdi, %rdi
	jne	.LBB11_51
	jmp	.LBB11_54
.LBB11_32:                              # %min.iters.checked
	movq	%rcx, %rdx
	andq	$-8, %rdx
	je	.LBB11_36
# BB#33:                                # %vector.memcheck
	leaq	(%rdi,%rcx,4), %rsi
	cmpq	%rsi, %r15
	jae	.LBB11_37
# BB#34:                                # %vector.memcheck
	leaq	(%r15,%rcx,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB11_37
.LBB11_36:
	xorl	%edx, %edx
.LBB11_45:                              # %scalar.ph.preheader
	movq	%rbp, %r8
	subl	%edx, %eax
	leaq	-1(%rcx), %rsi
	subq	%rdx, %rsi
	andq	$7, %rax
	je	.LBB11_48
# BB#46:                                # %scalar.ph.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB11_47:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %ebp
	movl	%ebp, (%r15,%rdx,4)
	incq	%rdx
	incq	%rax
	jne	.LBB11_47
.LBB11_48:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rsi
	movq	%r8, %rbp
	jb	.LBB11_51
# BB#49:                                # %scalar.ph.preheader.new
	subq	%rdx, %rcx
	leaq	28(%r15,%rdx,4), %rax
	leaq	28(%rdi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB11_50:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx), %esi
	movl	%esi, -28(%rax)
	movl	-24(%rdx), %esi
	movl	%esi, -24(%rax)
	movl	-20(%rdx), %esi
	movl	%esi, -20(%rax)
	movl	-16(%rdx), %esi
	movl	%esi, -16(%rax)
	movl	-12(%rdx), %esi
	movl	%esi, -12(%rax)
	movl	-8(%rdx), %esi
	movl	%esi, -8(%rax)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rax)
	movl	(%rdx), %esi
	movl	%esi, (%rax)
	addq	$32, %rax
	addq	$32, %rdx
	addq	$-8, %rcx
	jne	.LBB11_50
.LBB11_51:                              # %_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4copyEiiPS2_.exit.thread.i.i
	cmpb	$0, 120(%r12)
	je	.LBB11_53
# BB#52:
	movq	%r10, %rbx
	callq	_Z21btAlignedFreeInternalPv
	movq	%rbx, %r10
.LBB11_53:
	movq	$0, 112(%r12)
	movl	100(%r12), %eax
.LBB11_54:                              # %_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE10deallocateEv.exit.i.i
	movb	$1, 120(%r12)
	movq	%r15, 112(%r12)
	movl	%r13d, 104(%r12)
	movl	12(%rsp), %edi          # 4-byte Reload
.LBB11_55:                              # %_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE9push_backERKS2_.exit
	movslq	%edi, %r15
	movq	112(%r12), %rcx
	cltq
	movl	(%r10), %edx
	movl	%edx, (%rcx,%rax,4)
	incl	100(%r12)
	cmpl	72(%r12), %ebp
	jge	.LBB11_57
# BB#56:
	movl	%edi, %ebp
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, %rbx
	callq	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10growTablesERKS2_
	movl	%ebp, %edi
	movl	(%rbx), %eax
	movl	%eax, %ecx
	shll	$15, %ecx
	notl	%ecx
	addl	%eax, %ecx
	movl	%ecx, %eax
	sarl	$10, %eax
	xorl	%ecx, %eax
	leal	(%rax,%rax,8), %eax
	movl	%eax, %ecx
	sarl	$6, %ecx
	xorl	%eax, %ecx
	movl	%ecx, %eax
	shll	$11, %eax
	notl	%eax
	addl	%ecx, %eax
	movl	%eax, %ecx
	sarl	$16, %ecx
	xorl	%eax, %ecx
	movl	72(%r12), %r14d
	decl	%r14d
	andl	%ecx, %r14d
.LBB11_57:
	movslq	%r14d, %rax
	movq	16(%r12), %rcx
	movq	48(%r12), %rdx
	movl	(%rcx,%rax,4), %esi
	movl	%esi, (%rdx,%r15,4)
	movl	%edi, (%rcx,%rax,4)
.LBB11_58:                              # %_ZNK9btHashMapI9btHashKeyI10btTriIndexES1_E9findIndexERKS2_.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_37:                              # %vector.body.preheader
	movq	%rbp, %r9
	leaq	-8(%rdx), %rbx
	movl	%ebx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB11_40
# BB#38:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_39:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp,4), %xmm0
	movups	16(%rdi,%rbp,4), %xmm1
	movups	%xmm0, (%r15,%rbp,4)
	movups	%xmm1, 16(%r15,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB11_39
	jmp	.LBB11_41
.LBB11_40:
	xorl	%ebp, %ebp
.LBB11_41:                              # %vector.body.prol.loopexit
	cmpq	$24, %rbx
	jb	.LBB11_44
# BB#42:                                # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rbp, %rsi
	leaq	112(%r15,%rbp,4), %rbx
	leaq	112(%rdi,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB11_43:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rbp
	addq	$-32, %rsi
	jne	.LBB11_43
.LBB11_44:                              # %middle.block
	cmpq	%rdx, %rcx
	movq	%r9, %rbp
	je	.LBB11_51
	jmp	.LBB11_45
.Lfunc_end11:
	.size	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E6insertERKS2_RKS1_, .Lfunc_end11-_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E6insertERKS2_RKS1_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_0:
	.long	1031127695              # float 0.0599999987
.LCPI12_1:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI12_3:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZN26btSoftBodyTriangleCallback22setTimeStepAndCountersEfRK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN26btSoftBodyTriangleCallback22setTimeStepAndCountersEfRK16btDispatcherInfoP16btManifoldResult,@function
_ZN26btSoftBodyTriangleCallback22setTimeStepAndCountersEfRK16btDispatcherInfoP16btManifoldResult: # @_ZN26btSoftBodyTriangleCallback22setTimeStepAndCountersEfRK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 16
	subq	$128, %rsp
.Lcfi87:
	.cfi_def_cfa_offset 144
.Lcfi88:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	%rsi, 72(%rbx)
	addss	.LCPI12_0(%rip), %xmm0
	movss	%xmm0, 80(%rbx)
	movq	%rdx, 56(%rbx)
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	32(%rsp), %rsi
	leaq	16(%rsp), %rdx
	callq	*32(%rax)
	movss	16(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm7
	subss	%xmm0, %xmm7
	movaps	%xmm11, %xmm2
	subss	%xmm1, %xmm2
	movss	24(%rsp), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	40(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm4
	subss	%xmm3, %xmm4
	movss	.LCPI12_1(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm7
	movaps	%xmm7, 112(%rsp)        # 16-byte Spill
	mulss	%xmm6, %xmm2
	movaps	%xmm2, 96(%rsp)         # 16-byte Spill
	mulss	%xmm6, %xmm4
	movaps	%xmm4, 80(%rsp)         # 16-byte Spill
	addss	%xmm0, %xmm5
	addss	%xmm1, %xmm11
	addss	%xmm3, %xmm14
	mulss	%xmm6, %xmm5
	mulss	%xmm6, %xmm11
	mulss	%xmm6, %xmm14
	movq	16(%rbx), %rax
	movsd	8(%rax), %xmm13         # xmm13 = mem[0],zero
	movsd	24(%rax), %xmm8         # xmm8 = mem[0],zero
	movsd	40(%rax), %xmm12        # xmm12 = mem[0],zero
	movss	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	movss	32(%rax), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movd	56(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movdqa	.LCPI12_2(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm0, %xmm7
	movd	60(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movdqa	%xmm1, %xmm3
	pxor	%xmm0, %xmm3
	pshufd	$224, %xmm7, %xmm6      # xmm6 = xmm7[0,0,2,3]
	mulps	%xmm13, %xmm6
	pshufd	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm8, %xmm3
	addps	%xmm6, %xmm3
	movd	64(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	pxor	%xmm6, %xmm0
	pshufd	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm12, %xmm0
	addps	%xmm3, %xmm0
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	movss	48(%rax), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm7
	mulss	%xmm15, %xmm1
	subss	%xmm1, %xmm7
	mulss	%xmm10, %xmm6
	subss	%xmm6, %xmm7
	xorps	%xmm0, %xmm0
	movaps	%xmm13, %xmm6
	mulps	%xmm0, %xmm6
	movaps	%xmm6, %xmm1
	addps	%xmm8, %xmm1
	movaps	%xmm11, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm8, %xmm4
	mulps	%xmm0, %xmm8
	movaps	%xmm5, %xmm9
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	mulps	%xmm13, %xmm9
	movaps	%xmm13, %xmm3
	addps	%xmm8, %xmm3
	addps	%xmm8, %xmm6
	addps	%xmm12, %xmm6
	movaps	%xmm14, %xmm13
	shufps	$224, %xmm13, %xmm13    # xmm13 = xmm13[0,0,2,3]
	mulps	%xmm12, %xmm13
	mulps	%xmm0, %xmm12
	addps	%xmm12, %xmm3
	addps	%xmm12, %xmm1
	movaps	%xmm1, 48(%rsp)         # 16-byte Spill
	xorps	%xmm8, %xmm8
	movaps	%xmm15, %xmm2
	movaps	%xmm2, %xmm1
	mulss	%xmm8, %xmm1
	movaps	%xmm10, %xmm15
	mulss	%xmm8, %xmm15
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm8
	mulss	%xmm0, %xmm5
	movaps	%xmm0, %xmm12
	addss	%xmm1, %xmm12
	addss	%xmm15, %xmm12
	movaps	%xmm8, %xmm0
	addss	%xmm2, %xmm0
	addss	%xmm15, %xmm0
	addss	%xmm1, %xmm8
	addps	%xmm9, %xmm4
	addps	%xmm4, %xmm13
	addps	64(%rsp), %xmm13        # 16-byte Folded Reload
	mulss	%xmm2, %xmm11
	addss	%xmm5, %xmm11
	addss	%xmm10, %xmm8
	mulss	%xmm10, %xmm14
	addss	%xmm11, %xmm14
	addss	%xmm7, %xmm14
	movss	80(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	112(%rsp), %xmm7        # 16-byte Reload
	addss	%xmm1, %xmm7
	movaps	96(%rsp), %xmm5         # 16-byte Reload
	addss	%xmm1, %xmm5
	movaps	80(%rsp), %xmm9         # 16-byte Reload
	addss	%xmm1, %xmm9
	movaps	.LCPI12_3(%rip), %xmm1  # xmm1 = [nan,nan,nan,nan]
	andps	%xmm1, %xmm3
	movaps	48(%rsp), %xmm4         # 16-byte Reload
	andps	%xmm1, %xmm4
	andps	%xmm1, %xmm6
	andps	%xmm1, %xmm12
	andps	%xmm1, %xmm0
	andps	%xmm1, %xmm8
	movaps	%xmm7, %xmm1
	mulss	%xmm1, %xmm12
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm3, %xmm1
	movaps	%xmm5, %xmm2
	mulss	%xmm2, %xmm0
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movaps	%xmm9, %xmm1
	mulss	%xmm1, %xmm8
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm6, %xmm1
	addps	%xmm2, %xmm1
	addss	%xmm12, %xmm0
	addss	%xmm0, %xmm8
	movaps	%xmm14, %xmm0
	subss	%xmm8, %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movaps	%xmm13, %xmm0
	subps	%xmm1, %xmm0
	movlps	%xmm0, 24(%rbx)
	movlps	%xmm2, 32(%rbx)
	addps	%xmm13, %xmm1
	addss	%xmm14, %xmm8
	xorps	%xmm0, %xmm0
	movss	%xmm8, %xmm0            # xmm0 = xmm8[0],xmm0[1,2,3]
	movlps	%xmm1, 40(%rbx)
	movlps	%xmm0, 48(%rbx)
	addq	$128, %rsp
	popq	%rbx
	retq
.Lfunc_end12:
	.size	_ZN26btSoftBodyTriangleCallback22setTimeStepAndCountersEfRK16btDispatcherInfoP16btManifoldResult, .Lfunc_end12-_ZN26btSoftBodyTriangleCallback22setTimeStepAndCountersEfRK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.globl	_ZN35btSoftBodyConcaveCollisionAlgorithm10clearCacheEv
	.p2align	4, 0x90
	.type	_ZN35btSoftBodyConcaveCollisionAlgorithm10clearCacheEv,@function
_ZN35btSoftBodyConcaveCollisionAlgorithm10clearCacheEv: # @_ZN35btSoftBodyConcaveCollisionAlgorithm10clearCacheEv
	.cfi_startproc
# BB#0:
	addq	$24, %rdi
	jmp	_ZN26btSoftBodyTriangleCallback10clearCacheEv # TAILCALL
.Lfunc_end13:
	.size	_ZN35btSoftBodyConcaveCollisionAlgorithm10clearCacheEv, .Lfunc_end13-_ZN35btSoftBodyConcaveCollisionAlgorithm10clearCacheEv
	.cfi_endproc

	.globl	_ZN35btSoftBodyConcaveCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN35btSoftBodyConcaveCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN35btSoftBodyConcaveCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN35btSoftBodyConcaveCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi92:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 48
.Lcfi94:
	.cfi_offset %rbx, -48
.Lcfi95:
	.cfi_offset %r12, -40
.Lcfi96:
	.cfi_offset %r13, -32
.Lcfi97:
	.cfi_offset %r14, -24
.Lcfi98:
	.cfi_offset %r15, -16
	movq	%r8, %r15
	movq	%rcx, %r12
	movq	%rdi, %r14
	cmpb	$0, 16(%r14)
	cmoveq	%rdx, %rsi
	movq	200(%rsi), %rbx
	movl	8(%rbx), %eax
	addl	$-21, %eax
	cmpl	$8, %eax
	ja	.LBB14_1
# BB#2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	leaq	24(%r14), %r13
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	_ZN26btSoftBodyTriangleCallback22setTimeStepAndCountersEfRK16btDispatcherInfoP16btManifoldResult
	movq	(%rbx), %rax
	movq	96(%rax), %rax
	leaq	48(%r14), %rdx
	addq	$64, %r14
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%r14, %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB14_1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	_ZN35btSoftBodyConcaveCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end14-_ZN35btSoftBodyConcaveCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI15_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI15_2:
	.zero	16
	.text
	.globl	_ZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%rbp
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 24
	subq	$808, %rsp              # imm = 0x328
.Lcfi101:
	.cfi_def_cfa_offset 832
.Lcfi102:
	.cfi_offset %rbx, -24
.Lcfi103:
	.cfi_offset %rbp, -16
	cmpb	$0, 16(%rdi)
	movq	%rsi, %rbx
	cmovneq	%rdx, %rbx
	cmovneq	%rsi, %rdx
	movss	120(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	124(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	56(%rbx), %xmm0
	subss	60(%rbx), %xmm1
	movss	128(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	64(%rbx), %xmm2
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	268(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	movss	.LCPI15_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	ja	.LBB15_20
# BB#1:
	movq	200(%rdx), %rdi
	movl	8(%rdi), %eax
	addl	$-21, %eax
	cmpl	$8, %eax
	ja	.LBB15_20
# BB#2:
	movss	24(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	40(%rdx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movss	12(%rdx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	28(%rdx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	44(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	16(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	32(%rdx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	48(%rdx), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movss	56(%rdx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	xorps	.LCPI15_1(%rip), %xmm12
	movss	60(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm14
	mulss	%xmm12, %xmm14
	movaps	%xmm4, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm14
	movaps	%xmm7, %xmm2
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm14
	movaps	%xmm11, %xmm10
	mulss	%xmm12, %xmm10
	movaps	%xmm13, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm10
	movaps	%xmm6, %xmm2
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm10
	mulss	%xmm5, %xmm12
	mulss	%xmm8, %xmm0
	subss	%xmm0, %xmm12
	mulss	%xmm15, %xmm1
	subss	%xmm1, %xmm12
	movaps	%xmm9, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	120(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	mulps	%xmm0, %xmm2
	movss	124(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	60(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	movaps	%xmm4, 752(%rsp)        # 16-byte Spill
	movaps	%xmm4, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	addps	%xmm2, %xmm3
	movss	128(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	movaps	%xmm7, 384(%rsp)        # 16-byte Spill
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm4
	addps	%xmm3, %xmm4
	shufps	$224, %xmm14, %xmm14    # xmm14 = xmm14[0,0,2,3]
	addps	%xmm4, %xmm14
	movaps	%xmm11, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm0, %xmm3
	movaps	%xmm13, 352(%rsp)       # 16-byte Spill
	movaps	%xmm13, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm1, %xmm4
	addps	%xmm3, %xmm4
	movaps	%xmm6, 400(%rsp)        # 16-byte Spill
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm2, %xmm3
	addps	%xmm4, %xmm3
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	addps	%xmm3, %xmm10
	movaps	%xmm5, 64(%rsp)         # 16-byte Spill
	movaps	%xmm5, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm0, %xmm3
	movaps	%xmm8, 368(%rsp)        # 16-byte Spill
	movaps	%xmm8, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	movaps	%xmm14, %xmm4
	unpcklps	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1]
	addps	%xmm3, %xmm0
	movaps	%xmm15, 416(%rsp)       # 16-byte Spill
	movaps	%xmm15, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	addps	%xmm0, %xmm1
	shufps	$224, %xmm12, %xmm12    # xmm12 = xmm12[0,0,2,3]
	addps	%xmm1, %xmm12
	xorps	%xmm1, %xmm1
	movaps	%xmm12, %xmm0
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movaps	%xmm14, %xmm15
	shufps	$229, %xmm15, %xmm15    # xmm15 = xmm15[1,1,2,3]
	movaps	%xmm10, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm12, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	movaps	%xmm4, 768(%rsp)        # 16-byte Spill
	movaps	%xmm0, 736(%rsp)        # 16-byte Spill
	unpcklpd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0]
	movss	8(%rbx), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movss	40(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movss	28(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	44(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	movss	16(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 124(%rsp)        # 4-byte Spill
	movss	48(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	movss	72(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	88(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movss	104(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
	movss	76(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	92(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movss	108(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movss	80(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, (%rsp)           # 4-byte Spill
	movss	96(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 120(%rsp)        # 4-byte Spill
	movss	112(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 128(%rsp)        # 4-byte Spill
	movapd	%xmm4, 688(%rsp)        # 16-byte Spill
	movupd	%xmm4, 104(%rsp)
	ucomiss	%xmm15, %xmm14
	movaps	%xmm14, 336(%rsp)       # 16-byte Spill
	movss	%xmm10, 60(%rsp)        # 4-byte Spill
	movss	%xmm14, 56(%rsp)        # 4-byte Spill
	jbe	.LBB15_4
# BB#3:
	movss	%xmm15, 104(%rsp)
	movaps	%xmm15, %xmm14
.LBB15_4:                               # %_Z8btSetMinIfEvRT_RKS0_.exit.i
	movss	%xmm14, 140(%rsp)       # 4-byte Spill
	movss	60(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm0
	jbe	.LBB15_6
# BB#5:
	movss	%xmm6, 108(%rsp)
	movaps	%xmm6, %xmm0
.LBB15_6:                               # %_Z8btSetMinIfEvRT_RKS0_.exit7.i
	movaps	%xmm6, 704(%rsp)        # 16-byte Spill
	movss	%xmm0, 136(%rsp)        # 4-byte Spill
	ucomiss	%xmm7, %xmm12
	movaps	%xmm12, %xmm0
	jbe	.LBB15_8
# BB#7:
	movss	%xmm7, 112(%rsp)
	movaps	%xmm7, %xmm0
.LBB15_8:                               # %_ZN9btVector36setMinERKS_.exit
	movss	%xmm0, 132(%rsp)        # 4-byte Spill
	movaps	%xmm7, 720(%rsp)        # 16-byte Spill
	movaps	%xmm10, 784(%rsp)       # 16-byte Spill
	movaps	%xmm9, %xmm0
	mulss	%xmm8, %xmm0
	movaps	%xmm0, 256(%rsp)        # 16-byte Spill
	movaps	752(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm0, %xmm4
	movss	4(%rsp), %xmm7          # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm4
	movaps	%xmm4, 640(%rsp)        # 16-byte Spill
	movaps	%xmm9, %xmm6
	mulss	%xmm1, %xmm6
	movaps	%xmm6, 320(%rsp)        # 16-byte Spill
	movaps	%xmm0, %xmm4
	mulss	%xmm13, %xmm4
	movaps	%xmm4, 656(%rsp)        # 16-byte Spill
	movaps	%xmm9, %xmm6
	mulss	%xmm5, %xmm6
	movaps	%xmm6, 304(%rsp)        # 16-byte Spill
	movaps	%xmm13, %xmm14
	movaps	%xmm0, %xmm4
	movss	124(%rsp), %xmm10       # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	movaps	%xmm4, 672(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm6
	mulss	%xmm8, %xmm6
	movaps	%xmm6, 288(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm6
	mulss	%xmm1, %xmm6
	movaps	%xmm6, 272(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm6
	mulss	%xmm5, %xmm6
	movaps	%xmm6, 208(%rsp)        # 16-byte Spill
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	mulss	%xmm4, %xmm8
	movss	%xmm8, 40(%rsp)         # 4-byte Spill
	mulss	%xmm4, %xmm1
	movss	%xmm1, 44(%rsp)         # 4-byte Spill
	mulss	%xmm4, %xmm5
	movss	%xmm5, 36(%rsp)         # 4-byte Spill
	movaps	%xmm9, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm1, 192(%rsp)        # 16-byte Spill
	movaps	%xmm9, %xmm1
	mulss	%xmm3, %xmm1
	movaps	%xmm1, 176(%rsp)        # 16-byte Spill
	movss	(%rsp), %xmm5           # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm9
	movaps	%xmm9, 224(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm1, 160(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm1
	mulss	%xmm3, %xmm1
	movaps	%xmm1, 144(%rsp)        # 16-byte Spill
	mulss	%xmm5, %xmm11
	movaps	%xmm11, 240(%rsp)       # 16-byte Spill
	movaps	%xmm10, %xmm11
	movaps	%xmm0, %xmm10
	mulss	%xmm4, %xmm2
	movss	%xmm2, 52(%rsp)         # 4-byte Spill
	mulss	%xmm4, %xmm3
	movss	%xmm3, 48(%rsp)         # 4-byte Spill
	mulss	%xmm5, %xmm4
	movaps	%xmm4, 64(%rsp)         # 16-byte Spill
	movaps	352(%rsp), %xmm3        # 16-byte Reload
	movaps	%xmm3, %xmm0
	mulss	%xmm7, %xmm0
	movaps	%xmm3, %xmm5
	mulss	%xmm14, %xmm5
	movaps	%xmm3, %xmm4
	mulss	%xmm11, %xmm4
	movaps	368(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm2, %xmm7
	movss	%xmm7, 4(%rsp)          # 4-byte Spill
	mulss	%xmm2, %xmm14
	movss	%xmm14, (%rsp)          # 4-byte Spill
	mulss	%xmm2, %xmm11
	movaps	%xmm10, %xmm9
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm9
	movaps	%xmm2, %xmm6
	movaps	%xmm10, %xmm14
	movss	12(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm14
	movss	120(%rsp), %xmm13       # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm10
	movaps	%xmm3, %xmm2
	mulss	%xmm1, %xmm2
	movaps	%xmm3, %xmm8
	mulss	%xmm7, %xmm8
	mulss	%xmm13, %xmm3
	movaps	%xmm3, 352(%rsp)        # 16-byte Spill
	mulss	%xmm6, %xmm1
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	mulss	%xmm6, %xmm7
	movss	%xmm7, 12(%rsp)         # 4-byte Spill
	mulss	%xmm13, %xmm6
	movaps	%xmm6, 368(%rsp)        # 16-byte Spill
	movaps	688(%rsp), %xmm1        # 16-byte Reload
	movups	%xmm1, 88(%rsp)
	ucomiss	336(%rsp), %xmm15       # 16-byte Folded Reload
	jbe	.LBB15_10
# BB#9:
	movss	%xmm15, 88(%rsp)
	movss	%xmm15, 56(%rsp)        # 4-byte Spill
.LBB15_10:                              # %_Z8btSetMaxIfEvRT_RKS0_.exit.i
	movaps	256(%rsp), %xmm1        # 16-byte Reload
	addss	640(%rsp), %xmm1        # 16-byte Folded Reload
	movaps	%xmm1, 256(%rsp)        # 16-byte Spill
	movaps	320(%rsp), %xmm3        # 16-byte Reload
	addss	656(%rsp), %xmm3        # 16-byte Folded Reload
	movaps	%xmm3, 320(%rsp)        # 16-byte Spill
	movaps	304(%rsp), %xmm3        # 16-byte Reload
	addss	672(%rsp), %xmm3        # 16-byte Folded Reload
	movaps	%xmm3, 304(%rsp)        # 16-byte Spill
	movaps	288(%rsp), %xmm3        # 16-byte Reload
	addss	%xmm0, %xmm3
	movaps	%xmm3, 288(%rsp)        # 16-byte Spill
	movaps	272(%rsp), %xmm0        # 16-byte Reload
	addss	%xmm5, %xmm0
	movaps	%xmm0, 272(%rsp)        # 16-byte Spill
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	addss	%xmm4, %xmm0
	movaps	%xmm0, 208(%rsp)        # 16-byte Spill
	movss	40(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	4(%rsp), %xmm0          # 4-byte Folded Reload
	movss	%xmm0, 40(%rsp)         # 4-byte Spill
	movss	44(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	(%rsp), %xmm0           # 4-byte Folded Reload
	movss	%xmm0, 44(%rsp)         # 4-byte Spill
	movss	36(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	%xmm11, %xmm0
	movss	%xmm0, 36(%rsp)         # 4-byte Spill
	movaps	384(%rsp), %xmm3        # 16-byte Reload
	movaps	%xmm3, %xmm11
	movss	16(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm11
	movaps	192(%rsp), %xmm0        # 16-byte Reload
	addss	%xmm9, %xmm0
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	movaps	%xmm3, %xmm9
	movss	20(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm9
	movaps	176(%rsp), %xmm0        # 16-byte Reload
	addss	%xmm14, %xmm0
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	movaps	%xmm3, %xmm13
	movss	24(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm13
	movaps	224(%rsp), %xmm1        # 16-byte Reload
	addss	%xmm10, %xmm1
	movaps	%xmm1, 224(%rsp)        # 16-byte Spill
	movaps	400(%rsp), %xmm10       # 16-byte Reload
	movaps	%xmm10, %xmm15
	mulss	%xmm6, %xmm15
	movaps	160(%rsp), %xmm1        # 16-byte Reload
	addss	%xmm2, %xmm1
	movaps	%xmm1, 160(%rsp)        # 16-byte Spill
	movaps	%xmm10, %xmm5
	mulss	%xmm4, %xmm5
	movaps	144(%rsp), %xmm1        # 16-byte Reload
	addss	%xmm8, %xmm1
	movaps	%xmm1, 144(%rsp)        # 16-byte Spill
	movaps	%xmm10, %xmm1
	mulss	%xmm0, %xmm1
	movaps	416(%rsp), %xmm14       # 16-byte Reload
	mulss	%xmm14, %xmm6
	movss	%xmm6, 16(%rsp)         # 4-byte Spill
	mulss	%xmm14, %xmm4
	movss	%xmm4, 20(%rsp)         # 4-byte Spill
	mulss	%xmm14, %xmm0
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	movaps	240(%rsp), %xmm0        # 16-byte Reload
	addss	352(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, 240(%rsp)        # 16-byte Spill
	movaps	%xmm3, %xmm2
	movss	28(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	movss	52(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	8(%rsp), %xmm0          # 4-byte Folded Reload
	movss	%xmm0, 52(%rsp)         # 4-byte Spill
	movaps	%xmm3, %xmm0
	movss	32(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	movss	128(%rsp), %xmm8        # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	movaps	%xmm3, 384(%rsp)        # 16-byte Spill
	movss	48(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	addss	12(%rsp), %xmm3         # 4-byte Folded Reload
	movss	%xmm3, 48(%rsp)         # 4-byte Spill
	movaps	%xmm10, %xmm3
	mulss	%xmm7, %xmm3
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	addss	368(%rsp), %xmm4        # 16-byte Folded Reload
	movaps	%xmm4, 64(%rsp)         # 16-byte Spill
	movaps	%xmm10, %xmm4
	mulss	%xmm6, %xmm4
	mulss	%xmm8, %xmm10
	movaps	%xmm10, 400(%rsp)       # 16-byte Spill
	mulss	%xmm14, %xmm7
	movss	%xmm7, 28(%rsp)         # 4-byte Spill
	mulss	%xmm14, %xmm6
	movss	%xmm6, 32(%rsp)         # 4-byte Spill
	mulss	%xmm8, %xmm14
	movaps	%xmm14, 416(%rsp)       # 16-byte Spill
	movaps	784(%rsp), %xmm10       # 16-byte Reload
	shufps	$17, 336(%rsp), %xmm10  # 16-byte Folded Reload
                                        # xmm10 = xmm10[1,0],mem[1,0]
	movss	60(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	704(%rsp), %xmm7        # 16-byte Reload
	ucomiss	%xmm6, %xmm7
	jbe	.LBB15_12
# BB#11:
	movss	%xmm7, 92(%rsp)
	movaps	%xmm7, %xmm6
.LBB15_12:                              # %_Z8btSetMaxIfEvRT_RKS0_.exit7.i
	movaps	256(%rsp), %xmm8        # 16-byte Reload
	addss	%xmm11, %xmm8
	movaps	320(%rsp), %xmm11       # 16-byte Reload
	addss	%xmm9, %xmm11
	movaps	304(%rsp), %xmm7        # 16-byte Reload
	addss	%xmm13, %xmm7
	movaps	%xmm7, %xmm13
	movaps	288(%rsp), %xmm14       # 16-byte Reload
	addss	%xmm15, %xmm14
	movaps	272(%rsp), %xmm15       # 16-byte Reload
	addss	%xmm5, %xmm15
	movaps	208(%rsp), %xmm5        # 16-byte Reload
	addss	%xmm1, %xmm5
	movaps	%xmm5, 208(%rsp)        # 16-byte Spill
	movss	40(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	addss	16(%rsp), %xmm9         # 4-byte Folded Reload
	movss	44(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	addss	20(%rsp), %xmm5         # 4-byte Folded Reload
	movss	36(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	addss	24(%rsp), %xmm7         # 4-byte Folded Reload
	movaps	192(%rsp), %xmm1        # 16-byte Reload
	addss	%xmm2, %xmm1
	movaps	%xmm1, 192(%rsp)        # 16-byte Spill
	movaps	176(%rsp), %xmm1        # 16-byte Reload
	addss	%xmm0, %xmm1
	movaps	%xmm1, 176(%rsp)        # 16-byte Spill
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	addss	384(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	addss	%xmm3, %xmm0
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
	movaps	144(%rsp), %xmm0        # 16-byte Reload
	addss	%xmm4, %xmm0
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	movaps	240(%rsp), %xmm0        # 16-byte Reload
	addss	400(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, 240(%rsp)        # 16-byte Spill
	movss	52(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	addss	28(%rsp), %xmm3         # 4-byte Folded Reload
	movss	48(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	addss	32(%rsp), %xmm4         # 4-byte Folded Reload
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	addss	416(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	shufps	$226, 336(%rsp), %xmm10 # 16-byte Folded Reload
                                        # xmm10 = xmm10[2,0],mem[2,3]
	movaps	720(%rsp), %xmm1        # 16-byte Reload
	movaps	%xmm1, %xmm0
	unpcklps	.LCPI15_2, %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	ucomiss	%xmm12, %xmm1
	jbe	.LBB15_14
# BB#13:
	movss	%xmm1, 96(%rsp)
	movaps	%xmm1, %xmm12
.LBB15_14:                              # %_ZN9btVector36setMaxERKS_.exit
	movss	264(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	140(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	movss	%xmm2, 104(%rsp)
	movss	136(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	movss	%xmm2, 108(%rsp)
	movss	132(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	movss	%xmm2, 112(%rsp)
	movss	56(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm2
	movss	%xmm2, 88(%rsp)
	addss	%xmm1, %xmm6
	movss	%xmm6, 92(%rsp)
	addss	%xmm1, %xmm12
	movss	%xmm12, 96(%rsp)
	movq	$_ZTVZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback+16, 432(%rsp)
	movss	%xmm8, 440(%rsp)
	movss	%xmm11, 444(%rsp)
	movss	%xmm13, 448(%rsp)
	movl	$0, 452(%rsp)
	movss	%xmm14, 456(%rsp)
	movss	%xmm15, 460(%rsp)
	movaps	208(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 464(%rsp)
	movl	$0, 468(%rsp)
	movss	%xmm9, 472(%rsp)
	movss	%xmm5, 476(%rsp)
	movss	%xmm7, 480(%rsp)
	movl	$0, 484(%rsp)
	movaps	768(%rsp), %xmm2        # 16-byte Reload
	movlps	%xmm2, 488(%rsp)
	movaps	736(%rsp), %xmm2        # 16-byte Reload
	movlps	%xmm2, 496(%rsp)
	movaps	192(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 504(%rsp)
	movaps	176(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 508(%rsp)
	movaps	224(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 512(%rsp)
	movl	$0, 516(%rsp)
	movaps	160(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 520(%rsp)
	movaps	144(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 524(%rsp)
	movaps	240(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 528(%rsp)
	movl	$0, 532(%rsp)
	movss	%xmm3, 536(%rsp)
	movss	%xmm4, 540(%rsp)
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movss	%xmm2, 544(%rsp)
	movl	$0, 548(%rsp)
	movlps	%xmm10, 552(%rsp)
	movlps	%xmm0, 560(%rsp)
	movss	%xmm1, 632(%rsp)
	movl	260(%rbx), %eax
	movl	%eax, 636(%rsp)
	testq	%rdi, %rdi
	movb	$1, %bpl
	je	.LBB15_15
# BB#16:
	movq	(%rdi), %rax
.Ltmp88:
	leaq	432(%rsp), %rsi
	leaq	104(%rsp), %rdx
	leaq	88(%rsp), %rcx
	callq	*96(%rax)
.Ltmp89:
# BB#17:
	movss	636(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movd	260(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	movss	%xmm1, 64(%rsp)         # 4-byte Spill
	jbe	.LBB15_19
# BB#18:
	movss	%xmm1, 260(%rbx)
	xorl	%ebp, %ebp
	jmp	.LBB15_19
.LBB15_15:
	movd	%eax, %xmm0
	movd	%xmm0, 64(%rsp)         # 4-byte Folded Spill
.LBB15_19:                              # %.thread
	leaq	432(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
	testb	%bpl, %bpl
	movss	.LCPI15_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movss	64(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	je	.LBB15_21
.LBB15_20:
	movaps	%xmm1, %xmm0
.LBB15_21:
	addq	$808, %rsp              # imm = 0x328
	popq	%rbx
	popq	%rbp
	retq
.LBB15_22:
.Ltmp90:
	movq	%rax, %rbx
.Ltmp91:
	leaq	432(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp92:
# BB#23:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB15_24:
.Ltmp93:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end15-_ZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp88-.Lfunc_begin8   # >> Call Site 1 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin8   #     jumps to .Ltmp90
	.byte	0                       #   On action: cleanup
	.long	.Ltmp89-.Lfunc_begin8   # >> Call Site 2 <<
	.long	.Ltmp91-.Ltmp89         #   Call between .Ltmp89 and .Ltmp91
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin8   # >> Call Site 3 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin8   #     jumps to .Ltmp93
	.byte	1                       #   On action: 1
	.long	.Ltmp92-.Lfunc_begin8   # >> Call Site 4 <<
	.long	.Lfunc_end15-.Ltmp92    #   Call between .Ltmp92 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN35btSoftBodyConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,"axG",@progbits,_ZN35btSoftBodyConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,comdat
	.weak	_ZN35btSoftBodyConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.p2align	4, 0x90
	.type	_ZN35btSoftBodyConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,@function
_ZN35btSoftBodyConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE: # @_ZN35btSoftBodyConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end16:
	.size	_ZN35btSoftBodyConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE, .Lfunc_end16-_ZN35btSoftBodyConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackD0Ev,@function
_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackD0Ev: # @_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -24
.Lcfi108:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp94:
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp95:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB17_2:
.Ltmp96:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end17:
	.size	_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackD0Ev, .Lfunc_end17-_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp94-.Lfunc_begin9   # >> Call Site 1 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin9   #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.long	.Ltmp95-.Lfunc_begin9   # >> Call Site 2 <<
	.long	.Lfunc_end17-.Ltmp95    #   Call between .Ltmp95 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallback15processTriangleEP9btVector3ii,@function
_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallback15processTriangleEP9btVector3ii: # @_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallback15processTriangleEP9btVector3ii
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 40
	subq	$824, %rsp              # imm = 0x338
.Lcfi113:
	.cfi_def_cfa_offset 864
.Lcfi114:
	.cfi_offset %rbx, -40
.Lcfi115:
	.cfi_offset %r14, -32
.Lcfi116:
	.cfi_offset %r15, -24
.Lcfi117:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movl	$1065353216, 32(%rsp)   # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 36(%rsp)
	movl	$1065353216, 52(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 56(%rsp)
	movl	$1065353216, 72(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 76(%rsp)
	movl	$0, 92(%rsp)
	movq	$_ZTVN12btConvexCast10CastResultE+16, 272(%rsp)
	movq	$0, 448(%rsp)
	movl	$0, 456(%rsp)
	movl	204(%r15), %eax
	movl	%eax, 440(%rsp)
	movl	200(%r15), %ebp
.Ltmp97:
	leaq	96(%rsp), %r14
	movq	%r14, %rdi
	callq	_ZN21btConvexInternalShapeC2Ev
.Ltmp98:
# BB#1:
	movq	$_ZTV13btSphereShape+16, 96(%rsp)
	movl	$8, 104(%rsp)
	movl	%ebp, 136(%rsp)
	movl	%ebp, 152(%rsp)
.Ltmp100:
	leaq	160(%rsp), %rdi
	callq	_ZN23btPolyhedralConvexShapeC2Ev
.Ltmp101:
# BB#2:
	movq	$_ZTV15btTriangleShape+16, 160(%rsp)
	movl	$1, 168(%rsp)
	movups	(%rbx), %xmm0
	movups	%xmm0, 224(%rsp)
	movups	16(%rbx), %xmm0
	movups	%xmm0, 240(%rsp)
	movups	32(%rbx), %xmm0
	movups	%xmm0, 256(%rsp)
	movb	$0, 792(%rsp)
.Ltmp103:
	movq	%rsp, %rdi
	leaq	96(%rsp), %r14
	leaq	160(%rsp), %rdx
	leaq	464(%rsp), %rcx
	movq	%r14, %rsi
	callq	_ZN22btSubsimplexConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
.Ltmp104:
# BB#3:
	leaq	8(%r15), %rsi
	leaq	72(%r15), %rdx
.Ltmp106:
	movq	%rsp, %rdi
	leaq	32(%rsp), %rcx
	leaq	272(%rsp), %r9
	movq	%rcx, %r8
	callq	_ZN22btSubsimplexConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
.Ltmp107:
# BB#4:
	testb	%al, %al
	je	.LBB18_7
# BB#5:
	movss	204(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	440(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB18_7
# BB#6:
	movss	%xmm0, 204(%r15)
.LBB18_7:
.Ltmp111:
	movq	%rsp, %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp112:
# BB#8:
.Ltmp116:
	leaq	160(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp117:
# BB#9:
.Ltmp122:
	leaq	96(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp123:
# BB#10:
	addq	$824, %rsp              # imm = 0x338
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_18:
.Ltmp124:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB18_14:
.Ltmp118:
	jmp	.LBB18_20
.LBB18_13:
.Ltmp113:
	jmp	.LBB18_16
.LBB18_12:
.Ltmp108:
	movq	%rax, %rbx
.Ltmp109:
	movq	%rsp, %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp110:
	jmp	.LBB18_17
.LBB18_15:
.Ltmp105:
.LBB18_16:
	movq	%rax, %rbx
.LBB18_17:
.Ltmp114:
	leaq	160(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp115:
	jmp	.LBB18_21
.LBB18_19:
.Ltmp102:
.LBB18_20:
	movq	%rax, %rbx
.LBB18_21:
.Ltmp119:
	movq	%r14, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp120:
# BB#22:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB18_23:
.Ltmp121:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_11:
.Ltmp99:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end18:
	.size	_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallback15processTriangleEP9btVector3ii, .Lfunc_end18-_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallback15processTriangleEP9btVector3ii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp97-.Lfunc_begin10  # >> Call Site 1 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin10  #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp100-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin10 #     jumps to .Ltmp102
	.byte	0                       #   On action: cleanup
	.long	.Ltmp103-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin10 #     jumps to .Ltmp105
	.byte	0                       #   On action: cleanup
	.long	.Ltmp106-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin10 #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp111-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Ltmp112-.Ltmp111       #   Call between .Ltmp111 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin10 #     jumps to .Ltmp113
	.byte	0                       #   On action: cleanup
	.long	.Ltmp116-.Lfunc_begin10 # >> Call Site 6 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin10 #     jumps to .Ltmp118
	.byte	0                       #   On action: cleanup
	.long	.Ltmp122-.Lfunc_begin10 # >> Call Site 7 <<
	.long	.Ltmp123-.Ltmp122       #   Call between .Ltmp122 and .Ltmp123
	.long	.Ltmp124-.Lfunc_begin10 #     jumps to .Ltmp124
	.byte	0                       #   On action: cleanup
	.long	.Ltmp123-.Lfunc_begin10 # >> Call Site 8 <<
	.long	.Ltmp109-.Ltmp123       #   Call between .Ltmp123 and .Ltmp109
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin10 # >> Call Site 9 <<
	.long	.Ltmp120-.Ltmp109       #   Call between .Ltmp109 and .Ltmp120
	.long	.Ltmp121-.Lfunc_begin10 #     jumps to .Ltmp121
	.byte	1                       #   On action: 1
	.long	.Ltmp120-.Lfunc_begin10 # >> Call Site 10 <<
	.long	.Lfunc_end18-.Ltmp120   #   Call between .Ltmp120 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN12btConvexCast10CastResultD2Ev,"axG",@progbits,_ZN12btConvexCast10CastResultD2Ev,comdat
	.weak	_ZN12btConvexCast10CastResultD2Ev
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResultD2Ev,@function
_ZN12btConvexCast10CastResultD2Ev:      # @_ZN12btConvexCast10CastResultD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end19:
	.size	_ZN12btConvexCast10CastResultD2Ev, .Lfunc_end19-_ZN12btConvexCast10CastResultD2Ev
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResult9DebugDrawEf,"axG",@progbits,_ZN12btConvexCast10CastResult9DebugDrawEf,comdat
	.weak	_ZN12btConvexCast10CastResult9DebugDrawEf
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResult9DebugDrawEf,@function
_ZN12btConvexCast10CastResult9DebugDrawEf: # @_ZN12btConvexCast10CastResult9DebugDrawEf
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end20:
	.size	_ZN12btConvexCast10CastResult9DebugDrawEf, .Lfunc_end20-_ZN12btConvexCast10CastResult9DebugDrawEf
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,"axG",@progbits,_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,comdat
	.weak	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,@function
_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform: # @_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end21:
	.size	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform, .Lfunc_end21-_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResultD0Ev,"axG",@progbits,_ZN12btConvexCast10CastResultD0Ev,comdat
	.weak	_ZN12btConvexCast10CastResultD0Ev
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResultD0Ev,@function
_ZN12btConvexCast10CastResultD0Ev:      # @_ZN12btConvexCast10CastResultD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end22:
	.size	_ZN12btConvexCast10CastResultD0Ev, .Lfunc_end22-_ZN12btConvexCast10CastResultD0Ev
	.cfi_endproc

	.section	.text._ZN15btTriangleShapeD0Ev,"axG",@progbits,_ZN15btTriangleShapeD0Ev,comdat
	.weak	_ZN15btTriangleShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN15btTriangleShapeD0Ev,@function
_ZN15btTriangleShapeD0Ev:               # @_ZN15btTriangleShapeD0Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi120:
	.cfi_def_cfa_offset 32
.Lcfi121:
	.cfi_offset %rbx, -24
.Lcfi122:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp125:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp126:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB23_2:
.Ltmp127:
	movq	%rax, %r14
.Ltmp128:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp129:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB23_4:
.Ltmp130:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end23:
	.size	_ZN15btTriangleShapeD0Ev, .Lfunc_end23-_ZN15btTriangleShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp125-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp126-.Ltmp125       #   Call between .Ltmp125 and .Ltmp126
	.long	.Ltmp127-.Lfunc_begin11 #     jumps to .Ltmp127
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp128-.Ltmp126       #   Call between .Ltmp126 and .Ltmp128
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp129-.Ltmp128       #   Call between .Ltmp128 and .Ltmp129
	.long	.Ltmp130-.Lfunc_begin11 #     jumps to .Ltmp130
	.byte	1                       #   On action: 1
	.long	.Ltmp129-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Lfunc_end23-.Ltmp129   #   Call between .Ltmp129 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_,"axG",@progbits,_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_,comdat
	.weak	_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	120(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end24:
	.size	_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end24-_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end25:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end25-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3,"axG",@progbits,_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3,comdat
	.weak	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3: # @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
	retq
.Lfunc_end26:
	.size	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end26-_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape7getNameEv,"axG",@progbits,_ZNK15btTriangleShape7getNameEv,comdat
	.weak	_ZNK15btTriangleShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape7getNameEv,@function
_ZNK15btTriangleShape7getNameEv:        # @_ZNK15btTriangleShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end27:
	.size	_ZNK15btTriangleShape7getNameEv, .Lfunc_end27-_ZNK15btTriangleShape7getNameEv
	.cfi_endproc

	.section	.text._ZN21btConvexInternalShape9setMarginEf,"axG",@progbits,_ZN21btConvexInternalShape9setMarginEf,comdat
	.weak	_ZN21btConvexInternalShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape9setMarginEf,@function
_ZN21btConvexInternalShape9setMarginEf: # @_ZN21btConvexInternalShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end28:
	.size	_ZN21btConvexInternalShape9setMarginEf, .Lfunc_end28-_ZN21btConvexInternalShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end29:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end29-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3,"axG",@progbits,_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3,comdat
	.weak	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	80(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	64(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	68(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	84(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm3, %xmm6
	addps	%xmm5, %xmm6
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	88(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	addps	%xmm6, %xmm5
	mulss	96(%rdi), %xmm2
	mulss	100(%rdi), %xmm1
	addss	%xmm2, %xmm1
	mulss	104(%rdi), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm5, -16(%rsp)
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm1, -12(%rsp)
	movss	%xmm0, -8(%rsp)
	movl	$0, -4(%rsp)
	xorl	%eax, %eax
	ucomiss	%xmm5, %xmm1
	seta	%al
	ucomiss	-16(%rsp,%rax,4), %xmm0
	movl	$2, %ecx
	cmovbeq	%rax, %rcx
	shlq	$4, %rcx
	movsd	64(%rdi,%rcx), %xmm0    # xmm0 = mem[0],zero
	movsd	72(%rdi,%rcx), %xmm1    # xmm1 = mem[0],zero
	retq
.Lfunc_end30:
	.size	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end30-_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,"axG",@progbits,_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,comdat
	.weak	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	jle	.LBB31_3
# BB#1:                                 # %.lr.ph
	movl	%ecx, %eax
	addq	$8, %rsi
	movl	$2, %r8d
	.p2align	4, 0x90
.LBB31_2:                               # =>This Inner Loop Header: Depth=1
	movss	-8(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	-4(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	80(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	64(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	68(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	84(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm3, %xmm6
	addps	%xmm5, %xmm6
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	88(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	addps	%xmm6, %xmm5
	mulss	96(%rdi), %xmm2
	mulss	100(%rdi), %xmm1
	addss	%xmm2, %xmm1
	mulss	104(%rdi), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm5, -16(%rsp)
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm1, -12(%rsp)
	movss	%xmm0, -8(%rsp)
	movl	$0, -4(%rsp)
	xorl	%ecx, %ecx
	ucomiss	%xmm5, %xmm1
	seta	%cl
	ucomiss	-16(%rsp,%rcx,4), %xmm0
	cmovaq	%r8, %rcx
	shlq	$4, %rcx
	movups	64(%rdi,%rcx), %xmm0
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	addq	$16, %rsi
	decq	%rax
	jne	.LBB31_2
.LBB31_3:                               # %._crit_edge
	retq
.Lfunc_end31:
	.size	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end31-_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	movl	$2, %eax
	retq
.Lfunc_end32:
	.size	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end32-_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI33_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI33_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi125:
	.cfi_def_cfa_offset 32
.Lcfi126:
	.cfi_offset %rbx, -24
.Lcfi127:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %ebp
	movss	64(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	80(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	movsd	84(%rdi), %xmm1         # xmm1 = mem[0],zero
	movsd	68(%rdi), %xmm4         # xmm4 = mem[0],zero
	subps	%xmm4, %xmm1
	movss	100(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	movss	96(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	104(%rdi), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	pshufd	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	subps	%xmm3, %xmm6
	movaps	%xmm1, %xmm3
	mulps	%xmm6, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm0
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm1, %xmm6
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	shufps	$0, %xmm1, %xmm4        # xmm4 = xmm4[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm4      # xmm4 = xmm4[2,0],xmm1[2,3]
	mulps	%xmm4, %xmm2
	subps	%xmm2, %xmm3
	subss	%xmm6, %xmm0
	movaps	%xmm3, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm3, (%rbx)
	movlps	%xmm2, 8(%rbx)
	mulss	%xmm3, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB33_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB33_2:                               # %.split
	movss	.LCPI33_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	%xmm2, 4(%rbx)
	mulss	8(%rbx), %xmm0
	movss	%xmm0, 8(%rbx)
	testl	%ebp, %ebp
	je	.LBB33_4
# BB#3:
	movaps	.LCPI33_1(%rip), %xmm3  # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm3, %xmm1
	movss	%xmm1, (%rbx)
	xorps	%xmm3, %xmm2
	movss	%xmm2, 4(%rbx)
	xorps	%xmm3, %xmm0
	movss	%xmm0, 8(%rbx)
.LBB33_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end33:
	.size	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end33-_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape14getNumVerticesEv,"axG",@progbits,_ZNK15btTriangleShape14getNumVerticesEv,comdat
	.weak	_ZNK15btTriangleShape14getNumVerticesEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape14getNumVerticesEv,@function
_ZNK15btTriangleShape14getNumVerticesEv: # @_ZNK15btTriangleShape14getNumVerticesEv
	.cfi_startproc
# BB#0:
	movl	$3, %eax
	retq
.Lfunc_end34:
	.size	_ZNK15btTriangleShape14getNumVerticesEv, .Lfunc_end34-_ZNK15btTriangleShape14getNumVerticesEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape11getNumEdgesEv,"axG",@progbits,_ZNK15btTriangleShape11getNumEdgesEv,comdat
	.weak	_ZNK15btTriangleShape11getNumEdgesEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape11getNumEdgesEv,@function
_ZNK15btTriangleShape11getNumEdgesEv:   # @_ZNK15btTriangleShape11getNumEdgesEv
	.cfi_startproc
# BB#0:
	movl	$3, %eax
	retq
.Lfunc_end35:
	.size	_ZNK15btTriangleShape11getNumEdgesEv, .Lfunc_end35-_ZNK15btTriangleShape11getNumEdgesEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape7getEdgeEiR9btVector3S1_,"axG",@progbits,_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_,comdat
	.weak	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_,@function
_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_: # @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 32
.Lcfi131:
	.cfi_offset %rbx, -32
.Lcfi132:
	.cfi_offset %r14, -24
.Lcfi133:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*168(%rax)
	movq	(%rbx), %rax
	movq	168(%rax), %rax
	leal	1(%r15), %ecx
	movslq	%ecx, %rcx
	imulq	$1431655766, %rcx, %rcx # imm = 0x55555556
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$32, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx,2), %ecx
	negl	%ecx
	leal	1(%r15,%rcx), %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.Lfunc_end36:
	.size	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_, .Lfunc_end36-_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape9getVertexEiR9btVector3,"axG",@progbits,_ZNK15btTriangleShape9getVertexEiR9btVector3,comdat
	.weak	_ZNK15btTriangleShape9getVertexEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape9getVertexEiR9btVector3,@function
_ZNK15btTriangleShape9getVertexEiR9btVector3: # @_ZNK15btTriangleShape9getVertexEiR9btVector3
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	shlq	$4, %rax
	movups	64(%rdi,%rax), %xmm0
	movups	%xmm0, (%rdx)
	retq
.Lfunc_end37:
	.size	_ZNK15btTriangleShape9getVertexEiR9btVector3, .Lfunc_end37-_ZNK15btTriangleShape9getVertexEiR9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape12getNumPlanesEv,"axG",@progbits,_ZNK15btTriangleShape12getNumPlanesEv,comdat
	.weak	_ZNK15btTriangleShape12getNumPlanesEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape12getNumPlanesEv,@function
_ZNK15btTriangleShape12getNumPlanesEv:  # @_ZNK15btTriangleShape12getNumPlanesEv
	.cfi_startproc
# BB#0:
	movl	$1, %eax
	retq
.Lfunc_end38:
	.size	_ZNK15btTriangleShape12getNumPlanesEv, .Lfunc_end38-_ZNK15btTriangleShape12getNumPlanesEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape8getPlaneER9btVector3S1_i,"axG",@progbits,_ZNK15btTriangleShape8getPlaneER9btVector3S1_i,comdat
	.weak	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i,@function
_ZNK15btTriangleShape8getPlaneER9btVector3S1_i: # @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
	movq	%rsi, %rdx
	movq	(%rdi), %rsi
	movq	200(%rsi), %rax
	movl	%ecx, %esi
	movq	%r8, %rcx
	jmpq	*%rax                   # TAILCALL
.Lfunc_end39:
	.size	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i, .Lfunc_end39-_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI40_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI40_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK15btTriangleShape8isInsideERK9btVector3f,"axG",@progbits,_ZNK15btTriangleShape8isInsideERK9btVector3f,comdat
	.weak	_ZNK15btTriangleShape8isInsideERK9btVector3f
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape8isInsideERK9btVector3f,@function
_ZNK15btTriangleShape8isInsideERK9btVector3f: # @_ZNK15btTriangleShape8isInsideERK9btVector3f
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi134:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi135:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi136:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi137:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 48
	subq	$144, %rsp
.Lcfi139:
	.cfi_def_cfa_offset 192
.Lcfi140:
	.cfi_offset %rbx, -48
.Lcfi141:
	.cfi_offset %r12, -40
.Lcfi142:
	.cfi_offset %r14, -32
.Lcfi143:
	.cfi_offset %r15, -24
.Lcfi144:
	.cfi_offset %rbp, -16
	movaps	%xmm0, %xmm8
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movss	80(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	84(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm3
	movss	88(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm6
	movss	96(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm5
	movss	100(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm7
	movss	104(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movaps	%xmm6, %xmm2
	mulss	%xmm5, %xmm6
	mulss	%xmm3, %xmm5
	mulss	%xmm1, %xmm3
	mulss	%xmm7, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm6
	mulss	%xmm0, %xmm7
	subss	%xmm5, %xmm7
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm7, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB40_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	movaps	%xmm8, 32(%rsp)         # 16-byte Spill
	movss	%xmm7, 16(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	16(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm8         # 16-byte Reload
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	movaps	64(%rsp), %xmm6         # 16-byte Reload
.LBB40_2:                               # %.split
	movss	.LCPI40_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm5
	divss	%xmm0, %xmm5
	mulss	%xmm5, %xmm3
	mulss	%xmm5, %xmm6
	mulss	%xmm7, %xmm5
	movss	64(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%r14), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	movss	4(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movaps	%xmm4, 48(%rsp)         # 16-byte Spill
	movaps	%xmm4, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	movaps	%xmm6, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm3, %xmm1
	addps	%xmm0, %xmm1
	movaps	%xmm5, 32(%rsp)         # 16-byte Spill
	movaps	%xmm5, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	72(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	mulps	%xmm2, %xmm0
	addps	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	subss	%xmm1, %xmm0
	movaps	.LCPI40_1(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm8, %xmm1
	xorl	%eax, %eax
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	ucomiss	%xmm1, %xmm0
	jb	.LBB40_11
# BB#3:                                 # %.split
	ucomiss	%xmm0, %xmm8
	jb	.LBB40_11
# BB#4:                                 # %.preheader
	xorl	%ebp, %ebp
	movq	%rsp, %r15
	leaq	80(%rsp), %r12
	.p2align	4, 0x90
.LBB40_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	*160(%rax)
	movss	80(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	84(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	(%rsp), %xmm5
	subss	4(%rsp), %xmm0
	movss	88(%rsp), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	8(%rsp), %xmm6
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	movaps	%xmm4, %xmm7
	mulss	%xmm0, %xmm7
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm2, %xmm1
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm7
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	mulss	%xmm3, %xmm6
	movaps	%xmm4, %xmm1
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm6
	mulss	%xmm2, %xmm5
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm5
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB40_7
# BB#6:                                 # %call.sqrt86
                                        #   in Loop: Header=BB40_5 Depth=1
	movaps	%xmm5, 128(%rsp)        # 16-byte Spill
	movaps	%xmm6, 112(%rsp)        # 16-byte Spill
	movaps	%xmm7, 96(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	96(%rsp), %xmm7         # 16-byte Reload
	movaps	112(%rsp), %xmm6        # 16-byte Reload
	movaps	128(%rsp), %xmm5        # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB40_7:                               # %.split85
                                        #   in Loop: Header=BB40_5 Depth=1
	movss	.LCPI40_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm7
	mulss	%xmm0, %xmm6
	mulss	%xmm0, %xmm5
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	(%r14), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm2, %xmm7
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm3, %xmm6
	addps	%xmm7, %xmm6
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	mulps	%xmm5, %xmm1
	addps	%xmm6, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm0, %xmm1
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	ucomiss	%xmm1, %xmm0
	ja	.LBB40_10
# BB#8:                                 #   in Loop: Header=BB40_5 Depth=1
	incl	%ebp
	cmpl	$2, %ebp
	jle	.LBB40_5
# BB#9:
	movb	$1, %al
	jmp	.LBB40_11
.LBB40_10:
	xorl	%eax, %eax
.LBB40_11:                              # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end40:
	.size	_ZNK15btTriangleShape8isInsideERK9btVector3f, .Lfunc_end40-_ZNK15btTriangleShape8isInsideERK9btVector3f
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI41_0:
	.long	1065353216              # float 1
	.section	.text._ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_,"axG",@progbits,_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_,comdat
	.weak	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_,@function
_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_: # @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 32
.Lcfi148:
	.cfi_offset %rbx, -32
.Lcfi149:
	.cfi_offset %r14, -24
.Lcfi150:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movss	64(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	80(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	movsd	84(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	68(%rbx), %xmm4         # xmm4 = mem[0],zero
	subps	%xmm4, %xmm1
	movss	100(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	movss	96(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	104(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	pshufd	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	subps	%xmm3, %xmm6
	movaps	%xmm1, %xmm3
	mulps	%xmm6, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm0
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm1, %xmm6
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	shufps	$0, %xmm1, %xmm4        # xmm4 = xmm4[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm4      # xmm4 = xmm4[2,0],xmm1[2,3]
	mulps	%xmm4, %xmm2
	subps	%xmm2, %xmm3
	subss	%xmm6, %xmm0
	movaps	%xmm3, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm3, (%r15)
	movlps	%xmm2, 8(%r15)
	mulss	%xmm3, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB41_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB41_2:                               # %.split
	movss	.LCPI41_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%r15)
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 4(%r15)
	mulss	8(%r15), %xmm0
	movss	%xmm0, 8(%r15)
	movups	64(%rbx), %xmm0
	movups	%xmm0, (%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end41:
	.size	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_, .Lfunc_end41-_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.cfi_endproc

	.section	.text._ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10growTablesERKS2_,"axG",@progbits,_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10growTablesERKS2_,comdat
	.weak	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10growTablesERKS2_
	.p2align	4, 0x90
	.type	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10growTablesERKS2_,@function
_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10growTablesERKS2_: # @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10growTablesERKS2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi151:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi152:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi153:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi154:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi155:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi157:
	.cfi_def_cfa_offset 80
.Lcfi158:
	.cfi_offset %rbx, -56
.Lcfi159:
	.cfi_offset %r12, -48
.Lcfi160:
	.cfi_offset %r13, -40
.Lcfi161:
	.cfi_offset %r14, -32
.Lcfi162:
	.cfi_offset %r15, -24
.Lcfi163:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movl	4(%r13), %r12d
	movl	72(%r13), %r14d
	cmpl	%r14d, %r12d
	jge	.LBB42_72
# BB#1:
	movslq	%r14d, %rbp
	movslq	%r12d, %rcx
	cmpl	%r14d, 8(%r13)
	jge	.LBB42_4
# BB#2:
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	testl	%r14d, %r14d
	je	.LBB42_5
# BB#3:
	leaq	(,%rbp,4), %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	4(%r13), %ecx
	jmp	.LBB42_6
.LBB42_4:                               # %..lr.ph.i40_crit_edge
	leaq	16(%r13), %rbx
	movq	16(%r13), %r15
	jmp	.LBB42_33
.LBB42_5:
	xorl	%r15d, %r15d
	movl	%r12d, %ecx
.LBB42_6:                               # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i28
	movq	16(%r13), %rdi
	testl	%ecx, %ecx
	jle	.LBB42_9
# BB#7:                                 # %.lr.ph.i.i.i30
	movslq	%ecx, %rax
	cmpl	$8, %ecx
	jae	.LBB42_11
# BB#8:
	xorl	%edx, %edx
	jmp	.LBB42_24
.LBB42_9:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i34
	testq	%rdi, %rdi
	jne	.LBB42_30
# BB#10:                                # %_ZN20btAlignedObjectArrayIiE7reserveEi.exit.preheader.thread26.i36
	leaq	24(%r13), %rbx
	jmp	.LBB42_32
.LBB42_11:                              # %min.iters.checked
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB42_15
# BB#12:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rsi
	cmpq	%rsi, %r15
	jae	.LBB42_16
# BB#13:                                # %vector.memcheck
	leaq	(%r15,%rax,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB42_16
.LBB42_15:
	xorl	%edx, %edx
.LBB42_24:                              # %scalar.ph.preheader
	movq	%rbp, %rbx
	subl	%edx, %ecx
	leaq	-1(%rax), %rsi
	subq	%rdx, %rsi
	andq	$7, %rcx
	je	.LBB42_27
# BB#25:                                # %scalar.ph.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB42_26:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %ebp
	movl	%ebp, (%r15,%rdx,4)
	incq	%rdx
	incq	%rcx
	jne	.LBB42_26
.LBB42_27:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rsi
	movq	%rbx, %rbp
	jb	.LBB42_30
# BB#28:                                # %scalar.ph.preheader.new
	subq	%rdx, %rax
	leaq	28(%rdi,%rdx,4), %rcx
	leaq	28(%r15,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB42_29:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %rax
	jne	.LBB42_29
.LBB42_30:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i37
	leaq	24(%r13), %rbx
	cmpb	$0, 24(%r13)
	je	.LBB42_32
# BB#31:
	callq	_Z21btAlignedFreeInternalPv
.LBB42_32:                              # %.lr.ph.i40.sink.split
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	16(%r13), %rax
	movb	$1, (%rbx)
	movq	%rax, %rbx
	movq	%r15, 16(%r13)
	movl	%r14d, 8(%r13)
.LBB42_33:                              # %.lr.ph.i40
	leaq	(%r15,%rcx,4), %rdi
	shlq	$2, %rbp
	shlq	$2, %rcx
	movq	%rbp, %rdx
	subq	%rcx, %rdx
	xorl	%esi, %esi
	callq	memset
	movl	%r14d, 4(%r13)
	movl	36(%r13), %ecx
	cmpl	%r14d, %ecx
	jge	.LBB42_67
# BB#34:
	movslq	%ecx, %rax
	cmpl	%r14d, 40(%r13)
	jge	.LBB42_37
# BB#35:
	testl	%r14d, %r14d
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB42_38
# BB#36:
	movl	$16, %esi
	movq	%rbp, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	36(%r13), %ecx
	jmp	.LBB42_39
.LBB42_37:                              # %..lr.ph.i_crit_edge
	movq	48(%r13), %r15
	jmp	.LBB42_66
.LBB42_38:
	xorl	%r15d, %r15d
.LBB42_39:                              # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i
	movq	48(%r13), %rdi
	testl	%ecx, %ecx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	jle	.LBB42_42
# BB#40:                                # %.lr.ph.i.i.i
	movslq	%ecx, %rax
	cmpl	$8, %ecx
	jae	.LBB42_44
# BB#41:
	xorl	%edx, %edx
	jmp	.LBB42_57
.LBB42_42:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i
	testq	%rdi, %rdi
	jne	.LBB42_63
# BB#43:                                # %_ZN20btAlignedObjectArrayIiE7reserveEi.exit.preheader.thread26.i
	leaq	56(%r13), %rbx
	jmp	.LBB42_65
.LBB42_44:                              # %min.iters.checked81
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB42_48
# BB#45:                                # %vector.memcheck93
	leaq	(%rdi,%rax,4), %rsi
	cmpq	%rsi, %r15
	jae	.LBB42_49
# BB#46:                                # %vector.memcheck93
	leaq	(%r15,%rax,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB42_49
.LBB42_48:
	xorl	%edx, %edx
.LBB42_57:                              # %scalar.ph79.preheader
	movq	%rbp, %rbx
	subl	%edx, %ecx
	leaq	-1(%rax), %rsi
	subq	%rdx, %rsi
	andq	$7, %rcx
	je	.LBB42_60
# BB#58:                                # %scalar.ph79.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB42_59:                              # %scalar.ph79.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %ebp
	movl	%ebp, (%r15,%rdx,4)
	incq	%rdx
	incq	%rcx
	jne	.LBB42_59
.LBB42_60:                              # %scalar.ph79.prol.loopexit
	cmpq	$7, %rsi
	movq	%rbx, %rbp
	jb	.LBB42_63
# BB#61:                                # %scalar.ph79.preheader.new
	subq	%rdx, %rax
	leaq	28(%rdi,%rdx,4), %rcx
	leaq	28(%r15,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB42_62:                              # %scalar.ph79
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %rax
	jne	.LBB42_62
.LBB42_63:                              # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i
	leaq	56(%r13), %rbx
	cmpb	$0, 56(%r13)
	je	.LBB42_65
# BB#64:
	callq	_Z21btAlignedFreeInternalPv
.LBB42_65:                              # %.lr.ph.i.sink.split
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	$1, (%rbx)
	movq	%r15, 48(%r13)
	movl	%r14d, 40(%r13)
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB42_66:                              # %.lr.ph.i
	leaq	(%r15,%rax,4), %rdi
	shlq	$2, %rax
	subq	%rax, %rbp
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
.LBB42_67:                              # %_ZN20btAlignedObjectArrayIiE6resizeEiRKi.exit
	movl	%r14d, 36(%r13)
	testl	%r14d, %r14d
	jle	.LBB42_69
# BB#68:                                # %.lr.ph55
	movq	(%rbx), %rdi
	decl	%r14d
	leaq	4(,%r14,4), %r14
	movl	$255, %esi
	movq	%r14, %rdx
	callq	memset
	movq	48(%r13), %rdi
	movl	$255, %esi
	movq	%r14, %rdx
	callq	memset
.LBB42_69:                              # %.preheader
	testl	%r12d, %r12d
	jle	.LBB42_72
# BB#70:                                # %.lr.ph
	movq	112(%r13), %rax
	movq	16(%r13), %rcx
	movq	48(%r13), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB42_71:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rsi,4), %ebp
	movl	%ebp, %edi
	shll	$15, %edi
	notl	%edi
	addl	%ebp, %edi
	movl	%edi, %ebp
	sarl	$10, %ebp
	xorl	%edi, %ebp
	leal	(%rbp,%rbp,8), %edi
	movl	%edi, %ebp
	sarl	$6, %ebp
	xorl	%edi, %ebp
	movl	%ebp, %edi
	shll	$11, %edi
	notl	%edi
	addl	%ebp, %edi
	movl	%edi, %ebp
	sarl	$16, %ebp
	xorl	%edi, %ebp
	movl	72(%r13), %edi
	decl	%edi
	andl	%ebp, %edi
	movslq	%edi, %rdi
	movl	(%rcx,%rdi,4), %ebp
	movl	%ebp, (%rdx,%rsi,4)
	movl	%esi, (%rcx,%rdi,4)
	incq	%rsi
	cmpq	%rsi, %r12
	jne	.LBB42_71
.LBB42_72:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB42_16:                              # %vector.body.preheader
	movq	%rbp, %r8
	leaq	-8(%rdx), %rbp
	movl	%ebp, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB42_19
# BB#17:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB42_18:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r15,%rbx,4)
	movups	%xmm1, 16(%r15,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB42_18
	jmp	.LBB42_20
.LBB42_49:                              # %vector.body77.preheader
	movq	%rbp, %r8
	leaq	-8(%rdx), %rbp
	movl	%ebp, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB42_52
# BB#50:                                # %vector.body77.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB42_51:                              # %vector.body77.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r15,%rbx,4)
	movups	%xmm1, 16(%r15,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB42_51
	jmp	.LBB42_53
.LBB42_19:
	xorl	%ebx, %ebx
.LBB42_20:                              # %vector.body.prol.loopexit
	cmpq	$24, %rbp
	jb	.LBB42_23
# BB#21:                                # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rbx, %rsi
	leaq	112(%rdi,%rbx,4), %rbp
	leaq	112(%r15,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB42_22:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-32, %rsi
	jne	.LBB42_22
.LBB42_23:                              # %middle.block
	cmpq	%rdx, %rax
	movq	%r8, %rbp
	je	.LBB42_30
	jmp	.LBB42_24
.LBB42_52:
	xorl	%ebx, %ebx
.LBB42_53:                              # %vector.body77.prol.loopexit
	cmpq	$24, %rbp
	jb	.LBB42_56
# BB#54:                                # %vector.body77.preheader.new
	movq	%rdx, %rsi
	subq	%rbx, %rsi
	leaq	112(%rdi,%rbx,4), %rbp
	leaq	112(%r15,%rbx,4), %rbx
.LBB42_55:                              # %vector.body77
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-32, %rsi
	jne	.LBB42_55
.LBB42_56:                              # %middle.block78
	cmpq	%rdx, %rax
	movq	%r8, %rbp
	je	.LBB42_63
	jmp	.LBB42_57
.Lfunc_end42:
	.size	_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10growTablesERKS2_, .Lfunc_end42-_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10growTablesERKS2_
	.cfi_endproc

	.type	_ZTV35btSoftBodyConcaveCollisionAlgorithm,@object # @_ZTV35btSoftBodyConcaveCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTV35btSoftBodyConcaveCollisionAlgorithm
	.p2align	3
_ZTV35btSoftBodyConcaveCollisionAlgorithm:
	.quad	0
	.quad	_ZTI35btSoftBodyConcaveCollisionAlgorithm
	.quad	_ZN35btSoftBodyConcaveCollisionAlgorithmD2Ev
	.quad	_ZN35btSoftBodyConcaveCollisionAlgorithmD0Ev
	.quad	_ZN35btSoftBodyConcaveCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN35btSoftBodyConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.size	_ZTV35btSoftBodyConcaveCollisionAlgorithm, 56

	.type	_ZTV26btSoftBodyTriangleCallback,@object # @_ZTV26btSoftBodyTriangleCallback
	.globl	_ZTV26btSoftBodyTriangleCallback
	.p2align	3
_ZTV26btSoftBodyTriangleCallback:
	.quad	0
	.quad	_ZTI26btSoftBodyTriangleCallback
	.quad	_ZN26btSoftBodyTriangleCallbackD2Ev
	.quad	_ZN26btSoftBodyTriangleCallbackD0Ev
	.quad	_ZN26btSoftBodyTriangleCallback15processTriangleEP9btVector3ii
	.size	_ZTV26btSoftBodyTriangleCallback, 40

	.type	_ZTS35btSoftBodyConcaveCollisionAlgorithm,@object # @_ZTS35btSoftBodyConcaveCollisionAlgorithm
	.globl	_ZTS35btSoftBodyConcaveCollisionAlgorithm
	.p2align	4
_ZTS35btSoftBodyConcaveCollisionAlgorithm:
	.asciz	"35btSoftBodyConcaveCollisionAlgorithm"
	.size	_ZTS35btSoftBodyConcaveCollisionAlgorithm, 38

	.type	_ZTS20btCollisionAlgorithm,@object # @_ZTS20btCollisionAlgorithm
	.section	.rodata._ZTS20btCollisionAlgorithm,"aG",@progbits,_ZTS20btCollisionAlgorithm,comdat
	.weak	_ZTS20btCollisionAlgorithm
	.p2align	4
_ZTS20btCollisionAlgorithm:
	.asciz	"20btCollisionAlgorithm"
	.size	_ZTS20btCollisionAlgorithm, 23

	.type	_ZTI20btCollisionAlgorithm,@object # @_ZTI20btCollisionAlgorithm
	.section	.rodata._ZTI20btCollisionAlgorithm,"aG",@progbits,_ZTI20btCollisionAlgorithm,comdat
	.weak	_ZTI20btCollisionAlgorithm
	.p2align	3
_ZTI20btCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS20btCollisionAlgorithm
	.size	_ZTI20btCollisionAlgorithm, 16

	.type	_ZTI35btSoftBodyConcaveCollisionAlgorithm,@object # @_ZTI35btSoftBodyConcaveCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTI35btSoftBodyConcaveCollisionAlgorithm
	.p2align	4
_ZTI35btSoftBodyConcaveCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS35btSoftBodyConcaveCollisionAlgorithm
	.quad	_ZTI20btCollisionAlgorithm
	.size	_ZTI35btSoftBodyConcaveCollisionAlgorithm, 24

	.type	_ZTS26btSoftBodyTriangleCallback,@object # @_ZTS26btSoftBodyTriangleCallback
	.globl	_ZTS26btSoftBodyTriangleCallback
	.p2align	4
_ZTS26btSoftBodyTriangleCallback:
	.asciz	"26btSoftBodyTriangleCallback"
	.size	_ZTS26btSoftBodyTriangleCallback, 29

	.type	_ZTI26btSoftBodyTriangleCallback,@object # @_ZTI26btSoftBodyTriangleCallback
	.globl	_ZTI26btSoftBodyTriangleCallback
	.p2align	4
_ZTI26btSoftBodyTriangleCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS26btSoftBodyTriangleCallback
	.quad	_ZTI18btTriangleCallback
	.size	_ZTI26btSoftBodyTriangleCallback, 24

	.type	_ZTVZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback,@object # @_ZTVZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback
	.p2align	3
_ZTVZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback:
	.quad	0
	.quad	_ZTIZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback
	.quad	_ZN18btTriangleCallbackD2Ev
	.quad	_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackD0Ev
	.quad	_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallback15processTriangleEP9btVector3ii
	.size	_ZTVZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback, 40

	.type	_ZTSZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback,@object # @_ZTSZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback
	.p2align	4
_ZTSZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback:
	.asciz	"ZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback"
	.size	_ZTSZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback, 160

	.type	_ZTIZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback,@object # @_ZTIZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback
	.p2align	4
_ZTIZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback
	.quad	_ZTI18btTriangleCallback
	.size	_ZTIZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback, 24

	.type	_ZTVN12btConvexCast10CastResultE,@object # @_ZTVN12btConvexCast10CastResultE
	.section	.rodata._ZTVN12btConvexCast10CastResultE,"aG",@progbits,_ZTVN12btConvexCast10CastResultE,comdat
	.weak	_ZTVN12btConvexCast10CastResultE
	.p2align	3
_ZTVN12btConvexCast10CastResultE:
	.quad	0
	.quad	_ZTIN12btConvexCast10CastResultE
	.quad	_ZN12btConvexCast10CastResult9DebugDrawEf
	.quad	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.quad	_ZN12btConvexCast10CastResultD2Ev
	.quad	_ZN12btConvexCast10CastResultD0Ev
	.size	_ZTVN12btConvexCast10CastResultE, 48

	.type	_ZTSN12btConvexCast10CastResultE,@object # @_ZTSN12btConvexCast10CastResultE
	.section	.rodata._ZTSN12btConvexCast10CastResultE,"aG",@progbits,_ZTSN12btConvexCast10CastResultE,comdat
	.weak	_ZTSN12btConvexCast10CastResultE
	.p2align	4
_ZTSN12btConvexCast10CastResultE:
	.asciz	"N12btConvexCast10CastResultE"
	.size	_ZTSN12btConvexCast10CastResultE, 29

	.type	_ZTIN12btConvexCast10CastResultE,@object # @_ZTIN12btConvexCast10CastResultE
	.section	.rodata._ZTIN12btConvexCast10CastResultE,"aG",@progbits,_ZTIN12btConvexCast10CastResultE,comdat
	.weak	_ZTIN12btConvexCast10CastResultE
	.p2align	3
_ZTIN12btConvexCast10CastResultE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN12btConvexCast10CastResultE
	.size	_ZTIN12btConvexCast10CastResultE, 16

	.type	_ZTV15btTriangleShape,@object # @_ZTV15btTriangleShape
	.section	.rodata._ZTV15btTriangleShape,"aG",@progbits,_ZTV15btTriangleShape,comdat
	.weak	_ZTV15btTriangleShape
	.p2align	3
_ZTV15btTriangleShape:
	.quad	0
	.quad	_ZTI15btTriangleShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN15btTriangleShapeD0Ev
	.quad	_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK15btTriangleShape7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	_ZNK15btTriangleShape14getNumVerticesEv
	.quad	_ZNK15btTriangleShape11getNumEdgesEv
	.quad	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.quad	_ZNK15btTriangleShape9getVertexEiR9btVector3
	.quad	_ZNK15btTriangleShape12getNumPlanesEv
	.quad	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.quad	_ZNK15btTriangleShape8isInsideERK9btVector3f
	.quad	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.size	_ZTV15btTriangleShape, 224

	.type	_ZTS15btTriangleShape,@object # @_ZTS15btTriangleShape
	.section	.rodata._ZTS15btTriangleShape,"aG",@progbits,_ZTS15btTriangleShape,comdat
	.weak	_ZTS15btTriangleShape
	.p2align	4
_ZTS15btTriangleShape:
	.asciz	"15btTriangleShape"
	.size	_ZTS15btTriangleShape, 18

	.type	_ZTI15btTriangleShape,@object # @_ZTI15btTriangleShape
	.section	.rodata._ZTI15btTriangleShape,"aG",@progbits,_ZTI15btTriangleShape,comdat
	.weak	_ZTI15btTriangleShape
	.p2align	4
_ZTI15btTriangleShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15btTriangleShape
	.quad	_ZTI23btPolyhedralConvexShape
	.size	_ZTI15btTriangleShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Triangle"
	.size	.L.str, 9


	.globl	_ZN35btSoftBodyConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	.type	_ZN35btSoftBodyConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b,@function
_ZN35btSoftBodyConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b = _ZN35btSoftBodyConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	.globl	_ZN35btSoftBodyConcaveCollisionAlgorithmD1Ev
	.type	_ZN35btSoftBodyConcaveCollisionAlgorithmD1Ev,@function
_ZN35btSoftBodyConcaveCollisionAlgorithmD1Ev = _ZN35btSoftBodyConcaveCollisionAlgorithmD2Ev
	.globl	_ZN26btSoftBodyTriangleCallbackC1EP12btDispatcherP17btCollisionObjectS3_b
	.type	_ZN26btSoftBodyTriangleCallbackC1EP12btDispatcherP17btCollisionObjectS3_b,@function
_ZN26btSoftBodyTriangleCallbackC1EP12btDispatcherP17btCollisionObjectS3_b = _ZN26btSoftBodyTriangleCallbackC2EP12btDispatcherP17btCollisionObjectS3_b
	.globl	_ZN26btSoftBodyTriangleCallbackD1Ev
	.type	_ZN26btSoftBodyTriangleCallbackD1Ev,@function
_ZN26btSoftBodyTriangleCallbackD1Ev = _ZN26btSoftBodyTriangleCallbackD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
