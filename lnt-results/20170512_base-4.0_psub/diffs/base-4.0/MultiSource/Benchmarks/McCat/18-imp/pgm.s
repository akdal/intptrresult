	.text
	.file	"pgm.bc"
	.globl	PGM_InitImage
	.p2align	4, 0x90
	.type	PGM_InitImage,@function
PGM_InitImage:                          # @PGM_InitImage
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, 8(%r14)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	8(%r14), %rdi
	callq	__strdup
	movq	%rax, 16(%r14)
	movq	8(%r14), %rbx
	cmpb	$0, (%rbx)
	je	.LBB0_6
# BB#1:                                 # %.lr.ph21.preheader
	addq	$2, %rbx
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph21
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rbx), %eax
	cmpb	$47, %al
	jne	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	16(%r14), %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movzbl	-1(%rbx), %eax
.LBB0_4:                                # %.backedge18
                                        #   in Loop: Header=BB0_2 Depth=1
	incq	%rbx
	testb	%al, %al
	jne	.LBB0_2
# BB#5:                                 # %._crit_edge22.loopexit
	movq	16(%r14), %rax
.LBB0_6:                                # %._crit_edge22
	cmpb	$0, (%rax)
	je	.LBB0_11
# BB#7:                                 # %.lr.ph.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.LBB0_11
# BB#9:                                 # %.lr.ph
                                        #   in Loop: Header=BB0_8 Depth=1
	incq	%rax
	cmpb	$46, %cl
	jne	.LBB0_8
# BB#10:                                # %.backedge.thread
	movb	$0, -1(%rax)
.LBB0_11:                               # %._crit_edge
	movb	$0, (%r14)
	movq	$0, 72(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	PGM_InitImage, .Lfunc_end0-PGM_InitImage
	.cfi_endproc

	.globl	PGM_FreeImage
	.p2align	4, 0x90
	.type	PGM_FreeImage,@function
PGM_FreeImage:                          # @PGM_FreeImage
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	callq	free
	movq	16(%rbx), %rdi
	callq	free
	movq	48(%rbx), %rdi
	callq	free
	movq	80(%rbx), %rdi
	callq	free
	movb	$0, (%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	PGM_FreeImage, .Lfunc_end1-PGM_FreeImage
	.cfi_endproc

	.globl	PGM_PrintInfo
	.p2align	4, 0x90
	.type	PGM_PrintInfo,@function
PGM_PrintInfo:                          # @PGM_PrintInfo
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpb	$0, (%rbx)
	je	.LBB2_1
# BB#2:
	movq	8(%rbx), %r14
	movl	$47, %esi
	movq	%r14, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%r14, %rsi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	36(%rbx), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	32(%rbx), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movl	40(%rbx), %esi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	printf                  # TAILCALL
.LBB2_1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	PGM_PrintInfo, .Lfunc_end2-PGM_PrintInfo
	.cfi_endproc

	.globl	PGM_GetValue
	.p2align	4, 0x90
	.type	PGM_GetValue,@function
PGM_GetValue:                           # @PGM_GetValue
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -24
.Lcfi16:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	leaq	4(%rsp), %rdx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	callq	fscanf
	testl	%eax, %eax
	jne	.LBB3_5
# BB#1:                                 # %.lr.ph.preheader
	leaq	4(%rsp), %r14
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_3 Depth 2
	movq	24(%rbx), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	fscanf
	cmpl	$0, 4(%rsp)
	jle	.LBB3_4
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rbx), %rdi
	callq	fgetc
	cmpb	$10, %al
	jne	.LBB3_3
.LBB3_4:                                # %.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	24(%rbx), %rdi
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	callq	fscanf
	movq	24(%rbx), %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	fscanf
	testl	%eax, %eax
	je	.LBB3_2
.LBB3_5:                                # %._crit_edge
	movl	4(%rsp), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	PGM_GetValue, .Lfunc_end3-PGM_GetValue
	.cfi_endproc

	.globl	PGM_Open
	.p2align	4, 0x90
	.type	PGM_Open,@function
PGM_Open:                               # @PGM_Open
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movl	$.L.str.7, %esi
	callq	fopen
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.LBB4_1
# BB#2:
	leaq	6(%rsp), %rdi
	movl	$1, %esi
	movl	$2, %edx
	movq	%rax, %rcx
	callq	fread
	movl	$-2, %ebp
	cmpq	$2, %rax
	jne	.LBB4_5
# BB#3:
	leaq	6(%rsp), %rdi
	movl	$.L.str.8, %esi
	movl	$2, %edx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB4_5
# BB#4:
	movq	%rbx, %rdi
	callq	PGM_GetValue
	movl	%eax, 36(%rbx)
	movq	%rbx, %rdi
	callq	PGM_GetValue
	movl	%eax, 32(%rbx)
	movq	%rbx, %rdi
	callq	PGM_GetValue
	movl	%eax, 40(%rbx)
	movb	$1, (%rbx)
	xorl	%ebp, %ebp
	jmp	.LBB4_5
.LBB4_1:
	movl	$-1, %ebp
.LBB4_5:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	PGM_Open, .Lfunc_end4-PGM_Open
	.cfi_endproc

	.globl	PGM_Close
	.p2align	4, 0x90
	.type	PGM_Close,@function
PGM_Close:                              # @PGM_Close
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	jmp	fclose                  # TAILCALL
.Lfunc_end5:
	.size	PGM_Close, .Lfunc_end5-PGM_Close
	.cfi_endproc

	.globl	PGM_LoadImage
	.p2align	4, 0x90
	.type	PGM_LoadImage,@function
PGM_LoadImage:                          # @PGM_LoadImage
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 48
.Lcfi26:
	.cfi_offset %rbx, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	$0, 12(%rsp)
	callq	PGM_Open
	testl	%eax, %eax
	jne	.LBB6_14
# BB#1:
	movl	36(%r15), %eax
	testl	%eax, %eax
	je	.LBB6_3
# BB#2:
	movl	32(%r15), %ecx
	testl	%ecx, %ecx
	je	.LBB6_3
# BB#4:
	imull	%eax, %ecx
	movslq	%ecx, %rdi
	callq	malloc
	movq	%rax, 48(%r15)
	movslq	40(%r15), %r14
	incq	%r14
	movl	$4, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 112(%r15)
	movl	$8, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 120(%r15)
	movq	$0, 72(%r15)
	movq	24(%r15), %rdi
	leaq	12(%rsp), %rdx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	callq	fscanf
	cmpl	$0, 12(%rsp)
	jle	.LBB6_6
	.p2align	4, 0x90
.LBB6_5:                                # %.preheader35
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rdi
	callq	fgetc
	cmpl	$10, %eax
	jne	.LBB6_5
.LBB6_6:                                # %.loopexit
	movq	24(%r15), %rdi
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	callq	fscanf
	movl	32(%r15), %eax
	movl	36(%r15), %edx
	movl	%eax, %ecx
	imull	%edx, %ecx
	testl	%ecx, %ecx
	jle	.LBB6_9
# BB#7:                                 # %.lr.ph39.preheader
	xorl	%ebx, %ebx
	leaq	11(%rsp), %r14
	.p2align	4, 0x90
.LBB6_8:                                # %.lr.ph39
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rcx
	movl	$1, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	fread
	movzbl	11(%rsp), %eax
	movq	48(%r15), %rcx
	movb	%al, (%rcx,%rbx)
	movq	112(%r15), %rcx
	incl	(%rcx,%rax,4)
	incq	%rbx
	movl	32(%r15), %eax
	movl	36(%r15), %edx
	movl	%eax, %ecx
	imull	%edx, %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB6_8
.LBB6_9:                                # %.preheader
	movb	$0, 11(%rsp)
	movl	40(%r15), %ecx
	testl	%ecx, %ecx
	jle	.LBB6_13
# BB#10:                                # %.lr.ph
	movq	112(%r15), %rsi
	movq	120(%r15), %rdi
	imull	%edx, %eax
	cvtsi2sdl	%eax, %xmm0
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_11:                               # =>This Inner Loop Header: Depth=1
	movzbl	%al, %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	(%rsi,%rax,4), %xmm1
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdi,%rax,8)
	incb	%al
	movzbl	%al, %edx
	cmpl	%ecx, %edx
	jl	.LBB6_11
# BB#12:                                # %._crit_edge
	movb	%al, 11(%rsp)
.LBB6_13:
	movq	24(%r15), %rdi
	callq	fclose
	xorl	%eax, %eax
	jmp	.LBB6_14
.LBB6_3:
	movq	24(%r15), %rdi
	callq	fclose
	movl	$-3, %eax
.LBB6_14:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	PGM_LoadImage, .Lfunc_end6-PGM_LoadImage
	.cfi_endproc

	.globl	PGM_WriteBinary
	.p2align	4, 0x90
	.type	PGM_WriteBinary,@function
PGM_WriteBinary:                        # @PGM_WriteBinary
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 48
.Lcfi34:
	.cfi_offset %rbx, -48
.Lcfi35:
	.cfi_offset %r12, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$-3, %r15d
	cmpb	$0, (%rbx)
	je	.LBB7_9
# BB#1:
	cmpl	$0, 36(%rbx)
	je	.LBB7_9
# BB#2:
	cmpl	$0, 32(%rbx)
	je	.LBB7_9
# BB#3:
	movq	16(%rbx), %rbp
	movq	%rbp, %rdi
	callq	strlen
	leaq	8(%rax), %rdi
	callq	malloc
	movq	%rax, %r14
	xorl	%r15d, %r15d
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbp, %rdx
	callq	sprintf
	movq	stdout(%rip), %r12
	movl	$.L.str.10, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	movl	32(%rbx), %ecx
	movl	36(%rbx), %edx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	fprintf
	movl	40(%rbx), %edx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	fprintf
	movl	32(%rbx), %eax
	imull	36(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB7_8
# BB#4:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_5:                                # =>This Inner Loop Header: Depth=1
	movq	48(%rbx), %rax
	movzbl	(%rax,%rbp), %eax
	movzbl	44(%rbx), %ecx
	cmpl	%ecx, %eax
	movl	$0, %eax
	jbe	.LBB7_7
# BB#6:                                 #   in Loop: Header=BB7_5 Depth=1
	movl	40(%rbx), %eax
.LBB7_7:                                #   in Loop: Header=BB7_5 Depth=1
	movsbl	%al, %edi
	movq	%r12, %rsi
	callq	fputc
	incq	%rbp
	movslq	32(%rbx), %rax
	movslq	36(%rbx), %rcx
	imulq	%rax, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB7_5
.LBB7_8:                                # %._crit_edge
	movq	%r14, %rdi
	callq	free
.LBB7_9:
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	PGM_WriteBinary, .Lfunc_end7-PGM_WriteBinary
	.cfi_endproc

	.globl	PGM_WriteImage
	.p2align	4, 0x90
	.type	PGM_WriteImage,@function
PGM_WriteImage:                         # @PGM_WriteImage
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 48
.Lcfi44:
	.cfi_offset %rbx, -40
.Lcfi45:
	.cfi_offset %r12, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movl	$-3, %eax
	cmpb	$0, (%r12)
	je	.LBB8_19
# BB#1:
	cmpl	$0, 36(%r12)
	je	.LBB8_19
# BB#2:
	cmpl	$0, 32(%r12)
	je	.LBB8_19
# BB#3:
	movsbl	%sil, %ecx
	cmpl	$3, %ecx
	ja	.LBB8_18
# BB#4:
	jmpq	*.LJTI8_0(,%rcx,8)
.LBB8_5:
	cmpq	$0, 48(%r12)
	je	.LBB8_19
# BB#6:
	movq	16(%r12), %r15
	movq	%r15, %rdi
	callq	strlen
	leaq	9(%rax), %rdi
	callq	malloc
	movq	%rax, %r14
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	sprintf
	movq	stdout(%rip), %r15
	movl	$.L.str.10, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movl	32(%r12), %ecx
	movl	36(%r12), %edx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movl	40(%r12), %edx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movl	32(%r12), %eax
	imull	36(%r12), %eax
	testl	%eax, %eax
	jle	.LBB8_17
# BB#7:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%r12), %rax
	movsbl	(%rax,%rbx), %edi
	movq	%r15, %rsi
	callq	fputc
	incq	%rbx
	movslq	32(%r12), %rax
	movslq	36(%r12), %rcx
	imulq	%rax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB8_8
	jmp	.LBB8_17
.LBB8_9:
	cmpq	$0, 80(%r12)
	je	.LBB8_19
# BB#10:
	movq	16(%r12), %r15
	movq	%r15, %rdi
	callq	strlen
	leaq	11(%rax), %rdi
	callq	malloc
	movq	%rax, %r14
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	sprintf
	movq	stdout(%rip), %r15
	movl	$.L.str.10, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movl	32(%r12), %ecx
	movl	36(%r12), %edx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movl	$.L.str.15, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movl	32(%r12), %eax
	imull	36(%r12), %eax
	testl	%eax, %eax
	jle	.LBB8_17
# BB#11:                                # %.lr.ph71.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_12:                               # %.lr.ph71
                                        # =>This Inner Loop Header: Depth=1
	movq	80(%r12), %rax
	cvttss2si	(%rax,%rbx,4), %eax
	movsbl	%al, %edi
	movq	%r15, %rsi
	callq	fputc
	incq	%rbx
	movslq	32(%r12), %rax
	movslq	36(%r12), %rcx
	imulq	%rax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB8_12
	jmp	.LBB8_17
.LBB8_13:
	cmpq	$0, 72(%r12)
	je	.LBB8_19
# BB#14:
	movq	16(%r12), %r15
	movq	%r15, %rdi
	callq	strlen
	leaq	10(%rax), %rdi
	callq	malloc
	movq	%rax, %r14
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	sprintf
	movq	stdout(%rip), %r15
	movl	$.L.str.10, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movl	32(%r12), %ecx
	movl	36(%r12), %edx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movl	$.L.str.15, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movl	32(%r12), %eax
	imull	36(%r12), %eax
	testl	%eax, %eax
	jle	.LBB8_17
# BB#15:                                # %.lr.ph75.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_16:                               # %.lr.ph75
                                        # =>This Inner Loop Header: Depth=1
	movq	72(%r12), %rax
	cvttss2si	(%rax,%rbx,4), %eax
	movsbl	%al, %edi
	movq	%r15, %rsi
	callq	fputc
	incq	%rbx
	movslq	32(%r12), %rax
	movslq	36(%r12), %rcx
	imulq	%rax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB8_16
.LBB8_17:                               # %._crit_edge76
	movq	%r14, %rdi
	callq	free
.LBB8_18:
	xorl	%eax, %eax
.LBB8_19:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	PGM_WriteImage, .Lfunc_end8-PGM_WriteImage
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_5
	.quad	.LBB8_19
	.quad	.LBB8_9
	.quad	.LBB8_13

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"filename:\t%s\n"
	.size	.L.str, 14

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"width   :\t%d\n"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"height  :\t%d\n"
	.size	.L.str.2, 14

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"max gray:\t%d\n"
	.size	.L.str.3, 14

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%d"
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"#%n"
	.size	.L.str.5, 4

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"r"
	.size	.L.str.7, 2

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"P5"
	.size	.L.str.8, 3

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%s.jo.pgm"
	.size	.L.str.9, 10

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"P5\n"
	.size	.L.str.10, 4

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%d %d\n"
	.size	.L.str.11, 7

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"%d\n"
	.size	.L.str.12, 4

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"%s.raw.pgm"
	.size	.L.str.13, 11

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%s.cedge.pgm"
	.size	.L.str.14, 13

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"255\n"
	.size	.L.str.15, 5

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%s.hvar.pgm"
	.size	.L.str.16, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
