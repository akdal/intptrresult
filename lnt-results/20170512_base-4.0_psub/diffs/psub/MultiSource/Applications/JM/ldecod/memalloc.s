	.text
	.file	"memalloc.bc"
	.globl	get_mem2Dpel
	.p2align	4, 0x90
	.type	get_mem2Dpel,@function
get_mem2Dpel:                           # @get_mem2Dpel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movslq	%ebp, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	movq	%r12, (%rbx)
	testq	%r12, %r12
	jne	.LBB0_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %r12
.LBB0_2:
	movl	%r15d, %r14d
	imull	%ebp, %r14d
	movslq	%r14d, %rdi
	movl	$2, %esi
	callq	calloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.LBB0_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB0_4:                                # %.preheader
	cmpl	$2, %ebp
	jl	.LBB0_13
# BB#5:                                 # %.lr.ph
	movslq	%r15d, %rax
	movl	%ebp, %r9d
	addl	$3, %ebp
	leaq	-2(%r9), %r8
	andq	$3, %rbp
	je	.LBB0_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	leaq	(%rax,%rax), %rdi
	.p2align	4, 0x90
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rcx
	addq	%rdi, %rcx
	movq	%rcx, 8(%rsi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB0_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %r8
	jae	.LBB0_11
	jmp	.LBB0_13
.LBB0_6:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB0_13
.LBB0_11:                               # %.lr.ph.new
	addq	%rax, %rax
	.p2align	4, 0x90
.LBB0_12:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	-8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 8(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	16(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 24(%rcx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %r9
	jne	.LBB0_12
.LBB0_13:                               # %._crit_edge
	addl	%r14d, %r14d
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	get_mem2Dpel, .Lfunc_end0-get_mem2Dpel
	.cfi_endproc

	.globl	no_mem_exit
	.p2align	4, 0x90
	.type	no_mem_exit,@function
no_mem_exit:                            # @no_mem_exit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	popq	%rax
	jmp	error                   # TAILCALL
.Lfunc_end1:
	.size	no_mem_exit, .Lfunc_end1-no_mem_exit
	.cfi_endproc

	.globl	get_mem3Dpel
	.p2align	4, 0x90
	.type	get_mem3Dpel,@function
get_mem3Dpel:                           # @get_mem3Dpel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r12d
	movl	%esi, %r14d
	movq	%rdi, %r13
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r13)
	testq	%rax, %rax
	jne	.LBB2_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.1, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB2_2:                                # %.preheader
	testl	%r14d, %r14d
	jle	.LBB2_5
# BB#3:                                 # %.lr.ph.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	addq	%rbp, %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	callq	get_mem2Dpel
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB2_4
.LBB2_5:                                # %._crit_edge
	imull	%r12d, %r14d
	imull	%r15d, %r14d
	addl	%r14d, %r14d
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	get_mem3Dpel, .Lfunc_end2-get_mem3Dpel
	.cfi_endproc

	.globl	free_mem2Dpel
	.p2align	4, 0x90
	.type	free_mem2Dpel,@function
free_mem2Dpel:                          # @free_mem2Dpel
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
.Lcfi25:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB3_5
# BB#1:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_3
# BB#2:
	callq	free
	jmp	.LBB3_4
.LBB3_5:
	movl	$.L.str.2, %edi
	movl	$100, %esi
	popq	%rbx
	jmp	error                   # TAILCALL
.LBB3_3:
	movl	$.L.str.2, %edi
	movl	$100, %esi
	callq	error
.LBB3_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end3:
	.size	free_mem2Dpel, .Lfunc_end3-free_mem2Dpel
	.cfi_endproc

	.globl	free_mem3Dpel
	.p2align	4, 0x90
	.type	free_mem3Dpel,@function
free_mem3Dpel:                          # @free_mem3Dpel
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r12, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB4_11
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB4_10
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %r12d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.LBB4_8
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_3 Depth=1
	callq	free
	jmp	.LBB4_7
	.p2align	4, 0x90
.LBB4_8:                                #   in Loop: Header=BB4_3 Depth=1
	movl	$.L.str.2, %edi
	movl	$100, %esi
	callq	error
	jmp	.LBB4_9
	.p2align	4, 0x90
.LBB4_6:                                #   in Loop: Header=BB4_3 Depth=1
	movl	$.L.str.2, %edi
	movl	$100, %esi
	callq	error
.LBB4_7:                                #   in Loop: Header=BB4_3 Depth=1
	movq	%r15, %rdi
	callq	free
.LBB4_9:                                # %free_mem2Dpel.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	addq	$8, %rbx
	decq	%r12
	jne	.LBB4_3
.LBB4_10:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB4_11:
	movl	$.L.str.3, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	error                   # TAILCALL
.Lfunc_end4:
	.size	free_mem3Dpel, .Lfunc_end4-free_mem3Dpel
	.cfi_endproc

	.globl	get_mem2D
	.p2align	4, 0x90
	.type	get_mem2D,@function
get_mem2D:                              # @get_mem2D
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 48
.Lcfi40:
	.cfi_offset %rbx, -48
.Lcfi41:
	.cfi_offset %r12, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbp, (%rbx)
	testq	%rbp, %rbp
	jne	.LBB5_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %rbp
.LBB5_2:
	movl	%r15d, %r12d
	imull	%r14d, %r12d
	movslq	%r12d, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, (%rbp)
	testq	%rax, %rax
	jne	.LBB5_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.4, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB5_4:                                # %.preheader
	cmpl	$2, %r14d
	jl	.LBB5_12
# BB#5:                                 # %.lr.ph
	movslq	%r15d, %rax
	movl	%r14d, %ecx
	addl	$3, %r14d
	leaq	-2(%rcx), %rsi
	andq	$3, %r14
	je	.LBB5_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_8:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rdi,%rdx,8), %rbp
	addq	%rax, %rbp
	movq	%rbp, 8(%rdi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %r14
	jne	.LBB5_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %rsi
	jae	.LBB5_11
	jmp	.LBB5_12
.LBB5_6:
	movl	$1, %edx
	cmpq	$3, %rsi
	jb	.LBB5_12
	.p2align	4, 0x90
.LBB5_11:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	-8(%rsi,%rdx,8), %rdi
	addq	%rax, %rdi
	movq	%rdi, (%rsi,%rdx,8)
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rdi
	addq	%rax, %rdi
	movq	%rdi, 8(%rsi,%rdx,8)
	movq	(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rdi
	addq	%rax, %rdi
	movq	%rdi, 16(%rsi,%rdx,8)
	movq	(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rdi
	addq	%rax, %rdi
	movq	%rdi, 24(%rsi,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB5_11
.LBB5_12:                               # %._crit_edge
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	get_mem2D, .Lfunc_end5-get_mem2D
	.cfi_endproc

	.globl	get_mem2Dint
	.p2align	4, 0x90
	.type	get_mem2Dint,@function
get_mem2Dint:                           # @get_mem2Dint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 48
.Lcfi50:
	.cfi_offset %rbx, -48
.Lcfi51:
	.cfi_offset %r12, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movslq	%ebp, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	movq	%r12, (%rbx)
	testq	%r12, %r12
	jne	.LBB6_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.5, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %r12
.LBB6_2:
	movl	%r15d, %r14d
	imull	%ebp, %r14d
	movslq	%r14d, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.LBB6_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.5, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB6_4:                                # %.preheader
	cmpl	$2, %ebp
	jl	.LBB6_13
# BB#5:                                 # %.lr.ph
	movslq	%r15d, %rax
	movl	%ebp, %r9d
	addl	$3, %ebp
	leaq	-2(%r9), %r8
	andq	$3, %rbp
	je	.LBB6_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	leaq	(,%rax,4), %rdi
	.p2align	4, 0x90
.LBB6_8:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rcx
	addq	%rdi, %rcx
	movq	%rcx, 8(%rsi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB6_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %r8
	jae	.LBB6_11
	jmp	.LBB6_13
.LBB6_6:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB6_13
.LBB6_11:                               # %.lr.ph.new
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB6_12:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	-8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 8(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	16(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 24(%rcx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %r9
	jne	.LBB6_12
.LBB6_13:                               # %._crit_edge
	shll	$2, %r14d
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	get_mem2Dint, .Lfunc_end6-get_mem2Dint
	.cfi_endproc

	.globl	get_mem2Dint64
	.p2align	4, 0x90
	.type	get_mem2Dint64,@function
get_mem2Dint64:                         # @get_mem2Dint64
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 48
.Lcfi60:
	.cfi_offset %rbx, -48
.Lcfi61:
	.cfi_offset %r12, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movslq	%ebp, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	movq	%r12, (%rbx)
	testq	%r12, %r12
	jne	.LBB7_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.6, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %r12
.LBB7_2:
	movl	%r15d, %r14d
	imull	%ebp, %r14d
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.LBB7_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.6, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB7_4:                                # %.preheader
	cmpl	$2, %ebp
	jl	.LBB7_13
# BB#5:                                 # %.lr.ph
	movslq	%r15d, %rax
	movl	%ebp, %r9d
	addl	$3, %ebp
	leaq	-2(%r9), %r8
	andq	$3, %rbp
	je	.LBB7_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	leaq	(,%rax,8), %rdi
	.p2align	4, 0x90
.LBB7_8:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rcx
	addq	%rdi, %rcx
	movq	%rcx, 8(%rsi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB7_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %r8
	jae	.LBB7_11
	jmp	.LBB7_13
.LBB7_6:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB7_13
.LBB7_11:                               # %.lr.ph.new
	shlq	$3, %rax
	.p2align	4, 0x90
.LBB7_12:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	-8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 8(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	16(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 24(%rcx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %r9
	jne	.LBB7_12
.LBB7_13:                               # %._crit_edge
	shll	$3, %r14d
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	get_mem2Dint64, .Lfunc_end7-get_mem2Dint64
	.cfi_endproc

	.globl	get_mem3D
	.p2align	4, 0x90
	.type	get_mem3D,@function
get_mem3D:                              # @get_mem3D
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi71:
	.cfi_def_cfa_offset 64
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r13d
	movl	%esi, %r14d
	movq	%rdi, %r12
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.LBB8_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.7, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB8_2:                                # %.preheader
	testl	%r14d, %r14d
	jle	.LBB8_5
# BB#3:                                 # %.lr.ph.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdi
	addq	%rbp, %rdi
	movl	%r13d, %esi
	movl	%r15d, %edx
	callq	get_mem2D
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB8_4
.LBB8_5:                                # %._crit_edge
	imull	%r14d, %r13d
	imull	%r15d, %r13d
	movl	%r13d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	get_mem3D, .Lfunc_end8-get_mem3D
	.cfi_endproc

	.globl	get_mem3Dint
	.p2align	4, 0x90
	.type	get_mem3Dint,@function
get_mem3Dint:                           # @get_mem3Dint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi82:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 64
.Lcfi85:
	.cfi_offset %rbx, -56
.Lcfi86:
	.cfi_offset %r12, -48
.Lcfi87:
	.cfi_offset %r13, -40
.Lcfi88:
	.cfi_offset %r14, -32
.Lcfi89:
	.cfi_offset %r15, -24
.Lcfi90:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r12d
	movl	%esi, %r14d
	movq	%rdi, %r13
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r13)
	testq	%rax, %rax
	jne	.LBB9_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.8, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB9_2:                                # %.preheader
	testl	%r14d, %r14d
	jle	.LBB9_5
# BB#3:                                 # %.lr.ph.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	addq	%rbp, %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	callq	get_mem2Dint
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB9_4
.LBB9_5:                                # %._crit_edge
	imull	%r12d, %r14d
	imull	%r15d, %r14d
	shll	$2, %r14d
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	get_mem3Dint, .Lfunc_end9-get_mem3Dint
	.cfi_endproc

	.globl	get_mem3Dint64
	.p2align	4, 0x90
	.type	get_mem3Dint64,@function
get_mem3Dint64:                         # @get_mem3Dint64
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi97:
	.cfi_def_cfa_offset 64
.Lcfi98:
	.cfi_offset %rbx, -56
.Lcfi99:
	.cfi_offset %r12, -48
.Lcfi100:
	.cfi_offset %r13, -40
.Lcfi101:
	.cfi_offset %r14, -32
.Lcfi102:
	.cfi_offset %r15, -24
.Lcfi103:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r12d
	movl	%esi, %r14d
	movq	%rdi, %r13
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r13)
	testq	%rax, %rax
	jne	.LBB10_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.9, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB10_2:                               # %.preheader
	testl	%r14d, %r14d
	jle	.LBB10_5
# BB#3:                                 # %.lr.ph.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	addq	%rbp, %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	callq	get_mem2Dint64
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB10_4
.LBB10_5:                               # %._crit_edge
	imull	%r12d, %r14d
	imull	%r15d, %r14d
	shll	$3, %r14d
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	get_mem3Dint64, .Lfunc_end10-get_mem3Dint64
	.cfi_endproc

	.globl	get_mem4Dint
	.p2align	4, 0x90
	.type	get_mem4Dint,@function
get_mem4Dint:                           # @get_mem4Dint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi107:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi108:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi110:
	.cfi_def_cfa_offset 96
.Lcfi111:
	.cfi_offset %rbx, -56
.Lcfi112:
	.cfi_offset %r12, -48
.Lcfi113:
	.cfi_offset %r13, -40
.Lcfi114:
	.cfi_offset %r14, -32
.Lcfi115:
	.cfi_offset %r15, -24
.Lcfi116:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movl	%ecx, %ebp
	movl	%edx, (%rsp)            # 4-byte Spill
	movl	%esi, %r14d
	movq	%rdi, %r15
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%rax, (%r15)
	testq	%rax, %rax
	jne	.LBB11_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.10, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB11_2:                               # %.preheader
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	testl	%r14d, %r14d
	jle	.LBB11_14
# BB#3:                                 # %.lr.ph
	movl	(%rsp), %eax            # 4-byte Reload
	movslq	%eax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	movl	4(%rsp), %ecx           # 4-byte Reload
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jle	.LBB11_4
# BB#8:                                 # %.lr.ph.split.us.preheader
	movl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB11_9:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_12 Depth 2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r14
	movl	$8, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	calloc
	movq	%rax, (%r14,%r12,8)
	testq	%rax, %rax
	jne	.LBB11_11
# BB#10:                                #   in Loop: Header=BB11_9 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.8, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB11_11:                              # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB11_9 Depth=1
	xorl	%r15d, %r15d
	movq	32(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB11_12:                              # %.lr.ph.i.us
                                        #   Parent Loop BB11_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r12,8), %rdi
	addq	%r15, %rdi
	movl	%ebp, %esi
	movl	%ebx, %edx
	callq	get_mem2Dint
	addq	$8, %r15
	decq	%r13
	jne	.LBB11_12
# BB#13:                                # %get_mem3Dint.exit.loopexit.us
                                        #   in Loop: Header=BB11_9 Depth=1
	incq	%r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB11_9
	jmp	.LBB11_14
.LBB11_4:                               # %.lr.ph.split.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB11_5:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r15
	movl	$8, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	calloc
	movq	%rax, (%r15,%r14,8)
	testq	%rax, %rax
	jne	.LBB11_7
# BB#6:                                 #   in Loop: Header=BB11_5 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.8, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB11_7:                               # %get_mem3Dint.exit
                                        #   in Loop: Header=BB11_5 Depth=1
	incq	%r14
	cmpq	%r14, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB11_5
.LBB11_14:                              # %._crit_edge
	movl	4(%rsp), %eax           # 4-byte Reload
	imull	(%rsp), %eax            # 4-byte Folded Reload
	imull	%ebp, %eax
	imull	%ebx, %eax
	shll	$2, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	get_mem4Dint, .Lfunc_end11-get_mem4Dint
	.cfi_endproc

	.globl	free_mem2D
	.p2align	4, 0x90
	.type	free_mem2D,@function
free_mem2D:                             # @free_mem2D
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 16
.Lcfi118:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB12_5
# BB#1:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_3
# BB#2:
	callq	free
	jmp	.LBB12_4
.LBB12_5:
	movl	$.L.str.11, %edi
	movl	$100, %esi
	popq	%rbx
	jmp	error                   # TAILCALL
.LBB12_3:
	movl	$.L.str.11, %edi
	movl	$100, %esi
	callq	error
.LBB12_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end12:
	.size	free_mem2D, .Lfunc_end12-free_mem2D
	.cfi_endproc

	.globl	free_mem2Dint
	.p2align	4, 0x90
	.type	free_mem2Dint,@function
free_mem2Dint:                          # @free_mem2Dint
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 16
.Lcfi120:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB13_5
# BB#1:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_3
# BB#2:
	callq	free
	jmp	.LBB13_4
.LBB13_5:
	movl	$.L.str.12, %edi
	movl	$100, %esi
	popq	%rbx
	jmp	error                   # TAILCALL
.LBB13_3:
	movl	$.L.str.12, %edi
	movl	$100, %esi
	callq	error
.LBB13_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end13:
	.size	free_mem2Dint, .Lfunc_end13-free_mem2Dint
	.cfi_endproc

	.globl	free_mem2Dint64
	.p2align	4, 0x90
	.type	free_mem2Dint64,@function
free_mem2Dint64:                        # @free_mem2Dint64
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi121:
	.cfi_def_cfa_offset 16
.Lcfi122:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB14_5
# BB#1:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_3
# BB#2:
	callq	free
	jmp	.LBB14_4
.LBB14_5:
	movl	$.L.str.13, %edi
	movl	$100, %esi
	popq	%rbx
	jmp	error                   # TAILCALL
.LBB14_3:
	movl	$.L.str.13, %edi
	movl	$100, %esi
	callq	error
.LBB14_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end14:
	.size	free_mem2Dint64, .Lfunc_end14-free_mem2Dint64
	.cfi_endproc

	.globl	free_mem3D
	.p2align	4, 0x90
	.type	free_mem3D,@function
free_mem3D:                             # @free_mem3D
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi125:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi127:
	.cfi_def_cfa_offset 48
.Lcfi128:
	.cfi_offset %rbx, -40
.Lcfi129:
	.cfi_offset %r12, -32
.Lcfi130:
	.cfi_offset %r14, -24
.Lcfi131:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB15_11
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB15_10
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %r12d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB15_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.LBB15_8
# BB#4:                                 #   in Loop: Header=BB15_3 Depth=1
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB15_6
# BB#5:                                 #   in Loop: Header=BB15_3 Depth=1
	callq	free
	jmp	.LBB15_7
	.p2align	4, 0x90
.LBB15_8:                               #   in Loop: Header=BB15_3 Depth=1
	movl	$.L.str.11, %edi
	movl	$100, %esi
	callq	error
	jmp	.LBB15_9
	.p2align	4, 0x90
.LBB15_6:                               #   in Loop: Header=BB15_3 Depth=1
	movl	$.L.str.11, %edi
	movl	$100, %esi
	callq	error
.LBB15_7:                               #   in Loop: Header=BB15_3 Depth=1
	movq	%r15, %rdi
	callq	free
.LBB15_9:                               # %free_mem2D.exit
                                        #   in Loop: Header=BB15_3 Depth=1
	addq	$8, %rbx
	decq	%r12
	jne	.LBB15_3
.LBB15_10:                              # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB15_11:
	movl	$.L.str.14, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	error                   # TAILCALL
.Lfunc_end15:
	.size	free_mem3D, .Lfunc_end15-free_mem3D
	.cfi_endproc

	.globl	free_mem3Dint
	.p2align	4, 0x90
	.type	free_mem3Dint,@function
free_mem3Dint:                          # @free_mem3Dint
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi136:
	.cfi_def_cfa_offset 48
.Lcfi137:
	.cfi_offset %rbx, -40
.Lcfi138:
	.cfi_offset %r12, -32
.Lcfi139:
	.cfi_offset %r14, -24
.Lcfi140:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB16_11
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB16_10
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %r12d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB16_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.LBB16_8
# BB#4:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB16_6
# BB#5:                                 #   in Loop: Header=BB16_3 Depth=1
	callq	free
	jmp	.LBB16_7
	.p2align	4, 0x90
.LBB16_8:                               #   in Loop: Header=BB16_3 Depth=1
	movl	$.L.str.12, %edi
	movl	$100, %esi
	callq	error
	jmp	.LBB16_9
	.p2align	4, 0x90
.LBB16_6:                               #   in Loop: Header=BB16_3 Depth=1
	movl	$.L.str.12, %edi
	movl	$100, %esi
	callq	error
.LBB16_7:                               #   in Loop: Header=BB16_3 Depth=1
	movq	%r15, %rdi
	callq	free
.LBB16_9:                               # %free_mem2Dint.exit
                                        #   in Loop: Header=BB16_3 Depth=1
	addq	$8, %rbx
	decq	%r12
	jne	.LBB16_3
.LBB16_10:                              # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB16_11:
	movl	$.L.str.14, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	error                   # TAILCALL
.Lfunc_end16:
	.size	free_mem3Dint, .Lfunc_end16-free_mem3Dint
	.cfi_endproc

	.globl	free_mem3Dint64
	.p2align	4, 0x90
	.type	free_mem3Dint64,@function
free_mem3Dint64:                        # @free_mem3Dint64
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi143:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi145:
	.cfi_def_cfa_offset 48
.Lcfi146:
	.cfi_offset %rbx, -40
.Lcfi147:
	.cfi_offset %r12, -32
.Lcfi148:
	.cfi_offset %r14, -24
.Lcfi149:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB17_11
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB17_10
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %r12d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB17_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.LBB17_8
# BB#4:                                 #   in Loop: Header=BB17_3 Depth=1
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB17_6
# BB#5:                                 #   in Loop: Header=BB17_3 Depth=1
	callq	free
	jmp	.LBB17_7
	.p2align	4, 0x90
.LBB17_8:                               #   in Loop: Header=BB17_3 Depth=1
	movl	$.L.str.13, %edi
	movl	$100, %esi
	callq	error
	jmp	.LBB17_9
	.p2align	4, 0x90
.LBB17_6:                               #   in Loop: Header=BB17_3 Depth=1
	movl	$.L.str.13, %edi
	movl	$100, %esi
	callq	error
.LBB17_7:                               #   in Loop: Header=BB17_3 Depth=1
	movq	%r15, %rdi
	callq	free
.LBB17_9:                               # %free_mem2Dint64.exit
                                        #   in Loop: Header=BB17_3 Depth=1
	addq	$8, %rbx
	decq	%r12
	jne	.LBB17_3
.LBB17_10:                              # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB17_11:
	movl	$.L.str.15, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	error                   # TAILCALL
.Lfunc_end17:
	.size	free_mem3Dint64, .Lfunc_end17-free_mem3Dint64
	.cfi_endproc

	.globl	free_mem4Dint
	.p2align	4, 0x90
	.type	free_mem4Dint,@function
free_mem4Dint:                          # @free_mem4Dint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi150:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi151:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi152:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi154:
	.cfi_def_cfa_offset 48
.Lcfi155:
	.cfi_offset %rbx, -40
.Lcfi156:
	.cfi_offset %r14, -32
.Lcfi157:
	.cfi_offset %r15, -24
.Lcfi158:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB18_5
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB18_4
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %ebx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB18_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movl	%r15d, %esi
	callq	free_mem3Dint
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB18_3
.LBB18_4:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB18_5:
	movl	$.L.str.16, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.Lfunc_end18:
	.size	free_mem4Dint, .Lfunc_end18-free_mem4Dint
	.cfi_endproc

	.globl	get_mem2Dshort
	.p2align	4, 0x90
	.type	get_mem2Dshort,@function
get_mem2Dshort:                         # @get_mem2Dshort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi162:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi163:
	.cfi_def_cfa_offset 48
.Lcfi164:
	.cfi_offset %rbx, -48
.Lcfi165:
	.cfi_offset %r12, -40
.Lcfi166:
	.cfi_offset %r14, -32
.Lcfi167:
	.cfi_offset %r15, -24
.Lcfi168:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movslq	%ebp, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	movq	%r12, (%rbx)
	testq	%r12, %r12
	jne	.LBB19_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.18, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
	movq	(%rbx), %r12
.LBB19_2:
	movl	%r15d, %r14d
	imull	%ebp, %r14d
	movslq	%r14d, %rdi
	movl	$2, %esi
	callq	calloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.LBB19_4
# BB#3:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.18, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB19_4:                               # %.preheader
	cmpl	$2, %ebp
	jl	.LBB19_13
# BB#5:                                 # %.lr.ph
	movslq	%r15d, %rax
	movl	%ebp, %r9d
	addl	$3, %ebp
	leaq	-2(%r9), %r8
	andq	$3, %rbp
	je	.LBB19_6
# BB#7:                                 # %.prol.preheader
	xorl	%edx, %edx
	leaq	(%rax,%rax), %rdi
	.p2align	4, 0x90
.LBB19_8:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rcx
	addq	%rdi, %rcx
	movq	%rcx, 8(%rsi,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB19_8
# BB#9:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdx
	cmpq	$3, %r8
	jae	.LBB19_11
	jmp	.LBB19_13
.LBB19_6:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB19_13
.LBB19_11:                              # %.lr.ph.new
	addq	%rax, %rax
	.p2align	4, 0x90
.LBB19_12:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	-8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 8(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	8(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx,%rdx,8)
	movq	(%rbx), %rcx
	movq	16(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movq	%rsi, 24(%rcx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %r9
	jne	.LBB19_12
.LBB19_13:                              # %._crit_edge
	addl	%r14d, %r14d
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	get_mem2Dshort, .Lfunc_end19-get_mem2Dshort
	.cfi_endproc

	.globl	get_mem3Dshort
	.p2align	4, 0x90
	.type	get_mem3Dshort,@function
get_mem3Dshort:                         # @get_mem3Dshort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi171:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi172:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi173:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi174:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi175:
	.cfi_def_cfa_offset 64
.Lcfi176:
	.cfi_offset %rbx, -56
.Lcfi177:
	.cfi_offset %r12, -48
.Lcfi178:
	.cfi_offset %r13, -40
.Lcfi179:
	.cfi_offset %r14, -32
.Lcfi180:
	.cfi_offset %r15, -24
.Lcfi181:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r12d
	movl	%esi, %r14d
	movq	%rdi, %r13
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, (%r13)
	testq	%rax, %rax
	jne	.LBB20_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.19, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB20_2:                               # %.preheader
	testl	%r14d, %r14d
	jle	.LBB20_5
# BB#3:                                 # %.lr.ph.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB20_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	addq	%rbp, %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	callq	get_mem2Dshort
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB20_4
.LBB20_5:                               # %._crit_edge
	imull	%r12d, %r14d
	imull	%r15d, %r14d
	addl	%r14d, %r14d
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	get_mem3Dshort, .Lfunc_end20-get_mem3Dshort
	.cfi_endproc

	.globl	get_mem4Dshort
	.p2align	4, 0x90
	.type	get_mem4Dshort,@function
get_mem4Dshort:                         # @get_mem4Dshort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi182:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi183:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi184:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi185:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi186:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi187:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi188:
	.cfi_def_cfa_offset 96
.Lcfi189:
	.cfi_offset %rbx, -56
.Lcfi190:
	.cfi_offset %r12, -48
.Lcfi191:
	.cfi_offset %r13, -40
.Lcfi192:
	.cfi_offset %r14, -32
.Lcfi193:
	.cfi_offset %r15, -24
.Lcfi194:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movl	%ecx, %ebp
	movl	%edx, (%rsp)            # 4-byte Spill
	movl	%esi, %r14d
	movq	%rdi, %r15
	movslq	%r14d, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%rax, (%r15)
	testq	%rax, %rax
	jne	.LBB21_2
# BB#1:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.20, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB21_2:                               # %.preheader
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	testl	%r14d, %r14d
	jle	.LBB21_14
# BB#3:                                 # %.lr.ph
	movl	(%rsp), %eax            # 4-byte Reload
	movslq	%eax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	movl	4(%rsp), %ecx           # 4-byte Reload
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jle	.LBB21_4
# BB#8:                                 # %.lr.ph.split.us.preheader
	movl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB21_9:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_12 Depth 2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r14
	movl	$8, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	calloc
	movq	%rax, (%r14,%r12,8)
	testq	%rax, %rax
	jne	.LBB21_11
# BB#10:                                #   in Loop: Header=BB21_9 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.19, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB21_11:                              # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB21_9 Depth=1
	xorl	%r15d, %r15d
	movq	32(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB21_12:                              # %.lr.ph.i.us
                                        #   Parent Loop BB21_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r12,8), %rdi
	addq	%r15, %rdi
	movl	%ebp, %esi
	movl	%ebx, %edx
	callq	get_mem2Dshort
	addq	$8, %r15
	decq	%r13
	jne	.LBB21_12
# BB#13:                                # %get_mem3Dshort.exit.loopexit.us
                                        #   in Loop: Header=BB21_9 Depth=1
	incq	%r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB21_9
	jmp	.LBB21_14
.LBB21_4:                               # %.lr.ph.split.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB21_5:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r15
	movl	$8, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	calloc
	movq	%rax, (%r15,%r14,8)
	testq	%rax, %rax
	jne	.LBB21_7
# BB#6:                                 #   in Loop: Header=BB21_5 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.17, %edx
	movl	$.L.str.19, %ecx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$100, %esi
	callq	error
.LBB21_7:                               # %get_mem3Dshort.exit
                                        #   in Loop: Header=BB21_5 Depth=1
	incq	%r14
	cmpq	%r14, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB21_5
.LBB21_14:                              # %._crit_edge
	movl	4(%rsp), %eax           # 4-byte Reload
	imull	(%rsp), %eax            # 4-byte Folded Reload
	imull	%ebp, %eax
	imull	%ebx, %eax
	addl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	get_mem4Dshort, .Lfunc_end21-get_mem4Dshort
	.cfi_endproc

	.globl	free_mem2Dshort
	.p2align	4, 0x90
	.type	free_mem2Dshort,@function
free_mem2Dshort:                        # @free_mem2Dshort
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi195:
	.cfi_def_cfa_offset 16
.Lcfi196:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB22_5
# BB#1:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_3
# BB#2:
	callq	free
	jmp	.LBB22_4
.LBB22_5:
	movl	$.L.str.21, %edi
	movl	$100, %esi
	popq	%rbx
	jmp	error                   # TAILCALL
.LBB22_3:
	movl	$.L.str.21, %edi
	movl	$100, %esi
	callq	error
.LBB22_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end22:
	.size	free_mem2Dshort, .Lfunc_end22-free_mem2Dshort
	.cfi_endproc

	.globl	free_mem3Dshort
	.p2align	4, 0x90
	.type	free_mem3Dshort,@function
free_mem3Dshort:                        # @free_mem3Dshort
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi197:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi198:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi199:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi200:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi201:
	.cfi_def_cfa_offset 48
.Lcfi202:
	.cfi_offset %rbx, -40
.Lcfi203:
	.cfi_offset %r12, -32
.Lcfi204:
	.cfi_offset %r14, -24
.Lcfi205:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB23_11
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB23_10
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %r12d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB23_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.LBB23_8
# BB#4:                                 #   in Loop: Header=BB23_3 Depth=1
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB23_6
# BB#5:                                 #   in Loop: Header=BB23_3 Depth=1
	callq	free
	jmp	.LBB23_7
	.p2align	4, 0x90
.LBB23_8:                               #   in Loop: Header=BB23_3 Depth=1
	movl	$.L.str.21, %edi
	movl	$100, %esi
	callq	error
	jmp	.LBB23_9
	.p2align	4, 0x90
.LBB23_6:                               #   in Loop: Header=BB23_3 Depth=1
	movl	$.L.str.21, %edi
	movl	$100, %esi
	callq	error
.LBB23_7:                               #   in Loop: Header=BB23_3 Depth=1
	movq	%r15, %rdi
	callq	free
.LBB23_9:                               # %free_mem2Dshort.exit
                                        #   in Loop: Header=BB23_3 Depth=1
	addq	$8, %rbx
	decq	%r12
	jne	.LBB23_3
.LBB23_10:                              # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB23_11:
	movl	$.L.str.22, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	error                   # TAILCALL
.Lfunc_end23:
	.size	free_mem3Dshort, .Lfunc_end23-free_mem3Dshort
	.cfi_endproc

	.globl	free_mem4Dshort
	.p2align	4, 0x90
	.type	free_mem4Dshort,@function
free_mem4Dshort:                        # @free_mem4Dshort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi206:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi207:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi208:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi209:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi210:
	.cfi_def_cfa_offset 48
.Lcfi211:
	.cfi_offset %rbx, -40
.Lcfi212:
	.cfi_offset %r14, -32
.Lcfi213:
	.cfi_offset %r15, -24
.Lcfi214:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB24_5
# BB#1:                                 # %.preheader
	testl	%esi, %esi
	jle	.LBB24_4
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %ebx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB24_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movl	%r15d, %esi
	callq	free_mem3Dshort
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB24_3
.LBB24_4:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB24_5:
	movl	$.L.str.23, %edi
	movl	$100, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.Lfunc_end24:
	.size	free_mem4Dshort, .Lfunc_end24-free_mem4Dshort
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"get_mem2Dpel: array2D"
	.size	.L.str, 22

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"get_mem3Dpel: array3D"
	.size	.L.str.1, 22

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"free_mem2Dpel: trying to free unused memory"
	.size	.L.str.2, 44

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"free_mem3Dpel: trying to free unused memory"
	.size	.L.str.3, 44

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"get_mem2D: array2D"
	.size	.L.str.4, 19

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"get_mem2Dint: array2D"
	.size	.L.str.5, 22

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"get_mem2Dint64: array2D"
	.size	.L.str.6, 24

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"get_mem3D: array3D"
	.size	.L.str.7, 19

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"get_mem3Dint: array3D"
	.size	.L.str.8, 22

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"get_mem3Dint64: array3D"
	.size	.L.str.9, 24

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"get_mem4Dint: array4D"
	.size	.L.str.10, 22

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"free_mem2D: trying to free unused memory"
	.size	.L.str.11, 41

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"free_mem2Dint: trying to free unused memory"
	.size	.L.str.12, 44

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"free_mem2Dint64: trying to free unused memory"
	.size	.L.str.13, 46

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"free_mem3D: trying to free unused memory"
	.size	.L.str.14, 41

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"free_mem3Dint64: trying to free unused memory"
	.size	.L.str.15, 46

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"free_mem4D: trying to free unused memory"
	.size	.L.str.16, 41

	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Could not allocate memory: %s"
	.size	.L.str.17, 30

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"get_mem2Dshort: array2D"
	.size	.L.str.18, 24

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"get_mem3Dshort: array3D"
	.size	.L.str.19, 24

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"get_mem4Dshort: array4D"
	.size	.L.str.20, 24

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"free_mem2Dshort: trying to free unused memory"
	.size	.L.str.21, 46

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"free_mem3Dshort: trying to free unused memory"
	.size	.L.str.22, 46

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"free_mem4Dshort: trying to free unused memory"
	.size	.L.str.23, 46

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
