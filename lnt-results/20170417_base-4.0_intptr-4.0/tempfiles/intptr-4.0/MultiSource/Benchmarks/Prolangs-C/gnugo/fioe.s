	.text
	.file	"fioe.bc"
	.globl	fioe
	.p2align	4, 0x90
	.type	fioe,@function
fioe:                                   # @fioe
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	je	.LBB0_1
# BB#13:
	cmpl	$18, %edi
	jne	.LBB0_25
# BB#14:
	testl	%esi, %esi
	je	.LBB0_15
# BB#17:
	cmpl	$18, %esi
	jne	.LBB0_18
# BB#19:
	movzbl	p+341(%rip), %edx
	movl	mymove(%rip), %ecx
	cmpl	%ecx, %edx
	jne	.LBB0_22
# BB#20:
	movl	$1, %eax
	cmpb	%dl, p+359(%rip)
	movl	%edx, %ecx
	jne	.LBB0_22
	jmp	.LBB0_38
.LBB0_1:
	testl	%esi, %esi
	je	.LBB0_4
# BB#2:
	cmpl	$18, %esi
	jne	.LBB0_3
# BB#6:
	movzbl	p+37(%rip), %edx
	movl	mymove(%rip), %ecx
	cmpl	%ecx, %edx
	jne	.LBB0_9
# BB#7:
	movl	$1, %eax
	cmpb	%dl, p+17(%rip)
	movl	%edx, %ecx
	jne	.LBB0_9
	jmp	.LBB0_38
.LBB0_25:
	movslq	%edi, %rax
	testl	%esi, %esi
	je	.LBB0_26
# BB#29:
	cmpl	$18, %esi
	jne	.LBB0_33
# BB#30:
	imulq	$19, %rax, %rax
	movzbl	p+17(%rax), %ecx
	cmpl	mymove(%rip), %ecx
	jne	.LBB0_37
# BB#31:
	leal	-1(%rdi), %eax
	cltq
	imulq	$19, %rax, %rax
	cmpb	%cl, p+18(%rax)
	jne	.LBB0_37
# BB#32:
	incl	%edi
	movslq	%edi, %rax
	imulq	$19, %rax, %rdx
	movl	$1, %eax
	cmpb	%cl, p+18(%rdx)
	jne	.LBB0_37
	jmp	.LBB0_38
.LBB0_4:
	movzbl	p+19(%rip), %edx
	movl	mymove(%rip), %ecx
	cmpl	%ecx, %edx
	jne	.LBB0_9
# BB#5:
	movl	$1, %eax
	cmpb	%dl, p+1(%rip)
	movl	%edx, %ecx
	jne	.LBB0_9
	jmp	.LBB0_38
.LBB0_3:                                # %._crit_edge
	movl	mymove(%rip), %ecx
.LBB0_9:
	movslq	%esi, %rax
	movzbl	p+19(%rax), %edx
	cmpl	%ecx, %edx
	jne	.LBB0_37
# BB#10:
	movzbl	p-1(%rax), %edx
	cmpl	%ecx, %edx
	jne	.LBB0_37
# BB#11:
	movzbl	p+1(%rax), %edx
	jmp	.LBB0_12
.LBB0_15:
	movzbl	p+323(%rip), %edx
	movl	mymove(%rip), %ecx
	cmpl	%ecx, %edx
	jne	.LBB0_22
# BB#16:
	movl	$1, %eax
	cmpb	%dl, p+343(%rip)
	movl	%edx, %ecx
	jne	.LBB0_22
	jmp	.LBB0_38
.LBB0_26:
	imulq	$19, %rax, %rax
	movzbl	p+1(%rax), %ecx
	cmpl	mymove(%rip), %ecx
	jne	.LBB0_37
# BB#27:
	leal	-1(%rdi), %eax
	cltq
	imulq	$19, %rax, %rax
	cmpb	%cl, p(%rax)
	jne	.LBB0_37
# BB#28:
	incl	%edi
	movslq	%edi, %rax
	imulq	$19, %rax, %rdx
	movl	$1, %eax
	cmpb	%cl, p(%rdx)
	jne	.LBB0_37
	jmp	.LBB0_38
.LBB0_18:                               # %..thread_crit_edge
	movl	mymove(%rip), %ecx
.LBB0_22:                               # %.thread
	movslq	%esi, %rax
	movzbl	p+323(%rax), %edx
	cmpl	%ecx, %edx
	jne	.LBB0_37
# BB#23:
	movzbl	p+341(%rax), %edx
	cmpl	%ecx, %edx
	jne	.LBB0_37
# BB#24:
	movzbl	p+343(%rax), %edx
.LBB0_12:
	movl	$1, %eax
	cmpl	%ecx, %edx
	jne	.LBB0_37
	jmp	.LBB0_38
.LBB0_33:
	movslq	%esi, %rcx
	imulq	$19, %rax, %rax
	movzbl	p-1(%rax,%rcx), %edx
	cmpl	mymove(%rip), %edx
	jne	.LBB0_37
# BB#34:
	cmpb	%dl, p+1(%rax,%rcx)
	jne	.LBB0_37
# BB#35:
	leal	-1(%rdi), %eax
	cltq
	imulq	$19, %rax, %rax
	cmpb	%dl, p(%rax,%rcx)
	jne	.LBB0_37
# BB#36:
	incl	%edi
	movslq	%edi, %rax
	imulq	$19, %rax, %rsi
	movl	$1, %eax
	cmpb	%dl, p(%rsi,%rcx)
	je	.LBB0_38
.LBB0_37:
	xorl	%eax, %eax
.LBB0_38:
	retq
.Lfunc_end0:
	.size	fioe, .Lfunc_end0-fioe
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
