	.text
	.file	"semi_interp.bc"
	.globl	hypre_SemiInterpCreate
	.p2align	4, 0x90
	.type	hypre_SemiInterpCreate,@function
hypre_SemiInterpCreate:                 # @hypre_SemiInterpCreate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	movl	$64, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movl	$.L.str, %edi
	callq	hypre_InitializeTiming
	movl	%eax, 60(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	hypre_SemiInterpCreate, .Lfunc_end0-hypre_SemiInterpCreate
	.cfi_endproc

	.globl	hypre_SemiInterpSetup
	.p2align	4, 0x90
	.type	hypre_SemiInterpSetup,@function
hypre_SemiInterpSetup:                  # @hypre_SemiInterpSetup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 128
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, %r13
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movq	128(%rsp), %rbp
	movq	136(%rsp), %r14
	movq	8(%r13), %r15
	movq	24(%rsi), %rsi
	leaq	8(%rsp), %rax
	leaq	16(%rsp), %r10
	leaq	32(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	64(%rsp), %r8
	leaq	56(%rsp), %r9
	movq	%r15, %rdi
	pushq	%rax
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CreateComputeInfo
	addq	$16, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -16
	movq	32(%rsp), %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	24(%rsp), %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	16(%rsp), %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	8(%rsp), %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	32(%rsp), %rdi
	movq	24(%rsp), %rsi
	movq	64(%rsp), %r8
	movq	56(%rsp), %r9
	subq	$8, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	leaq	56(%rsp), %rax
	movq	%r14, %rdx
	movq	%r14, %rcx
	pushq	%rax
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	16(%r13)
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	72(%rsp)
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	callq	hypre_ComputePkgCreate
	addq	$64, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset -64
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	hypre_StructMatrixRef
	movq	%rax, (%rbx)
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 8(%rbx)
	movq	48(%rsp), %rax
	movq	%rax, 16(%rbx)
	movl	(%r12), %eax
	movl	%eax, 24(%rbx)
	movl	4(%r12), %eax
	movl	%eax, 28(%rbx)
	movl	8(%r12), %eax
	movl	%eax, 32(%rbx)
	movl	(%rbp), %eax
	movl	%eax, 36(%rbx)
	movl	4(%rbp), %eax
	movl	%eax, 40(%rbx)
	movl	8(%rbp), %eax
	movl	%eax, 44(%rbx)
	movl	(%r14), %eax
	movl	%eax, 48(%rbx)
	movl	4(%r14), %eax
	movl	%eax, 52(%rbx)
	movl	8(%r14), %eax
	movl	%eax, 56(%rbx)
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_SemiInterpSetup, .Lfunc_end1-hypre_SemiInterpSetup
	.cfi_endproc

	.globl	hypre_SemiInterp
	.p2align	4, 0x90
	.type	hypre_SemiInterp,@function
hypre_SemiInterp:                       # @hypre_SemiInterp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$552, %rsp              # imm = 0x228
.Lcfi33:
	.cfi_def_cfa_offset 608
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movl	60(%rbp), %edi
	callq	hypre_BeginTiming
	movl	8(%rbp), %eax
	movl	%eax, 336(%rsp)         # 4-byte Spill
	movq	16(%rbp), %rax
	movq	%rax, 376(%rsp)         # 8-byte Spill
	leaq	48(%rbp), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	%r15, 456(%rsp)         # 8-byte Spill
	movq	24(%r15), %rax
	movq	(%rax), %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movq	%r14, 384(%rsp)         # 8-byte Spill
	movq	8(%r14), %rax
	movq	8(%rax), %rsi
	cmpl	$0, 8(%rsi)
	movq	%rbp, 368(%rsp)         # 8-byte Spill
	jle	.LBB2_1
# BB#6:                                 # %.preheader675.lr.ph
	leaq	24(%rbp), %rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	8(%rbx), %rcx
	movq	16(%rcx), %rdi
	movq	16(%rax), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	leaq	24(%rbx), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	leaq	16(%rbx), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	addq	$40, %rbx
	leaq	52(%rbp), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	leaq	56(%rbp), %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	leaq	64(%rsp), %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	leaq	76(%rsp), %rax
	movq	%rax, 416(%rsp)         # 8-byte Spill
	leaq	132(%rsp), %rax
	movq	%rax, 408(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	xorl	%ecx, %ecx
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	movq	%rsi, 256(%rsp)         # 8-byte Spill
	movq	%rdi, 248(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_7:                                # %.preheader675
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_8 Depth 2
                                        #     Child Loop BB2_13 Depth 2
                                        #       Child Loop BB2_15 Depth 3
                                        #         Child Loop BB2_17 Depth 4
                                        #         Child Loop BB2_21 Depth 4
	movq	240(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%rbp,4), %eax
	movslq	%ecx, %rcx
	leaq	(%rcx,%rcx,2), %rdx
	leaq	-1(%rcx), %rbx
	leaq	-2(%rdx,%rdx), %rcx
	.p2align	4, 0x90
.LBB2_8:                                #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %r14
	movq	%rcx, %r13
	leaq	1(%r14), %rbx
	leaq	6(%r13), %rcx
	cmpl	%eax, 4(%rdi,%r14,4)
	jne	.LBB2_8
# BB#9:                                 #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, 264(%rsp)         # 8-byte Spill
	movq	(%rsi), %rax
	leaq	(,%rbp,8), %rcx
	leaq	(%rcx,%rcx,2), %r12
	movl	(%rax,%r12), %ecx
	movl	%ecx, 64(%rsp)
	movl	4(%rax,%r12), %ecx
	movl	%ecx, 68(%rsp)
	movl	8(%rax,%r12), %ecx
	leaq	(%rax,%r12), %r15
	movl	%ecx, 72(%rsp)
	leaq	64(%rsp), %rdi
	movq	160(%rsp), %rsi         # 8-byte Reload
	movq	152(%rsp), %rdx         # 8-byte Reload
	leaq	76(%rsp), %rcx
	callq	hypre_StructMapCoarseToFine
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	384(%rsp), %rcx         # 8-byte Reload
	movq	16(%rcx), %rax
	movq	24(%rcx), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	(%rax), %rbx
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movslq	4(%rax,%r14,4), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	40(%rcx), %rax
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	movslq	(%rax,%rbp,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r15, %rdi
	leaq	132(%rsp), %rsi
	callq	hypre_BoxGetSize
	movq	%rbx, %r11
	movl	(%r11,%r12), %r8d
	movl	4(%r11,%r12), %r9d
	movl	12(%r11,%r12), %ecx
	movl	%ecx, %edi
	subl	%r8d, %edi
	incl	%edi
	cmpl	%r8d, %ecx
	movl	16(%r11,%r12), %edx
	movl	$0, %esi
	cmovsl	%esi, %edi
	movl	%edi, 48(%rsp)          # 4-byte Spill
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%edx, %ecx
	subl	%r9d, %ecx
	incl	%ecx
	cmpl	%r9d, %edx
	movl	8(%rdi,%r13,4), %r10d
	movl	20(%rdi,%r13,4), %edx
	cmovsl	%esi, %ecx
	movl	%edx, %r15d
	subl	%r10d, %r15d
	incl	%r15d
	cmpl	%r10d, %edx
	movl	12(%rdi,%r13,4), %r14d
	movl	24(%rdi,%r13,4), %edx
	cmovsl	%esi, %r15d
	movl	%edx, %ebx
	subl	%r14d, %ebx
	incl	%ebx
	cmpl	%r14d, %edx
	cmovsl	%esi, %ebx
	movl	132(%rsp), %eax
	movl	136(%rsp), %esi
	movl	140(%rsp), %ebp
	cmpl	%eax, %esi
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	%eax, %edx
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	cmovgel	%esi, %edx
	cmpl	%edx, %ebp
	movl	%ebp, 176(%rsp)         # 4-byte Spill
	cmovgel	%ebp, %edx
	testl	%edx, %edx
	jle	.LBB2_25
# BB#10:                                # %.lr.ph768
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpl	$0, 176(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_25
# BB#11:                                # %.lr.ph768
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpl	$0, 112(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_25
# BB#12:                                # %.preheader674.us.preheader
                                        #   in Loop: Header=BB2_7 Depth=1
	movl	72(%rsp), %ebp
	movq	168(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdx,2), %rdx
	subl	8(%r11,%rdx,8), %ebp
	movq	152(%rsp), %rdx         # 8-byte Reload
	movl	%r9d, 24(%rsp)          # 4-byte Spill
	movslq	(%rdx), %r9
	movl	84(%rsp), %r11d
	subl	16(%rdi,%r13,4), %r11d
	movq	224(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %edx
	imull	%r15d, %edx
	movq	120(%rsp), %r13         # 8-byte Reload
	movl	%r13d, %esi
	imull	%r9d, %esi
	movl	%edx, %eax
	subl	%esi, %eax
	movq	112(%rsp), %r12         # 8-byte Reload
	movl	%r14d, 8(%rsp)          # 4-byte Spill
	movl	%r12d, %r14d
	imull	%edx, %r14d
	leal	-1(%r12), %edi
	movl	%eax, 104(%rsp)         # 4-byte Spill
	imull	%edi, %eax
	addl	%edx, %eax
	subl	%esi, %eax
	movl	%eax, 288(%rsp)         # 4-byte Spill
	movl	%r10d, 40(%rsp)         # 4-byte Spill
	movl	48(%rsp), %r10d         # 4-byte Reload
	movl	%r10d, %eax
	subl	%r13d, %eax
	imull	%edi, %eax
	movl	64(%rsp), %edx
	subl	%r8d, %edx
	movl	68(%rsp), %esi
	subl	24(%rsp), %esi          # 4-byte Folded Reload
	imull	%ecx, %ebp
	addl	%esi, %ebp
	imull	%r10d, %ebp
	addl	%edx, %ebp
	movl	76(%rsp), %r8d
	subl	40(%rsp), %r8d          # 4-byte Folded Reload
	movl	80(%rsp), %edx
	subl	8(%rsp), %edx           # 4-byte Folded Reload
	movl	%r15d, %edi
	imull	%ebx, %edi
	imull	%ebx, %r11d
	movq	216(%rsp), %rsi         # 8-byte Reload
	imull	(%rsi), %edi
	movl	%r14d, 272(%rsp)        # 4-byte Spill
	subl	%r14d, %edi
	movl	%edi, 4(%rsp)           # 4-byte Spill
	subl	%r12d, %ecx
	imull	%r10d, %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	addl	%r10d, %eax
	subl	%r13d, %eax
	movl	%eax, 280(%rsp)         # 4-byte Spill
	addl	%edx, %r11d
	leal	-1(%r13), %eax
	imull	%r15d, %r11d
	movl	%r11d, %esi
	movq	%rax, 24(%rsp)          # 8-byte Spill
	incq	%rax
	imulq	%r9, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	addl	%r8d, %esi
	imull	%r12d, %r10d
	movl	%r10d, 60(%rsp)         # 4-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%r13d, %ecx
	andl	$3, %ecx
	movq	%r9, %r12
	shlq	$5, %r12
	leaq	(%r9,%r9,2), %rax
	shlq	$3, %rax
	movq	88(%rsp), %rdx          # 8-byte Reload
	leaq	(%rax,%rdx,8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r9, %rax
	shlq	$4, %rax
	leaq	(%rax,%rdx,8), %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,8), %r11
	leaq	(,%rdx,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	(%rax,%r9,8), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_13:                               # %.preheader674.us
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_15 Depth 3
                                        #         Child Loop BB2_17 Depth 4
                                        #         Child Loop BB2_21 Depth 4
	cmpl	$0, 120(%rsp)           # 4-byte Folded Reload
	movl	288(%rsp), %eax         # 4-byte Reload
	movl	280(%rsp), %edx         # 4-byte Reload
	jle	.LBB2_24
# BB#14:                                # %.preheader673.us.us.preheader
                                        #   in Loop: Header=BB2_13 Depth=2
	movl	%edi, 296(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movl	%ebp, 312(%rsp)         # 4-byte Spill
	movl	%ebp, %r13d
	movl	%esi, 304(%rsp)         # 4-byte Spill
	movl	%esi, %r14d
	.p2align	4, 0x90
.LBB2_15:                               # %.preheader673.us.us
                                        #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_13 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_17 Depth 4
                                        #         Child Loop BB2_21 Depth 4
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movslq	%r14d, %r14
	testl	%ecx, %ecx
	movslq	%r13d, %r13
	movq	%r14, %rbx
	movq	%r13, %rdi
	movl	$0, %r15d
	je	.LBB2_19
# BB#16:                                # %.prol.preheader
                                        #   in Loop: Header=BB2_15 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r13,8), %rax
	xorl	%r15d, %r15d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB2_17:                               #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_13 Depth=2
                                        #       Parent Loop BB2_15 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rax,%r15,8), %rdx
	movq	%rdx, (%r11,%rbx,8)
	addq	%r9, %rbx
	incq	%r15
	cmpl	%r15d, %ecx
	jne	.LBB2_17
# BB#18:                                # %.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB2_15 Depth=3
	leaq	(%r13,%r15), %rdi
.LBB2_19:                               # %.prol.loopexit
                                        #   in Loop: Header=BB2_15 Depth=3
	addq	40(%rsp), %r14          # 8-byte Folded Reload
	cmpl	$3, 24(%rsp)            # 4-byte Folded Reload
	jb	.LBB2_22
# BB#20:                                # %.preheader673.us.us.new
                                        #   in Loop: Header=BB2_15 Depth=3
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rbx,8), %rdx
	movq	320(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rbx,8), %rsi
	movq	184(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rbx,8), %rbx
	movq	32(%rsp), %rbp          # 8-byte Reload
	leaq	24(%rbp,%rdi,8), %r8
	movq	120(%rsp), %rdi         # 8-byte Reload
	movl	%edi, %r10d
	subl	%r15d, %r10d
	movq	96(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_21:                               #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_13 Depth=2
                                        #       Parent Loop BB2_15 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	-24(%r8), %rbp
	movq	%rbp, (%rax,%rdi)
	movq	-16(%r8), %rbp
	movq	%rbp, (%rbx,%rdi)
	movq	-8(%r8), %rbp
	movq	%rbp, (%rsi,%rdi)
	movq	(%r8), %rbp
	movq	%rbp, (%rdx,%rdi)
	addq	%r12, %rdi
	addq	$32, %r8
	addl	$-4, %r10d
	jne	.LBB2_21
.LBB2_22:                               # %._crit_edge736.us.us
                                        #   in Loop: Header=BB2_15 Depth=3
	addl	104(%rsp), %r14d        # 4-byte Folded Reload
	addl	48(%rsp), %r13d         # 4-byte Folded Reload
	movl	8(%rsp), %eax           # 4-byte Reload
	incl	%eax
	cmpl	112(%rsp), %eax         # 4-byte Folded Reload
	jne	.LBB2_15
# BB#23:                                #   in Loop: Header=BB2_13 Depth=2
	movl	272(%rsp), %eax         # 4-byte Reload
	movl	60(%rsp), %edx          # 4-byte Reload
	movl	312(%rsp), %ebp         # 4-byte Reload
	movl	304(%rsp), %esi         # 4-byte Reload
	movl	296(%rsp), %edi         # 4-byte Reload
.LBB2_24:                               # %._crit_edge742.us
                                        #   in Loop: Header=BB2_13 Depth=2
	addl	%ebp, %edx
	addl	%esi, %eax
	addl	4(%rsp), %eax           # 4-byte Folded Reload
	addl	(%rsp), %edx            # 4-byte Folded Reload
	incl	%edi
	cmpl	176(%rsp), %edi         # 4-byte Folded Reload
	movl	%edx, %ebp
	movl	%eax, %esi
	jne	.LBB2_13
.LBB2_25:                               # %._crit_edge769
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	168(%rsp), %rbp         # 8-byte Reload
	incq	%rbp
	movq	256(%rsp), %rsi         # 8-byte Reload
	movslq	8(%rsi), %rax
	cmpq	%rax, %rbp
	movq	248(%rsp), %rdi         # 8-byte Reload
	movq	264(%rsp), %rcx         # 8-byte Reload
	jl	.LBB2_7
# BB#26:
	movq	368(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB2_2
.LBB2_1:                                # %..preheader672_crit_edge
	leaq	24(%rbx), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	leaq	16(%rbx), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	addq	$40, %rbx
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	leaq	76(%rsp), %rax
	movq	%rax, 416(%rsp)         # 8-byte Spill
	leaq	64(%rsp), %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	leaq	132(%rsp), %rax
	movq	%rax, 408(%rsp)         # 8-byte Spill
	leaq	52(%rbp), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	leaq	56(%rbp), %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
.LBB2_2:                                # %.preheader672
	leaq	36(%rbp), %rax
	movq	%rax, 488(%rsp)         # 8-byte Spill
	movq	376(%rsp), %rax         # 8-byte Reload
	leaq	8(%rax), %rcx
	movq	%rcx, 448(%rsp)         # 8-byte Spill
	leaq	16(%rax), %rax
	movq	%rax, 440(%rsp)         # 8-byte Spill
                                        # implicit-def: %RDX
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_31 Depth 2
                                        #       Child Loop BB2_36 Depth 3
                                        #         Child Loop BB2_40 Depth 4
                                        #           Child Loop BB2_42 Depth 5
                                        #             Child Loop BB2_53 Depth 6
                                        #             Child Loop BB2_49 Depth 6
	cmpl	$1, %eax
	movl	%eax, 332(%rsp)         # 4-byte Spill
	je	.LBB2_27
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=1
	testl	%eax, %eax
	jne	.LBB2_29
# BB#5:                                 #   in Loop: Header=BB2_3 Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rsi
	movq	376(%rsp), %rdi         # 8-byte Reload
	leaq	544(%rsp), %rdx
	callq	hypre_InitializeIndtComputations
	movq	448(%rsp), %rax         # 8-byte Reload
	jmp	.LBB2_28
	.p2align	4, 0x90
.LBB2_27:                               #   in Loop: Header=BB2_3 Depth=1
	movq	544(%rsp), %rdi
	callq	hypre_FinalizeIndtComputations
	movq	440(%rsp), %rax         # 8-byte Reload
.LBB2_28:                               # %.sink.split
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	(%rax), %rdx
.LBB2_29:                               #   in Loop: Header=BB2_3 Depth=1
	movl	8(%rdx), %r8d
	testl	%r8d, %r8d
	jle	.LBB2_61
# BB#30:                                # %.lr.ph724
                                        #   in Loop: Header=BB2_3 Depth=1
	xorl	%esi, %esi
	movq	%rdx, 392(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_31:                               #   Parent Loop BB2_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_36 Depth 3
                                        #         Child Loop BB2_40 Depth 4
                                        #           Child Loop BB2_42 Depth 5
                                        #             Child Loop BB2_53 Depth 6
                                        #             Child Loop BB2_49 Depth 6
	movq	456(%rsp), %rcx         # 8-byte Reload
	movq	64(%rcx), %rax
	movq	(%rax,%rsi,8), %rdi
	leaq	4(%rdi), %r9
	cmpl	$0, 336(%rsp)           # 4-byte Folded Reload
	movq	(%rdx), %rax
	movq	(%rax,%rsi,8), %rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movq	40(%rcx), %rax
	movq	48(%rcx), %r10
	movq	(%rax), %r15
	movq	%rsi, 400(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rsi,2), %rsi
	leaq	(%r15,%rsi,8), %rdx
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rcx
	movq	(%rcx), %r12
	movq	%rdx, 504(%rsp)         # 8-byte Spill
	je	.LBB2_33
# BB#32:                                #   in Loop: Header=BB2_31 Depth=2
	movl	12(%r15,%rsi,8), %ecx
	movl	(%rdx), %ebp
	movl	%ecx, %ebx
	subl	%ebp, %ebx
	incl	%ebx
	cmpl	%ebp, %ecx
	movl	16(%r15,%rsi,8), %eax
	movl	4(%r15,%rsi,8), %ebp
	movl	$0, %r13d
	cmovsl	%r13d, %ebx
	movl	%eax, %ecx
	subl	%ebp, %ecx
	incl	%ecx
	cmpl	%ebp, %eax
	movslq	(%rdi), %rax
	leaq	(%r10,%rax,8), %rdi
	movq	208(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r11d
	movl	4(%rax), %r14d
	movl	8(%rax), %eax
	cmovsl	%r13d, %ecx
	imull	%eax, %ecx
	addl	%r14d, %ecx
	imull	%ebx, %ecx
	addl	%r11d, %ecx
	movslq	%ecx, %rcx
	shlq	$3, %rcx
	subq	%rcx, %rdi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	jmp	.LBB2_34
	.p2align	4, 0x90
.LBB2_33:                               #   in Loop: Header=BB2_31 Depth=2
	movslq	(%r9), %rax
	leaq	(%r10,%rax,8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	208(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r11d
	movl	4(%rax), %r14d
	movl	8(%rax), %eax
	movq	%rdi, %r9
	xorl	%r13d, %r13d
.LBB2_34:                               #   in Loop: Header=BB2_31 Depth=2
	movl	12(%r12,%rsi,8), %ecx
	movl	(%r12,%rsi,8), %ebx
	movl	%ecx, %edi
	subl	%ebx, %edi
	incl	%edi
	cmpl	%ebx, %ecx
	movl	16(%r12,%rsi,8), %ecx
	movl	4(%r12,%rsi,8), %ebx
	cmovsl	%r13d, %edi
	movl	%ecx, %edx
	subl	%ebx, %edx
	incl	%edx
	cmpl	%ebx, %ecx
	cmovsl	%r13d, %edx
	movq	232(%rsp), %rcx         # 8-byte Reload
	cmpl	$0, 8(%rcx)
	jle	.LBB2_60
# BB#35:                                # %.lr.ph716
                                        #   in Loop: Header=BB2_31 Depth=2
	movslq	(%r9), %rcx
	leaq	(%r10,%rcx,8), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %r9
	movq	192(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %rcx
	movq	400(%rsp), %r8          # 8-byte Reload
	movslq	(%rcx,%r8,4), %rbx
	leaq	(%r9,%rbx,8), %r13
	leaq	16(%r12,%rsi,8), %rcx
	movq	%rcx, 496(%rsp)         # 8-byte Spill
	imull	%edx, %eax
	addl	%r14d, %eax
	imull	%edi, %eax
	addl	%r11d, %eax
	movslq	%eax, %rsi
	leaq	(%r13,%rsi,8), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	208(%rsp), %rcx         # 8-byte Reload
	movslq	12(%rcx), %rax
	imull	20(%rcx), %edx
	addl	16(%rcx), %edx
	imull	%edi, %edx
	movslq	%edx, %rdx
	addq	%rax, %rdx
	leaq	(%r13,%rdx,8), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	leaq	(,%r8,8), %rax
	movq	232(%rsp), %rcx         # 8-byte Reload
	leaq	(%rax,%rax,2), %rax
	leaq	4(%r15,%rax), %rdi
	movq	%rdi, 432(%rsp)         # 8-byte Spill
	leaq	8(%r12,%rax), %rax
	movq	%rax, 480(%rsp)         # 8-byte Spill
	leaq	(%rbx,%rsi), %rax
	leaq	(%r9,%rax,8), %rax
	movq	%rax, 472(%rsp)         # 8-byte Spill
	leaq	(%rbx,%rdx), %rax
	movq	%r9, 304(%rsp)          # 8-byte Spill
	leaq	(%r9,%rax,8), %rax
	movq	%rax, 464(%rsp)         # 8-byte Spill
	shlq	$3, %rsi
	movq	%rsi, 536(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rbx,8), %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	%rdx, 528(%rsp)         # 8-byte Spill
	leaq	(,%rdx,8), %rax
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	leaq	(,%rbx,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rdx
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	leaq	16(%rax), %rax
	movq	%rax, 520(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movq	%r13, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_36:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_31 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_40 Depth 4
                                        #           Child Loop BB2_42 Depth 5
                                        #             Child Loop BB2_53 Depth 6
                                        #             Child Loop BB2_49 Depth 6
	movq	(%rcx), %rax
	movq	%rdx, 512(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rdx,2), %rcx
	leaq	(%rax,%rcx,8), %rbp
	movl	(%rax,%rcx,8), %edx
	movl	%edx, 76(%rsp)
	movl	4(%rax,%rcx,8), %edx
	movl	%edx, 80(%rsp)
	movl	8(%rax,%rcx,8), %eax
	movl	%eax, 84(%rsp)
	movq	416(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	152(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdx
	movq	424(%rsp), %rcx         # 8-byte Reload
	callq	hypre_StructMapFineToCoarse
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	408(%rsp), %rdx         # 8-byte Reload
	callq	hypre_BoxGetStrideSize
	movq	496(%rsp), %rcx         # 8-byte Reload
	movl	-16(%rcx), %r8d
	movl	-4(%rcx), %eax
	movl	%eax, %edx
	subl	%r8d, %edx
	incl	%edx
	cmpl	%r8d, %eax
	movl	-12(%rcx), %r10d
	movl	(%rcx), %eax
	movl	$0, %ecx
	cmovsl	%ecx, %edx
	movl	%eax, %edi
	subl	%r10d, %edi
	incl	%edi
	cmpl	%r10d, %eax
	movq	504(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %ebp
	movq	432(%rsp), %rsi         # 8-byte Reload
	movl	8(%rsi), %eax
	cmovsl	%ecx, %edi
	movl	%eax, %ebx
	subl	%ebp, %ebx
	incl	%ebx
	cmpl	%ebp, %eax
	movl	(%rsi), %r11d
	movl	12(%rsi), %eax
	cmovsl	%ecx, %ebx
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	movl	%eax, %r12d
	subl	%r11d, %r12d
	incl	%r12d
	cmpl	%r11d, %eax
	cmovsl	%ecx, %r12d
	movl	132(%rsp), %eax
	movl	136(%rsp), %ecx
	movl	140(%rsp), %esi
	cmpl	%eax, %ecx
	movq	%rax, 8(%rsp)           # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	cmovgel	%ecx, %eax
	cmpl	%eax, %esi
	movl	%esi, 128(%rsp)         # 4-byte Spill
	cmovgel	%esi, %eax
	testl	%eax, %eax
	jle	.LBB2_58
# BB#37:                                # %.lr.ph710
                                        #   in Loop: Header=BB2_36 Depth=3
	cmpl	$0, 128(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_58
# BB#38:                                # %.lr.ph710
                                        #   in Loop: Header=BB2_36 Depth=3
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_58
# BB#39:                                # %.preheader671.us.preheader
                                        #   in Loop: Header=BB2_36 Depth=3
	movq	152(%rsp), %rax         # 8-byte Reload
	movslq	(%rax), %rsi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	224(%rsp), %rax         # 8-byte Reload
	movl	%ebp, 104(%rsp)         # 4-byte Spill
	movl	%r8d, %ebp
	movl	(%rax), %r8d
	imull	%edx, %r8d
	movl	%edx, %ecx
	imull	%edi, %ecx
	movq	216(%rsp), %rax         # 8-byte Reload
	imull	(%rax), %ecx
	movl	32(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %r14d
	movq	8(%rsp), %rax           # 8-byte Reload
	subl	%eax, %r14d
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%esi, %eax
	movl	%r8d, %esi
	subl	%eax, %esi
	movl	%r12d, %ebx
	movq	24(%rsp), %r9           # 8-byte Reload
	subl	%r9d, %ebx
	imull	%r15d, %ebx
	movl	%ebx, 348(%rsp)         # 4-byte Spill
	leal	-1(%r9), %ebx
	imull	%ebx, %r14d
	imull	%ebx, %esi
	movl	%r9d, %ebx
	imull	%r8d, %ebx
	movl	%ebx, 344(%rsp)         # 4-byte Spill
	subl	%ebx, %ecx
	movl	%ecx, 360(%rsp)         # 4-byte Spill
	addl	%r15d, %r14d
	subl	8(%rsp), %r14d          # 4-byte Folded Reload
	movl	%r14d, 356(%rsp)        # 4-byte Spill
	movl	%r8d, 112(%rsp)         # 4-byte Spill
	addl	%r8d, %esi
	subl	%eax, %esi
	movl	%esi, 352(%rsp)         # 4-byte Spill
	movl	76(%rsp), %eax
	subl	%ebp, %eax
	movl	80(%rsp), %ecx
	subl	%r10d, %ecx
	movl	84(%rsp), %ebx
	movq	480(%rsp), %rsi         # 8-byte Reload
	subl	(%rsi), %ebx
	imull	%edi, %ebx
	movq	8(%rsp), %rsi           # 8-byte Reload
	leal	-1(%rsi), %esi
	addl	%ecx, %ebx
	movl	%r15d, %ecx
	imull	24(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ecx, 340(%rsp)         # 4-byte Spill
	imull	%edx, %ebx
	addl	%eax, %ebx
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movl	64(%rsp), %eax
	movl	68(%rsp), %ecx
	subl	104(%rsp), %eax         # 4-byte Folded Reload
	subl	%r11d, %ecx
	movl	72(%rsp), %edi
	movq	432(%rsp), %rdx         # 8-byte Reload
	subl	4(%rdx), %edi
	imull	%r12d, %edi
	addl	%ecx, %edi
	imull	%r15d, %edi
	addl	%eax, %edi
	movl	%edi, (%rsp)            # 4-byte Spill
	movq	472(%rsp), %rdx         # 8-byte Reload
	leaq	8(%rdx,%rsi,8), %rax
	leaq	1(%rsi), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rcx, %rdi
	movabsq	$8589934588, %rcx       # imm = 0x1FFFFFFFC
	andq	%rcx, %rdi
	setne	%cl
	movq	40(%rsp), %r8           # 8-byte Reload
	cmpl	$1, %r8d
	sete	%bl
	andb	%cl, %bl
	movb	%bl, 320(%rsp)          # 1-byte Spill
	cmpq	%rax, %r13
	leaq	8(%r13,%rsi,8), %rbx
	sbbb	%al, %al
	cmpq	%rbx, %rdx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 60(%rsp)           # 1-byte Spill
	movq	464(%rsp), %rcx         # 8-byte Reload
	leaq	8(%rcx,%rsi,8), %rax
	cmpq	%rax, %r13
	sbbb	%al, %al
	movq	%rbx, 272(%rsp)         # 8-byte Spill
	cmpq	%rbx, %rcx
	sbbb	%cl, %cl
	andb	%al, %cl
	andb	$1, %cl
	movb	%cl, 168(%rsp)          # 1-byte Spill
	movq	%r8, %rcx
	movq	%rcx, %rax
	movq	%rdi, 160(%rsp)         # 8-byte Spill
	imulq	%rdi, %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movq	%rcx, %rdx
	shlq	$5, %rdx
	movq	%rcx, %r10
	shlq	$4, %r10
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax,%rsi,8), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 120(%rsp)         # 8-byte Spill
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	xorl	%esi, %esi
	movq	%rdx, 240(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_40:                               # %.preheader671.us
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_31 Depth=2
                                        #       Parent Loop BB2_36 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB2_42 Depth 5
                                        #             Child Loop BB2_53 Depth 6
                                        #             Child Loop BB2_49 Depth 6
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movl	352(%rsp), %eax         # 4-byte Reload
	movl	356(%rsp), %ecx         # 4-byte Reload
	jle	.LBB2_57
# BB#41:                                # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB2_40 Depth=4
	movl	%esi, 364(%rsp)         # 4-byte Spill
	xorl	%r14d, %r14d
	movl	4(%rsp), %r13d          # 4-byte Reload
	movl	(%rsp), %r12d           # 4-byte Reload
	jmp	.LBB2_42
.LBB2_52:                               # %vector.body.preheader
                                        #   in Loop: Header=BB2_42 Depth=5
	movq	520(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r8,8), %rsi
	leaq	16(%rdx,%r8,8), %rcx
	leaq	(%r15,%rax,8), %rax
	addq	248(%rsp), %r11         # 8-byte Folded Reload
	movq	160(%rsp), %rdi         # 8-byte Reload
	addq	%rdi, %r8
	movq	536(%rsp), %rbp         # 8-byte Reload
	movq	528(%rsp), %rbx         # 8-byte Reload
	movq	240(%rsp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_53:                               # %vector.body
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_31 Depth=2
                                        #       Parent Loop BB2_36 Depth=3
                                        #         Parent Loop BB2_40 Depth=4
                                        #           Parent Loop BB2_42 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movupd	-16(%rcx), %xmm0
	movupd	(%rcx), %xmm1
	movupd	(%rbp,%rax), %xmm2
	movupd	16(%rbp,%rax), %xmm3
	mulpd	%xmm0, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	-16(%rsi), %xmm0
	movupd	(%rsi), %xmm1
	movupd	(%rax,%rbx,8), %xmm4
	movupd	16(%rax,%rbx,8), %xmm5
	mulpd	%xmm0, %xmm4
	mulpd	%xmm1, %xmm5
	addpd	%xmm2, %xmm4
	addpd	%xmm3, %xmm5
	movupd	%xmm4, (%rax)
	movupd	%xmm5, 16(%rax)
	addq	$32, %rsi
	addq	$32, %rcx
	addq	%rdx, %rax
	addq	$-4, %rdi
	jne	.LBB2_53
# BB#54:                                # %middle.block
                                        #   in Loop: Header=BB2_42 Depth=5
	movq	160(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, 104(%rsp)         # 8-byte Folded Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	jne	.LBB2_44
	jmp	.LBB2_55
	.p2align	4, 0x90
.LBB2_42:                               # %.preheader.us.us
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_31 Depth=2
                                        #       Parent Loop BB2_36 Depth=3
                                        #         Parent Loop BB2_40 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB2_53 Depth 6
                                        #             Child Loop BB2_49 Depth 6
	movslq	%r12d, %r8
	movslq	%r13d, %rax
	cmpq	$3, 104(%rsp)           # 8-byte Folded Reload
	jbe	.LBB2_43
# BB#50:                                # %min.iters.checked
                                        #   in Loop: Header=BB2_42 Depth=5
	cmpb	$0, 320(%rsp)           # 1-byte Folded Reload
	je	.LBB2_43
# BB#51:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_42 Depth=5
	movl	112(%rsp), %ecx         # 4-byte Reload
	imull	%r14d, %ecx
	addl	4(%rsp), %ecx           # 4-byte Folded Reload
	movslq	%ecx, %r11
	movq	96(%rsp), %r15          # 8-byte Reload
	leaq	(%r15,%r11,8), %rsi
	movq	272(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r11,8), %r9
	movl	32(%rsp), %edi          # 4-byte Reload
	imull	%r14d, %edi
	addl	(%rsp), %edi            # 4-byte Folded Reload
	movslq	%edi, %rdi
	movq	264(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rbp
	cmpq	%rbp, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rbp
	sbbb	%bl, %bl
	cmpq	%r9, %rbp
	sbbb	%cl, %cl
	andb	%bl, %cl
	movq	256(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rdi,8), %rbp
	andb	$1, %cl
	orb	60(%rsp), %cl           # 1-byte Folded Reload
	cmpq	%rbp, %rsi
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdi,8), %rsi
	sbbb	%dil, %dil
	cmpq	%r9, %rsi
	sbbb	%bl, %bl
	andb	%dil, %bl
	andb	$1, %bl
	orb	%cl, %bl
	orb	168(%rsp), %bl          # 1-byte Folded Reload
	je	.LBB2_52
	.p2align	4, 0x90
.LBB2_43:                               #   in Loop: Header=BB2_42 Depth=5
	movq	%rax, %r11
	xorl	%eax, %eax
.LBB2_44:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_42 Depth=5
	movq	8(%rsp), %rcx           # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%eax, %ecx
	testb	$1, %cl
	jne	.LBB2_46
# BB#45:                                #   in Loop: Header=BB2_42 Depth=5
	movl	%eax, %ebx
	cmpl	%eax, 120(%rsp)         # 4-byte Folded Reload
	jne	.LBB2_48
	jmp	.LBB2_55
	.p2align	4, 0x90
.LBB2_46:                               # %scalar.ph.prol
                                        #   in Loop: Header=BB2_42 Depth=5
	movq	48(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%r8,8), %xmm0     # xmm0 = mem[0],zero
	movq	296(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r11,8), %xmm0
	movq	16(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%r8,8), %xmm1     # xmm1 = mem[0],zero
	movq	176(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r11,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	96(%rsp), %rcx          # 8-byte Reload
	movsd	%xmm1, (%rcx,%r11,8)
	incq	%r8
	addq	40(%rsp), %r11          # 8-byte Folded Reload
	leal	1(%rax), %ebx
	cmpl	%eax, 120(%rsp)         # 4-byte Folded Reload
	je	.LBB2_55
.LBB2_48:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB2_42 Depth=5
	movq	288(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%r11,8), %r15
	movq	280(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r11,8), %r9
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r11,8), %rax
	movq	312(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r11,8), %rsi
	addq	40(%rsp), %r11          # 8-byte Folded Reload
	leaq	(%rcx,%r11,8), %rcx
	leaq	(%rdi,%r11,8), %rdx
	movq	48(%rsp), %rdi          # 8-byte Reload
	leaq	8(%rdi,%r8,8), %r11
	movq	184(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%r8,8), %rdi
	movq	8(%rsp), %rbp           # 8-byte Reload
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill>
	subl	%ebx, %ebp
	movq	304(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_49:                               # %scalar.ph
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_31 Depth=2
                                        #       Parent Loop BB2_36 Depth=3
                                        #         Parent Loop BB2_40 Depth=4
                                        #           Parent Loop BB2_42 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movsd	-8(%r11), %xmm0         # xmm0 = mem[0],zero
	mulsd	(%r15,%rbx), %xmm0
	movsd	-8(%rdi), %xmm1         # xmm1 = mem[0],zero
	mulsd	(%r9,%rbx), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax,%rbx)
	movsd	(%r11), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rdx,%rbx), %xmm0
	movsd	(%rdi), %xmm1           # xmm1 = mem[0],zero
	mulsd	(%rcx,%rbx), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsi,%rbx)
	addq	%r10, %rbx
	addq	$16, %r11
	addq	$16, %rdi
	addl	$-2, %ebp
	jne	.LBB2_49
.LBB2_55:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB2_42 Depth=5
	addl	32(%rsp), %r12d         # 4-byte Folded Reload
	addl	112(%rsp), %r13d        # 4-byte Folded Reload
	incl	%r14d
	cmpl	24(%rsp), %r14d         # 4-byte Folded Reload
	jne	.LBB2_42
# BB#56:                                #   in Loop: Header=BB2_40 Depth=4
	movl	344(%rsp), %eax         # 4-byte Reload
	movl	340(%rsp), %ecx         # 4-byte Reload
	movq	96(%rsp), %r13          # 8-byte Reload
	movl	364(%rsp), %esi         # 4-byte Reload
.LBB2_57:                               # %._crit_edge684.us
                                        #   in Loop: Header=BB2_40 Depth=4
	addl	(%rsp), %ecx            # 4-byte Folded Reload
	addl	4(%rsp), %eax           # 4-byte Folded Reload
	addl	348(%rsp), %ecx         # 4-byte Folded Reload
	addl	360(%rsp), %eax         # 4-byte Folded Reload
	incl	%esi
	cmpl	128(%rsp), %esi         # 4-byte Folded Reload
	movl	%ecx, (%rsp)            # 4-byte Spill
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jne	.LBB2_40
.LBB2_58:                               # %._crit_edge711
                                        #   in Loop: Header=BB2_36 Depth=3
	movq	512(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	movq	232(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %rdx
	jl	.LBB2_36
# BB#59:                                # %._crit_edge717.loopexit
                                        #   in Loop: Header=BB2_31 Depth=2
	movq	392(%rsp), %rax         # 8-byte Reload
	movl	8(%rax), %r8d
.LBB2_60:                               # %._crit_edge717
                                        #   in Loop: Header=BB2_31 Depth=2
	movq	400(%rsp), %rsi         # 8-byte Reload
	incq	%rsi
	movslq	%r8d, %rax
	cmpq	%rax, %rsi
	movq	392(%rsp), %rdx         # 8-byte Reload
	jl	.LBB2_31
.LBB2_61:                               # %._crit_edge725
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	332(%rsp), %eax         # 4-byte Reload
	incl	%eax
	cmpl	$2, %eax
	jne	.LBB2_3
# BB#62:
	movq	384(%rsp), %rax         # 8-byte Reload
	movl	72(%rax), %eax
	leal	(%rax,%rax,2), %edi
	callq	hypre_IncFLOPCount
	movq	368(%rsp), %rax         # 8-byte Reload
	movl	60(%rax), %edi
	callq	hypre_EndTiming
	xorl	%eax, %eax
	addq	$552, %rsp              # imm = 0x228
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	hypre_SemiInterp, .Lfunc_end2-hypre_SemiInterp
	.cfi_endproc

	.globl	hypre_SemiInterpDestroy
	.p2align	4, 0x90
	.type	hypre_SemiInterpDestroy,@function
hypre_SemiInterpDestroy:                # @hypre_SemiInterpDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 16
.Lcfi41:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB3_2
# BB#1:
	movq	(%rbx), %rdi
	callq	hypre_StructMatrixDestroy
	movq	16(%rbx), %rdi
	callq	hypre_ComputePkgDestroy
	movl	60(%rbx), %edi
	callq	hypre_FinalizeTiming
	movq	%rbx, %rdi
	callq	hypre_Free
.LBB3_2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	hypre_SemiInterpDestroy, .Lfunc_end3-hypre_SemiInterpDestroy
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"SemiInterp"
	.size	.L.str, 11


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
