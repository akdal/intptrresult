	.text
	.file	"updateMetricsData.bc"
	.globl	updateMetricsData
	.p2align	4, 0x90
	.type	updateMetricsData,@function
updateMetricsData:                      # @updateMetricsData
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	216(%r14), %eax
	cmpl	$3, %eax
	je	.LBB0_5
# BB#1:
	cmpl	$2, %eax
	je	.LBB0_4
# BB#2:
	cmpl	$1, %eax
	jne	.LBB0_13
# BB#3:
	leaq	24(%r14), %rbx
	jmp	.LBB0_6
.LBB0_5:
	leaq	152(%r14), %rbx
	jmp	.LBB0_6
.LBB0_4:
	leaq	88(%r14), %rbx
.LBB0_6:
	callq	getTime
	subq	(%rbx), %rax
	js	.LBB0_7
# BB#8:
	cmpq	40(%rbx), %rax
	jge	.LBB0_10
# BB#9:
	movq	%rax, 40(%rbx)
.LBB0_10:
	cmpq	32(%rbx), %rax
	jle	.LBB0_12
# BB#11:
	movq	%rax, 32(%rbx)
.LBB0_12:
	cvtsi2sdq	%rax, %xmm0
	movapd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm1
	movupd	16(%rbx), %xmm2
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	addpd	%xmm2, %xmm0
	movupd	%xmm0, 16(%rbx)
	incq	8(%rbx)
	jmp	.LBB0_13
.LBB0_7:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$updateMetricsData.name, %edi
	movl	$1, %esi
	callq	errorMessage
	callq	flushErrorMessage
.LBB0_13:
	movl	$5, 216(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	updateMetricsData, .Lfunc_end0-updateMetricsData
	.cfi_endproc

	.type	updateMetricsData.name,@object # @updateMetricsData.name
	.data
	.p2align	4
updateMetricsData.name:
	.asciz	"updateMetricsData"
	.size	updateMetricsData.name, 18

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"lastTimeMark doesn't seem to be set"
	.size	.L.str, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
