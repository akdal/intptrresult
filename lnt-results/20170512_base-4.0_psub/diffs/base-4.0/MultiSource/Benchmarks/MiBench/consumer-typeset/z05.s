	.text
	.file	"z05.bc"
	.globl	ReadPrependDef
	.p2align	4, 0x90
	.type	ReadPrependDef,@function
ReadPrependDef:                         # @ReadPrependDef
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movl	%edi, %r14d
	callq	LexGetToken
	movq	%rax, %rbx
	movq	%rbx, 8(%rsp)
	cmpb	$102, 32(%rbx)
	jne	.LBB0_1
# BB#2:
	leaq	8(%rsp), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbp, %rsi
	callq	Parse
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %rbx
	leaq	32(%rbx), %r8
	movb	32(%rbx), %al
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB0_3
# BB#4:
	addq	$64, %rbx
	xorl	%eax, %eax
	cmpl	$114, %r14d
	setne	%al
	incl	%eax
	movl	$.L.str.3, %esi
	movl	$6, %ecx
	movq	%rbx, %rdi
	movq	%r8, %rdx
	movl	%eax, %r8d
	callq	DefineFile
	jmp	.LBB0_5
.LBB0_1:
	leaq	32(%rbx), %r8
	movl	$5, %edi
	movl	$5, %esi
	movl	$.L.str, %edx
	movl	$2, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	movq	%rbx, %rdx
	addq	$33, %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	jmp	.LBB0_5
.LBB0_3:
	movl	$5, %edi
	movl	$6, %esi
	movl	$.L.str.2, %edx
	movl	$2, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%rbx, %rdi
	callq	DisposeObject
.LBB0_5:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	ReadPrependDef, .Lfunc_end0-ReadPrependDef
	.cfi_endproc

	.globl	ReadDatabaseDef
	.p2align	4, 0x90
	.type	ReadDatabaseDef,@function
ReadDatabaseDef:                        # @ReadDatabaseDef
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 64
.Lcfi12:
	.cfi_offset %rbx, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movl	%edi, %r14d
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#2:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_3
.LBB1_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_3:
	movb	$17, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	jmp	.LBB1_4
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_4 Depth=1
	leaq	32(%rbp), %r8
	leaq	64(%rbp), %r9
	movl	$5, %edi
	movl	$7, %esi
	movl	$.L.str.5, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	%rbp, zz_hold(%rip)
	movzbl	32(%rbp), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	movq	%rbp, %rdx
	addq	$33, %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	leaq	zz_free(,%rax,8), %rcx
	movq	zz_free(,%rax,8), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	%rax, (%rcx)
.LBB1_4:                                # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	callq	LexGetToken
	movq	%rax, %rbp
	movq	%rbp, 16(%rsp)
	movzbl	32(%rbp), %eax
	cmpb	$11, %al
	je	.LBB1_7
# BB#5:                                 # %.backedge
                                        #   in Loop: Header=BB1_4 Depth=1
	cmpb	$2, %al
	jne	.LBB1_19
# BB#6:                                 # %.thread
                                        #   in Loop: Header=BB1_4 Depth=1
	movzbl	32(%rbp), %eax
	cmpb	$2, %al
	jne	.LBB1_18
	jmp	.LBB1_9
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_4 Depth=1
	movzbl	32(%rbp), %eax
	cmpb	$64, 64(%rbp)
	jne	.LBB1_20
# BB#8:                                 #   in Loop: Header=BB1_4 Depth=1
	cmpb	$2, %al
	jne	.LBB1_18
.LBB1_9:                                #   in Loop: Header=BB1_4 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_10
# BB#11:                                #   in Loop: Header=BB1_4 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_12
.LBB1_10:                               #   in Loop: Header=BB1_4 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_12:                               #   in Loop: Header=BB1_4 Depth=1
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB1_15
# BB#13:                                #   in Loop: Header=BB1_4 Depth=1
	testq	%rax, %rax
	je	.LBB1_15
# BB#14:                                #   in Loop: Header=BB1_4 Depth=1
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_15:                               #   in Loop: Header=BB1_4 Depth=1
	movq	%rax, zz_res(%rip)
	movq	16(%rsp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_4
# BB#16:                                #   in Loop: Header=BB1_4 Depth=1
	testq	%rcx, %rcx
	je	.LBB1_4
# BB#17:                                #   in Loop: Header=BB1_4 Depth=1
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	addq	$24, %rdx
	movq	%rax, (%rdx)
	jmp	.LBB1_4
.LBB1_19:                               # %.thread35
	movb	32(%rbp), %al
.LBB1_20:                               # %.loopexit
	cmpb	$102, %al
	jne	.LBB1_21
# BB#22:
	cmpq	%rbx, 8(%rbx)
	jne	.LBB1_24
# BB#23:
	addq	$32, %rbp
	movl	$5, %edi
	movl	$9, %esi
	movl	$.L.str.9, %edx
	movl	$2, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
.LBB1_24:
	leaq	16(%rsp), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	callq	Parse
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %rbp
	leaq	32(%rbp), %r15
	movb	32(%rbp), %al
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB1_25
# BB#27:
	movq	%rbp, %rdi
	addq	$64, %rdi
	movl	$.L.str.10, %esi
	callq	StringEndsWith
	testl	%eax, %eax
	je	.LBB1_29
# BB#28:
	movq	$.L.str.8, (%rsp)
	movl	$5, %edi
	movl	$47, %esi
	movl	$.L.str.11, %edx
	movl	$2, %ecx
	movl	$.L.str.10, %r9d
	jmp	.LBB1_26
.LBB1_21:
	leaq	32(%rbp), %r8
	movq	$.L.str.8, (%rsp)
	movl	$5, %edi
	movl	$8, %esi
	movl	$.L.str.6, %edx
	movl	$2, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%rbp, zz_hold(%rip)
	movzbl	32(%rbp), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	movq	%rbp, %rdx
	addq	$33, %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbp)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	jmp	.LBB1_31
.LBB1_25:
	movl	$5, %edi
	movl	$10, %esi
	movl	$.L.str.2, %edx
	movl	$2, %ecx
	movl	$.L.str.8, %r9d
.LBB1_26:
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
	movq	%rbp, %rdi
	callq	DisposeObject
.LBB1_31:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_29:
	cmpq	%rbx, 8(%rbx)
	je	.LBB1_31
# BB#30:
	xorl	%esi, %esi
	cmpl	$116, %r14d
	setne	%sil
	addl	$3, %esi
	movl	InMemoryDbIndexes(%rip), %r8d
	movl	$1, %edx
	movq	%rbp, %rdi
	movq	%rbx, %rcx
	callq	DbLoad
	jmp	.LBB1_31
.Lfunc_end1:
	.size	ReadDatabaseDef, .Lfunc_end1-ReadDatabaseDef
	.cfi_endproc

	.globl	ReadDefinitions
	.p2align	4, 0x90
	.type	ReadDefinitions,@function
ReadDefinitions:                        # @ReadDefinitions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 112
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, %r14
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	(%rdi), %rbp
	movq	%rbp, (%rsp)
	movl	%r12d, 28(%rsp)         # 4-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	cmpb	$-113, %r12b
	je	.LBB2_7
	jmp	.LBB2_3
	.p2align	4, 0x90
.LBB2_1:                                # %ReadLangDef.exit
                                        #   in Loop: Header=BB2_7 Depth=1
	callq	LexGetToken
	movq	%rax, %rbp
	movq	%rbp, (%rsp)
.LBB2_2:                                # %.backedge1916
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpb	$-113, %r12b
	je	.LBB2_7
.LBB2_3:
	cmpb	$11, 32(%rbp)
	jne	.LBB2_5
# BB#4:
	leaq	64(%rbp), %rdi
	movl	$.L.str.12, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_7
.LBB2_5:
	cmpb	$11, 32(%rbp)
	jne	.LBB2_287
# BB#6:
	leaq	64(%rbp), %rdi
	movl	$.L.str.13, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_287
.LBB2_7:                                # %.critedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_39 Depth 2
                                        #     Child Loop BB2_96 Depth 2
                                        #       Child Loop BB2_99 Depth 3
                                        #         Child Loop BB2_100 Depth 4
                                        #     Child Loop BB2_117 Depth 2
                                        #     Child Loop BB2_164 Depth 2
                                        #     Child Loop BB2_209 Depth 2
                                        #     Child Loop BB2_259 Depth 2
                                        #       Child Loop BB2_260 Depth 3
                                        #     Child Loop BB2_186 Depth 2
                                        #     Child Loop BB2_273 Depth 2
                                        #     Child Loop BB2_66 Depth 2
                                        #     Child Loop BB2_75 Depth 2
	cmpb	$11, 32(%rbp)
	jne	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%rbp), %rdi
	movl	$.L.str.14, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_31
.LBB2_9:                                #   in Loop: Header=BB2_7 Depth=1
	movb	32(%rbp), %al
	movl	%eax, %ecx
	addb	$-114, %cl
	cmpb	$2, %cl
	jb	.LBB2_12
# BB#10:                                #   in Loop: Header=BB2_7 Depth=1
	movl	%eax, %ecx
	addb	$-116, %cl
	cmpb	$2, %cl
	jae	.LBB2_14
# BB#11:                                #   in Loop: Header=BB2_7 Depth=1
	movzbl	32(%rbp), %edi
	movq	%r14, %rsi
	callq	ReadDatabaseDef
	jmp	.LBB2_13
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_7 Depth=1
	movzbl	32(%rbp), %edi
	movq	%r14, %rsi
	callq	ReadPrependDef
.LBB2_13:                               # %.backedge1916.backedge
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB2_1
	.p2align	4, 0x90
.LBB2_14:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, %al
	jne	.LBB2_19
# BB#15:                                #   in Loop: Header=BB2_7 Depth=1
	cmpb	$100, 64(%rbp)
	jne	.LBB2_19
# BB#16:                                #   in Loop: Header=BB2_7 Depth=1
	cmpb	$101, 65(%rbp)
	jne	.LBB2_19
# BB#17:                                #   in Loop: Header=BB2_7 Depth=1
	cmpb	$102, 66(%rbp)
	jne	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_7 Depth=1
	cmpb	$0, 67(%rbp)
	je	.LBB2_29
	.p2align	4, 0x90
.LBB2_19:                               # %.thread2019
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%rbp)
	jne	.LBB2_21
# BB#20:                                #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%rbp), %rdi
	movl	$.L.str.16, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_29
.LBB2_21:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%rbp)
	jne	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%rbp), %rdi
	movl	$.L.str.12, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_29
.LBB2_23:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%rbp)
	jne	.LBB2_25
# BB#24:                                #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%rbp), %rdi
	movl	$.L.str.13, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_29
.LBB2_25:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%rbp)
	jne	.LBB2_27
# BB#26:                                #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%rbp), %rdi
	movl	$.L.str.17, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_29
.LBB2_27:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%rbp)
	jne	.LBB2_287
# BB#28:                                #   in Loop: Header=BB2_7 Depth=1
	addq	$64, %rbp
	movl	$.L.str.18, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_287
.LBB2_29:                               #   in Loop: Header=BB2_7 Depth=1
	callq	BodyParNotAllowed
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB2_33
# BB#30:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_34
.LBB2_31:                               #   in Loop: Header=BB2_7 Depth=1
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB2_63
# BB#32:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_64
.LBB2_33:                               #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB2_34:                               #   in Loop: Header=BB2_7 Depth=1
	movb	$17, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	movq	(%rsp), %rbp
	xorl	%r13d, %r13d
	cmpb	$11, 32(%rbp)
	jne	.LBB2_110
# BB#35:                                #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%rbp), %rbx
	movl	$.L.str.13, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_84
# BB#36:                                #   in Loop: Header=BB2_7 Depth=1
	movl	$.L.str.17, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_110
# BB#37:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rbp, zz_hold(%rip)
	movzbl	32(%rbp), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbp), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbp)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	%r14, 8(%rsp)           # 8-byte Spill
	jmp	.LBB2_39
	.p2align	4, 0x90
.LBB2_38:                               # %.backedge1915
                                        #   in Loop: Header=BB2_39 Depth=2
	callq	Error
	movq	(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
.LBB2_39:                               # %.backedge1915
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	LexGetToken
	movq	%rax, %rbx
	movq	%rbx, (%rsp)
	movzbl	32(%rbx), %eax
	cmpb	$2, %al
	je	.LBB2_50
# BB#40:                                # %.backedge1915
                                        #   in Loop: Header=BB2_39 Depth=2
	cmpb	$11, %al
	jne	.LBB2_111
# BB#41:                                #   in Loop: Header=BB2_39 Depth=2
	leaq	64(%rbx), %rbp
	movl	$.L.str.18, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_111
# BB#42:                                #   in Loop: Header=BB2_39 Depth=2
	movzbl	32(%rbx), %eax
	cmpb	$11, %al
	jne	.LBB2_49
# BB#43:                                #   in Loop: Header=BB2_39 Depth=2
	cmpb	$100, (%rbp)
	jne	.LBB2_48
# BB#44:                                #   in Loop: Header=BB2_39 Depth=2
	cmpb	$101, 65(%rbx)
	jne	.LBB2_47
# BB#45:                                #   in Loop: Header=BB2_39 Depth=2
	cmpb	$102, 66(%rbx)
	jne	.LBB2_47
# BB#46:                                #   in Loop: Header=BB2_39 Depth=2
	cmpb	$0, 67(%rbx)
	je	.LBB2_111
.LBB2_47:                               # %.thread2027
                                        #   in Loop: Header=BB2_39 Depth=2
	cmpb	$11, %al
	jne	.LBB2_49
.LBB2_48:                               # %.thread2027.thread
                                        #   in Loop: Header=BB2_39 Depth=2
	movl	$.L.str.16, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_111
.LBB2_49:                               # %.thread2028
                                        #   in Loop: Header=BB2_39 Depth=2
	addq	$32, %rbx
	movl	$5, %edi
	movl	$29, %esi
	movl	$.L.str.23, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	jmp	.LBB2_38
	.p2align	4, 0x90
.LBB2_50:                               #   in Loop: Header=BB2_39 Depth=2
	movq	%rbx, %rbp
	addq	$32, %rbp
	movq	80(%rbx), %rdi
	cmpq	$0, 96(%rdi)
	je	.LBB2_52
# BB#51:                                #   in Loop: Header=BB2_39 Depth=2
	callq	SymName
	movq	%rax, %rbx
	subq	$8, %rsp
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	movl	$5, %edi
	movl	$48, %esi
	movl	$.L.str.21, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	%rbx, %r9
	pushq	$.L.str.13
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi31:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_39
.LBB2_52:                               #   in Loop: Header=BB2_39 Depth=2
	cmpb	$-113, 32(%rdi)
	je	.LBB2_54
# BB#53:                                #   in Loop: Header=BB2_39 Depth=2
	movl	$5, %edi
	movl	$28, %esi
	movl	$.L.str.22, %edx
	movl	$2, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	jmp	.LBB2_38
.LBB2_54:                               #   in Loop: Header=BB2_39 Depth=2
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	PushScope
	movq	(%rsp), %rax
	movq	80(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_56
# BB#55:                                #   in Loop: Header=BB2_39 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_57
.LBB2_56:                               #   in Loop: Header=BB2_39 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_57:                               #   in Loop: Header=BB2_39 Depth=2
	testq	%r15, %r15
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB2_60
# BB#58:                                #   in Loop: Header=BB2_39 Depth=2
	testq	%rax, %rax
	je	.LBB2_60
# BB#59:                                #   in Loop: Header=BB2_39 Depth=2
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_60:                               #   in Loop: Header=BB2_39 Depth=2
	movq	%rax, zz_res(%rip)
	movq	(%rsp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_39
# BB#61:                                #   in Loop: Header=BB2_39 Depth=2
	testq	%rcx, %rcx
	je	.LBB2_39
# BB#62:                                #   in Loop: Header=BB2_39 Depth=2
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	jmp	.LBB2_39
.LBB2_63:                               #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB2_64:                               #   in Loop: Header=BB2_7 Depth=1
	movb	$17, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	testq	%rbp, %rbp
	jne	.LBB2_66
	jmp	.LBB2_75
	.p2align	4, 0x90
.LBB2_65:                               #   in Loop: Header=BB2_66 Depth=2
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB2_66:                               # %.split.i
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	LexGetToken
	movq	%rax, %rbx
	movq	%rbx, 40(%rsp)
	movzbl	32(%rbx), %eax
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jae	.LBB2_81
# BB#67:                                # %.critedge.i
                                        #   in Loop: Header=BB2_66 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_69
# BB#68:                                #   in Loop: Header=BB2_66 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_70
	.p2align	4, 0x90
.LBB2_69:                               #   in Loop: Header=BB2_66 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_70:                               #   in Loop: Header=BB2_66 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_72
# BB#71:                                #   in Loop: Header=BB2_66 Depth=2
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_72:                               #   in Loop: Header=BB2_66 Depth=2
	movq	%rax, zz_res(%rip)
	movq	40(%rsp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_66
# BB#73:                                #   in Loop: Header=BB2_66 Depth=2
	testq	%rcx, %rcx
	jne	.LBB2_65
	jmp	.LBB2_66
	.p2align	4, 0x90
.LBB2_74:                               #   in Loop: Header=BB2_75 Depth=2
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	%rax, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB2_75:                               # %.split.us.i
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	LexGetToken
	movq	%rax, %rbx
	movq	%rbx, 40(%rsp)
	movzbl	32(%rbx), %eax
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jae	.LBB2_81
# BB#76:                                # %.critedge.us.i
                                        #   in Loop: Header=BB2_75 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_78
# BB#77:                                #   in Loop: Header=BB2_75 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_79
	.p2align	4, 0x90
.LBB2_78:                               #   in Loop: Header=BB2_75 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_79:                               #   in Loop: Header=BB2_75 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	40(%rsp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_75
# BB#80:                                #   in Loop: Header=BB2_75 Depth=2
	testq	%rcx, %rcx
	jne	.LBB2_74
	jmp	.LBB2_75
.LBB2_81:                               # %.split.us.i
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpb	$102, %al
	jne	.LBB2_83
# BB#82:                                # %.us-lcssa21.us.i
                                        #   in Loop: Header=BB2_7 Depth=1
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	40(%rsp), %rdi
	movq	%r14, %rsi
	callq	Parse
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	LanguageDefine
	jmp	.LBB2_1
.LBB2_83:                               # %.us-lcssa.us.i
                                        #   in Loop: Header=BB2_7 Depth=1
	leaq	32(%rbx), %r8
	movl	$5, %edi
	movl	$4, %esi
	movl	$.L.str.48, %edx
	movl	$2, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	jmp	.LBB2_1
.LBB2_84:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rbp, zz_hold(%rip)
	movzbl	32(%rbp), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbp), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	leaq	zz_free(,%rax,8), %rcx
	movq	zz_free(,%rax,8), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	xorl	%r13d, %r13d
	jmp	.LBB2_96
	.p2align	4, 0x90
.LBB2_85:                               #   in Loop: Header=BB2_96 Depth=2
	cmpb	$11, %al
	jne	.LBB2_110
# BB#86:                                #   in Loop: Header=BB2_96 Depth=2
	leaq	64(%rbx), %rbp
	movl	$.L.str.18, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_110
# BB#87:                                #   in Loop: Header=BB2_96 Depth=2
	movb	32(%rbx), %al
	cmpb	$11, %al
	jne	.LBB2_95
# BB#88:                                #   in Loop: Header=BB2_96 Depth=2
	cmpb	$100, (%rbp)
	jne	.LBB2_93
# BB#89:                                #   in Loop: Header=BB2_96 Depth=2
	cmpb	$101, 65(%rbx)
	jne	.LBB2_92
# BB#90:                                #   in Loop: Header=BB2_96 Depth=2
	cmpb	$102, 66(%rbx)
	jne	.LBB2_92
# BB#91:                                #   in Loop: Header=BB2_96 Depth=2
	cmpb	$0, 67(%rbx)
	je	.LBB2_110
.LBB2_92:                               # %.thread2021
                                        #   in Loop: Header=BB2_96 Depth=2
	cmpb	$11, %al
	jne	.LBB2_95
.LBB2_93:                               # %.thread2021.thread
                                        #   in Loop: Header=BB2_96 Depth=2
	movl	$.L.str.16, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_110
# BB#94:                                #   in Loop: Header=BB2_96 Depth=2
	movl	$.L.str.12, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_110
.LBB2_95:                               # %.thread2024
                                        #   in Loop: Header=BB2_96 Depth=2
	addq	$32, %rbx
	movl	$5, %edi
	movl	$27, %esi
	movl	$.L.str.20, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
	movq	(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %edx
	movl	%edx, zz_size(%rip)
	leaq	zz_free(,%rdx,8), %rcx
	movq	zz_free(,%rdx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
.LBB2_96:                               # %.sink.split1831
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_99 Depth 3
                                        #         Child Loop BB2_100 Depth 4
	movl	$1, %ebp
	jmp	.LBB2_99
	.p2align	4, 0x90
.LBB2_97:                               #   in Loop: Header=BB2_99 Depth=3
	movq	16(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rax), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rcx, 16(%rax)
	addq	$24, %rcx
	jmp	.LBB2_99
	.p2align	4, 0x90
.LBB2_98:                               #   in Loop: Header=BB2_99 Depth=3
	addq	$32, %rbx
	movl	$5, %edi
	movl	$26, %esi
	movl	$.L.str.19, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movq	(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %edx
	movl	%edx, zz_size(%rip)
	leaq	zz_free(,%rdx,8), %rcx
	movq	zz_free(,%rdx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
.LBB2_99:                               # %.sink.split1831
                                        #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_96 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_100 Depth 4
	movq	%rax, (%rcx)
	.p2align	4, 0x90
.LBB2_100:                              #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_96 Depth=2
                                        #       Parent Loop BB2_99 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	callq	LexGetToken
	movq	%rax, %rbx
	movq	%rbx, (%rsp)
	movzbl	32(%rbx), %eax
	cmpb	$2, %al
	jne	.LBB2_85
# BB#101:                               #   in Loop: Header=BB2_100 Depth=4
	movq	80(%rbx), %rdi
	cmpb	$-113, 32(%rdi)
	jne	.LBB2_98
# BB#102:                               #   in Loop: Header=BB2_100 Depth=4
	xorl	%esi, %esi
	movl	$1, %edx
	callq	PushScope
	movq	(%rsp), %rax
	cmpq	%r14, 80(%rax)
	cmovel	%ebp, %r13d
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_104
# BB#103:                               #   in Loop: Header=BB2_100 Depth=4
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_105
	.p2align	4, 0x90
.LBB2_104:                              #   in Loop: Header=BB2_100 Depth=4
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_105:                              #   in Loop: Header=BB2_100 Depth=4
	testq	%r15, %r15
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB2_108
# BB#106:                               #   in Loop: Header=BB2_100 Depth=4
	testq	%rax, %rax
	je	.LBB2_108
# BB#107:                               #   in Loop: Header=BB2_100 Depth=4
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_108:                              #   in Loop: Header=BB2_100 Depth=4
	movq	%rax, zz_res(%rip)
	movq	(%rsp), %rdx
	movq	%rdx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_100
# BB#109:                               #   in Loop: Header=BB2_100 Depth=4
	testq	%rdx, %rdx
	je	.LBB2_100
	jmp	.LBB2_97
	.p2align	4, 0x90
.LBB2_110:                              # %.critedge90.loopexit
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	%r14, 8(%rsp)           # 8-byte Spill
.LBB2_111:                              # %.critedge90
                                        #   in Loop: Header=BB2_7 Depth=1
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB2_113
# BB#112:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_114
.LBB2_113:                              #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB2_114:                              #   in Loop: Header=BB2_7 Depth=1
	movb	$17, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	(%rsp), %rbx
	cmpb	$11, 32(%rbx)
	jne	.LBB2_139
# BB#115:                               #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%rbx), %rdi
	movl	$.L.str.18, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_139
# BB#116:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	SuppressScope
	callq	LexGetToken
	movq	%rax, %rbx
	movq	%rbx, (%rsp)
	movb	32(%rbx), %al
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB2_138
	.p2align	4, 0x90
.LBB2_117:                              #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$11, 32(%rbx)
	jne	.LBB2_122
# BB#118:                               #   in Loop: Header=BB2_117 Depth=2
	cmpb	$100, 64(%rbx)
	jne	.LBB2_122
# BB#119:                               #   in Loop: Header=BB2_117 Depth=2
	cmpb	$101, 65(%rbx)
	jne	.LBB2_122
# BB#120:                               #   in Loop: Header=BB2_117 Depth=2
	cmpb	$102, 66(%rbx)
	jne	.LBB2_122
# BB#121:                               #   in Loop: Header=BB2_117 Depth=2
	cmpb	$0, 67(%rbx)
	je	.LBB2_138
	.p2align	4, 0x90
.LBB2_122:                              # %.thread2030
                                        #   in Loop: Header=BB2_117 Depth=2
	cmpb	$11, 32(%rbx)
	jne	.LBB2_124
# BB#123:                               #   in Loop: Header=BB2_117 Depth=2
	leaq	64(%rbx), %rdi
	movl	$.L.str.13, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_138
.LBB2_124:                              #   in Loop: Header=BB2_117 Depth=2
	cmpb	$11, 32(%rbx)
	jne	.LBB2_126
# BB#125:                               #   in Loop: Header=BB2_117 Depth=2
	leaq	64(%rbx), %rdi
	movl	$.L.str.16, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_138
.LBB2_126:                              #   in Loop: Header=BB2_117 Depth=2
	cmpb	$11, 32(%rbx)
	jne	.LBB2_128
# BB#127:                               #   in Loop: Header=BB2_117 Depth=2
	addq	$64, %rbx
	movl	$.L.str.17, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_138
.LBB2_128:                              # %.critedge1791
                                        #   in Loop: Header=BB2_117 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_130
# BB#129:                               #   in Loop: Header=BB2_117 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_131
	.p2align	4, 0x90
.LBB2_130:                              #   in Loop: Header=BB2_117 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_131:                              #   in Loop: Header=BB2_117 Depth=2
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB2_134
# BB#132:                               #   in Loop: Header=BB2_117 Depth=2
	testq	%rax, %rax
	je	.LBB2_134
# BB#133:                               #   in Loop: Header=BB2_117 Depth=2
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_134:                              #   in Loop: Header=BB2_117 Depth=2
	movq	%rax, zz_res(%rip)
	movq	(%rsp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_137
# BB#135:                               #   in Loop: Header=BB2_117 Depth=2
	testq	%rcx, %rcx
	je	.LBB2_137
# BB#136:                               #   in Loop: Header=BB2_117 Depth=2
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB2_137:                              # %.backedge
                                        #   in Loop: Header=BB2_117 Depth=2
	callq	LexGetToken
	movq	%rax, %rbx
	movq	%rbx, (%rsp)
	movzbl	32(%rbx), %eax
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB2_117
.LBB2_138:                              # %.critedge158
                                        #   in Loop: Header=BB2_7 Depth=1
	callq	UnSuppressScope
	.p2align	4, 0x90
.LBB2_139:                              #   in Loop: Header=BB2_7 Depth=1
	cmpb	$-113, %r12b
	jne	.LBB2_147
# BB#140:                               #   in Loop: Header=BB2_7 Depth=1
	movq	(%rsp), %rbx
	movb	32(%rbx), %al
	cmpb	$11, %al
	jne	.LBB2_279
# BB#141:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$100, 64(%rbx)
	jne	.LBB2_146
# BB#142:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$101, 65(%rbx)
	jne	.LBB2_145
# BB#143:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$102, 66(%rbx)
	jne	.LBB2_145
# BB#144:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$0, 67(%rbx)
	je	.LBB2_147
.LBB2_145:                              # %.thread2032
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, %al
	jne	.LBB2_279
.LBB2_146:                              # %.thread2032.thread
                                        #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%rbx), %rdi
	movl	$.L.str.16, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_279
.LBB2_147:                              #   in Loop: Header=BB2_7 Depth=1
	movq	(%rsp), %rbx
	movb	32(%rbx), %al
	cmpb	$-111, %r12b
	jne	.LBB2_150
# BB#148:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, %al
	jne	.LBB2_280
# BB#149:                               #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%rbx), %rdi
	movl	$.L.str.12, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_151
	jmp	.LBB2_280
.LBB2_150:                              #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, %al
	jne	.LBB2_152
.LBB2_151:                              # %.thread2034
                                        #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%rbx), %rdi
	movl	$.L.str.16, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_179
.LBB2_152:                              #   in Loop: Header=BB2_7 Depth=1
	callq	SuppressScope
	movq	(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	callq	LexGetToken
	movq	%rax, %rbx
	movq	%rbx, (%rsp)
	xorl	%r14d, %r14d
	cmpb	$-111, %r12b
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	jne	.LBB2_156
# BB#153:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%rbx)
	jne	.LBB2_156
# BB#154:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, %rdi
	addq	$64, %rdi
	movl	$.L.str.27, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_156
# BB#155:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, zz_hold(%rip)
	movzbl	33(%rbx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	LexGetToken
	movq	%rax, %rbx
	movq	%rbx, (%rsp)
	movl	$1, %r14d
.LBB2_156:                              #   in Loop: Header=BB2_7 Depth=1
	leaq	32(%rbx), %rbp
	movb	32(%rbx), %al
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB2_281
# BB#157:                               #   in Loop: Header=BB2_7 Depth=1
	addq	$64, %rbx
	subq	$8, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	movzbl	%r12b, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	pushq	$0
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	movq	24(%rsp), %rbx          # 8-byte Reload
	pushq	%rbx
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi36:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r12
	cmpq	32(%rsp), %rbx          # 8-byte Folded Reload
	je	.LBB2_159
# BB#158:                               #   in Loop: Header=BB2_7 Depth=1
	orb	$1, 43(%r12)
.LBB2_159:                              #   in Loop: Header=BB2_7 Depth=1
	testl	%r13d, %r13d
	je	.LBB2_161
# BB#160:                               #   in Loop: Header=BB2_7 Depth=1
	orb	$64, 43(%r12)
.LBB2_161:                              #   in Loop: Header=BB2_7 Depth=1
	testl	%r14d, %r14d
	movq	32(%rsp), %rax          # 8-byte Reload
	je	.LBB2_164
# BB#162:                               #   in Loop: Header=BB2_7 Depth=1
	incw	122(%rax)
	orb	$64, 126(%r12)
	jmp	.LBB2_164
	.p2align	4, 0x90
.LBB2_163:                              # %.thread1903
                                        #   in Loop: Header=BB2_164 Depth=2
	addq	$32, %r13
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	InsertAlternativeName
.LBB2_164:                              # %.preheader
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	callq	LexGetToken
	movq	%rax, %r13
	movq	%r13, (%rsp)
	movzbl	32(%r13), %eax
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$1, %cl
	ja	.LBB2_191
# BB#165:                               #   in Loop: Header=BB2_164 Depth=2
	leaq	64(%r13), %rbx
	cmpb	$11, %al
	jne	.LBB2_163
# BB#166:                               #   in Loop: Header=BB2_164 Depth=2
	movl	$.L.str.12, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_192
# BB#167:                               #   in Loop: Header=BB2_164 Depth=2
	leaq	64(%r13), %rbx
	movl	$.L.str.13, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_192
# BB#168:                               #   in Loop: Header=BB2_164 Depth=2
	movl	$.L.str.29, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_192
# BB#169:                               #   in Loop: Header=BB2_164 Depth=2
	movl	$.L.str.30, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_192
# BB#170:                               #   in Loop: Header=BB2_164 Depth=2
	movl	$.L.str.31, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_192
# BB#171:                               #   in Loop: Header=BB2_164 Depth=2
	movl	$.L.str.32, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_192
# BB#172:                               #   in Loop: Header=BB2_164 Depth=2
	movl	$.L.str.33, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_192
# BB#173:                               #   in Loop: Header=BB2_164 Depth=2
	movl	$.L.str.34, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_192
# BB#174:                               #   in Loop: Header=BB2_164 Depth=2
	movl	$.L.str.35, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_192
# BB#175:                               #   in Loop: Header=BB2_164 Depth=2
	movl	$.L.str.36, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_192
# BB#176:                               #   in Loop: Header=BB2_164 Depth=2
	cmpb	$123, 64(%r13)
	jne	.LBB2_178
# BB#177:                               #   in Loop: Header=BB2_164 Depth=2
	cmpb	$0, 65(%r13)
	je	.LBB2_192
.LBB2_178:                              # %.thread2045
                                        #   in Loop: Header=BB2_164 Depth=2
	movl	$.L.str.37, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_163
	jmp	.LBB2_192
.LBB2_179:                              #   in Loop: Header=BB2_7 Depth=1
	cmpq	%rbp, 8(%rbp)
	je	.LBB2_181
# BB#180:                               #   in Loop: Header=BB2_7 Depth=1
	addq	$32, %rbx
	movl	$5, %edi
	movl	$32, %esi
	movl	$.L.str.26, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
.LBB2_181:                              #   in Loop: Header=BB2_7 Depth=1
	callq	SuppressScope
	movq	(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	callq	LexGetToken
	movq	%rax, %rbx
	leaq	32(%rbx), %rbp
	movb	32(%rbx), %al
	addb	$-11, %al
	cmpb	$2, %al
	movq	32(%rsp), %r14          # 8-byte Reload
	jae	.LBB2_190
# BB#182:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, %rdi
	addq	$64, %rdi
	subq	$8, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	movl	$142, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$1, %r9d
	movq	%rbp, %rdx
	pushq	$0
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	movq	24(%rsp), %r13          # 8-byte Reload
	pushq	%r13
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi41:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r12
	cmpq	%r14, %r13
	je	.LBB2_184
# BB#183:                               #   in Loop: Header=BB2_7 Depth=1
	orb	$1, 43(%r12)
.LBB2_184:                              #   in Loop: Header=BB2_7 Depth=1
	callq	UnSuppressScope
	jmp	.LBB2_186
	.p2align	4, 0x90
.LBB2_185:                              #   in Loop: Header=BB2_186 Depth=2
	movq	%rbx, %rdi
	addq	$64, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	InsertAlternativeName
.LBB2_186:                              #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, zz_hold(%rip)
	movzbl	(%rbp), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	LexGetToken
	movq	%rax, %rbx
	leaq	32(%rbx), %rbp
	movzbl	32(%rbx), %eax
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$1, %cl
	jbe	.LBB2_185
# BB#187:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$102, %al
	jne	.LBB2_216
# BB#188:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	ReadTokenList
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	movq	%rbx, %rdx
	addq	$33, %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	56(%r12), %rax
	movq	16(%rax), %rax
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_229
# BB#189:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB2_230
.LBB2_190:                              #   in Loop: Header=BB2_7 Depth=1
	movl	$5, %edi
	movl	$24, %esi
	movl	$.L.str.49, %edx
	movl	$2, %ecx
	movl	$.L.str.16, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
	callq	UnSuppressScope
	jmp	.LBB2_217
.LBB2_191:                              # %.loopexit
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, %al
	jne	.LBB2_198
.LBB2_192:                              # %.loopexit.thread
                                        #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%r13), %rdi
	movl	$.L.str.29, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_198
# BB#193:                               #   in Loop: Header=BB2_7 Depth=1
	orb	$-128, 41(%r12)
	movq	%r13, zz_hold(%rip)
	movzbl	32(%r13), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	movq	%r13, %rdx
	addq	$33, %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r13)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	LexGetToken
	movq	%rax, %r13
	movq	%r13, (%rsp)
	cmpb	$11, 32(%r13)
	jne	.LBB2_195
# BB#194:                               #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%r13), %rdi
	movl	$.L.str.30, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_198
.LBB2_195:                              #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%r13)
	jne	.LBB2_197
# BB#196:                               #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%r13), %rdi
	movl	$.L.str.31, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_198
.LBB2_197:                              #   in Loop: Header=BB2_7 Depth=1
	addq	$32, %r13
	movl	$5, %edi
	movl	$34, %esi
	movl	$.L.str.38, %edx
	movl	$2, %ecx
	movl	$.L.str.30, %r9d
	xorl	%eax, %eax
	movq	%r13, %r8
	callq	Error
	movq	(%rsp), %r13
.LBB2_198:                              #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%r13)
	jne	.LBB2_201
# BB#199:                               #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%r13), %rdi
	movl	$.L.str.31, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_201
# BB#200:                               #   in Loop: Header=BB2_7 Depth=1
	andb	$-33, 43(%r12)
	movq	%r13, zz_hold(%rip)
	movzbl	32(%r13), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r13), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r13)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	LexGetToken
	movq	%rax, %r13
	movq	%r13, (%rsp)
.LBB2_201:                              #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%r13)
	jne	.LBB2_204
# BB#202:                               #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%r13), %rdi
	movl	$.L.str.30, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_213
# BB#203:                               #   in Loop: Header=BB2_7 Depth=1
	xorl	%r14d, %r14d
	cmpb	$11, 32(%r13)
	je	.LBB2_206
	jmp	.LBB2_222
.LBB2_204:                              #   in Loop: Header=BB2_7 Depth=1
	xorl	%r14d, %r14d
.LBB2_205:                              #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%r13)
	jne	.LBB2_222
.LBB2_206:                              #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%r13), %rdi
	movl	$.L.str.32, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_222
# BB#207:                               #   in Loop: Header=BB2_7 Depth=1
	movb	32(%r13), %al
	xorl	%ebx, %ebx
	jmp	.LBB2_209
	.p2align	4, 0x90
.LBB2_208:                              #   in Loop: Header=BB2_209 Depth=2
	leal	(%rbx,%rbx,4), %ecx
	leal	-48(%rax,%rcx,2), %ebx
	movb	$11, %al
.LBB2_209:                              #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, zz_hold(%rip)
	movzbl	%al, %ecx
	addb	$-11, %al
	leaq	33(%r13), %rdx
	cmpb	$2, %al
	leaq	zz_lengths(%rcx), %rax
	cmovbq	%rdx, %rax
	movzbl	(%rax), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r13)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	LexGetToken
	movq	%rax, %r13
	movq	%r13, (%rsp)
	cmpb	$11, 32(%r13)
	jne	.LBB2_211
# BB#210:                               #   in Loop: Header=BB2_209 Depth=2
	movzbl	64(%r13), %eax
	movl	%eax, %ecx
	addb	$-48, %cl
	cmpb	$9, %cl
	jbe	.LBB2_208
.LBB2_211:                              # %.critedge1825
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpl	$9, %ebx
	jg	.LBB2_218
# BB#212:                               #   in Loop: Header=BB2_7 Depth=1
	addq	$32, %r13
	movl	$10, %ebx
	movl	$5, %edi
	movl	$37, %esi
	movl	$.L.str.39, %edx
	movl	$2, %ecx
	movl	$10, %r9d
	jmp	.LBB2_220
.LBB2_213:                              #   in Loop: Header=BB2_7 Depth=1
	callq	UnSuppressScope
	movq	(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	callq	LexGetToken
	movq	%rax, %rbp
	movq	%rbp, (%rsp)
	cmpb	$102, 32(%rbp)
	jne	.LBB2_283
# BB#214:                               #   in Loop: Header=BB2_7 Depth=1
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rsp, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	Parse
	movq	%rax, %r14
	callq	SuppressScope
	movq	(%rsp), %r13
	testq	%r13, %r13
	jne	.LBB2_205
# BB#215:                               #   in Loop: Header=BB2_7 Depth=1
	callq	LexGetToken
	movq	%rax, %r13
	movq	%r13, (%rsp)
	cmpb	$11, 32(%r13)
	je	.LBB2_206
	jmp	.LBB2_222
.LBB2_216:                              #   in Loop: Header=BB2_7 Depth=1
	subq	$8, %rsp
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	movl	$5, %edi
	movl	$25, %esi
	movl	$.L.str.50, %edx
	movl	$2, %ecx
	movl	$.L.str.16, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	pushq	$.L.str.7
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi44:
	.cfi_adjust_cfa_offset -16
.LBB2_217:                              #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, (%rsp)
	xorl	%r12d, %r12d
	jmp	.LBB2_272
.LBB2_218:                              #   in Loop: Header=BB2_7 Depth=1
	cmpl	$101, %ebx
	jl	.LBB2_221
# BB#219:                               #   in Loop: Header=BB2_7 Depth=1
	addq	$32, %r13
	movl	$100, %ebx
	movl	$5, %edi
	movl	$38, %esi
	movl	$.L.str.40, %edx
	movl	$2, %ecx
	movl	$100, %r9d
.LBB2_220:                              #   in Loop: Header=BB2_7 Depth=1
	xorl	%eax, %eax
	movq	%r13, %r8
	callq	Error
.LBB2_221:                              #   in Loop: Header=BB2_7 Depth=1
	movb	%bl, 40(%r12)
	movq	(%rsp), %r13
.LBB2_222:                              #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%r13)
	jne	.LBB2_233
# BB#223:                               #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%r13), %rdi
	movl	$.L.str.33, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_233
# BB#224:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%r13, zz_hold(%rip)
	movzbl	32(%r13), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r13), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r13)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	LexGetToken
	movq	%rax, %rbx
	movq	%rbx, (%rsp)
	cmpb	$11, 32(%rbx)
	jne	.LBB2_226
# BB#225:                               #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%rbx), %rdi
	movl	$.L.str.34, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_231
.LBB2_226:                              #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%rbx)
	jne	.LBB2_228
# BB#227:                               #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%rbx), %rdi
	movl	$.L.str.35, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_232
.LBB2_228:                              #   in Loop: Header=BB2_7 Depth=1
	addq	$32, %rbx
	movl	$5, %edi
	movl	$39, %esi
	movl	$.L.str.41, %edx
	movl	$2, %ecx
	movl	$.L.str.35, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movq	(%rsp), %rbx
	jmp	.LBB2_232
.LBB2_229:                              #   in Loop: Header=BB2_7 Depth=1
	xorl	%ecx, %ecx
.LBB2_230:                              #   in Loop: Header=BB2_7 Depth=1
	movq	%rcx, 56(%r12)
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	andb	$-5, 42(%r12)
	xorl	%eax, %eax
	movq	%rax, (%rsp)
	jmp	.LBB2_272
.LBB2_231:                              #   in Loop: Header=BB2_7 Depth=1
	andb	$-17, 41(%r12)
.LBB2_232:                              #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	LexGetToken
	movq	%rax, %r13
	movq	%r13, (%rsp)
.LBB2_233:                              #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%r13)
	jne	.LBB2_237
# BB#234:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%r13, %rdi
	addq	$64, %rdi
	movl	$.L.str.34, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_237
# BB#235:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%r13, zz_hold(%rip)
	movzbl	32(%r13), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r13), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r13)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	LexGetToken
	movq	%rax, (%rsp)
	leaq	32(%rax), %rbp
	cmpb	$11, 32(%rax)
	jne	.LBB2_284
# BB#236:                               #   in Loop: Header=BB2_7 Depth=1
	addq	$64, %rax
	subq	$8, %rsp
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rax, %rdi
	movq	%rbp, %rdx
	pushq	$0
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi49:
	.cfi_adjust_cfa_offset -32
	movq	(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	callq	LexGetToken
	movq	%rax, (%rsp)
.LBB2_237:                              #   in Loop: Header=BB2_7 Depth=1
	callq	UnSuppressScope
	movl	$145, %edx
	movq	%rsp, %r13
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	ReadDefinitions
	movq	(%rsp), %rbx
	cmpb	$11, 32(%rbx)
	jne	.LBB2_242
# BB#238:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, %rbp
	addq	$64, %rbp
	movl	$.L.str.35, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_240
# BB#239:                               #   in Loop: Header=BB2_7 Depth=1
	movl	$.L.str.36, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_242
.LBB2_240:                              #   in Loop: Header=BB2_7 Depth=1
	movl	$.L.str.36, %esi
	movq	%rbp, %rdi
	callq	strcmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	movzwl	41(%r12), %eax
	movzbl	43(%r12), %edx
	shll	$16, %edx
	orl	%eax, %edx
	shll	$8, %ecx
	andl	$16776959, %edx         # imm = 0xFFFEFF
	orl	%edx, %ecx
	shrl	$16, %edx
	movb	%dl, 43(%r12)
	movw	%cx, 41(%r12)
	callq	SuppressScope
	movq	(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	callq	LexGetToken
	movq	%rax, (%rsp)
	leaq	32(%rax), %rbp
	cmpb	$11, 32(%rax)
	jne	.LBB2_282
# BB#241:                               #   in Loop: Header=BB2_7 Depth=1
	addq	$64, %rax
	subq	$8, %rsp
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rax, %rdi
	movq	%rbp, %rdx
	pushq	$0
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi54:
	.cfi_adjust_cfa_offset -32
	callq	UnSuppressScope
	movq	(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	callq	LexGetToken
	movq	%rax, %rbx
	movq	%rbx, (%rsp)
.LBB2_242:                              #   in Loop: Header=BB2_7 Depth=1
	testq	%r14, %r14
	je	.LBB2_244
# BB#243:                               #   in Loop: Header=BB2_7 Depth=1
	leaq	32(%r14), %rdx
	subq	$8, %rsp
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.43, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	%r14
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi59:
	.cfi_adjust_cfa_offset -32
	movq	(%rsp), %rbx
.LBB2_244:                              #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%rbx)
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB2_247
# BB#245:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$123, 64(%rbx)
	jne	.LBB2_247
# BB#246:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$0, 65(%rbx)
	je	.LBB2_253
.LBB2_247:                              # %.thread
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpb	$11, 32(%rbx)
	jne	.LBB2_249
# BB#248:                               #   in Loop: Header=BB2_7 Depth=1
	leaq	64(%rbx), %rdi
	movl	$.L.str.37, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_252
.LBB2_249:                              #   in Loop: Header=BB2_7 Depth=1
	movb	32(%rbx), %al
	cmpb	$102, %al
	je	.LBB2_255
# BB#250:                               #   in Loop: Header=BB2_7 Depth=1
	cmpb	$104, %al
	je	.LBB2_255
# BB#251:                               #   in Loop: Header=BB2_7 Depth=1
	addq	$32, %rbx
	movq	%r12, %rdi
	callq	SymName
	movq	%rax, %rbp
	movl	$5, %edi
	movl	$42, %esi
	movl	$.L.str.44, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	movq	16(%rsp), %rbp          # 8-byte Reload
	callq	Error
	movq	(%rsp), %rbx
	cmpb	$104, 32(%rbx)
	je	.LBB2_256
	jmp	.LBB2_257
.LBB2_252:                              #   in Loop: Header=BB2_7 Depth=1
	addq	$32, %rbx
	movq	StartSym(%rip), %r9
	movl	$104, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$1, %r8d
	jmp	.LBB2_254
.LBB2_253:                              #   in Loop: Header=BB2_7 Depth=1
	addq	$32, %rbx
	movq	StartSym(%rip), %r9
	movl	$102, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$3, %r8d
.LBB2_254:                              #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, %rsi
	callq	NewToken
	movq	%rax, %rbx
	movq	(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	%rbx, (%rsp)
.LBB2_255:                              #   in Loop: Header=BB2_7 Depth=1
	cmpb	$104, 32(%rbx)
	jne	.LBB2_257
.LBB2_256:                              #   in Loop: Header=BB2_7 Depth=1
	movq	%r12, 80(%rbx)
.LBB2_257:                              #   in Loop: Header=BB2_7 Depth=1
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	PushScope
	callq	BodyParAllowed
	movl	$1, %edx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	Parse
	movq	%rax, 56(%r12)
	movq	8(%rbp), %r14
	cmpq	%rbp, %r14
	jne	.LBB2_259
	jmp	.LBB2_271
	.p2align	4, 0x90
.LBB2_264:                              #   in Loop: Header=BB2_259 Depth=2
	movzwl	41(%r12), %ecx
	testb	$1, %ch
	je	.LBB2_267
# BB#265:                               #   in Loop: Header=BB2_259 Depth=2
	cmpb	$-110, 32(%rax)
	jne	.LBB2_267
# BB#266:                               #   in Loop: Header=BB2_259 Depth=2
	movl	$5, %edi
	movl	$44, %esi
	movl	$.L.str.46, %edx
	jmp	.LBB2_270
.LBB2_267:                              #   in Loop: Header=BB2_259 Depth=2
	movzwl	41(%rax), %edx
	movzbl	43(%rax), %ecx
	shll	$16, %ecx
	orl	%edx, %ecx
	testl	$65536, %ecx            # imm = 0x10000
	jne	.LBB2_269
# BB#268:                               #   in Loop: Header=BB2_259 Depth=2
	movl	%ecx, %edx
	orl	$65536, %edx            # imm = 0x10000
	movw	%cx, 41(%rax)
	shrl	$16, %edx
	movb	%dl, 43(%rax)
	jmp	.LBB2_258
.LBB2_269:                              #   in Loop: Header=BB2_259 Depth=2
	movl	$5, %edi
	movl	$45, %esi
	movl	$.L.str.47, %edx
.LBB2_270:                              #   in Loop: Header=BB2_259 Depth=2
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	movq	%rbx, %r9
	callq	Error
	jmp	.LBB2_258
	.p2align	4, 0x90
.LBB2_259:                              #   Parent Loop BB2_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_260 Depth 3
	leaq	16(%r14), %rax
	.p2align	4, 0x90
.LBB2_260:                              #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_259 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rbx
	leaq	16(%rbx), %rax
	cmpb	$0, 32(%rbx)
	je	.LBB2_260
# BB#261:                               #   in Loop: Header=BB2_259 Depth=2
	leaq	32(%rbx), %r13
	addq	$64, %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	SearchSym
	testq	%rax, %rax
	je	.LBB2_263
# BB#262:                               #   in Loop: Header=BB2_259 Depth=2
	cmpq	%r12, 48(%rax)
	je	.LBB2_264
.LBB2_263:                              #   in Loop: Header=BB2_259 Depth=2
	movq	%r12, %rdi
	callq	SymName
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	movl	$5, %edi
	movl	$43, %esi
	movl	$.L.str.45, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	movq	%rbx, %r9
	pushq	%rbp
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi62:
	.cfi_adjust_cfa_offset -16
.LBB2_258:                              #   in Loop: Header=BB2_259 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	8(%r14), %r14
	cmpq	%rbp, %r14
	jne	.LBB2_259
.LBB2_271:                              # %._crit_edge
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	%rbp, %rdi
	callq	DisposeObject
	callq	PopScope
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB2_272:                              #   in Loop: Header=BB2_7 Depth=1
	movq	8(%r15), %rbx
	cmpq	%r15, %rbx
	je	.LBB2_277
	.p2align	4, 0x90
.LBB2_273:                              # %.lr.ph1962
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	PopScope
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB2_273
# BB#274:                               # %._crit_edge1963
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpq	%r14, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB2_277
# BB#275:                               # %._crit_edge1963
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpq	%r15, 8(%r15)
	je	.LBB2_277
# BB#276:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%r15, 96(%r12)
	jmp	.LBB2_278
.LBB2_277:                              # %._crit_edge1963.thread
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	%r15, %rdi
	callq	DisposeObject
.LBB2_278:                              #   in Loop: Header=BB2_7 Depth=1
	movl	28(%rsp), %r12d         # 4-byte Reload
	callq	BodyParAllowed
	movq	(%rsp), %rbp
	testq	%rbp, %rbp
	jne	.LBB2_2
	jmp	.LBB2_1
.LBB2_279:                              # %.thread2033
	addq	$32, %rbx
	subq	$8, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	movl	$5, %edi
	movl	$30, %esi
	movl	$.L.str.24, %edx
	movl	$2, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	pushq	$.L.str.16
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi65:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_287
.LBB2_280:
	addq	$32, %rbx
	movl	$5, %edi
	movl	$31, %esi
	movl	$.L.str.25, %edx
	movl	$2, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	jmp	.LBB2_287
.LBB2_281:
	movl	$5, %edi
	movl	$33, %esi
	movl	$.L.str.28, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
	jmp	.LBB2_286
.LBB2_282:
	movl	$5, %edi
	movl	$41, %esi
	movl	$.L.str.42, %edx
	movl	$2, %ecx
	movl	$.L.str.35, %r9d
	jmp	.LBB2_285
.LBB2_283:
	addq	$32, %rbp
	movl	$5, %edi
	movl	$36, %esi
	movl	$.L.str.38, %edx
	movl	$2, %ecx
	movl	$.L.str.7, %r9d
	jmp	.LBB2_285
.LBB2_284:
	movl	$5, %edi
	movl	$40, %esi
	movl	$.L.str.42, %edx
	movl	$2, %ecx
	movl	$.L.str.34, %r9d
.LBB2_285:                              # %.critedge13
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
.LBB2_286:                              # %.critedge13
	callq	UnSuppressScope
.LBB2_287:                              # %.critedge13
	movq	(%rsp), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	ReadDefinitions, .Lfunc_end2-ReadDefinitions
	.cfi_endproc

	.p2align	4, 0x90
	.type	ReadTokenList,@function
ReadTokenList:                          # @ReadTokenList
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi72:
	.cfi_def_cfa_offset 96
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	jmp	.LBB3_1
.LBB3_10:                               #   in Loop: Header=BB3_1 Depth=1
	movq	80(%rbx), %rdi
	callq	SymName
	movq	%rax, %rbx
	movl	$5, %edi
	movl	$12, %esi
	movl	$.L.str.52, %edx
	jmp	.LBB3_11
.LBB3_8:                                #   in Loop: Header=BB3_1 Depth=1
	cmpb	$64, 64(%rbx)
	jne	.LBB3_1
# BB#9:                                 #   in Loop: Header=BB3_1 Depth=1
	addq	$64, %rbx
	movl	$5, %edi
	movl	$11, %esi
	movl	$.L.str.51, %edx
	jmp	.LBB3_11
.LBB3_12:                               #   in Loop: Header=BB3_1 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	ReadTokenList
	jmp	.LBB3_1
.LBB3_14:                               #   in Loop: Header=BB3_1 Depth=1
	movq	80(%rbx), %rdi
	callq	SymName
	movq	%rax, %rbx
	movl	$5, %edi
	movl	$14, %esi
	movl	$.L.str.54, %edx
	jmp	.LBB3_11
.LBB3_69:                               #   in Loop: Header=BB3_1 Depth=1
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	PushScope
	movl	$146, %esi
	movq	%r12, %rdi
	callq	ChildSym
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	PushScope
	cmpb	$104, (%r13)
	jne	.LBB3_71
# BB#70:                                #   in Loop: Header=BB3_1 Depth=1
	movq	%r12, 80(%rbx)
.LBB3_71:                               #   in Loop: Header=BB3_1 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	ReadTokenList
	callq	PopScope
	callq	PopScope
	jmp	.LBB3_1
.LBB3_11:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r12, %r8
	movq	%rbx, %r9
	callq	Error
	.p2align	4, 0x90
.LBB3_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_6 Depth 2
                                        #       Child Loop BB3_39 Depth 3
                                        #         Child Loop BB3_54 Depth 4
                                        #           Child Loop BB3_55 Depth 5
                                        #         Child Loop BB3_60 Depth 4
	callq	LexGetToken
	movq	%rax, %rbx
	movq	56(%r15), %rax
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB3_4
# BB#2:                                 #   in Loop: Header=BB3_1 Depth=1
	testq	%rax, %rax
	movq	%rbx, %rcx
	je	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_1 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_4:                                #   in Loop: Header=BB3_1 Depth=1
	movq	%rax, %rcx
.LBB3_5:                                #   in Loop: Header=BB3_1 Depth=1
	movq	%rcx, 56(%r15)
	jmp	.LBB3_6
.LBB3_73:                               #   in Loop: Header=BB3_6 Depth=2
	callq	Image
	movq	%rax, %r9
	movl	$5, %edi
	movl	$23, %esi
	movl	$.L.str.62, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r12, %r8
	callq	Error
	jmp	.LBB3_6
.LBB3_46:                               #   in Loop: Header=BB3_6 Depth=2
	cmpb	$103, %al
	je	.LBB3_47
# BB#49:                                #   in Loop: Header=BB3_6 Depth=2
	movq	80(%rbx), %rdi
	addq	$32, %rbx
	callq	SymName
	movq	%rax, (%rsp)
	movl	$5, %edi
	movl	$21, %esi
	movl	$.L.str.60, %edx
	movl	$2, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movq	%r13, %rbx
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB3_66:                               # %.critedge
                                        #   in Loop: Header=BB3_6 Depth=2
	movzwl	41(%r12), %eax
	testb	$1, %ah
	je	.LBB3_6
# BB#67:                                #   in Loop: Header=BB3_6 Depth=2
	leaq	32(%rbx), %r13
	movb	32(%rbx), %al
	addb	$-102, %al
	cmpb	$3, %al
	ja	.LBB3_72
# BB#68:                                #   in Loop: Header=BB3_6 Depth=2
	movzbl	%al, %eax
	jmpq	*.LJTI3_1(,%rax,8)
.LBB3_72:                               #   in Loop: Header=BB3_6 Depth=2
	movq	%r12, %rdi
	callq	SymName
	movq	%rax, %r9
	movq	$.L.str.7, (%rsp)
	movl	$5, %edi
	movl	$22, %esi
	movl	$.L.str.61, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	callq	Error
	jmp	.LBB3_6
.LBB3_13:                               #   in Loop: Header=BB3_6 Depth=2
	movl	$5, %edi
	movl	$13, %esi
	movl	$.L.str.53, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r12, %r8
	callq	Error
	.p2align	4, 0x90
.LBB3_6:                                # %.backedge
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_39 Depth 3
                                        #         Child Loop BB3_54 Depth 4
                                        #           Child Loop BB3_55 Depth 5
                                        #         Child Loop BB3_60 Depth 4
	leaq	32(%rbx), %r12
	movzbl	32(%rbx), %edi
	movl	%edi, %eax
	addb	$-2, %al
	movzbl	%al, %eax
	cmpb	$115, %al
	ja	.LBB3_73
# BB#7:                                 # %.backedge
                                        #   in Loop: Header=BB3_6 Depth=2
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_33:                               #   in Loop: Header=BB3_6 Depth=2
	movq	80(%rbx), %r12
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	PushScope
	callq	LexGetToken
	movq	%rax, %rbx
	movq	56(%r15), %rax
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB3_36
# BB#34:                                #   in Loop: Header=BB3_6 Depth=2
	testq	%rax, %rax
	movq	%rbx, %rcx
	je	.LBB3_37
# BB#35:                                #   in Loop: Header=BB3_6 Depth=2
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_36:                               #   in Loop: Header=BB3_6 Depth=2
	movq	%rax, %rcx
.LBB3_37:                               #   in Loop: Header=BB3_6 Depth=2
	movq	%rcx, 56(%r15)
	callq	PopScope
	movb	32(%rbx), %al
	movl	%eax, %ecx
	andb	$-2, %cl
	cmpb	$6, %cl
	je	.LBB3_1
# BB#38:                                # %.preheader185
                                        #   in Loop: Header=BB3_6 Depth=2
	cmpb	$2, %al
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	jne	.LBB3_66
	.p2align	4, 0x90
.LBB3_39:                               # %.lr.ph204
                                        #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_54 Depth 4
                                        #           Child Loop BB3_55 Depth 5
                                        #         Child Loop BB3_60 Depth 4
	movq	80(%rbx), %rax
	cmpq	%r12, 48(%rax)
	jne	.LBB3_66
# BB#40:                                #   in Loop: Header=BB3_39 Depth=3
	cmpb	$-111, 32(%rax)
	jne	.LBB3_66
# BB#41:                                #   in Loop: Header=BB3_39 Depth=3
	callq	LexGetToken
	movq	%rax, %r13
	movq	56(%r15), %rax
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB3_44
# BB#42:                                #   in Loop: Header=BB3_39 Depth=3
	testq	%rax, %rax
	movq	%r13, %rcx
	je	.LBB3_45
# BB#43:                                #   in Loop: Header=BB3_39 Depth=3
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_44:                               #   in Loop: Header=BB3_39 Depth=3
	movq	%rax, %rcx
.LBB3_45:                               #   in Loop: Header=BB3_39 Depth=3
	movq	%rcx, 56(%r15)
	movb	32(%r13), %al
	cmpb	$102, %al
	jne	.LBB3_46
# BB#50:                                #   in Loop: Header=BB3_39 Depth=3
	movq	80(%rbx), %rdi
	movq	96(%rdi), %r14
	xorl	%ebp, %ebp
	testq	%r14, %r14
	je	.LBB3_51
# BB#52:                                # %.preheader184
                                        #   in Loop: Header=BB3_39 Depth=3
	movq	8(%r14), %r12
	cmpq	%r14, %r12
	je	.LBB3_58
# BB#53:                                # %.preheader.preheader
                                        #   in Loop: Header=BB3_39 Depth=3
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_54:                               # %.preheader
                                        #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_6 Depth=2
                                        #       Parent Loop BB3_39 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB3_55 Depth 5
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB3_55:                               #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_6 Depth=2
                                        #       Parent Loop BB3_39 Depth=3
                                        #         Parent Loop BB3_54 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB3_55
# BB#56:                                #   in Loop: Header=BB3_54 Depth=4
	movq	80(%rax), %rdi
	xorl	%esi, %esi
	movl	$1, %edx
	callq	PushScope
	incl	%ebp
	movq	8(%r12), %r12
	cmpq	%r14, %r12
	jne	.LBB3_54
# BB#57:                                # %.loopexit.loopexit
                                        #   in Loop: Header=BB3_39 Depth=3
	movq	80(%rbx), %rdi
.LBB3_58:                               # %.loopexit
                                        #   in Loop: Header=BB3_39 Depth=3
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB3_59
	.p2align	4, 0x90
.LBB3_51:                               #   in Loop: Header=BB3_39 Depth=3
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB3_59:                               # %.loopexit
                                        #   in Loop: Header=BB3_39 Depth=3
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	PushScope
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	ReadTokenList
	callq	PopScope
	testl	%ebp, %ebp
	jle	.LBB3_61
	.p2align	4, 0x90
.LBB3_60:                               # %.lr.ph
                                        #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_6 Depth=2
                                        #       Parent Loop BB3_39 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	callq	PopScope
	decl	%ebp
	jne	.LBB3_60
.LBB3_61:                               # %._crit_edge
                                        #   in Loop: Header=BB3_39 Depth=3
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	PushScope
	callq	LexGetToken
	movq	%rax, %rbx
	movq	56(%r15), %rax
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB3_64
# BB#62:                                #   in Loop: Header=BB3_39 Depth=3
	testq	%rax, %rax
	movq	%rbx, %rcx
	je	.LBB3_65
# BB#63:                                #   in Loop: Header=BB3_39 Depth=3
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_64:                               #   in Loop: Header=BB3_39 Depth=3
	movq	%rax, %rcx
.LBB3_65:                               #   in Loop: Header=BB3_39 Depth=3
	movq	%rcx, 56(%r15)
	callq	PopScope
	cmpb	$2, 32(%rbx)
	je	.LBB3_39
	jmp	.LBB3_66
.LBB3_15:
	cmpb	$102, 32(%r14)
	jne	.LBB3_16
.LBB3_32:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_19:
	cmpb	$104, 32(%r14)
	jne	.LBB3_20
# BB#21:
	callq	LexGetToken
	movq	%rax, %rbx
	movq	56(%r15), %rax
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB3_24
# BB#22:
	testq	%rax, %rax
	movq	%rbx, %rcx
	je	.LBB3_25
# BB#23:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_24:
	movq	%rax, %rcx
.LBB3_25:
	movq	%rcx, 56(%r15)
	leaq	32(%rbx), %r15
	movb	32(%rbx), %al
	cmpb	$2, %al
	je	.LBB3_30
# BB#26:
	cmpb	$11, %al
	jne	.LBB3_29
# BB#27:
	cmpb	$64, 64(%rbx)
	jne	.LBB3_29
# BB#28:
	addq	$64, %rbx
	movl	$5, %edi
	movl	$17, %esi
	movl	$.L.str.51, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	movq	%rbx, %r9
	jmp	.LBB3_18
.LBB3_16:
	movl	$5, %edi
	movl	$15, %esi
	movl	$.L.str.55, %edx
	movl	$2, %ecx
	movl	$.L.str.56, %r9d
	jmp	.LBB3_17
.LBB3_20:
	movl	$5, %edi
	movl	$16, %esi
	movl	$.L.str.55, %edx
	movl	$2, %ecx
	movl	$.L.str.57, %r9d
.LBB3_17:
	xorl	%eax, %eax
	movq	%r12, %r8
.LBB3_18:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	Error                   # TAILCALL
.LBB3_29:
	movl	$5, %edi
	movl	$18, %esi
	movl	$.L.str.58, %edx
	movl	$2, %ecx
	movl	$.L.str.57, %r9d
	xorl	%eax, %eax
	movq	%r15, %r8
	jmp	.LBB3_18
.LBB3_30:
	movq	80(%r14), %rdi
	cmpq	80(%rbx), %rdi
	je	.LBB3_32
# BB#31:
	callq	SymName
	movq	%rax, %r14
	movq	80(%rbx), %rdi
	callq	SymName
	movq	%rax, 8(%rsp)
	movq	$.L.str.57, 16(%rsp)
	movq	$.L.str.37, (%rsp)
	movl	$5, %edi
	movl	$19, %esi
	movl	$.L.str.59, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	movq	%r14, %r9
	callq	Error
	jmp	.LBB3_32
.LBB3_47:
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpb	$102, 32(%rax)
	je	.LBB3_32
# BB#48:
	addq	$32, %r13
	movl	$5, %edi
	movl	$20, %esi
	movl	$.L.str.55, %edx
	movl	$2, %ecx
	movl	$.L.str.56, %r9d
	xorl	%eax, %eax
	movq	%r13, %r8
	jmp	.LBB3_18
.Lfunc_end3:
	.size	ReadTokenList, .Lfunc_end3-ReadTokenList
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_33
	.quad	.LBB3_73
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_8
	.quad	.LBB3_1
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_73
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_73
	.quad	.LBB3_10
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_73
	.quad	.LBB3_10
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_12
	.quad	.LBB3_15
	.quad	.LBB3_14
	.quad	.LBB3_19
	.quad	.LBB3_10
	.quad	.LBB3_1
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_13
	.quad	.LBB3_73
	.quad	.LBB3_73
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_10
	.quad	.LBB3_10
.LJTI3_1:
	.quad	.LBB3_69
	.quad	.LBB3_6
	.quad	.LBB3_69
	.quad	.LBB3_6

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"left brace expected here in %s declaration"
	.size	.L.str, 43

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"@PrependGraphic"
	.size	.L.str.1, 16

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"name of %s file expected here"
	.size	.L.str.2, 30

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.zero	1
	.size	.L.str.3, 1

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"unknown or misspelt symbol %s"
	.size	.L.str.5, 30

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"symbol name or %s expected here (%s declaration)"
	.size	.L.str.6, 49

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"{"
	.size	.L.str.7, 2

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"@Database"
	.size	.L.str.8, 10

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"symbol names missing in %s declaration"
	.size	.L.str.9, 39

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	".ld"
	.size	.L.str.10, 4

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%s suffix should be omitted in %s clause"
	.size	.L.str.11, 41

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"named"
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"import"
	.size	.L.str.13, 7

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"langdef"
	.size	.L.str.14, 8

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"def"
	.size	.L.str.15, 4

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"macro"
	.size	.L.str.16, 6

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"extend"
	.size	.L.str.17, 7

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"export"
	.size	.L.str.18, 7

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"import name expected here"
	.size	.L.str.19, 26

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"import %s not in scope"
	.size	.L.str.20, 23

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%s has %s clause, so cannot be extended"
	.size	.L.str.21, 40

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"%s symbol name expected here"
	.size	.L.str.22, 29

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"extend symbol %s not in scope"
	.size	.L.str.23, 30

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"keyword %s or %s expected here"
	.size	.L.str.24, 31

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"keyword %s expected here"
	.size	.L.str.25, 25

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"ignoring export list of macro"
	.size	.L.str.26, 30

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"compulsory"
	.size	.L.str.27, 11

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"symbol name expected here"
	.size	.L.str.28, 26

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"force"
	.size	.L.str.29, 6

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"into"
	.size	.L.str.30, 5

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"horizontally"
	.size	.L.str.31, 13

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"precedence"
	.size	.L.str.32, 11

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"associativity"
	.size	.L.str.33, 14

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"left"
	.size	.L.str.34, 5

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"right"
	.size	.L.str.35, 6

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"body"
	.size	.L.str.36, 5

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"@Begin"
	.size	.L.str.37, 7

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"%s expected here"
	.size	.L.str.38, 17

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"precedence is too low (%d substituted)"
	.size	.L.str.39, 39

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"precedence is too high (%d substituted)"
	.size	.L.str.40, 40

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"associativity altered to %s"
	.size	.L.str.41, 28

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"cannot find %s parameter name"
	.size	.L.str.42, 30

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"@Target"
	.size	.L.str.43, 8

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"opening left brace or @Begin of %s expected"
	.size	.L.str.44, 44

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"exported symbol %s is not defined in %s"
	.size	.L.str.45, 40

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"body parameter %s may not be exported"
	.size	.L.str.46, 38

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"symbol %s exported twice"
	.size	.L.str.47, 25

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"expected opening %s of langdef here"
	.size	.L.str.48, 36

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"%s ignored (name is missing)"
	.size	.L.str.49, 29

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"%s ignored (opening %s is missing)"
	.size	.L.str.50, 35

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"symbol %s unknown or misspelt"
	.size	.L.str.51, 30

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"symbol %s not allowed in macro"
	.size	.L.str.52, 31

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"unexpected end of input"
	.size	.L.str.53, 24

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"%s not expected here"
	.size	.L.str.54, 21

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"unmatched %s in macro"
	.size	.L.str.55, 22

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"}"
	.size	.L.str.56, 2

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"@End"
	.size	.L.str.57, 5

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"symbol name expected after %s"
	.size	.L.str.58, 30

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"%s %s does not match %s %s"
	.size	.L.str.59, 27

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"%s must follow named parameter %s"
	.size	.L.str.60, 34

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"right parameter of %s must begin with %s"
	.size	.L.str.61, 41

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"ReadTokenList: %s"
	.size	.L.str.62, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
