	.text
	.file	"Halignmm.bc"
	.globl	imp_match_out_scH
	.p2align	4, 0x90
	.type	imp_match_out_scH,@function
imp_match_out_scH:                      # @imp_match_out_scH
	.cfi_startproc
# BB#0:
	movq	impmtx(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	%esi, %rcx
	movss	(%rax,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end0:
	.size	imp_match_out_scH, .Lfunc_end0-imp_match_out_scH
	.cfi_endproc

	.globl	imp_match_init_strictH
	.p2align	4, 0x90
	.type	imp_match_init_strictH,@function
imp_match_init_strictH:                 # @imp_match_init_strictH
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movl	%r8d, %r15d
	movl	%ecx, %r13d
	movl	%edx, %ebp
	movl	%esi, %r14d
	movl	imp_match_init_strictH.impalloclen(%rip), %eax
	leal	2(%r13), %ecx
	cmpl	%ecx, %eax
	jl	.LBB1_2
# BB#1:
	leal	2(%r15), %ecx
	cmpl	%ecx, %eax
	jge	.LBB1_9
.LBB1_2:
	movq	impmtx(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	callq	FreeFloatMtx
.LBB1_4:
	movq	imp_match_init_strictH.nocount1(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_6
# BB#5:
	callq	free
.LBB1_6:
	movq	imp_match_init_strictH.nocount2(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_8
# BB#7:
	callq	free
.LBB1_8:
	cmpl	%r15d, %r13d
	movl	%r15d, %edi
	cmovgel	%r13d, %edi
	addl	$2, %edi
	movl	%edi, imp_match_init_strictH.impalloclen(%rip)
	movl	%edi, %esi
	callq	AllocateFloatMtx
	movq	%rax, impmtx(%rip)
	movl	imp_match_init_strictH.impalloclen(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, imp_match_init_strictH.nocount1(%rip)
	movl	imp_match_init_strictH.impalloclen(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, imp_match_init_strictH.nocount2(%rip)
.LBB1_9:                                # %.preheader208
	testl	%r13d, %r13d
	jle	.LBB1_19
# BB#10:                                # %.preheader207.lr.ph
	testl	%r14d, %r14d
	movq	imp_match_init_strictH.nocount1(%rip), %rax
	jle	.LBB1_11
# BB#23:                                # %.preheader207.us.preheader
	movslq	%r14d, %rcx
	movl	%r13d, %edx
	xorl	%esi, %esi
	testb	$1, %dl
	je	.LBB1_27
	.p2align	4, 0x90
.LBB1_24:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi,%rsi,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB1_26
# BB#25:                                #   in Loop: Header=BB1_24 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB1_24
.LBB1_26:                               # %._crit_edge229.us.prol
	cmpl	%r14d, %esi
	setne	(%rax)
	movl	$1, %esi
.LBB1_27:                               # %.preheader207.us.prol.loopexit
	cmpl	$1, %r13d
	je	.LBB1_19
	.p2align	4, 0x90
.LBB1_28:                               # %.preheader207.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_29 Depth 2
                                        #     Child Loop BB1_32 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_29:                               #   Parent Loop BB1_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, (%rbx,%rsi)
	je	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_29 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB1_29
.LBB1_31:                               # %._crit_edge229.us
                                        #   in Loop: Header=BB1_28 Depth=1
	cmpl	%r14d, %edi
	setne	(%rax,%rsi)
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_32:                               #   Parent Loop BB1_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, 1(%rbx,%rsi)
	je	.LBB1_34
# BB#33:                                #   in Loop: Header=BB1_32 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB1_32
.LBB1_34:                               # %._crit_edge229.us.1
                                        #   in Loop: Header=BB1_28 Depth=1
	cmpl	%r14d, %edi
	setne	1(%rax,%rsi)
	addq	$2, %rsi
	cmpq	%rdx, %rsi
	jne	.LBB1_28
	jmp	.LBB1_19
.LBB1_11:                               # %.preheader207.preheader
	setne	%cl
	movl	%r13d, %edx
	cmpl	$31, %r13d
	jbe	.LBB1_12
# BB#15:                                # %min.iters.checked
	movl	%r13d, %r8d
	andl	$31, %r8d
	movq	%rdx, %rdi
	subq	%r8, %rdi
	je	.LBB1_12
# BB#16:                                # %vector.ph
	movzbl	%cl, %esi
	movd	%esi, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	16(%rax), %rbx
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB1_17:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	addq	$32, %rbx
	addq	$-32, %rsi
	jne	.LBB1_17
# BB#18:                                # %middle.block
	testl	%r8d, %r8d
	jne	.LBB1_13
	jmp	.LBB1_19
.LBB1_12:
	xorl	%edi, %edi
.LBB1_13:                               # %.preheader207.preheader307
	addq	%rdi, %rax
	subq	%rdi, %rdx
	.p2align	4, 0x90
.LBB1_14:                               # %.preheader207
                                        # =>This Inner Loop Header: Depth=1
	movb	%cl, (%rax)
	incq	%rax
	decq	%rdx
	jne	.LBB1_14
.LBB1_19:                               # %.preheader206
	testl	%r15d, %r15d
	jle	.LBB1_41
# BB#20:                                # %.preheader205.lr.ph
	testl	%ebp, %ebp
	movq	imp_match_init_strictH.nocount2(%rip), %rax
	jle	.LBB1_21
# BB#45:                                # %.preheader205.us.preheader
	movslq	%ebp, %rcx
	movl	%r15d, %edx
	xorl	%esi, %esi
	testb	$1, %dl
	je	.LBB1_49
	.p2align	4, 0x90
.LBB1_46:                               # =>This Inner Loop Header: Depth=1
	movq	112(%rsp), %rdi
	movq	(%rdi,%rsi,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB1_48
# BB#47:                                #   in Loop: Header=BB1_46 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB1_46
.LBB1_48:                               # %._crit_edge223.us.prol
	cmpl	%ebp, %esi
	setne	(%rax)
	movl	$1, %esi
.LBB1_49:                               # %.preheader205.us.prol.loopexit
	cmpl	$1, %r15d
	je	.LBB1_41
	.p2align	4, 0x90
.LBB1_50:                               # %.preheader205.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_51 Depth 2
                                        #     Child Loop BB1_54 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_51:                               #   Parent Loop BB1_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rsp), %rbx
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, (%rbx,%rsi)
	je	.LBB1_53
# BB#52:                                #   in Loop: Header=BB1_51 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB1_51
.LBB1_53:                               # %._crit_edge223.us
                                        #   in Loop: Header=BB1_50 Depth=1
	cmpl	%ebp, %edi
	setne	(%rax,%rsi)
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_54:                               #   Parent Loop BB1_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rsp), %rbx
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, 1(%rbx,%rsi)
	je	.LBB1_56
# BB#55:                                #   in Loop: Header=BB1_54 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB1_54
.LBB1_56:                               # %._crit_edge223.us.1
                                        #   in Loop: Header=BB1_50 Depth=1
	cmpl	%ebp, %edi
	setne	1(%rax,%rsi)
	addq	$2, %rsi
	cmpq	%rdx, %rsi
	jne	.LBB1_50
	jmp	.LBB1_41
.LBB1_21:                               # %.preheader205.preheader
	setne	%cl
	movl	%r15d, %edx
	cmpl	$31, %r15d
	jbe	.LBB1_22
# BB#35:                                # %min.iters.checked290
	movl	%r15d, %r8d
	andl	$31, %r8d
	movq	%rdx, %rdi
	subq	%r8, %rdi
	je	.LBB1_22
# BB#36:                                # %vector.ph294
	movzbl	%cl, %esi
	movd	%esi, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	16(%rax), %rbx
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB1_37:                               # %vector.body286
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	addq	$32, %rbx
	addq	$-32, %rsi
	jne	.LBB1_37
# BB#38:                                # %middle.block287
	testl	%r8d, %r8d
	jne	.LBB1_39
	jmp	.LBB1_41
.LBB1_22:
	xorl	%edi, %edi
.LBB1_39:                               # %.preheader205.preheader305
	addq	%rdi, %rax
	subq	%rdi, %rdx
	.p2align	4, 0x90
.LBB1_40:                               # %.preheader205
                                        # =>This Inner Loop Header: Depth=1
	movb	%cl, (%rax)
	incq	%rax
	decq	%rdx
	jne	.LBB1_40
.LBB1_41:                               # %.preheader204
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	testl	%r13d, %r13d
	jle	.LBB1_62
# BB#42:                                # %.preheader203.lr.ph
	testl	%r15d, %r15d
	jle	.LBB1_62
# BB#43:                                # %.preheader203.us.preheader
	movq	impmtx(%rip), %r12
	decl	%r15d
	leaq	4(,%r15,4), %rbp
	movl	%r13d, %r15d
	leaq	-1(%r15), %rax
	movq	%r15, %rbx
	andq	$7, %rbx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB1_44
# BB#57:                                # %.preheader203.us.prol.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB1_58:                               # %.preheader203.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%r13,8), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	incq	%r13
	cmpq	%r13, %rbx
	jne	.LBB1_58
	jmp	.LBB1_59
.LBB1_44:
	xorl	%r13d, %r13d
.LBB1_59:                               # %.preheader203.us.prol.loopexit
	cmpq	$7, 24(%rsp)            # 8-byte Folded Reload
	jb	.LBB1_62
# BB#60:                                # %.preheader203.us.preheader.new
	subq	%r13, %r15
	leaq	56(%r12,%r13,8), %rbx
	.p2align	4, 0x90
.LBB1_61:                               # %.preheader203.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	addq	$64, %rbx
	addq	$-8, %r15
	jne	.LBB1_61
.LBB1_62:                               # %._crit_edge220
	testl	%r14d, %r14d
	movl	4(%rsp), %eax           # 4-byte Reload
	jle	.LBB1_100
# BB#63:                                # %.preheader202.lr.ph
	movsd	fastathreshold(%rip), %xmm0 # xmm0 = mem[0],zero
	movq	impmtx(%rip), %r12
	movl	%eax, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%r14d, %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$4294967295, %r15d      # imm = 0xFFFFFFFF
	.p2align	4, 0x90
.LBB1_64:                               # %.preheader202
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_66 Depth 2
                                        #       Child Loop BB1_68 Depth 3
                                        #         Child Loop BB1_69 Depth 4
                                        #         Child Loop BB1_73 Depth 4
                                        #         Child Loop BB1_77 Depth 4
                                        #         Child Loop BB1_80 Depth 4
                                        #         Child Loop BB1_84 Depth 4
	testl	%eax, %eax
	jle	.LBB1_99
# BB#65:                                # %.lr.ph212
                                        #   in Loop: Header=BB1_64 Depth=1
	movq	120(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movsd	(%rax,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movq	136(%rsp), %rax
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_66:                               #   Parent Loop BB1_64 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_68 Depth 3
                                        #         Child Loop BB1_69 Depth 4
                                        #         Child Loop BB1_73 Depth 4
                                        #         Child Loop BB1_77 Depth 4
                                        #         Child Loop BB1_80 Depth 4
                                        #         Child Loop BB1_84 Depth 4
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rsi,8), %r14
	testq	%r14, %r14
	je	.LBB1_98
# BB#67:                                # %.lr.ph
                                        #   in Loop: Header=BB1_66 Depth=2
	movq	128(%rsp), %rax
	movsd	(%rax,%rsi,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rax,%rcx,8), %r8
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_68:                               #   Parent Loop BB1_64 Depth=1
                                        #     Parent Loop BB1_66 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_69 Depth 4
                                        #         Child Loop BB1_73 Depth 4
                                        #         Child Loop BB1_77 Depth 4
                                        #         Child Loop BB1_80 Depth 4
                                        #         Child Loop BB1_84 Depth 4
	movl	$-1, %ebp
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB1_69:                               #   Parent Loop BB1_64 Depth=1
                                        #     Parent Loop BB1_66 Depth=2
                                        #       Parent Loop BB1_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.LBB1_70
# BB#71:                                #   in Loop: Header=BB1_69 Depth=4
	incq	%rdi
	xorl	%ecx, %ecx
	cmpb	$45, %al
	setne	%cl
	addl	%ecx, %ebp
	cmpl	24(%r14), %ebp
	movl	%ebp, %ecx
	jne	.LBB1_69
	jmp	.LBB1_72
	.p2align	4, 0x90
.LBB1_70:                               # %._crit_edge272
                                        #   in Loop: Header=BB1_68 Depth=3
	movl	24(%r14), %ecx
.LBB1_72:                               # %.loopexit
                                        #   in Loop: Header=BB1_68 Depth=3
	movq	%rdi, %rax
	subq	%r8, %rax
	addq	%r15, %rax
	movl	28(%r14), %edx
	cmpl	%edx, %ecx
	movl	%eax, %r9d
	je	.LBB1_76
	.p2align	4, 0x90
.LBB1_73:                               # %.preheader201
                                        #   Parent Loop BB1_64 Depth=1
                                        #     Parent Loop BB1_66 Depth=2
                                        #       Parent Loop BB1_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %ecx
	testb	%cl, %cl
	je	.LBB1_75
# BB#74:                                #   in Loop: Header=BB1_73 Depth=4
	incq	%rdi
	xorl	%ebx, %ebx
	cmpb	$45, %cl
	setne	%bl
	addl	%ebx, %ebp
	cmpl	%edx, %ebp
	jne	.LBB1_73
.LBB1_75:                               #   in Loop: Header=BB1_68 Depth=3
	subl	%r8d, %edi
	leal	(%r15,%rdi), %r9d
.LBB1_76:                               #   in Loop: Header=BB1_68 Depth=3
	movq	112(%rsp), %rcx
	movq	(%rcx,%rsi,8), %r13
	movl	$-1, %ebp
	movq	%r13, %rdi
	.p2align	4, 0x90
.LBB1_77:                               #   Parent Loop BB1_64 Depth=1
                                        #     Parent Loop BB1_66 Depth=2
                                        #       Parent Loop BB1_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %ecx
	testb	%cl, %cl
	je	.LBB1_79
# BB#78:                                #   in Loop: Header=BB1_77 Depth=4
	incq	%rdi
	xorl	%edx, %edx
	cmpb	$45, %cl
	setne	%dl
	addl	%edx, %ebp
	cmpl	32(%r14), %ebp
	jne	.LBB1_77
.LBB1_79:                               #   in Loop: Header=BB1_68 Depth=3
	movq	%rdi, %r10
	subq	%r13, %r10
	addq	%r15, %r10
	movl	36(%r14), %edx
	cmpl	%edx, 32(%r14)
	movl	%r10d, %r11d
	je	.LBB1_83
	.p2align	4, 0x90
.LBB1_80:                               # %.preheader
                                        #   Parent Loop BB1_64 Depth=1
                                        #     Parent Loop BB1_66 Depth=2
                                        #       Parent Loop BB1_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %ecx
	testb	%cl, %cl
	je	.LBB1_82
# BB#81:                                #   in Loop: Header=BB1_80 Depth=4
	incq	%rdi
	xorl	%ebx, %ebx
	cmpb	$45, %cl
	setne	%bl
	addl	%ebx, %ebp
	cmpl	%edx, %ebp
	jne	.LBB1_80
.LBB1_82:                               #   in Loop: Header=BB1_68 Depth=3
	subl	%r13d, %edi
	leal	(%r15,%rdi), %r11d
.LBB1_83:                               #   in Loop: Header=BB1_68 Depth=3
	movslq	%eax, %r15
	addq	%r8, %r15
	movslq	%r10d, %rcx
	addq	%rcx, %r13
	.p2align	4, 0x90
.LBB1_84:                               #   Parent Loop BB1_64 Depth=1
                                        #     Parent Loop BB1_66 Depth=2
                                        #       Parent Loop BB1_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%r15), %ecx
	testb	%cl, %cl
	je	.LBB1_97
# BB#85:                                #   in Loop: Header=BB1_84 Depth=4
	movzbl	(%r13), %edx
	testb	%dl, %dl
	je	.LBB1_97
# BB#86:                                #   in Loop: Header=BB1_84 Depth=4
	cmpb	$45, %cl
	je	.LBB1_89
# BB#87:                                #   in Loop: Header=BB1_84 Depth=4
	cmpb	$45, %dl
	je	.LBB1_89
# BB#88:                                #   in Loop: Header=BB1_84 Depth=4
	movss	64(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	cltq
	movq	(%r12,%rax,8), %rcx
	movslq	%r10d, %r10
	addss	(%rcx,%r10,4), %xmm3
	movss	%xmm3, (%rcx,%r10,4)
	incl	%eax
	incl	%r10d
	incq	%r15
	jmp	.LBB1_94
	.p2align	4, 0x90
.LBB1_89:                               #   in Loop: Header=BB1_84 Depth=4
	cmpb	$45, %dl
	setne	%bl
	cmpb	$45, %cl
	je	.LBB1_91
# BB#90:                                #   in Loop: Header=BB1_84 Depth=4
	testb	%bl, %bl
	je	.LBB1_93
.LBB1_91:                               #   in Loop: Header=BB1_84 Depth=4
	cmpb	$45, %cl
	jne	.LBB1_95
# BB#92:                                #   in Loop: Header=BB1_84 Depth=4
	incl	%eax
	incq	%r15
	cmpb	$45, %dl
	jne	.LBB1_95
	.p2align	4, 0x90
.LBB1_93:                               #   in Loop: Header=BB1_84 Depth=4
	incl	%r10d
.LBB1_94:                               # %.thread
                                        #   in Loop: Header=BB1_84 Depth=4
	incq	%r13
.LBB1_95:                               # %.thread
                                        #   in Loop: Header=BB1_84 Depth=4
	cmpl	%r11d, %r10d
	jg	.LBB1_97
# BB#96:                                # %.thread
                                        #   in Loop: Header=BB1_84 Depth=4
	cmpl	%r9d, %eax
	jle	.LBB1_84
	.p2align	4, 0x90
.LBB1_97:                               # %.critedge
                                        #   in Loop: Header=BB1_68 Depth=3
	movq	8(%r14), %r14
	testq	%r14, %r14
	movl	$4294967295, %r15d      # imm = 0xFFFFFFFF
	movq	24(%rsp), %rsi          # 8-byte Reload
	jne	.LBB1_68
.LBB1_98:                               # %._crit_edge
                                        #   in Loop: Header=BB1_66 Depth=2
	incq	%rsi
	cmpq	40(%rsp), %rsi          # 8-byte Folded Reload
	jne	.LBB1_66
.LBB1_99:                               # %._crit_edge213
                                        #   in Loop: Header=BB1_64 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	32(%rsp), %rcx          # 8-byte Folded Reload
	movl	4(%rsp), %eax           # 4-byte Reload
	jne	.LBB1_64
.LBB1_100:                              # %._crit_edge215
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	imp_match_init_strictH, .Lfunc_end1-imp_match_init_strictH
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4608533498688228557     # double 1.3
.LCPI2_1:
	.quad	-4620693217682128896    # double -0.5
.LCPI2_2:
	.quad	4602678819172646912     # double 0.5
.LCPI2_3:
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_4:
	.long	1176256512              # float 1.0E+4
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_5:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI2_6:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI2_7:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI2_8:
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
.LCPI2_9:
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.text
	.globl	H__align
	.p2align	4, 0x90
	.type	H__align,@function
H__align:                               # @H__align
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$440, %rsp              # imm = 0x1B8
.Lcfi19:
	.cfi_def_cfa_offset 496
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%r9d, 36(%rsp)          # 4-byte Spill
	movl	%r8d, 32(%rsp)          # 4-byte Spill
	movq	%rcx, %r15
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	cvtsi2ssl	penalty(%rip), %xmm0
	movss	%xmm0, 64(%rsp)         # 4-byte Spill
	movl	H__align.orlgth1(%rip), %r14d
	testl	%r14d, %r14d
	jne	.LBB2_2
# BB#1:
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, H__align.mseq1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, H__align.mseq2(%rip)
	movl	H__align.orlgth1(%rip), %r14d
.LBB2_2:
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	(%rbp), %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	cmpl	%r14d, %ebp
	movl	H__align.orlgth2(%rip), %r12d
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movq	%r15, 48(%rsp)          # 8-byte Spill
	jg	.LBB2_5
# BB#3:
	cmpl	%r12d, %eax
	jg	.LBB2_5
# BB#4:
	movl	36(%rsp), %r9d          # 4-byte Reload
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jg	.LBB2_9
	jmp	.LBB2_15
.LBB2_5:
	testl	%r14d, %r14d
	jle	.LBB2_8
# BB#6:
	testl	%r12d, %r12d
	jle	.LBB2_8
# BB#7:
	movq	H__align.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.match(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.m(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.mp(%rip), %rdi
	callq	FreeIntVec
	movq	H__align.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	H__align.gappat1(%rip), %rdi
	callq	free
	movq	H__align.gappat2(%rip), %rdi
	callq	free
	movq	H__align.digf1(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.digf2(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.diaf1(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.diaf2(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.gapz1(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.gapz2(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.gapf1(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.gapf2(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.ogcp1(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.ogcp2(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.fgcp1(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.fgcp2(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.ogcp1g(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.ogcp2g(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.fgcp1g(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.fgcp2g(%rip), %rdi
	callq	FreeFloatVec
	movq	H__align.cpmx1(%rip), %rdi
	callq	FreeFloatMtx
	movq	H__align.cpmx2(%rip), %rdi
	callq	FreeFloatMtx
	movq	H__align.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	H__align.intwork(%rip), %rdi
	callq	FreeIntMtx
	movl	H__align.orlgth1(%rip), %r14d
	movl	H__align.orlgth2(%rip), %r12d
.LBB2_8:
	movq	40(%rsp), %rax          # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	leal	100(%r14), %r15d
	movq	72(%rsp), %rax          # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r12d, %eax
	cmovgel	%eax, %r12d
	leal	100(%r12), %r13d
	leal	102(%r12), %ebx
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.w1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.w2(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.match(%rip)
	leal	102(%r14), %ebp
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.initverticalw(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.lastverticalw(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.m(%rip)
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, H__align.mp(%rip)
	movl	njob(%rip), %edi
	leal	200(%r12,%r14), %esi
	callq	AllocateCharMtx
	movq	%rax, H__align.mseq(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.digf1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.digf2(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.diaf1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.diaf2(%rip)
	movslq	%ebp, %rbp
	movl	$8, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	%rax, H__align.gappat1(%rip)
	movslq	%ebx, %rbx
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, H__align.gappat2(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.gapz1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.gapz2(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.gapf1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.gapf2(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.ogcp1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.ogcp2(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.fgcp1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.fgcp2(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.ogcp1g(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.ogcp2g(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.fgcp1g(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, H__align.fgcp2g(%rip)
	movl	$26, %edi
	movl	%ebp, %esi
	callq	AllocateFloatMtx
	movq	%rax, H__align.cpmx1(%rip)
	movl	$26, %edi
	movl	%ebx, %esi
	callq	AllocateFloatMtx
	movq	%rax, H__align.cpmx2(%rip)
	cmpl	%r13d, %r15d
	cmovgel	%r15d, %r13d
	addl	$2, %r13d
	movl	$26, %esi
	movl	%r13d, %edi
	callq	AllocateFloatMtx
	movq	%rax, H__align.floatwork(%rip)
	movl	$27, %esi
	movl	%r13d, %edi
	callq	AllocateIntMtx
	movq	%rax, H__align.intwork(%rip)
	movl	%r14d, H__align.orlgth1(%rip)
	movl	%r12d, H__align.orlgth2(%rip)
	movl	36(%rsp), %r9d          # 4-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_15
.LBB2_9:                                # %.lr.ph719
	movq	H__align.mseq(%rip), %r10
	movq	H__align.mseq1(%rip), %rdi
	movslq	40(%rsp), %rax          # 4-byte Folded Reload
	movl	32(%rsp), %ecx          # 4-byte Reload
	leaq	-1(%rcx), %r8
	movq	%rcx, %rbx
	xorl	%ebp, %ebp
	andq	$3, %rbx
	je	.LBB2_12
# BB#10:                                # %.prol.preheader1180
	movq	80(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_11:                               # =>This Inner Loop Header: Depth=1
	movq	(%r10,%rbp,8), %rdx
	movq	%rdx, (%rdi,%rbp,8)
	movq	(%rsi,%rbp,8), %rdx
	movb	$0, (%rdx,%rax)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB2_11
.LBB2_12:                               # %.prol.loopexit1181
	cmpq	$3, %r8
	jb	.LBB2_15
# BB#13:                                # %.lr.ph719.new
	subq	%rbp, %rcx
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	24(%rdx,%rbp,8), %rdx
	leaq	24(%rdi,%rbp,8), %rdi
	leaq	24(%r10,%rbp,8), %rsi
	.p2align	4, 0x90
.LBB2_14:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rsi), %rbp
	movq	%rbp, -24(%rdi)
	movq	-24(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	-16(%rsi), %rbp
	movq	%rbp, -16(%rdi)
	movq	-16(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	-8(%rsi), %rbp
	movq	%rbp, -8(%rdi)
	movq	-8(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	(%rsi), %rbp
	movq	%rbp, (%rdi)
	movq	(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	addq	$32, %rdx
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB2_14
.LBB2_15:                               # %.preheader661
	testl	%r9d, %r9d
	jle	.LBB2_23
# BB#16:                                # %.lr.ph716
	movq	H__align.mseq(%rip), %r8
	movq	H__align.mseq2(%rip), %r11
	movslq	72(%rsp), %rax          # 4-byte Folded Reload
	movslq	32(%rsp), %r10          # 4-byte Folded Reload
	movl	%r9d, %ecx
	leaq	-1(%rcx), %r9
	movq	%rcx, %rdx
	andq	$3, %rdx
	je	.LBB2_19
# BB#17:                                # %.prol.preheader1175
	leaq	(%r8,%r10,8), %rsi
	xorl	%ebx, %ebx
	movq	88(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_18:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rbx,8), %rbp
	movq	%rbp, (%r11,%rbx,8)
	movq	(%rdi,%rbx,8), %rbp
	movb	$0, (%rbp,%rax)
	incq	%rbx
	cmpq	%rbx, %rdx
	jne	.LBB2_18
	jmp	.LBB2_20
.LBB2_19:
	xorl	%ebx, %ebx
.LBB2_20:                               # %.prol.loopexit1176
	cmpq	$3, %r9
	jb	.LBB2_23
# BB#21:                                # %.lr.ph716.new
	subq	%rbx, %rcx
	movq	88(%rsp), %rdx          # 8-byte Reload
	leaq	24(%rdx,%rbx,8), %rsi
	leaq	24(%r11,%rbx,8), %rdi
	addq	%rbx, %r10
	leaq	24(%r8,%r10,8), %rdx
	.p2align	4, 0x90
.LBB2_22:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rbp
	movq	%rbp, -24(%rdi)
	movq	-24(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	-16(%rdx), %rbp
	movq	%rbp, -16(%rdi)
	movq	-16(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	-8(%rdx), %rbp
	movq	%rbp, -8(%rdi)
	movq	-8(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	(%rdx), %rbp
	movq	%rbp, (%rdi)
	movq	(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB2_22
.LBB2_23:                               # %._crit_edge717
	movl	commonAlloc1(%rip), %ebp
	cmpl	%ebp, %r14d
	movl	commonAlloc2(%rip), %ebx
	jg	.LBB2_26
# BB#24:                                # %._crit_edge717
	cmpl	%ebx, %r12d
	jg	.LBB2_26
# BB#25:                                # %._crit_edge803
	movq	commonIP(%rip), %rax
	jmp	.LBB2_30
.LBB2_26:                               # %._crit_edge717._crit_edge
	testl	%ebp, %ebp
	je	.LBB2_29
# BB#27:                                # %._crit_edge717._crit_edge
	testl	%ebx, %ebx
	je	.LBB2_29
# BB#28:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movl	H__align.orlgth1(%rip), %r14d
	movl	commonAlloc1(%rip), %ebp
	movl	H__align.orlgth2(%rip), %r12d
	movl	commonAlloc2(%rip), %ebx
.LBB2_29:
	cmpl	%ebp, %r14d
	cmovgel	%r14d, %ebp
	cmpl	%ebx, %r12d
	cmovgel	%r12d, %ebx
	leal	10(%rbp), %edi
	leal	10(%rbx), %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movl	%ebp, commonAlloc1(%rip)
	movl	%ebx, commonAlloc2(%rip)
.LBB2_30:
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	80(%rsp), %rbp          # 8-byte Reload
	movl	32(%rsp), %r8d          # 4-byte Reload
	movq	%rax, H__align.ijp(%rip)
	movq	H__align.cpmx1(%rip), %rsi
	movq	%rbp, %rdi
	movq	%r13, %rdx
	movq	%r13, %rbx
	movq	40(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r8d, %r13d
	callq	cpmx_calc_new
	movq	H__align.cpmx2(%rip), %rsi
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rdx
	movl	%r12d, %ecx
	movl	36(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %r8d
	callq	cpmx_calc_new
	movq	520(%rsp), %rax
	testq	%rax, %rax
	movq	H__align.ogcp1g(%rip), %rdi
	je	.LBB2_32
# BB#31:
	movq	536(%rsp), %rcx
	movq	%rcx, (%rsp)
	movl	%r13d, %esi
	movl	%r13d, %r12d
	movq	%rbp, %rdx
	movq	%rbp, %r14
	movq	%rbx, %rcx
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %r8
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%rax, %r9
	callq	new_OpeningGapCount_zure
	movq	H__align.ogcp2g(%rip), %rdi
	movq	536(%rsp), %rax
	movq	%rax, (%rsp)
	movq	%rax, %rbp
	movl	%r15d, %esi
	movq	88(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	528(%rsp), %r9
	callq	new_OpeningGapCount_zure
	movq	H__align.fgcp1g(%rip), %rdi
	movq	%rbp, (%rsp)
	movl	%r12d, %esi
	movq	%r14, %rbp
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	520(%rsp), %r14
	movq	%r14, %r9
	callq	new_FinalGapCount_zure
	movq	H__align.fgcp2g(%rip), %rdi
	movq	544(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r15d, %esi
	movq	%r13, %rdx
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	movq	72(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%r14, %r9
	callq	new_FinalGapCount_zure
	movq	H__align.digf1(%rip), %rdi
	movq	536(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r12d, %esi
	movq	%rbp, %rdx
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rcx
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	520(%rsp), %r9
	callq	getdigapfreq_part
	movq	H__align.digf2(%rip), %rdi
	movq	544(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r15d, %esi
	movq	%r13, %rdx
	movq	%rbx, %rcx
	movq	72(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	528(%rsp), %r9
	movq	72(%rsp), %r13          # 8-byte Reload
	callq	getdigapfreq_part
	movq	H__align.diaf1(%rip), %rdi
	movq	536(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r12d, %ebx
	movl	%ebx, %esi
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	movq	%r14, %r12
	movq	%r12, %rcx
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	520(%rsp), %r9
	callq	getdiaminofreq_part
	movq	H__align.diaf2(%rip), %rdi
	movq	544(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r15d, %esi
	movq	88(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdx
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rcx
	movl	%r13d, %r8d
	movq	520(%rsp), %r9
	callq	getdiaminofreq_part
	movq	H__align.gapf1(%rip), %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	movq	%r12, %rcx
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	getgapfreq
	movq	H__align.gapf2(%rip), %rdi
	movl	36(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %esi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movl	%r13d, %r8d
	callq	getgapfreq
	movq	H__align.gapz1(%rip), %rdi
	movl	32(%rsp), %esi          # 4-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %rcx
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	getgapfreq_zure
	movq	H__align.gapz2(%rip), %rdi
	movl	%ebx, %esi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movl	%r13d, %r8d
	callq	getgapfreq_zure
	jmp	.LBB2_33
.LBB2_32:
	movl	%r13d, %esi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r8d
	callq	st_OpeningGapCount
	movq	H__align.ogcp2g(%rip), %rdi
	movl	%r15d, %esi
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%r12, %r15
	movl	%r15d, %r8d
	callq	st_OpeningGapCount
	movq	H__align.fgcp1g(%rip), %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rcx
	movl	%ebx, %r8d
	callq	st_FinalGapCount_zure
	movq	H__align.fgcp2g(%rip), %rdi
	movl	36(%rsp), %r12d         # 4-byte Reload
	movl	%r12d, %esi
	movq	88(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, %r8d
	callq	st_FinalGapCount_zure
	movq	H__align.gappat1(%rip), %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	movq	%r14, %rbp
	movq	%rbp, %rcx
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	%r14d, %r8d
	callq	st_getGapPattern
	movq	H__align.gappat2(%rip), %rdi
	movl	%r12d, %esi
	movq	%rbx, %rdx
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rcx
	movl	%r15d, %r8d
	callq	st_getGapPattern
	movq	H__align.digf1(%rip), %rdi
	movl	%r13d, %esi
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	movl	%r14d, %r8d
	callq	getdigapfreq_st
	movq	H__align.digf2(%rip), %rdi
	movl	36(%rsp), %r13d         # 4-byte Reload
	movl	%r13d, %esi
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	movq	%r12, %rcx
	movl	%r15d, %r8d
	callq	getdigapfreq_st
	movq	H__align.diaf1(%rip), %rdi
	movl	32(%rsp), %r12d         # 4-byte Reload
	movl	%r12d, %esi
	movq	%rbx, %rdx
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	movq	%r14, %r15
	movl	%r15d, %r8d
	callq	getdiaminofreq_x
	movq	H__align.diaf2(%rip), %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	movq	%rbp, %r13
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rcx
	movq	72(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %r8d
	callq	getdiaminofreq_x
	movq	H__align.gapf1(%rip), %rdi
	movl	%r12d, %esi
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdx
	movq	%rbx, %rcx
	movl	%r15d, %r8d
	callq	getgapfreq
	movq	H__align.gapf2(%rip), %rdi
	movl	36(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %esi
	movq	%r13, %rdx
	movq	%r14, %rcx
	movl	%ebp, %r8d
	callq	getgapfreq
	movq	H__align.gapz1(%rip), %rdi
	movl	32(%rsp), %esi          # 4-byte Reload
	movq	%r12, %rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, %r8d
	callq	getgapfreq_zure
	movq	H__align.gapz2(%rip), %rdi
	movl	%ebx, %esi
	movq	%r13, %rdx
	movq	%r14, %rcx
	movl	%ebp, %r8d
	callq	getgapfreq_zure
	movq	%rbp, %r13
.LBB2_33:
	movq	H__align.w1(%rip), %r12
	movq	H__align.w2(%rip), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	H__align.initverticalw(%rip), %rdi
	movq	H__align.cpmx2(%rip), %rsi
	movq	H__align.cpmx1(%rip), %rdx
	movq	H__align.floatwork(%rip), %r9
	movq	H__align.intwork(%rip), %rax
	movq	%rax, (%rsp)
	movl	$1, 8(%rsp)
	xorl	%ecx, %ecx
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	match_calc
	cmpq	$0, 504(%rsp)
	je	.LBB2_39
# BB#34:
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_43
# BB#35:                                # %.lr.ph.i
	movq	H__align.initverticalw(%rip), %rax
	movq	impmtx(%rip), %r8
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %ecx
	leaq	-1(%rcx), %rsi
	movq	%rdx, %rbp
	andq	$3, %rbp
	je	.LBB2_40
# BB#36:                                # %.prol.preheader1170
	negq	%rbp
	xorl	%edi, %edi
	movq	%r8, %rdx
	.p2align	4, 0x90
.LBB2_37:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rbx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	leaq	4(%rax), %rax
	decq	%rdi
	addq	$8, %rdx
	cmpq	%rdi, %rbp
	jne	.LBB2_37
# BB#38:                                # %.prol.loopexit1171.unr-lcssa
	negq	%rdi
	cmpq	$3, %rsi
	jae	.LBB2_41
	jmp	.LBB2_43
.LBB2_39:                               # %.critedge
	movq	H__align.cpmx1(%rip), %rsi
	movq	H__align.cpmx2(%rip), %rdx
	movq	H__align.floatwork(%rip), %r9
	movq	H__align.intwork(%rip), %rax
	movq	%rax, (%rsp)
	movl	$1, 8(%rsp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	%r13d, %r8d
	callq	match_calc
	movss	64(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	cmpl	$1, outgap(%rip)
	je	.LBB2_55
	jmp	.LBB2_97
.LBB2_40:
	xorl	%edi, %edi
	cmpq	$3, %rsi
	jb	.LBB2_43
.LBB2_41:                               # %.lr.ph.i.new
	subq	%rdi, %rcx
	leaq	24(%r8,%rdi,8), %rdx
	.p2align	4, 0x90
.LBB2_42:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	movq	-16(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	4(%rax), %xmm0
	movss	%xmm0, 4(%rax)
	movq	-8(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	8(%rax), %xmm0
	movss	%xmm0, 8(%rax)
	movq	(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	12(%rax), %xmm0
	movss	%xmm0, 12(%rax)
	addq	$32, %rdx
	addq	$16, %rax
	addq	$-4, %rcx
	jne	.LBB2_42
.LBB2_43:                               # %imp_match_out_vead_tateH.exit
	movq	H__align.cpmx1(%rip), %rsi
	movq	H__align.cpmx2(%rip), %rdx
	movq	H__align.floatwork(%rip), %r9
	movq	H__align.intwork(%rip), %rax
	movq	%rax, (%rsp)
	movl	$1, 8(%rsp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	%r13d, %r8d
	callq	match_calc
	testl	%r13d, %r13d
	movss	64(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	je	.LBB2_54
# BB#44:                                # %.lr.ph.preheader.i533
	movq	impmtx(%rip), %rax
	movq	(%rax), %rsi
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	leal	(%r13,%rax), %edi
	incq	%rdi
	cmpq	$8, %rdi
	jb	.LBB2_48
# BB#45:                                # %min.iters.checked
	movl	%r13d, %ebp
	andl	$7, %ebp
	subq	%rbp, %rdi
	je	.LBB2_48
# BB#46:                                # %vector.memcheck
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	leal	(%r13,%rax), %eax
	leaq	4(%rsi,%rax,4), %rcx
	cmpq	%rcx, %r12
	jae	.LBB2_249
# BB#47:                                # %vector.memcheck
	leaq	4(%r12,%rax,4), %rax
	cmpq	%rax, %rsi
	jae	.LBB2_249
.LBB2_48:
	movq	%rsi, %rax
	movl	%r13d, %ecx
	movq	%r12, %rdx
.LBB2_49:                               # %.lr.ph.i534.preheader
	leal	-1(%rcx), %esi
	movl	%ecx, %edi
	andl	$3, %edi
	je	.LBB2_52
# BB#50:                                # %.lr.ph.i534.prol.preheader
	negl	%edi
	.p2align	4, 0x90
.LBB2_51:                               # %.lr.ph.i534.prol
                                        # =>This Inner Loop Header: Depth=1
	decl	%ecx
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rax
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	leaq	4(%rdx), %rdx
	incl	%edi
	jne	.LBB2_51
.LBB2_52:                               # %.lr.ph.i534.prol.loopexit
	cmpl	$3, %esi
	jb	.LBB2_54
	.p2align	4, 0x90
.LBB2_53:                               # %.lr.ph.i534
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	4(%rdx), %xmm0
	movss	%xmm0, 4(%rdx)
	movss	8(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	8(%rdx), %xmm0
	movss	%xmm0, 8(%rdx)
	addl	$-4, %ecx
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	leaq	16(%rax), %rax
	addss	12(%rdx), %xmm0
	movss	%xmm0, 12(%rdx)
	leaq	16(%rdx), %rdx
	jne	.LBB2_53
.LBB2_54:                               # %imp_match_out_veadH.exit
	cmpl	$1, outgap(%rip)
	jne	.LBB2_97
.LBB2_55:                               # %.preheader657
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_76
# BB#56:                                # %.lr.ph710
	movq	H__align.initverticalw(%rip), %r13
	movq	H__align.diaf1(%rip), %r10
	movq	H__align.gapf2(%rip), %r11
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm12, %xmm0
	movq	H__align.gappat1(%rip), %r14
	movq	H__align.gappat2(%rip), %r9
	movq	H__align.diaf2(%rip), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	1(%rax), %r15d
	movl	$1, %edi
	xorpd	%xmm1, %xmm1
	movsd	.LCPI2_3(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB2_57:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_60 Depth 2
                                        #     Child Loop BB2_67 Depth 2
                                        #       Child Loop BB2_68 Depth 3
                                        #     Child Loop BB2_73 Depth 2
	movss	(%r13,%rdi,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm3
	movss	%xmm3, (%r13,%rdi,4)
	movss	(%r10,%rdi,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	xorps	%xmm5, %xmm5
	cvtss2sd	%xmm4, %xmm5
	movss	(%r11), %xmm4           # xmm4 = mem[0],zero,zero,zero
	xorps	%xmm6, %xmm6
	cvtss2sd	%xmm4, %xmm6
	movapd	%xmm2, %xmm4
	subsd	%xmm6, %xmm4
	mulsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	cvtsd2ss	%xmm5, %xmm5
	movq	(%r14,%rdi,8), %rax
	movss	4(%rax), %xmm6          # xmm6 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm6
	jne	.LBB2_58
	jnp	.LBB2_75
.LBB2_58:                               #   in Loop: Header=BB2_57 Depth=1
	cvtss2sd	%xmm6, %xmm6
	mulsd	%xmm6, %xmm4
	mulsd	%xmm0, %xmm4
	cvtss2sd	%xmm5, %xmm5
	addsd	%xmm4, %xmm5
	movq	(%r9), %rdx
	movl	8(%rax), %r8d
	testl	%r8d, %r8d
	je	.LBB2_63
# BB#59:                                # %.lr.ph12.i535.preheader
                                        #   in Loop: Header=BB2_57 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	movss	(%rcx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	leaq	16(%rax), %rbx
	xorpd	%xmm4, %xmm4
	movl	%r8d, %ecx
	.p2align	4, 0x90
.LBB2_60:                               # %.lr.ph12.i535
                                        #   Parent Loop BB2_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %ecx
	cmpq	%rdi, %rcx
	jne	.LBB2_62
# BB#61:                                #   in Loop: Header=BB2_60 Depth=2
	movss	-4(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
.LBB2_62:                               #   in Loop: Header=BB2_60 Depth=2
	movl	(%rbx), %ecx
	addq	$8, %rbx
	testl	%ecx, %ecx
	jne	.LBB2_60
	jmp	.LBB2_64
	.p2align	4, 0x90
.LBB2_63:                               #   in Loop: Header=BB2_57 Depth=1
	xorpd	%xmm4, %xmm4
.LBB2_64:                               # %.preheader1.i
                                        #   in Loop: Header=BB2_57 Depth=1
	cvtsd2ss	%xmm5, %xmm5
	movl	8(%rdx), %ebx
	testl	%ebx, %ebx
	je	.LBB2_74
# BB#65:                                # %.preheader.lr.ph.i
                                        #   in Loop: Header=BB2_57 Depth=1
	testl	%r8d, %r8d
	je	.LBB2_72
# BB#66:                                # %.preheader.i536.preheader
                                        #   in Loop: Header=BB2_57 Depth=1
	leaq	8(%rdx), %rbp
	addq	$16, %rax
	.p2align	4, 0x90
.LBB2_67:                               # %.preheader.i536
                                        #   Parent Loop BB2_57 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_68 Depth 3
	addl	%edi, %ebx
	movq	%rax, %rcx
	movl	%r8d, %esi
	.p2align	4, 0x90
.LBB2_68:                               #   Parent Loop BB2_57 Depth=1
                                        #     Parent Loop BB2_67 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%esi, %ebx
	jne	.LBB2_70
# BB#69:                                #   in Loop: Header=BB2_68 Depth=3
	movss	12(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	-4(%rcx), %xmm6
	addss	%xmm6, %xmm4
.LBB2_70:                               #   in Loop: Header=BB2_68 Depth=3
	movl	(%rcx), %esi
	addq	$8, %rcx
	testl	%esi, %esi
	jne	.LBB2_68
# BB#71:                                # %.loopexit.i
                                        #   in Loop: Header=BB2_67 Depth=2
	movq	%rbp, %rdx
	movl	8(%rbp), %ebx
	addq	$8, %rbp
	testl	%ebx, %ebx
	jne	.LBB2_67
	jmp	.LBB2_74
.LBB2_72:                               # %.preheader.us.i.preheader
                                        #   in Loop: Header=BB2_57 Depth=1
	addq	$16, %rdx
	.p2align	4, 0x90
.LBB2_73:                               # %.preheader.us.i
                                        #   Parent Loop BB2_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB2_73
.LBB2_74:                               # %countnocountx.exit
                                        #   in Loop: Header=BB2_57 Depth=1
	mulss	%xmm12, %xmm4
	subss	%xmm4, %xmm5
.LBB2_75:                               #   in Loop: Header=BB2_57 Depth=1
	addss	%xmm3, %xmm5
	movss	%xmm5, (%r13,%rdi,4)
	incq	%rdi
	cmpq	%r15, %rdi
	jne	.LBB2_57
.LBB2_76:                               # %.preheader654
	movq	72(%rsp), %r15          # 8-byte Reload
	testl	%r15d, %r15d
	jle	.LBB2_100
# BB#77:                                # %.lr.ph706
	movq	H__align.diaf2(%rip), %r10
	movq	H__align.gapf1(%rip), %r11
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm12, %xmm0
	movq	H__align.gappat2(%rip), %r14
	movq	H__align.gappat1(%rip), %r9
	movq	H__align.diaf1(%rip), %r8
	leal	1(%r15), %r15d
	movl	$1, %edi
	xorpd	%xmm1, %xmm1
	movsd	.LCPI2_3(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB2_78:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_81 Depth 2
                                        #     Child Loop BB2_88 Depth 2
                                        #       Child Loop BB2_89 Depth 3
                                        #     Child Loop BB2_94 Depth 2
	movss	(%r12,%rdi,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm3
	movss	%xmm3, (%r12,%rdi,4)
	movss	(%r10,%rdi,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	xorps	%xmm5, %xmm5
	cvtss2sd	%xmm4, %xmm5
	movss	(%r11), %xmm4           # xmm4 = mem[0],zero,zero,zero
	xorps	%xmm6, %xmm6
	cvtss2sd	%xmm4, %xmm6
	movapd	%xmm2, %xmm4
	subsd	%xmm6, %xmm4
	mulsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	cvtsd2ss	%xmm5, %xmm5
	movq	(%r14,%rdi,8), %rbx
	movss	4(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm6
	jne	.LBB2_79
	jnp	.LBB2_96
.LBB2_79:                               #   in Loop: Header=BB2_78 Depth=1
	cvtss2sd	%xmm6, %xmm6
	mulsd	%xmm6, %xmm4
	mulsd	%xmm0, %xmm4
	cvtss2sd	%xmm5, %xmm5
	addsd	%xmm4, %xmm5
	movq	(%r9), %rax
	movl	8(%rbx), %r13d
	testl	%r13d, %r13d
	je	.LBB2_84
# BB#80:                                # %.lr.ph12.i546.preheader
                                        #   in Loop: Header=BB2_78 Depth=1
	movss	(%r8), %xmm6            # xmm6 = mem[0],zero,zero,zero
	leaq	16(%rbx), %rdx
	xorpd	%xmm4, %xmm4
	movl	%r13d, %esi
	.p2align	4, 0x90
.LBB2_81:                               # %.lr.ph12.i546
                                        #   Parent Loop BB2_78 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %ecx
	cmpq	%rdi, %rcx
	jne	.LBB2_83
# BB#82:                                #   in Loop: Header=BB2_81 Depth=2
	movss	-4(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
.LBB2_83:                               #   in Loop: Header=BB2_81 Depth=2
	movl	(%rdx), %esi
	addq	$8, %rdx
	testl	%esi, %esi
	jne	.LBB2_81
	jmp	.LBB2_85
	.p2align	4, 0x90
.LBB2_84:                               #   in Loop: Header=BB2_78 Depth=1
	xorpd	%xmm4, %xmm4
.LBB2_85:                               # %.preheader1.i539
                                        #   in Loop: Header=BB2_78 Depth=1
	cvtsd2ss	%xmm5, %xmm5
	movl	8(%rax), %edx
	testl	%edx, %edx
	je	.LBB2_95
# BB#86:                                # %.preheader.lr.ph.i540
                                        #   in Loop: Header=BB2_78 Depth=1
	testl	%r13d, %r13d
	je	.LBB2_93
# BB#87:                                # %.preheader.i553.preheader
                                        #   in Loop: Header=BB2_78 Depth=1
	leaq	8(%rax), %rbp
	addq	$16, %rbx
	.p2align	4, 0x90
.LBB2_88:                               # %.preheader.i553
                                        #   Parent Loop BB2_78 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_89 Depth 3
	addl	%edi, %edx
	movq	%rbx, %rsi
	movl	%r13d, %ecx
	.p2align	4, 0x90
.LBB2_89:                               #   Parent Loop BB2_78 Depth=1
                                        #     Parent Loop BB2_88 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ecx, %edx
	jne	.LBB2_91
# BB#90:                                #   in Loop: Header=BB2_89 Depth=3
	movss	12(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	-4(%rsi), %xmm6
	addss	%xmm6, %xmm4
.LBB2_91:                               #   in Loop: Header=BB2_89 Depth=3
	movl	(%rsi), %ecx
	addq	$8, %rsi
	testl	%ecx, %ecx
	jne	.LBB2_89
# BB#92:                                # %.loopexit.i549
                                        #   in Loop: Header=BB2_88 Depth=2
	movq	%rbp, %rax
	movl	8(%rbp), %edx
	addq	$8, %rbp
	testl	%edx, %edx
	jne	.LBB2_88
	jmp	.LBB2_95
.LBB2_93:                               # %.preheader.us.i543.preheader
                                        #   in Loop: Header=BB2_78 Depth=1
	addq	$16, %rax
	.p2align	4, 0x90
.LBB2_94:                               # %.preheader.us.i543
                                        #   Parent Loop BB2_78 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB2_94
.LBB2_95:                               # %countnocountx.exit558
                                        #   in Loop: Header=BB2_78 Depth=1
	mulss	%xmm12, %xmm4
	subss	%xmm4, %xmm5
.LBB2_96:                               #   in Loop: Header=BB2_78 Depth=1
	addss	%xmm3, %xmm5
	movss	%xmm5, (%r12,%rdi,4)
	incq	%rdi
	cmpq	%r15, %rdi
	jne	.LBB2_78
	jmp	.LBB2_110
.LBB2_97:                               # %.preheader660
	testl	%r13d, %r13d
	jle	.LBB2_104
# BB#98:                                # %.lr.ph714
	movslq	offset(%rip), %rax
	leaq	1(%r13), %rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB2_101
# BB#99:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI2_1(%rip), %xmm0
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%r12)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB2_102
	jmp	.LBB2_104
.LBB2_100:                              # %.loopexit655.thread
	movq	H__align.m(%rip), %rbp
	movl	$0, (%rbp)
	movb	$1, 31(%rsp)            # 1-byte Folded Spill
	movq	144(%rsp), %r10         # 8-byte Reload
	jmp	.LBB2_127
.LBB2_101:
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB2_104
.LBB2_102:                              # %.lr.ph714.new
	subq	%rdx, %rcx
	leaq	4(%r12,%rdx,4), %rsi
	leaq	1(%rdx), %rdi
	imull	%eax, %edx
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB2_103:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%eax, %ebp
	addq	$-2, %rcx
	jne	.LBB2_103
.LBB2_104:                              # %.preheader658
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_110
# BB#105:                               # %.lr.ph712
	movq	H__align.initverticalw(%rip), %rsi
	movslq	offset(%rip), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx), %rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB2_107
# BB#106:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI2_1(%rip), %xmm0
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsi)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB2_108
	jmp	.LBB2_110
.LBB2_107:
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB2_110
.LBB2_108:                              # %.lr.ph712.new
	subq	%rdx, %rcx
	leaq	4(%rsi,%rdx,4), %rsi
	leaq	1(%rdx), %rdi
	imull	%eax, %edx
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB2_109:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%eax, %ebp
	addq	$-2, %rcx
	jne	.LBB2_109
.LBB2_110:                              # %.loopexit655
	movq	H__align.m(%rip), %rbp
	movl	$0, (%rbp)
	movq	72(%rsp), %r15          # 8-byte Reload
	testl	%r15d, %r15d
	setle	31(%rsp)                # 1-byte Folded Spill
	movq	144(%rsp), %r10         # 8-byte Reload
	jle	.LBB2_113
# BB#111:                               # %.lr.ph701
	movq	H__align.mp(%rip), %rcx
	movss	.LCPI2_4(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	leaq	1(%r15), %rsi
	movl	%esi, %eax
	leaq	-1(%rax), %r8
	cmpq	$8, %r8
	jae	.LBB2_114
# BB#112:
	movl	$1, %edi
	jmp	.LBB2_122
.LBB2_113:
	movb	$1, 31(%rsp)            # 1-byte Folded Spill
	jmp	.LBB2_127
.LBB2_114:                              # %min.iters.checked845
	movl	%r15d, %r9d
	andl	$7, %r9d
	movq	%r8, %rdx
	subq	%r9, %rdx
	je	.LBB2_118
# BB#115:                               # %vector.memcheck860
	leaq	4(%rbp), %rdi
	leaq	-4(%r12,%rax,4), %rbx
	cmpq	%rbx, %rdi
	jae	.LBB2_119
# BB#116:                               # %vector.memcheck860
	leaq	(%rbp,%rax,4), %rdi
	cmpq	%rdi, %r12
	jae	.LBB2_119
.LBB2_118:
	movl	$1, %edi
	jmp	.LBB2_122
.LBB2_119:                              # %vector.ph861
	leaq	1(%rdx), %rdi
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	xorl	%ebx, %ebx
	xorps	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB2_120:                              # %vector.body841
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm2, 4(%rcx,%rbx,4)
	movups	%xmm2, 20(%rcx,%rbx,4)
	movups	(%r12,%rbx,4), %xmm3
	movups	16(%r12,%rbx,4), %xmm4
	addps	%xmm2, %xmm3
	addps	%xmm2, %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm1, %xmm4
	movups	%xmm3, 4(%rbp,%rbx,4)
	movups	%xmm4, 20(%rbp,%rbx,4)
	addq	$8, %rbx
	cmpq	%rbx, %rdx
	jne	.LBB2_120
# BB#121:                               # %middle.block842
	testq	%r9, %r9
	je	.LBB2_127
.LBB2_122:                              # %scalar.ph843.preheader
	subl	%edi, %esi
	testb	$1, %sil
	movq	%rdi, %rsi
	je	.LBB2_124
# BB#123:                               # %scalar.ph843.prol
	movl	$0, (%rcx,%rdi,4)
	xorps	%xmm1, %xmm1
	addss	-4(%r12,%rdi,4), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rbp,%rdi,4)
	leaq	1(%rdi), %rsi
.LBB2_124:                              # %scalar.ph843.prol.loopexit
	cmpq	%rdi, %r8
	je	.LBB2_127
# BB#125:                               # %scalar.ph843.preheader.new
	subq	%rsi, %rax
	leaq	(%r12,%rsi,4), %rdx
	leaq	4(%rcx,%rsi,4), %rcx
	leaq	4(%rbp,%rsi,4), %rsi
	xorps	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB2_126:                              # %scalar.ph843
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, -4(%rcx)
	movss	-4(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm2
	addss	%xmm0, %xmm2
	movss	%xmm2, -4(%rsi)
	movl	$0, (%rcx)
	movss	(%rdx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm2
	addss	%xmm0, %xmm2
	movss	%xmm2, (%rsi)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$8, %rsi
	addq	$-2, %rax
	jne	.LBB2_126
.LBB2_127:                              # %._crit_edge702
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	xorps	%xmm13, %xmm13
	testl	%r15d, %r15d
	xorpd	%xmm0, %xmm0
	je	.LBB2_129
# BB#128:
	movq	%r15, %rcx
	shlq	$32, %rcx
	addq	%rax, %rcx
	sarq	$30, %rcx
	movss	(%r12,%rcx), %xmm0      # xmm0 = mem[0],zero,zero,zero
.LBB2_129:
	movq	H__align.lastverticalw(%rip), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movss	%xmm0, (%rcx)
	movl	outgap(%rip), %edx
	cmpl	$1, %edx
	movq	40(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	sbbl	$-1, %ecx
	cmpl	$2, %ecx
	jl	.LBB2_213
# BB#130:                               # %.lr.ph693
	movl	%edx, 200(%rsp)         # 4-byte Spill
	movq	%rbp, 264(%rsp)         # 8-byte Spill
	testl	%r15d, %r15d
	sete	%dl
	movq	H__align.initverticalw(%rip), %rsi
	movq	%rsi, 216(%rsp)         # 8-byte Spill
	movq	H__align.cpmx1(%rip), %rdi
	movq	H__align.floatwork(%rip), %rsi
	movq	%rsi, 304(%rsp)         # 8-byte Spill
	movq	H__align.intwork(%rip), %rsi
	movq	%rsi, 296(%rsp)         # 8-byte Spill
	cmpq	$0, 504(%rsp)
	sete	%bl
	orb	%dl, %bl
	movb	%bl, 71(%rsp)           # 1-byte Spill
	movss	.LCPI2_4(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm8
	movq	H__align.ijp(%rip), %rdx
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	H__align.mp(%rip), %rdx
	movq	%rdx, 256(%rsp)         # 8-byte Spill
	movq	%r15, %rdx
	shlq	$32, %rdx
	addq	%rax, %rdx
	sarq	$32, %rdx
	movq	%rdx, 288(%rsp)         # 8-byte Spill
	movq	H__align.gappat2(%rip), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	cvtss2sd	%xmm12, %xmm11
	movl	%ecx, %eax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	leal	-1(%r15), %eax
	incq	%rax
	movabsq	$8589934584, %rcx       # imm = 0x1FFFFFFF8
	movq	%rax, 208(%rsp)         # 8-byte Spill
	andq	%rax, %rcx
	leaq	-8(%rcx), %rax
	shrq	$3, %rax
	movl	%r15d, %edx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	subl	%ecx, %edx
	movl	%edx, 204(%rsp)         # 4-byte Spill
	movq	%rax, 232(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	xorps	%xmm10, %xmm10
	movsd	.LCPI2_3(%rip), %xmm9   # xmm9 = mem[0],zero
	xorps	%xmm13, %xmm13
	movq	H__align.diaf1(%rip), %r13
	movq	H__align.gappat1(%rip), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movq	H__align.diaf2(%rip), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	H__align.gapf1(%rip), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	H__align.gapf2(%rip), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	impmtx(%rip), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	leal	1(%r15), %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	$1, %r8d
	movq	%r13, 176(%rsp)         # 8-byte Spill
	jmp	.LBB2_138
.LBB2_131:                              # %vector.body876.preheader
                                        #   in Loop: Header=BB2_138 Depth=1
	cmpq	$0, 224(%rsp)           # 8-byte Folded Reload
	jne	.LBB2_133
# BB#132:                               # %vector.body876.prol
                                        #   in Loop: Header=BB2_138 Depth=1
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm2
	movups	(%r11), %xmm3
	movups	16(%r11), %xmm4
	addps	%xmm0, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, (%r11)
	movups	%xmm4, 16(%r11)
	movl	$8, %esi
	cmpq	$0, 232(%rsp)           # 8-byte Folded Reload
	jne	.LBB2_134
	jmp	.LBB2_136
.LBB2_133:                              #   in Loop: Header=BB2_138 Depth=1
	xorl	%esi, %esi
	cmpq	$0, 232(%rsp)           # 8-byte Folded Reload
	je	.LBB2_136
.LBB2_134:                              # %vector.body876.preheader.new
                                        #   in Loop: Header=BB2_138 Depth=1
	movq	136(%rsp), %rax         # 8-byte Reload
	subq	%rsi, %rax
	leaq	48(%r11,%rsi,4), %rdx
	leaq	48(%rcx,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB2_135:                              # %vector.body876
                                        #   Parent Loop BB2_138 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm2
	movups	-48(%rdx), %xmm3
	movups	-32(%rdx), %xmm4
	addps	%xmm0, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -48(%rdx)
	movups	%xmm4, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm2
	movups	-16(%rdx), %xmm3
	movups	(%rdx), %xmm4
	addps	%xmm0, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rdx)
	movups	%xmm4, (%rdx)
	addq	$64, %rdx
	addq	$64, %rsi
	addq	$-16, %rax
	jne	.LBB2_135
.LBB2_136:                              # %middle.block877
                                        #   in Loop: Header=BB2_138 Depth=1
	movq	136(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, 208(%rsp)         # 8-byte Folded Reload
	je	.LBB2_158
# BB#137:                               #   in Loop: Header=BB2_138 Depth=1
	movq	136(%rsp), %rax         # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	leaq	(%r11,%rax,4), %rdx
	movl	204(%rsp), %eax         # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jmp	.LBB2_153
	.p2align	4, 0x90
.LBB2_138:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_139 Depth 2
                                        #       Child Loop BB2_140 Depth 3
                                        #     Child Loop BB2_143 Depth 2
                                        #       Child Loop BB2_145 Depth 3
                                        #     Child Loop BB2_135 Depth 2
                                        #     Child Loop BB2_155 Depth 2
                                        #     Child Loop BB2_157 Depth 2
                                        #     Child Loop BB2_161 Depth 2
                                        #       Child Loop BB2_168 Depth 3
                                        #       Child Loop BB2_175 Depth 3
                                        #         Child Loop BB2_176 Depth 4
                                        #       Child Loop BB2_181 Depth 3
                                        #       Child Loop BB2_191 Depth 3
                                        #       Child Loop BB2_197 Depth 3
                                        #         Child Loop BB2_198 Depth 4
                                        #       Child Loop BB2_203 Depth 3
	movq	%r10, %r11
	movq	%r12, %r10
	leaq	-1(%r8), %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	movq	216(%rsp), %rax         # 8-byte Reload
	movl	-4(%rax,%r8,4), %eax
	movl	%eax, (%r10)
	movl	$n_dis, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_139:                              #   Parent Loop BB2_138 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_140 Depth 3
	movl	$0, 336(%rsp,%rcx,4)
	xorpd	%xmm0, %xmm0
	movl	$1, %edx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB2_140:                              #   Parent Loop BB2_138 Depth=1
                                        #     Parent Loop BB2_139 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm2, %xmm2
	cvtsi2ssl	(%rsi), %xmm2
	movq	-8(%rdi,%rdx,8), %rbp
	mulss	(%rbp,%r8,4), %xmm2
	addss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	cvtsi2ssl	104(%rsi), %xmm0
	movq	(%rdi,%rdx,8), %rbp
	mulss	(%rbp,%r8,4), %xmm0
	addss	%xmm2, %xmm0
	addq	$208, %rsi
	addq	$2, %rdx
	cmpq	$27, %rdx
	jne	.LBB2_140
# BB#141:                               #   in Loop: Header=BB2_139 Depth=2
	movss	%xmm0, 336(%rsp,%rcx,4)
	incq	%rcx
	addq	$4, %rax
	cmpq	$26, %rcx
	jne	.LBB2_139
# BB#142:                               # %.preheader.i559
                                        #   in Loop: Header=BB2_138 Depth=1
	testl	%r15d, %r15d
	movl	%r15d, %r9d
	movq	296(%rsp), %rcx         # 8-byte Reload
	movq	304(%rsp), %rdx         # 8-byte Reload
	movq	%r11, %rsi
	je	.LBB2_147
	.p2align	4, 0x90
.LBB2_143:                              # %.lr.ph84.i
                                        #   Parent Loop BB2_138 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_145 Depth 3
	decl	%r9d
	movl	$0, (%rsi)
	movq	(%rcx), %rbp
	movl	(%rbp), %eax
	testl	%eax, %eax
	js	.LBB2_146
# BB#144:                               # %.lr.ph.preheader.i563
                                        #   in Loop: Header=BB2_143 Depth=2
	movq	(%rdx), %rbx
	addq	$4, %rbp
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB2_145:                              # %.lr.ph.i564
                                        #   Parent Loop BB2_138 Depth=1
                                        #     Parent Loop BB2_143 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cltq
	movss	336(%rsp,%rax,4), %xmm2 # xmm2 = mem[0],zero,zero,zero
	mulss	(%rbx), %xmm2
	addq	$4, %rbx
	addss	%xmm2, %xmm0
	movss	%xmm0, (%rsi)
	movl	(%rbp), %eax
	addq	$4, %rbp
	testl	%eax, %eax
	jns	.LBB2_145
.LBB2_146:                              # %._crit_edge.i
                                        #   in Loop: Header=BB2_143 Depth=2
	addq	$8, %rcx
	addq	$8, %rdx
	addq	$4, %rsi
	testl	%r9d, %r9d
	jne	.LBB2_143
.LBB2_147:                              # %match_calc.exit
                                        #   in Loop: Header=BB2_138 Depth=1
	cmpb	$0, 71(%rsp)            # 1-byte Folded Reload
	jne	.LBB2_158
# BB#148:                               # %.lr.ph.preheader.i565
                                        #   in Loop: Header=BB2_138 Depth=1
	movq	208(%rsp), %rdx         # 8-byte Reload
	cmpq	$8, %rdx
	movq	240(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r8,8), %rcx
	jb	.LBB2_152
# BB#149:                               # %min.iters.checked880
                                        #   in Loop: Header=BB2_138 Depth=1
	cmpq	$0, 136(%rsp)           # 8-byte Folded Reload
	je	.LBB2_152
# BB#150:                               # %vector.memcheck894
                                        #   in Loop: Header=BB2_138 Depth=1
	leaq	(%rcx,%rdx,4), %rax
	cmpq	%rax, %r11
	jae	.LBB2_131
# BB#151:                               # %vector.memcheck894
                                        #   in Loop: Header=BB2_138 Depth=1
	leaq	(%r11,%rdx,4), %rax
	cmpq	%rax, %rcx
	jae	.LBB2_131
	.p2align	4, 0x90
.LBB2_152:                              #   in Loop: Header=BB2_138 Depth=1
	movl	%r15d, %eax
	movq	%r11, %rdx
.LBB2_153:                              # %.lr.ph.i569.preheader
                                        #   in Loop: Header=BB2_138 Depth=1
	leal	-1(%rax), %esi
	movl	%eax, %ebp
	andl	$3, %ebp
	je	.LBB2_156
# BB#154:                               # %.lr.ph.i569.prol.preheader
                                        #   in Loop: Header=BB2_138 Depth=1
	negl	%ebp
	.p2align	4, 0x90
.LBB2_155:                              # %.lr.ph.i569.prol
                                        #   Parent Loop BB2_138 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%eax
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rcx
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	leaq	4(%rdx), %rdx
	incl	%ebp
	jne	.LBB2_155
.LBB2_156:                              # %.lr.ph.i569.prol.loopexit
                                        #   in Loop: Header=BB2_138 Depth=1
	cmpl	$3, %esi
	jb	.LBB2_158
	.p2align	4, 0x90
.LBB2_157:                              # %.lr.ph.i569
                                        #   Parent Loop BB2_138 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	movss	4(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	4(%rdx), %xmm0
	movss	%xmm0, 4(%rdx)
	movss	8(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	8(%rdx), %xmm0
	movss	%xmm0, 8(%rdx)
	addl	$-4, %eax
	movss	12(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	leaq	16(%rcx), %rcx
	addss	12(%rdx), %xmm0
	movss	%xmm0, 12(%rdx)
	leaq	16(%rdx), %rdx
	jne	.LBB2_157
.LBB2_158:                              # %imp_match_out_veadH.exit571
                                        #   in Loop: Header=BB2_138 Depth=1
	movq	216(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%r8,4), %eax
	movl	%eax, (%r11)
	movss	(%r10), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm4
	addss	%xmm10, %xmm4
	addss	%xmm8, %xmm4
	movss	%xmm4, H__align.mi(%rip)
	xorl	%r14d, %r14d
	cmpb	$0, 31(%rsp)            # 1-byte Folded Reload
	jne	.LBB2_211
# BB#159:                               # %.lr.ph686
                                        #   in Loop: Header=BB2_138 Depth=1
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r8,8), %r12
	movq	248(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r8,8), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	16(%rax), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movq	%r10, %rax
	movl	$1, %r10d
	movq	%r11, 272(%rsp)         # 8-byte Spill
	movq	%r11, %r15
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	256(%rsp), %rbx         # 8-byte Reload
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	%rax, %r11
	jmp	.LBB2_161
	.p2align	4, 0x90
.LBB2_160:                              # %._crit_edge805
                                        #   in Loop: Header=BB2_161 Depth=2
	addq	$4, %r11
	movss	(%r11), %xmm6           # xmm6 = mem[0],zero,zero,zero
.LBB2_161:                              #   Parent Loop BB2_138 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_168 Depth 3
                                        #       Child Loop BB2_175 Depth 3
                                        #         Child Loop BB2_176 Depth 4
                                        #       Child Loop BB2_181 Depth 3
                                        #       Child Loop BB2_191 Depth 3
                                        #       Child Loop BB2_197 Depth 3
                                        #         Child Loop BB2_198 Depth 4
                                        #       Child Loop BB2_203 Depth 3
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r10,8), %r9
	movss	4(%r9), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm10, %xmm0
	movaps	%xmm6, %xmm3
	jne	.LBB2_162
	jnp	.LBB2_163
.LBB2_162:                              #   in Loop: Header=BB2_161 Depth=2
	movss	(%r13,%r8,4), %xmm3     # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	mulss	%xmm12, %xmm3
	addss	%xmm6, %xmm3
.LBB2_163:                              #   in Loop: Header=BB2_161 Depth=2
	addq	$4, %r12
	movq	56(%rsp), %rax          # 8-byte Reload
	movss	4(%rax), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movq	104(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r10,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	ucomiss	%xmm10, %xmm7
	jne	.LBB2_164
	jnp	.LBB2_165
.LBB2_164:                              #   in Loop: Header=BB2_161 Depth=2
	movaps	%xmm7, %xmm2
	mulss	%xmm5, %xmm2
	mulss	%xmm12, %xmm2
	addss	%xmm2, %xmm3
.LBB2_165:                              # %._crit_edge807
                                        #   in Loop: Header=BB2_161 Depth=2
	movaps	%xmm7, %xmm13
	mulss	%xmm0, %xmm13
	mulss	%xmm12, %xmm13
	addss	%xmm3, %xmm13
	movl	$0, (%r12)
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm5, %xmm3
	movq	184(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r8,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm1
	movapd	%xmm9, %xmm2
	subsd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm3
	mulsd	%xmm11, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	ucomiss	%xmm10, %xmm0
	jne	.LBB2_166
	jnp	.LBB2_183
.LBB2_166:                              #   in Loop: Header=BB2_161 Depth=2
	cvtss2sd	%xmm0, %xmm0
	mulsd	%xmm0, %xmm2
	mulsd	%xmm11, %xmm2
	cvtss2sd	%xmm3, %xmm3
	addsd	%xmm2, %xmm3
	movl	%r10d, %eax
	movl	%r14d, 64(%rsp)         # 4-byte Spill
	subl	%r14d, %eax
	decl	%eax
	movl	8(%r9), %r14d
	testl	%r14d, %r14d
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	je	.LBB2_171
# BB#167:                               # %.lr.ph12.i581.preheader
                                        #   in Loop: Header=BB2_161 Depth=2
	movss	(%r13,%r8,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	leaq	16(%r9), %rcx
	xorpd	%xmm0, %xmm0
	movl	%r14d, %edx
	.p2align	4, 0x90
.LBB2_168:                              # %.lr.ph12.i581
                                        #   Parent Loop BB2_138 Depth=1
                                        #     Parent Loop BB2_161 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%eax, %edx
	jne	.LBB2_170
# BB#169:                               #   in Loop: Header=BB2_168 Depth=3
	movss	-4(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
.LBB2_170:                              #   in Loop: Header=BB2_168 Depth=3
	movl	(%rcx), %edx
	addq	$8, %rcx
	testl	%edx, %edx
	jne	.LBB2_168
	jmp	.LBB2_172
	.p2align	4, 0x90
.LBB2_171:                              #   in Loop: Header=BB2_161 Depth=2
	xorpd	%xmm0, %xmm0
.LBB2_172:                              # %.preheader1.i574
                                        #   in Loop: Header=BB2_161 Depth=2
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	cvtsd2ss	%xmm3, %xmm3
	movq	128(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %ebx
	movq	%r12, %rbp
	testl	%ebx, %ebx
	je	.LBB2_182
# BB#173:                               # %.preheader.lr.ph.i575
                                        #   in Loop: Header=BB2_161 Depth=2
	testl	%r14d, %r14d
	je	.LBB2_180
# BB#174:                               # %.preheader.i588.preheader
                                        #   in Loop: Header=BB2_161 Depth=2
	leaq	16(%r9), %r12
	movq	128(%rsp), %r13         # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_175:                              # %.preheader.i588
                                        #   Parent Loop BB2_138 Depth=1
                                        #     Parent Loop BB2_161 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_176 Depth 4
	addl	%eax, %ebx
	movq	%r12, %rcx
	movl	%r14d, %edx
	.p2align	4, 0x90
.LBB2_176:                              #   Parent Loop BB2_138 Depth=1
                                        #     Parent Loop BB2_161 Depth=2
                                        #       Parent Loop BB2_175 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	%edx, %ebx
	jne	.LBB2_178
# BB#177:                               #   in Loop: Header=BB2_176 Depth=4
	movss	12(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	-4(%rcx), %xmm1
	addss	%xmm1, %xmm0
.LBB2_178:                              #   in Loop: Header=BB2_176 Depth=4
	movl	(%rcx), %edx
	addq	$8, %rcx
	testl	%edx, %edx
	jne	.LBB2_176
# BB#179:                               # %.loopexit.i584
                                        #   in Loop: Header=BB2_175 Depth=3
	movq	%r13, %rsi
	movl	8(%r13), %ebx
	addq	$8, %r13
	testl	%ebx, %ebx
	jne	.LBB2_175
	jmp	.LBB2_182
.LBB2_180:                              # %.preheader.us.i578.preheader
                                        #   in Loop: Header=BB2_161 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_181:                              # %.preheader.us.i578
                                        #   Parent Loop BB2_138 Depth=1
                                        #     Parent Loop BB2_161 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$0, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB2_181
.LBB2_182:                              # %countnocountx.exit594
                                        #   in Loop: Header=BB2_161 Depth=2
	mulss	%xmm12, %xmm0
	subss	%xmm0, %xmm3
	movq	176(%rsp), %r13         # 8-byte Reload
	movl	64(%rsp), %r14d         # 4-byte Reload
	movq	%rbp, %r12
	movq	168(%rsp), %rdx         # 8-byte Reload
	movq	192(%rsp), %rbx         # 8-byte Reload
.LBB2_183:                              #   in Loop: Header=BB2_161 Depth=2
	addss	%xmm4, %xmm3
	ucomiss	%xmm13, %xmm3
	jbe	.LBB2_185
# BB#184:                               #   in Loop: Header=BB2_161 Depth=2
	movl	%r14d, %eax
	subl	%r10d, %eax
	movl	%eax, (%r12)
	movaps	%xmm3, %xmm13
.LBB2_185:                              #   in Loop: Header=BB2_161 Depth=2
	ucomiss	%xmm4, %xmm6
	jb	.LBB2_187
# BB#186:                               #   in Loop: Header=BB2_161 Depth=2
	movss	%xmm6, H__align.mi(%rip)
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	leal	(%r10,%rax), %r14d
	movaps	%xmm6, %xmm4
.LBB2_187:                              #   in Loop: Header=BB2_161 Depth=2
	addq	$4, %rbx
	addq	$4, %rdx
	movss	(%r13,%r8,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm1
	movq	152(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r10,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm0, %xmm2
	movapd	%xmm9, %xmm0
	subsd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm1
	mulsd	%xmm11, %xmm1
	cvtsd2ss	%xmm1, %xmm6
	ucomiss	%xmm10, %xmm7
	jne	.LBB2_188
	jnp	.LBB2_205
.LBB2_188:                              #   in Loop: Header=BB2_161 Depth=2
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm7, %xmm1
	mulsd	%xmm1, %xmm0
	mulsd	%xmm11, %xmm0
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm6, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm6, %xmm6
	cvtsd2ss	%xmm1, %xmm6
	movslq	(%rbx), %rcx
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	cmpl	$0, -4(%rax,%r10,4)
	jne	.LBB2_205
# BB#189:                               #   in Loop: Header=BB2_161 Depth=2
	movl	%r8d, %eax
	subl	%ecx, %eax
	decl	%eax
	movq	128(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %ebp
	testl	%ebp, %ebp
	movq	%r15, 320(%rsp)         # 8-byte Spill
	movq	%r11, 312(%rsp)         # 8-byte Spill
	movq	%rdx, %r15
	xorps	%xmm7, %xmm7
	je	.LBB2_194
# BB#190:                               # %.lr.ph12.i604.preheader
                                        #   in Loop: Header=BB2_161 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%ebp, %edx
	.p2align	4, 0x90
.LBB2_191:                              # %.lr.ph12.i604
                                        #   Parent Loop BB2_138 Depth=1
                                        #     Parent Loop BB2_161 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%eax, %edx
	jne	.LBB2_193
# BB#192:                               #   in Loop: Header=BB2_191 Depth=3
	movss	-4(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm7
.LBB2_193:                              #   in Loop: Header=BB2_191 Depth=3
	movl	(%rcx), %edx
	addq	$8, %rcx
	testl	%edx, %edx
	jne	.LBB2_191
.LBB2_194:                              # %.preheader1.i597
                                        #   in Loop: Header=BB2_161 Depth=2
	movl	8(%r9), %esi
	movq	%rbx, %r11
	testl	%esi, %esi
	je	.LBB2_204
# BB#195:                               # %.preheader.lr.ph.i598
                                        #   in Loop: Header=BB2_161 Depth=2
	testl	%ebp, %ebp
	je	.LBB2_202
# BB#196:                               # %.preheader.i611.preheader
                                        #   in Loop: Header=BB2_161 Depth=2
	leaq	8(%r9), %rcx
	.p2align	4, 0x90
.LBB2_197:                              # %.preheader.i611
                                        #   Parent Loop BB2_138 Depth=1
                                        #     Parent Loop BB2_161 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_198 Depth 4
	addl	%eax, %esi
	movq	48(%rsp), %rdx          # 8-byte Reload
	movl	%ebp, %ebx
	.p2align	4, 0x90
.LBB2_198:                              #   Parent Loop BB2_138 Depth=1
                                        #     Parent Loop BB2_161 Depth=2
                                        #       Parent Loop BB2_197 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	%ebx, %esi
	jne	.LBB2_200
# BB#199:                               #   in Loop: Header=BB2_198 Depth=4
	movss	12(%r9), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	-4(%rdx), %xmm0
	addss	%xmm0, %xmm7
.LBB2_200:                              #   in Loop: Header=BB2_198 Depth=4
	movl	(%rdx), %ebx
	addq	$8, %rdx
	testl	%ebx, %ebx
	jne	.LBB2_198
# BB#201:                               # %.loopexit.i607
                                        #   in Loop: Header=BB2_197 Depth=3
	movq	%rcx, %r9
	movl	8(%rcx), %esi
	addq	$8, %rcx
	testl	%esi, %esi
	jne	.LBB2_197
	jmp	.LBB2_204
.LBB2_202:                              # %.preheader.us.i601.preheader
                                        #   in Loop: Header=BB2_161 Depth=2
	addq	$16, %r9
	.p2align	4, 0x90
.LBB2_203:                              # %.preheader.us.i601
                                        #   Parent Loop BB2_138 Depth=1
                                        #     Parent Loop BB2_161 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$0, (%r9)
	leaq	8(%r9), %r9
	jne	.LBB2_203
.LBB2_204:                              # %countnocountx.exit617
                                        #   in Loop: Header=BB2_161 Depth=2
	mulss	%xmm12, %xmm7
	subss	%xmm7, %xmm6
	movq	%r15, %rdx
	movq	%r11, %rbx
	movq	320(%rsp), %r15         # 8-byte Reload
	movq	312(%rsp), %r11         # 8-byte Reload
.LBB2_205:                              #   in Loop: Header=BB2_161 Depth=2
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm6
	ucomiss	%xmm13, %xmm6
	jbe	.LBB2_207
# BB#206:                               #   in Loop: Header=BB2_161 Depth=2
	movl	%r8d, %eax
	subl	(%rbx), %eax
	movl	%eax, (%r12)
	movaps	%xmm6, %xmm13
.LBB2_207:                              #   in Loop: Header=BB2_161 Depth=2
	addq	$4, %r15
	movss	(%r11), %xmm2           # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	jb	.LBB2_209
# BB#208:                               #   in Loop: Header=BB2_161 Depth=2
	movss	%xmm2, (%rdx)
	movq	328(%rsp), %rax         # 8-byte Reload
	movl	%eax, (%rbx)
.LBB2_209:                              #   in Loop: Header=BB2_161 Depth=2
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm13, %xmm0
	movss	%xmm0, (%r15)
	incq	%r10
	cmpq	96(%rsp), %r10          # 8-byte Folded Reload
	jne	.LBB2_160
# BB#210:                               #   in Loop: Header=BB2_138 Depth=1
	movq	72(%rsp), %r15          # 8-byte Reload
	movq	144(%rsp), %r10         # 8-byte Reload
	movq	272(%rsp), %r11         # 8-byte Reload
.LBB2_211:                              # %._crit_edge687
                                        #   in Loop: Header=BB2_138 Depth=1
	movq	288(%rsp), %rax         # 8-byte Reload
	movl	(%r11,%rax,4), %eax
	movq	112(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx,%r8,4)
	incq	%r8
	cmpq	280(%rsp), %r8          # 8-byte Folded Reload
	movq	%r11, %r12
	jne	.LBB2_138
# BB#212:                               # %._crit_edge694
	movl	%r14d, H__align.mpi(%rip)
	movq	112(%rsp), %rax         # 8-byte Reload
	movl	200(%rsp), %edx         # 4-byte Reload
	testl	%edx, %edx
	jne	.LBB2_226
	jmp	.LBB2_214
.LBB2_213:
	movq	%r12, %r11
	movq	112(%rsp), %rax         # 8-byte Reload
	testl	%edx, %edx
	jne	.LBB2_226
.LBB2_214:                              # %.preheader650
	cmpb	$0, 31(%rsp)            # 1-byte Folded Reload
	jne	.LBB2_220
# BB#215:                               # %.lr.ph675
	movslq	offset(%rip), %r8
	movq	%r15, %rdx
	incq	%rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB2_217
# BB#216:
	leal	-1(%r15), %edx
	imull	%r8d, %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	mulsd	.LCPI2_1(%rip), %xmm0
	movss	4(%r11), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%r11)
	movl	$2, %ebp
	cmpq	$2, %rcx
	jne	.LBB2_218
	jmp	.LBB2_220
.LBB2_217:
	movl	$1, %ebp
	cmpq	$2, %rcx
	je	.LBB2_220
.LBB2_218:                              # %.lr.ph675.new
	subq	%rbp, %rcx
	movl	%r15d, %edx
	subl	%ebp, %edx
	imull	%r8d, %edx
	leaq	4(%r11,%rbp,4), %rsi
	leal	-1(%r15), %edi
	subl	%ebp, %edi
	imull	%r8d, %edi
	addl	%r8d, %r8d
	xorl	%ebp, %ebp
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB2_219:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	subl	%r8d, %ebp
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB2_219
.LBB2_220:                              # %.preheader649
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_226
# BB#221:                               # %.lr.ph673
	xorps	%xmm0, %xmm0
	cvtsi2sdl	offset(%rip), %xmm0
	movq	40(%rsp), %rcx          # 8-byte Reload
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	incq	%rcx
	movl	%ecx, %esi
	testb	$1, %cl
	jne	.LBB2_223
# BB#222:
	movsd	.LCPI2_1(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movss	4(%rax), %xmm3          # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm2, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, 4(%rax)
	movl	$2, %ecx
	cmpq	$2, %rsi
	jne	.LBB2_224
	jmp	.LBB2_226
.LBB2_223:
	movl	$1, %ecx
	cmpq	$2, %rsi
	je	.LBB2_226
.LBB2_224:
	movsd	.LCPI2_2(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB2_225:                              # =>This Inner Loop Header: Depth=1
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%ecx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	(%rax,%rcx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, (%rax,%rcx,4)
	leal	1(%rcx), %edx
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	4(%rax,%rcx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, 4(%rax,%rcx,4)
	addq	$2, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB2_225
.LBB2_226:                              # %.loopexit
	movq	%r11, %r15
	movss	%xmm13, 152(%rsp)       # 4-byte Spill
	movq	H__align.mseq1(%rip), %r13
	movq	H__align.mseq2(%rip), %r14
	movq	H__align.ijp(%rip), %r12
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	strlen
	leal	(%rax,%rbx), %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	movq	%rax, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	leal	1(%rax,%rbx), %ebx
	movl	%ebx, %edi
	callq	AllocateCharVec
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateCharVec
	movq	%rax, %r8
	movl	outgap(%rip), %eax
	cmpq	$0, 504(%rsp)
	movq	%r14, 160(%rsp)         # 8-byte Spill
	movq	%r12, 168(%rsp)         # 8-byte Spill
	movq	%r8, 120(%rsp)          # 8-byte Spill
	je	.LBB2_237
# BB#227:
	cmpl	$1, %eax
	movq	48(%rsp), %r9           # 8-byte Reload
	movq	56(%rsp), %r11          # 8-byte Reload
	je	.LBB2_261
# BB#228:
	movq	112(%rsp), %rbp         # 8-byte Reload
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	testl	%r11d, %r11d
	jle	.LBB2_234
# BB#229:                               # %.lr.ph55.i
	movslq	%r11d, %rax
	movslq	%r9d, %rcx
	movl	%eax, %edx
	addq	$4, %rbp
	decq	%rdx
	movl	%r11d, %esi
	movapd	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB2_232
	.p2align	4, 0x90
.LBB2_231:
	movq	(%r12,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movapd	%xmm0, %xmm1
	testq	%rdx, %rdx
	je	.LBB2_233
.LBB2_230:                              # %._crit_edge97.i
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rbp
	decq	%rdx
	decl	%esi
	ucomiss	%xmm1, %xmm0
	jae	.LBB2_231
.LBB2_232:
	testq	%rdx, %rdx
	jne	.LBB2_230
.LBB2_233:
	movapd	%xmm1, %xmm0
.LBB2_234:                              # %.preheader9.i
	testl	%r9d, %r9d
	jle	.LBB2_261
# BB#235:                               # %.lr.ph51.i
	movslq	%r11d, %r10
	movslq	%r9d, %rcx
	movl	%ecx, %edx
	testb	$1, %r9b
	jne	.LBB2_247
# BB#236:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB2_255
	jmp	.LBB2_261
.LBB2_237:
	cmpl	$1, %eax
	movq	48(%rsp), %r10          # 8-byte Reload
	movq	56(%rsp), %r11          # 8-byte Reload
	je	.LBB2_336
# BB#238:
	movq	112(%rsp), %rsi         # 8-byte Reload
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	testl	%r11d, %r11d
	jle	.LBB2_244
# BB#239:                               # %.lr.ph54.i
	movslq	%r11d, %rax
	movslq	%r10d, %rcx
	movl	%eax, %edx
	addq	$4, %rsi
	movq	%rsi, %rbp
	decq	%rdx
	movl	%r11d, %esi
	movapd	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB2_242
	.p2align	4, 0x90
.LBB2_241:
	movq	(%r12,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movapd	%xmm0, %xmm1
	testq	%rdx, %rdx
	je	.LBB2_243
.LBB2_240:                              # %._crit_edge96.i
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rbp
	decq	%rdx
	decl	%esi
	ucomiss	%xmm1, %xmm0
	jae	.LBB2_241
.LBB2_242:
	testq	%rdx, %rdx
	jne	.LBB2_240
.LBB2_243:
	movapd	%xmm1, %xmm0
.LBB2_244:                              # %.preheader8.i
	testl	%r10d, %r10d
	jle	.LBB2_336
# BB#245:                               # %.lr.ph50.i
	movslq	%r11d, %r9
	movslq	%r10d, %rcx
	movl	%ecx, %edx
	testb	$1, %r10b
	jne	.LBB2_252
# BB#246:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB2_330
	jmp	.LBB2_336
.LBB2_247:
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB2_254
# BB#248:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB2_255
	jmp	.LBB2_261
.LBB2_249:                              # %vector.body.preheader
	leaq	(%rsi,%rdi,4), %rax
	movl	%r13d, %ecx
	subl	%edi, %ecx
	leaq	(%r12,%rdi,4), %rdx
	leaq	16(%r12), %rbx
	addq	$16, %rsi
	.p2align	4, 0x90
.LBB2_250:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	-16(%rbx), %xmm2
	movups	(%rbx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, -16(%rbx)
	movups	%xmm3, (%rbx)
	addq	$32, %rbx
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB2_250
# BB#251:                               # %middle.block
	testq	%rbp, %rbp
	jne	.LBB2_49
	jmp	.LBB2_54
.LBB2_252:
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB2_329
# BB#253:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB2_330
	jmp	.LBB2_336
.LBB2_254:
	movl	%r9d, %esi
	negl	%esi
	movq	(%r12,%r10,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movl	$1, %esi
	movapd	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB2_261
.LBB2_255:                              # %.lr.ph51.i.new
	movl	%r9d, %edi
	negl	%edi
	.p2align	4, 0x90
.LBB2_256:                              # =>This Inner Loop Header: Depth=1
	movq	%r15, %rax
	movss	(%rax,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB2_258
# BB#257:                               #   in Loop: Header=BB2_256 Depth=1
	leal	(%rdi,%rsi), %ebp
	movq	(%r12,%r10,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movapd	%xmm1, %xmm0
.LBB2_258:                              #   in Loop: Header=BB2_256 Depth=1
	movss	4(%rax,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB2_260
# BB#259:                               #   in Loop: Header=BB2_256 Depth=1
	leal	1(%rdi,%rsi), %ebp
	movq	(%r12,%r10,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movapd	%xmm1, %xmm0
.LBB2_260:                              #   in Loop: Header=BB2_256 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB2_256
.LBB2_261:                              # %.preheader8.i618
	testl	%r11d, %r11d
	js	.LBB2_267
# BB#262:                               # %.lr.ph48.preheader.i
	movq	%r11, %rsi
	incq	%rsi
	movl	%esi, %eax
	leaq	-1(%rax), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB2_264
	.p2align	4, 0x90
.LBB2_263:                              # %.lr.ph48.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB2_263
.LBB2_264:                              # %.lr.ph48.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB2_267
# BB#265:                               # %.lr.ph48.preheader.i.new
	negq	%rax
	leaq	4(%rdx,%rax), %rax
	leaq	56(%r12,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_266:                              # %.lr.ph48.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%rax,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB2_266
.LBB2_267:                              # %.preheader7.i625
	testl	%r9d, %r9d
	js	.LBB2_276
# BB#268:                               # %.lr.ph45.i
	movq	(%r12), %rcx
	movq	%r9, %rsi
	incq	%rsi
	movl	%esi, %eax
	cmpq	$7, %rax
	jbe	.LBB2_273
# BB#269:                               # %min.iters.checked1017
	andl	$7, %esi
	movq	%rax, %rbp
	subq	%rsi, %rbp
	je	.LBB2_273
# BB#270:                               # %vector.body1013.preheader
	leaq	16(%rcx), %rdi
	movdqa	.LCPI2_5(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI2_6(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI2_7(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB2_271:                              # %vector.body1013
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdi)
	movdqu	%xmm4, (%rdi)
	paddd	%xmm3, %xmm0
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB2_271
# BB#272:                               # %middle.block1014
	testq	%rsi, %rsi
	jne	.LBB2_274
	jmp	.LBB2_276
.LBB2_273:
	xorl	%ebp, %ebp
.LBB2_274:                              # %scalar.ph1015.preheader
	movl	%ebp, %edx
	notl	%edx
	leaq	(%rcx,%rbp,4), %rcx
	subq	%rbp, %rax
	.p2align	4, 0x90
.LBB2_275:                              # %scalar.ph1015
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rcx)
	decl	%edx
	addq	$4, %rcx
	decq	%rax
	jne	.LBB2_275
.LBB2_276:                              # %._crit_edge46.i
	movq	%r13, 176(%rsp)         # 8-byte Spill
	movslq	%r11d, %rax
	movq	96(%rsp), %r15          # 8-byte Reload
	addq	%rax, %r15
	movslq	%r9d, %rcx
	movb	$0, (%rcx,%r15)
	addq	%rcx, %r15
	addq	%rax, %r8
	leaq	(%r8,%rcx), %r13
	movb	$0, (%rcx,%r8)
	movq	512(%rsp), %rax
	movl	$0, (%rax)
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	js	.LBB2_322
# BB#277:                               # %.lr.ph37.preheader.i
	xorl	%ecx, %ecx
	movq	impmtx(%rip), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movdqa	.LCPI2_9(%rip), %xmm0   # xmm0 = [45,0,45,0,45,0,45,0,45,0,45,0,45,0,45,0]
	packuswb	%xmm0, %xmm0
	movdqa	.LCPI2_8(%rip), %xmm1   # xmm1 = [111,0,111,0,111,0,111,0,111,0,111,0,111,0,111,0]
	packuswb	%xmm1, %xmm1
	movl	%r11d, %r12d
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	.p2align	4, 0x90
.LBB2_278:                              # %.lr.ph37.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_292 Depth 2
                                        #     Child Loop BB2_296 Depth 2
                                        #     Child Loop BB2_299 Depth 2
                                        #     Child Loop BB2_307 Depth 2
                                        #     Child Loop BB2_311 Depth 2
                                        #     Child Loop BB2_314 Depth 2
	movslq	%r12d, %rdx
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	movq	(%rax,%rdx,8), %rax
	movslq	%r9d, %rdx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movl	(%rax,%rdx,4), %r10d
	testl	%r10d, %r10d
	js	.LBB2_281
# BB#279:                               #   in Loop: Header=BB2_278 Depth=1
	je	.LBB2_282
# BB#280:                               #   in Loop: Header=BB2_278 Depth=1
	movl	%r12d, %edx
	subl	%r10d, %edx
	jmp	.LBB2_283
	.p2align	4, 0x90
.LBB2_281:                              #   in Loop: Header=BB2_278 Depth=1
	leal	-1(%r12), %edx
	jmp	.LBB2_284
	.p2align	4, 0x90
.LBB2_282:                              #   in Loop: Header=BB2_278 Depth=1
	leal	-1(%r12), %edx
.LBB2_283:                              #   in Loop: Header=BB2_278 Depth=1
	movl	$-1, %r10d
.LBB2_284:                              #   in Loop: Header=BB2_278 Depth=1
	movl	%r12d, %edi
	subl	%edx, %edi
	decl	%edi
	je	.LBB2_301
# BB#285:                               # %.lr.ph18.preheader.i
                                        #   in Loop: Header=BB2_278 Depth=1
	leal	-2(%r12), %ebp
	subl	%edx, %ebp
	movq	%rbp, %r8
	negq	%r8
	leaq	1(%rbp), %rsi
	cmpq	$16, %rsi
	jae	.LBB2_287
# BB#286:                               #   in Loop: Header=BB2_278 Depth=1
	movq	%r13, %rbp
	movq	%r15, %rax
	jmp	.LBB2_294
	.p2align	4, 0x90
.LBB2_287:                              # %min.iters.checked1076
                                        #   in Loop: Header=BB2_278 Depth=1
	movl	%esi, %r11d
	andl	$15, %r11d
	subq	%r11, %rsi
	je	.LBB2_290
# BB#288:                               # %vector.memcheck1089
                                        #   in Loop: Header=BB2_278 Depth=1
	leaq	-1(%r15,%r8), %rax
	cmpq	%r13, %rax
	jae	.LBB2_291
# BB#289:                               # %vector.memcheck1089
                                        #   in Loop: Header=BB2_278 Depth=1
	leaq	-1(%r13,%r8), %rax
	cmpq	%r15, %rax
	jae	.LBB2_291
.LBB2_290:                              #   in Loop: Header=BB2_278 Depth=1
	movq	%r13, %rbp
	movq	%r15, %rax
	movq	56(%rsp), %r11          # 8-byte Reload
	jmp	.LBB2_294
.LBB2_291:                              # %vector.body1072.preheader
                                        #   in Loop: Header=BB2_278 Depth=1
	movl	%edx, 104(%rsp)         # 4-byte Spill
	subl	%esi, %edi
	leaq	-1(%r11), %rax
	subq	%rbp, %rax
	leaq	(%r13,%rax), %rbp
	addq	%r15, %rax
	leaq	-8(%r15), %rbx
	leaq	-8(%r13), %rdx
	.p2align	4, 0x90
.LBB2_292:                              # %vector.body1072
                                        #   Parent Loop BB2_278 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm1, (%rbx)
	movq	%xmm1, -8(%rbx)
	movq	%xmm0, (%rdx)
	movq	%xmm0, -8(%rdx)
	addq	$-16, %rbx
	addq	$-16, %rdx
	addq	$-16, %rsi
	jne	.LBB2_292
# BB#293:                               # %middle.block1073
                                        #   in Loop: Header=BB2_278 Depth=1
	testq	%r11, %r11
	movq	56(%rsp), %r11          # 8-byte Reload
	movl	104(%rsp), %edx         # 4-byte Reload
	je	.LBB2_300
	.p2align	4, 0x90
.LBB2_294:                              # %.lr.ph18.i.preheader
                                        #   in Loop: Header=BB2_278 Depth=1
	leal	-1(%rdi), %esi
	movl	%edi, %ebx
	andl	$7, %ebx
	je	.LBB2_297
# BB#295:                               # %.lr.ph18.i.prol.preheader
                                        #   in Loop: Header=BB2_278 Depth=1
	negl	%ebx
	.p2align	4, 0x90
.LBB2_296:                              # %.lr.ph18.i.prol
                                        #   Parent Loop BB2_278 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, -1(%rax)
	decq	%rax
	movb	$45, -1(%rbp)
	decq	%rbp
	decl	%edi
	incl	%ebx
	jne	.LBB2_296
.LBB2_297:                              # %.lr.ph18.i.prol.loopexit
                                        #   in Loop: Header=BB2_278 Depth=1
	cmpl	$7, %esi
	jb	.LBB2_300
# BB#298:                               # %.lr.ph18.i.preheader.new
                                        #   in Loop: Header=BB2_278 Depth=1
	decq	%rbp
	decq	%rax
	.p2align	4, 0x90
.LBB2_299:                              # %.lr.ph18.i
                                        #   Parent Loop BB2_278 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, (%rax)
	movb	$45, (%rbp)
	movb	$111, -1(%rax)
	movb	$45, -1(%rbp)
	movb	$111, -2(%rax)
	movb	$45, -2(%rbp)
	movb	$111, -3(%rax)
	movb	$45, -3(%rbp)
	movb	$111, -4(%rax)
	movb	$45, -4(%rbp)
	movb	$111, -5(%rax)
	movb	$45, -5(%rbp)
	movb	$111, -6(%rax)
	movb	$45, -6(%rbp)
	movb	$111, -7(%rax)
	movb	$45, -7(%rbp)
	addq	$-8, %rbp
	addq	$-8, %rax
	addl	$-8, %edi
	jne	.LBB2_299
.LBB2_300:                              # %._crit_edge19.loopexit.i
                                        #   in Loop: Header=BB2_278 Depth=1
	leaq	-1(%r15,%r8), %r15
	leaq	-1(%r13,%r8), %r13
	leal	-1(%rcx,%r12), %ecx
	subl	%edx, %ecx
.LBB2_301:                              # %._crit_edge19.i
                                        #   in Loop: Header=BB2_278 Depth=1
	cmpl	$-1, %r10d
	je	.LBB2_316
# BB#302:                               # %.lr.ph26.preheader.i
                                        #   in Loop: Header=BB2_278 Depth=1
	movl	%r10d, %edi
	notl	%edi
	movl	$-2, %esi
	subl	%r10d, %esi
	movq	%rsi, %rbx
	negq	%rbx
	leaq	1(%rsi), %rax
	cmpq	$16, %rax
	movl	%edi, %r8d
	movq	%r13, %rbp
	movq	%r15, %r14
	jb	.LBB2_309
# BB#303:                               # %min.iters.checked1038
                                        #   in Loop: Header=BB2_278 Depth=1
	movl	%eax, %r11d
	andl	$15, %r11d
	subq	%r11, %rax
	movl	%edi, %r8d
	movq	%r13, %rbp
	movq	%r15, %r14
	je	.LBB2_309
# BB#304:                               # %vector.memcheck1051
                                        #   in Loop: Header=BB2_278 Depth=1
	movl	%edx, 104(%rsp)         # 4-byte Spill
	leaq	-1(%r15,%rbx), %rdx
	cmpq	%r13, %rdx
	jae	.LBB2_306
# BB#305:                               # %vector.memcheck1051
                                        #   in Loop: Header=BB2_278 Depth=1
	leaq	-1(%r13,%rbx), %rdx
	cmpq	%r15, %rdx
	movl	104(%rsp), %edx         # 4-byte Reload
	movl	%edi, %r8d
	movq	%r13, %rbp
	movq	%r15, %r14
	jb	.LBB2_309
.LBB2_306:                              # %vector.body1034.preheader
                                        #   in Loop: Header=BB2_278 Depth=1
	movl	%edi, %r8d
	subl	%eax, %r8d
	leaq	-1(%r11), %r14
	subq	%rsi, %r14
	leaq	(%r13,%r14), %rbp
	addq	%r15, %r14
	leaq	-8(%r15), %rsi
	leaq	-8(%r13), %rdx
	.p2align	4, 0x90
.LBB2_307:                              # %vector.body1034
                                        #   Parent Loop BB2_278 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm0, (%rsi)
	movq	%xmm0, -8(%rsi)
	movq	%xmm1, (%rdx)
	movq	%xmm1, -8(%rdx)
	addq	$-16, %rsi
	addq	$-16, %rdx
	addq	$-16, %rax
	jne	.LBB2_307
# BB#308:                               # %middle.block1035
                                        #   in Loop: Header=BB2_278 Depth=1
	testq	%r11, %r11
	movl	104(%rsp), %edx         # 4-byte Reload
	je	.LBB2_315
	.p2align	4, 0x90
.LBB2_309:                              # %.lr.ph26.i.preheader
                                        #   in Loop: Header=BB2_278 Depth=1
	leal	-1(%r8), %eax
	movl	%r8d, %esi
	andl	$7, %esi
	je	.LBB2_312
# BB#310:                               # %.lr.ph26.i.prol.preheader
                                        #   in Loop: Header=BB2_278 Depth=1
	negl	%esi
	.p2align	4, 0x90
.LBB2_311:                              # %.lr.ph26.i.prol
                                        #   Parent Loop BB2_278 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, -1(%r14)
	decq	%r14
	movb	$111, -1(%rbp)
	decq	%rbp
	decl	%r8d
	incl	%esi
	jne	.LBB2_311
.LBB2_312:                              # %.lr.ph26.i.prol.loopexit
                                        #   in Loop: Header=BB2_278 Depth=1
	cmpl	$7, %eax
	jb	.LBB2_315
# BB#313:                               # %.lr.ph26.i.preheader.new
                                        #   in Loop: Header=BB2_278 Depth=1
	decq	%rbp
	decq	%r14
	.p2align	4, 0x90
.LBB2_314:                              # %.lr.ph26.i
                                        #   Parent Loop BB2_278 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, (%r14)
	movb	$111, (%rbp)
	movb	$45, -1(%r14)
	movb	$111, -1(%rbp)
	movb	$45, -2(%r14)
	movb	$111, -2(%rbp)
	movb	$45, -3(%r14)
	movb	$111, -3(%rbp)
	movb	$45, -4(%r14)
	movb	$111, -4(%rbp)
	movb	$45, -5(%r14)
	movb	$111, -5(%rbp)
	movb	$45, -6(%r14)
	movb	$111, -6(%rbp)
	movb	$45, -7(%r14)
	movb	$111, -7(%rbp)
	addq	$-8, %rbp
	addq	$-8, %r14
	addl	$-8, %r8d
	jne	.LBB2_314
.LBB2_315:                              # %._crit_edge27.loopexit.i
                                        #   in Loop: Header=BB2_278 Depth=1
	leaq	-1(%r15,%rbx), %r15
	leaq	-1(%r13,%rbx), %r13
	addl	%edi, %ecx
	movq	160(%rsp), %r14         # 8-byte Reload
	movq	56(%rsp), %r11          # 8-byte Reload
.LBB2_316:                              # %._crit_edge27.i
                                        #   in Loop: Header=BB2_278 Depth=1
	cmpl	%r11d, %r12d
	je	.LBB2_319
# BB#317:                               # %._crit_edge27.i
                                        #   in Loop: Header=BB2_278 Depth=1
	cmpl	48(%rsp), %r9d          # 4-byte Folded Reload
	je	.LBB2_319
# BB#318:                               #   in Loop: Header=BB2_278 Depth=1
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	192(%rsp), %rsi         # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movq	128(%rsp), %rsi         # 8-byte Reload
	movss	(%rax,%rsi,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movq	512(%rsp), %rax
	addss	(%rax), %xmm2
	movss	%xmm2, (%rax)
.LBB2_319:                              #   in Loop: Header=BB2_278 Depth=1
	testl	%r12d, %r12d
	jle	.LBB2_322
# BB#320:                               #   in Loop: Header=BB2_278 Depth=1
	testl	%r9d, %r9d
	jle	.LBB2_322
# BB#321:                               #   in Loop: Header=BB2_278 Depth=1
	addl	%r10d, %r9d
	movb	$111, -1(%r15)
	decq	%r15
	movb	$111, -1(%r13)
	decq	%r13
	addl	$2, %ecx
	cmpl	64(%rsp), %ecx          # 4-byte Folded Reload
	movl	%edx, %r12d
	jle	.LBB2_278
.LBB2_322:                              # %._crit_edge38.i
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	movq	176(%rsp), %r12         # 8-byte Reload
	jle	.LBB2_325
# BB#323:                               # %.lr.ph13.preheader.i
	movl	32(%rsp), %ebx          # 4-byte Reload
	movq	80(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_324:                              # %.lr.ph13.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdi
	movq	(%rbp), %rsi
	movq	%r15, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %r12
	decq	%rbx
	jne	.LBB2_324
.LBB2_325:                              # %.preheader.i639
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movq	88(%rsp), %r15          # 8-byte Reload
	jle	.LBB2_328
# BB#326:                               # %.lr.ph.preheader.i641
	movl	36(%rsp), %ebx          # 4-byte Reload
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB2_327:                              # %.lr.ph.i645
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rdi
	movq	(%rbp), %rsi
	movq	%r13, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %r14
	decq	%rbx
	jne	.LBB2_327
.LBB2_328:                              # %Atracking_localhom.exit
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	120(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	72(%rsp), %r14          # 8-byte Reload
	movl	496(%rsp), %ebx
	movq	80(%rsp), %r12          # 8-byte Reload
	jmp	.LBB2_401
.LBB2_329:
	movl	%r10d, %esi
	negl	%esi
	movq	(%r12,%r9,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movl	$1, %esi
	movapd	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB2_336
.LBB2_330:                              # %.lr.ph50.i.new
	movl	%r10d, %edi
	negl	%edi
	.p2align	4, 0x90
.LBB2_331:                              # =>This Inner Loop Header: Depth=1
	movq	%r15, %rax
	movss	(%rax,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB2_333
# BB#332:                               #   in Loop: Header=BB2_331 Depth=1
	leal	(%rdi,%rsi), %ebp
	movq	(%r12,%r9,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movapd	%xmm1, %xmm0
.LBB2_333:                              #   in Loop: Header=BB2_331 Depth=1
	movss	4(%rax,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB2_335
# BB#334:                               #   in Loop: Header=BB2_331 Depth=1
	leal	1(%rdi,%rsi), %ebp
	movq	(%r12,%r9,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movapd	%xmm1, %xmm0
.LBB2_335:                              #   in Loop: Header=BB2_331 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB2_331
.LBB2_336:                              # %.preheader7.i
	testl	%r11d, %r11d
	js	.LBB2_342
# BB#337:                               # %.lr.ph47.preheader.i
	movq	%r11, %rsi
	incq	%rsi
	movl	%esi, %eax
	leaq	-1(%rax), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB2_339
	.p2align	4, 0x90
.LBB2_338:                              # %.lr.ph47.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB2_338
.LBB2_339:                              # %.lr.ph47.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB2_342
# BB#340:                               # %.lr.ph47.preheader.i.new
	negq	%rax
	leaq	4(%rdx,%rax), %rax
	leaq	56(%r12,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_341:                              # %.lr.ph47.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%rax,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB2_341
.LBB2_342:                              # %.preheader6.i
	testl	%r10d, %r10d
	js	.LBB2_351
# BB#343:                               # %.lr.ph44.i
	movq	(%r12), %rcx
	movq	%r10, %rsi
	incq	%rsi
	movl	%esi, %eax
	cmpq	$7, %rax
	jbe	.LBB2_348
# BB#344:                               # %min.iters.checked923
	andl	$7, %esi
	movq	%rax, %rbp
	subq	%rsi, %rbp
	je	.LBB2_348
# BB#345:                               # %vector.body919.preheader
	leaq	16(%rcx), %rdi
	movdqa	.LCPI2_5(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI2_6(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI2_7(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB2_346:                              # %vector.body919
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdi)
	movdqu	%xmm4, (%rdi)
	paddd	%xmm3, %xmm0
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB2_346
# BB#347:                               # %middle.block920
	testq	%rsi, %rsi
	jne	.LBB2_349
	jmp	.LBB2_351
.LBB2_348:
	xorl	%ebp, %ebp
.LBB2_349:                              # %scalar.ph921.preheader
	movl	%ebp, %edx
	notl	%edx
	leaq	(%rcx,%rbp,4), %rcx
	subq	%rbp, %rax
	.p2align	4, 0x90
.LBB2_350:                              # %scalar.ph921
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rcx)
	decl	%edx
	addq	$4, %rcx
	decq	%rax
	jne	.LBB2_350
.LBB2_351:                              # %._crit_edge45.i
	movslq	%r11d, %rax
	movq	96(%rsp), %r15          # 8-byte Reload
	addq	%rax, %r15
	movslq	%r10d, %rcx
	movb	$0, (%rcx,%r15)
	addq	%rcx, %r15
	addq	%rax, %r8
	leaq	(%r8,%rcx), %r14
	movb	$0, (%rcx,%r8)
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	js	.LBB2_394
# BB#352:                               # %.lr.ph36.i.preheader
	xorl	%eax, %eax
	movdqa	.LCPI2_9(%rip), %xmm0   # xmm0 = [45,0,45,0,45,0,45,0,45,0,45,0,45,0,45,0]
	packuswb	%xmm0, %xmm0
	movdqa	.LCPI2_8(%rip), %xmm1   # xmm1 = [111,0,111,0,111,0,111,0,111,0,111,0,111,0,111,0]
	packuswb	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB2_353:                              # %.lr.ph36.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_367 Depth 2
                                        #     Child Loop BB2_371 Depth 2
                                        #     Child Loop BB2_374 Depth 2
                                        #     Child Loop BB2_382 Depth 2
                                        #     Child Loop BB2_386 Depth 2
                                        #     Child Loop BB2_389 Depth 2
	movslq	%r11d, %rcx
	movq	(%r12,%rcx,8), %rcx
	movslq	%r10d, %rdx
	movl	(%rcx,%rdx,4), %r9d
	testl	%r9d, %r9d
	js	.LBB2_356
# BB#354:                               #   in Loop: Header=BB2_353 Depth=1
	je	.LBB2_357
# BB#355:                               #   in Loop: Header=BB2_353 Depth=1
	movl	%r11d, %r8d
	subl	%r9d, %r8d
	jmp	.LBB2_358
	.p2align	4, 0x90
.LBB2_356:                              #   in Loop: Header=BB2_353 Depth=1
	leal	-1(%r11), %r8d
	jmp	.LBB2_359
	.p2align	4, 0x90
.LBB2_357:                              #   in Loop: Header=BB2_353 Depth=1
	leal	-1(%r11), %r8d
.LBB2_358:                              #   in Loop: Header=BB2_353 Depth=1
	movl	$-1, %r9d
.LBB2_359:                              #   in Loop: Header=BB2_353 Depth=1
	movl	%r11d, %esi
	subl	%r8d, %esi
	decl	%esi
	movq	%r10, 48(%rsp)          # 8-byte Spill
	movq	%r11, 56(%rsp)          # 8-byte Spill
	je	.LBB2_376
# BB#360:                               # %.lr.ph17.preheader.i
                                        #   in Loop: Header=BB2_353 Depth=1
	leal	-2(%r11), %ecx
	subl	%r8d, %ecx
	movq	%rcx, %r10
	negq	%r10
	leaq	1(%rcx), %rdx
	cmpq	$16, %rdx
	jae	.LBB2_362
# BB#361:                               #   in Loop: Header=BB2_353 Depth=1
	movq	%r14, %rcx
	movq	%r15, %rbp
	jmp	.LBB2_369
	.p2align	4, 0x90
.LBB2_362:                              # %min.iters.checked979
                                        #   in Loop: Header=BB2_353 Depth=1
	movl	%edx, %r11d
	andl	$15, %r11d
	subq	%r11, %rdx
	je	.LBB2_365
# BB#363:                               # %vector.memcheck992
                                        #   in Loop: Header=BB2_353 Depth=1
	leaq	-1(%r15,%r10), %rdi
	cmpq	%r14, %rdi
	jae	.LBB2_366
# BB#364:                               # %vector.memcheck992
                                        #   in Loop: Header=BB2_353 Depth=1
	leaq	-1(%r14,%r10), %rdi
	cmpq	%r15, %rdi
	jae	.LBB2_366
.LBB2_365:                              #   in Loop: Header=BB2_353 Depth=1
	movq	%r14, %rcx
	movq	%r15, %rbp
	movq	56(%rsp), %r11          # 8-byte Reload
	jmp	.LBB2_369
.LBB2_366:                              # %vector.body975.preheader
                                        #   in Loop: Header=BB2_353 Depth=1
	subl	%edx, %esi
	leaq	-1(%r11), %rbp
	subq	%rcx, %rbp
	leaq	(%r14,%rbp), %rcx
	addq	%r15, %rbp
	leaq	-8(%r15), %rdi
	leaq	-8(%r14), %rbx
	.p2align	4, 0x90
.LBB2_367:                              # %vector.body975
                                        #   Parent Loop BB2_353 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm1, (%rdi)
	movq	%xmm1, -8(%rdi)
	movq	%xmm0, (%rbx)
	movq	%xmm0, -8(%rbx)
	addq	$-16, %rdi
	addq	$-16, %rbx
	addq	$-16, %rdx
	jne	.LBB2_367
# BB#368:                               # %middle.block976
                                        #   in Loop: Header=BB2_353 Depth=1
	testq	%r11, %r11
	movq	56(%rsp), %r11          # 8-byte Reload
	je	.LBB2_375
	.p2align	4, 0x90
.LBB2_369:                              # %.lr.ph17.i.preheader
                                        #   in Loop: Header=BB2_353 Depth=1
	leal	-1(%rsi), %edx
	movl	%esi, %ebx
	andl	$7, %ebx
	je	.LBB2_372
# BB#370:                               # %.lr.ph17.i.prol.preheader
                                        #   in Loop: Header=BB2_353 Depth=1
	negl	%ebx
	.p2align	4, 0x90
.LBB2_371:                              # %.lr.ph17.i.prol
                                        #   Parent Loop BB2_353 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, -1(%rbp)
	decq	%rbp
	movb	$45, -1(%rcx)
	decq	%rcx
	decl	%esi
	incl	%ebx
	jne	.LBB2_371
.LBB2_372:                              # %.lr.ph17.i.prol.loopexit
                                        #   in Loop: Header=BB2_353 Depth=1
	cmpl	$7, %edx
	jb	.LBB2_375
# BB#373:                               # %.lr.ph17.i.preheader.new
                                        #   in Loop: Header=BB2_353 Depth=1
	decq	%rcx
	decq	%rbp
	.p2align	4, 0x90
.LBB2_374:                              # %.lr.ph17.i
                                        #   Parent Loop BB2_353 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, (%rbp)
	movb	$45, (%rcx)
	movb	$111, -1(%rbp)
	movb	$45, -1(%rcx)
	movb	$111, -2(%rbp)
	movb	$45, -2(%rcx)
	movb	$111, -3(%rbp)
	movb	$45, -3(%rcx)
	movb	$111, -4(%rbp)
	movb	$45, -4(%rcx)
	movb	$111, -5(%rbp)
	movb	$45, -5(%rcx)
	movb	$111, -6(%rbp)
	movb	$45, -6(%rcx)
	movb	$111, -7(%rbp)
	movb	$45, -7(%rcx)
	addq	$-8, %rcx
	addq	$-8, %rbp
	addl	$-8, %esi
	jne	.LBB2_374
.LBB2_375:                              # %._crit_edge18.loopexit.i
                                        #   in Loop: Header=BB2_353 Depth=1
	leaq	-1(%r15,%r10), %r15
	leaq	-1(%r14,%r10), %r14
	leal	-1(%rax,%r11), %eax
	subl	%r8d, %eax
	movq	48(%rsp), %r10          # 8-byte Reload
.LBB2_376:                              # %._crit_edge18.i
                                        #   in Loop: Header=BB2_353 Depth=1
	cmpl	$-1, %r9d
	je	.LBB2_391
# BB#377:                               # %.lr.ph25.preheader.i
                                        #   in Loop: Header=BB2_353 Depth=1
	movl	%r9d, %r12d
	notl	%r12d
	movl	$-2, %edi
	subl	%r9d, %edi
	movq	%rdi, %r11
	negq	%r11
	leaq	1(%rdi), %rdx
	cmpq	$16, %rdx
	movl	%r12d, %ecx
	movq	%r14, %rbp
	movq	%r15, %rbx
	jb	.LBB2_384
# BB#378:                               # %min.iters.checked941
                                        #   in Loop: Header=BB2_353 Depth=1
	movl	%edx, %r10d
	andl	$15, %r10d
	subq	%r10, %rdx
	movl	%r12d, %ecx
	movq	%r14, %rbp
	movq	%r15, %rbx
	je	.LBB2_384
# BB#379:                               # %vector.memcheck954
                                        #   in Loop: Header=BB2_353 Depth=1
	leaq	-1(%r15,%r11), %rcx
	cmpq	%r14, %rcx
	jae	.LBB2_381
# BB#380:                               # %vector.memcheck954
                                        #   in Loop: Header=BB2_353 Depth=1
	leaq	-1(%r14,%r11), %rcx
	cmpq	%r15, %rcx
	movl	%r12d, %ecx
	movq	%r14, %rbp
	movq	%r15, %rbx
	jb	.LBB2_384
.LBB2_381:                              # %vector.body937.preheader
                                        #   in Loop: Header=BB2_353 Depth=1
	movl	%r12d, %ecx
	subl	%edx, %ecx
	leaq	-1(%r10), %rbx
	subq	%rdi, %rbx
	leaq	(%r14,%rbx), %rbp
	addq	%r15, %rbx
	leaq	-8(%r15), %rdi
	leaq	-8(%r14), %rsi
	.p2align	4, 0x90
.LBB2_382:                              # %vector.body937
                                        #   Parent Loop BB2_353 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm0, (%rdi)
	movq	%xmm0, -8(%rdi)
	movq	%xmm1, (%rsi)
	movq	%xmm1, -8(%rsi)
	addq	$-16, %rdi
	addq	$-16, %rsi
	addq	$-16, %rdx
	jne	.LBB2_382
# BB#383:                               # %middle.block938
                                        #   in Loop: Header=BB2_353 Depth=1
	testq	%r10, %r10
	je	.LBB2_390
	.p2align	4, 0x90
.LBB2_384:                              # %.lr.ph25.i.preheader
                                        #   in Loop: Header=BB2_353 Depth=1
	leal	-1(%rcx), %edx
	movl	%ecx, %edi
	andl	$7, %edi
	je	.LBB2_387
# BB#385:                               # %.lr.ph25.i.prol.preheader
                                        #   in Loop: Header=BB2_353 Depth=1
	negl	%edi
	.p2align	4, 0x90
.LBB2_386:                              # %.lr.ph25.i.prol
                                        #   Parent Loop BB2_353 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, -1(%rbx)
	decq	%rbx
	movb	$111, -1(%rbp)
	decq	%rbp
	decl	%ecx
	incl	%edi
	jne	.LBB2_386
.LBB2_387:                              # %.lr.ph25.i.prol.loopexit
                                        #   in Loop: Header=BB2_353 Depth=1
	cmpl	$7, %edx
	jb	.LBB2_390
# BB#388:                               # %.lr.ph25.i.preheader.new
                                        #   in Loop: Header=BB2_353 Depth=1
	decq	%rbp
	decq	%rbx
	.p2align	4, 0x90
.LBB2_389:                              # %.lr.ph25.i
                                        #   Parent Loop BB2_353 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, (%rbx)
	movb	$111, (%rbp)
	movb	$45, -1(%rbx)
	movb	$111, -1(%rbp)
	movb	$45, -2(%rbx)
	movb	$111, -2(%rbp)
	movb	$45, -3(%rbx)
	movb	$111, -3(%rbp)
	movb	$45, -4(%rbx)
	movb	$111, -4(%rbp)
	movb	$45, -5(%rbx)
	movb	$111, -5(%rbp)
	movb	$45, -6(%rbx)
	movb	$111, -6(%rbp)
	movb	$45, -7(%rbx)
	movb	$111, -7(%rbp)
	addq	$-8, %rbp
	addq	$-8, %rbx
	addl	$-8, %ecx
	jne	.LBB2_389
.LBB2_390:                              # %._crit_edge26.loopexit.i
                                        #   in Loop: Header=BB2_353 Depth=1
	leaq	-1(%r15,%r11), %r15
	leaq	-1(%r14,%r11), %r14
	addl	%r12d, %eax
	movq	168(%rsp), %r12         # 8-byte Reload
	movq	48(%rsp), %r10          # 8-byte Reload
	movq	56(%rsp), %r11          # 8-byte Reload
.LBB2_391:                              # %._crit_edge26.i
                                        #   in Loop: Header=BB2_353 Depth=1
	testl	%r11d, %r11d
	jle	.LBB2_394
# BB#392:                               # %._crit_edge26.i
                                        #   in Loop: Header=BB2_353 Depth=1
	testl	%r10d, %r10d
	jle	.LBB2_394
# BB#393:                               #   in Loop: Header=BB2_353 Depth=1
	addl	%r9d, %r10d
	movb	$111, -1(%r15)
	decq	%r15
	movb	$111, -1(%r14)
	decq	%r14
	addl	$2, %eax
	cmpl	64(%rsp), %eax          # 4-byte Folded Reload
	movl	%r8d, %r11d
	jle	.LBB2_353
.LBB2_394:                              # %._crit_edge37.i
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	movq	80(%rsp), %r12          # 8-byte Reload
	jle	.LBB2_397
# BB#395:                               # %.lr.ph12.preheader.i
	movl	32(%rsp), %ebx          # 4-byte Reload
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB2_396:                              # %.lr.ph12.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	movq	(%rbp), %rsi
	movq	%r15, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %r13
	decq	%rbx
	jne	.LBB2_396
.LBB2_397:                              # %.preheader.i
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	160(%rsp), %r13         # 8-byte Reload
	jle	.LBB2_400
# BB#398:                               # %.lr.ph.preheader.i
	movl	36(%rsp), %ebx          # 4-byte Reload
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB2_399:                              # %.lr.ph.i532
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	movq	(%rbp), %rsi
	movq	%r14, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %r13
	decq	%rbx
	jne	.LBB2_399
.LBB2_400:                              # %Atracking.exit
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	120(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	72(%rsp), %r14          # 8-byte Reload
	movl	496(%rsp), %ebx
.LBB2_401:
	movq	H__align.mseq1(%rip), %rax
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %rcx
	cmpl	%ebx, %ecx
	jg	.LBB2_416
# BB#402:
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB2_416
.LBB2_403:                              # %.preheader648
	movl	32(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB2_406
# BB#404:                               # %.lr.ph671.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_405:                              # %.lr.ph671
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbp,8), %rdi
	movq	H__align.mseq1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB2_405
.LBB2_406:                              # %.preheader647
	movl	36(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB2_409
# BB#407:                               # %.lr.ph668.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_408:                              # %.lr.ph668
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbp,8), %rdi
	movq	H__align.mseq2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB2_408
.LBB2_409:                              # %._crit_edge669
	movq	stderr(%rip), %rdi
	movss	152(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.2, %esi
	movb	$1, %al
	callq	fprintf
	movq	40(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	js	.LBB2_412
# BB#410:                               # %.lr.ph666.preheader
	leal	1(%rax), %ebx
	movq	H__align.gappat1(%rip), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_411:                              # %.lr.ph666
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rbp,8), %rdi
	callq	free
	movq	H__align.gappat1(%rip), %rax
	movq	$0, (%rax,%rbp,8)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB2_411
.LBB2_412:                              # %.preheader
	testl	%r14d, %r14d
	js	.LBB2_415
# BB#413:                               # %.lr.ph.preheader
	leal	1(%r14), %ebx
	movq	H__align.gappat2(%rip), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_414:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rbp,8), %rdi
	callq	free
	movq	H__align.gappat2(%rip), %rax
	movq	$0, (%rax,%rbp,8)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB2_414
.LBB2_415:                              # %._crit_edge
	movss	152(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$440, %rsp              # imm = 0x1B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_416:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
	movl	%ebx, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.1, %edi
	callq	ErrorExit
	jmp	.LBB2_403
.Lfunc_end2:
	.size	H__align, .Lfunc_end2-H__align
	.cfi_endproc

	.p2align	4, 0x90
	.type	match_calc,@function
match_calc:                             # @match_calc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
.Lcfi30:
	.cfi_offset %rbx, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	40(%rsp), %r11
	cmpl	$0, 48(%rsp)
	je	.LBB3_10
# BB#1:
	testl	%r8d, %r8d
	jle	.LBB3_10
# BB#2:                                 # %.preheader77.preheader
	movl	%r8d, %r10d
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader77
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_4:                                #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rax,8), %rbx
	movss	(%rbx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB3_5
	jnp	.LBB3_6
.LBB3_5:                                #   in Loop: Header=BB3_4 Depth=2
	movq	(%r9,%r14,8), %rbx
	movslq	%r15d, %r15
	movss	%xmm1, (%rbx,%r15,4)
	movq	(%r11,%r14,8), %rbx
	movl	%eax, (%rbx,%r15,4)
	incl	%r15d
.LBB3_6:                                #   in Loop: Header=BB3_4 Depth=2
	movq	8(%rdx,%rax,8), %rbx
	movss	(%rbx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB3_7
	jnp	.LBB3_8
.LBB3_7:                                #   in Loop: Header=BB3_4 Depth=2
	movq	(%r9,%r14,8), %rbx
	movslq	%r15d, %r15
	movss	%xmm1, (%rbx,%r15,4)
	movq	(%r11,%r14,8), %rbx
	leal	1(%rax), %ebp
	movl	%ebp, (%rbx,%r15,4)
	incl	%r15d
.LBB3_8:                                #   in Loop: Header=BB3_4 Depth=2
	addq	$2, %rax
	cmpq	$26, %rax
	jne	.LBB3_4
# BB#9:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	(%r11,%r14,8), %rax
	movslq	%r15d, %rbx
	movl	$-1, (%rax,%rbx,4)
	incq	%r14
	cmpq	%r10, %r14
	jne	.LBB3_3
.LBB3_10:                               # %.preheader76
	movl	$n_dis, %r10d
	movslq	%ecx, %rcx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_12 Depth 2
	movl	$0, -104(%rsp,%r14,4)
	xorps	%xmm0, %xmm0
	movl	$1, %ebx
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB3_12:                               #   Parent Loop BB3_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	(%rax), %xmm1
	movq	-8(%rsi,%rbx,8), %rdx
	mulss	(%rdx,%rcx,4), %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	104(%rax), %xmm0
	movq	(%rsi,%rbx,8), %rdx
	mulss	(%rdx,%rcx,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rax
	addq	$2, %rbx
	cmpq	$27, %rbx
	jne	.LBB3_12
# BB#13:                                #   in Loop: Header=BB3_11 Depth=1
	movss	%xmm0, -104(%rsp,%r14,4)
	incq	%r14
	addq	$4, %r10
	cmpq	$26, %r14
	jne	.LBB3_11
	jmp	.LBB3_14
	.p2align	4, 0x90
.LBB3_18:                               # %._crit_edge
                                        #   in Loop: Header=BB3_14 Depth=1
	addq	$8, %r11
	addq	$8, %r9
	addq	$4, %rdi
.LBB3_14:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_17 Depth 2
	testl	%r8d, %r8d
	je	.LBB3_19
# BB#15:                                # %.lr.ph84
                                        #   in Loop: Header=BB3_14 Depth=1
	decl	%r8d
	movl	$0, (%rdi)
	movq	(%r11), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	js	.LBB3_18
# BB#16:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB3_14 Depth=1
	movq	(%r9), %rcx
	addq	$4, %rax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB3_17:                               # %.lr.ph
                                        #   Parent Loop BB3_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	movss	-104(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addq	$4, %rcx
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdi)
	movl	(%rax), %edx
	addq	$4, %rax
	testl	%edx, %edx
	jns	.LBB3_17
	jmp	.LBB3_18
.LBB3_19:                               # %._crit_edge85
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	match_calc, .Lfunc_end3-match_calc
	.cfi_endproc

	.type	impmtx,@object          # @impmtx
	.local	impmtx
	.comm	impmtx,8,8
	.type	imp_match_init_strictH.impalloclen,@object # @imp_match_init_strictH.impalloclen
	.local	imp_match_init_strictH.impalloclen
	.comm	imp_match_init_strictH.impalloclen,4,4
	.type	imp_match_init_strictH.nocount1,@object # @imp_match_init_strictH.nocount1
	.local	imp_match_init_strictH.nocount1
	.comm	imp_match_init_strictH.nocount1,8,8
	.type	imp_match_init_strictH.nocount2,@object # @imp_match_init_strictH.nocount2
	.local	imp_match_init_strictH.nocount2
	.comm	imp_match_init_strictH.nocount2,8,8
	.type	H__align.mi,@object     # @H__align.mi
	.local	H__align.mi
	.comm	H__align.mi,4,4
	.type	H__align.m,@object      # @H__align.m
	.local	H__align.m
	.comm	H__align.m,8,8
	.type	H__align.ijp,@object    # @H__align.ijp
	.local	H__align.ijp
	.comm	H__align.ijp,8,8
	.type	H__align.mpi,@object    # @H__align.mpi
	.local	H__align.mpi
	.comm	H__align.mpi,4,4
	.type	H__align.mp,@object     # @H__align.mp
	.local	H__align.mp
	.comm	H__align.mp,8,8
	.type	H__align.w1,@object     # @H__align.w1
	.local	H__align.w1
	.comm	H__align.w1,8,8
	.type	H__align.w2,@object     # @H__align.w2
	.local	H__align.w2
	.comm	H__align.w2,8,8
	.type	H__align.match,@object  # @H__align.match
	.local	H__align.match
	.comm	H__align.match,8,8
	.type	H__align.initverticalw,@object # @H__align.initverticalw
	.local	H__align.initverticalw
	.comm	H__align.initverticalw,8,8
	.type	H__align.lastverticalw,@object # @H__align.lastverticalw
	.local	H__align.lastverticalw
	.comm	H__align.lastverticalw,8,8
	.type	H__align.mseq1,@object  # @H__align.mseq1
	.local	H__align.mseq1
	.comm	H__align.mseq1,8,8
	.type	H__align.mseq2,@object  # @H__align.mseq2
	.local	H__align.mseq2
	.comm	H__align.mseq2,8,8
	.type	H__align.mseq,@object   # @H__align.mseq
	.local	H__align.mseq
	.comm	H__align.mseq,8,8
	.type	H__align.gappat1,@object # @H__align.gappat1
	.local	H__align.gappat1
	.comm	H__align.gappat1,8,8
	.type	H__align.gappat2,@object # @H__align.gappat2
	.local	H__align.gappat2
	.comm	H__align.gappat2,8,8
	.type	H__align.digf1,@object  # @H__align.digf1
	.local	H__align.digf1
	.comm	H__align.digf1,8,8
	.type	H__align.digf2,@object  # @H__align.digf2
	.local	H__align.digf2
	.comm	H__align.digf2,8,8
	.type	H__align.diaf1,@object  # @H__align.diaf1
	.local	H__align.diaf1
	.comm	H__align.diaf1,8,8
	.type	H__align.diaf2,@object  # @H__align.diaf2
	.local	H__align.diaf2
	.comm	H__align.diaf2,8,8
	.type	H__align.gapz1,@object  # @H__align.gapz1
	.local	H__align.gapz1
	.comm	H__align.gapz1,8,8
	.type	H__align.gapz2,@object  # @H__align.gapz2
	.local	H__align.gapz2
	.comm	H__align.gapz2,8,8
	.type	H__align.gapf1,@object  # @H__align.gapf1
	.local	H__align.gapf1
	.comm	H__align.gapf1,8,8
	.type	H__align.gapf2,@object  # @H__align.gapf2
	.local	H__align.gapf2
	.comm	H__align.gapf2,8,8
	.type	H__align.ogcp1g,@object # @H__align.ogcp1g
	.local	H__align.ogcp1g
	.comm	H__align.ogcp1g,8,8
	.type	H__align.ogcp2g,@object # @H__align.ogcp2g
	.local	H__align.ogcp2g
	.comm	H__align.ogcp2g,8,8
	.type	H__align.fgcp1g,@object # @H__align.fgcp1g
	.local	H__align.fgcp1g
	.comm	H__align.fgcp1g,8,8
	.type	H__align.fgcp2g,@object # @H__align.fgcp2g
	.local	H__align.fgcp2g
	.comm	H__align.fgcp2g,8,8
	.type	H__align.ogcp1,@object  # @H__align.ogcp1
	.local	H__align.ogcp1
	.comm	H__align.ogcp1,8,8
	.type	H__align.ogcp2,@object  # @H__align.ogcp2
	.local	H__align.ogcp2
	.comm	H__align.ogcp2,8,8
	.type	H__align.fgcp1,@object  # @H__align.fgcp1
	.local	H__align.fgcp1
	.comm	H__align.fgcp1,8,8
	.type	H__align.fgcp2,@object  # @H__align.fgcp2
	.local	H__align.fgcp2
	.comm	H__align.fgcp2,8,8
	.type	H__align.cpmx1,@object  # @H__align.cpmx1
	.local	H__align.cpmx1
	.comm	H__align.cpmx1,8,8
	.type	H__align.cpmx2,@object  # @H__align.cpmx2
	.local	H__align.cpmx2
	.comm	H__align.cpmx2,8,8
	.type	H__align.intwork,@object # @H__align.intwork
	.local	H__align.intwork
	.comm	H__align.intwork,8,8
	.type	H__align.floatwork,@object # @H__align.floatwork
	.local	H__align.floatwork
	.comm	H__align.floatwork,8,8
	.type	H__align.orlgth1,@object # @H__align.orlgth1
	.local	H__align.orlgth1
	.comm	H__align.orlgth1,4,4
	.type	H__align.orlgth2,@object # @H__align.orlgth2
	.local	H__align.orlgth2
	.comm	H__align.orlgth2,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"alloclen=%d, resultlen=%d, N=%d\n"
	.size	.L.str, 33

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"LENGTH OVER!\n"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"wm = %f\n"
	.size	.L.str.2, 9


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
