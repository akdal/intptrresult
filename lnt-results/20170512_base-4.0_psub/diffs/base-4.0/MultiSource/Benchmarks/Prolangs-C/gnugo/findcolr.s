	.text
	.file	"findcolr.bc"
	.globl	findcolor
	.p2align	4, 0x90
	.type	findcolor,@function
findcolor:                              # @findcolor
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	movslq	%edi, %rdi
	imulq	$19, %rdi, %r8
	leaq	p-19(%r8,%rax), %rdx
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %ecx
	cmpq	$2, %rsi
	jl	.LBB0_3
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	decq	%rsi
	addq	$-19, %rdx
	testb	%cl, %cl
	je	.LBB0_1
.LBB0_3:                                # %.preheader55.preheader
	incq	%rdi
	imulq	$19, %rdi, %rdx
	leaq	p(%rdx,%rax), %rsi
	.p2align	4, 0x90
.LBB0_4:                                # %.preheader55
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %edx
	testb	%dl, %dl
	jne	.LBB0_6
# BB#5:                                 # %.preheader55
                                        #   in Loop: Header=BB0_4 Depth=1
	addq	$19, %rsi
	cmpq	$18, %rdi
	leaq	1(%rdi), %rdi
	jl	.LBB0_4
.LBB0_6:
	testb	%cl, %cl
	je	.LBB0_7
.LBB0_15:
	testb	%dl, %dl
	movb	%cl, %al
	je	.LBB0_17
# BB#16:
	xorl	%eax, %eax
.LBB0_17:
	cmpb	%dl, %cl
	je	.LBB0_19
# BB#18:
	movl	%eax, %ecx
.LBB0_19:
	movl	%ecx, %edx
	movzbl	%dl, %eax
	retq
.LBB0_7:
	testb	%dl, %dl
	jne	.LBB0_20
# BB#8:                                 # %.preheader54.preheader
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB0_9:                                # %.preheader54
                                        # =>This Inner Loop Header: Depth=1
	movzbl	p-1(%r8,%rdx), %ecx
	cmpq	$2, %rdx
	jl	.LBB0_11
# BB#10:                                # %.preheader54
                                        #   in Loop: Header=BB0_9 Depth=1
	decq	%rdx
	testb	%cl, %cl
	je	.LBB0_9
.LBB0_11:                               # %.preheader.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB0_12:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	p(%r8,%rax), %edx
	testb	%dl, %dl
	jne	.LBB0_14
# BB#13:                                # %.preheader
                                        #   in Loop: Header=BB0_12 Depth=1
	cmpq	$18, %rax
	leaq	1(%rax), %rax
	jl	.LBB0_12
.LBB0_14:
	testb	%cl, %cl
	jne	.LBB0_15
.LBB0_20:
	movzbl	%dl, %eax
	retq
.Lfunc_end0:
	.size	findcolor, .Lfunc_end0-findcolor
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
