	.text
	.file	"store.bc"
	.globl	Write_Frame
	.p2align	4, 0x90
	.type	Write_Frame,@function
Write_Frame:                            # @Write_Frame
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$256, %rsp              # imm = 0x100
.Lcfi3:
	.cfi_def_cfa_offset 288
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	progressive_frame(%rip), %eax
	orl	progressive_sequence(%rip), %eax
	orl	Frame_Store_Flag(%rip), %eax
	movq	Output_Picture_Filename(%rip), %rsi
	movq	%rsp, %r14
	je	.LBB0_2
# BB#1:
	movl	$102, %ecx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	sprintf
	movl	Coded_Picture_Width(%rip), %ecx
	movl	vertical_size(%rip), %r8d
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	jmp	.LBB0_3
.LBB0_2:
	movl	$97, %ecx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	sprintf
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movl	vertical_size(%rip), %r8d
	sarl	%r8d
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	store_one
	movq	Output_Picture_Filename(%rip), %rsi
	movl	$98, %ecx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	sprintf
	movl	Coded_Picture_Width(%rip), %edx
	leal	(%rdx,%rdx), %ecx
	movl	vertical_size(%rip), %r8d
	sarl	%r8d
	movq	%r14, %rdi
	movq	%rbx, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
.LBB0_3:
	callq	store_one
	addq	$256, %rsp              # imm = 0x100
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Write_Frame, .Lfunc_end0-Write_Frame
	.cfi_endproc

	.p2align	4, 0x90
	.type	store_one,@function
store_one:                              # @store_one
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi13:
	.cfi_def_cfa_offset 368
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movl	%ecx, %ebx
	movl	%edx, %r12d
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %r15
	movl	Output_Type(%rip), %eax
	cmpq	$3, %rax
	ja	.LBB1_5
# BB#1:
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_2:
	movl	horizontal_size(%rip), %r13d
	leaq	48(%rsp), %r14
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	sprintf
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rsi
	movq	%r14, %rdi
	movl	%r12d, %edx
	movl	%ebx, %ecx
	movl	%r13d, %r8d
	movl	%ebp, %r9d
	callq	store_yuv1
	movl	chroma_format(%rip), %eax
	cmpl	$3, %eax
	je	.LBB1_4
# BB#3:
	sarl	%r12d
	sarl	%ebx
	sarl	%r13d
.LBB1_4:                                # %store_yuv.exit
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	movl	%r12d, %r13d
	movl	%ebx, %r12d
	cmpl	$1, %eax
	sete	%cl
	sarl	%cl, %ebp
	leaq	48(%rsp), %r14
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	sprintf
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	movl	%r13d, %edx
	movl	%r12d, %ecx
	movl	8(%rsp), %r14d          # 4-byte Reload
	movl	%r14d, %r8d
	movl	%ebp, %r9d
	callq	store_yuv1
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	leaq	48(%rsp), %rdi
	movq	%r15, %rdx
	callq	sprintf
	movq	16(%rbx), %rsi
	leaq	48(%rsp), %rdi
	movl	%r13d, %edx
	movl	%r12d, %ecx
	movl	%r14d, %r8d
	movl	%ebp, %r9d
	callq	store_yuv1
.LBB1_5:
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_6:
	movl	chroma_format(%rip), %eax
	cmpl	$3, %eax
	jne	.LBB1_8
# BB#7:
	movl	$.L.str.5, %edi
	callq	Error
	movl	chroma_format(%rip), %eax
.LBB1_8:
	cmpl	$2, %eax
	jne	.LBB1_10
# BB#9:
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	8(%rcx), %rax
	movq	%rax, store_sif.u422(%rip)
	movq	16(%rcx), %rax
	movq	%rax, store_sif.v422(%rip)
	jmp	.LBB1_16
.LBB1_38:
	movl	$1, %r9d
	jmp	.LBB1_39
.LBB1_40:
	xorl	%r9d, %r9d
.LBB1_39:
	movq	%r15, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%r12d, %edx
	movl	%ebx, %ecx
	movl	%ebp, %r8d
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	store_ppm_tga           # TAILCALL
.LBB1_10:
	movl	%ebp, %r13d
	movl	%ebx, %ebp
	cmpq	$0, store_sif.u422(%rip)
	jne	.LBB1_15
# BB#11:
	movl	Coded_Picture_Width(%rip), %ebx
	movl	%ebx, %eax
	sarl	%eax
	movl	Coded_Picture_Height(%rip), %r14d
	imull	%r14d, %eax
	movslq	%eax, %rdi
	callq	malloc
	movq	%rax, store_sif.u422(%rip)
	testq	%rax, %rax
	jne	.LBB1_13
# BB#12:
	movl	$.L.str.6, %edi
	callq	Error
	movl	Coded_Picture_Width(%rip), %ebx
	movl	Coded_Picture_Height(%rip), %r14d
.LBB1_13:
	sarl	%ebx
	imull	%r14d, %ebx
	movslq	%ebx, %rdi
	callq	malloc
	movq	%rax, store_sif.v422(%rip)
	testq	%rax, %rax
	jne	.LBB1_15
# BB#14:
	movl	$.L.str.6, %edi
	callq	Error
.LBB1_15:
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	8(%rbx), %rdi
	movq	store_sif.u422(%rip), %rsi
	callq	conv420to422
	movq	16(%rbx), %rdi
	movq	store_sif.v422(%rip), %rsi
	callq	conv420to422
	movl	%ebp, %ebx
	movl	%r13d, %ebp
.LBB1_16:
	movq	%r15, %rdi
	callq	strlen
	movb	$0, 4(%r15,%rax)
	movl	$1179210542, (%r15,%rax) # imm = 0x4649532E
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB1_18
# BB#17:
	movq	stdout(%rip), %r14
	movl	$47, %esi
	movq	%r15, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rdx
	cmoveq	%r15, %rdx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
.LBB1_18:
	movl	$577, %esi              # imm = 0x241
	movl	$438, %edx              # imm = 0x1B6
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	open
	movl	%eax, outfile(%rip)
	cmpl	$-1, %eax
	jne	.LBB1_20
# BB#19:
	movl	$Error_Text, %edi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	sprintf
	movl	$Error_Text, %edi
	callq	Error
.LBB1_20:
	movq	$obfr, optr(%rip)
	testl	%ebp, %ebp
	jle	.LBB1_37
# BB#21:                                # %.lr.ph48.i
	movslq	%r12d, %rcx
	sarl	%r12d
	movslq	%r12d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	%ebx, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	sarl	%ebx
	movslq	%ebx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	horizontal_size(%rip), %eax
	movl	$obfr, %edx
	movl	%ebp, %esi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movl	$obfr+4096, %r12d
	.p2align	4, 0x90
.LBB1_22:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_24 Depth 2
	testl	%eax, %eax
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jle	.LBB1_34
# BB#23:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB1_22 Depth=1
	movq	%r15, %rax
	imulq	16(%rsp), %rax          # 8-byte Folded Reload
	movq	store_sif.v422(%rip), %r14
	movq	24(%rsp), %rsi          # 8-byte Reload
	addq	%rsi, %r14
	addq	%rax, %r14
	movq	store_sif.u422(%rip), %r13
	addq	%rsi, %r13
	addq	%rax, %r13
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rbx
	addq	%rcx, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_24:                               # %.lr.ph.i
                                        #   Parent Loop BB1_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r13), %eax
	leaq	1(%rdx), %rcx
	movq	%rcx, optr(%rip)
	movb	%al, (%rdx)
	cmpq	%r12, %rcx
	je	.LBB1_26
# BB#25:                                # %.lr.ph.putbyte.exit_crit_edge.i
                                        #   in Loop: Header=BB1_24 Depth=2
	movq	optr(%rip), %rcx
	jmp	.LBB1_27
	.p2align	4, 0x90
.LBB1_26:                               #   in Loop: Header=BB1_24 Depth=2
	movl	outfile(%rip), %edi
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
	movl	$obfr, %ecx
.LBB1_27:                               # %putbyte.exit.i
                                        #   in Loop: Header=BB1_24 Depth=2
	movzbl	(%rbx,%rbp), %edx
	leaq	1(%rcx), %rax
	movq	%rax, optr(%rip)
	movb	%dl, (%rcx)
	cmpq	%r12, %rax
	jne	.LBB1_29
# BB#28:                                #   in Loop: Header=BB1_24 Depth=2
	movl	outfile(%rip), %edi
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
	movl	$obfr, %eax
.LBB1_29:                               # %putbyte.exit39.i
                                        #   in Loop: Header=BB1_24 Depth=2
	movzbl	(%r14), %edx
	leaq	1(%rax), %rcx
	movq	%rcx, optr(%rip)
	movb	%dl, (%rax)
	cmpq	%r12, %rcx
	jne	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_24 Depth=2
	movl	outfile(%rip), %edi
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
	movl	$obfr, %ecx
.LBB1_31:                               # %putbyte.exit40.i
                                        #   in Loop: Header=BB1_24 Depth=2
	movzbl	1(%rbx,%rbp), %eax
	leaq	1(%rcx), %rdx
	movq	%rdx, optr(%rip)
	movb	%al, (%rcx)
	cmpq	%r12, %rdx
	jne	.LBB1_33
# BB#32:                                #   in Loop: Header=BB1_24 Depth=2
	movl	outfile(%rip), %edi
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
	movl	$obfr, %edx
.LBB1_33:                               # %putbyte.exit41.i
                                        #   in Loop: Header=BB1_24 Depth=2
	incq	%r13
	incq	%r14
	movl	horizontal_size(%rip), %eax
	addq	$2, %rbp
	cmpl	%eax, %ebp
	jl	.LBB1_24
.LBB1_34:                               # %._crit_edge.i
                                        #   in Loop: Header=BB1_22 Depth=1
	incq	%r15
	movq	8(%rsp), %rcx           # 8-byte Reload
	addq	40(%rsp), %rcx          # 8-byte Folded Reload
	cmpq	32(%rsp), %r15          # 8-byte Folded Reload
	jne	.LBB1_22
# BB#35:                                # %._crit_edge49.i
	movl	$obfr, %eax
	cmpq	%rax, %rdx
	je	.LBB1_37
# BB#36:
	movl	outfile(%rip), %edi
	movl	$obfr, %eax
	subq	%rax, %rdx
	movl	$obfr, %esi
	callq	write
.LBB1_37:                               # %store_sif.exit
	movl	outfile(%rip), %edi
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	close                   # TAILCALL
.Lfunc_end1:
	.size	store_one, .Lfunc_end1-store_one
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_2
	.quad	.LBB1_6
	.quad	.LBB1_38
	.quad	.LBB1_40

	.text
	.p2align	4, 0x90
	.type	store_ppm_tga,@function
store_ppm_tga:                          # @store_ppm_tga
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$344, %rsp              # imm = 0x158
.Lcfi26:
	.cfi_def_cfa_offset 400
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	movl	%r8d, 8(%rsp)           # 4-byte Spill
	movl	%ecx, %r14d
	movl	%edx, %r13d
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movl	chroma_format(%rip), %eax
	cmpl	$3, %eax
	jne	.LBB2_2
# BB#1:
	movq	8(%r12), %rax
	movq	%rax, store_ppm_tga.u444(%rip)
	movq	16(%r12), %rax
	movq	%rax, store_ppm_tga.v444(%rip)
	jmp	.LBB2_16
.LBB2_2:
	cmpq	$0, store_ppm_tga.u444(%rip)
	jne	.LBB2_12
# BB#3:
	movl	%r14d, %r15d
	cmpl	$1, %eax
	jne	.LBB2_8
# BB#4:
	movl	Coded_Picture_Width(%rip), %ebx
	movl	%ebx, %eax
	sarl	%eax
	movl	Coded_Picture_Height(%rip), %r14d
	imull	%r14d, %eax
	movslq	%eax, %rdi
	callq	malloc
	movq	%rax, store_ppm_tga.u422(%rip)
	testq	%rax, %rax
	jne	.LBB2_6
# BB#5:
	movl	$.L.str.6, %edi
	callq	Error
	movl	Coded_Picture_Width(%rip), %ebx
	movl	Coded_Picture_Height(%rip), %r14d
.LBB2_6:
	sarl	%ebx
	imull	%r14d, %ebx
	movslq	%ebx, %rdi
	callq	malloc
	movq	%rax, store_ppm_tga.v422(%rip)
	testq	%rax, %rax
	jne	.LBB2_8
# BB#7:
	movl	$.L.str.6, %edi
	callq	Error
.LBB2_8:
	movl	Coded_Picture_Width(%rip), %r14d
	movl	Coded_Picture_Height(%rip), %ebx
	movl	%ebx, %eax
	imull	%r14d, %eax
	movslq	%eax, %rdi
	callq	malloc
	movq	%rax, store_ppm_tga.u444(%rip)
	testq	%rax, %rax
	jne	.LBB2_10
# BB#9:
	movl	$.L.str.6, %edi
	callq	Error
	movl	Coded_Picture_Width(%rip), %r14d
	movl	Coded_Picture_Height(%rip), %ebx
.LBB2_10:
	imull	%r14d, %ebx
	movslq	%ebx, %rdi
	callq	malloc
	movq	%rax, store_ppm_tga.v444(%rip)
	testq	%rax, %rax
	movl	%r15d, %r14d
	jne	.LBB2_12
# BB#11:
	movl	$.L.str.6, %edi
	callq	Error
.LBB2_12:
	movq	8(%r12), %rdi
	cmpl	$1, chroma_format(%rip)
	jne	.LBB2_14
# BB#13:
	movl	%r13d, %ebx
	movq	store_ppm_tga.u422(%rip), %r13
	movq	%r13, %rsi
	callq	conv420to422
	movq	16(%r12), %rdi
	movq	store_ppm_tga.v422(%rip), %r15
	movq	%r15, %rsi
	callq	conv420to422
	movq	store_ppm_tga.u444(%rip), %rsi
	movq	%r13, %rdi
	movl	%ebx, %r13d
	callq	conv422to444
	movq	store_ppm_tga.v444(%rip), %rsi
	movq	%r15, %rdi
	jmp	.LBB2_15
.LBB2_14:
	movq	store_ppm_tga.u444(%rip), %rsi
	callq	conv422to444
	movq	16(%r12), %rdi
	movq	store_ppm_tga.v444(%rip), %rsi
.LBB2_15:
	callq	conv422to444
.LBB2_16:
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	movl	$.L.str.8, %eax
	movl	$.L.str.9, %ebx
	cmovneq	%rax, %rbx
	movq	%rbp, %rdi
	callq	strlen
	movb	4(%rbx), %cl
	movb	%cl, 4(%rbp,%rax)
	movl	(%rbx), %ecx
	movl	%ecx, (%rbp,%rax)
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB2_18
# BB#17:
	movq	stdout(%rip), %r15
	movl	$47, %esi
	movq	%rbp, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rdx
	cmoveq	%rbp, %rdx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
.LBB2_18:
	movl	$577, %esi              # imm = 0x241
	movl	$438, %edx              # imm = 0x1B6
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, outfile(%rip)
	cmpl	$-1, %eax
	jne	.LBB2_20
# BB#19:
	movl	$Error_Text, %edi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	sprintf
	movl	$Error_Text, %edi
	callq	Error
.LBB2_20:
	movq	$obfr, optr(%rip)
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB2_37
# BB#21:                                # %.preheader.preheader
	movl	$obfr, %eax
	movq	$-12, %rbx
	movl	$obfr+4096, %ebp
	.p2align	4, 0x90
.LBB2_22:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	store_ppm_tga.tga24+12(%rbx), %ecx
	movb	%cl, (%rax)
	incq	%rax
	movq	%rax, optr(%rip)
	cmpq	%rbp, %rax
	jne	.LBB2_24
# BB#23:                                #   in Loop: Header=BB2_22 Depth=1
	movl	outfile(%rip), %edi
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
	movl	$obfr, %eax
.LBB2_24:                               # %putbyte.exit
                                        #   in Loop: Header=BB2_22 Depth=1
	incq	%rbx
	jne	.LBB2_22
# BB#25:
	movl	horizontal_size(%rip), %ecx
	leaq	1(%rax), %rbx
	movq	%rbx, optr(%rip)
	movb	%cl, (%rax)
	movl	$obfr+4096, %eax
	cmpq	%rax, %rbx
	jne	.LBB2_27
# BB#26:
	movl	outfile(%rip), %edi
	movl	$obfr, %ebx
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	movl	%ecx, %ebp
	callq	write
	movl	%ebp, %ecx
	movq	$obfr, optr(%rip)
.LBB2_27:                               # %putbyte.exit.i
	leaq	1(%rbx), %rbp
	movq	%rbp, optr(%rip)
	movb	%ch, (%rbx)  # NOREX
	movl	$obfr+4096, %eax
	cmpq	%rax, %rbp
	jne	.LBB2_29
# BB#28:
	movl	outfile(%rip), %edi
	movl	$obfr, %ebp
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
.LBB2_29:                               # %putword.exit
	leaq	1(%rbp), %rbx
	movq	%rbx, optr(%rip)
	movl	8(%rsp), %ecx           # 4-byte Reload
	movb	%cl, (%rbp)
	movl	$obfr+4096, %eax
	cmpq	%rax, %rbx
	jne	.LBB2_31
# BB#30:
	movl	outfile(%rip), %edi
	movl	$obfr, %ebx
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movl	8(%rsp), %ecx           # 4-byte Reload
	movq	$obfr, optr(%rip)
.LBB2_31:                               # %putbyte.exit.i87
	leaq	1(%rbx), %rbp
	movq	%rbp, optr(%rip)
	movb	%ch, (%rbx)  # NOREX
	movl	$obfr+4096, %eax
	cmpq	%rax, %rbp
	jne	.LBB2_33
# BB#32:
	movl	outfile(%rip), %edi
	movl	$obfr, %ebp
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
.LBB2_33:                               # %putword.exit88
	leaq	1(%rbp), %rbx
	movq	%rbx, optr(%rip)
	movb	$24, (%rbp)
	movl	$obfr+4096, %eax
	cmpq	%rax, %rbx
	jne	.LBB2_35
# BB#34:
	movl	outfile(%rip), %edi
	movl	$obfr, %ebx
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
.LBB2_35:                               # %putbyte.exit89
	leaq	1(%rbx), %rax
	movq	%rax, optr(%rip)
	movb	$32, (%rbx)
	movl	$obfr+4096, %ecx
	cmpq	%rcx, %rax
	jne	.LBB2_42
# BB#36:
	movl	outfile(%rip), %edi
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
	jmp	.LBB2_42
.LBB2_37:
	movl	horizontal_size(%rip), %edx
	leaq	80(%rsp), %rdi
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movl	8(%rsp), %ecx           # 4-byte Reload
	callq	sprintf
	movb	80(%rsp), %al
	testb	%al, %al
	je	.LBB2_42
# BB#38:                                # %.lr.ph110.preheader
	movq	optr(%rip), %rcx
	leaq	81(%rsp), %rbx
	movl	$obfr+4096, %ebp
	.p2align	4, 0x90
.LBB2_39:                               # %.lr.ph110
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, optr(%rip)
	movb	%al, (%rcx)
	cmpq	%rbp, %rdx
	jne	.LBB2_41
# BB#40:                                #   in Loop: Header=BB2_39 Depth=1
	movl	outfile(%rip), %edi
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
	movl	$obfr, %edx
.LBB2_41:                               # %putbyte.exit91
                                        #   in Loop: Header=BB2_39 Depth=1
	movzbl	(%rbx), %eax
	incq	%rbx
	testb	%al, %al
	movq	%rdx, %rcx
	jne	.LBB2_39
.LBB2_42:                               # %putbyte.exit90
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movl	8(%rsp), %ecx           # 4-byte Reload
	testl	%ecx, %ecx
	jle	.LBB2_63
# BB#43:                                # %.lr.ph105
	movslq	matrix_coefficients(%rip), %rax
	shlq	$4, %rax
	movl	Inverse_Table_6_9(%rax), %edx
	movl	%edx, 28(%rsp)          # 4-byte Spill
	movl	Inverse_Table_6_9+4(%rax), %edx
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movl	Inverse_Table_6_9+8(%rax), %edx
	movl	%edx, 20(%rsp)          # 4-byte Spill
	movl	Inverse_Table_6_9+12(%rax), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movslq	%r13d, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movslq	%r14d, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%ecx, %eax
	movl	horizontal_size(%rip), %ecx
	movl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	movl	$obfr+4096, %r12d
	.p2align	4, 0x90
.LBB2_44:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_46 Depth 2
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	jle	.LBB2_62
# BB#45:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_44 Depth=1
	movq	optr(%rip), %rax
	movq	store_ppm_tga.v444(%rip), %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	addq	%rdx, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	store_ppm_tga.u444(%rip), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %r13
	addq	%rdx, %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_46:                               # %.lr.ph
                                        #   Parent Loop BB2_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	72(%rsp), %rcx          # 8-byte Reload
	movzbl	(%rcx,%rbp), %ecx
	addl	$-128, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movzbl	(%rdx,%rbp), %edx
	addl	$-128, %edx
	movl	%edx, %esi
	movl	%ecx, %edi
	imull	20(%rsp), %edi          # 4-byte Folded Reload
	imull	16(%rsp), %edx          # 4-byte Folded Reload
	addl	%edi, %edx
	movzbl	(%r13,%rbp), %edi
	imull	$76309, %edi, %edi      # imm = 0x12A15
	imull	28(%rsp), %esi          # 4-byte Folded Reload
	leal	-1188176(%rsi,%rdi), %esi
	negl	%edx
	leal	-1188176(%rdi,%rdx), %edx
	imull	24(%rsp), %ecx          # 4-byte Folded Reload
	leal	-1188158(%rcx,%rdi), %ecx
	movq	Clip(%rip), %rdi
	sarl	$16, %esi
	movslq	%esi, %rsi
	movzbl	(%rdi,%rsi), %r14d
	sarl	$16, %edx
	movslq	%edx, %rdx
	movzbl	(%rdi,%rdx), %ebx
	sarl	$16, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rdi,%rcx), %r15d
	leaq	1(%rax), %rcx
	movq	%rcx, optr(%rip)
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB2_53
# BB#47:                                #   in Loop: Header=BB2_46 Depth=2
	movb	%r15b, (%rax)
	cmpq	%r12, %rcx
	je	.LBB2_49
# BB#48:                                # %.putbyte.exit92_crit_edge
                                        #   in Loop: Header=BB2_46 Depth=2
	movq	optr(%rip), %rax
	jmp	.LBB2_50
	.p2align	4, 0x90
.LBB2_53:                               #   in Loop: Header=BB2_46 Depth=2
	movb	%r14b, (%rax)
	cmpq	%r12, %rcx
	je	.LBB2_55
# BB#54:                                # %.putbyte.exit95_crit_edge
                                        #   in Loop: Header=BB2_46 Depth=2
	movq	optr(%rip), %rax
	jmp	.LBB2_56
	.p2align	4, 0x90
.LBB2_49:                               #   in Loop: Header=BB2_46 Depth=2
	movl	outfile(%rip), %edi
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
	movl	$obfr, %eax
.LBB2_50:                               # %putbyte.exit92
                                        #   in Loop: Header=BB2_46 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, optr(%rip)
	movb	%bl, (%rax)
	cmpq	%r12, %rcx
	jne	.LBB2_52
# BB#51:                                #   in Loop: Header=BB2_46 Depth=2
	movl	outfile(%rip), %edi
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
	movl	$obfr, %ecx
.LBB2_52:                               # %putbyte.exit93
                                        #   in Loop: Header=BB2_46 Depth=2
	leaq	1(%rcx), %rax
	movq	%rax, optr(%rip)
	movb	%r14b, (%rcx)
	cmpq	%r12, %rax
	jne	.LBB2_61
	jmp	.LBB2_60
.LBB2_55:                               #   in Loop: Header=BB2_46 Depth=2
	movl	outfile(%rip), %edi
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
	movl	$obfr, %eax
.LBB2_56:                               # %putbyte.exit95
                                        #   in Loop: Header=BB2_46 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, optr(%rip)
	movb	%bl, (%rax)
	cmpq	%r12, %rcx
	jne	.LBB2_58
# BB#57:                                #   in Loop: Header=BB2_46 Depth=2
	movl	outfile(%rip), %edi
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
	movl	$obfr, %ecx
.LBB2_58:                               # %putbyte.exit96
                                        #   in Loop: Header=BB2_46 Depth=2
	leaq	1(%rcx), %rax
	movq	%rax, optr(%rip)
	movb	%r15b, (%rcx)
	cmpq	%r12, %rax
	jne	.LBB2_61
.LBB2_60:                               #   in Loop: Header=BB2_46 Depth=2
	movl	outfile(%rip), %edi
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
	movl	$obfr, %eax
.LBB2_61:                               # %putbyte.exit94
                                        #   in Loop: Header=BB2_46 Depth=2
	movl	horizontal_size(%rip), %ecx
	incq	%rbp
	cmpl	%ecx, %ebp
	jl	.LBB2_46
.LBB2_62:                               # %._crit_edge
                                        #   in Loop: Header=BB2_44 Depth=1
	movq	64(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movq	56(%rsp), %rax          # 8-byte Reload
	addq	%rax, 32(%rsp)          # 8-byte Folded Spill
	cmpq	48(%rsp), %rdx          # 8-byte Folded Reload
	jne	.LBB2_44
.LBB2_63:                               # %._crit_edge106
	movq	optr(%rip), %rdx
	movl	$obfr, %eax
	cmpq	%rax, %rdx
	je	.LBB2_65
# BB#64:
	movl	outfile(%rip), %edi
	movl	$obfr, %eax
	subq	%rax, %rdx
	movl	$obfr, %esi
	callq	write
.LBB2_65:
	movl	outfile(%rip), %edi
	callq	close
	addq	$344, %rsp              # imm = 0x158
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	store_ppm_tga, .Lfunc_end2-store_ppm_tga
	.cfi_endproc

	.p2align	4, 0x90
	.type	store_yuv1,@function
store_yuv1:                             # @store_yuv1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 80
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movl	%ecx, %r15d
	movl	%edx, %ebp
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB3_2
# BB#1:
	movq	stdout(%rip), %rbx
	movl	$47, %esi
	movq	%r13, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rdx
	cmoveq	%r13, %rdx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
.LBB3_2:
	movl	$577, %esi              # imm = 0x241
	movl	$438, %edx              # imm = 0x1B6
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	open
	movl	%eax, outfile(%rip)
	cmpl	$-1, %eax
	jne	.LBB3_4
# BB#3:
	movl	$Error_Text, %edi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%r13, %rdx
	callq	sprintf
	movl	$Error_Text, %edi
	callq	Error
.LBB3_4:
	movq	$obfr, optr(%rip)
	testl	%r14d, %r14d
	jle	.LBB3_15
# BB#5:                                 # %.lr.ph24
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_15
# BB#6:                                 # %.lr.ph24.split.us.preheader
	movslq	%ebp, %rax
	addq	%rax, 16(%rsp)          # 8-byte Folded Spill
	movslq	%r15d, %r13
	movl	%r14d, %r14d
	xorl	%edx, %edx
	movl	$obfr+4096, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph24.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_8 Depth 2
	movq	%r12, %rbx
	imulq	%r13, %rbx
	addq	16(%rsp), %rbx          # 8-byte Folded Reload
	movl	12(%rsp), %r15d         # 4-byte Reload
	.p2align	4, 0x90
.LBB3_8:                                #   Parent Loop BB3_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx), %eax
	leaq	obfr+1(%rdx), %rcx
	movq	%rcx, optr(%rip)
	movb	%al, obfr(%rdx)
	cmpq	%rbp, %rcx
	je	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_8 Depth=2
	incq	%rdx
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_10:                               #   in Loop: Header=BB3_8 Depth=2
	movl	outfile(%rip), %edi
	movl	$obfr, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	write
	movq	$obfr, optr(%rip)
	xorl	%edx, %edx
.LBB3_11:                               # %putbyte.exit.us
                                        #   in Loop: Header=BB3_8 Depth=2
	incq	%rbx
	decl	%r15d
	jne	.LBB3_8
# BB#12:                                # %._crit_edge.us
                                        #   in Loop: Header=BB3_7 Depth=1
	incq	%r12
	cmpq	%r14, %r12
	jne	.LBB3_7
# BB#13:                                # %._crit_edge25
	testq	%rdx, %rdx
	je	.LBB3_15
# BB#14:
	movl	outfile(%rip), %edi
	movl	$obfr, %esi
	callq	write
.LBB3_15:                               # %._crit_edge25.thread
	movl	outfile(%rip), %edi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	close                   # TAILCALL
.Lfunc_end3:
	.size	store_yuv1, .Lfunc_end3-store_yuv1
	.cfi_endproc

	.p2align	4, 0x90
	.type	conv420to422,@function
conv420to422:                           # @conv420to422
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	subq	$16, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 72
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movabsq	$-4294967295, %r10      # imm = 0xFFFFFFFF00000001
	movl	Coded_Picture_Width(%rip), %r14d
	sarl	%r14d
	movl	Coded_Picture_Height(%rip), %eax
	sarl	%eax
	movq	%rax, -128(%rsp)        # 8-byte Spill
	cmpl	$0, progressive_frame(%rip)
	je	.LBB4_7
# BB#1:                                 # %.preheader247
	testl	%r14d, %r14d
	jle	.LBB4_13
# BB#2:                                 # %.preheader246.lr.ph
	movq	-128(%rsp), %rbx        # 8-byte Reload
	leal	-1(%rbx), %eax
	leal	-2(%rbx), %ecx
	leal	-3(%rbx), %edx
	movslq	%r14d, %rbp
	movslq	%edx, %rdx
	movq	%rdx, -88(%rsp)         # 8-byte Spill
	movslq	%ecx, %r11
	movslq	%eax, %r15
	movl	%ebx, %eax
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rbp, -112(%rsp)        # 8-byte Spill
	movq	%r11, -72(%rsp)         # 8-byte Spill
	movq	%rax, -80(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader246
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
	movl	%ecx, -96(%rsp)         # 4-byte Spill
	cmpl	$0, -128(%rsp)          # 4-byte Folded Reload
	movl	$4294967293, %ebx       # imm = 0xFFFFFFFD
	jle	.LBB4_6
# BB#4:                                 # %.lr.ph254.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	leaq	(%rsi,%rbp), %rcx
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	movq	%rsi, -120(%rsp)        # 8-byte Spill
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph254
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r13,%rbx), %rax
	leaq	(%rax,%r10), %r8
	cmpq	$3, %r13
	leaq	2(%r10,%rax), %rsi
	movl	%eax, %ecx
	cmovll	%r9d, %ecx
	leaq	2(%rbx,%r8), %rbp
	cmpq	$2, %rsi
	leaq	1(%r10,%rbp), %rdx
	leaq	3(%rbx,%r8), %rax
	cmovll	%r9d, %eax
	leaq	1(%rbx,%rdx), %rbx
	leaq	2(%r10,%rbp), %rbp
	testq	%rbp, %rbp
	movl	$4294967293, %esi       # imm = 0xFFFFFFFD
	leaq	3(%rsi,%rdx), %r8
	cmovlel	%r9d, %r8d
	leaq	2(%r10,%rbx), %rbp
	cmpq	%r15, %rbp
	leaq	3(%r10,%rbx), %r13
	movl	%r15d, %r9d
	cmovll	%r13d, %r9d
	leal	2(%rbp), %edx
	cmpq	%r11, %rbp
	cmovgel	%r15d, %edx
	leal	3(%rbp), %r11d
	movq	-88(%rsp), %rsi         # 8-byte Reload
	cmpq	%rsi, %rbp
	cmovgel	%r15d, %r11d
	imull	%r14d, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rdi,%rcx), %ebp
	imull	%r14d, %eax
	movslq	%eax, %rbx
	imull	%r14d, %edx
	movslq	%edx, %r10
	movzbl	(%rdi,%r10), %eax
	movq	%r15, %rcx
	leal	(,%rax,8), %r15d
	subl	%eax, %r15d
	movzbl	(%rdi,%rbx), %edx
	shll	$4, %edx
	imull	%r14d, %r8d
	movslq	%r8d, %r8
	leal	128(%rbp,%rbp,2), %ebp
	subl	%edx, %ebp
	movzbl	(%rdi,%r8), %edx
	imull	$67, %edx, %edx
	addl	%edx, %ebp
	movzbl	(%rdi,%r12), %edx
	imull	$227, %edx, %eax
	imull	%r14d, %r9d
	movslq	%r9d, %rdx
	xorl	%r9d, %r9d
	movq	-120(%rsp), %rsi        # 8-byte Reload
	addl	%eax, %ebp
	movzbl	(%rdi,%rdx), %eax
	shll	$5, %eax
	subl	%eax, %ebp
	addl	%r15d, %ebp
	movq	%rcx, %r15
	movq	Clip(%rip), %rax
	sarl	$8, %ebp
	movslq	%ebp, %rbp
	movzbl	(%rax,%rbp), %eax
	movb	%al, (%rsi,%r12,2)
	movzbl	(%rdi,%rbx), %eax
	leal	(,%rax,8), %ecx
	subl	%eax, %ecx
	imull	%r14d, %r11d
	movslq	%r11d, %rax
	movq	-72(%rsp), %r11         # 8-byte Reload
	movzbl	(%rdi,%rax), %eax
	movzbl	(%rdi,%r10), %ebp
	movabsq	$-4294967295, %r10      # imm = 0xFFFFFFFF00000001
	shll	$4, %ebp
	leal	128(%rax,%rax,2), %eax
	subl	%ebp, %eax
	movzbl	(%rdi,%rdx), %edx
	imull	$67, %edx, %edx
	addl	%edx, %eax
	movzbl	(%rdi,%r12), %edx
	imull	$227, %edx, %edx
	addl	%edx, %eax
	movzbl	(%rdi,%r8), %edx
	movl	$4294967293, %ebx       # imm = 0xFFFFFFFD
	movq	-112(%rsp), %rbp        # 8-byte Reload
	shll	$5, %edx
	subl	%edx, %eax
	addl	%ecx, %eax
	movq	Clip(%rip), %rcx
	sarl	$8, %eax
	cltq
	movzbl	(%rcx,%rax), %eax
	movq	-104(%rsp), %rcx        # 8-byte Reload
	movb	%al, (%rcx,%r12,2)
	movq	-80(%rsp), %rax         # 8-byte Reload
	addq	%rbp, %r12
	cmpq	%rax, %r13
	jne	.LBB4_5
.LBB4_6:                                # %._crit_edge255
                                        #   in Loop: Header=BB4_3 Depth=1
	incq	%rdi
	incq	%rsi
	movl	-96(%rsp), %ecx         # 4-byte Reload
	incl	%ecx
	cmpl	%r14d, %ecx
	jne	.LBB4_3
	jmp	.LBB4_13
.LBB4_7:                                # %.preheader245
	testl	%r14d, %r14d
	jle	.LBB4_13
# BB#8:                                 # %.preheader.lr.ph
	movq	-128(%rsp), %rdx        # 8-byte Reload
	leal	-2(%rdx), %r8d
	leal	-4(%rdx), %r9d
	leal	-6(%rdx), %r11d
	leal	-1(%rdx), %r15d
	leal	-3(%rdx), %ebp
	leal	-5(%rdx), %ecx
	leal	-7(%rdx), %eax
	movslq	%r14d, %r12
	cltq
	movq	%rax, -88(%rsp)         # 8-byte Spill
	movslq	%ecx, %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movslq	%ebp, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movslq	%r15d, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movslq	%r11d, %r11
	movslq	%r9d, %r15
	movslq	%edx, %rax
	movslq	%r8d, %rcx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	leaq	(,%r12,4), %rcx
	leaq	(%r12,%r12,2), %rdx
	leaq	(%r12,%r12), %r9
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	movq	%r12, -112(%rsp)        # 8-byte Spill
	movq	%r11, -8(%rsp)          # 8-byte Spill
	movq	%r15, -16(%rsp)         # 8-byte Spill
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	movq	%rdx, -48(%rsp)         # 8-byte Spill
	movq	%r9, -56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_9:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_11 Depth 2
	movl	%r8d, -60(%rsp)         # 4-byte Spill
	movq	%rsi, -120(%rsp)        # 8-byte Spill
	cmpl	$0, -128(%rsp)          # 4-byte Folded Reload
	movl	$4294967293, %esi       # imm = 0xFFFFFFFD
	jle	.LBB4_12
# BB#10:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_9 Depth=1
	movq	%rdi, %r13
	movq	-120(%rsp), %rbp        # 8-byte Reload
	movq	%rbp, -104(%rsp)        # 8-byte Spill
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB4_11:                               # %.lr.ph
                                        #   Parent Loop BB4_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %r9
	leaq	(%r8,%rsi), %rax
	leaq	-3(%r10,%rax), %rcx
	cmpq	$6, %r8
	movl	$4294967293, %r12d      # imm = 0xFFFFFFFD
	leaq	-3(%r8,%r12), %rsi
	cmovll	%ebx, %esi
	leaq	5(%r12,%rcx), %rbp
	leaq	2(%r10,%rax), %rax
	cmpq	$4, %rax
	leaq	-1(%r10,%rbp), %rax
	leaq	4(%r12,%rcx), %rdx
	cmovll	%ebx, %edx
	leaq	3(%r12,%rax), %rcx
	leaq	2(%r10,%rbp), %rbp
	cmpq	$2, %rbp
	movl	$0, %ebp
	leaq	1(%r10,%rcx), %rbx
	movq	%rbx, -80(%rsp)         # 8-byte Spill
	leaq	4(%r12,%rax), %rbx
	cmovll	%ebp, %ebx
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	leaq	2(%rax,%rcx), %r10
	leal	2(%r10), %eax
	movq	-32(%rsp), %r15         # 8-byte Reload
	cmpq	%r15, %r10
	cmovgel	%r15d, %eax
	leal	4(%r10), %ebp
	cmpq	%r9, %r10
	cmovgel	%r15d, %ebp
	leal	6(%r10), %ecx
	cmpq	%r11, %r10
	cmovgel	%r15d, %ecx
	imull	%r14d, %esi
	movslq	%esi, %rsi
	imull	%r14d, %edx
	movzbl	(%rdi,%rsi), %r11d
	movslq	%edx, %r15
	movzbl	(%rdi,%r15), %edx
	imull	%r14d, %ebx
	imull	$-7, %edx, %edx
	movslq	%ebx, %rbx
	addl	%r11d, %edx
	movzbl	(%rdi,%rbx), %esi
	imull	$30, %esi, %esi
	addl	%esi, %edx
	movzbl	(%r13), %esi
	movq	%r13, %r9
	movq	%r9, -72(%rsp)          # 8-byte Spill
	imull	$248, %esi, %esi
	imull	%r14d, %eax
	cltq
	addl	%esi, %edx
	movzbl	(%rdi,%rax), %esi
	imull	$-21, %esi, %esi
	imull	%r14d, %ebp
	movslq	%ebp, %rbp
	addl	%esi, %edx
	movzbl	(%rdi,%rbp), %esi
	leal	(%rsi,%rsi,4), %esi
	leal	128(%rdx,%rsi), %edx
	movq	Clip(%rip), %rsi
	sarl	$8, %edx
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %edx
	movq	-104(%rsp), %r13        # 8-byte Reload
	movb	%dl, (%r13)
	movq	Clip(%rip), %r11
	movzbl	(%rdi,%r15), %esi
	leal	(,%rsi,8), %edx
	movzbl	(%rdi,%rbx), %ebx
	subl	%esi, %edx
	imull	$-35, %ebx, %esi
	movzbl	(%r9), %ebx
	movzbl	(%rdi,%rax), %eax
	imull	$194, %ebx, %ebx
	imull	$110, %eax, %eax
	movzbl	(%rdi,%rbp), %ebp
	imull	%r14d, %ecx
	imull	$-24, %ebp, %ebp
	movslq	%ecx, %rcx
	movzbl	(%rdi,%rcx), %ecx
	addl	%edx, %esi
	addl	%ebx, %esi
	addl	%eax, %esi
	addl	%ebp, %esi
	leal	128(%rsi,%rcx,4), %eax
	sarl	$8, %eax
	movq	-80(%rsp), %rsi         # 8-byte Reload
	leaq	1(%r12,%rsi), %rcx
	cmpq	$5, %r10
	movabsq	$-4294967295, %rdx      # imm = 0xFFFFFFFF00000001
	leaq	-2(%rdx,%rcx), %rdx
	movslq	%eax, %r10
	leaq	-1(%r12,%rsi), %rbx
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	leaq	2(%rax,%rcx), %rcx
	leaq	4(%r12,%rdx), %rdx
	movl	$1, %r15d
	cmovll	%r15d, %ebx
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	leaq	(%rdx,%rax), %rsi
	cmpq	$3, %rcx
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	leaq	2(%rax,%rdx), %rax
	movl	%edx, %ebp
	movzbl	(%r11,%r10), %r9d
	cmovll	%r15d, %ebp
	leaq	2(%r12,%rsi), %r10
	leaq	4(%r12,%rsi), %r12
	testq	%rax, %rax
	cmovlel	%r15d, %r12d
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	leaq	2(%rax,%r10), %rax
	incl	%r8d
	movq	(%rsp), %rcx            # 8-byte Reload
	cmpq	%rcx, %rax
	cmovgel	%ecx, %r8d
	leal	3(%rax), %esi
	cmpq	8(%rsp), %rax           # 8-byte Folded Reload
	movq	-112(%rsp), %rdx        # 8-byte Reload
	movb	%r9b, (%r13,%rdx,2)
	cmovgel	%ecx, %esi
	leal	5(%rax), %edx
	cmpq	-96(%rsp), %rax         # 8-byte Folded Reload
	cmovgel	%ecx, %edx
	leal	7(%rax), %r15d
	cmpq	-88(%rsp), %rax         # 8-byte Folded Reload
	cmovgel	%ecx, %r15d
	imull	%r14d, %edx
	movslq	%edx, %r11
	movzbl	(%rdi,%r11), %eax
	leal	(,%rax,8), %edx
	subl	%eax, %edx
	imull	%r14d, %esi
	movslq	%esi, %r9
	movzbl	(%rdi,%r9), %eax
	imull	$-35, %eax, %esi
	imull	%r14d, %r8d
	movslq	%r8d, %r8
	addl	%edx, %esi
	movzbl	(%rdi,%r8), %edx
	imull	$194, %edx, %edx
	imull	%r14d, %r12d
	movslq	%r12d, %rcx
	addl	%edx, %esi
	movzbl	(%rdi,%rcx), %eax
	imull	%r14d, %ebp
	movslq	%ebp, %rdx
	imull	$110, %eax, %eax
	addl	%eax, %esi
	movzbl	(%rdi,%rdx), %eax
	imull	$-24, %eax, %eax
	addl	%eax, %esi
	imull	%r14d, %ebx
	movslq	%ebx, %rax
	movq	-112(%rsp), %r12        # 8-byte Reload
	movzbl	(%rdi,%rax), %eax
	leal	128(%rsi,%rax,4), %eax
	movq	Clip(%rip), %rsi
	sarl	$8, %eax
	cltq
	movzbl	(%rsi,%rax), %eax
	movb	%al, (%r12,%r13)
	imull	%r14d, %r15d
	movslq	%r15d, %rax
	movq	-16(%rsp), %r15         # 8-byte Reload
	movzbl	(%rdi,%rax), %eax
	movzbl	(%rdi,%r11), %esi
	movq	-8(%rsp), %r11          # 8-byte Reload
	imull	$-7, %esi, %esi
	addl	%eax, %esi
	movzbl	(%rdi,%r9), %eax
	xorl	%ebx, %ebx
	movq	-56(%rsp), %r9          # 8-byte Reload
	imull	$30, %eax, %eax
	addl	%eax, %esi
	movzbl	(%rdi,%r8), %eax
	imull	$248, %eax, %eax
	addl	%eax, %esi
	movzbl	(%rdi,%rcx), %eax
	imull	$-21, %eax, %eax
	addl	%eax, %esi
	movzbl	(%rdi,%rdx), %eax
	movq	-48(%rsp), %rdx         # 8-byte Reload
	leal	(%rax,%rax,4), %eax
	leal	128(%rsi,%rax), %eax
	movl	$4294967293, %esi       # imm = 0xFFFFFFFD
	movq	Clip(%rip), %rcx
	sarl	$8, %eax
	cltq
	movzbl	(%rcx,%rax), %eax
	movq	-40(%rsp), %rcx         # 8-byte Reload
	movb	%al, (%rdx,%r13)
	addq	%rcx, %r13
	movq	%r13, -104(%rsp)        # 8-byte Spill
	movq	-72(%rsp), %r13         # 8-byte Reload
	addq	%r9, %r13
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	leaq	4(%rax,%r10), %r8
	movq	-24(%rsp), %rax         # 8-byte Reload
	movabsq	$-4294967295, %r10      # imm = 0xFFFFFFFF00000001
	cmpq	%rax, %r8
	jl	.LBB4_11
.LBB4_12:                               # %._crit_edge
                                        #   in Loop: Header=BB4_9 Depth=1
	incq	%rdi
	movq	-120(%rsp), %rsi        # 8-byte Reload
	incq	%rsi
	movl	-60(%rsp), %r8d         # 4-byte Reload
	incl	%r8d
	cmpl	%r14d, %r8d
	jne	.LBB4_9
.LBB4_13:                               # %.loopexit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	conv420to422, .Lfunc_end4-conv420to422
	.cfi_endproc

	.p2align	4, 0x90
	.type	conv422to444,@function
conv422to444:                           # @conv422to444
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	Coded_Picture_Width(%rip), %edx
	sarl	%edx
	cmpl	$0, base+3144(%rip)
	movl	Coded_Picture_Height(%rip), %ebp
	je	.LBB5_11
# BB#1:                                 # %.preheader124
	testl	%ebp, %ebp
	jle	.LBB5_21
# BB#2:                                 # %.preheader123.lr.ph
	testl	%edx, %edx
	jle	.LBB5_3
# BB#7:                                 # %.preheader123.us.preheader
	leal	-1(%rdx), %esi
	movslq	%edx, %rcx
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	leal	-3(%rdx), %ecx
	leal	-2(%rdx), %ebx
	movslq	%esi, %r12
	movslq	%ebx, %r11
	movslq	%ecx, %rsi
	movl	%edx, %r15d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_8:                                # %.preheader123.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_9 Depth 2
	movl	%ecx, -32(%rsp)         # 4-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_9:                                #   Parent Loop BB5_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-2(%rbx), %rcx
	leaq	-1(%rbx), %rdx
	leaq	1(%rbx), %r13
	cmpq	%r12, %rbx
	movl	%r12d, %r9d
	cmovll	%r13d, %r9d
	leal	2(%rbx), %eax
	cmpq	%r11, %rbx
	cmovgel	%r12d, %eax
	leal	3(%rbx), %r8d
	cmpq	%rsi, %rbx
	cmovgel	%r12d, %r8d
	cmpq	$2, %rbx
	movzbl	(%rdi,%rbx), %r10d
	movb	%r10b, (%r14,%rbx,2)
	movl	$0, %r10d
	cmovlq	%r10, %rcx
	movzbl	(%rdi,%rcx), %ecx
	movslq	%r8d, %rbp
	movzbl	(%rdi,%rbp), %ebp
	addl	%ecx, %ebp
	imull	$21, %ebp, %ecx
	testq	%rbx, %rbx
	movq	Clip(%rip), %r8
	cmovleq	%r10, %rdx
	movzbl	(%rdi,%rdx), %edx
	cltq
	movzbl	(%rdi,%rax), %eax
	addl	%edx, %eax
	imull	$-52, %eax, %eax
	movzbl	(%rdi,%rbx), %edx
	movslq	%r9d, %rbp
	movzbl	(%rdi,%rbp), %ebp
	addl	%edx, %ebp
	imull	$159, %ebp, %edx
	addl	%ecx, %eax
	leal	128(%rdx,%rax), %eax
	sarl	$8, %eax
	cltq
	movzbl	(%r8,%rax), %eax
	movb	%al, 1(%r14,%rbx,2)
	cmpq	%r15, %r13
	movq	%r13, %rbx
	jne	.LBB5_9
# BB#10:                                # %._crit_edge132.us
                                        #   in Loop: Header=BB5_8 Depth=1
	addq	-24(%rsp), %rdi         # 8-byte Folded Reload
	movslq	Coded_Picture_Width(%rip), %rax
	addq	%rax, %r14
	movl	-32(%rsp), %ecx         # 4-byte Reload
	incl	%ecx
	cmpl	Coded_Picture_Height(%rip), %ecx
	jl	.LBB5_8
	jmp	.LBB5_21
.LBB5_11:                               # %.preheader122
	testl	%ebp, %ebp
	jle	.LBB5_21
# BB#12:                                # %.preheader.lr.ph
	testl	%edx, %edx
	jle	.LBB5_13
# BB#17:                                # %.preheader.us.preheader
	leal	-1(%rdx), %ebp
	movslq	%edx, %rcx
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	leal	-3(%rdx), %ecx
	leal	-2(%rdx), %esi
	movslq	%ebp, %rbp
	movslq	%esi, %r9
	movslq	%ecx, %rcx
	movl	%edx, %edx
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movq	%rdx, -24(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB5_18:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_19 Depth 2
	movq	%r14, -32(%rsp)         # 8-byte Spill
	movl	%esi, -12(%rsp)         # 4-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_19:                               #   Parent Loop BB5_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-3(%rbx), %r11
	leaq	-2(%rbx), %r10
	leaq	-1(%rbx), %r14
	leaq	1(%rbx), %r13
	cmpq	%rbp, %rbx
	movl	%ebp, %edx
	cmovll	%r13d, %edx
	leal	2(%rbx), %r8d
	cmpq	%r9, %rbx
	cmovgel	%ebp, %r8d
	leal	3(%rbx), %r12d
	cmpq	%rcx, %rbx
	cmovgel	%ebp, %r12d
	cmpq	$3, %rbx
	cmovlq	%r15, %r11
	cmpq	$2, %rbx
	cmovlq	%r15, %r10
	xorl	%esi, %esi
	movq	%r9, %r15
	movzbl	(%rdi,%r10), %r9d
	imull	$-21, %r9d, %r9d
	testq	%rbx, %rbx
	movzbl	(%rdi,%r11), %eax
	leal	(%rax,%rax,4), %eax
	cmovleq	%rsi, %r14
	addl	%r9d, %eax
	movq	%rcx, %r11
	movzbl	(%rdi,%r14), %ecx
	imull	$70, %ecx, %ecx
	addl	%ecx, %eax
	movzbl	(%rdi,%rbx), %ecx
	imull	$228, %ecx, %ecx
	movslq	%edx, %r9
	addl	%ecx, %eax
	movzbl	(%rdi,%r9), %ecx
	imull	$-37, %ecx, %ecx
	movslq	%r8d, %rdx
	addl	%ecx, %eax
	movzbl	(%rdi,%rdx), %ecx
	imull	$11, %ecx, %ecx
	leal	128(%rcx,%rax), %eax
	movq	Clip(%rip), %rcx
	sarl	$8, %eax
	cltq
	movzbl	(%rcx,%rax), %eax
	movq	-32(%rsp), %rsi         # 8-byte Reload
	movb	%al, (%rsi,%rbx,2)
	movslq	%r12d, %rax
	movzbl	(%rdi,%rax), %eax
	leal	(%rax,%rax,4), %eax
	movzbl	(%rdi,%rdx), %ecx
	movq	-24(%rsp), %rdx         # 8-byte Reload
	imull	$-21, %ecx, %ecx
	addl	%eax, %ecx
	movzbl	(%rdi,%r9), %eax
	movq	%r15, %r9
	xorl	%r15d, %r15d
	imull	$70, %eax, %eax
	addl	%eax, %ecx
	movzbl	(%rdi,%rbx), %eax
	imull	$228, %eax, %eax
	addl	%eax, %ecx
	movzbl	(%rdi,%r14), %eax
	imull	$-37, %eax, %eax
	addl	%eax, %ecx
	movzbl	(%rdi,%r10), %eax
	imull	$11, %eax, %eax
	leal	128(%rax,%rcx), %eax
	movq	Clip(%rip), %rcx
	sarl	$8, %eax
	cltq
	movzbl	(%rcx,%rax), %eax
	movq	%r11, %rcx
	movb	%al, 1(%rsi,%rbx,2)
	cmpq	%rdx, %r13
	movq	%r13, %rbx
	jne	.LBB5_19
# BB#20:                                # %._crit_edge.us
                                        #   in Loop: Header=BB5_18 Depth=1
	addq	-8(%rsp), %rdi          # 8-byte Folded Reload
	movslq	Coded_Picture_Width(%rip), %rax
	movq	-32(%rsp), %r14         # 8-byte Reload
	addq	%rax, %r14
	movl	-12(%rsp), %esi         # 4-byte Reload
	incl	%esi
	cmpl	Coded_Picture_Height(%rip), %esi
	jl	.LBB5_18
	jmp	.LBB5_21
.LBB5_3:                                # %.preheader123.preheader
	leal	-1(%rbp), %edx
	movl	%ebp, %esi
	xorl	%ecx, %ecx
	andl	$7, %esi
	je	.LBB5_5
	.p2align	4, 0x90
.LBB5_4:                                # %.preheader123.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	%ecx, %esi
	jne	.LBB5_4
.LBB5_5:                                # %.preheader123.prol.loopexit
	cmpl	$7, %edx
	jb	.LBB5_21
	.p2align	4, 0x90
.LBB5_6:                                # %.preheader123
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %ecx
	cmpl	%ebp, %ecx
	jl	.LBB5_6
	jmp	.LBB5_21
.LBB5_13:                               # %.preheader.preheader
	leal	-1(%rbp), %edx
	movl	%ebp, %esi
	xorl	%ecx, %ecx
	andl	$7, %esi
	je	.LBB5_15
	.p2align	4, 0x90
.LBB5_14:                               # %.preheader.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	%ecx, %esi
	jne	.LBB5_14
.LBB5_15:                               # %.preheader.prol.loopexit
	cmpl	$7, %edx
	jb	.LBB5_21
	.p2align	4, 0x90
.LBB5_16:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %ecx
	cmpl	%ebp, %ecx
	jl	.LBB5_16
.LBB5_21:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	conv422to444, .Lfunc_end5-conv422to444
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s.Y"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%s.U"
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s.V"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"saving %s\n"
	.size	.L.str.3, 11

	.type	outfile,@object         # @outfile
	.local	outfile
	.comm	outfile,4,4
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Couldn't create %s\n"
	.size	.L.str.4, 20

	.type	obfr,@object            # @obfr
	.local	obfr
	.comm	obfr,4096,16
	.type	optr,@object            # @optr
	.local	optr
	.comm	optr,8,8
	.type	store_sif.u422,@object  # @store_sif.u422
	.local	store_sif.u422
	.comm	store_sif.u422,8,8
	.type	store_sif.v422,@object  # @store_sif.v422
	.local	store_sif.v422
	.comm	store_sif.v422,8,8
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"4:4:4 not supported for SIF format"
	.size	.L.str.5, 35

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"malloc failed"
	.size	.L.str.6, 14

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	".SIF"
	.size	.L.str.7, 5

	.type	store_ppm_tga.tga24,@object # @store_ppm_tga.tga24
	.section	.rodata,"a",@progbits
store_ppm_tga.tga24:
	.ascii	"\000\000\002\000\000\000\000\000\000\000\000\000\030 "
	.size	store_ppm_tga.tga24, 14

	.type	store_ppm_tga.u422,@object # @store_ppm_tga.u422
	.local	store_ppm_tga.u422
	.comm	store_ppm_tga.u422,8,8
	.type	store_ppm_tga.v422,@object # @store_ppm_tga.v422
	.local	store_ppm_tga.v422
	.comm	store_ppm_tga.v422,8,8
	.type	store_ppm_tga.u444,@object # @store_ppm_tga.u444
	.local	store_ppm_tga.u444
	.comm	store_ppm_tga.u444,8,8
	.type	store_ppm_tga.v444,@object # @store_ppm_tga.v444
	.local	store_ppm_tga.v444
	.comm	store_ppm_tga.v444,8,8
	.type	.L.str.8,@object        # @.str.8
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.8:
	.asciz	".tga"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	".ppm"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"P6\n%d %d\n255\n"
	.size	.L.str.10, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
