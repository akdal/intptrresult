	.text
	.file	"jdphuff.bc"
	.globl	jinit_phuff_decoder
	.p2align	4, 0x90
	.type	jinit_phuff_decoder,@function
jinit_phuff_decoder:                    # @jinit_phuff_decoder
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$96, %edx
	callq	*(%rax)
	movq	%rax, 576(%rbx)
	movq	$start_pass_phuff_decoder, (%rax)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 72(%rax)
	movdqu	%xmm0, 56(%rax)
	movq	8(%rbx), %rax
	movslq	48(%rbx), %rdx
	shlq	$8, %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, 184(%rbx)
	cmpl	$0, 48(%rbx)
	jle	.LBB0_3
# BB#1:                                 # %.preheader.preheader
	xorl	%ecx, %ecx
	pcmpeqd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, 240(%rax)
	movdqu	%xmm0, 224(%rax)
	movdqu	%xmm0, 208(%rax)
	movdqu	%xmm0, 192(%rax)
	movdqu	%xmm0, 176(%rax)
	movdqu	%xmm0, 160(%rax)
	movdqu	%xmm0, 144(%rax)
	movdqu	%xmm0, 128(%rax)
	movdqu	%xmm0, 112(%rax)
	movdqu	%xmm0, 96(%rax)
	movdqu	%xmm0, 80(%rax)
	movdqu	%xmm0, 64(%rax)
	movdqu	%xmm0, 48(%rax)
	movdqu	%xmm0, 32(%rax)
	movdqu	%xmm0, 16(%rax)
	movdqu	%xmm0, (%rax)
	incl	%ecx
	addq	$256, %rax              # imm = 0x100
	cmpl	48(%rbx), %ecx
	jl	.LBB0_2
.LBB0_3:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end0:
	.size	jinit_phuff_decoder, .Lfunc_end0-jinit_phuff_decoder
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_phuff_decoder,@function
start_pass_phuff_decoder:               # @start_pass_phuff_decoder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 80
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	576(%r12), %rdi
	movl	508(%r12), %ebx
	movl	512(%r12), %eax
	testl	%ebx, %ebx
	je	.LBB1_1
# BB#2:
	cmpl	%eax, %ebx
	setg	%cl
	cmpl	$63, %eax
	setg	%al
	orb	%cl, %al
	movzbl	%al, %eax
	cmpl	$1, 416(%r12)
	sete	%cl
	jmp	.LBB1_3
.LBB1_1:
	testl	%eax, %eax
	sete	%cl
	xorl	%eax, %eax
.LBB1_3:
	testb	%cl, %cl
	movl	$1, %edx
	cmovel	%edx, %eax
	movl	516(%r12), %esi
	movl	520(%r12), %ecx
	testl	%esi, %esi
	je	.LBB1_5
# BB#4:
	decl	%esi
	cmpl	%esi, %ecx
	cmovnel	%edx, %eax
.LBB1_5:                                # %._crit_edge154
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	jne	.LBB1_7
# BB#6:                                 # %._crit_edge154
	cmpl	$14, %ecx
	jl	.LBB1_8
.LBB1_7:
	movq	(%r12), %rax
	movl	$14, 40(%rax)
	movl	%ebx, 44(%rax)
	movl	512(%r12), %ecx
	movl	%ecx, 48(%rax)
	movl	516(%r12), %ecx
	movl	%ecx, 52(%rax)
	movl	520(%r12), %ecx
	movl	%ecx, 56(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
.LBB1_8:                                # %.preheader
	movl	416(%r12), %eax
	testl	%eax, %eax
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	jle	.LBB1_27
# BB#9:                                 # %.lr.ph140
	testl	%ebx, %ebx
	je	.LBB1_19
# BB#10:                                # %.lr.ph140.split.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_11:                               # %.lr.ph140.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_15 Depth 2
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	424(%r12,%rcx,8), %rax
	movl	4(%rax), %ebp
	movq	184(%r12), %r15
	movslq	%ebp, %r13
	shlq	$8, %r13
	cmpl	$0, (%r15,%r13)
	jns	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_11 Depth=1
	movq	(%r12), %rax
	movl	$111, 40(%rax)
	movl	%ebp, 44(%rax)
	movl	$0, 48(%rax)
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	*8(%rax)
.LBB1_13:                               #   in Loop: Header=BB1_11 Depth=1
	movl	508(%r12), %r14d
	cmpl	512(%r12), %r14d
	jg	.LBB1_18
# BB#14:                                # %.lr.ph137.preheader
                                        #   in Loop: Header=BB1_11 Depth=1
	movslq	%r14d, %rbx
	decq	%rbx
	addq	%r13, %r15
	.p2align	4, 0x90
.LBB1_15:                               # %.lr.ph137
                                        #   Parent Loop BB1_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%r15,%rbx,4), %eax
	testl	%eax, %eax
	movl	$0, %ecx
	cmovsl	%ecx, %eax
	cmpl	%eax, 516(%r12)
	je	.LBB1_17
# BB#16:                                #   in Loop: Header=BB1_15 Depth=2
	movq	(%r12), %rax
	movl	$111, 40(%rax)
	movl	%ebp, 44(%rax)
	movl	%r14d, 48(%rax)
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	*8(%rax)
.LBB1_17:                               #   in Loop: Header=BB1_15 Depth=2
	movl	520(%r12), %eax
	movl	%eax, 4(%r15,%rbx,4)
	movslq	512(%r12), %rax
	incq	%rbx
	incl	%r14d
	cmpq	%rax, %rbx
	jl	.LBB1_15
.LBB1_18:                               # %._crit_edge138
                                        #   in Loop: Header=BB1_11 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	incq	%rcx
	movslq	416(%r12), %rax
	cmpq	%rax, %rcx
	jl	.LBB1_11
	jmp	.LBB1_27
.LBB1_19:                               # %.lr.ph140.split.us.preheader
	movl	512(%r12), %ecx
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_20:                               # %.lr.ph140.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_22 Depth 2
	movl	508(%r12), %ebp
	cmpl	%ecx, %ebp
	jg	.LBB1_26
# BB#21:                                # %.lr.ph137.us.preheader
                                        #   in Loop: Header=BB1_20 Depth=1
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	424(%r12,%rsi,8), %rax
	movl	4(%rax), %r14d
	movslq	%r14d, %rbx
	movslq	%ebp, %r13
	decq	%r13
	shlq	$8, %rbx
	addq	184(%r12), %rbx
	.p2align	4, 0x90
.LBB1_22:                               # %.lr.ph137.us
                                        #   Parent Loop BB1_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rbx,%r13,4), %eax
	testl	%eax, %eax
	cmovsl	%r15d, %eax
	cmpl	%eax, 516(%r12)
	je	.LBB1_24
# BB#23:                                #   in Loop: Header=BB1_22 Depth=2
	movq	(%r12), %rax
	movl	$111, 40(%rax)
	movl	%r14d, 44(%rax)
	movl	%ebp, 48(%rax)
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	*8(%rax)
.LBB1_24:                               #   in Loop: Header=BB1_22 Depth=2
	movl	520(%r12), %eax
	movl	%eax, 4(%rbx,%r13,4)
	movslq	512(%r12), %rcx
	incq	%r13
	incl	%ebp
	cmpq	%rcx, %r13
	jl	.LBB1_22
# BB#25:                                # %._crit_edge138.us.loopexit
                                        #   in Loop: Header=BB1_20 Depth=1
	movl	416(%r12), %eax
	movq	(%rsp), %rsi            # 8-byte Reload
.LBB1_26:                               # %._crit_edge138.us
                                        #   in Loop: Header=BB1_20 Depth=1
	incq	%rsi
	movslq	%eax, %rdx
	cmpq	%rdx, %rsi
	jl	.LBB1_20
.LBB1_27:                               # %._crit_edge141
	movl	12(%rsp), %ebp          # 4-byte Reload
	testl	%ebp, %ebp
	movl	516(%r12), %ecx
	movl	$decode_mcu_DC_refine, %edx
	movl	$decode_mcu_AC_refine, %esi
	cmoveq	%rdx, %rsi
	movl	$decode_mcu_DC_first, %edx
	movl	$decode_mcu_AC_first, %edi
	cmoveq	%rdx, %rdi
	testl	%ecx, %ecx
	cmovneq	%rsi, %rdi
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%rdi, 8(%r14)
	testl	%eax, %eax
	jle	.LBB1_34
# BB#28:                                # %.lr.ph
	testl	%ebp, %ebp
	je	.LBB1_35
# BB#29:                                # %.lr.ph.split.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_30:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	424(%r12,%rbp,8), %rax
	movslq	24(%rax), %rbx
	cmpq	$3, %rbx
	ja	.LBB1_32
# BB#31:                                #   in Loop: Header=BB1_30 Depth=1
	movq	256(%r12,%rbx,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB1_33
.LBB1_32:                               # %.lr.ph.split._crit_edge
                                        #   in Loop: Header=BB1_30 Depth=1
	movq	(%r12), %rax
	movl	$49, 40(%rax)
	movl	%ebx, 44(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
	movq	256(%r12,%rbx,8), %rsi
.LBB1_33:                               #   in Loop: Header=BB1_30 Depth=1
	leaq	56(%r14,%rbx,8), %rdx
	movq	%r12, %rdi
	callq	jpeg_make_d_derived_tbl
	movq	56(%r14,%rbx,8), %rax
	movq	%rax, 88(%r14)
	movl	$0, 36(%r14,%rbp,4)
	incq	%rbp
	movslq	416(%r12), %rax
	cmpq	%rax, %rbp
	jl	.LBB1_30
	jmp	.LBB1_34
.LBB1_35:                               # %.lr.ph.split.us.preheader
	movl	$53, %ebp
	testl	%ecx, %ecx
	jne	.LBB1_41
	jmp	.LBB1_37
	.p2align	4, 0x90
.LBB1_42:                               # %..lr.ph.split.us_crit_edge
                                        #   in Loop: Header=BB1_41 Depth=1
	movl	516(%r12), %ecx
	incq	%rbp
	testl	%ecx, %ecx
	jne	.LBB1_41
.LBB1_37:
	movq	(%r12,%rbp,8), %rax
	movslq	20(%rax), %rbx
	cmpq	$3, %rbx
	ja	.LBB1_39
# BB#38:
	movq	224(%r12,%rbx,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB1_40
.LBB1_39:                               # %._crit_edge166
	movq	(%r12), %rax
	movl	$49, 40(%rax)
	movl	%ebx, 44(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
	movq	224(%r12,%rbx,8), %rsi
.LBB1_40:
	leaq	56(%r14,%rbx,8), %rdx
	movq	%r12, %rdi
	callq	jpeg_make_d_derived_tbl
.LBB1_41:                               # =>This Inner Loop Header: Depth=1
	movl	$0, -176(%r14,%rbp,4)
	leaq	-52(%rbp), %rax
	movslq	416(%r12), %rcx
	cmpq	%rcx, %rax
	jl	.LBB1_42
.LBB1_34:                               # %._crit_edge
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r14)
	movl	$0, 32(%r14)
	movl	360(%r12), %eax
	movl	%eax, 52(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	start_pass_phuff_decoder, .Lfunc_end1-start_pass_phuff_decoder
	.cfi_endproc

	.p2align	4, 0x90
	.type	decode_mcu_DC_first,@function
decode_mcu_DC_first:                    # @decode_mcu_DC_first
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 176
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movq	576(%r13), %rbx
	movl	520(%r13), %r15d
	cmpl	$0, 360(%r13)
	je	.LBB2_7
# BB#1:
	cmpl	$0, 52(%rbx)
	jne	.LBB2_7
# BB#2:
	movl	24(%rbx), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	sarl	$3, %ecx
	movq	568(%r13), %rax
	addl	%ecx, 172(%rax)
	movl	$0, 24(%rbx)
	movq	%r13, %rdi
	callq	*16(%rax)
	testl	%eax, %eax
	je	.LBB2_28
# BB#3:                                 # %.preheader.i
	cmpl	$0, 416(%r13)
	jle	.LBB2_6
# BB#4:                                 # %.lr.ph.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 36(%rbx,%rax,4)
	incq	%rax
	movslq	416(%r13), %rcx
	cmpq	%rcx, %rax
	jl	.LBB2_5
.LBB2_6:                                # %process_restart.exit
	movl	$0, 32(%rbx)
	movl	360(%r13), %eax
	movl	%eax, 52(%rbx)
	movl	$0, 28(%rbx)
.LBB2_7:
	movq	%r13, 88(%rsp)
	movq	32(%r13), %rax
	movups	(%rax), %xmm0
	movaps	%xmm0, 48(%rsp)
	movl	524(%r13), %ecx
	movl	%ecx, 64(%rsp)
	movq	16(%rbx), %rsi
	movl	24(%rbx), %edx
	leaq	28(%rbx), %rdi
	movq	%rdi, 96(%rsp)
	movl	48(%rbx), %edi
	movl	%edi, 32(%rsp)
	movups	32(%rbx), %xmm1
	movaps	%xmm1, 16(%rsp)
	cmpl	$0, 464(%r13)
	jle	.LBB2_25
# BB#8:                                 # %.lr.ph
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_9:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14,%r12,8), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movslq	468(%r13,%r12,4), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	424(%r13,%rax,8), %rax
	movslq	20(%rax), %rax
	movq	56(%rbx,%rax,8), %rbp
	cmpl	$7, %edx
	jg	.LBB2_12
# BB#10:                                #   in Loop: Header=BB2_9 Depth=1
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorl	%ecx, %ecx
	leaq	48(%rsp), %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	jpeg_fill_bit_buffer
	testl	%eax, %eax
	je	.LBB2_26
# BB#11:                                #   in Loop: Header=BB2_9 Depth=1
	movq	72(%rsp), %rsi
	movl	80(%rsp), %edx
	movl	$1, %r8d
	cmpl	$8, %edx
	jl	.LBB2_16
.LBB2_12:                               #   in Loop: Header=BB2_9 Depth=1
	leal	-8(%rdx), %ecx
	movq	%rsi, %rax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarq	%cl, %rax
	movzbl	%al, %eax
	movl	360(%rbp,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB2_15
# BB#13:                                #   in Loop: Header=BB2_9 Depth=1
	subl	%ecx, %edx
	movzbl	1384(%rbp,%rax), %ebp
	testl	%ebp, %ebp
	jne	.LBB2_18
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_15:                               #   in Loop: Header=BB2_9 Depth=1
	movl	$9, %r8d
.LBB2_16:                               #   in Loop: Header=BB2_9 Depth=1
	leaq	48(%rsp), %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%rbp, %rcx
	callq	jpeg_huff_decode
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB2_28
# BB#17:                                #   in Loop: Header=BB2_9 Depth=1
	movq	72(%rsp), %rsi
	movl	80(%rsp), %edx
	testl	%ebp, %ebp
	je	.LBB2_14
.LBB2_18:                               #   in Loop: Header=BB2_9 Depth=1
	cmpl	%ebp, %edx
	jge	.LBB2_21
# BB#19:                                #   in Loop: Header=BB2_9 Depth=1
	leaq	48(%rsp), %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%ebp, %ecx
	callq	jpeg_fill_bit_buffer
	testl	%eax, %eax
	je	.LBB2_28
# BB#20:                                #   in Loop: Header=BB2_9 Depth=1
	movq	72(%rsp), %rsi
	movl	80(%rsp), %edx
.LBB2_21:                               #   in Loop: Header=BB2_9 Depth=1
	subl	%ebp, %edx
	movq	%rsi, %rdi
	movl	%edx, %ecx
	sarq	%cl, %rdi
	movl	$1, %eax
	movl	%ebp, %ecx
	shll	%cl, %eax
	decl	%eax
	andl	%edi, %eax
	movslq	%ebp, %rcx
	cmpl	extend_test(,%rcx,4), %eax
	jge	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_9 Depth=1
	addl	extend_offset(,%rcx,4), %eax
	jmp	.LBB2_23
	.p2align	4, 0x90
.LBB2_14:                               #   in Loop: Header=BB2_9 Depth=1
	xorl	%eax, %eax
.LBB2_23:                               #   in Loop: Header=BB2_9 Depth=1
	movq	104(%rsp), %rcx         # 8-byte Reload
	addl	20(%rsp,%rcx,4), %eax
	movl	%eax, 20(%rsp,%rcx,4)
	movl	%r15d, %ecx
	shll	%cl, %eax
	movq	112(%rsp), %rcx         # 8-byte Reload
	movw	%ax, (%rcx)
	incq	%r12
	movslq	464(%r13), %rax
	cmpq	%rax, %r12
	jl	.LBB2_9
# BB#24:                                # %._crit_edge.loopexit
	movq	32(%r13), %rax
	movaps	48(%rsp), %xmm0
	movl	64(%rsp), %ecx
.LBB2_25:                               # %._crit_edge
	leaq	32(%rbx), %rdi
	movups	%xmm0, (%rax)
	movl	%ecx, 524(%r13)
	movq	%rsi, 16(%rbx)
	movl	%edx, 24(%rbx)
	movl	32(%rsp), %eax
	movl	%eax, 16(%rdi)
	movaps	16(%rsp), %xmm0
	movups	%xmm0, (%rdi)
	decl	52(%rbx)
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
	jmp	.LBB2_26
.LBB2_28:
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
.LBB2_26:                               # %process_restart.exit.thread
	movl	12(%rsp), %eax          # 4-byte Reload
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	decode_mcu_DC_first, .Lfunc_end2-decode_mcu_DC_first
	.cfi_endproc

	.p2align	4, 0x90
	.type	decode_mcu_AC_first,@function
decode_mcu_AC_first:                    # @decode_mcu_AC_first
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 144
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r13
	movq	576(%r13), %r15
	movl	512(%r13), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	520(%r13), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	cmpl	$0, 360(%r13)
	je	.LBB3_7
# BB#1:
	cmpl	$0, 52(%r15)
	jne	.LBB3_7
# BB#2:
	movl	24(%r15), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	sarl	$3, %ecx
	movq	568(%r13), %rax
	addl	%ecx, 172(%rax)
	movl	$0, 24(%r15)
	movq	%r13, %rdi
	callq	*16(%rax)
	testl	%eax, %eax
	je	.LBB3_31
# BB#3:                                 # %.preheader.i
	cmpl	$0, 416(%r13)
	jle	.LBB3_6
# BB#4:                                 # %.lr.ph.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 36(%r15,%rax,4)
	incq	%rax
	movslq	416(%r13), %rcx
	cmpq	%rcx, %rax
	jl	.LBB3_5
.LBB3_6:                                # %process_restart.exit
	movl	$0, 32(%r15)
	movl	360(%r13), %eax
	movl	%eax, 52(%r15)
	movl	$0, 28(%r15)
.LBB3_7:
	movl	32(%r15), %ebx
	testl	%ebx, %ebx
	je	.LBB3_9
# BB#8:
	decl	%ebx
	jmp	.LBB3_39
.LBB3_9:
	movq	%r13, 56(%rsp)
	movq	32(%r13), %rax
	movups	(%rax), %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	524(%r13), %eax
	movl	%eax, 32(%rsp)
	movq	16(%r15), %rsi
	movl	24(%r15), %edx
	leaq	28(%r15), %rax
	movq	%rax, 64(%rsp)
	movl	508(%r13), %r14d
	xorl	%ebx, %ebx
	cmpl	8(%rsp), %r14d          # 4-byte Folded Reload
	jg	.LBB3_38
# BB#10:                                # %.lr.ph
	movq	(%rbp), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	88(%r15), %r12
	.p2align	4, 0x90
.LBB3_11:                               # =>This Inner Loop Header: Depth=1
	cmpl	$7, %edx
	jg	.LBB3_14
# BB#12:                                #   in Loop: Header=BB3_11 Depth=1
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	leaq	16(%rsp), %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	jpeg_fill_bit_buffer
	testl	%eax, %eax
	je	.LBB3_40
# BB#13:                                #   in Loop: Header=BB3_11 Depth=1
	movq	40(%rsp), %rsi
	movl	48(%rsp), %edx
	movl	$1, %r8d
	cmpl	$8, %edx
	jl	.LBB3_17
.LBB3_14:                               #   in Loop: Header=BB3_11 Depth=1
	leal	-8(%rdx), %ecx
	movq	%rsi, %rax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarq	%cl, %rax
	movzbl	%al, %eax
	movl	360(%r12,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB3_16
# BB#15:                                #   in Loop: Header=BB3_11 Depth=1
	subl	%ecx, %edx
	movzbl	1384(%r12,%rax), %ebx
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_16:                               #   in Loop: Header=BB3_11 Depth=1
	movl	$9, %r8d
.LBB3_17:                               #   in Loop: Header=BB3_11 Depth=1
	leaq	16(%rsp), %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%r12, %rcx
	callq	jpeg_huff_decode
	movl	%eax, %ebx
	testl	%ebx, %ebx
	js	.LBB3_31
# BB#18:                                #   in Loop: Header=BB3_11 Depth=1
	movq	40(%rsp), %rsi
	movl	48(%rsp), %edx
.LBB3_19:                               #   in Loop: Header=BB3_11 Depth=1
	movl	%ebx, %ebp
	sarl	$4, %ebp
	andl	$15, %ebx
	je	.LBB3_26
# BB#20:                                #   in Loop: Header=BB3_11 Depth=1
	cmpl	%ebx, %edx
	jge	.LBB3_23
# BB#21:                                #   in Loop: Header=BB3_11 Depth=1
	leaq	16(%rsp), %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%ebx, %ecx
	callq	jpeg_fill_bit_buffer
	testl	%eax, %eax
	je	.LBB3_42
# BB#22:                                #   in Loop: Header=BB3_11 Depth=1
	movq	40(%rsp), %rsi
	movl	48(%rsp), %edx
.LBB3_23:                               #   in Loop: Header=BB3_11 Depth=1
	addl	%r14d, %ebp
	subl	%ebx, %edx
	movq	%rsi, %rdi
	movl	%edx, %ecx
	sarq	%cl, %rdi
	movl	$1, %eax
	movl	%ebx, %ecx
	shll	%cl, %eax
	decl	%eax
	andl	%edi, %eax
	movl	%ebx, %ecx
	cmpl	extend_test(,%rcx,4), %eax
	jge	.LBB3_25
# BB#24:                                #   in Loop: Header=BB3_11 Depth=1
	addl	extend_offset(,%rcx,4), %eax
.LBB3_25:                               #   in Loop: Header=BB3_11 Depth=1
	movl	12(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movslq	%ebp, %rcx
	movslq	jpeg_natural_order(,%rcx,4), %rcx
	movq	80(%rsp), %rdi          # 8-byte Reload
	movw	%ax, (%rdi,%rcx,2)
	jmp	.LBB3_28
	.p2align	4, 0x90
.LBB3_26:                               #   in Loop: Header=BB3_11 Depth=1
	cmpl	$15, %ebp
	jne	.LBB3_32
# BB#27:                                #   in Loop: Header=BB3_11 Depth=1
	addl	$15, %r14d
	movl	%r14d, %ebp
.LBB3_28:                               #   in Loop: Header=BB3_11 Depth=1
	leal	1(%rbp), %r14d
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jl	.LBB3_11
# BB#29:
	xorl	%ebx, %ebx
	jmp	.LBB3_38
.LBB3_31:
	xorl	%ebx, %ebx
	jmp	.LBB3_40
.LBB3_32:
	movl	$1, %ebx
	movl	%ebp, %ecx
	shll	%cl, %ebx
	testl	%ebp, %ebp
	je	.LBB3_37
# BB#33:
	cmpl	%ebp, %edx
	jge	.LBB3_36
# BB#34:
	leaq	16(%rsp), %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%ebp, %ecx
	callq	jpeg_fill_bit_buffer
	testl	%eax, %eax
	je	.LBB3_42
# BB#35:
	movq	40(%rsp), %rsi
	movl	48(%rsp), %edx
.LBB3_36:
	subl	%ebp, %edx
	movq	%rsi, %rax
	movl	%edx, %ecx
	sarq	%cl, %rax
	leal	-1(%rbx), %ecx
	andl	%eax, %ecx
	addl	%ebx, %ecx
	movl	%ecx, %ebx
.LBB3_37:
	decl	%ebx
.LBB3_38:                               # %.loopexit
	movq	32(%r13), %rax
	movaps	16(%rsp), %xmm0
	movups	%xmm0, (%rax)
	movl	32(%rsp), %eax
	movl	%eax, 524(%r13)
	movq	%rsi, 16(%r15)
	movl	%edx, 24(%r15)
.LBB3_39:
	movl	%ebx, 32(%r15)
	decl	52(%r15)
	movl	$1, %ebx
.LBB3_40:                               # %process_restart.exit.thread
	movl	%ebx, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_42:
	xorl	%ebx, %ebx
	jmp	.LBB3_40
.Lfunc_end3:
	.size	decode_mcu_AC_first, .Lfunc_end3-decode_mcu_AC_first
	.cfi_endproc

	.p2align	4, 0x90
	.type	decode_mcu_DC_refine,@function
decode_mcu_DC_refine:                   # @decode_mcu_DC_refine
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi47:
	.cfi_def_cfa_offset 112
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	576(%rbx), %r12
	movb	520(%rbx), %cl
	movl	$1, %r13d
	shll	%cl, %r13d
	cmpl	$0, 360(%rbx)
	je	.LBB4_7
# BB#1:
	cmpl	$0, 52(%r12)
	jne	.LBB4_7
# BB#2:
	movl	24(%r12), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	sarl	$3, %ecx
	movq	568(%rbx), %rax
	addl	%ecx, 172(%rax)
	movl	$0, 24(%r12)
	movq	%rbx, %rdi
	callq	*16(%rax)
	testl	%eax, %eax
	je	.LBB4_19
# BB#3:                                 # %.preheader.i
	cmpl	$0, 416(%rbx)
	jle	.LBB4_6
# BB#4:                                 # %.lr.ph.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 36(%r12,%rax,4)
	incq	%rax
	movslq	416(%rbx), %rcx
	cmpq	%rcx, %rax
	jl	.LBB4_5
.LBB4_6:                                # %process_restart.exit
	movl	$0, 32(%r12)
	movl	360(%rbx), %eax
	movl	%eax, 52(%r12)
	movl	$0, 28(%r12)
.LBB4_7:
	movq	%rbx, 40(%rsp)
	movq	32(%rbx), %rax
	movups	(%rax), %xmm0
	movaps	%xmm0, (%rsp)
	movl	524(%rbx), %ecx
	movl	%ecx, 16(%rsp)
	movq	16(%r12), %rsi
	movl	24(%r12), %edx
	leaq	28(%r12), %rdi
	movq	%rdi, 48(%rsp)
	cmpl	$0, 464(%rbx)
	jle	.LBB4_16
# BB#8:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_9:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbp,8), %r15
	testl	%edx, %edx
	jg	.LBB4_12
# BB#10:                                #   in Loop: Header=BB4_9 Depth=1
	movl	$1, %ecx
	movq	%rsp, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	jpeg_fill_bit_buffer
	testl	%eax, %eax
	je	.LBB4_19
# BB#11:                                #   in Loop: Header=BB4_9 Depth=1
	movq	24(%rsp), %rsi
	movl	32(%rsp), %edx
.LBB4_12:                               #   in Loop: Header=BB4_9 Depth=1
	decl	%edx
	btq	%rdx, %rsi
	jae	.LBB4_14
# BB#13:                                #   in Loop: Header=BB4_9 Depth=1
	movzwl	(%r15), %eax
	orl	%r13d, %eax
	movw	%ax, (%r15)
.LBB4_14:                               #   in Loop: Header=BB4_9 Depth=1
	incq	%rbp
	movslq	464(%rbx), %rax
	cmpq	%rax, %rbp
	jl	.LBB4_9
# BB#15:                                # %._crit_edge.loopexit
	movq	32(%rbx), %rax
	movaps	(%rsp), %xmm0
	movl	16(%rsp), %ecx
.LBB4_16:                               # %._crit_edge
	movups	%xmm0, (%rax)
	movl	%ecx, 524(%rbx)
	movq	%rsi, 16(%r12)
	movl	%edx, 24(%r12)
	decl	52(%r12)
	movl	$1, %eax
	jmp	.LBB4_17
.LBB4_19:
	xorl	%eax, %eax
.LBB4_17:                               # %process_restart.exit.thread
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	decode_mcu_DC_refine, .Lfunc_end4-decode_mcu_DC_refine
	.cfi_endproc

	.p2align	4, 0x90
	.type	decode_mcu_AC_refine,@function
decode_mcu_AC_refine:                   # @decode_mcu_AC_refine
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	subq	$376, %rsp              # imm = 0x178
.Lcfi60:
	.cfi_def_cfa_offset 432
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r12
	movq	576(%r12), %rbx
	movslq	512(%r12), %rdx
	movb	520(%r12), %cl
	movl	$1, %esi
	shll	%cl, %esi
	movl	$-1, %eax
	shll	%cl, %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	cmpl	$0, 360(%r12)
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%esi, 4(%rsp)           # 4-byte Spill
	je	.LBB5_7
# BB#1:
	cmpl	$0, 52(%rbx)
	jne	.LBB5_7
# BB#2:
	movl	24(%rbx), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	sarl	$3, %ecx
	movq	568(%r12), %rax
	addl	%ecx, 172(%rax)
	movl	$0, 24(%rbx)
	movq	%r12, %rdi
	callq	*16(%rax)
	testl	%eax, %eax
	je	.LBB5_57
# BB#3:                                 # %.preheader.i
	cmpl	$0, 416(%r12)
	jle	.LBB5_6
# BB#4:                                 # %.lr.ph.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 36(%rbx,%rax,4)
	incq	%rax
	movslq	416(%r12), %rcx
	cmpq	%rcx, %rax
	jl	.LBB5_5
.LBB5_6:                                # %process_restart.exit
	movl	$0, 32(%rbx)
	movl	360(%r12), %eax
	movl	%eax, 52(%rbx)
	movl	$0, 28(%rbx)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
.LBB5_7:
	movq	%r12, 88(%rsp)
	movq	32(%r12), %rax
	movups	(%rax), %xmm0
	movaps	%xmm0, 48(%rsp)
	movl	524(%r12), %eax
	movl	%eax, 64(%rsp)
	movq	16(%rbx), %r9
	movl	24(%rbx), %r15d
	leaq	28(%rbx), %rax
	movq	%rax, 96(%rsp)
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movl	32(%rbx), %ecx
	movq	(%rbp), %r14
	movl	508(%r12), %eax
	xorl	%r13d, %r13d
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	movq	%r12, 104(%rsp)         # 8-byte Spill
	je	.LBB5_19
# BB#8:                                 # %.thread.preheader
	cmpl	%edx, %eax
	jg	.LBB5_18
.LBB5_9:                                # %.lr.ph238
	movslq	%eax, %rbp
	decq	%rbp
	leaq	48(%rsp), %r12
	.p2align	4, 0x90
.LBB5_10:                               # =>This Inner Loop Header: Depth=1
	movslq	jpeg_natural_order+4(,%rbp,4), %rbx
	cmpw	$0, (%r14,%rbx,2)
	je	.LBB5_17
# BB#11:                                #   in Loop: Header=BB5_10 Depth=1
	testl	%r15d, %r15d
	jg	.LBB5_14
# BB#12:                                #   in Loop: Header=BB5_10 Depth=1
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r9, %rsi
	movl	%r15d, %edx
	callq	jpeg_fill_bit_buffer
	testl	%eax, %eax
	je	.LBB5_58
# BB#13:                                #   in Loop: Header=BB5_10 Depth=1
	movq	72(%rsp), %r9
	movl	80(%rsp), %r15d
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
.LBB5_14:                               #   in Loop: Header=BB5_10 Depth=1
	decl	%r15d
	btq	%r15, %r9
	jae	.LBB5_17
# BB#15:                                #   in Loop: Header=BB5_10 Depth=1
	movswl	(%r14,%rbx,2), %eax
	testl	%esi, %eax
	jne	.LBB5_17
# BB#16:                                #   in Loop: Header=BB5_10 Depth=1
	testw	%ax, %ax
	movl	28(%rsp), %ecx          # 4-byte Reload
	cmovnsl	%esi, %ecx
	addl	%eax, %ecx
	movw	%cx, (%r14,%rbx,2)
	.p2align	4, 0x90
.LBB5_17:                               # %.thread
                                        #   in Loop: Header=BB5_10 Depth=1
	incq	%rbp
	cmpq	%rdx, %rbp
	jl	.LBB5_10
.LBB5_18:                               # %.thread._crit_edge
	movq	32(%rsp), %rdi          # 8-byte Reload
	decl	%edi
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	104(%rsp), %r12         # 8-byte Reload
	jmp	.LBB5_21
.LBB5_19:                               # %.preheader213
	xorl	%edi, %edi
	cmpl	%edx, %eax
	jle	.LBB5_23
.LBB5_20:
	movq	40(%rsp), %rcx          # 8-byte Reload
.LBB5_21:                               # %.loopexit.thread
	movq	32(%r12), %rax
	movaps	48(%rsp), %xmm0
	movups	%xmm0, (%rax)
	movl	64(%rsp), %eax
	movl	%eax, 524(%r12)
	movq	%r9, 16(%rcx)
	movl	%r15d, 24(%rcx)
	movl	%edi, 32(%rcx)
	decl	52(%rcx)
	movl	$1, %eax
.LBB5_22:                               # %process_restart.exit.thread
	addq	$376, %rsp              # imm = 0x178
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_23:                               # %.lr.ph245
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	88(%rax), %rbp
	xorl	%ebx, %ebx
	movq	%rbp, 32(%rsp)          # 8-byte Spill
.LBB5_24:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_42 Depth 2
	cmpl	$7, %r15d
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	jg	.LBB5_27
# BB#25:                                #   in Loop: Header=BB5_24 Depth=1
	xorl	%ecx, %ecx
	leaq	48(%rsp), %rdi
	movq	%r9, %rsi
	movl	%r15d, %edx
	callq	jpeg_fill_bit_buffer
	testl	%eax, %eax
	je	.LBB5_59
# BB#26:                                #   in Loop: Header=BB5_24 Depth=1
	movq	72(%rsp), %r9
	movl	80(%rsp), %r15d
	movl	$1, %r8d
	cmpl	$8, %r15d
	movl	4(%rsp), %esi           # 4-byte Reload
	jl	.LBB5_30
.LBB5_27:                               #   in Loop: Header=BB5_24 Depth=1
	leal	-8(%r15), %ecx
	movq	%r9, %rax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarq	%cl, %rax
	movzbl	%al, %eax
	movl	360(%rbp,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB5_29
# BB#28:                                #   in Loop: Header=BB5_24 Depth=1
	subl	%ecx, %r15d
	movzbl	1384(%rbp,%rax), %eax
	jmp	.LBB5_32
	.p2align	4, 0x90
.LBB5_29:                               #   in Loop: Header=BB5_24 Depth=1
	movl	$9, %r8d
.LBB5_30:                               #   in Loop: Header=BB5_24 Depth=1
	leaq	48(%rsp), %rdi
	movq	%r9, %rsi
	movl	%r15d, %edx
	movq	%rbp, %rcx
	callq	jpeg_huff_decode
	testl	%eax, %eax
	js	.LBB5_59
# BB#31:                                #   in Loop: Header=BB5_24 Depth=1
	movq	72(%rsp), %r9
	movl	80(%rsp), %r15d
	movl	4(%rsp), %esi           # 4-byte Reload
.LBB5_32:                               #   in Loop: Header=BB5_24 Depth=1
	movl	%eax, %ebx
	sarl	$4, %ebx
	andb	$15, %al
	movq	16(%rsp), %rdx          # 8-byte Reload
	je	.LBB5_39
# BB#33:                                #   in Loop: Header=BB5_24 Depth=1
	cmpb	$1, %al
	je	.LBB5_35
# BB#34:                                #   in Loop: Header=BB5_24 Depth=1
	movq	(%r12), %rax
	movl	$114, 40(%rax)
	movl	$-1, %esi
	movq	%r12, %rdi
	movq	%r9, %r12
	callq	*8(%rax)
	movq	%r12, %r9
	movl	4(%rsp), %esi           # 4-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB5_35:                               #   in Loop: Header=BB5_24 Depth=1
	testl	%r15d, %r15d
	jg	.LBB5_38
# BB#36:                                #   in Loop: Header=BB5_24 Depth=1
	movl	$1, %ecx
	leaq	48(%rsp), %rdi
	movq	%r9, %rsi
	movl	%r15d, %edx
	callq	jpeg_fill_bit_buffer
	testl	%eax, %eax
	je	.LBB5_69
# BB#37:                                #   in Loop: Header=BB5_24 Depth=1
	movq	72(%rsp), %r9
	movl	80(%rsp), %r15d
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
.LBB5_38:                               #   in Loop: Header=BB5_24 Depth=1
	decl	%r15d
	btq	%r15, %r9
	movl	%esi, %edi
	cmovael	28(%rsp), %edi          # 4-byte Folded Reload
	jmp	.LBB5_41
	.p2align	4, 0x90
.LBB5_39:                               #   in Loop: Header=BB5_24 Depth=1
	cmpl	$15, %ebx
	jne	.LBB5_62
# BB#40:                                #   in Loop: Header=BB5_24 Depth=1
	xorl	%edi, %edi
	movl	$15, %ebx
.LBB5_41:                               # %.preheader211
                                        #   in Loop: Header=BB5_24 Depth=1
	movslq	12(%rsp), %r12          # 4-byte Folded Reload
	decq	%r12
	.p2align	4, 0x90
.LBB5_42:                               #   Parent Loop BB5_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	jpeg_natural_order+4(,%r12,4), %r13
	cmpw	$0, (%r14,%r13,2)
	je	.LBB5_49
# BB#43:                                #   in Loop: Header=BB5_42 Depth=2
	testl	%r15d, %r15d
	jg	.LBB5_46
# BB#44:                                #   in Loop: Header=BB5_42 Depth=2
	movq	%r14, %rbp
	movl	%edi, %r14d
	movl	$1, %ecx
	leaq	48(%rsp), %rdi
	movq	%r9, %rsi
	movl	%r15d, %edx
	callq	jpeg_fill_bit_buffer
	testl	%eax, %eax
	je	.LBB5_56
# BB#45:                                #   in Loop: Header=BB5_42 Depth=2
	movq	72(%rsp), %r9
	movl	80(%rsp), %r15d
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
	movl	%r14d, %edi
	movq	%rbp, %r14
.LBB5_46:                               #   in Loop: Header=BB5_42 Depth=2
	decl	%r15d
	btq	%r15, %r9
	jae	.LBB5_51
# BB#47:                                #   in Loop: Header=BB5_42 Depth=2
	movswl	(%r14,%r13,2), %eax
	testl	%esi, %eax
	jne	.LBB5_51
# BB#48:                                #   in Loop: Header=BB5_42 Depth=2
	testw	%ax, %ax
	movl	28(%rsp), %ecx          # 4-byte Reload
	cmovnsl	%esi, %ecx
	addl	%eax, %ecx
	movw	%cx, (%r14,%r13,2)
	jmp	.LBB5_51
	.p2align	4, 0x90
.LBB5_49:                               #   in Loop: Header=BB5_42 Depth=2
	testl	%ebx, %ebx
	jle	.LBB5_52
# BB#50:                                #   in Loop: Header=BB5_42 Depth=2
	decl	%ebx
.LBB5_51:                               #   in Loop: Header=BB5_42 Depth=2
	incq	%r12
	cmpq	%rdx, %r12
	jl	.LBB5_42
.LBB5_52:                               # %._crit_edge
                                        #   in Loop: Header=BB5_24 Depth=1
	incl	%r12d
	testl	%edi, %edi
	movl	8(%rsp), %ebx           # 4-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB5_54
# BB#53:                                #   in Loop: Header=BB5_24 Depth=1
	movslq	%r12d, %rax
	movslq	jpeg_natural_order(,%rax,4), %rax
	movw	%di, (%r14,%rax,2)
	movslq	%ebx, %rcx
	incl	%ebx
	movl	%eax, 112(%rsp,%rcx,4)
.LBB5_54:                               #   in Loop: Header=BB5_24 Depth=1
	leal	1(%r12), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	cmpl	%edx, %r12d
	movq	104(%rsp), %r12         # 8-byte Reload
	jl	.LBB5_24
.LBB5_55:
	xorl	%edi, %edi
	jmp	.LBB5_20
.LBB5_56:
	movl	8(%rsp), %ebx           # 4-byte Reload
	movq	%rbp, %r14
	jmp	.LBB5_59
.LBB5_57:
	xorl	%eax, %eax
	jmp	.LBB5_22
.LBB5_58:
	movl	%r13d, %ebx
.LBB5_59:                               # %.preheader
	xorl	%eax, %eax
	testl	%ebx, %ebx
	jle	.LBB5_22
# BB#60:                                # %.lr.ph.preheader
	movslq	%ebx, %rcx
	incq	%rcx
	.p2align	4, 0x90
.LBB5_61:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	104(%rsp,%rcx,4), %rdx
	movw	$0, (%r14,%rdx,2)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB5_61
	jmp	.LBB5_22
.LBB5_62:
	movl	$1, %r13d
	movl	%ebx, %ecx
	shll	%cl, %r13d
	testl	%ebx, %ebx
	je	.LBB5_67
# BB#63:
	cmpl	%ebx, %r15d
	jge	.LBB5_66
# BB#64:
	leaq	48(%rsp), %rdi
	movq	%r9, %rsi
	movl	%r15d, %edx
	movl	%ebx, %ecx
	callq	jpeg_fill_bit_buffer
	testl	%eax, %eax
	je	.LBB5_69
# BB#65:
	movq	72(%rsp), %r9
	movl	80(%rsp), %r15d
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
.LBB5_66:                               # %.loopexit
	subl	%ebx, %r15d
	movq	%r9, %rax
	movl	%r15d, %ecx
	sarq	%cl, %rax
	leal	-1(%r13), %ecx
	andl	%eax, %ecx
	addl	%ecx, %r13d
	je	.LBB5_55
.LBB5_67:
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movl	8(%rsp), %r13d          # 4-byte Reload
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	%edx, %eax
	jle	.LBB5_9
	jmp	.LBB5_18
.LBB5_69:
	movl	8(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB5_59
.Lfunc_end5:
	.size	decode_mcu_AC_refine, .Lfunc_end5-decode_mcu_AC_refine
	.cfi_endproc

	.type	extend_test,@object     # @extend_test
	.section	.rodata,"a",@progbits
	.p2align	4
extend_test:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	16                      # 0x10
	.long	32                      # 0x20
	.long	64                      # 0x40
	.long	128                     # 0x80
	.long	256                     # 0x100
	.long	512                     # 0x200
	.long	1024                    # 0x400
	.long	2048                    # 0x800
	.long	4096                    # 0x1000
	.long	8192                    # 0x2000
	.long	16384                   # 0x4000
	.size	extend_test, 64

	.type	extend_offset,@object   # @extend_offset
	.p2align	4
extend_offset:
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	4294967293              # 0xfffffffd
	.long	4294967289              # 0xfffffff9
	.long	4294967281              # 0xfffffff1
	.long	4294967265              # 0xffffffe1
	.long	4294967233              # 0xffffffc1
	.long	4294967169              # 0xffffff81
	.long	4294967041              # 0xffffff01
	.long	4294966785              # 0xfffffe01
	.long	4294966273              # 0xfffffc01
	.long	4294965249              # 0xfffff801
	.long	4294963201              # 0xfffff001
	.long	4294959105              # 0xffffe001
	.long	4294950913              # 0xffffc001
	.long	4294934529              # 0xffff8001
	.size	extend_offset, 64


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
