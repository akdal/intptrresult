	.text
	.file	"btHingeConstraint.bc"
	.globl	_ZN17btHingeConstraintC2Ev
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraintC2Ev,@function
_ZN17btHingeConstraintC2Ev:             # @_ZN17btHingeConstraintC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$4, %esi
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintType
	movq	$_ZTV17btHingeConstraint+16, (%rbx)
	movb	$0, 781(%rbx)
	movb	$0, 783(%rbx)
	movb	$0, 784(%rbx)
	movl	$1065353216, 776(%rbx)  # imm = 0x3F800000
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN17btHingeConstraintC2Ev, .Lfunc_end0-_ZN17btHingeConstraintC2Ev
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1065353214              # float 0.99999988
.LCPI1_2:
	.long	3212836862              # float -0.99999988
.LCPI1_3:
	.long	1065353216              # float 1
.LCPI1_4:
	.long	1056964608              # float 0.5
.LCPI1_6:
	.long	1060439283              # float 0.707106769
.LCPI1_8:
	.long	3212836864              # float -1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI1_5:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI1_7:
	.long	1050253722              # float 0.300000012
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	3212836864              # float -1
	.text
	.globl	_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK9btVector3S4_RS2_S5_b
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK9btVector3S4_RS2_S5_b,@function
_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK9btVector3S4_RS2_S5_b: # @_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK9btVector3S4_RS2_S5_b
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 160
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %rbp
	movq	%rcx, %r13
	movq	%rdx, %rax
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movb	168(%rsp), %r14b
	movl	$4, %esi
	movq	%rbx, %rdx
	movq	%rax, %rcx
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_
	movq	$_ZTV17btHingeConstraint+16, (%r12)
	movb	$0, 780(%r12)
	movb	$0, 781(%r12)
	movb	$0, 783(%r12)
	movb	%r14b, 784(%r12)
	movups	(%r13), %xmm0
	movups	%xmm0, 648(%r12)
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	40(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movlps	%xmm4, 8(%rsp)
	movss	(%r15), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm5
	mulss	%xmm8, %xmm5
	movq	4(%r15), %xmm2          # xmm2 = mem[0],zero
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	movaps	%xmm1, %xmm6
	mulss	%xmm2, %xmm6
	addss	%xmm5, %xmm6
	mulss	%xmm4, %xmm0
	addss	%xmm6, %xmm0
	ucomiss	.LCPI1_0(%rip), %xmm0
	jae	.LBB1_3
# BB#1:
	movss	.LCPI1_2(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm5
	jae	.LBB1_4
# BB#2:
	leaq	8(%rsp), %rax
	movaps	%xmm3, %xmm0
	shufps	$0, %xmm4, %xmm0        # xmm0 = xmm0[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm0      # xmm0 = xmm0[2,0],xmm4[2,3]
	mulps	%xmm2, %xmm0
	movaps	%xmm1, %xmm5
	unpcklps	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1]
	movaps	%xmm2, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	shufps	$0, %xmm6, %xmm4        # xmm4 = xmm4[0,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm4      # xmm4 = xmm4[2,0],xmm6[2,3]
	mulps	%xmm5, %xmm4
	subps	%xmm4, %xmm0
	movaps	%xmm0, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	mulss	%xmm8, %xmm1
	mulss	%xmm2, %xmm3
	subss	%xmm3, %xmm1
	xorps	%xmm3, %xmm3
	movaps	%xmm1, %xmm5
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movlps	%xmm5, 16(%rsp)
	movaps	%xmm1, %xmm3
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	mulss	%xmm8, %xmm4
	shufps	$0, %xmm6, %xmm8        # xmm8 = xmm8[0,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm8      # xmm8 = xmm8[2,0],xmm6[2,3]
	mulps	%xmm3, %xmm8
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	mulps	%xmm2, %xmm1
	subps	%xmm1, %xmm8
	movaps	%xmm0, %xmm3
	mulss	%xmm2, %xmm3
	subss	%xmm4, %xmm3
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	jmp	.LBB1_6
.LBB1_3:
	movss	16(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm8    # xmm8 = xmm8[0],xmm0[0],xmm8[1],xmm0[1]
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm8
	movss	48(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	jmp	.LBB1_5
.LBB1_4:
	movss	16(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm8    # xmm8 = xmm8[0],xmm0[0],xmm8[1],xmm0[1]
	movss	48(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
.LBB1_5:
	movlps	%xmm0, 8(%rsp)
	movss	12(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	28(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movss	44(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	leaq	16(%rsp), %rax
.LBB1_6:
	movq	160(%rsp), %rbx
	movlps	%xmm1, (%rax)
	movss	%xmm8, 600(%r12)
	shufps	$212, %xmm0, %xmm0      # xmm0 = xmm0[0,1,1,3]
	movss	%xmm0, 604(%r12)
	movl	(%r15), %eax
	movl	%eax, 608(%r12)
	movl	$0, 612(%r12)
	movaps	%xmm8, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movss	%xmm6, 616(%r12)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, 620(%r12)
	movl	4(%r15), %eax
	movl	%eax, 624(%r12)
	movl	$0, 628(%r12)
	movl	8(%rsp), %eax
	movl	%eax, 632(%r12)
	movl	16(%rsp), %ecx
	movl	%ecx, 636(%r12)
	movl	8(%r15), %ecx
	movl	%ecx, 640(%r12)
	movl	$0, 644(%r12)
	movss	(%r15), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	8(%r15), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm0, %xmm5
	mulss	%xmm10, %xmm5
	addss	%xmm1, %xmm5
	movaps	%xmm4, %xmm1
	mulss	%xmm7, %xmm1
	addss	%xmm5, %xmm1
	movd	%eax, %xmm9
	movss	.LCPI1_2(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm5
	jbe	.LBB1_11
# BB#7:
	movaps	.LCPI1_5(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	andps	%xmm7, %xmm1
	ucomiss	.LCPI1_6(%rip), %xmm1
	jbe	.LBB1_14
# BB#8:
	mulss	%xmm0, %xmm0
	mulss	%xmm7, %xmm7
	addss	%xmm0, %xmm7
	xorps	%xmm0, %xmm0
	sqrtss	%xmm7, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB1_10
# BB#9:                                 # %call.sqrt
	movaps	%xmm7, %xmm0
	movaps	%xmm8, 48(%rsp)         # 16-byte Spill
	movaps	%xmm6, 32(%rsp)         # 16-byte Spill
	movd	%xmm9, 4(%rsp)          # 4-byte Folded Spill
	callq	sqrtf
	movd	4(%rsp), %xmm9          # 4-byte Folded Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm6         # 16-byte Reload
	movaps	48(%rsp), %xmm8         # 16-byte Reload
.LBB1_10:                               # %.split
	movss	.LCPI1_3(%rip), %xmm10  # xmm10 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm10
	movss	8(%r15), %xmm7          # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm7
	xorps	.LCPI1_1(%rip), %xmm7
	mulss	4(%r15), %xmm10
	xorps	%xmm11, %xmm11
	jmp	.LBB1_17
.LBB1_11:
	movaps	%xmm0, %xmm11
	mulss	%xmm4, %xmm11
	movaps	%xmm7, %xmm5
	mulss	%xmm10, %xmm5
	subss	%xmm5, %xmm11
	mulss	%xmm3, %xmm7
	mulss	%xmm2, %xmm4
	subss	%xmm4, %xmm7
	mulss	%xmm2, %xmm10
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm10
	addss	.LCPI1_3(%rip), %xmm1
	addss	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB1_13
# BB#12:                                # %call.sqrt209
	movaps	%xmm1, %xmm0
	movaps	%xmm8, 48(%rsp)         # 16-byte Spill
	movaps	%xmm6, 32(%rsp)         # 16-byte Spill
	movaps	%xmm7, 80(%rsp)         # 16-byte Spill
	movd	%xmm9, 4(%rsp)          # 4-byte Folded Spill
	movss	%xmm10, 28(%rsp)        # 4-byte Spill
	movaps	%xmm11, 64(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	64(%rsp), %xmm11        # 16-byte Reload
	movss	28(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movd	4(%rsp), %xmm9          # 4-byte Folded Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	80(%rsp), %xmm7         # 16-byte Reload
	movaps	32(%rsp), %xmm6         # 16-byte Reload
	movaps	48(%rsp), %xmm8         # 16-byte Reload
.LBB1_13:                               # %.split208
	movss	.LCPI1_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm11
	mulss	%xmm1, %xmm7
	mulss	%xmm1, %xmm10
	mulss	.LCPI1_4(%rip), %xmm0
	jmp	.LBB1_18
.LBB1_14:
	mulss	%xmm2, %xmm2
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB1_16
# BB#15:                                # %call.sqrt207
	movaps	%xmm8, 48(%rsp)         # 16-byte Spill
	movaps	%xmm6, 32(%rsp)         # 16-byte Spill
	movd	%xmm9, 4(%rsp)          # 4-byte Folded Spill
	callq	sqrtf
	movd	4(%rsp), %xmm9          # 4-byte Folded Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm6         # 16-byte Reload
	movaps	48(%rsp), %xmm8         # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB1_16:                               # %.split206
	movss	.LCPI1_3(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm7
	movss	4(%r15), %xmm11         # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm11
	xorps	.LCPI1_1(%rip), %xmm11
	mulss	(%r15), %xmm7
	xorps	%xmm10, %xmm10
.LBB1_17:
	xorps	%xmm0, %xmm0
.LBB1_18:
	movaps	%xmm8, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm7, %xmm2
	mulss	%xmm9, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm6, %xmm1
	mulss	%xmm10, %xmm1
	subss	%xmm1, %xmm2
	movaps	%xmm6, %xmm3
	mulss	%xmm0, %xmm3
	movaps	%xmm8, %xmm1
	mulss	%xmm10, %xmm1
	addss	%xmm3, %xmm1
	movaps	%xmm11, %xmm3
	mulss	%xmm9, %xmm3
	subss	%xmm3, %xmm1
	movaps	%xmm0, %xmm3
	mulss	%xmm9, %xmm3
	movaps	%xmm6, %xmm4
	mulss	%xmm11, %xmm4
	addss	%xmm3, %xmm4
	movaps	%xmm8, %xmm3
	mulss	%xmm7, %xmm3
	subss	%xmm3, %xmm4
	mulss	%xmm11, %xmm8
	xorps	.LCPI1_1(%rip), %xmm8
	mulss	%xmm7, %xmm6
	subss	%xmm6, %xmm8
	mulss	%xmm10, %xmm9
	subss	%xmm9, %xmm8
	movaps	%xmm8, %xmm3
	mulss	%xmm11, %xmm3
	movaps	%xmm0, %xmm5
	mulss	%xmm2, %xmm5
	subss	%xmm3, %xmm5
	movaps	%xmm1, %xmm3
	mulss	%xmm10, %xmm3
	subss	%xmm3, %xmm5
	movaps	%xmm4, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm5, %xmm3
	movaps	%xmm8, %xmm5
	mulss	%xmm7, %xmm5
	movaps	%xmm0, %xmm6
	mulss	%xmm1, %xmm6
	subss	%xmm5, %xmm6
	mulss	%xmm4, %xmm0
	mulss	%xmm11, %xmm4
	subss	%xmm4, %xmm6
	movaps	%xmm2, %xmm4
	mulss	%xmm10, %xmm4
	addss	%xmm6, %xmm4
	mulss	%xmm10, %xmm8
	subss	%xmm8, %xmm0
	mulss	%xmm7, %xmm2
	subss	%xmm2, %xmm0
	mulss	%xmm11, %xmm1
	addss	%xmm0, %xmm1
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm1, %xmm5
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm7
	mulss	%xmm4, %xmm7
	subss	%xmm7, %xmm5
	mulss	%xmm3, %xmm6
	movaps	%xmm0, %xmm7
	mulss	%xmm1, %xmm7
	subss	%xmm7, %xmm6
	mulss	%xmm4, %xmm0
	mulss	%xmm3, %xmm2
	subss	%xmm2, %xmm0
	movups	(%rbp), %xmm2
	movups	%xmm2, 712(%r12)
	movss	%xmm3, 664(%r12)
	movss	%xmm5, 668(%r12)
	movl	(%rbx), %eax
	movl	%eax, 672(%r12)
	movl	$0, 676(%r12)
	movss	%xmm4, 680(%r12)
	movss	%xmm6, 684(%r12)
	movl	4(%rbx), %eax
	movl	%eax, 688(%r12)
	movl	$0, 692(%r12)
	movss	%xmm1, 696(%r12)
	movss	%xmm0, 700(%r12)
	movl	8(%rbx), %eax
	movl	%eax, 704(%r12)
	movl	$0, 708(%r12)
	movaps	.LCPI1_7(%rip), %xmm0   # xmm0 = [3.000000e-01,1.000000e+00,1.000000e+00,-1.000000e+00]
	movups	%xmm0, 740(%r12)
	movl	$1063675494, 736(%r12)  # imm = 0x3F666666
	movb	$0, 782(%r12)
	cmpb	$0, 784(%r12)
	jne	.LBB1_20
# BB#19:
	movss	.LCPI1_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB1_21
.LBB1_20:
	movss	.LCPI1_8(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB1_21:
	movss	%xmm0, 776(%r12)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK9btVector3S4_RS2_S5_b, .Lfunc_end1-_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK9btVector3S4_RS2_S5_b
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI2_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI2_6:
	.long	1050253722              # float 0.300000012
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	3212836864              # float -1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_1:
	.long	1060439283              # float 0.707106769
.LCPI2_2:
	.long	1065353216              # float 1
.LCPI2_4:
	.long	3212836862              # float -0.99999988
.LCPI2_5:
	.long	1056964608              # float 0.5
.LCPI2_7:
	.long	3212836864              # float -1
	.text
	.globl	_ZN17btHingeConstraintC2ER11btRigidBodyRK9btVector3RS2_b
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraintC2ER11btRigidBodyRK9btVector3RS2_b,@function
_ZN17btHingeConstraintC2ER11btRigidBodyRK9btVector3RS2_b: # @_ZN17btHingeConstraintC2ER11btRigidBodyRK9btVector3RS2_b
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 48
	subq	$112, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 160
.Lcfi21:
	.cfi_offset %rbx, -48
.Lcfi22:
	.cfi_offset %r12, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movq	%rcx, %r12
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$4, %esi
	movq	%r14, %rdx
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody
	movq	$_ZTV17btHingeConstraint+16, (%rbx)
	movb	$0, 780(%rbx)
	movb	$0, 781(%rbx)
	movb	$0, 783(%rbx)
	movb	%bpl, 784(%rbx)
	movss	8(%r12), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	andps	%xmm4, %xmm0
	ucomiss	.LCPI2_1(%rip), %xmm0
	jbe	.LBB2_4
# BB#1:
	movss	4(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm4, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	sqrtss	%xmm4, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB2_3
# BB#2:                                 # %call.sqrt
	movaps	%xmm4, %xmm0
	movaps	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	16(%rsp), %xmm4         # 16-byte Reload
.LBB2_3:                                # %.split
	leaq	4(%r12), %rax
	movss	.LCPI2_2(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm8
	movss	8(%r12), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm5
	movaps	.LCPI2_3(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm5
	mulss	%xmm8, %xmm4
	mulss	4(%r12), %xmm8
	movss	(%r12), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm2, %xmm0
	xorps	%xmm1, %xmm0
	movaps	%xmm5, %xmm3
	mulss	%xmm2, %xmm3
	xorps	%xmm10, %xmm10
	jmp	.LBB2_7
.LBB2_4:
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	xorps	%xmm0, %xmm0
	sqrtss	%xmm3, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB2_6
# BB#5:                                 # %call.sqrt104
	movaps	%xmm3, %xmm0
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	16(%rsp), %xmm3         # 16-byte Reload
.LBB2_6:                                # %.split103
	leaq	4(%r12), %rax
	movss	.LCPI2_2(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm5
	movss	4(%r12), %xmm10         # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm10
	movaps	.LCPI2_3(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm10
	mulss	%xmm5, %xmm3
	mulss	(%r12), %xmm5
	movss	8(%r12), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm0
	mulss	%xmm4, %xmm0
	mulss	%xmm5, %xmm4
	xorps	%xmm1, %xmm4
	xorps	%xmm8, %xmm8
.LBB2_7:
	movups	(%r15), %xmm1
	movups	%xmm1, 648(%rbx)
	movss	%xmm10, 600(%rbx)
	movss	%xmm4, 604(%rbx)
	movl	(%r12), %ecx
	movl	%ecx, 608(%rbx)
	movl	$0, 612(%rbx)
	movss	%xmm5, 616(%rbx)
	movss	%xmm0, 620(%rbx)
	movl	(%rax), %eax
	movl	%eax, 624(%rbx)
	movl	$0, 628(%rbx)
	movss	%xmm8, 632(%rbx)
	movss	%xmm3, 636(%rbx)
	movl	8(%r12), %eax
	movl	%eax, 640(%rbx)
	movl	$0, 644(%rbx)
	movss	(%r12), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm1
	movss	12(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movss	8(%r12), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	16(%r14), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm12
	addss	%xmm2, %xmm12
	movss	24(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm1
	movss	28(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movss	32(%r14), %xmm13        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm13
	addss	%xmm2, %xmm13
	movss	40(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm1
	movss	44(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movss	48(%r14), %xmm14        # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm14
	addss	%xmm2, %xmm14
	movaps	%xmm6, %xmm1
	mulss	%xmm12, %xmm1
	movaps	%xmm0, %xmm2
	mulss	%xmm13, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm7, %xmm1
	mulss	%xmm14, %xmm1
	addss	%xmm2, %xmm1
	movss	.LCPI2_4(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jbe	.LBB2_12
# BB#8:
	movaps	.LCPI2_0(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	andps	%xmm7, %xmm1
	ucomiss	.LCPI2_1(%rip), %xmm1
	jbe	.LBB2_15
# BB#9:
	mulss	%xmm0, %xmm0
	mulss	%xmm7, %xmm7
	addss	%xmm0, %xmm7
	xorps	%xmm0, %xmm0
	sqrtss	%xmm7, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB2_11
# BB#10:                                # %call.sqrt106
	movaps	%xmm7, %xmm0
	movss	%xmm12, 16(%rsp)        # 4-byte Spill
	movaps	%xmm10, 64(%rsp)        # 16-byte Spill
	movss	%xmm13, 12(%rsp)        # 4-byte Spill
	movss	%xmm14, 8(%rsp)         # 4-byte Spill
	movaps	%xmm5, 48(%rsp)         # 16-byte Spill
	movss	%xmm8, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movss	8(%rsp), %xmm14         # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movaps	64(%rsp), %xmm10        # 16-byte Reload
	movss	16(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
.LBB2_11:                               # %.split105
	movss	.LCPI2_2(%rip), %xmm6   # xmm6 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm6
	movss	8(%r12), %xmm7          # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm7
	xorps	.LCPI2_3(%rip), %xmm7
	mulss	4(%r12), %xmm6
	xorps	%xmm11, %xmm11
	jmp	.LBB2_18
.LBB2_12:
	movaps	%xmm0, %xmm11
	mulss	%xmm14, %xmm11
	movaps	%xmm7, %xmm2
	mulss	%xmm13, %xmm2
	subss	%xmm2, %xmm11
	mulss	%xmm12, %xmm7
	movaps	%xmm6, %xmm2
	mulss	%xmm14, %xmm2
	subss	%xmm2, %xmm7
	mulss	%xmm13, %xmm6
	mulss	%xmm12, %xmm0
	subss	%xmm0, %xmm6
	addss	.LCPI2_2(%rip), %xmm1
	addss	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB2_14
# BB#13:                                # %call.sqrt110
	movaps	%xmm1, %xmm0
	movss	%xmm12, 16(%rsp)        # 4-byte Spill
	movaps	%xmm10, 64(%rsp)        # 16-byte Spill
	movss	%xmm13, 12(%rsp)        # 4-byte Spill
	movss	%xmm14, 8(%rsp)         # 4-byte Spill
	movaps	%xmm5, 48(%rsp)         # 16-byte Spill
	movss	%xmm8, 4(%rsp)          # 4-byte Spill
	movaps	%xmm7, 96(%rsp)         # 16-byte Spill
	movss	%xmm6, 44(%rsp)         # 4-byte Spill
	movaps	%xmm11, 80(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	80(%rsp), %xmm11        # 16-byte Reload
	movss	44(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	96(%rsp), %xmm7         # 16-byte Reload
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movss	8(%rsp), %xmm14         # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movaps	64(%rsp), %xmm10        # 16-byte Reload
	movss	16(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
.LBB2_14:                               # %.split109
	movss	.LCPI2_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm11
	mulss	%xmm1, %xmm7
	mulss	%xmm1, %xmm6
	mulss	.LCPI2_5(%rip), %xmm0
	jmp	.LBB2_19
.LBB2_15:
	mulss	%xmm6, %xmm6
	mulss	%xmm0, %xmm0
	addss	%xmm6, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB2_17
# BB#16:                                # %call.sqrt108
	movss	%xmm12, 16(%rsp)        # 4-byte Spill
	movaps	%xmm10, 64(%rsp)        # 16-byte Spill
	movss	%xmm13, 12(%rsp)        # 4-byte Spill
	movss	%xmm14, 8(%rsp)         # 4-byte Spill
	movaps	%xmm5, 48(%rsp)         # 16-byte Spill
	movss	%xmm8, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movss	8(%rsp), %xmm14         # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movaps	64(%rsp), %xmm10        # 16-byte Reload
	movss	16(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB2_17:                               # %.split107
	movss	.LCPI2_2(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm7
	movss	4(%r12), %xmm11         # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm11
	xorps	.LCPI2_3(%rip), %xmm11
	mulss	(%r12), %xmm7
	xorps	%xmm6, %xmm6
.LBB2_18:
	xorps	%xmm0, %xmm0
.LBB2_19:
	movaps	%xmm10, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm8, %xmm4
	mulss	%xmm7, %xmm4
	addss	%xmm1, %xmm4
	movaps	%xmm5, %xmm1
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm4
	movaps	%xmm5, %xmm2
	mulss	%xmm0, %xmm2
	movaps	%xmm10, %xmm1
	mulss	%xmm6, %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm8, %xmm2
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm1
	movaps	%xmm8, %xmm2
	mulss	%xmm0, %xmm2
	movaps	%xmm5, %xmm3
	mulss	%xmm11, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm10, %xmm2
	mulss	%xmm7, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm11, %xmm10
	xorps	.LCPI2_3(%rip), %xmm10
	mulss	%xmm7, %xmm5
	subss	%xmm5, %xmm10
	mulss	%xmm6, %xmm8
	subss	%xmm8, %xmm10
	movaps	%xmm10, %xmm2
	mulss	%xmm11, %xmm2
	movaps	%xmm0, %xmm5
	mulss	%xmm4, %xmm5
	subss	%xmm2, %xmm5
	movaps	%xmm1, %xmm2
	mulss	%xmm6, %xmm2
	subss	%xmm2, %xmm5
	movaps	%xmm3, %xmm9
	mulss	%xmm7, %xmm9
	addss	%xmm5, %xmm9
	movaps	%xmm10, %xmm2
	mulss	%xmm7, %xmm2
	movaps	%xmm0, %xmm5
	mulss	%xmm1, %xmm5
	subss	%xmm2, %xmm5
	mulss	%xmm3, %xmm0
	mulss	%xmm11, %xmm3
	subss	%xmm3, %xmm5
	movaps	%xmm4, %xmm3
	mulss	%xmm6, %xmm3
	addss	%xmm5, %xmm3
	mulss	%xmm6, %xmm10
	subss	%xmm10, %xmm0
	mulss	%xmm7, %xmm4
	subss	%xmm4, %xmm0
	mulss	%xmm11, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm13, %xmm8
	mulss	%xmm1, %xmm8
	movaps	%xmm14, %xmm0
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm8
	movaps	%xmm14, %xmm10
	mulss	%xmm9, %xmm10
	movaps	%xmm12, %xmm0
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm10
	movaps	%xmm12, %xmm11
	mulss	%xmm3, %xmm11
	movaps	%xmm13, %xmm0
	mulss	%xmm9, %xmm0
	subss	%xmm0, %xmm11
	movss	(%r15), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	12(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
	addss	%xmm0, %xmm4
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	16(%r14), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	addss	%xmm4, %xmm7
	addss	56(%r14), %xmm7
	movss	24(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	movss	28(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm5
	addss	%xmm4, %xmm5
	movss	32(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	addss	60(%r14), %xmm4
	mulss	40(%r14), %xmm2
	mulss	44(%r14), %xmm6
	addss	%xmm2, %xmm6
	mulss	48(%r14), %xmm0
	addss	%xmm6, %xmm0
	addss	64(%r14), %xmm0
	unpcklps	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1]
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm7, 712(%rbx)
	movlps	%xmm2, 720(%rbx)
	movss	%xmm9, 664(%rbx)
	movss	%xmm8, 668(%rbx)
	movss	%xmm12, 672(%rbx)
	movl	$0, 676(%rbx)
	movss	%xmm3, 680(%rbx)
	movss	%xmm10, 684(%rbx)
	movss	%xmm13, 688(%rbx)
	movl	$0, 692(%rbx)
	movss	%xmm1, 696(%rbx)
	movss	%xmm11, 700(%rbx)
	movss	%xmm14, 704(%rbx)
	movl	$0, 708(%rbx)
	movaps	.LCPI2_6(%rip), %xmm0   # xmm0 = [3.000000e-01,1.000000e+00,1.000000e+00,-1.000000e+00]
	movups	%xmm0, 740(%rbx)
	movl	$1063675494, 736(%rbx)  # imm = 0x3F666666
	movb	$0, 782(%rbx)
	cmpb	$0, 784(%rbx)
	jne	.LBB2_21
# BB#20:
	movss	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB2_22
.LBB2_21:
	movss	.LCPI2_7(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB2_22:
	movss	%xmm0, 776(%rbx)
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN17btHingeConstraintC2ER11btRigidBodyRK9btVector3RS2_b, .Lfunc_end2-_ZN17btHingeConstraintC2ER11btRigidBodyRK9btVector3RS2_b
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	1063675494              # float 0.899999976
	.long	1050253722              # float 0.300000012
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_1:
	.long	3212836864              # float -1
.LCPI3_2:
	.long	1065353216              # float 1
	.text
	.globl	_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b,@function
_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b: # @_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movq	%r8, %r15
	movq	%rcx, %rbp
	movq	%rdx, %rax
	movq	%rsi, %rcx
	movq	%rdi, %rbx
	movl	$4, %esi
	movq	%rcx, %rdx
	movq	%rax, %rcx
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_
	movq	$_ZTV17btHingeConstraint+16, (%rbx)
	movups	(%rbp), %xmm0
	movups	%xmm0, 600(%rbx)
	movups	16(%rbp), %xmm0
	movups	%xmm0, 616(%rbx)
	movups	32(%rbp), %xmm0
	movups	%xmm0, 632(%rbx)
	movups	48(%rbp), %xmm0
	movups	%xmm0, 648(%rbx)
	movups	(%r15), %xmm0
	movups	%xmm0, 664(%rbx)
	movups	16(%r15), %xmm0
	movups	%xmm0, 680(%rbx)
	movups	32(%r15), %xmm0
	movups	%xmm0, 696(%rbx)
	movups	48(%r15), %xmm0
	movups	%xmm0, 712(%rbx)
	movb	$0, 780(%rbx)
	movb	$0, 781(%rbx)
	movb	$0, 783(%rbx)
	movb	%r14b, 784(%rbx)
	movl	$-1082130432, 752(%rbx) # imm = 0xBF800000
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [9.000000e-01,3.000000e-01,1.000000e+00,1.000000e+00]
	movups	%xmm0, 736(%rbx)
	movb	$0, 782(%rbx)
	testb	%r14b, %r14b
	jne	.LBB3_1
# BB#2:
	movss	.LCPI3_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB3_3
.LBB3_1:
	movss	.LCPI3_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB3_3:
	movss	%xmm0, 776(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b, .Lfunc_end3-_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	1063675494              # float 0.899999976
	.long	1050253722              # float 0.300000012
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_1:
	.long	3212836864              # float -1
.LCPI4_2:
	.long	1065353216              # float 1
	.text
	.globl	_ZN17btHingeConstraintC2ER11btRigidBodyRK11btTransformb
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraintC2ER11btRigidBodyRK11btTransformb,@function
_ZN17btHingeConstraintC2ER11btRigidBodyRK11btTransformb: # @_ZN17btHingeConstraintC2ER11btRigidBodyRK11btTransformb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -32
.Lcfi39:
	.cfi_offset %r14, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %rbp
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movl	$4, %esi
	movq	%rax, %rdx
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody
	movq	$_ZTV17btHingeConstraint+16, (%rbx)
	movups	(%rbp), %xmm0
	movups	%xmm0, 600(%rbx)
	movups	16(%rbp), %xmm0
	movups	%xmm0, 616(%rbx)
	movups	32(%rbp), %xmm0
	movups	%xmm0, 632(%rbx)
	movups	48(%rbp), %xmm0
	movups	%xmm0, 648(%rbx)
	movups	(%rbp), %xmm0
	movups	%xmm0, 664(%rbx)
	movups	16(%rbp), %xmm0
	movups	%xmm0, 680(%rbx)
	movups	32(%rbp), %xmm0
	movups	%xmm0, 696(%rbx)
	movups	48(%rbp), %xmm0
	movups	%xmm0, 712(%rbx)
	movb	$0, 780(%rbx)
	movb	$0, 781(%rbx)
	movb	$0, 783(%rbx)
	movb	%r14b, 784(%rbx)
	movq	24(%rbx), %rax
	movss	648(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	652(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	656(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	24(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	28(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	32(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	movsd	56(%rax), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm3, %xmm4
	mulss	40(%rax), %xmm2
	mulss	44(%rax), %xmm1
	addss	%xmm2, %xmm1
	mulss	48(%rax), %xmm0
	addss	%xmm1, %xmm0
	addss	64(%rax), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm4, 712(%rbx)
	movlps	%xmm1, 720(%rbx)
	movl	$-1082130432, 752(%rbx) # imm = 0xBF800000
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [9.000000e-01,3.000000e-01,1.000000e+00,1.000000e+00]
	movups	%xmm0, 736(%rbx)
	movb	$0, 782(%rbx)
	testb	%r14b, %r14b
	jne	.LBB4_1
# BB#2:
	movss	.LCPI4_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB4_3
.LBB4_1:
	movss	.LCPI4_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB4_3:
	movss	%xmm0, 776(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN17btHingeConstraintC2ER11btRigidBodyRK11btTransformb, .Lfunc_end4-_ZN17btHingeConstraintC2ER11btRigidBodyRK11btTransformb
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	872415232               # float 1.1920929E-7
.LCPI5_1:
	.long	1065353216              # float 1
.LCPI5_3:
	.long	1060439283              # float 0.707106769
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_2:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI5_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN17btHingeConstraint13buildJacobianEv
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraint13buildJacobianEv,@function
_ZN17btHingeConstraint13buildJacobianEv: # @_ZN17btHingeConstraint13buildJacobianEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi47:
	.cfi_def_cfa_offset 368
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpb	$0, 783(%r14)
	je	.LBB5_30
# BB#1:
	movl	$0, 40(%r14)
	movl	$0, 788(%r14)
	cmpb	$0, 780(%r14)
	jne	.LBB5_16
# BB#2:
	movss	648(%r14), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	652(%r14), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movss	656(%r14), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movq	24(%r14), %rax
	movq	32(%r14), %rcx
	movss	28(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm11, %xmm14
	shufps	$224, %xmm14, %xmm14    # xmm14 = xmm14[0,0,2,3]
	mulps	%xmm4, %xmm14
	movss	32(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm10, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	movsd	56(%rax), %xmm8         # xmm8 = mem[0],zero
	mulss	44(%rax), %xmm11
	mulss	48(%rax), %xmm10
	movss	712(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	716(%r14), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	720(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	28(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rcx), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movss	12(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	movaps	%xmm7, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm5, %xmm0
	movss	32(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	16(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1]
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm1, %xmm5
	movsd	56(%rcx), %xmm9         # xmm9 = mem[0],zero
	movss	40(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	mulss	44(%rcx), %xmm7
	addss	%xmm1, %xmm7
	mulss	48(%rcx), %xmm4
	addss	%xmm7, %xmm4
	addss	64(%rcx), %xmm4
	movss	24(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm12   # xmm12 = xmm12[0],xmm1[0],xmm12[1],xmm1[1]
	movaps	%xmm6, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm12, %xmm7
	addps	%xmm14, %xmm7
	addps	%xmm2, %xmm7
	addps	%xmm8, %xmm7
	movss	24(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm13   # xmm13 = xmm13[0],xmm1[0],xmm13[1],xmm1[1]
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm13, %xmm3
	addps	%xmm0, %xmm3
	addps	%xmm5, %xmm3
	addps	%xmm9, %xmm3
	mulss	40(%rax), %xmm6
	addss	%xmm11, %xmm6
	addss	%xmm10, %xmm6
	addss	64(%rax), %xmm6
	movaps	%xmm3, %xmm2
	subps	%xmm7, %xmm2
	movaps	%xmm2, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm4, %xmm5
	subss	%xmm6, %xmm5
	movaps	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	.LCPI5_0(%rip), %xmm0
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	movaps	%xmm4, 32(%rsp)         # 16-byte Spill
	movaps	%xmm7, 16(%rsp)         # 16-byte Spill
	jbe	.LBB5_6
# BB#3:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB5_5
# BB#4:                                 # %call.sqrt
	movaps	%xmm5, (%rsp)           # 16-byte Spill
	movaps	%xmm2, 224(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	224(%rsp), %xmm2        # 16-byte Reload
	movaps	(%rsp), %xmm5           # 16-byte Reload
	movaps	16(%rsp), %xmm7         # 16-byte Reload
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	movaps	64(%rsp), %xmm6         # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB5_5:                                # %.split
	movss	.LCPI5_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm5
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1,2,3]
	movlps	%xmm2, 80(%rsp)
	movaps	%xmm2, %xmm9
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	movlps	%xmm0, 88(%rsp)
	jmp	.LBB5_7
.LBB5_6:
	movq	$1065353216, 80(%rsp)   # imm = 0x3F800000
	movq	$0, 88(%rsp)
	movss	.LCPI5_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm9, %xmm9
	xorps	%xmm5, %xmm5
.LBB5_7:
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	andps	%xmm5, %xmm0
	ucomiss	.LCPI5_3(%rip), %xmm0
	jbe	.LBB5_11
# BB#8:
	mulss	%xmm9, %xmm9
	mulss	%xmm5, %xmm5
	addss	%xmm9, %xmm5
	xorps	%xmm0, %xmm0
	sqrtss	%xmm5, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB5_10
# BB#9:                                 # %call.sqrt417
	movaps	%xmm5, %xmm0
	movaps	%xmm5, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movaps	(%rsp), %xmm5           # 16-byte Reload
	movaps	16(%rsp), %xmm7         # 16-byte Reload
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	movaps	64(%rsp), %xmm6         # 16-byte Reload
.LBB5_10:                               # %.split416
	movss	.LCPI5_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movss	88(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movaps	.LCPI5_4(%rip), %xmm8   # xmm8 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm8, %xmm2
	mulss	%xmm1, %xmm5
	mulss	84(%rsp), %xmm1
	movl	$0, 96(%rsp)
	movss	%xmm1, 104(%rsp)
	movss	80(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	xorps	%xmm8, %xmm1
	movaps	%xmm2, %xmm9
	mulss	%xmm0, %xmm9
	jmp	.LBB5_14
.LBB5_11:
	mulss	%xmm2, %xmm2
	mulss	%xmm9, %xmm9
	addss	%xmm2, %xmm9
	xorps	%xmm0, %xmm0
	sqrtss	%xmm9, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB5_13
# BB#12:                                # %call.sqrt419
	movaps	%xmm9, %xmm0
	movaps	%xmm9, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movaps	(%rsp), %xmm9           # 16-byte Reload
	movaps	16(%rsp), %xmm7         # 16-byte Reload
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	movaps	64(%rsp), %xmm6         # 16-byte Reload
.LBB5_13:                               # %.split418
	movss	.LCPI5_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movss	84(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	movaps	.LCPI5_4(%rip), %xmm8   # xmm8 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm8, %xmm1
	mulss	%xmm2, %xmm9
	mulss	80(%rsp), %xmm2
	movss	%xmm1, 96(%rsp)
	movl	$0, 104(%rsp)
	movss	88(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm0, %xmm5
	xorps	%xmm8, %xmm5
	mulss	%xmm0, %xmm1
.LBB5_14:                               # %_Z13btPlaneSpace1RK9btVector3RS_S2_.exit152
	movss	%xmm2, 100(%rsp)
	movl	$0, 108(%rsp)
	movss	%xmm5, 112(%rsp)
	movss	%xmm1, 116(%rsp)
	movss	%xmm9, 120(%rsp)
	movl	$0, 124(%rsp)
	leaq	96(%r14), %rbx
	xorl	%r15d, %r15d
	leaq	256(%rsp), %r13
	leaq	240(%rsp), %r12
	.p2align	4, 0x90
.LBB5_15:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rcx
	movl	8(%rcx), %eax
	movl	%eax, 176(%rsp)
	movl	24(%rcx), %eax
	movl	%eax, 180(%rsp)
	movl	40(%rcx), %eax
	movl	%eax, 184(%rsp)
	movl	$0, 188(%rsp)
	movl	12(%rcx), %eax
	movl	%eax, 192(%rsp)
	movl	28(%rcx), %eax
	movl	%eax, 196(%rsp)
	movl	44(%rcx), %eax
	movl	%eax, 200(%rsp)
	movl	$0, 204(%rsp)
	movl	16(%rcx), %eax
	movl	%eax, 208(%rsp)
	movl	32(%rcx), %eax
	movl	%eax, 212(%rsp)
	movl	48(%rcx), %eax
	movl	%eax, 216(%rsp)
	movl	$0, 220(%rsp)
	movq	32(%r14), %rax
	movl	8(%rax), %edx
	movl	%edx, 128(%rsp)
	movl	24(%rax), %edx
	movl	%edx, 132(%rsp)
	movl	40(%rax), %edx
	movl	%edx, 136(%rsp)
	movl	$0, 140(%rsp)
	movl	12(%rax), %edx
	movl	%edx, 144(%rsp)
	movl	28(%rax), %edx
	movl	%edx, 148(%rsp)
	movl	44(%rax), %edx
	movl	%edx, 152(%rsp)
	movl	$0, 156(%rsp)
	movl	16(%rax), %edx
	movl	%edx, 160(%rsp)
	movl	32(%rax), %edx
	movl	%edx, 164(%rsp)
	movl	48(%rax), %edx
	movl	%edx, 168(%rsp)
	movl	$0, 172(%rsp)
	movsd	56(%rcx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm7, %xmm1
	subps	%xmm0, %xmm1
	movaps	%xmm6, %xmm0
	subss	64(%rcx), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 256(%rsp)
	movlps	%xmm2, 264(%rsp)
	movsd	56(%rax), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm3, %xmm1
	subps	%xmm0, %xmm1
	movaps	%xmm4, %xmm0
	subss	64(%rax), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 240(%rsp)
	movlps	%xmm2, 248(%rsp)
	leaq	80(%rsp,%r15), %r9
	movq	24(%r14), %rbp
	movss	360(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addq	$428, %rbp              # imm = 0x1AC
	movss	360(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addq	$428, %rax              # imm = 0x1AC
	movq	%rbx, %rdi
	leaq	176(%rsp), %rsi
	leaq	128(%rsp), %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	pushq	%rax
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	callq	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	movaps	32(%rsp), %xmm7         # 16-byte Reload
	movaps	48(%rsp), %xmm4         # 16-byte Reload
	movaps	64(%rsp), %xmm3         # 16-byte Reload
	movaps	80(%rsp), %xmm6         # 16-byte Reload
	addq	$16, %rsp
.Lcfi56:
	.cfi_adjust_cfa_offset -16
	addq	$16, %r15
	addq	$84, %rbx
	cmpq	$48, %r15
	jne	.LBB5_15
.LBB5_16:
	movss	608(%r14), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movss	624(%r14), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movss	640(%r14), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	andps	%xmm5, %xmm0
	ucomiss	.LCPI5_3(%rip), %xmm0
	jbe	.LBB5_20
# BB#17:
	movaps	%xmm9, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm5, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	sqrtss	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB5_19
# BB#18:                                # %call.sqrt421
	movaps	%xmm2, %xmm0
	movaps	%xmm9, 32(%rsp)         # 16-byte Spill
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
	movaps	%xmm2, 48(%rsp)         # 16-byte Spill
	movss	%xmm10, (%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	(%rsp), %xmm10          # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm2         # 16-byte Reload
	movaps	16(%rsp), %xmm5         # 16-byte Reload
	movaps	32(%rsp), %xmm9         # 16-byte Reload
.LBB5_19:                               # %.split420
	movss	.LCPI5_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm5
	movaps	.LCPI5_4(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm5
	mulss	%xmm1, %xmm9
	mulss	%xmm1, %xmm2
	movaps	%xmm2, 48(%rsp)         # 16-byte Spill
	movaps	%xmm10, %xmm15
	mulss	%xmm9, %xmm15
	xorps	%xmm0, %xmm15
	movaps	%xmm5, %xmm0
	mulss	%xmm10, %xmm0
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	xorps	%xmm12, %xmm12
	movaps	%xmm5, %xmm10
	jmp	.LBB5_23
.LBB5_20:
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm9, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	sqrtss	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB5_22
# BB#21:                                # %call.sqrt423
	movaps	%xmm2, %xmm0
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	movaps	%xmm9, 32(%rsp)         # 16-byte Spill
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
	movss	%xmm10, (%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	(%rsp), %xmm10          # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm5         # 16-byte Reload
	movaps	32(%rsp), %xmm9         # 16-byte Reload
	movaps	64(%rsp), %xmm2         # 16-byte Reload
.LBB5_22:                               # %.split422
	movss	.LCPI5_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm9
	movaps	.LCPI5_4(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm9
	mulss	%xmm1, %xmm10
	movaps	%xmm5, %xmm3
	mulss	%xmm10, %xmm3
	xorps	%xmm0, %xmm3
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	movaps	%xmm9, %xmm15
	mulss	%xmm5, %xmm15
	mulss	%xmm1, %xmm2
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	movaps	%xmm9, %xmm12
	xorps	%xmm9, %xmm9
.LBB5_23:                               # %_Z13btPlaneSpace1RK9btVector3RS_S2_.exit
	movss	608(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	624(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	640(%r14), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movq	24(%r14), %rax
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	movaps	%xmm1, 224(%rsp)        # 16-byte Spill
	movaps	%xmm4, %xmm0
	mulss	%xmm1, %xmm0
	pshufd	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm2, %xmm5
	mulss	%xmm1, %xmm5
	addss	%xmm0, %xmm5
	movss	16(%rax), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm13, %xmm0
	addss	%xmm5, %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movsd	24(%rax), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm4, %xmm0
	mulss	%xmm3, %xmm0
	pshufd	$229, %xmm3, %xmm5      # xmm5 = xmm3[1,1,2,3]
	movaps	%xmm3, 272(%rsp)        # 16-byte Spill
	movaps	%xmm2, %xmm7
	mulss	%xmm5, %xmm7
	addss	%xmm0, %xmm7
	movss	32(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm6
	mulss	%xmm0, %xmm6
	addss	%xmm7, %xmm6
	movaps	%xmm6, 16(%rsp)         # 16-byte Spill
	movsd	40(%rax), %xmm14        # xmm14 = mem[0],zero
	mulss	%xmm14, %xmm4
	pshufd	$229, %xmm14, %xmm8     # xmm8 = xmm14[1,1,2,3]
	mulss	%xmm8, %xmm2
	movaps	%xmm8, 288(%rsp)        # 16-byte Spill
	addss	%xmm4, %xmm2
	movss	48(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm11
	addss	%xmm2, %xmm11
	movaps	%xmm11, (%rsp)          # 16-byte Spill
	movaps	%xmm12, %xmm2
	movaps	224(%rsp), %xmm11       # 16-byte Reload
	mulss	%xmm11, %xmm2
	movaps	%xmm10, %xmm6
	mulss	%xmm1, %xmm6
	addss	%xmm2, %xmm6
	movaps	%xmm9, %xmm4
	mulss	%xmm13, %xmm4
	addss	%xmm6, %xmm4
	movaps	%xmm12, %xmm2
	mulss	%xmm3, %xmm2
	movaps	%xmm10, %xmm6
	mulss	%xmm5, %xmm6
	addss	%xmm2, %xmm6
	movaps	%xmm9, %xmm2
	mulss	%xmm0, %xmm2
	addss	%xmm6, %xmm2
	mulss	%xmm14, %xmm12
	mulss	%xmm8, %xmm10
	addss	%xmm12, %xmm10
	mulss	%xmm7, %xmm9
	addss	%xmm10, %xmm9
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm3, %xmm6
	mulss	%xmm11, %xmm6
	mulss	%xmm15, %xmm1
	addss	%xmm6, %xmm1
	movaps	64(%rsp), %xmm8         # 16-byte Reload
	movaps	%xmm8, %xmm6
	mulss	%xmm13, %xmm6
	addss	%xmm1, %xmm6
	movaps	%xmm3, %xmm1
	movaps	272(%rsp), %xmm12       # 16-byte Reload
	mulss	%xmm12, %xmm1
	mulss	%xmm15, %xmm5
	addss	%xmm1, %xmm5
	movaps	%xmm8, %xmm10
	mulss	%xmm0, %xmm10
	addss	%xmm5, %xmm10
	mulss	288(%rsp), %xmm15       # 16-byte Folded Reload
	mulss	%xmm14, %xmm3
	addss	%xmm3, %xmm15
	movq	32(%r14), %rcx
	mulss	%xmm7, %xmm8
	addss	%xmm15, %xmm8
	movaps	%xmm4, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm11, %xmm1
	movaps	%xmm2, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm12, %xmm5
	movsd	8(%rcx), %xmm15         # xmm15 = mem[0],zero
	addps	%xmm1, %xmm5
	movaps	%xmm9, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm14, %xmm3
	movsd	24(%rcx), %xmm12        # xmm12 = mem[0],zero
	addps	%xmm5, %xmm3
	movsd	40(%rcx), %xmm14        # xmm14 = mem[0],zero
	mulss	%xmm4, %xmm13
	mulss	%xmm2, %xmm0
	addss	%xmm13, %xmm0
	movss	16(%rcx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm7
	addss	%xmm0, %xmm7
	movss	32(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	48(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	movups	%xmm0, 348(%r14)
	xorps	%xmm0, %xmm0
	movss	%xmm7, %xmm0            # xmm0 = xmm7[0],xmm0[1,2,3]
	movlps	%xmm3, 364(%r14)
	movlps	%xmm0, 372(%r14)
	movaps	.LCPI5_4(%rip), %xmm11  # xmm11 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm11, %xmm4
	pshufd	$224, %xmm4, %xmm0      # xmm0 = xmm4[0,0,2,3]
	mulps	%xmm15, %xmm0
	mulss	%xmm2, %xmm1
	xorps	%xmm11, %xmm2
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm12, %xmm2
	addps	%xmm0, %xmm2
	mulss	%xmm9, %xmm5
	xorps	%xmm11, %xmm9
	pshufd	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	mulps	%xmm14, %xmm9
	addps	%xmm2, %xmm9
	mulss	%xmm13, %xmm4
	subss	%xmm1, %xmm4
	subss	%xmm5, %xmm4
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movlps	%xmm9, 380(%r14)
	movlps	%xmm1, 388(%r14)
	movsd	428(%rax), %xmm1        # xmm1 = mem[0],zero
	mulps	%xmm3, %xmm1
	movss	436(%rax), %xmm13       # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm13
	xorps	%xmm5, %xmm5
	movss	%xmm13, %xmm5           # xmm5 = xmm13[0],xmm5[1,2,3]
	movlps	%xmm1, 396(%r14)
	movlps	%xmm5, 404(%r14)
	movsd	428(%rcx), %xmm5        # xmm5 = mem[0],zero
	mulps	%xmm9, %xmm5
	movss	436(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm5, 412(%r14)
	movlps	%xmm2, 420(%r14)
	movaps	%xmm1, %xmm2
	mulss	%xmm3, %xmm1
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	mulss	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm5, %xmm1
	mulss	%xmm9, %xmm5
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	%xmm7, %xmm13
	addss	%xmm2, %xmm13
	mulss	%xmm9, %xmm1
	addss	%xmm5, %xmm1
	mulss	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	addss	%xmm13, %xmm0
	movss	%xmm0, 428(%r14)
	movq	24(%r14), %rcx
	movq	32(%r14), %rax
	movsd	8(%rcx), %xmm1          # xmm1 = mem[0],zero
	movsd	24(%rcx), %xmm0         # xmm0 = mem[0],zero
	movsd	40(%rcx), %xmm2         # xmm2 = mem[0],zero
	movsd	8(%rax), %xmm5          # xmm5 = mem[0],zero
	movsd	24(%rax), %xmm14        # xmm14 = mem[0],zero
	movsd	40(%rax), %xmm13        # xmm13 = mem[0],zero
	movss	16(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm7
	movaps	%xmm6, %xmm4
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm1, %xmm6
	movaps	%xmm10, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	movss	32(%rcx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm9
	addps	%xmm6, %xmm1
	movaps	%xmm8, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm2, %xmm3
	movss	48(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm6
	addps	%xmm1, %xmm3
	movss	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm9
	movss	32(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm9
	movss	48(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	xorps	%xmm12, %xmm12
	movups	%xmm12, 432(%r14)
	xorps	%xmm1, %xmm1
	movss	%xmm9, %xmm1            # xmm1 = xmm9[0],xmm1[1,2,3]
	movlps	%xmm3, 448(%r14)
	movlps	%xmm1, 456(%r14)
	xorps	%xmm11, %xmm4
	mulss	%xmm4, %xmm2
	pshufd	$224, %xmm4, %xmm1      # xmm1 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm1
	mulss	%xmm10, %xmm7
	xorps	%xmm11, %xmm10
	pshufd	$224, %xmm10, %xmm4     # xmm4 = xmm10[0,0,2,3]
	mulps	%xmm14, %xmm4
	mulss	%xmm8, %xmm6
	xorps	%xmm11, %xmm8
	addps	%xmm1, %xmm4
	pshufd	$224, %xmm8, %xmm1      # xmm1 = xmm8[0,0,2,3]
	mulps	%xmm13, %xmm1
	addps	%xmm4, %xmm1
	subss	%xmm7, %xmm2
	subss	%xmm6, %xmm2
	xorps	%xmm4, %xmm4
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
	movlps	%xmm1, 464(%r14)
	movlps	%xmm4, 472(%r14)
	movsd	428(%rcx), %xmm5        # xmm5 = mem[0],zero
	mulps	%xmm3, %xmm5
	movss	436(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm4
	xorps	%xmm6, %xmm6
	movss	%xmm4, %xmm6            # xmm6 = xmm4[0],xmm6[1,2,3]
	movlps	%xmm5, 480(%r14)
	movlps	%xmm6, 488(%r14)
	movsd	428(%rax), %xmm6        # xmm6 = mem[0],zero
	mulps	%xmm1, %xmm6
	movss	436(%rax), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm7
	xorps	%xmm0, %xmm0
	movss	%xmm7, %xmm0            # xmm0 = xmm7[0],xmm0[1,2,3]
	movlps	%xmm6, 496(%r14)
	movlps	%xmm0, 504(%r14)
	movaps	%xmm5, %xmm0
	mulss	%xmm3, %xmm5
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm3, %xmm0
	movaps	%xmm6, %xmm3
	mulss	%xmm1, %xmm6
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	addss	%xmm5, %xmm0
	mulss	%xmm9, %xmm4
	addss	%xmm0, %xmm4
	mulss	%xmm1, %xmm3
	addss	%xmm6, %xmm3
	mulss	%xmm2, %xmm7
	addss	%xmm3, %xmm7
	addss	%xmm4, %xmm7
	movss	%xmm7, 512(%r14)
	movq	24(%r14), %rcx
	movq	32(%r14), %rax
	movsd	8(%rcx), %xmm3          # xmm3 = mem[0],zero
	movsd	24(%rcx), %xmm1         # xmm1 = mem[0],zero
	movsd	40(%rcx), %xmm2         # xmm2 = mem[0],zero
	movsd	8(%rax), %xmm10         # xmm10 = mem[0],zero
	movsd	24(%rax), %xmm9         # xmm9 = mem[0],zero
	movsd	40(%rax), %xmm8         # xmm8 = mem[0],zero
	movss	16(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	mulss	%xmm4, %xmm7
	movss	32(%rcx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	mulss	%xmm0, %xmm13
	movaps	%xmm4, %xmm14
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	movaps	%xmm4, %xmm6
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movss	48(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	(%rsp), %xmm5           # 16-byte Reload
	mulss	%xmm5, %xmm4
	addps	%xmm6, %xmm3
	movaps	%xmm5, %xmm1
	movaps	%xmm5, %xmm6
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movss	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	addps	%xmm3, %xmm1
	movss	32(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm13
	movss	48(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movups	%xmm12, 516(%r14)
	addss	%xmm4, %xmm13
	xorps	%xmm4, %xmm4
	movss	%xmm13, %xmm4           # xmm4 = xmm13[0],xmm4[1,2,3]
	movlps	%xmm1, 532(%r14)
	movlps	%xmm4, 540(%r14)
	xorps	%xmm11, %xmm14
	mulss	%xmm0, %xmm3
	mulss	%xmm6, %xmm7
	xorps	%xmm11, %xmm0
	xorps	%xmm11, %xmm6
	mulss	%xmm14, %xmm2
	pshufd	$224, %xmm14, %xmm4     # xmm4 = xmm14[0,0,2,3]
	mulps	%xmm10, %xmm4
	pshufd	$224, %xmm0, %xmm5      # xmm5 = xmm0[0,0,2,3]
	mulps	%xmm9, %xmm5
	addps	%xmm4, %xmm5
	pshufd	$224, %xmm6, %xmm4      # xmm4 = xmm6[0,0,2,3]
	mulps	%xmm8, %xmm4
	addps	%xmm5, %xmm4
	subss	%xmm3, %xmm2
	subss	%xmm7, %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movlps	%xmm4, 548(%r14)
	movlps	%xmm3, 556(%r14)
	movsd	428(%rcx), %xmm0        # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movss	436(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm3
	xorps	%xmm6, %xmm6
	movss	%xmm3, %xmm6            # xmm6 = xmm3[0],xmm6[1,2,3]
	movlps	%xmm0, 564(%r14)
	movlps	%xmm6, 572(%r14)
	movsd	428(%rax), %xmm6        # xmm6 = mem[0],zero
	mulps	%xmm4, %xmm6
	movss	436(%rax), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm7
	xorps	%xmm5, %xmm5
	movss	%xmm7, %xmm5            # xmm5 = xmm7[0],xmm5[1,2,3]
	movlps	%xmm6, 580(%r14)
	movlps	%xmm5, 588(%r14)
	movaps	%xmm0, %xmm5
	mulss	%xmm1, %xmm0
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	mulss	%xmm1, %xmm5
	addss	%xmm0, %xmm5
	mulss	%xmm13, %xmm3
	addss	%xmm5, %xmm3
	movaps	%xmm6, %xmm0
	mulss	%xmm4, %xmm6
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm4, %xmm0
	addss	%xmm6, %xmm0
	mulss	%xmm2, %xmm7
	addss	%xmm0, %xmm7
	addss	%xmm3, %xmm7
	movss	%xmm7, 596(%r14)
	movl	$0, 768(%r14)
	movq	24(%r14), %rsi
	movq	32(%r14), %rdx
	addq	$8, %rsi
	addq	$8, %rdx
	movq	%r14, %rdi
	callq	_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_
	movss	%xmm0, 772(%r14)
	movl	$0, 764(%r14)
	movl	$0, 760(%r14)
	movb	$0, 782(%r14)
	movss	748(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	752(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jb	.LBB5_29
# BB#24:
	callq	_Z21btAdjustAngleToLimitsfff
	movss	%xmm0, 772(%r14)
	movss	748(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB5_25
# BB#26:
	movss	752(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jb	.LBB5_29
# BB#27:
	subss	%xmm0, %xmm1
	movss	%xmm1, 764(%r14)
	movl	$-1082130432, 760(%r14) # imm = 0xBF800000
	jmp	.LBB5_28
.LBB5_25:
	subss	%xmm0, %xmm1
	movss	%xmm1, 764(%r14)
	movl	$1065353216, 760(%r14)  # imm = 0x3F800000
.LBB5_28:                               # %_ZN17btHingeConstraint9testLimitERK11btTransformS2_.exit
	movb	$1, 782(%r14)
.LBB5_29:                               # %_ZN17btHingeConstraint9testLimitERK11btTransformS2_.exit
	movss	608(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	624(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	640(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movq	24(%r14), %rcx
	movq	32(%r14), %rax
	movss	8(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	12(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	movss	16(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	24(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	movss	28(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm2, %xmm5
	movss	32(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm5, %xmm2
	mulss	40(%rcx), %xmm4
	mulss	44(%rcx), %xmm3
	addss	%xmm4, %xmm3
	mulss	48(%rcx), %xmm0
	addss	%xmm3, %xmm0
	movss	280(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	296(%rcx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	312(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	movss	284(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	300(%rcx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	316(%rcx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	addss	%xmm5, %xmm6
	movss	288(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	304(%rcx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	320(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm5, %xmm3
	mulss	%xmm1, %xmm4
	mulss	%xmm2, %xmm6
	addss	%xmm4, %xmm6
	mulss	%xmm0, %xmm3
	addss	%xmm6, %xmm3
	movss	280(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	movss	296(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm4, %xmm5
	movss	312(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	movss	284(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	movss	300(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	addss	%xmm5, %xmm6
	movss	316(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	addss	%xmm6, %xmm5
	movss	288(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	movss	304(%rax), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm7
	addss	%xmm6, %xmm7
	movss	320(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	addss	%xmm7, %xmm6
	mulss	%xmm1, %xmm4
	mulss	%xmm2, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm0, %xmm6
	addss	%xmm5, %xmm6
	addss	%xmm3, %xmm6
	movss	.LCPI5_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm6, %xmm0
	movss	%xmm0, 756(%r14)
.LBB5_30:
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN17btHingeConstraint13buildJacobianEv, .Lfunc_end5-_ZN17btHingeConstraint13buildJacobianEv
	.cfi_endproc

	.section	.text._ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,"axG",@progbits,_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,comdat
	.weak	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.p2align	4, 0x90
	.type	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,@function
_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f: # @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.cfi_startproc
# BB#0:
	movq	16(%rsp), %r10
	movq	8(%rsp), %rax
	movups	(%r9), %xmm2
	movups	%xmm2, (%rdi)
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm7
	mulss	%xmm11, %xmm7
	movss	8(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	(%rdi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm7
	mulss	%xmm9, %xmm5
	movaps	%xmm11, %xmm4
	mulss	%xmm2, %xmm4
	subss	%xmm4, %xmm5
	mulss	%xmm10, %xmm2
	mulss	%xmm9, %xmm6
	subss	%xmm6, %xmm2
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	16(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1]
	mulps	%xmm4, %xmm6
	movss	20(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm6, %xmm4
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm12         # xmm12 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm12   # xmm12 = xmm12[0],xmm8[0],xmm12[1],xmm8[1]
	mulps	%xmm3, %xmm12
	addps	%xmm4, %xmm12
	mulss	32(%rsi), %xmm7
	mulss	36(%rsi), %xmm5
	addss	%xmm7, %xmm5
	mulss	40(%rsi), %xmm2
	addss	%xmm5, %xmm2
	xorps	%xmm8, %xmm8
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movlps	%xmm12, 16(%rdi)
	movlps	%xmm3, 24(%rdi)
	movss	(%r8), %xmm3            # xmm3 = mem[0],zero,zero,zero
	movss	4(%r8), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	mulss	%xmm11, %xmm5
	movss	8(%r8), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm6
	mulss	%xmm10, %xmm6
	subss	%xmm5, %xmm6
	mulss	%xmm9, %xmm7
	mulss	%xmm3, %xmm11
	subss	%xmm7, %xmm11
	mulss	%xmm10, %xmm3
	mulss	%xmm9, %xmm4
	subss	%xmm3, %xmm4
	movss	16(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	movss	20(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm9    # xmm9 = xmm9[0],xmm5[0],xmm9[1],xmm5[1]
	movaps	%xmm11, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm9, %xmm7
	addps	%xmm3, %xmm7
	movaps	%xmm4, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rdx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm5    # xmm5 = xmm5[0],xmm9[0],xmm5[1],xmm9[1]
	mulps	%xmm3, %xmm5
	addps	%xmm7, %xmm5
	mulss	32(%rdx), %xmm6
	mulss	36(%rdx), %xmm11
	addss	%xmm6, %xmm11
	mulss	40(%rdx), %xmm4
	addss	%xmm11, %xmm4
	xorps	%xmm3, %xmm3
	movss	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1,2,3]
	movlps	%xmm5, 32(%rdi)
	movlps	%xmm3, 40(%rdi)
	movsd	(%rax), %xmm9           # xmm9 = mem[0],zero
	mulps	%xmm12, %xmm9
	mulss	8(%rax), %xmm2
	xorps	%xmm6, %xmm6
	movss	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1,2,3]
	movlps	%xmm9, 48(%rdi)
	movlps	%xmm6, 56(%rdi)
	movsd	(%r10), %xmm6           # xmm6 = mem[0],zero
	mulps	%xmm5, %xmm6
	mulss	8(%r10), %xmm4
	movaps	%xmm6, %xmm7
	movlps	%xmm6, 64(%rdi)
	mulss	%xmm5, %xmm6
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movss	%xmm4, %xmm8            # xmm8 = xmm4[0],xmm8[1,2,3]
	movlps	%xmm8, 72(%rdi)
	movss	16(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	mulss	20(%rdi), %xmm9
	addss	%xmm3, %xmm9
	mulss	24(%rdi), %xmm2
	addss	%xmm9, %xmm2
	addss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm5, %xmm7
	addss	%xmm6, %xmm7
	mulss	40(%rdi), %xmm4
	addss	%xmm7, %xmm4
	addss	%xmm2, %xmm4
	movss	%xmm4, 80(%rdi)
	retq
.Lfunc_end6:
	.size	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f, .Lfunc_end6-_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.cfi_endproc

	.text
	.globl	_ZN17btHingeConstraint9testLimitERK11btTransformS2_
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraint9testLimitERK11btTransformS2_,@function
_ZN17btHingeConstraint9testLimitERK11btTransformS2_: # @_ZN17btHingeConstraint9testLimitERK11btTransformS2_
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 16
.Lcfi58:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_
	movss	%xmm0, 772(%rbx)
	movl	$0, 764(%rbx)
	movl	$0, 760(%rbx)
	movb	$0, 782(%rbx)
	movss	748(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	752(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jb	.LBB7_6
# BB#1:
	callq	_Z21btAdjustAngleToLimitsfff
	movss	%xmm0, 772(%rbx)
	movss	748(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB7_2
# BB#3:
	movss	752(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jb	.LBB7_6
# BB#4:
	subss	%xmm0, %xmm1
	movss	%xmm1, 764(%rbx)
	movl	$-1082130432, 760(%rbx) # imm = 0xBF800000
	jmp	.LBB7_5
.LBB7_2:
	subss	%xmm0, %xmm1
	movss	%xmm1, 764(%rbx)
	movl	$1065353216, 760(%rbx)  # imm = 0x3F800000
.LBB7_5:
	movb	$1, 782(%rbx)
.LBB7_6:
	popq	%rbx
	retq
.Lfunc_end7:
	.size	_ZN17btHingeConstraint9testLimitERK11btTransformS2_, .Lfunc_end7-_ZN17btHingeConstraint9testLimitERK11btTransformS2_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_0:
	.long	1065353216              # float 1
.LCPI8_1:
	.long	3197737370              # float -0.300000012
.LCPI8_2:
	.long	925353388               # float 9.99999974E-6
.LCPI8_3:
	.long	2147483648              # float -0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN17btHingeConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraint23solveConstraintObsoleteER12btSolverBodyS1_f,@function
_ZN17btHingeConstraint23solveConstraintObsoleteER12btSolverBodyS1_f: # @_ZN17btHingeConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 32
	subq	$320, %rsp              # imm = 0x140
.Lcfi62:
	.cfi_def_cfa_offset 352
.Lcfi63:
	.cfi_offset %rbx, -32
.Lcfi64:
	.cfi_offset %r14, -24
.Lcfi65:
	.cfi_offset %r15, -16
	movaps	%xmm0, %xmm13
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cmpb	$0, 783(%r14)
	je	.LBB8_35
# BB#1:
	movq	32(%r14), %rax
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movss	24(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	28(%rax), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	32(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	40(%rax), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	44(%rax), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	48(%rax), %xmm15        # xmm15 = mem[0],zero,zero,zero
	cmpb	$0, 780(%r14)
	movss	%xmm13, 12(%rsp)        # 4-byte Spill
	je	.LBB8_3
# BB#2:                                 # %..loopexit_crit_edge
	leaq	72(%rbx), %rax
	jmp	.LBB8_12
.LBB8_3:
	movaps	%xmm11, %xmm7
	movss	648(%r14), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	652(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	656(%r14), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movq	24(%r14), %rcx
	movss	24(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	movaps	%xmm15, %xmm11
	movss	28(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	addss	%xmm0, %xmm4
	movaps	%xmm8, %xmm10
	movss	32(%rcx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm8
	addss	%xmm4, %xmm8
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	8(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	40(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	movss	12(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulps	%xmm5, %xmm4
	movss	44(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm2, %xmm5
	addps	%xmm4, %xmm5
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movss	16(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	48(%rcx), %xmm15        # xmm15 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm15   # xmm15 = xmm15[0],xmm0[0],xmm15[1],xmm0[1]
	mulps	%xmm6, %xmm15
	addps	%xmm5, %xmm15
	movss	56(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	movss	712(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	716(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	mulss	%xmm4, %xmm14
	addss	%xmm3, %xmm14
	movss	720(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm10
	addss	%xmm14, %xmm10
	unpcklps	%xmm1, %xmm9    # xmm9 = xmm9[0],xmm1[0],xmm9[1],xmm1[1]
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm9
	unpcklps	%xmm12, %xmm7   # xmm7 = xmm7[0],xmm12[0],xmm7[1],xmm12[1]
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm4, %xmm7
	movaps	%xmm11, %xmm4
	addps	%xmm9, %xmm7
	unpcklps	16(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	60(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm8
	addps	%xmm6, %xmm15
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm7, %xmm4
	movss	56(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	64(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	movaps	%xmm8, %xmm9
	subss	%xmm1, %xmm9
	movaps	%xmm15, %xmm14
	subps	%xmm6, %xmm14
	movss	60(%rax), %xmm11        # xmm11 = mem[0],zero,zero,zero
	addss	%xmm11, %xmm10
	movss	%xmm10, 16(%rsp)        # 4-byte Spill
	addps	%xmm0, %xmm4
	movq	72(%rbx), %rax
	testq	%rax, %rax
	je	.LBB8_4
# BB#5:
	movsd	328(%rax), %xmm3        # xmm3 = mem[0],zero
	movsd	(%rbx), %xmm2           # xmm2 = mem[0],zero
	addps	%xmm3, %xmm2
	movss	336(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm10
	movss	344(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	addss	8(%rbx), %xmm3
	addss	16(%rbx), %xmm4
	movsd	348(%rax), %xmm5        # xmm5 = mem[0],zero
	movsd	20(%rbx), %xmm6         # xmm6 = mem[0],zero
	addps	%xmm5, %xmm6
	movaps	%xmm14, %xmm12
	mulps	%xmm6, %xmm12
	movaps	%xmm9, %xmm5
	unpcklps	%xmm14, %xmm5   # xmm5 = xmm5[0],xmm14[0],xmm5[1],xmm14[1]
	movaps	%xmm14, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	mulss	%xmm6, %xmm7
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm9, %xmm1
	mulss	%xmm4, %xmm1
	shufps	$0, %xmm6, %xmm4        # xmm4 = xmm4[0,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm4      # xmm4 = xmm4[2,0],xmm6[2,3]
	mulps	%xmm5, %xmm4
	subps	%xmm4, %xmm12
	movaps	%xmm10, %xmm4
	subss	%xmm7, %xmm1
	addps	%xmm2, %xmm12
	addss	%xmm3, %xmm1
	xorps	%xmm10, %xmm10
	movss	%xmm1, %xmm10           # xmm10 = xmm1[0],xmm10[1,2,3]
	jmp	.LBB8_6
.LBB8_4:
	xorps	%xmm12, %xmm12
	xorps	%xmm10, %xmm10
.LBB8_6:                                # %_ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit336
	movss	16(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	subss	%xmm11, %xmm7
	movaps	%xmm4, %xmm6
	subps	%xmm0, %xmm6
	movq	72(%r15), %rax
	testq	%rax, %rax
	movaps	%xmm7, 208(%rsp)        # 16-byte Spill
	je	.LBB8_7
# BB#8:
	movsd	328(%rax), %xmm0        # xmm0 = mem[0],zero
	movsd	(%r15), %xmm1           # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	movss	336(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	344(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	addss	8(%r15), %xmm2
	addss	16(%r15), %xmm3
	movsd	348(%rax), %xmm0        # xmm0 = mem[0],zero
	movaps	%xmm4, %xmm11
	movsd	20(%r15), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm0, %xmm4
	movaps	%xmm6, %xmm0
	mulps	%xmm4, %xmm0
	movaps	%xmm7, %xmm5
	unpcklps	%xmm6, %xmm5    # xmm5 = xmm5[0],xmm6[0],xmm5[1],xmm6[1]
	movaps	%xmm6, %xmm1
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm4, %xmm6
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	mulss	%xmm3, %xmm7
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	movaps	%xmm11, %xmm4
	mulps	%xmm5, %xmm3
	subps	%xmm3, %xmm0
	subss	%xmm6, %xmm7
	movaps	%xmm1, %xmm6
	addps	64(%rsp), %xmm0         # 16-byte Folded Reload
	addss	%xmm2, %xmm7
	xorps	%xmm1, %xmm1
	movss	%xmm7, %xmm1            # xmm1 = xmm7[0],xmm1[1,2,3]
	jmp	.LBB8_9
.LBB8_7:                                # %_ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit336._ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit_crit_edge
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
.LBB8_9:                                # %_ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit
	movaps	%xmm6, 192(%rsp)        # 16-byte Spill
	leaq	72(%rbx), %rax
	movaps	%xmm12, %xmm2
	subss	%xmm0, %xmm2
	movaps	%xmm2, 48(%rsp)         # 16-byte Spill
	shufps	$229, %xmm12, %xmm12    # xmm12 = xmm12[1,1,2,3]
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm0, %xmm12
	subss	%xmm1, %xmm10
	movaps	%xmm4, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm15, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	subss	%xmm0, %xmm1
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	subss	16(%rsp), %xmm8         # 4-byte Folded Reload
	subss	%xmm4, %xmm15
	movaps	%xmm14, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	movaps	%xmm6, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
	xorl	%ecx, %ecx
	movss	%xmm8, 16(%rsp)         # 4-byte Spill
	movaps	%xmm14, 80(%rsp)        # 16-byte Spill
	movaps	%xmm12, 64(%rsp)        # 16-byte Spill
	movaps	%xmm10, 224(%rsp)       # 16-byte Spill
	.p2align	4, 0x90
.LBB8_10:                               # =>This Inner Loop Header: Depth=1
	movss	.LCPI8_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	divss	176(%r14,%rcx), %xmm2
	movss	96(%r14,%rcx), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movss	100(%r14,%rcx), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movaps	%xmm12, %xmm6
	mulss	%xmm3, %xmm5
	mulss	%xmm4, %xmm6
	movss	104(%r14,%rcx), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm7
	mulss	%xmm0, %xmm7
	addss	%xmm5, %xmm6
	mulss	32(%rsp), %xmm3         # 16-byte Folded Reload
	mulss	%xmm8, %xmm4
	mulss	%xmm15, %xmm0
	addss	%xmm3, %xmm4
	addss	%xmm4, %xmm0
	mulss	.LCPI8_1(%rip), %xmm0
	divss	%xmm13, %xmm0
	addss	%xmm6, %xmm7
	mulss	%xmm2, %xmm0
	mulss	%xmm2, %xmm7
	movss	40(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm0
	addss	%xmm0, %xmm2
	movss	%xmm2, 40(%r14)
	movss	96(%r14,%rcx), %xmm13   # xmm13 = mem[0],zero,zero,zero
	movss	100(%r14,%rcx), %xmm12  # xmm12 = mem[0],zero,zero,zero
	movss	104(%r14,%rcx), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm2
	mulss	%xmm11, %xmm2
	movaps	%xmm14, %xmm4
	mulss	%xmm12, %xmm4
	movaps	%xmm14, %xmm3
	subss	%xmm4, %xmm2
	mulss	%xmm13, %xmm3
	movaps	176(%rsp), %xmm7        # 16-byte Reload
	movaps	%xmm7, %xmm4
	mulss	%xmm11, %xmm4
	mulss	%xmm12, %xmm7
	movaps	%xmm9, %xmm1
	subss	%xmm4, %xmm3
	mulss	%xmm13, %xmm1
	movaps	208(%rsp), %xmm8        # 16-byte Reload
	movaps	%xmm8, %xmm5
	mulss	%xmm11, %xmm5
	movaps	192(%rsp), %xmm6        # 16-byte Reload
	movaps	%xmm6, %xmm14
	mulss	%xmm12, %xmm14
	subss	%xmm1, %xmm7
	mulss	%xmm13, %xmm6
	movaps	160(%rsp), %xmm1        # 16-byte Reload
	movaps	%xmm1, %xmm4
	mulss	%xmm11, %xmm4
	mulss	%xmm12, %xmm1
	movaps	%xmm9, %xmm10
	movaps	%xmm15, %xmm9
	movaps	%xmm8, %xmm15
	subss	%xmm14, %xmm5
	mulss	%xmm13, %xmm15
	movq	24(%r14), %rdx
	movss	360(%rdx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm6
	mulss	%xmm14, %xmm13
	mulss	%xmm14, %xmm12
	mulss	%xmm14, %xmm11
	subss	%xmm15, %xmm1
	movss	280(%rdx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm14
	movss	284(%rdx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addss	%xmm14, %xmm4
	movss	288(%rdx), %xmm15       # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm15
	addss	%xmm4, %xmm15
	movss	296(%rdx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	movss	300(%rdx), %xmm8        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm8
	addss	%xmm4, %xmm8
	movss	304(%rdx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm14
	addss	%xmm8, %xmm14
	mulss	312(%rdx), %xmm2
	mulss	316(%rdx), %xmm3
	addss	%xmm2, %xmm3
	mulss	320(%rdx), %xmm7
	mulss	%xmm0, %xmm13
	addss	(%rbx), %xmm13
	movss	%xmm13, (%rbx)
	movss	12(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm7
	mulss	%xmm0, %xmm12
	addss	4(%rbx), %xmm12
	movss	%xmm12, 4(%rbx)
	movaps	64(%rsp), %xmm12        # 16-byte Reload
	mulss	%xmm0, %xmm11
	addss	8(%rbx), %xmm11
	movss	%xmm11, 8(%rbx)
	movss	32(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	mulss	%xmm15, %xmm2
	movaps	%xmm9, %xmm15
	movaps	%xmm10, %xmm9
	movaps	224(%rsp), %xmm10       # 16-byte Reload
	movss	36(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	mulss	%xmm14, %xmm3
	movaps	80(%rsp), %xmm14        # 16-byte Reload
	movss	40(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	mulss	%xmm7, %xmm4
	addss	16(%rbx), %xmm2
	movss	%xmm2, 16(%rbx)
	addss	20(%rbx), %xmm3
	movss	%xmm3, 20(%rbx)
	addss	24(%rbx), %xmm4
	movss	%xmm4, 24(%rbx)
	movq	32(%r14), %rdx
	movss	280(%rdx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	movss	284(%rdx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
	movss	288(%rdx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	mulss	%xmm1, %xmm2
	movss	296(%rdx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	addss	%xmm4, %xmm2
	movss	300(%rdx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
	addss	%xmm3, %xmm4
	movss	304(%rdx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm4, %xmm3
	movss	360(%rdx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	312(%rdx), %xmm5
	mulss	316(%rdx), %xmm6
	addss	%xmm5, %xmm6
	movss	96(%r14,%rcx), %xmm5    # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	mulss	320(%rdx), %xmm1
	addss	%xmm6, %xmm1
	movss	(%r15), %xmm6           # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	subss	%xmm5, %xmm6
	movss	100(%r14,%rcx), %xmm5   # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	mulss	104(%r14,%rcx), %xmm4
	mulss	%xmm0, %xmm5
	movss	%xmm6, (%r15)
	movss	4(%r15), %xmm6          # xmm6 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm6
	mulss	%xmm0, %xmm4
	movss	%xmm6, 4(%r15)
	movss	8(%r15), %xmm5          # xmm5 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm5
	movss	%xmm5, 8(%r15)
	movss	32(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	mulss	%xmm2, %xmm4
	movss	36(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	mulss	%xmm3, %xmm2
	mulss	40(%r15), %xmm0
	mulss	%xmm1, %xmm0
	movss	16(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movss	%xmm1, 16(%r15)
	movss	20(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	movss	%xmm1, 20(%r15)
	movss	24(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 24(%r15)
	addq	$84, %rcx
	cmpq	$252, %rcx
	jne	.LBB8_10
# BB#11:                                # %.loopexit.loopexit
	movq	32(%r14), %rcx
	movss	8(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rcx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	16(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movss	24(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	28(%rcx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	32(%rcx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	40(%rcx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	44(%rcx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	48(%rcx), %xmm15        # xmm15 = mem[0],zero,zero,zero
.LBB8_12:                               # %.loopexit
	movq	24(%r14), %rcx
	movss	608(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	624(%r14), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	640(%r14), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	8(%rcx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	12(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm7
	addss	%xmm4, %xmm7
	movss	24(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	28(%rcx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm10
	addss	%xmm4, %xmm10
	mulss	40(%rcx), %xmm0
	mulss	44(%rcx), %xmm5
	addss	%xmm0, %xmm5
	movss	672(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	688(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	mulss	%xmm4, %xmm12
	addss	%xmm1, %xmm12
	movss	16(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	mulss	%xmm0, %xmm3
	mulss	%xmm4, %xmm14
	addss	%xmm3, %xmm14
	movss	32(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	mulss	48(%rcx), %xmm6
	mulss	%xmm0, %xmm9
	movss	704(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm0, %xmm1
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	mulss	%xmm0, %xmm8
	mulss	%xmm4, %xmm11
	addss	%xmm9, %xmm11
	mulss	%xmm0, %xmm15
	movq	(%rax), %rax
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 208(%rsp)        # 16-byte Spill
	je	.LBB8_14
# BB#13:
	movsd	344(%rax), %xmm0        # xmm0 = mem[0],zero
	movsd	16(%rbx), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movaps	%xmm1, 224(%rsp)        # 16-byte Spill
	movss	352(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	24(%rbx), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movaps	%xmm1, 208(%rsp)        # 16-byte Spill
.LBB8_14:                               # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit245
	addss	%xmm2, %xmm7
	addss	%xmm3, %xmm10
	addss	%xmm6, %xmm5
	addss	16(%rsp), %xmm12        # 16-byte Folded Reload
	addss	%xmm8, %xmm14
	addss	%xmm15, %xmm11
	movaps	%xmm11, 48(%rsp)        # 16-byte Spill
	movq	72(%r15), %rax
	testq	%rax, %rax
	xorps	%xmm11, %xmm11
	je	.LBB8_16
# BB#15:
	movsd	344(%rax), %xmm0        # xmm0 = mem[0],zero
	movsd	16(%r15), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	movss	352(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	24(%r15), %xmm0
	xorps	%xmm11, %xmm11
	movss	%xmm0, %xmm11           # xmm11 = xmm0[0],xmm11[1,2,3]
.LBB8_16:                               # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit
	movaps	%xmm7, %xmm0
	movaps	224(%rsp), %xmm9        # 16-byte Reload
	mulss	%xmm9, %xmm0
	movaps	%xmm9, %xmm8
	shufps	$229, %xmm8, %xmm8      # xmm8 = xmm8[1,1,2,3]
	movaps	%xmm10, %xmm1
	mulss	%xmm8, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm5, %xmm2
	movaps	208(%rsp), %xmm15       # 16-byte Reload
	mulss	%xmm15, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm7, %xmm4
	mulss	%xmm2, %xmm4
	movaps	%xmm10, %xmm3
	mulss	%xmm2, %xmm3
	mulss	%xmm5, %xmm2
	movaps	%xmm12, %xmm0
	movaps	%xmm12, %xmm6
	movss	%xmm5, 16(%rsp)         # 4-byte Spill
	movss	%xmm7, 80(%rsp)         # 4-byte Spill
	movaps	64(%rsp), %xmm12        # 16-byte Reload
	mulss	%xmm12, %xmm0
	movaps	%xmm12, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	movaps	%xmm14, %xmm1
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movaps	%xmm0, %xmm5
	mulss	%xmm11, %xmm5
	addss	%xmm1, %xmm5
	movaps	%xmm6, 192(%rsp)        # 16-byte Spill
	movaps	%xmm6, %xmm1
	mulss	%xmm5, %xmm1
	movaps	%xmm14, %xmm6
	mulss	%xmm5, %xmm6
	mulss	%xmm0, %xmm5
	movss	%xmm4, 124(%rsp)        # 4-byte Spill
	subss	%xmm4, %xmm9
	movaps	%xmm8, 304(%rsp)        # 16-byte Spill
	movaps	%xmm8, %xmm4
	movss	%xmm3, 128(%rsp)        # 4-byte Spill
	subss	%xmm3, %xmm4
	movss	%xmm2, 132(%rsp)        # 4-byte Spill
	subss	%xmm2, %xmm15
	movaps	%xmm12, %xmm0
	movss	80(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movss	%xmm1, 116(%rsp)        # 4-byte Spill
	subss	%xmm1, %xmm0
	movaps	%xmm7, 288(%rsp)        # 16-byte Spill
	movaps	%xmm7, %xmm1
	movss	%xmm6, 112(%rsp)        # 4-byte Spill
	subss	%xmm6, %xmm1
	subss	%xmm0, %xmm9
	movaps	%xmm11, %xmm0
	movss	%xmm5, 120(%rsp)        # 4-byte Spill
	subss	%xmm5, %xmm0
	subss	%xmm1, %xmm4
	subss	%xmm0, %xmm15
	movaps	%xmm9, 240(%rsp)        # 16-byte Spill
	mulss	%xmm9, %xmm9
	movaps	%xmm4, 256(%rsp)        # 16-byte Spill
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm9, %xmm1
	movaps	%xmm15, 144(%rsp)       # 16-byte Spill
	mulss	%xmm15, %xmm15
	addss	%xmm1, %xmm15
	xorps	%xmm0, %xmm0
	sqrtss	%xmm15, %xmm0
	ucomiss	%xmm0, %xmm0
	movss	%xmm10, 176(%rsp)       # 4-byte Spill
	movaps	%xmm11, 160(%rsp)       # 16-byte Spill
	jnp	.LBB8_18
# BB#17:                                # %call.sqrt
	movaps	%xmm15, %xmm0
	movss	%xmm14, 32(%rsp)        # 4-byte Spill
	movaps	%xmm15, 272(%rsp)       # 16-byte Spill
	callq	sqrtf
	movaps	272(%rsp), %xmm15       # 16-byte Reload
	movaps	160(%rsp), %xmm11       # 16-byte Reload
	movss	32(%rsp), %xmm14        # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movss	176(%rsp), %xmm10       # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	80(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
.LBB8_18:                               # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit.split
	ucomiss	.LCPI8_2(%rip), %xmm0
	jbe	.LBB8_22
# BB#19:
	movss	%xmm14, 32(%rsp)        # 4-byte Spill
	xorps	%xmm0, %xmm0
	sqrtss	%xmm15, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB8_21
# BB#20:                                # %call.sqrt867
	movaps	%xmm15, %xmm0
	callq	sqrtf
.LBB8_21:                               # %.split
	movss	.LCPI8_0(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm14
	divss	%xmm0, %xmm14
	movaps	240(%rsp), %xmm9        # 16-byte Reload
	mulss	%xmm14, %xmm9
	movaps	256(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm14, %xmm2
	mulss	144(%rsp), %xmm14       # 16-byte Folded Reload
	movq	24(%r14), %rcx
	movq	32(%r14), %rax
	movss	280(%rcx), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movss	%xmm10, 272(%rsp)       # 4-byte Spill
	movss	284(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, 108(%rsp)        # 4-byte Spill
	mulss	%xmm9, %xmm10
	movss	296(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 104(%rsp)        # 4-byte Spill
	movaps	%xmm9, %xmm1
	mulss	%xmm3, %xmm1
	movss	300(%rcx), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	mulss	%xmm13, %xmm3
	addss	%xmm1, %xmm3
	movss	316(%rcx), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm8
	mulss	%xmm12, %xmm8
	addss	%xmm3, %xmm8
	mulss	%xmm2, %xmm8
	movss	300(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm4
	mulss	%xmm0, %xmm4
	addss	%xmm10, %xmm4
	movss	284(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm6
	addss	%xmm6, %xmm1
	movss	316(%rax), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm7
	addss	%xmm1, %xmm7
	movss	296(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movss	304(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	mulss	%xmm2, %xmm7
	movss	288(%rcx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm10
	mulss	%xmm6, %xmm10
	movss	304(%rcx), %xmm15       # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm2
	addss	%xmm10, %xmm2
	movss	320(%rcx), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm5
	mulss	%xmm10, %xmm5
	addss	%xmm2, %xmm5
	movss	288(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	addss	%xmm2, %xmm0
	movss	320(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm2
	addss	%xmm0, %xmm2
	mulss	%xmm14, %xmm5
	movss	312(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm0
	mulss	%xmm14, %xmm2
	movss	312(%rcx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm14
	addss	%xmm4, %xmm14
	mulss	%xmm9, %xmm14
	addss	%xmm14, %xmm8
	addss	%xmm8, %xmm5
	movss	280(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm4
	addss	%xmm4, %xmm3
	addss	%xmm3, %xmm0
	mulss	%xmm9, %xmm0
	addss	%xmm0, %xmm7
	addss	%xmm7, %xmm2
	addss	%xmm5, %xmm2
	movaps	240(%rsp), %xmm0        # 16-byte Reload
	movss	272(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movaps	256(%rsp), %xmm5        # 16-byte Reload
	movss	108(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	addss	%xmm4, %xmm3
	movaps	%xmm3, %xmm4
	movaps	144(%rsp), %xmm3        # 16-byte Reload
	mulss	%xmm3, %xmm6
	addss	%xmm4, %xmm6
	movss	104(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	mulss	%xmm5, %xmm13
	addss	%xmm4, %xmm13
	mulss	%xmm3, %xmm15
	addss	%xmm13, %xmm15
	mulss	%xmm0, %xmm1
	movaps	%xmm0, %xmm4
	mulss	%xmm5, %xmm12
	movaps	%xmm5, %xmm7
	addss	%xmm1, %xmm12
	mulss	%xmm3, %xmm10
	movaps	%xmm3, %xmm5
	addss	%xmm12, %xmm10
	divss	%xmm2, %xmm11
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%rbx)
	addss	8(%rbx), %xmm0
	movss	%xmm0, 8(%rbx)
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	mulss	%xmm6, %xmm0
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	mulss	%xmm15, %xmm1
	movss	40(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	mulss	%xmm10, %xmm2
	movss	16(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm3
	movss	%xmm3, 16(%rbx)
	movss	20(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	%xmm0, 20(%rbx)
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	movss	%xmm0, 24(%rbx)
	movq	32(%r14), %rax
	movss	280(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	movss	284(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movss	288(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	addss	%xmm1, %xmm0
	movss	296(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	300(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	addss	%xmm1, %xmm3
	movss	304(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	addss	%xmm3, %xmm2
	mulss	312(%rax), %xmm4
	movaps	%xmm7, %xmm1
	mulss	316(%rax), %xmm1
	addss	%xmm4, %xmm1
	movaps	%xmm1, %xmm3
	movaps	%xmm5, %xmm1
	mulss	320(%rax), %xmm1
	addss	%xmm3, %xmm1
	movaps	%xmm1, %xmm4
	xorps	%xmm1, %xmm1
	mulss	%xmm11, %xmm1
	movss	(%r15), %xmm3           # xmm3 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm3
	movss	%xmm3, (%r15)
	movss	4(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm3
	movss	%xmm3, 4(%r15)
	addss	8(%r15), %xmm1
	movss	%xmm1, 8(%r15)
	movss	32(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	mulss	%xmm0, %xmm1
	movss	36(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	mulss	%xmm2, %xmm0
	mulss	40(%r15), %xmm11
	mulss	%xmm4, %xmm11
	addss	16(%r15), %xmm1
	movss	%xmm1, 16(%r15)
	addss	20(%r15), %xmm0
	movss	%xmm0, 20(%r15)
	addss	24(%r15), %xmm11
	movss	%xmm11, 24(%r15)
	movss	16(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movss	80(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	176(%rsp), %xmm10       # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm14        # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movaps	160(%rsp), %xmm11       # 16-byte Reload
.LBB8_22:
	movaps	%xmm10, %xmm2
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm1, %xmm2
	movaps	%xmm12, %xmm0
	mulss	%xmm14, %xmm0
	subss	%xmm0, %xmm2
	movaps	%xmm12, %xmm3
	movaps	192(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm0, %xmm3
	mulss	%xmm8, %xmm1
	subss	%xmm1, %xmm3
	mulss	%xmm8, %xmm14
	mulss	%xmm10, %xmm0
	subss	%xmm0, %xmm14
	movss	.LCPI8_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm13, %xmm0
	mulss	%xmm0, %xmm2
	mulss	%xmm0, %xmm3
	movss	%xmm0, 192(%rsp)        # 4-byte Spill
	mulss	%xmm0, %xmm14
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	movaps	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	movss	%xmm3, 48(%rsp)         # 4-byte Spill
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm14, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	xorps	%xmm0, %xmm0
	sqrtss	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB8_24
# BB#23:                                # %call.sqrt869
	movaps	%xmm2, %xmm0
	movss	%xmm14, 32(%rsp)        # 4-byte Spill
	movss	%xmm2, 144(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	144(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	160(%rsp), %xmm11       # 16-byte Reload
	movss	32(%rsp), %xmm14        # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movss	176(%rsp), %xmm10       # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	80(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
.LBB8_24:                               # %.split868
	ucomiss	.LCPI8_2(%rip), %xmm0
	jbe	.LBB8_28
# BB#25:
	xorps	%xmm0, %xmm0
	sqrtss	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB8_27
# BB#26:                                # %call.sqrt871
	movaps	%xmm2, %xmm0
	movss	%xmm14, 32(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	32(%rsp), %xmm14        # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
.LBB8_27:                               # %.split870
	movss	.LCPI8_0(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm1
	divss	%xmm0, %xmm1
	movss	12(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm15
	movss	48(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	mulss	%xmm14, %xmm1
	movq	24(%r14), %rcx
	movq	32(%r14), %rax
	movss	280(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, 32(%rsp)         # 4-byte Spill
	movss	284(%rcx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	%xmm5, 256(%rsp)        # 4-byte Spill
	mulss	%xmm15, %xmm3
	movss	296(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 240(%rsp)        # 4-byte Spill
	movaps	%xmm15, %xmm2
	mulss	%xmm5, %xmm2
	movss	300(%rcx), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	mulss	%xmm12, %xmm5
	addss	%xmm2, %xmm5
	movss	316(%rcx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 144(%rsp)        # 4-byte Spill
	movaps	%xmm1, %xmm8
	mulss	%xmm2, %xmm8
	addss	%xmm5, %xmm8
	mulss	%xmm4, %xmm8
	movss	300(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm6
	movaps	%xmm4, %xmm7
	mulss	%xmm0, %xmm7
	addss	%xmm3, %xmm7
	movss	284(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm3
	addss	%xmm3, %xmm6
	movss	316(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm6, %xmm5
	movss	296(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	304(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	mulss	%xmm4, %xmm5
	movaps	%xmm4, %xmm6
	movss	288(%rcx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm13
	mulss	%xmm9, %xmm13
	movaps	%xmm14, %xmm10
	movss	304(%rcx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm6
	addss	%xmm13, %xmm6
	movss	320(%rcx), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm13, %xmm4
	addss	%xmm6, %xmm4
	movss	288(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm6
	addss	%xmm6, %xmm0
	movss	320(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	addss	%xmm0, %xmm6
	mulss	%xmm1, %xmm4
	movss	312(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm6
	movss	312(%rcx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	addss	%xmm7, %xmm1
	mulss	%xmm15, %xmm1
	addss	%xmm1, %xmm8
	addss	%xmm8, %xmm4
	movss	280(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm1
	addss	%xmm1, %xmm3
	addss	%xmm3, %xmm0
	mulss	%xmm15, %xmm0
	addss	%xmm0, %xmm5
	addss	%xmm5, %xmm6
	addss	%xmm4, %xmm6
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	48(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	256(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm4, %xmm3
	mulss	%xmm10, %xmm9
	addss	%xmm3, %xmm9
	movss	240(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	mulss	%xmm1, %xmm12
	addss	%xmm3, %xmm12
	mulss	%xmm10, %xmm14
	addss	%xmm12, %xmm14
	mulss	%xmm0, %xmm2
	movaps	%xmm0, %xmm3
	movss	144(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm4
	addss	%xmm2, %xmm0
	mulss	%xmm10, %xmm13
	addss	%xmm0, %xmm13
	divss	%xmm6, %xmm11
	xorps	%xmm0, %xmm0
	mulss	%xmm11, %xmm0
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%rbx)
	addss	8(%rbx), %xmm0
	movss	%xmm0, 8(%rbx)
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	mulss	%xmm9, %xmm0
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	mulss	%xmm14, %xmm1
	movss	40(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	mulss	%xmm13, %xmm2
	addss	16(%rbx), %xmm0
	movss	%xmm0, 16(%rbx)
	addss	20(%rbx), %xmm1
	movss	%xmm1, 20(%rbx)
	addss	24(%rbx), %xmm2
	movss	%xmm2, 24(%rbx)
	movq	32(%r14), %rax
	movss	280(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	284(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	288(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm0
	addss	%xmm1, %xmm0
	movss	296(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movss	300(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	addss	%xmm1, %xmm2
	movss	304(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm1
	addss	%xmm2, %xmm1
	mulss	312(%rax), %xmm3
	movaps	%xmm4, %xmm2
	mulss	316(%rax), %xmm2
	addss	%xmm3, %xmm2
	mulss	320(%rax), %xmm10
	addss	%xmm2, %xmm10
	movss	.LCPI8_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	movss	(%r15), %xmm3           # xmm3 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm3
	movss	%xmm3, (%r15)
	movss	4(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm3
	movss	%xmm3, 4(%r15)
	addss	8(%r15), %xmm2
	movss	%xmm2, 8(%r15)
	movss	32(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	mulss	%xmm0, %xmm2
	movss	36(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	mulss	%xmm1, %xmm0
	mulss	40(%r15), %xmm11
	mulss	%xmm10, %xmm11
	movss	16(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	movss	%xmm1, 16(%r15)
	movss	20(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 20(%r15)
	movss	24(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm11, %xmm0
	movss	%xmm0, 24(%r15)
	movss	16(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movss	80(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	176(%rsp), %xmm10       # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movaps	160(%rsp), %xmm11       # 16-byte Reload
.LBB8_28:
	cmpb	$0, 782(%r14)
	je	.LBB8_30
# BB#29:
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	subss	224(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	288(%rsp), %xmm1        # 16-byte Reload
	subss	304(%rsp), %xmm1        # 16-byte Folded Reload
	subss	208(%rsp), %xmm11       # 16-byte Folded Reload
	mulss	%xmm8, %xmm0
	mulss	%xmm10, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm12, %xmm11
	addss	%xmm1, %xmm11
	mulss	744(%r14), %xmm11
	movss	192(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	764(%r14), %xmm0
	mulss	740(%r14), %xmm0
	addss	%xmm11, %xmm0
	movss	760(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	mulss	756(%r14), %xmm0
	movss	768(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm0
	movss	%xmm0, 140(%rsp)
	movl	$0, 136(%rsp)
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm0
	leaq	140(%rsp), %rax
	leaq	136(%rsp), %rcx
	cmovaq	%rax, %rcx
	movl	(%rcx), %eax
	movl	%eax, 768(%r14)
	movd	%eax, %xmm0
	subss	%xmm3, %xmm0
	movq	24(%r14), %rax
	movss	280(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	movss	284(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm3, %xmm4
	movss	288(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm3
	addss	%xmm4, %xmm3
	movss	296(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm4
	movss	300(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm5
	addss	%xmm4, %xmm5
	movss	304(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm4
	addss	%xmm5, %xmm4
	movss	312(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm5
	movss	316(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm6
	addss	%xmm5, %xmm6
	movss	320(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm5
	addss	%xmm6, %xmm5
	mulss	%xmm0, %xmm1
	mulss	%xmm1, %xmm2
	movss	(%rbx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm6
	movss	%xmm6, (%rbx)
	movss	4(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm6
	movss	%xmm6, 4(%rbx)
	addss	8(%rbx), %xmm2
	movss	%xmm2, 8(%rbx)
	movss	32(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	36(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	mulss	40(%rbx), %xmm1
	mulss	%xmm3, %xmm2
	mulss	%xmm4, %xmm6
	mulss	%xmm5, %xmm1
	addss	16(%rbx), %xmm2
	movss	%xmm2, 16(%rbx)
	addss	20(%rbx), %xmm6
	movss	%xmm6, 20(%rbx)
	addss	24(%rbx), %xmm1
	movss	%xmm1, 24(%rbx)
	movq	32(%r14), %rax
	movss	280(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm1
	movss	284(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	addss	%xmm1, %xmm2
	movss	288(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm1
	addss	%xmm2, %xmm1
	movss	296(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	movss	300(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm3
	addss	%xmm2, %xmm3
	movss	304(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm2
	addss	%xmm3, %xmm2
	movss	312(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	movss	316(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm3, %xmm4
	movss	320(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm3
	addss	%xmm4, %xmm3
	mulss	760(%r14), %xmm0
	movss	.LCPI8_3(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	(%r15), %xmm5           # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, (%r15)
	movss	4(%r15), %xmm5          # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, 4(%r15)
	addss	8(%r15), %xmm4
	movss	%xmm4, 8(%r15)
	movss	32(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	36(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	mulss	40(%r15), %xmm0
	mulss	%xmm1, %xmm4
	mulss	%xmm2, %xmm5
	mulss	%xmm3, %xmm0
	movss	16(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movss	%xmm1, 16(%r15)
	movss	20(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm1
	movss	%xmm1, 20(%r15)
	movss	24(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 24(%r15)
.LBB8_30:
	cmpb	$0, 781(%r14)
	je	.LBB8_35
# BB#31:
	movss	124(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	116(%rsp), %xmm2        # 4-byte Folded Reload
	movss	128(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	112(%rsp), %xmm0        # 4-byte Folded Reload
	movss	132(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	120(%rsp), %xmm1        # 4-byte Folded Reload
	mulss	%xmm8, %xmm2
	mulss	%xmm10, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm12, %xmm1
	addss	%xmm0, %xmm1
	movss	728(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	732(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	mulss	756(%r14), %xmm0
	movss	788(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	addss	%xmm1, %xmm3
	ucomiss	%xmm2, %xmm3
	ja	.LBB8_33
# BB#32:
	xorps	.LCPI8_4(%rip), %xmm2
	ucomiss	%xmm3, %xmm2
	jbe	.LBB8_34
.LBB8_33:
	subss	%xmm1, %xmm2
	movaps	%xmm2, %xmm0
.LBB8_34:
	addss	%xmm0, %xmm1
	movss	%xmm1, 788(%r14)
	movq	24(%r14), %rax
	movss	280(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm1
	movss	284(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	addss	%xmm1, %xmm2
	movss	288(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm1
	addss	%xmm2, %xmm1
	movss	296(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	movss	300(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm3
	addss	%xmm2, %xmm3
	movss	304(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm2
	addss	%xmm3, %xmm2
	movss	312(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	movss	316(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm3, %xmm4
	movss	320(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm3
	addss	%xmm4, %xmm3
	xorps	%xmm4, %xmm4
	mulss	%xmm0, %xmm4
	movss	(%rbx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, (%rbx)
	movss	4(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, 4(%rbx)
	addss	8(%rbx), %xmm4
	movss	%xmm4, 8(%rbx)
	movss	32(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	36(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	movss	40(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	mulss	%xmm1, %xmm4
	mulss	%xmm2, %xmm5
	mulss	%xmm3, %xmm6
	addss	16(%rbx), %xmm4
	movss	%xmm4, 16(%rbx)
	addss	20(%rbx), %xmm5
	movss	%xmm5, 20(%rbx)
	addss	24(%rbx), %xmm6
	movss	%xmm6, 24(%rbx)
	movq	32(%r14), %rax
	movss	280(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm1
	movss	284(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	addss	%xmm1, %xmm2
	movss	288(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm1
	addss	%xmm2, %xmm1
	movss	296(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	movss	300(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm3
	addss	%xmm2, %xmm3
	movss	304(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm2
	addss	%xmm3, %xmm2
	mulss	312(%rax), %xmm8
	mulss	316(%rax), %xmm10
	addss	%xmm8, %xmm10
	mulss	320(%rax), %xmm12
	addss	%xmm10, %xmm12
	movss	.LCPI8_3(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	(%r15), %xmm4           # xmm4 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	movss	%xmm4, (%r15)
	movss	4(%r15), %xmm4          # xmm4 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	movss	%xmm4, 4(%r15)
	addss	8(%r15), %xmm3
	movss	%xmm3, 8(%r15)
	movss	32(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	36(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	mulss	40(%r15), %xmm0
	mulss	%xmm1, %xmm3
	mulss	%xmm2, %xmm4
	mulss	%xmm12, %xmm0
	movss	16(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm1
	movss	%xmm1, 16(%r15)
	movss	20(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movss	%xmm1, 20(%r15)
	movss	24(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 24(%r15)
.LBB8_35:
	addq	$320, %rsp              # imm = 0x140
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_ZN17btHingeConstraint23solveConstraintObsoleteER12btSolverBodyS1_f, .Lfunc_end8-_ZN17btHingeConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.cfi_endproc

	.globl	_ZN17btHingeConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E,@function
_ZN17btHingeConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E: # @_ZN17btHingeConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 32
.Lcfi69:
	.cfi_offset %rbx, -24
.Lcfi70:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpb	$0, 783(%rbx)
	je	.LBB9_2
# BB#1:
	movq	$0, (%r14)
	jmp	.LBB9_11
.LBB9_2:
	movabsq	$4294967301, %rax       # imm = 0x100000005
	movq	%rax, (%r14)
	movq	24(%rbx), %rsi
	movq	32(%rbx), %rdx
	addq	$8, %rsi
	addq	$8, %rdx
	movq	%rbx, %rdi
	callq	_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_
	movss	%xmm0, 772(%rbx)
	movl	$0, 764(%rbx)
	movl	$0, 760(%rbx)
	movb	$0, 782(%rbx)
	movss	748(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	752(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jae	.LBB9_4
.LBB9_3:                                # %_ZN17btHingeConstraint9testLimitERK11btTransformS2_.exit.thread6
	cmpb	$0, 781(%rbx)
	jne	.LBB9_10
	jmp	.LBB9_11
.LBB9_4:
	callq	_Z21btAdjustAngleToLimitsfff
	movss	%xmm0, 772(%rbx)
	movss	748(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB9_7
# BB#5:
	movss	752(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jae	.LBB9_8
# BB#6:                                 # %_ZN17btHingeConstraint9testLimitERK11btTransformS2_.exit
	cmpb	$0, 782(%rbx)
	jne	.LBB9_10
	jmp	.LBB9_3
.LBB9_7:
	subss	%xmm0, %xmm1
	movss	%xmm1, 764(%rbx)
	movl	$1065353216, 760(%rbx)  # imm = 0x3F800000
	jmp	.LBB9_9
.LBB9_8:
	subss	%xmm0, %xmm1
	movss	%xmm1, 764(%rbx)
	movl	$-1082130432, 760(%rbx) # imm = 0xBF800000
.LBB9_9:                                # %_ZN17btHingeConstraint9testLimitERK11btTransformS2_.exit.thread
	movb	$1, 782(%rbx)
.LBB9_10:                               # %_ZN17btHingeConstraint9testLimitERK11btTransformS2_.exit.thread
	incl	(%r14)
	decl	4(%r14)
.LBB9_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	_ZN17btHingeConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E, .Lfunc_end9-_ZN17btHingeConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.cfi_endproc

	.globl	_ZN17btHingeConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E,@function
_ZN17btHingeConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E: # @_ZN17btHingeConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpb	$0, 783(%rdi)
	movl	$6, %ecx
	cmovnel	%eax, %ecx
	movl	%ecx, (%rsi)
	movl	$0, 4(%rsi)
	retq
.Lfunc_end10:
	.size	_ZN17btHingeConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E, .Lfunc_end10-_ZN17btHingeConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.cfi_endproc

	.globl	_ZN17btHingeConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E,@function
_ZN17btHingeConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E: # @_ZN17btHingeConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %r8
	movq	32(%rdi), %r9
	leaq	8(%r8), %rdx
	addq	$344, %r8               # imm = 0x158
	leaq	8(%r9), %rcx
	addq	$344, %r9               # imm = 0x158
	jmp	_ZN17btHingeConstraint16getInfo2InternalEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_ # TAILCALL
.Lfunc_end11:
	.size	_ZN17btHingeConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E, .Lfunc_end11-_ZN17btHingeConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_1:
	.long	2139095039              # float 3.40282347E+38
	.text
	.globl	_ZN17btHingeConstraint16getInfo2InternalEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraint16getInfo2InternalEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_,@function
_ZN17btHingeConstraint16getInfo2InternalEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_: # @_ZN17btHingeConstraint16getInfo2InternalEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi77:
	.cfi_def_cfa_offset 304
.Lcfi78:
	.cfi_offset %rbx, -56
.Lcfi79:
	.cfi_offset %r12, -48
.Lcfi80:
	.cfi_offset %r13, -40
.Lcfi81:
	.cfi_offset %r14, -32
.Lcfi82:
	.cfi_offset %r15, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %r14
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
	movss	4(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movss	604(%rbx), %xmm15       # xmm15 = mem[0],zero,zero,zero
	movss	620(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	mulss	%xmm1, %xmm3
	movaps	%xmm3, 144(%rsp)        # 16-byte Spill
	movss	636(%rbx), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm4
	mulss	%xmm8, %xmm4
	movaps	%xmm4, 128(%rsp)        # 16-byte Spill
	movss	16(%rdx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	20(%rdx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	24(%rdx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm4
	mulss	%xmm10, %xmm4
	movaps	%xmm1, %xmm6
	mulss	%xmm13, %xmm6
	addss	%xmm4, %xmm6
	movaps	%xmm8, %xmm4
	mulss	%xmm9, %xmm4
	addss	%xmm6, %xmm4
	movaps	%xmm4, 80(%rsp)         # 16-byte Spill
	movss	32(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	36(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	40(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm7
	mulss	%xmm3, %xmm7
	movaps	%xmm3, %xmm11
	movss	%xmm11, 36(%rsp)        # 4-byte Spill
	mulss	%xmm4, %xmm1
	addss	%xmm7, %xmm1
	movaps	%xmm8, %xmm3
	mulss	%xmm6, %xmm3
	addss	%xmm1, %xmm3
	movaps	%xmm3, 64(%rsp)         # 16-byte Spill
	movss	648(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm7
	unpcklps	%xmm10, %xmm7   # xmm7 = xmm7[0],xmm10[0],xmm7[1],xmm10[1]
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm7, %xmm3
	movss	616(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm5
	mulss	%xmm2, %xmm5
	movss	%xmm5, 28(%rsp)         # 4-byte Spill
	movaps	%xmm0, %xmm5
	mulss	%xmm13, %xmm5
	movss	%xmm5, 48(%rsp)         # 4-byte Spill
	movss	624(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm7
	mulss	%xmm13, %xmm7
	movss	%xmm7, 56(%rsp)         # 4-byte Spill
	movaps	%xmm2, %xmm7
	unpcklps	%xmm13, %xmm2   # xmm2 = xmm2[0],xmm13[0],xmm2[1],xmm13[1]
	mulss	%xmm5, %xmm7
	movaps	%xmm7, 224(%rsp)        # 16-byte Spill
	mulss	%xmm4, %xmm0
	movss	%xmm0, 60(%rsp)         # 4-byte Spill
	mulss	%xmm4, %xmm5
	movss	%xmm5, 8(%rsp)          # 4-byte Spill
	movss	652(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm4
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm2, %xmm5
	addps	%xmm3, %xmm5
	movss	632(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	mulss	%xmm12, %xmm0
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	movaps	%xmm2, %xmm0
	mulss	%xmm9, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movss	640(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	mulss	%xmm9, %xmm3
	movss	%xmm3, 40(%rsp)         # 4-byte Spill
	movaps	%xmm12, %xmm3
	unpcklps	%xmm9, %xmm12   # xmm12 = xmm12[0],xmm9[0],xmm12[1],xmm9[1]
	mulss	%xmm0, %xmm3
	movaps	%xmm3, 208(%rsp)        # 16-byte Spill
	mulss	%xmm6, %xmm2
	movss	%xmm2, 44(%rsp)         # 4-byte Spill
	mulss	%xmm6, %xmm0
	movss	%xmm0, 52(%rsp)         # 4-byte Spill
	movss	656(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm12, %xmm2
	addps	%xmm5, %xmm2
	movsd	48(%rdx), %xmm8         # xmm8 = mem[0],zero
	addps	%xmm2, %xmm8
	mulss	%xmm11, %xmm1
	addss	%xmm4, %xmm1
	addss	%xmm6, %xmm1
	movss	(%rcx), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	16(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm2
	unpcklps	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	movss	712(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm2, %xmm3
	movss	688(%rbx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm2, %xmm0
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	movss	4(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	unpcklps	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1]
	mulss	%xmm14, %xmm0
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	movss	36(%rcx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm14
	movss	716(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm12
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	addps	%xmm3, %xmm2
	movss	704(%rbx), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movss	24(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm0
	mulss	%xmm3, %xmm0
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movss	8(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	mulss	%xmm13, %xmm0
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	movss	40(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm13
	movss	720(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm5, %xmm0
	addps	%xmm2, %xmm0
	movsd	48(%rcx), %xmm5         # xmm5 = mem[0],zero
	addps	%xmm0, %xmm5
	movss	32(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm7
	addss	%xmm12, %xmm7
	addss	%xmm3, %xmm7
	movslq	40(%r12), %rax
	addss	56(%rdx), %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	addss	56(%rcx), %xmm7
	movss	600(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	608(%rbx), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movss	672(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movlps	%xmm8, 112(%rsp)
	movlps	%xmm0, 120(%rsp)
	xorps	%xmm0, %xmm0
	movss	%xmm7, %xmm0            # xmm0 = xmm7[0],xmm0[1,2,3]
	movlps	%xmm5, 96(%rsp)
	movlps	%xmm0, 104(%rsp)
	movq	8(%r12), %rsi
	movl	$1065353216, (%rsi)     # imm = 0x3F800000
	movl	$1065353216, 4(%rsi,%rax,4) # imm = 0x3F800000
	movl	$1065353216, 8(%rsi,%rax,8) # imm = 0x3F800000
	movaps	%xmm8, %xmm0
	movss	48(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm8
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	52(%rdx), %xmm0
	subss	56(%rdx), %xmm1
	movq	16(%r12), %rsi
	movaps	.LCPI12_0(%rip), %xmm12 # xmm12 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm0, %xmm6
	xorps	%xmm12, %xmm6
	movl	$0, (%rsi)
	movss	%xmm1, 4(%rsi)
	movss	%xmm6, 8(%rsi)
	xorps	%xmm12, %xmm1
	movl	$0, 12(%rsi)
	movss	%xmm1, (%rsi,%rax,4)
	leaq	(%rax,%rax), %rdx
	movslq	%edx, %rdx
	movl	$0, 4(%rsi,%rax,4)
	movss	%xmm8, 8(%rsi,%rax,4)
	movl	$0, 12(%rsi,%rax,4)
	movss	%xmm0, (%rsi,%rdx,4)
	xorps	%xmm12, %xmm8
	movss	%xmm8, 4(%rsi,%rdx,4)
	movaps	160(%rsp), %xmm8        # 16-byte Reload
	movq	$0, 8(%rsi,%rdx,4)
	movaps	%xmm5, %xmm0
	movss	48(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm5
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	52(%rcx), %xmm0
	subss	56(%rcx), %xmm7
	movq	32(%r12), %rcx
	movaps	%xmm7, %xmm1
	xorps	%xmm12, %xmm1
	movl	$0, (%rcx)
	movss	%xmm1, 4(%rcx)
	movss	%xmm0, 8(%rcx)
	movl	$0, 12(%rcx)
	movss	%xmm7, (%rcx,%rax,4)
	movaps	%xmm5, %xmm1
	xorps	%xmm12, %xmm1
	movl	$0, 4(%rcx,%rax,4)
	movss	%xmm1, 8(%rcx,%rax,4)
	movl	$0, 12(%rcx,%rax,4)
	xorps	%xmm12, %xmm0
	movss	%xmm0, (%rcx,%rdx,4)
	movss	%xmm5, 4(%rcx,%rdx,4)
	movq	$0, 8(%rcx,%rdx,4)
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	4(%r12), %xmm1
	movq	48(%r12), %rcx
	movss	96(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	112(%rsp), %xmm0
	mulss	%xmm1, %xmm0
	movss	%xmm0, (%rcx)
	movss	100(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	subss	116(%rsp), %xmm0
	mulss	%xmm1, %xmm0
	movss	%xmm0, (%rcx,%rax,4)
	movss	104(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	subss	120(%rsp), %xmm0
	mulss	%xmm1, %xmm0
	movss	%xmm0, (%rcx,%rax,8)
	movaps	%xmm2, %xmm7
	mulss	%xmm8, %xmm7
	addss	28(%rsp), %xmm7         # 4-byte Folded Reload
	addss	24(%rsp), %xmm7         # 4-byte Folded Reload
	mulss	%xmm8, %xmm15
	addss	144(%rsp), %xmm15       # 16-byte Folded Reload
	addss	128(%rsp), %xmm15       # 16-byte Folded Reload
	mulss	%xmm11, %xmm8
	addss	224(%rsp), %xmm8        # 16-byte Folded Reload
	addss	208(%rsp), %xmm8        # 16-byte Folded Reload
	movaps	%xmm2, %xmm0
	mulss	%xmm10, %xmm0
	addss	48(%rsp), %xmm0         # 4-byte Folded Reload
	addss	32(%rsp), %xmm0         # 4-byte Folded Reload
	mulss	%xmm11, %xmm10
	addss	56(%rsp), %xmm10        # 4-byte Folded Reload
	addss	40(%rsp), %xmm10        # 4-byte Folded Reload
	movss	36(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	addss	60(%rsp), %xmm2         # 4-byte Folded Reload
	addss	44(%rsp), %xmm2         # 4-byte Folded Reload
	mulss	%xmm5, %xmm11
	addss	8(%rsp), %xmm11         # 4-byte Folded Reload
	addss	52(%rsp), %xmm11        # 4-byte Folded Reload
	mulss	%xmm3, %xmm9
	addss	192(%rsp), %xmm9        # 16-byte Folded Reload
	addss	176(%rsp), %xmm9        # 16-byte Folded Reload
	mulss	%xmm3, %xmm4
	addss	20(%rsp), %xmm4         # 4-byte Folded Reload
	addss	16(%rsp), %xmm4         # 4-byte Folded Reload
	mulss	12(%rsp), %xmm3         # 4-byte Folded Reload
	addss	%xmm14, %xmm3
	addss	%xmm13, %xmm3
	movslq	40(%r12), %rax
	leaq	(%rax,%rax,2), %rdi
	movq	16(%r12), %rcx
	movss	%xmm7, (%rcx,%rdi,4)
	movslq	%edi, %rbp
	movss	%xmm0, 4(%rcx,%rbp,4)
	movss	%xmm2, 8(%rcx,%rbp,4)
	movq	%rax, %rdx
	shlq	$4, %rdx
	movss	%xmm15, (%rcx,%rdx)
	movaps	%xmm15, %xmm13
	leal	1(,%rax,4), %esi
	movslq	%esi, %r9
	movaps	80(%rsp), %xmm14        # 16-byte Reload
	movss	%xmm14, (%rcx,%r9,4)
	leal	2(,%rax,4), %esi
	movslq	%esi, %r8
	movaps	64(%rsp), %xmm15        # 16-byte Reload
	movss	%xmm15, (%rcx,%r8,4)
	movaps	%xmm7, %xmm5
	xorps	%xmm12, %xmm5
	movq	32(%r12), %rsi
	movss	%xmm5, (%rsi,%rdi,4)
	movaps	%xmm0, %xmm5
	xorps	%xmm12, %xmm5
	movss	%xmm5, 4(%rsi,%rbp,4)
	movaps	%xmm2, %xmm5
	xorps	%xmm12, %xmm5
	movss	%xmm5, 8(%rsi,%rbp,4)
	movaps	%xmm10, %xmm5
	mulss	%xmm3, %xmm5
	movaps	%xmm11, %xmm6
	mulss	%xmm4, %xmm6
	subss	%xmm6, %xmm5
	mulss	%xmm5, %xmm7
	movaps	%xmm13, %xmm6
	mulss	%xmm6, %xmm5
	xorps	%xmm12, %xmm13
	movss	%xmm13, (%rsi,%rdx)
	movaps	%xmm11, %xmm13
	movaps	%xmm11, %xmm6
	mulss	%xmm9, %xmm6
	mulss	%xmm8, %xmm3
	subss	%xmm3, %xmm6
	mulss	%xmm6, %xmm0
	movaps	%xmm14, %xmm3
	mulss	%xmm3, %xmm6
	xorps	%xmm12, %xmm3
	movss	%xmm3, (%rsi,%r9,4)
	mulss	%xmm8, %xmm4
	movaps	%xmm10, %xmm11
	mulss	%xmm10, %xmm9
	subss	%xmm9, %xmm4
	mulss	%xmm4, %xmm2
	mulss	%xmm15, %xmm4
	xorps	%xmm12, %xmm15
	movss	%xmm15, (%rsi,%r8,4)
	addss	%xmm7, %xmm0
	addss	%xmm0, %xmm2
	mulss	%xmm1, %xmm2
	movq	48(%r12), %r8
	movss	%xmm2, (%r8,%rdi,4)
	addss	%xmm5, %xmm6
	addss	%xmm6, %xmm4
	mulss	%xmm1, %xmm4
	movss	%xmm4, (%r8,%rdx)
	cmpb	$0, 782(%rbx)
	je	.LBB12_1
# BB#2:
	movss	764(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	776(%rbx), %xmm3
	xorps	%xmm0, %xmm0
	xorl	%ebp, %ebp
	ucomiss	%xmm0, %xmm3
	setbe	%bpl
	incl	%ebp
	jmp	.LBB12_3
.LBB12_1:
	xorps	%xmm3, %xmm3
	xorl	%ebp, %ebp
.LBB12_3:
	movzbl	781(%rbx), %edx
	movl	%edx, %edi
	orl	%ebp, %edi
	je	.LBB12_21
# BB#4:
	testl	%ebp, %ebp
	setne	%dil
	leaq	(%rax,%rax,4), %r13
	movss	%xmm8, (%rcx,%r13,4)
	movslq	%r13d, %rax
	movaps	%xmm11, %xmm1
	movss	%xmm1, 4(%rcx,%rax,4)
	movaps	%xmm13, %xmm2
	movss	%xmm2, 8(%rcx,%rax,4)
	movaps	%xmm8, %xmm0
	xorps	%xmm12, %xmm0
	movss	%xmm0, (%rsi,%r13,4)
	movaps	%xmm1, %xmm0
	xorps	%xmm12, %xmm0
	movss	%xmm0, 4(%rsi,%rax,4)
	xorps	%xmm2, %xmm12
	movss	%xmm12, 8(%rsi,%rax,4)
	movss	748(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	752(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	setnp	%cl
	sete	%al
	movl	$0, (%r8,%r13,4)
	testb	%dl, %dl
	je	.LBB12_7
# BB#5:
	andb	%cl, %al
	andb	%al, %dil
	jne	.LBB12_7
# BB#6:
	movq	56(%r12), %rax
	movl	$0, (%rax,%r13,4)
	movss	%xmm3, 8(%rsp)          # 4-byte Spill
	movss	728(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	772(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	(%r12), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	4(%r12), %xmm4
	movq	%rbx, %rdi
	movss	%xmm1, 80(%rsp)         # 4-byte Spill
	movss	80(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm2, 64(%rsp)         # 4-byte Spill
	movss	64(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm8, 160(%rsp)        # 16-byte Spill
	movaps	%xmm11, 144(%rsp)       # 16-byte Spill
	movaps	%xmm13, 128(%rsp)       # 16-byte Spill
	callq	_ZN17btTypedConstraint14getMotorFactorEfffff
	movss	64(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	80(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	128(%rsp), %xmm13       # 16-byte Reload
	movaps	144(%rsp), %xmm11       # 16-byte Reload
	movaps	160(%rsp), %xmm8        # 16-byte Reload
	mulss	728(%rbx), %xmm0
	mulss	776(%rbx), %xmm0
	movq	48(%r12), %r8
	addss	(%r8,%r13,4), %xmm0
	movss	%xmm0, (%r8,%r13,4)
	movss	732(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	.LCPI12_0(%rip), %xmm0
	movq	64(%r12), %rax
	movss	%xmm0, (%rax,%r13,4)
	movl	732(%rbx), %eax
	movq	72(%r12), %rcx
	movl	%eax, (%rcx,%r13,4)
.LBB12_7:
	testl	%ebp, %ebp
	je	.LBB12_21
# BB#8:
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	4(%r12), %xmm0
	mulss	%xmm0, %xmm3
	addss	(%r8,%r13,4), %xmm3
	movss	%xmm3, (%r8,%r13,4)
	movq	56(%r12), %rax
	movl	$0, (%rax,%r13,4)
	ucomiss	%xmm2, %xmm1
	jne	.LBB12_10
	jp	.LBB12_10
# BB#9:
	movq	64(%r12), %rax
	movl	$-8388609, (%rax,%r13,4) # imm = 0xFF7FFFFF
	movss	.LCPI12_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB12_13
.LBB12_10:
	movq	64(%r12), %rax
	cmpl	$1, %ebp
	jne	.LBB12_12
# BB#11:
	movl	$0, (%rax,%r13,4)
	movss	.LCPI12_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB12_13
.LBB12_12:
	movl	$-8388609, (%rax,%r13,4) # imm = 0xFF7FFFFF
	xorps	%xmm0, %xmm0
.LBB12_13:
	movq	72(%r12), %rax
	movss	%xmm0, (%rax,%r13,4)
	movss	744(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB12_20
# BB#14:
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	(%r14), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm4          # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	mulps	%xmm8, %xmm3
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	unpcklps	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	mulps	%xmm11, %xmm4
	addps	%xmm3, %xmm4
	shufps	$224, %xmm13, %xmm13    # xmm13 = xmm13[0,0,2,3]
	movss	8(%r15), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	mulps	%xmm13, %xmm1
	addps	%xmm4, %xmm1
	movaps	%xmm1, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	subss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	cmpl	$1, %ebp
	jne	.LBB12_17
# BB#15:
	ucomiss	%xmm1, %xmm2
	jbe	.LBB12_20
# BB#16:
	mulss	%xmm1, %xmm0
	xorps	.LCPI12_0(%rip), %xmm0
	ucomiss	(%r8,%r13,4), %xmm0
	ja	.LBB12_19
	jmp	.LBB12_20
.LBB12_17:
	ucomiss	%xmm2, %xmm1
	jbe	.LBB12_20
# BB#18:
	mulss	%xmm1, %xmm0
	xorps	.LCPI12_0(%rip), %xmm0
	movss	(%r8,%r13,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB12_20
.LBB12_19:
	movss	%xmm0, (%r8,%r13,4)
.LBB12_20:
	movss	740(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	(%r8,%r13,4), %xmm0
	movss	%xmm0, (%r8,%r13,4)
.LBB12_21:
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN17btHingeConstraint16getInfo2InternalEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_, .Lfunc_end12-_ZN17btHingeConstraint16getInfo2InternalEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_
	.cfi_endproc

	.globl	_ZN17btHingeConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_,@function
_ZN17btHingeConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_: # @_ZN17btHingeConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi90:
	.cfi_def_cfa_offset 64
.Lcfi91:
	.cfi_offset %rbx, -56
.Lcfi92:
	.cfi_offset %r12, -48
.Lcfi93:
	.cfi_offset %r13, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rcx, %r12
	movq	%rdx, %rbx
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_
	movss	%xmm0, 772(%rbp)
	movl	$0, 764(%rbp)
	movl	$0, 760(%rbp)
	movb	$0, 782(%rbp)
	movss	748(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	752(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jb	.LBB13_6
# BB#1:
	callq	_Z21btAdjustAngleToLimitsfff
	movss	%xmm0, 772(%rbp)
	movss	748(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB13_2
# BB#3:
	movss	752(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jb	.LBB13_6
# BB#4:
	subss	%xmm0, %xmm1
	movss	%xmm1, 764(%rbp)
	movl	$-1082130432, 760(%rbp) # imm = 0xBF800000
	jmp	.LBB13_5
.LBB13_2:
	subss	%xmm0, %xmm1
	movss	%xmm1, 764(%rbp)
	movl	$1065353216, 760(%rbp)  # imm = 0x3F800000
.LBB13_5:                               # %_ZN17btHingeConstraint9testLimitERK11btTransformS2_.exit
	movb	$1, 782(%rbp)
.LBB13_6:                               # %_ZN17btHingeConstraint9testLimitERK11btTransformS2_.exit
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	%r14, %r9
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17btHingeConstraint16getInfo2InternalEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_ # TAILCALL
.Lfunc_end13:
	.size	_ZN17btHingeConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_, .Lfunc_end13-_ZN17btHingeConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_
	.cfi_endproc

	.globl	_ZN17btHingeConstraint9updateRHSEf
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraint9updateRHSEf,@function
_ZN17btHingeConstraint9updateRHSEf:     # @_ZN17btHingeConstraint9updateRHSEf
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end14:
	.size	_ZN17btHingeConstraint9updateRHSEf, .Lfunc_end14-_ZN17btHingeConstraint9updateRHSEf
	.cfi_endproc

	.globl	_ZN17btHingeConstraint13getHingeAngleEv
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraint13getHingeAngleEv,@function
_ZN17btHingeConstraint13getHingeAngleEv: # @_ZN17btHingeConstraint13getHingeAngleEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rsi
	movq	32(%rdi), %rdx
	addq	$8, %rsi
	addq	$8, %rdx
	jmp	_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_ # TAILCALL
.Lfunc_end15:
	.size	_ZN17btHingeConstraint13getHingeAngleEv, .Lfunc_end15-_ZN17btHingeConstraint13getHingeAngleEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI16_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI16_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI16_1:
	.long	1061752795              # float 0.785398185
.LCPI16_2:
	.long	1075235812              # float 2.3561945
.LCPI16_3:
	.long	3209236443              # float -0.785398185
	.text
	.globl	_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_,@function
_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_: # @_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_
	.cfi_startproc
# BB#0:
	movss	616(%rdi), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	632(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm13          # xmm13 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	600(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	604(%rdi), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm13, %xmm0
	movaps	%xmm5, %xmm2
	mulss	%xmm7, %xmm2
	addss	%xmm0, %xmm2
	movss	8(%rsi), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm12
	mulss	%xmm9, %xmm12
	addss	%xmm2, %xmm12
	movss	16(%rsi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	mulss	%xmm14, %xmm2
	movss	20(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm6
	mulss	%xmm0, %xmm6
	addss	%xmm2, %xmm6
	movss	24(%rsi), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm10, %xmm4
	addss	%xmm6, %xmm4
	movss	32(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	movss	36(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	40(%rsi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	addss	%xmm5, %xmm1
	movss	620(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	636(%rdi), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm13
	mulss	%xmm3, %xmm7
	addss	%xmm13, %xmm7
	mulss	%xmm5, %xmm9
	addss	%xmm7, %xmm9
	mulss	%xmm8, %xmm14
	mulss	%xmm3, %xmm0
	addss	%xmm14, %xmm0
	mulss	%xmm5, %xmm10
	addss	%xmm0, %xmm10
	mulss	%xmm8, %xmm6
	mulss	%xmm3, %xmm2
	addss	%xmm6, %xmm2
	mulss	%xmm5, %xmm11
	addss	%xmm2, %xmm11
	movss	668(%rdi), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	684(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	700(%rdi), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	movss	4(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	8(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	addss	%xmm3, %xmm0
	movss	16(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	movss	20(%rdx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm7
	addss	%xmm3, %xmm7
	movss	24(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	addss	%xmm7, %xmm3
	mulss	32(%rdx), %xmm5
	mulss	36(%rdx), %xmm2
	addss	%xmm5, %xmm2
	mulss	40(%rdx), %xmm6
	addss	%xmm2, %xmm6
	mulss	%xmm0, %xmm12
	mulss	%xmm3, %xmm4
	addss	%xmm12, %xmm4
	mulss	%xmm6, %xmm1
	addss	%xmm4, %xmm1
	mulss	%xmm9, %xmm0
	mulss	%xmm10, %xmm3
	addss	%xmm0, %xmm3
	mulss	%xmm11, %xmm6
	addss	%xmm3, %xmm6
	movaps	.LCPI16_0(%rip), %xmm2  # xmm2 = [nan,nan,nan,nan]
	andps	%xmm1, %xmm2
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm6
	jae	.LBB16_1
# BB#2:
	movaps	%xmm2, %xmm0
	addss	%xmm6, %xmm0
	subss	%xmm6, %xmm2
	divss	%xmm2, %xmm0
	movss	.LCPI16_2(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB16_3
.LBB16_1:
	movaps	%xmm6, %xmm0
	subss	%xmm2, %xmm0
	addss	%xmm6, %xmm2
	divss	%xmm2, %xmm0
	movss	.LCPI16_1(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
.LBB16_3:                               # %_Z11btAtan2Fastff.exit
	mulss	.LCPI16_3(%rip), %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	cmpltss	%xmm2, %xmm1
	movaps	%xmm1, %xmm2
	andnps	%xmm0, %xmm2
	xorps	.LCPI16_4(%rip), %xmm0
	andps	%xmm1, %xmm0
	orps	%xmm2, %xmm0
	mulss	776(%rdi), %xmm0
	retq
.Lfunc_end16:
	.size	_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_, .Lfunc_end16-_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI17_0:
	.long	1086918619              # float 6.28318548
.LCPI17_1:
	.long	3226013659              # float -3.14159274
.LCPI17_2:
	.long	1078530011              # float 3.14159274
.LCPI17_3:
	.long	3234402267              # float -6.28318548
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_4:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.text._Z21btAdjustAngleToLimitsfff,"axG",@progbits,_Z21btAdjustAngleToLimitsfff,comdat
	.weak	_Z21btAdjustAngleToLimitsfff
	.p2align	4, 0x90
	.type	_Z21btAdjustAngleToLimitsfff,@function
_Z21btAdjustAngleToLimitsfff:           # @_Z21btAdjustAngleToLimitsfff
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi97:
	.cfi_def_cfa_offset 48
	movaps	%xmm1, %xmm3
	ucomiss	%xmm2, %xmm3
	jae	.LBB17_21
# BB#1:
	ucomiss	%xmm0, %xmm3
	jbe	.LBB17_11
# BB#2:
	movss	%xmm2, (%rsp)           # 4-byte Spill
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	subss	%xmm0, %xmm3
	movss	.LCPI17_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	callq	fmodf
	movss	.LCPI17_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB17_4
# BB#3:
	addss	.LCPI17_0(%rip), %xmm0
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB17_6
.LBB17_11:
	ucomiss	%xmm2, %xmm0
	jbe	.LBB17_21
# BB#12:
	movss	%xmm3, (%rsp)           # 4-byte Spill
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	subss	%xmm2, %xmm0
	movss	.LCPI17_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	callq	fmodf
	movaps	%xmm0, %xmm1
	movss	.LCPI17_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB17_14
# BB#13:
	addss	.LCPI17_0(%rip), %xmm1
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB17_16
.LBB17_4:
	ucomiss	.LCPI17_2(%rip), %xmm0
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jbe	.LBB17_6
# BB#5:
	addss	.LCPI17_3(%rip), %xmm0
.LBB17_6:                               # %_Z16btNormalizeAnglef.exit
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	subss	%xmm1, %xmm2
	movss	.LCPI17_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	callq	fmodf
	movss	.LCPI17_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB17_8
# BB#7:
	addss	.LCPI17_0(%rip), %xmm0
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movaps	(%rsp), %xmm2           # 16-byte Reload
	jmp	.LBB17_10
.LBB17_8:
	ucomiss	.LCPI17_2(%rip), %xmm0
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movaps	(%rsp), %xmm2           # 16-byte Reload
	jbe	.LBB17_10
# BB#9:
	addss	.LCPI17_3(%rip), %xmm0
.LBB17_10:                              # %_Z16btNormalizeAnglef.exit30
	andps	.LCPI17_4(%rip), %xmm0
	cmpltss	%xmm0, %xmm2
	movaps	%xmm2, %xmm0
	andps	%xmm1, %xmm0
	addss	.LCPI17_0(%rip), %xmm1
	andnps	%xmm1, %xmm2
	orps	%xmm0, %xmm2
	movaps	%xmm2, %xmm0
	addq	$40, %rsp
	retq
.LBB17_14:
	ucomiss	.LCPI17_2(%rip), %xmm1
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jbe	.LBB17_16
# BB#15:
	addss	.LCPI17_3(%rip), %xmm1
.LBB17_16:                              # %_Z16btNormalizeAnglef.exit32
	movss	%xmm1, (%rsp)           # 4-byte Spill
	subss	%xmm2, %xmm0
	movss	.LCPI17_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	callq	fmodf
	movss	.LCPI17_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB17_18
# BB#17:
	addss	.LCPI17_0(%rip), %xmm0
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	movss	(%rsp), %xmm3           # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	jmp	.LBB17_20
.LBB17_18:
	ucomiss	.LCPI17_2(%rip), %xmm0
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	movss	(%rsp), %xmm3           # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	jbe	.LBB17_20
# BB#19:
	addss	.LCPI17_3(%rip), %xmm0
.LBB17_20:                              # %_Z16btNormalizeAnglef.exit34
	andps	.LCPI17_4(%rip), %xmm0
	movss	.LCPI17_3(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	cmpltss	%xmm3, %xmm0
	andps	%xmm0, %xmm1
	andnps	%xmm2, %xmm0
	orps	%xmm1, %xmm0
.LBB17_21:
	addq	$40, %rsp
	retq
.Lfunc_end17:
	.size	_Z21btAdjustAngleToLimitsfff, .Lfunc_end17-_Z21btAdjustAngleToLimitsfff
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI18_4:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI18_1:
	.long	1065353216              # float 1
.LCPI18_2:
	.long	3212836862              # float -0.99999988
.LCPI18_3:
	.long	1056964608              # float 0.5
.LCPI18_5:
	.long	1060439283              # float 0.707106769
.LCPI18_6:
	.long	1078530011              # float 3.14159274
	.text
	.globl	_ZN17btHingeConstraint14setMotorTargetERK12btQuaternionf
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraint14setMotorTargetERK12btQuaternionf,@function
_ZN17btHingeConstraint14setMotorTargetERK12btQuaternionf: # @_ZN17btHingeConstraint14setMotorTargetERK12btQuaternionf
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 24
	subq	$136, %rsp
.Lcfi100:
	.cfi_def_cfa_offset 160
.Lcfi101:
	.cfi_offset %rbx, -24
.Lcfi102:
	.cfi_offset %r14, -16
	movss	%xmm0, 76(%rsp)         # 4-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	leaq	664(%r14), %rdi
	leaq	80(%rsp), %rsi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movsd	80(%rsp), %xmm12        # xmm12 = mem[0],zero
	movsd	88(%rsp), %xmm11        # xmm11 = mem[0],zero
	movaps	.LCPI18_0(%rip), %xmm10 # xmm10 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm12, %xmm0
	xorps	%xmm10, %xmm0
	movaps	%xmm12, %xmm4
	shufps	$1, %xmm11, %xmm4       # xmm4 = xmm4[1,0],xmm11[0,0]
	shufps	$226, %xmm11, %xmm4     # xmm4 = xmm4[2,0],xmm11[2,3]
	pshufd	$229, %xmm11, %xmm1     # xmm1 = xmm11[1,1,2,3]
	movss	12(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm7
	shufps	$0, %xmm1, %xmm7        # xmm7 = xmm7[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm7      # xmm7 = xmm7[2,0],xmm1[2,3]
	movaps	%xmm3, %xmm6
	unpcklps	%xmm13, %xmm6   # xmm6 = xmm6[0],xmm13[0],xmm6[1],xmm13[1]
	mulps	%xmm7, %xmm6
	movaps	%xmm13, %xmm7
	unpcklps	%xmm9, %xmm7    # xmm7 = xmm7[0],xmm9[0],xmm7[1],xmm9[1]
	movaps	%xmm3, %xmm2
	mulss	%xmm12, %xmm2
	movaps	%xmm9, %xmm5
	mulss	%xmm9, %xmm12
	unpcklps	%xmm8, %xmm9    # xmm9 = xmm9[0],xmm8[0],xmm9[1],xmm8[1]
	mulps	%xmm4, %xmm9
	xorps	%xmm10, %xmm4
	shufps	$17, %xmm4, %xmm11      # xmm11 = xmm11[1,0],xmm4[1,0]
	shufps	$226, %xmm4, %xmm11     # xmm11 = xmm11[2,0],xmm4[2,3]
	mulps	%xmm7, %xmm11
	pshufd	$229, %xmm4, %xmm7      # xmm7 = xmm4[1,1,2,3]
	addps	%xmm6, %xmm11
	subps	%xmm9, %xmm11
	unpcklps	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	movaps	%xmm1, %xmm6
	mulss	%xmm8, %xmm6
	mulss	%xmm13, %xmm1
	mulss	%xmm4, %xmm13
	mulss	%xmm8, %xmm4
	unpcklps	%xmm3, %xmm8    # xmm8 = xmm8[0],xmm3[0],xmm8[1],xmm3[1]
	mulps	%xmm8, %xmm0
	subps	%xmm0, %xmm11
	movaps	%xmm11, 32(%rsp)        # 16-byte Spill
	addss	%xmm6, %xmm13
	subss	%xmm2, %xmm13
	mulss	%xmm7, %xmm5
	subss	%xmm5, %xmm13
	movaps	%xmm13, 16(%rsp)        # 16-byte Spill
	addss	%xmm1, %xmm12
	mulss	%xmm7, %xmm3
	subss	%xmm3, %xmm12
	subss	%xmm4, %xmm12
	movaps	%xmm12, (%rsp)          # 16-byte Spill
	leaq	600(%r14), %rdi
	leaq	80(%rsp), %rsi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movq	80(%rsp), %xmm3         # xmm3 = mem[0],zero
	movq	88(%rsp), %xmm0         # xmm0 = mem[0],zero
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	pshufd	$229, %xmm3, %xmm10     # xmm10 = xmm3[1,1,2,3]
	movaps	(%rsp), %xmm7           # 16-byte Reload
	movaps	%xmm7, %xmm4
	mulss	%xmm10, %xmm4
	movaps	32(%rsp), %xmm6         # 16-byte Reload
	movaps	%xmm6, %xmm5
	mulss	%xmm2, %xmm5
	addss	%xmm4, %xmm5
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm1, %xmm9
	mulss	%xmm3, %xmm9
	addss	%xmm5, %xmm9
	movaps	%xmm6, %xmm4
	movaps	%xmm6, %xmm8
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	movaps	%xmm4, %xmm5
	mulss	%xmm0, %xmm5
	subss	%xmm5, %xmm9
	movaps	%xmm7, %xmm5
	movaps	%xmm7, %xmm12
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movaps	%xmm1, %xmm6
	movaps	%xmm1, %xmm11
	shufps	$0, %xmm4, %xmm6        # xmm6 = xmm6[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm6      # xmm6 = xmm6[2,0],xmm4[2,3]
	movaps	%xmm3, %xmm7
	pshufd	$225, %xmm3, %xmm1      # xmm1 = xmm3[1,0,2,3]
	mulss	%xmm3, %xmm4
	unpcklps	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	mulps	%xmm5, %xmm3
	mulps	%xmm2, %xmm6
	addps	%xmm3, %xmm6
	shufps	$1, %xmm0, %xmm7        # xmm7 = xmm7[1,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm7      # xmm7 = xmm7[2,0],xmm0[2,3]
	movaps	%xmm8, %xmm5
	mulps	%xmm5, %xmm7
	addps	%xmm6, %xmm7
	movaps	%xmm11, %xmm6
	movaps	%xmm6, %xmm3
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	mulps	%xmm3, %xmm1
	subps	%xmm1, %xmm7
	movaps	%xmm7, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movaps	%xmm12, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm1, %xmm2
	subss	%xmm4, %xmm2
	mulss	%xmm10, %xmm5
	subss	%xmm5, %xmm2
	mulss	%xmm0, %xmm6
	subss	%xmm6, %xmm2
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm9, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm3, %xmm3
	addss	%xmm1, %xmm3
	movaps	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB18_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm9, 32(%rsp)         # 16-byte Spill
	movaps	%xmm2, (%rsp)           # 16-byte Spill
	movaps	%xmm7, 16(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	16(%rsp), %xmm7         # 16-byte Reload
	movaps	(%rsp), %xmm2           # 16-byte Reload
	movaps	32(%rsp), %xmm9         # 16-byte Reload
.LBB18_2:                               # %.split
	movss	.LCPI18_1(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm11
	divss	%xmm0, %xmm11
	mulss	%xmm11, %xmm9
	movaps	%xmm11, %xmm10
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm7, %xmm10
	movaps	%xmm10, %xmm12
	shufps	$229, %xmm12, %xmm12    # xmm12 = xmm12[1,1,2,3]
	mulss	%xmm2, %xmm11
	movss	_ZL6vHinge(%rip), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm11, %xmm2
	movss	_ZL6vHinge+8(%rip), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm9, %xmm0
	addss	%xmm2, %xmm0
	movss	_ZL6vHinge+4(%rip), %xmm4 # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm2
	mulss	%xmm12, %xmm2
	subss	%xmm2, %xmm0
	movaps	%xmm1, %xmm2
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movaps	%xmm11, %xmm5
	shufps	$0, %xmm12, %xmm5       # xmm5 = xmm5[0,0],xmm12[0,0]
	shufps	$226, %xmm12, %xmm5     # xmm5 = xmm5[2,0],xmm12[2,3]
	mulps	%xmm2, %xmm5
	movaps	%xmm4, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movaps	%xmm11, %xmm2
	unpcklps	%xmm10, %xmm2   # xmm2 = xmm2[0],xmm10[0],xmm2[1],xmm10[1]
	mulps	%xmm6, %xmm2
	addps	%xmm5, %xmm2
	movaps	%xmm3, %xmm5
	unpcklps	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	movaps	%xmm9, %xmm6
	shufps	$0, %xmm10, %xmm6       # xmm6 = xmm6[0,0],xmm10[0,0]
	shufps	$226, %xmm10, %xmm6     # xmm6 = xmm6[2,0],xmm10[2,3]
	mulps	%xmm5, %xmm6
	subps	%xmm6, %xmm2
	mulss	%xmm10, %xmm1
	movaps	.LCPI18_0(%rip), %xmm13 # xmm13 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm13, %xmm1
	mulss	%xmm9, %xmm4
	subss	%xmm4, %xmm1
	mulss	%xmm12, %xmm3
	subss	%xmm3, %xmm1
	movaps	%xmm9, %xmm4
	xorps	%xmm13, %xmm4
	movaps	%xmm12, %xmm3
	unpcklps	%xmm10, %xmm3   # xmm3 = xmm3[0],xmm10[0],xmm3[1],xmm10[1]
	movaps	%xmm2, %xmm6
	mulps	%xmm3, %xmm6
	xorps	%xmm13, %xmm3
	pshufd	$229, %xmm3, %xmm14     # xmm14 = xmm3[1,1,2,3]
	movaps	%xmm4, %xmm7
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	mulss	%xmm1, %xmm3
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	shufps	$0, %xmm14, %xmm7       # xmm7 = xmm7[0,0],xmm14[0,0]
	shufps	$226, %xmm14, %xmm7     # xmm7 = xmm7[2,0],xmm14[2,3]
	mulps	%xmm1, %xmm7
	movaps	%xmm11, %xmm1
	unpcklps	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1]
	movaps	%xmm0, %xmm5
	unpcklps	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1]
	mulps	%xmm1, %xmm5
	addps	%xmm7, %xmm5
	subps	%xmm6, %xmm5
	movaps	%xmm2, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm6, %xmm1        # xmm1 = xmm1[0,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm1      # xmm1 = xmm1[2,0],xmm6[2,3]
	mulps	%xmm1, %xmm4
	subps	%xmm4, %xmm5
	movaps	%xmm5, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	mulss	%xmm11, %xmm6
	addss	%xmm3, %xmm6
	mulss	%xmm9, %xmm0
	subss	%xmm0, %xmm6
	mulss	%xmm14, %xmm2
	subss	%xmm2, %xmm6
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm4, %xmm4
	addss	%xmm0, %xmm4
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm4, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	movaps	%xmm13, %xmm7
	jnp	.LBB18_4
# BB#3:                                 # %call.sqrt156
	movaps	%xmm1, %xmm0
	movaps	%xmm9, 32(%rsp)         # 16-byte Spill
	movaps	%xmm10, (%rsp)          # 16-byte Spill
	movaps	%xmm11, 16(%rsp)        # 16-byte Spill
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	movaps	%xmm6, 112(%rsp)        # 16-byte Spill
	movaps	%xmm5, 96(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	96(%rsp), %xmm5         # 16-byte Reload
	movaps	112(%rsp), %xmm6        # 16-byte Reload
	movaps	48(%rsp), %xmm12        # 16-byte Reload
	movaps	16(%rsp), %xmm11        # 16-byte Reload
	movaps	(%rsp), %xmm10          # 16-byte Reload
	movaps	32(%rsp), %xmm9         # 16-byte Reload
	movss	.LCPI18_1(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movaps	.LCPI18_0(%rip), %xmm7  # xmm7 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
.LBB18_4:                               # %.split.split
	movaps	%xmm8, %xmm2
	divss	%xmm0, %xmm2
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	movaps	%xmm4, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	mulss	%xmm6, %xmm2
	movss	_ZL6vHinge+4(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	_ZL6vHinge+8(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	movss	_ZL6vHinge(%rip), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	mulss	%xmm4, %xmm1
	movaps	%xmm0, %xmm6
	mulss	%xmm5, %xmm6
	addss	%xmm1, %xmm6
	movaps	%xmm13, %xmm1
	mulss	%xmm2, %xmm1
	addss	%xmm6, %xmm1
	movss	.LCPI18_2(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm6
	jbe	.LBB18_9
# BB#5:
	movaps	.LCPI18_4(%rip), %xmm1  # xmm1 = [nan,nan,nan,nan]
	andps	%xmm13, %xmm1
	ucomiss	.LCPI18_5(%rip), %xmm1
	jbe	.LBB18_12
# BB#6:
	mulss	%xmm0, %xmm0
	mulss	%xmm13, %xmm13
	addss	%xmm0, %xmm13
	xorps	%xmm0, %xmm0
	sqrtss	%xmm13, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB18_8
# BB#7:                                 # %call.sqrt158
	movaps	%xmm13, %xmm0
	movaps	%xmm9, 32(%rsp)         # 16-byte Spill
	movaps	%xmm10, (%rsp)          # 16-byte Spill
	movaps	%xmm11, 16(%rsp)        # 16-byte Spill
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm12        # 16-byte Reload
	movaps	16(%rsp), %xmm11        # 16-byte Reload
	movaps	(%rsp), %xmm10          # 16-byte Reload
	movaps	32(%rsp), %xmm9         # 16-byte Reload
	movss	.LCPI18_1(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movaps	.LCPI18_0(%rip), %xmm7  # xmm7 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
.LBB18_8:                               # %.split157
	movaps	%xmm8, %xmm1
	divss	%xmm0, %xmm1
	movss	_ZL6vHinge+8(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm13
	xorps	.LCPI18_0(%rip), %xmm13
	mulss	_ZL6vHinge+4(%rip), %xmm1
	unpcklps	%xmm1, %xmm13   # xmm13 = xmm13[0],xmm1[0],xmm13[1],xmm1[1]
	xorps	%xmm14, %xmm14
	jmp	.LBB18_15
.LBB18_9:
	movaps	%xmm0, %xmm14
	mulss	%xmm2, %xmm14
	mulss	%xmm13, %xmm5
	subss	%xmm5, %xmm14
	unpcklps	%xmm3, %xmm13   # xmm13 = xmm13[0],xmm3[0],xmm13[1],xmm3[1]
	mulps	%xmm4, %xmm13
	unpcklps	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	unpcklps	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	mulps	%xmm3, %xmm2
	subps	%xmm2, %xmm13
	addss	.LCPI18_1(%rip), %xmm1
	addss	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB18_11
# BB#10:                                # %call.sqrt162
	movaps	%xmm1, %xmm0
	movaps	%xmm9, 32(%rsp)         # 16-byte Spill
	movaps	%xmm10, (%rsp)          # 16-byte Spill
	movaps	%xmm11, 16(%rsp)        # 16-byte Spill
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	movaps	%xmm13, 112(%rsp)       # 16-byte Spill
	movaps	%xmm14, 96(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	96(%rsp), %xmm14        # 16-byte Reload
	movaps	112(%rsp), %xmm13       # 16-byte Reload
	movaps	48(%rsp), %xmm12        # 16-byte Reload
	movaps	16(%rsp), %xmm11        # 16-byte Reload
	movaps	(%rsp), %xmm10          # 16-byte Reload
	movaps	32(%rsp), %xmm9         # 16-byte Reload
	movss	.LCPI18_1(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movaps	.LCPI18_0(%rip), %xmm7  # xmm7 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
.LBB18_11:                              # %.split161
	movaps	%xmm8, %xmm1
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm14
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm13
	mulss	.LCPI18_3(%rip), %xmm0
	jmp	.LBB18_16
.LBB18_12:
	mulss	%xmm3, %xmm3
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB18_14
# BB#13:                                # %call.sqrt160
	movaps	%xmm9, 32(%rsp)         # 16-byte Spill
	movaps	%xmm10, (%rsp)          # 16-byte Spill
	movaps	%xmm11, 16(%rsp)        # 16-byte Spill
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm12        # 16-byte Reload
	movaps	16(%rsp), %xmm11        # 16-byte Reload
	movaps	(%rsp), %xmm10          # 16-byte Reload
	movaps	32(%rsp), %xmm9         # 16-byte Reload
	movss	.LCPI18_1(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movaps	.LCPI18_0(%rip), %xmm7  # xmm7 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm0, %xmm1
.LBB18_14:                              # %.split159
	movaps	%xmm8, %xmm0
	divss	%xmm1, %xmm0
	movss	_ZL6vHinge+4(%rip), %xmm14 # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm14
	xorps	.LCPI18_0(%rip), %xmm14
	mulss	_ZL6vHinge(%rip), %xmm0
	xorps	%xmm13, %xmm13
	movss	%xmm0, %xmm13           # xmm13 = xmm0[0],xmm13[1,2,3]
.LBB18_15:                              # %_Z15shortestArcQuatRK9btVector3S1_.exit
	xorps	%xmm0, %xmm0
.LBB18_16:                              # %_Z15shortestArcQuatRK9btVector3S1_.exit
	movaps	%xmm14, %xmm1
	xorps	%xmm7, %xmm1
	xorps	%xmm13, %xmm7
	movaps	%xmm10, %xmm3
	mulss	%xmm0, %xmm3
	movaps	%xmm11, %xmm2
	mulss	%xmm14, %xmm2
	subss	%xmm2, %xmm3
	movaps	%xmm12, %xmm2
	mulss	%xmm7, %xmm2
	addss	%xmm3, %xmm2
	pshufd	$229, %xmm7, %xmm4      # xmm4 = xmm7[1,1,2,3]
	movaps	%xmm9, %xmm3
	mulss	%xmm4, %xmm3
	subss	%xmm3, %xmm2
	movaps	%xmm9, %xmm3
	mulss	%xmm0, %xmm3
	movaps	%xmm11, %xmm5
	mulss	%xmm7, %xmm5
	addss	%xmm3, %xmm5
	movaps	%xmm10, %xmm6
	mulss	%xmm4, %xmm6
	addss	%xmm5, %xmm6
	movaps	%xmm12, %xmm3
	mulss	%xmm14, %xmm3
	addss	%xmm6, %xmm3
	mulss	%xmm0, %xmm12
	mulss	%xmm11, %xmm4
	addss	%xmm12, %xmm4
	mulss	%xmm9, %xmm1
	mulss	%xmm0, %xmm11
	mulss	%xmm10, %xmm14
	addss	%xmm11, %xmm14
	mulss	%xmm9, %xmm7
	unpcklps	%xmm14, %xmm1   # xmm1 = xmm1[0],xmm14[0],xmm1[1],xmm14[1]
	unpcklps	%xmm7, %xmm4    # xmm4 = xmm4[0],xmm7[0],xmm4[1],xmm7[1]
	movaps	%xmm1, %xmm0
	addps	%xmm4, %xmm0
	subps	%xmm4, %xmm1
	shufps	$1, %xmm0, %xmm1        # xmm1 = xmm1[1,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm1      # xmm1 = xmm1[2,0],xmm0[2,3]
	mulps	%xmm13, %xmm10
	addps	%xmm1, %xmm10
	movaps	%xmm10, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm2, %xmm2
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm10, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB18_18
# BB#17:                                # %call.sqrt163
	movaps	%xmm10, (%rsp)          # 16-byte Spill
	callq	sqrtf
	movaps	(%rsp), %xmm10          # 16-byte Reload
	movss	.LCPI18_1(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB18_18:                              # %_Z15shortestArcQuatRK9btVector3S1_.exit.split
	divss	%xmm1, %xmm8
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm10, %xmm8
	movaps	%xmm8, (%rsp)           # 16-byte Spill
	movaps	%xmm8, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	callq	acosf
	addss	%xmm0, %xmm0
	ucomiss	.LCPI18_6(%rip), %xmm0
	jbe	.LBB18_20
# BB#19:
	movdqa	(%rsp), %xmm0           # 16-byte Reload
	pxor	.LCPI18_0(%rip), %xmm0
	movdqa	%xmm0, (%rsp)           # 16-byte Spill
	pshufd	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	callq	acosf
	addss	%xmm0, %xmm0
.LBB18_20:
	movaps	(%rsp), %xmm2           # 16-byte Reload
	xorps	%xmm1, %xmm1
	cmpltss	%xmm1, %xmm2
	movaps	%xmm2, %xmm1
	andnps	%xmm0, %xmm1
	xorps	.LCPI18_0(%rip), %xmm0
	andps	%xmm2, %xmm0
	orps	%xmm1, %xmm0
	movss	748(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	752(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	jbe	.LBB18_24
# BB#21:
	ucomiss	%xmm0, %xmm2
	ja	.LBB18_25
# BB#22:
	ucomiss	%xmm1, %xmm0
	movaps	%xmm0, %xmm2
	jbe	.LBB18_25
# BB#23:
	movaps	%xmm1, %xmm2
	jmp	.LBB18_25
.LBB18_24:
	movaps	%xmm0, %xmm2
.LBB18_25:                              # %_ZN17btHingeConstraint14setMotorTargetEff.exit
	movss	%xmm2, (%rsp)           # 4-byte Spill
	movq	24(%r14), %rsi
	movq	32(%r14), %rdx
	addq	$8, %rsi
	addq	$8, %rdx
	movq	%r14, %rdi
	callq	_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_
	movss	(%rsp), %xmm1           # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	divss	76(%rsp), %xmm1         # 4-byte Folded Reload
	movss	%xmm1, 728(%r14)
	addq	$136, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	_ZN17btHingeConstraint14setMotorTargetERK12btQuaternionf, .Lfunc_end18-_ZN17btHingeConstraint14setMotorTargetERK12btQuaternionf
	.cfi_endproc

	.globl	_ZN17btHingeConstraint14setMotorTargetEff
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraint14setMotorTargetEff,@function
_ZN17btHingeConstraint14setMotorTargetEff: # @_ZN17btHingeConstraint14setMotorTargetEff
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi104:
	.cfi_def_cfa_offset 32
.Lcfi105:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movss	748(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	752(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm2
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	jbe	.LBB19_1
# BB#2:
	ucomiss	%xmm0, %xmm3
	ja	.LBB19_5
# BB#3:
	ucomiss	%xmm2, %xmm0
	movaps	%xmm0, %xmm3
	jbe	.LBB19_5
# BB#4:
	movaps	%xmm2, %xmm3
	jmp	.LBB19_5
.LBB19_1:
	movaps	%xmm0, %xmm3
.LBB19_5:
	movss	%xmm3, 8(%rsp)          # 4-byte Spill
	movq	24(%rbx), %rsi
	movq	32(%rbx), %rdx
	addq	$8, %rsi
	addq	$8, %rdx
	movq	%rbx, %rdi
	callq	_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	divss	12(%rsp), %xmm1         # 4-byte Folded Reload
	movss	%xmm1, 728(%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end19:
	.size	_ZN17btHingeConstraint14setMotorTargetEff, .Lfunc_end19-_ZN17btHingeConstraint14setMotorTargetEff
	.cfi_endproc

	.section	.text._ZN17btTypedConstraintD2Ev,"axG",@progbits,_ZN17btTypedConstraintD2Ev,comdat
	.weak	_ZN17btTypedConstraintD2Ev
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraintD2Ev,@function
_ZN17btTypedConstraintD2Ev:             # @_ZN17btTypedConstraintD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end20:
	.size	_ZN17btTypedConstraintD2Ev, .Lfunc_end20-_ZN17btTypedConstraintD2Ev
	.cfi_endproc

	.section	.text._ZN17btHingeConstraintD0Ev,"axG",@progbits,_ZN17btHingeConstraintD0Ev,comdat
	.weak	_ZN17btHingeConstraintD0Ev
	.p2align	4, 0x90
	.type	_ZN17btHingeConstraintD0Ev,@function
_ZN17btHingeConstraintD0Ev:             # @_ZN17btHingeConstraintD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end21:
	.size	_ZN17btHingeConstraintD0Ev, .Lfunc_end21-_ZN17btHingeConstraintD0Ev
	.cfi_endproc

	.section	.text._ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,"axG",@progbits,_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,comdat
	.weak	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,@function
_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif: # @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end22:
	.size	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif, .Lfunc_end22-_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI23_0:
	.long	1065353216              # float 1
.LCPI23_1:
	.long	1056964608              # float 0.5
	.section	.text._ZNK11btMatrix3x311getRotationER12btQuaternion,"axG",@progbits,_ZNK11btMatrix3x311getRotationER12btQuaternion,comdat
	.weak	_ZNK11btMatrix3x311getRotationER12btQuaternion
	.p2align	4, 0x90
	.type	_ZNK11btMatrix3x311getRotationER12btQuaternion,@function
_ZNK11btMatrix3x311getRotationER12btQuaternion: # @_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi109:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi110:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi112:
	.cfi_def_cfa_offset 80
.Lcfi113:
	.cfi_offset %rbx, -56
.Lcfi114:
	.cfi_offset %r12, -48
.Lcfi115:
	.cfi_offset %r13, -40
.Lcfi116:
	.cfi_offset %r14, -32
.Lcfi117:
	.cfi_offset %r15, -24
.Lcfi118:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm1
	jbe	.LBB23_4
# BB#1:
	addss	.LCPI23_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB23_3
# BB#2:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movq	%rsi, %rbp
	callq	sqrtf
	movq	%rbp, %rsi
.LBB23_3:                               # %.split
	movss	.LCPI23_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	32(%rbx), %xmm2
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movaps	%xmm1, %xmm3
	divss	%xmm0, %xmm3
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	24(%rbx), %xmm1
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	4(%rbx), %xmm4
	movaps	%xmm3, %xmm5
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	unpcklps	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1]
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	mulps	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)
	jmp	.LBB23_7
.LBB23_4:
	xorl	%eax, %eax
	ucomiss	%xmm0, %xmm2
	seta	%al
	maxss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm3
	movl	$2, %r15d
	cmovbel	%eax, %r15d
	leal	1(%r15), %eax
	movl	$2863311531, %ecx       # imm = 0xAAAAAAAB
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	1(%r15,%rax), %edx
	movl	%r15d, %eax
	addl	$2, %eax
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	2(%r15,%rax), %r14d
	movq	%r15, %rbp
	shlq	$4, %rbp
	addq	%rbx, %rbp
	movss	(%rbp,%r15,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	%rdx, %r12
	shlq	$4, %r12
	addq	%rbx, %r12
	subss	(%r12,%rdx,4), %xmm0
	movq	%r14, %r13
	shlq	$4, %r13
	addq	%rbx, %r13
	subss	(%r13,%r14,4), %xmm0
	addss	.LCPI23_0(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB23_6
# BB#5:                                 # %call.sqrt72
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %rbx
	callq	sqrtf
	movq	%rbx, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movaps	%xmm0, %xmm1
.LBB23_6:                               # %.split71
	movss	.LCPI23_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, (%rsp,%r15,4)
	divss	%xmm1, %xmm0
	movss	(%r13,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	(%r12,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)
	movss	(%r12,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%rdx,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%rdx,4)
	movss	(%r13,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%r14,4)
	movaps	(%rsp), %xmm1
.LBB23_7:
	movups	%xmm1, (%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	_ZNK11btMatrix3x311getRotationER12btQuaternion, .Lfunc_end23-_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_endproc

	.type	_ZTV17btHingeConstraint,@object # @_ZTV17btHingeConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTV17btHingeConstraint
	.p2align	3
_ZTV17btHingeConstraint:
	.quad	0
	.quad	_ZTI17btHingeConstraint
	.quad	_ZN17btTypedConstraintD2Ev
	.quad	_ZN17btHingeConstraintD0Ev
	.quad	_ZN17btHingeConstraint13buildJacobianEv
	.quad	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.quad	_ZN17btHingeConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.quad	_ZN17btHingeConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.quad	_ZN17btHingeConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.size	_ZTV17btHingeConstraint, 72

	.type	_ZL6vHinge,@object      # @_ZL6vHinge
	.data
	.p2align	2
_ZL6vHinge:
	.long	0                       # float 0
	.long	0                       # float 0
	.long	1065353216              # float 1
	.long	0                       # float 0
	.size	_ZL6vHinge, 16

	.type	_ZTS17btHingeConstraint,@object # @_ZTS17btHingeConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTS17btHingeConstraint
	.p2align	4
_ZTS17btHingeConstraint:
	.asciz	"17btHingeConstraint"
	.size	_ZTS17btHingeConstraint, 20

	.type	_ZTS17btTypedConstraint,@object # @_ZTS17btTypedConstraint
	.section	.rodata._ZTS17btTypedConstraint,"aG",@progbits,_ZTS17btTypedConstraint,comdat
	.weak	_ZTS17btTypedConstraint
	.p2align	4
_ZTS17btTypedConstraint:
	.asciz	"17btTypedConstraint"
	.size	_ZTS17btTypedConstraint, 20

	.type	_ZTS13btTypedObject,@object # @_ZTS13btTypedObject
	.section	.rodata._ZTS13btTypedObject,"aG",@progbits,_ZTS13btTypedObject,comdat
	.weak	_ZTS13btTypedObject
_ZTS13btTypedObject:
	.asciz	"13btTypedObject"
	.size	_ZTS13btTypedObject, 16

	.type	_ZTI13btTypedObject,@object # @_ZTI13btTypedObject
	.section	.rodata._ZTI13btTypedObject,"aG",@progbits,_ZTI13btTypedObject,comdat
	.weak	_ZTI13btTypedObject
	.p2align	3
_ZTI13btTypedObject:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13btTypedObject
	.size	_ZTI13btTypedObject, 16

	.type	_ZTI17btTypedConstraint,@object # @_ZTI17btTypedConstraint
	.section	.rodata._ZTI17btTypedConstraint,"aG",@progbits,_ZTI17btTypedConstraint,comdat
	.weak	_ZTI17btTypedConstraint
	.p2align	4
_ZTI17btTypedConstraint:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS17btTypedConstraint
	.long	0                       # 0x0
	.long	1                       # 0x1
	.quad	_ZTI13btTypedObject
	.quad	2050                    # 0x802
	.size	_ZTI17btTypedConstraint, 40

	.type	_ZTI17btHingeConstraint,@object # @_ZTI17btHingeConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTI17btHingeConstraint
	.p2align	4
_ZTI17btHingeConstraint:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS17btHingeConstraint
	.quad	_ZTI17btTypedConstraint
	.size	_ZTI17btHingeConstraint, 24


	.globl	_ZN17btHingeConstraintC1Ev
	.type	_ZN17btHingeConstraintC1Ev,@function
_ZN17btHingeConstraintC1Ev = _ZN17btHingeConstraintC2Ev
	.globl	_ZN17btHingeConstraintC1ER11btRigidBodyS1_RK9btVector3S4_RS2_S5_b
	.type	_ZN17btHingeConstraintC1ER11btRigidBodyS1_RK9btVector3S4_RS2_S5_b,@function
_ZN17btHingeConstraintC1ER11btRigidBodyS1_RK9btVector3S4_RS2_S5_b = _ZN17btHingeConstraintC2ER11btRigidBodyS1_RK9btVector3S4_RS2_S5_b
	.globl	_ZN17btHingeConstraintC1ER11btRigidBodyRK9btVector3RS2_b
	.type	_ZN17btHingeConstraintC1ER11btRigidBodyRK9btVector3RS2_b,@function
_ZN17btHingeConstraintC1ER11btRigidBodyRK9btVector3RS2_b = _ZN17btHingeConstraintC2ER11btRigidBodyRK9btVector3RS2_b
	.globl	_ZN17btHingeConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b
	.type	_ZN17btHingeConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b,@function
_ZN17btHingeConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b = _ZN17btHingeConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.globl	_ZN17btHingeConstraintC1ER11btRigidBodyRK11btTransformb
	.type	_ZN17btHingeConstraintC1ER11btRigidBodyRK11btTransformb,@function
_ZN17btHingeConstraintC1ER11btRigidBodyRK11btTransformb = _ZN17btHingeConstraintC2ER11btRigidBodyRK11btTransformb
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
