	.text
	.file	"d6-arith.bc"
	.globl	_Z9pushbinopP9Classfile
	.p2align	4, 0x90
	.type	_Z9pushbinopP9Classfile,@function
_Z9pushbinopP9Classfile:                # @_Z9pushbinopP9Classfile
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %ebp
	decl	%ebp
	movq	stkptr(%rip), %r14
	leaq	-8(%r14), %rax
	movq	%rax, stkptr(%rip)
	movq	-16(%r14), %r12
	movq	-8(%r14), %r15
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	16(%r12), %eax
	cmpl	16(%r15), %eax
	movq	%r15, %rax
	cmovbq	%r12, %rax
	movl	16(%rax), %eax
	cmpl	%ebp, %eax
	cmovael	%ebp, %eax
	movq	(%r12), %rcx
	movl	8(%rcx), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movl	ch(%rip), %r13d
	cmpl	$115, %r13d
	jg	.LBB0_2
# BB#1:
	addl	$-96, %r13d
	sarl	$2, %r13d
	jmp	.LBB0_3
.LBB0_2:
	addl	$-120, %r13d
	sarl	%r13d
	addl	$7, %r13d
.LBB0_3:
	movl	$1, 8(%rbx)
	movl	%ebp, 12(%rbx)
	movl	%eax, 16(%rbx)
.Ltmp0:
	movl	$24, %edi
	callq	_Znwm
.Ltmp1:
# BB#4:
	movl	$0, (%rax)
	movl	$4, 4(%rax)
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 8(%rax)
	movl	%r13d, 12(%rax)
	movq	%rax, (%rbx)
	movq	%r12, 24(%rbx)
	movq	%r15, 32(%rbx)
	movq	%rbx, -16(%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_5:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z9pushbinopP9Classfile, .Lfunc_end0-_Z9pushbinopP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	39                      # 0x27
.LCPI1_1:
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	19                      # 0x13
.LCPI1_2:
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	8                       # 0x8
	.long	18                      # 0x12
.LCPI1_3:
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	17                      # 0x11
	.text
	.globl	_Z8pushunopP9Classfile
	.p2align	4, 0x90
	.type	_Z8pushunopP9Classfile,@function
_Z8pushunopP9Classfile:                 # @_Z8pushunopP9Classfile
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	ch(%rip), %esi
	addl	$-116, %esi
	cmpl	$75, %esi
	ja	.LBB1_33
# BB#1:
	movl	currpc(%rip), %eax
	leal	-1(%rax), %r12d
	movq	stkptr(%rip), %rcx
	leaq	-8(%rcx), %rdx
	movq	-8(%rcx), %r15
	movl	$4, %ebx
	movl	$15, %r14d
	jmpq	*.LJTI1_0(,%rsi,8)
.LBB1_34:
	movq	(%r15), %rax
	movl	8(%rax), %ebx
	movl	$14, %r14d
	jmp	.LBB1_35
.LBB1_2:
	movl	$5, %ebx
	jmp	.LBB1_35
.LBB1_3:
	movl	$6, %ebx
	jmp	.LBB1_35
.LBB1_4:
	movl	$7, %ebx
	jmp	.LBB1_35
.LBB1_19:
	movl	$stack, %esi
	cmpq	%rsi, %rcx
	je	.LBB1_27
# BB#20:
	cmpl	$-1, cond_pcend(%rip)
	jne	.LBB1_21
# BB#23:
	addl	$2, %eax
	movl	%eax, currpc(%rip)
	addl	$-2, bufflength(%rip)
	movq	inbuff(%rip), %rsi
	leaq	2(%rsi), %rdi
	movq	%rdi, inbuff(%rip)
	movzbl	(%rsi), %edi
	shll	$8, %edi
	movzbl	1(%rsi), %esi
	orl	%edi, %esi
	movswl	%si, %esi
	addl	%r12d, %esi
	movl	%esi, cond_pcend(%rip)
	movq	%rcx, cond_stkptr(%rip)
	movq	%rdx, stkptr(%rip)
	movq	-8(%rcx), %rcx
	movq	%rcx, cond_e2(%rip)
	movq	donestkptr(%rip), %rdx
	leaq	-8(%rdx), %rcx
	movq	%rcx, donestkptr(%rip)
	movq	-8(%rdx), %rdx
	movq	(%rdx), %rsi
	cmpl	$8, 4(%rsi)
	jne	.LBB1_26
# BB#24:
	cmpl	%eax, 48(%rdx)
	jne	.LBB1_25
# BB#38:
	movq	%rdx, cond_e(%rip)
	movq	%rcx, cond_donestkptr(%rip)
	jmp	.LBB1_39
.LBB1_5:
	movl	$1, %ebx
	jmp	.LBB1_35
.LBB1_6:
	movl	$2, %ebx
	jmp	.LBB1_35
.LBB1_7:
	movl	$3, %ebx
	jmp	.LBB1_35
.LBB1_13:
	addl	$2, %eax
	movl	%eax, currpc(%rip)
	addl	$-2, bufflength(%rip)
	movq	inbuff(%rip), %rax
	leaq	2(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movzbl	(%rax), %ecx
	shll	$8, %ecx
	movzbl	1(%rax), %eax
	orl	%ecx, %eax
	movq	40(%rdi), %rcx
	movzwl	%ax, %r13d
	movq	%r13, %rax
	shlq	$4, %rax
	movq	8(%rcx,%rax), %rax
	shlq	$4, %rax
	movq	8(%rcx,%rax), %rbx
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, %r14
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp11:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp12:
# BB#14:                                # %.noexc
	movq	%r14, (%rbp)
	movl	$0, 8(%rbp)
	movl	$2, 12(%rbp)
	movl	%r13d, 16(%rbp)
	movl	$1, 8(%r15)
	movl	%r12d, 16(%r15)
	movl	%r12d, 12(%r15)
.Ltmp13:
	movl	$24, %edi
	callq	_Znwm
.Ltmp14:
# BB#15:
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [0,1,0,39]
	movups	%xmm0, (%rax)
	movq	%rbp, 16(%rax)
	movq	%rax, (%r15)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movl	$1, 8(%rbp)
	movl	%r12d, 12(%rbp)
	movl	%r12d, 16(%rbp)
.Ltmp16:
	movl	$24, %edi
	callq	_Znwm
.Ltmp17:
# BB#16:
	movaps	.LCPI1_2(%rip), %xmm0   # xmm0 = [0,2,8,18]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbp)
	movq	%r15, 24(%rbp)
	movq	$0, 32(%rbp)
	movq	stkptr(%rip), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, stkptr(%rip)
	movq	%rbp, (%rax)
.LBB1_17:
	xorl	%ebx, %ebx
	movl	$18, %r14d
.LBB1_35:
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movl	16(%r15), %eax
	cmpl	%r12d, %eax
	cmovael	%r12d, %eax
	movl	$1, 8(%rbp)
	movl	%r12d, 12(%rbp)
	movl	%eax, 16(%rbp)
.Ltmp22:
	movl	$24, %edi
	callq	_Znwm
.Ltmp23:
# BB#36:
	movl	$0, (%rax)
	movl	$2, 4(%rax)
	movl	%ebx, 8(%rax)
	movl	%r14d, 12(%rax)
	movq	%rax, (%rbp)
	movq	%r15, 24(%rbp)
	movq	$0, 32(%rbp)
	movq	stkptr(%rip), %rax
	movq	%rbp, -8(%rax)
.LBB1_39:
	xorl	%eax, %eax
.LBB1_40:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_8:
	movq	%rdx, stkptr(%rip)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movl	16(%r15), %eax
	cmpl	%r12d, %eax
	cmovael	%r12d, %eax
	movl	$1, 8(%rbp)
	movl	%r12d, 12(%rbp)
	movl	%eax, 16(%rbp)
.Ltmp19:
	movl	$24, %edi
	callq	_Znwm
.Ltmp20:
# BB#9:
	movaps	.LCPI1_3(%rip), %xmm0   # xmm0 = [0,2,0,17]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbp)
	movq	%r15, 24(%rbp)
	movq	$0, 32(%rbp)
	jmp	.LBB1_10
.LBB1_27:
	addl	$2, %eax
	movl	%eax, currpc(%rip)
	addl	$-2, bufflength(%rip)
	movq	inbuff(%rip), %rax
	leaq	2(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movzbl	(%rax), %ecx
	shll	$8, %ecx
	movzbl	1(%rax), %eax
	orl	%ecx, %eax
	movswl	%ax, %r14d
	addl	%r12d, %r14d
	movl	$100, %edi
	callq	_Znam
	movq	%rax, %rbx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	sprintf
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, %r15
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	callq	_ZdaPv
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp3:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp4:
# BB#28:                                # %.noexc78
	movq	%r15, (%rbp)
	movl	$0, 8(%rbp)
	movl	$1, 12(%rbp)
	movl	$0, 16(%rbp)
	movl	$1, 8(%rbx)
	movl	%r12d, 16(%rbx)
	movl	%r12d, 12(%rbx)
.Ltmp5:
	movl	$24, %edi
	callq	_Znwm
.Ltmp6:
# BB#29:
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [0,1,0,39]
	movups	%xmm0, (%rax)
	movq	%rbp, 16(%rax)
	movq	%rax, (%rbx)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movl	$1, 8(%rbp)
	movl	%r12d, 16(%rbp)
	movl	%r12d, 12(%rbp)
.Ltmp8:
	movl	$24, %edi
	callq	_Znwm
.Ltmp9:
# BB#30:
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [0,2,0,19]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbp)
	movq	%rbx, 24(%rbp)
	movl	%r14d, 48(%rbp)
.LBB1_10:
	movq	donestkptr(%rip), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, donestkptr(%rip)
	movq	%rbp, (%rax)
	jmp	.LBB1_39
.LBB1_21:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$46, %esi
	jmp	.LBB1_22
.LBB1_26:
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$57, %esi
	jmp	.LBB1_22
.LBB1_25:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$31, %esi
.LBB1_22:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %eax
	jmp	.LBB1_40
.LBB1_33:
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$33, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$-1, %edi
	callq	exit
.LBB1_32:
.Ltmp10:
	jmp	.LBB1_12
.LBB1_31:
.Ltmp7:
	movq	%rax, %r14
	movq	%rbx, %rdi
	jmp	.LBB1_42
.LBB1_11:
.Ltmp21:
	jmp	.LBB1_12
.LBB1_18:
.Ltmp18:
	jmp	.LBB1_12
.LBB1_41:
.Ltmp15:
	movq	%rax, %r14
	movq	%r15, %rdi
	jmp	.LBB1_42
.LBB1_37:
.Ltmp24:
.LBB1_12:
	movq	%rax, %r14
	movq	%rbp, %rdi
.LBB1_42:
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_Z8pushunopP9Classfile, .Lfunc_end1-_Z8pushunopP9Classfile
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_2
	.quad	.LBB1_3
	.quad	.LBB1_4
	.quad	.LBB1_35
	.quad	.LBB1_3
	.quad	.LBB1_4
	.quad	.LBB1_35
	.quad	.LBB1_2
	.quad	.LBB1_4
	.quad	.LBB1_35
	.quad	.LBB1_2
	.quad	.LBB1_3
	.quad	.LBB1_5
	.quad	.LBB1_6
	.quad	.LBB1_7
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_19
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_17
	.quad	.LBB1_13
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_33
	.quad	.LBB1_8
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\254\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\251\001"              # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp11-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp14-.Ltmp11         #   Call between .Ltmp11 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin1   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp16-.Ltmp14         #   Call between .Ltmp14 and .Ltmp16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp22-.Ltmp17         #   Call between .Ltmp17 and .Ltmp22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin1   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp19-.Ltmp23         #   Call between .Ltmp23 and .Ltmp19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin1   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp3-.Ltmp20          #   Call between .Ltmp20 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 10 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin1    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 11 <<
	.long	.Ltmp8-.Ltmp6           #   Call between .Ltmp6 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 12 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin1   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 13 <<
	.long	.Lfunc_end1-.Ltmp9      #   Call between .Ltmp9 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Can't handle recursive conditional operators!\n"
	.size	.L.str, 47

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Error in conditional operator!\n"
	.size	.L.str.1, 32

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Use of comma operator in conditionals not yet supported.\n"
	.size	.L.str.2, 58

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"label%i"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Error in pushing unary operation\n"
	.size	.L.str.4, 34


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
