	.text
	.file	"z32.bc"
	.globl	Next
	.p2align	4, 0x90
	.type	Next,@function
Next:                                   # @Next
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$552, %rsp              # imm = 0x228
.Lcfi6:
	.cfi_def_cfa_offset 608
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r13d
	movq	%rdi, %rbx
	movzbl	32(%rbx), %edi
	movl	%edi, %eax
	decb	%al
	cmpb	$98, %al
	ja	.LBB0_34
# BB#1:
	movzbl	%al, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_22:
	movq	(%rbx), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB0_23:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB0_23
# BB#24:
	movl	%r13d, %esi
	movq	%r14, %rdx
	callq	Next
.LBB0_35:                               # %.critedge4
	movq	%rbx, %rax
.LBB0_36:                               # %.critedge4
	addq	$552, %rsp              # imm = 0x228
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_25:
	movq	(%rbx), %rbp
	cmpq	%rbx, %rbp
	jne	.LBB0_27
	jmp	.LBB0_35
.LBB0_2:
	leaq	32(%rbx), %r15
	leaq	64(%rbx), %r12
	movq	%r12, %rdi
	callq	strlen
	movslq	%eax, %rcx
	movq	%rax, %rdx
	shlq	$32, %rdx
	leal	-1(%rcx), %eax
	movabsq	$4294967296, %rbp       # imm = 0x100000000
	addq	%rdx, %rbp
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	testq	%rcx, %rcx
	jle	.LBB0_35
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movzbl	63(%rbx,%rcx), %edx
	decq	%rcx
	addb	$-48, %dl
	decl	%eax
	addq	%rsi, %rbp
	cmpb	$9, %dl
	ja	.LBB0_3
# BB#5:                                 # %.critedge
	testl	%eax, %eax
	movq	%r15, 24(%rsp)          # 8-byte Spill
	js	.LBB0_9
# BB#6:                                 # %.lr.ph.preheader
	movslq	%eax, %rcx
	addq	$64, %rcx
.LBB0_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx,%rcx), %edx
	addb	$-48, %dl
	cmpb	$9, %dl
	ja	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_7 Depth=1
	decl	%eax
	leaq	-1(%rcx), %rdx
	addq	$-64, %rcx
	testq	%rcx, %rcx
	movq	%rdx, %rcx
	jg	.LBB0_7
.LBB0_9:                                # %.critedge1
	movslq	%eax, %r15
	leaq	1(%r12,%r15), %rdi
	leaq	20(%rsp), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	sscanf
	movb	$0, 1(%r12,%r15)
	leaq	32(%rsp), %r15
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	strcpy
	addl	20(%rsp), %r13d
	movl	%r13d, %edi
	callq	StringInt
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	strcat
	sarq	$32, %rbp
	addq	%r12, %rbp
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	strcat
	movq	%r15, %rdi
	callq	strlen
	cmpq	$512, %rax              # imm = 0x200
	movq	24(%rsp), %rbp          # 8-byte Reload
	jb	.LBB0_11
# BB#10:
	leaq	32(%rsp), %r9
	movl	$32, %edi
	movl	$1, %esi
	movl	$.L.str.1, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
.LBB0_11:                               # %.critedge1._crit_edge
	movzbl	32(%rbx), %edi
	leaq	32(%rsp), %rsi
	movq	%rbp, %rdx
	callq	MakeWord
	movl	$4095, %edx             # imm = 0xFFF
	andl	40(%rbx), %edx
	movl	$-4096, %ecx            # imm = 0xF000
	andl	40(%rax), %ecx
	orl	%edx, %ecx
	movl	%ecx, 40(%rax)
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	40(%rbx), %edx
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%edx, %ecx
	movl	%ecx, 40(%rax)
	movl	$4194304, %edx          # imm = 0x400000
	andl	40(%rbx), %edx
	andl	$-4194305, %ecx         # imm = 0xFFBFFFFF
	orl	%edx, %ecx
	movl	%ecx, 40(%rax)
	movl	$528482304, %edx        # imm = 0x1F800000
	andl	40(%rbx), %edx
	andl	$-528482305, %ecx       # imm = 0xE07FFFFF
	orl	%edx, %ecx
	movl	%ecx, 40(%rax)
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	40(%rbx), %edx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 40(%rax)
	movl	$1610612736, %edx       # imm = 0x60000000
	andl	40(%rbx), %edx
	andl	$-1610612737, %ecx      # imm = 0x9FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 40(%rax)
	movq	%rax, xx_res(%rip)
	movq	%rbx, xx_hold(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	24(%rbx), %rcx
	cmpq	%rbx, %rcx
	je	.LBB0_12
# BB#13:
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rbx), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB0_16
# BB#14:
	testq	%rax, %rax
	je	.LBB0_16
# BB#15:
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	jmp	.LBB0_16
.LBB0_33:                               # %.outer
                                        #   in Loop: Header=BB0_27 Depth=1
	movq	(%rbp), %rbp
	cmpq	%rbx, %rbp
	je	.LBB0_35
.LBB0_27:                               # %.lr.ph99.split.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_28 Depth 2
                                        #       Child Loop BB0_29 Depth 3
	cmpl	$0, (%r14)
	jne	.LBB0_35
	.p2align	4, 0x90
.LBB0_28:                               # %.preheader.preheader
                                        #   Parent Loop BB0_27 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_29 Depth 3
	movq	%rbp, %rdi
	.p2align	4, 0x90
.LBB0_29:                               # %.preheader
                                        #   Parent Loop BB0_27 Depth=1
                                        #     Parent Loop BB0_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %ecx
	testb	%cl, %cl
	je	.LBB0_29
# BB#30:                                #   in Loop: Header=BB0_28 Depth=2
	addb	$-119, %cl
	xorl	%eax, %eax
	cmpb	$20, %cl
	jb	.LBB0_32
# BB#31:                                #   in Loop: Header=BB0_28 Depth=2
	movl	%r13d, %esi
	movq	%r14, %rdx
	callq	Next
	movl	(%r14), %eax
	testl	%eax, %eax
	je	.LBB0_33
.LBB0_32:                               # %.lr.ph99.split.backedge
                                        #   in Loop: Header=BB0_28 Depth=2
	testl	%eax, %eax
	je	.LBB0_28
	jmp	.LBB0_35
.LBB0_34:
	movq	no_fpos(%rip), %rbp
	callq	Image
	movq	%rax, (%rsp)
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.2, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
	jmp	.LBB0_35
.LBB0_12:                               # %.thread
	movq	$0, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB0_16:
	movq	%rbx, zz_hold(%rip)
	movq	8(%rbx), %rcx
	cmpq	%rbx, %rcx
	je	.LBB0_17
# BB#18:
	movq	%rcx, zz_res(%rip)
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	xx_res(%rip), %rdx
	movq	%rcx, xx_tmp(%rip)
	movq	%rdx, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB0_21
# BB#19:
	testq	%rdx, %rdx
	je	.LBB0_21
# BB#20:
	movq	(%rcx), %rsi
	movq	%rsi, zz_tmp(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	zz_hold(%rip), %rcx
	movq	zz_res(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	zz_tmp(%rip), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_tmp(%rip), %rdx
	movq	%rcx, 8(%rdx)
	jmp	.LBB0_21
.LBB0_17:                               # %.thread117
	movq	$0, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB0_21:
	movq	xx_hold(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %edx
	leaq	zz_lengths(%rdx), %rsi
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
	addb	$-11, %dl
	leaq	33(%rcx), %rdi
	cmpb	$2, %dl
	cmovbq	%rdi, %rsi
	movzbl	(%rsi), %edx
	movl	%edx, zz_size(%rip)
	movq	zz_free(,%rdx,8), %rsi
	movq	%rsi, (%rcx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rdx,8)
	movl	$1, (%r14)
	jmp	.LBB0_36
.Lfunc_end0:
	.size	Next, .Lfunc_end0-Next
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_35
	.quad	.LBB0_35
	.quad	.LBB0_34
	.quad	.LBB0_35
	.quad	.LBB0_35
	.quad	.LBB0_35
	.quad	.LBB0_35
	.quad	.LBB0_34
	.quad	.LBB0_22
	.quad	.LBB0_34
	.quad	.LBB0_2
	.quad	.LBB0_2
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_25
	.quad	.LBB0_25
	.quad	.LBB0_25
	.quad	.LBB0_25
	.quad	.LBB0_25
	.quad	.LBB0_22
	.quad	.LBB0_35
	.quad	.LBB0_22
	.quad	.LBB0_35
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_35
	.quad	.LBB0_35
	.quad	.LBB0_35
	.quad	.LBB0_35
	.quad	.LBB0_35
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_35
	.quad	.LBB0_35
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"word %s is too long"
	.size	.L.str.1, 20

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"assert failed in %s %s"
	.size	.L.str.2, 23

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Next:"
	.size	.L.str.3, 6


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
