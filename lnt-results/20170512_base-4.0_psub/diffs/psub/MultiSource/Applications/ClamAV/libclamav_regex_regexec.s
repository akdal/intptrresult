	.text
	.file	"libclamav_regex_regexec.bc"
	.globl	cli_regexec
	.p2align	4, 0x90
	.type	cli_regexec,@function
cli_regexec:                            # @cli_regexec
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 304
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movl	$2, %eax
	cmpl	$62053, (%rdi)          # imm = 0xF265
	jne	.LBB0_213
# BB#1:
	movq	24(%rdi), %rbx
	cmpl	$53829, (%rbx)          # imm = 0xD245
	jne	.LBB0_213
# BB#2:
	testb	$4, 72(%rbx)
	jne	.LBB0_213
# BB#3:
	movl	%r8d, %ebp
	andl	$7, %ebp
	movq	48(%rbx), %r15
	cmpq	$64, %r15
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	ja	.LBB0_6
# BB#4:
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	56(%rbx), %rbp
	movq	64(%rbx), %r15
	xorl	%eax, %eax
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	testb	$4, 40(%rbx)
	cmoveq	%rdx, %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	testb	$4, %r8b
	jne	.LBB0_8
# BB#5:
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rcx
	movq	%r12, %r13
	jmp	.LBB0_9
.LBB0_6:
	movq	56(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	64(%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	testb	$4, 40(%rbx)
	cmoveq	%rdx, %r13
	testb	$4, %r8b
	jne	.LBB0_101
# BB#7:
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rsi
	movq	%r12, %rdx
	jmp	.LBB0_102
.LBB0_8:
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r13
	addq	%r12, %r13
	movq	8(%rax), %rcx
.LBB0_9:
	addq	%r12, %rcx
	movl	$16, %eax
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	cmpq	%r13, %rcx
	jb	.LBB0_213
# BB#10:
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	96(%rax), %rax
	movq	%rax, %r14
	testq	%rax, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%r15, 16(%rsp)          # 8-byte Spill
	je	.LBB0_18
# BB#11:                                # %.preheader172.i
	cmpq	%rcx, %r13
	movq	%r13, %rbx
	jae	.LBB0_17
# BB#12:                                # %.lr.ph193.i
	movb	(%r14), %r15b
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB0_13:                               # =>This Inner Loop Header: Depth=1
	cmpb	%r15b, (%rbx)
	jne	.LBB0_16
# BB#14:                                #   in Loop: Header=BB0_13 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	subq	%rbx, %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movslq	104(%rcx), %rdx
	cmpq	%rdx, %rax
	jl	.LBB0_16
# BB#15:                                #   in Loop: Header=BB0_13 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_17
.LBB0_16:                               #   in Loop: Header=BB0_13 Depth=1
	incq	%rbx
	cmpq	24(%rsp), %rbx          # 8-byte Folded Reload
	jb	.LBB0_13
.LBB0_17:                               # %._crit_edge194.i
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %rbx
	movq	16(%rsp), %r15          # 8-byte Reload
	je	.LBB0_203
.LBB0_18:
	incq	%rbp
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 72(%rsp)
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 80(%rsp)
	movq	$0, 88(%rsp)
	movq	$0, 128(%rsp)
	movq	%r12, 96(%rsp)
	movq	%r13, 104(%rsp)
	movq	%rcx, 112(%rsp)
	movl	$1, %eax
	movl	$1, %edx
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movl	%ebp, %ecx
	shlq	%cl, %rdx
	movq	%rdx, 232(%rsp)         # 8-byte Spill
	movl	%r15d, %ecx
	shlq	%cl, %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 160(%rsp)
	movdqu	%xmm0, 144(%rsp)
	movq	%r13, %rbp
	cmpq	%rbp, %r13
	je	.LBB0_21
.LBB0_20:
	movsbl	-1(%rbp), %ebx
	jmp	.LBB0_22
.LBB0_21:
	movl	$128, %ebx
.LBB0_22:
	movq	72(%rsp), %r14
	movl	$132, %r8d
	movq	%r14, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	232(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, %r9
	callq	sstep
	movq	%rax, %r12
	movq	112(%rsp), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movl	80(%rsp), %eax
	movl	%eax, %ecx
	andl	$2, %ecx
	movl	%ecx, 224(%rsp)         # 4-byte Spill
	andl	$1, %eax
	movl	%eax, 228(%rsp)         # 4-byte Spill
	movq	%r12, %r15
	xorl	%r13d, %r13d
	movl	%ebx, %eax
	movq	%r14, 56(%rsp)          # 8-byte Spill
	movq	%r12, 240(%rsp)         # 8-byte Spill
	cmpq	200(%rsp), %rbp         # 8-byte Folded Reload
	jne	.LBB0_24
	jmp	.LBB0_25
	.p2align	4, 0x90
.LBB0_23:
	movq	%r14, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movl	12(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %r8d
	movq	240(%rsp), %r12         # 8-byte Reload
	movq	%r12, %r9
	callq	sstep
	movq	%rax, %r15
	incq	%rbp
	movl	%ebx, %eax
	cmpq	200(%rsp), %rbp         # 8-byte Folded Reload
	jne	.LBB0_24
	jmp	.LBB0_25
.LBB0_59:
	cmpl	$128, %esi
	je	.LBB0_54
# BB#60:
	cmpl	$95, %esi
	je	.LBB0_54
# BB#61:
	movslq	%esi, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$8, %eax
	testw	%ax, %ax
	je	.LBB0_56
	jmp	.LBB0_54
	.p2align	4, 0x90
.LBB0_24:
	movsbl	(%rbp), %ecx
	jmp	.LBB0_26
	.p2align	4, 0x90
.LBB0_25:
	movl	$128, %ecx
.LBB0_26:
	cmpq	%r12, %r15
	cmoveq	%rbp, %r13
	xorl	%r12d, %r12d
	cmpl	$128, %eax
	movq	%rbp, 216(%rsp)         # 8-byte Spill
	movq	%r13, 208(%rsp)         # 8-byte Spill
	je	.LBB0_29
# BB#27:
	cmpl	$10, %eax
	jne	.LBB0_31
# BB#28:
	testb	$8, 40(%r14)
	movl	$0, %r13d
	movq	48(%rsp), %rbx          # 8-byte Reload
	jne	.LBB0_30
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_29:
	cmpl	$0, 228(%rsp)           # 4-byte Folded Reload
	movl	$0, %r13d
	movq	48(%rsp), %rbx          # 8-byte Reload
	jne	.LBB0_32
.LBB0_30:
	movl	76(%r14), %r13d
	movl	$129, %r12d
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_31:
	xorl	%r13d, %r13d
	movq	48(%rsp), %rbx          # 8-byte Reload
.LBB0_32:
	cmpl	$128, %ecx
	movl	%eax, 32(%rsp)          # 4-byte Spill
	je	.LBB0_35
# BB#33:
	cmpl	$10, %ecx
	jne	.LBB0_37
# BB#34:
	testb	$8, 40(%r14)
	jne	.LBB0_36
	jmp	.LBB0_37
	.p2align	4, 0x90
.LBB0_35:
	cmpl	$0, 224(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_37
.LBB0_36:
	xorl	%eax, %eax
	cmpl	$129, %r12d
	sete	%al
	orl	$130, %eax
	addl	80(%r14), %r13d
	movl	%eax, %r12d
.LBB0_37:
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	testl	%r13d, %r13d
	jle	.LBB0_40
# BB#38:                                # %.preheader.i.i.preheader
	incl	%r13d
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_39:                               # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movl	%r12d, %r8d
	movq	%r15, %r9
	callq	sstep
	movq	%rax, %r15
	decl	%r13d
	cmpl	$1, %r13d
	jg	.LBB0_39
.LBB0_40:                               # %.loopexit.i.i
	cmpl	$129, %r12d
	jne	.LBB0_43
# BB#41:
	movl	$129, %r13d
	cmpl	$128, 12(%rsp)          # 4-byte Folded Reload
	movq	56(%rsp), %r14          # 8-byte Reload
	movl	32(%rsp), %edi          # 4-byte Reload
	je	.LBB0_49
# BB#42:                                # %._crit_edge.i.i
	callq	__ctype_b_loc
	movl	32(%rsp), %edi          # 4-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	(%rax), %rax
	jmp	.LBB0_48
	.p2align	4, 0x90
.LBB0_43:
	cmpl	$128, 32(%rsp)          # 4-byte Folded Reload
	movq	56(%rsp), %r14          # 8-byte Reload
	jne	.LBB0_45
# BB#44:
	movl	%r12d, %r13d
	movq	24(%rsp), %r12          # 8-byte Reload
	jmp	.LBB0_54
	.p2align	4, 0x90
.LBB0_45:
	callq	__ctype_b_loc
	movl	32(%rsp), %edi          # 4-byte Reload
	movq	(%rax), %rax
	movslq	%edi, %rcx
	movzwl	(%rax,%rcx,2), %ecx
	movl	12(%rsp), %esi          # 4-byte Reload
	cmpl	$128, %esi
	je	.LBB0_52
# BB#46:
	cmpl	$95, %edi
	je	.LBB0_52
# BB#47:
	movl	%ecx, %edx
	andl	$8, %edx
	testw	%dx, %dx
	jne	.LBB0_52
.LBB0_48:
	movslq	%esi, %rcx
	testb	$8, (%rax,%rcx,2)
	movl	$133, %eax
	cmovnel	%eax, %r12d
	cmpl	$95, %esi
	cmovel	%eax, %r12d
	movl	%r12d, %r13d
.LBB0_49:
	cmpl	$128, %edi
	jne	.LBB0_51
# BB#50:
	movq	24(%rsp), %r12          # 8-byte Reload
	jmp	.LBB0_54
	.p2align	4, 0x90
.LBB0_51:                               # %..thread132_crit_edge.i.i
	callq	__ctype_b_loc
	movl	32(%rsp), %edi          # 4-byte Reload
	movslq	%edi, %rcx
	movq	(%rax), %rax
	movw	(%rax,%rcx,2), %cx
	movq	24(%rsp), %r12          # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	cmpl	$95, %edi
	jne	.LBB0_53
	jmp	.LBB0_55
	.p2align	4, 0x90
.LBB0_52:
	movl	%r12d, %r13d
	movq	24(%rsp), %r12          # 8-byte Reload
	cmpl	$95, %edi
	je	.LBB0_55
.LBB0_53:                               # %.thread132.i.i
	andl	$8, %ecx
	testw	%cx, %cx
	jne	.LBB0_55
.LBB0_54:                               # %.thread.i.i
	leal	-133(%r13), %eax
	cmpl	$1, %eax
	movl	%r13d, %r8d
	jbe	.LBB0_56
	jmp	.LBB0_57
	.p2align	4, 0x90
.LBB0_55:
	movl	$134, %r8d
	cmpl	$130, %r13d
	jne	.LBB0_59
	.p2align	4, 0x90
.LBB0_56:                               # %.thread134.i.i
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movq	%r15, %r9
	callq	sstep
	movq	%rax, %r15
.LBB0_57:
	movq	%r15, %rax
	andq	192(%rsp), %rax         # 8-byte Folded Reload
	movq	216(%rsp), %rbp         # 8-byte Reload
	cmpq	%r12, %rbp
	movq	208(%rsp), %r13         # 8-byte Reload
	je	.LBB0_62
# BB#58:
	testq	%rax, %rax
	je	.LBB0_23
.LBB0_62:                               # %sfast.exit.i
	testq	%rax, %rax
	movq	%r13, 120(%rsp)
	je	.LBB0_204
# BB#63:
	cmpq	$0, 184(%rsp)           # 8-byte Folded Reload
	jne	.LBB0_65
# BB#64:
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 120(%rax)
	je	.LBB0_98
.LBB0_65:                               # %.preheader168.i
	leaq	72(%rsp), %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	sslow
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB0_68
# BB#66:                                # %.lr.ph188.i.preheader
	incq	%r13
	movq	%r13, %rbx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	48(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_67:                               # %.lr.ph188.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, 120(%rsp)
	leaq	72(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movq	%r13, %rcx
	movq	%rbp, %r8
	callq	sslow
	movq	%rax, %r15
	incq	%rbx
	testq	%r15, %r15
	je	.LBB0_67
.LBB0_68:                               # %._crit_edge.i
	cmpq	$1, 184(%rsp)           # 8-byte Folded Reload
	jne	.LBB0_70
# BB#69:
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 120(%rax)
	je	.LBB0_91
.LBB0_70:
	movq	88(%rsp), %rax
	testq	%rax, %rax
	jne	.LBB0_73
# BB#71:
	movq	112(%r14), %rdi
	shlq	$4, %rdi
	addq	$16, %rdi
	callq	cli_malloc
	movq	%rax, 88(%rsp)
	testq	%rax, %rax
	je	.LBB0_212
# BB#72:                                # %..thread.preheader_crit_edge.i
	movq	72(%rsp), %r14
.LBB0_73:                               # %.thread.preheader.i
	movq	112(%r14), %rcx
	testq	%rcx, %rcx
	pcmpeqd	%xmm0, %xmm0
	je	.LBB0_76
# BB#74:                                # %.thread.i.preheader
	addq	$16, %rax
	movl	$1, %edx
	.p2align	4, 0x90
.LBB0_75:                               # %.thread.i
                                        # =>This Inner Loop Header: Depth=1
	incq	%rdx
	movdqu	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rcx, %rdx
	jbe	.LBB0_75
.LBB0_76:                               # %.thread._crit_edge.i
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 120(%rax)
	leaq	72(%rsp), %r13
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	jne	.LBB0_79
# BB#77:
	testb	$4, 81(%rsp)
	jne	.LBB0_79
# BB#78:
	movq	120(%rsp), %rsi
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	movq	%r14, %r8
	callq	sdissect
	testq	%rax, %rax
	je	.LBB0_86
	jmp	.LBB0_91
.LBB0_79:
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	128(%rax), %rcx
	movq	128(%rsp), %rax
	testq	%rcx, %rcx
	jle	.LBB0_82
# BB#80:
	testq	%rax, %rax
	jne	.LBB0_82
# BB#81:
	leaq	8(,%rcx,8), %rdi
	callq	cli_malloc
	movq	%rax, 128(%rsp)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	128(%rcx), %rcx
.LBB0_82:
	testq	%rax, %rax
	jne	.LBB0_84
# BB#83:
	testq	%rcx, %rcx
	jg	.LBB0_205
.LBB0_84:
	movq	120(%rsp), %rsi
	movl	$0, (%rsp)
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	jmp	.LBB0_85
.LBB0_19:                               # %._crit_edge204.i
	incq	%rbx
	movq	%rbx, %rbp
	movq	104(%rsp), %r13
	cmpq	%rbp, %r13
	jne	.LBB0_20
	jmp	.LBB0_21
	.p2align	4, 0x90
.LBB0_85:
	movq	%r15, %rdx
	movq	%rbp, %rcx
	movq	%r14, %r8
	callq	sbackref
	testq	%rax, %rax
	jne	.LBB0_91
.LBB0_86:                               # %.preheader167.i
	movq	120(%rsp), %rbx
	cmpq	%rbx, %r15
	jbe	.LBB0_90
# BB#87:
	decq	%r15
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	movq	%r14, %r8
	callq	sslow
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_89
# BB#88:
	movl	$0, (%rsp)
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	movq	%rbx, %rsi
	jmp	.LBB0_85
.LBB0_89:
	xorl	%r15d, %r15d
.LBB0_90:
	cmpq	%r12, %rbx
	jne	.LBB0_19
.LBB0_91:                               # %.loopexit.i
	cmpq	$0, 184(%rsp)           # 8-byte Folded Reload
	je	.LBB0_98
# BB#92:
	movq	96(%rsp), %rax
	movq	120(%rsp), %rcx
	subq	%rax, %rcx
	subq	%rax, %r15
	cmpq	$1, 184(%rsp)           # 8-byte Folded Reload
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%r15, 8(%rax)
	je	.LBB0_98
# BB#93:                                # %.lr.ph.preheader.i
	movq	72(%rsp), %rax
	movl	$1, %ecx
	movl	$16, %edx
	pcmpeqd	%xmm0, %xmm0
.LBB0_94:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	112(%rax), %rcx
	jbe	.LBB0_96
# BB#95:                                #   in Loop: Header=BB0_94 Depth=1
	movq	64(%rsp), %rsi          # 8-byte Reload
	movdqu	%xmm0, (%rsi,%rdx)
	jmp	.LBB0_97
.LBB0_96:                               #   in Loop: Header=BB0_94 Depth=1
	movq	88(%rsp), %rsi
	movups	(%rsi,%rdx), %xmm1
	movq	64(%rsp), %rsi          # 8-byte Reload
	movups	%xmm1, (%rsi,%rdx)
.LBB0_97:                               #   in Loop: Header=BB0_94 Depth=1
	incq	%rcx
	addq	$16, %rdx
	cmpq	184(%rsp), %rcx         # 8-byte Folded Reload
	jb	.LBB0_94
.LBB0_98:                               # %.thread166.i
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_100
# BB#99:
	callq	free
.LBB0_100:
	movq	128(%rsp), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	jne	.LBB0_201
	jmp	.LBB0_213
.LBB0_101:
	movq	(%rcx), %rdx
	addq	%r12, %rdx
	movq	8(%rcx), %rsi
.LBB0_102:
	addq	%r12, %rsi
	movl	$16, %eax
	cmpq	%rdx, %rsi
	jb	.LBB0_213
# BB#103:
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	96(%rbx), %r14
	testq	%r14, %r14
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	je	.LBB0_111
# BB#104:                               # %.preheader192.i
	cmpq	%rsi, %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rdx, %rbp
	jae	.LBB0_110
# BB#105:                               # %.lr.ph214.i
	movb	(%r14), %bl
	movq	32(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_106:                              # =>This Inner Loop Header: Depth=1
	cmpb	%bl, (%rbp)
	jne	.LBB0_109
# BB#107:                               #   in Loop: Header=BB0_106 Depth=1
	movq	%rsi, %rax
	subq	%rbp, %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movslq	104(%rcx), %rdx
	cmpq	%rdx, %rax
	jl	.LBB0_109
# BB#108:                               #   in Loop: Header=BB0_106 Depth=1
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	memcmp
	movq	56(%rsp), %rsi          # 8-byte Reload
	testl	%eax, %eax
	je	.LBB0_110
.LBB0_109:                              #   in Loop: Header=BB0_106 Depth=1
	incq	%rbp
	cmpq	%rsi, %rbp
	jb	.LBB0_106
.LBB0_110:                              # %._crit_edge215.i
	cmpq	%rsi, %rbp
	movq	32(%rsp), %rdx          # 8-byte Reload
	je	.LBB0_203
.LBB0_111:
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, 72(%rsp)
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 80(%rsp)
	movq	$0, 88(%rsp)
	movq	$0, 128(%rsp)
	movq	%r12, 96(%rsp)
	movq	%rdx, 104(%rsp)
	movq	%rsi, 112(%rsp)
	shlq	$2, %r15
	movq	%r15, %rdi
	movq	%rdx, %r15
	callq	cli_malloc
	movq	%rax, %r14
	movq	%r14, 144(%rsp)
	movl	$12, %eax
	testq	%r14, %r14
	je	.LBB0_213
# BB#112:
	movq	%r13, 192(%rsp)         # 8-byte Spill
	movq	16(%rsp), %rbx          # 8-byte Reload
	incq	%rbx
	movq	%r14, 152(%rsp)
	movq	48(%rbp), %rdx
	leaq	(%r14,%rdx), %rbp
	movq	%rbp, 160(%rsp)
	leaq	(%r14,%rdx,2), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	%rax, 168(%rsp)
	movq	$4, 136(%rsp)
	leaq	(%rdx,%rdx,2), %rdi
	addq	%r14, %rdi
	movq	%rdi, 176(%rsp)
	xorl	%esi, %esi
	callq	memset
	movq	%r15, %r12
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	48(%rsp), %r13          # 8-byte Reload
	cmpq	%r12, %r15
	je	.LBB0_115
.LBB0_114:
	movsbl	-1(%r12), %r15d
	jmp	.LBB0_116
.LBB0_115:
	movl	$128, %r15d
.LBB0_116:
	movq	72(%rsp), %rax
	movq	48(%rax), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	memset
	movb	$1, (%r14,%rbx)
	movq	72(%rsp), %rdi
	movl	$132, %r8d
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	%r14, %r9
	callq	lstep
	movq	72(%rsp), %rax
	movq	48(%rax), %rdx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	memmove
	xorl	%ebx, %ebx
	movl	%r15d, %eax
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	jmp	.LBB0_118
	.p2align	4, 0x90
.LBB0_117:                              #   in Loop: Header=BB0_118 Depth=1
	movq	72(%rsp), %rax
	movq	48(%rax), %rdx
	movq	200(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	memmove
	movq	72(%rsp), %rax
	movq	48(%rax), %rdx
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	memmove
	movq	72(%rsp), %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%rbx, %rcx
	movl	32(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %r8d
	movq	%r14, %r9
	callq	lstep
	incq	%r12
	movl	%ebx, %eax
	movq	216(%rsp), %rbx         # 8-byte Reload
.LBB0_118:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_134 Depth 2
	movl	%eax, 12(%rsp)          # 4-byte Spill
	cmpq	112(%rsp), %r12
	je	.LBB0_120
# BB#119:                               #   in Loop: Header=BB0_118 Depth=1
	movsbl	(%r12), %r13d
	jmp	.LBB0_121
	.p2align	4, 0x90
.LBB0_120:                              #   in Loop: Header=BB0_118 Depth=1
	movl	$128, %r13d
.LBB0_121:                              #   in Loop: Header=BB0_118 Depth=1
	movq	72(%rsp), %r15
	movq	48(%r15), %rdx
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	memcmp
	testl	%eax, %eax
	cmoveq	%r12, %rbx
	xorl	%ebp, %ebp
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	$128, %eax
	movq	%rbx, 216(%rsp)         # 8-byte Spill
	je	.LBB0_124
# BB#122:                               #   in Loop: Header=BB0_118 Depth=1
	cmpl	$10, %eax
	jne	.LBB0_126
# BB#123:                               #   in Loop: Header=BB0_118 Depth=1
	testb	$8, 40(%r15)
	movl	$0, %ebx
	jne	.LBB0_125
	jmp	.LBB0_127
	.p2align	4, 0x90
.LBB0_124:                              #   in Loop: Header=BB0_118 Depth=1
	testb	$1, 80(%rsp)
	movl	$0, %ebx
	jne	.LBB0_127
.LBB0_125:                              #   in Loop: Header=BB0_118 Depth=1
	movl	76(%r15), %ebx
	movl	$129, %ebp
	jmp	.LBB0_127
	.p2align	4, 0x90
.LBB0_126:                              #   in Loop: Header=BB0_118 Depth=1
	xorl	%ebx, %ebx
.LBB0_127:                              #   in Loop: Header=BB0_118 Depth=1
	cmpl	$128, %r13d
	movl	%r13d, 32(%rsp)         # 4-byte Spill
	movq	%r12, 208(%rsp)         # 8-byte Spill
	je	.LBB0_130
# BB#128:                               #   in Loop: Header=BB0_118 Depth=1
	cmpl	$10, %r13d
	jne	.LBB0_132
# BB#129:                               #   in Loop: Header=BB0_118 Depth=1
	testb	$8, 40(%r15)
	jne	.LBB0_131
	jmp	.LBB0_132
	.p2align	4, 0x90
.LBB0_130:                              #   in Loop: Header=BB0_118 Depth=1
	testb	$2, 80(%rsp)
	jne	.LBB0_132
.LBB0_131:                              #   in Loop: Header=BB0_118 Depth=1
	xorl	%eax, %eax
	cmpl	$129, %ebp
	sete	%al
	orl	$130, %eax
	addl	80(%r15), %ebx
	movl	%eax, %ebp
.LBB0_132:                              #   in Loop: Header=BB0_118 Depth=1
	testl	%ebx, %ebx
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	jle	.LBB0_135
# BB#133:                               # %.preheader.i.preheader.i
                                        #   in Loop: Header=BB0_118 Depth=1
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	movl	%ebp, %r8d
	movq	%r14, %r9
	callq	lstep
	cmpl	$1, %ebx
	je	.LBB0_135
	.p2align	4, 0x90
.LBB0_134:                              # %.preheader..preheader_crit_edge.i.i
                                        #   Parent Loop BB0_118 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%ebx
	movq	72(%rsp), %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	movl	%ebp, %r8d
	movq	%r14, %r9
	callq	lstep
	cmpl	$1, %ebx
	jg	.LBB0_134
.LBB0_135:                              # %.loopexit.i.i25
                                        #   in Loop: Header=BB0_118 Depth=1
	cmpl	$129, %ebp
	jne	.LBB0_138
# BB#136:                               #   in Loop: Header=BB0_118 Depth=1
	movl	$129, %ebx
	cmpl	$128, 32(%rsp)          # 4-byte Folded Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	208(%rsp), %r12         # 8-byte Reload
	je	.LBB0_144
# BB#137:                               # %._crit_edge.i.i31
                                        #   in Loop: Header=BB0_118 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	jmp	.LBB0_143
	.p2align	4, 0x90
.LBB0_138:                              #   in Loop: Header=BB0_118 Depth=1
	cmpl	$128, 12(%rsp)          # 4-byte Folded Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	208(%rsp), %r12         # 8-byte Reload
	jne	.LBB0_140
# BB#139:                               #   in Loop: Header=BB0_118 Depth=1
	movl	%ebp, %ebx
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_152
	.p2align	4, 0x90
.LBB0_140:                              #   in Loop: Header=BB0_118 Depth=1
	callq	__ctype_b_loc
	movl	12(%rsp), %edx          # 4-byte Reload
	movq	(%rax), %rax
	movslq	%edx, %rcx
	movzwl	(%rax,%rcx,2), %ecx
	cmpl	$128, 32(%rsp)          # 4-byte Folded Reload
	je	.LBB0_147
# BB#141:                               #   in Loop: Header=BB0_118 Depth=1
	cmpl	$95, %edx
	je	.LBB0_147
# BB#142:                               #   in Loop: Header=BB0_118 Depth=1
	movl	%ecx, %edx
	andl	$8, %edx
	testw	%dx, %dx
	jne	.LBB0_158
.LBB0_143:                              #   in Loop: Header=BB0_118 Depth=1
	movl	32(%rsp), %edx          # 4-byte Reload
	movslq	%edx, %rcx
	testb	$8, (%rax,%rcx,2)
	movl	$133, %eax
	cmovnel	%eax, %ebp
	cmpl	$95, %edx
	cmovel	%eax, %ebp
	movl	%ebp, %ebx
.LBB0_144:                              #   in Loop: Header=BB0_118 Depth=1
	cmpl	$128, 12(%rsp)          # 4-byte Folded Reload
	jne	.LBB0_146
# BB#145:                               #   in Loop: Header=BB0_118 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_152
	.p2align	4, 0x90
.LBB0_146:                              # %..thread141_crit_edge.i.i
                                        #   in Loop: Header=BB0_118 Depth=1
	callq	__ctype_b_loc
	movl	12(%rsp), %edx          # 4-byte Reload
	movslq	%edx, %rcx
	movq	(%rax), %rax
	movw	(%rax,%rcx,2), %cx
	jmp	.LBB0_148
.LBB0_147:                              #   in Loop: Header=BB0_118 Depth=1
	movl	%ebp, %ebx
.LBB0_148:                              # %.thread141.i.i
                                        #   in Loop: Header=BB0_118 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
	cmpl	$95, %edx
	je	.LBB0_150
.LBB0_149:                              # %.thread141.i.i
                                        #   in Loop: Header=BB0_118 Depth=1
	andl	$8, %ecx
	testw	%cx, %cx
	je	.LBB0_152
.LBB0_150:                              #   in Loop: Header=BB0_118 Depth=1
	movl	$134, %r8d
	cmpl	$130, %ebx
	je	.LBB0_153
# BB#151:                               #   in Loop: Header=BB0_118 Depth=1
	movl	32(%rsp), %ecx          # 4-byte Reload
	cmpl	$128, %ecx
	jne	.LBB0_156
	.p2align	4, 0x90
.LBB0_152:                              # %.thread.i.i38
                                        #   in Loop: Header=BB0_118 Depth=1
	leal	-133(%rbx), %eax
	cmpl	$1, %eax
	movl	%ebx, %r8d
	ja	.LBB0_154
.LBB0_153:                              # %.thread143.i.i
                                        #   in Loop: Header=BB0_118 Depth=1
	movq	72(%rsp), %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	%r14, %r9
	callq	lstep
.LBB0_154:                              #   in Loop: Header=BB0_118 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	cmpq	%rdx, %r12
	je	.LBB0_159
# BB#155:                               #   in Loop: Header=BB0_118 Depth=1
	movb	(%r14,%r13), %al
	testb	%al, %al
	je	.LBB0_117
	jmp	.LBB0_159
.LBB0_156:                              #   in Loop: Header=BB0_118 Depth=1
	cmpl	$95, %ecx
	je	.LBB0_152
# BB#157:                               #   in Loop: Header=BB0_118 Depth=1
	movslq	%ecx, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$8, %eax
	testw	%ax, %ax
	je	.LBB0_153
	jmp	.LBB0_152
.LBB0_158:                              #   in Loop: Header=BB0_118 Depth=1
	movl	%ebp, %ebx
	movq	24(%rsp), %rbp          # 8-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	cmpl	$95, %edx
	jne	.LBB0_149
	jmp	.LBB0_150
.LBB0_159:                              # %lfast.exit.i
	movq	216(%rsp), %rsi         # 8-byte Reload
	movq	%rsi, 120(%rsp)
	cmpb	$0, (%r14,%r13)
	je	.LBB0_206
# BB#160:
	movq	192(%rsp), %r12         # 8-byte Reload
	testq	%r12, %r12
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	jne	.LBB0_162
# BB#161:
	cmpl	$0, 120(%rbx)
	je	.LBB0_196
.LBB0_162:                              # %.preheader189.i
	leaq	72(%rsp), %r14
	movq	%r14, %rdi
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r13, %r8
	callq	lslow
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB0_166
# BB#163:                               # %.lr.ph209.i.preheader
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%r14, %r15
	movq	56(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_164:                              # %.lr.ph209.i
                                        # =>This Inner Loop Header: Depth=1
	movq	120(%rsp), %rsi
	incq	%rsi
	movq	%rsi, 120(%rsp)
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%rbx, %rcx
	movq	%r13, %r8
	callq	lslow
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_164
# BB#165:
	movq	%r15, %r14
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
.LBB0_166:                              # %._crit_edge.i40
	cmpq	$1, %r12
	jne	.LBB0_168
# BB#167:
	cmpl	$0, 120(%rbx)
	je	.LBB0_189
.LBB0_168:
	movq	88(%rsp), %rax
	testq	%rax, %rax
	jne	.LBB0_170
# BB#169:
	movq	72(%rsp), %rax
	movq	112(%rax), %rdi
	shlq	$4, %rdi
	addq	$16, %rdi
	callq	cli_malloc
	movq	%rax, 88(%rsp)
	testq	%rax, %rax
	je	.LBB0_210
.LBB0_170:                              # %.thread.preheader.i41
	movq	72(%rsp), %rcx
	movq	112(%rcx), %rcx
	testq	%rcx, %rcx
	pcmpeqd	%xmm0, %xmm0
	je	.LBB0_173
# BB#171:                               # %.thread.i42.preheader
	addq	$16, %rax
	movl	$1, %edx
	.p2align	4, 0x90
.LBB0_172:                              # %.thread.i42
                                        # =>This Inner Loop Header: Depth=1
	incq	%rdx
	movdqu	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rcx, %rdx
	jbe	.LBB0_172
.LBB0_173:                              # %.thread._crit_edge.i43
	cmpl	$0, 120(%rbx)
	jne	.LBB0_176
# BB#174:
	testb	$4, 81(%rsp)
	jne	.LBB0_176
# BB#175:
	movq	120(%rsp), %rsi
	movq	%r14, %rdi
	movq	%rbp, %rdx
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	movq	%r13, %r8
	callq	ldissect
	jmp	.LBB0_183
.LBB0_176:
	movq	128(%rbx), %rcx
	movq	128(%rsp), %rax
	testq	%rcx, %rcx
	jle	.LBB0_179
# BB#177:
	testq	%rax, %rax
	jne	.LBB0_179
# BB#178:
	leaq	8(,%rcx,8), %rdi
	callq	cli_malloc
	movq	%rax, 128(%rsp)
	movq	128(%rbx), %rcx
.LBB0_179:
	testq	%rax, %rax
	movq	16(%rsp), %rbx          # 8-byte Reload
	jne	.LBB0_181
# BB#180:
	testq	%rcx, %rcx
	jg	.LBB0_208
.LBB0_181:
	movq	120(%rsp), %rsi
	movl	$0, (%rsp)
	xorl	%r9d, %r9d
	movq	%r14, %rdi
	jmp	.LBB0_182
.LBB0_113:                              # %._crit_edge224.i
	incq	%r12
	movq	104(%rsp), %r15
	movq	152(%rsp), %r14
	movq	160(%rsp), %rbp
	movq	168(%rsp), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	cmpq	%r12, %r15
	jne	.LBB0_114
	jmp	.LBB0_115
	.p2align	4, 0x90
.LBB0_182:
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movq	%r13, %r8
	callq	lbackref
.LBB0_183:
	testq	%rax, %rax
	movq	56(%rsp), %rax          # 8-byte Reload
	jne	.LBB0_189
# BB#184:                               # %.preheader188.i
	movq	120(%rsp), %r12
	cmpq	%r12, %rbp
	jbe	.LBB0_188
# BB#185:
	decq	%rbp
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movq	%r13, %r8
	callq	lslow
	movq	%rax, %rbp
	testq	%rbp, %rbp
	movq	120(%rsp), %r12
	je	.LBB0_187
# BB#186:
	movl	$0, (%rsp)
	xorl	%r9d, %r9d
	movq	%r14, %rdi
	movq	%r12, %rsi
	jmp	.LBB0_182
.LBB0_187:
	xorl	%ebp, %ebp
	movq	56(%rsp), %rax          # 8-byte Reload
.LBB0_188:                              # %._crit_edge232.i
	cmpq	%rax, %r12
	jne	.LBB0_113
.LBB0_189:                              # %.loopexit.i46
	movq	192(%rsp), %rdi         # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB0_196
# BB#190:
	movq	96(%rsp), %rax
	movq	120(%rsp), %rcx
	subq	%rax, %rcx
	subq	%rax, %rbp
	cmpq	$1, %rdi
	movq	%rcx, (%r15)
	movq	%rbp, 8(%r15)
	je	.LBB0_196
# BB#191:                               # %.lr.ph.preheader.i47
	movq	72(%rsp), %rax
	movl	$1, %ecx
	movl	$16, %edx
	pcmpeqd	%xmm0, %xmm0
.LBB0_192:                              # %.lr.ph.i48
                                        # =>This Inner Loop Header: Depth=1
	cmpq	112(%rax), %rcx
	jbe	.LBB0_194
# BB#193:                               #   in Loop: Header=BB0_192 Depth=1
	movdqu	%xmm0, (%r15,%rdx)
	jmp	.LBB0_195
.LBB0_194:                              #   in Loop: Header=BB0_192 Depth=1
	movq	88(%rsp), %rsi
	movups	(%rsi,%rdx), %xmm1
	movups	%xmm1, (%r15,%rdx)
.LBB0_195:                              #   in Loop: Header=BB0_192 Depth=1
	incq	%rcx
	addq	$16, %rdx
	cmpq	%rdi, %rcx
	jb	.LBB0_192
.LBB0_196:                              # %.thread187.i
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_198
# BB#197:
	callq	free
.LBB0_198:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_200
# BB#199:
	callq	free
.LBB0_200:
	movq	144(%rsp), %rdi
.LBB0_201:
	callq	free
	xorl	%eax, %eax
	jmp	.LBB0_213
.LBB0_203:
	movl	$1, %eax
.LBB0_213:
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_204:
	movq	88(%rsp), %rdi
	callq	free
	movq	128(%rsp), %rdi
	jmp	.LBB0_207
.LBB0_205:
	movq	88(%rsp), %rdi
	jmp	.LBB0_211
.LBB0_206:
	movq	88(%rsp), %rdi
	callq	free
	movq	128(%rsp), %rdi
	callq	free
	movq	144(%rsp), %rdi
.LBB0_207:
	callq	free
	movl	$1, %eax
	jmp	.LBB0_213
.LBB0_208:
	movq	88(%rsp), %rdi
	callq	free
.LBB0_210:
	movq	144(%rsp), %rdi
.LBB0_211:
	callq	free
.LBB0_212:
	movl	$12, %eax
	jmp	.LBB0_213
.Lfunc_end0:
	.size	cli_regexec, .Lfunc_end0-cli_regexec
	.cfi_endproc

	.p2align	4, 0x90
	.type	sslow,@function
sslow:                                  # @sslow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 144
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, %r14
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%rsi, %r15
	movq	96(%rdi), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	cmpq	%r15, 32(%rdi)
	je	.LBB1_1
# BB#2:
	movsbl	-1(%r15), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	jmp	.LBB1_3
.LBB1_1:
	movl	$128, (%rsp)            # 4-byte Folded Spill
.LBB1_3:
	movl	$1, %r12d
	movl	$1, %eax
	movl	%r14d, %ecx
	shlq	%cl, %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %r13
	movl	$132, %r8d
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%rax, %rcx
	movq	%rax, %r9
	callq	sstep
	movq	%rax, %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	40(%rax), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%ebx, %ecx
	shlq	%cl, %r12
	movq	%r12, 64(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movl	(%rsp), %edx            # 4-byte Reload
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movq	%r13, 32(%rsp)          # 8-byte Spill
	jmp	.LBB1_4
	.p2align	4, 0x90
.LBB1_45:                               #   in Loop: Header=BB1_4 Depth=1
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	%rbp, %rcx
	movl	4(%rsp), %r12d          # 4-byte Reload
	movl	%r12d, %r8d
	movq	%rax, %r9
	callq	sstep
	movq	%rax, %rbp
	incq	%r15
	movl	%r12d, %edx
.LBB1_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_20 Depth 2
	cmpq	56(%rsp), %r15          # 8-byte Folded Reload
	movq	%r15, 8(%rsp)           # 8-byte Spill
	je	.LBB1_5
# BB#6:                                 #   in Loop: Header=BB1_4 Depth=1
	movsbl	(%r15), %ecx
	jmp	.LBB1_7
	.p2align	4, 0x90
.LBB1_5:                                #   in Loop: Header=BB1_4 Depth=1
	movl	$128, %ecx
.LBB1_7:                                #   in Loop: Header=BB1_4 Depth=1
	xorl	%r15d, %r15d
	cmpl	$128, %edx
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	je	.LBB1_11
# BB#8:                                 #   in Loop: Header=BB1_4 Depth=1
	cmpl	$10, %edx
	jne	.LBB1_9
# BB#10:                                #   in Loop: Header=BB1_4 Depth=1
	testb	$8, 40(%r13)
	movl	$0, %r12d
	jne	.LBB1_12
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_11:                               #   in Loop: Header=BB1_4 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	testb	$1, 8(%rax)
	movl	$0, %r12d
	jne	.LBB1_13
.LBB1_12:                               #   in Loop: Header=BB1_4 Depth=1
	movl	76(%r13), %r12d
	movl	$129, %r15d
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_9:                                #   in Loop: Header=BB1_4 Depth=1
	xorl	%r12d, %r12d
.LBB1_13:                               #   in Loop: Header=BB1_4 Depth=1
	cmpl	$128, %ecx
	movl	%edx, (%rsp)            # 4-byte Spill
	je	.LBB1_16
# BB#14:                                #   in Loop: Header=BB1_4 Depth=1
	cmpl	$10, %ecx
	jne	.LBB1_18
# BB#15:                                #   in Loop: Header=BB1_4 Depth=1
	testb	$8, 40(%r13)
	jne	.LBB1_17
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_16:                               #   in Loop: Header=BB1_4 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	testb	$2, 8(%rax)
	jne	.LBB1_18
.LBB1_17:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%eax, %eax
	cmpl	$129, %r15d
	sete	%al
	orl	$130, %eax
	addl	80(%r13), %r12d
	movl	%eax, %r15d
.LBB1_18:                               #   in Loop: Header=BB1_4 Depth=1
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	testl	%r12d, %r12d
	jle	.LBB1_21
# BB#19:                                # %.preheader.preheader
                                        #   in Loop: Header=BB1_4 Depth=1
	incl	%r12d
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_20:                               # %.preheader
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%rbp, %rcx
	movl	%r15d, %r8d
	movq	%rbp, %r9
	callq	sstep
	movq	%rax, %rbp
	decl	%r12d
	cmpl	$1, %r12d
	jg	.LBB1_20
.LBB1_21:                               # %.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	cmpl	$129, %r15d
	jne	.LBB1_22
# BB#28:                                #   in Loop: Header=BB1_4 Depth=1
	movl	$129, %r12d
	cmpl	$128, 4(%rsp)           # 4-byte Folded Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	movl	(%rsp), %edi            # 4-byte Reload
	je	.LBB1_31
# BB#29:                                # %._crit_edge
                                        #   in Loop: Header=BB1_4 Depth=1
	callq	__ctype_b_loc
	movl	(%rsp), %edi            # 4-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
	jmp	.LBB1_30
	.p2align	4, 0x90
.LBB1_22:                               #   in Loop: Header=BB1_4 Depth=1
	cmpl	$128, (%rsp)            # 4-byte Folded Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	jne	.LBB1_24
# BB#23:                                #   in Loop: Header=BB1_4 Depth=1
	movl	%r15d, %r12d
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB1_40
	.p2align	4, 0x90
.LBB1_24:                               #   in Loop: Header=BB1_4 Depth=1
	callq	__ctype_b_loc
	movl	(%rsp), %edi            # 4-byte Reload
	movslq	%edi, %rcx
	movl	4(%rsp), %esi           # 4-byte Reload
	cmpl	$128, %esi
	je	.LBB1_25
# BB#26:                                #   in Loop: Header=BB1_4 Depth=1
	cmpl	$95, %edi
	je	.LBB1_25
# BB#27:                                #   in Loop: Header=BB1_4 Depth=1
	movq	(%rax), %rdx
	movzwl	(%rdx,%rcx,2), %edx
	andl	$8, %edx
	testw	%dx, %dx
	jne	.LBB1_25
.LBB1_30:                               #   in Loop: Header=BB1_4 Depth=1
	movq	(%rax), %rax
	movslq	%esi, %rcx
	testb	$8, (%rax,%rcx,2)
	movl	$133, %eax
	cmovnel	%eax, %r15d
	cmpl	$95, %esi
	cmovel	%eax, %r15d
	movl	%r15d, %r12d
.LBB1_31:                               #   in Loop: Header=BB1_4 Depth=1
	cmpl	$128, %edi
	jne	.LBB1_33
# BB#32:                                #   in Loop: Header=BB1_4 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB1_40
	.p2align	4, 0x90
.LBB1_33:                               # %..thread125_crit_edge
                                        #   in Loop: Header=BB1_4 Depth=1
	callq	__ctype_b_loc
	movl	(%rsp), %edi            # 4-byte Reload
	movslq	%edi, %rcx
	movq	8(%rsp), %r15           # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
	jmp	.LBB1_34
.LBB1_25:                               #   in Loop: Header=BB1_4 Depth=1
	movl	%r15d, %r12d
	movq	8(%rsp), %r15           # 8-byte Reload
.LBB1_34:                               # %.thread125
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	(%rax), %rax
	cmpl	$95, %edi
	je	.LBB1_36
# BB#35:                                # %.thread125
                                        #   in Loop: Header=BB1_4 Depth=1
	movzwl	(%rax,%rcx,2), %ecx
	andl	$8, %ecx
	testw	%cx, %cx
	je	.LBB1_40
.LBB1_36:                               #   in Loop: Header=BB1_4 Depth=1
	movl	$134, %r8d
	cmpl	$130, %r12d
	je	.LBB1_42
# BB#37:                                #   in Loop: Header=BB1_4 Depth=1
	cmpl	$128, %esi
	je	.LBB1_40
# BB#38:                                #   in Loop: Header=BB1_4 Depth=1
	cmpl	$95, %esi
	je	.LBB1_40
# BB#39:                                #   in Loop: Header=BB1_4 Depth=1
	movslq	%esi, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$8, %eax
	testw	%ax, %ax
	je	.LBB1_42
	.p2align	4, 0x90
.LBB1_40:                               # %.thread
                                        #   in Loop: Header=BB1_4 Depth=1
	leal	-133(%r12), %eax
	cmpl	$1, %eax
	movl	%r12d, %r8d
	ja	.LBB1_41
.LBB1_42:                               # %.thread127
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdx
	movq	%rbp, %rcx
	movq	%rbp, %r9
	callq	sstep
	movq	%rax, %rbp
	movq	48(%rsp), %rax          # 8-byte Reload
	jmp	.LBB1_43
	.p2align	4, 0x90
.LBB1_41:                               #   in Loop: Header=BB1_4 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
.LBB1_43:                               #   in Loop: Header=BB1_4 Depth=1
	testq	64(%rsp), %rbp          # 8-byte Folded Reload
	movq	80(%rsp), %rbx          # 8-byte Reload
	cmovneq	%r15, %rbx
	cmpq	72(%rsp), %r15          # 8-byte Folded Reload
	je	.LBB1_46
# BB#44:                                #   in Loop: Header=BB1_4 Depth=1
	cmpq	%rax, %rbp
	jne	.LBB1_45
.LBB1_46:
	movq	%rbx, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	sslow, .Lfunc_end1-sslow
	.cfi_endproc

	.p2align	4, 0x90
	.type	sdissect,@function
sdissect:                               # @sdissect
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 128
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rsi, %r9
	movq	%rdi, %r12
	cmpq	%r8, %r15
	jge	.LBB2_17
# BB#1:                                 # %.lr.ph188
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	$2415919104, %edx       # imm = 0x90000000
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%r12, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_6 Depth 2
                                        #     Child Loop BB2_25 Depth 2
                                        #     Child Loop BB2_29 Depth 2
                                        #     Child Loop BB2_11 Depth 2
                                        #     Child Loop BB2_19 Depth 2
                                        #     Child Loop BB2_21 Depth 2
	movq	%r15, %rbx
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	movq	(%rsi,%rbx,8), %rdi
	movl	%edi, %eax
	andl	$-134217728, %eax       # imm = 0xF8000000
	cmpl	$1207959552, %eax       # imm = 0x48000000
	je	.LBB2_33
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpl	$1476395008, %eax       # imm = 0x58000000
	je	.LBB2_33
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpl	$2013265920, %eax       # imm = 0x78000000
	movq	%rbx, %r14
	jne	.LBB2_7
# BB#5:                                 # %.preheader174
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	%edi, %eax
	andl	$-134217728, %eax       # imm = 0xF8000000
	cmpq	%rdx, %rax
	movq	%rdi, %rax
	movq	%rbx, %r14
	je	.LBB2_7
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	andl	$134217727, %eax        # imm = 0x7FFFFFF
	addq	%rax, %r14
	movq	(%rsi,%r14,8), %rax
	movl	%eax, %ecx
	andl	$-134217728, %ecx       # imm = 0xF8000000
	cmpq	%rdx, %rcx
	jne	.LBB2_6
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_33:                               #   in Loop: Header=BB2_2 Depth=1
	movl	%edi, %r14d
	andl	$134217727, %r14d       # imm = 0x7FFFFFF
	addq	%rbx, %r14
.LBB2_7:                                # %.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	leaq	1(%r14), %r15
	movl	%edi, %eax
	andl	$-134217728, %eax       # imm = 0xF8000000
	addq	$-134217728, %rax       # imm = 0xF8000000
	shrq	$27, %rax
	decq	%rax
	cmpq	$13, %rax
	ja	.LBB2_16
# BB#8:                                 # %.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r9, 8(%rsp)            # 8-byte Spill
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_9:                                #   in Loop: Header=BB2_2 Depth=1
	incq	%r9
	jmp	.LBB2_16
.LBB2_18:                               # %.preheader172.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB2_19:                               # %.preheader172
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%rbx, %rcx
	movq	%r15, %r8
	callq	sslow
	movq	%rax, %rbp
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	sslow
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	leaq	-1(%rbp), %rdx
	cmpq	%r13, %rax
	jne	.LBB2_19
# BB#20:                                #   in Loop: Header=BB2_2 Depth=1
	incq	%rbx
	movq	%r9, %r12
	.p2align	4, 0x90
.LBB2_21:                               #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r12, %r13
	movq	%r9, %r12
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movq	%r14, %r8
	callq	sslow
	movq	%rax, %r9
	testq	%r9, %r9
	je	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_21 Depth=2
	cmpq	%r12, %r9
	jne	.LBB2_21
.LBB2_23:                               #   in Loop: Header=BB2_2 Depth=1
	testq	%r9, %r9
	cmovneq	%r12, %r13
	cmoveq	%r12, %r9
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%r9, %rdx
	movq	%rbx, %rcx
	jmp	.LBB2_14
.LBB2_10:                               # %.preheader173.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB2_11:                               # %.preheader173
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%rbx, %rcx
	movq	%r15, %r8
	callq	sslow
	movq	%rax, %rbp
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	sslow
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	leaq	-1(%rbp), %rdx
	cmpq	%r13, %rax
	jne	.LBB2_11
# BB#12:                                #   in Loop: Header=BB2_2 Depth=1
	incq	%rbx
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movq	%rbx, %r13
	movq	%r14, %r8
	movq	%r9, %rbx
	callq	sslow
	testq	%rax, %rax
	je	.LBB2_15
# BB#13:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
.LBB2_14:                               # %.backedge
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%r14, %r8
	callq	sdissect
.LBB2_15:                               # %.backedge
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%rbp, %r9
	movl	$2415919104, %edx       # imm = 0x90000000
	jmp	.LBB2_16
.LBB2_31:                               #   in Loop: Header=BB2_2 Depth=1
	andl	$134217727, %edi        # imm = 0x7FFFFFF
	movq	%r9, %rax
	subq	24(%r12), %rax
	movq	16(%r12), %rsi
	shlq	$4, %rdi
	movq	%rax, (%rsi,%rdi)
	jmp	.LBB2_16
.LBB2_32:                               #   in Loop: Header=BB2_2 Depth=1
	andl	$134217727, %edi        # imm = 0x7FFFFFF
	movq	%r9, %rax
	subq	24(%r12), %rax
	movq	16(%r12), %rsi
	shlq	$4, %rdi
	movq	%rax, 8(%rsi,%rdi)
	jmp	.LBB2_16
.LBB2_24:                               # %.preheader.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%r15, %r13
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	movq	16(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_25:                               # %.preheader
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%rbx, %rcx
	movq	%r13, %r8
	callq	sslow
	movq	%rax, %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	movq	%r15, %r8
	callq	sslow
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	leaq	-1(%r14), %rdx
	cmpq	%rbp, %rax
	jne	.LBB2_25
# BB#26:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%r13, 64(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	andl	$134217727, %eax        # imm = 0x7FFFFFF
	leaq	-1(%rbx,%rax), %rbp
	incq	%rbx
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r14, %rdx
	movq	%rbx, %rcx
	movq	%rbx, %r15
	movq	%rbp, %r8
	callq	sslow
	cmpq	%r14, %rax
	je	.LBB2_27
# BB#28:                                # %.lr.ph181.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	48(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_29:                               # %.lr.ph181
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13,%rbp,8), %rbx
	movl	$134217727, %eax        # imm = 0x7FFFFFF
	andq	%rax, %rbx
	leaq	1(%rbx,%rbp), %rax
	movq	(%r13,%rax,8), %rcx
	movl	$4160749568, %edx       # imm = 0xF8000000
	andq	%rdx, %rcx
	movl	$2415919104, %edx       # imm = 0x90000000
	leaq	-134217728(%rdx), %rdx
	addq	%rbp, %rbx
	leaq	2(%rbp), %r15
	cmpq	%rdx, %rcx
	cmovneq	%rax, %rbx
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%rbx, %r8
	callq	sslow
	movq	8(%rsp), %rsi           # 8-byte Reload
	cmpq	%r14, %rax
	movq	%rbx, %rbp
	jne	.LBB2_29
	jmp	.LBB2_30
.LBB2_27:                               #   in Loop: Header=BB2_2 Depth=1
	movq	%rbp, %rbx
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB2_30:                               # %._crit_edge
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%rbx, %r8
	callq	sdissect
	movq	%r14, %r9
	movl	$2415919104, %edx       # imm = 0x90000000
	movq	64(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_16:                               # %.backedge
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	16(%rsp), %r8           # 8-byte Reload
	cmpq	%r8, %r15
	jl	.LBB2_2
.LBB2_17:                               # %._crit_edge189
	movq	%r9, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	sdissect, .Lfunc_end2-sdissect
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_9
	.quad	.LBB2_16
	.quad	.LBB2_16
	.quad	.LBB2_9
	.quad	.LBB2_9
	.quad	.LBB2_16
	.quad	.LBB2_16
	.quad	.LBB2_18
	.quad	.LBB2_16
	.quad	.LBB2_10
	.quad	.LBB2_16
	.quad	.LBB2_31
	.quad	.LBB2_32
	.quad	.LBB2_24

	.text
	.p2align	4, 0x90
	.type	sbackref,@function
sbackref:                               # @sbackref
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 144
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %rbp
	cmpq	%r8, %r15
	jge	.LBB3_49
# BB#1:                                 # %.lr.ph346.lr.ph.lr.ph.preheader
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movl	144(%rsp), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	$2415919104, %ebx       # imm = 0x90000000
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%r8, 64(%rsp)           # 8-byte Spill
.LBB3_2:                                # %.lr.ph346.lr.ph.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_3 Depth 2
                                        #       Child Loop BB3_4 Depth 3
                                        #         Child Loop BB3_51 Depth 4
                                        #       Child Loop BB3_64 Depth 3
	movq	%r15, %r13
.LBB3_3:                                # %.lr.ph346
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_4 Depth 3
                                        #         Child Loop BB3_51 Depth 4
                                        #       Child Loop BB3_64 Depth 3
	movq	(%rbp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	8(%rax), %r15
	.p2align	4, 0x90
.LBB3_4:                                #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_3 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_51 Depth 4
	movq	(%r15,%r13,8), %rax
	movl	%eax, %ecx
	andl	$-134217728, %ecx       # imm = 0xF8000000
	addq	$-268435456, %rcx       # imm = 0xF0000000
	shrq	$27, %rcx
	cmpq	$18, %rcx
	ja	.LBB3_52
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=3
	jmpq	*.LJTI3_0(,%rcx,8)
.LBB3_6:                                #   in Loop: Header=BB3_4 Depth=3
	cmpq	%r12, %r14
	je	.LBB3_84
# BB#7:                                 #   in Loop: Header=BB3_4 Depth=3
	movsbl	(%r14), %ecx
	movsbl	%al, %eax
	cmpl	%eax, %ecx
	jne	.LBB3_84
# BB#8:                                 #   in Loop: Header=BB3_4 Depth=3
	incq	%r14
	jmp	.LBB3_48
.LBB3_14:                               #   in Loop: Header=BB3_4 Depth=3
	cmpq	32(%rbp), %r14
	jne	.LBB3_16
# BB#15:                                #   in Loop: Header=BB3_4 Depth=3
	testb	$1, 8(%rbp)
	je	.LBB3_48
.LBB3_16:                               #   in Loop: Header=BB3_4 Depth=3
	cmpq	40(%rbp), %r14
	jae	.LBB3_84
# BB#17:                                #   in Loop: Header=BB3_4 Depth=3
	cmpb	$10, -1(%r14)
	je	.LBB3_18
	jmp	.LBB3_84
.LBB3_19:                               #   in Loop: Header=BB3_4 Depth=3
	movq	40(%rbp), %rax
	cmpq	%rax, %r14
	jne	.LBB3_21
# BB#20:                                #   in Loop: Header=BB3_4 Depth=3
	testb	$2, 8(%rbp)
	je	.LBB3_48
.LBB3_21:                               #   in Loop: Header=BB3_4 Depth=3
	cmpq	%rax, %r14
	jae	.LBB3_84
# BB#22:                                #   in Loop: Header=BB3_4 Depth=3
	cmpb	$10, (%r14)
	jne	.LBB3_84
.LBB3_18:                               #   in Loop: Header=BB3_4 Depth=3
	movq	56(%rsp), %rax          # 8-byte Reload
	testb	$8, 40(%rax)
	jne	.LBB3_48
	jmp	.LBB3_84
.LBB3_9:                                #   in Loop: Header=BB3_4 Depth=3
	cmpq	%r12, %r14
	je	.LBB3_84
# BB#10:                                #   in Loop: Header=BB3_4 Depth=3
	incq	%r14
	jmp	.LBB3_48
.LBB3_11:                               #   in Loop: Header=BB3_4 Depth=3
	cmpq	%r12, %r14
	je	.LBB3_84
# BB#12:                                #   in Loop: Header=BB3_4 Depth=3
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	24(%rcx), %rcx
	andl	$134217727, %eax        # imm = 0x7FFFFFF
	shlq	$5, %rax
	movq	(%rcx,%rax), %rdx
	movzbl	(%r14), %esi
	movb	8(%rcx,%rax), %al
	testb	(%rdx,%rsi), %al
	je	.LBB3_84
# BB#13:                                #   in Loop: Header=BB3_4 Depth=3
	incq	%r14
	jmp	.LBB3_48
.LBB3_50:                               #   in Loop: Header=BB3_4 Depth=3
	movq	8(%r15,%r13,8), %rax
	incq	%r13
	.p2align	4, 0x90
.LBB3_51:                               #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_3 Depth=2
                                        #       Parent Loop BB3_4 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	andl	$134217727, %eax        # imm = 0x7FFFFFF
	addq	%rax, %r13
	movq	(%r15,%r13,8), %rax
	movl	%eax, %ecx
	andl	$-134217728, %ecx       # imm = 0xF8000000
	cmpq	%rbx, %rcx
	jne	.LBB3_51
	jmp	.LBB3_48
.LBB3_23:                               #   in Loop: Header=BB3_4 Depth=3
	movq	32(%rbp), %rax
	cmpq	%rax, %r14
	jne	.LBB3_26
# BB#24:                                #   in Loop: Header=BB3_4 Depth=3
	testb	$1, 8(%rbp)
	jne	.LBB3_26
# BB#25:                                # %._crit_edge
                                        #   in Loop: Header=BB3_4 Depth=3
	movq	40(%rbp), %r12
	cmpq	%r12, %r14
	jb	.LBB3_33
	jmp	.LBB3_84
.LBB3_36:                               #   in Loop: Header=BB3_4 Depth=3
	movq	40(%rbp), %rax
	cmpq	%rax, %r14
	jne	.LBB3_38
# BB#37:                                #   in Loop: Header=BB3_4 Depth=3
	movq	24(%rsp), %rcx          # 8-byte Reload
	testb	$2, 8(%rcx)
	je	.LBB3_43
.LBB3_38:                               #   in Loop: Header=BB3_4 Depth=3
	cmpq	%rax, %r14
	jae	.LBB3_84
# BB#39:                                #   in Loop: Header=BB3_4 Depth=3
	cmpb	$10, (%r14)
	jne	.LBB3_41
# BB#40:                                #   in Loop: Header=BB3_4 Depth=3
	movq	56(%rsp), %rax          # 8-byte Reload
	testb	$8, 40(%rax)
	jne	.LBB3_43
.LBB3_41:                               #   in Loop: Header=BB3_4 Depth=3
	callq	__ctype_b_loc
	movsbq	(%r14), %rcx
	cmpq	$95, %rcx
	je	.LBB3_84
# BB#42:                                #   in Loop: Header=BB3_4 Depth=3
	movq	(%rax), %rax
	movzwl	(%rax,%rcx,2), %eax
	andl	$8, %eax
	testw	%ax, %ax
	jne	.LBB3_84
.LBB3_43:                               #   in Loop: Header=BB3_4 Depth=3
	movq	24(%rsp), %rbp          # 8-byte Reload
	cmpq	32(%rbp), %r14
	jbe	.LBB3_84
# BB#44:                                #   in Loop: Header=BB3_4 Depth=3
	callq	__ctype_b_loc
	movsbq	-1(%r14), %rcx
	cmpq	$95, %rcx
	je	.LBB3_45
# BB#46:                                #   in Loop: Header=BB3_4 Depth=3
	movq	(%rax), %rax
	movzwl	(%rax,%rcx,2), %eax
	andl	$8, %eax
	testw	%ax, %ax
	jmp	.LBB3_47
.LBB3_26:                               #   in Loop: Header=BB3_4 Depth=3
	movq	40(%rbp), %r12
	cmpq	%r12, %r14
	jae	.LBB3_29
# BB#27:                                #   in Loop: Header=BB3_4 Depth=3
	cmpb	$10, -1(%r14)
	jne	.LBB3_29
# BB#28:                                #   in Loop: Header=BB3_4 Depth=3
	movq	56(%rsp), %rcx          # 8-byte Reload
	testb	$8, 40(%rcx)
	jne	.LBB3_32
.LBB3_29:                               #   in Loop: Header=BB3_4 Depth=3
	cmpq	%rax, %r14
	jbe	.LBB3_84
# BB#30:                                #   in Loop: Header=BB3_4 Depth=3
	callq	__ctype_b_loc
	movsbq	-1(%r14), %rcx
	cmpq	$95, %rcx
	je	.LBB3_84
# BB#31:                                #   in Loop: Header=BB3_4 Depth=3
	movq	(%rax), %rax
	movzwl	(%rax,%rcx,2), %eax
	andl	$8, %eax
	testw	%ax, %ax
	jne	.LBB3_84
.LBB3_32:                               #   in Loop: Header=BB3_4 Depth=3
	cmpq	%r12, %r14
	jae	.LBB3_84
.LBB3_33:                               #   in Loop: Header=BB3_4 Depth=3
	callq	__ctype_b_loc
	movsbq	(%r14), %rcx
	cmpq	$95, %rcx
	je	.LBB3_34
# BB#35:                                #   in Loop: Header=BB3_4 Depth=3
	movq	(%rax), %rax
	movzwl	(%rax,%rcx,2), %eax
	andl	$8, %eax
	testw	%ax, %ax
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB3_47:                               #   in Loop: Header=BB3_4 Depth=3
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	jne	.LBB3_48
	jmp	.LBB3_84
.LBB3_34:                               #   in Loop: Header=BB3_4 Depth=3
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB3_45:                               #   in Loop: Header=BB3_4 Depth=3
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB3_48:                               # %.loopexit271
                                        #   in Loop: Header=BB3_4 Depth=3
	incq	%r13
	cmpq	%r8, %r13
	jl	.LBB3_4
	jmp	.LBB3_49
.LBB3_52:                               # %.critedge.thread
                                        #   in Loop: Header=BB3_3 Depth=2
	movq	(%rbp), %rax
	movq	8(%rax), %rdi
	movq	(%rdi,%r13,8), %r12
	movl	%r12d, %eax
	andl	$-134217728, %eax       # imm = 0xF8000000
	addq	$-939524096, %rax       # imm = 0xC8000000
	shrq	$27, %rax
	cmpq	$8, %rax
	ja	.LBB3_84
# BB#53:                                # %.critedge.thread
                                        #   in Loop: Header=BB3_3 Depth=2
	leaq	1(%r13), %r15
	jmpq	*.LJTI3_1(,%rax,8)
.LBB3_66:                               #   in Loop: Header=BB3_3 Depth=2
	movq	%r12, %r13
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r8, %rbx
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	sbackref
	testq	%rax, %rax
	jne	.LBB3_83
# BB#67:                                # %tailrecurse
                                        #   in Loop: Header=BB3_3 Depth=2
	andl	$134217727, %r13d       # imm = 0x7FFFFFF
	addq	%r15, %r13
	cmpq	%rbx, %r13
	movq	%rbx, %r8
	movl	$2415919104, %ebx       # imm = 0x90000000
	jl	.LBB3_3
	jmp	.LBB3_49
.LBB3_54:                               #   in Loop: Header=BB3_3 Depth=2
	movl	%r12d, %r15d
	andl	$134217727, %r15d       # imm = 0x7FFFFFF
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rbp), %rax
	movq	%r15, %rcx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rdx
	cmpq	$-1, %rdx
	je	.LBB3_84
# BB#55:                                #   in Loop: Header=BB3_3 Depth=2
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx), %rsi
	subq	%rsi, %rdx
	je	.LBB3_57
# BB#56:                                #   in Loop: Header=BB3_3 Depth=2
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB3_59
.LBB3_57:                               #   in Loop: Header=BB3_3 Depth=2
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	$100, %eax
	movq	32(%rsp), %r12          # 8-byte Reload
	jg	.LBB3_84
# BB#58:                                #   in Loop: Header=BB3_3 Depth=2
	incl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
.LBB3_59:                               #   in Loop: Header=BB3_3 Depth=2
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	%r12, %rax
	subq	%rdx, %rax
	cmpq	%rax, %r14
	ja	.LBB3_84
# BB#60:                                #   in Loop: Header=BB3_3 Depth=2
	addq	24(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB3_84
# BB#61:                                # %.preheader
                                        #   in Loop: Header=BB3_3 Depth=2
	orq	$1073741824, %r15       # imm = 0x40000000
	incq	%r13
	cmpq	%r15, 56(%rsp)          # 8-byte Folded Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	jne	.LBB3_63
# BB#62:                                #   in Loop: Header=BB3_3 Depth=2
	movq	72(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB3_65
.LBB3_63:                               # %._crit_edge404.preheader
                                        #   in Loop: Header=BB3_3 Depth=2
	movq	72(%rsp), %rcx          # 8-byte Reload
.LBB3_64:                               # %._crit_edge404
                                        #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r15, (%rax,%r13,8)
	leaq	1(%r13), %r13
	jne	.LBB3_64
.LBB3_65:                               # %tailrecurse.outer275
                                        #   in Loop: Header=BB3_3 Depth=2
	addq	%rcx, %r14
	cmpq	%r8, %r13
	jl	.LBB3_3
	jmp	.LBB3_49
.LBB3_68:                               #   in Loop: Header=BB3_2 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	56(%rbp), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%r14, 8(%rax,%rcx,8)
	incq	%rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	jmp	.LBB3_69
.LBB3_70:                               #   in Loop: Header=BB3_2 Depth=1
	movq	%r12, %rcx
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	56(%rbp), %rax
	movq	48(%rsp), %r12          # 8-byte Reload
	cmpq	(%rax,%r12,8), %r14
	je	.LBB3_71
# BB#72:                                #   in Loop: Header=BB3_2 Depth=1
	movq	%r14, (%rax,%r12,8)
	movq	%rcx, %rax
	andl	$134217727, %eax        # imm = 0x7FFFFFF
	movq	%r15, %rcx
	subq	%rax, %rcx
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %r9
	callq	sbackref
	testq	%rax, %rax
	jne	.LBB3_83
# BB#73:                                #   in Loop: Header=BB3_2 Depth=1
	decq	%r12
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	64(%rsp), %r8           # 8-byte Reload
	jmp	.LBB3_69
.LBB3_71:                               #   in Loop: Header=BB3_2 Depth=1
	decq	%r12
	movq	%r12, 48(%rsp)          # 8-byte Spill
.LBB3_69:                               # %tailrecurse.outer.backedge
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	%r8, %r15
	movq	32(%rsp), %r12          # 8-byte Reload
	jl	.LBB3_2
.LBB3_49:                               # %.critedge.thread412
	xorl	%eax, %eax
	cmpq	%r12, %r14
	cmoveq	%r14, %rax
.LBB3_83:                               # %.loopexit
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB3_84:                               # %.loopexit
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_78:
	andl	$134217727, %r12d       # imm = 0x7FFFFFF
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rdi), %rax
	shlq	$4, %r12
	movq	(%rax,%r12), %rbx
	movq	%r14, %rcx
	subq	24(%rdi), %rcx
	movq	%rcx, (%rax,%r12)
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	sbackref
	movq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB3_84
# BB#79:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	%rbx, (%rax,%r12)
	jmp	.LBB3_82
.LBB3_80:
	andl	$134217727, %r12d       # imm = 0x7FFFFFF
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rdi), %rax
	shlq	$4, %r12
	movq	8(%rax,%r12), %rbx
	movq	%r14, %rcx
	subq	24(%rdi), %rcx
	movq	%rcx, 8(%rax,%r12)
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	sbackref
	movq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB3_84
# BB#81:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	%rbx, 8(%rax,%r12)
	jmp	.LBB3_82
.LBB3_74:
	andl	$134217727, %r12d       # imm = 0x7FFFFFF
	leaq	-1(%r13,%r12), %r12
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movq	%r12, %r8
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	sbackref
	movq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB3_84
# BB#75:                                # %.lr.ph.preheader
	movl	$4160749568, %r15d      # imm = 0xF8000000
	movl	$134217727, %r13d       # imm = 0x7FFFFFF
.LBB3_76:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r12,8), %rcx
	andq	%r15, %rcx
	cmpq	%rbx, %rcx
	je	.LBB3_82
# BB#77:                                #   in Loop: Header=BB3_76 Depth=1
	movq	8(%rax,%r12,8), %rbp
	andq	%r13, %rbp
	leaq	1(%rbp,%r12), %rdx
	movq	(%rax,%rdx,8), %rax
	andq	%r15, %rax
	leaq	-134217728(%rbx), %rsi
	addq	%r12, %rbp
	leaq	2(%r12), %rcx
	cmpq	%rsi, %rax
	cmovneq	%rdx, %rbp
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %r8
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	sbackref
	movq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	movq	%rbp, %r12
	je	.LBB3_76
	jmp	.LBB3_84
.LBB3_82:                               # %.loopexit
	xorl	%eax, %eax
	jmp	.LBB3_83
.Lfunc_end3:
	.size	sbackref, .Lfunc_end3-sbackref
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_6
	.quad	.LBB3_14
	.quad	.LBB3_19
	.quad	.LBB3_9
	.quad	.LBB3_11
	.quad	.LBB3_52
	.quad	.LBB3_52
	.quad	.LBB3_52
	.quad	.LBB3_52
	.quad	.LBB3_52
	.quad	.LBB3_48
	.quad	.LBB3_52
	.quad	.LBB3_52
	.quad	.LBB3_52
	.quad	.LBB3_50
	.quad	.LBB3_52
	.quad	.LBB3_52
	.quad	.LBB3_23
	.quad	.LBB3_36
.LJTI3_1:
	.quad	.LBB3_54
	.quad	.LBB3_84
	.quad	.LBB3_68
	.quad	.LBB3_70
	.quad	.LBB3_66
	.quad	.LBB3_84
	.quad	.LBB3_78
	.quad	.LBB3_80
	.quad	.LBB3_74

	.text
	.p2align	4, 0x90
	.type	sstep,@function
sstep:                                  # @sstep
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%rcx, %r10
	cmpq	%rdx, %rsi
	je	.LBB4_29
# BB#1:                                 # %.lr.ph139
	movl	$1, %ebx
	movl	%esi, %ecx
	shlq	%cl, %rbx
	movq	8(%rdi), %r14
	movl	%r8d, %r11d
	orl	$2, %r11d
	movl	%r8d, %r15d
	orl	$1, %r15d
	movzbl	%r8b, %eax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movl	$2415919104, %r13d      # imm = 0x90000000
	.p2align	4, 0x90
.LBB4_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_21 Depth 2
	movq	(%r14,%rsi,8), %rcx
	movl	%ecx, %ebp
	andl	$-134217728, %ebp       # imm = 0xF8000000
	addq	$-134217728, %rbp       # imm = 0xF8000000
	shrq	$27, %rbp
	decq	%rbp
	cmpq	$18, %rbp
	ja	.LBB4_28
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	jmpq	*.LJTI4_0(,%rbp,8)
.LBB4_26:                               #   in Loop: Header=BB4_2 Depth=1
	movq	%r9, %rax
	andq	%rbx, %rax
	addq	%rax, %rax
	jmp	.LBB4_27
.LBB4_14:                               #   in Loop: Header=BB4_2 Depth=1
	movq	%r9, %rcx
	andq	%rbx, %rcx
	jmp	.LBB4_6
.LBB4_18:                               #   in Loop: Header=BB4_2 Depth=1
	movq	%r9, %rax
	andq	%rbx, %rax
	addq	%rax, %rax
	orq	%r9, %rax
	movq	%rax, %r9
	andq	%rbx, %r9
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %r9
	jmp	.LBB4_27
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	movsbl	%cl, %ecx
	cmpl	%r8d, %ecx
	je	.LBB4_5
	jmp	.LBB4_28
.LBB4_7:                                #   in Loop: Header=BB4_2 Depth=1
	cmpl	$131, %r11d
	je	.LBB4_5
	jmp	.LBB4_28
.LBB4_8:                                #   in Loop: Header=BB4_2 Depth=1
	cmpl	$131, %r15d
	je	.LBB4_5
	jmp	.LBB4_28
.LBB4_11:                               #   in Loop: Header=BB4_2 Depth=1
	cmpl	$127, %r8d
	jle	.LBB4_5
	jmp	.LBB4_28
.LBB4_12:                               #   in Loop: Header=BB4_2 Depth=1
	cmpl	$127, %r8d
	jg	.LBB4_28
# BB#13:                                #   in Loop: Header=BB4_2 Depth=1
	andl	$134217727, %ecx        # imm = 0x7FFFFFF
	movq	24(%rdi), %rbp
	shlq	$5, %rcx
	movq	(%rbp,%rcx), %r12
	movb	8(%rbp,%rcx), %cl
	movq	-8(%rsp), %rax          # 8-byte Reload
	testb	(%r12,%rax), %cl
	jne	.LBB4_5
	jmp	.LBB4_28
.LBB4_15:                               #   in Loop: Header=BB4_2 Depth=1
	movq	%r9, %rax
	andq	%rbx, %rax
	addq	%rax, %rax
	orq	%r9, %rax
	andl	$134217727, %ecx        # imm = 0x7FFFFFF
	movq	%rbx, %rbp
	shrq	%cl, %rbp
	movq	%rax, %r9
	andq	%rbx, %r9
	shrq	%cl, %r9
	orq	%rax, %r9
	testq	%rax, %rbp
	jne	.LBB4_28
# BB#16:                                #   in Loop: Header=BB4_2 Depth=1
	andq	%r9, %rbp
	je	.LBB4_28
# BB#17:                                #   in Loop: Header=BB4_2 Depth=1
	decq	%rsi
	subq	%rcx, %rsi
	movl	$1, %ebx
	movl	%esi, %ecx
	shlq	%cl, %rbx
	jmp	.LBB4_28
.LBB4_19:                               #   in Loop: Header=BB4_2 Depth=1
	movq	%r9, %rbp
	andq	%rbx, %rbp
	je	.LBB4_28
# BB#20:                                # %.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	8(%r14,%rsi,8), %r12
	movl	%r12d, %eax
	andl	$-134217728, %eax       # imm = 0xF8000000
	movl	$1, %ecx
	cmpq	%r13, %rax
	je	.LBB4_23
	.p2align	4, 0x90
.LBB4_21:                               # %.lr.ph
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	andl	$134217727, %r12d       # imm = 0x7FFFFFF
	addq	%r12, %rcx
	leaq	(%rcx,%rsi), %rax
	movq	(%r14,%rax,8), %r12
	movl	%r12d, %eax
	andl	$-134217728, %eax       # imm = 0xF8000000
	cmpq	%r13, %rax
	jne	.LBB4_21
.LBB4_23:                               # %._crit_edge
                                        #   in Loop: Header=BB4_2 Depth=1
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rbp
	orq	%rbp, %r9
	jmp	.LBB4_28
.LBB4_24:                               #   in Loop: Header=BB4_2 Depth=1
	movq	%r9, %rax
	andq	%rbx, %rax
	addq	%rax, %rax
	orq	%rax, %r9
	andl	$134217727, %ecx        # imm = 0x7FFFFFF
	leaq	(%rcx,%rsi), %rax
	movq	(%r14,%rax,8), %rax
	movl	$4160749568, %ebp       # imm = 0xF8000000
	andq	%rbp, %rax
	cmpq	%r13, %rax
	je	.LBB4_28
# BB#25:                                #   in Loop: Header=BB4_2 Depth=1
	movq	%r9, %rax
	andq	%rbx, %rax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rax
	.p2align	4, 0x90
.LBB4_27:                               #   in Loop: Header=BB4_2 Depth=1
	orq	%rax, %r9
	jmp	.LBB4_28
.LBB4_9:                                #   in Loop: Header=BB4_2 Depth=1
	cmpl	$133, %r8d
	je	.LBB4_5
	jmp	.LBB4_28
.LBB4_10:                               #   in Loop: Header=BB4_2 Depth=1
	cmpl	$134, %r8d
	jne	.LBB4_28
.LBB4_5:                                #   in Loop: Header=BB4_2 Depth=1
	movq	%rbx, %rcx
	andq	%r10, %rcx
.LBB4_6:                                #   in Loop: Header=BB4_2 Depth=1
	addq	%rcx, %rcx
	orq	%rcx, %r9
.LBB4_28:                               #   in Loop: Header=BB4_2 Depth=1
	incq	%rsi
	addq	%rbx, %rbx
	cmpq	%rdx, %rsi
	jne	.LBB4_2
.LBB4_29:                               # %._crit_edge140
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	sstep, .Lfunc_end4-sstep
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_4
	.quad	.LBB4_7
	.quad	.LBB4_8
	.quad	.LBB4_11
	.quad	.LBB4_12
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_14
	.quad	.LBB4_15
	.quad	.LBB4_18
	.quad	.LBB4_26
	.quad	.LBB4_26
	.quad	.LBB4_26
	.quad	.LBB4_18
	.quad	.LBB4_19
	.quad	.LBB4_24
	.quad	.LBB4_26
	.quad	.LBB4_9
	.quad	.LBB4_10

	.text
	.p2align	4, 0x90
	.type	lslow,@function
lslow:                                  # @lslow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi70:
	.cfi_def_cfa_offset 112
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movq	%rcx, %r14
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	80(%r12), %rbp
	movq	104(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	96(%r12), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpq	%r15, 32(%r12)
	je	.LBB5_1
# BB#2:
	movsbl	-1(%r15), %ebx
	jmp	.LBB5_3
.LBB5_1:
	movl	$128, %ebx
.LBB5_3:
	movq	(%r12), %rax
	movq	48(%rax), %rdx
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	memset
	movb	$1, (%rbp,%r14)
	movq	(%r12), %rdi
	movl	$132, %r8d
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%rbp, %rcx
	movq	%rbp, %r9
	callq	lstep
	movq	%rax, %rbp
	jmp	.LBB5_4
	.p2align	4, 0x90
.LBB5_46:                               #   in Loop: Header=BB5_4 Depth=1
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memmove
	movq	(%r12), %rax
	movq	48(%rax), %rdx
	movq	%rbp, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	memmove
	movq	(%r12), %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	16(%rsp), %r15          # 8-byte Reload
	movl	12(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %r8d
	movq	%rbp, %r9
	callq	lstep
	movq	%rax, %rbp
	incq	%r15
.LBB5_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_22 Depth 2
	movl	%ebx, %edx
	cmpq	40(%r12), %r15
	movq	%r15, 16(%rsp)          # 8-byte Spill
	je	.LBB5_5
# BB#6:                                 #   in Loop: Header=BB5_4 Depth=1
	movsbl	(%r15), %ecx
	jmp	.LBB5_7
	.p2align	4, 0x90
.LBB5_5:                                #   in Loop: Header=BB5_4 Depth=1
	movl	$128, %ecx
.LBB5_7:                                #   in Loop: Header=BB5_4 Depth=1
	xorl	%r15d, %r15d
	cmpl	$128, %edx
	je	.LBB5_11
# BB#8:                                 #   in Loop: Header=BB5_4 Depth=1
	cmpl	$10, %edx
	jne	.LBB5_9
# BB#10:                                #   in Loop: Header=BB5_4 Depth=1
	movq	(%r12), %rax
	testb	$8, 40(%rax)
	movl	$0, %ebx
	jne	.LBB5_13
	jmp	.LBB5_14
	.p2align	4, 0x90
.LBB5_11:                               #   in Loop: Header=BB5_4 Depth=1
	testb	$1, 8(%r12)
	movl	$0, %ebx
	jne	.LBB5_14
# BB#12:                                # %._crit_edge
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	(%r12), %rax
.LBB5_13:                               #   in Loop: Header=BB5_4 Depth=1
	movl	76(%rax), %ebx
	movl	$129, %r15d
	jmp	.LBB5_14
	.p2align	4, 0x90
.LBB5_9:                                #   in Loop: Header=BB5_4 Depth=1
	xorl	%ebx, %ebx
.LBB5_14:                               #   in Loop: Header=BB5_4 Depth=1
	cmpl	$128, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	%edx, 8(%rsp)           # 4-byte Spill
	je	.LBB5_17
# BB#15:                                #   in Loop: Header=BB5_4 Depth=1
	cmpl	$10, %ecx
	jne	.LBB5_20
# BB#16:                                #   in Loop: Header=BB5_4 Depth=1
	movq	(%r12), %rax
	testb	$8, 40(%rax)
	jne	.LBB5_19
	jmp	.LBB5_20
	.p2align	4, 0x90
.LBB5_17:                               #   in Loop: Header=BB5_4 Depth=1
	testb	$2, 8(%r12)
	jne	.LBB5_20
# BB#18:                                # %._crit_edge137
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	(%r12), %rax
.LBB5_19:                               #   in Loop: Header=BB5_4 Depth=1
	xorl	%ecx, %ecx
	cmpl	$129, %r15d
	sete	%cl
	orl	$130, %ecx
	addl	80(%rax), %ebx
	movl	%ecx, %r15d
.LBB5_20:                               #   in Loop: Header=BB5_4 Depth=1
	testl	%ebx, %ebx
	jle	.LBB5_23
# BB#21:                                # %.preheader.preheader
                                        #   in Loop: Header=BB5_4 Depth=1
	incl	%ebx
	.p2align	4, 0x90
.LBB5_22:                               # %.preheader
                                        #   Parent Loop BB5_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%rbp, %rcx
	movl	%r15d, %r8d
	movq	%rbp, %r9
	callq	lstep
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB5_22
.LBB5_23:                               # %.loopexit
                                        #   in Loop: Header=BB5_4 Depth=1
	cmpl	$129, %r15d
	jne	.LBB5_24
# BB#30:                                #   in Loop: Header=BB5_4 Depth=1
	movl	$129, %ebx
	cmpl	$128, 12(%rsp)          # 4-byte Folded Reload
	movl	8(%rsp), %edi           # 4-byte Reload
	je	.LBB5_33
# BB#31:                                # %._crit_edge139
                                        #   in Loop: Header=BB5_4 Depth=1
	callq	__ctype_b_loc
	movl	8(%rsp), %edi           # 4-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	jmp	.LBB5_32
	.p2align	4, 0x90
.LBB5_24:                               #   in Loop: Header=BB5_4 Depth=1
	cmpl	$128, 8(%rsp)           # 4-byte Folded Reload
	jne	.LBB5_26
# BB#25:                                #   in Loop: Header=BB5_4 Depth=1
	movl	%r15d, %ebx
	movq	16(%rsp), %r15          # 8-byte Reload
	jmp	.LBB5_42
	.p2align	4, 0x90
.LBB5_26:                               #   in Loop: Header=BB5_4 Depth=1
	callq	__ctype_b_loc
	movl	8(%rsp), %edi           # 4-byte Reload
	movslq	%edi, %rcx
	movl	12(%rsp), %esi          # 4-byte Reload
	cmpl	$128, %esi
	je	.LBB5_27
# BB#28:                                #   in Loop: Header=BB5_4 Depth=1
	cmpl	$95, %edi
	je	.LBB5_27
# BB#29:                                #   in Loop: Header=BB5_4 Depth=1
	movq	(%rax), %rdx
	movzwl	(%rdx,%rcx,2), %edx
	andl	$8, %edx
	testw	%dx, %dx
	jne	.LBB5_27
.LBB5_32:                               #   in Loop: Header=BB5_4 Depth=1
	movq	(%rax), %rax
	movslq	%esi, %rcx
	testb	$8, (%rax,%rcx,2)
	movl	$133, %eax
	cmovnel	%eax, %r15d
	cmpl	$95, %esi
	cmovel	%eax, %r15d
	movl	%r15d, %ebx
.LBB5_33:                               #   in Loop: Header=BB5_4 Depth=1
	cmpl	$128, %edi
	jne	.LBB5_35
# BB#34:                                #   in Loop: Header=BB5_4 Depth=1
	movq	16(%rsp), %r15          # 8-byte Reload
	jmp	.LBB5_42
	.p2align	4, 0x90
.LBB5_35:                               # %..thread132_crit_edge
                                        #   in Loop: Header=BB5_4 Depth=1
	callq	__ctype_b_loc
	movl	8(%rsp), %edi           # 4-byte Reload
	movslq	%edi, %rcx
	jmp	.LBB5_36
.LBB5_27:                               #   in Loop: Header=BB5_4 Depth=1
	movl	%r15d, %ebx
.LBB5_36:                               # %.thread132
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	(%rax), %rax
	cmpl	$95, %edi
	je	.LBB5_38
# BB#37:                                # %.thread132
                                        #   in Loop: Header=BB5_4 Depth=1
	movzwl	(%rax,%rcx,2), %ecx
	andl	$8, %ecx
	testw	%cx, %cx
	je	.LBB5_42
.LBB5_38:                               #   in Loop: Header=BB5_4 Depth=1
	movl	$134, %r8d
	cmpl	$130, %ebx
	je	.LBB5_43
# BB#39:                                #   in Loop: Header=BB5_4 Depth=1
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmpl	$128, %ecx
	je	.LBB5_42
# BB#40:                                #   in Loop: Header=BB5_4 Depth=1
	cmpl	$95, %ecx
	je	.LBB5_42
# BB#41:                                #   in Loop: Header=BB5_4 Depth=1
	movslq	%ecx, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$8, %eax
	testw	%ax, %ax
	je	.LBB5_43
	.p2align	4, 0x90
.LBB5_42:                               # %.thread
                                        #   in Loop: Header=BB5_4 Depth=1
	leal	-133(%rbx), %eax
	cmpl	$1, %eax
	movl	%ebx, %r8d
	ja	.LBB5_44
.LBB5_43:                               # %.thread134
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	(%r12), %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%rbp, %rcx
	movq	%rbp, %r9
	callq	lstep
	movq	%rax, %rbp
.LBB5_44:                               #   in Loop: Header=BB5_4 Depth=1
	cmpb	$0, (%rbp,%r13)
	movq	24(%rsp), %rax          # 8-byte Reload
	cmovneq	%r15, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	(%r12), %rax
	movq	48(%rax), %rbx
	movq	%rbp, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	memcmp
	cmpq	48(%rsp), %r15          # 8-byte Folded Reload
	je	.LBB5_47
# BB#45:                                #   in Loop: Header=BB5_4 Depth=1
	testl	%eax, %eax
	jne	.LBB5_46
.LBB5_47:
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	lslow, .Lfunc_end5-lslow
	.cfi_endproc

	.p2align	4, 0x90
	.type	ldissect,@function
ldissect:                               # @ldissect
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi83:
	.cfi_def_cfa_offset 96
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rsi, %r9
	movq	%rdi, %r12
	cmpq	%r8, %rbx
	jge	.LBB6_16
# BB#1:                                 # %.lr.ph188
	movl	$2415919104, %edi       # imm = 0x90000000
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_6 Depth 2
                                        #     Child Loop BB6_24 Depth 2
                                        #     Child Loop BB6_28 Depth 2
                                        #     Child Loop BB6_11 Depth 2
                                        #     Child Loop BB6_18 Depth 2
                                        #     Child Loop BB6_20 Depth 2
	movq	(%r12), %rax
	movq	8(%rax), %rcx
	movq	(%rcx,%rbx,8), %rax
	movl	%eax, %edx
	andl	$-134217728, %edx       # imm = 0xF8000000
	cmpl	$1207959552, %edx       # imm = 0x48000000
	je	.LBB6_33
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpl	$1476395008, %edx       # imm = 0x58000000
	je	.LBB6_33
# BB#4:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpl	$2013265920, %edx       # imm = 0x78000000
	movq	%rbx, %r14
	jne	.LBB6_7
# BB#5:                                 # %.preheader174
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	%eax, %edx
	andl	$-134217728, %edx       # imm = 0xF8000000
	cmpq	%rdi, %rdx
	movq	%rax, %rdx
	movq	%rbx, %r14
	je	.LBB6_7
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	andl	$134217727, %edx        # imm = 0x7FFFFFF
	addq	%rdx, %r14
	movq	(%rcx,%r14,8), %rdx
	movl	%edx, %esi
	andl	$-134217728, %esi       # imm = 0xF8000000
	cmpq	%rdi, %rsi
	jne	.LBB6_6
	jmp	.LBB6_7
	.p2align	4, 0x90
.LBB6_33:                               #   in Loop: Header=BB6_2 Depth=1
	movl	%eax, %r14d
	andl	$134217727, %r14d       # imm = 0x7FFFFFF
	addq	%rbx, %r14
.LBB6_7:                                # %.loopexit
                                        #   in Loop: Header=BB6_2 Depth=1
	leaq	1(%r14), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	%eax, %ecx
	andl	$-134217728, %ecx       # imm = 0xF8000000
	addq	$-134217728, %rcx       # imm = 0xF8000000
	shrq	$27, %rcx
	decq	%rcx
	cmpq	$13, %rcx
	ja	.LBB6_15
# BB#8:                                 # %.loopexit
                                        #   in Loop: Header=BB6_2 Depth=1
	jmpq	*.LJTI6_0(,%rcx,8)
.LBB6_9:                                #   in Loop: Header=BB6_2 Depth=1
	incq	%r9
	jmp	.LBB6_15
.LBB6_17:                               # %.preheader172.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	%r12, %r15
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdx
	movq	24(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_18:                               # %.preheader172
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rdi
	movq	%r9, %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rbx, %r8
	movq	%r9, %r12
	callq	lslow
	movq	%rax, %rbp
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movq	%rbx, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	lslow
	movq	%r12, %r9
	leaq	-1(%rbp), %rdx
	cmpq	%r13, %rax
	jne	.LBB6_18
# BB#19:                                #   in Loop: Header=BB6_2 Depth=1
	incq	8(%rsp)                 # 8-byte Folded Spill
	movq	%r9, %rbx
	movq	%r15, %r12
	.p2align	4, 0x90
.LBB6_20:                               #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %r13
	movq	%r9, %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%r14, %r8
	callq	lslow
	movq	%rax, %r9
	testq	%r9, %r9
	je	.LBB6_22
# BB#21:                                #   in Loop: Header=BB6_20 Depth=2
	cmpq	%rbx, %r9
	jne	.LBB6_20
.LBB6_22:                               #   in Loop: Header=BB6_2 Depth=1
	testq	%r9, %r9
	cmovneq	%rbx, %r13
	cmoveq	%rbx, %r9
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%r9, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%r14, %r8
	jmp	.LBB6_30
.LBB6_10:                               # %.preheader173.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	%r12, %r15
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%r9, %rbx
	movq	24(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_11:                               # %.preheader173
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%r13, %r8
	callq	lslow
	movq	%rax, %rbp
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	movq	%r13, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	lslow
	leaq	-1(%rbp), %rdx
	cmpq	%r12, %rax
	jne	.LBB6_11
# BB#12:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%rbx, %rsi
	movq	8(%rsp), %rbx           # 8-byte Reload
	incq	%rbx
	movq	%r15, %rdi
	movq	%rsi, %r13
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movq	%r14, %r8
	callq	lslow
	testq	%rax, %rax
	movq	%r15, %r12
	je	.LBB6_14
# BB#13:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movq	%r14, %r8
	callq	ldissect
.LBB6_14:                               # %.backedge
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	%rbp, %r9
	movl	$2415919104, %edi       # imm = 0x90000000
	movq	16(%rsp), %r8           # 8-byte Reload
	jmp	.LBB6_15
.LBB6_31:                               #   in Loop: Header=BB6_2 Depth=1
	andl	$134217727, %eax        # imm = 0x7FFFFFF
	movq	%r9, %rcx
	subq	24(%r12), %rcx
	movq	16(%r12), %rdx
	shlq	$4, %rax
	movq	%rcx, (%rdx,%rax)
	jmp	.LBB6_15
.LBB6_32:                               #   in Loop: Header=BB6_2 Depth=1
	andl	$134217727, %eax        # imm = 0x7FFFFFF
	movq	%r9, %rcx
	subq	24(%r12), %rcx
	movq	16(%r12), %rdx
	shlq	$4, %rax
	movq	%rcx, 8(%rdx,%rax)
	jmp	.LBB6_15
.LBB6_23:                               # %.preheader.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movq	%r12, %r15
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdx
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r8, %r13
	.p2align	4, 0x90
.LBB6_24:                               # %.preheader
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rbx, %rcx
	movq	%r14, %r8
	callq	lslow
	movq	%rax, %rbp
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%r13, %r8
	callq	lslow
	leaq	-1(%rbp), %rdx
	cmpq	%r12, %rax
	jne	.LBB6_24
# BB#25:                                #   in Loop: Header=BB6_2 Depth=1
	movq	(%r15), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movl	$134217727, %ecx        # imm = 0x7FFFFFF
	andq	%rcx, %rax
	leaq	-1(%rbx,%rax), %r13
	leaq	1(%rbx), %r14
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rbp, %rdx
	movq	%r14, %rcx
	movq	%r13, %r8
	callq	lslow
	cmpq	%rbp, %rax
	movq	%r15, %r12
	je	.LBB6_26
# BB#27:                                # %.lr.ph181.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	.p2align	4, 0x90
.LBB6_28:                               # %.lr.ph181
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rax
	movq	8(%rax), %rax
	movq	8(%rax,%r13,8), %rbx
	movl	$134217727, %ecx        # imm = 0x7FFFFFF
	andq	%rcx, %rbx
	leaq	1(%rbx,%r13), %rcx
	movq	(%rax,%rcx,8), %rax
	movl	$4160749568, %edx       # imm = 0xF8000000
	andq	%rdx, %rax
	movl	$2415919104, %edx       # imm = 0x90000000
	leaq	-134217728(%rdx), %rdx
	addq	%r13, %rbx
	leaq	2(%r13), %r14
	cmpq	%rdx, %rax
	cmovneq	%rcx, %rbx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%r14, %rcx
	movq	%rbx, %r8
	callq	lslow
	cmpq	%rbp, %rax
	movq	%rbx, %r13
	jne	.LBB6_28
	jmp	.LBB6_29
.LBB6_26:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%r13, %rbx
	movq	8(%rsp), %r15           # 8-byte Reload
.LBB6_29:                               # %._crit_edge
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%r14, %rcx
	movq	%rbx, %r8
.LBB6_30:                               # %.backedge
                                        #   in Loop: Header=BB6_2 Depth=1
	callq	ldissect
	movq	%rbp, %r9
	movq	16(%rsp), %r8           # 8-byte Reload
	movl	$2415919104, %edi       # imm = 0x90000000
.LBB6_15:                               # %.backedge
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmpq	%r8, %rbx
	jl	.LBB6_2
.LBB6_16:                               # %._crit_edge189
	movq	%r9, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	ldissect, .Lfunc_end6-ldissect
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_9
	.quad	.LBB6_15
	.quad	.LBB6_15
	.quad	.LBB6_9
	.quad	.LBB6_9
	.quad	.LBB6_15
	.quad	.LBB6_15
	.quad	.LBB6_17
	.quad	.LBB6_15
	.quad	.LBB6_10
	.quad	.LBB6_15
	.quad	.LBB6_31
	.quad	.LBB6_32
	.quad	.LBB6_23

	.text
	.p2align	4, 0x90
	.type	lbackref,@function
lbackref:                               # @lbackref
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi93:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi94:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi96:
	.cfi_def_cfa_offset 144
.Lcfi97:
	.cfi_offset %rbx, -56
.Lcfi98:
	.cfi_offset %r12, -48
.Lcfi99:
	.cfi_offset %r13, -40
.Lcfi100:
	.cfi_offset %r14, -32
.Lcfi101:
	.cfi_offset %r15, -24
.Lcfi102:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %rbp
	cmpq	%r8, %r15
	jge	.LBB7_49
# BB#1:                                 # %.lr.ph346.lr.ph.lr.ph.preheader
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movl	144(%rsp), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	$2415919104, %ebx       # imm = 0x90000000
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%r8, 64(%rsp)           # 8-byte Spill
.LBB7_2:                                # %.lr.ph346.lr.ph.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_3 Depth 2
                                        #       Child Loop BB7_4 Depth 3
                                        #         Child Loop BB7_51 Depth 4
                                        #       Child Loop BB7_64 Depth 3
	movq	%r15, %r13
.LBB7_3:                                # %.lr.ph346
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_4 Depth 3
                                        #         Child Loop BB7_51 Depth 4
                                        #       Child Loop BB7_64 Depth 3
	movq	(%rbp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	8(%rax), %r15
	.p2align	4, 0x90
.LBB7_4:                                #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_3 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB7_51 Depth 4
	movq	(%r15,%r13,8), %rax
	movl	%eax, %ecx
	andl	$-134217728, %ecx       # imm = 0xF8000000
	addq	$-268435456, %rcx       # imm = 0xF0000000
	shrq	$27, %rcx
	cmpq	$18, %rcx
	ja	.LBB7_52
# BB#5:                                 #   in Loop: Header=BB7_4 Depth=3
	jmpq	*.LJTI7_0(,%rcx,8)
.LBB7_6:                                #   in Loop: Header=BB7_4 Depth=3
	cmpq	%r12, %r14
	je	.LBB7_84
# BB#7:                                 #   in Loop: Header=BB7_4 Depth=3
	movsbl	(%r14), %ecx
	movsbl	%al, %eax
	cmpl	%eax, %ecx
	jne	.LBB7_84
# BB#8:                                 #   in Loop: Header=BB7_4 Depth=3
	incq	%r14
	jmp	.LBB7_48
.LBB7_14:                               #   in Loop: Header=BB7_4 Depth=3
	cmpq	32(%rbp), %r14
	jne	.LBB7_16
# BB#15:                                #   in Loop: Header=BB7_4 Depth=3
	testb	$1, 8(%rbp)
	je	.LBB7_48
.LBB7_16:                               #   in Loop: Header=BB7_4 Depth=3
	cmpq	40(%rbp), %r14
	jae	.LBB7_84
# BB#17:                                #   in Loop: Header=BB7_4 Depth=3
	cmpb	$10, -1(%r14)
	je	.LBB7_18
	jmp	.LBB7_84
.LBB7_19:                               #   in Loop: Header=BB7_4 Depth=3
	movq	40(%rbp), %rax
	cmpq	%rax, %r14
	jne	.LBB7_21
# BB#20:                                #   in Loop: Header=BB7_4 Depth=3
	testb	$2, 8(%rbp)
	je	.LBB7_48
.LBB7_21:                               #   in Loop: Header=BB7_4 Depth=3
	cmpq	%rax, %r14
	jae	.LBB7_84
# BB#22:                                #   in Loop: Header=BB7_4 Depth=3
	cmpb	$10, (%r14)
	jne	.LBB7_84
.LBB7_18:                               #   in Loop: Header=BB7_4 Depth=3
	movq	56(%rsp), %rax          # 8-byte Reload
	testb	$8, 40(%rax)
	jne	.LBB7_48
	jmp	.LBB7_84
.LBB7_9:                                #   in Loop: Header=BB7_4 Depth=3
	cmpq	%r12, %r14
	je	.LBB7_84
# BB#10:                                #   in Loop: Header=BB7_4 Depth=3
	incq	%r14
	jmp	.LBB7_48
.LBB7_11:                               #   in Loop: Header=BB7_4 Depth=3
	cmpq	%r12, %r14
	je	.LBB7_84
# BB#12:                                #   in Loop: Header=BB7_4 Depth=3
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	24(%rcx), %rcx
	andl	$134217727, %eax        # imm = 0x7FFFFFF
	shlq	$5, %rax
	movq	(%rcx,%rax), %rdx
	movzbl	(%r14), %esi
	movb	8(%rcx,%rax), %al
	testb	(%rdx,%rsi), %al
	je	.LBB7_84
# BB#13:                                #   in Loop: Header=BB7_4 Depth=3
	incq	%r14
	jmp	.LBB7_48
.LBB7_50:                               #   in Loop: Header=BB7_4 Depth=3
	movq	8(%r15,%r13,8), %rax
	incq	%r13
	.p2align	4, 0x90
.LBB7_51:                               #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_3 Depth=2
                                        #       Parent Loop BB7_4 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	andl	$134217727, %eax        # imm = 0x7FFFFFF
	addq	%rax, %r13
	movq	(%r15,%r13,8), %rax
	movl	%eax, %ecx
	andl	$-134217728, %ecx       # imm = 0xF8000000
	cmpq	%rbx, %rcx
	jne	.LBB7_51
	jmp	.LBB7_48
.LBB7_23:                               #   in Loop: Header=BB7_4 Depth=3
	movq	32(%rbp), %rax
	cmpq	%rax, %r14
	jne	.LBB7_26
# BB#24:                                #   in Loop: Header=BB7_4 Depth=3
	testb	$1, 8(%rbp)
	jne	.LBB7_26
# BB#25:                                # %._crit_edge
                                        #   in Loop: Header=BB7_4 Depth=3
	movq	40(%rbp), %r12
	cmpq	%r12, %r14
	jb	.LBB7_33
	jmp	.LBB7_84
.LBB7_36:                               #   in Loop: Header=BB7_4 Depth=3
	movq	40(%rbp), %rax
	cmpq	%rax, %r14
	jne	.LBB7_38
# BB#37:                                #   in Loop: Header=BB7_4 Depth=3
	movq	24(%rsp), %rcx          # 8-byte Reload
	testb	$2, 8(%rcx)
	je	.LBB7_43
.LBB7_38:                               #   in Loop: Header=BB7_4 Depth=3
	cmpq	%rax, %r14
	jae	.LBB7_84
# BB#39:                                #   in Loop: Header=BB7_4 Depth=3
	cmpb	$10, (%r14)
	jne	.LBB7_41
# BB#40:                                #   in Loop: Header=BB7_4 Depth=3
	movq	56(%rsp), %rax          # 8-byte Reload
	testb	$8, 40(%rax)
	jne	.LBB7_43
.LBB7_41:                               #   in Loop: Header=BB7_4 Depth=3
	callq	__ctype_b_loc
	movsbq	(%r14), %rcx
	cmpq	$95, %rcx
	je	.LBB7_84
# BB#42:                                #   in Loop: Header=BB7_4 Depth=3
	movq	(%rax), %rax
	movzwl	(%rax,%rcx,2), %eax
	andl	$8, %eax
	testw	%ax, %ax
	jne	.LBB7_84
.LBB7_43:                               #   in Loop: Header=BB7_4 Depth=3
	movq	24(%rsp), %rbp          # 8-byte Reload
	cmpq	32(%rbp), %r14
	jbe	.LBB7_84
# BB#44:                                #   in Loop: Header=BB7_4 Depth=3
	callq	__ctype_b_loc
	movsbq	-1(%r14), %rcx
	cmpq	$95, %rcx
	je	.LBB7_45
# BB#46:                                #   in Loop: Header=BB7_4 Depth=3
	movq	(%rax), %rax
	movzwl	(%rax,%rcx,2), %eax
	andl	$8, %eax
	testw	%ax, %ax
	jmp	.LBB7_47
.LBB7_26:                               #   in Loop: Header=BB7_4 Depth=3
	movq	40(%rbp), %r12
	cmpq	%r12, %r14
	jae	.LBB7_29
# BB#27:                                #   in Loop: Header=BB7_4 Depth=3
	cmpb	$10, -1(%r14)
	jne	.LBB7_29
# BB#28:                                #   in Loop: Header=BB7_4 Depth=3
	movq	56(%rsp), %rcx          # 8-byte Reload
	testb	$8, 40(%rcx)
	jne	.LBB7_32
.LBB7_29:                               #   in Loop: Header=BB7_4 Depth=3
	cmpq	%rax, %r14
	jbe	.LBB7_84
# BB#30:                                #   in Loop: Header=BB7_4 Depth=3
	callq	__ctype_b_loc
	movsbq	-1(%r14), %rcx
	cmpq	$95, %rcx
	je	.LBB7_84
# BB#31:                                #   in Loop: Header=BB7_4 Depth=3
	movq	(%rax), %rax
	movzwl	(%rax,%rcx,2), %eax
	andl	$8, %eax
	testw	%ax, %ax
	jne	.LBB7_84
.LBB7_32:                               #   in Loop: Header=BB7_4 Depth=3
	cmpq	%r12, %r14
	jae	.LBB7_84
.LBB7_33:                               #   in Loop: Header=BB7_4 Depth=3
	callq	__ctype_b_loc
	movsbq	(%r14), %rcx
	cmpq	$95, %rcx
	je	.LBB7_34
# BB#35:                                #   in Loop: Header=BB7_4 Depth=3
	movq	(%rax), %rax
	movzwl	(%rax,%rcx,2), %eax
	andl	$8, %eax
	testw	%ax, %ax
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB7_47:                               #   in Loop: Header=BB7_4 Depth=3
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	jne	.LBB7_48
	jmp	.LBB7_84
.LBB7_34:                               #   in Loop: Header=BB7_4 Depth=3
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB7_45:                               #   in Loop: Header=BB7_4 Depth=3
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB7_48:                               # %.loopexit271
                                        #   in Loop: Header=BB7_4 Depth=3
	incq	%r13
	cmpq	%r8, %r13
	jl	.LBB7_4
	jmp	.LBB7_49
.LBB7_52:                               # %.critedge.thread
                                        #   in Loop: Header=BB7_3 Depth=2
	movq	(%rbp), %rax
	movq	8(%rax), %rdi
	movq	(%rdi,%r13,8), %r12
	movl	%r12d, %eax
	andl	$-134217728, %eax       # imm = 0xF8000000
	addq	$-939524096, %rax       # imm = 0xC8000000
	shrq	$27, %rax
	cmpq	$8, %rax
	ja	.LBB7_84
# BB#53:                                # %.critedge.thread
                                        #   in Loop: Header=BB7_3 Depth=2
	leaq	1(%r13), %r15
	jmpq	*.LJTI7_1(,%rax,8)
.LBB7_66:                               #   in Loop: Header=BB7_3 Depth=2
	movq	%r12, %r13
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r8, %rbx
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	lbackref
	testq	%rax, %rax
	jne	.LBB7_83
# BB#67:                                # %tailrecurse
                                        #   in Loop: Header=BB7_3 Depth=2
	andl	$134217727, %r13d       # imm = 0x7FFFFFF
	addq	%r15, %r13
	cmpq	%rbx, %r13
	movq	%rbx, %r8
	movl	$2415919104, %ebx       # imm = 0x90000000
	jl	.LBB7_3
	jmp	.LBB7_49
.LBB7_54:                               #   in Loop: Header=BB7_3 Depth=2
	movl	%r12d, %r15d
	andl	$134217727, %r15d       # imm = 0x7FFFFFF
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rbp), %rax
	movq	%r15, %rcx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rdx
	cmpq	$-1, %rdx
	je	.LBB7_84
# BB#55:                                #   in Loop: Header=BB7_3 Depth=2
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx), %rsi
	subq	%rsi, %rdx
	je	.LBB7_57
# BB#56:                                #   in Loop: Header=BB7_3 Depth=2
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB7_59
.LBB7_57:                               #   in Loop: Header=BB7_3 Depth=2
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	$100, %eax
	movq	32(%rsp), %r12          # 8-byte Reload
	jg	.LBB7_84
# BB#58:                                #   in Loop: Header=BB7_3 Depth=2
	incl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
.LBB7_59:                               #   in Loop: Header=BB7_3 Depth=2
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	%r12, %rax
	subq	%rdx, %rax
	cmpq	%rax, %r14
	ja	.LBB7_84
# BB#60:                                #   in Loop: Header=BB7_3 Depth=2
	addq	24(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB7_84
# BB#61:                                # %.preheader
                                        #   in Loop: Header=BB7_3 Depth=2
	orq	$1073741824, %r15       # imm = 0x40000000
	incq	%r13
	cmpq	%r15, 56(%rsp)          # 8-byte Folded Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	jne	.LBB7_63
# BB#62:                                #   in Loop: Header=BB7_3 Depth=2
	movq	72(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB7_65
.LBB7_63:                               # %._crit_edge404.preheader
                                        #   in Loop: Header=BB7_3 Depth=2
	movq	72(%rsp), %rcx          # 8-byte Reload
.LBB7_64:                               # %._crit_edge404
                                        #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r15, (%rax,%r13,8)
	leaq	1(%r13), %r13
	jne	.LBB7_64
.LBB7_65:                               # %tailrecurse.outer275
                                        #   in Loop: Header=BB7_3 Depth=2
	addq	%rcx, %r14
	cmpq	%r8, %r13
	jl	.LBB7_3
	jmp	.LBB7_49
.LBB7_68:                               #   in Loop: Header=BB7_2 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	56(%rbp), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%r14, 8(%rax,%rcx,8)
	incq	%rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	jmp	.LBB7_69
.LBB7_70:                               #   in Loop: Header=BB7_2 Depth=1
	movq	%r12, %rcx
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	56(%rbp), %rax
	movq	48(%rsp), %r12          # 8-byte Reload
	cmpq	(%rax,%r12,8), %r14
	je	.LBB7_71
# BB#72:                                #   in Loop: Header=BB7_2 Depth=1
	movq	%r14, (%rax,%r12,8)
	movq	%rcx, %rax
	andl	$134217727, %eax        # imm = 0x7FFFFFF
	movq	%r15, %rcx
	subq	%rax, %rcx
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %r9
	callq	lbackref
	testq	%rax, %rax
	jne	.LBB7_83
# BB#73:                                #   in Loop: Header=BB7_2 Depth=1
	decq	%r12
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	64(%rsp), %r8           # 8-byte Reload
	jmp	.LBB7_69
.LBB7_71:                               #   in Loop: Header=BB7_2 Depth=1
	decq	%r12
	movq	%r12, 48(%rsp)          # 8-byte Spill
.LBB7_69:                               # %tailrecurse.outer.backedge
                                        #   in Loop: Header=BB7_2 Depth=1
	cmpq	%r8, %r15
	movq	32(%rsp), %r12          # 8-byte Reload
	jl	.LBB7_2
.LBB7_49:                               # %.critedge.thread412
	xorl	%eax, %eax
	cmpq	%r12, %r14
	cmoveq	%r14, %rax
.LBB7_83:                               # %.loopexit
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB7_84:                               # %.loopexit
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_78:
	andl	$134217727, %r12d       # imm = 0x7FFFFFF
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rdi), %rax
	shlq	$4, %r12
	movq	(%rax,%r12), %rbx
	movq	%r14, %rcx
	subq	24(%rdi), %rcx
	movq	%rcx, (%rax,%r12)
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	lbackref
	movq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB7_84
# BB#79:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	%rbx, (%rax,%r12)
	jmp	.LBB7_82
.LBB7_80:
	andl	$134217727, %r12d       # imm = 0x7FFFFFF
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rdi), %rax
	shlq	$4, %r12
	movq	8(%rax,%r12), %rbx
	movq	%r14, %rcx
	subq	24(%rdi), %rcx
	movq	%rcx, 8(%rax,%r12)
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	lbackref
	movq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB7_84
# BB#81:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	%rbx, 8(%rax,%r12)
	jmp	.LBB7_82
.LBB7_74:
	andl	$134217727, %r12d       # imm = 0x7FFFFFF
	leaq	-1(%r13,%r12), %r12
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movq	%r12, %r8
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	lbackref
	movq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB7_84
# BB#75:                                # %.lr.ph.preheader
	movl	$4160749568, %r15d      # imm = 0xF8000000
	movl	$134217727, %r13d       # imm = 0x7FFFFFF
.LBB7_76:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r12,8), %rcx
	andq	%r15, %rcx
	cmpq	%rbx, %rcx
	je	.LBB7_82
# BB#77:                                #   in Loop: Header=BB7_76 Depth=1
	movq	8(%rax,%r12,8), %rbp
	andq	%r13, %rbp
	leaq	1(%rbp,%r12), %rdx
	movq	(%rax,%rdx,8), %rax
	andq	%r15, %rax
	leaq	-134217728(%rbx), %rsi
	addq	%r12, %rbp
	leaq	2(%r12), %rcx
	cmpq	%rsi, %rax
	cmovneq	%rdx, %rbp
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %r8
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	lbackref
	movq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	movq	%rbp, %r12
	je	.LBB7_76
	jmp	.LBB7_84
.LBB7_82:                               # %.loopexit
	xorl	%eax, %eax
	jmp	.LBB7_83
.Lfunc_end7:
	.size	lbackref, .Lfunc_end7-lbackref
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_6
	.quad	.LBB7_14
	.quad	.LBB7_19
	.quad	.LBB7_9
	.quad	.LBB7_11
	.quad	.LBB7_52
	.quad	.LBB7_52
	.quad	.LBB7_52
	.quad	.LBB7_52
	.quad	.LBB7_52
	.quad	.LBB7_48
	.quad	.LBB7_52
	.quad	.LBB7_52
	.quad	.LBB7_52
	.quad	.LBB7_50
	.quad	.LBB7_52
	.quad	.LBB7_52
	.quad	.LBB7_23
	.quad	.LBB7_36
.LJTI7_1:
	.quad	.LBB7_54
	.quad	.LBB7_84
	.quad	.LBB7_68
	.quad	.LBB7_70
	.quad	.LBB7_66
	.quad	.LBB7_84
	.quad	.LBB7_78
	.quad	.LBB7_80
	.quad	.LBB7_74

	.text
	.p2align	4, 0x90
	.type	lstep,@function
lstep:                                  # @lstep
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi106:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi107:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 56
.Lcfi109:
	.cfi_offset %rbx, -56
.Lcfi110:
	.cfi_offset %r12, -48
.Lcfi111:
	.cfi_offset %r13, -40
.Lcfi112:
	.cfi_offset %r14, -32
.Lcfi113:
	.cfi_offset %r15, -24
.Lcfi114:
	.cfi_offset %rbp, -16
	movq	%rsi, %r11
	cmpq	%rdx, %r11
	je	.LBB8_27
# BB#1:                                 # %.lr.ph156
	movl	%r8d, %r10d
	orl	$2, %r10d
	movl	%r8d, %r15d
	orl	$1, %r15d
	movzbl	%r8b, %eax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movl	$2415919104, %r12d      # imm = 0x90000000
	movq	%r11, %rsi
	.p2align	4, 0x90
.LBB8_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_19 Depth 2
	movq	8(%rdi), %rbx
	movq	(%rbx,%rsi,8), %rax
	movl	%eax, %ebp
	andl	$-134217728, %ebp       # imm = 0xF8000000
	addq	$-134217728, %rbp       # imm = 0xF8000000
	shrq	$27, %rbp
	decq	%rbp
	cmpq	$18, %rbp
	ja	.LBB8_26
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	jmpq	*.LJTI8_0(,%rbp,8)
.LBB8_24:                               #   in Loop: Header=BB8_2 Depth=1
	movb	(%r9,%r11), %al
	jmp	.LBB8_25
.LBB8_16:                               #   in Loop: Header=BB8_2 Depth=1
	movb	(%r9,%r11), %bl
	orb	%bl, 1(%r9,%r11)
	andl	$134217727, %eax        # imm = 0x7FFFFFF
	addq	%r11, %rax
	orb	%bl, (%r9,%rax)
	jmp	.LBB8_26
.LBB8_4:                                #   in Loop: Header=BB8_2 Depth=1
	movsbl	%al, %eax
	cmpl	%r8d, %eax
	je	.LBB8_5
	jmp	.LBB8_26
.LBB8_6:                                #   in Loop: Header=BB8_2 Depth=1
	cmpl	$131, %r10d
	je	.LBB8_5
	jmp	.LBB8_26
.LBB8_7:                                #   in Loop: Header=BB8_2 Depth=1
	cmpl	$131, %r15d
	je	.LBB8_5
	jmp	.LBB8_26
.LBB8_10:                               #   in Loop: Header=BB8_2 Depth=1
	cmpl	$127, %r8d
	jle	.LBB8_5
	jmp	.LBB8_26
.LBB8_11:                               #   in Loop: Header=BB8_2 Depth=1
	cmpl	$127, %r8d
	jg	.LBB8_26
# BB#12:                                #   in Loop: Header=BB8_2 Depth=1
	andl	$134217727, %eax        # imm = 0x7FFFFFF
	movq	24(%rdi), %rbx
	shlq	$5, %rax
	movq	(%rbx,%rax), %rbp
	movb	8(%rbx,%rax), %al
	movq	-8(%rsp), %rbx          # 8-byte Reload
	testb	(%rbp,%rbx), %al
	jne	.LBB8_5
	jmp	.LBB8_26
.LBB8_13:                               #   in Loop: Header=BB8_2 Depth=1
	movb	(%r9,%r11), %bl
	orb	%bl, 1(%r9,%r11)
	andl	$134217727, %eax        # imm = 0x7FFFFFF
	movq	%r11, %rbp
	subq	%rax, %rbp
	movb	(%r9,%rbp), %r14b
	orb	%r14b, %bl
	movb	%bl, (%r9,%rbp)
	testb	%r14b, %r14b
	jne	.LBB8_26
# BB#14:                                #   in Loop: Header=BB8_2 Depth=1
	testb	%bl, %bl
	je	.LBB8_26
# BB#15:                                #   in Loop: Header=BB8_2 Depth=1
	decq	%rsi
	subq	%rax, %rsi
	movq	%rsi, %r11
	jmp	.LBB8_26
.LBB8_17:                               #   in Loop: Header=BB8_2 Depth=1
	movb	(%r9,%r11), %r13b
	testb	%r13b, %r13b
	je	.LBB8_26
# BB#18:                                # %.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	8(%rbx,%rsi,8), %rbp
	movl	%ebp, %eax
	andl	$-134217728, %eax       # imm = 0xF8000000
	movl	$1, %r14d
	cmpq	%r12, %rax
	je	.LBB8_21
	.p2align	4, 0x90
.LBB8_19:                               # %.lr.ph
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	andl	$134217727, %ebp        # imm = 0x7FFFFFF
	addq	%rbp, %r14
	leaq	(%r14,%rsi), %rax
	movq	(%rbx,%rax,8), %rbp
	movl	%ebp, %eax
	andl	$-134217728, %eax       # imm = 0xF8000000
	cmpq	%r12, %rax
	jne	.LBB8_19
.LBB8_21:                               # %._crit_edge
                                        #   in Loop: Header=BB8_2 Depth=1
	addq	%r11, %r14
	orb	%r13b, (%r9,%r14)
	jmp	.LBB8_26
.LBB8_22:                               #   in Loop: Header=BB8_2 Depth=1
	movb	(%r9,%r11), %r14b
	orb	%r14b, 1(%r9,%r11)
	movq	8(%rdi), %rbp
	andl	$134217727, %eax        # imm = 0x7FFFFFF
	leaq	(%rax,%rsi), %rbx
	movq	(%rbp,%rbx,8), %rbx
	movl	$4160749568, %ebp       # imm = 0xF8000000
	andq	%rbp, %rbx
	cmpq	%r12, %rbx
	je	.LBB8_26
# BB#23:                                #   in Loop: Header=BB8_2 Depth=1
	addq	%r11, %rax
	orb	%r14b, (%r9,%rax)
	jmp	.LBB8_26
.LBB8_8:                                #   in Loop: Header=BB8_2 Depth=1
	cmpl	$133, %r8d
	je	.LBB8_5
	jmp	.LBB8_26
.LBB8_9:                                #   in Loop: Header=BB8_2 Depth=1
	cmpl	$134, %r8d
	jne	.LBB8_26
.LBB8_5:                                #   in Loop: Header=BB8_2 Depth=1
	movb	(%rcx,%r11), %al
.LBB8_25:                               #   in Loop: Header=BB8_2 Depth=1
	orb	%al, 1(%r9,%r11)
.LBB8_26:                               #   in Loop: Header=BB8_2 Depth=1
	incq	%rsi
	incq	%r11
	cmpq	%rdx, %rsi
	jne	.LBB8_2
.LBB8_27:                               # %._crit_edge157
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	lstep, .Lfunc_end8-lstep
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_4
	.quad	.LBB8_6
	.quad	.LBB8_7
	.quad	.LBB8_10
	.quad	.LBB8_11
	.quad	.LBB8_24
	.quad	.LBB8_24
	.quad	.LBB8_24
	.quad	.LBB8_13
	.quad	.LBB8_16
	.quad	.LBB8_24
	.quad	.LBB8_24
	.quad	.LBB8_24
	.quad	.LBB8_16
	.quad	.LBB8_17
	.quad	.LBB8_22
	.quad	.LBB8_24
	.quad	.LBB8_8
	.quad	.LBB8_9


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
