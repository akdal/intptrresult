	.text
	.file	"UpdatePair.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	73                      # 0x49
	.long	110                     # 0x6e
	.long	116                     # 0x74
	.long	101                     # 0x65
.LCPI0_1:
	.long	114                     # 0x72
	.long	110                     # 0x6e
	.long	97                      # 0x61
	.long	108                     # 0x6c
.LCPI0_2:
	.long	32                      # 0x20
	.long	102                     # 0x66
	.long	105                     # 0x69
	.long	108                     # 0x6c
.LCPI0_3:
	.long	101                     # 0x65
	.long	32                      # 0x20
	.long	110                     # 0x6e
	.long	97                      # 0x61
.LCPI0_4:
	.long	109                     # 0x6d
	.long	101                     # 0x65
	.long	32                      # 0x20
	.long	99                      # 0x63
.LCPI0_5:
	.long	111                     # 0x6f
	.long	108                     # 0x6c
	.long	108                     # 0x6c
	.long	105                     # 0x69
.LCPI0_6:
	.long	115                     # 0x73
	.long	105                     # 0x69
	.long	111                     # 0x6f
	.long	110                     # 0x6e
.LCPI0_7:
	.long	32                      # 0x20
	.long	40                      # 0x28
	.long	102                     # 0x66
	.long	105                     # 0x69
.LCPI0_8:
	.long	108                     # 0x6c
	.long	101                     # 0x65
	.long	32                      # 0x20
	.long	111                     # 0x6f
.LCPI0_9:
	.long	110                     # 0x6e
	.long	32                      # 0x20
	.long	100                     # 0x64
	.long	105                     # 0x69
.LCPI0_10:
	.long	115                     # 0x73
	.long	107                     # 0x6b
	.long	44                      # 0x2c
	.long	32                      # 0x20
.LCPI0_11:
	.long	102                     # 0x66
	.long	105                     # 0x69
	.long	108                     # 0x6c
	.long	101                     # 0x65
.LCPI0_12:
	.long	32                      # 0x20
	.long	105                     # 0x69
	.long	110                     # 0x6e
	.long	32                      # 0x20
.LCPI0_13:
	.long	97                      # 0x61
	.long	114                     # 0x72
	.long	99                      # 0x63
	.long	104                     # 0x68
.LCPI0_14:
	.long	105                     # 0x69
	.long	118                     # 0x76
	.long	101                     # 0x65
	.long	41                      # 0x29
.LCPI0_15:
	.zero	16
	.text
	.globl	_Z21GetUpdatePairInfoListRK9CDirItemsRK13CObjectVectorI8CArcItemEN13NFileTimeType5EEnumER13CRecordVectorI11CUpdatePairE
	.p2align	4, 0x90
	.type	_Z21GetUpdatePairInfoListRK9CDirItemsRK13CObjectVectorI8CArcItemEN13NFileTimeType5EEnumER13CRecordVectorI11CUpdatePairE,@function
_Z21GetUpdatePairInfoListRK9CDirItemsRK13CObjectVectorI8CArcItemEN13NFileTimeType5EEnumER13CRecordVectorI11CUpdatePairE: # @_Z21GetUpdatePairInfoListRK9CDirItemsRK13CObjectVectorI8CArcItemEN13NFileTimeType5EEnumER13CRecordVectorI11CUpdatePairE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 272
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	%edx, 116(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	movups	%xmm0, 192(%rsp)
	movq	$4, 208(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 184(%rsp)
	movups	%xmm0, 160(%rsp)
	movq	$4, 176(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 152(%rsp)
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	movslq	108(%rdi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movl	12(%rsi), %esi
	movups	%xmm0, 32(%rsp)
	movq	$8, 48(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 24(%rsp)
.Ltmp0:
	leaq	24(%rsp), %rdi
	movl	%esi, 16(%rsp)          # 4-byte Spill
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp1:
# BB#1:                                 # %.preheader235
	movslq	16(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 96(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	jle	.LBB0_12
# BB#2:                                 # %.lr.ph264
	xorl	%r14d, %r14d
	leaq	24(%rsp), %r12
	.p2align	4, 0x90
.LBB0_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_9 Depth 2
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%r14,8), %r13
.Ltmp2:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp3:
# BB#4:                                 # %.noexc
                                        #   in Loop: Header=BB0_3 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movslq	24(%r13), %r15
	leaq	1(%r15), %rbx
	testl	%ebx, %ebx
	je	.LBB0_7
# BB#5:                                 # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp4:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp5:
# BB#6:                                 # %.noexc.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, (%rbp)
	movl	$0, (%rax)
	movl	%ebx, 12(%rbp)
	jmp	.LBB0_8
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_3 Depth=1
	xorl	%eax, %eax
.LBB0_8:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	16(%r13), %rcx
	.p2align	4, 0x90
.LBB0_9:                                #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB0_9
# BB#10:                                #   in Loop: Header=BB0_3 Depth=1
	movl	%r15d, 8(%rbp)
.Ltmp7:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp8:
# BB#11:                                #   in Loop: Header=BB0_3 Depth=1
	movq	40(%rsp), %rax
	movslq	36(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 36(%rsp)
	incq	%r14
	cmpq	96(%rsp), %r14          # 8-byte Folded Reload
	jl	.LBB0_3
.LBB0_12:                               # %._crit_edge265
.Ltmp10:
	leaq	24(%rsp), %rdi
	leaq	152(%rsp), %rsi
	callq	_Z13SortFileNamesRK13CObjectVectorI11CStringBaseIwEER13CRecordVectorIiE
.Ltmp11:
# BB#13:
.Ltmp12:
	leaq	24(%rsp), %rdi
	leaq	152(%rsp), %rsi
	callq	_ZL19TestDuplicateStringRK13CObjectVectorI11CStringBaseIwEERK13CRecordVectorIiE
.Ltmp13:
# BB#14:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 24(%rsp)
.Ltmp23:
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp24:
# BB#15:
.Ltmp29:
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp30:
# BB#16:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 72(%rsp)
	movq	$8, 88(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 64(%rsp)
.Ltmp32:
	leaq	64(%rsp), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp33:
# BB#17:                                # %.preheader234
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB0_31
# BB#18:                                # %.lr.ph261
	xorl	%r12d, %r12d
	leaq	64(%rsp), %rbp
	movq	$-1, %r13
	.p2align	4, 0x90
.LBB0_19:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_26 Depth 2
.Ltmp34:
	leaq	24(%rsp), %rdi
	movq	144(%rsp), %rsi         # 8-byte Reload
	movl	%r12d, %edx
	callq	_ZNK9CDirItems10GetLogPathEi
.Ltmp35:
# BB#20:                                #   in Loop: Header=BB0_19 Depth=1
.Ltmp37:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp38:
# BB#21:                                # %.noexc158
                                        #   in Loop: Header=BB0_19 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movslq	32(%rsp), %r15
	leaq	1(%r15), %rbx
	testl	%ebx, %ebx
	je	.LBB0_24
# BB#22:                                # %._crit_edge16.i.i.i153
                                        #   in Loop: Header=BB0_19 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	cmovoq	%r13, %rax
.Ltmp39:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp40:
# BB#23:                                # %.noexc.i154
                                        #   in Loop: Header=BB0_19 Depth=1
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%ebx, 12(%r14)
	jmp	.LBB0_25
	.p2align	4, 0x90
.LBB0_24:                               #   in Loop: Header=BB0_19 Depth=1
	xorl	%eax, %eax
.LBB0_25:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i155
                                        #   in Loop: Header=BB0_19 Depth=1
	movq	24(%rsp), %rcx
	.p2align	4, 0x90
.LBB0_26:                               #   Parent Loop BB0_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB0_26
# BB#27:                                #   in Loop: Header=BB0_19 Depth=1
	movl	%r15d, 8(%r14)
.Ltmp42:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp43:
# BB#28:                                #   in Loop: Header=BB0_19 Depth=1
	movq	80(%rsp), %rax
	movslq	76(%rsp), %rcx
	movq	%r14, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 76(%rsp)
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_30
# BB#29:                                #   in Loop: Header=BB0_19 Depth=1
	callq	_ZdaPv
.LBB0_30:                               # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB0_19 Depth=1
	incl	%r12d
	cmpl	8(%rsp), %r12d          # 4-byte Folded Reload
	jl	.LBB0_19
.LBB0_31:                               # %._crit_edge262
.Ltmp45:
	leaq	64(%rsp), %rdi
	leaq	184(%rsp), %rsi
	callq	_Z13SortFileNamesRK13CObjectVectorI11CStringBaseIwEER13CRecordVectorIiE
.Ltmp46:
# BB#32:
.Ltmp47:
	leaq	64(%rsp), %rdi
	leaq	184(%rsp), %rsi
	callq	_ZL19TestDuplicateStringRK13CObjectVectorI11CStringBaseIwEERK13CRecordVectorIiE
.Ltmp48:
# BB#33:                                # %.preheader233
	xorl	%r13d, %r13d
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movq	56(%rsp), %r12          # 8-byte Reload
	jle	.LBB0_68
# BB#34:                                # %.preheader233
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movl	$0, %ebx
	jle	.LBB0_70
# BB#35:                                # %.lr.ph258
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_36:                               # =>This Inner Loop Header: Depth=1
	movq	200(%rsp), %rax
	movslq	%ebx, %rcx
	movl	(%rax,%rcx,4), %r15d
	movq	168(%rsp), %rax
	movl	%r13d, 20(%rsp)         # 4-byte Spill
	movslq	%r13d, %rcx
	movslq	(%rax,%rcx,4), %r14
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	112(%rax), %rax
	movslq	%r15d, %r12
	movq	(%rax,%r12,8), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%r14,8), %r13
	movq	80(%rsp), %rax
	movq	(%rax,%r12,8), %rdi
	leaq	16(%r13), %rbp
.Ltmp50:
	movq	%rbp, %rsi
	callq	_Z16CompareFileNamesRK11CStringBaseIwES2_
.Ltmp51:
# BB#37:                                #   in Loop: Header=BB0_36 Depth=1
	testl	%eax, %eax
	js	.LBB0_40
# BB#38:                                #   in Loop: Header=BB0_36 Depth=1
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movzbl	35(%r13), %ebx
	je	.LBB0_41
# BB#39:                                #   in Loop: Header=BB0_36 Depth=1
	incl	20(%rsp)                # 4-byte Folded Spill
	movl	$-1, %r15d
	movq	56(%rsp), %r12          # 8-byte Reload
	jmp	.LBB0_65
	.p2align	4, 0x90
.LBB0_40:                               #   in Loop: Header=BB0_36 Depth=1
	incl	%ebx
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movl	$-1, %r14d
	movl	$2, %ebx
	movq	56(%rsp), %r12          # 8-byte Reload
	jmp	.LBB0_65
	.p2align	4, 0x90
.LBB0_41:                               #   in Loop: Header=BB0_36 Depth=1
	testb	%bl, %bl
	jne	.LBB0_45
# BB#42:                                # %.preheader232.preheader
                                        #   in Loop: Header=BB0_36 Depth=1
.Ltmp53:
	movl	$248, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp54:
# BB#43:                                # %.noexc164
                                        #   in Loop: Header=BB0_36 Depth=1
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [73,110,116,101]
	movups	%xmm0, (%rbx)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [114,110,97,108]
	movups	%xmm0, 16(%rbx)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [32,102,105,108]
	movups	%xmm0, 32(%rbx)
	movaps	.LCPI0_3(%rip), %xmm0   # xmm0 = [101,32,110,97]
	movups	%xmm0, 48(%rbx)
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [109,101,32,99]
	movups	%xmm0, 64(%rbx)
	movaps	.LCPI0_5(%rip), %xmm0   # xmm0 = [111,108,108,105]
	movups	%xmm0, 80(%rbx)
	movaps	.LCPI0_6(%rip), %xmm0   # xmm0 = [115,105,111,110]
	movups	%xmm0, 96(%rbx)
	movaps	.LCPI0_7(%rip), %xmm0   # xmm0 = [32,40,102,105]
	movups	%xmm0, 112(%rbx)
	movaps	.LCPI0_8(%rip), %xmm0   # xmm0 = [108,101,32,111]
	movups	%xmm0, 128(%rbx)
	movaps	.LCPI0_9(%rip), %xmm0   # xmm0 = [110,32,100,105]
	movups	%xmm0, 144(%rbx)
	movaps	.LCPI0_10(%rip), %xmm0  # xmm0 = [115,107,44,32]
	movups	%xmm0, 160(%rbx)
	movaps	.LCPI0_11(%rip), %xmm0  # xmm0 = [102,105,108,101]
	movups	%xmm0, 176(%rbx)
	movaps	.LCPI0_12(%rip), %xmm0  # xmm0 = [32,105,110,32]
	movups	%xmm0, 192(%rbx)
	movaps	.LCPI0_13(%rip), %xmm0  # xmm0 = [97,114,99,104]
	movups	%xmm0, 208(%rbx)
	movaps	.LCPI0_14(%rip), %xmm0  # xmm0 = [105,118,101,41]
	movups	%xmm0, 224(%rbx)
	movq	$58, 240(%rbx)
	movq	80(%rsp), %rax
	movq	(%rax,%r12,8), %rdx
.Ltmp56:
	movl	$61, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rcx
	callq	_ZL10ThrowErrorRK11CStringBaseIwES2_S2_
.Ltmp57:
# BB#44:                                # %_ZN11CStringBaseIwED2Ev.exit165
                                        #   in Loop: Header=BB0_36 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB0_45:                               #   in Loop: Header=BB0_36 Depth=1
	cmpb	$0, 34(%r13)
	movq	56(%rsp), %r12          # 8-byte Reload
	je	.LBB0_60
# BB#46:                                #   in Loop: Header=BB0_36 Depth=1
	movl	40(%r13), %eax
	cmpl	$-1, %eax
	cmovel	116(%rsp), %eax         # 4-byte Folded Reload
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	24(%rcx), %rdi
	leaq	8(%r13), %rbp
	cmpl	$2, %eax
	je	.LBB0_50
# BB#47:                                #   in Loop: Header=BB0_36 Depth=1
	cmpl	$1, %eax
	je	.LBB0_53
# BB#48:                                #   in Loop: Header=BB0_36 Depth=1
	testl	%eax, %eax
	jne	.LBB0_83
# BB#49:                                #   in Loop: Header=BB0_36 Depth=1
.Ltmp67:
	movq	%rbp, %rsi
	callq	CompareFileTime
.Ltmp68:
	jmp	.LBB0_57
.LBB0_50:                               #   in Loop: Header=BB0_36 Depth=1
.Ltmp59:
	leaq	124(%rsp), %rsi
	callq	_ZN8NWindows5NTime17FileTimeToDosTimeERK9_FILETIMERj
.Ltmp60:
# BB#51:                                # %.noexc170
                                        #   in Loop: Header=BB0_36 Depth=1
.Ltmp61:
	movq	%rbp, %rdi
	leaq	120(%rsp), %rsi
	callq	_ZN8NWindows5NTime17FileTimeToDosTimeERK9_FILETIMERj
.Ltmp62:
# BB#52:                                # %.noexc171
                                        #   in Loop: Header=BB0_36 Depth=1
	movl	124(%rsp), %ecx
	xorl	%eax, %eax
	cmpl	120(%rsp), %ecx
	jmp	.LBB0_56
.LBB0_53:                               #   in Loop: Header=BB0_36 Depth=1
.Ltmp63:
	leaq	132(%rsp), %rsi
	callq	_ZN8NWindows5NTime18FileTimeToUnixTimeERK9_FILETIMERj
.Ltmp64:
# BB#54:                                # %.noexc168
                                        #   in Loop: Header=BB0_36 Depth=1
.Ltmp65:
	movq	%rbp, %rdi
	leaq	128(%rsp), %rsi
	callq	_ZN8NWindows5NTime18FileTimeToUnixTimeERK9_FILETIMERj
.Ltmp66:
# BB#55:                                # %.noexc169
                                        #   in Loop: Header=BB0_36 Depth=1
	movl	132(%rsp), %ecx
	xorl	%eax, %eax
	cmpl	128(%rsp), %ecx
.LBB0_56:                               # %_ZL13MyCompareTimeN13NFileTimeType5EEnumERK9_FILETIMES3_.exit
                                        #   in Loop: Header=BB0_36 Depth=1
	setne	%al
	movl	$-1, %ecx
	cmovbl	%ecx, %eax
.LBB0_57:                               # %_ZL13MyCompareTimeN13NFileTimeType5EEnumERK9_FILETIMES3_.exit
                                        #   in Loop: Header=BB0_36 Depth=1
	cmpl	$-1, %eax
	je	.LBB0_63
# BB#58:                                # %_ZL13MyCompareTimeN13NFileTimeType5EEnumERK9_FILETIMES3_.exit
                                        #   in Loop: Header=BB0_36 Depth=1
	cmpl	$1, %eax
	jne	.LBB0_60
# BB#59:                                #   in Loop: Header=BB0_36 Depth=1
	movl	$4, %ebx
	jmp	.LBB0_64
.LBB0_60:                               # %_ZL13MyCompareTimeN13NFileTimeType5EEnumERK9_FILETIMES3_.exit.thread
                                        #   in Loop: Header=BB0_36 Depth=1
	cmpb	$0, 33(%r13)
	je	.LBB0_62
# BB#61:                                #   in Loop: Header=BB0_36 Depth=1
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	xorl	%ebx, %ebx
	cmpq	(%r13), %rax
	setne	%bl
	addq	$5, %rbx
	jmp	.LBB0_64
.LBB0_62:                               #   in Loop: Header=BB0_36 Depth=1
	movl	$6, %ebx
	jmp	.LBB0_64
.LBB0_63:                               #   in Loop: Header=BB0_36 Depth=1
	movl	$3, %ebx
.LBB0_64:                               # %.thread
                                        #   in Loop: Header=BB0_36 Depth=1
	incl	4(%rsp)                 # 4-byte Folded Spill
	incl	20(%rsp)                # 4-byte Folded Spill
.LBB0_65:                               #   in Loop: Header=BB0_36 Depth=1
.Ltmp72:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp73:
# BB#66:                                #   in Loop: Header=BB0_36 Depth=1
	shlq	$32, %r14
	orq	%rbx, %r14
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movq	%r14, (%rax,%rcx,4)
	movl	%r15d, 8(%rax,%rcx,4)
	incl	12(%r12)
	movl	20(%rsp), %r13d         # 4-byte Reload
	cmpl	16(%rsp), %r13d         # 4-byte Folded Reload
	jge	.LBB0_69
# BB#67:                                #   in Loop: Header=BB0_36 Depth=1
	movl	4(%rsp), %ebx           # 4-byte Reload
	cmpl	8(%rsp), %ebx           # 4-byte Folded Reload
	jl	.LBB0_36
	jmp	.LBB0_70
.LBB0_68:
	xorl	%ebx, %ebx
	cmpl	8(%rsp), %ebx           # 4-byte Folded Reload
	jl	.LBB0_71
	jmp	.LBB0_74
.LBB0_69:
	movl	4(%rsp), %ebx           # 4-byte Reload
.LBB0_70:                               # %.critedge.preheader
	cmpl	8(%rsp), %ebx           # 4-byte Folded Reload
	jge	.LBB0_74
.LBB0_71:                               # %.lr.ph253
	movslq	%ebx, %rbx
	movabsq	$-4294967294, %r14      # imm = 0xFFFFFFFF00000002
	.p2align	4, 0x90
.LBB0_72:                               # =>This Inner Loop Header: Depth=1
	movq	200(%rsp), %rax
	movl	(%rax,%rbx,4), %ebp
.Ltmp75:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp76:
# BB#73:                                # %.critedge
                                        #   in Loop: Header=BB0_72 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movq	%r14, (%rax,%rcx,4)
	movl	%ebp, 8(%rax,%rcx,4)
	incl	12(%r12)
	incq	%rbx
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	jl	.LBB0_72
.LBB0_74:                               # %.preheader
	cmpl	96(%rsp), %r13d         # 4-byte Folded Reload
	jge	.LBB0_78
# BB#75:                                # %.lr.ph
	movslq	%r13d, %rbx
	.p2align	4, 0x90
.LBB0_76:                               # =>This Inner Loop Header: Depth=1
	movq	168(%rsp), %rax
	movslq	(%rax,%rbx,4), %rbp
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movzbl	35(%rax), %r14d
.Ltmp78:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp79:
# BB#77:                                #   in Loop: Header=BB0_76 Depth=1
	movl	%ebp, %eax
	shlq	$32, %rax
	orq	%r14, %rax
	movq	16(%r12), %rcx
	movslq	12(%r12), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	movq	%rax, (%rcx,%rdx,4)
	movl	$-1, 8(%rcx,%rdx,4)
	incl	12(%r12)
	incq	%rbx
	cmpq	96(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB0_76
.LBB0_78:                               # %._crit_edge
.Ltmp81:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
.Ltmp82:
# BB#79:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 64(%rsp)
.Ltmp92:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp93:
# BB#80:
.Ltmp98:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp99:
# BB#81:                                # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit141
.Ltmp103:
	leaq	152(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp104:
# BB#82:
	leaq	184(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_83:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$4191618, (%rax)        # imm = 0x3FF582
.Ltmp69:
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp70:
# BB#84:                                # %.noexc172
.LBB0_85:                               # %.loopexit.split-lp
.Ltmp71:
	jmp	.LBB0_105
.LBB0_86:
.Ltmp105:
	movq	%rax, %r15
	jmp	.LBB0_118
.LBB0_87:
.Ltmp100:
	movq	%rax, %r15
	jmp	.LBB0_117
.LBB0_88:
.Ltmp94:
	movq	%rax, %r15
.Ltmp95:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp96:
	jmp	.LBB0_117
.LBB0_89:
.Ltmp97:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_90:
.Ltmp83:
	jmp	.LBB0_105
.LBB0_91:
.Ltmp31:
	movq	%rax, %r15
	jmp	.LBB0_117
.LBB0_92:
.Ltmp25:
	movq	%rax, %r15
.Ltmp26:
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp27:
	jmp	.LBB0_117
.LBB0_93:
.Ltmp28:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_94:                               # %_ZN11CStringBaseIwED2Ev.exit166
.Ltmp58:
	movq	%rax, %r15
	movq	%rbx, %rdi
	jmp	.LBB0_108
.LBB0_95:
.Ltmp55:
	jmp	.LBB0_105
.LBB0_96:
.Ltmp49:
	jmp	.LBB0_105
.LBB0_97:
.Ltmp14:
	jmp	.LBB0_114
.LBB0_98:
.Ltmp41:
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	_ZdlPv
	jmp	.LBB0_107
.LBB0_99:
.Ltmp6:
	movq	%rax, %r15
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB0_115
.LBB0_100:
.Ltmp80:
	jmp	.LBB0_105
.LBB0_101:
.Ltmp52:
	jmp	.LBB0_105
.LBB0_102:
.Ltmp77:
	jmp	.LBB0_105
.LBB0_103:                              # %.loopexit
.Ltmp74:
	jmp	.LBB0_105
.LBB0_104:
.Ltmp36:
.LBB0_105:
	movq	%rax, %r15
	jmp	.LBB0_109
.LBB0_106:
.Ltmp44:
	movq	%rax, %r15
.LBB0_107:                              # %.body160
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_109
.LBB0_108:
	callq	_ZdaPv
.LBB0_109:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 64(%rsp)
.Ltmp84:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp85:
# BB#110:
.Ltmp90:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp91:
	jmp	.LBB0_117
.LBB0_111:
.Ltmp86:
	movq	%rax, %rbx
.Ltmp87:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp88:
	jmp	.LBB0_123
.LBB0_112:
.Ltmp89:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_113:
.Ltmp9:
.LBB0_114:                              # %.body143
	movq	%rax, %r15
.LBB0_115:                              # %.body143
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 24(%rsp)
.Ltmp15:
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp16:
# BB#116:
.Ltmp21:
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp22:
.LBB0_117:
.Ltmp101:
	leaq	152(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp102:
.LBB0_118:
.Ltmp106:
	leaq	184(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp107:
# BB#119:
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB0_120:
.Ltmp17:
	movq	%rax, %rbx
.Ltmp18:
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp19:
	jmp	.LBB0_123
.LBB0_121:
.Ltmp20:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_122:
.Ltmp108:
	movq	%rax, %rbx
.LBB0_123:                              # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_Z21GetUpdatePairInfoListRK9CDirItemsRK13CObjectVectorI8CArcItemEN13NFileTimeType5EEnumER13CRecordVectorI11CUpdatePairE, .Lfunc_end0-_Z21GetUpdatePairInfoListRK9CDirItemsRK13CObjectVectorI8CArcItemEN13NFileTimeType5EEnumER13CRecordVectorI11CUpdatePairE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\203"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\272\003"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp2           #   Call between .Ltmp2 and .Ltmp3
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp5-.Ltmp4           #   Call between .Ltmp4 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp13-.Ltmp10         #   Call between .Ltmp10 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin0   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp49-.Lfunc_begin0   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin0   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp44-.Lfunc_begin0   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin0   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin0   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp48-.Ltmp45         #   Call between .Ltmp45 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin0   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin0   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin0   #     jumps to .Ltmp55
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin0   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp73-.Ltmp67         #   Call between .Ltmp67 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin0   #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp76-.Ltmp75         #   Call between .Ltmp75 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin0   #     jumps to .Ltmp77
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin0   #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin0   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin0   #     jumps to .Ltmp94
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin0  #     jumps to .Ltmp100
	.byte	0                       #   On action: cleanup
	.long	.Ltmp103-.Lfunc_begin0  # >> Call Site 23 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin0  #     jumps to .Ltmp105
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin0  # >> Call Site 24 <<
	.long	.Ltmp69-.Ltmp104        #   Call between .Ltmp104 and .Ltmp69
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin0   # >> Call Site 25 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin0   #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.long	.Ltmp95-.Lfunc_begin0   # >> Call Site 26 <<
	.long	.Ltmp96-.Ltmp95         #   Call between .Ltmp95 and .Ltmp96
	.long	.Ltmp97-.Lfunc_begin0   #     jumps to .Ltmp97
	.byte	1                       #   On action: 1
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 27 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	1                       #   On action: 1
	.long	.Ltmp84-.Lfunc_begin0   # >> Call Site 28 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin0   #     jumps to .Ltmp86
	.byte	1                       #   On action: 1
	.long	.Ltmp90-.Lfunc_begin0   # >> Call Site 29 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp108-.Lfunc_begin0  #     jumps to .Ltmp108
	.byte	1                       #   On action: 1
	.long	.Ltmp87-.Lfunc_begin0   # >> Call Site 30 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin0   #     jumps to .Ltmp89
	.byte	1                       #   On action: 1
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 31 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 32 <<
	.long	.Ltmp107-.Ltmp21        #   Call between .Ltmp21 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin0  #     jumps to .Ltmp108
	.byte	1                       #   On action: 1
	.long	.Ltmp107-.Lfunc_begin0  # >> Call Site 33 <<
	.long	.Ltmp18-.Ltmp107        #   Call between .Ltmp107 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 34 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	68                      # 0x44
	.long	117                     # 0x75
	.long	112                     # 0x70
	.long	108                     # 0x6c
.LCPI1_1:
	.long	105                     # 0x69
	.long	99                      # 0x63
	.long	97                      # 0x61
	.long	116                     # 0x74
.LCPI1_2:
	.long	101                     # 0x65
	.long	32                      # 0x20
	.long	102                     # 0x66
	.long	105                     # 0x69
.LCPI1_3:
	.long	108                     # 0x6c
	.long	101                     # 0x65
	.long	110                     # 0x6e
	.long	97                      # 0x61
.LCPI1_4:
	.long	109                     # 0x6d
	.long	101                     # 0x65
	.long	58                      # 0x3a
	.long	0                       # 0x0
	.text
	.p2align	4, 0x90
	.type	_ZL19TestDuplicateStringRK13CObjectVectorI11CStringBaseIwEERK13CRecordVectorIiE,@function
_ZL19TestDuplicateStringRK13CObjectVectorI11CStringBaseIwEERK13CRecordVectorIiE: # @_ZL19TestDuplicateStringRK13CObjectVectorI11CStringBaseIwEERK13CRecordVectorIiE
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r12, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	cmpl	$2, 12(%r15)
	jl	.LBB1_6
# BB#1:                                 # %.lr.ph
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rax
	movq	16(%r14), %rcx
	movslq	-4(%rax,%rbx,4), %rdx
	movq	(%rcx,%rdx,8), %rdi
	movslq	(%rax,%rbx,4), %rax
	movq	(%rcx,%rax,8), %rsi
	callq	_Z16CompareFileNamesRK11CStringBaseIwES2_
	testl	%eax, %eax
	jne	.LBB1_5
# BB#3:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	$80, %edi
	callq	_Znam
	movq	%rax, %r12
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [68,117,112,108]
	movups	%xmm0, (%r12)
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [105,99,97,116]
	movups	%xmm0, 16(%r12)
	movaps	.LCPI1_2(%rip), %xmm0   # xmm0 = [101,32,102,105]
	movups	%xmm0, 32(%r12)
	movaps	.LCPI1_3(%rip), %xmm0   # xmm0 = [108,101,110,97]
	movups	%xmm0, 48(%r12)
	movaps	.LCPI1_4(%rip), %xmm0   # xmm0 = [109,101,58,0]
	movups	%xmm0, 64(%r12)
	movq	16(%r15), %rax
	movq	16(%r14), %rcx
	movslq	-4(%rax,%rbx,4), %rdx
	movq	(%rcx,%rdx,8), %rdx
	movslq	(%rax,%rbx,4), %rax
	movq	(%rcx,%rax,8), %rcx
.Ltmp109:
	movl	$19, %esi
	movq	%r12, %rdi
	callq	_ZL10ThrowErrorRK11CStringBaseIwES2_S2_
.Ltmp110:
# BB#4:                                 # %_ZN11CStringBaseIwED2Ev.exit19
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%r12, %rdi
	callq	_ZdaPv
.LBB1_5:                                # %.backedge
                                        #   in Loop: Header=BB1_2 Depth=1
	incq	%rbx
	movslq	12(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB1_2
.LBB1_6:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB1_7:                                # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp111:
	movq	%rax, %r14
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZL19TestDuplicateStringRK13CObjectVectorI11CStringBaseIwEERK13CRecordVectorIiE, .Lfunc_end1-_ZL19TestDuplicateStringRK13CObjectVectorI11CStringBaseIwEERK13CRecordVectorIiE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp109-.Lfunc_begin1  #   Call between .Lfunc_begin1 and .Ltmp109
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin1  # >> Call Site 2 <<
	.long	.Ltmp110-.Ltmp109       #   Call between .Ltmp109 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin1  #     jumps to .Ltmp111
	.byte	0                       #   On action: cleanup
	.long	.Ltmp110-.Lfunc_begin1  # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp110    #   Call between .Ltmp110 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp112:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp113:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB2_2:
.Ltmp114:
	movq	%rax, %r14
.Ltmp115:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp116:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_4:
.Ltmp117:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end2-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp112-.Lfunc_begin2  # >> Call Site 1 <<
	.long	.Ltmp113-.Ltmp112       #   Call between .Ltmp112 and .Ltmp113
	.long	.Ltmp114-.Lfunc_begin2  #     jumps to .Ltmp114
	.byte	0                       #   On action: cleanup
	.long	.Ltmp113-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Ltmp115-.Ltmp113       #   Call between .Ltmp113 and .Ltmp115
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp115-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Ltmp116-.Ltmp115       #   Call between .Ltmp115 and .Ltmp116
	.long	.Ltmp117-.Lfunc_begin2  #     jumps to .Ltmp117
	.byte	1                       #   On action: 1
	.long	.Ltmp116-.Lfunc_begin2  # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp116    #   Call between .Ltmp116 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.section	.text._ZN11CStringBaseIwED2Ev,"axG",@progbits,_ZN11CStringBaseIwED2Ev,comdat
	.weak	_ZN11CStringBaseIwED2Ev
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwED2Ev,@function
_ZN11CStringBaseIwED2Ev:                # @_ZN11CStringBaseIwED2Ev
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB4_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB4_1:
	retq
.Lfunc_end4:
	.size	_ZN11CStringBaseIwED2Ev, .Lfunc_end4-_ZN11CStringBaseIwED2Ev
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZL10ThrowErrorRK11CStringBaseIwES2_S2_,@function
_ZL10ThrowErrorRK11CStringBaseIwES2_S2_: # @_ZL10ThrowErrorRK11CStringBaseIwES2_S2_
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 80
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	%r14d, %r12d
	incl	%r12d
	je	.LBB5_2
# BB#1:                                 # %._crit_edge16.i.i
	movslq	%r12d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movl	$0, (%rax)
	movl	%r12d, %ebp
	movq	%rax, %r13
	jmp	.LBB5_3
.LBB5_2:
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	xorl	%r13d, %r13d
.LBB5_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rdx), %ecx
	movl	%ecx, (%r13,%rdx)
	addq	$4, %rdx
	testl	%ecx, %ecx
	jne	.LBB5_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%ebp, %edi
	subl	%r14d, %edi
	cmpl	$1, %edi
	jle	.LBB5_7
# BB#6:
	movl	%ebp, %ebx
	jmp	.LBB5_18
.LBB5_7:
	leal	-1(%rdi), %ecx
	cmpl	$8, %ebp
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebp
	jl	.LBB5_9
# BB#8:                                 # %select.true.sink
	movl	%ebp, %edx
	shrl	$31, %edx
	addl	%ebp, %edx
	sarl	%edx
.LBB5_9:                                # %select.end
	movl	$2, %esi
	subl	%edi, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rbp,%rsi), %ebx
	cmpl	%ebp, %ebx
	jne	.LBB5_11
# BB#10:
	movl	%ebp, %ebx
	jmp	.LBB5_18
.LBB5_11:
	movq	%rax, (%rsp)            # 8-byte Spill
	movslq	%ebx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp118:
	callq	_Znam
.Ltmp119:
# BB#12:                                # %.noexc
	testl	%ebp, %ebp
	jle	.LBB5_17
# BB#13:                                # %.preheader.i.i
	testl	%r14d, %r14d
	jle	.LBB5_15
# BB#14:                                # %.lr.ph.i.i
	movslq	%r14d, %rdx
	shlq	$2, %rdx
	movq	%rax, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rax, %rbp
	callq	memcpy
	movq	%rbp, %rax
	jmp	.LBB5_16
.LBB5_15:                               # %._crit_edge.i.i
	testq	%r13, %r13
	je	.LBB5_17
.LBB5_16:                               # %._crit_edge.thread.i.i
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rax, %rbp
	callq	_ZdaPv
	movq	%rbp, %rax
.LBB5_17:                               # %._crit_edge16.i.i5
	movslq	%r14d, %rcx
	movl	$0, (%rax,%rcx,4)
	movq	%rax, %r13
.LBB5_18:
	movslq	%r14d, %rcx
	movl	$10, (%r13,%rcx,4)
	movslq	%r12d, %r8
	movl	$0, (%r13,%r8,4)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	8(%rdi), %ebp
	movl	%ebx, %ecx
	subl	%r12d, %ecx
	cmpl	%ebp, %ecx
	movq	%r15, 8(%rsp)           # 8-byte Spill
	jle	.LBB5_20
# BB#19:
	movl	%ebx, %r15d
	jmp	.LBB5_42
.LBB5_20:
	decl	%ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB5_22
# BB#21:                                # %select.true.sink329
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB5_22:                               # %select.end328
	leal	(%rdx,%rcx), %esi
	movl	%ebp, %edi
	subl	%ecx, %edi
	cmpl	%ebp, %esi
	cmovgel	%edx, %edi
	leal	1(%rbx,%rdi), %r15d
	cmpl	%ebx, %r15d
	jne	.LBB5_24
# BB#23:
	movl	%ebx, %r15d
	jmp	.LBB5_41
.LBB5_24:
	movq	%r8, (%rsp)             # 8-byte Spill
	movq	%rax, %rbp
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp121:
	callq	_Znam
.Ltmp122:
# BB#25:                                # %.noexc23
	testl	%ebx, %ebx
	movq	%rbp, %rdi
	movq	(%rsp), %r8             # 8-byte Reload
	jle	.LBB5_40
# BB#26:                                # %.preheader.i.i13
	testl	%r14d, %r14d
	js	.LBB5_38
# BB#27:                                # %.lr.ph.i.i14.preheader
	cmpl	$7, %r12d
	jbe	.LBB5_31
# BB#28:                                # %min.iters.checked
	movq	%r8, %rbp
	andq	$-8, %rbp
	je	.LBB5_31
# BB#29:                                # %vector.memcheck
	leaq	(%r13,%r8,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB5_94
# BB#30:                                # %vector.memcheck
	leaq	(%rax,%r8,4), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB5_94
.LBB5_31:
	xorl	%ebp, %ebp
.LBB5_32:                               # %.lr.ph.i.i14.preheader89
	subl	%ebp, %r12d
	leaq	-1(%r8), %rcx
	subq	%rbp, %rcx
	andq	$7, %r12
	je	.LBB5_35
# BB#33:                                # %.lr.ph.i.i14.prol.preheader
	negq	%r12
	.p2align	4, 0x90
.LBB5_34:                               # %.lr.ph.i.i14.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rbp,4), %edx
	movl	%edx, (%rax,%rbp,4)
	incq	%rbp
	incq	%r12
	jne	.LBB5_34
.LBB5_35:                               # %.lr.ph.i.i14.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB5_39
# BB#36:                                # %.lr.ph.i.i14.preheader89.new
	movq	%r8, %rcx
	subq	%rbp, %rcx
	leaq	28(%rax,%rbp,4), %rdx
	leaq	28(%r13,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB5_37:                               # %.lr.ph.i.i14
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rbp), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rbp), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rbp), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rbp), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rbp), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rbp), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rbp), %esi
	movl	%esi, -4(%rdx)
	movl	(%rbp), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rbp
	addq	$-8, %rcx
	jne	.LBB5_37
	jmp	.LBB5_39
.LBB5_38:                               # %._crit_edge.i.i15
	testq	%r13, %r13
	je	.LBB5_40
.LBB5_39:                               # %._crit_edge.thread.i.i20
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	(%rsp), %r8             # 8-byte Reload
	movq	%rbx, %rax
.LBB5_40:                               # %._crit_edge16.i.i21
	movl	$0, (%rax,%r8,4)
	movq	%rax, %r13
.LBB5_41:                               # %.noexc9
	movq	16(%rsp), %rdi          # 8-byte Reload
.LBB5_42:                               # %.noexc9
	leaq	(%r13,%r8,4), %rsi
	movq	(%rdi), %rcx
	.p2align	4, 0x90
.LBB5_43:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rsi)
	addq	$4, %rsi
	testl	%edx, %edx
	jne	.LBB5_43
# BB#44:
	movslq	8(%rdi), %r12
	addq	%r8, %r12
	movl	%r15d, %edi
	subl	%r12d, %edi
	cmpl	$1, %edi
	jg	.LBB5_48
# BB#45:
	cmpl	$8, %r15d
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %r15d
	jl	.LBB5_47
# BB#46:                                # %select.true.sink351
	movl	%r15d, %ecx
	shrl	$31, %ecx
	addl	%r15d, %ecx
	sarl	%ecx
.LBB5_47:                               # %select.end350
	leal	-1(%rcx,%rdi), %edx
	movl	$2, %esi
	subl	%edi, %esi
	testl	%edx, %edx
	cmovgl	%ecx, %esi
	leal	1(%r15,%rsi), %r14d
	cmpl	%r15d, %r14d
	jne	.LBB5_49
.LBB5_48:
	movl	%r15d, %r14d
	movq	8(%rsp), %r8            # 8-byte Reload
	jmp	.LBB5_66
.LBB5_49:
	movq	%rax, (%rsp)            # 8-byte Spill
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp123:
	callq	_Znam
.Ltmp124:
# BB#50:                                # %.noexc40
	testl	%r15d, %r15d
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	jle	.LBB5_65
# BB#51:                                # %.preheader.i.i30
	testl	%r12d, %r12d
	jle	.LBB5_63
# BB#52:                                # %.lr.ph.i.i31
	cmpl	$7, %r12d
	jbe	.LBB5_56
# BB#53:                                # %min.iters.checked34
	movq	%r12, %rbx
	andq	$-8, %rbx
	je	.LBB5_56
# BB#54:                                # %vector.memcheck47
	leaq	(%r13,%r12,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB5_97
# BB#55:                                # %vector.memcheck47
	leaq	(%rax,%r12,4), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB5_97
.LBB5_56:
	xorl	%ebx, %ebx
.LBB5_57:                               # %scalar.ph32.preheader
	movl	%r12d, %edx
	subl	%ebx, %edx
	leaq	-1(%r12), %rcx
	subq	%rbx, %rcx
	andq	$7, %rdx
	je	.LBB5_60
# BB#58:                                # %scalar.ph32.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB5_59:                               # %scalar.ph32.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rbx,4), %esi
	movl	%esi, (%rax,%rbx,4)
	incq	%rbx
	incq	%rdx
	jne	.LBB5_59
.LBB5_60:                               # %scalar.ph32.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB5_64
# BB#61:                                # %scalar.ph32.preheader.new
	movq	%r12, %rcx
	subq	%rbx, %rcx
	leaq	28(%rax,%rbx,4), %rdx
	leaq	28(%r13,%rbx,4), %rbp
	.p2align	4, 0x90
.LBB5_62:                               # %scalar.ph32
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rbp), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rbp), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rbp), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rbp), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rbp), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rbp), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rbp), %esi
	movl	%esi, -4(%rdx)
	movl	(%rbp), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rbp
	addq	$-8, %rcx
	jne	.LBB5_62
	jmp	.LBB5_64
.LBB5_63:                               # %._crit_edge.i.i32
	testq	%r13, %r13
	je	.LBB5_65
.LBB5_64:                               # %._crit_edge.thread.i.i37
	movq	%rax, %rbx
	movq	%r8, %rbp
	callq	_ZdaPv
	movq	%rbp, %r8
	movq	%rbx, %rax
.LBB5_65:                               # %._crit_edge16.i.i38
	movslq	%r12d, %rcx
	movl	$0, (%rax,%rcx,4)
	movq	%rax, %r13
.LBB5_66:
	movslq	%r12d, %r15
	movl	$10, (%r13,%r15,4)
	leal	1(%r12), %edx
	movl	$0, 4(%r13,%r15,4)
	incq	%r15
	movl	8(%r8), %ebp
	movl	%r14d, %ecx
	subl	%edx, %ecx
	cmpl	%ebp, %ecx
	jg	.LBB5_87
# BB#67:
	decl	%ecx
	cmpl	$8, %r14d
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %r14d
	jl	.LBB5_69
# BB#68:                                # %select.true.sink373
	movl	%r14d, %edx
	shrl	$31, %edx
	addl	%r14d, %edx
	sarl	%edx
.LBB5_69:                               # %select.end372
	leal	(%rdx,%rcx), %esi
	movl	%ebp, %edi
	subl	%ecx, %edi
	cmpl	%ebp, %esi
	cmovgel	%edx, %edi
	leal	1(%r14,%rdi), %ecx
	cmpl	%r14d, %ecx
	je	.LBB5_87
# BB#70:
	movq	%rax, %rbp
	movq	%r8, %rbx
	movslq	%ecx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp126:
	callq	_Znam
.Ltmp127:
# BB#71:                                # %.noexc59
	testl	%r14d, %r14d
	movq	%rbx, %r8
	movq	%rbp, %rdi
	jle	.LBB5_86
# BB#72:                                # %.preheader.i.i49
	testl	%r12d, %r12d
	js	.LBB5_84
# BB#73:                                # %.lr.ph.i.i50.preheader
	cmpl	$7, %r15d
	jbe	.LBB5_77
# BB#74:                                # %min.iters.checked63
	movq	%r15, %rbp
	andq	$-8, %rbp
	je	.LBB5_77
# BB#75:                                # %vector.memcheck76
	leaq	(%r13,%r15,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB5_100
# BB#76:                                # %vector.memcheck76
	leaq	(%rax,%r15,4), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB5_100
.LBB5_77:
	xorl	%ebp, %ebp
.LBB5_78:                               # %.lr.ph.i.i50.preheader88
	movl	%r15d, %edx
	subl	%ebp, %edx
	leaq	-1(%r15), %rcx
	subq	%rbp, %rcx
	andq	$7, %rdx
	je	.LBB5_81
# BB#79:                                # %.lr.ph.i.i50.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB5_80:                               # %.lr.ph.i.i50.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rbp,4), %esi
	movl	%esi, (%rax,%rbp,4)
	incq	%rbp
	incq	%rdx
	jne	.LBB5_80
.LBB5_81:                               # %.lr.ph.i.i50.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB5_85
# BB#82:                                # %.lr.ph.i.i50.preheader88.new
	movq	%r15, %rcx
	subq	%rbp, %rcx
	leaq	28(%rax,%rbp,4), %rdx
	leaq	28(%r13,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB5_83:                               # %.lr.ph.i.i50
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rbp), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rbp), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rbp), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rbp), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rbp), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rbp), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rbp), %esi
	movl	%esi, -4(%rdx)
	movl	(%rbp), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rbp
	addq	$-8, %rcx
	jne	.LBB5_83
	jmp	.LBB5_85
.LBB5_84:                               # %._crit_edge.i.i51
	testq	%r13, %r13
	je	.LBB5_86
.LBB5_85:                               # %._crit_edge.thread.i.i56
	movq	%rax, %rbx
	movq	%r8, %rbp
	callq	_ZdaPv
	movq	%rbp, %r8
	movq	%rbx, %rax
.LBB5_86:                               # %._crit_edge16.i.i57
	movl	$0, (%rax,%r15,4)
	movq	%rax, %r13
.LBB5_87:                               # %.noexc44
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	(%r13,%r15,4), %rax
	movq	(%r8), %rcx
	.p2align	4, 0x90
.LBB5_88:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB5_88
# BB#89:                                # %._crit_edge16.i.i61
	movslq	8(%r8), %rbx
	movslq	%r15d, %r15
	movl	$16, %edi
	callq	__cxa_allocate_exception
	movq	%rax, %rbp
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	leaq	1(%rbx,%r15), %r14
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp129:
	callq	_Znam
.Ltmp130:
# BB#90:                                # %.noexc65
	addq	%r15, %rbx
	movq	%rax, (%rbp)
	movl	$0, (%rax)
	movl	%r14d, 12(%rbp)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_91:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i62
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB5_91
# BB#92:
	movl	%ebx, 8(%rbp)
.Ltmp132:
	movl	$_ZTI11CStringBaseIwE, %esi
	movl	$_ZN11CStringBaseIwED2Ev, %edx
	movq	%rbp, %rdi
	callq	__cxa_throw
.Ltmp133:
# BB#93:
.LBB5_94:                               # %vector.body.preheader
	leaq	-8(%rbp), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB5_103
# BB#95:                                # %vector.body.prol.preheader
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_96:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r13,%rsi,4), %xmm0
	movups	16(%r13,%rsi,4), %xmm1
	movups	%xmm0, (%rax,%rsi,4)
	movups	%xmm1, 16(%rax,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB5_96
	jmp	.LBB5_104
.LBB5_97:                               # %vector.body30.preheader
	leaq	-8(%rbx), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB5_108
# BB#98:                                # %vector.body30.prol.preheader
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_99:                               # %vector.body30.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r13,%rsi,4), %xmm0
	movups	16(%r13,%rsi,4), %xmm1
	movups	%xmm0, (%rax,%rsi,4)
	movups	%xmm1, 16(%rax,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB5_99
	jmp	.LBB5_109
.LBB5_100:                              # %vector.body59.preheader
	leaq	-8(%rbp), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB5_113
# BB#101:                               # %vector.body59.prol.preheader
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_102:                              # %vector.body59.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r13,%rsi,4), %xmm0
	movups	16(%r13,%rsi,4), %xmm1
	movups	%xmm0, (%rax,%rsi,4)
	movups	%xmm1, 16(%rax,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB5_102
	jmp	.LBB5_114
.LBB5_103:
	xorl	%esi, %esi
.LBB5_104:                              # %vector.body.prol.loopexit
	cmpq	$24, %rcx
	jb	.LBB5_107
# BB#105:                               # %vector.body.preheader.new
	movq	%rbp, %rcx
	subq	%rsi, %rcx
	leaq	112(%rax,%rsi,4), %rdx
	leaq	112(%r13,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB5_106:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB5_106
.LBB5_107:                              # %middle.block
	cmpq	%rbp, %r8
	jne	.LBB5_32
	jmp	.LBB5_39
.LBB5_108:
	xorl	%esi, %esi
.LBB5_109:                              # %vector.body30.prol.loopexit
	cmpq	$24, %rcx
	jb	.LBB5_112
# BB#110:                               # %vector.body30.preheader.new
	movq	%rbx, %rcx
	subq	%rsi, %rcx
	leaq	112(%rax,%rsi,4), %rdx
	leaq	112(%r13,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB5_111:                              # %vector.body30
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB5_111
.LBB5_112:                              # %middle.block31
	cmpq	%rbx, %r12
	jne	.LBB5_57
	jmp	.LBB5_64
.LBB5_113:
	xorl	%esi, %esi
.LBB5_114:                              # %vector.body59.prol.loopexit
	cmpq	$24, %rcx
	jb	.LBB5_117
# BB#115:                               # %vector.body59.preheader.new
	movq	%rbp, %rcx
	subq	%rsi, %rcx
	leaq	112(%rax,%rsi,4), %rdx
	leaq	112(%r13,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB5_116:                              # %vector.body59
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB5_116
.LBB5_117:                              # %middle.block60
	cmpq	%rbp, %r15
	jne	.LBB5_78
	jmp	.LBB5_85
.LBB5_118:
.Ltmp125:
	jmp	.LBB5_122
.LBB5_119:
.Ltmp120:
	jmp	.LBB5_122
.LBB5_120:
.Ltmp128:
	movq	%rax, %rbx
	movq	%rbp, (%rsp)            # 8-byte Spill
	testq	%r13, %r13
	jne	.LBB5_124
	jmp	.LBB5_125
.LBB5_121:
.Ltmp134:
.LBB5_122:
	movq	%rax, %rbx
	testq	%r13, %r13
	jne	.LBB5_124
	jmp	.LBB5_125
.LBB5_123:
.Ltmp131:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	__cxa_free_exception
	testq	%r13, %r13
	je	.LBB5_125
.LBB5_124:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZdaPv
.LBB5_125:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZL10ThrowErrorRK11CStringBaseIwES2_S2_, .Lfunc_end5-_ZL10ThrowErrorRK11CStringBaseIwES2_S2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\205\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp118-.Lfunc_begin3  #   Call between .Lfunc_begin3 and .Ltmp118
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp118-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin3  #     jumps to .Ltmp120
	.byte	0                       #   On action: cleanup
	.long	.Ltmp119-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp121-.Ltmp119       #   Call between .Ltmp119 and .Ltmp121
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp121-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp128-.Lfunc_begin3  #     jumps to .Ltmp128
	.byte	0                       #   On action: cleanup
	.long	.Ltmp123-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp124-.Ltmp123       #   Call between .Ltmp123 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin3  #     jumps to .Ltmp125
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp128-.Lfunc_begin3  #     jumps to .Ltmp128
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin3  # >> Call Site 7 <<
	.long	.Ltmp129-.Ltmp127       #   Call between .Ltmp127 and .Ltmp129
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin3  # >> Call Site 8 <<
	.long	.Ltmp130-.Ltmp129       #   Call between .Ltmp129 and .Ltmp130
	.long	.Ltmp131-.Lfunc_begin3  #     jumps to .Ltmp131
	.byte	0                       #   On action: cleanup
	.long	.Ltmp132-.Lfunc_begin3  # >> Call Site 9 <<
	.long	.Ltmp133-.Ltmp132       #   Call between .Ltmp132 and .Ltmp133
	.long	.Ltmp134-.Lfunc_begin3  #     jumps to .Ltmp134
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin3  # >> Call Site 10 <<
	.long	.Lfunc_end5-.Ltmp133    #   Call between .Ltmp133 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIiED0Ev,"axG",@progbits,_ZN13CRecordVectorIiED0Ev,comdat
	.weak	_ZN13CRecordVectorIiED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIiED0Ev,@function
_ZN13CRecordVectorIiED0Ev:              # @_ZN13CRecordVectorIiED0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -24
.Lcfi44:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp135:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp136:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB6_2:
.Ltmp137:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN13CRecordVectorIiED0Ev, .Lfunc_end6-_ZN13CRecordVectorIiED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp135-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp136-.Ltmp135       #   Call between .Ltmp135 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin4  #     jumps to .Ltmp137
	.byte	0                       #   On action: cleanup
	.long	.Ltmp136-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Lfunc_end6-.Ltmp136    #   Call between .Ltmp136 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 32
.Lcfi48:
	.cfi_offset %rbx, -24
.Lcfi49:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp138:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp139:
# BB#1:
.Ltmp144:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp145:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB7_5:
.Ltmp146:
	movq	%rax, %r14
	jmp	.LBB7_6
.LBB7_3:
.Ltmp140:
	movq	%rax, %r14
.Ltmp141:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp142:
.LBB7_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_4:
.Ltmp143:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end7-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp138-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp139-.Ltmp138       #   Call between .Ltmp138 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin5  #     jumps to .Ltmp140
	.byte	0                       #   On action: cleanup
	.long	.Ltmp144-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp145-.Ltmp144       #   Call between .Ltmp144 and .Ltmp145
	.long	.Ltmp146-.Lfunc_begin5  #     jumps to .Ltmp146
	.byte	0                       #   On action: cleanup
	.long	.Ltmp141-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp142-.Ltmp141       #   Call between .Ltmp141 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin5  #     jumps to .Ltmp143
	.byte	1                       #   On action: 1
	.long	.Ltmp142-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Lfunc_end7-.Ltmp142    #   Call between .Ltmp142 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 64
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB8_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB8_6
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_5
# BB#4:                                 #   in Loop: Header=BB8_2 Depth=1
	callq	_ZdaPv
.LBB8_5:                                # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB8_6:                                #   in Loop: Header=BB8_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB8_2
.LBB8_7:                                # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end8:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end8-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.type	_ZTS11CStringBaseIwE,@object # @_ZTS11CStringBaseIwE
	.section	.rodata._ZTS11CStringBaseIwE,"aG",@progbits,_ZTS11CStringBaseIwE,comdat
	.weak	_ZTS11CStringBaseIwE
	.p2align	4
_ZTS11CStringBaseIwE:
	.asciz	"11CStringBaseIwE"
	.size	_ZTS11CStringBaseIwE, 17

	.type	_ZTI11CStringBaseIwE,@object # @_ZTI11CStringBaseIwE
	.section	.rodata._ZTI11CStringBaseIwE,"aG",@progbits,_ZTI11CStringBaseIwE,comdat
	.weak	_ZTI11CStringBaseIwE
	.p2align	3
_ZTI11CStringBaseIwE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS11CStringBaseIwE
	.size	_ZTI11CStringBaseIwE, 16

	.type	_ZTV13CRecordVectorIiE,@object # @_ZTV13CRecordVectorIiE
	.section	.rodata._ZTV13CRecordVectorIiE,"aG",@progbits,_ZTV13CRecordVectorIiE,comdat
	.weak	_ZTV13CRecordVectorIiE
	.p2align	3
_ZTV13CRecordVectorIiE:
	.quad	0
	.quad	_ZTI13CRecordVectorIiE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIiED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIiE, 40

	.type	_ZTS13CRecordVectorIiE,@object # @_ZTS13CRecordVectorIiE
	.section	.rodata._ZTS13CRecordVectorIiE,"aG",@progbits,_ZTS13CRecordVectorIiE,comdat
	.weak	_ZTS13CRecordVectorIiE
	.p2align	4
_ZTS13CRecordVectorIiE:
	.asciz	"13CRecordVectorIiE"
	.size	_ZTS13CRecordVectorIiE, 19

	.type	_ZTI13CRecordVectorIiE,@object # @_ZTI13CRecordVectorIiE
	.section	.rodata._ZTI13CRecordVectorIiE,"aG",@progbits,_ZTI13CRecordVectorIiE,comdat
	.weak	_ZTI13CRecordVectorIiE
	.p2align	4
_ZTI13CRecordVectorIiE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIiE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIiE, 24

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
