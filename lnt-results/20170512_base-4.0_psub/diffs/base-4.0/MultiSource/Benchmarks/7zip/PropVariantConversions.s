	.text
	.file	"PropVariantConversions.bc"
	.globl	_Z23ConvertFileTimeToStringRK9_FILETIMEPcbb
	.p2align	4, 0x90
	.type	_Z23ConvertFileTimeToStringRK9_FILETIMEPcbb,@function
_Z23ConvertFileTimeToStringRK9_FILETIMEPcbb: # @_Z23ConvertFileTimeToStringRK9_FILETIMEPcbb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 80
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %r15d
	movq	%rsi, %rbx
	leaq	16(%rsp), %rsi
	callq	LocalFileTimeToFileTime
	movl	16(%rsp), %eax
	movl	20(%rsp), %ecx
	shlq	$32, %rcx
	orq	%rcx, %rax
	movq	%rax, 32(%rsp)
	leaq	32(%rsp), %rdi
	leaq	12(%rsp), %rsi
	callq	_Z25RtlTimeToSecondsSince1970PK13LARGE_INTEGERPj
	movl	12(%rsp), %eax
	movq	%rax, 24(%rsp)
	leaq	24(%rsp), %rdi
	callq	localtime
	movq	%rax, %rbp
	movl	$1900, %edx             # imm = 0x76C
	addl	20(%rbp), %edx
	movl	12(%rbp), %r8d
	movl	16(%rbp), %ecx
	incl	%ecx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	testb	%r15b, %r15b
	je	.LBB0_3
# BB#1:
	movq	%rbx, %rdi
	callq	strlen
	leaq	(%rax,%rbx), %rdi
	movl	4(%rbp), %ecx
	movl	8(%rbp), %edx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	sprintf
	testb	%r14b, %r14b
	je	.LBB0_3
# BB#2:
	movq	%rbx, %rdi
	callq	strlen
	addq	%rax, %rbx
	movl	(%rbp), %edx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
.LBB0_3:
	movb	$1, %al
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_Z23ConvertFileTimeToStringRK9_FILETIMEPcbb, .Lfunc_end0-_Z23ConvertFileTimeToStringRK9_FILETIMEPcbb
	.cfi_endproc

	.globl	_Z23ConvertFileTimeToStringRK9_FILETIMEbb
	.p2align	4, 0x90
	.type	_Z23ConvertFileTimeToStringRK9_FILETIMEbb,@function
_Z23ConvertFileTimeToStringRK9_FILETIMEbb: # @_Z23ConvertFileTimeToStringRK9_FILETIMEbb
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 96
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r12, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movzbl	%dl, %edx
	movzbl	%cl, %ecx
	leaq	16(%rsp), %rbx
	movq	%rsi, %rdi
	movq	%rbx, %rsi
	callq	_Z23ConvertFileTimeToStringRK9_FILETIMEPcbb
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %r15d
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpb	$0, (%rbx)
	leaq	1(%rbx), %rbx
	jne	.LBB1_1
# BB#2:                                 # %_Z11MyStringLenIcEiPKT_.exit.i
	leaq	16(%rsp), %rbx
	leal	1(%r15), %eax
	movslq	%eax, %r12
	cmpl	$-1, %r15d
	movq	$-1, %rdi
	cmovgeq	%r12, %rdi
	callq	_Znam
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	%r12d, 12(%rsp)
	.p2align	4, 0x90
.LBB1_3:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %ecx
	incq	%rbx
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB1_3
# BB#4:                                 # %_ZN11CStringBaseIcEC2EPKc.exit
	movl	%r15d, 8(%rsp)
.Ltmp0:
	movq	%rsp, %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp1:
# BB#5:                                 # %_Z16GetUnicodeStringRK11CStringBaseIcE.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_7
# BB#6:
	callq	_ZdaPv
.LBB1_7:                                # %_ZN11CStringBaseIcED2Ev.exit
	movq	%r14, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB1_8:
.Ltmp2:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_10
# BB#9:
	callq	_ZdaPv
.LBB1_10:                               # %_ZN11CStringBaseIcED2Ev.exit4
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_Z23ConvertFileTimeToStringRK9_FILETIMEbb, .Lfunc_end1-_Z23ConvertFileTimeToStringRK9_FILETIMEbb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z26ConvertPropVariantToStringRK14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_Z26ConvertPropVariantToStringRK14tagPROPVARIANT,@function
_Z26ConvertPropVariantToStringRK14tagPROPVARIANT: # @_Z26ConvertPropVariantToStringRK14tagPROPVARIANT
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 40
	subq	$136, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 176
.Lcfi23:
	.cfi_offset %rbx, -40
.Lcfi24:
	.cfi_offset %r12, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movzwl	(%rsi), %eax
	cmpq	$64, %rax
	ja	.LBB2_48
# BB#1:
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_49:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	$4, 12(%r14)
	jmp	.LBB2_50
.LBB2_28:
	movswq	8(%rsi), %rdi
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	callq	_Z20ConvertInt64ToStringxPw
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$-1, %r15d
	.p2align	4, 0x90
.LBB2_29:                               # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpl	$0, (%rbx)
	leaq	4(%rbx), %rbx
	jne	.LBB2_29
# BB#30:                                # %_Z11MyStringLenIwEiPKT_.exit.i.i34
	movq	%rsp, %rbx
	leal	1(%r15), %eax
	movslq	%eax, %r12
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%r12d, 12(%r14)
	.p2align	4, 0x90
.LBB2_31:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i37
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB2_31
# BB#32:                                # %_ZL20ConvertInt64ToStringx.exit
	movl	%r15d, 8(%r14)
	jmp	.LBB2_50
.LBB2_33:
	movslq	8(%rsi), %rdi
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	callq	_Z20ConvertInt64ToStringxPw
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$-1, %r15d
	.p2align	4, 0x90
.LBB2_34:                               # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpl	$0, (%rbx)
	leaq	4(%rbx), %rbx
	jne	.LBB2_34
# BB#35:                                # %_Z11MyStringLenIwEiPKT_.exit.i.i40
	movq	%rsp, %rbx
	leal	1(%r15), %eax
	movslq	%eax, %r12
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%r12d, 12(%r14)
	.p2align	4, 0x90
.LBB2_36:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i43
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB2_36
# BB#37:                                # %_ZL20ConvertInt64ToStringx.exit44
	movl	%r15d, 8(%r14)
	jmp	.LBB2_50
.LBB2_2:
	movq	8(%rsi), %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$-1, %r12d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	incl	%r12d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB2_3
# BB#4:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%r12), %eax
	movslq	%eax, %r15
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%r15d, 12(%r14)
	.p2align	4, 0x90
.LBB2_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB2_5
# BB#6:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%r12d, 8(%r14)
	jmp	.LBB2_50
.LBB2_43:
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	movl	$.L.str.3, %eax
	movl	$.L.str.4, %ebx
	cmovneq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$-1, %r12d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB2_44:                               # =>This Inner Loop Header: Depth=1
	incl	%r12d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB2_44
# BB#45:                                # %_Z11MyStringLenIwEiPKT_.exit.i54
	leal	1(%r12), %eax
	movslq	%eax, %r15
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%r15d, 12(%r14)
	.p2align	4, 0x90
.LBB2_46:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i57
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB2_46
# BB#47:                                # %_ZN11CStringBaseIwEC2EPKw.exit58
	movl	%r12d, 8(%r14)
	jmp	.LBB2_50
.LBB2_7:
	movzbl	8(%rsi), %edi
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	callq	_Z21ConvertUInt64ToStringyPw
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$-1, %r15d
	.p2align	4, 0x90
.LBB2_8:                                # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpl	$0, (%rbx)
	leaq	4(%rbx), %rbx
	jne	.LBB2_8
# BB#9:                                 # %_Z11MyStringLenIwEiPKT_.exit.i.i
	movq	%rsp, %rbx
	leal	1(%r15), %eax
	movslq	%eax, %r12
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%r12d, 12(%r14)
	.p2align	4, 0x90
.LBB2_10:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB2_10
# BB#11:                                # %_ZL21ConvertUInt64ToStringy.exit
	movl	%r15d, 8(%r14)
	jmp	.LBB2_50
.LBB2_12:
	movzwl	8(%rsi), %edi
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	callq	_Z21ConvertUInt64ToStringyPw
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$-1, %r15d
	.p2align	4, 0x90
.LBB2_13:                               # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpl	$0, (%rbx)
	leaq	4(%rbx), %rbx
	jne	.LBB2_13
# BB#14:                                # %_Z11MyStringLenIwEiPKT_.exit.i.i13
	movq	%rsp, %rbx
	leal	1(%r15), %eax
	movslq	%eax, %r12
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%r12d, 12(%r14)
	.p2align	4, 0x90
.LBB2_15:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i16
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB2_15
# BB#16:                                # %_ZL21ConvertUInt64ToStringy.exit17
	movl	%r15d, 8(%r14)
	jmp	.LBB2_50
.LBB2_17:
	movl	8(%rsi), %edi
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	callq	_Z21ConvertUInt64ToStringyPw
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$-1, %r15d
	.p2align	4, 0x90
.LBB2_18:                               # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpl	$0, (%rbx)
	leaq	4(%rbx), %rbx
	jne	.LBB2_18
# BB#19:                                # %_Z11MyStringLenIwEiPKT_.exit.i.i20
	movq	%rsp, %rbx
	leal	1(%r15), %eax
	movslq	%eax, %r12
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%r12d, 12(%r14)
	.p2align	4, 0x90
.LBB2_20:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i23
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB2_20
# BB#21:                                # %_ZL21ConvertUInt64ToStringy.exit24
	movl	%r15d, 8(%r14)
	jmp	.LBB2_50
.LBB2_38:
	movq	8(%rsi), %rdi
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	callq	_Z20ConvertInt64ToStringxPw
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$-1, %r15d
	.p2align	4, 0x90
.LBB2_39:                               # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpl	$0, (%rbx)
	leaq	4(%rbx), %rbx
	jne	.LBB2_39
# BB#40:                                # %_Z11MyStringLenIwEiPKT_.exit.i.i47
	movq	%rsp, %rbx
	leal	1(%r15), %eax
	movslq	%eax, %r12
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%r12d, 12(%r14)
	.p2align	4, 0x90
.LBB2_41:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i50
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB2_41
# BB#42:                                # %_ZL20ConvertInt64ToStringx.exit51
	movl	%r15d, 8(%r14)
	jmp	.LBB2_50
.LBB2_22:
	movq	8(%rsi), %rdi
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	callq	_Z21ConvertUInt64ToStringyPw
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$-1, %r15d
	.p2align	4, 0x90
.LBB2_23:                               # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpl	$0, (%rbx)
	leaq	4(%rbx), %rbx
	jne	.LBB2_23
# BB#24:                                # %_Z11MyStringLenIwEiPKT_.exit.i.i27
	movq	%rsp, %rbx
	leal	1(%r15), %eax
	movslq	%eax, %r12
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%r12d, 12(%r14)
	.p2align	4, 0x90
.LBB2_25:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i30
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB2_25
# BB#26:                                # %_ZL21ConvertUInt64ToStringy.exit31
	movl	%r15d, 8(%r14)
	jmp	.LBB2_50
.LBB2_27:
	addq	$8, %rsi
	movl	$1, %edx
	movl	$1, %ecx
	movq	%r14, %rdi
	callq	_Z23ConvertFileTimeToStringRK9_FILETIMEbb
.LBB2_50:
	movq	%r14, %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB2_48:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$150245, (%rax)         # imm = 0x24AE5
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end2:
	.size	_Z26ConvertPropVariantToStringRK14tagPROPVARIANT, .Lfunc_end2-_Z26ConvertPropVariantToStringRK14tagPROPVARIANT
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_49
	.quad	.LBB2_48
	.quad	.LBB2_28
	.quad	.LBB2_33
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_2
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_43
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_7
	.quad	.LBB2_12
	.quad	.LBB2_17
	.quad	.LBB2_38
	.quad	.LBB2_22
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_48
	.quad	.LBB2_27

	.text
	.globl	_Z26ConvertPropVariantToUInt64RK14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_Z26ConvertPropVariantToUInt64RK14tagPROPVARIANT,@function
_Z26ConvertPropVariantToUInt64RK14tagPROPVARIANT: # @_Z26ConvertPropVariantToUInt64RK14tagPROPVARIANT
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 16
	movzwl	(%rdi), %eax
	addl	$-17, %eax
	movzwl	%ax, %eax
	cmpl	$4, %eax
	ja	.LBB3_5
# BB#1:
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_6:
	movzbl	8(%rdi), %eax
	popq	%rcx
	retq
.LBB3_3:
	movl	8(%rdi), %eax
	popq	%rcx
	retq
.LBB3_4:
	movq	8(%rdi), %rax
	popq	%rcx
	retq
.LBB3_2:
	movzwl	8(%rdi), %eax
	popq	%rcx
	retq
.LBB3_5:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$151199, (%rax)         # imm = 0x24E9F
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end3:
	.size	_Z26ConvertPropVariantToUInt64RK14tagPROPVARIANT, .Lfunc_end3-_Z26ConvertPropVariantToUInt64RK14tagPROPVARIANT
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_6
	.quad	.LBB3_2
	.quad	.LBB3_3
	.quad	.LBB3_5
	.quad	.LBB3_4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%04d-%02d-%02d"
	.size	.L.str, 15

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" %02d:%02d"
	.size	.L.str.1, 11

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	":%02d"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.3:
	.long	43                      # 0x2b
	.long	0                       # 0x0
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
	.p2align	2
.L.str.4:
	.long	45                      # 0x2d
	.long	0                       # 0x0
	.size	.L.str.4, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
