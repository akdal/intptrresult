	.text
	.file	"symtab.bc"
	.globl	new_D_Scope
	.p2align	4, 0x90
	.type	new_D_Scope,@function
new_D_Scope:                            # @new_D_Scope
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$72, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 64(%rbx)
	testq	%r14, %r14
	je	.LBB0_2
# BB#1:
	movb	(%r14), %al
	andb	$6, %al
	movb	%al, (%rbx)
	movq	%r14, 32(%rbx)
	movq	%r14, 40(%rbx)
	movq	%r14, 48(%rbx)
	movq	56(%r14), %rax
	movq	%rax, 64(%rbx)
	movq	%rbx, 56(%r14)
	jmp	.LBB0_3
.LBB0_2:
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r14)
	movups	%xmm0, 16(%r14)
	movups	%xmm0, (%r14)
	movabsq	$13473312413827, %rax   # imm = 0xC4100001883
	movq	%rax, 4(%r14)
	movl	$25096, %edi            # imm = 0x6208
	callq	malloc
	movq	%rax, 16(%r14)
	xorl	%esi, %esi
	movl	$25096, %edx            # imm = 0x6208
	movq	%rax, %rdi
	callq	memset
	movq	%r14, 16(%rbx)
.LBB0_3:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	new_D_Scope, .Lfunc_end0-new_D_Scope
	.cfi_endproc

	.globl	enter_D_Scope
	.p2align	4, 0x90
	.type	enter_D_Scope,@function
enter_D_Scope:                          # @enter_D_Scope
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$72, %edi
	callq	malloc
	movq	40(%rbx), %rcx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rcx, 40(%rax)
	movb	(%rbx), %cl
	andb	$6, %cl
	movb	%cl, (%rax)
	movq	%rbx, 32(%rax)
	movq	%r14, 48(%rax)
	movq	56(%r14), %rcx
	movq	%rcx, 64(%rax)
	movq	%rax, 56(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	enter_D_Scope, .Lfunc_end1-enter_D_Scope
	.cfi_endproc

	.globl	free_D_Scope
	.p2align	4, 0x90
	.type	free_D_Scope,@function
free_D_Scope:                           # @free_D_Scope
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movq	64(%rdi), %rbx
	xorl	%esi, %esi
	callq	free_D_Scope
	movq	%rbx, 56(%r14)
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB2_1
.LBB2_2:                                # %._crit_edge26
	testl	%ebp, %ebp
	jne	.LBB2_4
# BB#3:                                 # %._crit_edge26
	movb	(%r14), %al
	andb	$1, %al
	je	.LBB2_4
# BB#18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_4:
	movq	16(%r14), %r15
	testq	%r15, %r15
	je	.LBB2_5
# BB#7:
	movl	8(%r15), %ecx
	testl	%ecx, %ecx
	je	.LBB2_8
# BB#9:                                 # %.preheader.lr.ph.i
	movq	16(%r15), %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_10:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_11 Depth 2
	movq	(%rdi,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB2_13
	.p2align	4, 0x90
.LBB2_11:                               # %.lr.ph.i
                                        #   Parent Loop BB2_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rax), %rbp
	movq	%rax, %rdi
	callq	free
	movq	16(%r15), %rax
	movq	%rbp, (%rax,%rbx,8)
	movq	16(%r15), %rdi
	movq	(%rdi,%rbx,8), %rax
	testq	%rax, %rax
	jne	.LBB2_11
# BB#12:                                # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB2_10 Depth=1
	movl	8(%r15), %ecx
.LBB2_13:                               # %._crit_edge.i
                                        #   in Loop: Header=BB2_10 Depth=1
	incq	%rbx
	cmpl	%ecx, %ebx
	jb	.LBB2_10
	jmp	.LBB2_14
.LBB2_5:                                # %.preheader21
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_15
	.p2align	4, 0x90
.LBB2_6:                                # =>This Inner Loop Header: Depth=1
	movq	24(%rdi), %rbx
	callq	free
	movq	%rbx, 8(%r14)
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB2_6
	jmp	.LBB2_15
.LBB2_8:                                # %.._crit_edge17_crit_edge.i
	movq	16(%r15), %rdi
.LBB2_14:                               # %free_D_SymHash.exit
	callq	free
	movq	%r15, %rdi
	callq	free
.LBB2_15:                               # %.preheader
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_17
	.p2align	4, 0x90
.LBB2_16:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rdi), %rbx
	callq	free
	movq	%rbx, 24(%r14)
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB2_16
.LBB2_17:                               # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end2:
	.size	free_D_Scope, .Lfunc_end2-free_D_Scope
	.cfi_endproc

	.globl	free_D_Sym
	.p2align	4, 0x90
	.type	free_D_Sym,@function
free_D_Sym:                             # @free_D_Sym
	.cfi_startproc
# BB#0:
	jmp	free                    # TAILCALL
.Lfunc_end3:
	.size	free_D_Sym, .Lfunc_end3-free_D_Sym
	.cfi_endproc

	.globl	commit_D_Scope
	.p2align	4, 0x90
	.type	commit_D_Scope,@function
commit_D_Scope:                         # @commit_D_Scope
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpq	$0, 40(%r15)
	jne	.LBB4_18
# BB#1:                                 # %.preheader.preheader
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB4_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %r14
	movq	32(%r14), %rax
	testq	%rax, %rax
	jne	.LBB4_2
# BB#3:
	movq	16(%r14), %rsi
	movq	%r15, %rdi
	callq	commit_ll
	movq	16(%r14), %rax
	movl	8(%rax), %r8d
	testq	%r8, %r8
	je	.LBB4_17
# BB#4:                                 # %.lr.ph19.i
	testq	%r15, %r15
	movq	16(%rax), %rcx
	je	.LBB4_19
# BB#5:                                 # %.lr.ph19.split.i.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph19.split.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_8 Depth 2
                                        #       Child Loop BB4_9 Depth 3
                                        #         Child Loop BB4_11 Depth 4
	movq	(%rcx,%rdx,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB4_8
	jmp	.LBB4_16
.LBB4_14:                               #   in Loop: Header=BB4_8 Depth=2
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB4_15:                               # %current_D_Sym.exit.i
                                        #   in Loop: Header=BB4_8 Depth=2
	movq	%rdi, 16(%rsi)
	movq	24(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB4_16
.LBB4_8:                                # %.lr.ph.split.i
                                        #   Parent Loop BB4_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_9 Depth 3
                                        #         Child Loop BB4_11 Depth 4
	movq	16(%rsi), %rax
	testq	%rax, %rax
	cmoveq	%rsi, %rax
	movq	%r15, %rbx
.LBB4_9:                                # %.lr.ph24.i.i
                                        #   Parent Loop BB4_6 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_11 Depth 4
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB4_11
	jmp	.LBB4_13
	.p2align	4, 0x90
.LBB4_12:                               #   in Loop: Header=BB4_11 Depth=4
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB4_13
.LBB4_11:                               # %.lr.ph.i.i
                                        #   Parent Loop BB4_6 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        #       Parent Loop BB4_9 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rax, 16(%rdi)
	jne	.LBB4_12
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_13:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB4_9 Depth=3
	movq	48(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_9
	jmp	.LBB4_14
	.p2align	4, 0x90
.LBB4_16:                               # %._crit_edge.i
                                        #   in Loop: Header=BB4_6 Depth=1
	incq	%rdx
	cmpq	%r8, %rdx
	jne	.LBB4_6
	jmp	.LBB4_17
.LBB4_19:                               # %.lr.ph19.split.us.i.preheader
	testb	$1, %r8b
	jne	.LBB4_21
# BB#20:
	xorl	%edx, %edx
	cmpl	$1, %r8d
	jne	.LBB4_25
	jmp	.LBB4_17
.LBB4_21:                               # %.lr.ph19.split.us.i.prol
	movq	(%rcx), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.LBB4_24
	.p2align	4, 0x90
.LBB4_22:                               # %current_D_Sym.exit.us.us.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	cmoveq	%rax, %rsi
	movq	%rsi, 16(%rax)
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.LBB4_22
.LBB4_24:                               # %.lr.ph19.split.us.i.prol.loopexit
	cmpl	$1, %r8d
	je	.LBB4_17
	.p2align	4, 0x90
.LBB4_25:                               # %.lr.ph19.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_31 Depth 2
                                        #     Child Loop BB4_28 Depth 2
	movq	(%rcx,%rdx,8), %rax
	testq	%rax, %rax
	je	.LBB4_27
	.p2align	4, 0x90
.LBB4_31:                               # %current_D_Sym.exit.us.us.i
                                        #   Parent Loop BB4_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	cmoveq	%rax, %rsi
	movq	%rsi, 16(%rax)
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.LBB4_31
.LBB4_27:                               # %._crit_edge.us-lcssa.us.us.i
                                        #   in Loop: Header=BB4_25 Depth=1
	movq	8(%rcx,%rdx,8), %rax
	testq	%rax, %rax
	je	.LBB4_30
	.p2align	4, 0x90
.LBB4_28:                               # %current_D_Sym.exit.us.us.i.1
                                        #   Parent Loop BB4_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	cmoveq	%rax, %rsi
	movq	%rsi, 16(%rax)
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.LBB4_28
.LBB4_30:                               # %._crit_edge.us-lcssa.us.us.i.1
                                        #   in Loop: Header=BB4_25 Depth=1
	addq	$2, %rdx
	cmpq	%r8, %rdx
	jne	.LBB4_25
.LBB4_17:
	movq	%r14, %r15
.LBB4_18:                               # %commit_update.exit
	movq	%r15, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	commit_D_Scope, .Lfunc_end4-commit_D_Scope
	.cfi_endproc

	.p2align	4, 0x90
	.type	commit_ll,@function
commit_ll:                              # @commit_ll
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 32
.Lcfi28:
	.cfi_offset %rbx, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_3
# BB#1:
	movq	%r14, %rsi
	callq	commit_ll
	movq	8(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB5_3
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movq	24(%rsi), %rbx
	movq	%r14, %rdi
	callq	symhash_add
	movq	%rbx, 8(%r15)
	testq	%rbx, %rbx
	movq	%rbx, %rsi
	jne	.LBB5_2
.LBB5_3:                                # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	commit_ll, .Lfunc_end5-commit_ll
	.cfi_endproc

	.globl	new_D_Sym
	.p2align	4, 0x90
	.type	new_D_Sym,@function
new_D_Sym:                              # @new_D_Sym
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 48
.Lcfi36:
	.cfi_offset %rbx, -48
.Lcfi37:
	.cfi_offset %r12, -40
.Lcfi38:
	.cfi_offset %r13, -32
.Lcfi39:
	.cfi_offset %r14, -24
.Lcfi40:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r14
	subl	%r12d, %r15d
	movslq	%ecx, %r13
	movq	%r13, %rdi
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	callq	memset
	movq	%r12, (%rbx)
	movl	%r15d, 8(%rbx)
	movq	%r12, %rdi
	movl	%r15d, %esi
	callq	strhashl
	movl	%eax, 12(%rbx)
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB6_2
# BB#1:
	movq	%rbx, %rsi
	callq	symhash_add
	jmp	.LBB6_3
.LBB6_2:
	movq	8(%r14), %rax
	movq	%rax, 24(%rbx)
	movq	%rbx, 8(%r14)
.LBB6_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	new_D_Sym, .Lfunc_end6-new_D_Sym
	.cfi_endproc

	.p2align	4, 0x90
	.type	symhash_add,@function
symhash_add:                            # @symhash_add
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi47:
	.cfi_def_cfa_offset 112
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	12(%rsi), %eax
	movl	8(%r15), %r13d
	xorl	%edx, %edx
	divl	%r13d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	16(%r15), %rax
	movl	(%r15), %ecx
	leal	1(%rcx), %edi
	movl	%edi, (%r15)
	movq	(%rax,%rdx,8), %rdi
	movq	%rdi, 24(%rsi)
	movq	%rsi, (%rax,%rdx,8)
	movl	4(%r15), %ebp
	cmpl	%ebp, %ecx
	jl	.LBB7_13
# BB#1:
	movq	16(%r15), %rbx
	movl	%ebp, 8(%r15)
	leal	1(%rbp,%rbp), %eax
	movl	%eax, 4(%r15)
	leaq	(,%rbp,8), %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %r14
	movq	%r14, 16(%r15)
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	memset
	movl	$0, 8(%rsp)
	movq	$0, 16(%rsp)
	testl	%r13d, %r13d
	je	.LBB7_2
# BB#3:                                 # %.preheader62.lr.ph
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	leaq	24(%rsp), %rbp
	xorl	%r12d, %r12d
	leaq	8(%rsp), %r15
	jmp	.LBB7_4
	.p2align	4, 0x90
.LBB7_17:                               # %.backedge
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	%ecx, 8(%rsp)
	movq	%rsi, 24(%rsp,%rax,8)
.LBB7_4:                                # %.preheader62
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%r12,8), %rsi
	testq	%rsi, %rsi
	je	.LBB7_7
# BB#5:                                 #   in Loop: Header=BB7_4 Depth=1
	movq	24(%rsi), %rax
	movq	%rax, (%rbx,%r12,8)
	movq	16(%rsp), %rcx
	testq	%rcx, %rcx
	je	.LBB7_6
# BB#14:                                #   in Loop: Header=BB7_4 Depth=1
	movl	8(%rsp), %eax
	cmpq	%rbp, %rcx
	je	.LBB7_15
# BB#18:                                #   in Loop: Header=BB7_4 Depth=1
	testb	$7, %al
	je	.LBB7_20
# BB#19:                                #   in Loop: Header=BB7_4 Depth=1
	leal	1(%rax), %edx
	movl	%edx, 8(%rsp)
	movq	%rsi, (%rcx,%rax,8)
	jmp	.LBB7_4
	.p2align	4, 0x90
.LBB7_6:                                #   in Loop: Header=BB7_4 Depth=1
	movq	%rbp, 16(%rsp)
	movl	8(%rsp), %eax
	leal	1(%rax), %ecx
	jmp	.LBB7_17
	.p2align	4, 0x90
.LBB7_15:                               #   in Loop: Header=BB7_4 Depth=1
	cmpl	$2, %eax
	ja	.LBB7_20
# BB#16:                                #   in Loop: Header=BB7_4 Depth=1
	movl	%eax, %ecx
	incl	%ecx
	jmp	.LBB7_17
	.p2align	4, 0x90
.LBB7_20:                               #   in Loop: Header=BB7_4 Depth=1
	movq	%r15, %rdi
	callq	vec_add_internal
	jmp	.LBB7_4
.LBB7_7:                                # %._crit_edge64
                                        #   in Loop: Header=BB7_4 Depth=1
	incq	%r12
	cmpq	%r13, %r12
	jne	.LBB7_4
# BB#8:                                 # %.preheader.loopexit
	movq	16(%rsp), %r15
	movq	48(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB7_9
.LBB7_2:
	xorl	%r13d, %r13d
.LBB7_9:                                # %.preheader
	movq	(%r15,%r13,8), %rcx
	testq	%rcx, %rcx
	je	.LBB7_12
# BB#10:
	leaq	(%r15,%r13,8), %rax
	.p2align	4, 0x90
.LBB7_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rcx), %rdx
	movq	%rdx, (%rax)
	movl	12(%rcx), %eax
	xorl	%edx, %edx
	divl	%ebp
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	(%r14,%rdx,8), %rax
	movq	%rax, 24(%rcx)
	movq	%rcx, (%r14,%rdx,8)
	movq	16(%rsp), %rcx
	leaq	(%rcx,%r13,8), %rax
	movq	(%rcx,%r13,8), %rcx
	testq	%rcx, %rcx
	jne	.LBB7_11
.LBB7_12:                               # %._crit_edge
	movq	%rbx, %rdi
	callq	free
.LBB7_13:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	symhash_add, .Lfunc_end7-symhash_add
	.cfi_endproc

	.globl	current_D_Sym
	.p2align	4, 0x90
	.type	current_D_Sym,@function
current_D_Sym:                          # @current_D_Sym
	.cfi_startproc
# BB#0:
	movq	16(%rsi), %rax
	testq	%rax, %rax
	cmoveq	%rsi, %rax
	testq	%rdi, %rdi
	jne	.LBB8_2
	jmp	.LBB8_6
	.p2align	4, 0x90
.LBB8_8:                                # %._crit_edge
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB8_6
.LBB8_2:                                # %.lr.ph24
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_4 Depth 2
	movq	24(%rdi), %rcx
	testq	%rcx, %rcx
	jne	.LBB8_4
	jmp	.LBB8_8
	.p2align	4, 0x90
.LBB8_7:                                #   in Loop: Header=BB8_4 Depth=2
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB8_8
.LBB8_4:                                # %.lr.ph
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, 16(%rcx)
	jne	.LBB8_7
# BB#5:
	movq	%rcx, %rax
.LBB8_6:                                # %.loopexit
	retq
.Lfunc_end8:
	.size	current_D_Sym, .Lfunc_end8-current_D_Sym
	.cfi_endproc

	.globl	find_sym_internal
	.p2align	4, 0x90
	.type	find_sym_internal,@function
find_sym_internal:                      # @find_sym_internal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 64
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movl	%ecx, %r13d
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	%rdi, (%rsp)            # 8-byte Spill
	movslq	%r13d, %r14
	.p2align	4, 0x90
.LBB9_1:                                # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_9 Depth 2
	testq	%rbx, %rbx
	je	.LBB9_2
# BB#4:                                 #   in Loop: Header=BB9_1 Depth=1
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB9_6
# BB#5:                                 #   in Loop: Header=BB9_1 Depth=1
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	8(%rcx)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	shlq	$3, %rdx
	addq	16(%rcx), %rdx
	jmp	.LBB9_7
	.p2align	4, 0x90
.LBB9_6:                                #   in Loop: Header=BB9_1 Depth=1
	leaq	8(%rbx), %rdx
.LBB9_7:                                # %.preheader
                                        #   in Loop: Header=BB9_1 Depth=1
	movq	(%rdx), %r15
	testq	%r15, %r15
	jne	.LBB9_9
	jmp	.LBB9_13
	.p2align	4, 0x90
.LBB9_12:                               #   in Loop: Header=BB9_9 Depth=2
	movq	24(%r15), %r15
	testq	%r15, %r15
	je	.LBB9_13
.LBB9_9:                                # %.lr.ph
                                        #   Parent Loop BB9_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebp, 12(%r15)
	jne	.LBB9_12
# BB#10:                                #   in Loop: Header=BB9_9 Depth=2
	cmpl	%r13d, 8(%r15)
	jne	.LBB9_12
# BB#11:                                #   in Loop: Header=BB9_9 Depth=2
	movq	(%r15), %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB9_12
	jmp	.LBB9_14
	.p2align	4, 0x90
.LBB9_13:                               # %.critedge
                                        #   in Loop: Header=BB9_1 Depth=1
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_1
.LBB9_2:
	xorl	%eax, %eax
	jmp	.LBB9_3
.LBB9_14:
	movq	16(%r15), %rax
	testq	%rax, %rax
	cmoveq	%r15, %rax
	movq	(%rsp), %rdx            # 8-byte Reload
	testq	%rdx, %rdx
	jne	.LBB9_16
	jmp	.LBB9_3
	.p2align	4, 0x90
.LBB9_21:                               # %._crit_edge.i
                                        #   in Loop: Header=BB9_16 Depth=1
	movq	48(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB9_3
.LBB9_16:                               # %.lr.ph24.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_18 Depth 2
	movq	24(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB9_18
	jmp	.LBB9_21
	.p2align	4, 0x90
.LBB9_20:                               #   in Loop: Header=BB9_18 Depth=2
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB9_21
.LBB9_18:                               # %.lr.ph.i
                                        #   Parent Loop BB9_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, 16(%rcx)
	jne	.LBB9_20
# BB#19:
	movq	%rcx, %rax
.LBB9_3:                                # %current_D_Sym.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	find_sym_internal, .Lfunc_end9-find_sym_internal
	.cfi_endproc

	.globl	find_D_Sym
	.p2align	4, 0x90
	.type	find_D_Sym,@function
find_D_Sym:                             # @find_D_Sym
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 64
.Lcfi74:
	.cfi_offset %rbx, -56
.Lcfi75:
	.cfi_offset %r12, -48
.Lcfi76:
	.cfi_offset %r13, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	%rdi, %r14
	subq	%r12, %rbx
	movq	%r12, %rdi
	movl	%ebx, %esi
	callq	strhashl
	movl	%eax, %ebp
	movslq	%ebx, %r13
	movq	%r14, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB10_1:                               # %tailrecurse.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_9 Depth 2
	testq	%r14, %r14
	je	.LBB10_2
# BB#4:                                 #   in Loop: Header=BB10_1 Depth=1
	movq	16(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB10_6
# BB#5:                                 #   in Loop: Header=BB10_1 Depth=1
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	8(%rcx)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	shlq	$3, %rdx
	addq	16(%rcx), %rdx
	jmp	.LBB10_7
	.p2align	4, 0x90
.LBB10_6:                               #   in Loop: Header=BB10_1 Depth=1
	leaq	8(%r14), %rdx
.LBB10_7:                               # %.preheader.i
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	(%rdx), %r15
	testq	%r15, %r15
	jne	.LBB10_9
	jmp	.LBB10_13
	.p2align	4, 0x90
.LBB10_12:                              #   in Loop: Header=BB10_9 Depth=2
	movq	24(%r15), %r15
	testq	%r15, %r15
	je	.LBB10_13
.LBB10_9:                               # %.lr.ph.i
                                        #   Parent Loop BB10_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebp, 12(%r15)
	jne	.LBB10_12
# BB#10:                                #   in Loop: Header=BB10_9 Depth=2
	cmpl	%ebx, 8(%r15)
	jne	.LBB10_12
# BB#11:                                #   in Loop: Header=BB10_9 Depth=2
	movq	(%r15), %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB10_12
	jmp	.LBB10_14
	.p2align	4, 0x90
.LBB10_13:                              # %.critedge.i
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	32(%r14), %r14
	testq	%r14, %r14
	jne	.LBB10_1
.LBB10_2:
	xorl	%eax, %eax
	jmp	.LBB10_3
.LBB10_14:
	movq	16(%r15), %rax
	testq	%rax, %rax
	cmoveq	%r15, %rax
	movq	(%rsp), %rdx            # 8-byte Reload
	testq	%rdx, %rdx
	jne	.LBB10_16
	jmp	.LBB10_3
	.p2align	4, 0x90
.LBB10_21:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB10_16 Depth=1
	movq	48(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB10_3
.LBB10_16:                              # %.lr.ph24.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_18 Depth 2
	movq	24(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB10_18
	jmp	.LBB10_21
	.p2align	4, 0x90
.LBB10_20:                              #   in Loop: Header=BB10_18 Depth=2
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB10_21
.LBB10_18:                              # %.lr.ph.i.i
                                        #   Parent Loop BB10_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, 16(%rcx)
	jne	.LBB10_20
# BB#19:
	movq	%rcx, %rax
.LBB10_3:                               # %find_sym_internal.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	find_D_Sym, .Lfunc_end10-find_D_Sym
	.cfi_endproc

	.globl	find_D_Sym_in_Scope
	.p2align	4, 0x90
	.type	find_D_Sym_in_Scope,@function
find_D_Sym_in_Scope:                    # @find_D_Sym_in_Scope
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi83:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi84:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi86:
	.cfi_def_cfa_offset 64
.Lcfi87:
	.cfi_offset %rbx, -56
.Lcfi88:
	.cfi_offset %r12, -48
.Lcfi89:
	.cfi_offset %r13, -40
.Lcfi90:
	.cfi_offset %r14, -32
.Lcfi91:
	.cfi_offset %r15, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rsi, %r14
	movq	%rdi, %r15
	subq	%r14, %r13
	movq	%r14, %rdi
	movl	%r13d, %esi
	callq	strhashl
	movl	%eax, %ebp
	movslq	%r13d, %r12
	.p2align	4, 0x90
.LBB11_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_7 Depth 2
	testq	%r15, %r15
	je	.LBB11_13
# BB#2:                                 #   in Loop: Header=BB11_1 Depth=1
	movq	16(%r15), %rcx
	testq	%rcx, %rcx
	je	.LBB11_4
# BB#3:                                 #   in Loop: Header=BB11_1 Depth=1
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	8(%rcx)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	shlq	$3, %rdx
	addq	16(%rcx), %rdx
	jmp	.LBB11_5
	.p2align	4, 0x90
.LBB11_4:                               #   in Loop: Header=BB11_1 Depth=1
	leaq	8(%r15), %rdx
.LBB11_5:                               # %.preheader
                                        #   in Loop: Header=BB11_1 Depth=1
	movq	(%rdx), %rbx
	testq	%rbx, %rbx
	jne	.LBB11_7
	jmp	.LBB11_11
	.p2align	4, 0x90
.LBB11_10:                              #   in Loop: Header=BB11_7 Depth=2
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB11_11
.LBB11_7:                               # %.lr.ph
                                        #   Parent Loop BB11_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebp, 12(%rbx)
	jne	.LBB11_10
# BB#8:                                 #   in Loop: Header=BB11_7 Depth=2
	cmpl	%r13d, 8(%rbx)
	jne	.LBB11_10
# BB#9:                                 #   in Loop: Header=BB11_7 Depth=2
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB11_10
	jmp	.LBB11_14
	.p2align	4, 0x90
.LBB11_11:                              # %._crit_edge
                                        #   in Loop: Header=BB11_1 Depth=1
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB11_13
# BB#12:                                #   in Loop: Header=BB11_1 Depth=1
	movq	40(%rax), %rcx
	cmpq	40(%r15), %rcx
	movq	%rax, %r15
	je	.LBB11_1
.LBB11_13:
	xorl	%ebx, %ebx
.LBB11_14:                              # %.loopexit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	find_D_Sym_in_Scope, .Lfunc_end11-find_D_Sym_in_Scope
	.cfi_endproc

	.globl	update_D_Sym
	.p2align	4, 0x90
	.type	update_D_Sym,@function
update_D_Sym:                           # @update_D_Sym
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi95:
	.cfi_def_cfa_offset 32
.Lcfi96:
	.cfi_offset %rbx, -24
.Lcfi97:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	16(%rsi), %rbx
	testq	%rbx, %rbx
	cmoveq	%rsi, %rbx
	testq	%r14, %r14
	je	.LBB12_8
# BB#1:                                 # %.lr.ph24.i.preheader
	movq	%r14, %rax
.LBB12_2:                               # %.lr.ph24.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_4 Depth 2
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB12_4
	jmp	.LBB12_7
	.p2align	4, 0x90
.LBB12_6:                               #   in Loop: Header=BB12_4 Depth=2
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB12_7
.LBB12_4:                               # %.lr.ph.i
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, 16(%rcx)
	jne	.LBB12_6
	jmp	.LBB12_5
	.p2align	4, 0x90
.LBB12_7:                               # %._crit_edge.i
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	48(%rax), %rax
	testq	%rax, %rax
	jne	.LBB12_2
	jmp	.LBB12_8
.LBB12_5:
	movq	%rcx, %rbx
.LBB12_8:                               # %current_D_Sym.exit
	movslq	%edx, %rdi
	callq	malloc
	movq	32(%rbx), %rcx
	movq	%rcx, 32(%rax)
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	cmoveq	%rbx, %rcx
	movq	%rcx, 16(%rax)
	movq	24(%r14), %rcx
	movq	%rcx, 24(%rax)
	movq	%rax, 24(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	update_D_Sym, .Lfunc_end12-update_D_Sym
	.cfi_endproc

	.globl	print_sym
	.p2align	4, 0x90
	.type	print_sym,@function
print_sym:                              # @print_sym
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 32
.Lcfi101:
	.cfi_offset %rbx, -32
.Lcfi102:
	.cfi_offset %r14, -24
.Lcfi103:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movslq	8(%r14), %rbx
	leaq	1(%rbx), %rdi
	callq	malloc
	movq	%rax, %r15
	movq	(%r14), %r14
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movb	$0, (%r14,%rbx)
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	printf
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end13:
	.size	print_sym, .Lfunc_end13-print_sym
	.cfi_endproc

	.globl	print_scope
	.p2align	4, 0x90
	.type	print_scope,@function
print_scope:                            # @print_scope
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi106:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi107:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 48
.Lcfi109:
	.cfi_offset %rbx, -48
.Lcfi110:
	.cfi_offset %r12, -40
.Lcfi111:
	.cfi_offset %r13, -32
.Lcfi112:
	.cfi_offset %r14, -24
.Lcfi113:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	.p2align	4, 0x90
.LBB14_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_8 Depth 2
                                        #     Child Loop BB14_11 Depth 2
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
	movzbl	(%r14), %eax
	movl	%eax, %esi
	andl	$1, %esi
	shrb	%al
	andb	$3, %al
	movzbl	%al, %edx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	cmpq	$0, 8(%r14)
	je	.LBB14_3
# BB#2:                                 #   in Loop: Header=BB14_1 Depth=1
	movl	$.Lstr.2, %edi
	callq	puts
.LBB14_3:                               #   in Loop: Header=BB14_1 Depth=1
	cmpq	$0, 16(%r14)
	je	.LBB14_4
# BB#5:                                 #   in Loop: Header=BB14_1 Depth=1
	movl	$.Lstr.1, %edi
	callq	puts
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.LBB14_4
# BB#6:                                 # %.preheader
                                        #   in Loop: Header=BB14_1 Depth=1
	cmpl	$0, 8(%rax)
	je	.LBB14_13
# BB#7:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB14_1 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_8:                               # %.lr.ph
                                        #   Parent Loop BB14_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rax), %rcx
	movq	(%rcx,%rbx,8), %r12
	testq	%r12, %r12
	je	.LBB14_10
# BB#9:                                 #   in Loop: Header=BB14_8 Depth=2
	movslq	8(%r12), %r13
	leaq	1(%r13), %rdi
	callq	malloc
	movq	%rax, %r15
	movq	(%r12), %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	memcpy
	movb	$0, (%r12,%r13)
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	printf
	movq	%r15, %rdi
	callq	free
	movq	16(%r14), %rax
.LBB14_10:                              #   in Loop: Header=BB14_8 Depth=2
	incq	%rbx
	cmpl	8(%rax), %ebx
	jb	.LBB14_8
	jmp	.LBB14_13
	.p2align	4, 0x90
.LBB14_4:                               # %.thread.preheader
                                        #   in Loop: Header=BB14_1 Depth=1
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB14_13
	.p2align	4, 0x90
.LBB14_11:                              # %.thread
                                        #   Parent Loop BB14_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	8(%rbx), %r13
	leaq	1(%r13), %rdi
	callq	malloc
	movq	%rax, %r15
	movq	(%rbx), %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	memcpy
	movb	$0, (%r12,%r13)
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	printf
	movq	%r15, %rdi
	callq	free
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB14_11
.LBB14_13:                              # %.loopexit
                                        #   in Loop: Header=BB14_1 Depth=1
	movl	$.Lstr, %edi
	callq	puts
	movq	32(%r14), %r14
	testq	%r14, %r14
	jne	.LBB14_1
# BB#14:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	print_scope, .Lfunc_end14-print_scope
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s, "
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SCOPE %X: "
	.size	.L.str.1, 11

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"  owned: %d, kind: %d, "
	.size	.L.str.2, 24

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"\n"
	.size	.Lstr, 2

	.type	.Lstr.1,@object         # @str.1
.Lstr.1:
	.asciz	"  HASH"
	.size	.Lstr.1, 7

	.type	.Lstr.2,@object         # @str.2
.Lstr.2:
	.asciz	"  LL"
	.size	.Lstr.2, 5


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
