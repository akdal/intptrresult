	.text
	.file	"misc.bc"
	.globl	Z_setquiet
	.p2align	4, 0x90
	.type	Z_setquiet,@function
Z_setquiet:                             # @Z_setquiet
	.cfi_startproc
# BB#0:
	movb	$1, _Z_qflag(%rip)
	retq
.Lfunc_end0:
	.size	Z_setquiet, .Lfunc_end0-Z_setquiet
	.cfi_endproc

	.globl	Z_chatter
	.p2align	4, 0x90
	.type	Z_chatter,@function
Z_chatter:                              # @Z_chatter
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	_Z_qflag(%rip), %al
	testb	%al, %al
	je	.LBB1_2
# BB#1:
	popq	%rbx
	retq
.LBB1_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rsi
	movq	%rbx, %rdi
	popq	%rbx
	jmp	fputs                   # TAILCALL
.Lfunc_end1:
	.size	Z_chatter, .Lfunc_end1-Z_chatter
	.cfi_endproc

	.globl	Z_complain
	.p2align	4, 0x90
	.type	Z_complain,@function
Z_complain:                             # @Z_complain
	.cfi_startproc
# BB#0:
	movb	_Z_qflag(%rip), %al
	testb	%al, %al
	je	.LBB2_2
# BB#1:
	retq
.LBB2_2:
	movq	stderr(%rip), %rsi
	jmp	fputs                   # TAILCALL
.Lfunc_end2:
	.size	Z_complain, .Lfunc_end2-Z_complain
	.cfi_endproc

	.globl	_Z_qfatal
	.p2align	4, 0x90
	.type	_Z_qfatal,@function
_Z_qfatal:                              # @_Z_qfatal
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%eax, %eax
	callq	V_cleanup
	xorl	%eax, %eax
	callq	O_cleanup
	movq	%rbx, %rdi
	callq	Z_complain
	callq	_Z_errexit
.Lfunc_end3:
	.size	_Z_qfatal, .Lfunc_end3-_Z_qfatal
	.cfi_endproc

	.globl	Z_fatal
	.p2align	4, 0x90
	.type	Z_fatal,@function
Z_fatal:                                # @Z_fatal
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%eax, %eax
	callq	V_cleanup
	xorl	%eax, %eax
	callq	O_cleanup
	movq	stderr(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
	callq	_Z_errexit
.Lfunc_end4:
	.size	Z_fatal, .Lfunc_end4-Z_fatal
	.cfi_endproc

	.globl	_Z_myalloc
	.p2align	4, 0x90
	.type	_Z_myalloc,@function
_Z_myalloc:                             # @_Z_myalloc
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	movl	%edi, %edi
	movl	$1, %esi
	callq	calloc
	testq	%rax, %rax
	je	.LBB5_2
# BB#1:
	popq	%rcx
	retq
.LBB5_2:
	movl	$.L.str.1, %edi
	callq	Z_fatal
.Lfunc_end5:
	.size	_Z_myalloc, .Lfunc_end5-_Z_myalloc
	.cfi_endproc

	.globl	Z_exceed
	.p2align	4, 0x90
	.type	Z_exceed,@function
Z_exceed:                               # @Z_exceed
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 16
	movl	%edi, %ecx
	movl	$Z_err_buf, %edi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	sprintf
	movl	$Z_err_buf, %edi
	callq	_Z_qfatal
.Lfunc_end6:
	.size	Z_exceed, .Lfunc_end6-Z_exceed
	.cfi_endproc

	.p2align	4, 0x90
	.type	_Z_errexit,@function
_Z_errexit:                             # @_Z_errexit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 16
	movl	$2, %edi
	callq	exit
.Lfunc_end7:
	.size	_Z_errexit, .Lfunc_end7-_Z_errexit
	.cfi_endproc

	.type	_Z_qflag,@object        # @_Z_qflag
	.local	_Z_qflag
	.comm	_Z_qflag,1,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"spiff -- "
	.size	.L.str, 10

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Out of Memory\n"
	.size	.L.str.1, 15

	.type	Z_err_buf,@object       # @Z_err_buf
	.comm	Z_err_buf,1024,16
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"The files differ in more than %d places\n"
	.size	.L.str.2, 41


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
