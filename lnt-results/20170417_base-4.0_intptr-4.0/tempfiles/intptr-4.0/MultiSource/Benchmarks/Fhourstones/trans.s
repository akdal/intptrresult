	.text
	.file	"trans.bc"
	.globl	trans_init
	.p2align	4, 0x90
	.type	trans_init,@function
trans_init:                             # @trans_init
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	xorl	%eax, %eax
	callq	play_init
	movl	$1050011, %edi          # imm = 0x10059B
	movl	$4, %esi
	callq	calloc
	movq	%rax, %rbx
	movq	%rbx, ht(%rip)
	movl	$1050011, %edi          # imm = 0x10059B
	movl	$1, %esi
	callq	calloc
	movq	%rax, he(%rip)
	testq	%rbx, %rbx
	je	.LBB0_3
# BB#1:
	testq	%rax, %rax
	je	.LBB0_3
# BB#2:
	popq	%rbx
	retq
.LBB0_3:
	movl	$.L.str, %edi
	movl	$5250055, %esi          # imm = 0x501C07
	xorl	%eax, %eax
	callq	printf
	xorl	%edi, %edi
	callq	exit
.Lfunc_end0:
	.size	trans_init, .Lfunc_end0-trans_init
	.cfi_endproc

	.globl	emptyTT
	.p2align	4, 0x90
	.type	emptyTT,@function
emptyTT:                                # @emptyTT
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	movl	$4, %ecx
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_1 Depth=1
	addq	$2, %rax
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movq	he(%rip), %rdx
	movsbl	(%rdx,%rax), %esi
	movl	%esi, %edi
	andl	$31, %edi
	cmpl	$31, %edi
	je	.LBB1_3
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	cmpl	$16, %edi
	cmovael	%ecx, %edi
	subl	%edi, %esi
	movb	%sil, (%rdx,%rax)
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	cmpq	$1050010, %rax          # imm = 0x10059A
	je	.LBB1_7
# BB#4:                                 #   in Loop: Header=BB1_1 Depth=1
	movq	he(%rip), %rdx
	movsbl	1(%rdx,%rax), %esi
	movl	%esi, %edi
	andl	$31, %edi
	cmpl	$31, %edi
	je	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_1 Depth=1
	cmpl	$16, %edi
	cmovael	%ecx, %edi
	subl	%edi, %esi
	movb	%sil, 1(%rdx,%rax)
	jmp	.LBB1_6
.LBB1_7:
	movq	$0, hits(%rip)
	movq	$0, posed(%rip)
	retq
.Lfunc_end1:
	.size	emptyTT, .Lfunc_end1-emptyTT
	.cfi_endproc

	.globl	hitRate
	.p2align	4, 0x90
	.type	hitRate,@function
hitRate:                                # @hitRate
	.cfi_startproc
# BB#0:
	movq	posed(%rip), %rax
	testq	%rax, %rax
	je	.LBB2_1
# BB#2:
	cvtsi2sdq	hits(%rip), %xmm0
	cvtsi2sdq	%rax, %xmm1
	divsd	%xmm1, %xmm0
	retq
.LBB2_1:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end2:
	.size	hitRate, .Lfunc_end2-hitRate
	.cfi_endproc

	.globl	hash
	.p2align	4, 0x90
	.type	hash,@function
hash:                                   # @hash
	.cfi_startproc
# BB#0:
	movl	columns+4(%rip), %eax
	shll	$7, %eax
	orl	columns+8(%rip), %eax
	shll	$7, %eax
	orl	columns+12(%rip), %eax
	movl	columns+28(%rip), %ecx
	shll	$7, %ecx
	orl	columns+24(%rip), %ecx
	shll	$7, %ecx
	orl	columns+20(%rip), %ecx
	cmpl	%ecx, %eax
	movl	%ecx, %edx
	cmoval	%eax, %edx
	cmovbel	%eax, %ecx
	shll	$7, %edx
	orl	columns+16(%rip), %edx
	shlq	$21, %rdx
	orq	%rdx, %rcx
	movq	%rcx, %rsi
	shrq	$17, %rsi
	movl	%esi, lock(%rip)
	movabsq	$9210766893791620657, %rdx # imm = 0x7FD337B06D60AE31
	movq	%rcx, %rax
	mulq	%rdx
	shrq	$19, %rdx
	imull	$1050011, %edx, %eax    # imm = 0x10059B
	subl	%eax, %ecx
	movl	%ecx, htindex(%rip)
	movl	%esi, %eax
	imulq	$1847555765, %rax, %rax # imm = 0x6E1F76B5
	shrq	$32, %rax
	movl	%esi, %ecx
	subl	%eax, %ecx
	shrl	%ecx
	addl	%eax, %ecx
	shrl	$7, %ecx
	imull	$179, %ecx, %eax
	subl	%eax, %esi
	orl	$131072, %esi           # imm = 0x20000
	movl	%esi, stride(%rip)
	retq
.Lfunc_end3:
	.size	hash, .Lfunc_end3-hash
	.cfi_endproc

	.globl	transpose
	.p2align	4, 0x90
	.type	transpose,@function
transpose:                              # @transpose
	.cfi_startproc
# BB#0:
	movl	columns+4(%rip), %eax
	shll	$7, %eax
	orl	columns+8(%rip), %eax
	shll	$7, %eax
	orl	columns+12(%rip), %eax
	movl	columns+28(%rip), %ecx
	shll	$7, %ecx
	orl	columns+24(%rip), %ecx
	shll	$7, %ecx
	orl	columns+20(%rip), %ecx
	cmpl	%ecx, %eax
	movl	%ecx, %edx
	cmoval	%eax, %edx
	cmovbel	%eax, %ecx
	shll	$7, %edx
	orl	columns+16(%rip), %edx
	shlq	$21, %rdx
	orq	%rdx, %rcx
	movq	%rcx, %rsi
	shrq	$17, %rsi
	movl	%esi, lock(%rip)
	movabsq	$9210766893791620657, %rdx # imm = 0x7FD337B06D60AE31
	movq	%rcx, %rax
	mulq	%rdx
	shrq	$19, %rdx
	imulq	$1050011, %rdx, %rax    # imm = 0x10059B
	subq	%rax, %rcx
	movl	%ecx, htindex(%rip)
	movl	%esi, %eax
	imulq	$1847555765, %rax, %rax # imm = 0x6E1F76B5
	shrq	$32, %rax
	movl	%esi, %edx
	subl	%eax, %edx
	shrl	%edx
	addl	%eax, %edx
	shrl	$7, %edx
	imull	$179, %edx, %edx
	movl	%esi, %eax
	subl	%edx, %eax
	orl	$131072, %eax           # imm = 0x20000
	movl	%eax, stride(%rip)
	movq	ht(%rip), %r8
	cmpl	%esi, (%r8,%rcx,4)
	je	.LBB4_1
# BB#3:
	leal	(%rax,%rcx), %edx
	cmpl	$1050010, %edx          # imm = 0x10059A
	leal	-1050011(%rax,%rcx), %edi
	cmovbel	%edx, %edi
	movslq	%edi, %rcx
	cmpl	%esi, (%r8,%rcx,4)
	je	.LBB4_1
# BB#4:
	movl	%edi, %ecx
	addl	%eax, %ecx
	cmpl	$1050010, %ecx          # imm = 0x10059A
	leal	-1050011(%rax,%rdi), %edi
	cmovlel	%ecx, %edi
	movslq	%edi, %rcx
	cmpl	%esi, (%r8,%rcx,4)
	je	.LBB4_1
# BB#5:
	movl	%edi, %ecx
	addl	%eax, %ecx
	cmpl	$1050010, %ecx          # imm = 0x10059A
	leal	-1050011(%rax,%rdi), %edi
	cmovlel	%ecx, %edi
	movslq	%edi, %rcx
	cmpl	%esi, (%r8,%rcx,4)
	je	.LBB4_1
# BB#6:
	movl	%edi, %ecx
	addl	%eax, %ecx
	cmpl	$1050010, %ecx          # imm = 0x10059A
	leal	-1050011(%rax,%rdi), %edi
	cmovlel	%ecx, %edi
	movslq	%edi, %rcx
	cmpl	%esi, (%r8,%rcx,4)
	je	.LBB4_1
# BB#7:
	movl	%edi, %ecx
	addl	%eax, %ecx
	cmpl	$1050010, %ecx          # imm = 0x10059A
	leal	-1050011(%rax,%rdi), %edi
	cmovlel	%ecx, %edi
	movslq	%edi, %rcx
	cmpl	%esi, (%r8,%rcx,4)
	je	.LBB4_1
# BB#8:
	movl	%edi, %ecx
	addl	%eax, %ecx
	cmpl	$1050010, %ecx          # imm = 0x10059A
	leal	-1050011(%rax,%rdi), %edi
	cmovlel	%ecx, %edi
	movslq	%edi, %rcx
	cmpl	%esi, (%r8,%rcx,4)
	je	.LBB4_1
# BB#9:
	movl	%edi, %ecx
	addl	%eax, %ecx
	cmpl	$1050010, %ecx          # imm = 0x10059A
	leal	-1050011(%rax,%rdi), %eax
	cmovlel	%ecx, %eax
	movslq	%eax, %rcx
	movl	$-128, %eax
	cmpl	%esi, (%r8,%rcx,4)
	jne	.LBB4_2
.LBB4_1:
	movq	he(%rip), %rax
	movsbl	(%rax,%rcx), %eax
.LBB4_2:                                # %.loopexit
	retq
.Lfunc_end4:
	.size	transpose, .Lfunc_end4-transpose
	.cfi_endproc

	.globl	transput
	.p2align	4, 0x90
	.type	transput,@function
transput:                               # @transput
	.cfi_startproc
# BB#0:
	movq	he(%rip), %r9
	movslq	htindex(%rip), %rdx
	movzbl	(%r9,%rdx), %ecx
	andl	$31, %ecx
	cmpl	%esi, %ecx
	jl	.LBB5_1
# BB#3:
	movl	stride(%rip), %r8d
	leal	(%r8,%rdx), %eax
	cmpl	$1050010, %eax          # imm = 0x10059A
	leal	-1050011(%r8,%rdx), %ecx
	cmovlel	%eax, %ecx
	movslq	%ecx, %rdx
	movzbl	(%r9,%rdx), %eax
	andl	$31, %eax
	cmpl	%esi, %eax
	jl	.LBB5_1
# BB#4:
	movl	%ecx, %eax
	addl	%r8d, %eax
	cmpl	$1050010, %eax          # imm = 0x10059A
	leal	-1050011(%r8,%rcx), %ecx
	cmovlel	%eax, %ecx
	movslq	%ecx, %rdx
	movzbl	(%r9,%rdx), %eax
	andl	$31, %eax
	cmpl	%esi, %eax
	jl	.LBB5_1
# BB#5:
	movl	%ecx, %eax
	addl	%r8d, %eax
	cmpl	$1050010, %eax          # imm = 0x10059A
	leal	-1050011(%r8,%rcx), %ecx
	cmovlel	%eax, %ecx
	movslq	%ecx, %rdx
	movzbl	(%r9,%rdx), %eax
	andl	$31, %eax
	cmpl	%esi, %eax
	jl	.LBB5_1
# BB#6:
	movl	%ecx, %eax
	addl	%r8d, %eax
	cmpl	$1050010, %eax          # imm = 0x10059A
	leal	-1050011(%r8,%rcx), %ecx
	cmovlel	%eax, %ecx
	movslq	%ecx, %rdx
	movzbl	(%r9,%rdx), %eax
	andl	$31, %eax
	cmpl	%esi, %eax
	jl	.LBB5_1
# BB#7:
	movl	%ecx, %eax
	addl	%r8d, %eax
	cmpl	$1050010, %eax          # imm = 0x10059A
	leal	-1050011(%r8,%rcx), %ecx
	cmovlel	%eax, %ecx
	movslq	%ecx, %rdx
	movzbl	(%r9,%rdx), %eax
	andl	$31, %eax
	cmpl	%esi, %eax
	jl	.LBB5_1
# BB#8:
	movl	%ecx, %eax
	addl	%r8d, %eax
	cmpl	$1050010, %eax          # imm = 0x10059A
	leal	-1050011(%r8,%rcx), %ecx
	cmovlel	%eax, %ecx
	movslq	%ecx, %rdx
	movzbl	(%r9,%rdx), %eax
	andl	$31, %eax
	cmpl	%esi, %eax
	jl	.LBB5_1
# BB#9:
	movl	%ecx, %eax
	addl	%r8d, %eax
	cmpl	$1050010, %eax          # imm = 0x10059A
	leal	-1050011(%r8,%rcx), %ecx
	cmovlel	%eax, %ecx
	movslq	%ecx, %rdx
	movzbl	(%r9,%rdx), %eax
	andl	$31, %eax
	cmpl	%esi, %eax
	jge	.LBB5_2
.LBB5_1:
	incq	hits(%rip)
	movl	lock(%rip), %ecx
	movq	ht(%rip), %rax
	movl	%ecx, (%rax,%rdx,4)
	shll	$5, %edi
	orl	%esi, %edi
	movb	%dil, (%r9,%rdx)
.LBB5_2:                                # %.loopexit
	retq
.Lfunc_end5:
	.size	transput, .Lfunc_end5-transput
	.cfi_endproc

	.globl	transrestore
	.p2align	4, 0x90
	.type	transrestore,@function
transrestore:                           # @transrestore
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	cmpl	$32, %eax
	movl	$31, %esi
	cmovll	%eax, %esi
	incq	posed(%rip)
	movl	columns+4(%rip), %eax
	shll	$7, %eax
	orl	columns+8(%rip), %eax
	shll	$7, %eax
	orl	columns+12(%rip), %eax
	movl	columns+28(%rip), %ecx
	shll	$7, %ecx
	orl	columns+24(%rip), %ecx
	shll	$7, %ecx
	orl	columns+20(%rip), %ecx
	cmpl	%ecx, %eax
	movl	%ecx, %edx
	cmoval	%eax, %edx
	cmovbel	%eax, %ecx
	shll	$7, %edx
	orl	columns+16(%rip), %edx
	shlq	$21, %rdx
	orq	%rdx, %rcx
	movq	%rcx, %r8
	shrq	$17, %r8
	movl	%r8d, lock(%rip)
	movabsq	$9210766893791620657, %rdx # imm = 0x7FD337B06D60AE31
	movq	%rcx, %rax
	mulq	%rdx
	shrq	$19, %rdx
	imulq	$1050011, %rdx, %rax    # imm = 0x10059B
	subq	%rax, %rcx
	movl	%ecx, htindex(%rip)
	movl	%r8d, %eax
	imulq	$1847555765, %rax, %rax # imm = 0x6E1F76B5
	shrq	$32, %rax
	movl	%r8d, %edx
	subl	%eax, %edx
	shrl	%edx
	addl	%eax, %edx
	shrl	$7, %edx
	imull	$179, %edx, %edx
	movl	%r8d, %eax
	subl	%edx, %eax
	orl	$131072, %eax           # imm = 0x20000
	movl	%eax, stride(%rip)
	movq	ht(%rip), %r9
	cmpl	%r8d, (%r9,%rcx,4)
	je	.LBB6_8
# BB#1:
	leal	(%rax,%rcx), %r10d
	cmpl	$1050010, %r10d         # imm = 0x10059A
	leal	-1050011(%rax,%rcx), %edx
	cmovbel	%r10d, %edx
	movslq	%edx, %rcx
	cmpl	%r8d, (%r9,%rcx,4)
	je	.LBB6_8
# BB#2:
	movl	%edx, %ecx
	addl	%eax, %ecx
	cmpl	$1050010, %ecx          # imm = 0x10059A
	leal	-1050011(%rax,%rdx), %edx
	cmovlel	%ecx, %edx
	movslq	%edx, %rcx
	cmpl	%r8d, (%r9,%rcx,4)
	je	.LBB6_8
# BB#3:
	movl	%edx, %ecx
	addl	%eax, %ecx
	cmpl	$1050010, %ecx          # imm = 0x10059A
	leal	-1050011(%rax,%rdx), %edx
	cmovlel	%ecx, %edx
	movslq	%edx, %rcx
	cmpl	%r8d, (%r9,%rcx,4)
	je	.LBB6_8
# BB#4:
	movl	%edx, %ecx
	addl	%eax, %ecx
	cmpl	$1050010, %ecx          # imm = 0x10059A
	leal	-1050011(%rax,%rdx), %edx
	cmovlel	%ecx, %edx
	movslq	%edx, %rcx
	cmpl	%r8d, (%r9,%rcx,4)
	je	.LBB6_8
# BB#5:
	movl	%edx, %ecx
	addl	%eax, %ecx
	cmpl	$1050010, %ecx          # imm = 0x10059A
	leal	-1050011(%rax,%rdx), %edx
	cmovlel	%ecx, %edx
	movslq	%edx, %rcx
	cmpl	%r8d, (%r9,%rcx,4)
	je	.LBB6_8
# BB#6:
	movl	%edx, %ecx
	addl	%eax, %ecx
	cmpl	$1050010, %ecx          # imm = 0x10059A
	leal	-1050011(%rax,%rdx), %edx
	cmovlel	%ecx, %edx
	movslq	%edx, %rcx
	cmpl	%r8d, (%r9,%rcx,4)
	je	.LBB6_8
# BB#7:
	movl	%edx, %ecx
	addl	%eax, %ecx
	cmpl	$1050010, %ecx          # imm = 0x10059A
	leal	-1050011(%rax,%rdx), %eax
	cmovlel	%ecx, %eax
	movslq	%eax, %rcx
	cmpl	%r8d, (%r9,%rcx,4)
	jne	.LBB6_9
.LBB6_8:
	incq	hits(%rip)
	shll	$5, %edi
	orl	%edi, %esi
	movq	he(%rip), %rax
	movb	%sil, (%rax,%rcx)
	retq
.LBB6_9:
	jmp	transput                # TAILCALL
.Lfunc_end6:
	.size	transrestore, .Lfunc_end6-transrestore
	.cfi_endproc

	.globl	transtore
	.p2align	4, 0x90
	.type	transtore,@function
transtore:                              # @transtore
	.cfi_startproc
# BB#0:
	cmpl	$32, %esi
	movl	$31, %eax
	cmovgel	%eax, %esi
	incq	posed(%rip)
	movl	columns+4(%rip), %eax
	shll	$7, %eax
	orl	columns+8(%rip), %eax
	shll	$7, %eax
	orl	columns+12(%rip), %eax
	movl	columns+28(%rip), %ecx
	shll	$7, %ecx
	orl	columns+24(%rip), %ecx
	shll	$7, %ecx
	orl	columns+20(%rip), %ecx
	cmpl	%ecx, %eax
	movl	%ecx, %edx
	cmoval	%eax, %edx
	cmovbel	%eax, %ecx
	shll	$7, %edx
	orl	columns+16(%rip), %edx
	shlq	$21, %rdx
	orq	%rdx, %rcx
	movq	%rcx, %r8
	shrq	$17, %r8
	movl	%r8d, lock(%rip)
	movabsq	$9210766893791620657, %rdx # imm = 0x7FD337B06D60AE31
	movq	%rcx, %rax
	mulq	%rdx
	shrq	$19, %rdx
	imull	$1050011, %edx, %eax    # imm = 0x10059B
	subl	%eax, %ecx
	movl	%ecx, htindex(%rip)
	movl	%r8d, %eax
	imulq	$1847555765, %rax, %rax # imm = 0x6E1F76B5
	shrq	$32, %rax
	movl	%r8d, %ecx
	subl	%eax, %ecx
	shrl	%ecx
	addl	%eax, %ecx
	shrl	$7, %ecx
	imull	$179, %ecx, %eax
	subl	%eax, %r8d
	orl	$131072, %r8d           # imm = 0x20000
	movl	%r8d, stride(%rip)
	jmp	transput                # TAILCALL
.Lfunc_end7:
	.size	transtore, .Lfunc_end7-transtore
	.cfi_endproc

	.globl	htstat
	.p2align	4, 0x90
	.type	htstat,@function
htstat:                                 # @htstat
	.cfi_startproc
# BB#0:                                 # %.preheader31.preheader
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 224
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 144(%rsp)
	movapd	%xmm0, 128(%rsp)
	movapd	%xmm0, 112(%rsp)
	movapd	%xmm0, 96(%rsp)
	movapd	%xmm0, 80(%rsp)
	movapd	%xmm0, 64(%rsp)
	movapd	%xmm0, 48(%rsp)
	movapd	%xmm0, 32(%rsp)
	movapd	%xmm0, 16(%rsp)
	movapd	%xmm0, (%rsp)
	xorl	%eax, %eax
	movq	he(%rip), %rcx
	.p2align	4, 0x90
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	movsbl	(%rcx,%rax), %edx
	movl	%edx, %esi
	andl	$31, %esi
	incl	32(%rsp,%rsi,4)
	testb	$31, %dl
	je	.LBB8_3
# BB#2:                                 #   in Loop: Header=BB8_1 Depth=1
	sarl	$5, %edx
	movslq	%edx, %rdx
	incl	16(%rsp,%rdx,4)
.LBB8_3:                                #   in Loop: Header=BB8_1 Depth=1
	incq	%rax
	cmpq	$1050011, %rax          # imm = 0x10059B
	jne	.LBB8_1
# BB#4:                                 # %.preheader29.preheader
	movl	4(%rsp), %ebx
	movl	8(%rsp), %r14d
	addl	(%rsp), %ebx
	addl	%r14d, %ebx
	movl	12(%rsp), %ebp
	addl	%ebp, %ebx
	movl	16(%rsp), %r15d
	addl	%r15d, %ebx
	movl	20(%rsp), %r12d
	addl	%r12d, %ebx
	movl	24(%rsp), %r13d
	addl	%r13d, %ebx
	addl	28(%rsp), %ebx
	jle	.LBB8_9
# BB#5:
	movq	posed(%rip), %rax
	testq	%rax, %rax
	je	.LBB8_6
# BB#7:
	xorps	%xmm0, %xmm0
	cvtsi2sdq	hits(%rip), %xmm0
	cvtsi2sdq	%rax, %xmm1
	divsd	%xmm1, %xmm0
	jmp	.LBB8_8
.LBB8_6:
	xorpd	%xmm0, %xmm0
.LBB8_8:                                # %hitRate.exit
	movl	$.L.str.1, %edi
	movb	$1, %al
	callq	printf
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	cvtsi2sdl	%ebx, %xmm5
	divsd	%xmm5, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	divsd	%xmm5, %xmm1
	cvtsi2sdl	%r15d, %xmm2
	divsd	%xmm5, %xmm2
	cvtsi2sdl	%r12d, %xmm3
	divsd	%xmm5, %xmm3
	cvtsi2sdl	%r13d, %xmm4
	divsd	%xmm5, %xmm4
	movl	$.L.str.2, %edi
	movb	$5, %al
	callq	printf
.LBB8_9:                                # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_10:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	32(%rsp,%rbx,4), %esi
	movl	%ebx, %eax
	andl	$7, %eax
	xorl	%edx, %edx
	cmpq	$7, %rax
	sete	%dl
	addl	$9, %edx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	cmpq	$32, %rbx
	jne	.LBB8_10
# BB#11:
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	htstat, .Lfunc_end8-htstat
	.cfi_endproc

	.type	ht,@object              # @ht
	.comm	ht,8,8
	.type	he,@object              # @he
	.comm	he,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Failed to allocate %u bytes.\n"
	.size	.L.str, 30

	.type	hits,@object            # @hits
	.comm	hits,8,8
	.type	posed,@object           # @posed
	.comm	posed,8,8
	.type	lock,@object            # @lock
	.comm	lock,4,4
	.type	htindex,@object         # @htindex
	.comm	htindex,4,4
	.type	stride,@object          # @stride
	.comm	stride,4,4
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"store rate = %.3f\n"
	.size	.L.str.1, 19

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"- %5.3f  < %5.3f  = %5.3f  > %5.3f  + %5.3f\n"
	.size	.L.str.2, 45

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%7d%c"
	.size	.L.str.3, 6


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
