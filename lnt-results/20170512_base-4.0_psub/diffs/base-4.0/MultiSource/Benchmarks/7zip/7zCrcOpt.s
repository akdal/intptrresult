	.text
	.file	"7zCrcOpt.bc"
	.globl	CrcUpdateT4
	.p2align	4, 0x90
	.type	CrcUpdateT4,@function
CrcUpdateT4:                            # @CrcUpdateT4
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
.Lcfi2:
	.cfi_offset %rbx, -24
.Lcfi3:
	.cfi_offset %rbp, -16
	movl	%edi, %eax
	testq	%rdx, %rdx
	je	.LBB0_10
# BB#1:                                 # %.lr.ph50.preheader
	leaq	-4(%rdx), %r9
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph50
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rsi,%r10), %rdi
	testb	$3, %dil
	je	.LBB0_3
# BB#9:                                 #   in Loop: Header=BB0_2 Depth=1
	movzbl	(%rdi), %edi
	movzbl	%al, %ebx
	xorl	%edi, %ebx
	shrl	$8, %eax
	xorl	(%rcx,%rbx,4), %eax
	incq	%r10
	decq	%r9
	cmpq	%r10, %rdx
	jne	.LBB0_2
	jmp	.LBB0_10
.LBB0_3:                                # %.critedge.preheader
	movq	%rdx, %r8
	subq	%r10, %r8
	cmpq	$3, %r8
	jbe	.LBB0_4
	.p2align	4, 0x90
.LBB0_11:                               # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	xorl	(%rdi), %eax
	movzbl	%al, %r11d
	orl	$768, %r11d             # imm = 0x300
	movzbl	%ah, %ebp  # NOREX
	orl	$512, %ebp              # imm = 0x200
	movl	%eax, %ebx
	movl	(%rcx,%rbp,4), %ebp
	xorl	(%rcx,%r11,4), %ebp
	shrl	$16, %eax
	movzbl	%al, %eax
	orl	$256, %eax              # imm = 0x100
	xorl	(%rcx,%rax,4), %ebp
	shrl	$24, %ebx
	movl	%ebp, %eax
	xorl	(%rcx,%rbx,4), %eax
	addq	$-4, %r8
	addq	$4, %rdi
	cmpq	$3, %r8
	ja	.LBB0_11
# BB#12:                                # %.preheader
	andq	$-4, %r9
	movq	$-4, %r8
	subq	%r9, %r8
	addq	%rdx, %r8
	subq	%r10, %r8
	je	.LBB0_10
# BB#13:
	addq	%r9, %rsi
	leaq	4(%r10,%rsi), %rdi
.LBB0_4:                                # %.lr.ph.preheader
	testb	$1, %r8b
	jne	.LBB0_6
# BB#5:
	movq	%r8, %rdx
	cmpq	$1, %r8
	jne	.LBB0_8
	jmp	.LBB0_10
.LBB0_6:                                # %.lr.ph.prol
	movzbl	(%rdi), %edx
	movzbl	%al, %esi
	xorl	%edx, %esi
	shrl	$8, %eax
	xorl	(%rcx,%rsi,4), %eax
	leaq	-1(%r8), %rdx
	incq	%rdi
	cmpq	$1, %r8
	je	.LBB0_10
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %esi
	movzbl	%al, %ebp
	xorl	%esi, %ebp
	shrl	$8, %eax
	xorl	(%rcx,%rbp,4), %eax
	movzbl	1(%rdi), %esi
	movzbl	%al, %ebp
	xorl	%esi, %ebp
	shrl	$8, %eax
	xorl	(%rcx,%rbp,4), %eax
	addq	$2, %rdi
	addq	$-2, %rdx
	jne	.LBB0_8
.LBB0_10:                               # %._crit_edge
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	CrcUpdateT4, .Lfunc_end0-CrcUpdateT4
	.cfi_endproc

	.globl	CrcUpdateT8
	.p2align	4, 0x90
	.type	CrcUpdateT8,@function
CrcUpdateT8:                            # @CrcUpdateT8
	.cfi_startproc
# BB#0:
	jmp	CrcUpdateT4             # TAILCALL
.Lfunc_end1:
	.size	CrcUpdateT8, .Lfunc_end1-CrcUpdateT8
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
