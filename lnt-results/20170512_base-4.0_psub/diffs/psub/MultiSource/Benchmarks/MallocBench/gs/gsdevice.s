	.text
	.file	"gsdevice.bc"
	.globl	gx_default_open_device
	.p2align	4, 0x90
	.type	gx_default_open_device,@function
gx_default_open_device:                 # @gx_default_open_device
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	gx_default_open_device, .Lfunc_end0-gx_default_open_device
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1116733440              # float 72
.LCPI1_1:
	.long	3264217088              # float -72
	.text
	.globl	gx_default_get_initial_matrix
	.p2align	4, 0x90
	.type	gx_default_get_initial_matrix,@function
gx_default_get_initial_matrix:          # @gx_default_get_initial_matrix
	.cfi_startproc
# BB#0:
	movss	32(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	divss	.LCPI1_0(%rip), %xmm0
	movss	%xmm0, (%rsi)
	movl	$0, 16(%rsi)
	movl	$0, 32(%rsi)
	movss	36(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	divss	.LCPI1_1(%rip), %xmm0
	movss	%xmm0, 48(%rsi)
	movl	$0, 64(%rsi)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	28(%rdi), %xmm0
	movss	%xmm0, 80(%rsi)
	retq
.Lfunc_end1:
	.size	gx_default_get_initial_matrix, .Lfunc_end1-gx_default_get_initial_matrix
	.cfi_endproc

	.globl	gx_default_sync_output
	.p2align	4, 0x90
	.type	gx_default_sync_output,@function
gx_default_sync_output:                 # @gx_default_sync_output
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	gx_default_sync_output, .Lfunc_end2-gx_default_sync_output
	.cfi_endproc

	.globl	gx_default_output_page
	.p2align	4, 0x90
	.type	gx_default_output_page,@function
gx_default_output_page:                 # @gx_default_output_page
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.Lfunc_end3:
	.size	gx_default_output_page, .Lfunc_end3-gx_default_output_page
	.cfi_endproc

	.globl	gx_default_close_device
	.p2align	4, 0x90
	.type	gx_default_close_device,@function
gx_default_close_device:                # @gx_default_close_device
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	gx_default_close_device, .Lfunc_end4-gx_default_close_device
	.cfi_endproc

	.globl	gx_default_map_rgb_color
	.p2align	4, 0x90
	.type	gx_default_map_rgb_color,@function
gx_default_map_rgb_color:               # @gx_default_map_rgb_color
	.cfi_startproc
# BB#0:
	cmpw	%dx, %si
	cmovaw	%si, %dx
	cmpw	%cx, %dx
	cmovbew	%cx, %dx
	movzwl	%dx, %eax
	retq
.Lfunc_end5:
	.size	gx_default_map_rgb_color, .Lfunc_end5-gx_default_map_rgb_color
	.cfi_endproc

	.globl	gx_default_map_color_rgb
	.p2align	4, 0x90
	.type	gx_default_map_color_rgb,@function
gx_default_map_color_rgb:               # @gx_default_map_color_rgb
	.cfi_startproc
# BB#0:
	movw	%si, 4(%rdx)
	movw	%si, 2(%rdx)
	movw	%si, (%rdx)
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	gx_default_map_color_rgb, .Lfunc_end6-gx_default_map_color_rgb
	.cfi_endproc

	.globl	null_fill_rectangle
	.p2align	4, 0x90
	.type	null_fill_rectangle,@function
null_fill_rectangle:                    # @null_fill_rectangle
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	null_fill_rectangle, .Lfunc_end7-null_fill_rectangle
	.cfi_endproc

	.globl	null_tile_rectangle
	.p2align	4, 0x90
	.type	null_tile_rectangle,@function
null_tile_rectangle:                    # @null_tile_rectangle
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	null_tile_rectangle, .Lfunc_end8-null_tile_rectangle
	.cfi_endproc

	.globl	null_copy_mono
	.p2align	4, 0x90
	.type	null_copy_mono,@function
null_copy_mono:                         # @null_copy_mono
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	null_copy_mono, .Lfunc_end9-null_copy_mono
	.cfi_endproc

	.globl	null_copy_color
	.p2align	4, 0x90
	.type	null_copy_color,@function
null_copy_color:                        # @null_copy_color
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	null_copy_color, .Lfunc_end10-null_copy_color
	.cfi_endproc

	.globl	null_draw_line
	.p2align	4, 0x90
	.type	null_draw_line,@function
null_draw_line:                         # @null_draw_line
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	null_draw_line, .Lfunc_end11-null_draw_line
	.cfi_endproc

	.globl	null_fill_trapezoid
	.p2align	4, 0x90
	.type	null_fill_trapezoid,@function
null_fill_trapezoid:                    # @null_fill_trapezoid
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end12:
	.size	null_fill_trapezoid, .Lfunc_end12-null_fill_trapezoid
	.cfi_endproc

	.globl	null_tile_trapezoid
	.p2align	4, 0x90
	.type	null_tile_trapezoid,@function
null_tile_trapezoid:                    # @null_tile_trapezoid
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end13:
	.size	null_tile_trapezoid, .Lfunc_end13-null_tile_trapezoid
	.cfi_endproc

	.globl	gs_flushpage
	.p2align	4, 0x90
	.type	gs_flushpage,@function
gs_flushpage:                           # @gs_flushpage
	.cfi_startproc
# BB#0:
	movq	448(%rdi), %rax
	movq	(%rax), %rdi
	movq	8(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.Lfunc_end14:
	.size	gs_flushpage, .Lfunc_end14-gs_flushpage
	.cfi_endproc

	.globl	gs_copypage
	.p2align	4, 0x90
	.type	gs_copypage,@function
gs_copypage:                            # @gs_copypage
	.cfi_startproc
# BB#0:
	movq	448(%rdi), %rax
	movq	(%rax), %rdi
	movq	8(%rdi), %rax
	jmpq	*24(%rax)               # TAILCALL
.Lfunc_end15:
	.size	gs_copypage, .Lfunc_end15-gs_copypage
	.cfi_endproc

	.globl	gs_copyscanlines
	.p2align	4, 0x90
	.type	gs_copyscanlines,@function
gs_copyscanlines:                       # @gs_copyscanlines
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movl	%ecx, %r12d
	movq	%rdx, %r13
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	gs_device_is_memory
	testl	%eax, %eax
	je	.LBB16_1
# BB#2:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r13, %rdx
	movl	%r12d, %ecx
	callq	mem_copy_scan_lines
	movl	%eax, %r12d
	testq	%r15, %r15
	je	.LBB16_4
# BB#3:
	movl	%r12d, (%r15)
.LBB16_4:
	xorl	%ebp, %ebp
	testq	%r14, %r14
	je	.LBB16_6
# BB#5:
	movq	%rbx, %rdi
	callq	mem_bytes_per_scan_line
	imull	%r12d, %eax
	movl	%eax, (%r14)
	jmp	.LBB16_6
.LBB16_1:
	movl	$-21, %ebp
.LBB16_6:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	gs_copyscanlines, .Lfunc_end16-gs_copyscanlines
	.cfi_endproc

	.globl	gs_currentdevice
	.p2align	4, 0x90
	.type	gs_currentdevice,@function
gs_currentdevice:                       # @gs_currentdevice
	.cfi_startproc
# BB#0:
	movq	448(%rdi), %rax
	movq	(%rax), %rax
	retq
.Lfunc_end17:
	.size	gs_currentdevice, .Lfunc_end17-gs_currentdevice
	.cfi_endproc

	.globl	gs_devicename
	.p2align	4, 0x90
	.type	gs_devicename,@function
gs_devicename:                          # @gs_devicename
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end18:
	.size	gs_devicename, .Lfunc_end18-gs_devicename
	.cfi_endproc

	.globl	gs_deviceparams
	.p2align	4, 0x90
	.type	gs_deviceparams,@function
gs_deviceparams:                        # @gs_deviceparams
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	callq	*8(%rax)
	movl	24(%rbx), %eax
	movl	%eax, (%r15)
	movl	28(%rbx), %eax
	movl	%eax, (%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end19:
	.size	gs_deviceparams, .Lfunc_end19-gs_deviceparams
	.cfi_endproc

	.globl	gs_getdevice
	.p2align	4, 0x90
	.type	gs_getdevice,@function
gs_getdevice:                           # @gs_getdevice
	.cfi_startproc
# BB#0:
	movq	gx_device_list(%rip), %rax
	testq	%rax, %rax
	je	.LBB20_1
# BB#4:                                 # %.lr.ph.preheader
	movl	%edi, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB20_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rdx, %rcx
	je	.LBB20_6
# BB#2:                                 #   in Loop: Header=BB20_5 Depth=1
	movq	gx_device_list+8(,%rdx,8), %rax
	incq	%rdx
	testq	%rax, %rax
	jne	.LBB20_5
# BB#3:
	xorl	%eax, %eax
	retq
.LBB20_1:
	xorl	%eax, %eax
	retq
.LBB20_6:                               # %._crit_edge
	retq
.Lfunc_end20:
	.size	gs_getdevice, .Lfunc_end20-gs_getdevice
	.cfi_endproc

	.globl	gs_makedevice
	.p2align	4, 0x90
	.type	gs_makedevice,@function
gs_makedevice:                          # @gs_makedevice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	movl	%ecx, %r12d
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	(%rbx), %esi
	movl	$1, %edi
	movl	$.L.str.1, %edx
	callq	*%r9
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB21_1
# BB#2:
	testl	%r12d, %r12d
	movl	$-15, %eax
	je	.LBB21_5
# BB#3:
	testl	%r15d, %r15d
	je	.LBB21_5
# BB#4:
	movslq	(%rbx), %rdx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	memcpy
	movl	%r12d, 24(%rbp)
	movl	%r15d, 28(%rbp)
	movl	$0, 52(%rbp)
	movq	%rbp, (%r14)
	xorl	%eax, %eax
	jmp	.LBB21_5
.LBB21_1:
	movl	$-25, %eax
.LBB21_5:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	gs_makedevice, .Lfunc_end21-gs_makedevice
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI22_0:
	.quad	-4661117527937406468    # double -0.001
.LCPI22_1:
	.quad	4607186922399644778     # double 1.0009999999999999
.LCPI22_3:
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI22_2:
	.long	1132396544              # float 255
	.text
	.globl	gs_makeimagedevice
	.p2align	4, 0x90
	.type	gs_makeimagedevice,@function
gs_makeimagedevice:                     # @gs_makeimagedevice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$808, %rsp              # imm = 0x328
.Lcfi35:
	.cfi_def_cfa_offset 864
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	testl	%edx, %edx
	movl	$-15, %eax
	je	.LBB22_29
# BB#1:
	testl	%ecx, %ecx
	je	.LBB22_29
# BB#2:
	movl	$1, %r15d
	cmpl	$1, %r12d
	jg	.LBB22_6
# BB#3:
	movl	$-1, 4(%rsp)            # 4-byte Folded Spill
	xorl	%r13d, %r13d
	cmpl	$-32, %r12d
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	je	.LBB22_9
# BB#4:
	cmpl	$-24, %r12d
	jne	.LBB22_29
# BB#5:
	movl	$mem_true24_color_device, %ebp
	movl	$24, (%rsp)             # 4-byte Folded Spill
	xorl	%r12d, %r12d
	jmp	.LBB22_25
.LBB22_6:
	cmpl	$2, %r12d
	je	.LBB22_10
# BB#7:
	cmpl	$256, %r12d             # imm = 0x100
	jne	.LBB22_29
# BB#8:
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	$mem_mapped_color_device, %esi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	$8, (%rsp)              # 4-byte Folded Spill
	jmp	.LBB22_11
.LBB22_9:
	movl	$mem_true32_color_device, %ebp
	movl	$32, (%rsp)             # 4-byte Folded Spill
	xorl	%r12d, %r12d
	jmp	.LBB22_25
.LBB22_10:
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	$mem_mono_device, %esi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	$1, (%rsp)              # 4-byte Folded Spill
.LBB22_11:                              # %.lr.ph.preheader
	leaq	32(%rsp), %r11
	leal	(%r12,%r12,2), %r13d
	movl	$-1, 4(%rsp)            # 4-byte Folded Spill
	xorl	%r14d, %r14d
	movsd	.LCPI22_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movsd	.LCPI22_1(%rip), %xmm1  # xmm1 = mem[0],zero
	movss	.LCPI22_2(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movsd	.LCPI22_3(%rip), %xmm3  # xmm3 = mem[0],zero
	xorl	%r15d, %r15d
	movl	$-1, %r9d
	jmp	.LBB22_12
.LBB22_22:                              #   in Loop: Header=BB22_12 Depth=1
	leal	-2(%r14), %r9d
	jmp	.LBB22_23
	.p2align	4, 0x90
.LBB22_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%r8), %xmm4            # xmm4 = mem[0],zero,zero,zero
	xorps	%xmm5, %xmm5
	cvtss2sd	%xmm4, %xmm5
	ucomisd	%xmm5, %xmm0
	ja	.LBB22_29
# BB#13:                                # %.lr.ph
                                        #   in Loop: Header=BB22_12 Depth=1
	ucomisd	%xmm1, %xmm5
	ja	.LBB22_29
# BB#14:                                #   in Loop: Header=BB22_12 Depth=1
	mulss	%xmm2, %xmm4
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm3, %xmm4
	cvttsd2si	%xmm4, %r10d
	movb	%r10b, (%r11)
	movslq	%r14d, %rsi
	imulq	$1431655766, %rsi, %rbp # imm = 0x55555556
	movq	%rbp, %rbx
	shrq	$63, %rbx
	shrq	$32, %rbp
	addl	%ebx, %ebp
	leal	(%rbp,%rbp,2), %ebp
	subl	%ebp, %esi
	cmpl	$2, %esi
	jne	.LBB22_23
# BB#15:                                #   in Loop: Header=BB22_12 Depth=1
	cmpb	-1(%r11), %r10b
	jne	.LBB22_21
# BB#16:                                #   in Loop: Header=BB22_12 Depth=1
	cmpb	-2(%r11), %r10b
	jne	.LBB22_21
# BB#17:                                #   in Loop: Header=BB22_12 Depth=1
	cmpb	$-1, %r10b
	je	.LBB22_22
# BB#18:                                #   in Loop: Header=BB22_12 Depth=1
	testb	%r10b, %r10b
	jne	.LBB22_23
# BB#19:                                #   in Loop: Header=BB22_12 Depth=1
	leal	-2(%r14), %esi
	movl	%esi, 4(%rsp)           # 4-byte Spill
	jmp	.LBB22_23
	.p2align	4, 0x90
.LBB22_21:                              #   in Loop: Header=BB22_12 Depth=1
	movl	$1, %r15d
.LBB22_23:                              #   in Loop: Header=BB22_12 Depth=1
	incl	%r14d
	addq	$4, %r8
	incq	%r11
	cmpl	%r13d, %r14d
	jl	.LBB22_12
# BB#24:                                # %._crit_edge
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	orl	4(%rsp), %r9d           # 4-byte Folded Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	js	.LBB22_29
.LBB22_25:
	movl	%edx, %ebx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	(%rbp), %esi
	movl	$1, %edi
	movl	$.L.str.2, %edx
	callq	*864(%rsp)
	movq	%rax, %r14
	movl	$-25, %eax
	testq	%r14, %r14
	je	.LBB22_29
# BB#26:
	movl	$200, %edx
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	memcpy
	movq	16(%rsp), %rax          # 8-byte Reload
	movups	80(%rax), %xmm0
	movups	%xmm0, 136(%r14)
	movups	64(%rax), %xmm0
	movups	%xmm0, 120(%r14)
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movupd	48(%rax), %xmm3
	movupd	%xmm3, 104(%r14)
	movups	%xmm2, 88(%r14)
	movups	%xmm1, 72(%r14)
	movups	%xmm0, 56(%r14)
	movl	%ebx, 24(%r14)
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, 28(%r14)
	movl	%r15d, 40(%r14)
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, 48(%r14)
	movq	%r14, %rdi
	callq	gx_device_memory_bitmap_size
	movq	%rax, %rbp
	shrq	$32, %rax
	movl	$-13, %eax
	jne	.LBB22_29
# BB#27:
	leal	(%rbp,%r13), %esi
	movl	$1, %edi
	movl	$.L.str.3, %edx
	callq	*864(%rsp)
	movq	%rax, %rcx
	testq	%rcx, %rcx
	movl	$-25, %eax
	je	.LBB22_29
# BB#28:
	movq	%rcx, 160(%r14)
	negl	4(%rsp)                 # 4-byte Folded Spill
	sbbl	%eax, %eax
	movl	%eax, 184(%r14)
	movl	%r12d, 188(%r14)
	addq	%rcx, %rbp
	movq	%rbp, 192(%r14)
	movslq	%r13d, %rdx
	leaq	32(%rsp), %rsi
	movq	%rbp, %rdi
	callq	memcpy
	movl	$0, 52(%r14)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%r14, (%rax)
	xorl	%eax, %eax
.LBB22_29:                              # %.critedge
	addq	$808, %rsp              # imm = 0x328
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	gs_makeimagedevice, .Lfunc_end22-gs_makeimagedevice
	.cfi_endproc

	.globl	gs_setdevice
	.p2align	4, 0x90
	.type	gs_setdevice,@function
gs_setdevice:                           # @gs_setdevice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 48
.Lcfi47:
	.cfi_offset %rbx, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	448(%r14), %rbp
	movl	52(%rbx), %r15d
	testl	%r15d, %r15d
	jne	.LBB23_3
# BB#1:
	movq	8(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	testl	%eax, %eax
	js	.LBB23_8
# BB#2:
	movl	$1, 52(%rbx)
.LBB23_3:                               # %._crit_edge
	movq	8(%rbx), %rax
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	*40(%rax)
	movq	%rax, 24(%rbp)
	movq	8(%rbx), %rax
	movzwl	44(%rbx), %esi
	movq	%rbx, %rdi
	movl	%esi, %edx
	movl	%esi, %ecx
	callq	*40(%rax)
	movq	%rax, 16(%rbp)
	movq	%rbx, (%rbp)
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gs_initmatrix
	testl	%eax, %eax
	js	.LBB23_8
# BB#4:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gs_initclip
	testl	%eax, %eax
	js	.LBB23_8
# BB#5:
	testl	%r15d, %r15d
	jne	.LBB23_7
# BB#6:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gs_erasepage
	testl	%eax, %eax
	js	.LBB23_8
.LBB23_7:
	xorl	%eax, %eax
.LBB23_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	gs_setdevice, .Lfunc_end23-gs_setdevice
	.cfi_endproc

	.globl	gs_nulldevice
	.p2align	4, 0x90
	.type	gs_nulldevice,@function
gs_nulldevice:                          # @gs_nulldevice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 48
.Lcfi56:
	.cfi_offset %rbx, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	gx_device_null_p(%rip), %rbx
	movq	448(%r14), %rbp
	movl	52(%rbx), %r15d
	testl	%r15d, %r15d
	jne	.LBB24_3
# BB#1:
	movq	8(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	testl	%eax, %eax
	js	.LBB24_6
# BB#2:
	movl	$1, 52(%rbx)
.LBB24_3:                               # %._crit_edge.i
	movq	8(%rbx), %rax
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	*40(%rax)
	movq	%rax, 24(%rbp)
	movq	8(%rbx), %rax
	movzwl	44(%rbx), %esi
	movq	%rbx, %rdi
	movl	%esi, %edx
	movl	%esi, %ecx
	callq	*40(%rax)
	movq	%rax, 16(%rbp)
	movq	%rbx, (%rbp)
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gs_initmatrix
	testl	%eax, %eax
	js	.LBB24_6
# BB#4:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gs_initclip
	testl	%r15d, %r15d
	jne	.LBB24_6
# BB#5:
	testl	%eax, %eax
	js	.LBB24_6
# BB#7:
	xorl	%eax, %eax
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	gs_erasepage            # TAILCALL
.LBB24_6:                               # %gs_setdevice.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	gs_nulldevice, .Lfunc_end24-gs_nulldevice
	.cfi_endproc

	.globl	gx_device_no_output
	.p2align	4, 0x90
	.type	gx_device_no_output,@function
gx_device_no_output:                    # @gx_device_no_output
	.cfi_startproc
# BB#0:
	movq	448(%rdi), %rax
	movq	$null_device, (%rax)
	retq
.Lfunc_end25:
	.size	gx_device_no_output, .Lfunc_end25-gx_device_no_output
	.cfi_endproc

	.type	null_procs,@object      # @null_procs
	.data
	.globl	null_procs
	.p2align	3
null_procs:
	.quad	gx_default_open_device
	.quad	gx_default_get_initial_matrix
	.quad	gx_default_sync_output
	.quad	gx_default_output_page
	.quad	gx_default_close_device
	.quad	gx_default_map_rgb_color
	.quad	gx_default_map_color_rgb
	.quad	null_fill_rectangle
	.quad	null_tile_rectangle
	.quad	null_copy_mono
	.quad	null_copy_color
	.quad	null_draw_line
	.quad	null_fill_trapezoid
	.quad	null_tile_trapezoid
	.size	null_procs, 112

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"null"
	.size	.L.str, 5

	.type	null_device,@object     # @null_device
	.data
	.globl	null_device
	.p2align	3
null_device:
	.long	32                      # 0x20
	.zero	4
	.quad	null_procs
	.quad	.L.str
	.long	16383                   # 0x3fff
	.long	16383                   # 0x3fff
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	0                       # 0x0
	.short	1                       # 0x1
	.zero	2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.size	null_device, 56

	.type	gx_device_null_p,@object # @gx_device_null_p
	.globl	gx_device_null_p
	.p2align	3
gx_device_null_p:
	.quad	null_device
	.size	gx_device_null_p, 8

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"gs_makedevice"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"gs_makeimagedevice(device)"
	.size	.L.str.2, 27

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"gs_makeimagedevice(bits)"
	.size	.L.str.3, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
