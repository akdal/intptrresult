	.text
	.file	"vehicle.bc"
	.globl	_ZlsRSo7vehicle
	.p2align	4, 0x90
	.type	_ZlsRSo7vehicle,@function
_ZlsRSo7vehicle:                        # @_ZlsRSo7vehicle
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$144, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 176
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	leaq	176(%rsp), %r15
	movq	184(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
# BB#2:
	movq	%rbx, %rdi
	callq	strlen
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB0_3
.LBB0_1:
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	leaq	(%r14,%rax), %rdi
	movl	32(%r14,%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB0_3:                                # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
	movl	$.L.str, %esi
	movl	$4, %edx
	movq	%r14, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%r15), %rsi
	movl	$18, %ecx
	movq	%rsp, %rdi
	rep;movsq
	movq	%r14, %rdi
	callq	_ZlsRSo7roadlet
	movq	%rax, %rbx
	movl	$.L.str.1, %esi
	movl	$7, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	24(%r15), %esi
	movq	%rbx, %rdi
	callq	_ZlsRSo9direction
	movq	%r14, %rax
	addq	$144, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	_ZlsRSo7vehicle, .Lfunc_end0-_ZlsRSo7vehicle
	.cfi_endproc

	.globl	_ZN7vehicle4tickEv
	.p2align	4, 0x90
	.type	_ZN7vehicle4tickEv,@function
_ZN7vehicle4tickEv:                     # @_ZN7vehicle4tickEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
.Lcfi8:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	20(%rbx), %eax
	addl	$100, %eax
	movl	%eax, 20(%rbx)
	cmpl	16(%rbx), %eax
	jl	.LBB1_2
	.p2align	4, 0x90
.LBB1_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_ZN7vehicle4moveEv
	movl	16(%rbx), %eax
	movl	20(%rbx), %ecx
	subl	%eax, %ecx
	movl	%ecx, 20(%rbx)
	cmpl	%eax, %ecx
	jge	.LBB1_1
.LBB1_2:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN7vehicle4tickEv, .Lfunc_end1-_ZN7vehicle4tickEv
	.cfi_endproc

	.globl	_ZN7vehicle4moveEv
	.p2align	4, 0x90
	.type	_ZN7vehicle4moveEv,@function
_ZN7vehicle4moveEv:                     # @_ZN7vehicle4moveEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 80
.Lcfi15:
	.cfi_offset %rbx, -48
.Lcfi16:
	.cfi_offset %r12, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	cmpl	$-1, 28(%r15)
	je	.LBB2_3
# BB#1:
	movl	NO_DIRECTION(%rip), %r14d
	movl	$_ZSt4cout, %edi
	movl	$.L.str.2, %esi
	movl	$17, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	28(%r15), %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSolsEi
	movl	$.L.str.3, %esi
	movl	$2, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%r15), %rax
	movslq	28(%r15), %rcx
	imulq	$954437177, %rcx, %rdx  # imm = 0x38E38E39
	movq	%rdx, %rsi
	shrq	$63, %rsi
	sarq	$33, %rdx
	addl	%esi, %edx
	leal	(%rdx,%rdx,8), %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	movq	16(%rax,%rdx,8), %rdi
	movq	%r15, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*80(%rax,%rcx,8)
	testq	%rax, %rax
	je	.LBB2_13
# BB#2:
	movslq	28(%r15), %r14
	imulq	$954437177, %r14, %rax  # imm = 0x38E38E39
	movq	%rax, %rcx
	shrq	$63, %rcx
	sarq	$33, %rax
	addl	%ecx, %eax
	leal	(%rax,%rax,8), %eax
	subl	%eax, %r14d
	movl	$-1, 28(%r15)
	jmp	.LBB2_13
.LBB2_3:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movq	(%r15), %r14
	movl	N(%rip), %ebp
	movl	NW(%rip), %esi
	movl	%ebp, %edi
	callq	_Zle9directionS_
	testl	%eax, %eax
	je	.LBB2_11
# BB#4:                                 # %.lr.ph.preheader.i
	movslq	%ebp, %rbx
	shlq	$3, %rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	16(%rax,%rbx), %rdi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	*80(%r14,%rbx)
	testq	%rax, %rax
	je	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	movslq	%r12d, %r12
	movl	%ebp, (%rsp,%r12,4)
	incl	%r12d
.LBB2_7:                                #   in Loop: Header=BB2_5 Depth=1
	movl	NW(%rip), %esi
	incl	%ebp
	movl	%ebp, %edi
	callq	_Zle9directionS_
	addq	$8, %rbx
	testl	%eax, %eax
	jne	.LBB2_5
# BB#8:                                 # %._crit_edge.i
	testl	%r12d, %r12d
	je	.LBB2_11
# BB#9:
	callq	random
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	%r12d
	movslq	%edx, %rax
	leaq	(%rsp,%rax,4), %rax
	jmp	.LBB2_12
.LBB2_11:
	movl	$NO_DIRECTION, %eax
.LBB2_12:                               # %_ZN7vehicle11select_moveEv.exit
	movl	(%rax), %r14d
.LBB2_13:
	movl	NO_DIRECTION(%rip), %esi
	movl	%r14d, %edi
	callq	_Zne9directionS_
	testl	%eax, %eax
	je	.LBB2_19
# BB#14:
	movq	(%r15), %rax
	movq	$0, 8(%rax)
	movslq	%r14d, %rcx
	movq	16(%rax,%rcx,8), %rax
	movq	%r15, 8(%rax)
	movq	%rax, (%r15)
	movl	N(%rip), %esi
	movl	%r14d, %edi
	callq	_Zeq9directionS_
	testl	%eax, %eax
	jne	.LBB2_18
# BB#15:
	movl	S(%rip), %esi
	movl	%r14d, %edi
	callq	_Zeq9directionS_
	testl	%eax, %eax
	jne	.LBB2_18
# BB#16:
	movl	E(%rip), %esi
	movl	%r14d, %edi
	callq	_Zeq9directionS_
	testl	%eax, %eax
	jne	.LBB2_18
# BB#17:
	movl	W(%rip), %esi
	movl	%r14d, %edi
	callq	_Zeq9directionS_
	testl	%eax, %eax
	je	.LBB2_19
.LBB2_18:
	movl	%r14d, 24(%r15)
.LBB2_19:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN7vehicle4moveEv, .Lfunc_end2-_ZN7vehicle4moveEv
	.cfi_endproc

	.globl	_ZN7vehicle11select_moveEv
	.p2align	4, 0x90
	.type	_ZN7vehicle11select_moveEv,@function
_ZN7vehicle11select_moveEv:             # @_ZN7vehicle11select_moveEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 80
.Lcfi26:
	.cfi_offset %rbx, -48
.Lcfi27:
	.cfi_offset %r12, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movq	(%r15), %r14
	movl	N(%rip), %ebp
	movl	NW(%rip), %esi
	movl	%ebp, %edi
	callq	_Zle9directionS_
	testl	%eax, %eax
	je	.LBB3_8
# BB#1:                                 # %.lr.ph.preheader
	movslq	%ebp, %rbx
	shlq	$3, %rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	16(%rax,%rbx), %rdi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	*80(%r14,%rbx)
	testq	%rax, %rax
	je	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movslq	%r12d, %r12
	movl	%ebp, (%rsp,%r12,4)
	incl	%r12d
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	movl	NW(%rip), %esi
	incl	%ebp
	movl	%ebp, %edi
	callq	_Zle9directionS_
	addq	$8, %rbx
	testl	%eax, %eax
	jne	.LBB3_2
# BB#5:                                 # %._crit_edge
	testl	%r12d, %r12d
	je	.LBB3_8
# BB#6:
	callq	random
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	%r12d
	movslq	%edx, %rax
	leaq	(%rsp,%rax,4), %rax
	jmp	.LBB3_9
.LBB3_8:
	movl	$NO_DIRECTION, %eax
.LBB3_9:                                # %._crit_edge.thread
	movl	(%rax), %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN7vehicle11select_moveEv, .Lfunc_end3-_ZN7vehicle11select_moveEv
	.cfi_endproc

	.globl	_ZlsRSo3car
	.p2align	4, 0x90
	.type	_ZlsRSo3car,@function
_ZlsRSo3car:                            # @_ZlsRSo3car
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 48
.Lcfi33:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str.4, %esi
	movl	$4, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movaps	48(%rsp), %xmm0
	movaps	64(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%rbx, %rdi
	callq	_ZlsRSo7vehicle
	movq	%rbx, %rax
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZlsRSo3car, .Lfunc_end4-_ZlsRSo3car
	.cfi_endproc

	.globl	_ZlsRSo5truck
	.p2align	4, 0x90
	.type	_ZlsRSo5truck,@function
_ZlsRSo5truck:                          # @_ZlsRSo5truck
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 48
.Lcfi36:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str.5, %esi
	movl	$6, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movaps	48(%rsp), %xmm0
	movaps	64(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%rbx, %rdi
	callq	_ZlsRSo7vehicle
	movq	%rbx, %rax
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_ZlsRSo5truck, .Lfunc_end5-_ZlsRSo5truck
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_vehicle.ii,@function
_GLOBAL__sub_I_vehicle.ii:              # @_GLOBAL__sub_I_vehicle.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end6:
	.size	_GLOBAL__sub_I_vehicle.ii, .Lfunc_end6-_GLOBAL__sub_I_vehicle.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" at "
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" going "
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"there is a plan! "
	.size	.L.str.2, 18

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" \n"
	.size	.L.str.3, 3

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Car "
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Truck "
	.size	.L.str.5, 7

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_vehicle.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
