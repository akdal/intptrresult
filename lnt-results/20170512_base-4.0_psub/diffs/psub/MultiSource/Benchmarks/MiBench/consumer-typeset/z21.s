	.text
	.file	"z21.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1120403456              # float 100
.LCPI0_2:
	.long	1124073472              # float 128
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_1:
	.quad	4576918229304087675     # double 0.01
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_3:
	.zero	16
	.text
	.globl	SizeGalley
	.p2align	4, 0x90
	.type	SizeGalley,@function
SizeGalley:                             # @SizeGalley
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 256
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, 140(%rsp)         # 4-byte Spill
	movl	%r8d, 60(%rsp)          # 4-byte Spill
	movl	%ecx, %ebx
	movl	%edx, 40(%rsp)          # 4-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r13
	cmpb	$8, 32(%r13)
	jne	.LBB0_2
# BB#1:
	cmpq	%r13, 8(%r13)
	jne	.LBB0_3
.LBB0_2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_3:
	testb	$2, 42(%r13)
	je	.LBB0_5
# BB#4:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_5:
	movq	304(%rsp), %rax
	movq	272(%rsp), %rdx
	movq	264(%rsp), %r15
	leaq	32(%r13), %rcx
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	leaq	8(%r13), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movq	8(%r13), %rcx
	addq	$16, %rcx
	.p2align	4, 0x90
.LBB0_6:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %r14
	leaq	16(%r14), %rcx
	cmpb	$0, 32(%r14)
	je	.LBB0_6
# BB#7:
	movq	%rdx, 128(%rsp)
	movq	%rax, 120(%rsp)
	movq	$0, 96(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 160(%rsp)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB0_8
# BB#9:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_10
.LBB0_8:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB0_10:
	movb	$17, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_11
# BB#12:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_13
.LBB0_11:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_13:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_16
# BB#14:
	testq	%rax, %rax
	je	.LBB0_16
# BB#15:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_16:
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB0_19
# BB#17:
	testq	%rax, %rax
	je	.LBB0_19
# BB#18:
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_19:
	cmpl	$0, AllowCrossDb(%rip)
	je	.LBB0_24
# BB#20:
	cmpb	$2, 32(%r14)
	jne	.LBB0_24
# BB#21:
	movq	80(%r14), %rax
	testb	$2, 126(%rax)
	je	.LBB0_24
# BB#22:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	FindOptimize
	testl	%eax, %eax
	je	.LBB0_24
# BB#23:
	movq	%r13, %rdi
	movq	256(%rsp), %rsi
	callq	SetOptimize
.LBB0_24:
	testl	%ebx, %ebx
	je	.LBB0_43
# BB#25:
	movzbl	zz_lengths+139(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_26
# BB#27:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_28
.LBB0_43:
	movzwl	42(%r13), %eax
	shrl	$7, %eax
	andl	$1, %eax
	leaq	120(%rsp), %rcx
	movq	%rcx, 24(%rsp)
	movl	%eax, 16(%rsp)
	leaq	96(%rsp), %rax
	movq	%rax, (%rsp)
	movl	$0, 32(%rsp)
	movl	$1, 8(%rsp)
	leaq	160(%rsp), %rcx
	leaq	144(%rsp), %r8
	leaq	128(%rsp), %r9
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	256(%rsp), %rdx
	callq	Manifest
	movq	%rax, %r14
	jmp	.LBB0_44
.LBB0_26:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_28:
	movb	$-117, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, 160(%rsp)
	movzbl	zz_lengths+139(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_29
# BB#30:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_31
.LBB0_29:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_31:
	movb	$-117, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, 144(%rsp)
	movzwl	42(%r13), %eax
	shrl	$7, %eax
	andl	$1, %eax
	leaq	120(%rsp), %rcx
	movq	%rcx, 24(%rsp)
	movl	%eax, 16(%rsp)
	leaq	96(%rsp), %rax
	movq	%rax, (%rsp)
	movl	$0, 32(%rsp)
	movl	$1, 8(%rsp)
	leaq	160(%rsp), %rcx
	leaq	144(%rsp), %r8
	leaq	128(%rsp), %r9
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	256(%rsp), %rdx
	callq	Manifest
	movq	%rax, %r14
	movq	160(%rsp), %rdi
	cmpq	%rdi, 8(%rdi)
	je	.LBB0_33
# BB#32:
	movq	144(%rsp), %rax
	cmpq	%rax, 8(%rax)
	jne	.LBB0_34
.LBB0_33:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	160(%rsp), %rdi
.LBB0_34:
	movq	8(%rdi), %rax
	.p2align	4, 0x90
.LBB0_35:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB0_35
# BB#36:
	movq	%rax, 128(%rsp)
	movq	144(%rsp), %rcx
	movq	8(%rcx), %rdx
	leaq	16(%rdx), %rbx
	.p2align	4, 0x90
.LBB0_37:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	leaq	16(%rsi), %rbx
	cmpb	$0, 32(%rsi)
	je	.LBB0_37
# BB#38:
	movq	%rsi, 120(%rsp)
	movq	8(%rdi), %rbx
	cmpq	(%rdi), %rbx
	je	.LBB0_39
.LBB0_41:
	movq	%r14, %rbx
	addq	$32, %rbx
	movq	80(%r13), %rdi
	callq	SymName
	movq	%rax, %r9
	movl	$21, %edi
	movl	$1, %esi
	movl	$.L.str.5, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movq	160(%rsp), %rdi
.LBB0_42:
	callq	DisposeObject
	movq	144(%rsp), %rdi
	callq	DisposeObject
.LBB0_44:
	movq	%rbp, %rdi
	callq	DisposeObject
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_45
# BB#46:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_47
.LBB0_45:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_47:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, 48(%rsp)
	leaq	48(%rsp), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	MinSize
	movzwl	42(%r13), %ecx
	testb	$1, %ch
	jne	.LBB0_48
# BB#56:
	testb	$2, %ch
	je	.LBB0_60
# BB#57:
	movq	104(%r13), %rcx
	testq	%rcx, %rcx
	je	.LBB0_60
# BB#58:
	cmpb	$17, 32(%rax)
	jne	.LBB0_60
# BB#59:
	movq	%rax, %rdi
	callq	Hyphenate
	jmp	.LBB0_60
.LBB0_48:
	movl	(%r15), %ecx
	movl	%ecx, 64(%r13)
	movl	4(%r15), %ecx
	movl	%ecx, 68(%r13)
	movl	8(%r15), %ecx
	movl	%ecx, 72(%r13)
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	BreakObject
	movq	%rax, %rbx
	movl	48(%rbx), %edi
	cmpl	(%r15), %edi
	jg	.LBB0_51
# BB#49:
	movl	56(%rbx), %eax
	leal	(%rax,%rdi), %ecx
	cmpl	4(%r15), %ecx
	jg	.LBB0_51
# BB#50:
	cmpl	8(%r15), %eax
	jle	.LBB0_52
.LBB0_51:                               # %._crit_edge798
	movq	%rbx, %rbp
	addq	$32, %rbp
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	EchoLength
	movq	%rax, %r14
	movl	56(%rbx), %edi
	callq	EchoLength
	movq	%rax, (%rsp)
	movl	$21, %edi
	movl	$13, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	%r14, %r9
	callq	Error
	movl	48(%rbx), %edi
.LBB0_52:
	movl	%edi, 48(%r13)
	movl	56(%rbx), %eax
	movl	%eax, 56(%r13)
	cmpl	(%r15), %edi
	jg	.LBB0_55
# BB#53:
	addl	%eax, %edi
	cmpl	4(%r15), %edi
	jg	.LBB0_55
# BB#54:
	cmpl	8(%r15), %eax
	jle	.LBB0_60
.LBB0_55:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_60:
	andb	$-33, 42(%r13)
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	movq	%r13, 72(%rsp)          # 8-byte Spill
	je	.LBB0_180
# BB#61:                                # %.preheader675
	movq	8(%r13), %rbx
	cmpq	%r13, %rbx
	je	.LBB0_180
# BB#62:                                # %.preheader673.preheader
	movq	176(%rsp), %r12         # 8-byte Reload
	movq	%r13, %r14
	jmp	.LBB0_63
.LBB0_65:                               #   in Loop: Header=BB0_63 Depth=1
	testb	$2, 45(%rbx)
	jne	.LBB0_179
# BB#66:                                #   in Loop: Header=BB0_63 Depth=1
	orb	$32, 42(%r13)
	jmp	.LBB0_179
.LBB0_95:                               #   in Loop: Header=BB0_63 Depth=1
	movq	24(%rbx), %rax
	cmpq	16(%rbx), %rax
	je	.LBB0_97
# BB#96:                                #   in Loop: Header=BB0_63 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.9, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_97:                               #   in Loop: Header=BB0_63 Depth=1
	movq	(%rbx), %rbp
	.p2align	4, 0x90
.LBB0_98:                               #   Parent Loop BB0_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$16, %rbp
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_98
# BB#99:                                #   in Loop: Header=BB0_63 Depth=1
	cmpb	$1, %al
	je	.LBB0_179
# BB#100:                               #   in Loop: Header=BB0_63 Depth=1
	cmpb	$8, %al
	ja	.LBB0_102
# BB#101:                               #   in Loop: Header=BB0_63 Depth=1
	orb	$16, 42(%rbp)
	jmp	.LBB0_179
.LBB0_81:                               #   in Loop: Header=BB0_63 Depth=1
	testb	$1, 43(%r13)
	jne	.LBB0_179
# BB#82:                                #   in Loop: Header=BB0_63 Depth=1
	movq	8(%rbx), %rbp
	cmpq	%rbx, %rbp
	je	.LBB0_87
# BB#83:                                #   in Loop: Header=BB0_63 Depth=1
	movq	24(%rbx), %r15
	cmpb	$0, 32(%rbp)
	je	.LBB0_85
# BB#84:                                #   in Loop: Header=BB0_63 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_85:                               #   in Loop: Header=BB0_63 Depth=1
	movq	%rbp, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbp), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%rbp, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB0_87
# BB#86:                                #   in Loop: Header=BB0_63 Depth=1
	movq	(%r15), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbp), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB0_87:                               #   in Loop: Header=BB0_63 Depth=1
	movq	24(%rbx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_88
# BB#89:                                #   in Loop: Header=BB0_63 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_90
.LBB0_67:                               #   in Loop: Header=BB0_63 Depth=1
	testb	$1, 43(%r13)
	je	.LBB0_179
# BB#68:                                #   in Loop: Header=BB0_63 Depth=1
	movq	8(%rbx), %rbp
	cmpq	%rbx, %rbp
	je	.LBB0_73
# BB#69:                                #   in Loop: Header=BB0_63 Depth=1
	movq	24(%rbx), %r15
	cmpb	$0, 32(%rbp)
	je	.LBB0_71
# BB#70:                                #   in Loop: Header=BB0_63 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_71:                               #   in Loop: Header=BB0_63 Depth=1
	movq	%rbp, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbp), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%rbp, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB0_73
# BB#72:                                #   in Loop: Header=BB0_63 Depth=1
	movq	(%r15), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbp), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB0_73:                               #   in Loop: Header=BB0_63 Depth=1
	movq	24(%rbx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_74
# BB#75:                                #   in Loop: Header=BB0_63 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_76
.LBB0_102:                              #   in Loop: Header=BB0_63 Depth=1
	cmpb	$19, %al
	jne	.LBB0_179
# BB#103:                               #   in Loop: Header=BB0_63 Depth=1
	movq	8(%rbx), %r13
	.p2align	4, 0x90
.LBB0_104:                              #   Parent Loop BB0_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$16, %r13
	movq	(%r13), %r13
	movzbl	32(%r13), %eax
	testb	%al, %al
	je	.LBB0_104
# BB#105:                               #   in Loop: Header=BB0_63 Depth=1
	cmpb	$16, %al
	je	.LBB0_107
# BB#106:                               #   in Loop: Header=BB0_63 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.10, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_107:                              # %.loopexit672
                                        #   in Loop: Header=BB0_63 Depth=1
	movq	24(%rbp), %rcx
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB0_108:                              #   Parent Loop BB0_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB0_108
# BB#109:                               #   in Loop: Header=BB0_63 Depth=1
	cmpq	%rax, %r13
	je	.LBB0_110
# BB#111:                               #   in Loop: Header=BB0_63 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	24(%rbp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	jmp	.LBB0_112
.LBB0_74:                               #   in Loop: Header=BB0_63 Depth=1
	xorl	%ecx, %ecx
.LBB0_76:                               #   in Loop: Header=BB0_63 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_78
# BB#77:                                #   in Loop: Header=BB0_63 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_78:                               #   in Loop: Header=BB0_63 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_80
# BB#79:                                #   in Loop: Header=BB0_63 Depth=1
	callq	DisposeObject
.LBB0_80:                               #   in Loop: Header=BB0_63 Depth=1
	movq	(%r14), %r14
	jmp	.LBB0_179
.LBB0_88:                               #   in Loop: Header=BB0_63 Depth=1
	xorl	%ecx, %ecx
.LBB0_90:                               #   in Loop: Header=BB0_63 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_92
# BB#91:                                #   in Loop: Header=BB0_63 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_92:                               #   in Loop: Header=BB0_63 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_94
# BB#93:                                #   in Loop: Header=BB0_63 Depth=1
	callq	DisposeObject
.LBB0_94:                               #   in Loop: Header=BB0_63 Depth=1
	movq	(%r14), %r14
	jmp	.LBB0_179
.LBB0_110:                              #   in Loop: Header=BB0_63 Depth=1
	movq	%rcx, 64(%rsp)          # 8-byte Spill
.LBB0_112:                              #   in Loop: Header=BB0_63 Depth=1
	movq	(%rbp), %rsi
	cmpq	%rbp, %rsi
	je	.LBB0_127
# BB#113:                               # %.preheader671.lr.ph
                                        #   in Loop: Header=BB0_63 Depth=1
	movq	8(%rbx), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%r12, 88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_114:                              # %.preheader671
                                        #   Parent Loop BB0_63 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_115 Depth 3
	movq	%rsi, %r15
	.p2align	4, 0x90
.LBB0_115:                              #   Parent Loop BB0_63 Depth=1
                                        #     Parent Loop BB0_114 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r15), %r15
	movzbl	32(%r15), %eax
	testb	%al, %al
	je	.LBB0_115
# BB#116:                               #   in Loop: Header=BB0_114 Depth=2
	cmpb	$1, %al
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	jne	.LBB0_135
# BB#117:                               #   in Loop: Header=BB0_114 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_118
# BB#119:                               #   in Loop: Header=BB0_114 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_120
	.p2align	4, 0x90
.LBB0_135:                              #   in Loop: Header=BB0_114 Depth=2
	movzbl	zz_lengths+9(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r12
	testq	%r12, %r12
	je	.LBB0_136
# BB#137:                               #   in Loop: Header=BB0_114 Depth=2
	movq	%r12, zz_hold(%rip)
	movq	(%r12), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_138
.LBB0_118:                              #   in Loop: Header=BB0_114 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_120:                              #   in Loop: Header=BB0_114 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%r12), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	je	.LBB0_123
# BB#121:                               #   in Loop: Header=BB0_114 Depth=2
	testq	%rcx, %rcx
	je	.LBB0_123
# BB#122:                               #   in Loop: Header=BB0_114 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_123:                              #   in Loop: Header=BB0_114 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	jne	.LBB0_124
	jmp	.LBB0_126
.LBB0_136:                              #   in Loop: Header=BB0_114 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r12
	movq	%r12, zz_hold(%rip)
.LBB0_138:                              #   in Loop: Header=BB0_114 Depth=2
	movb	$9, 32(%r12)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%r12, 8(%r12)
	movq	%r12, (%r12)
	movl	48(%r13), %eax
	movl	%eax, 48(%r12)
	movl	56(%r13), %eax
	movl	%eax, 56(%r12)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_139
# BB#140:                               #   in Loop: Header=BB0_114 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_141
.LBB0_139:                              #   in Loop: Header=BB0_114 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_141:                              #   in Loop: Header=BB0_114 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_144
# BB#142:                               #   in Loop: Header=BB0_114 Depth=2
	testq	%rcx, %rcx
	je	.LBB0_144
# BB#143:                               #   in Loop: Header=BB0_114 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_144:                              #   in Loop: Header=BB0_114 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB0_147
# BB#145:                               #   in Loop: Header=BB0_114 Depth=2
	testq	%rax, %rax
	je	.LBB0_147
# BB#146:                               #   in Loop: Header=BB0_114 Depth=2
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_147:                              #   in Loop: Header=BB0_114 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_148
# BB#149:                               #   in Loop: Header=BB0_114 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_150
.LBB0_148:                              #   in Loop: Header=BB0_114 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_150:                              #   in Loop: Header=BB0_114 Depth=2
	testq	%r12, %r12
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	je	.LBB0_153
# BB#151:                               #   in Loop: Header=BB0_114 Depth=2
	testq	%rax, %rax
	je	.LBB0_153
# BB#152:                               #   in Loop: Header=BB0_114 Depth=2
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_153:                              #   in Loop: Header=BB0_114 Depth=2
	movq	%rax, zz_res(%rip)
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	24(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_156
# BB#154:                               #   in Loop: Header=BB0_114 Depth=2
	testq	%rcx, %rcx
	je	.LBB0_156
# BB#155:                               #   in Loop: Header=BB0_114 Depth=2
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB0_156:                              #   in Loop: Header=BB0_114 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_157
# BB#158:                               #   in Loop: Header=BB0_114 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_159
.LBB0_157:                              #   in Loop: Header=BB0_114 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_159:                              #   in Loop: Header=BB0_114 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	8(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_162
# BB#160:                               #   in Loop: Header=BB0_114 Depth=2
	testq	%rcx, %rcx
	je	.LBB0_162
# BB#161:                               #   in Loop: Header=BB0_114 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_162:                              #   in Loop: Header=BB0_114 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB0_165
# BB#163:                               #   in Loop: Header=BB0_114 Depth=2
	testq	%rax, %rax
	je	.LBB0_165
# BB#164:                               #   in Loop: Header=BB0_114 Depth=2
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_165:                              #   in Loop: Header=BB0_114 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_166
# BB#167:                               #   in Loop: Header=BB0_114 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_168
.LBB0_166:                              #   in Loop: Header=BB0_114 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_168:                              #   in Loop: Header=BB0_114 Depth=2
	testq	%r12, %r12
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	movq	40(%rsp), %rsi          # 8-byte Reload
	je	.LBB0_171
# BB#169:                               #   in Loop: Header=BB0_114 Depth=2
	testq	%rax, %rax
	je	.LBB0_171
# BB#170:                               #   in Loop: Header=BB0_114 Depth=2
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_171:                              #   in Loop: Header=BB0_114 Depth=2
	testq	%r15, %r15
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	movq	88(%rsp), %r12          # 8-byte Reload
	je	.LBB0_126
.LBB0_124:                              #   in Loop: Header=BB0_114 Depth=2
	testq	%rax, %rax
	je	.LBB0_126
# BB#125:                               #   in Loop: Header=BB0_114 Depth=2
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_126:                              # %.backedge
                                        #   in Loop: Header=BB0_114 Depth=2
	movq	(%rsi), %rsi
	cmpq	%rbp, %rsi
	jne	.LBB0_114
.LBB0_127:                              # %._crit_edge721
                                        #   in Loop: Header=BB0_63 Depth=1
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, xx_link(%rip)
	movq	%rsi, zz_hold(%rip)
	movq	24(%rsi), %rax
	cmpq	%rsi, %rax
	je	.LBB0_129
# BB#128:                               #   in Loop: Header=BB0_63 Depth=1
	movq	%rax, zz_res(%rip)
	movq	16(%rsi), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rsi), %rcx
	movq	%rax, 24(%rcx)
	movq	%rsi, 24(%rsi)
	movq	%rsi, 16(%rsi)
.LBB0_129:                              #   in Loop: Header=BB0_63 Depth=1
	movq	%rsi, zz_hold(%rip)
	movq	8(%rsi), %rax
	cmpq	%rsi, %rax
	movq	72(%rsp), %r13          # 8-byte Reload
	je	.LBB0_131
# BB#130:                               #   in Loop: Header=BB0_63 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%rsi), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rsi
.LBB0_131:                              #   in Loop: Header=BB0_63 Depth=1
	movq	%rsi, zz_hold(%rip)
	movzbl	32(%rsi), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rsi), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rsi)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	24(%rbx), %rax
	cmpq	16(%rbx), %rax
	je	.LBB0_133
# BB#132:                               #   in Loop: Header=BB0_63 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	24(%rbx), %rax
.LBB0_133:                              #   in Loop: Header=BB0_63 Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_134
# BB#172:                               #   in Loop: Header=BB0_63 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_173
.LBB0_134:                              #   in Loop: Header=BB0_63 Depth=1
	xorl	%ecx, %ecx
.LBB0_173:                              #   in Loop: Header=BB0_63 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_175
# BB#174:                               #   in Loop: Header=BB0_63 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_175:                              #   in Loop: Header=BB0_63 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_177
# BB#176:                               #   in Loop: Header=BB0_63 Depth=1
	callq	DisposeObject
.LBB0_177:                              #   in Loop: Header=BB0_63 Depth=1
	movq	(%r14), %r14
	jmp	.LBB0_179
	.p2align	4, 0x90
.LBB0_63:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_98 Depth 2
                                        #     Child Loop BB0_104 Depth 2
                                        #     Child Loop BB0_108 Depth 2
                                        #     Child Loop BB0_114 Depth 2
                                        #       Child Loop BB0_115 Depth 3
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	cmpq	$19, %rax
	ja	.LBB0_179
# BB#64:                                #   in Loop: Header=BB0_63 Depth=1
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_178:                              #   in Loop: Header=BB0_63 Depth=1
	movzwl	42(%r13), %eax
	shrl	$5, %eax
	andl	$8, %eax
	movl	%eax, %ecx
	xorl	$8, %ecx
	addl	$-17, %ecx
	addl	$8, %eax
	andw	42(%rbx), %cx
	orl	%eax, %ecx
	movw	%cx, 42(%rbx)
.LBB0_179:                              # %.thread
                                        #   in Loop: Header=BB0_63 Depth=1
	movq	8(%r14), %r14
	leaq	8(%r14), %r12
	movq	8(%r14), %rbx
	cmpq	%r13, %rbx
	jne	.LBB0_63
.LBB0_180:                              # %.loopexit676
	movq	48(%rsp), %rdi
	movq	8(%rdi), %r14
	cmpq	%rdi, %r14
	je	.LBB0_181
# BB#182:                               # %.preheader669.lr.ph
	leaq	192(%rsp), %r15
	jmp	.LBB0_183
.LBB0_205:                              #   in Loop: Header=BB0_183 Depth=1
	cvtss2sd	%xmm0, %xmm0
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB0_215
# BB#206:                               #   in Loop: Header=BB0_183 Depth=1
	leaq	32(%rbx), %r13
	movl	$21, %edi
	testl	%eax, %eax
	je	.LBB0_207
# BB#208:                               #   in Loop: Header=BB0_183 Depth=1
	movl	$7, %esi
	movl	$.L.str.22, %edx
	jmp	.LBB0_209
.LBB0_215:                              #   in Loop: Header=BB0_183 Depth=1
	shll	$7, %eax
	cltd
	idivl	%ecx
	movl	%eax, 72(%rbp)
	movl	%eax, 64(%rbp)
	jmp	.LBB0_216
.LBB0_207:                              #   in Loop: Header=BB0_183 Depth=1
	movl	$6, %esi
	movl	$.L.str.21, %edx
.LBB0_209:                              #   in Loop: Header=BB0_183 Depth=1
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r12, %r8
	callq	Error
	movl	$128, 72(%rbp)
	movl	$128, 64(%rbp)
	movl	$11, %edi
	movl	$.L.str.23, %esi
	movq	%r13, %rdx
	callq	MakeWord
	movq	%rax, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r12)
	andl	$1610612736, 40(%r12)   # imm = 0x60000000
	movq	%rbx, zz_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB0_210
# BB#211:                               #   in Loop: Header=BB0_183 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rbx), %rcx
	movq	%rax, 24(%rcx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rax, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%r12, %r12
	movq	72(%rsp), %r13          # 8-byte Reload
	je	.LBB0_214
# BB#212:                               #   in Loop: Header=BB0_183 Depth=1
	testq	%rax, %rax
	je	.LBB0_214
# BB#213:                               #   in Loop: Header=BB0_183 Depth=1
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r12), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r12)
	movq	%r12, 24(%rcx)
	jmp	.LBB0_214
.LBB0_210:                              # %.thread836
                                        #   in Loop: Header=BB0_183 Depth=1
	movq	$0, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	movq	72(%rsp), %r13          # 8-byte Reload
.LBB0_214:                              #   in Loop: Header=BB0_183 Depth=1
	movq	%rbx, %rdi
	callq	DisposeObject
	movl	72(%rbp), %eax
	jmp	.LBB0_217
	.p2align	4, 0x90
.LBB0_183:                              # %.preheader669
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_184 Depth 2
                                        #     Child Loop BB0_193 Depth 2
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB0_184:                              #   Parent Loop BB0_183 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB0_184
# BB#185:                               #   in Loop: Header=BB0_183 Depth=1
	cmpb	$-120, %cl
	jne	.LBB0_220
# BB#186:                               #   in Loop: Header=BB0_183 Depth=1
	movq	80(%rax), %rbp
	cmpb	$34, 32(%rbp)
	je	.LBB0_188
# BB#187:                               #   in Loop: Header=BB0_183 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_188:                              #   in Loop: Header=BB0_183 Depth=1
	cmpl	$0, 64(%rbp)
	je	.LBB0_190
# BB#189:                               #   in Loop: Header=BB0_183 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.14, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_190:                              #   in Loop: Header=BB0_183 Depth=1
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB0_192
# BB#191:                               #   in Loop: Header=BB0_183 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbp), %rax
.LBB0_192:                              #   in Loop: Header=BB0_183 Depth=1
	leaq	32(%rbp), %r12
	addq	$16, %rax
	.p2align	4, 0x90
.LBB0_193:                              #   Parent Loop BB0_183 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbx
	leaq	16(%rbx), %rax
	cmpb	$0, 32(%rbx)
	je	.LBB0_193
# BB#194:                               #   in Loop: Header=BB0_183 Depth=1
	testb	$1, 43(%r13)
	jne	.LBB0_196
# BB#195:                               #   in Loop: Header=BB0_183 Depth=1
	movl	$21, %edi
	movl	$2, %esi
	movl	$.L.str.16, %edx
	movl	$1, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	movq	%r12, %r8
	callq	Error
.LBB0_196:                              #   in Loop: Header=BB0_183 Depth=1
	xorl	%edx, %edx
	movq	%rbp, %rdi
	leaq	104(%rsp), %rsi
	movq	%r15, %rcx
	callq	Constrained
	movl	108(%rsp), %eax
	cmpl	$8388607, 104(%rsp)     # imm = 0x7FFFFF
	jne	.LBB0_201
# BB#197:                               #   in Loop: Header=BB0_183 Depth=1
	cmpl	$8388607, %eax          # imm = 0x7FFFFF
	jne	.LBB0_201
# BB#198:                               #   in Loop: Header=BB0_183 Depth=1
	cmpl	$8388607, 112(%rsp)     # imm = 0x7FFFFF
	jne	.LBB0_201
# BB#199:                               #   in Loop: Header=BB0_183 Depth=1
	movl	$21, %edi
	movl	$3, %esi
	movl	$.L.str.18, %edx
	jmp	.LBB0_200
	.p2align	4, 0x90
.LBB0_201:                              #   in Loop: Header=BB0_183 Depth=1
	movl	56(%rbx), %ecx
	addl	48(%rbx), %ecx
	je	.LBB0_202
# BB#203:                               #   in Loop: Header=BB0_183 Depth=1
	cvtsi2ssl	%eax, %xmm0
	cvtsi2ssl	%ecx, %xmm1
	divss	%xmm1, %xmm0
	ucomiss	.LCPI0_0(%rip), %xmm0
	jbe	.LBB0_205
# BB#204:                               #   in Loop: Header=BB0_183 Depth=1
	movl	$21, %edi
	movl	$5, %esi
	movl	$.L.str.20, %edx
	jmp	.LBB0_200
.LBB0_202:                              #   in Loop: Header=BB0_183 Depth=1
	movl	$21, %edi
	movl	$4, %esi
	movl	$.L.str.19, %edx
.LBB0_200:                              #   in Loop: Header=BB0_183 Depth=1
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r12, %r8
	callq	Error
	movl	$128, 72(%rbp)
	movl	$128, 64(%rbp)
	movl	$128, %eax
.LBB0_216:                              #   in Loop: Header=BB0_183 Depth=1
	movq	%rbx, %r12
.LBB0_217:                              #   in Loop: Header=BB0_183 Depth=1
	movl	48(%r12), %ecx
	imull	%eax, %ecx
	movl	%ecx, %esi
	sarl	$31, %esi
	shrl	$25, %esi
	addl	%ecx, %esi
	sarl	$7, %esi
	imull	56(%r12), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$25, %edx
	addl	%eax, %edx
	sarl	$7, %edx
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	callq	AdjustSize
	testb	$1, 42(%rbp)
	je	.LBB0_219
# BB#218:                               #   in Loop: Header=BB0_183 Depth=1
	movl	72(%rbp), %eax
	movl	52(%r12), %ecx
	imull	%eax, %ecx
	movl	%ecx, %esi
	sarl	$31, %esi
	shrl	$25, %esi
	addl	%ecx, %esi
	sarl	$7, %esi
	imull	60(%r12), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$25, %edx
	addl	%eax, %edx
	sarl	$7, %edx
	movl	$1, %ecx
	movq	%rbp, %rdi
	callq	AdjustSize
.LBB0_219:                              #   in Loop: Header=BB0_183 Depth=1
	movq	48(%rsp), %rdi
.LBB0_220:                              # %.loopexit670
                                        #   in Loop: Header=BB0_183 Depth=1
	movq	8(%r14), %r14
	cmpq	%rdi, %r14
	jne	.LBB0_183
	jmp	.LBB0_221
.LBB0_181:
	movq	%r14, %rdi
.LBB0_221:                              # %._crit_edge716
	callq	DisposeObject
	movq	280(%rsp), %rax
	movq	$0, (%rax)
	movq	296(%rsp), %rax
	movq	$0, (%rax)
	movq	288(%rsp), %r15
	movq	$0, (%r15)
	movq	%r13, %rax
	movq	8(%rax), %r13
	cmpq	%rax, %r13
	je	.LBB0_268
# BB#222:                               # %.preheader667.lr.ph
	movl	60(%rsp), %eax          # 4-byte Reload
	andl	$1, %eax
	movzwl	%ax, %eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_223:                              # %.preheader667
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_224 Depth 2
                                        #     Child Loop BB0_231 Depth 2
                                        #       Child Loop BB0_232 Depth 3
                                        #       Child Loop BB0_293 Depth 3
                                        #       Child Loop BB0_310 Depth 3
                                        #         Child Loop BB0_311 Depth 4
                                        #       Child Loop BB0_297 Depth 3
                                        #         Child Loop BB0_298 Depth 4
                                        #       Child Loop BB0_302 Depth 3
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB0_224:                              #   Parent Loop BB0_223 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_224
# BB#225:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$1, %al
	je	.LBB0_267
# BB#226:                               #   in Loop: Header=BB0_223 Depth=1
	addb	$-119, %al
	cmpb	$20, %al
	jb	.LBB0_267
# BB#227:                               #   in Loop: Header=BB0_223 Depth=1
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_228
# BB#229:                               #   in Loop: Header=BB0_223 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_230
.LBB0_228:                              #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_230:                              #   in Loop: Header=BB0_223 Depth=1
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, 48(%rsp)
	movl	$1, %esi
	movq	%rbx, %rdi
	leaq	48(%rsp), %rdx
	callq	MinSize
	movq	48(%rsp), %rax
	movq	8(%rax), %r14
	cmpq	%rax, %r14
	jne	.LBB0_231
	jmp	.LBB0_258
	.p2align	4, 0x90
.LBB0_235:                              #   in Loop: Header=BB0_231 Depth=2
	movl	%edi, %ecx
	addb	$-120, %cl
	cmpb	$7, %cl
	ja	.LBB0_236
# BB#325:                               #   in Loop: Header=BB0_231 Depth=2
	movzbl	%cl, %ecx
	jmpq	*.LJTI0_1(,%rcx,8)
.LBB0_279:                              #   in Loop: Header=BB0_231 Depth=2
	testl	%r12d, %r12d
	jne	.LBB0_257
# BB#280:                               #   in Loop: Header=BB0_231 Depth=2
	movq	296(%rsp), %rax
	cmpq	$0, (%rax)
	jne	.LBB0_285
# BB#281:                               #   in Loop: Header=BB0_231 Depth=2
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_282
# BB#283:                               #   in Loop: Header=BB0_231 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_284
.LBB0_236:                              #   in Loop: Header=BB0_231 Depth=2
	cmpb	$-119, %dil
	je	.LBB0_292
# BB#237:                               #   in Loop: Header=BB0_231 Depth=2
	cmpb	$-118, %dil
	je	.LBB0_257
.LBB0_238:                              #   in Loop: Header=BB0_231 Depth=2
	movq	no_fpos(%rip), %rbx
	callq	Image
	movq	%rax, (%rsp)
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.28, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.29, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	jmp	.LBB0_257
.LBB0_239:                              #   in Loop: Header=BB0_231 Depth=2
	cmpl	$0, 140(%rsp)           # 4-byte Folded Reload
	setne	%cl
	movq	80(%rbx), %rdx
	movq	80(%rdx), %rsi
	movzwl	41(%rsi), %esi
	andl	$2048, %esi             # imm = 0x800
	shrl	$11, %esi
	andb	%cl, %sil
	movzbl	%sil, %ecx
	shll	$6, %ecx
	andl	$-98, %eax
	addl	88(%rsp), %eax          # 4-byte Folded Reload
	orl	%ecx, %eax
	movw	%ax, 42(%rbx)
	movq	80(%rdx), %rax
	movq	GalleySym(%rip), %rcx
	cmpq	%rcx, %rax
	je	.LBB0_241
# BB#240:                               #   in Loop: Header=BB0_231 Depth=2
	movq	ForceGalleySym(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.LBB0_242
.LBB0_241:                              #   in Loop: Header=BB0_231 Depth=2
	movq	280(%rsp), %rax
	movq	%rbx, (%rax)
	movq	80(%rbx), %rax
	movq	80(%rax), %rax
	movq	GalleySym(%rip), %rcx
	movq	ForceGalleySym(%rip), %rdx
.LBB0_242:                              #   in Loop: Header=BB0_231 Depth=2
	cmpq	InputSym(%rip), %rax
	movl	$1, %esi
	cmovel	%esi, %r12d
	cmpq	%rdx, %rax
	cmovel	%esi, %r12d
	cmpq	%rcx, %rax
	cmovel	%esi, %r12d
	jmp	.LBB0_257
.LBB0_243:                              #   in Loop: Header=BB0_231 Depth=2
	cmpq	$0, (%r15)
	jne	.LBB0_248
# BB#244:                               #   in Loop: Header=BB0_231 Depth=2
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_245
# BB#246:                               #   in Loop: Header=BB0_231 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_247
.LBB0_292:                              #   in Loop: Header=BB0_231 Depth=2
	movl	%r12d, 40(%rsp)         # 4-byte Spill
	movq	80(%rbx), %rbx
	leaq	32(%rbx), %r10
	movzbl	32(%rbx), %r15d
	leaq	16(%rbx), %rax
	leaq	24(%rbx), %rcx
	xorl	%edx, %edx
	xorl	%r12d, %r12d
	cmpl	$32, %r15d
	sete	%dl
	setne	%r12b
	leal	17(%rdx,%rdx), %r8d
	cmovneq	%rax, %rcx
	leal	30(%r12), %ebp
	movq	(%rcx), %rax
	.p2align	4, 0x90
.LBB0_293:                              #   Parent Loop BB0_223 Depth=1
                                        #     Parent Loop BB0_231 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rax
	movzbl	32(%rax), %edi
	testl	%edi, %edi
	je	.LBB0_293
# BB#294:                               # %.preheader
                                        #   in Loop: Header=BB0_231 Depth=2
	xorl	%esi, %esi
	cmpb	$32, %r15b
	sete	%sil
	setne	%r9b
	leal	15(%rsi), %ecx
	cmpb	$9, %dil
	je	.LBB0_296
# BB#295:                               # %.preheader
                                        #   in Loop: Header=BB0_231 Depth=2
	cmpl	%ecx, %edi
	jne	.LBB0_301
.LBB0_296:                              # %.critedge.lr.ph
                                        #   in Loop: Header=BB0_231 Depth=2
	cmpb	$32, %r15b
	jne	.LBB0_310
	.p2align	4, 0x90
.LBB0_297:                              # %.critedge.us
                                        #   Parent Loop BB0_223 Depth=1
                                        #     Parent Loop BB0_231 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_298 Depth 4
	movq	24(%rax), %rax
	.p2align	4, 0x90
.LBB0_298:                              #   Parent Loop BB0_223 Depth=1
                                        #     Parent Loop BB0_231 Depth=2
                                        #       Parent Loop BB0_297 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rax), %rax
	movzbl	32(%rax), %edi
	testl	%edi, %edi
	je	.LBB0_298
# BB#299:                               # %.loopexit.us
                                        #   in Loop: Header=BB0_297 Depth=3
	cmpb	$9, %dil
	je	.LBB0_297
# BB#300:                               # %.loopexit.us
                                        #   in Loop: Header=BB0_297 Depth=3
	cmpl	%ecx, %edi
	je	.LBB0_297
	jmp	.LBB0_301
	.p2align	4, 0x90
.LBB0_310:                              # %.critedge
                                        #   Parent Loop BB0_223 Depth=1
                                        #     Parent Loop BB0_231 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_311 Depth 4
	movq	16(%rax), %rax
	.p2align	4, 0x90
.LBB0_311:                              #   Parent Loop BB0_223 Depth=1
                                        #     Parent Loop BB0_231 Depth=2
                                        #       Parent Loop BB0_310 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rax), %rax
	movzbl	32(%rax), %edi
	testl	%edi, %edi
	je	.LBB0_311
# BB#308:                               # %.loopexit
                                        #   in Loop: Header=BB0_310 Depth=3
	cmpb	$9, %dil
	je	.LBB0_310
# BB#309:                               # %.loopexit
                                        #   in Loop: Header=BB0_310 Depth=3
	cmpl	%ecx, %edi
	je	.LBB0_310
.LBB0_301:                              # %._crit_edge
                                        #   in Loop: Header=BB0_231 Depth=2
	orl	$18, %esi
	orb	$24, %r9b
	movq	8(%rbx), %rdx
	.p2align	4, 0x90
.LBB0_302:                              #   Parent Loop BB0_223 Depth=1
                                        #     Parent Loop BB0_231 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdx), %rdx
	cmpb	$0, 32(%rdx)
	je	.LBB0_302
# BB#303:                               #   in Loop: Header=BB0_231 Depth=2
	movzbl	%dil, %ecx
	cmpl	%r8d, %ecx
	je	.LBB0_312
# BB#304:                               #   in Loop: Header=BB0_231 Depth=2
	cmpl	%esi, %ecx
	je	.LBB0_312
# BB#305:                               #   in Loop: Header=BB0_231 Depth=2
	movl	%r15d, %edi
	movq	%r10, %rbx
	callq	Image
	movq	%rax, %r15
	movl	%ebp, %edi
	callq	Image
	movq	%rax, (%rsp)
	movl	$21, %edi
	movl	$8, %esi
	movl	$.L.str.24, %edx
	jmp	.LBB0_306
.LBB0_312:                              #   in Loop: Header=BB0_231 Depth=2
	movl	48(%rdx,%r12,4), %ecx
	movl	56(%rdx,%r12,4), %edi
	testl	%ecx, %ecx
	je	.LBB0_313
# BB#316:                               # %.thread644
                                        #   in Loop: Header=BB0_231 Depth=2
	testl	%edi, %edi
	je	.LBB0_317
# BB#318:                               #   in Loop: Header=BB0_231 Depth=2
	movl	%ebp, 80(%rsp)          # 4-byte Spill
	movq	%r10, 64(%rsp)          # 8-byte Spill
	movl	48(%rax,%r12,4), %edx
	movl	56(%rax,%r12,4), %eax
	movl	%edx, %esi
	addl	%eax, %esi
	jne	.LBB0_320
# BB#319:                               #   in Loop: Header=BB0_231 Depth=2
	movl	%r15d, %edi
	callq	Image
	movq	%rax, %r15
	movl	80(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %edi
	callq	Image
	movq	%rax, (%rsp)
	movl	$21, %edi
	movl	$10, %esi
	movl	$.L.str.26, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	64(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_307
.LBB0_313:                              #   in Loop: Header=BB0_231 Depth=2
	testl	%edi, %edi
	je	.LBB0_314
.LBB0_317:                              #   in Loop: Header=BB0_231 Depth=2
	movl	%r15d, %edi
	movq	%r10, %rbx
	callq	Image
	movq	%rax, %r15
	movl	%ebp, %edi
	callq	Image
	movq	%rax, (%rsp)
	movl	$21, %edi
	movl	$9, %esi
	movl	$.L.str.25, %edx
	jmp	.LBB0_306
.LBB0_245:                              #   in Loop: Header=BB0_231 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_247:                              #   in Loop: Header=BB0_231 Depth=2
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, (%r15)
.LBB0_248:                              #   in Loop: Header=BB0_231 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_249
# BB#250:                               #   in Loop: Header=BB0_231 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_251
.LBB0_249:                              #   in Loop: Header=BB0_231 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_251:                              #   in Loop: Header=BB0_231 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%r15), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_254
# BB#252:                               #   in Loop: Header=BB0_231 Depth=2
	testq	%rcx, %rcx
	je	.LBB0_254
# BB#253:                               #   in Loop: Header=BB0_231 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_254:                              #   in Loop: Header=BB0_231 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	jne	.LBB0_256
	jmp	.LBB0_257
.LBB0_320:                              #   in Loop: Header=BB0_231 Depth=2
	cvtsi2ssl	%edx, %xmm0
	cvtsi2ssl	%ecx, %xmm1
	divss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	cvtsi2ssl	%edi, %xmm2
	divss	%xmm2, %xmm1
	maxss	%xmm0, %xmm1
	mulss	.LCPI0_2(%rip), %xmm1
	cvttss2si	%xmm1, %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	imull	%eax, %ecx
	movl	%ecx, %ebp
	sarl	$31, %ebp
	shrl	$25, %ebp
	addl	%ecx, %ebp
	sarl	$7, %ebp
	movl	%edi, 136(%rsp)         # 4-byte Spill
	movq	%rbx, %rdi
	leaq	104(%rsp), %rsi
	movl	%r12d, %edx
	leaq	192(%rsp), %rcx
	callq	Constrained
	movl	60(%rsp), %edi          # 4-byte Reload
	cmpl	104(%rsp), %ebp
	jle	.LBB0_321
.LBB0_324:                              #   in Loop: Header=BB0_231 Depth=2
	movq	64(%rsp), %rbx          # 8-byte Reload
	movzbl	(%rbx), %edi
	callq	Image
	movq	%rax, %r15
	movl	80(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %edi
	callq	Image
	movq	%rax, (%rsp)
	movl	$21, %edi
	movl	$11, %esi
	movl	$.L.str.27, %edx
.LBB0_306:                              #   in Loop: Header=BB0_231 Depth=2
	movl	$2, %ecx
	xorl	%eax, %eax
.LBB0_307:                              #   in Loop: Header=BB0_231 Depth=2
	movq	%rbx, %r8
	movq	%r15, %r9
	callq	Error
	movb	%bpl, (%rbx)
.LBB0_315:                              #   in Loop: Header=BB0_231 Depth=2
	movq	288(%rsp), %r15
	movl	40(%rsp), %r12d         # 4-byte Reload
	jmp	.LBB0_257
.LBB0_314:                              #   in Loop: Header=BB0_231 Depth=2
	movb	%r9b, (%r10)
	jmp	.LBB0_315
.LBB0_282:                              #   in Loop: Header=BB0_231 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_284:                              #   in Loop: Header=BB0_231 Depth=2
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	296(%rsp), %rcx
	movq	%rax, (%rcx)
.LBB0_285:                              #   in Loop: Header=BB0_231 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_286
# BB#287:                               #   in Loop: Header=BB0_231 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_288
.LBB0_286:                              #   in Loop: Header=BB0_231 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_288:                              #   in Loop: Header=BB0_231 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	296(%rsp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_291
# BB#289:                               #   in Loop: Header=BB0_231 Depth=2
	testq	%rcx, %rcx
	je	.LBB0_291
# BB#290:                               #   in Loop: Header=BB0_231 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_291:                              #   in Loop: Header=BB0_231 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	xorl	%r12d, %r12d
	testq	%rax, %rax
	je	.LBB0_257
.LBB0_256:                              #   in Loop: Header=BB0_231 Depth=2
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB0_257
.LBB0_321:                              #   in Loop: Header=BB0_231 Depth=2
	movl	136(%rsp), %eax         # 4-byte Reload
	imull	%edi, %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$25, %edx
	addl	%eax, %edx
	sarl	$7, %edx
	leal	(%rbp,%rdx), %eax
	cmpl	108(%rsp), %eax
	jg	.LBB0_324
# BB#322:                               #   in Loop: Header=BB0_231 Depth=2
	cmpl	112(%rsp), %edx
	jg	.LBB0_324
# BB#323:                               #   in Loop: Header=BB0_231 Depth=2
	cmpb	$32, %r15b
	movb	$34, 32(%rbx)
	movl	$128, %eax
	cmovel	%edi, %eax
	movl	$128, %ecx
	cmovel	%ecx, %edi
	movl	%eax, 64(%rbx)
	movl	%edi, 72(%rbx)
	movq	%rbx, %rdi
	movl	%ebp, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r12d, %ecx
	callq	AdjustSize
	jmp	.LBB0_315
	.p2align	4, 0x90
.LBB0_231:                              # %.preheader666
                                        #   Parent Loop BB0_223 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_232 Depth 3
                                        #       Child Loop BB0_293 Depth 3
                                        #       Child Loop BB0_310 Depth 3
                                        #         Child Loop BB0_311 Depth 4
                                        #       Child Loop BB0_297 Depth 3
                                        #         Child Loop BB0_298 Depth 4
                                        #       Child Loop BB0_302 Depth 3
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB0_232:                              #   Parent Loop BB0_223 Depth=1
                                        #     Parent Loop BB0_231 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %edi
	testl	%edi, %edi
	je	.LBB0_232
# BB#233:                               #   in Loop: Header=BB0_231 Depth=2
	movzwl	42(%rbx), %eax
	movl	%eax, %ecx
	andl	$65503, %ecx            # imm = 0xFFDF
	movw	%cx, 42(%rbx)
	cmpb	$-120, %dil
	jg	.LBB0_235
# BB#234:                               #   in Loop: Header=BB0_231 Depth=2
	cmpb	$-119, %dil
	jge	.LBB0_238
.LBB0_257:                              #   in Loop: Header=BB0_231 Depth=2
	movq	8(%r14), %r14
	cmpq	48(%rsp), %r14
	jne	.LBB0_231
.LBB0_258:                              # %._crit_edge705
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	8(%r14), %rbx
	cmpq	%r14, %rbx
	je	.LBB0_263
# BB#259:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$0, 32(%rbx)
	je	.LBB0_261
# BB#260:                               #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_261:                              #   in Loop: Header=BB0_223 Depth=1
	movq	%rbx, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%rbx, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB0_263
# BB#262:                               #   in Loop: Header=BB0_223 Depth=1
	movq	(%r13), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB0_263:                              #   in Loop: Header=BB0_223 Depth=1
	movq	48(%rsp), %rax
	cmpq	%rax, 8(%rax)
	jne	.LBB0_265
# BB#264:                               #   in Loop: Header=BB0_223 Depth=1
	cmpq	%rax, 24(%rax)
	je	.LBB0_266
.LBB0_265:                              #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.30, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	48(%rsp), %rax
.LBB0_266:                              #   in Loop: Header=BB0_223 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
.LBB0_267:                              # %.loopexit668
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	8(%r13), %r13
	cmpq	72(%rsp), %r13          # 8-byte Folded Reload
	jne	.LBB0_223
.LBB0_268:                              # %._crit_edge713
	movq	96(%rsp), %rbp
	testq	%rbp, %rbp
	movq	72(%rsp), %r15          # 8-byte Reload
	je	.LBB0_275
# BB#269:
	movq	8(%rbp), %rbx
	cmpq	%rbp, %rbx
	je	.LBB0_274
# BB#270:
	movq	176(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %r14
	cmpb	$0, 32(%rbx)
	je	.LBB0_272
# BB#271:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_272:
	movq	%rbx, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%rbx, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB0_274
# BB#273:
	movq	(%r14), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB0_274:
	movq	96(%rsp), %rdi
	callq	DisposeObject
.LBB0_275:
	movq	272(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_278
# BB#276:
	movq	280(%rsp), %rax
	cmpq	$0, (%rax)
	jne	.LBB0_278
# BB#277:
	callq	SymName
	movq	%rax, %rbx
	movq	80(%r15), %rdi
	callq	SymName
	movq	%rax, (%rsp)
	movl	$21, %edi
	movl	$12, %esi
	movl	$.L.str.31, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	184(%rsp), %r8          # 8-byte Reload
	movq	%rbx, %r9
	callq	Error
.LBB0_278:
	orb	$2, 42(%r15)
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_39:
	cmpq	(%rcx), %rdx
	jne	.LBB0_41
# BB#40:
	cmpq	%rsi, %rax
	je	.LBB0_42
	jmp	.LBB0_41
.Lfunc_end0:
	.size	SizeGalley, .Lfunc_end0-SizeGalley
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_63
	.quad	.LBB0_65
	.quad	.LBB0_178
	.quad	.LBB0_179
	.quad	.LBB0_179
	.quad	.LBB0_179
	.quad	.LBB0_179
	.quad	.LBB0_179
	.quad	.LBB0_178
	.quad	.LBB0_95
	.quad	.LBB0_179
	.quad	.LBB0_179
	.quad	.LBB0_179
	.quad	.LBB0_179
	.quad	.LBB0_179
	.quad	.LBB0_179
	.quad	.LBB0_179
	.quad	.LBB0_81
	.quad	.LBB0_179
	.quad	.LBB0_67
.LJTI0_1:
	.quad	.LBB0_279
	.quad	.LBB0_239
	.quad	.LBB0_238
	.quad	.LBB0_243
	.quad	.LBB0_238
	.quad	.LBB0_238
	.quad	.LBB0_238
	.quad	.LBB0_257

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SizeGalley: precondition!"
	.size	.L.str.1, 26

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"SizeGalley: already sized!"
	.size	.L.str.2, 27

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"SizeGalley: threads!"
	.size	.L.str.4, 21

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"galley %s must have just one column mark"
	.size	.L.str.5, 41

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%s,%s object too wide for available space"
	.size	.L.str.6, 42

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"SizeGalley: BreakObject failed to fit!"
	.size	.L.str.7, 39

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"TransferLinks: start_link!"
	.size	.L.str.8, 27

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"SizeGalley COL_THR: Up(y)!=LastUp(y)!"
	.size	.L.str.9, 38

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"SizeGalley: missing COL_THR!"
	.size	.L.str.10, 29

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"SizeGalley/SPLIT: hor != thor!"
	.size	.L.str.11, 31

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"SizeGalley COL_THR: Up(y) != LastUp(y)!"
	.size	.L.str.12, 40

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"SizeObject: type(z) != SCALE!"
	.size	.L.str.13, 30

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"SizeObject: bc(constraint(z)) != 0"
	.size	.L.str.14, 35

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"SizeObject SCALE: Down(z) == z!"
	.size	.L.str.15, 32

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%s with unspecified scale factor in horizontal galley"
	.size	.L.str.16, 54

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"@Scale"
	.size	.L.str.17, 7

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"replacing infinite scale factor (unconstrained width) by 1.0"
	.size	.L.str.18, 61

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"replacing infinite scale factor (zero width object) by 1.0"
	.size	.L.str.19, 59

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"replacing very large scale factor (over 100) by 1.0"
	.size	.L.str.20, 52

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"object deleted (scale factor is zero)"
	.size	.L.str.21, 38

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"object deleted (scale factor is smaller than 0.01)"
	.size	.L.str.22, 51

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.zero	1
	.size	.L.str.23, 1

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"%s replaced by %s (mark not shared)"
	.size	.L.str.24, 36

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"%s replaced by %s (infinite scale factor)"
	.size	.L.str.25, 42

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"%s replaced by %s (zero scale factor)"
	.size	.L.str.26, 38

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"%s replaced by %s (insufficient space)"
	.size	.L.str.27, 39

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"assert failed in %s %s"
	.size	.L.str.28, 23

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"SizeGalley:"
	.size	.L.str.29, 12

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"SizeG: extras!"
	.size	.L.str.30, 15

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"unexpected absence of %s from the body of %s"
	.size	.L.str.31, 45


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
