	.text
	.file	"add.bc"
	.globl	gsm_add
	.p2align	4, 0x90
	.type	gsm_add,@function
gsm_add:                                # @gsm_add
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	movslq	%esi, %rcx
	addq	%rax, %rcx
	cmpq	$32767, %rcx            # imm = 0x7FFF
	movw	$32767, %dx             # imm = 0x7FFF
	cmovlw	%cx, %dx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movw	$-32768, %ax            # imm = 0x8000
	cmovgew	%dx, %ax
	retq
.Lfunc_end0:
	.size	gsm_add, .Lfunc_end0-gsm_add
	.cfi_endproc

	.globl	gsm_sub
	.p2align	4, 0x90
	.type	gsm_sub,@function
gsm_sub:                                # @gsm_sub
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	movslq	%esi, %rcx
	subq	%rcx, %rax
	cmpq	$32767, %rax            # imm = 0x7FFF
	movw	$32767, %cx             # imm = 0x7FFF
	cmovlw	%ax, %cx
	cmpq	$-32768, %rax           # imm = 0x8000
	movw	$-32768, %ax            # imm = 0x8000
	cmovgew	%cx, %ax
	retq
.Lfunc_end1:
	.size	gsm_sub, .Lfunc_end1-gsm_sub
	.cfi_endproc

	.globl	gsm_mult
	.p2align	4, 0x90
	.type	gsm_mult,@function
gsm_mult:                               # @gsm_mult
	.cfi_startproc
# BB#0:
	movzwl	%di, %eax
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB2_2
# BB#1:
	movw	$32767, %ax             # imm = 0x7FFF
	movzwl	%si, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	je	.LBB2_3
.LBB2_2:
	movswl	%di, %ecx
	movswl	%si, %eax
	imull	%ecx, %eax
	shrl	$15, %eax
.LBB2_3:
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	retq
.Lfunc_end2:
	.size	gsm_mult, .Lfunc_end2-gsm_mult
	.cfi_endproc

	.globl	gsm_mult_r
	.p2align	4, 0x90
	.type	gsm_mult_r,@function
gsm_mult_r:                             # @gsm_mult_r
	.cfi_startproc
# BB#0:
	movzwl	%di, %eax
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB3_2
# BB#1:
	movw	$32767, %ax             # imm = 0x7FFF
	movzwl	%si, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	je	.LBB3_3
.LBB3_2:
	movswl	%di, %ecx
	movswl	%si, %eax
	imull	%ecx, %eax
	addl	$16384, %eax            # imm = 0x4000
	shrl	$15, %eax
.LBB3_3:
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	retq
.Lfunc_end3:
	.size	gsm_mult_r, .Lfunc_end3-gsm_mult_r
	.cfi_endproc

	.globl	gsm_abs
	.p2align	4, 0x90
	.type	gsm_abs,@function
gsm_abs:                                # @gsm_abs
	.cfi_startproc
# BB#0:
	testw	%di, %di
	js	.LBB4_1
# BB#2:
	movl	%edi, %eax
	retq
.LBB4_1:
	movzwl	%di, %eax
	negl	%edi
	cmpl	$32768, %eax            # imm = 0x8000
	movw	$32767, %ax             # imm = 0x7FFF
	cmovnew	%di, %ax
	retq
.Lfunc_end4:
	.size	gsm_abs, .Lfunc_end4-gsm_abs
	.cfi_endproc

	.globl	gsm_L_mult
	.p2align	4, 0x90
	.type	gsm_L_mult,@function
gsm_L_mult:                             # @gsm_L_mult
	.cfi_startproc
# BB#0:
	movslq	%edi, %rcx
	movslq	%esi, %rax
	imulq	%rcx, %rax
	addq	%rax, %rax
	retq
.Lfunc_end5:
	.size	gsm_L_mult, .Lfunc_end5-gsm_L_mult
	.cfi_endproc

	.globl	gsm_L_add
	.p2align	4, 0x90
	.type	gsm_L_add,@function
gsm_L_add:                              # @gsm_L_add
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	js	.LBB6_1
# BB#4:
	addq	%rsi, %rdi
	testq	%rsi, %rsi
	jle	.LBB6_6
# BB#5:
	cmpq	$2147483647, %rdi       # imm = 0x7FFFFFFF
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	cmovaeq	%rax, %rdi
.LBB6_6:
	movq	%rdi, %rax
	retq
.LBB6_1:
	testq	%rsi, %rsi
	js	.LBB6_3
# BB#2:
	addq	%rsi, %rdi
	movq	%rdi, %rax
	retq
.LBB6_3:
	notq	%rdi
	notq	%rsi
	addq	%rdi, %rsi
	movq	$-2, %rax
	subq	%rsi, %rax
	cmpq	$2147483646, %rsi       # imm = 0x7FFFFFFE
	movq	$-2147483648, %rdi      # imm = 0x80000000
	cmovbeq	%rax, %rdi
	movq	%rdi, %rax
	retq
.Lfunc_end6:
	.size	gsm_L_add, .Lfunc_end6-gsm_L_add
	.cfi_endproc

	.globl	gsm_L_sub
	.p2align	4, 0x90
	.type	gsm_L_sub,@function
gsm_L_sub:                              # @gsm_L_sub
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	js	.LBB7_4
# BB#1:
	testq	%rsi, %rsi
	js	.LBB7_3
# BB#2:
	subq	%rsi, %rdi
	movq	%rdi, %rax
	retq
.LBB7_4:
	testq	%rsi, %rsi
	jle	.LBB7_5
# BB#6:
	notq	%rdi
	addq	%rdi, %rsi
	cmpq	$2147483646, %rsi       # imm = 0x7FFFFFFE
	notq	%rsi
	movq	$-2147483648, %rdi      # imm = 0x80000000
	cmovbeq	%rsi, %rdi
	movq	%rdi, %rax
	retq
.LBB7_3:
	notq	%rsi
	leaq	(%rsi,%rdi), %rax
	cmpq	$2147483646, %rax       # imm = 0x7FFFFFFE
	leaq	1(%rsi,%rdi), %rax
	movl	$2147483647, %edi       # imm = 0x7FFFFFFF
	cmovbeq	%rax, %rdi
	movq	%rdi, %rax
	retq
.LBB7_5:
	subq	%rsi, %rdi
	movq	%rdi, %rax
	retq
.Lfunc_end7:
	.size	gsm_L_sub, .Lfunc_end7-gsm_L_sub
	.cfi_endproc

	.globl	gsm_norm
	.p2align	4, 0x90
	.type	gsm_norm,@function
gsm_norm:                               # @gsm_norm
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	jns	.LBB8_4
# BB#1:
	cmpq	$-1073741823, %rdi      # imm = 0xC0000001
	jge	.LBB8_3
# BB#2:
	xorl	%eax, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	retq
.LBB8_3:
	notq	%rdi
.LBB8_4:
	testl	$-65536, %edi           # imm = 0xFFFF0000
	je	.LBB8_8
# BB#5:
	movl	$4278190080, %ecx       # imm = 0xFF000000
	andq	%rdi, %rcx
	movw	$-1, %dx
	movw	$7, %ax
	cmovnew	%dx, %ax
	testq	%rcx, %rcx
	movb	$24, %cl
	jne	.LBB8_7
# BB#6:
	movb	$16, %cl
.LBB8_7:
	sarq	%cl, %rdi
	jmp	.LBB8_9
.LBB8_8:
	movq	%rdi, %rcx
	sarq	$8, %rcx
	movq	%rdi, %rdx
	andq	$65280, %rdx            # imm = 0xFF00
	movw	$15, %si
	movw	$23, %ax
	cmovnew	%si, %ax
	testq	%rdx, %rdx
	cmovneq	%rcx, %rdi
.LBB8_9:
	movzbl	%dil, %ecx
	movzbl	bitoff(%rcx), %ecx
	addl	%ecx, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	retq
.Lfunc_end8:
	.size	gsm_norm, .Lfunc_end8-gsm_norm
	.cfi_endproc

	.globl	gsm_L_asl
	.p2align	4, 0x90
	.type	gsm_L_asl,@function
gsm_L_asl:                              # @gsm_L_asl
	.cfi_startproc
# BB#0:
	cmpl	$31, %esi
	jle	.LBB9_2
# BB#1:
	xorl	%edi, %edi
	movq	%rdi, %rax
	retq
.LBB9_2:
	cmpl	$-32, %esi
	jg	.LBB9_4
# BB#3:
	sarq	$63, %rdi
	movq	%rdi, %rax
	retq
.LBB9_4:
	testl	%esi, %esi
	js	.LBB9_5
# BB#6:
	movl	%esi, %ecx
	shlq	%cl, %rdi
	movq	%rdi, %rax
	retq
.LBB9_5:
	negl	%esi
	movl	%esi, %ecx
	sarq	%cl, %rdi
	movq	%rdi, %rax
	retq
.Lfunc_end9:
	.size	gsm_L_asl, .Lfunc_end9-gsm_L_asl
	.cfi_endproc

	.globl	gsm_L_asr
	.p2align	4, 0x90
	.type	gsm_L_asr,@function
gsm_L_asr:                              # @gsm_L_asr
	.cfi_startproc
# BB#0:
	cmpl	$32, %esi
	jl	.LBB10_2
# BB#1:
	sarq	$63, %rdi
	movq	%rdi, %rax
	retq
.LBB10_2:
	cmpl	$-31, %esi
	jge	.LBB10_4
# BB#3:
	xorl	%edi, %edi
	movq	%rdi, %rax
	retq
.LBB10_4:
	testl	%esi, %esi
	js	.LBB10_5
# BB#6:
	movl	%esi, %ecx
	sarq	%cl, %rdi
	movq	%rdi, %rax
	retq
.LBB10_5:
	negl	%esi
	movl	%esi, %ecx
	shlq	%cl, %rdi
	movq	%rdi, %rax
	retq
.Lfunc_end10:
	.size	gsm_L_asr, .Lfunc_end10-gsm_L_asr
	.cfi_endproc

	.globl	gsm_asl
	.p2align	4, 0x90
	.type	gsm_asl,@function
gsm_asl:                                # @gsm_asl
	.cfi_startproc
# BB#0:
	cmpl	$15, %esi
	jle	.LBB11_2
# BB#1:
	xorl	%edi, %edi
	movl	%edi, %eax
	retq
.LBB11_2:
	cmpl	$-16, %esi
	jg	.LBB11_4
# BB#3:
	sarw	$15, %di
	movl	%edi, %eax
	retq
.LBB11_4:
	testl	%esi, %esi
	js	.LBB11_5
# BB#6:
	movswl	%di, %edi
	movl	%esi, %ecx
	shll	%cl, %edi
	movl	%edi, %eax
	retq
.LBB11_5:
	negl	%esi
	movswl	%di, %edi
	movl	%esi, %ecx
	sarl	%cl, %edi
	movl	%edi, %eax
	retq
.Lfunc_end11:
	.size	gsm_asl, .Lfunc_end11-gsm_asl
	.cfi_endproc

	.globl	gsm_asr
	.p2align	4, 0x90
	.type	gsm_asr,@function
gsm_asr:                                # @gsm_asr
	.cfi_startproc
# BB#0:
	cmpl	$16, %esi
	jl	.LBB12_2
# BB#1:
	sarw	$15, %di
	movl	%edi, %eax
	retq
.LBB12_2:
	cmpl	$-15, %esi
	jge	.LBB12_4
# BB#3:
	xorl	%edi, %edi
	movl	%edi, %eax
	retq
.LBB12_4:
	movswl	%di, %edi
	testl	%esi, %esi
	js	.LBB12_5
# BB#6:
	movl	%esi, %ecx
	sarl	%cl, %edi
	movl	%edi, %eax
	retq
.LBB12_5:
	negl	%esi
	movl	%esi, %ecx
	shll	%cl, %edi
	movl	%edi, %eax
	retq
.Lfunc_end12:
	.size	gsm_asr, .Lfunc_end12-gsm_asr
	.cfi_endproc

	.globl	gsm_div
	.p2align	4, 0x90
	.type	gsm_div,@function
gsm_div:                                # @gsm_div
	.cfi_startproc
# BB#0:
	testw	%di, %di
	je	.LBB13_1
# BB#2:                                 # %.preheader.preheader
	movslq	%esi, %rdx
	movswq	%di, %rcx
	addq	%rcx, %rcx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	movq	%rdx, %rdi
	cmovlq	%r8, %rdi
	setge	%sil
	subq	%rdi, %rcx
	addq	%rcx, %rcx
	xorl	%edi, %edi
	cmpq	%rdx, %rcx
	movq	%rdx, %rax
	cmovlq	%r8, %rax
	setge	%dil
	subq	%rax, %rcx
	leal	(%rdi,%rsi,2), %eax
	addq	%rcx, %rcx
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	movq	%rdx, %rdi
	cmovlq	%r8, %rdi
	setge	%sil
	subq	%rdi, %rcx
	leal	(%rsi,%rax,2), %eax
	addq	%rcx, %rcx
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	movq	%rdx, %rdi
	cmovlq	%r8, %rdi
	setge	%sil
	subq	%rdi, %rcx
	leal	(%rsi,%rax,2), %eax
	addq	%rcx, %rcx
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	movq	%rdx, %rdi
	cmovlq	%r8, %rdi
	setge	%sil
	subq	%rdi, %rcx
	leal	(%rsi,%rax,2), %eax
	addq	%rcx, %rcx
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	movq	%rdx, %rdi
	cmovlq	%r8, %rdi
	setge	%sil
	subq	%rdi, %rcx
	leal	(%rsi,%rax,2), %eax
	addq	%rcx, %rcx
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	movq	%rdx, %rdi
	cmovlq	%r8, %rdi
	setge	%sil
	subq	%rdi, %rcx
	leal	(%rsi,%rax,2), %eax
	addq	%rcx, %rcx
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	movq	%rdx, %rdi
	cmovlq	%r8, %rdi
	setge	%sil
	subq	%rdi, %rcx
	leal	(%rsi,%rax,2), %eax
	addq	%rcx, %rcx
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	movq	%rdx, %rdi
	cmovlq	%r8, %rdi
	setge	%sil
	subq	%rdi, %rcx
	leal	(%rsi,%rax,2), %eax
	addq	%rcx, %rcx
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	movq	%rdx, %rdi
	cmovlq	%r8, %rdi
	setge	%sil
	subq	%rdi, %rcx
	leal	(%rsi,%rax,2), %eax
	addq	%rcx, %rcx
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	movq	%rdx, %rdi
	cmovlq	%r8, %rdi
	setge	%sil
	subq	%rdi, %rcx
	leal	(%rsi,%rax,2), %eax
	addq	%rcx, %rcx
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	movq	%rdx, %rdi
	cmovlq	%r8, %rdi
	setge	%sil
	subq	%rdi, %rcx
	leal	(%rsi,%rax,2), %eax
	addq	%rcx, %rcx
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	movq	%rdx, %rdi
	cmovlq	%r8, %rdi
	setge	%sil
	subq	%rdi, %rcx
	leal	(%rsi,%rax,2), %eax
	addq	%rcx, %rcx
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	cmovgeq	%rdx, %r8
	setge	%sil
	subq	%r8, %rcx
	leal	(%rsi,%rax,2), %eax
	addq	%rcx, %rcx
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	setge	%sil
	leal	(%rsi,%rax,2), %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	retq
.LBB13_1:
	xorl	%eax, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	retq
.Lfunc_end13:
	.size	gsm_div, .Lfunc_end13-gsm_div
	.cfi_endproc

	.type	bitoff,@object          # @bitoff
	.section	.rodata,"a",@progbits
	.p2align	4
bitoff:
	.asciz	"\b\007\006\006\005\005\005\005\004\004\004\004\004\004\004\004\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.size	bitoff, 256


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
