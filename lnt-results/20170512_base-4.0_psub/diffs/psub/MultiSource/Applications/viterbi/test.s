	.text
	.file	"test.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$19688, %rsp            # imm = 0x4CE8
.Lcfi6:
	.cfi_def_cfa_offset 19744
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	xorl	%r13d, %r13d
	xorps	%xmm0, %xmm0
	movq	%rsp, %rbx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
	movaps	%xmm0, (%rsp)
	movaps	%xmm0, 32(%rsp)
	movl	$1, %esi
	leaq	56(%rsp), %rbp
	movq	%rbp, %rdi
	callq	init_viterbi
	movl	$18304, %edi            # imm = 0x4780
	callq	malloc
	movq	%rax, %r14
	movl	$18304, %edx            # imm = 0x4780
	movq	%r14, %rdi
	leaq	1384(%rsp), %rsi
	callq	memcpy
	movq	$128, 16(%rsp)
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, 24(%rsp)
	movl	$1024, %edx             # imm = 0x400
	movq	%rax, %rdi
	leaq	360(%rsp), %rsi
	callq	memcpy
	movl	$.L.str, %esi
	movq	%rbx, %rdi
	callq	read_dmatrix
	movl	$.Lstr, %edi
	callq	puts
	xorl	%r9d, %r9d
	leaq	16(%rsp), %rdi
	movq	%r14, %rsi
	leaq	32(%rsp), %rdx
	movq	%rbx, %rcx
	movq	%rbp, %r8
	callq	dec_viterbi_F
	movl	$.Lstr.1, %edi
	callq	puts
	movq	24(%rsp), %rdi
	callq	free
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	cmpq	$0, (%rsp)
	movq	8(%rsp), %r15
	je	.LBB0_2
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph.i
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r15,%rbp), %rdi
	callq	free
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15,%rbp)
	incq	%r12
	movq	8(%rsp), %r15
	addq	$16, %rbp
	cmpq	(%rsp), %r12
	jb	.LBB0_4
	jmp	.LBB0_5
	.p2align	4, 0x90
.LBB0_2:                                #   in Loop: Header=BB0_1 Depth=1
	xorps	%xmm0, %xmm0
.LBB0_5:                                # %dvarray_clear.exit
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	%r15, %rdi
	callq	free
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movq	40(%rsp), %rdi
	callq	free
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movq	%r14, %rdi
	callq	free
	incl	%r13d
	cmpl	$10, %r13d
	xorps	%xmm0, %xmm0
	jne	.LBB0_1
# BB#6:
	xorl	%eax, %eax
	addq	$19688, %rsp            # imm = 0x4CE8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/viterbi/Dist_demux"
	.size	.L.str, 82

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Starting Viterbi"
	.size	.Lstr, 17

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"Viterbi finished"
	.size	.Lstr.1, 17


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
