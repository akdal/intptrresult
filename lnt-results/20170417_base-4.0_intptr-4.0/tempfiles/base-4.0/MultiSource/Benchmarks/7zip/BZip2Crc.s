	.text
	.file	"BZip2Crc.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI0_1:
	.long	79764919                # 0x4c11db7
	.long	79764919                # 0x4c11db7
	.long	79764919                # 0x4c11db7
	.long	79764919                # 0x4c11db7
.LCPI0_2:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.text
	.globl	_ZN9CBZip2Crc9InitTableEv
	.p2align	4, 0x90
	.type	_ZN9CBZip2Crc9InitTableEv,@function
_ZN9CBZip2Crc9InitTableEv:              # @_ZN9CBZip2Crc9InitTableEv
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movq	$-1024, %rax            # imm = 0xFC00
	movdqa	.LCPI0_1(%rip), %xmm1   # xmm1 = [79764919,79764919,79764919,79764919]
	movdqa	.LCPI0_2(%rip), %xmm2   # xmm2 = [4,4,4,4]
	movdqa	%xmm0, %xmm3
	.p2align	4, 0x90
.LBB0_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm3, %xmm5
	pslld	$24, %xmm5
	psrad	$24, %xmm5
	pxor	%xmm4, %xmm4
	pcmpgtd	%xmm5, %xmm4
	movdqa	%xmm0, %xmm5
	pslld	$25, %xmm5
	movdqa	%xmm5, %xmm6
	pxor	%xmm1, %xmm6
	pand	%xmm4, %xmm6
	pandn	%xmm5, %xmm4
	por	%xmm6, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm4, %xmm5
	paddd	%xmm4, %xmm4
	movdqa	%xmm5, %xmm6
	pandn	%xmm4, %xmm6
	pxor	%xmm1, %xmm4
	pand	%xmm5, %xmm4
	por	%xmm6, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm4, %xmm5
	paddd	%xmm4, %xmm4
	movdqa	%xmm5, %xmm6
	pandn	%xmm4, %xmm6
	pxor	%xmm1, %xmm4
	pand	%xmm5, %xmm4
	por	%xmm6, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm4, %xmm5
	paddd	%xmm4, %xmm4
	movdqa	%xmm5, %xmm6
	pandn	%xmm4, %xmm6
	pxor	%xmm1, %xmm4
	pand	%xmm5, %xmm4
	por	%xmm6, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm4, %xmm5
	paddd	%xmm4, %xmm4
	movdqa	%xmm5, %xmm6
	pandn	%xmm4, %xmm6
	pxor	%xmm1, %xmm4
	pand	%xmm5, %xmm4
	por	%xmm6, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm4, %xmm5
	paddd	%xmm4, %xmm4
	movdqa	%xmm5, %xmm6
	pandn	%xmm4, %xmm6
	pxor	%xmm1, %xmm4
	pand	%xmm5, %xmm4
	por	%xmm6, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm4, %xmm5
	paddd	%xmm4, %xmm4
	movdqa	%xmm5, %xmm6
	pandn	%xmm4, %xmm6
	pxor	%xmm1, %xmm4
	pand	%xmm5, %xmm4
	por	%xmm6, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm4, %xmm5
	paddd	%xmm4, %xmm4
	movdqa	%xmm5, %xmm6
	pandn	%xmm4, %xmm6
	pxor	%xmm1, %xmm4
	pand	%xmm5, %xmm4
	por	%xmm6, %xmm4
	movdqa	%xmm4, _ZN9CBZip2Crc5TableE+1024(%rax)
	paddd	%xmm2, %xmm0
	paddd	%xmm2, %xmm3
	addq	$16, %rax
	jne	.LBB0_1
# BB#2:                                 # %middle.block
	retq
.Lfunc_end0:
	.size	_ZN9CBZip2Crc9InitTableEv, .Lfunc_end0-_ZN9CBZip2Crc9InitTableEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI1_1:
	.long	79764919                # 0x4c11db7
	.long	79764919                # 0x4c11db7
	.long	79764919                # 0x4c11db7
	.long	79764919                # 0x4c11db7
.LCPI1_2:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_BZip2Crc.ii,@function
_GLOBAL__sub_I_BZip2Crc.ii:             # @_GLOBAL__sub_I_BZip2Crc.ii
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movdqa	.LCPI1_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movq	$-1024, %rax            # imm = 0xFC00
	movdqa	.LCPI1_1(%rip), %xmm1   # xmm1 = [79764919,79764919,79764919,79764919]
	movdqa	.LCPI1_2(%rip), %xmm2   # xmm2 = [4,4,4,4]
	movdqa	%xmm0, %xmm3
	.p2align	4, 0x90
.LBB1_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm3, %xmm5
	pslld	$24, %xmm5
	psrad	$24, %xmm5
	pxor	%xmm4, %xmm4
	pcmpgtd	%xmm5, %xmm4
	movdqa	%xmm0, %xmm5
	pslld	$25, %xmm5
	movdqa	%xmm5, %xmm6
	pxor	%xmm1, %xmm6
	pand	%xmm4, %xmm6
	pandn	%xmm5, %xmm4
	por	%xmm6, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm4, %xmm5
	paddd	%xmm4, %xmm4
	movdqa	%xmm5, %xmm6
	pandn	%xmm4, %xmm6
	pxor	%xmm1, %xmm4
	pand	%xmm5, %xmm4
	por	%xmm6, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm4, %xmm5
	paddd	%xmm4, %xmm4
	movdqa	%xmm5, %xmm6
	pandn	%xmm4, %xmm6
	pxor	%xmm1, %xmm4
	pand	%xmm5, %xmm4
	por	%xmm6, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm4, %xmm5
	paddd	%xmm4, %xmm4
	movdqa	%xmm5, %xmm6
	pandn	%xmm4, %xmm6
	pxor	%xmm1, %xmm4
	pand	%xmm5, %xmm4
	por	%xmm6, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm4, %xmm5
	paddd	%xmm4, %xmm4
	movdqa	%xmm5, %xmm6
	pandn	%xmm4, %xmm6
	pxor	%xmm1, %xmm4
	pand	%xmm5, %xmm4
	por	%xmm6, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm4, %xmm5
	paddd	%xmm4, %xmm4
	movdqa	%xmm5, %xmm6
	pandn	%xmm4, %xmm6
	pxor	%xmm1, %xmm4
	pand	%xmm5, %xmm4
	por	%xmm6, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm4, %xmm5
	paddd	%xmm4, %xmm4
	movdqa	%xmm5, %xmm6
	pandn	%xmm4, %xmm6
	pxor	%xmm1, %xmm4
	pand	%xmm5, %xmm4
	por	%xmm6, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtd	%xmm4, %xmm5
	paddd	%xmm4, %xmm4
	movdqa	%xmm5, %xmm6
	pandn	%xmm4, %xmm6
	pxor	%xmm1, %xmm4
	pand	%xmm5, %xmm4
	por	%xmm6, %xmm4
	movdqa	%xmm4, _ZN9CBZip2Crc5TableE+1024(%rax)
	paddd	%xmm2, %xmm0
	paddd	%xmm2, %xmm3
	addq	$16, %rax
	jne	.LBB1_1
# BB#2:                                 # %__cxx_global_var_init.exit
	retq
.Lfunc_end1:
	.size	_GLOBAL__sub_I_BZip2Crc.ii, .Lfunc_end1-_GLOBAL__sub_I_BZip2Crc.ii
	.cfi_endproc

	.type	_ZN9CBZip2Crc5TableE,@object # @_ZN9CBZip2Crc5TableE
	.bss
	.globl	_ZN9CBZip2Crc5TableE
	.p2align	4
_ZN9CBZip2Crc5TableE:
	.zero	1024
	.size	_ZN9CBZip2Crc5TableE, 1024

	.type	g_BZip2CrcTableInit,@object # @g_BZip2CrcTableInit
	.globl	g_BZip2CrcTableInit
g_BZip2CrcTableInit:
	.zero	1
	.size	g_BZip2CrcTableInit, 1

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_BZip2Crc.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
