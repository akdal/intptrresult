	.text
	.file	"z11.bc"
	.globl	SpaceChange
	.p2align	4, 0x90
	.type	SpaceChange,@function
SpaceChange:                            # @SpaceChange
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	movb	32(%r12), %al
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jb	.LBB0_8
# BB#1:
	cmpb	$5, %al
	je	.LBB0_12
# BB#2:
	leaq	32(%r12), %r15
	cmpb	$17, %al
	jne	.LBB0_15
# BB#3:                                 # %.preheader81
	movq	8(%r12), %rbx
	cmpq	%r12, %rbx
	jne	.LBB0_5
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_5 Depth=1
	cmpb	$0, 64(%rsi)
	je	.LBB0_11
# BB#13:                                #   in Loop: Header=BB0_5 Depth=1
	movq	%r14, %rdi
	callq	changespace
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.LBB0_5
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_5 Depth=1
	movl	$11, %edi
	movl	$3, %esi
	movl	$.L.str.1, %edx
	movl	$2, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.LBB0_5
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_11:                               # %.backedge
                                        #   in Loop: Header=BB0_5 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	je	.LBB0_12
.LBB0_5:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB0_6:                                #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rsi), %rsi
	movzbl	32(%rsi), %eax
	cmpq	$12, %rax
	ja	.LBB0_14
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=2
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_8:
	cmpb	$0, 64(%r12)
	je	.LBB0_12
# BB#9:
	movq	%r14, %rdi
	movq	%r12, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	changespace             # TAILCALL
.LBB0_12:                               # %.loopexit82
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB0_15:
	movl	$11, %edi
	movl	$4, %esi
	movl	$.L.str.1, %edx
	movl	$2, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	movq	%r15, %r8
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	Error                   # TAILCALL
.Lfunc_end0:
	.size	SpaceChange, .Lfunc_end0-SpaceChange
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_6
	.quad	.LBB0_11
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_11
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_14
	.quad	.LBB0_10
	.quad	.LBB0_10

	.text
	.p2align	4, 0x90
	.type	changespace,@function
changespace:                            # @changespace
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 64
.Lcfi15:
	.cfi_offset %rbx, -48
.Lcfi16:
	.cfi_offset %r12, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movb	32(%rbx), %al
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB1_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_2:
	leaq	32(%rbx), %r14
	leaq	64(%rbx), %r15
	movb	64(%rbx), %bpl
	movl	%ebp, %eax
	addb	$-97, %al
	cmpb	$25, %al
	ja	.LBB1_12
# BB#3:
	movl	$.L.str.9, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_17
# BB#4:
	movl	$.L.str.10, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_18
# BB#5:
	movl	$.L.str.11, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_21
# BB#6:
	movl	$.L.str.12, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_23
# BB#7:
	cmpb	$116, %bpl
	jne	.LBB1_11
# BB#8:
	cmpb	$101, 1(%r15)
	jne	.LBB1_11
# BB#9:
	cmpb	$120, 66(%rbx)
	jne	.LBB1_11
# BB#10:
	cmpb	$0, 3(%r15)
	je	.LBB1_26
.LBB1_11:                               # %.thread
	movq	%r15, (%rsp)
	movl	$11, %edi
	movl	$1, %esi
	movl	$.L.str.14, %edx
	movl	$2, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
	jmp	.LBB1_25
.LBB1_12:
	leaq	8(%rsp), %rdx
	leaq	12(%rsp), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	GetGap
	movl	12(%rsp), %edx
	cmpl	$158, %edx
	movw	8(%rsp), %ax
	movw	4(%r12), %cx
	jne	.LBB1_15
# BB#13:
	movl	%eax, %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$1023, %ecx             # imm = 0x3FF
	orl	%edx, %ecx
	andl	$-8192, %eax            # imm = 0xE000
	orl	%ecx, %eax
	movw	%ax, 4(%r12)
	movswl	10(%rsp), %eax
	addq	$6, %r12
.LBB1_14:
	movw	%ax, (%r12)
	jmp	.LBB1_25
.LBB1_15:
	movl	%ecx, %ebx
	xorl	%eax, %ebx
	testb	$28, %bh
	je	.LBB1_19
# BB#16:
	movl	$11, %edi
	movl	$2, %esi
	movl	$.L.str.15, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%r15, %r9
	callq	Error
	jmp	.LBB1_25
.LBB1_17:
	andb	$-113, (%r12)
	jmp	.LBB1_25
.LBB1_18:
	movb	(%r12), %al
	andb	$-113, %al
	orb	$16, %al
	jmp	.LBB1_24
.LBB1_19:
	movl	%eax, %esi
	andl	$7168, %esi             # imm = 0x1C00
	andl	$1023, %ecx             # imm = 0x3FF
	orl	%esi, %ecx
	andl	$-8192, %eax            # imm = 0xE000
	orl	%ecx, %eax
	movw	%ax, 4(%r12)
	movswl	6(%r12), %ecx
	addq	$6, %r12
	movswl	10(%rsp), %eax
	cmpl	$159, %edx
	jne	.LBB1_22
# BB#20:
	addl	%ecx, %eax
	jmp	.LBB1_14
.LBB1_21:
	movb	(%r12), %al
	andb	$-113, %al
	orb	$32, %al
	jmp	.LBB1_24
.LBB1_22:
	xorl	%edx, %edx
	subl	%eax, %ecx
	cmovsl	%edx, %ecx
	movl	%ecx, %eax
	jmp	.LBB1_14
.LBB1_23:
	movb	(%r12), %al
	andb	$-113, %al
	orb	$48, %al
.LBB1_24:
	movb	%al, (%r12)
.LBB1_25:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_26:
	movb	(%r12), %al
	andb	$-113, %al
	orb	$64, %al
	jmp	.LBB1_24
.Lfunc_end1:
	.size	changespace, .Lfunc_end1-changespace
	.cfi_endproc

	.globl	BreakChange
	.p2align	4, 0x90
	.type	BreakChange,@function
BreakChange:                            # @BreakChange
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 48
.Lcfi25:
	.cfi_offset %rbx, -40
.Lcfi26:
	.cfi_offset %r12, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	movb	32(%r12), %al
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jb	.LBB2_8
# BB#1:
	cmpb	$5, %al
	je	.LBB2_12
# BB#2:
	leaq	32(%r12), %r15
	cmpb	$17, %al
	jne	.LBB2_15
# BB#3:                                 # %.preheader81
	movq	8(%r12), %rbx
	cmpq	%r12, %rbx
	jne	.LBB2_5
	jmp	.LBB2_12
	.p2align	4, 0x90
.LBB2_10:                               #   in Loop: Header=BB2_5 Depth=1
	cmpb	$0, 64(%rsi)
	je	.LBB2_11
# BB#13:                                #   in Loop: Header=BB2_5 Depth=1
	movq	%r14, %rdi
	callq	changebreak
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.LBB2_5
	jmp	.LBB2_12
	.p2align	4, 0x90
.LBB2_14:                               #   in Loop: Header=BB2_5 Depth=1
	movl	$11, %edi
	movl	$7, %esi
	movl	$.L.str.1, %edx
	movl	$2, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.LBB2_5
	jmp	.LBB2_12
	.p2align	4, 0x90
.LBB2_11:                               # %.backedge
                                        #   in Loop: Header=BB2_5 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	je	.LBB2_12
.LBB2_5:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_6 Depth 2
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB2_6:                                #   Parent Loop BB2_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rsi), %rsi
	movzbl	32(%rsi), %eax
	cmpq	$12, %rax
	ja	.LBB2_14
# BB#7:                                 #   in Loop: Header=BB2_6 Depth=2
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_8:
	cmpb	$0, 64(%r12)
	je	.LBB2_12
# BB#9:
	movq	%r14, %rdi
	movq	%r12, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	changebreak             # TAILCALL
.LBB2_12:                               # %.loopexit82
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB2_15:
	movl	$11, %edi
	movl	$8, %esi
	movl	$.L.str.1, %edx
	movl	$2, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	movq	%r15, %r8
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	Error                   # TAILCALL
.Lfunc_end2:
	.size	BreakChange, .Lfunc_end2-BreakChange
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_6
	.quad	.LBB2_11
	.quad	.LBB2_14
	.quad	.LBB2_14
	.quad	.LBB2_14
	.quad	.LBB2_11
	.quad	.LBB2_14
	.quad	.LBB2_14
	.quad	.LBB2_14
	.quad	.LBB2_14
	.quad	.LBB2_14
	.quad	.LBB2_10
	.quad	.LBB2_10

	.text
	.p2align	4, 0x90
	.type	changebreak,@function
changebreak:                            # @changebreak
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 48
.Lcfi33:
	.cfi_offset %rbx, -32
.Lcfi34:
	.cfi_offset %r14, -24
.Lcfi35:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	leaq	64(%rbx), %r15
	movb	64(%rbx), %al
	addb	$-97, %al
	cmpb	$25, %al
	ja	.LBB3_32
# BB#1:
	movl	$.L.str.16, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_2
# BB#3:
	movl	$.L.str.17, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_4
# BB#5:
	movl	$.L.str.18, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_6
# BB#7:
	movl	$.L.str.19, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_8
# BB#9:
	movl	$.L.str.20, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_10
# BB#11:
	movl	$.L.str.21, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_12
# BB#13:
	movl	$.L.str.22, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_14
# BB#15:
	movl	$.L.str.23, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_16
# BB#17:
	movl	$.L.str.24, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_18
# BB#19:
	movl	$.L.str.25, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_20
# BB#21:
	movl	$.L.str.26, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_22
# BB#23:
	movl	$.L.str.27, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_24
# BB#25:
	movl	$.L.str.28, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_26
# BB#27:
	movl	$.L.str.29, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_28
# BB#29:
	movl	$.L.str.30, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_30
# BB#31:
	addq	$32, %rbx
	movq	%r15, (%rsp)
	movl	$11, %edi
	movl	$5, %esi
	movl	$.L.str.14, %edx
	movl	$2, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	jmp	.LBB3_40
.LBB3_32:
	leaq	8(%rsp), %rdx
	leaq	12(%rsp), %rcx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	GetGap
	movl	12(%rsp), %edx
	cmpl	$158, %edx
	movw	8(%rsp), %ax
	movw	(%r14), %si
	jne	.LBB3_33
# BB#35:
	movl	%eax, %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$1023, %esi             # imm = 0x3FF
	orl	%ecx, %esi
	andl	$-8192, %eax            # imm = 0xE000
	orl	%esi, %eax
	movw	%ax, (%r14)
	movswl	10(%rsp), %eax
	addq	$2, %r14
.LBB3_39:
	movw	%ax, (%r14)
	jmp	.LBB3_40
.LBB3_33:
	movl	%esi, %ecx
	xorl	%eax, %ecx
	testb	$28, %ch
	je	.LBB3_36
# BB#34:
	addq	$32, %rbx
	movl	$11, %edi
	movl	$6, %esi
	movl	$.L.str.31, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%r15, %r9
	callq	Error
	jmp	.LBB3_40
.LBB3_2:
	movb	4(%r14), %al
	andb	$-4, %al
	orb	$2, %al
	movb	%al, 4(%r14)
	jmp	.LBB3_40
.LBB3_4:
	movb	4(%r14), %al
	andb	$-4, %al
	orb	$1, %al
	movb	%al, 4(%r14)
	jmp	.LBB3_40
.LBB3_36:
	movl	%eax, %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$1023, %esi             # imm = 0x3FF
	orl	%ecx, %esi
	andl	$-8192, %eax            # imm = 0xE000
	orl	%esi, %eax
	movw	%ax, (%r14)
	movswl	2(%r14), %ecx
	addq	$2, %r14
	movswl	10(%rsp), %eax
	cmpl	$159, %edx
	jne	.LBB3_38
# BB#37:
	addl	%ecx, %eax
	jmp	.LBB3_39
.LBB3_6:
	movb	4(%r14), %al
	andb	$-125, %al
	orb	$24, %al
	movb	%al, 4(%r14)
	jmp	.LBB3_40
.LBB3_38:
	xorl	%edx, %edx
	subl	%eax, %ecx
	cmovsl	%edx, %ecx
	movl	%ecx, %eax
	jmp	.LBB3_39
.LBB3_8:
	movb	4(%r14), %al
	andb	$-125, %al
	orb	$40, %al
	movb	%al, 4(%r14)
	jmp	.LBB3_40
.LBB3_10:
	movb	4(%r14), %al
	andb	$-125, %al
	orb	$72, %al
	movb	%al, 4(%r14)
	jmp	.LBB3_40
.LBB3_12:
	movb	4(%r14), %al
	andb	$-125, %al
	orb	$88, %al
	movb	%al, 4(%r14)
	jmp	.LBB3_40
.LBB3_14:
	movb	4(%r14), %al
	andb	$-125, %al
	orb	$104, %al
	movb	%al, 4(%r14)
	jmp	.LBB3_40
.LBB3_16:
	movb	4(%r14), %al
	andb	$-125, %al
	orb	$56, %al
	movb	%al, 4(%r14)
	jmp	.LBB3_40
.LBB3_18:
	movb	4(%r14), %al
	andb	$-125, %al
	orb	$68, %al
	movb	%al, 4(%r14)
	jmp	.LBB3_40
.LBB3_20:
	movb	4(%r14), %al
	andb	$-125, %al
	orb	$84, %al
	movb	%al, 4(%r14)
	jmp	.LBB3_40
.LBB3_22:
	movb	4(%r14), %al
	andb	$-125, %al
	orb	$100, %al
	movb	%al, 4(%r14)
	jmp	.LBB3_40
.LBB3_24:
	orb	$-128, 15(%r14)
	jmp	.LBB3_40
.LBB3_26:
	andb	$127, 15(%r14)
	jmp	.LBB3_40
.LBB3_28:
	orb	$64, 15(%r14)
	jmp	.LBB3_40
.LBB3_30:
	andb	$-65, 15(%r14)
.LBB3_40:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	changebreak, .Lfunc_end3-changebreak
	.cfi_endproc

	.globl	YUnitChange
	.p2align	4, 0x90
	.type	YUnitChange,@function
YUnitChange:                            # @YUnitChange
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 32
.Lcfi39:
	.cfi_offset %rbx, -24
.Lcfi40:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rsp, %rdx
	leaq	4(%rsp), %rcx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	GetGap
	movzwl	(%rsp), %eax
	andl	$7168, %eax             # imm = 0x1C00
	cmpl	$1024, %eax             # imm = 0x400
	jne	.LBB4_1
# BB#2:
	movl	4(%rsp), %eax
	cmpl	$159, %eax
	je	.LBB4_5
# BB#3:
	cmpl	$158, %eax
	jne	.LBB4_6
# BB#4:
	movzwl	2(%rsp), %eax
	movw	%ax, 8(%r14)
	jmp	.LBB4_7
.LBB4_1:
	addq	$32, %rbx
	movl	$11, %edi
	movl	$9, %esi
	movl	$.L.str.4, %edx
	movl	$2, %ecx
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	jmp	.LBB4_7
.LBB4_5:
	movzwl	2(%rsp), %eax
	addw	%ax, 8(%r14)
	jmp	.LBB4_7
.LBB4_6:
	movswl	8(%r14), %eax
	movswl	2(%rsp), %ecx
	xorl	%edx, %edx
	subl	%ecx, %eax
	cmovgw	%ax, %dx
	movw	%dx, 8(%r14)
.LBB4_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	YUnitChange, .Lfunc_end4-YUnitChange
	.cfi_endproc

	.globl	ZUnitChange
	.p2align	4, 0x90
	.type	ZUnitChange,@function
ZUnitChange:                            # @ZUnitChange
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 32
.Lcfi44:
	.cfi_offset %rbx, -24
.Lcfi45:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rsp, %rdx
	leaq	4(%rsp), %rcx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	GetGap
	movzwl	(%rsp), %eax
	andl	$7168, %eax             # imm = 0x1C00
	cmpl	$1024, %eax             # imm = 0x400
	jne	.LBB5_1
# BB#2:
	movl	4(%rsp), %eax
	cmpl	$159, %eax
	je	.LBB5_5
# BB#3:
	cmpl	$158, %eax
	jne	.LBB5_6
# BB#4:
	movzwl	2(%rsp), %eax
	movw	%ax, 10(%r14)
	jmp	.LBB5_7
.LBB5_1:
	addq	$32, %rbx
	movl	$11, %edi
	movl	$10, %esi
	movl	$.L.str.4, %edx
	movl	$2, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	jmp	.LBB5_7
.LBB5_5:
	movzwl	2(%rsp), %eax
	addw	%ax, 10(%r14)
	jmp	.LBB5_7
.LBB5_6:
	movswl	10(%r14), %eax
	movswl	2(%rsp), %ecx
	xorl	%edx, %edx
	subl	%ecx, %eax
	cmovgw	%ax, %dx
	movw	%dx, 10(%r14)
.LBB5_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	ZUnitChange, .Lfunc_end5-ZUnitChange
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.zero	1
	.size	.L.str, 1

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"invalid left parameter of %s"
	.size	.L.str.1, 29

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"@Space"
	.size	.L.str.2, 7

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"@Break"
	.size	.L.str.3, 7

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"this unit not allowed with %s symbol"
	.size	.L.str.4, 37

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"@YUnit"
	.size	.L.str.5, 7

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"@ZUnit"
	.size	.L.str.6, 7

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"assert failed in %s"
	.size	.L.str.7, 20

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"changespace: type(x)!"
	.size	.L.str.8, 22

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"lout"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"compress"
	.size	.L.str.10, 9

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"separate"
	.size	.L.str.11, 9

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"troff"
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"tex"
	.size	.L.str.13, 4

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"unknown option to %s symbol (%s)"
	.size	.L.str.14, 33

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"spacing %s is not compatible with current spacing"
	.size	.L.str.15, 50

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"hyphen"
	.size	.L.str.16, 7

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"nohyphen"
	.size	.L.str.17, 9

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"adjust"
	.size	.L.str.18, 7

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"outdent"
	.size	.L.str.19, 8

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"ragged"
	.size	.L.str.20, 7

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"cragged"
	.size	.L.str.21, 8

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"rragged"
	.size	.L.str.22, 8

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"oragged"
	.size	.L.str.23, 8

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"lines"
	.size	.L.str.24, 6

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"clines"
	.size	.L.str.25, 7

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"rlines"
	.size	.L.str.26, 7

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"unbreakablefirst"
	.size	.L.str.27, 17

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"breakablefirst"
	.size	.L.str.28, 15

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"unbreakablelast"
	.size	.L.str.29, 16

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"breakablelast"
	.size	.L.str.30, 14

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"line spacing %s is not compatible with current spacing"
	.size	.L.str.31, 55


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
