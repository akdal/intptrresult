for i in 1 5 ;  do

echo "Making cc1.sorted.${i}per.csv : "
python2 ../../lnt/sort.py cc1.csv cc1.sorted.${i}per.csv ${i}
echo "Making cc2.sorted.${i}per.csv : "
python2 ../../lnt/sort.py cc2.csv cc2.sorted.${i}per.csv ${i}
echo "Making rt1.sorted.${i}per.csv : "
python2 ../../lnt/sort.py rt1.csv rt1.sorted.${i}per.csv ${i}
echo "Making rt2.sorted.${i}per.csv : "
python2 ../../lnt/sort.py rt2.csv rt2.sorted.${i}per.csv ${i}

done
