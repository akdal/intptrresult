	.text
	.file	"delete.bc"
	.globl	delete
	.p2align	4, 0x90
	.type	delete,@function
delete:                                 # @delete
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	%rbx, %rdi
	callq	validIndexKey
	testb	%al, %al
	je	.LBB0_5
# BB#1:
	movq	%r14, %rdi
	callq	validAttributes
	testb	%al, %al
	je	.LBB0_6
# BB#2:
	movq	(%r15), %rdi
	leaq	15(%rsp), %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	deleteEntry
	movq	(%r15), %rbx
	movq	(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB0_7
# BB#3:
	cmpq	$0, 8(%rbx)
	jne	.LBB0_8
# BB#4:                                 # %.preheader.thread
	movq	$0, (%rbx)
	jmp	.LBB0_12
.LBB0_5:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$delete.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$1, %r14d
	jmp	.LBB0_13
.LBB0_6:
	movl	$.L.str.1, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$delete.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$2, %r14d
	jmp	.LBB0_13
.LBB0_7:                                # %.preheader
	testq	%rax, %rax
	je	.LBB0_12
.LBB0_8:                                # %.lr.ph
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_13
# BB#10:                                #   in Loop: Header=BB0_9 Depth=1
	cmpq	$0, 40(%rax)
	jne	.LBB0_13
# BB#11:                                #   in Loop: Header=BB0_9 Depth=1
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	8(%rbx), %rdi
	callq	free
	movq	$0, 8(%rbx)
	movq	%rbx, %rdi
	callq	deleteIndexNode
	movq	(%r15), %rbx
	cmpq	$0, (%rbx)
	jne	.LBB0_9
	jmp	.LBB0_13
.LBB0_12:
	xorl	%r14d, %r14d
.LBB0_13:                               # %.critedge
	movq	%r14, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	delete, .Lfunc_end0-delete
	.cfi_endproc

	.type	delete.name,@object     # @delete.name
	.data
delete.name:
	.asciz	"delete"
	.size	delete.name, 7

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"invalid index key search values"
	.size	.L.str, 32

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"invalid non-key search values"
	.size	.L.str.1, 30


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
