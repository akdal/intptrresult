	.text
	.file	"compat.bc"
	.globl	compat
	.p2align	4, 0x90
	.type	compat,@function
compat:                                 # @compat
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	cmpl	$0, BESTMATCH(%rip)
	je	.LBB0_3
# BB#1:
	movl	FILENAMEONLY(%rip), %ecx
	orl	COUNT(%rip), %ecx
	orl	APPROX(%rip), %ecx
	movl	PAT_FILE(%rip), %eax
	orl	%eax, %ecx
	je	.LBB0_4
# BB#2:
	movl	$0, BESTMATCH(%rip)
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
.LBB0_3:                                # %thread-pre-split
	movl	PAT_FILE(%rip), %eax
.LBB0_4:
	testl	%eax, %eax
	je	.LBB0_12
# BB#5:
	cmpl	$0, APPROX(%rip)
	jne	.LBB0_6
# BB#7:
	cmpl	$0, LINENUM(%rip)
	jne	.LBB0_8
.LBB0_10:
	cmpl	$0, DELIMITER(%rip)
	jne	.LBB0_11
.LBB0_12:
	cmpl	$0, JUMP(%rip)
	je	.LBB0_19
# BB#13:
	cmpl	$0, REGEX(%rip)
	jne	.LBB0_14
# BB#15:
	cmpl	$0, I(%rip)
	je	.LBB0_18
.LBB0_16:
	movl	S(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_18
# BB#17:
	movl	DD(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_18
.LBB0_19:
	cmpl	$0, DELIMITER(%rip)
	je	.LBB0_22
# BB#20:
	movl	WHOLELINE(%rip), %eax
	testl	%eax, %eax
	jne	.LBB0_21
.LBB0_22:
	popq	%rax
	retq
.LBB0_6:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$65, %esi
	movl	$1, %edx
	callq	fwrite
	cmpl	$0, LINENUM(%rip)
	je	.LBB0_10
.LBB0_8:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	jmp	.LBB0_9
.LBB0_14:
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$77, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$0, JUMP(%rip)
	cmpl	$0, I(%rip)
	jne	.LBB0_16
.LBB0_18:
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	jmp	.LBB0_9
.LBB0_21:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	jmp	.LBB0_9
.LBB0_11:
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
.LBB0_9:
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$2, %edi
	callq	exit
.Lfunc_end0:
	.size	compat, .Lfunc_end0-compat
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s: WARNING!!! -B option ignored when -c, -l, -f, or -# is on\n"
	.size	.L.str, 63

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"WARNING!!!  approximate matching is not supported with -f option\n"
	.size	.L.str.1, 66

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s: -f and -n are not compatible\n"
	.size	.L.str.2, 34

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s: -f and -d are not compatible\n"
	.size	.L.str.3, 34

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"WARNING!!! -D#, -I#, or -S# option is ignored for regular expression pattern\n"
	.size	.L.str.4, 78

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%s: the error cost cannot be 0\n"
	.size	.L.str.5, 32

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%s: -d and -x is not compatible\n"
	.size	.L.str.6, 33


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
