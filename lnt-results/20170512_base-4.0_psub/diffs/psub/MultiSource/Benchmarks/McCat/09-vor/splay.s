	.text
	.file	"splay.bc"
	.globl	find
	.p2align	4, 0x90
	.type	find,@function
find:                                   # @find
	.cfi_startproc
# BB#0:
	.p2align	4, 0x90
.LBB0_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rax
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	jge	.LBB0_3
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	32(%rax), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_1
.LBB0_3:                                #   in Loop: Header=BB0_1 Depth=1
	cmpq	%rsi, %rcx
	jle	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_1
.LBB0_5:
	retq
.Lfunc_end0:
	.size	find, .Lfunc_end0-find
	.cfi_endproc

	.globl	rotate
	.p2align	4, 0x90
	.type	rotate,@function
rotate:                                 # @rotate
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rcx
	cmpq	%rdi, 24(%rcx)
	je	.LBB1_1
# BB#2:
	leaq	24(%rdi), %rax
	movq	24(%rdi), %rdx
	movq	%rdx, 32(%rcx)
	testq	%rdx, %rdx
	jne	.LBB1_4
	jmp	.LBB1_5
.LBB1_1:
	leaq	32(%rdi), %rax
	movq	32(%rdi), %rdx
	movq	%rdx, 24(%rcx)
	testq	%rdx, %rdx
	je	.LBB1_5
.LBB1_4:
	movq	%rcx, 16(%rdx)
.LBB1_5:
	movq	16(%rdi), %rcx
	movq	%rcx, (%rax)
	movq	16(%rcx), %rax
	movq	%rdi, 16(%rcx)
	testq	%rax, %rax
	je	.LBB1_7
# BB#6:
	leaq	24(%rax), %rcx
	movq	24(%rax), %rdx
	leaq	32(%rax), %rsi
	cmpq	16(%rdi), %rdx
	cmoveq	%rcx, %rsi
	movq	%rdi, (%rsi)
.LBB1_7:
	movq	%rax, 16(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	rotate, .Lfunc_end1-rotate
	.cfi_endproc

	.globl	splay
	.p2align	4, 0x90
	.type	splay,@function
splay:                                  # @splay
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	(%rdi), %rcx
	.p2align	4, 0x90
.LBB2_1:                                # %tailrecurse.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rdx
	cmpq	%rsi, %rdx
	jge	.LBB2_3
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movq	32(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB2_1
.LBB2_3:                                #   in Loop: Header=BB2_1 Depth=1
	cmpq	%rsi, %rdx
	jle	.LBB2_5
# BB#4:                                 #   in Loop: Header=BB2_1 Depth=1
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB2_1
.LBB2_5:                                # %find.exit.preheader
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB2_51
# BB#6:                                 # %.lr.ph
	leaq	32(%rax), %r8
	leaq	24(%rax), %r9
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_29:                               # %find.exit.backedge
                                        #   in Loop: Header=BB2_7 Depth=1
	leaq	24(%rsi), %rcx
	movq	24(%rsi), %rdx
	leaq	32(%rsi), %rbx
	cmpq	16(%rax), %rdx
	cmoveq	%rcx, %rbx
	movq	%rax, (%rbx)
	movq	%rsi, 16(%rax)
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rsi), %rcx
	movq	24(%rsi), %rdx
	leaq	24(%rsi), %r11
	testq	%rcx, %rcx
	je	.LBB2_8
# BB#14:                                #   in Loop: Header=BB2_7 Depth=1
	cmpq	%rax, %rdx
	jne	.LBB2_30
# BB#15:                                #   in Loop: Header=BB2_7 Depth=1
	cmpq	%rsi, 24(%rcx)
	je	.LBB2_16
.LBB2_30:                               #   in Loop: Header=BB2_7 Depth=1
	leaq	32(%rsi), %r10
	movq	32(%rsi), %rbx
	cmpq	%rax, %rbx
	jne	.LBB2_41
# BB#31:                                #   in Loop: Header=BB2_7 Depth=1
	cmpq	%rsi, 32(%rcx)
	je	.LBB2_32
.LBB2_41:                               #   in Loop: Header=BB2_7 Depth=1
	cmpq	%rax, %rdx
	je	.LBB2_42
# BB#44:                                #   in Loop: Header=BB2_7 Depth=1
	movq	(%r9), %rdx
	movq	%rdx, (%r10)
	testq	%rdx, %rdx
	movq	%r9, %rcx
	je	.LBB2_46
# BB#45:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rsi, 16(%rdx)
	movq	%r9, %rcx
	jmp	.LBB2_46
	.p2align	4, 0x90
.LBB2_8:                                #   in Loop: Header=BB2_7 Depth=1
	cmpq	%rax, %rdx
	je	.LBB2_9
# BB#11:                                #   in Loop: Header=BB2_7 Depth=1
	movq	(%r9), %rdx
	movq	%rdx, 32(%rsi)
	testq	%rdx, %rdx
	movq	%r9, %rcx
	je	.LBB2_13
# BB#12:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rsi, 16(%rdx)
	movq	%r9, %rcx
	jmp	.LBB2_13
.LBB2_42:                               #   in Loop: Header=BB2_7 Depth=1
	movq	(%r8), %rdx
	movq	%rdx, (%r11)
	testq	%rdx, %rdx
	movq	%r8, %rcx
	je	.LBB2_46
# BB#43:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rsi, 16(%rdx)
	movq	%r8, %rcx
.LBB2_46:                               #   in Loop: Header=BB2_7 Depth=1
	movq	16(%rax), %rdx
	movq	%rdx, (%rcx)
	movq	16(%rdx), %rcx
	movq	%rax, 16(%rdx)
	leaq	24(%rcx), %rdx
	testq	%rcx, %rcx
	je	.LBB2_48
# BB#47:                                #   in Loop: Header=BB2_7 Depth=1
	movq	24(%rcx), %rsi
	leaq	32(%rcx), %rbx
	cmpq	16(%rax), %rsi
	cmoveq	%rdx, %rbx
	movq	%rax, (%rbx)
.LBB2_48:                               # %rotate.exit23
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	%rcx, 16(%rax)
	cmpq	%rax, 24(%rcx)
	jne	.LBB2_25
# BB#49:                                #   in Loop: Header=BB2_7 Depth=1
	movq	(%r8), %rsi
	movq	%rsi, (%rdx)
	jmp	.LBB2_23
.LBB2_9:                                #   in Loop: Header=BB2_7 Depth=1
	movq	(%r8), %rdx
	movq	%rdx, (%r11)
	testq	%rdx, %rdx
	movq	%r8, %rcx
	je	.LBB2_13
# BB#10:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rsi, 16(%rdx)
	movq	%r8, %rcx
.LBB2_13:                               #   in Loop: Header=BB2_7 Depth=1
	movq	16(%rax), %rdx
	movq	%rdx, (%rcx)
	movq	16(%rdx), %rsi
	movq	%rax, 16(%rdx)
	testq	%rsi, %rsi
	jne	.LBB2_29
	jmp	.LBB2_50
.LBB2_16:                               #   in Loop: Header=BB2_7 Depth=1
	movq	32(%rsi), %rdx
	movq	%rdx, 24(%rcx)
	testq	%rdx, %rdx
	je	.LBB2_18
# BB#17:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rcx, 16(%rdx)
	movq	16(%rsi), %rcx
.LBB2_18:                               # %._crit_edge
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	%rcx, 32(%rsi)
	movq	16(%rcx), %rdx
	movq	%rsi, 16(%rcx)
	testq	%rdx, %rdx
	je	.LBB2_20
# BB#19:                                #   in Loop: Header=BB2_7 Depth=1
	leaq	24(%rdx), %r10
	movq	24(%rdx), %rbx
	leaq	32(%rdx), %rcx
	cmpq	16(%rsi), %rbx
	cmoveq	%r10, %rcx
	movq	%rsi, (%rcx)
.LBB2_20:                               # %rotate.exit39
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	%rdx, 16(%rsi)
	jmp	.LBB2_21
.LBB2_32:                               #   in Loop: Header=BB2_7 Depth=1
	cmpq	%rsi, 24(%rcx)
	je	.LBB2_33
# BB#35:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rdx, 32(%rcx)
	testq	%rdx, %rdx
	je	.LBB2_37
# BB#36:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rcx, 16(%rdx)
.LBB2_37:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%r11, %r10
	jmp	.LBB2_38
.LBB2_33:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, 24(%rcx)
	testq	%rbx, %rbx
	je	.LBB2_38
# BB#34:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rcx, 16(%rax)
.LBB2_38:                               #   in Loop: Header=BB2_7 Depth=1
	movq	16(%rsi), %rdx
	movq	%rdx, (%r10)
	movq	16(%rdx), %rcx
	movq	%rsi, 16(%rdx)
	testq	%rcx, %rcx
	je	.LBB2_40
# BB#39:                                #   in Loop: Header=BB2_7 Depth=1
	leaq	24(%rcx), %r10
	movq	24(%rcx), %rbx
	leaq	32(%rcx), %rdx
	cmpq	16(%rsi), %rbx
	cmoveq	%r10, %rdx
	movq	%rsi, (%rdx)
.LBB2_40:                               # %rotate.exit31
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	%rcx, 16(%rsi)
.LBB2_21:                               # %rotate.exit39
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	16(%rax), %rcx
	cmpq	%rax, 24(%rcx)
	je	.LBB2_22
.LBB2_25:                               #   in Loop: Header=BB2_7 Depth=1
	movq	(%r9), %rsi
	movq	%rsi, 32(%rcx)
	testq	%rsi, %rsi
	movq	%r9, %rdx
	je	.LBB2_27
# BB#26:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rcx, 16(%rsi)
	movq	%r9, %rdx
	jmp	.LBB2_27
.LBB2_22:                               #   in Loop: Header=BB2_7 Depth=1
	movq	(%r8), %rsi
	movq	%rsi, 24(%rcx)
.LBB2_23:                               #   in Loop: Header=BB2_7 Depth=1
	testq	%rsi, %rsi
	movq	%r8, %rdx
	je	.LBB2_27
# BB#24:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rcx, 16(%rsi)
	movq	%r8, %rdx
.LBB2_27:                               #   in Loop: Header=BB2_7 Depth=1
	movq	16(%rax), %rcx
	movq	%rcx, (%rdx)
	movq	16(%rcx), %rsi
	movq	%rax, 16(%rcx)
	testq	%rsi, %rsi
	jne	.LBB2_29
.LBB2_50:                               # %find.exit._crit_edge.loopexit
	movq	$0, 16(%rax)
.LBB2_51:                               # %find.exit._crit_edge
	movq	%rax, (%rdi)
	popq	%rbx
	retq
.Lfunc_end2:
	.size	splay, .Lfunc_end2-splay
	.cfi_endproc

	.globl	free_tree
	.p2align	4, 0x90
	.type	free_tree,@function
free_tree:                              # @free_tree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB3_1
# BB#2:
	movq	24(%rbx), %rdi
	callq	free_tree
	movq	32(%rbx), %rdi
	callq	free_tree
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB3_1:
	popq	%rbx
	retq
.Lfunc_end3:
	.size	free_tree, .Lfunc_end3-free_tree
	.cfi_endproc

	.globl	create_node
	.p2align	4, 0x90
	.type	create_node,@function
create_node:                            # @create_node
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$40, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB4_2
# BB#1:
	movq	%rbx, %rcx
	shrq	$32, %rcx
	imull	$10000, %ebx, %edx      # imm = 0x2710
	addl	%ecx, %edx
	movslq	%edx, %rdx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movq	$0, 32(%rax)
	movq	%rdx, (%rax)
	movl	%ebx, 8(%rax)
	movl	%ecx, 12(%rax)
	popq	%rbx
	retq
.LBB4_2:
	movl	$.Lstr, %edi
	callq	puts
	xorl	%edi, %edi
	callq	exit
.Lfunc_end4:
	.size	create_node, .Lfunc_end4-create_node
	.cfi_endproc

	.globl	init
	.p2align	4, 0x90
	.type	init,@function
init:                                   # @init
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	init, .Lfunc_end5-init
	.cfi_endproc

	.globl	insert
	.p2align	4, 0x90
	.type	insert,@function
insert:                                 # @insert
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$40, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB6_13
# BB#1:                                 # %create_node.exit
	movq	%r15, %rax
	shrq	$32, %rax
	imull	$10000, %r15d, %ecx     # imm = 0x2710
	addl	%eax, %ecx
	movslq	%ecx, %rsi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	$0, 32(%rbx)
	movq	%rsi, (%rbx)
	movl	%r15d, 8(%rbx)
	movl	%eax, 12(%rbx)
	cmpq	$0, (%r14)
	je	.LBB6_11
# BB#2:
	movq	%r14, %rdi
	callq	splay
	movq	(%rbx), %rcx
	movq	(%r14), %rax
	movq	(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.LBB6_12
# BB#3:
	cmpq	%rcx, %rdx
	jle	.LBB6_7
# BB#4:
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rbx)
	testq	%rcx, %rcx
	je	.LBB6_6
# BB#5:
	movq	%rbx, 16(%rcx)
	movq	(%r14), %rax
.LBB6_6:                                # %._crit_edge31
	movq	%rax, 32(%rbx)
	addq	$24, %rax
	jmp	.LBB6_10
.LBB6_7:
	movq	32(%rax), %rcx
	movq	%rcx, 32(%rbx)
	testq	%rcx, %rcx
	je	.LBB6_9
# BB#8:
	movq	%rbx, 16(%rcx)
	movq	(%r14), %rax
.LBB6_9:                                # %._crit_edge
	movq	%rax, 24(%rbx)
	addq	$32, %rax
.LBB6_10:
	movq	$0, (%rax)
	movq	(%r14), %rax
	movq	%rbx, 16(%rax)
.LBB6_11:
	movq	%rbx, (%r14)
.LBB6_12:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB6_13:
	movl	$.Lstr, %edi
	callq	puts
	xorl	%edi, %edi
	callq	exit
.Lfunc_end6:
	.size	insert, .Lfunc_end6-insert
	.cfi_endproc

	.globl	delete_min
	.p2align	4, 0x90
	.type	delete_min,@function
delete_min:                             # @delete_min
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -24
.Lcfi16:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpq	$0, (%rbx)
	je	.LBB7_4
# BB#1:
	movq	$-1, %rsi
	movq	%rbx, %rdi
	callq	splay
	movq	(%rbx), %rdi
	movq	8(%rdi), %r14
	movq	32(%rdi), %rax
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB7_3
# BB#2:
	movq	$0, 16(%rax)
.LBB7_3:
	callq	free
	jmp	.LBB7_5
.LBB7_4:
	movl	$.Lstr.1, %edi
	callq	puts
                                        # implicit-def: %R14
.LBB7_5:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	delete_min, .Lfunc_end7-delete_min
	.cfi_endproc

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Can't create node"
	.size	.Lstr, 18

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"No elements in tree! [delete_min]"
	.size	.Lstr.1, 34


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
