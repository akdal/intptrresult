	.text
	.file	"leaky_bucket.bc"
	.globl	get_LeakyBucketRate
	.p2align	4, 0x90
	.type	get_LeakyBucketRate,@function
get_LeakyBucketRate:                    # @get_LeakyBucketRate
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 64
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r13, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	input(%rip), %rdi
	addq	$4192, %rdi             # imm = 0x1060
	movl	$.L.str, %esi
	callq	fopen64
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_6
# BB#1:                                 # %.preheader
	testq	%r15, %r15
	je	.LBB0_5
# BB#2:                                 # %.lr.ph.preheader
	leaq	8(%rsp), %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	fscanf
	cmpl	$1, %eax
	jne	.LBB0_7
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	8(%rsp), %rax
	movq	%rax, (%r14,%rbx,8)
	incq	%rbx
	cmpq	%r15, %rbx
	jb	.LBB0_3
.LBB0_5:                                # %._crit_edge
	movq	%r13, %rdi
	callq	fclose
	movl	$1, %eax
	jmp	.LBB0_9
.LBB0_6:
	movl	$.Lstr.1, %edi
	callq	puts
	jmp	.LBB0_8
.LBB0_7:
	movl	$.Lstr, %edi
	callq	puts
	movq	%r13, %rdi
	callq	fclose
.LBB0_8:
	xorl	%eax, %eax
.LBB0_9:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	get_LeakyBucketRate, .Lfunc_end0-get_LeakyBucketRate
	.cfi_endproc

	.globl	PutBigDoubleWord
	.p2align	4, 0x90
	.type	PutBigDoubleWord,@function
PutBigDoubleWord:                       # @PutBigDoubleWord
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	shrl	$24, %edi
	callq	fputc
	movl	%ebx, %eax
	shrl	$16, %eax
	movzbl	%al, %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	%bh, %edi  # NOREX
	movq	%r14, %rsi
	callq	fputc
	movzbl	%bl, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	fputc                   # TAILCALL
.Lfunc_end1:
	.size	PutBigDoubleWord, .Lfunc_end1-PutBigDoubleWord
	.cfi_endproc

	.globl	write_buffer
	.p2align	4, 0x90
	.type	write_buffer,@function
write_buffer:                           # @write_buffer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 64
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	input(%rip), %rdi
	addq	$4448, %rdi             # imm = 0x1160
	movl	$.L.str.4, %esi
	callq	fopen64
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB2_2
# BB#1:
	movq	input(%rip), %rcx
	addq	$4448, %rcx             # imm = 0x1160
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.5, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$1, %esi
	callq	error
.LBB2_2:
	movl	%ebx, %edi
	shrl	$24, %edi
	movq	%r13, %rsi
	callq	fputc
	movl	%ebx, %eax
	shrl	$16, %eax
	movzbl	%al, %edi
	movq	%r13, %rsi
	callq	fputc
	movzbl	%bh, %edi  # NOREX
	movq	%r13, %rsi
	callq	fputc
	movzbl	%bl, %edi
	movq	%r13, %rsi
	callq	fputc
	testq	%rbx, %rbx
	je	.LBB2_4
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rbp
	movl	(%r12), %ebx
	movl	%ebx, %edi
	shrl	$24, %edi
	movq	%r13, %rsi
	callq	fputc
	movl	%ebx, %eax
	shrl	$16, %eax
	movzbl	%al, %edi
	movq	%r13, %rsi
	callq	fputc
	movzbl	%bh, %edi  # NOREX
	movq	%r13, %rsi
	callq	fputc
	movzbl	%bl, %edi
	movq	%r13, %rsi
	callq	fputc
	movl	(%r15), %ebx
	movl	%ebx, %edi
	shrl	$24, %edi
	movq	%r13, %rsi
	callq	fputc
	movl	%ebx, %eax
	shrl	$16, %eax
	movzbl	%al, %edi
	movq	%r13, %rsi
	callq	fputc
	movzbl	%bh, %edi  # NOREX
	movq	%r13, %rsi
	callq	fputc
	movzbl	%bl, %edi
	movq	%r13, %rsi
	callq	fputc
	movl	(%r14), %ebx
	movl	%ebx, %edi
	shrl	$24, %edi
	movq	%r13, %rsi
	callq	fputc
	movl	%ebx, %eax
	shrl	$16, %eax
	movzbl	%al, %edi
	movq	%r13, %rsi
	callq	fputc
	movzbl	%bh, %edi  # NOREX
	movq	%r13, %rsi
	callq	fputc
	movzbl	%bl, %edi
	movq	%rbp, %rbx
	movq	%r13, %rsi
	callq	fputc
	addq	$8, %r12
	addq	$8, %r15
	addq	$8, %r14
	decq	%rbx
	jne	.LBB2_3
.LBB2_4:                                # %._crit_edge
	movq	%r13, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fclose                  # TAILCALL
.Lfunc_end2:
	.size	write_buffer, .Lfunc_end2-write_buffer
	.cfi_endproc

	.globl	Sort
	.p2align	4, 0x90
	.type	Sort,@function
Sort:                                   # @Sort
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 24
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rdi, %r10
	decq	%r10
	je	.LBB3_15
# BB#1:                                 # %.lr.ph27.preheader
	leaq	-2(%rdi), %r9
	leaq	8(%rsi), %r8
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph27
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_10 Depth 2
	movq	%r11, %r14
	leaq	1(%r14), %r11
	cmpq	%rdi, %r11
	jae	.LBB3_2
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	%r10d, %eax
	subl	%r14d, %eax
	testb	$1, %al
	movq	%r11, %rcx
	je	.LBB3_8
# BB#5:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	(%rsi,%r14,8), %rax
	movq	8(%rsi,%r14,8), %rcx
	cmpq	%rcx, %rax
	jbe	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	%rcx, (%rsi,%r14,8)
	movq	%rax, 8(%rsi,%r14,8)
.LBB3_7:                                #   in Loop: Header=BB3_3 Depth=1
	leaq	2(%r14), %rcx
.LBB3_8:                                # %.prol.loopexit
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	%r14, %r9
	je	.LBB3_2
# BB#9:                                 # %.lr.ph.new
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	%rdi, %rax
	subq	%rcx, %rax
	leaq	(%r8,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB3_10:                               #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi,%r14,8), %rbx
	movq	-8(%rcx), %rdx
	cmpq	%rdx, %rbx
	jbe	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_10 Depth=2
	movq	%rdx, (%rsi,%r14,8)
	movq	%rbx, -8(%rcx)
.LBB3_12:                               #   in Loop: Header=BB3_10 Depth=2
	movq	(%rsi,%r14,8), %rbx
	movq	(%rcx), %rdx
	cmpq	%rdx, %rbx
	jbe	.LBB3_14
# BB#13:                                #   in Loop: Header=BB3_10 Depth=2
	movq	%rdx, (%rsi,%r14,8)
	movq	%rbx, (%rcx)
.LBB3_14:                               #   in Loop: Header=BB3_10 Depth=2
	addq	$16, %rcx
	addq	$-2, %rax
	jne	.LBB3_10
.LBB3_2:                                # %.loopexit
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	%r10, %r11
	jne	.LBB3_3
.LBB3_15:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	Sort, .Lfunc_end3-Sort
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1593835520              # float 9.22337203E+18
	.text
	.globl	calc_buffer
	.p2align	4, 0x90
	.type	calc_buffer,@function
calc_buffer:                            # @calc_buffer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 112
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	stdout(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$80, %esi
	movl	$1, %edx
	callq	fwrite
	movq	total_frame_buffer(%rip), %rsi
	movq	input(%rip), %rax
	movl	8(%rax), %edx
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	movq	input(%rip), %rax
	movslq	4188(%rax), %r14
	movl	$8, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB4_2
# BB#1:
	movl	$.L.str.9, %edi
	callq	no_mem_exit
.LBB4_2:
	movl	$8, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB4_4
# BB#3:
	movl	$.L.str.10, %edi
	callq	no_mem_exit
.LBB4_4:
	movl	$8, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB4_6
# BB#5:
	movl	$.L.str.11, %edi
	callq	no_mem_exit
.LBB4_6:                                # %.preheader144
	movq	total_frame_buffer(%rip), %rax
	testq	%rax, %rax
	je	.LBB4_7
# BB#8:                                 # %.lr.ph157
	movq	Bit_Buffer(%rip), %rcx
	cmpq	$4, %rax
	jb	.LBB4_9
# BB#10:                                # %min.iters.checked
	movq	%rax, %rdx
	andq	$-4, %rdx
	je	.LBB4_9
# BB#11:                                # %vector.body.preheader
	leaq	-4(%rdx), %rbp
	movl	%ebp, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB4_12
# BB#13:                                # %vector.body.prol.preheader
	negq	%rsi
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB4_14:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rcx,%rdi,8), %xmm2
	movdqu	16(%rcx,%rdi,8), %xmm3
	paddq	%xmm2, %xmm0
	paddq	%xmm3, %xmm1
	addq	$4, %rdi
	incq	%rsi
	jne	.LBB4_14
	jmp	.LBB4_15
.LBB4_9:
	xorl	%esi, %esi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_19:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	addq	(%rcx,%rdx,8), %rsi
	incq	%rdx
	cmpq	%rax, %rdx
	jb	.LBB4_19
.LBB4_20:                               # %._crit_edge158.loopexit
	testq	%rax, %rax
	js	.LBB4_21
# BB#22:                                # %._crit_edge158.loopexit
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	testq	%rsi, %rsi
	jns	.LBB4_25
	jmp	.LBB4_24
.LBB4_7:
	xorps	%xmm0, %xmm0
	xorl	%esi, %esi
	testq	%rsi, %rsi
	jns	.LBB4_25
	jmp	.LBB4_24
.LBB4_21:
	movq	%rax, %rcx
	shrq	%rcx
	andl	$1, %eax
	orq	%rcx, %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	addss	%xmm0, %xmm0
	testq	%rsi, %rsi
	js	.LBB4_24
.LBB4_25:                               # %._crit_edge158
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rsi, %xmm1
	jmp	.LBB4_26
.LBB4_24:
	movq	%rsi, %rax
	shrq	%rax
	andl	$1, %esi
	orq	%rax, %rsi
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rsi, %xmm1
	addss	%xmm1, %xmm1
.LBB4_26:                               # %._crit_edge158
	divss	%xmm0, %xmm1
	movss	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	subss	%xmm2, %xmm0
	cvttss2si	%xmm0, %rax
	movabsq	$-9223372036854775808, %rbp # imm = 0x8000000000000000
	xorq	%rbp, %rax
	cvttss2si	%xmm1, %r13
	ucomiss	%xmm2, %xmm1
	cmovaeq	%rax, %r13
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	get_LeakyBucketRate
	cmpl	$1, %eax
	je	.LBB4_39
# BB#27:                                # %._crit_edge158
	testl	%r14d, %r14d
	je	.LBB4_39
# BB#28:                                # %.lr.ph154
	testq	%r13, %r13
	js	.LBB4_29
# BB#30:                                # %.lr.ph154
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%r13, %xmm0
	jmp	.LBB4_31
.LBB4_29:
	movq	%r13, %rax
	shrq	%rax
	movl	%r13d, %ecx
	andl	$1, %ecx
	orq	%rax, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rcx, %xmm0
	addss	%xmm0, %xmm0
.LBB4_31:                               # %.lr.ph154
	movq	img(%rip), %rcx
	movq	input(%rip), %rsi
	movq	%r13, %rax
	shrq	$2, %rax
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rax, %xmm1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_32:                               # =>This Inner Loop Header: Depth=1
	testq	%rdi, %rdi
	je	.LBB4_33
# BB#34:                                #   in Loop: Header=BB4_32 Depth=1
	movq	-8(%r12,%rdi,8), %rax
	testq	%rax, %rax
	js	.LBB4_35
# BB#36:                                #   in Loop: Header=BB4_32 Depth=1
	xorps	%xmm2, %xmm2
	cvtsi2ssq	%rax, %xmm2
	jmp	.LBB4_37
	.p2align	4, 0x90
.LBB4_33:                               #   in Loop: Header=BB4_32 Depth=1
	movss	48(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movaps	%xmm2, %xmm3
	movss	.LCPI4_0(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm3
	cvttss2si	%xmm3, %rdx
	xorq	%rbp, %rdx
	cvttss2si	%xmm2, %rax
	ucomiss	%xmm4, %xmm2
	cmovaeq	%rdx, %rax
	movslq	20(%rsi), %rbx
	incq	%rbx
	xorl	%edx, %edx
	divq	%rbx
	jmp	.LBB4_38
	.p2align	4, 0x90
.LBB4_35:                               #   in Loop: Header=BB4_32 Depth=1
	movq	%rax, %rdx
	shrq	%rdx
	andl	$1, %eax
	orq	%rdx, %rax
	xorps	%xmm2, %xmm2
	cvtsi2ssq	%rax, %xmm2
	addss	%xmm2, %xmm2
.LBB4_37:                               #   in Loop: Header=BB4_32 Depth=1
	movss	48(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movl	20(%rsi), %eax
	incl	%eax
	cvtsi2ssl	%eax, %xmm4
	divss	%xmm4, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm3, %xmm2
	movss	.LCPI4_0(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	cvttss2si	%xmm2, %rdx
	xorq	%rbp, %rdx
	cvttss2si	%xmm3, %rax
	ucomiss	%xmm4, %xmm3
	cmovaeq	%rdx, %rax
.LBB4_38:                               #   in Loop: Header=BB4_32 Depth=1
	movq	%rax, (%r12,%rdi,8)
	incq	%rdi
	cmpq	%r14, %rdi
	jb	.LBB4_32
.LBB4_39:                               # %.loopexit
	movq	%r14, %rax
	decq	%rax
	je	.LBB4_54
# BB#40:                                # %.lr.ph27.i.preheader
	leaq	-2(%r14), %r9
	movq	%r12, %r8
	addq	$8, %r8
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_42:                               # %.lr.ph27.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_49 Depth 2
	movq	%rsi, %rdi
	leaq	1(%rdi), %rsi
	cmpq	%r14, %rsi
	jae	.LBB4_41
# BB#43:                                # %.lr.ph.i
                                        #   in Loop: Header=BB4_42 Depth=1
	movl	%eax, %ecx
	subl	%edi, %ecx
	testb	$1, %cl
	movq	%rsi, %rbx
	je	.LBB4_47
# BB#44:                                #   in Loop: Header=BB4_42 Depth=1
	movq	(%r12,%rdi,8), %rbp
	movq	8(%r12,%rdi,8), %rdx
	cmpq	%rdx, %rbp
	jbe	.LBB4_46
# BB#45:                                #   in Loop: Header=BB4_42 Depth=1
	movq	%rdx, (%r12,%rdi,8)
	movq	%rbp, 8(%r12,%rdi,8)
.LBB4_46:                               #   in Loop: Header=BB4_42 Depth=1
	leaq	2(%rdi), %rbx
.LBB4_47:                               # %.prol.loopexit176
                                        #   in Loop: Header=BB4_42 Depth=1
	cmpq	%rdi, %r9
	je	.LBB4_41
# BB#48:                                # %.lr.ph.i.new
                                        #   in Loop: Header=BB4_42 Depth=1
	movq	%r14, %rbp
	subq	%rbx, %rbp
	leaq	(%r8,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB4_49:                               #   Parent Loop BB4_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rdi,8), %rdx
	movq	-8(%rbx), %rcx
	cmpq	%rcx, %rdx
	jbe	.LBB4_51
# BB#50:                                #   in Loop: Header=BB4_49 Depth=2
	movq	%rcx, (%r12,%rdi,8)
	movq	%rdx, -8(%rbx)
.LBB4_51:                               #   in Loop: Header=BB4_49 Depth=2
	movq	(%r12,%rdi,8), %rdx
	movq	(%rbx), %rcx
	cmpq	%rcx, %rdx
	jbe	.LBB4_53
# BB#52:                                #   in Loop: Header=BB4_49 Depth=2
	movq	%rcx, (%r12,%rdi,8)
	movq	%rdx, (%rbx)
.LBB4_53:                               #   in Loop: Header=BB4_49 Depth=2
	addq	$16, %rbx
	addq	$-2, %rbp
	jne	.LBB4_49
.LBB4_41:                               # %.loopexit.i
                                        #   in Loop: Header=BB4_42 Depth=1
	cmpq	%rax, %rsi
	jne	.LBB4_42
.LBB4_54:                               # %Sort.exit
	testl	%r14d, %r14d
	je	.LBB4_71
# BB#55:                                # %.lr.ph151
	movq	input(%rip), %rax
	movslq	20(%rax), %rax
	incq	%rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	total_frame_buffer(%rip), %r11
	testq	%r11, %r11
	movq	Bit_Buffer(%rip), %r15
	movq	(%r15), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB4_69
# BB#56:                                # %.lr.ph151.split.us.preheader
	shlq	$2, %r13
	leaq	(%r13,%r13,4), %rbx
	movq	img(%rip), %rax
	movss	48(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movl	%r11d, %eax
	andl	$1, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r10d, %r10d
	xorl	%esi, %esi
	movq	%r14, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_57:                               # %.lr.ph151.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_64 Depth 2
                                        #     Child Loop BB4_66 Depth 2
	movq	(%r12,%r10,8), %rax
	imulq	32(%rsp), %rax          # 8-byte Folded Reload
	testq	%rax, %rax
	js	.LBB4_58
# BB#59:                                # %.lr.ph151.split.us
                                        #   in Loop: Header=BB4_57 Depth=1
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rax, %xmm1
	jmp	.LBB4_60
	.p2align	4, 0x90
.LBB4_58:                               #   in Loop: Header=BB4_57 Depth=1
	movq	%rax, %rdx
	shrq	%rdx
	andl	$1, %eax
	orq	%rdx, %rax
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rax, %xmm1
	addss	%xmm1, %xmm1
.LBB4_60:                               # %.lr.ph151.split.us
                                        #   in Loop: Header=BB4_57 Depth=1
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	divss	%xmm0, %xmm1
	cvttss2si	%xmm1, %rbp
	jne	.LBB4_62
# BB#61:                                #   in Loop: Header=BB4_57 Depth=1
	movq	%rbx, %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	cmpq	$1, %r11
	jne	.LBB4_64
	jmp	.LBB4_65
	.p2align	4, 0x90
.LBB4_62:                               #   in Loop: Header=BB4_57 Depth=1
	movq	(%r15), %rax
	movq	%rbx, %rdx
	subq	%rax, %rdx
	testq	%rax, %rax
	movl	$0, %eax
	cmovgq	%rax, %rsi
	movq	%rbx, %rdi
	cmovgq	%rdx, %rdi
	addq	%rbp, %rdx
	cmpq	%rbx, %rdx
	cmovgq	%rbx, %rdx
	movl	$1, %eax
	cmpq	$1, %r11
	je	.LBB4_65
	.p2align	4, 0x90
.LBB4_64:                               #   Parent Loop BB4_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subq	(%r15,%rax,8), %rdx
	cmpq	%rdi, %rdx
	cmovlq	%rax, %rsi
	cmovleq	%rdx, %rdi
	addq	%rbp, %rdx
	cmpq	%rbx, %rdx
	cmovgq	%rbx, %rdx
	subq	8(%r15,%rax,8), %rdx
	leaq	1(%rax), %rcx
	cmpq	%rdi, %rdx
	cmovlq	%rcx, %rsi
	cmovleq	%rdx, %rdi
	addq	%rbp, %rdx
	cmpq	%rbx, %rdx
	cmovgq	%rbx, %rdx
	addq	$2, %rax
	cmpq	%r11, %rax
	jne	.LBB4_64
.LBB4_65:                               # %._crit_edge.us
                                        #   in Loop: Header=BB4_57 Depth=1
	movq	%rbx, %rax
	subq	%rdi, %rax
	leaq	1(%rsi), %rdi
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %r13
	movq	%rcx, %rdx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB4_66:                               #   Parent Loop BB4_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdi, %r8
	jae	.LBB4_68
# BB#67:                                #   in Loop: Header=BB4_66 Depth=2
	subq	(%r15,%r8,8), %r13
	movl	$0, %r14d
	cmovnsq	%r13, %r14
	movq	%r13, %r9
	sarq	$63, %r9
	andq	%r13, %r9
	subq	%r9, %rdx
	addq	%rbp, %r14
	incq	%r8
	cmpq	%rax, %r14
	movq	%r14, %r13
	jle	.LBB4_66
.LBB4_68:                               #   in Loop: Header=BB4_57 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, (%rcx,%r10,8)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rdx, (%rax,%r10,8)
	incq	%r10
	movq	48(%rsp), %r14          # 8-byte Reload
	cmpq	%r14, %r10
	jb	.LBB4_57
	jmp	.LBB4_71
.LBB4_69:                               # %.lr.ph151.split.preheader
	cmpq	$1, %r14
	movl	$1, %edx
	cmovaq	%r14, %rdx
	shlq	$3, %rdx
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	memset
	.p2align	4, 0x90
.LBB4_70:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbx,8), %rax
	imulq	32(%rsp), %rax          # 8-byte Folded Reload
	testq	%rax, %rax
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax,%rbx,8)
	incq	%rbx
	cmpq	%r14, %rbx
	jb	.LBB4_70
.LBB4_71:                               # %._crit_edge152
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	callq	write_buffer
	movq	%r12, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	%rbp, %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB4_12:
	xorl	%edi, %edi
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
.LBB4_15:                               # %vector.body.prol.loopexit
	cmpq	$12, %rbp
	jb	.LBB4_18
# BB#16:                                # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	leaq	112(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB4_17:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-112(%rdi), %xmm2
	movdqu	-96(%rdi), %xmm3
	movdqu	-80(%rdi), %xmm4
	movdqu	-64(%rdi), %xmm5
	paddq	%xmm0, %xmm2
	paddq	%xmm1, %xmm3
	movdqu	-48(%rdi), %xmm6
	movdqu	-32(%rdi), %xmm7
	paddq	%xmm4, %xmm6
	paddq	%xmm2, %xmm6
	paddq	%xmm5, %xmm7
	paddq	%xmm3, %xmm7
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	paddq	%xmm6, %xmm0
	paddq	%xmm7, %xmm1
	subq	$-128, %rdi
	addq	$-16, %rsi
	jne	.LBB4_17
.LBB4_18:                               # %middle.block
	paddq	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm0, %xmm1
	movd	%xmm1, %rsi
	cmpq	%rdx, %rax
	jne	.LBB4_19
	jmp	.LBB4_20
.Lfunc_end4:
	.size	calc_buffer, .Lfunc_end4-calc_buffer
	.cfi_endproc

	.type	total_frame_buffer,@object # @total_frame_buffer
	.bss
	.globl	total_frame_buffer
	.p2align	3
total_frame_buffer:
	.quad	0                       # 0x0
	.size	total_frame_buffer, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%lu"
	.size	.L.str.2, 4

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"wb"
	.size	.L.str.4, 3

	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Error open file lk %s  \n"
	.size	.L.str.5, 25

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"-------------------------------------------------------------------------------\n"
	.size	.L.str.6, 81

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	" Total Frames:  %ld (%d) \n"
	.size	.L.str.7, 27

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"init_buffer: Rmin"
	.size	.L.str.9, 18

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"init_buffer: Bmin"
	.size	.L.str.10, 18

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"init_buffer: Fmin"
	.size	.L.str.11, 18

	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	" Leaky BucketRateFile does not have valid entries.\n Using rate calculated from avg. rate "
	.size	.Lstr, 90

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	" LeakyBucketRate File does not exist. Using rate calculated from avg. rate "
	.size	.Lstr.1, 76


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
