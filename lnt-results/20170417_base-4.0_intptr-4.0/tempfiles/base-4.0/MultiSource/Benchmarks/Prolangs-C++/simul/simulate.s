	.text
	.file	"simulate.bc"
	.globl	_Z6printfPcii
	.p2align	4, 0x90
	.type	_Z6printfPcii,@function
_Z6printfPcii:                          # @_Z6printfPcii
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	_Z6printfPcii, .Lfunc_end0-_Z6printfPcii
	.cfi_endproc

	.globl	_ZN17cursor_controller2upEi
	.p2align	4, 0x90
	.type	_ZN17cursor_controller2upEi,@function
_ZN17cursor_controller2upEi:            # @_ZN17cursor_controller2upEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN17cursor_controller2upEi, .Lfunc_end1-_ZN17cursor_controller2upEi
	.cfi_endproc

	.globl	_ZN17cursor_controller4downEi
	.p2align	4, 0x90
	.type	_ZN17cursor_controller4downEi,@function
_ZN17cursor_controller4downEi:          # @_ZN17cursor_controller4downEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end2:
	.size	_ZN17cursor_controller4downEi, .Lfunc_end2-_ZN17cursor_controller4downEi
	.cfi_endproc

	.globl	_ZN17cursor_controller5rightEi
	.p2align	4, 0x90
	.type	_ZN17cursor_controller5rightEi,@function
_ZN17cursor_controller5rightEi:         # @_ZN17cursor_controller5rightEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end3:
	.size	_ZN17cursor_controller5rightEi, .Lfunc_end3-_ZN17cursor_controller5rightEi
	.cfi_endproc

	.globl	_ZN17cursor_controller4leftEi
	.p2align	4, 0x90
	.type	_ZN17cursor_controller4leftEi,@function
_ZN17cursor_controller4leftEi:          # @_ZN17cursor_controller4leftEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end4:
	.size	_ZN17cursor_controller4leftEi, .Lfunc_end4-_ZN17cursor_controller4leftEi
	.cfi_endproc

	.globl	_ZN17cursor_controller4moveEii
	.p2align	4, 0x90
	.type	_ZN17cursor_controller4moveEii,@function
_ZN17cursor_controller4moveEii:         # @_ZN17cursor_controller4moveEii
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end5:
	.size	_ZN17cursor_controller4moveEii, .Lfunc_end5-_ZN17cursor_controller4moveEii
	.cfi_endproc

	.globl	_ZN17cursor_controller12clear_screenEv
	.p2align	4, 0x90
	.type	_ZN17cursor_controller12clear_screenEv,@function
_ZN17cursor_controller12clear_screenEv: # @_ZN17cursor_controller12clear_screenEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end6:
	.size	_ZN17cursor_controller12clear_screenEv, .Lfunc_end6-_ZN17cursor_controller12clear_screenEv
	.cfi_endproc

	.globl	_ZN17cursor_controller9clear_eolEv
	.p2align	4, 0x90
	.type	_ZN17cursor_controller9clear_eolEv,@function
_ZN17cursor_controller9clear_eolEv:     # @_ZN17cursor_controller9clear_eolEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end7:
	.size	_ZN17cursor_controller9clear_eolEv, .Lfunc_end7-_ZN17cursor_controller9clear_eolEv
	.cfi_endproc

	.globl	_ZN17cursor_controller4saveEv
	.p2align	4, 0x90
	.type	_ZN17cursor_controller4saveEv,@function
_ZN17cursor_controller4saveEv:          # @_ZN17cursor_controller4saveEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end8:
	.size	_ZN17cursor_controller4saveEv, .Lfunc_end8-_ZN17cursor_controller4saveEv
	.cfi_endproc

	.globl	_ZN17cursor_controller7restoreEv
	.p2align	4, 0x90
	.type	_ZN17cursor_controller7restoreEv,@function
_ZN17cursor_controller7restoreEv:       # @_ZN17cursor_controller7restoreEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end9:
	.size	_ZN17cursor_controller7restoreEv, .Lfunc_end9-_ZN17cursor_controller7restoreEv
	.cfi_endproc

	.globl	_ZN18cursor_controller26normalEv
	.p2align	4, 0x90
	.type	_ZN18cursor_controller26normalEv,@function
_ZN18cursor_controller26normalEv:       # @_ZN18cursor_controller26normalEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end10:
	.size	_ZN18cursor_controller26normalEv, .Lfunc_end10-_ZN18cursor_controller26normalEv
	.cfi_endproc

	.globl	_ZN18cursor_controller214high_intensityEv
	.p2align	4, 0x90
	.type	_ZN18cursor_controller214high_intensityEv,@function
_ZN18cursor_controller214high_intensityEv: # @_ZN18cursor_controller214high_intensityEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	_ZN18cursor_controller214high_intensityEv, .Lfunc_end11-_ZN18cursor_controller214high_intensityEv
	.cfi_endproc

	.globl	_ZN18cursor_controller25blinkEv
	.p2align	4, 0x90
	.type	_ZN18cursor_controller25blinkEv,@function
_ZN18cursor_controller25blinkEv:        # @_ZN18cursor_controller25blinkEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end12:
	.size	_ZN18cursor_controller25blinkEv, .Lfunc_end12-_ZN18cursor_controller25blinkEv
	.cfi_endproc

	.globl	_ZN18cursor_controller27reverseEv
	.p2align	4, 0x90
	.type	_ZN18cursor_controller27reverseEv,@function
_ZN18cursor_controller27reverseEv:      # @_ZN18cursor_controller27reverseEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end13:
	.size	_ZN18cursor_controller27reverseEv, .Lfunc_end13-_ZN18cursor_controller27reverseEv
	.cfi_endproc

	.globl	_ZN18cursor_controller29invisibleEv
	.p2align	4, 0x90
	.type	_ZN18cursor_controller29invisibleEv,@function
_ZN18cursor_controller29invisibleEv:    # @_ZN18cursor_controller29invisibleEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end14:
	.size	_ZN18cursor_controller29invisibleEv, .Lfunc_end14-_ZN18cursor_controller29invisibleEv
	.cfi_endproc

	.globl	_Z5errorPc
	.p2align	4, 0x90
	.type	_Z5errorPc,@function
_Z5errorPc:                             # @_Z5errorPc
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end15:
	.size	_Z5errorPc, .Lfunc_end15-_Z5errorPc
	.cfi_endproc

	.globl	_Z6strlenPc
	.p2align	4, 0x90
	.type	_Z6strlenPc,@function
_Z6strlenPc:                            # @_Z6strlenPc
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end16:
	.size	_Z6strlenPc, .Lfunc_end16-_Z6strlenPc
	.cfi_endproc

	.globl	_ZN17screen_controllerC2Eii
	.p2align	4, 0x90
	.type	_ZN17screen_controllerC2Eii,@function
_ZN17screen_controllerC2Eii:            # @_ZN17screen_controllerC2Eii
	.cfi_startproc
# BB#0:
	movl	%esi, (%rdi)
	movl	%edx, 4(%rdi)
	movb	_ZL12object_count(%rip), %al
	testb	%al, %al
	jne	.LBB17_2
# BB#1:
	movb	$1, _ZL12object_count(%rip)
.LBB17_2:
	retq
.Lfunc_end17:
	.size	_ZN17screen_controllerC2Eii, .Lfunc_end17-_ZN17screen_controllerC2Eii
	.cfi_endproc

	.globl	_ZN17screen_controller7setrowsEi
	.p2align	4, 0x90
	.type	_ZN17screen_controller7setrowsEi,@function
_ZN17screen_controller7setrowsEi:       # @_ZN17screen_controller7setrowsEi
	.cfi_startproc
# BB#0:
	movl	%esi, (%rdi)
	retq
.Lfunc_end18:
	.size	_ZN17screen_controller7setrowsEi, .Lfunc_end18-_ZN17screen_controller7setrowsEi
	.cfi_endproc

	.globl	_ZN17screen_controller7setcolsEi
	.p2align	4, 0x90
	.type	_ZN17screen_controller7setcolsEi,@function
_ZN17screen_controller7setcolsEi:       # @_ZN17screen_controller7setcolsEi
	.cfi_startproc
# BB#0:
	movl	%esi, 4(%rdi)
	retq
.Lfunc_end19:
	.size	_ZN17screen_controller7setcolsEi, .Lfunc_end19-_ZN17screen_controller7setcolsEi
	.cfi_endproc

	.globl	_ZN17screen_controllerD2Ev
	.p2align	4, 0x90
	.type	_ZN17screen_controllerD2Ev,@function
_ZN17screen_controllerD2Ev:             # @_ZN17screen_controllerD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end20:
	.size	_ZN17screen_controllerD2Ev, .Lfunc_end20-_ZN17screen_controllerD2Ev
	.cfi_endproc

	.globl	_ZN17screen_controller10upper_leftEv
	.p2align	4, 0x90
	.type	_ZN17screen_controller10upper_leftEv,@function
_ZN17screen_controller10upper_leftEv:   # @_ZN17screen_controller10upper_leftEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end21:
	.size	_ZN17screen_controller10upper_leftEv, .Lfunc_end21-_ZN17screen_controller10upper_leftEv
	.cfi_endproc

	.globl	_ZN17screen_controller4moveEii
	.p2align	4, 0x90
	.type	_ZN17screen_controller4moveEii,@function
_ZN17screen_controller4moveEii:         # @_ZN17screen_controller4moveEii
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end22:
	.size	_ZN17screen_controller4moveEii, .Lfunc_end22-_ZN17screen_controller4moveEii
	.cfi_endproc

	.globl	_ZN17screen_controller10lower_leftEv
	.p2align	4, 0x90
	.type	_ZN17screen_controller10lower_leftEv,@function
_ZN17screen_controller10lower_leftEv:   # @_ZN17screen_controller10lower_leftEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end23:
	.size	_ZN17screen_controller10lower_leftEv, .Lfunc_end23-_ZN17screen_controller10lower_leftEv
	.cfi_endproc

	.globl	_ZN17screen_controller11upper_rightEv
	.p2align	4, 0x90
	.type	_ZN17screen_controller11upper_rightEv,@function
_ZN17screen_controller11upper_rightEv:  # @_ZN17screen_controller11upper_rightEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end24:
	.size	_ZN17screen_controller11upper_rightEv, .Lfunc_end24-_ZN17screen_controller11upper_rightEv
	.cfi_endproc

	.globl	_ZN17screen_controller11lower_rightEv
	.p2align	4, 0x90
	.type	_ZN17screen_controller11lower_rightEv,@function
_ZN17screen_controller11lower_rightEv:  # @_ZN17screen_controller11lower_rightEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end25:
	.size	_ZN17screen_controller11lower_rightEv, .Lfunc_end25-_ZN17screen_controller11lower_rightEv
	.cfi_endproc

	.globl	_ZN17screen_controller13draw_verticalEiiic
	.p2align	4, 0x90
	.type	_ZN17screen_controller13draw_verticalEiiic,@function
_ZN17screen_controller13draw_verticalEiiic: # @_ZN17screen_controller13draw_verticalEiiic
	.cfi_startproc
# BB#0:
	addl	%esi, %ecx
	decl	%esi
	.p2align	4, 0x90
.LBB26_1:                               # =>This Inner Loop Header: Depth=1
	incl	%esi
	cmpl	%ecx, %esi
	jle	.LBB26_1
# BB#2:
	retq
.Lfunc_end26:
	.size	_ZN17screen_controller13draw_verticalEiiic, .Lfunc_end26-_ZN17screen_controller13draw_verticalEiiic
	.cfi_endproc

	.globl	_ZN17screen_controller15draw_horizontalEiiic
	.p2align	4, 0x90
	.type	_ZN17screen_controller15draw_horizontalEiiic,@function
_ZN17screen_controller15draw_horizontalEiiic: # @_ZN17screen_controller15draw_horizontalEiiic
	.cfi_startproc
# BB#0:
	addl	%edx, %ecx
	decl	%edx
	.p2align	4, 0x90
.LBB27_1:                               # =>This Inner Loop Header: Depth=1
	incl	%edx
	cmpl	%ecx, %edx
	jle	.LBB27_1
# BB#2:
	retq
.Lfunc_end27:
	.size	_ZN17screen_controller15draw_horizontalEiiic, .Lfunc_end27-_ZN17screen_controller15draw_horizontalEiiic
	.cfi_endproc

	.globl	_ZN17screen_controller6centerEiPc
	.p2align	4, 0x90
	.type	_ZN17screen_controller6centerEiPc,@function
_ZN17screen_controller6centerEiPc:      # @_ZN17screen_controller6centerEiPc
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end28:
	.size	_ZN17screen_controller6centerEiPc, .Lfunc_end28-_ZN17screen_controller6centerEiPc
	.cfi_endproc

	.globl	_ZN17screen_controller5pauseEi
	.p2align	4, 0x90
	.type	_ZN17screen_controller5pauseEi,@function
_ZN17screen_controller5pauseEi:         # @_ZN17screen_controller5pauseEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end29:
	.size	_ZN17screen_controller5pauseEi, .Lfunc_end29-_ZN17screen_controller5pauseEi
	.cfi_endproc

	.globl	_ZN17screen_controller7drawboxEiiiiii
	.p2align	4, 0x90
	.type	_ZN17screen_controller7drawboxEiiiiii,@function
_ZN17screen_controller7drawboxEiiiiii:  # @_ZN17screen_controller7drawboxEiiiiii
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end30:
	.size	_ZN17screen_controller7drawboxEiiiiii, .Lfunc_end30-_ZN17screen_controller7drawboxEiiiiii
	.cfi_endproc

	.globl	_ZN9init_gridC2Ev
	.p2align	4, 0x90
	.type	_ZN9init_gridC2Ev,@function
_ZN9init_gridC2Ev:                      # @_ZN9init_gridC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$s_grid, %edi
	xorl	%esi, %esi
	movl	$16000, %edx            # imm = 0x3E80
	callq	memset
	popq	%rax
	retq
.Lfunc_end31:
	.size	_ZN9init_gridC2Ev, .Lfunc_end31-_ZN9init_gridC2Ev
	.cfi_endproc

	.globl	_ZN15simulation_unit4moveEii
	.p2align	4, 0x90
	.type	_ZN15simulation_unit4moveEii,@function
_ZN15simulation_unit4moveEii:           # @_ZN15simulation_unit4moveEii
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 48
.Lcfi6:
	.cfi_offset %rbx, -40
.Lcfi7:
	.cfi_offset %r12, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movslq	8(%rbx), %rax
	movslq	%esi, %r14
	addq	%rax, %r14
	movslq	12(%rbx), %rcx
	movslq	%edx, %r15
	addq	%rcx, %r15
	leaq	(%r14,%r14,4), %rdx
	shlq	$7, %rdx
	cmpq	$0, s_grid(%rdx,%r15,8)
	je	.LBB32_2
# BB#1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB32_2:
	leaq	s_grid(%rdx,%r15,8), %r12
	leaq	(%rax,%rax,4), %rax
	shlq	$7, %rax
	movq	$0, s_grid(%rax,%rcx,8)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	movl	%r14d, 8(%rbx)
	movl	%r15d, 12(%rbx)
	movq	%rbx, (%r12)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end32:
	.size	_ZN15simulation_unit4moveEii, .Lfunc_end32-_ZN15simulation_unit4moveEii
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r12, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movl	$s_grid, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB33_1:                               # %.preheader53
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_2 Depth 2
	movq	%r14, %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB33_2:                               #   Parent Loop BB33_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$0, (%r12)
	jne	.LBB33_4
# BB#3:                                 #   in Loop: Header=BB33_2 Depth=2
	movl	$16, %edi
	callq	_Znwm
	movl	%r15d, 8(%rax)
	movl	%ebx, 12(%rax)
	movq	$_ZTV10pop_around+16, (%rax)
	movq	%rax, (%r12)
.LBB33_4:                               #   in Loop: Header=BB33_2 Depth=2
	incq	%rbx
	addq	$8, %r12
	cmpq	$80, %rbx
	jne	.LBB33_2
# BB#5:                                 #   in Loop: Header=BB33_1 Depth=1
	incq	%r15
	addq	$640, %r14              # imm = 0x280
	cmpq	$25, %r15
	jne	.LBB33_1
# BB#6:                                 # %.preheader.preheader
	movl	$1000000, %ebx          # imm = 0xF4240
	.p2align	4, 0x90
.LBB33_7:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	s_grid+3248(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB33_9
# BB#8:                                 #   in Loop: Header=BB33_7 Depth=1
	movq	(%rdi), %rax
	callq	*32(%rax)
.LBB33_9:                               #   in Loop: Header=BB33_7 Depth=1
	decl	%ebx
	jne	.LBB33_7
# BB#10:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end33:
	.size	main, .Lfunc_end33-main
	.cfi_endproc

	.section	.text._ZN10pop_around7displayEv,"axG",@progbits,_ZN10pop_around7displayEv,comdat
	.weak	_ZN10pop_around7displayEv
	.p2align	4, 0x90
	.type	_ZN10pop_around7displayEv,@function
_ZN10pop_around7displayEv:              # @_ZN10pop_around7displayEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end34:
	.size	_ZN10pop_around7displayEv, .Lfunc_end34-_ZN10pop_around7displayEv
	.cfi_endproc

	.section	.text._ZN10pop_around5eraseEv,"axG",@progbits,_ZN10pop_around5eraseEv,comdat
	.weak	_ZN10pop_around5eraseEv
	.p2align	4, 0x90
	.type	_ZN10pop_around5eraseEv,@function
_ZN10pop_around5eraseEv:                # @_ZN10pop_around5eraseEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end35:
	.size	_ZN10pop_around5eraseEv, .Lfunc_end35-_ZN10pop_around5eraseEv
	.cfi_endproc

	.section	.text._ZN15simulation_unitD2Ev,"axG",@progbits,_ZN15simulation_unitD2Ev,comdat
	.weak	_ZN15simulation_unitD2Ev
	.p2align	4, 0x90
	.type	_ZN15simulation_unitD2Ev,@function
_ZN15simulation_unitD2Ev:               # @_ZN15simulation_unitD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV15simulation_unit+16, (%rdi)
	retq
.Lfunc_end36:
	.size	_ZN15simulation_unitD2Ev, .Lfunc_end36-_ZN15simulation_unitD2Ev
	.cfi_endproc

	.section	.text._ZN10pop_aroundD0Ev,"axG",@progbits,_ZN10pop_aroundD0Ev,comdat
	.weak	_ZN10pop_aroundD0Ev
	.p2align	4, 0x90
	.type	_ZN10pop_aroundD0Ev,@function
_ZN10pop_aroundD0Ev:                    # @_ZN10pop_aroundD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end37:
	.size	_ZN10pop_aroundD0Ev, .Lfunc_end37-_ZN10pop_aroundD0Ev
	.cfi_endproc

	.section	.text._ZN10pop_around5cycleEv,"axG",@progbits,_ZN10pop_around5cycleEv,comdat
	.weak	_ZN10pop_around5cycleEv
	.p2align	4, 0x90
	.type	_ZN10pop_around5cycleEv,@function
_ZN10pop_around5cycleEv:                # @_ZN10pop_around5cycleEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movslq	8(%rbx), %rcx
	movslq	12(%rbx), %rax
	cmpq	$1, %rax
	sbbl	%edx, %edx
	notl	%edx
	orl	$1, %edx
	cmpq	$1, %rcx
	sbbl	%ebp, %ebp
	notl	%ebp
	orl	$1, %ebp
	addl	%ecx, %ebp
	addl	%eax, %edx
	movslq	%ebp, %rsi
	movslq	%edx, %r14
	leaq	(%rsi,%rsi,4), %rdx
	shlq	$7, %rdx
	cmpq	$0, s_grid(%rdx,%r14,8)
	je	.LBB38_2
# BB#1:                                 # %_ZN15simulation_unit4moveEii.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB38_2:
	leaq	s_grid(%rdx,%r14,8), %r15
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$7, %rcx
	movq	$0, s_grid(%rcx,%rax,8)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	movl	%ebp, 8(%rbx)
	movl	%r14d, 12(%rbx)
	movq	%rbx, (%r15)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end38:
	.size	_ZN10pop_around5cycleEv, .Lfunc_end38-_ZN10pop_around5cycleEv
	.cfi_endproc

	.section	.text._ZN15simulation_unit7displayEv,"axG",@progbits,_ZN15simulation_unit7displayEv,comdat
	.weak	_ZN15simulation_unit7displayEv
	.p2align	4, 0x90
	.type	_ZN15simulation_unit7displayEv,@function
_ZN15simulation_unit7displayEv:         # @_ZN15simulation_unit7displayEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end39:
	.size	_ZN15simulation_unit7displayEv, .Lfunc_end39-_ZN15simulation_unit7displayEv
	.cfi_endproc

	.section	.text._ZN15simulation_unit5eraseEv,"axG",@progbits,_ZN15simulation_unit5eraseEv,comdat
	.weak	_ZN15simulation_unit5eraseEv
	.p2align	4, 0x90
	.type	_ZN15simulation_unit5eraseEv,@function
_ZN15simulation_unit5eraseEv:           # @_ZN15simulation_unit5eraseEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end40:
	.size	_ZN15simulation_unit5eraseEv, .Lfunc_end40-_ZN15simulation_unit5eraseEv
	.cfi_endproc

	.section	.text._ZN15simulation_unitD0Ev,"axG",@progbits,_ZN15simulation_unitD0Ev,comdat
	.weak	_ZN15simulation_unitD0Ev
	.p2align	4, 0x90
	.type	_ZN15simulation_unitD0Ev,@function
_ZN15simulation_unitD0Ev:               # @_ZN15simulation_unitD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end41:
	.size	_ZN15simulation_unitD0Ev, .Lfunc_end41-_ZN15simulation_unitD0Ev
	.cfi_endproc

	.section	.text._ZN15simulation_unit5cycleEv,"axG",@progbits,_ZN15simulation_unit5cycleEv,comdat
	.weak	_ZN15simulation_unit5cycleEv
	.p2align	4, 0x90
	.type	_ZN15simulation_unit5cycleEv,@function
_ZN15simulation_unit5cycleEv:           # @_ZN15simulation_unit5cycleEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end42:
	.size	_ZN15simulation_unit5cycleEv, .Lfunc_end42-_ZN15simulation_unit5cycleEv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_simulate.ii,@function
_GLOBAL__sub_I_simulate.ii:             # @_GLOBAL__sub_I_simulate.ii
	.cfi_startproc
# BB#0:
	movabsq	$343597383705, %rax     # imm = 0x5000000019
	movq	%rax, screen(%rip)
	movb	_ZL12object_count(%rip), %al
	testb	%al, %al
	jne	.LBB43_2
# BB#1:
	movb	$1, _ZL12object_count(%rip)
.LBB43_2:                               # %__cxx_global_var_init.exit
	movl	$s_grid, %edi
	xorl	%esi, %esi
	movl	$16000, %edx            # imm = 0x3E80
	jmp	memset                  # TAILCALL
.Lfunc_end43:
	.size	_GLOBAL__sub_I_simulate.ii, .Lfunc_end43-_GLOBAL__sub_I_simulate.ii
	.cfi_endproc

	.type	screen,@object          # @screen
	.bss
	.globl	screen
	.p2align	2
screen:
	.zero	8
	.size	screen, 8

	.type	_ZL12object_count,@object # @_ZL12object_count
	.local	_ZL12object_count
	.comm	_ZL12object_count,1,4
	.type	s_grid,@object          # @s_grid
	.globl	s_grid
	.p2align	4
s_grid:
	.zero	16000
	.size	s_grid, 16000

	.type	grid_initializer,@object # @grid_initializer
	.globl	grid_initializer
grid_initializer:
	.zero	1
	.size	grid_initializer, 1

	.type	_ZTV10pop_around,@object # @_ZTV10pop_around
	.section	.rodata._ZTV10pop_around,"aG",@progbits,_ZTV10pop_around,comdat
	.weak	_ZTV10pop_around
	.p2align	3
_ZTV10pop_around:
	.quad	0
	.quad	_ZTI10pop_around
	.quad	_ZN10pop_around7displayEv
	.quad	_ZN10pop_around5eraseEv
	.quad	_ZN15simulation_unitD2Ev
	.quad	_ZN10pop_aroundD0Ev
	.quad	_ZN10pop_around5cycleEv
	.size	_ZTV10pop_around, 56

	.type	_ZTS10pop_around,@object # @_ZTS10pop_around
	.section	.rodata._ZTS10pop_around,"aG",@progbits,_ZTS10pop_around,comdat
	.weak	_ZTS10pop_around
_ZTS10pop_around:
	.asciz	"10pop_around"
	.size	_ZTS10pop_around, 13

	.type	_ZTS15simulation_unit,@object # @_ZTS15simulation_unit
	.section	.rodata._ZTS15simulation_unit,"aG",@progbits,_ZTS15simulation_unit,comdat
	.weak	_ZTS15simulation_unit
	.p2align	4
_ZTS15simulation_unit:
	.asciz	"15simulation_unit"
	.size	_ZTS15simulation_unit, 18

	.type	_ZTI15simulation_unit,@object # @_ZTI15simulation_unit
	.section	.rodata._ZTI15simulation_unit,"aG",@progbits,_ZTI15simulation_unit,comdat
	.weak	_ZTI15simulation_unit
	.p2align	3
_ZTI15simulation_unit:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS15simulation_unit
	.size	_ZTI15simulation_unit, 16

	.type	_ZTI10pop_around,@object # @_ZTI10pop_around
	.section	.rodata._ZTI10pop_around,"aG",@progbits,_ZTI10pop_around,comdat
	.weak	_ZTI10pop_around
	.p2align	4
_ZTI10pop_around:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10pop_around
	.quad	_ZTI15simulation_unit
	.size	_ZTI10pop_around, 24

	.type	_ZTV15simulation_unit,@object # @_ZTV15simulation_unit
	.section	.rodata._ZTV15simulation_unit,"aG",@progbits,_ZTV15simulation_unit,comdat
	.weak	_ZTV15simulation_unit
	.p2align	3
_ZTV15simulation_unit:
	.quad	0
	.quad	_ZTI15simulation_unit
	.quad	_ZN15simulation_unit7displayEv
	.quad	_ZN15simulation_unit5eraseEv
	.quad	_ZN15simulation_unitD2Ev
	.quad	_ZN15simulation_unitD0Ev
	.quad	_ZN15simulation_unit5cycleEv
	.size	_ZTV15simulation_unit, 56

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_simulate.ii

	.globl	_ZN17screen_controllerC1Eii
	.type	_ZN17screen_controllerC1Eii,@function
_ZN17screen_controllerC1Eii = _ZN17screen_controllerC2Eii
	.globl	_ZN17screen_controllerD1Ev
	.type	_ZN17screen_controllerD1Ev,@function
_ZN17screen_controllerD1Ev = _ZN17screen_controllerD2Ev
	.globl	_ZN9init_gridC1Ev
	.type	_ZN9init_gridC1Ev,@function
_ZN9init_gridC1Ev = _ZN9init_gridC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
