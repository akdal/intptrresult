	.text
	.file	"rdswitch.bc"
	.globl	read_quant_tables
	.p2align	4, 0x90
	.type	read_quant_tables,@function
read_quant_tables:                      # @read_quant_tables
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi6:
	.cfi_def_cfa_offset 368
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movl	$.L.str, %esi
	movq	%rbp, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_15
# BB#1:                                 # %.preheader
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	leaq	24(%rsp), %rsi
	leaq	12(%rsp), %rdx
	movq	%rbx, %rdi
	callq	read_text_integer
	testl	%eax, %eax
	je	.LBB0_11
# BB#2:                                 # %.lr.ph
	xorl	%ebp, %ebp
	leaq	24(%rsp), %r15
	leaq	12(%rsp), %r12
.LBB0_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
	cmpl	$4, %ebp
	jge	.LBB0_4
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	24(%rsp), %eax
	movl	%eax, 48(%rsp)
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB0_7:                                #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	read_text_integer
	testl	%eax, %eax
	je	.LBB0_8
# BB#9:                                 #   in Loop: Header=BB0_7 Depth=2
	movl	24(%rsp), %eax
	movl	%eax, 48(%rsp,%r13,4)
	incq	%r13
	cmpq	$64, %r13
	jl	.LBB0_7
# BB#10:                                #   in Loop: Header=BB0_3 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	leaq	48(%rsp), %rdx
	movl	36(%rsp), %ecx          # 4-byte Reload
	movl	%r14d, %r8d
	callq	jpeg_add_quant_table
	incl	%ebp
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	read_text_integer
	testl	%eax, %eax
	jne	.LBB0_3
.LBB0_11:                               # %._crit_edge
	cmpl	$-1, 12(%rsp)
	jne	.LBB0_12
# BB#13:
	movq	%rbx, %rdi
	callq	fclose
	movl	$1, %r14d
	jmp	.LBB0_14
.LBB0_8:
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$.L.str.3, %esi
.LBB0_5:
	xorl	%eax, %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	fprintf
	movq	%rbx, %rdi
	callq	fclose
.LBB0_14:
	movl	%r14d, %eax
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_15:
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	fprintf
	jmp	.LBB0_14
.LBB0_4:
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$.L.str.2, %esi
	jmp	.LBB0_5
.LBB0_12:
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$.L.str.4, %esi
	jmp	.LBB0_5
.Lfunc_end0:
	.size	read_quant_tables, .Lfunc_end0-read_quant_tables
	.cfi_endproc

	.p2align	4, 0x90
	.type	read_text_integer,@function
read_text_integer:                      # @read_text_integer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_2 Depth 2
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$35, %ebp
	jne	.LBB1_4
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader.i
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$10, %ebp
	je	.LBB1_4
# BB#3:                                 # %.preheader.i
                                        #   in Loop: Header=BB1_2 Depth=2
	cmpl	$-1, %ebp
	jne	.LBB1_2
.LBB1_4:                                # %text_getc.exit
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpl	$-1, %ebp
	je	.LBB1_5
# BB#6:                                 #   in Loop: Header=BB1_1 Depth=1
	callq	__ctype_b_loc
	movq	%rax, %r12
	movq	(%r12), %rax
	movslq	%ebp, %rcx
	movzwl	(%rax,%rcx,2), %eax
	testb	$32, %ah
	jne	.LBB1_1
# BB#7:
	testb	$8, %ah
	je	.LBB1_15
# BB#8:
	addl	$-48, %ebp
	movslq	%ebp, %r13
	jmp	.LBB1_9
	.p2align	4, 0x90
.LBB1_12:                               # %text_getc.exit23
                                        #   in Loop: Header=BB1_9 Depth=1
	cmpl	$-1, %ebp
	je	.LBB1_13
# BB#16:                                #   in Loop: Header=BB1_9 Depth=1
	movq	(%r12), %rcx
	movslq	%ebp, %rax
	testb	$8, 1(%rcx,%rax,2)
	je	.LBB1_14
# BB#17:                                #   in Loop: Header=BB1_9 Depth=1
	leaq	(%r13,%r13,4), %rcx
	leaq	-48(%rax,%rcx,2), %r13
.LBB1_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_10 Depth 2
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$35, %ebp
	jne	.LBB1_12
	.p2align	4, 0x90
.LBB1_10:                               # %.preheader.i21
                                        #   Parent Loop BB1_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$10, %ebp
	je	.LBB1_12
# BB#11:                                # %.preheader.i21
                                        #   in Loop: Header=BB1_10 Depth=2
	cmpl	$-1, %ebp
	jne	.LBB1_10
	jmp	.LBB1_12
.LBB1_5:
	movl	$-1, %ebp
	jmp	.LBB1_15
.LBB1_13:
	movl	$-1, %ebp
.LBB1_14:
	movq	%r13, (%r14)
	movl	$1, %r13d
.LBB1_15:                               # %.loopexit
	movl	%ebp, (%r15)
	movl	%r13d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	read_text_integer, .Lfunc_end1-read_text_integer
	.cfi_endproc

	.globl	read_scan_script
	.p2align	4, 0x90
	.type	read_scan_script,@function
read_scan_script:                       # @read_scan_script
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$3640, %rsp             # imm = 0xE38
.Lcfi32:
	.cfi_def_cfa_offset 3696
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movl	$.L.str, %esi
	movq	%rbp, %rdi
	callq	fopen
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB2_1
# BB#2:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	leaq	8(%rsp), %rsi
	leaq	4(%rsp), %rdx
	movq	%r12, %rdi
	callq	read_scan_integer
	xorl	%r15d, %r15d
	testl	%eax, %eax
	je	.LBB2_27
# BB#3:                                 # %.lr.ph75.preheader
	leaq	32(%rsp), %rbp
	leaq	8(%rsp), %rbx
	leaq	4(%rsp), %r13
.LBB2_4:                                # %.lr.ph75
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_9 Depth 2
	cmpl	$100, %r15d
	jge	.LBB2_5
# BB#7:                                 #   in Loop: Header=BB2_4 Depth=1
	movl	8(%rsp), %eax
	movl	%eax, 4(%rbp)
	movl	4(%rsp), %eax
	movl	$1, %r14d
	cmpl	$32, %eax
	jne	.LBB2_13
# BB#8:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB2_9:                                # %.lr.ph
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$4, %r14
	jge	.LBB2_10
# BB#11:                                #   in Loop: Header=BB2_9 Depth=2
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	read_scan_integer
	testl	%eax, %eax
	je	.LBB2_25
# BB#12:                                #   in Loop: Header=BB2_9 Depth=2
	movl	8(%rsp), %eax
	movl	%eax, 4(%rbp,%r14,4)
	incq	%r14
	movl	4(%rsp), %eax
	cmpl	$32, %eax
	je	.LBB2_9
	.p2align	4, 0x90
.LBB2_13:                               # %._crit_edge
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	%r14d, (%rbp)
	cmpl	$58, %eax
	jne	.LBB2_22
# BB#14:                                #   in Loop: Header=BB2_4 Depth=1
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	read_scan_integer
	testl	%eax, %eax
	je	.LBB2_25
# BB#15:                                #   in Loop: Header=BB2_4 Depth=1
	cmpl	$32, 4(%rsp)
	jne	.LBB2_25
# BB#16:                                #   in Loop: Header=BB2_4 Depth=1
	movl	8(%rsp), %eax
	movl	%eax, 20(%rbp)
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	read_scan_integer
	testl	%eax, %eax
	je	.LBB2_25
# BB#17:                                #   in Loop: Header=BB2_4 Depth=1
	cmpl	$32, 4(%rsp)
	jne	.LBB2_25
# BB#18:                                #   in Loop: Header=BB2_4 Depth=1
	movl	8(%rsp), %eax
	movl	%eax, 24(%rbp)
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	read_scan_integer
	testl	%eax, %eax
	je	.LBB2_25
# BB#19:                                #   in Loop: Header=BB2_4 Depth=1
	cmpl	$32, 4(%rsp)
	jne	.LBB2_25
# BB#20:                                #   in Loop: Header=BB2_4 Depth=1
	movl	8(%rsp), %eax
	movl	%eax, 28(%rbp)
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	read_scan_integer
	testl	%eax, %eax
	je	.LBB2_25
# BB#21:                                #   in Loop: Header=BB2_4 Depth=1
	movl	8(%rsp), %ecx
	movl	4(%rsp), %eax
	jmp	.LBB2_23
	.p2align	4, 0x90
.LBB2_22:                               #   in Loop: Header=BB2_4 Depth=1
	movabsq	$270582939648, %rcx     # imm = 0x3F00000000
	movq	%rcx, 20(%rbp)
	movl	$0, 28(%rbp)
	xorl	%ecx, %ecx
.LBB2_23:                               #   in Loop: Header=BB2_4 Depth=1
	movl	%ecx, 32(%rbp)
	cmpl	$-1, %eax
	je	.LBB2_26
# BB#24:                                #   in Loop: Header=BB2_4 Depth=1
	cmpl	$59, %eax
	jne	.LBB2_25
.LBB2_26:                               #   in Loop: Header=BB2_4 Depth=1
	addq	$36, %rbp
	incl	%r15d
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	read_scan_integer
	testl	%eax, %eax
	jne	.LBB2_4
.LBB2_27:                               # %._crit_edge76
	cmpl	$-1, 4(%rsp)
	jne	.LBB2_28
# BB#29:
	testl	%r15d, %r15d
	jle	.LBB2_31
# BB#30:
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	8(%rbx), %rax
	movslq	%r15d, %rcx
	shlq	$2, %rcx
	leaq	(%rcx,%rcx,8), %r14
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	*(%rax)
	movq	%rax, %rbp
	leaq	32(%rsp), %rsi
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	%rbp, 240(%rbx)
	movl	%r15d, 232(%rbx)
.LBB2_31:
	movq	%r12, %rdi
	callq	fclose
	movl	$1, %r14d
	jmp	.LBB2_32
.LBB2_25:                               # %.loopexit
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$.L.str.8, %esi
	jmp	.LBB2_6
.LBB2_10:
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$.L.str.7, %esi
	jmp	.LBB2_6
.LBB2_5:
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$.L.str.6, %esi
	jmp	.LBB2_6
.LBB2_1:
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	fprintf
	jmp	.LBB2_32
.LBB2_28:
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$.L.str.4, %esi
.LBB2_6:
	xorl	%eax, %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	fprintf
	movq	%r12, %rdi
	callq	fclose
.LBB2_32:
	movl	%r14d, %eax
	addq	$3640, %rsp             # imm = 0xE38
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	read_scan_script, .Lfunc_end2-read_scan_script
	.cfi_endproc

	.p2align	4, 0x90
	.type	read_scan_integer,@function
read_scan_integer:                      # @read_scan_integer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 48
.Lcfi44:
	.cfi_offset %rbx, -48
.Lcfi45:
	.cfi_offset %r12, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rdi, %rbp
	callq	read_text_integer
	xorl	%r14d, %r14d
	testl	%eax, %eax
	je	.LBB3_14
# BB#1:
	movl	(%r15), %ebx
	callq	__ctype_b_loc
	movq	%rax, %r12
	jmp	.LBB3_2
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	(%r12), %rax
	movslq	%ebx, %rcx
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB3_4
# BB#7:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$35, %ebx
	jne	.LBB3_2
	.p2align	4, 0x90
.LBB3_8:                                # %.preheader.i
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB3_2
# BB#9:                                 # %.preheader.i
                                        #   in Loop: Header=BB3_8 Depth=2
	cmpl	$10, %ebx
	jne	.LBB3_8
.LBB3_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_8 Depth 2
	cmpl	$-1, %ebx
	jne	.LBB3_6
# BB#3:
	movl	$-1, %ebx
.LBB3_4:                                # %.critedge
	movq	(%r12), %rax
	movslq	%ebx, %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB3_5
# BB#10:
	leal	1(%rbx), %eax
	cmpl	$60, %eax
	ja	.LBB3_12
# BB#11:
	movabsq	$1729382256910270465, %rcx # imm = 0x1800000000000001
	btq	%rax, %rcx
	jb	.LBB3_13
.LBB3_12:
	movl	$32, %ebx
	jmp	.LBB3_13
.LBB3_5:
	movl	%ebx, %edi
	movq	%rbp, %rsi
	callq	ungetc
	movl	$32, %ebx
	cmpl	$-1, %eax
	je	.LBB3_14
.LBB3_13:
	movl	%ebx, (%r15)
	movl	$1, %r14d
.LBB3_14:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	read_scan_integer, .Lfunc_end3-read_scan_integer
	.cfi_endproc

	.globl	set_quant_slots
	.p2align	4, 0x90
	.type	set_quant_slots,@function
set_quant_slots:                        # @set_quant_slots
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 64
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$0, 4(%rsp)
	xorl	%eax, %eax
	leaq	4(%rsp), %r15
	leaq	3(%rsp), %r12
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_7 Depth 2
	cmpb	$0, (%rbx)
	je	.LBB4_9
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	movb	$44, 3(%rsp)
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	sscanf
	testl	%eax, %eax
	jle	.LBB4_12
# BB#3:                                 #   in Loop: Header=BB4_1 Depth=1
	cmpb	$44, 3(%rsp)
	jne	.LBB4_12
# BB#4:                                 #   in Loop: Header=BB4_1 Depth=1
	movl	4(%rsp), %eax
	cmpl	$4, %eax
	jae	.LBB4_5
# BB#6:                                 #   in Loop: Header=BB4_1 Depth=1
	movq	80(%r14), %rcx
	leaq	(%rbp,%rbp,2), %rdx
	shlq	$5, %rdx
	movl	%eax, 16(%rcx,%rdx)
	.p2align	4, 0x90
.LBB4_7:                                #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx), %ecx
	testb	%cl, %cl
	je	.LBB4_10
# BB#8:                                 #   in Loop: Header=BB4_7 Depth=2
	incq	%rbx
	cmpb	$44, %cl
	jne	.LBB4_7
	jmp	.LBB4_10
	.p2align	4, 0x90
.LBB4_9:                                #   in Loop: Header=BB4_1 Depth=1
	movq	80(%r14), %rcx
	leaq	(%rbp,%rbp,2), %rdx
	shlq	$5, %rdx
	movl	%eax, 16(%rcx,%rdx)
.LBB4_10:                               # %.critedge
                                        #   in Loop: Header=BB4_1 Depth=1
	incq	%rbp
	cmpq	$10, %rbp
	jl	.LBB4_1
# BB#11:
	movl	$1, %r13d
	jmp	.LBB4_12
.LBB4_5:
	movq	stderr(%rip), %rdi
	xorl	%r13d, %r13d
	movl	$.L.str.10, %esi
	movl	$3, %edx
	xorl	%eax, %eax
	callq	fprintf
.LBB4_12:                               # %.loopexit
	movl	%r13d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	set_quant_slots, .Lfunc_end4-set_quant_slots
	.cfi_endproc

	.globl	set_sample_factors
	.p2align	4, 0x90
	.type	set_sample_factors,@function
set_sample_factors:                     # @set_sample_factors
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 80
.Lcfi69:
	.cfi_offset %rbx, -56
.Lcfi70:
	.cfi_offset %r12, -48
.Lcfi71:
	.cfi_offset %r13, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	xorl	%r12d, %r12d
	leaq	16(%rsp), %r13
	leaq	14(%rsp), %rbp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB5_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_8 Depth 2
	cmpb	$0, (%rbx)
	je	.LBB5_10
# BB#2:                                 #   in Loop: Header=BB5_1 Depth=1
	movb	$44, 14(%rsp)
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	leaq	20(%rsp), %rdx
	leaq	15(%rsp), %rcx
	movq	%r13, %r8
	movq	%rbp, %r9
	callq	sscanf
	cmpl	$3, %eax
	jl	.LBB5_13
# BB#3:                                 #   in Loop: Header=BB5_1 Depth=1
	movb	15(%rsp), %al
	orb	$32, %al
	cmpb	$120, %al
	jne	.LBB5_13
# BB#4:                                 #   in Loop: Header=BB5_1 Depth=1
	cmpb	$44, 14(%rsp)
	jne	.LBB5_13
# BB#5:                                 #   in Loop: Header=BB5_1 Depth=1
	movl	20(%rsp), %eax
	leal	-1(%rax), %edx
	movl	16(%rsp), %ecx
	leal	-1(%rcx), %esi
	orl	%edx, %esi
	cmpl	$4, %esi
	jae	.LBB5_6
# BB#7:                                 #   in Loop: Header=BB5_1 Depth=1
	movq	80(%r14), %rdx
	leaq	(%r12,%r12,2), %rsi
	shlq	$5, %rsi
	movl	%eax, 8(%rdx,%rsi)
	movl	%ecx, 12(%rdx,%rsi)
	.p2align	4, 0x90
.LBB5_8:                                #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.LBB5_11
# BB#9:                                 #   in Loop: Header=BB5_8 Depth=2
	incq	%rbx
	cmpb	$44, %al
	jne	.LBB5_8
	jmp	.LBB5_11
	.p2align	4, 0x90
.LBB5_10:                               #   in Loop: Header=BB5_1 Depth=1
	movq	80(%r14), %rax
	leaq	(%r12,%r12,2), %rcx
	shlq	$5, %rcx
	movl	$1, 8(%rax,%rcx)
	movl	$1, 12(%rax,%rcx)
.LBB5_11:                               # %.critedge
                                        #   in Loop: Header=BB5_1 Depth=1
	incq	%r12
	cmpq	$10, %r12
	jl	.LBB5_1
# BB#12:
	movl	$1, %r15d
	jmp	.LBB5_13
.LBB5_6:
	movq	stderr(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$35, %esi
	movl	$1, %edx
	callq	fwrite
.LBB5_13:                               # %.loopexit
	movl	%r15d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	set_sample_factors, .Lfunc_end5-set_sample_factors
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Can't open table file %s\n"
	.size	.L.str.1, 26

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Too many tables in file %s\n"
	.size	.L.str.2, 28

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Invalid table data in file %s\n"
	.size	.L.str.3, 31

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Non-numeric data in file %s\n"
	.size	.L.str.4, 29

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Can't open scan definition file %s\n"
	.size	.L.str.5, 36

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Too many scans defined in file %s\n"
	.size	.L.str.6, 35

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Too many components in one scan in file %s\n"
	.size	.L.str.7, 44

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Invalid scan entry format in file %s\n"
	.size	.L.str.8, 38

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%d%c"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"JPEG quantization tables are numbered 0..%d\n"
	.size	.L.str.10, 45

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%d%c%d%c"
	.size	.L.str.11, 9

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"JPEG sampling factors must be 1..4\n"
	.size	.L.str.12, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
