	.text
	.file	"functs.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI0_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_2:
	.quad	4636737291354636288     # double 100
	.text
	.globl	speedup_test
	.p2align	4, 0x90
	.type	speedup_test,@function
speedup_test:                           # @speedup_test
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$368, %rsp              # imm = 0x170
.Lcfi3:
	.cfi_def_cfa_offset 400
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	callq	rewind
	leaq	32(%rsp), %rbx
	leaq	16(%rsp), %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fscanf
	leaq	144(%rsp), %r15
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movl	$100, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	fgets
	cmpb	$69, 32(%rsp)
	jne	.LBB0_1
# BB#2:
	leaq	144(%rsp), %rdi
	leaq	256(%rsp), %r15
	leaq	24(%rsp), %rbx
	leaq	8(%rsp), %r8
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	movq	%rbx, %rcx
	callq	sscanf
	leaq	32(%rsp), %rdi
	movq	%rsp, %r8
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	movq	%rbx, %rcx
	callq	sscanf
	movq	8(%rsp), %rsi
	subq	16(%rsp), %rsi
	movq	%rsi, loop_time(%rip)
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rsi
	movq	%rsi, prog_time(%rip)
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	movq	8(%rsp), %rax
	subq	16(%rsp), %rax
	movd	%rax, %xmm1
	movdqa	.LCPI0_0(%rip), %xmm2   # xmm2 = [1127219200,1160773632,0,0]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movapd	.LCPI0_1(%rip), %xmm3   # xmm3 = [4.503600e+15,1.934281e+25]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	movq	(%rsp), %xmm1           # xmm1 = mem[0],zero
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	mulsd	.LCPI0_2(%rip), %xmm0
	movl	$.L.str.5, %edi
	movb	$1, %al
	callq	printf
	movq	%r14, %rdi
	callq	rewind
	addq	$368, %rsp              # imm = 0x170
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	speedup_test, .Lfunc_end0-speedup_test
	.cfi_endproc

	.globl	imix_test
	.p2align	4, 0x90
	.type	imix_test,@function
imix_test:                              # @imix_test
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 40
	subq	$360, %rsp              # imm = 0x168
.Lcfi11:
	.cfi_def_cfa_offset 400
.Lcfi12:
	.cfi_offset %rbx, -40
.Lcfi13:
	.cfi_offset %r12, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	callq	rewind
	leaq	32(%rsp), %r15
	movl	$100, %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	fgets
	xorl	%ebx, %ebx
	leaq	16(%rsp), %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	fscanf
	leaq	144(%rsp), %r12
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	cmpb	$58, 34(%rsp)
	sete	%al
	addq	%rax, %rbx
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movl	$100, %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	fgets
	cmpb	$69, 32(%rsp)
	jne	.LBB1_1
# BB#2:
	leaq	144(%rsp), %rdi
	leaq	256(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	8(%rsp), %r8
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	sscanf
	movq	%r14, %rdi
	callq	rewind
	movq	%rbx, %rax
	addq	$360, %rsp              # imm = 0x168
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	imix_test, .Lfunc_end1-imix_test
	.cfi_endproc

	.globl	find_hard_raws
	.p2align	4, 0x90
	.type	find_hard_raws,@function
find_hard_raws:                         # @find_hard_raws
	.cfi_startproc
# BB#0:
	movq	hard_raw_list(%rip), %rax
	testq	%rax, %rax
	jne	.LBB2_2
	jmp	.LBB2_6
	.p2align	4, 0x90
.LBB2_5:                                # %.backedge
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB2_6
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%rax), %rcx
	cmpq	32(%rax), %rcx
	jbe	.LBB2_5
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB2_5
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	jmp	.LBB2_5
.LBB2_6:                                # %._crit_edge
	retq
.Lfunc_end2:
	.size	find_hard_raws, .Lfunc_end2-find_hard_raws
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI3_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.globl	specul_time_o
	.p2align	4, 0x90
	.type	specul_time_o,@function
specul_time_o:                          # @specul_time_o
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 64
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
	movl	%r8d, %ebp
	movl	%ecx, %r15d
	movl	%esi, %r12d
	testl	%r12d, %r12d
	jle	.LBB3_7
# BB#1:                                 # %.lr.ph95.preheader
	movl	%r12d, %eax
	testb	$1, %al
	jne	.LBB3_3
# BB#2:
	xorl	%ecx, %ecx
	cmpl	$1, %r12d
	jne	.LBB3_5
	jmp	.LBB3_7
.LBB3_3:                                # %.lr.ph95.prol
	movq	32(%rdi), %rcx
	subq	24(%rdi), %rcx
	movq	%rcx, 40(%rdi)
	movq	$0, 48(%rdi)
	movl	$1, %ecx
	cmpl	$1, %r12d
	je	.LBB3_7
.LBB3_5:                                # %.lr.ph95.preheader.new
	subq	%rcx, %rax
	imulq	$56, %rcx, %rcx
	leaq	104(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph95
                                        # =>This Inner Loop Header: Depth=1
	movq	-72(%rcx), %rsi
	subq	-80(%rcx), %rsi
	movq	%rsi, -64(%rcx)
	movq	$0, -56(%rcx)
	movq	-16(%rcx), %rsi
	subq	-24(%rcx), %rsi
	movq	%rsi, -8(%rcx)
	movq	$0, (%rcx)
	addq	$112, %rcx
	addq	$-2, %rax
	jne	.LBB3_6
.LBB3_7:                                # %.preheader82
	movq	hard_raw_list(%rip), %rax
	xorl	%ebx, %ebx
	testq	%rax, %rax
	jne	.LBB3_21
	jmp	.LBB3_9
	.p2align	4, 0x90
.LBB3_23:                               #   in Loop: Header=BB3_21 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB3_9
.LBB3_21:                               # %.lr.ph91
                                        # =>This Inner Loop Header: Depth=1
	movslq	24(%rax), %rcx
	imulq	$56, %rcx, %rcx
	movq	48(%rdi,%rcx), %rcx
	addq	32(%rax), %rcx
	movslq	40(%rax), %rsi
	imulq	$56, %rsi, %rsi
	cmpq	48(%rdi,%rsi), %rcx
	jbe	.LBB3_23
# BB#22:                                #   in Loop: Header=BB3_21 Depth=1
	leaq	48(%rdi,%rsi), %rsi
	movq	%rcx, (%rsi)
	incq	%rbx
	jmp	.LBB3_23
.LBB3_9:                                # %.preheader
	testl	%r12d, %r12d
	movq	%rdx, (%rsp)            # 8-byte Spill
	jle	.LBB3_20
# BB#10:                                # %.lr.ph86.preheader
	movl	%r12d, %eax
	testb	$1, %al
	jne	.LBB3_12
# BB#11:
	xorl	%esi, %esi
	cmpl	$1, %r12d
	jne	.LBB3_14
	jmp	.LBB3_16
.LBB3_20:                               # %._crit_edge87.thread
	movq	40(%rdi), %r14
	jmp	.LBB3_30
.LBB3_12:                               # %.lr.ph86.prol
	movq	48(%rdi), %rcx
	addq	%rcx, 40(%rdi)
	movl	$1, %esi
	cmpl	$1, %r12d
	je	.LBB3_16
.LBB3_14:                               # %.lr.ph86.preheader.new
	movq	%rax, %rcx
	subq	%rsi, %rcx
	imulq	$56, %rsi, %rdx
	leaq	40(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB3_15:                               # %.lr.ph86
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdx), %rsi
	addq	%rsi, (%rdx)
	movq	64(%rdx), %rsi
	addq	%rsi, 56(%rdx)
	addq	$112, %rdx
	addq	$-2, %rcx
	jne	.LBB3_15
.LBB3_16:                               # %._crit_edge87
	movq	40(%rdi), %r14
	testl	%r12d, %r12d
	jle	.LBB3_30
# BB#17:                                # %.lr.ph.preheader
	cmpl	$1, %r12d
	je	.LBB3_30
# BB#18:                                # %.lr.ph..lr.ph_crit_edge.preheader
	leal	3(%rax), %esi
	leaq	-2(%rax), %r8
	andq	$3, %rsi
	je	.LBB3_19
# BB#24:                                # %.lr.ph..lr.ph_crit_edge.prol.preheader
	movl	%ebp, %r9d
	leaq	96(%rdi), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_25:                               # %.lr.ph..lr.ph_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rbp
	cmpq	%r14, %rbp
	cmovaq	%rbp, %r14
	incq	%rdx
	addq	$56, %rcx
	cmpq	%rdx, %rsi
	jne	.LBB3_25
# BB#26:                                # %.lr.ph..lr.ph_crit_edge.prol.loopexit.unr-lcssa
	incq	%rdx
	movl	%r9d, %ebp
	cmpq	$3, %r8
	jae	.LBB3_28
	jmp	.LBB3_30
.LBB3_19:
	movl	$1, %edx
	cmpq	$3, %r8
	jb	.LBB3_30
.LBB3_28:                               # %.lr.ph..lr.ph_crit_edge.preheader.new
	subq	%rdx, %rax
	imulq	$56, %rdx, %rcx
	leaq	208(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB3_29:                               # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	-168(%rcx), %rdx
	movq	-112(%rcx), %rsi
	cmpq	%r14, %rdx
	cmovaq	%rdx, %r14
	cmpq	%r14, %rsi
	cmovaq	%rsi, %r14
	movq	-56(%rcx), %rdx
	cmpq	%r14, %rdx
	cmovaq	%rdx, %r14
	movq	(%rcx), %rdx
	cmpq	%r14, %rdx
	cmovaq	%rdx, %r14
	addq	$224, %rcx
	addq	$-4, %rax
	jne	.LBB3_29
.LBB3_30:                               # %._crit_edge
	movl	$.Lstr, %edi
	callq	puts
	movslq	%r12d, %rax
	addq	%rax, %rbx
	movslq	%ebp, %rax
	imulq	%rbx, %rax
	imull	%r12d, %r13d
	movslq	%r13d, %rbx
	addq	%rax, %rbx
	addq	%r14, %rbx
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	movl	%r15d, %ebp
	orl	$2, %ebp
	cmpl	$3, %ebp
	jne	.LBB3_32
# BB#31:
	movq	loop_time(%rip), %xmm1  # xmm1 = mem[0],zero
	movdqa	.LCPI3_0(%rip), %xmm2   # xmm2 = [1127219200,1160773632,0,0]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movapd	.LCPI3_1(%rip), %xmm3   # xmm3 = [4.503600e+15,1.934281e+25]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	movd	%rbx, %xmm1
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	movl	$.L.str.8, %edi
	movb	$1, %al
	callq	printf
.LBB3_32:
	orl	$1, %r15d
	cmpl	$3, %r15d
	movq	(%rsp), %r14            # 8-byte Reload
	jne	.LBB3_34
# BB#33:
	movq	prog_time(%rip), %rax
	movd	%rax, %xmm1
	movdqa	.LCPI3_0(%rip), %xmm2   # xmm2 = [1127219200,1160773632,0,0]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movapd	.LCPI3_1(%rip), %xmm3   # xmm3 = [4.503600e+15,1.934281e+25]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	addq	%rbx, %rax
	subq	loop_time(%rip), %rax
	movd	%rax, %xmm1
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	movl	$.L.str.9, %edi
	movb	$1, %al
	callq	printf
.LBB3_34:
	testq	%r14, %r14
	je	.LBB3_38
# BB#35:
	cmpl	$3, %ebp
	jne	.LBB3_37
# BB#36:
	movq	loop_time(%rip), %xmm1  # xmm1 = mem[0],zero
	movdqa	.LCPI3_0(%rip), %xmm2   # xmm2 = [1127219200,1160773632,0,0]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movapd	.LCPI3_1(%rip), %xmm3   # xmm3 = [4.503600e+15,1.934281e+25]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	movd	%rbx, %xmm1
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	movl	$.L.str.10, %esi
	movb	$1, %al
	movq	%r14, %rdi
	callq	fprintf
.LBB3_37:
	cmpl	$3, %r15d
	jne	.LBB3_38
# BB#39:
	movq	prog_time(%rip), %rax
	movd	%rax, %xmm1
	movdqa	.LCPI3_0(%rip), %xmm2   # xmm2 = [1127219200,1160773632,0,0]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movapd	.LCPI3_1(%rip), %xmm3   # xmm3 = [4.503600e+15,1.934281e+25]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	addq	%rax, %rbx
	subq	loop_time(%rip), %rbx
	movd	%rbx, %xmm1
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	movl	$.L.str.11, %esi
	movb	$1, %al
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fprintf                 # TAILCALL
.LBB3_38:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	specul_time_o, .Lfunc_end3-specul_time_o
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI4_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.globl	specul_time_r
	.p2align	4, 0x90
	.type	specul_time_r,@function
specul_time_r:                          # @specul_time_r
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 80
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movl	%r9d, 12(%rsp)          # 4-byte Spill
	movl	%r8d, %r15d
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %r14d
	movl	%esi, %r13d
	movq	%rdi, %rbx
	movl	80(%rsp), %r12d
	testl	%r14d, %r14d
	je	.LBB4_1
# BB#30:
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	testl	%r13d, %r13d
	jle	.LBB4_37
# BB#31:                                # %.lr.ph217.preheader
	movl	%r13d, %eax
	testb	$1, %al
	jne	.LBB4_33
# BB#32:
	xorl	%ecx, %ecx
	cmpl	$1, %r13d
	jne	.LBB4_35
	jmp	.LBB4_37
.LBB4_1:
	movl	$.Lstr.1, %edi
	callq	puts
	testl	%r13d, %r13d
	jle	.LBB4_8
# BB#2:                                 # %.lr.ph191.preheader
	movl	%r13d, %eax
	testb	$1, %al
	jne	.LBB4_4
# BB#3:
	xorl	%ecx, %ecx
	cmpl	$1, %r13d
	jne	.LBB4_6
	jmp	.LBB4_8
.LBB4_33:                               # %.lr.ph217.prol
	movq	32(%rbx), %rcx
	subq	24(%rbx), %rcx
	movq	%rcx, 40(%rbx)
	movq	$0, 48(%rbx)
	movl	$1, %ecx
	cmpl	$1, %r13d
	je	.LBB4_37
.LBB4_35:                               # %.lr.ph217.preheader.new
	subq	%rcx, %rax
	imulq	$56, %rcx, %rcx
	leaq	104(%rbx,%rcx), %rcx
	.p2align	4, 0x90
.LBB4_36:                               # %.lr.ph217
                                        # =>This Inner Loop Header: Depth=1
	movq	-72(%rcx), %rdx
	subq	-80(%rcx), %rdx
	movq	%rdx, -64(%rcx)
	movq	$0, -56(%rcx)
	movq	-16(%rcx), %rdx
	subq	-24(%rcx), %rdx
	movq	%rdx, -8(%rcx)
	movq	$0, (%rcx)
	addq	$112, %rcx
	addq	$-2, %rax
	jne	.LBB4_36
.LBB4_37:                               # %.preheader179
	cmpl	%r13d, %r14d
	jge	.LBB4_42
# BB#38:                                # %.lr.ph214.preheader
	movslq	%r14d, %r10
	movslq	%r13d, %r8
	leaq	48(%rbx), %r9
	xorl	%esi, %esi
	movq	%r10, %rdi
	.p2align	4, 0x90
.LBB4_39:                               # %.lr.ph214
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_51 Depth 2
                                        #     Child Loop BB4_49 Depth 2
	testl	%r14d, %r14d
	jle	.LBB4_40
# BB#50:                                # %.lr.ph208.preheader
                                        #   in Loop: Header=BB4_39 Depth=1
	movslq	%esi, %rcx
	imulq	$56, %rcx, %rdx
	addq	%r9, %rdx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_51:                               # %.lr.ph208
                                        #   Parent Loop BB4_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rax
	movq	(%rdx), %rbp
	addq	-8(%rdx), %rbp
	cmpq	%rax, %rbp
	cmovbeq	%rax, %rbp
	incq	%rcx
	addq	$56, %rdx
	cmpq	%rdi, %rcx
	jl	.LBB4_51
# BB#47:                                # %.preheader177
                                        #   in Loop: Header=BB4_39 Depth=1
	leaq	(%rdi,%r10), %rcx
	testl	%r14d, %r14d
	jle	.LBB4_41
# BB#48:                                # %.lr.ph211.preheader
                                        #   in Loop: Header=BB4_39 Depth=1
	imulq	$56, %rdi, %rdx
	addq	%r9, %rdx
	.p2align	4, 0x90
.LBB4_49:                               # %.lr.ph211
                                        #   Parent Loop BB4_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, (%rdx)
	incq	%rdi
	addq	$56, %rdx
	cmpq	%rcx, %rdi
	jl	.LBB4_49
	jmp	.LBB4_41
	.p2align	4, 0x90
.LBB4_40:                               # %.preheader177.thread
                                        #   in Loop: Header=BB4_39 Depth=1
	addq	%r10, %rdi
	movq	%rdi, %rcx
.LBB4_41:                               # %.loopexit178
                                        #   in Loop: Header=BB4_39 Depth=1
	addl	%r14d, %esi
	cmpq	%r8, %rcx
	movq	%rcx, %rdi
	jl	.LBB4_39
.LBB4_42:                               # %.preheader176
	movq	hard_raw_list(%rip), %rcx
	xorl	%r10d, %r10d
	testq	%rcx, %rcx
	jne	.LBB4_61
	jmp	.LBB4_44
	.p2align	4, 0x90
.LBB4_63:                               #   in Loop: Header=BB4_61 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB4_44
.LBB4_61:                               # %.lr.ph203
                                        # =>This Inner Loop Header: Depth=1
	movslq	24(%rcx), %rdx
	imulq	$56, %rdx, %rdx
	movq	48(%rbx,%rdx), %rsi
	movq	32(%rcx), %rax
	addq	%rsi, %rax
	movslq	40(%rcx), %rdi
	imulq	$56, %rdi, %rbp
	movq	48(%rbx,%rbp), %rdi
	cmpq	%rdi, %rax
	jbe	.LBB4_63
# BB#62:                                #   in Loop: Header=BB4_61 Depth=1
	leaq	48(%rbx,%rbp), %rax
	addq	%rsi, %rdi
	addq	40(%rbx,%rdx), %rdi
	movq	%rdi, (%rax)
	incq	%r10
	jmp	.LBB4_63
.LBB4_44:                               # %.preheader175
	testl	%r13d, %r13d
	jle	.LBB4_60
# BB#45:                                # %.lr.ph198.preheader
	movl	%r13d, %edx
	testb	$1, %dl
	jne	.LBB4_52
# BB#46:
	xorl	%esi, %esi
	cmpl	$1, %r13d
	jne	.LBB4_54
	jmp	.LBB4_56
.LBB4_52:                               # %.lr.ph198.prol
	movq	48(%rbx), %rcx
	addq	%rcx, 40(%rbx)
	movl	$1, %esi
	cmpl	$1, %r13d
	je	.LBB4_56
.LBB4_54:                               # %.lr.ph198.preheader.new
	movq	%rdx, %rcx
	subq	%rsi, %rcx
	imulq	$56, %rsi, %rsi
	leaq	40(%rbx,%rsi), %rsi
	.p2align	4, 0x90
.LBB4_55:                               # %.lr.ph198
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsi), %rax
	addq	%rax, (%rsi)
	movq	64(%rsi), %rax
	addq	%rax, 56(%rsi)
	addq	$112, %rsi
	addq	$-2, %rcx
	jne	.LBB4_55
.LBB4_56:                               # %._crit_edge199
	movq	40(%rbx), %rcx
	testl	%r13d, %r13d
	jle	.LBB4_70
# BB#57:                                # %.lr.ph195.preheader
	cmpl	$1, %r13d
	je	.LBB4_70
# BB#58:                                # %.lr.ph195..lr.ph195_crit_edge.preheader
	leal	3(%rdx), %ebp
	leaq	-2(%rdx), %r8
	andq	$3, %rbp
	je	.LBB4_59
# BB#64:                                # %.lr.ph195..lr.ph195_crit_edge.prol.preheader
	leaq	96(%rbx), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_65:                               # %.lr.ph195..lr.ph195_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	cmpq	%rcx, %rax
	cmovaq	%rax, %rcx
	incq	%rdi
	addq	$56, %rsi
	cmpq	%rdi, %rbp
	jne	.LBB4_65
# BB#66:                                # %.lr.ph195..lr.ph195_crit_edge.prol.loopexit.unr-lcssa
	incq	%rdi
	cmpq	$3, %r8
	jb	.LBB4_70
	jmp	.LBB4_68
.LBB4_4:                                # %.lr.ph191.prol
	movq	32(%rbx), %rcx
	subq	24(%rbx), %rcx
	movq	%rcx, 40(%rbx)
	movq	$0, 48(%rbx)
	movl	$1, %ecx
	cmpl	$1, %r13d
	je	.LBB4_8
.LBB4_6:                                # %.lr.ph191.preheader.new
	subq	%rcx, %rax
	imulq	$56, %rcx, %rcx
	leaq	104(%rbx,%rcx), %rcx
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph191
                                        # =>This Inner Loop Header: Depth=1
	movq	-72(%rcx), %rdx
	subq	-80(%rcx), %rdx
	movq	%rdx, -64(%rcx)
	movq	$0, -56(%rcx)
	movq	-16(%rcx), %rdx
	subq	-24(%rcx), %rdx
	movq	%rdx, -8(%rcx)
	movq	$0, (%rcx)
	addq	$112, %rcx
	addq	$-2, %rax
	jne	.LBB4_7
.LBB4_8:                                # %.preheader173
	movq	hard_raw_list(%rip), %rcx
	xorl	%r10d, %r10d
	testq	%rcx, %rcx
	jne	.LBB4_21
	jmp	.LBB4_10
	.p2align	4, 0x90
.LBB4_23:                               #   in Loop: Header=BB4_21 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB4_10
.LBB4_21:                               # %.lr.ph187
                                        # =>This Inner Loop Header: Depth=1
	movslq	24(%rcx), %rdx
	imulq	$56, %rdx, %rdx
	movq	48(%rbx,%rdx), %rsi
	movq	32(%rcx), %rbp
	addq	%rsi, %rbp
	movslq	40(%rcx), %rdi
	imulq	$56, %rdi, %rdi
	cmpq	48(%rbx,%rdi), %rbp
	jbe	.LBB4_23
# BB#22:                                #   in Loop: Header=BB4_21 Depth=1
	leaq	48(%rbx,%rdi), %rdi
	addq	40(%rbx,%rdx), %rsi
	movq	%rsi, (%rdi)
	incq	%r10
	jmp	.LBB4_23
.LBB4_10:                               # %.preheader
	testl	%r13d, %r13d
	jle	.LBB4_60
# BB#11:                                # %.lr.ph183.preheader
	movl	%r13d, %edx
	testb	$1, %dl
	jne	.LBB4_13
# BB#12:
	xorl	%esi, %esi
	cmpl	$1, %r13d
	jne	.LBB4_15
	jmp	.LBB4_17
.LBB4_60:                               # %._crit_edge.thread
	movq	40(%rbx), %rcx
.LBB4_70:
	movl	12(%rsp), %edx          # 4-byte Reload
	movslq	%r13d, %rax
	addq	%r10, %rax
	movslq	%edx, %rdx
	imulq	%rax, %rdx
	imull	%r13d, %r12d
	movslq	%r12d, %rbx
	addq	%rcx, %rbx
	addq	%rdx, %rbx
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	movl	%r15d, %ebp
	orl	$2, %ebp
	cmpl	$3, %ebp
	jne	.LBB4_72
# BB#71:
	movq	loop_time(%rip), %xmm1  # xmm1 = mem[0],zero
	movdqa	.LCPI4_0(%rip), %xmm2   # xmm2 = [1127219200,1160773632,0,0]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movapd	.LCPI4_1(%rip), %xmm3   # xmm3 = [4.503600e+15,1.934281e+25]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	movd	%rbx, %xmm1
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	movl	$.L.str.8, %edi
	movb	$1, %al
	callq	printf
.LBB4_72:
	orl	$1, %r15d
	cmpl	$3, %r15d
	movq	16(%rsp), %r12          # 8-byte Reload
	jne	.LBB4_74
# BB#73:
	movq	prog_time(%rip), %rax
	movd	%rax, %xmm1
	movdqa	.LCPI4_0(%rip), %xmm2   # xmm2 = [1127219200,1160773632,0,0]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movapd	.LCPI4_1(%rip), %xmm3   # xmm3 = [4.503600e+15,1.934281e+25]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	addq	%rbx, %rax
	subq	loop_time(%rip), %rax
	movd	%rax, %xmm1
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	movl	$.L.str.9, %edi
	movb	$1, %al
	callq	printf
.LBB4_74:
	testq	%r12, %r12
	je	.LBB4_78
# BB#75:
	cmpl	$3, %ebp
	jne	.LBB4_77
# BB#76:
	movq	loop_time(%rip), %xmm1  # xmm1 = mem[0],zero
	movdqa	.LCPI4_0(%rip), %xmm2   # xmm2 = [1127219200,1160773632,0,0]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movapd	.LCPI4_1(%rip), %xmm3   # xmm3 = [4.503600e+15,1.934281e+25]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	movd	%rbx, %xmm1
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	movl	$.L.str.14, %esi
	movb	$1, %al
	movq	%r12, %rdi
	movl	%r14d, %edx
	callq	fprintf
.LBB4_77:
	cmpl	$3, %r15d
	jne	.LBB4_78
# BB#79:
	movq	prog_time(%rip), %rax
	movd	%rax, %xmm1
	movdqa	.LCPI4_0(%rip), %xmm2   # xmm2 = [1127219200,1160773632,0,0]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movapd	.LCPI4_1(%rip), %xmm3   # xmm3 = [4.503600e+15,1.934281e+25]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	addq	%rax, %rbx
	subq	loop_time(%rip), %rbx
	movd	%rbx, %xmm1
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	subpd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	movl	$.L.str.15, %esi
	movb	$1, %al
	movq	%r12, %rdi
	movl	%r14d, %edx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fprintf                 # TAILCALL
.LBB4_78:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_13:                               # %.lr.ph183.prol
	movq	48(%rbx), %rcx
	addq	%rcx, 40(%rbx)
	movl	$1, %esi
	cmpl	$1, %r13d
	je	.LBB4_17
.LBB4_15:                               # %.lr.ph183.preheader.new
	movq	%rdx, %rcx
	subq	%rsi, %rcx
	imulq	$56, %rsi, %rsi
	leaq	40(%rbx,%rsi), %rsi
	.p2align	4, 0x90
.LBB4_16:                               # %.lr.ph183
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsi), %rdi
	addq	%rdi, (%rsi)
	movq	64(%rsi), %rdi
	addq	%rdi, 56(%rsi)
	addq	$112, %rsi
	addq	$-2, %rcx
	jne	.LBB4_16
.LBB4_17:                               # %._crit_edge
	movq	40(%rbx), %rcx
	testl	%r13d, %r13d
	jle	.LBB4_70
# BB#18:                                # %.lr.ph.preheader
	cmpl	$1, %r13d
	je	.LBB4_70
# BB#19:                                # %.lr.ph..lr.ph_crit_edge.preheader
	leal	3(%rdx), %r9d
	leaq	-2(%rdx), %r8
	andq	$3, %r9
	je	.LBB4_20
# BB#24:                                # %.lr.ph..lr.ph_crit_edge.prol.preheader
	leaq	96(%rbx), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_25:                               # %.lr.ph..lr.ph_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rbp
	cmpq	%rcx, %rbp
	cmovaq	%rbp, %rcx
	incq	%rdi
	addq	$56, %rsi
	cmpq	%rdi, %r9
	jne	.LBB4_25
# BB#26:                                # %.lr.ph..lr.ph_crit_edge.prol.loopexit.unr-lcssa
	incq	%rdi
	cmpq	$3, %r8
	jb	.LBB4_70
	jmp	.LBB4_28
.LBB4_59:
	movl	$1, %edi
	cmpq	$3, %r8
	jb	.LBB4_70
.LBB4_68:                               # %.lr.ph195..lr.ph195_crit_edge.preheader.new
	subq	%rdi, %rdx
	imulq	$56, %rdi, %rax
	leaq	208(%rbx,%rax), %rsi
	.p2align	4, 0x90
.LBB4_69:                               # %.lr.ph195..lr.ph195_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	-168(%rsi), %rax
	movq	-112(%rsi), %rdi
	cmpq	%rcx, %rax
	cmovaq	%rax, %rcx
	cmpq	%rcx, %rdi
	cmovaq	%rdi, %rcx
	movq	-56(%rsi), %rax
	cmpq	%rcx, %rax
	cmovaq	%rax, %rcx
	movq	(%rsi), %rax
	cmpq	%rcx, %rax
	cmovaq	%rax, %rcx
	addq	$224, %rsi
	addq	$-4, %rdx
	jne	.LBB4_69
	jmp	.LBB4_70
.LBB4_20:
	movl	$1, %edi
	cmpq	$3, %r8
	jb	.LBB4_70
.LBB4_28:                               # %.lr.ph..lr.ph_crit_edge.preheader.new
	subq	%rdi, %rdx
	imulq	$56, %rdi, %rsi
	leaq	208(%rbx,%rsi), %rsi
	.p2align	4, 0x90
.LBB4_29:                               # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	-168(%rsi), %rdi
	movq	-112(%rsi), %rbp
	cmpq	%rcx, %rdi
	cmovaq	%rdi, %rcx
	cmpq	%rcx, %rbp
	cmovaq	%rbp, %rcx
	movq	-56(%rsi), %rdi
	cmpq	%rcx, %rdi
	cmovaq	%rdi, %rcx
	movq	(%rsi), %rdi
	cmpq	%rcx, %rdi
	cmovaq	%rdi, %rcx
	addq	$224, %rsi
	addq	$-4, %rdx
	jne	.LBB4_29
	jmp	.LBB4_70
.Lfunc_end4:
	.size	specul_time_r, .Lfunc_end4-specul_time_r
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s %lu"
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%s %lx %lu"
	.size	.L.str.1, 11

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s %lu %lu"
	.size	.L.str.2, 11

	.type	loop_time,@object       # @loop_time
	.comm	loop_time,8,8
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Time for loop: %lu issues\n"
	.size	.L.str.3, 27

	.type	prog_time,@object       # @prog_time
	.comm	prog_time,8,8
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Time for program: %lu issues\n"
	.size	.L.str.4, 30

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Loop is %0.3g %% of program\n"
	.size	.L.str.5, 29

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Time for speculative loop is %lu issues\n"
	.size	.L.str.7, 41

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Potential speedup for loop: %0.3g times\n"
	.size	.L.str.8, 41

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Potential speedup for program: %0.3g times\n"
	.size	.L.str.9, 44

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"optloop: %0.3g\n"
	.size	.L.str.10, 16

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"optprog: %0.3g\n"
	.size	.L.str.11, 16

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"REALISTIC RESTART RESULTS -- %d CPUs\n"
	.size	.L.str.13, 38

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"realloop: %d %0.3g\n"
	.size	.L.str.14, 20

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"realprog: %d %0.3g\n"
	.size	.L.str.15, 20

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"OPTIMUM RESTART RESULTS"
	.size	.Lstr, 24

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"REALISTIC RESTART RESULTS -- Unlimited amount of CPUs"
	.size	.Lstr.1, 54


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
