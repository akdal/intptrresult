	.text
	.file	"liolib.bc"
	.globl	luaopen_io
	.p2align	4, 0x90
	.type	luaopen_io,@function
luaopen_io:                             # @luaopen_io
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$.L.str.5, %esi
	callq	luaL_newmetatable
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-2, %esi
	movl	$.L.str.6, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	xorl	%esi, %esi
	movl	$flib, %edx
	movq	%rbx, %rdi
	callq	luaL_register
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_createtable
	movl	$io_fclose, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$-2, %esi
	movl	$.L.str.17, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$-10001, %esi           # imm = 0xD8EF
	movq	%rbx, %rdi
	callq	lua_replace
	movl	$.L.str, %esi
	movl	$iolib, %edx
	movq	%rbx, %rdi
	callq	luaL_register
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_createtable
	movl	$io_noclose, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$-2, %esi
	movl	$.L.str.17, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movq	stdin(%rip), %r15
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	lua_newuserdata
	movq	%rax, %r14
	movq	$0, (%r14)
	movl	$-10000, %esi           # imm = 0xD8F0
	movl	$.L.str.5, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setmetatable
	movq	%r15, (%r14)
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-10001, %esi           # imm = 0xD8EF
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_rawseti
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setfenv
	movl	$-3, %esi
	movl	$.L.str.1, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movq	stdout(%rip), %r15
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	lua_newuserdata
	movq	%rax, %r14
	movq	$0, (%r14)
	movl	$-10000, %esi           # imm = 0xD8F0
	movl	$.L.str.5, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setmetatable
	movq	%r15, (%r14)
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-10001, %esi           # imm = 0xD8EF
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	lua_rawseti
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setfenv
	movl	$-3, %esi
	movl	$.L.str.2, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movq	stderr(%rip), %r15
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	lua_newuserdata
	movq	%rax, %r14
	movq	$0, (%r14)
	movl	$-10000, %esi           # imm = 0xD8F0
	movl	$.L.str.5, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setmetatable
	movq	%r15, (%r14)
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setfenv
	movl	$-3, %esi
	movl	$.L.str.3, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$-1, %esi
	movl	$.L.str.4, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_createtable
	movl	$io_pclose, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$-2, %esi
	movl	$.L.str.17, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setfenv
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	luaopen_io, .Lfunc_end0-luaopen_io
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_fclose,@function
io_fclose:                              # @io_fclose
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 48
.Lcfi11:
	.cfi_offset %rbx, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	$1, %r14d
	movl	$1, %esi
	movl	$.L.str.5, %edx
	callq	luaL_checkudata
	movq	%rax, %rbp
	movq	(%rbp), %rdi
	callq	fclose
	movl	%eax, %ebx
	movq	$0, (%rbp)
	callq	__errno_location
	testl	%ebx, %ebx
	je	.LBB1_1
# BB#2:
	movslq	(%rax), %rbx
	movq	%r15, %rdi
	callq	lua_pushnil
	movl	%ebx, %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	callq	lua_pushfstring
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	lua_pushinteger
	movl	$3, %r14d
	jmp	.LBB1_3
.LBB1_1:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	lua_pushboolean
.LBB1_3:                                # %pushresult.exit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	io_fclose, .Lfunc_end1-io_fclose
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_noclose,@function
io_noclose:                             # @io_noclose
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 16
.Lcfi16:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	lua_pushnil
	movl	$.L.str.44, %esi
	movl	$26, %edx
	movq	%rbx, %rdi
	callq	lua_pushlstring
	movl	$2, %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	io_noclose, .Lfunc_end2-io_noclose
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_pclose,@function
io_pclose:                              # @io_pclose
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 48
.Lcfi22:
	.cfi_offset %rbx, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	$1, %r14d
	movl	$1, %esi
	movl	$.L.str.5, %edx
	callq	luaL_checkudata
	movq	%rax, %rbp
	movq	(%rbp), %rdi
	callq	pclose
	movl	%eax, %ebx
	movq	$0, (%rbp)
	callq	__errno_location
	cmpl	$-1, %ebx
	je	.LBB3_2
# BB#1:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	lua_pushboolean
	jmp	.LBB3_3
.LBB3_2:
	movslq	(%rax), %rbx
	movq	%r15, %rdi
	callq	lua_pushnil
	movl	%ebx, %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	callq	lua_pushfstring
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	lua_pushinteger
	movl	$3, %r14d
.LBB3_3:                                # %pushresult.exit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	io_pclose, .Lfunc_end3-io_pclose
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_close,@function
io_close:                               # @io_close
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 16
.Lcfi27:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	lua_type
	cmpl	$-1, %eax
	jne	.LBB4_2
# BB#1:
	movl	$-10001, %esi           # imm = 0xD8EF
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	lua_rawgeti
.LBB4_2:
	movl	$1, %esi
	movl	$.L.str.5, %edx
	movq	%rbx, %rdi
	callq	luaL_checkudata
	cmpq	$0, (%rax)
	jne	.LBB4_4
# BB#3:
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB4_4:                                # %tofile.exit
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_getfenv
	movl	$-1, %esi
	movl	$.L.str.17, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_tocfunction
	movq	%rbx, %rdi
	popq	%rbx
	jmpq	*%rax                   # TAILCALL
.Lfunc_end4:
	.size	io_close, .Lfunc_end4-io_close
	.cfi_endproc

	.p2align	4, 0x90
	.type	f_flush,@function
f_flush:                                # @f_flush
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$1, %esi
	movl	$.L.str.5, %edx
	callq	luaL_checkudata
	movq	%rax, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB5_2
# BB#1:
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	luaL_error
	movq	(%rbx), %rdi
.LBB5_2:                                # %tofile.exit
	callq	fflush
	movl	%eax, %ebx
	callq	__errno_location
	testl	%ebx, %ebx
	je	.LBB5_3
# BB#4:
	movslq	(%rax), %rbx
	movq	%r14, %rdi
	callq	lua_pushnil
	movl	%ebx, %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rcx, %rdx
	callq	lua_pushfstring
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	lua_pushinteger
	movl	$3, %ebx
	jmp	.LBB5_5
.LBB5_3:
	movl	$1, %ebx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	lua_pushboolean
.LBB5_5:                                # %pushresult.exit
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	f_flush, .Lfunc_end5-f_flush
	.cfi_endproc

	.p2align	4, 0x90
	.type	f_lines,@function
f_lines:                                # @f_lines
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 16
.Lcfi34:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$.L.str.5, %edx
	callq	luaL_checkudata
	cmpq	$0, (%rax)
	jne	.LBB6_2
# BB#1:
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB6_2:                                # %tofile.exit
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	lua_pushboolean
	movl	$io_readline, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	f_lines, .Lfunc_end6-f_lines
	.cfi_endproc

	.p2align	4, 0x90
	.type	f_read,@function
f_read:                                 # @f_read
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$.L.str.5, %edx
	callq	luaL_checkudata
	movq	%rax, %r14
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	jne	.LBB7_2
# BB#1:
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
	movq	(%r14), %rsi
.LBB7_2:                                # %tofile.exit
	movl	$2, %edx
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	g_read                  # TAILCALL
.Lfunc_end7:
	.size	f_read, .Lfunc_end7-f_read
	.cfi_endproc

	.p2align	4, 0x90
	.type	f_seek,@function
f_seek:                                 # @f_seek
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 48
.Lcfi45:
	.cfi_offset %rbx, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	$1, %esi
	movl	$.L.str.5, %edx
	callq	luaL_checkudata
	movq	%rax, %rbx
	movq	(%rbx), %rbp
	testq	%rbp, %rbp
	jne	.LBB8_2
# BB#1:
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	luaL_error
	movq	(%rbx), %rbp
.LBB8_2:                                # %tofile.exit
	movl	$2, %esi
	movl	$.L.str.26, %edx
	movl	$f_seek.modenames, %ecx
	movq	%r15, %rdi
	callq	luaL_checkoption
	movl	%eax, %ebx
	movl	$3, %r14d
	movl	$3, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	luaL_optinteger
	movslq	%ebx, %rcx
	movl	f_seek.mode(,%rcx,4), %edx
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	fseek
	testl	%eax, %eax
	je	.LBB8_4
# BB#3:
	callq	__errno_location
	movl	(%rax), %ebx
	movq	%r15, %rdi
	callq	lua_pushnil
	movl	%ebx, %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	callq	lua_pushfstring
	movslq	%ebx, %rsi
	movq	%r15, %rdi
	callq	lua_pushinteger
	jmp	.LBB8_5
.LBB8_4:
	movq	%rbp, %rdi
	callq	ftell
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	lua_pushinteger
	movl	$1, %r14d
.LBB8_5:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	f_seek, .Lfunc_end8-f_seek
	.cfi_endproc

	.p2align	4, 0x90
	.type	f_setvbuf,@function
f_setvbuf:                              # @f_setvbuf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 48
.Lcfi54:
	.cfi_offset %rbx, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	$1, %esi
	movl	$.L.str.5, %edx
	callq	luaL_checkudata
	movq	%rax, %rbx
	movq	(%rbx), %rbp
	testq	%rbp, %rbp
	jne	.LBB9_2
# BB#1:
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	luaL_error
	movq	(%rbx), %rbp
.LBB9_2:                                # %tofile.exit
	movl	$2, %esi
	xorl	%edx, %edx
	movl	$f_setvbuf.modenames, %ecx
	movq	%r15, %rdi
	callq	luaL_checkoption
	movl	%eax, %ebx
	movl	$3, %r14d
	movl	$3, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r15, %rdi
	callq	luaL_optinteger
	movslq	%ebx, %rcx
	movl	f_setvbuf.mode(,%rcx,4), %edx
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movq	%rax, %rcx
	callq	setvbuf
	movl	%eax, %ebx
	callq	__errno_location
	testl	%ebx, %ebx
	je	.LBB9_3
# BB#4:
	movslq	(%rax), %rbx
	movq	%r15, %rdi
	callq	lua_pushnil
	movl	%ebx, %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	callq	lua_pushfstring
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	lua_pushinteger
	jmp	.LBB9_5
.LBB9_3:
	movl	$1, %r14d
	movl	$1, %esi
	movq	%r15, %rdi
	callq	lua_pushboolean
.LBB9_5:                                # %pushresult.exit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	f_setvbuf, .Lfunc_end9-f_setvbuf
	.cfi_endproc

	.p2align	4, 0x90
	.type	f_write,@function
f_write:                                # @f_write
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 32
.Lcfi61:
	.cfi_offset %rbx, -24
.Lcfi62:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$.L.str.5, %edx
	callq	luaL_checkudata
	movq	%rax, %r14
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	jne	.LBB10_2
# BB#1:
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
	movq	(%r14), %rsi
.LBB10_2:                               # %tofile.exit
	movl	$2, %edx
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	g_write                 # TAILCALL
.Lfunc_end10:
	.size	f_write, .Lfunc_end10-f_write
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_gc,@function
io_gc:                                  # @io_gc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 16
.Lcfi64:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$.L.str.5, %edx
	callq	luaL_checkudata
	cmpq	$0, (%rax)
	je	.LBB11_2
# BB#1:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_getfenv
	movl	$-1, %esi
	movl	$.L.str.17, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_tocfunction
	movq	%rbx, %rdi
	callq	*%rax
.LBB11_2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end11:
	.size	io_gc, .Lfunc_end11-io_gc
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_tostring,@function
io_tostring:                            # @io_tostring
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 16
.Lcfi66:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$.L.str.5, %edx
	callq	luaL_checkudata
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB12_1
# BB#2:
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	lua_pushfstring
	jmp	.LBB12_3
.LBB12_1:
	movl	$.L.str.32, %esi
	movl	$13, %edx
	movq	%rbx, %rdi
	callq	lua_pushlstring
.LBB12_3:
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end12:
	.size	io_tostring, .Lfunc_end12-io_tostring
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_readline,@function
io_readline:                            # @io_readline
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 32
.Lcfi70:
	.cfi_offset %rbx, -32
.Lcfi71:
	.cfi_offset %r14, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$-10003, %esi           # imm = 0xD8ED
	callq	lua_touserdata
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	jne	.LBB13_2
# BB#1:
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	luaL_error
.LBB13_2:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	read_line
	movl	%eax, %ebx
	movq	%rbp, %rdi
	callq	ferror
	testl	%eax, %eax
	je	.LBB13_3
# BB#7:
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rcx, %rdx
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	luaL_error              # TAILCALL
.LBB13_3:
	movl	$1, %ebp
	testl	%ebx, %ebx
	jne	.LBB13_6
# BB#4:
	movl	$-10004, %esi           # imm = 0xD8EC
	movq	%r14, %rdi
	callq	lua_toboolean
	xorl	%ebp, %ebp
	testl	%eax, %eax
	je	.LBB13_6
# BB#5:
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	lua_settop
	movl	$-10003, %esi           # imm = 0xD8ED
	movq	%r14, %rdi
	callq	lua_pushvalue
	movl	$1, %esi
	movq	%r14, %rdi
	callq	lua_getfenv
	movl	$-1, %esi
	movl	$.L.str.17, %edx
	movq	%r14, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	lua_tocfunction
	movq	%r14, %rdi
	callq	*%rax
.LBB13_6:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end13:
	.size	io_readline, .Lfunc_end13-io_readline
	.cfi_endproc

	.p2align	4, 0x90
	.type	read_line,@function
read_line:                              # @read_line
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 40
	subq	$8216, %rsp             # imm = 0x2018
.Lcfi77:
	.cfi_def_cfa_offset 8256
.Lcfi78:
	.cfi_offset %rbx, -40
.Lcfi79:
	.cfi_offset %r12, -32
.Lcfi80:
	.cfi_offset %r14, -24
.Lcfi81:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	callq	luaL_buffinit
	movq	%rbx, %rdi
	callq	luaL_prepbuffer
	movq	%rax, %rbx
	movl	$8192, %esi             # imm = 0x2000
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB14_6
# BB#1:
	movq	%rsp, %r12
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB14_5
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	cmpb	$10, -1(%rbx,%rax)
	je	.LBB14_4
.LBB14_5:                               #   in Loop: Header=BB14_2 Depth=1
	addq	%rax, (%rsp)
	movq	%r12, %rdi
	callq	luaL_prepbuffer
	movq	%rax, %rbx
	movl	$8192, %esi             # imm = 0x2000
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB14_2
.LBB14_6:                               # %._crit_edge
	movq	%rsp, %rdi
	callq	luaL_pushresult
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	lua_objlen
	movq	%rax, %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	setne	%al
	jmp	.LBB14_7
.LBB14_4:
	decq	%rax
	addq	%rax, (%rsp)
	movq	%rsp, %rdi
	callq	luaL_pushresult
	movl	$1, %eax
.LBB14_7:
	addq	$8216, %rsp             # imm = 0x2018
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	read_line, .Lfunc_end14-read_line
	.cfi_endproc

	.p2align	4, 0x90
	.type	g_read,@function
g_read:                                 # @g_read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	subq	$8248, %rsp             # imm = 0x2038
.Lcfi88:
	.cfi_def_cfa_offset 8304
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movq	%rsi, %r12
	movq	%rdi, %rbp
	callq	lua_gettop
	movl	%eax, %ebx
	movq	%r12, %rdi
	callq	clearerr
	cmpl	$1, %ebx
	movq	%r13, 24(%rsp)          # 8-byte Spill
	jne	.LBB15_2
# BB#1:
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	read_line
	movl	%eax, %r14d
	leal	1(%r13), %r13d
	jmp	.LBB15_30
.LBB15_2:                               # %.lr.ph
	leal	19(%rbx), %esi
	movl	$.L.str.21, %edx
	movq	%rbp, %rdi
	callq	luaL_checkstack
	addl	$-2, %ebx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	leaq	32(%rsp), %r15
                                        # kill: %R13D<def> %R13D<kill> %R13<kill>
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB15_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_20 Depth 2
                                        #     Child Loop BB15_6 Depth 2
	movq	%rbp, %rdi
	movl	%r13d, %esi
	callq	lua_type
	cmpl	$3, %eax
	jne	.LBB15_10
# BB#4:                                 #   in Loop: Header=BB15_3 Depth=1
	movq	%rbp, %rdi
	movl	%r13d, %esi
	callq	lua_tointeger
	movq	%rbp, %rbx
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB15_18
# BB#5:                                 #   in Loop: Header=BB15_3 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	luaL_buffinit
	movl	$8192, %ebx             # imm = 0x2000
	.p2align	4, 0x90
.LBB15_6:                               #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rdi
	callq	luaL_prepbuffer
	cmpq	%rbp, %rbx
	cmovaq	%rbp, %rbx
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rcx
	callq	fread
	addq	%rax, 32(%rsp)
	subq	%rax, %rbp
	cmpq	%rbx, %rax
	jne	.LBB15_8
# BB#7:                                 #   in Loop: Header=BB15_6 Depth=2
	testq	%rbp, %rbp
	jne	.LBB15_6
.LBB15_8:                               # %.critedge.i
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	%r15, %rdi
	callq	luaL_pushresult
	testq	%rbp, %rbp
	je	.LBB15_26
# BB#9:                                 #   in Loop: Header=BB15_3 Depth=1
	movl	$-1, %esi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	callq	lua_objlen
	testq	%rax, %rax
	setne	%al
	jmp	.LBB15_27
	.p2align	4, 0x90
.LBB15_10:                              #   in Loop: Header=BB15_3 Depth=1
	xorl	%edx, %edx
	movq	%rbp, %rdi
	movl	%r13d, %esi
	callq	lua_tolstring
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB15_12
# BB#11:                                #   in Loop: Header=BB15_3 Depth=1
	cmpb	$42, (%rbx)
	je	.LBB15_13
.LBB15_12:                              #   in Loop: Header=BB15_3 Depth=1
	movl	$.L.str.22, %edx
	movq	%rbp, %rdi
	movl	%r13d, %esi
	callq	luaL_argerror
.LBB15_13:                              #   in Loop: Header=BB15_3 Depth=1
	movsbl	1(%rbx), %eax
	cmpl	$97, %eax
	je	.LBB15_19
# BB#14:                                #   in Loop: Header=BB15_3 Depth=1
	cmpl	$108, %eax
	je	.LBB15_25
# BB#15:                                #   in Loop: Header=BB15_3 Depth=1
	cmpl	$110, %eax
	jne	.LBB15_35
# BB#16:                                #   in Loop: Header=BB15_3 Depth=1
	xorl	%r14d, %r14d
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r15, %rdx
	callq	fscanf
	cmpl	$1, %eax
	jne	.LBB15_28
# BB#17:                                #   in Loop: Header=BB15_3 Depth=1
	movsd	32(%rsp), %xmm0         # xmm0 = mem[0],zero
	movq	%rbp, %rdi
	callq	lua_pushnumber
	movl	$1, %r14d
	jmp	.LBB15_28
.LBB15_18:                              #   in Loop: Header=BB15_3 Depth=1
	movq	%r12, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movl	%ebp, %edi
	movq	%r12, %rsi
	callq	ungetc
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushlstring
	xorl	%r14d, %r14d
	cmpl	$-1, %ebp
	setne	%r14b
	movq	%rbx, %rbp
	jmp	.LBB15_28
.LBB15_19:                              #   in Loop: Header=BB15_3 Depth=1
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	luaL_buffinit
	movq	$-1, %rbx
	movl	$8192, %ebp             # imm = 0x2000
	.p2align	4, 0x90
.LBB15_20:                              #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rdi
	callq	luaL_prepbuffer
	cmpq	%rbx, %rbp
	cmovaq	%rbx, %rbp
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rbp, %rdx
	movq	%r12, %rcx
	callq	fread
	addq	%rax, 32(%rsp)
	subq	%rax, %rbx
	cmpq	%rbp, %rax
	jne	.LBB15_22
# BB#21:                                #   in Loop: Header=BB15_20 Depth=2
	testq	%rbx, %rbx
	jne	.LBB15_20
.LBB15_22:                              # %.critedge.i64
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	%r15, %rdi
	callq	luaL_pushresult
	testq	%rbx, %rbx
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB15_24
# BB#23:                                #   in Loop: Header=BB15_3 Depth=1
	movl	$-1, %esi
	movq	%rbp, %rdi
	callq	lua_objlen
.LBB15_24:                              # %read_chars.exit65
                                        #   in Loop: Header=BB15_3 Depth=1
	movl	$1, %r14d
	jmp	.LBB15_28
.LBB15_25:                              #   in Loop: Header=BB15_3 Depth=1
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	read_line
	movl	%eax, %r14d
	jmp	.LBB15_28
.LBB15_26:                              #   in Loop: Header=BB15_3 Depth=1
	movb	$1, %al
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB15_27:                              # %.thread
                                        #   in Loop: Header=BB15_3 Depth=1
	movzbl	%al, %r14d
.LBB15_28:                              # %.thread
                                        #   in Loop: Header=BB15_3 Depth=1
	incl	%r13d
	testl	%r14d, %r14d
	je	.LBB15_30
# BB#29:                                # %.thread
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	leal	-1(%rax), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jne	.LBB15_3
.LBB15_30:                              # %.loopexit
	movq	%r12, %rdi
	callq	ferror
	testl	%eax, %eax
	je	.LBB15_32
# BB#31:
	callq	__errno_location
	movl	(%rax), %ebx
	movq	%rbp, %rdi
	callq	lua_pushnil
	movl	%ebx, %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	lua_pushfstring
	movslq	%ebx, %rsi
	movq	%rbp, %rdi
	callq	lua_pushinteger
	movl	$3, %r13d
	jmp	.LBB15_36
.LBB15_32:
	testl	%r14d, %r14d
	jne	.LBB15_34
# BB#33:
	movl	$-2, %esi
	movq	%rbp, %rdi
	callq	lua_settop
	movq	%rbp, %rdi
	callq	lua_pushnil
.LBB15_34:
	subl	24(%rsp), %r13d         # 4-byte Folded Reload
	jmp	.LBB15_36
.LBB15_35:
	movl	$.L.str.23, %edx
	movq	%rbp, %rdi
	movl	%r13d, %esi
	callq	luaL_argerror
	movl	%eax, %r13d
.LBB15_36:
	movl	%r13d, %eax
	addq	$8248, %rsp             # imm = 0x2038
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	g_read, .Lfunc_end15-g_read
	.cfi_endproc

	.p2align	4, 0x90
	.type	g_write,@function
g_write:                                # @g_write
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi99:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 64
.Lcfi102:
	.cfi_offset %rbx, -56
.Lcfi103:
	.cfi_offset %r12, -48
.Lcfi104:
	.cfi_offset %r13, -40
.Lcfi105:
	.cfi_offset %r14, -32
.Lcfi106:
	.cfi_offset %r15, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %r12
	callq	lua_gettop
	cmpl	$1, %eax
	je	.LBB16_11
# BB#1:                                 # %.lr.ph
	movl	$1, %r13d
	movl	$1, %ebx
	subl	%eax, %ebx
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB16_2:                               # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	lua_type
	cmpl	$3, %eax
	jne	.LBB16_5
# BB#3:                                 #   in Loop: Header=BB16_2 Depth=1
	testl	%r13d, %r13d
	je	.LBB16_8
# BB#4:                                 #   in Loop: Header=BB16_2 Depth=1
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	lua_tonumber
	movl	$.L.str.31, %esi
	movb	$1, %al
	movq	%r14, %rdi
	callq	fprintf
	testl	%eax, %eax
	setg	%al
	jmp	.LBB16_9
	.p2align	4, 0x90
.LBB16_5:                               #   in Loop: Header=BB16_2 Depth=1
	movq	%r12, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	luaL_checklstring
	testl	%r13d, %r13d
	je	.LBB16_8
# BB#6:                                 #   in Loop: Header=BB16_2 Depth=1
	movq	(%rsp), %rdx
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%r14, %rcx
	callq	fwrite
	cmpq	(%rsp), %rax
	sete	%al
	jmp	.LBB16_9
	.p2align	4, 0x90
.LBB16_8:                               #   in Loop: Header=BB16_2 Depth=1
	xorl	%eax, %eax
.LBB16_9:                               #   in Loop: Header=BB16_2 Depth=1
	movzbl	%al, %r13d
	incl	%ebp
	incl	%ebx
	jne	.LBB16_2
# BB#10:                                # %._crit_edge
	callq	__errno_location
	testl	%r13d, %r13d
	je	.LBB16_12
.LBB16_11:                              # %._crit_edge.thread
	movl	$1, %ebp
	movl	$1, %esi
	movq	%r12, %rdi
	callq	lua_pushboolean
	jmp	.LBB16_13
.LBB16_12:
	movslq	(%rax), %rbp
	movq	%r12, %rdi
	callq	lua_pushnil
	movl	%ebp, %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rdx
	callq	lua_pushfstring
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	lua_pushinteger
	movl	$3, %ebp
.LBB16_13:                              # %pushresult.exit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	g_write, .Lfunc_end16-g_write
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_flush,@function
io_flush:                               # @io_flush
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi110:
	.cfi_def_cfa_offset 32
.Lcfi111:
	.cfi_offset %rbx, -24
.Lcfi112:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$-10001, %esi           # imm = 0xD8EF
	movl	$2, %edx
	callq	lua_rawgeti
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	lua_touserdata
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB17_2
# BB#1:
	movl	$.L.str.39, %esi
	movl	$.L.str.36, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	luaL_error
.LBB17_2:                               # %getiofile.exit
	movq	%rbx, %rdi
	callq	fflush
	movl	%eax, %ebx
	callq	__errno_location
	testl	%ebx, %ebx
	je	.LBB17_3
# BB#4:
	movslq	(%rax), %rbx
	movq	%r14, %rdi
	callq	lua_pushnil
	movl	%ebx, %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rcx, %rdx
	callq	lua_pushfstring
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	lua_pushinteger
	movl	$3, %ebx
	jmp	.LBB17_5
.LBB17_3:
	movl	$1, %ebx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	lua_pushboolean
.LBB17_5:                               # %pushresult.exit
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end17:
	.size	io_flush, .Lfunc_end17-io_flush
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_input,@function
io_input:                               # @io_input
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi113:
	.cfi_def_cfa_offset 16
	movl	$1, %esi
	movl	$.L.str.40, %edx
	callq	g_iofile
	movl	$1, %eax
	popq	%rcx
	retq
.Lfunc_end18:
	.size	io_input, .Lfunc_end18-io_input
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_lines,@function
io_lines:                               # @io_lines
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 32
.Lcfi117:
	.cfi_offset %rbx, -32
.Lcfi118:
	.cfi_offset %r14, -24
.Lcfi119:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	lua_type
	testl	%eax, %eax
	jle	.LBB19_1
# BB#4:
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	luaL_checklstring
	movq	%rax, %r14
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	lua_newuserdata
	movq	%rax, %r15
	movq	$0, (%r15)
	movl	$-10000, %esi           # imm = 0xD8F0
	movl	$.L.str.5, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setmetatable
	movl	$.L.str.40, %esi
	movq	%r14, %rdi
	callq	fopen
	movq	%rax, (%r15)
	testq	%rax, %rax
	jne	.LBB19_6
# BB#5:
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_pushfstring
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_tolstring
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	callq	luaL_argerror
.LBB19_6:
	movq	%rbx, %rdi
	callq	lua_gettop
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	lua_pushvalue
	movl	$1, %esi
	jmp	.LBB19_7
.LBB19_1:
	movl	$-10001, %esi           # imm = 0xD8EF
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_rawgeti
	movl	$1, %esi
	movl	$.L.str.5, %edx
	movq	%rbx, %rdi
	callq	luaL_checkudata
	cmpq	$0, (%rax)
	jne	.LBB19_3
# BB#2:
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB19_3:                               # %f_lines.exit
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	xorl	%esi, %esi
.LBB19_7:
	movq	%rbx, %rdi
	callq	lua_pushboolean
	movl	$io_readline, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$1, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end19:
	.size	io_lines, .Lfunc_end19-io_lines
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_open,@function
io_open:                                # @io_open
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi120:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi121:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 48
.Lcfi125:
	.cfi_offset %rbx, -48
.Lcfi126:
	.cfi_offset %r12, -40
.Lcfi127:
	.cfi_offset %r14, -32
.Lcfi128:
	.cfi_offset %r15, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %r15d
	movl	$1, %esi
	xorl	%edx, %edx
	callq	luaL_checklstring
	movq	%rax, %r14
	movl	$2, %esi
	movl	$.L.str.40, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	luaL_optlstring
	movq	%rax, %r12
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	lua_newuserdata
	movq	%rax, %rbp
	movq	$0, (%rbp)
	movl	$-10000, %esi           # imm = 0xD8F0
	movl	$.L.str.5, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setmetatable
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	fopen
	movq	%rax, (%rbp)
	testq	%rax, %rax
	jne	.LBB20_5
# BB#1:
	callq	__errno_location
	movl	(%rax), %eax
	movslq	%eax, %rbp
	movq	%rbx, %rdi
	callq	lua_pushnil
	movl	%ebp, %edi
	callq	strerror
	movq	%rax, %rcx
	testq	%r14, %r14
	je	.LBB20_3
# BB#2:
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_pushfstring
	jmp	.LBB20_4
.LBB20_3:
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	lua_pushfstring
.LBB20_4:                               # %pushresult.exit
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	lua_pushinteger
	movl	$3, %r15d
.LBB20_5:
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	io_open, .Lfunc_end20-io_open
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_output,@function
io_output:                              # @io_output
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi130:
	.cfi_def_cfa_offset 16
	movl	$2, %esi
	movl	$.L.str.41, %edx
	callq	g_iofile
	movl	$1, %eax
	popq	%rcx
	retq
.Lfunc_end21:
	.size	io_output, .Lfunc_end21-io_output
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_popen,@function
io_popen:                               # @io_popen
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi133:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 48
.Lcfi136:
	.cfi_offset %rbx, -48
.Lcfi137:
	.cfi_offset %r12, -40
.Lcfi138:
	.cfi_offset %r14, -32
.Lcfi139:
	.cfi_offset %r15, -24
.Lcfi140:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %r15d
	movl	$1, %esi
	xorl	%edx, %edx
	callq	luaL_checklstring
	movq	%rax, %r14
	movl	$2, %esi
	movl	$.L.str.40, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	luaL_optlstring
	movq	%rax, %r12
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	lua_newuserdata
	movq	%rax, %rbp
	movq	$0, (%rbp)
	movl	$-10000, %esi           # imm = 0xD8F0
	movl	$.L.str.5, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setmetatable
	xorl	%edi, %edi
	callq	fflush
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	popen
	movq	%rax, (%rbp)
	testq	%rax, %rax
	jne	.LBB22_5
# BB#1:
	callq	__errno_location
	movl	(%rax), %eax
	movslq	%eax, %rbp
	movq	%rbx, %rdi
	callq	lua_pushnil
	movl	%ebp, %edi
	callq	strerror
	movq	%rax, %rcx
	testq	%r14, %r14
	je	.LBB22_3
# BB#2:
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_pushfstring
	jmp	.LBB22_4
.LBB22_3:
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	lua_pushfstring
.LBB22_4:                               # %pushresult.exit
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	lua_pushinteger
	movl	$3, %r15d
.LBB22_5:
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	io_popen, .Lfunc_end22-io_popen
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_read,@function
io_read:                                # @io_read
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi143:
	.cfi_def_cfa_offset 32
.Lcfi144:
	.cfi_offset %rbx, -24
.Lcfi145:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$-10001, %esi           # imm = 0xD8EF
	movl	$1, %edx
	callq	lua_rawgeti
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_touserdata
	movq	(%rax), %r14
	testq	%r14, %r14
	jne	.LBB23_2
# BB#1:
	movl	$.L.str.39, %esi
	movl	$.L.str.34, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB23_2:                               # %getiofile.exit
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	g_read                  # TAILCALL
.Lfunc_end23:
	.size	io_read, .Lfunc_end23-io_read
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_tmpfile,@function
io_tmpfile:                             # @io_tmpfile
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi148:
	.cfi_def_cfa_offset 32
.Lcfi149:
	.cfi_offset %rbx, -24
.Lcfi150:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$8, %esi
	callq	lua_newuserdata
	movq	%rax, %rbx
	movq	$0, (%rbx)
	movl	$-10000, %esi           # imm = 0xD8F0
	movl	$.L.str.5, %edx
	movq	%r14, %rdi
	callq	lua_getfield
	movl	$-2, %esi
	movq	%r14, %rdi
	callq	lua_setmetatable
	callq	tmpfile
	movq	%rax, %rcx
	movq	%rcx, (%rbx)
	movl	$1, %eax
	testq	%rcx, %rcx
	jne	.LBB24_2
# BB#1:
	callq	__errno_location
	movl	(%rax), %ebx
	movq	%r14, %rdi
	callq	lua_pushnil
	movl	%ebx, %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rcx, %rdx
	callq	lua_pushfstring
	movslq	%ebx, %rsi
	movq	%r14, %rdi
	callq	lua_pushinteger
	movl	$3, %eax
.LBB24_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end24:
	.size	io_tmpfile, .Lfunc_end24-io_tmpfile
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_type,@function
io_type:                                # @io_type
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi151:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi153:
	.cfi_def_cfa_offset 32
.Lcfi154:
	.cfi_offset %rbx, -24
.Lcfi155:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checkany
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_touserdata
	movq	%rax, %r14
	movl	$-10000, %esi           # imm = 0xD8F0
	movl	$.L.str.5, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	testq	%r14, %r14
	je	.LBB25_3
# BB#1:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_getmetatable
	testl	%eax, %eax
	je	.LBB25_3
# BB#2:
	movl	$-2, %esi
	movl	$-1, %edx
	movq	%rbx, %rdi
	callq	lua_rawequal
	testl	%eax, %eax
	je	.LBB25_3
# BB#4:
	cmpq	$0, (%r14)
	je	.LBB25_5
# BB#6:
	movl	$.L.str.43, %esi
	movl	$4, %edx
	jmp	.LBB25_7
.LBB25_3:
	movq	%rbx, %rdi
	callq	lua_pushnil
	jmp	.LBB25_8
.LBB25_5:
	movl	$.L.str.42, %esi
	movl	$11, %edx
.LBB25_7:
	movq	%rbx, %rdi
	callq	lua_pushlstring
.LBB25_8:
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end25:
	.size	io_type, .Lfunc_end25-io_type
	.cfi_endproc

	.p2align	4, 0x90
	.type	io_write,@function
io_write:                               # @io_write
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi156:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi158:
	.cfi_def_cfa_offset 32
.Lcfi159:
	.cfi_offset %rbx, -24
.Lcfi160:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$-10001, %esi           # imm = 0xD8EF
	movl	$2, %edx
	callq	lua_rawgeti
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_touserdata
	movq	(%rax), %r14
	testq	%r14, %r14
	jne	.LBB26_2
# BB#1:
	movl	$.L.str.39, %esi
	movl	$.L.str.36, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB26_2:                               # %getiofile.exit
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	g_write                 # TAILCALL
.Lfunc_end26:
	.size	io_write, .Lfunc_end26-io_write
	.cfi_endproc

	.p2align	4, 0x90
	.type	g_iofile,@function
g_iofile:                               # @g_iofile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi163:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi164:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 48
.Lcfi166:
	.cfi_offset %rbx, -48
.Lcfi167:
	.cfi_offset %r12, -40
.Lcfi168:
	.cfi_offset %r14, -32
.Lcfi169:
	.cfi_offset %r15, -24
.Lcfi170:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	lua_type
	testl	%eax, %eax
	jle	.LBB27_8
# BB#1:
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_tolstring
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB27_4
# BB#2:
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	lua_newuserdata
	movq	%rax, %rbp
	movq	$0, (%rbp)
	movl	$-10000, %esi           # imm = 0xD8F0
	movl	$.L.str.5, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setmetatable
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	fopen
	movq	%rax, (%rbp)
	testq	%rax, %rax
	jne	.LBB27_7
# BB#3:
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r12, %rdx
	callq	lua_pushfstring
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_tolstring
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	callq	luaL_argerror
	jmp	.LBB27_7
.LBB27_4:
	movl	$1, %esi
	movl	$.L.str.5, %edx
	movq	%rbx, %rdi
	callq	luaL_checkudata
	cmpq	$0, (%rax)
	jne	.LBB27_6
# BB#5:
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB27_6:                               # %tofile.exit
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
.LBB27_7:
	movl	$-10001, %esi           # imm = 0xD8EF
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	lua_rawseti
.LBB27_8:
	movl	$-10001, %esi           # imm = 0xD8EF
	movq	%rbx, %rdi
	movl	%r14d, %edx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	lua_rawgeti             # TAILCALL
.Lfunc_end27:
	.size	g_iofile, .Lfunc_end27-g_iofile
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"io"
	.size	.L.str, 3

	.type	iolib,@object           # @iolib
	.section	.rodata,"a",@progbits
	.p2align	4
iolib:
	.quad	.L.str.7
	.quad	io_close
	.quad	.L.str.8
	.quad	io_flush
	.quad	.L.str.34
	.quad	io_input
	.quad	.L.str.9
	.quad	io_lines
	.quad	.L.str.35
	.quad	io_open
	.quad	.L.str.36
	.quad	io_output
	.quad	.L.str.4
	.quad	io_popen
	.quad	.L.str.10
	.quad	io_read
	.quad	.L.str.37
	.quad	io_tmpfile
	.quad	.L.str.38
	.quad	io_type
	.quad	.L.str.13
	.quad	io_write
	.zero	16
	.size	iolib, 192

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"stdin"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"stdout"
	.size	.L.str.2, 7

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"stderr"
	.size	.L.str.3, 7

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"popen"
	.size	.L.str.4, 6

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"FILE*"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"__index"
	.size	.L.str.6, 8

	.type	flib,@object            # @flib
	.section	.rodata,"a",@progbits
	.p2align	4
flib:
	.quad	.L.str.7
	.quad	io_close
	.quad	.L.str.8
	.quad	f_flush
	.quad	.L.str.9
	.quad	f_lines
	.quad	.L.str.10
	.quad	f_read
	.quad	.L.str.11
	.quad	f_seek
	.quad	.L.str.12
	.quad	f_setvbuf
	.quad	.L.str.13
	.quad	f_write
	.quad	.L.str.14
	.quad	io_gc
	.quad	.L.str.15
	.quad	io_tostring
	.zero	16
	.size	flib, 160

	.type	.L.str.7,@object        # @.str.7
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.7:
	.asciz	"close"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"flush"
	.size	.L.str.8, 6

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"lines"
	.size	.L.str.9, 6

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"read"
	.size	.L.str.10, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"seek"
	.size	.L.str.11, 5

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"setvbuf"
	.size	.L.str.12, 8

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"write"
	.size	.L.str.13, 6

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"__gc"
	.size	.L.str.14, 5

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"__tostring"
	.size	.L.str.15, 11

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"attempt to use a closed file"
	.size	.L.str.16, 29

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"__close"
	.size	.L.str.17, 8

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"%s: %s"
	.size	.L.str.18, 7

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"%s"
	.size	.L.str.19, 3

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"file is already closed"
	.size	.L.str.20, 23

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"too many arguments"
	.size	.L.str.21, 19

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"invalid option"
	.size	.L.str.22, 15

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"invalid format"
	.size	.L.str.23, 15

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"%lf"
	.size	.L.str.24, 4

	.type	f_seek.mode,@object     # @f_seek.mode
	.section	.rodata,"a",@progbits
	.p2align	2
f_seek.mode:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.size	f_seek.mode, 12

	.type	f_seek.modenames,@object # @f_seek.modenames
	.p2align	4
f_seek.modenames:
	.quad	.L.str.25
	.quad	.L.str.26
	.quad	.L.str.27
	.quad	0
	.size	f_seek.modenames, 32

	.type	.L.str.25,@object       # @.str.25
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.25:
	.asciz	"set"
	.size	.L.str.25, 4

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"cur"
	.size	.L.str.26, 4

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"end"
	.size	.L.str.27, 4

	.type	f_setvbuf.mode,@object  # @f_setvbuf.mode
	.section	.rodata,"a",@progbits
	.p2align	2
f_setvbuf.mode:
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.size	f_setvbuf.mode, 12

	.type	f_setvbuf.modenames,@object # @f_setvbuf.modenames
	.p2align	4
f_setvbuf.modenames:
	.quad	.L.str.28
	.quad	.L.str.29
	.quad	.L.str.30
	.quad	0
	.size	f_setvbuf.modenames, 32

	.type	.L.str.28,@object       # @.str.28
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.28:
	.asciz	"no"
	.size	.L.str.28, 3

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"full"
	.size	.L.str.29, 5

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"line"
	.size	.L.str.30, 5

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"%.14g"
	.size	.L.str.31, 6

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"file (closed)"
	.size	.L.str.32, 14

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"file (%p)"
	.size	.L.str.33, 10

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"input"
	.size	.L.str.34, 6

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"open"
	.size	.L.str.35, 5

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"output"
	.size	.L.str.36, 7

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"tmpfile"
	.size	.L.str.37, 8

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"type"
	.size	.L.str.38, 5

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"standard %s file is closed"
	.size	.L.str.39, 27

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"r"
	.size	.L.str.40, 2

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"w"
	.size	.L.str.41, 2

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"closed file"
	.size	.L.str.42, 12

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"file"
	.size	.L.str.43, 5

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"cannot close standard file"
	.size	.L.str.44, 27


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
