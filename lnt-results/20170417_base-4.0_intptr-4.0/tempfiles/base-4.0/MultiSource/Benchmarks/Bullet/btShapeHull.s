	.text
	.file	"btShapeHull.bc"
	.globl	_ZN11btShapeHullC2EPK13btConvexShape
	.p2align	4, 0x90
	.type	_ZN11btShapeHullC2EPK13btConvexShape,@function
_ZN11btShapeHullC2EPK13btConvexShape:   # @_ZN11btShapeHullC2EPK13btConvexShape
	.cfi_startproc
# BB#0:
	movq	%rsi, 72(%rdi)
	movb	$1, 24(%rdi)
	movq	$0, 16(%rdi)
	movl	$0, 4(%rdi)
	movl	$0, 8(%rdi)
	movb	$1, 56(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 36(%rdi)
	movl	$0, 64(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN11btShapeHullC2EPK13btConvexShape, .Lfunc_end0-_ZN11btShapeHullC2EPK13btConvexShape
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN11btShapeHullD2Ev
	.p2align	4, 0x90
	.type	_ZN11btShapeHullD2Ev,@function
_ZN11btShapeHullD2Ev:                   # @_ZN11btShapeHullD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#1:
	cmpb	$0, 56(%rbx)
	je	.LBB2_3
# BB#2:
.Ltmp0:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp1:
.LBB2_3:                                # %.noexc
	movq	$0, 48(%rbx)
.LBB2_4:
	movb	$1, 56(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 36(%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_5
# BB#6:
	leaq	24(%rbx), %r15
	cmpb	$0, (%r15)
	je	.LBB2_7
# BB#8:
.Ltmp2:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp3:
# BB#9:                                 # %..noexc4_crit_edge
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	sete	%al
	jmp	.LBB2_10
.LBB2_5:                                # %.thread
	movb	$1, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 4(%rbx)
	leaq	8(%rbx), %r14
	jmp	.LBB2_12
.LBB2_7:
	movb	$1, %al
	xorl	%edi, %edi
.LBB2_10:
	movb	$1, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 4(%rbx)
	leaq	8(%rbx), %r14
	testb	%al, %al
	jne	.LBB2_12
# BB#11:
	cmpb	$0, 56(%rbx)
	je	.LBB2_12
# BB#13:
	leaq	16(%rbx), %r12
.Ltmp7:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp8:
# BB#14:
	movq	16(%rbx), %rdi
	movb	$1, 56(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 36(%rbx)
	testq	%rdi, %rdi
	je	.LBB2_18
# BB#15:
	cmpb	$0, (%r15)
	je	.LBB2_17
# BB#16:
	callq	_Z21btAlignedFreeInternalPv
.LBB2_17:
	movq	$0, (%r12)
	jmp	.LBB2_18
.LBB2_12:                               # %.thread21
	movb	$1, 56(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 36(%rbx)
.LBB2_18:                               # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit
	movb	$1, 24(%rbx)
	movq	$0, 16(%rbx)
	movl	$0, 4(%rbx)
	movl	$0, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB2_24:
.Ltmp9:
	movq	%rax, %r14
	jmp	.LBB2_25
.LBB2_19:
.Ltmp4:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_23
# BB#20:
	cmpb	$0, 56(%rbx)
	je	.LBB2_22
# BB#21:
.Ltmp5:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp6:
.LBB2_22:                               # %.noexc8
	movq	$0, 48(%rbx)
.LBB2_23:                               # %_ZN20btAlignedObjectArrayIjED2Ev.exit9
	movb	$1, 56(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 36(%rbx)
	leaq	16(%rbx), %r12
	leaq	24(%rbx), %r15
.LBB2_25:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB2_29
# BB#26:
	cmpb	$0, (%r15)
	je	.LBB2_28
# BB#27:
.Ltmp10:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp11:
.LBB2_28:                               # %.noexc11
	movq	$0, (%r12)
.LBB2_29:
	movb	$1, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 4(%rbx)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_30:
.Ltmp12:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN11btShapeHullD2Ev, .Lfunc_end2-_ZN11btShapeHullD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp5-.Ltmp8           #   Call between .Ltmp8 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp11-.Ltmp5          #   Call between .Ltmp5 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	1                       #   On action: 1
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Lfunc_end2-.Ltmp11     #   Call between .Ltmp11 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN11btShapeHull9buildHullEf
	.p2align	4, 0x90
	.type	_ZN11btShapeHull9buildHullEf,@function
_ZN11btShapeHull9buildHullEf:           # @_ZN11btShapeHull9buildHullEf
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$1192, %rsp             # imm = 0x4A8
.Lcfi15:
	.cfi_def_cfa_offset 1248
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	72(%r12), %rdi
	movq	(%rdi), %rax
	callq	*128(%rax)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jle	.LBB3_4
# BB#1:                                 # %.lr.ph109
	xorl	%ebx, %ebx
	movl	$_ZL18btUnitSpherePoints+672, %ebp
	leaq	192(%rsp), %r14
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	72(%r12), %rdi
	movq	(%rdi), %rax
	movl	%ebx, %esi
	movq	%r14, %rdx
	callq	*136(%rax)
	movupd	192(%rsp), %xmm0
	movupd	%xmm0, (%rbp)
	incl	%ebx
	addq	$16, %rbp
	cmpl	%ebx, %r15d
	jne	.LBB3_2
# BB#3:                                 # %.loopexit98
	addl	$42, %r15d
	testl	%r15d, %r15d
	jg	.LBB3_5
	jmp	.LBB3_7
.LBB3_4:                                # %.loopexit98.thread
	leaq	192(%rsp), %r14
	movl	$42, %r15d
.LBB3_5:                                # %.lr.ph105.preheader
	movl	%r15d, %r13d
	leaq	200(%rsp), %rbp
	movl	$_ZL18btUnitSpherePoints, %ebx
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph105
                                        # =>This Inner Loop Header: Depth=1
	movq	72(%r12), %rdi
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	callq	*96(%rax)
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movapd	%xmm0, -8(%rbp)
	addq	$16, %rbx
	addq	$16, %rbp
	decq	%r13
	jne	.LBB3_6
.LBB3_7:                                # %._crit_edge106
	movl	$981668463, 180(%rsp)   # imm = 0x3A83126F
	movl	$4096, 184(%rsp)        # imm = 0x1000
	movl	$4096, 188(%rsp)        # imm = 0x1000
	movl	$1, 160(%rsp)
	movl	%r15d, 164(%rsp)
	movq	%r14, 168(%rsp)
	movl	$16, 176(%rsp)
	movb	$1, 104(%rsp)
	movq	$0, 96(%rsp)
	movl	$0, 84(%rsp)
	movl	$0, 88(%rsp)
	movb	$1, 136(%rsp)
	movq	$0, 128(%rsp)
	movq	$0, 116(%rsp)
	movb	$1, 32(%rsp)
	movq	$0, 24(%rsp)
	movl	$0, 12(%rsp)
	movl	$0, 16(%rsp)
	movb	$1, 72(%rsp)
	movq	$0, 64(%rsp)
	movl	$0, 52(%rsp)
	movl	$0, 56(%rsp)
	movb	$1, (%rsp)
	movl	$0, 4(%rsp)
	movl	$0, 40(%rsp)
	movl	$0, 44(%rsp)
.Ltmp13:
	leaq	80(%rsp), %rdi
	leaq	160(%rsp), %rsi
	movq	%rsp, %rdx
	callq	_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult
.Ltmp14:
# BB#8:
	cmpl	$1, %eax
	jne	.LBB3_10
# BB#9:
	xorl	%ebx, %ebx
	jmp	.LBB3_79
.LBB3_10:
	movl	4(%rsp), %r15d
	movl	4(%r12), %r13d
	cmpl	%r15d, %r13d
	movl	%r15d, %eax
	jge	.LBB3_37
# BB#11:
	movslq	%r15d, %rbp
	cmpl	%r15d, 8(%r12)
	jge	.LBB3_15
# BB#12:
	testl	%r15d, %r15d
	je	.LBB3_16
# BB#13:
	movq	%rbp, %rdi
	shlq	$4, %rdi
.Ltmp15:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r14
.Ltmp16:
# BB#14:                                # %.noexc54
	movl	4(%r12), %eax
	jmp	.LBB3_17
.LBB3_15:                               # %..lr.ph.i_crit_edge
	leaq	16(%r12), %rbx
	jmp	.LBB3_30
.LBB3_16:
	xorl	%r14d, %r14d
	movl	%r13d, %eax
.LBB3_17:                               # %_ZN20btAlignedObjectArrayI9btVector3E8allocateEi.exit.i.i
	leaq	16(%r12), %rbx
	testl	%eax, %eax
	jle	.LBB3_25
# BB#18:                                # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB3_21
# BB#19:                                # %.prol.preheader
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_20:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movupd	(%rdx,%rdi), %xmm0
	movupd	%xmm0, (%r14,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB3_20
	jmp	.LBB3_22
.LBB3_21:
	xorl	%ecx, %ecx
.LBB3_22:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB3_25
# BB#23:                                # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB3_24:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%r14,%rcx)
	movq	(%rbx), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%r14,%rcx)
	movq	(%rbx), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%r14,%rcx)
	movq	(%rbx), %rdx
	movupd	(%rdx,%rcx), %xmm0
	movupd	%xmm0, (%r14,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB3_24
.LBB3_25:                               # %_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_.exit.i.i
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB3_29
# BB#26:
	cmpb	$0, 24(%r12)
	je	.LBB3_28
# BB#27:
.Ltmp17:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp18:
.LBB3_28:                               # %.noexc55
	movq	$0, (%rbx)
.LBB3_29:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.preheader.i
	movb	$1, 24(%r12)
	movq	%r14, 16(%r12)
	movl	%r15d, 8(%r12)
.LBB3_30:                               # %.lr.ph.i
	movslq	%r13d, %rax
	movl	%ebp, %edx
	subl	%r13d, %edx
	leaq	-1(%rbp), %rcx
	subq	%rax, %rcx
	andq	$3, %rdx
	je	.LBB3_33
# BB#31:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol.preheader
	movq	%rax, %rsi
	shlq	$4, %rsi
	negq	%rdx
	.p2align	4, 0x90
.LBB3_32:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movupd	144(%rsp), %xmm0
	movupd	%xmm0, (%rdi,%rsi)
	incq	%rax
	addq	$16, %rsi
	incq	%rdx
	jne	.LBB3_32
.LBB3_33:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB3_36
# BB#34:                                # %.lr.ph.i.new
	subq	%rax, %rbp
	shlq	$4, %rax
	addq	$48, %rax
	.p2align	4, 0x90
.LBB3_35:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movups	144(%rsp), %xmm0
	movups	%xmm0, -48(%rcx,%rax)
	movq	(%rbx), %rcx
	movups	144(%rsp), %xmm0
	movups	%xmm0, -32(%rcx,%rax)
	movq	(%rbx), %rcx
	movups	144(%rsp), %xmm0
	movups	%xmm0, -16(%rcx,%rax)
	movq	(%rbx), %rcx
	movupd	144(%rsp), %xmm0
	movupd	%xmm0, (%rcx,%rax)
	addq	$64, %rax
	addq	$-4, %rbp
	jne	.LBB3_35
.LBB3_36:                               # %.loopexit96.loopexit
	movl	4(%rsp), %eax
.LBB3_37:                               # %.loopexit96
	movl	%r15d, 4(%r12)
	testl	%eax, %eax
	jle	.LBB3_40
# BB#38:                                # %.lr.ph102
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_39:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax
	movq	16(%r12), %rdx
	movupd	(%rax,%rcx), %xmm0
	movupd	%xmm0, (%rdx,%rcx)
	incq	%rsi
	movslq	4(%rsp), %rax
	addq	$16, %rcx
	cmpq	%rax, %rsi
	jl	.LBB3_39
.LBB3_40:                               # %._crit_edge103
	movl	44(%rsp), %r13d
	movl	%r13d, 64(%r12)
	movl	36(%r12), %ebp
	cmpl	%r13d, %ebp
	movl	%r13d, %eax
	jge	.LBB3_75
# BB#41:
	movslq	%r13d, %r14
	cmpl	%r13d, 40(%r12)
	jge	.LBB3_45
# BB#42:
	testl	%r13d, %r13d
	je	.LBB3_46
# BB#43:
	leaq	(,%r14,4), %rdi
.Ltmp20:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
.Ltmp21:
# BB#44:                                # %.noexc69
	movl	36(%r12), %ecx
	jmp	.LBB3_47
.LBB3_45:                               # %..lr.ph.i65_crit_edge
	movq	48(%r12), %r15
	jmp	.LBB3_74
.LBB3_46:
	xorl	%r15d, %r15d
	movl	%ebp, %ecx
.LBB3_47:                               # %_ZN20btAlignedObjectArrayIjE8allocateEi.exit.i.i
	movq	48(%r12), %rdi
	testl	%ecx, %ecx
	jle	.LBB3_50
# BB#48:                                # %.lr.ph.i.i.i59
	movslq	%ecx, %rax
	cmpl	$8, %ecx
	jae	.LBB3_52
# BB#49:
	xorl	%edx, %edx
	jmp	.LBB3_65
.LBB3_50:                               # %_ZNK20btAlignedObjectArrayIjE4copyEiiPj.exit.i.i
	testq	%rdi, %rdi
	jne	.LBB3_71
# BB#51:                                # %_ZN20btAlignedObjectArrayIjE7reserveEi.exit.preheader.thread26.i
	leaq	56(%r12), %rbx
	jmp	.LBB3_73
.LBB3_52:                               # %min.iters.checked
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB3_56
# BB#53:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rsi
	cmpq	%rsi, %r15
	jae	.LBB3_57
# BB#54:                                # %vector.memcheck
	leaq	(%r15,%rax,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB3_57
.LBB3_56:
	xorl	%edx, %edx
.LBB3_65:                               # %scalar.ph.preheader
	movl	%ebp, %ebx
	subl	%edx, %ecx
	leaq	-1(%rax), %rsi
	subq	%rdx, %rsi
	andq	$7, %rcx
	je	.LBB3_68
# BB#66:                                # %scalar.ph.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB3_67:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %ebp
	movl	%ebp, (%r15,%rdx,4)
	incq	%rdx
	incq	%rcx
	jne	.LBB3_67
.LBB3_68:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rsi
	movl	%ebx, %ebp
	jb	.LBB3_71
# BB#69:                                # %scalar.ph.preheader.new
	subq	%rdx, %rax
	leaq	28(%rdi,%rdx,4), %rcx
	leaq	28(%r15,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB3_70:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %rax
	jne	.LBB3_70
.LBB3_71:                               # %_ZNK20btAlignedObjectArrayIjE4copyEiiPj.exit.thread.i.i
	leaq	56(%r12), %rbx
	cmpb	$0, 56(%r12)
	je	.LBB3_73
# BB#72:
.Ltmp22:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp23:
.LBB3_73:                               # %.lr.ph.i65.sink.split
	movb	$1, (%rbx)
	movq	%r15, 48(%r12)
	movl	%r13d, 40(%r12)
.LBB3_74:                               # %.lr.ph.i65
	movslq	%ebp, %rax
	leaq	(%r15,%rax,4), %rdi
	subq	%rax, %r14
	shlq	$2, %r14
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movl	64(%r12), %eax
.LBB3_75:                               # %.loopexit
	movl	%r13d, 36(%r12)
	testl	%eax, %eax
	jle	.LBB3_78
# BB#76:                                # %.lr.ph
	movq	64(%rsp), %rax
	movq	48(%r12), %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_77:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rsi,4), %edx
	movl	%edx, (%rcx,%rsi,4)
	incq	%rsi
	movslq	64(%r12), %rdx
	cmpq	%rdx, %rsi
	jl	.LBB3_77
.LBB3_78:                               # %._crit_edge
	movb	$1, %bl
.Ltmp25:
	leaq	80(%rsp), %rdi
	movq	%rsp, %rsi
	callq	_ZN11HullLibrary13ReleaseResultER10HullResult
.Ltmp26:
.LBB3_79:
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_83
# BB#80:
	cmpb	$0, 72(%rsp)
	je	.LBB3_82
# BB#81:
.Ltmp36:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp37:
.LBB3_82:                               # %.noexc.i72
	movq	$0, 64(%rsp)
.LBB3_83:
	movb	$1, 72(%rsp)
	movq	$0, 64(%rsp)
	movq	$0, 52(%rsp)
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_87
# BB#84:
	cmpb	$0, 32(%rsp)
	je	.LBB3_86
# BB#85:
.Ltmp42:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp43:
.LBB3_86:                               # %.noexc76
	movq	$0, 24(%rsp)
.LBB3_87:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_91
# BB#88:
	cmpb	$0, 136(%rsp)
	je	.LBB3_90
# BB#89:
.Ltmp54:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp55:
.LBB3_90:                               # %.noexc.i81
	movq	$0, 128(%rsp)
.LBB3_91:
	movb	$1, 136(%rsp)
	movq	$0, 128(%rsp)
	movq	$0, 116(%rsp)
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_95
# BB#92:
	cmpb	$0, 104(%rsp)
	je	.LBB3_94
# BB#93:
	callq	_Z21btAlignedFreeInternalPv
.LBB3_94:
	movq	$0, 96(%rsp)
.LBB3_95:                               # %_ZN11HullLibraryD2Ev.exit85
	movl	%ebx, %eax
	addq	$1192, %rsp             # imm = 0x4A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_57:                               # %vector.body.preheader
	movl	%ebp, %r8d
	leaq	-8(%rdx), %rbp
	movl	%ebp, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB3_60
# BB#58:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_59:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rdi,%rbx,4), %xmm0
	movupd	16(%rdi,%rbx,4), %xmm1
	movupd	%xmm0, (%r15,%rbx,4)
	movupd	%xmm1, 16(%r15,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB3_59
	jmp	.LBB3_61
.LBB3_60:
	xorl	%ebx, %ebx
.LBB3_61:                               # %vector.body.prol.loopexit
	cmpq	$24, %rbp
	jb	.LBB3_64
# BB#62:                                # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rbx, %rsi
	leaq	112(%rdi,%rbx,4), %rbp
	leaq	112(%r15,%rbx,4), %rbx
.LBB3_63:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movupd	-16(%rbp), %xmm0
	movupd	(%rbp), %xmm1
	movupd	%xmm0, -16(%rbx)
	movupd	%xmm1, (%rbx)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-32, %rsi
	jne	.LBB3_63
.LBB3_64:                               # %middle.block
	cmpq	%rdx, %rax
	movl	%r8d, %ebp
	je	.LBB3_71
	jmp	.LBB3_65
.LBB3_96:
.Ltmp19:
	jmp	.LBB3_108
.LBB3_97:
.Ltmp24:
	jmp	.LBB3_108
.LBB3_98:
.Ltmp56:
	movq	%rax, %rbx
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_125
# BB#99:
	cmpb	$0, 104(%rsp)
	je	.LBB3_124
# BB#100:
.Ltmp57:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp58:
	jmp	.LBB3_124
.LBB3_101:
.Ltmp59:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_102:
.Ltmp44:
	movq	%rax, %rbx
	jmp	.LBB3_117
.LBB3_103:
.Ltmp38:
	movq	%rax, %rbx
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_116
# BB#104:
	cmpb	$0, 32(%rsp)
	je	.LBB3_115
# BB#105:
.Ltmp39:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp40:
	jmp	.LBB3_115
.LBB3_106:
.Ltmp41:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_107:
.Ltmp27:
.LBB3_108:
	movq	%rax, %rbx
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_112
# BB#109:
	cmpb	$0, 72(%rsp)
	je	.LBB3_111
# BB#110:
.Ltmp28:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp29:
.LBB3_111:                              # %.noexc.i47
	movq	$0, 64(%rsp)
.LBB3_112:
	movb	$1, 72(%rsp)
	movq	$0, 64(%rsp)
	movq	$0, 52(%rsp)
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_116
# BB#113:
	cmpb	$0, 32(%rsp)
	je	.LBB3_115
# BB#114:
.Ltmp34:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp35:
.LBB3_115:                              # %.noexc51
	movq	$0, 24(%rsp)
.LBB3_116:                              # %_ZN10HullResultD2Ev.exit
	movb	$1, 32(%rsp)
	movq	$0, 24(%rsp)
	movq	$0, 12(%rsp)
.LBB3_117:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_121
# BB#118:
	cmpb	$0, 136(%rsp)
	je	.LBB3_120
# BB#119:
.Ltmp45:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp46:
.LBB3_120:                              # %.noexc.i
	movq	$0, 128(%rsp)
.LBB3_121:
	movb	$1, 136(%rsp)
	movq	$0, 128(%rsp)
	movq	$0, 116(%rsp)
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_125
# BB#122:
	cmpb	$0, 104(%rsp)
	je	.LBB3_124
# BB#123:
.Ltmp51:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp52:
.LBB3_124:                              # %.noexc4.i84
	movq	$0, 96(%rsp)
.LBB3_125:                              # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB3_126:
.Ltmp30:
	movq	%rax, %rbx
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_130
# BB#127:
	cmpb	$0, 32(%rsp)
	je	.LBB3_129
# BB#128:
.Ltmp31:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp32:
.LBB3_129:                              # %.noexc4.i50
	movq	$0, 24(%rsp)
.LBB3_130:
	movb	$1, 32(%rsp)
	movq	$0, 24(%rsp)
	movq	$0, 12(%rsp)
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB3_131:
.Ltmp33:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_132:
.Ltmp47:
	movq	%rax, %rbx
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_136
# BB#133:
	cmpb	$0, 104(%rsp)
	je	.LBB3_135
# BB#134:
.Ltmp48:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp49:
.LBB3_135:                              # %.noexc4.i
	movq	$0, 96(%rsp)
.LBB3_136:
	movb	$1, 104(%rsp)
	movq	$0, 96(%rsp)
	movq	$0, 84(%rsp)
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB3_137:
.Ltmp50:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_138:
.Ltmp53:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN11btShapeHull9buildHullEf, .Lfunc_end3-_ZN11btShapeHull9buildHullEf
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\200\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\367\001"              # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp13-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp27-.Lfunc_begin1   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp18-.Ltmp15         #   Call between .Ltmp15 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin1   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp23-.Ltmp20         #   Call between .Ltmp20 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin1   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp25-.Ltmp23         #   Call between .Ltmp23 and .Ltmp25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin1   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin1   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin1   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin1   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp57-.Ltmp55         #   Call between .Ltmp55 and .Ltmp57
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin1   #     jumps to .Ltmp59
	.byte	1                       #   On action: 1
	.long	.Ltmp39-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin1   #     jumps to .Ltmp41
	.byte	1                       #   On action: 1
	.long	.Ltmp28-.Lfunc_begin1   # >> Call Site 13 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin1   #     jumps to .Ltmp30
	.byte	1                       #   On action: 1
	.long	.Ltmp34-.Lfunc_begin1   # >> Call Site 14 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp53-.Lfunc_begin1   #     jumps to .Ltmp53
	.byte	1                       #   On action: 1
	.long	.Ltmp45-.Lfunc_begin1   # >> Call Site 15 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin1   #     jumps to .Ltmp47
	.byte	1                       #   On action: 1
	.long	.Ltmp51-.Lfunc_begin1   # >> Call Site 16 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin1   #     jumps to .Ltmp53
	.byte	1                       #   On action: 1
	.long	.Ltmp52-.Lfunc_begin1   # >> Call Site 17 <<
	.long	.Ltmp31-.Ltmp52         #   Call between .Ltmp52 and .Ltmp31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin1   # >> Call Site 18 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin1   #     jumps to .Ltmp33
	.byte	1                       #   On action: 1
	.long	.Ltmp48-.Lfunc_begin1   # >> Call Site 19 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin1   #     jumps to .Ltmp50
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZNK11btShapeHull12numTrianglesEv
	.p2align	4, 0x90
	.type	_ZNK11btShapeHull12numTrianglesEv,@function
_ZNK11btShapeHull12numTrianglesEv:      # @_ZNK11btShapeHull12numTrianglesEv
	.cfi_startproc
# BB#0:
	movl	64(%rdi), %ecx
	movl	$2863311531, %eax       # imm = 0xAAAAAAAB
	imulq	%rcx, %rax
	shrq	$33, %rax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end4:
	.size	_ZNK11btShapeHull12numTrianglesEv, .Lfunc_end4-_ZNK11btShapeHull12numTrianglesEv
	.cfi_endproc

	.globl	_ZNK11btShapeHull11numVerticesEv
	.p2align	4, 0x90
	.type	_ZNK11btShapeHull11numVerticesEv,@function
_ZNK11btShapeHull11numVerticesEv:       # @_ZNK11btShapeHull11numVerticesEv
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %eax
	retq
.Lfunc_end5:
	.size	_ZNK11btShapeHull11numVerticesEv, .Lfunc_end5-_ZNK11btShapeHull11numVerticesEv
	.cfi_endproc

	.globl	_ZNK11btShapeHull10numIndicesEv
	.p2align	4, 0x90
	.type	_ZNK11btShapeHull10numIndicesEv,@function
_ZNK11btShapeHull10numIndicesEv:        # @_ZNK11btShapeHull10numIndicesEv
	.cfi_startproc
# BB#0:
	movl	64(%rdi), %eax
	retq
.Lfunc_end6:
	.size	_ZNK11btShapeHull10numIndicesEv, .Lfunc_end6-_ZNK11btShapeHull10numIndicesEv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_btShapeHull.ii,@function
_GLOBAL__sub_I_btShapeHull.ii:          # @_GLOBAL__sub_I_btShapeHull.ii
	.cfi_startproc
# BB#0:
	movl	$0, _ZL18btUnitSpherePoints(%rip)
	movl	$-2147483648, _ZL18btUnitSpherePoints+4(%rip) # imm = 0x80000000
	movl	$-1082130432, _ZL18btUnitSpherePoints+8(%rip) # imm = 0xBF800000
	movl	$0, _ZL18btUnitSpherePoints+12(%rip)
	movl	$1060716128, _ZL18btUnitSpherePoints+16(%rip) # imm = 0x3F393E60
	movl	$-1090087446, _ZL18btUnitSpherePoints+20(%rip) # imm = 0xBF0695EA
	movl	$-1092290076, _ZL18btUnitSpherePoints+24(%rip) # imm = 0xBEE4F9E4
	movl	$0, _ZL18btUnitSpherePoints+28(%rip)
	movl	$-1098022214, _ZL18btUnitSpherePoints+32(%rip) # imm = 0xBE8D82BA
	movl	$-1084636126, _ZL18btUnitSpherePoints+36(%rip) # imm = 0xBF59C422
	movl	$-1092290076, _ZL18btUnitSpherePoints+40(%rip) # imm = 0xBEE4F9E4
	movl	$0, _ZL18btUnitSpherePoints+44(%rip)
	movl	$-1083901670, _ZL18btUnitSpherePoints+48(%rip) # imm = 0xBF64F91A
	movl	$-2147483648, _ZL18btUnitSpherePoints+52(%rip) # imm = 0x80000000
	movl	$-1092290177, _ZL18btUnitSpherePoints+56(%rip) # imm = 0xBEE4F97F
	movl	$0, _ZL18btUnitSpherePoints+60(%rip)
	movl	$-1098022214, _ZL18btUnitSpherePoints+64(%rip) # imm = 0xBE8D82BA
	movl	$1062847522, _ZL18btUnitSpherePoints+68(%rip) # imm = 0x3F59C422
	movl	$-1092290043, _ZL18btUnitSpherePoints+72(%rip) # imm = 0xBEE4FA05
	movl	$0, _ZL18btUnitSpherePoints+76(%rip)
	movl	$1060716128, _ZL18btUnitSpherePoints+80(%rip) # imm = 0x3F393E60
	movl	$1057396202, _ZL18btUnitSpherePoints+84(%rip) # imm = 0x3F0695EA
	movl	$-1092290076, _ZL18btUnitSpherePoints+88(%rip) # imm = 0xBEE4F9E4
	movl	$0, _ZL18btUnitSpherePoints+92(%rip)
	movl	$1049461434, _ZL18btUnitSpherePoints+96(%rip) # imm = 0x3E8D82BA
	movl	$-1084636126, _ZL18btUnitSpherePoints+100(%rip) # imm = 0xBF59C422
	movl	$1055193605, _ZL18btUnitSpherePoints+104(%rip) # imm = 0x3EE4FA05
	movl	$0, _ZL18btUnitSpherePoints+108(%rip)
	movl	$-1086767520, _ZL18btUnitSpherePoints+112(%rip) # imm = 0xBF393E60
	movl	$-1090087446, _ZL18btUnitSpherePoints+116(%rip) # imm = 0xBF0695EA
	movl	$1055193572, _ZL18btUnitSpherePoints+120(%rip) # imm = 0x3EE4F9E4
	movl	$0, _ZL18btUnitSpherePoints+124(%rip)
	movl	$-1086767520, _ZL18btUnitSpherePoints+128(%rip) # imm = 0xBF393E60
	movl	$1057396202, _ZL18btUnitSpherePoints+132(%rip) # imm = 0x3F0695EA
	movl	$1055193572, _ZL18btUnitSpherePoints+136(%rip) # imm = 0x3EE4F9E4
	movl	$0, _ZL18btUnitSpherePoints+140(%rip)
	movl	$1049461434, _ZL18btUnitSpherePoints+144(%rip) # imm = 0x3E8D82BA
	movl	$1062847522, _ZL18btUnitSpherePoints+148(%rip) # imm = 0x3F59C422
	movl	$1055193572, _ZL18btUnitSpherePoints+152(%rip) # imm = 0x3EE4F9E4
	movl	$0, _ZL18btUnitSpherePoints+156(%rip)
	movl	$1063581978, _ZL18btUnitSpherePoints+160(%rip) # imm = 0x3F64F91A
	movl	$0, _ZL18btUnitSpherePoints+164(%rip)
	movl	$1055193471, _ZL18btUnitSpherePoints+168(%rip) # imm = 0x3EE4F97F
	movl	$0, _ZL18btUnitSpherePoints+172(%rip)
	movl	$-2147483648, _ZL18btUnitSpherePoints+176(%rip) # imm = 0x80000000
	movl	$0, _ZL18btUnitSpherePoints+180(%rip)
	movl	$1065353216, _ZL18btUnitSpherePoints+184(%rip) # imm = 0x3F800000
	movl	$0, _ZL18btUnitSpherePoints+188(%rip)
	movl	$1054458864, _ZL18btUnitSpherePoints+192(%rip) # imm = 0x3ED9C3F0
	movl	$-1096927567, _ZL18btUnitSpherePoints+196(%rip) # imm = 0xBE9E36B1
	movl	$-1084636042, _ZL18btUnitSpherePoints+200(%rip) # imm = 0xBF59C476
	movl	$0, _ZL18btUnitSpherePoints+204(%rip)
	movl	$-1104782626, _ZL18btUnitSpherePoints+208(%rip) # imm = 0xBE265ADE
	movl	$-1090519208, _ZL18btUnitSpherePoints+212(%rip) # imm = 0xBEFFFF58
	movl	$-1084636042, _ZL18btUnitSpherePoints+216(%rip) # imm = 0xBF59C476
	movl	$0, _ZL18btUnitSpherePoints+220(%rip)
	movl	$1049007812, _ZL18btUnitSpherePoints+224(%rip) # imm = 0x3E8696C4
	movl	$-1085334679, _ZL18btUnitSpherePoints+228(%rip) # imm = 0xBF4F1B69
	movl	$-1090087228, _ZL18btUnitSpherePoints+232(%rip) # imm = 0xBF0696C4
	movl	$0, _ZL18btUnitSpherePoints+236(%rip)
	movl	$1054458864, _ZL18btUnitSpherePoints+240(%rip) # imm = 0x3ED9C3F0
	movl	$1050556081, _ZL18btUnitSpherePoints+244(%rip) # imm = 0x3E9E36B1
	movl	$-1084636042, _ZL18btUnitSpherePoints+248(%rip) # imm = 0xBF59C476
	movl	$0, _ZL18btUnitSpherePoints+252(%rip)
	movl	$1062847505, _ZL18btUnitSpherePoints+256(%rip) # imm = 0x3F59C411
	movl	$-2147483648, _ZL18btUnitSpherePoints+260(%rip) # imm = 0x80000000
	movl	$-1090087262, _ZL18btUnitSpherePoints+264(%rip) # imm = 0xBF0696A2
	movl	$0, _ZL18btUnitSpherePoints+268(%rip)
	movl	$-1090087362, _ZL18btUnitSpherePoints+272(%rip) # imm = 0xBF06963E
	movl	$-2147483648, _ZL18btUnitSpherePoints+276(%rip) # imm = 0x80000000
	movl	$-1084636076, _ZL18btUnitSpherePoints+280(%rip) # imm = 0xBF59C454
	movl	$0, _ZL18btUnitSpherePoints+284(%rip)
	movl	$-1087361736, _ZL18btUnitSpherePoints+288(%rip) # imm = 0xBF302D38
	movl	$-1090519141, _ZL18btUnitSpherePoints+292(%rip) # imm = 0xBEFFFF9B
	movl	$-1090087262, _ZL18btUnitSpherePoints+296(%rip) # imm = 0xBF0696A2
	movl	$0, _ZL18btUnitSpherePoints+300(%rip)
	movl	$-1104782626, _ZL18btUnitSpherePoints+304(%rip) # imm = 0xBE265ADE
	movl	$1056964440, _ZL18btUnitSpherePoints+308(%rip) # imm = 0x3EFFFF58
	movl	$-1084636042, _ZL18btUnitSpherePoints+312(%rip) # imm = 0xBF59C476
	movl	$0, _ZL18btUnitSpherePoints+316(%rip)
	movl	$-1087361736, _ZL18btUnitSpherePoints+320(%rip) # imm = 0xBF302D38
	movl	$1056964507, _ZL18btUnitSpherePoints+324(%rip) # imm = 0x3EFFFF9B
	movl	$-1090087262, _ZL18btUnitSpherePoints+328(%rip) # imm = 0xBF0696A2
	movl	$0, _ZL18btUnitSpherePoints+332(%rip)
	movl	$1049007812, _ZL18btUnitSpherePoints+336(%rip) # imm = 0x3E8696C4
	movl	$1062148969, _ZL18btUnitSpherePoints+340(%rip) # imm = 0x3F4F1B69
	movl	$-1090087228, _ZL18btUnitSpherePoints+344(%rip) # imm = 0xBF0696C4
	movl	$0, _ZL18btUnitSpherePoints+348(%rip)
	movl	$1064532105, _ZL18btUnitSpherePoints+352(%rip) # imm = 0x3F737889
	movl	$1050556148, _ZL18btUnitSpherePoints+356(%rip) # imm = 0x3E9E36F4
	movl	$0, _ZL18btUnitSpherePoints+360(%rip)
	movl	$0, _ZL18btUnitSpherePoints+364(%rip)
	movl	$1064532105, _ZL18btUnitSpherePoints+368(%rip) # imm = 0x3F737889
	movl	$-1096927500, _ZL18btUnitSpherePoints+372(%rip) # imm = 0xBE9E36F4
	movl	$0, _ZL18btUnitSpherePoints+376(%rip)
	movl	$0, _ZL18btUnitSpherePoints+380(%rip)
	movl	$1058437413, _ZL18btUnitSpherePoints+384(%rip) # imm = 0x3F167925
	movl	$-1085334595, _ZL18btUnitSpherePoints+388(%rip) # imm = 0xBF4F1BBD
	movl	$0, _ZL18btUnitSpherePoints+392(%rip)
	movl	$0, _ZL18btUnitSpherePoints+396(%rip)
	movl	$0, _ZL18btUnitSpherePoints+400(%rip)
	movl	$-1082130432, _ZL18btUnitSpherePoints+404(%rip) # imm = 0xBF800000
	movl	$0, _ZL18btUnitSpherePoints+408(%rip)
	movl	$0, _ZL18btUnitSpherePoints+412(%rip)
	movl	$-1089046235, _ZL18btUnitSpherePoints+416(%rip) # imm = 0xBF167925
	movl	$-1085334595, _ZL18btUnitSpherePoints+420(%rip) # imm = 0xBF4F1BBD
	movl	$0, _ZL18btUnitSpherePoints+424(%rip)
	movl	$0, _ZL18btUnitSpherePoints+428(%rip)
	movl	$-1082951543, _ZL18btUnitSpherePoints+432(%rip) # imm = 0xBF737889
	movl	$-1096927500, _ZL18btUnitSpherePoints+436(%rip) # imm = 0xBE9E36F4
	movl	$-2147483648, _ZL18btUnitSpherePoints+440(%rip) # imm = 0x80000000
	movl	$0, _ZL18btUnitSpherePoints+444(%rip)
	movl	$-1082951543, _ZL18btUnitSpherePoints+448(%rip) # imm = 0xBF737889
	movl	$1050556148, _ZL18btUnitSpherePoints+452(%rip) # imm = 0x3E9E36F4
	movl	$-2147483648, _ZL18btUnitSpherePoints+456(%rip) # imm = 0x80000000
	movl	$0, _ZL18btUnitSpherePoints+460(%rip)
	movl	$-1089046235, _ZL18btUnitSpherePoints+464(%rip) # imm = 0xBF167925
	movl	$1062149053, _ZL18btUnitSpherePoints+468(%rip) # imm = 0x3F4F1BBD
	movl	$-2147483648, _ZL18btUnitSpherePoints+472(%rip) # imm = 0x80000000
	movl	$0, _ZL18btUnitSpherePoints+476(%rip)
	movl	$-2147483648, _ZL18btUnitSpherePoints+480(%rip) # imm = 0x80000000
	movl	$1065353216, _ZL18btUnitSpherePoints+484(%rip) # imm = 0x3F800000
	movl	$-2147483648, _ZL18btUnitSpherePoints+488(%rip) # imm = 0x80000000
	movl	$0, _ZL18btUnitSpherePoints+492(%rip)
	movl	$1058437413, _ZL18btUnitSpherePoints+496(%rip) # imm = 0x3F167925
	movl	$1062149053, _ZL18btUnitSpherePoints+500(%rip) # imm = 0x3F4F1BBD
	movl	$-2147483648, _ZL18btUnitSpherePoints+504(%rip) # imm = 0x80000000
	movl	$0, _ZL18btUnitSpherePoints+508(%rip)
	movl	$1060121912, _ZL18btUnitSpherePoints+512(%rip) # imm = 0x3F302D38
	movl	$-1090519141, _ZL18btUnitSpherePoints+516(%rip) # imm = 0xBEFFFF9B
	movl	$1057396386, _ZL18btUnitSpherePoints+520(%rip) # imm = 0x3F0696A2
	movl	$0, _ZL18btUnitSpherePoints+524(%rip)
	movl	$-1098475836, _ZL18btUnitSpherePoints+528(%rip) # imm = 0xBE8696C4
	movl	$-1085334679, _ZL18btUnitSpherePoints+532(%rip) # imm = 0xBF4F1B69
	movl	$1057396420, _ZL18btUnitSpherePoints+536(%rip) # imm = 0x3F0696C4
	movl	$0, _ZL18btUnitSpherePoints+540(%rip)
	movl	$-1084636143, _ZL18btUnitSpherePoints+544(%rip) # imm = 0xBF59C411
	movl	$0, _ZL18btUnitSpherePoints+548(%rip)
	movl	$1057396386, _ZL18btUnitSpherePoints+552(%rip) # imm = 0x3F0696A2
	movl	$0, _ZL18btUnitSpherePoints+556(%rip)
	movl	$-1098475836, _ZL18btUnitSpherePoints+560(%rip) # imm = 0xBE8696C4
	movl	$1062148969, _ZL18btUnitSpherePoints+564(%rip) # imm = 0x3F4F1B69
	movl	$1057396420, _ZL18btUnitSpherePoints+568(%rip) # imm = 0x3F0696C4
	movl	$0, _ZL18btUnitSpherePoints+572(%rip)
	movl	$1060121912, _ZL18btUnitSpherePoints+576(%rip) # imm = 0x3F302D38
	movl	$1056964507, _ZL18btUnitSpherePoints+580(%rip) # imm = 0x3EFFFF9B
	movl	$1057396386, _ZL18btUnitSpherePoints+584(%rip) # imm = 0x3F0696A2
	movl	$0, _ZL18btUnitSpherePoints+588(%rip)
	movl	$1057396286, _ZL18btUnitSpherePoints+592(%rip) # imm = 0x3F06963E
	movl	$0, _ZL18btUnitSpherePoints+596(%rip)
	movl	$1062847572, _ZL18btUnitSpherePoints+600(%rip) # imm = 0x3F59C454
	movl	$0, _ZL18btUnitSpherePoints+604(%rip)
	movl	$1042701022, _ZL18btUnitSpherePoints+608(%rip) # imm = 0x3E265ADE
	movl	$-1090519208, _ZL18btUnitSpherePoints+612(%rip) # imm = 0xBEFFFF58
	movl	$1062847606, _ZL18btUnitSpherePoints+616(%rip) # imm = 0x3F59C476
	movl	$0, _ZL18btUnitSpherePoints+620(%rip)
	movl	$-1093024784, _ZL18btUnitSpherePoints+624(%rip) # imm = 0xBED9C3F0
	movl	$-1096927567, _ZL18btUnitSpherePoints+628(%rip) # imm = 0xBE9E36B1
	movl	$1062847606, _ZL18btUnitSpherePoints+632(%rip) # imm = 0x3F59C476
	movl	$0, _ZL18btUnitSpherePoints+636(%rip)
	movl	$-1093024784, _ZL18btUnitSpherePoints+640(%rip) # imm = 0xBED9C3F0
	movl	$1050556081, _ZL18btUnitSpherePoints+644(%rip) # imm = 0x3E9E36B1
	movl	$1062847606, _ZL18btUnitSpherePoints+648(%rip) # imm = 0x3F59C476
	movl	$0, _ZL18btUnitSpherePoints+652(%rip)
	movabsq	$4539627703877655262, %rax # imm = 0x3EFFFF583E265ADE
	movq	%rax, _ZL18btUnitSpherePoints+656(%rip)
	movq	$1062847606, _ZL18btUnitSpherePoints+664(%rip) # imm = 0x3F59C476
	retq
.Lfunc_end7:
	.size	_GLOBAL__sub_I_btShapeHull.ii, .Lfunc_end7-_GLOBAL__sub_I_btShapeHull.ii
	.cfi_endproc

	.type	_ZL18btUnitSpherePoints,@object # @_ZL18btUnitSpherePoints
	.local	_ZL18btUnitSpherePoints
	.comm	_ZL18btUnitSpherePoints,992,16
	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_btShapeHull.ii

	.globl	_ZN11btShapeHullC1EPK13btConvexShape
	.type	_ZN11btShapeHullC1EPK13btConvexShape,@function
_ZN11btShapeHullC1EPK13btConvexShape = _ZN11btShapeHullC2EPK13btConvexShape
	.globl	_ZN11btShapeHullD1Ev
	.type	_ZN11btShapeHullD1Ev,@function
_ZN11btShapeHullD1Ev = _ZN11btShapeHullD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
