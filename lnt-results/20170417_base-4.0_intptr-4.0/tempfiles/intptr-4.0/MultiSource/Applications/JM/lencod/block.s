	.text
	.file	"block.bc"
	.globl	intrapred_luma
	.p2align	4, 0x90
	.type	intrapred_luma,@function
intrapred_luma:                         # @intrapred_luma
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 288
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movl	%esi, %ebx
	movl	%edi, %r13d
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	andl	$15, %r13d
	andl	$15, %ebx
	movq	img(%rip), %rax
	movl	12(%rax), %ebp
	leal	-1(%r13), %r14d
	leaq	112(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebp, %edi
	movl	%r14d, %esi
	movl	%ebx, %edx
	callq	*getNeighbour(%rip)
	leal	1(%rbx), %edx
	leaq	136(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebp, %edi
	movl	%r14d, %esi
	callq	*getNeighbour(%rip)
	leal	2(%rbx), %edx
	leaq	160(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebp, %edi
	movl	%r14d, %esi
	callq	*getNeighbour(%rip)
	leal	3(%rbx), %edx
	leaq	184(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebp, %edi
	movl	%r14d, %esi
	callq	*getNeighbour(%rip)
	leal	-1(%rbx), %r12d
	leaq	88(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebp, %edi
	movl	%r13d, %esi
	movl	%r12d, %edx
	callq	*getNeighbour(%rip)
	leal	4(%r13), %esi
	leaq	64(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebp, %edi
	movl	%r12d, %edx
	callq	*getNeighbour(%rip)
	leaq	208(%rsp), %r15
	xorl	%ecx, %ecx
	movl	%ebp, %edi
	movl	%r14d, %esi
	movl	%r12d, %edx
	movq	%r15, %r8
	callq	*getNeighbour(%rip)
	cmpl	$0, 64(%rsp)
	je	.LBB0_1
# BB#2:
	orl	$8, %ebx
	cmpl	$4, %r13d
	setne	%al
	cmpl	$12, %ebx
	setne	%cl
	orb	%al, %cl
	jmp	.LBB0_3
.LBB0_1:
	xorl	%ecx, %ecx
.LBB0_3:
	movzbl	%cl, %edx
	movl	%edx, 64(%rsp)
	movq	input(%rip), %rax
	cmpl	$0, 272(%rax)
	je	.LBB0_19
# BB#4:                                 # %.preheader241
	movq	img(%rip), %rcx
	xorl	%esi, %esi
	cmpl	$0, 112(%rsp)
	movl	$0, %eax
	je	.LBB0_6
# BB#5:
	movq	14240(%rcx), %rax
	movslq	116(%rsp), %rdi
	movl	(%rax,%rdi,4), %eax
.LBB0_6:
	cmpl	$0, 136(%rsp)
	je	.LBB0_8
# BB#7:
	movq	14240(%rcx), %rsi
	movslq	140(%rsp), %rdi
	movl	(%rsi,%rdi,4), %esi
.LBB0_8:
	xorl	%r12d, %r12d
	cmpl	$0, 160(%rsp)
	movl	$0, %edi
	je	.LBB0_10
# BB#9:
	movq	14240(%rcx), %rdi
	movslq	164(%rsp), %rbp
	movl	(%rdi,%rbp,4), %edi
.LBB0_10:
	andl	$1, %eax
	cmpl	$0, 184(%rsp)
	je	.LBB0_12
# BB#11:
	movq	14240(%rcx), %rbp
	movslq	188(%rsp), %rbx
	movl	(%rbp,%rbx,4), %r12d
.LBB0_12:
	andl	%eax, %esi
	xorl	%eax, %eax
	cmpl	$0, 88(%rsp)
	movl	$0, %r13d
	je	.LBB0_14
# BB#13:
	movq	14240(%rcx), %rbp
	movslq	92(%rsp), %rbx
	movl	(%rbp,%rbx,4), %r13d
.LBB0_14:
	andl	%esi, %edi
	testl	%edx, %edx
	je	.LBB0_16
# BB#15:
	movq	14240(%rcx), %rax
	movslq	68(%rsp), %rdx
	movl	(%rax,%rdx,4), %eax
.LBB0_16:
	andl	%edi, %r12d
	cmpl	$0, 208(%rsp)
	movq	32(%rsp), %rsi          # 8-byte Reload
	je	.LBB0_17
# BB#18:
	movslq	212(%rsp), %r15
	shlq	$2, %r15
	addq	14240(%rcx), %r15
	jmp	.LBB0_20
.LBB0_19:
	movl	112(%rsp), %r12d
	movl	88(%rsp), %r13d
	movl	%edx, %eax
	movq	32(%rsp), %rsi          # 8-byte Reload
.LBB0_20:                               # %.sink.split
	cmpl	$0, (%r15)
	setne	%r9b
	jmp	.LBB0_21
.LBB0_17:
	xorl	%r9d, %r9d
.LBB0_21:
	testl	%r13d, %r13d
	setne	%dl
	testl	%r12d, %r12d
	setne	%r11b
	andb	%dl, %r11b
	movl	%r11d, %ecx
	andb	%r9b, %cl
	testl	%r13d, %r13d
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	%r12d, (%rdx)
	movq	56(%rsp), %rdx          # 8-byte Reload
	movl	%r13d, (%rdx)
	movb	%cl, 32(%rsp)           # 1-byte Spill
	movzbl	%cl, %edx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%edx, (%rcx)
	je	.LBB0_23
# BB#22:
	movslq	108(%rsp), %rdx
	movq	(%rsi,%rdx,8), %rdx
	movslq	104(%rsp), %rdi
	movw	(%rdx,%rdi,2), %r15w
	movw	%r15w, 2(%rsp)
	movw	2(%rdx,%rdi,2), %r10w
	movw	%r10w, 4(%rsp)
	movw	4(%rdx,%rdi,2), %r14w
	movw	%r14w, 6(%rsp)
	movw	6(%rdx,%rdi,2), %dx
	jmp	.LBB0_24
.LBB0_23:
	movq	img(%rip), %rdx
	movw	15512(%rdx), %r14w
	movw	%r14w, 6(%rsp)
	movw	%r14w, 4(%rsp)
	movw	%r14w, 2(%rsp)
	movw	%r14w, %r10w
	movw	%r14w, %r15w
	movw	%r14w, %dx
.LBB0_24:
	movw	%dx, 8(%rsp)
	testl	%eax, %eax
	je	.LBB0_26
# BB#25:
	movslq	84(%rsp), %rax
	movq	(%rsi,%rax,8), %rax
	movslq	80(%rsp), %rdi
	movzwl	(%rax,%rdi,2), %ecx
	movw	%cx, 30(%rsp)           # 2-byte Spill
	movw	%cx, 10(%rsp)
	movzwl	2(%rax,%rdi,2), %ecx
	movw	%cx, 40(%rsp)           # 2-byte Spill
	movw	%cx, 12(%rsp)
	movzwl	4(%rax,%rdi,2), %ecx
	movw	%cx, 48(%rsp)           # 2-byte Spill
	movw	%cx, 14(%rsp)
	movzwl	6(%rax,%rdi,2), %eax
	jmp	.LBB0_27
.LBB0_26:
	movw	%dx, 14(%rsp)
	movw	%dx, 12(%rsp)
	movw	%dx, 10(%rsp)
	movl	%edx, %eax
	movw	%ax, 48(%rsp)           # 2-byte Spill
	movw	%ax, 40(%rsp)           # 2-byte Spill
	movw	%ax, 30(%rsp)           # 2-byte Spill
.LBB0_27:
	movw	%ax, 56(%rsp)           # 2-byte Spill
	movw	%ax, 16(%rsp)
	testl	%r12d, %r12d
	je	.LBB0_29
# BB#28:
	movslq	132(%rsp), %rax
	movq	(%rsi,%rax,8), %rax
	movslq	128(%rsp), %rdi
	movzwl	(%rax,%rdi,2), %ebp
	movw	%bp, 18(%rsp)
	movslq	156(%rsp), %rax
	movq	(%rsi,%rax,8), %rax
	movslq	152(%rsp), %rdi
	movzwl	(%rax,%rdi,2), %ebx
	movw	%bx, 20(%rsp)
	movslq	180(%rsp), %rax
	movq	(%rsi,%rax,8), %rax
	movslq	176(%rsp), %rdi
	movzwl	(%rax,%rdi,2), %eax
	movw	%ax, 22(%rsp)
	movslq	204(%rsp), %rdi
	movq	(%rsi,%rdi,8), %rdi
	movslq	200(%rsp), %rcx
	movzwl	(%rdi,%rcx,2), %r8d
	jmp	.LBB0_30
.LBB0_29:
	movq	img(%rip), %rax
	movzwl	15512(%rax), %eax
	movw	%ax, 22(%rsp)
	movw	%ax, 20(%rsp)
	movw	%ax, 18(%rsp)
	movl	%eax, %ebx
	movl	%eax, %ebp
	movl	%eax, %r8d
.LBB0_30:
	movw	%r8w, 24(%rsp)
	testb	%r9b, %r9b
	je	.LBB0_32
# BB#31:
	movslq	228(%rsp), %rcx
	movq	(%rsi,%rcx,8), %rcx
	movslq	224(%rsp), %rdi
	movzwl	(%rcx,%rdi,2), %edi
	movq	img(%rip), %rcx
	jmp	.LBB0_33
.LBB0_32:
	movq	img(%rip), %rcx
	movzwl	15512(%rcx), %edi
.LBB0_33:
	movw	%di, (%rsp)
	movw	$-1, 208(%rcx)
	movw	$-1, 720(%rcx)
	movw	$-1, 1232(%rcx)
	movw	$-1, 1744(%rcx)
	movw	$-1, 2256(%rcx)
	movw	$-1, 2768(%rcx)
	movw	$-1, 3280(%rcx)
	movw	$-1, 3792(%rcx)
	movw	$-1, 4304(%rcx)
	testb	%r11b, %r11b
	je	.LBB0_35
# BB#34:
	movzwl	%r15w, %edi
	movzwl	%r10w, %esi
	addl	%edi, %esi
	movzwl	%r14w, %edi
	addl	%edi, %esi
	movzwl	%dx, %edi
	addl	%edi, %esi
	movzwl	%bp, %edi
	addl	%edi, %esi
	movzwl	%bx, %edi
	addl	%edi, %esi
	movzwl	%ax, %edi
	addl	%edi, %esi
	movzwl	%r8w, %edi
	leal	4(%rdi,%rsi), %edi
	sarl	$3, %edi
	jmp	.LBB0_43
.LBB0_35:
	testl	%r13d, %r13d
	jne	.LBB0_39
# BB#36:
	testl	%r12d, %r12d
	je	.LBB0_39
# BB#37:
	movzwl	%bp, %esi
	movzwl	%bx, %edi
	movzwl	%ax, %r9d
	movzwl	%r8w, %r11d
	jmp	.LBB0_38
.LBB0_39:
	testl	%r13d, %r13d
	je	.LBB0_42
# BB#40:
	testl	%r12d, %r12d
	jne	.LBB0_42
# BB#41:
	movzwl	%r15w, %esi
	movzwl	%r10w, %edi
	movzwl	%r14w, %r9d
	movzwl	%dx, %r11d
.LBB0_38:                               # %.preheader
	addl	%esi, %edi
	addl	%r9d, %edi
	leal	2(%r11,%rdi), %edi
	shrl	$2, %edi
	jmp	.LBB0_43
.LBB0_42:
	movl	15512(%rcx), %edi
.LBB0_43:                               # %.preheader
	movb	32(%rsp), %r9b          # 1-byte Reload
	testl	%r13d, %r13d
	movw	%di, 1232(%rcx)
	movw	%di, 1234(%rcx)
	movw	%di, 1236(%rcx)
	movw	%di, 1238(%rcx)
	movw	%di, 1264(%rcx)
	movw	%di, 1266(%rcx)
	movw	%di, 1268(%rcx)
	movw	%di, 1270(%rcx)
	movw	%di, 1296(%rcx)
	movw	%di, 1298(%rcx)
	movw	%di, 1300(%rcx)
	movw	%di, 1302(%rcx)
	movw	%di, 1328(%rcx)
	movw	%di, 1330(%rcx)
	movw	%di, 1332(%rcx)
	movw	%di, 1334(%rcx)
	movw	%r15w, 304(%rcx)
	movw	%r15w, 272(%rcx)
	movw	%r15w, 240(%rcx)
	movw	%r15w, 208(%rcx)
	movw	%r10w, 306(%rcx)
	movw	%r10w, 274(%rcx)
	movw	%r10w, 242(%rcx)
	movw	%r10w, 210(%rcx)
	movw	%r14w, 308(%rcx)
	movw	%r14w, 276(%rcx)
	movw	%r14w, 244(%rcx)
	movw	%r14w, 212(%rcx)
	movw	%dx, 310(%rcx)
	movw	%dx, 278(%rcx)
	movw	%dx, 246(%rcx)
	movw	%dx, 214(%rcx)
	jne	.LBB0_45
# BB#44:
	movw	$-1, 208(%rcx)
.LBB0_45:
	testl	%r12d, %r12d
	movw	%bp, 726(%rcx)
	movw	%bp, 724(%rcx)
	movw	%bp, 722(%rcx)
	movw	%bp, 720(%rcx)
	movw	%bx, 758(%rcx)
	movw	%bx, 756(%rcx)
	movw	%bx, 754(%rcx)
	movw	%bx, 752(%rcx)
	movw	%ax, 790(%rcx)
	movw	%ax, 788(%rcx)
	movw	%ax, 786(%rcx)
	movw	%ax, 784(%rcx)
	movw	%r8w, 822(%rcx)
	movw	%r8w, 820(%rcx)
	movw	%r8w, 818(%rcx)
	movw	%r8w, 816(%rcx)
	jne	.LBB0_47
# BB#46:
	movw	$-1, 720(%rcx)
.LBB0_47:
	testl	%r13d, %r13d
	je	.LBB0_49
# BB#48:
	movzwl	%r15w, %ebp
	movzwl	%r14w, %eax
	movzwl	%r10w, %esi
	leal	(%rbp,%rax), %edi
	leal	2(%rdi,%rsi,2), %edi
	shrl	$2, %edi
	movw	%di, 1744(%rcx)
	movzwl	%dx, %edx
	leal	1(%rbp,%rsi), %edi
	leal	1(%rsi,%rax), %ebp
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	addl	%edx, %esi
	leal	2(%rsi,%rax,2), %esi
	shrl	$2, %esi
	movw	%si, 1776(%rcx)
	movw	%si, 1746(%rcx)
	movzwl	30(%rsp), %esi          # 2-byte Folded Reload
	leal	1(%rax,%rdx), %ebx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	addl	%esi, %eax
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movw	%ax, 1808(%rcx)
	movw	%ax, 1778(%rcx)
	movw	%ax, 1748(%rcx)
	movzwl	40(%rsp), %eax          # 2-byte Folded Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	addl	%eax, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, 1840(%rcx)
	movw	%dx, 1810(%rcx)
	movw	%dx, 1780(%rcx)
	movw	%dx, 1750(%rcx)
	movzwl	48(%rsp), %edx          # 2-byte Folded Reload
	addl	%edx, %esi
	leal	2(%rsi,%rax,2), %esi
	shrl	$2, %esi
	movw	%si, 1842(%rcx)
	movw	%si, 1812(%rcx)
	movw	%si, 1782(%rcx)
	movzwl	56(%rsp), %esi          # 2-byte Folded Reload
	addl	%esi, %eax
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movw	%ax, 1844(%rcx)
	movw	%ax, 1814(%rcx)
	leal	(%rsi,%rsi,2), %eax
	leal	2(%rdx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, 1846(%rcx)
	shrl	%edi
	movw	%di, 3792(%rcx)
	shrl	%ebp
	movw	%bp, 3856(%rcx)
	movw	%bp, 3794(%rcx)
	shrl	%ebx
	movw	%bx, 3858(%rcx)
	movw	%bx, 3796(%rcx)
	movzwl	8(%rsp), %edx
	movzwl	10(%rsp), %eax
	leal	1(%rdx,%rax), %esi
	shrl	%esi
	movw	%si, 3860(%rcx)
	movw	%si, 3798(%rcx)
	movzwl	12(%rsp), %ebp
	leal	1(%rax,%rbp), %esi
	shrl	%esi
	movw	%si, 3862(%rcx)
	movzwl	2(%rsp), %r15d
	movzwl	4(%rsp), %r10d
	movzwl	6(%rsp), %r14d
	leal	(%r15,%r10,2), %esi
	leal	2(%r14,%rsi), %esi
	shrl	$2, %esi
	movw	%si, 3824(%rcx)
	leal	(%r10,%r14,2), %esi
	leal	2(%rdx,%rsi), %esi
	shrl	$2, %esi
	movw	%si, 3888(%rcx)
	movw	%si, 3826(%rcx)
	leal	(%r14,%rdx,2), %esi
	leal	2(%rax,%rsi), %esi
	shrl	$2, %esi
	movw	%si, 3890(%rcx)
	movw	%si, 3828(%rcx)
	leal	(%rdx,%rax,2), %esi
	leal	2(%rbp,%rsi), %esi
	shrl	$2, %esi
	movw	%si, 3892(%rcx)
	movw	%si, 3830(%rcx)
	movzwl	14(%rsp), %esi
	leal	(%rax,%rbp,2), %eax
	leal	2(%rsi,%rax), %eax
	shrl	$2, %eax
	movw	%ax, 3894(%rcx)
.LBB0_49:
	testl	%r12d, %r12d
	je	.LBB0_51
# BB#50:
	movq	img(%rip), %rax
	movzwl	18(%rsp), %esi
	movzwl	20(%rsp), %ebp
	leal	1(%rsi,%rbp), %ecx
	shrl	%ecx
	movw	%cx, 4304(%rax)
	movzwl	22(%rsp), %ecx
	leal	(%rsi,%rbp,2), %esi
	leal	2(%rcx,%rsi), %esi
	shrl	$2, %esi
	movw	%si, 4306(%rax)
	leal	1(%rbp,%rcx), %esi
	shrl	%esi
	movw	%si, 4336(%rax)
	movw	%si, 4308(%rax)
	movzwl	24(%rsp), %edi
	leal	(%rbp,%rcx,2), %esi
	leal	2(%rdi,%rsi), %esi
	shrl	$2, %esi
	movw	%si, 4338(%rax)
	movw	%si, 4310(%rax)
	leal	1(%rcx,%rdi), %esi
	shrl	%esi
	movw	%si, 4368(%rax)
	movw	%si, 4340(%rax)
	addl	%edi, %ecx
	leal	2(%rcx,%rdi,2), %ecx
	shrl	$2, %ecx
	movw	%cx, 4370(%rax)
	movw	%cx, 4342(%rax)
	movw	%di, 4406(%rax)
	movw	%di, 4404(%rax)
	movw	%di, 4402(%rax)
	movw	%di, 4374(%rax)
	movw	%di, 4372(%rax)
	movw	%di, 4400(%rax)
.LBB0_51:
	testb	%r9b, %r9b
	je	.LBB0_53
# BB#52:
	movq	img(%rip), %rax
	movzwl	24(%rsp), %ecx
	movzwl	22(%rsp), %esi
	movzwl	20(%rsp), %edi
	leal	(%rcx,%rsi,2), %ecx
	leal	2(%rdi,%rcx), %ecx
	shrl	$2, %ecx
	movw	%cx, 2352(%rax)
	movzwl	18(%rsp), %ebx
	leal	(%rsi,%rdi,2), %ecx
	leal	2(%rbx,%rcx), %ecx
	shrl	$2, %ecx
	movw	%cx, 2354(%rax)
	movw	%cx, 2320(%rax)
	movzwl	(%rsp), %ecx
	leal	(%rdi,%rbx,2), %esi
	leal	2(%rcx,%rsi), %esi
	shrl	$2, %esi
	movw	%si, 2356(%rax)
	movw	%si, 2322(%rax)
	movw	%si, 2288(%rax)
	movzwl	%r15w, %ebp
	leal	(%rbx,%rcx,2), %esi
	leal	2(%rbp,%rsi), %esi
	shrl	$2, %esi
	movw	%si, 2358(%rax)
	movw	%si, 2324(%rax)
	movw	%si, 2290(%rax)
	movw	%si, 2256(%rax)
	movzwl	%r10w, %esi
	leal	(%rcx,%rbp,2), %edi
	leal	2(%rsi,%rdi), %edi
	shrl	$2, %edi
	movw	%di, 2326(%rax)
	movw	%di, 2292(%rax)
	movw	%di, 2258(%rax)
	movzwl	%r14w, %edi
	leal	(%rbp,%rsi,2), %ebx
	leal	2(%rdi,%rbx), %ebx
	shrl	$2, %ebx
	movw	%bx, 2294(%rax)
	movw	%bx, 2260(%rax)
	movzwl	%dx, %edx
	leal	(%rsi,%rdi,2), %esi
	leal	2(%rdx,%rsi), %edx
	shrl	$2, %edx
	movw	%dx, 2262(%rax)
	leal	1(%rcx,%rbp), %ecx
	shrl	%ecx
	movw	%cx, 2834(%rax)
	movw	%cx, 2768(%rax)
	movzwl	2(%rsp), %esi
	movzwl	4(%rsp), %ecx
	leal	1(%rsi,%rcx), %edx
	shrl	%edx
	movw	%dx, 2836(%rax)
	movw	%dx, 2770(%rax)
	movzwl	6(%rsp), %ebx
	leal	1(%rcx,%rbx), %edx
	shrl	%edx
	movw	%dx, 2838(%rax)
	movw	%dx, 2772(%rax)
	movzwl	8(%rsp), %r8d
	leal	1(%rbx,%r8), %edx
	shrl	%edx
	movw	%dx, 2774(%rax)
	movzwl	18(%rsp), %r9d
	movzwl	(%rsp), %edi
	leal	2(%r9,%rdi,2), %ebp
	addl	%esi, %ebp
	shrl	$2, %ebp
	movw	%bp, 2866(%rax)
	movw	%bp, 2800(%rax)
	leal	2(%rdi,%rsi,2), %edx
	addl	%ecx, %edx
	shrl	$2, %edx
	movw	%dx, 2868(%rax)
	movw	%dx, 2802(%rax)
	leal	(%rsi,%rcx,2), %edx
	leal	2(%rbx,%rdx), %edx
	shrl	$2, %edx
	movw	%dx, 2870(%rax)
	movw	%dx, 2804(%rax)
	leal	(%rcx,%rbx,2), %ecx
	leal	2(%r8,%rcx), %ecx
	shrl	$2, %ecx
	movw	%cx, 2806(%rax)
	movzwl	20(%rsp), %ecx
	leal	2(%rdi,%r9,2), %edx
	addl	%ecx, %edx
	shrl	$2, %edx
	movw	%dx, 2832(%rax)
	movzwl	22(%rsp), %esi
	leal	2(%r9,%rcx,2), %edx
	addl	%esi, %edx
	shrl	$2, %edx
	movw	%dx, 2864(%rax)
	movq	img(%rip), %rax
	leal	1(%rdi,%r9), %edx
	shrl	%edx
	movw	%dx, 3316(%rax)
	movw	%dx, 3280(%rax)
	movw	%bp, 3318(%rax)
	movw	%bp, 3282(%rax)
	movzwl	2(%rsp), %edx
	movzwl	4(%rsp), %ebp
	leal	2(%rdi,%rdx,2), %edi
	addl	%ebp, %edi
	shrl	$2, %edi
	movw	%di, 3284(%rax)
	movzwl	6(%rsp), %edi
	leal	(%rdx,%rbp,2), %edx
	leal	2(%rdi,%rdx), %edx
	shrl	$2, %edx
	movw	%dx, 3286(%rax)
	leal	1(%r9,%rcx), %edx
	shrl	%edx
	movw	%dx, 3348(%rax)
	movw	%dx, 3312(%rax)
	movzwl	(%rsp), %edx
	movzwl	18(%rsp), %edi
	leal	(%rdx,%rdi,2), %edx
	leal	2(%rcx,%rdx), %edx
	shrl	$2, %edx
	movw	%dx, 3350(%rax)
	movw	%dx, 3314(%rax)
	leal	1(%rcx,%rsi), %edx
	shrl	%edx
	movw	%dx, 3380(%rax)
	movw	%dx, 3344(%rax)
	leal	(%rdi,%rcx,2), %edx
	leal	2(%rsi,%rdx), %edx
	shrl	$2, %edx
	movw	%dx, 3382(%rax)
	movw	%dx, 3346(%rax)
	movzwl	24(%rsp), %edx
	leal	1(%rsi,%rdx), %edi
	shrl	%edi
	movw	%di, 3376(%rax)
	leal	(%rcx,%rsi,2), %ecx
	leal	2(%rdx,%rcx), %ecx
	shrl	$2, %ecx
	movw	%cx, 3378(%rax)
.LBB0_53:
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	intrapred_luma, .Lfunc_end0-intrapred_luma
	.cfi_endproc

	.globl	intrapred_luma_16x16
	.p2align	4, 0x90
	.type	intrapred_luma_16x16,@function
intrapred_luma_16x16:                   # @intrapred_luma_16x16
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$504, %rsp              # imm = 0x1F8
.Lcfi19:
	.cfi_def_cfa_offset 560
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %r14
	movq	img(%rip), %rax
	movl	12(%rax), %r15d
	leaq	96(%rsp), %rbx
	xorl	%ebp, %ebp
	movq	getNeighbour(%rip), %rax
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	leal	-1(%rbp), %edx
	movl	$-1, %esi
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	movq	%rbx, %r8
	callq	*%rax
	incq	%rbp
	movq	getNeighbour(%rip), %rax
	addq	$24, %rbx
	cmpq	$17, %rbp
	jne	.LBB1_1
# BB#2:
	xorl	%ebx, %ebx
	movq	%rsp, %r8
	xorl	%esi, %esi
	movl	$-1, %edx
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	callq	*%rax
	movq	input(%rip), %rax
	movl	(%rsp), %r15d
	cmpl	$0, 272(%rax)
	je	.LBB1_3
# BB#4:
	testl	%r15d, %r15d
	movq	img(%rip), %rcx
	je	.LBB1_6
# BB#5:
	movq	14240(%rcx), %rax
	movslq	4(%rsp), %rdx
	movl	(%rax,%rdx,4), %ebx
.LBB1_6:                                # %._crit_edge
	movl	$1, %eax
	movl	$52, %edx
	.p2align	4, 0x90
.LBB1_7:                                # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	cmpl	$0, 68(%rsp,%rdx)
	movl	$0, %edi
	je	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=1
	movq	14240(%rcx), %rdi
	movslq	72(%rsp,%rdx), %rbp
	movl	(%rdi,%rbp,4), %edi
.LBB1_9:                                #   in Loop: Header=BB1_7 Depth=1
	andl	%eax, %edi
	cmpl	$0, 92(%rsp,%rdx)
	je	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_7 Depth=1
	movq	14240(%rcx), %rax
	movslq	96(%rsp,%rdx), %rsi
	movl	(%rax,%rsi,4), %esi
.LBB1_11:                               #   in Loop: Header=BB1_7 Depth=1
	movl	%esi, %eax
	andl	%edi, %eax
	addq	$48, %rdx
	cmpq	$436, %rdx              # imm = 0x1B4
	jne	.LBB1_7
# BB#12:
	cmpl	$0, 96(%rsp)
	je	.LBB1_13
# BB#14:
	movq	14240(%rcx), %rcx
	movslq	100(%rsp), %rdx
	movl	(%rcx,%rdx,4), %r9d
	jmp	.LBB1_15
.LBB1_3:
	movl	96(%rsp), %r9d
	movl	120(%rsp), %eax
	jmp	.LBB1_16
.LBB1_13:
	xorl	%r9d, %r9d
.LBB1_15:
	movl	%ebx, %r15d
.LBB1_16:
	xorl	%edi, %edi
	testl	%r15d, %r15d
	movl	$0, %ebx
	je	.LBB1_30
# BB#17:                                # %.lr.ph
	movslq	16(%rsp), %rcx
	movslq	20(%rsp), %rdx
	movq	(%r14,%rdx,8), %rbp
	xorl	%ebx, %ebx
	testb	%bl, %bl
	je	.LBB1_19
# BB#18:
	movq	%rcx, %rdx
	jmp	.LBB1_28
.LBB1_19:                               # %min.iters.checked
	movl	$16, %r8d
	movl	$16, %r10d
	andq	$-8, %r10
	movl	$16, %r11d
	andq	$-8, %r11
	movq	%rcx, %rdx
	je	.LBB1_28
# BB#20:                                # %vector.body.preheader
	leaq	-8(%r11), %rbx
	movq	%rbx, %rdx
	shrq	$3, %rdx
	btl	$3, %ebx
	jb	.LBB1_21
# BB#22:                                # %vector.body.prol
	movq	(%rbp,%rcx,2), %xmm0    # xmm0 = mem[0],zero
	movq	8(%rbp,%rcx,2), %xmm1   # xmm1 = mem[0],zero
	pxor	%xmm2, %xmm2
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	movl	$8, %ebx
	testq	%rdx, %rdx
	jne	.LBB1_24
	jmp	.LBB1_26
.LBB1_21:
	pxor	%xmm0, %xmm0
	xorl	%ebx, %ebx
	pxor	%xmm1, %xmm1
	testq	%rdx, %rdx
	je	.LBB1_26
.LBB1_24:                               # %vector.body.preheader.new
	movq	%r11, %rdx
	subq	%rbx, %rdx
	addq	%rcx, %rbx
	leaq	24(%rbp,%rbx,2), %rbx
	pxor	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB1_25:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rbx), %xmm3        # xmm3 = mem[0],zero
	movq	-16(%rbx), %xmm4        # xmm4 = mem[0],zero
	punpcklwd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3]
	punpcklwd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	paddd	%xmm0, %xmm3
	paddd	%xmm1, %xmm4
	movq	-8(%rbx), %xmm0         # xmm0 = mem[0],zero
	movq	(%rbx), %xmm1           # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	paddd	%xmm3, %xmm0
	paddd	%xmm4, %xmm1
	addq	$32, %rbx
	addq	$-16, %rdx
	jne	.LBB1_25
.LBB1_26:                               # %middle.block
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ebx
	cmpq	%r11, %r8
	je	.LBB1_30
# BB#27:
	addq	%rcx, %r10
	movq	%r10, %rdx
.LBB1_28:                               # %scalar.ph.preheader
	addq	$15, %rcx
	decq	%rdx
	.p2align	4, 0x90
.LBB1_29:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rbp,%rdx,2), %esi
	addl	%esi, %ebx
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB1_29
.LBB1_30:                               # %.loopexit145
	testl	%eax, %eax
	je	.LBB1_33
# BB#31:                                # %.preheader143.preheader
	xorl	%edi, %edi
	movl	$68, %ecx
	.p2align	4, 0x90
.LBB1_32:                               # %.preheader143
                                        # =>This Inner Loop Header: Depth=1
	movslq	72(%rsp,%rcx), %rdx
	movq	(%r14,%rdx,8), %rdx
	movslq	68(%rsp,%rcx), %rsi
	movzwl	(%rdx,%rsi,2), %edx
	addl	%edi, %edx
	movslq	96(%rsp,%rcx), %rsi
	movq	(%r14,%rsi,8), %rsi
	movslq	92(%rsp,%rcx), %rdi
	movzwl	(%rsi,%rdi,2), %edi
	addl	%edx, %edi
	addq	$48, %rcx
	cmpq	$452, %rcx              # imm = 0x1C4
	jne	.LBB1_32
.LBB1_33:                               # %.loopexit144
	testl	%r15d, %r15d
	je	.LBB1_35
# BB#34:
	leal	8(%rbx), %edx
	testl	%eax, %eax
	setne	%cl
	leal	16(%rbx,%rdi), %edi
	cmovel	%edx, %edi
	orb	$4, %cl
	sarl	%cl, %edi
	testl	%eax, %eax
	movslq	20(%rsp), %rcx
	movq	(%r14,%rcx,8), %rcx
	movslq	16(%rsp), %rdx
	movups	16(%rcx,%rdx,2), %xmm0
	movaps	%xmm0, 48(%rsp)
	movdqu	(%rcx,%rdx,2), %xmm0
	movdqa	%xmm0, 32(%rsp)
	jne	.LBB1_37
	jmp	.LBB1_40
.LBB1_35:
	testl	%eax, %eax
	je	.LBB1_39
# BB#36:                                # %.thread
	addl	$8, %edi
	sarl	$4, %edi
.LBB1_37:                               # %.preheader142
	leaq	164(%rsp), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_38:                               # =>This Inner Loop Header: Depth=1
	movslq	-24(%rcx), %rsi
	movq	(%r14,%rsi,8), %rsi
	movslq	-28(%rcx), %rbp
	movzwl	(%rsi,%rbp,2), %esi
	movw	%si, 64(%rsp,%rdx,2)
	movslq	(%rcx), %rsi
	movq	(%r14,%rsi,8), %rsi
	movslq	-4(%rcx), %rbp
	movzwl	(%rsi,%rbp,2), %esi
	movw	%si, 66(%rsp,%rdx,2)
	addq	$2, %rdx
	addq	$48, %rcx
	cmpq	$16, %rdx
	jne	.LBB1_38
.LBB1_40:                               # %.preheader141
	movq	img(%rip), %r12
	movd	%edi, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	4816(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_41:                               # =>This Inner Loop Header: Depth=1
	movaps	32(%rsp), %xmm1
	movdqa	48(%rsp), %xmm2
	movdqu	%xmm2, 16(%rcx)
	movups	%xmm1, (%rcx)
	movzwl	64(%rsp,%rdx,2), %esi
	movd	%esi, %xmm1
	pshuflw	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm1, %xmm1       # xmm1 = xmm1[0,0,1,1]
	movdqu	%xmm1, 512(%rcx)
	movdqu	%xmm0, 1024(%rcx)
	movdqu	%xmm1, 528(%rcx)
	movdqu	%xmm0, 1040(%rcx)
	incq	%rdx
	addq	$32, %rcx
	cmpq	$16, %rdx
	jne	.LBB1_41
# BB#42:
	testl	%r15d, %r15d
	je	.LBB1_53
# BB#43:
	testl	%eax, %eax
	je	.LBB1_53
# BB#44:
	testl	%r9d, %r9d
	je	.LBB1_53
# BB#45:                                # %.preheader140
	movslq	20(%rsp), %rax
	movq	(%r14,%rax,8), %r11
	movslq	16(%rsp), %rax
	movslq	116(%rsp), %r8
	movl	112(%rsp), %r10d
	leal	6(%rax), %r9d
	leaq	284(%rsp), %rbx
	leaq	332(%rsp), %rsi
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	16(%r11,%rax,2), %r15
	xorl	%edx, %edx
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_46:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%r15,%rdx,2), %ecx
	cmpq	$7, %rdx
	movl	%r9d, %ebp
	movq	%r11, %rdi
	jne	.LBB1_48
# BB#47:                                #   in Loop: Header=BB1_46 Depth=1
	movq	(%r14,%r8,8), %rdi
	movl	%r10d, %ebp
.LBB1_48:                               #   in Loop: Header=BB1_46 Depth=1
	movslq	%ebp, %rbp
	movzwl	(%rdi,%rbp,2), %edi
	subl	%edi, %ecx
	incq	%rdx
	imull	%edx, %ecx
	addl	%ecx, %eax
	movslq	(%rsi), %rcx
	movq	(%r14,%rcx,8), %rcx
	movslq	-4(%rsi), %rdi
	movzwl	(%rcx,%rdi,2), %ecx
	movslq	(%rbx), %rdi
	movq	(%r14,%rdi,8), %rdi
	movslq	-4(%rbx), %rbp
	movzwl	(%rdi,%rbp,2), %edi
	subl	%edi, %ecx
	imull	%edx, %ecx
	addl	%ecx, %r13d
	decl	%r9d
	addq	$-24, %rbx
	addq	$24, %rsi
	cmpq	$8, %rdx
	jne	.LBB1_46
# BB#49:
	leal	32(%rax,%rax,4), %eax
	sarl	$6, %eax
	leal	32(%r13,%r13,4), %r8d
	sarl	$6, %r8d
	movq	24(%rsp), %rcx          # 8-byte Reload
	movzwl	30(%r11,%rcx,2), %ecx
	movslq	500(%rsp), %rdx
	movq	(%r14,%rdx,8), %rdx
	movslq	496(%rsp), %rsi
	movzwl	(%rdx,%rsi,2), %r9d
	addl	%ecx, %r9d
	shll	$4, %r9d
	addl	$16, %r9d
	leaq	6354(%r12), %r10
	imull	$-6, %eax, %r14d
	leal	(,%r8,8), %ecx
	subl	%r8d, %ecx
	subl	%ecx, %r14d
	leal	(%rax,%rax), %r15d
	imull	$-7, %r8d, %ebx
	leal	(,%rax,8), %ecx
	subl	%eax, %ecx
	subl	%ecx, %ebx
	xorl	%eax, %eax
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB1_50:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_51 Depth 2
	movl	%r9d, %esi
	movl	$16, %edx
	movq	%r10, %rcx
	.p2align	4, 0x90
.LBB1_51:                               #   Parent Loop BB1_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	15520(%r12), %edi
	leal	(%rbx,%rsi), %ebp
	sarl	$5, %ebp
	cmovsl	%eax, %ebp
	cmpl	%edi, %ebp
	cmovlw	%bp, %di
	movw	%di, -2(%rcx)
	movl	15520(%r12), %edi
	leal	(%r14,%rsi), %ebp
	sarl	$5, %ebp
	cmovsl	%eax, %ebp
	cmpl	%edi, %ebp
	cmovlw	%bp, %di
	movw	%di, (%rcx)
	addq	$4, %rcx
	addl	%r15d, %esi
	addq	$-2, %rdx
	jne	.LBB1_51
# BB#52:                                #   in Loop: Header=BB1_50 Depth=1
	incq	%r11
	addq	$32, %r10
	addl	%r8d, %r9d
	cmpq	$16, %r11
	jne	.LBB1_50
.LBB1_53:                               # %.loopexit
	addq	$504, %rsp              # imm = 0x1F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_39:                               # %.thread137
	movq	img(%rip), %rcx
	movl	15512(%rcx), %edi
	jmp	.LBB1_40
.Lfunc_end1:
	.size	intrapred_luma_16x16, .Lfunc_end1-intrapred_luma_16x16
	.cfi_endproc

	.globl	dct_luma_16x16
	.p2align	4, 0x90
	.type	dct_luma_16x16,@function
dct_luma_16x16:                         # @dct_luma_16x16
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$64, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 120
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	img(%rip), %r15
	movq	14168(%r15), %rdx
	movq	14224(%r15), %rax
	movslq	12(%r15), %rsi
	movq	(%rdx), %rdx
	movq	(%rdx), %rcx
	movq	8(%rdx), %rdx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	imulq	$536, %rsi, %rsi        # imm = 0x218
	movl	15452(%r15), %edx
	addl	8(%rax,%rsi), %edx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	jne	.LBB2_1
# BB#2:
	cmpl	$1, 15540(%r15)
	sete	%cl
	movl	%ecx, -108(%rsp)        # 4-byte Spill
	jmp	.LBB2_3
.LBB2_1:
	movl	$0, -108(%rsp)          # 4-byte Folded Spill
.LBB2_3:
	movl	428(%rax,%rsi), %r8d
	movq	qp_per_matrix(%rip), %rax
	movslq	%edx, %rdx
	movslq	(%rax,%rdx,4), %rcx
	movq	qp_rem_matrix(%rip), %rax
	movq	LevelScale4x4Luma(%rip), %rsi
	movq	8(%rsi), %rsi
	movslq	(%rax,%rdx,4), %rax
	movq	(%rsi,%rax,8), %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	InvLevelScale4x4Luma(%rip), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	LevelOffset4x4Luma(%rip), %rax
	movq	8(%rax), %rax
	movq	%rcx, -80(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	imgY_org(%rip), %r9
	movslq	%edi, %rax
	shlq	$9, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%r15,%rax), %r14
	xorl	%r11d, %r11d
	movl	$4816, %r10d            # imm = 0x12D0
	.p2align	4, 0x90
.LBB2_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_5 Depth 2
	movl	%r11d, %edx
	sarl	$2, %edx
	movslq	%r11d, %rax
	movl	%r11d, %r12d
	andl	$3, %r12d
	movslq	196(%r15), %rdi
	addq	%rax, %rdi
	movq	(%r9,%rdi,8), %r13
	movslq	%edx, %rax
	movq	%r10, %rbp
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_5:                                #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	192(%r15), %ecx
	addl	%edi, %ecx
	movslq	%ecx, %rcx
	movzwl	(%r13,%rcx,2), %ecx
	movzwl	(%r14,%rbp), %esi
	subl	%esi, %ecx
	movl	%ecx, dct_luma_16x16.M1-9632(%rbp,%rbp)
	movl	%edi, %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	movl	%edi, %ebx
	andl	$3, %ebx
	movq	%rax, %rdx
	shlq	$8, %rdx
	shlq	$6, %rsi
	addq	%rdx, %rsi
	movq	%r12, %rdx
	shlq	$4, %rdx
	addq	%rsi, %rdx
	movl	%ecx, dct_luma_16x16.M0(%rdx,%rbx,4)
	incq	%rdi
	addq	$2, %rbp
	cmpq	$16, %rdi
	jne	.LBB2_5
# BB#6:                                 #   in Loop: Header=BB2_4 Depth=1
	incq	%r11
	addq	$32, %r10
	cmpq	$16, %r11
	jne	.LBB2_4
# BB#7:
	testl	%r8d, %r8d
	movl	$FIELD_SCAN, %eax
	movl	$SNGL_SCAN, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	movq	-80(%rsp), %r9          # 8-byte Reload
	leal	15(%r9), %eax
	movl	%eax, -84(%rsp)         # 4-byte Spill
	movl	-108(%rsp), %ebx        # 4-byte Reload
	testb	%bl, %bl
	je	.LBB2_8
# BB#23:                                # %.preheader532.preheader
	movl	dct_luma_16x16.M0(%rip), %eax
	movl	%eax, dct_luma_16x16.M4(%rip)
	movl	dct_luma_16x16.M0+64(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+4(%rip)
	movl	dct_luma_16x16.M0+128(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+8(%rip)
	movl	dct_luma_16x16.M0+192(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+12(%rip)
	movl	dct_luma_16x16.M0+256(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+16(%rip)
	movl	dct_luma_16x16.M0+320(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+20(%rip)
	movl	dct_luma_16x16.M0+384(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+24(%rip)
	movl	dct_luma_16x16.M0+448(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+28(%rip)
	movl	dct_luma_16x16.M0+512(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+32(%rip)
	movl	dct_luma_16x16.M0+576(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+36(%rip)
	movl	dct_luma_16x16.M0+640(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+40(%rip)
	movl	dct_luma_16x16.M0+704(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+44(%rip)
	movl	dct_luma_16x16.M0+768(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+48(%rip)
	movl	dct_luma_16x16.M0+832(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+52(%rip)
	movl	dct_luma_16x16.M0+896(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+56(%rip)
	movl	dct_luma_16x16.M0+960(%rip), %eax
	movl	%eax, dct_luma_16x16.M4+60(%rip)
	xorl	%eax, %eax
	movl	$-1, %r11d
	movq	input(%rip), %r8
	xorl	%esi, %esi
	movq	-64(%rsp), %r10         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_24:                               # =>This Inner Loop Header: Depth=1
	incl	%r11d
	movq	-40(%rsp), %rdx         # 8-byte Reload
	movzbl	1(%rdx,%rax,2), %ecx
	movzbl	(%rdx,%rax,2), %edx
	shlq	$4, %rcx
	movl	dct_luma_16x16.M4(%rcx,%rdx,4), %ebp
	movl	%ebp, %edi
	negl	%edi
	cmovll	%ebp, %edi
	cmpl	$0, 4008(%r8)
	je	.LBB2_25
.LBB2_27:                               #   in Loop: Header=BB2_24 Depth=1
	testl	%edi, %edi
	movl	%edi, %edx
	jne	.LBB2_28
	jmp	.LBB2_29
	.p2align	4, 0x90
.LBB2_25:                               #   in Loop: Header=BB2_24 Depth=1
	cmpl	$2064, %edi             # imm = 0x810
	jl	.LBB2_27
# BB#26:                                #   in Loop: Header=BB2_24 Depth=1
	movl	$2063, %edx             # imm = 0x80F
	cmpl	$10, 36(%r15)
	jge	.LBB2_27
.LBB2_28:                               # %.thread513
                                        #   in Loop: Header=BB2_24 Depth=1
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%ebp, %ebp
	cmovnsl	%ecx, %edx
	movslq	%esi, %rsi
	movl	%edx, (%r10,%rsi,4)
	movq	-56(%rsp), %rcx         # 8-byte Reload
	movl	%r11d, (%rcx,%rsi,4)
	incl	%esi
	movl	$-1, %r11d
.LBB2_29:                               #   in Loop: Header=BB2_24 Depth=1
	incq	%rax
	cmpq	$16, %rax
	jne	.LBB2_24
# BB#30:
	movslq	%esi, %rax
	movl	$0, (%r10,%rax,4)
	leal	16(%r9), %eax
	movl	%eax, -128(%rsp)        # 4-byte Spill
	jmp	.LBB2_32
.LBB2_8:                                # %.preheader542.preheader
	movl	%ebx, -108(%rsp)        # 4-byte Spill
	xorl	%ecx, %ecx
	movl	$dct_luma_16x16.M0+60, %edi
	.p2align	4, 0x90
.LBB2_9:                                # %.preheader542
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_10 Depth 2
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	movl	$4, %ecx
	movq	%rdi, -32(%rsp)         # 8-byte Spill
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB2_10:                               # %.preheader541
                                        #   Parent Loop BB2_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, -128(%rsp)        # 8-byte Spill
	movl	-48(%rbx), %edx
	movl	-60(%rbx), %r9d
	movl	-56(%rbx), %esi
	leal	(%rdx,%r9), %ecx
	movl	-52(%rbx), %edi
	leal	(%rdi,%rsi), %ebp
	subl	%edi, %esi
	subl	%edx, %r9d
	leal	(%rbp,%rcx), %r11d
	subl	%ebp, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	leal	(%rsi,%r9,2), %ecx
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	addl	%esi, %esi
	subl	%esi, %r9d
	movl	-44(%rbx), %esi
	movl	-32(%rbx), %edx
	leal	(%rdx,%rsi), %r8d
	movl	-40(%rbx), %edi
	movl	-36(%rbx), %ecx
	leal	(%rcx,%rdi), %ebp
	subl	%ecx, %edi
	subl	%edx, %esi
	leal	(%rbp,%r8), %r10d
	subl	%ebp, %r8d
	movq	%r8, -72(%rsp)          # 8-byte Spill
	leal	(%rdi,%rsi,2), %ecx
	movq	%rcx, -120(%rsp)        # 8-byte Spill
	addl	%edi, %edi
	subl	%edi, %esi
	movl	-28(%rbx), %r8d
	movl	-16(%rbx), %ecx
	leal	(%rcx,%r8), %r12d
	movl	-24(%rbx), %edx
	movl	-20(%rbx), %edi
	leal	(%rdi,%rdx), %ebp
	subl	%edi, %edx
	subl	%ecx, %r8d
	leal	(%rbp,%r12), %eax
	subl	%ebp, %r12d
	leal	(%rdx,%r8,2), %ecx
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	addl	%edx, %edx
	subl	%edx, %r8d
	movl	-8(%rbx), %ebp
	movl	-4(%rbx), %edx
	leal	(%rdx,%rbp), %ecx
	subl	%edx, %ebp
	movl	-12(%rbx), %r15d
	movl	(%rbx), %edx
	leal	(%rdx,%r15), %r13d
	subl	%edx, %r15d
	leal	(%rcx,%r13), %r14d
	subl	%ecx, %r13d
	leal	(%rbp,%r15,2), %edx
	addl	%ebp, %ebp
	subl	%ebp, %r15d
	movq	%r11, %rdi
	leal	(%r14,%rdi), %ecx
	leal	(%rax,%r10), %ebp
	leal	(%rbp,%rcx), %r11d
	movl	%r11d, -60(%rbx)
	movq	-104(%rsp), %r11        # 8-byte Reload
	subl	%ebp, %ecx
	movl	%ecx, -28(%rbx)
	movq	%r10, (%rsp)            # 8-byte Spill
	movl	%r10d, %ecx
	movq	-120(%rsp), %r10        # 8-byte Reload
	movq	%rax, -48(%rsp)         # 8-byte Spill
	subl	%eax, %ecx
	movq	%rdi, %rbp
	movq	%rbp, -8(%rsp)          # 8-byte Spill
	movq	%r14, -16(%rsp)         # 8-byte Spill
	subl	%r14d, %ebp
	leal	(%rcx,%rbp,2), %edi
	movl	%edi, -44(%rbx)
	addl	%ecx, %ecx
	subl	%ecx, %ebp
	movl	%ebp, -12(%rbx)
	leal	(%rdx,%r11), %ecx
	movq	-96(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r10), %edi
	leal	(%rdi,%rcx), %ebp
	movl	%ebp, -56(%rbx)
	subl	%edi, %ecx
	movl	%ecx, -24(%rbx)
	movl	%r10d, %ecx
	movq	-72(%rsp), %r10         # 8-byte Reload
	movq	%rax, %rdi
	subl	%edi, %ecx
	movl	%r11d, %edi
	movq	%rdx, %r14
	subl	%edx, %edi
	leal	(%rcx,%rdi,2), %ebp
	movl	%ebp, -40(%rbx)
	addl	%ecx, %ecx
	subl	%ecx, %edi
	movl	%edi, -8(%rbx)
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%r13,%rax), %ecx
	leal	(%r12,%r10), %edi
	leal	(%rdi,%rcx), %ebp
	movl	%ebp, -52(%rbx)
	subl	%edi, %ecx
	movl	%ecx, -20(%rbx)
	movl	%r10d, %ecx
	subl	%r12d, %ecx
	movq	%rax, %rdx
	movl	%eax, %edi
	subl	%r13d, %edi
	leal	(%rcx,%rdi,2), %ebp
	movl	%ebp, -36(%rbx)
	addl	%ecx, %ecx
	subl	%ecx, %edi
	movl	%edi, -4(%rbx)
	leal	(%r15,%r9), %ecx
	leal	(%r8,%rsi), %edi
	leal	(%rdi,%rcx), %ebp
	movl	%ebp, -48(%rbx)
	subl	%edi, %ecx
	movl	%ecx, -16(%rbx)
	movl	%esi, %ecx
	subl	%r8d, %ecx
	movl	%r9d, %edi
	subl	%r15d, %edi
	leal	(%rcx,%rdi,2), %ebp
	movl	%ebp, -32(%rbx)
	addl	%ecx, %ecx
	subl	%ecx, %edi
	movq	-128(%rsp), %rcx        # 8-byte Reload
	movl	%edi, (%rbx)
	addq	$64, %rbx
	decq	%rcx
	jne	.LBB2_10
# BB#11:                                #   in Loop: Header=BB2_9 Depth=1
	movq	%r14, -128(%rsp)        # 8-byte Spill
	movq	-120(%rsp), %rbp        # 8-byte Reload
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	-24(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movq	-32(%rsp), %rdi         # 8-byte Reload
	addq	$256, %rdi              # imm = 0x100
	cmpq	$4, %rcx
	jne	.LBB2_9
# BB#12:                                # %.preheader539
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, dct_luma_16x16.M4(%rip)
	movl	%edx, dct_luma_16x16.M4+8(%rip)
	movq	-104(%rsp), %rcx        # 8-byte Reload
	movl	%ecx, dct_luma_16x16.M4+4(%rip)
	movl	%r9d, dct_luma_16x16.M4+12(%rip)
	movl	%ebx, dct_luma_16x16.M4+16(%rip)
	movl	%r10d, dct_luma_16x16.M4+24(%rip)
	movl	%ebp, dct_luma_16x16.M4+20(%rip)
	movl	%esi, dct_luma_16x16.M4+28(%rip)
	movq	-48(%rsp), %rax         # 8-byte Reload
	movl	%eax, dct_luma_16x16.M4+32(%rip)
	movl	%r12d, dct_luma_16x16.M4+40(%rip)
	movq	-96(%rsp), %rax         # 8-byte Reload
	movl	%eax, dct_luma_16x16.M4+36(%rip)
	movl	%r8d, dct_luma_16x16.M4+44(%rip)
	movq	-16(%rsp), %rax         # 8-byte Reload
	movl	%eax, dct_luma_16x16.M4+48(%rip)
	movl	%r13d, dct_luma_16x16.M4+56(%rip)
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, dct_luma_16x16.M4+52(%rip)
	movl	%r15d, dct_luma_16x16.M4+60(%rip)
	movl	dct_luma_16x16.M0(%rip), %r13d
	movl	dct_luma_16x16.M0+64(%rip), %edi
	movl	dct_luma_16x16.M0+128(%rip), %ebp
	movl	dct_luma_16x16.M0+192(%rip), %esi
	movl	dct_luma_16x16.M0+256(%rip), %r15d
	movl	dct_luma_16x16.M0+320(%rip), %eax
	movl	dct_luma_16x16.M0+384(%rip), %edx
	movl	dct_luma_16x16.M0+448(%rip), %r12d
	movl	dct_luma_16x16.M0+512(%rip), %r14d
	movl	dct_luma_16x16.M0+576(%rip), %ecx
	movl	dct_luma_16x16.M0+640(%rip), %r10d
	movl	dct_luma_16x16.M0+704(%rip), %ebx
	movq	%rbx, -128(%rsp)        # 8-byte Spill
	movl	dct_luma_16x16.M0+832(%rip), %r11d
	movl	dct_luma_16x16.M0+896(%rip), %ebx
	movq	%rbx, -104(%rsp)        # 8-byte Spill
	leal	(%rbp,%rdi), %ebx
	subl	%ebp, %edi
	leal	(%rsi,%r13), %ebp
	subl	%esi, %r13d
	leal	(%rbx,%rbp), %esi
	movq	%rsi, -120(%rsp)        # 8-byte Spill
	subl	%ebx, %ebp
	leal	(%rdi,%r13), %ebx
	subl	%edi, %r13d
	leal	(%rdx,%rax), %esi
	subl	%edx, %eax
	leal	(%r12,%r15), %r8d
	subl	%r12d, %r15d
	leal	(%rsi,%r8), %edx
	movq	%rdx, -96(%rsp)         # 8-byte Spill
	subl	%esi, %r8d
	leal	(%rax,%r15), %r9d
	subl	%eax, %r15d
	leal	(%r10,%rcx), %edi
	subl	%r10d, %ecx
	movq	-128(%rsp), %rax        # 8-byte Reload
	leal	(%rax,%r14), %r10d
	subl	%eax, %r14d
	leal	(%rdi,%r10), %r12d
	subl	%edi, %r10d
	leal	(%rcx,%r14), %eax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	subl	%ecx, %r14d
	movq	-104(%rsp), %rax        # 8-byte Reload
	leal	(%rax,%r11), %edx
	subl	%eax, %r11d
	movl	dct_luma_16x16.M0+768(%rip), %ecx
	movl	dct_luma_16x16.M0+960(%rip), %eax
	leal	(%rax,%rcx), %edi
	subl	%eax, %ecx
	leal	(%rdx,%rdi), %esi
	subl	%edx, %edi
	leal	(%r11,%rcx), %edx
	subl	%r11d, %ecx
	movq	-96(%rsp), %rax         # 8-byte Reload
	leal	(%r12,%rax), %r11d
	subl	%r12d, %eax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movq	-120(%rsp), %rax        # 8-byte Reload
	leal	(%rsi,%rax), %r12d
	subl	%esi, %eax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	leal	(%r11,%r12), %esi
	sarl	%esi
	movl	%esi, dct_luma_16x16.M4(%rip)
	subl	%r11d, %r12d
	movq	-72(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r9), %esi
	movl	%esi, -128(%rsp)        # 4-byte Spill
	subl	%eax, %r9d
	leal	(%rdx,%rbx), %r11d
	subl	%edx, %ebx
	leal	(%r9,%rbx), %eax
	movl	%eax, -104(%rsp)        # 4-byte Spill
	subl	%r9d, %ebx
	sarl	%ebx
	movl	%ebx, dct_luma_16x16.M4+52(%rip)
	leal	(%r10,%r8), %ebx
	subl	%r10d, %r8d
	leal	(%rdi,%rbp), %esi
	subl	%edi, %ebp
	leal	(%r8,%rbp), %edi
	subl	%r8d, %ebp
	sarl	%ebp
	movl	%ebp, dct_luma_16x16.M4+56(%rip)
	leal	(%r14,%r15), %eax
	subl	%r14d, %r15d
	leal	(%rcx,%r13), %edx
	subl	%ecx, %r13d
	movd	-96(%rsp), %xmm0        # 4-byte Folded Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movd	%ebx, %xmm1
	movd	-128(%rsp), %xmm2       # 4-byte Folded Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movd	-120(%rsp), %xmm3       # 4-byte Folded Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movd	%esi, %xmm4
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movd	%eax, %xmm0
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	punpckldq	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movd	%edx, %xmm0
	movd	%r11d, %xmm1
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	punpckldq	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	movdqa	%xmm2, %xmm0
	paddd	%xmm1, %xmm0
	psrad	$1, %xmm0
	movdqu	%xmm0, dct_luma_16x16.M4+4(%rip)
	psubd	%xmm2, %xmm1
	psrad	$1, %xmm1
	movdqu	%xmm1, dct_luma_16x16.M4+36(%rip)
	leal	(%r15,%r13), %ecx
	movd	%r12d, %xmm0
	movd	%edi, %xmm1
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movd	%ecx, %xmm0
	movd	-104(%rsp), %xmm2       # 4-byte Folded Reload
                                        # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	psrad	$1, %xmm2
	movdqu	%xmm2, dct_luma_16x16.M4+20(%rip)
	movl	%r13d, dct_luma_16x16.M5.3(%rip)
	movl	%r13d, %ecx
	subl	%r15d, %ecx
	sarl	%ecx
	movl	%ecx, dct_luma_16x16.M4+60(%rip)
	movl	%edx, dct_luma_16x16.M5.0(%rip)
	movl	%eax, dct_luma_16x16.M5.1(%rip)
	movl	%r15d, dct_luma_16x16.M5.2(%rip)
	movq	-80(%rsp), %rax         # 8-byte Reload
	leal	16(%rax), %eax
	movl	%eax, -128(%rsp)        # 4-byte Spill
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r9
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r10
	movq	input(%rip), %r11
	xorl	%ebp, %ebp
	movl	$-1, %eax
	movq	img(%rip), %r8
	xorl	%ebx, %ebx
	movq	-64(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_13:                               # =>This Inner Loop Header: Depth=1
	incl	%eax
	movq	-40(%rsp), %rdx         # 8-byte Reload
	movzbl	1(%rdx,%rbp,2), %ecx
	movzbl	(%rdx,%rbp,2), %esi
	shlq	$4, %rcx
	leaq	dct_luma_16x16.M4(%rcx,%rsi,4), %r15
	movl	dct_luma_16x16.M4(%rcx,%rsi,4), %esi
	movl	%esi, %ecx
	negl	%ecx
	cmovll	%esi, %ecx
	imull	(%r9), %ecx
	movl	(%r10), %edi
	leal	(%rcx,%rdi,2), %edi
	movl	-128(%rsp), %ecx        # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edi
	cmpl	$0, 4008(%r11)
	je	.LBB2_14
.LBB2_16:                               #   in Loop: Header=BB2_13 Depth=1
	testl	%edi, %edi
	movl	%edi, %ecx
	jne	.LBB2_18
# BB#17:                                #   in Loop: Header=BB2_13 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB2_19
	.p2align	4, 0x90
.LBB2_14:                               #   in Loop: Header=BB2_13 Depth=1
	cmpl	$2064, %edi             # imm = 0x810
	jl	.LBB2_16
# BB#15:                                #   in Loop: Header=BB2_13 Depth=1
	movl	$2063, %ecx             # imm = 0x80F
	cmpl	$10, 36(%r8)
	jge	.LBB2_16
.LBB2_18:                               # %.thread
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	%ecx, %edi
	negl	%edi
	cmovll	%ecx, %edi
	movl	%edi, %edx
	negl	%edx
	testl	%esi, %esi
	cmovnsl	%edi, %edx
	movslq	%ebx, %rbx
	movl	%edx, (%r13,%rbx,4)
	movq	-56(%rsp), %rdx         # 8-byte Reload
	movl	%eax, (%rdx,%rbx,4)
	incl	%ebx
	movl	(%r15), %esi
	movl	$-1, %eax
.LBB2_19:                               #   in Loop: Header=BB2_13 Depth=1
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movl	%edx, %ecx
	negl	%ecx
	testl	%esi, %esi
	cmovnsl	%edx, %ecx
	movl	%ecx, (%r15)
	incq	%rbp
	cmpq	$16, %rbp
	jne	.LBB2_13
# BB#20:                                # %.preheader534729
	movslq	%ebx, %rax
	movl	$0, (%r13,%rax,4)
	movl	dct_luma_16x16.M4(%rip), %eax
	movl	dct_luma_16x16.M4+8(%rip), %ecx
	leal	(%rcx,%rax), %edx
	subl	%ecx, %eax
	movl	dct_luma_16x16.M4+4(%rip), %ecx
	movl	dct_luma_16x16.M4+12(%rip), %esi
	movl	%ecx, %edi
	subl	%esi, %edi
	addl	%ecx, %esi
	leal	(%rdx,%rsi), %r10d
	movl	%r10d, dct_luma_16x16.M4(%rip)
	leal	(%rdi,%rax), %ecx
	movl	%ecx, dct_luma_16x16.M4+4(%rip)
	subl	%edi, %eax
	movl	%eax, dct_luma_16x16.M4+8(%rip)
	subl	%esi, %edx
	movl	%edx, dct_luma_16x16.M4+12(%rip)
	movl	dct_luma_16x16.M4+16(%rip), %eax
	movl	dct_luma_16x16.M4+24(%rip), %ecx
	leal	(%rcx,%rax), %edx
	subl	%ecx, %eax
	movl	dct_luma_16x16.M4+20(%rip), %ecx
	movl	dct_luma_16x16.M4+28(%rip), %esi
	movl	%ecx, %edi
	subl	%esi, %edi
	addl	%ecx, %esi
	leal	(%rdx,%rsi), %r8d
	movl	%r8d, dct_luma_16x16.M4+16(%rip)
	leal	(%rdi,%rax), %ecx
	movl	%ecx, dct_luma_16x16.M4+20(%rip)
	subl	%edi, %eax
	movl	%eax, dct_luma_16x16.M4+24(%rip)
	subl	%esi, %edx
	movl	%edx, dct_luma_16x16.M4+28(%rip)
	movl	dct_luma_16x16.M4+32(%rip), %eax
	movl	dct_luma_16x16.M4+40(%rip), %ecx
	leal	(%rcx,%rax), %edx
	subl	%ecx, %eax
	movl	dct_luma_16x16.M4+36(%rip), %ecx
	movl	dct_luma_16x16.M4+44(%rip), %esi
	movl	%ecx, %edi
	subl	%esi, %edi
	addl	%ecx, %esi
	leal	(%rdx,%rsi), %ecx
	movl	%ecx, dct_luma_16x16.M4+32(%rip)
	leal	(%rdi,%rax), %ebp
	movl	%ebp, dct_luma_16x16.M4+36(%rip)
	subl	%edi, %eax
	movl	%eax, dct_luma_16x16.M4+40(%rip)
	subl	%esi, %edx
	movl	%edx, dct_luma_16x16.M4+44(%rip)
	movl	dct_luma_16x16.M4+48(%rip), %eax
	movl	dct_luma_16x16.M4+56(%rip), %edx
	leal	(%rdx,%rax), %esi
	subl	%edx, %eax
	movl	dct_luma_16x16.M4+52(%rip), %edx
	movl	dct_luma_16x16.M4+60(%rip), %edi
	movl	%edx, %ebp
	subl	%edi, %ebp
	addl	%edx, %edi
	leal	(%rsi,%rdi), %edx
	movl	%edx, dct_luma_16x16.M4+48(%rip)
	leal	(%rbp,%rax), %ebx
	movl	%ebx, dct_luma_16x16.M4+52(%rip)
	movl	%eax, dct_luma_16x16.M6.1(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%ebp, %eax
	movl	%eax, dct_luma_16x16.M4+56(%rip)
	movl	%esi, dct_luma_16x16.M6.0(%rip)
	movl	%esi, %eax
	subl	%edi, %eax
	movl	%eax, dct_luma_16x16.M4+60(%rip)
	movl	%ebp, dct_luma_16x16.M6.2(%rip)
	movl	%edi, dct_luma_16x16.M6.3(%rip)
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r9
	movq	$-192, %rbp
	movl	$dct_luma_16x16.M4+52, %ebx
	jmp	.LBB2_21
	.p2align	4, 0x90
.LBB2_22:                               # %._crit_edge
                                        #   in Loop: Header=BB2_21 Depth=1
	movl	-48(%rbx), %r10d
	movl	-32(%rbx), %r8d
	movl	-16(%rbx), %ecx
	movl	(%rbx), %edx
	addq	$64, %rbp
	addq	$4, %rbx
.LBB2_21:                               # =>This Inner Loop Header: Depth=1
	leal	(%rcx,%r10), %esi
	subl	%ecx, %r10d
	movl	%r8d, %eax
	subl	%edx, %eax
	addl	%r8d, %edx
	leal	(%rsi,%rdx), %edi
	imull	(%r9), %edi
	movq	-80(%rsp), %rcx         # 8-byte Reload
	shll	%cl, %edi
	addl	$32, %edi
	sarl	$6, %edi
	movl	%edi, dct_luma_16x16.M0+192(%rbp)
	leal	(%rax,%r10), %edi
	imull	(%r9), %edi
	shll	%cl, %edi
	addl	$32, %edi
	sarl	$6, %edi
	movl	%edi, dct_luma_16x16.M0+448(%rbp)
	movl	%r10d, %edi
	subl	%eax, %edi
	imull	(%r9), %edi
	shll	%cl, %edi
	addl	$32, %edi
	sarl	$6, %edi
	movl	%edi, dct_luma_16x16.M0+704(%rbp)
	movl	%esi, %edi
	subl	%edx, %edi
	imull	(%r9), %edi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edi
	addl	$32, %edi
	sarl	$6, %edi
	movl	%edi, dct_luma_16x16.M0+960(%rbp)
	testq	%rbp, %rbp
	jne	.LBB2_22
# BB#31:                                # %.preheader530.loopexit
	movl	%esi, dct_luma_16x16.M6.0(%rip)
	movl	%r10d, dct_luma_16x16.M6.1(%rip)
	movl	%eax, dct_luma_16x16.M6.2(%rip)
	movl	%edx, dct_luma_16x16.M6.3(%rip)
	movq	-80(%rsp), %r9          # 8-byte Reload
	movl	-108(%rsp), %ebx        # 4-byte Reload
.LBB2_32:                               # %.preheader530
	movl	$1, %eax
	movl	-84(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movl	%eax, -72(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB2_33:                               # %.preheader529
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_34 Depth 2
                                        #       Child Loop BB2_36 Depth 3
                                        #       Child Loop BB2_47 Depth 3
	movl	%eax, %ecx
	andl	$-2, %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	leaq	(%rax,%rax), %rcx
	andl	$2, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	leaq	(,%rax,4), %rcx
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	xorl	%r14d, %r14d
	movq	%rax, -48(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_34:                               # %.preheader528
                                        #   Parent Loop BB2_33 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_36 Depth 3
                                        #       Child Loop BB2_47 Depth 3
	movq	%r14, %rdi
	shlq	$6, %rdi
	movq	%rax, %rbp
	shlq	$8, %rbp
	movaps	dct_luma_16x16.M0(%rbp,%rdi), %xmm0
	movaps	%xmm0, dct_luma_16x16.M4(%rip)
	movaps	dct_luma_16x16.M0+16(%rbp,%rdi), %xmm0
	movaps	%xmm0, dct_luma_16x16.M4+16(%rip)
	movaps	dct_luma_16x16.M0+32(%rbp,%rdi), %xmm0
	movaps	%xmm0, dct_luma_16x16.M4+32(%rip)
	movdqa	dct_luma_16x16.M0+48(%rbp,%rdi), %xmm0
	movdqa	%xmm0, dct_luma_16x16.M4+48(%rip)
	movl	%r14d, %eax
	sarl	%eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	movl	%r14d, %ecx
	andl	$1, %ecx
	orq	(%rsp), %rcx            # 8-byte Folded Reload
	movq	img(%rip), %r15
	movq	14160(%r15), %rdx
	cltq
	movq	(%rdx,%rax,8), %rax
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %r13
	movq	8(%rax), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	leaq	(,%r14,4), %r11
	testb	%bl, %bl
	je	.LBB2_35
# BB#46:                                # %.preheader524.preheader
                                        #   in Loop: Header=BB2_34 Depth=2
	movl	%ebx, %r12d
	xorl	%eax, %eax
	movl	$-1, %r8d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_47:                               # %.preheader524
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-40(%rsp), %rdx         # 8-byte Reload
	movzbl	2(%rdx,%rax,2), %esi
	movzbl	3(%rdx,%rax,2), %edx
	incl	%r8d
	movq	%rdx, %rdi
	shlq	$4, %rdi
	movl	dct_luma_16x16.M4(%rdi,%rsi,4), %ebp
	movl	%ebp, %edi
	sarl	$31, %edi
	leal	(%rbp,%rdi), %ebx
	xorl	%edi, %ebx
	je	.LBB2_49
# BB#48:                                #   in Loop: Header=BB2_47 Depth=3
	negl	%ebx
	testl	%ebp, %ebp
	cmovnsl	%ebp, %ebx
	movslq	%ecx, %rcx
	movl	%ebx, (%r13,%rcx,4)
	movq	-120(%rsp), %rdi        # 8-byte Reload
	movl	%r8d, (%rdi,%rcx,4)
	incl	%ecx
	movl	$-1, %r8d
	movl	$15, %r10d
.LBB2_49:                               #   in Loop: Header=BB2_47 Depth=3
	cmpl	$0, 15260(%r15)
	je	.LBB2_51
# BB#50:                                #   in Loop: Header=BB2_47 Depth=3
	movq	14176(%r15), %rdi
	movq	16(%rdi), %rdi
	addl	-96(%rsp), %edx         # 4-byte Folded Reload
	movslq	%edx, %rdx
	movq	(%rdi,%rdx,8), %rdx
	addl	%r11d, %esi
	movslq	%esi, %rsi
	movl	$0, (%rdx,%rsi,4)
.LBB2_51:                               #   in Loop: Header=BB2_47 Depth=3
	incq	%rax
	cmpq	$15, %rax
	jne	.LBB2_47
# BB#52:                                #   in Loop: Header=BB2_34 Depth=2
	movslq	%ecx, %rax
	movl	$0, (%r13,%rax,4)
	movq	-48(%rsp), %rax         # 8-byte Reload
	movl	%r12d, %ebx
	movq	24(%rsp), %r12          # 8-byte Reload
	jmp	.LBB2_53
	.p2align	4, 0x90
.LBB2_35:                               # %.preheader527.preheader
                                        #   in Loop: Header=BB2_34 Depth=2
	movq	%r14, -8(%rsp)          # 8-byte Spill
	movl	%r10d, -104(%rsp)       # 4-byte Spill
	movq	%rdi, -16(%rsp)         # 8-byte Spill
	movq	%rbp, -24(%rsp)         # 8-byte Spill
	leaq	dct_luma_16x16.M0(%rbp,%rdi), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	xorl	%esi, %esi
	movl	$-1, %r14d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB2_36:                               # %.preheader527
                                        #   Parent Loop BB2_33 Depth=1
                                        #     Parent Loop BB2_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-40(%rsp), %rax         # 8-byte Reload
	movzbl	2(%rax,%rsi,2), %edx
	movzbl	3(%rax,%rsi,2), %eax
	movq	%rax, %r12
	shlq	$4, %r12
	movl	dct_luma_16x16.M4(%r12,%rdx,4), %ecx
	movl	%ecx, %edi
	negl	%edi
	cmovll	%ecx, %edi
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rcx
	imull	(%rcx,%rdx,4), %edi
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rcx
	movl	(%rcx,%rdx,4), %r8d
	addl	%edi, %r8d
	movl	-84(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r8d
	cmpl	$0, 15260(%r15)
	je	.LBB2_41
# BB#37:                                #   in Loop: Header=BB2_36 Depth=3
	testl	%r8d, %r8d
	je	.LBB2_38
# BB#39:                                #   in Loop: Header=BB2_36 Depth=3
	movl	%r8d, %r9d
	movl	-84(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r9d
	subl	%r9d, %edi
	movq	-80(%rsp), %r9          # 8-byte Reload
	imull	AdaptRndWeight(%rip), %edi
	addl	-72(%rsp), %edi         # 4-byte Folded Reload
	movl	-128(%rsp), %ecx        # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edi
	jmp	.LBB2_40
	.p2align	4, 0x90
.LBB2_38:                               #   in Loop: Header=BB2_36 Depth=3
	xorl	%edi, %edi
.LBB2_40:                               #   in Loop: Header=BB2_36 Depth=3
	movq	14176(%r15), %rcx
	movq	16(%rcx), %rcx
	movq	-96(%rsp), %rbx         # 8-byte Reload
	leal	(%rax,%rbx), %ebp
	movslq	%ebp, %rbp
	movq	(%rcx,%rbp,8), %rcx
	leal	(%rdx,%r11), %ebp
	movslq	%ebp, %rbp
	movl	%edi, (%rcx,%rbp,4)
.LBB2_41:                               #   in Loop: Header=BB2_36 Depth=3
	incl	%r14d
	leaq	dct_luma_16x16.M4(%r12,%rdx,4), %rdi
	movl	%r8d, %ecx
	testl	%r8d, %r8d
	je	.LBB2_42
# BB#43:                                #   in Loop: Header=BB2_36 Depth=3
	negl	%ecx
	movl	%r8d, %ebx
	cmovgel	%ecx, %ebx
	movl	%ebx, %ebp
	negl	%ebp
	cmpl	$0, (%rdi)
	cmovnsl	%ebx, %ebp
	movslq	%r10d, %r10
	movl	%ebp, (%r13,%r10,4)
	movq	-120(%rsp), %rbx        # 8-byte Reload
	movl	%r14d, (%rbx,%r10,4)
	incl	%r10d
	movl	$-1, %r14d
	movl	$15, -104(%rsp)         # 4-byte Folded Spill
	jmp	.LBB2_44
	.p2align	4, 0x90
.LBB2_42:                               # %._crit_edge789
                                        #   in Loop: Header=BB2_36 Depth=3
	negl	%ecx
.LBB2_44:                               #   in Loop: Header=BB2_36 Depth=3
	testl	%r8d, %r8d
	cmovnsl	%r8d, %ecx
	movl	%ecx, %ebp
	negl	%ebp
	cmpl	$0, (%rdi)
	cmovnsl	%ecx, %ebp
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	imull	(%rax,%rdx,4), %ebp
	movl	%r9d, %ecx
	shll	%cl, %ebp
	addl	$8, %ebp
	sarl	$4, %ebp
	movl	%ebp, (%rdi)
	incq	%rsi
	cmpq	$15, %rsi
	jne	.LBB2_36
# BB#45:                                # %.preheader525696
                                        #   in Loop: Header=BB2_34 Depth=2
	movslq	%r10d, %rax
	movl	$0, (%r13,%rax,4)
	movl	dct_luma_16x16.M4(%rip), %r11d
	movl	dct_luma_16x16.M4+8(%rip), %eax
	leal	(%rax,%r11), %edx
	subl	%eax, %r11d
	movl	dct_luma_16x16.M4+4(%rip), %eax
	movl	%eax, %edi
	sarl	%edi
	movl	dct_luma_16x16.M4+12(%rip), %ebp
	subl	%ebp, %edi
	sarl	%ebp
	addl	%eax, %ebp
	leal	(%rdx,%rbp), %r12d
	movl	%r12d, dct_luma_16x16.M4(%rip)
	leal	(%rdi,%r11), %eax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movl	%eax, dct_luma_16x16.M4+4(%rip)
	subl	%edi, %r11d
	movl	%r11d, dct_luma_16x16.M4+8(%rip)
	subl	%ebp, %edx
	movl	%edx, dct_luma_16x16.M4+12(%rip)
	movl	dct_luma_16x16.M4+16(%rip), %r13d
	movl	dct_luma_16x16.M4+24(%rip), %eax
	leal	(%rax,%r13), %ecx
	subl	%eax, %r13d
	movl	dct_luma_16x16.M4+20(%rip), %eax
	movl	%eax, %edx
	sarl	%edx
	movl	dct_luma_16x16.M4+28(%rip), %edi
	subl	%edi, %edx
	sarl	%edi
	addl	%eax, %edi
	leal	(%rcx,%rdi), %esi
	movl	%esi, dct_luma_16x16.M4+16(%rip)
	leal	(%rdx,%r13), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	%eax, dct_luma_16x16.M4+20(%rip)
	subl	%edx, %r13d
	movl	%r13d, dct_luma_16x16.M4+24(%rip)
	subl	%edi, %ecx
	movq	%rcx, -120(%rsp)        # 8-byte Spill
	movl	%ecx, dct_luma_16x16.M4+28(%rip)
	movl	dct_luma_16x16.M4+32(%rip), %r10d
	movl	dct_luma_16x16.M4+40(%rip), %eax
	leal	(%rax,%r10), %ecx
	subl	%eax, %r10d
	movl	dct_luma_16x16.M4+36(%rip), %eax
	movl	%eax, %edi
	sarl	%edi
	movl	dct_luma_16x16.M4+44(%rip), %ebp
	subl	%ebp, %edi
	sarl	%ebp
	addl	%eax, %ebp
	leal	(%rdi,%r10), %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	subl	%edi, %r10d
	leal	(%rcx,%rbp), %ebx
	movl	%ebx, dct_luma_16x16.M4+32(%rip)
	movl	%eax, dct_luma_16x16.M4+36(%rip)
	movl	%r10d, dct_luma_16x16.M4+40(%rip)
	subl	%ebp, %ecx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movl	%ecx, dct_luma_16x16.M4+44(%rip)
	movl	dct_luma_16x16.M4+52(%rip), %ebp
	movl	%ebp, %r9d
	sarl	%r9d
	movl	dct_luma_16x16.M4+60(%rip), %edi
	subl	%edi, %r9d
	sarl	%edi
	addl	%ebp, %edi
	leal	(%rbx,%r12), %r8d
	subl	%ebx, %r12d
	movl	dct_luma_16x16.M4+48(%rip), %ebx
	movl	dct_luma_16x16.M4+56(%rip), %r14d
	leal	(%r14,%rbx), %ebp
	leal	(%rbp,%rdi), %eax
	movl	%eax, dct_luma_16x16.M4+48(%rip)
	movl	%esi, %ecx
	sarl	%ecx
	subl	%eax, %ecx
	sarl	%eax
	addl	%esi, %eax
	leal	(%r8,%rax), %r15d
	movq	-32(%rsp), %rsi         # 8-byte Reload
	movl	%r15d, (%rsi)
	leal	(%rcx,%r12), %edx
	movl	%edx, 16(%rsi)
	subl	%ecx, %r12d
	movl	%r12d, 32(%rsi)
	subl	%eax, %r8d
	movl	%r8d, 48(%rsi)
	subl	%r14d, %ebx
	leal	(%r9,%rbx), %eax
	subl	%r9d, %ebx
	movl	%eax, dct_luma_16x16.M4+52(%rip)
	subl	%edi, %ebp
	movq	-56(%rsp), %rdx         # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rdx), %r9d
	subl	%ecx, %edx
	movq	%rdx, %rcx
	movl	20(%rsp), %esi          # 4-byte Reload
	movl	%esi, %edx
	sarl	%edx
	subl	%eax, %edx
	sarl	%eax
	addl	%esi, %eax
	leal	(%r9,%rax), %esi
	movq	-16(%rsp), %r8          # 8-byte Reload
	movq	-24(%rsp), %rdi         # 8-byte Reload
	movl	%esi, dct_luma_16x16.M0+4(%rdi,%r8)
	leal	(%rdx,%rcx), %esi
	movl	%esi, dct_luma_16x16.M0+20(%rdi,%r8)
	subl	%edx, %ecx
	movl	%ecx, dct_luma_16x16.M0+36(%rdi,%r8)
	subl	%eax, %r9d
	movl	%ebx, dct_luma_16x16.M4+56(%rip)
	movl	%r9d, dct_luma_16x16.M0+52(%rdi,%r8)
	leal	(%r10,%r11), %eax
	subl	%r10d, %r11d
	movq	-80(%rsp), %r9          # 8-byte Reload
	movl	%r13d, %ecx
	sarl	%ecx
	subl	%ebx, %ecx
	sarl	%ebx
	addl	%r13d, %ebx
	leal	(%rax,%rbx), %edx
	movl	%edx, dct_luma_16x16.M0+8(%rdi,%r8)
	leal	(%rcx,%r11), %edx
	movl	%edx, dct_luma_16x16.M0+24(%rdi,%r8)
	subl	%ecx, %r11d
	movl	%r11d, dct_luma_16x16.M0+40(%rdi,%r8)
	movl	%ebp, dct_luma_16x16.M4+60(%rip)
	subl	%ebx, %eax
	movl	%eax, dct_luma_16x16.M0+56(%rdi,%r8)
	movl	dct_luma_16x16.M4+12(%rip), %eax
	movq	-64(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	subl	%edx, %eax
	movq	-120(%rsp), %rsi        # 8-byte Reload
	movl	%esi, %edx
	sarl	%edx
	subl	%ebp, %edx
	sarl	%ebp
	addl	%esi, %ebp
	leal	(%rcx,%rbp), %esi
	movl	%esi, dct_luma_16x16.M0+12(%rdi,%r8)
	leal	(%rdx,%rax), %esi
	movl	%esi, dct_luma_16x16.M0+28(%rdi,%r8)
	movl	%eax, %esi
	subl	%edx, %esi
	movl	%esi, dct_luma_16x16.M0+44(%rdi,%r8)
	movl	%ecx, %esi
	subl	%ebp, %esi
	movl	%esi, dct_luma_16x16.M0+60(%rdi,%r8)
	movl	%ecx, dct_luma_16x16.M6.0(%rip)
	movl	%eax, dct_luma_16x16.M6.1(%rip)
	movl	%edx, dct_luma_16x16.M6.2(%rip)
	movl	%ebp, dct_luma_16x16.M6.3(%rip)
	movq	24(%rsp), %r12          # 8-byte Reload
	movl	-108(%rsp), %ebx        # 4-byte Reload
	movl	-104(%rsp), %r10d       # 4-byte Reload
	movq	-48(%rsp), %rax         # 8-byte Reload
	movq	-8(%rsp), %r14          # 8-byte Reload
.LBB2_53:                               #   in Loop: Header=BB2_34 Depth=2
	incq	%r14
	cmpq	$4, %r14
	jne	.LBB2_34
# BB#54:                                #   in Loop: Header=BB2_33 Depth=1
	incq	%rax
	cmpq	$4, %rax
	jne	.LBB2_33
# BB#55:                                # %.preheader522.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_56:                               # %.preheader522
                                        # =>This Inner Loop Header: Depth=1
	movaps	dct_luma_16x16.M0(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1(%rax)
	movaps	dct_luma_16x16.M0+16(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1+64(%rax)
	movaps	dct_luma_16x16.M0+32(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1+128(%rax)
	movaps	dct_luma_16x16.M0+48(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1+192(%rax)
	movaps	dct_luma_16x16.M0+64(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1+16(%rax)
	movaps	dct_luma_16x16.M0+80(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1+80(%rax)
	movaps	dct_luma_16x16.M0+96(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1+144(%rax)
	movaps	dct_luma_16x16.M0+112(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1+208(%rax)
	movaps	dct_luma_16x16.M0+128(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1+32(%rax)
	movaps	dct_luma_16x16.M0+144(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1+96(%rax)
	movaps	dct_luma_16x16.M0+160(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1+160(%rax)
	movaps	dct_luma_16x16.M0+176(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1+224(%rax)
	movaps	dct_luma_16x16.M0+192(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1+48(%rax)
	movaps	dct_luma_16x16.M0+208(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1+112(%rax)
	movaps	dct_luma_16x16.M0+224(%rax), %xmm0
	movaps	%xmm0, dct_luma_16x16.M1+176(%rax)
	movdqa	dct_luma_16x16.M0+240(%rax), %xmm0
	movdqa	%xmm0, dct_luma_16x16.M1+240(%rax)
	addq	$256, %rax              # imm = 0x100
	cmpq	$1024, %rax             # imm = 0x400
	jne	.LBB2_56
# BB#57:
	movq	img(%rip), %r8
	movl	20(%r8), %esi
	movl	%r10d, %eax
	testb	%bl, %bl
	je	.LBB2_67
# BB#58:
	movq	enc_picture(%rip), %rdx
	movq	6440(%rdx), %r9
	cmpl	$3, %esi
	je	.LBB2_63
# BB#59:                                # %.preheader.preheader
	movslq	180(%r8), %r14
	addq	%r8, %r12
	movl	176(%r8), %ecx
	xorl	%r11d, %r11d
	movl	$4818, %r10d            # imm = 0x12D2
	.p2align	4, 0x90
.LBB2_60:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_61 Depth 2
	movslq	%r11d, %rdx
	addq	%r14, %rdx
	movq	(%r9,%rdx,8), %rdx
	movq	%r10, %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_61:                               #   Parent Loop BB2_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-2(%r12,%rdi), %esi
	addl	dct_luma_16x16.M1-9636(%rdi,%rdi), %esi
	leaq	(%rcx,%rbx), %rbp
	movslq	%ebp, %rbp
	movw	%si, (%rdx,%rbp,2)
	movzwl	(%r12,%rdi), %esi
	addl	dct_luma_16x16.M1-9632(%rdi,%rdi), %esi
	incl	%ebp
	movslq	%ebp, %rbp
	movw	%si, (%rdx,%rbp,2)
	addq	$2, %rbx
	addq	$4, %rdi
	cmpq	$16, %rbx
	jne	.LBB2_61
# BB#62:                                #   in Loop: Header=BB2_60 Depth=1
	incq	%r11
	addq	$32, %r10
	cmpq	$16, %r11
	jne	.LBB2_60
	jmp	.LBB2_76
.LBB2_67:
	movq	enc_picture(%rip), %rdx
	movq	6440(%rdx), %r9
	cmpl	$3, %esi
	je	.LBB2_72
# BB#68:                                # %.preheader517.preheader
	movslq	180(%r8), %r13
	movl	15520(%r8), %esi
	addq	%r8, %r12
	movl	176(%r8), %r15d
	xorl	%r14d, %r14d
	movl	$4816, %r10d            # imm = 0x12D0
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB2_69:                               # %.preheader517
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_70 Depth 2
	movslq	%r11d, %rcx
	addq	%r13, %rcx
	movq	(%r9,%rcx,8), %rcx
	movq	%r10, %rdx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_70:                               #   Parent Loop BB2_69 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	dct_luma_16x16.M1-9632(%rdx,%rdx), %ebx
	movzwl	(%r12,%rdx), %edi
	shll	$6, %edi
	leal	32(%rbx,%rdi), %edi
	sarl	$6, %edi
	cmovsl	%r14d, %edi
	cmpl	%esi, %edi
	cmovgew	%si, %di
	leal	(%r15,%rbp), %ebx
	movslq	%ebx, %rbx
	movw	%di, (%rcx,%rbx,2)
	incq	%rbp
	addq	$2, %rdx
	cmpq	$16, %rbp
	jne	.LBB2_70
# BB#71:                                #   in Loop: Header=BB2_69 Depth=1
	incq	%r11
	addq	$32, %r10
	cmpq	$16, %r11
	jne	.LBB2_69
	jmp	.LBB2_76
.LBB2_63:                               # %.preheader515
	movq	lrec(%rip), %r14
	addq	%r8, %r12
	xorl	%r11d, %r11d
	movl	$4818, %r10d            # imm = 0x12D2
	.p2align	4, 0x90
.LBB2_64:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_65 Depth 2
	movslq	180(%r8), %rdx
	movslq	%r11d, %rdi
	addq	%rdx, %rdi
	movq	(%r9,%rdi,8), %rbp
	movq	(%r14,%rdi,8), %rbx
	movq	%r10, %rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_65:                               #   Parent Loop BB2_64 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-2(%r12,%rdi), %ecx
	addl	dct_luma_16x16.M1-9636(%rdi,%rdi), %ecx
	movl	176(%r8), %esi
	addl	%edx, %esi
	movslq	%esi, %rsi
	movw	%cx, (%rbp,%rsi,2)
	movl	$-16, (%rbx,%rsi,4)
	movzwl	(%r12,%rdi), %ecx
	addl	dct_luma_16x16.M1-9632(%rdi,%rdi), %ecx
	movl	176(%r8), %esi
	leal	1(%rdx,%rsi), %esi
	movslq	%esi, %rsi
	movw	%cx, (%rbp,%rsi,2)
	movl	$-16, (%rbx,%rsi,4)
	addq	$2, %rdx
	addq	$4, %rdi
	cmpq	$16, %rdx
	jne	.LBB2_65
# BB#66:                                #   in Loop: Header=BB2_64 Depth=1
	incq	%r11
	addq	$32, %r10
	cmpq	$16, %r11
	jne	.LBB2_64
	jmp	.LBB2_76
.LBB2_72:                               # %.preheader519
	movq	lrec(%rip), %rcx
	addq	%r8, %r12
	xorl	%r14d, %r14d
	movl	$4816, %r10d            # imm = 0x12D0
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB2_73:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_74 Depth 2
	movslq	180(%r8), %rsi
	movslq	%r11d, %rdx
	addq	%rsi, %rdx
	movq	(%r9,%rdx,8), %r15
	movq	(%rcx,%rdx,8), %r13
	movq	%r10, %rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_74:                               #   Parent Loop BB2_73 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	15520(%r8), %ebp
	movl	dct_luma_16x16.M1-9632(%rdi,%rdi), %esi
	movzwl	(%r12,%rdi), %ebx
	shll	$6, %ebx
	leal	32(%rsi,%rbx), %esi
	sarl	$6, %esi
	cmovsl	%r14d, %esi
	cmpl	%ebp, %esi
	cmovlw	%si, %bp
	movl	176(%r8), %esi
	addl	%edx, %esi
	movslq	%esi, %rsi
	movw	%bp, (%r15,%rsi,2)
	movl	$-16, (%r13,%rsi,4)
	incq	%rdx
	addq	$2, %rdi
	cmpq	$16, %rdx
	jne	.LBB2_74
# BB#75:                                #   in Loop: Header=BB2_73 Depth=1
	incq	%r11
	addq	$32, %r10
	cmpq	$16, %r11
	jne	.LBB2_73
.LBB2_76:                               # %.loopexit
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	dct_luma_16x16, .Lfunc_end2-dct_luma_16x16
	.cfi_endproc

	.globl	dct_luma
	.p2align	4, 0x90
	.type	dct_luma,@function
dct_luma:                               # @dct_luma
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	%edi, %eax
	shrl	$2, %eax
	movl	%esi, %ebx
	sarl	$3, %ebx
	movl	%edi, -96(%rsp)         # 4-byte Spill
	movl	%edi, %ebp
	sarl	$3, %ebp
	leal	(%rbp,%rbx,2), %ebx
	movl	%esi, %ebp
	shrl	%ebp
	andl	$2, %ebp
	andl	$1, %eax
	orl	%ebp, %eax
	movq	img(%rip), %r13
	movslq	%ebx, %rbp
	movq	14160(%r13), %rbx
	movq	14224(%r13), %r8
	movq	(%rbx,%rbp,8), %rbp
	movq	(%rbp,%rax,8), %rax
	movq	(%rax), %rbp
	movq	%rbp, -104(%rsp)        # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	movslq	12(%r13), %rax
	imulq	$536, %rax, %rbp        # imm = 0x218
	movl	15452(%r13), %eax
	addl	8(%r8,%rbp), %eax
	jne	.LBB3_1
# BB#2:
	cmpl	$1, 15540(%r13)
	sete	%bl
	jmp	.LBB3_3
.LBB3_1:
	xorl	%ebx, %ebx
.LBB3_3:
	cmpl	$0, 428(%r8,%rbp)
	movl	$FIELD_SCAN, %ebp
	movl	$SNGL_SCAN, %r12d
	cmovneq	%rbp, %r12
	movslq	%ecx, %rbp
	testb	%bl, %bl
	movq	%rbp, -88(%rsp)         # 8-byte Spill
	je	.LBB3_11
# BB#4:                                 # %.preheader.preheader
	movl	$-1, %r8d
	xorl	%r11d, %r11d
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_5:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r12,%r11,2), %r10d
	movq	%r12, %rdi
	movzbl	1(%r12,%r11,2), %r12d
	movq	%r12, %r14
	shlq	$6, %r14
	addq	%r13, %r14
	movl	13136(%r14,%r10,4), %ecx
	movl	%ecx, %ebp
	negl	%ebp
	cmovll	%ecx, %ebp
	cmpl	$0, 15260(%r13)
	je	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=1
	movq	14176(%r13), %rcx
	movq	-88(%rsp), %rbx         # 8-byte Reload
	movq	(%rcx,%rbx,8), %r15
	movslq	%esi, %rcx
	addq	%rcx, %r12
	movq	(%r15,%r12,8), %rbx
	movslq	-96(%rsp), %rcx         # 4-byte Folded Reload
	addq	%r10, %rcx
	movl	$0, (%rbx,%rcx,4)
.LBB3_7:                                #   in Loop: Header=BB3_5 Depth=1
	incl	%r8d
	testl	%ebp, %ebp
	je	.LBB3_9
# BB#8:                                 #   in Loop: Header=BB3_5 Depth=1
	addl	$999999, (%rdx)         # imm = 0xF423F
	leaq	13136(%r14,%r10,4), %rcx
	movl	%ebp, %eax
	negl	%eax
	cmpl	$0, (%rcx)
	cmovnsl	%ebp, %eax
	movslq	%r9d, %r9
	movq	-104(%rsp), %rcx        # 8-byte Reload
	movl	%eax, (%rcx,%r9,4)
	movq	-64(%rsp), %rax         # 8-byte Reload
	movl	%r8d, (%rax,%r9,4)
	incl	%r9d
	movl	$-1, %r8d
	movl	$1, %eax
.LBB3_9:                                #   in Loop: Header=BB3_5 Depth=1
	movq	%rdi, %r12
	incq	%r11
	cmpq	$16, %r11
	jne	.LBB3_5
# BB#10:                                # %.loopexit.loopexit373
	movslq	%r9d, %rcx
	movq	-104(%rsp), %rdx        # 8-byte Reload
	movl	$0, (%rdx,%rcx,4)
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %r8
	movslq	-96(%rsp), %rcx         # 4-byte Folded Reload
	movslq	%esi, %rdi
	movl	180(%r13), %r9d
	movslq	176(%r13), %rbx
	addq	%rcx, %rbx
	addl	%r9d, %esi
	movslq	%esi, %rbp
	movq	(%r8,%rbp,8), %rbp
	movq	%rdi, %rdx
	shlq	$5, %rdx
	leaq	12624(%r13,%rdx), %rdx
	movzwl	(%rdx,%rcx,2), %esi
	addl	13136(%r13), %esi
	movw	%si, (%rbp,%rbx,2)
	movzwl	2(%rdx,%rcx,2), %esi
	addl	13140(%r13), %esi
	movw	%si, 2(%rbp,%rbx,2)
	movzwl	4(%rdx,%rcx,2), %esi
	addl	13144(%r13), %esi
	movw	%si, 4(%rbp,%rbx,2)
	movzwl	6(%rdx,%rcx,2), %edx
	addl	13148(%r13), %edx
	movw	%dx, 6(%rbp,%rbx,2)
	leaq	1(%rdi), %rdx
	leal	(%rdx,%r9), %esi
	movslq	%esi, %rsi
	movq	(%r8,%rsi,8), %rsi
	shlq	$5, %rdx
	leaq	12624(%r13,%rdx), %rdx
	movzwl	(%rdx,%rcx,2), %ebp
	addl	13200(%r13), %ebp
	movw	%bp, (%rsi,%rbx,2)
	movzwl	2(%rdx,%rcx,2), %ebp
	addl	13204(%r13), %ebp
	movw	%bp, 2(%rsi,%rbx,2)
	movzwl	4(%rdx,%rcx,2), %ebp
	addl	13208(%r13), %ebp
	movw	%bp, 4(%rsi,%rbx,2)
	movzwl	6(%rdx,%rcx,2), %edx
	addl	13212(%r13), %edx
	movw	%dx, 6(%rsi,%rbx,2)
	leaq	2(%rdi), %rdx
	leal	(%rdx,%r9), %esi
	movslq	%esi, %rsi
	movq	(%r8,%rsi,8), %rsi
	shlq	$5, %rdx
	leaq	12624(%r13,%rdx), %rdx
	movzwl	(%rdx,%rcx,2), %ebp
	addl	13264(%r13), %ebp
	movw	%bp, (%rsi,%rbx,2)
	movzwl	2(%rdx,%rcx,2), %ebp
	addl	13268(%r13), %ebp
	movw	%bp, 2(%rsi,%rbx,2)
	movzwl	4(%rdx,%rcx,2), %ebp
	addl	13272(%r13), %ebp
	movw	%bp, 4(%rsi,%rbx,2)
	movzwl	6(%rdx,%rcx,2), %edx
	addl	13276(%r13), %edx
	movw	%dx, 6(%rsi,%rbx,2)
	addq	$3, %rdi
	addl	%edi, %r9d
	movslq	%r9d, %rdx
	movq	(%r8,%rdx,8), %rdx
	shlq	$5, %rdi
	leaq	12624(%r13,%rdi), %rsi
	movzwl	(%rsi,%rcx,2), %edi
	addl	13328(%r13), %edi
	movw	%di, (%rdx,%rbx,2)
	movzwl	2(%rsi,%rcx,2), %edi
	addl	13332(%r13), %edi
	movw	%di, 2(%rdx,%rbx,2)
	movzwl	4(%rsi,%rcx,2), %edi
	addl	13336(%r13), %edi
	movw	%di, 4(%rdx,%rbx,2)
	movzwl	6(%rsi,%rcx,2), %ecx
	addl	13340(%r13), %ecx
	movw	%cx, 6(%rdx,%rbx,2)
	jmp	.LBB3_27
.LBB3_11:                               # %.preheader279
	movq	%rdx, -8(%rsp)          # 8-byte Spill
	movq	%rsi, -48(%rsp)         # 8-byte Spill
	movq	qp_per_matrix(%rip), %rcx
	cltq
	movslq	(%rcx,%rax,4), %rsi
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	movq	qp_rem_matrix(%rip), %rcx
	movq	LevelScale4x4Luma(%rip), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movslq	(%rcx,%rax,4), %rax
	movq	(%rdx,%rax,8), %rcx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movq	LevelOffset4x4Luma(%rip), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	movq	InvLevelScale4x4Luma(%rip), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movl	13148(%r13), %eax
	movl	13136(%r13), %ecx
	movl	13140(%r13), %edx
	leal	(%rax,%rcx), %esi
	movl	13144(%r13), %edi
	leal	(%rdi,%rdx), %ebp
	subl	%edi, %edx
	subl	%eax, %ecx
	leal	(%rbp,%rsi), %r9d
	subl	%ebp, %esi
	movl	%esi, dct_luma.m4+8(%rip)
	leal	(%rdx,%rcx,2), %r14d
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, dct_luma.m4+12(%rip)
	movl	13212(%r13), %eax
	movl	13200(%r13), %r10d
	movl	13204(%r13), %edx
	leal	(%rax,%r10), %edi
	movl	13208(%r13), %esi
	leal	(%rsi,%rdx), %ebp
	subl	%esi, %edx
	subl	%eax, %r10d
	leal	(%rbp,%rdi), %ecx
	subl	%ebp, %edi
	leal	(%rdx,%r10,2), %esi
	addl	%edx, %edx
	subl	%edx, %r10d
	movl	13264(%r13), %r11d
	movl	13268(%r13), %edx
	movl	13272(%r13), %ebp
	leal	(%rbp,%rdx), %ebx
	subl	%ebp, %edx
	movl	13276(%r13), %ebp
	leal	(%rbp,%r11), %eax
	subl	%ebp, %r11d
	leal	(%rbx,%rax), %ebp
	subl	%ebx, %eax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	leal	(%rdx,%r11,2), %eax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	addl	%edx, %edx
	subl	%edx, %r11d
	movl	13332(%r13), %ebx
	movl	13336(%r13), %edx
	leal	(%rdx,%rbx), %eax
	subl	%edx, %ebx
	movl	13328(%r13), %r8d
	movl	13340(%r13), %r13d
	leal	(%r13,%r8), %edx
	subl	%r13d, %r8d
	leal	(%rax,%rdx), %r13d
	subl	%eax, %edx
	leal	(%rbx,%r8,2), %r15d
	addl	%ebx, %ebx
	subl	%ebx, %r8d
	leal	(%rbp,%rcx), %ebx
	subl	%ebp, %ecx
	leal	(%r13,%r9), %ebp
	subl	%r13d, %r9d
	leal	(%rbx,%rbp), %eax
	movl	%eax, dct_luma.m4(%rip)
	subl	%ebx, %ebp
	movl	%ebp, dct_luma.m4+32(%rip)
	leal	(%rcx,%r9,2), %eax
	movl	%eax, dct_luma.m4+16(%rip)
	addl	%ecx, %ecx
	subl	%ecx, %r9d
	movl	%r9d, dct_luma.m4+48(%rip)
	movq	-72(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%rsi), %eax
	subl	%ecx, %esi
	leal	(%r15,%r14), %ecx
	subl	%r15d, %r14d
	leal	(%rax,%rcx), %ebp
	movl	%ebp, dct_luma.m4+4(%rip)
	subl	%eax, %ecx
	movl	%ecx, dct_luma.m4+36(%rip)
	leal	(%rsi,%r14,2), %eax
	movl	%eax, dct_luma.m4+20(%rip)
	addl	%esi, %esi
	subl	%esi, %r14d
	movl	%r14d, dct_luma.m4+52(%rip)
	movl	dct_luma.m4+8(%rip), %eax
	movq	-56(%rsp), %rsi         # 8-byte Reload
	leal	(%rsi,%rdi), %ecx
	subl	%esi, %edi
	leal	(%rdx,%rax), %esi
	subl	%edx, %eax
	leal	(%rcx,%rsi), %edx
	movl	%edx, dct_luma.m4+8(%rip)
	movq	-80(%rsp), %rbp         # 8-byte Reload
	leal	15(%rbp), %edx
	subl	%ecx, %esi
	movl	%esi, dct_luma.m4+40(%rip)
	leal	(%rdi,%rax,2), %ecx
	movl	%ecx, dct_luma.m4+24(%rip)
	addl	%edi, %edi
	subl	%edi, %eax
	movl	%edx, %edi
	movl	%eax, dct_luma.m4+56(%rip)
	movl	dct_luma.m4+12(%rip), %eax
	leal	(%r8,%rax), %ecx
	leal	(%r11,%r10), %edx
	subl	%r11d, %r10d
	subl	%r8d, %eax
	leal	(%rdx,%rcx), %esi
	movl	%esi, dct_luma.m4+12(%rip)
	movl	%ecx, dct_luma.m5.0(%rip)
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%edx, %ecx
	movl	%ecx, dct_luma.m4+44(%rip)
	leal	(%r10,%rax,2), %ecx
	movl	%ecx, dct_luma.m4+28(%rip)
	leal	(%r10,%r10), %ecx
	movl	%eax, dct_luma.m5.3(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%ecx, %eax
	movl	%eax, dct_luma.m4+60(%rip)
	movl	%edx, dct_luma.m5.1(%rip)
	movl	%r10d, dct_luma.m5.2(%rip)
	movq	%rbp, %rax
	leal	16(%rax), %eax
	movl	%eax, -56(%rsp)         # 4-byte Spill
	movl	$1, %eax
	movl	%edi, %ecx
	shll	%cl, %eax
	movl	%eax, -20(%rsp)         # 4-byte Spill
	movl	$-1, %r9d
	xorl	%r10d, %r10d
	movq	img(%rip), %rbx
	xorl	%r15d, %r15d
	movl	$0, -72(%rsp)           # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB3_12:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%r12,%r10,2), %eax
	movzbl	1(%r12,%r10,2), %r14d
	movq	%r14, %rbp
	shlq	$4, %rbp
	movl	dct_luma.m4(%rbp,%rax,4), %r11d
	movl	%r11d, %edx
	negl	%edx
	cmovll	%r11d, %edx
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%r14,8), %rcx
	imull	(%rcx,%rax,4), %edx
	movq	-40(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%r14,8), %rcx
	movl	(%rcx,%rax,4), %r13d
	addl	%edx, %r13d
	movl	%edi, %ecx
	sarl	%cl, %r13d
	cmpl	$0, 15260(%rbx)
	je	.LBB3_17
# BB#13:                                #   in Loop: Header=BB3_12 Depth=1
	testl	%r13d, %r13d
	je	.LBB3_14
# BB#15:                                #   in Loop: Header=BB3_12 Depth=1
	movl	%r13d, %r8d
	movl	%edi, %ecx
	shll	%cl, %r8d
	subl	%r8d, %edx
	movq	-88(%rsp), %rsi         # 8-byte Reload
	imull	AdaptRndWeight(%rip), %edx
	addl	-20(%rsp), %edx         # 4-byte Folded Reload
	movl	-56(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	jmp	.LBB3_16
	.p2align	4, 0x90
.LBB3_14:                               #   in Loop: Header=BB3_12 Depth=1
	xorl	%edx, %edx
	movq	-88(%rsp), %rsi         # 8-byte Reload
.LBB3_16:                               #   in Loop: Header=BB3_12 Depth=1
	movq	14176(%rbx), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movslq	-48(%rsp), %rsi         # 4-byte Folded Reload
	addq	%r14, %rsi
	movq	(%rcx,%rsi,8), %rcx
	movslq	-96(%rsp), %rsi         # 4-byte Folded Reload
	addq	%rax, %rsi
	movl	%edx, (%rcx,%rsi,4)
.LBB3_17:                               #   in Loop: Header=BB3_12 Depth=1
	incl	%r9d
	leaq	dct_luma.m4(%rbp,%rax,4), %rbp
	testl	%r13d, %r13d
	je	.LBB3_18
# BB#19:                                #   in Loop: Header=BB3_12 Depth=1
	movl	%edi, %r8d
	movl	$999999, %ecx           # imm = 0xF423F
	cmpl	$1, %r13d
	jg	.LBB3_21
# BB#20:                                #   in Loop: Header=BB3_12 Depth=1
	movq	input(%rip), %rcx
	movslq	4180(%rcx), %rcx
	movslq	%r9d, %rdx
	shlq	$4, %rcx
	movzbl	COEFF_COST(%rcx,%rdx), %ecx
.LBB3_21:                               #   in Loop: Header=BB3_12 Depth=1
	movq	-8(%rsp), %rdx          # 8-byte Reload
	addl	%ecx, (%rdx)
	movl	%r13d, %ecx
	negl	%ecx
	cmovll	%r13d, %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%r11d, %r11d
	movl	%ecx, %esi
	cmovsl	%edx, %esi
	movslq	%r15d, %r15
	movq	-104(%rsp), %rdi        # 8-byte Reload
	movl	%esi, (%rdi,%r15,4)
	movq	-64(%rsp), %rsi         # 8-byte Reload
	movl	%r9d, (%rsi,%r15,4)
	incl	%r15d
	cmpl	$0, (%rbp)
	cmovnsl	%ecx, %edx
	movq	-16(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%r14,8), %rcx
	imull	(%rcx,%rax,4), %edx
	movq	-80(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edx
	addl	$8, %edx
	sarl	$4, %edx
	movl	$-1, %r9d
	movl	$1, -72(%rsp)           # 4-byte Folded Spill
	movl	%r8d, %edi
	jmp	.LBB3_22
	.p2align	4, 0x90
.LBB3_18:                               #   in Loop: Header=BB3_12 Depth=1
	xorl	%edx, %edx
.LBB3_22:                               #   in Loop: Header=BB3_12 Depth=1
	movl	%edx, (%rbp)
	incq	%r10
	cmpq	$16, %r10
	jne	.LBB3_12
# BB#23:                                # %.preheader276343
	movslq	%r15d, %rax
	movq	-104(%rsp), %rcx        # 8-byte Reload
	movl	$0, (%rcx,%rax,4)
	movl	dct_luma.m4(%rip), %eax
	movl	dct_luma.m4+8(%rip), %ecx
	leal	(%rcx,%rax), %edx
	subl	%ecx, %eax
	movl	dct_luma.m4+4(%rip), %ecx
	movl	%ecx, %esi
	sarl	%esi
	movl	dct_luma.m4+12(%rip), %edi
	subl	%edi, %esi
	sarl	%edi
	addl	%ecx, %edi
	leal	(%rdx,%rdi), %r9d
	movl	%r9d, dct_luma.m4(%rip)
	leal	(%rsi,%rax), %ecx
	movl	%ecx, dct_luma.m4+4(%rip)
	subl	%esi, %eax
	movl	%eax, dct_luma.m4+8(%rip)
	subl	%edi, %edx
	movl	%edx, dct_luma.m4+12(%rip)
	movl	dct_luma.m4+16(%rip), %ecx
	movl	dct_luma.m4+24(%rip), %eax
	leal	(%rax,%rcx), %edx
	subl	%eax, %ecx
	movl	dct_luma.m4+20(%rip), %eax
	movl	%eax, %esi
	sarl	%esi
	movl	dct_luma.m4+28(%rip), %edi
	subl	%edi, %esi
	sarl	%edi
	addl	%eax, %edi
	leal	(%rdx,%rdi), %eax
	movl	%eax, dct_luma.m4+16(%rip)
	leal	(%rsi,%rcx), %ebp
	movl	%ebp, dct_luma.m4+20(%rip)
	subl	%esi, %ecx
	movl	%ecx, dct_luma.m4+24(%rip)
	subl	%edi, %edx
	movl	%edx, dct_luma.m4+28(%rip)
	movl	dct_luma.m4+32(%rip), %ecx
	movl	dct_luma.m4+40(%rip), %edx
	leal	(%rdx,%rcx), %r8d
	subl	%edx, %ecx
	movl	dct_luma.m4+36(%rip), %edx
	movl	%edx, %edi
	sarl	%edi
	movl	dct_luma.m4+44(%rip), %ebp
	subl	%ebp, %edi
	sarl	%ebp
	addl	%edx, %ebp
	leal	(%r8,%rbp), %r12d
	movl	%r12d, dct_luma.m4+32(%rip)
	leal	(%rdi,%rcx), %esi
	movl	%esi, dct_luma.m4+36(%rip)
	subl	%edi, %ecx
	movl	%ecx, dct_luma.m4+40(%rip)
	subl	%ebp, %r8d
	movl	%r8d, dct_luma.m4+44(%rip)
	movl	dct_luma.m4+48(%rip), %r10d
	movl	dct_luma.m4+56(%rip), %esi
	leal	(%rsi,%r10), %r8d
	subl	%esi, %r10d
	movl	dct_luma.m4+52(%rip), %ebp
	movl	%ebp, %edi
	sarl	%edi
	movl	dct_luma.m4+60(%rip), %esi
	subl	%esi, %edi
	sarl	%esi
	addl	%ebp, %esi
	leal	(%r8,%rsi), %ebp
	movl	%ebp, dct_luma.m4+48(%rip)
	leal	(%rdi,%r10), %ecx
	movl	%ecx, dct_luma.m4+52(%rip)
	movl	%r10d, dct_luma.m6.1(%rip)
	movl	%r10d, %ecx
	subl	%edi, %ecx
	movl	%ecx, dct_luma.m4+56(%rip)
	movl	%r8d, dct_luma.m6.0(%rip)
	movl	%r8d, %ecx
	subl	%esi, %ecx
	movl	%ecx, dct_luma.m4+60(%rip)
	movl	%edi, dct_luma.m6.2(%rip)
	movl	%esi, dct_luma.m6.3(%rip)
	movq	-48(%rsp), %rdx         # 8-byte Reload
	movslq	%edx, %rcx
	leal	1(%rdx), %esi
	movl	%esi, -40(%rsp)         # 4-byte Spill
	movslq	%esi, %rsi
	leal	2(%rdx), %edi
	movl	%edi, -32(%rsp)         # 4-byte Spill
	movslq	%edi, %rdi
	leal	3(%rdx), %edx
	movl	%edx, -88(%rsp)         # 4-byte Spill
	movslq	%edx, %r8
	movslq	-96(%rsp), %rdx         # 4-byte Folded Reload
	shlq	$5, %rcx
	leaq	(%rcx,%rdx,2), %rcx
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	shlq	$5, %rsi
	leaq	(%rsi,%rdx,2), %rcx
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	shlq	$5, %rdi
	leaq	(%rdi,%rdx,2), %rcx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	shlq	$5, %r8
	movq	%rdx, -80(%rsp)         # 8-byte Spill
	leaq	(%r8,%rdx,2), %r13
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	jmp	.LBB3_24
	.p2align	4, 0x90
.LBB3_25:                               # %._crit_edge
                                        #   in Loop: Header=BB3_24 Depth=1
	movl	dct_luma.m4+4(%rsi,%rsi), %r9d
	movl	dct_luma.m4+36(%rsi,%rsi), %r12d
	movl	dct_luma.m4+20(%rsi,%rsi), %eax
	movl	dct_luma.m4+52(%rsi,%rsi), %ebp
	addq	$2, %rsi
.LBB3_24:                               # =>This Inner Loop Header: Depth=1
	leal	(%r12,%r9), %r8d
	movl	%r9d, %ecx
	subl	%r12d, %ecx
	movl	%eax, %r10d
	sarl	%r10d
	subl	%ebp, %r10d
	sarl	%ebp
	addl	%eax, %ebp
	leaq	(%rbx,%rsi), %rax
	movq	-96(%rsp), %rdx         # 8-byte Reload
	movzwl	12624(%rdx,%rax), %r11d
	shll	$6, %r11d
	leal	(%rbp,%r8), %r14d
	leal	32(%r11,%r14), %edi
	movl	15520(%rbx), %edx
	sarl	$6, %edi
	cmovsl	%r15d, %edi
	cmpl	%edx, %edi
	cmovgl	%edx, %edi
	movl	%edi, 13136(%rbx,%rsi,2)
	movq	-104(%rsp), %rdx        # 8-byte Reload
	movzwl	12624(%rdx,%rax), %edx
	shll	$6, %edx
	leal	32(%rcx,%r10), %edi
	addl	%edx, %edi
	movl	15520(%rbx), %edx
	sarl	$6, %edi
	cmovsl	%r15d, %edi
	cmpl	%edx, %edi
	cmovgl	%edx, %edi
	leal	32(%rcx), %edx
	movl	%edi, 13200(%rbx,%rsi,2)
	movq	-64(%rsp), %rdi         # 8-byte Reload
	movzwl	12624(%rdi,%rax), %edi
	shll	$6, %edi
	subl	%r10d, %edx
	addl	%edi, %edx
	movl	15520(%rbx), %edi
	sarl	$6, %edx
	cmovsl	%r15d, %edx
	cmpl	%edi, %edx
	cmovgl	%edi, %edx
	movl	%edx, 13264(%rbx,%rsi,2)
	leal	32(%r12,%r9), %edx
	movzwl	12624(%r13,%rax), %eax
	shll	$6, %eax
	subl	%ebp, %edx
	addl	%eax, %edx
	movl	15520(%rbx), %eax
	sarl	$6, %edx
	cmovsl	%r15d, %edx
	cmpl	%eax, %edx
	cmovgl	%eax, %edx
	movl	%edx, 13328(%rbx,%rsi,2)
	cmpq	$6, %rsi
	jne	.LBB3_25
# BB#26:                                # %.preheader274
	movl	%r8d, dct_luma.m6.0(%rip)
	movl	%ecx, dct_luma.m6.1(%rip)
	movl	%r10d, dct_luma.m6.2(%rip)
	movl	%ebp, dct_luma.m6.3(%rip)
	movl	180(%rbx), %ecx
	movslq	176(%rbx), %rax
	addq	-80(%rsp), %rax         # 8-byte Folded Reload
	movq	enc_picture(%rip), %rdx
	movq	6440(%rdx), %rdx
	movq	-48(%rsp), %rsi         # 8-byte Reload
	addl	%ecx, %esi
	movslq	%esi, %rsi
	movq	(%rdx,%rsi,8), %rsi
	movzwl	13136(%rbx), %edi
	movw	%di, (%rsi,%rax,2)
	movzwl	13140(%rbx), %edi
	movw	%di, 2(%rsi,%rax,2)
	movzwl	13144(%rbx), %edi
	movw	%di, 4(%rsi,%rax,2)
	movzwl	13148(%rbx), %edi
	movw	%di, 6(%rsi,%rax,2)
	movl	-40(%rsp), %esi         # 4-byte Reload
	addl	%ecx, %esi
	movslq	%esi, %rsi
	movq	(%rdx,%rsi,8), %rsi
	movzwl	13200(%rbx), %edi
	movw	%di, (%rsi,%rax,2)
	movzwl	13204(%rbx), %edi
	movw	%di, 2(%rsi,%rax,2)
	movzwl	13208(%rbx), %edi
	movw	%di, 4(%rsi,%rax,2)
	movzwl	13212(%rbx), %edi
	movw	%di, 6(%rsi,%rax,2)
	movl	-32(%rsp), %esi         # 4-byte Reload
	addl	%ecx, %esi
	movslq	%esi, %rsi
	movq	(%rdx,%rsi,8), %rsi
	movzwl	13264(%rbx), %edi
	movw	%di, (%rsi,%rax,2)
	movzwl	13268(%rbx), %edi
	movw	%di, 2(%rsi,%rax,2)
	movzwl	13272(%rbx), %edi
	movw	%di, 4(%rsi,%rax,2)
	movzwl	13276(%rbx), %edi
	movw	%di, 6(%rsi,%rax,2)
	movl	-88(%rsp), %esi         # 4-byte Reload
	addl	%ecx, %esi
	movslq	%esi, %rcx
	movq	(%rdx,%rcx,8), %rcx
	movzwl	13328(%rbx), %edx
	movw	%dx, (%rcx,%rax,2)
	movzwl	13332(%rbx), %edx
	movw	%dx, 2(%rcx,%rax,2)
	movzwl	13336(%rbx), %edx
	movw	%dx, 4(%rcx,%rax,2)
	movzwl	13340(%rbx), %edx
	movw	%dx, 6(%rcx,%rax,2)
	movl	-72(%rsp), %eax         # 4-byte Reload
.LBB3_27:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	dct_luma, .Lfunc_end3-dct_luma
	.cfi_endproc

	.globl	dct_chroma
	.p2align	4, 0x90
	.type	dct_chroma,@function
dct_chroma:                             # @dct_chroma
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$128, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 184
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movl	%esi, %r13d
	movq	img(%rip), %r15
	movq	14168(%r15), %rax
	movq	14224(%r15), %r12
	movslq	12(%r15), %rcx
	movl	%edi, -84(%rsp)         # 4-byte Spill
	movslq	%edi, %r10
	movq	8(%rax,%r10,8), %rax
	movq	(%rax), %rdx
	movq	%rdx, -112(%rsp)        # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	imulq	$536, %rcx, %rdi        # imm = 0x218
	movl	8(%r12,%rdi), %edx
	movl	72(%r12,%rdi), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	sbbb	%bl, %bl
	movb	$51, %al
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %al
	andb	%bl, %al
	andb	$1, %al
	movl	15528(%r15), %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movslq	15536(%r15), %r8
	cmpl	$0, 428(%r12,%rdi)
	movl	$FIELD_SCAN, %ecx
	movl	$SNGL_SCAN, %esi
	cmovneq	%rcx, %rsi
	xorl	%ecx, %ecx
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	addl	15452(%r15), %edx
	movl	$0, %ecx
	movq	%rcx, -72(%rsp)         # 8-byte Spill
	jne	.LBB4_2
# BB#1:
	cmpl	$1, 15540(%r15)
	sete	%cl
	movq	%rcx, -72(%rsp)         # 8-byte Spill
.LBB4_2:
	movq	qp_per_matrix(%rip), %r9
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	leaq	(%r12,%rdi), %rdx
	movl	12(%rdx,%r10,4), %edx
	movl	15456(%r15), %edi
	leal	(%rdi,%rdx), %ebp
	movslq	%ebp, %rbx
	movslq	(%r9,%rbx,4), %rbp
	movq	%rbp, -80(%rsp)         # 8-byte Spill
	movq	qp_rem_matrix(%rip), %rbp
	movslq	(%rbp,%rbx,4), %rbx
	movq	%rbx, -48(%rsp)         # 8-byte Spill
	movq	LevelScale4x4Chroma(%rip), %rbx
	movq	(%rbx,%r10,8), %rbx
	movzbl	%al, %ecx
	movq	(%rbx,%rcx,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	LevelOffset4x4Chroma(%rip), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	InvLevelScale4x4Chroma(%rip), %rax
	movq	(%rax,%r10,8), %rax
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	cmpl	$2, %r8d
	movq	%rsi, -56(%rsp)         # 8-byte Spill
	jne	.LBB4_4
# BB#3:
	leal	3(%rdx,%rdi), %eax
	cltq
	movl	(%r9,%rax,4), %ecx
	movslq	(%rbp,%rax,4), %r8
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	leal	16(%rcx), %eax
	movl	%eax, -32(%rsp)         # 4-byte Spill
	jmp	.LBB4_5
.LBB4_4:
	movl	$1, -32(%rsp)           # 4-byte Folded Spill
	xorl	%r8d, %r8d
.LBB4_5:
	cmpb	$0, -72(%rsp)           # 1-byte Folded Reload
	movq	%r12, -40(%rsp)         # 8-byte Spill
	jne	.LBB4_13
# BB#6:                                 # %.preheader859
	movl	15548(%r15), %eax
	testl	%eax, %eax
	jle	.LBB4_13
# BB#7:                                 # %.preheader858.lr.ph
	movl	15544(%r15), %edx
	leaq	13136(%r15), %r9
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_8:                                # %.preheader858
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_10 Depth 2
	testl	%edx, %edx
	jle	.LBB4_12
# BB#9:                                 # %.preheader857.lr.ph
                                        #   in Loop: Header=BB4_8 Depth=1
	movq	%r9, %rcx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_10:                               # %.preheader857
                                        #   Parent Loop BB4_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	12(%rcx), %edx
	movl	(%rcx), %ebx
	movl	4(%rcx), %ebp
	leal	(%rdx,%rbx), %edi
	movl	8(%rcx), %esi
	leal	(%rsi,%rbp), %eax
	subl	%esi, %ebp
	subl	%edx, %ebx
	leal	(%rax,%rdi), %edx
	movl	%edx, (%rcx)
	subl	%eax, %edi
	movl	%edi, 8(%rcx)
	leal	(%rbp,%rbx,2), %eax
	movl	%eax, 4(%rcx)
	addl	%ebp, %ebp
	subl	%ebp, %ebx
	movl	%ebx, 12(%rcx)
	movl	76(%rcx), %eax
	movl	64(%rcx), %edx
	movl	68(%rcx), %esi
	leal	(%rax,%rdx), %edi
	movl	72(%rcx), %ebx
	leal	(%rbx,%rsi), %ebp
	subl	%ebx, %esi
	subl	%eax, %edx
	leal	(%rbp,%rdi), %eax
	movl	%eax, 64(%rcx)
	subl	%ebp, %edi
	movl	%edi, 72(%rcx)
	leal	(%rsi,%rdx,2), %eax
	movl	%eax, 68(%rcx)
	addl	%esi, %esi
	subl	%esi, %edx
	movl	%edx, 76(%rcx)
	movl	140(%rcx), %eax
	movl	128(%rcx), %edx
	movl	132(%rcx), %esi
	leal	(%rax,%rdx), %edi
	movl	136(%rcx), %ebx
	leal	(%rbx,%rsi), %ebp
	subl	%ebx, %esi
	subl	%eax, %edx
	leal	(%rbp,%rdi), %eax
	movl	%eax, 128(%rcx)
	subl	%ebp, %edi
	movl	%edi, 136(%rcx)
	leal	(%rsi,%rdx,2), %eax
	movl	%eax, 132(%rcx)
	addl	%esi, %esi
	subl	%esi, %edx
	movl	%edx, 140(%rcx)
	movl	204(%rcx), %eax
	movl	192(%rcx), %edx
	movl	196(%rcx), %esi
	leal	(%rax,%rdx), %edi
	movl	200(%rcx), %ebx
	leal	(%rbx,%rsi), %ebp
	subl	%ebx, %esi
	subl	%eax, %edx
	leal	(%rbp,%rdi), %eax
	movl	%eax, 192(%rcx)
	subl	%ebp, %edi
	movl	%edi, 200(%rcx)
	leal	(%rsi,%rdx,2), %eax
	movl	%eax, 196(%rcx)
	addl	%esi, %esi
	subl	%esi, %edx
	movl	%edx, 204(%rcx)
	movdqu	(%rcx), %xmm2
	movdqu	64(%rcx), %xmm3
	movdqu	128(%rcx), %xmm0
	movdqu	192(%rcx), %xmm1
	movd	%xmm2, %eax
	movd	%xmm1, %edx
	addl	%eax, %edx
	movd	%xmm3, %eax
	movd	%xmm0, %esi
	addl	%eax, %esi
	leal	(%rsi,%rdx), %eax
	movl	%eax, (%rcx)
	subl	%esi, %edx
	movl	%edx, 128(%rcx)
	pshufd	$229, %xmm2, %xmm4      # xmm4 = xmm2[1,1,2,3]
	movd	%xmm4, %ebx
	pshufd	$229, %xmm1, %xmm4      # xmm4 = xmm1[1,1,2,3]
	movd	%xmm4, %edx
	pshufd	$229, %xmm3, %xmm4      # xmm4 = xmm3[1,1,2,3]
	pshufd	$78, %xmm3, %xmm5       # xmm5 = xmm3[2,3,0,1]
	pshufd	$231, %xmm3, %xmm6      # xmm6 = xmm3[3,1,2,3]
	psubd	%xmm0, %xmm3
	movd	%xmm4, %eax
	pshufd	$229, %xmm0, %xmm4      # xmm4 = xmm0[1,1,2,3]
	movd	%xmm4, %esi
	pshufd	$78, %xmm2, %xmm4       # xmm4 = xmm2[2,3,0,1]
	movd	%xmm4, %edi
	pshufd	$78, %xmm1, %xmm4       # xmm4 = xmm1[2,3,0,1]
	movd	%xmm4, %ebp
	pshufd	$231, %xmm2, %xmm4      # xmm4 = xmm2[3,1,2,3]
	psubd	%xmm1, %xmm2
	addl	%ebx, %edx
	addl	%eax, %esi
	leal	(%rsi,%rdx), %eax
	movl	%eax, 4(%rcx)
	subl	%esi, %edx
	movl	%edx, 132(%rcx)
	addl	%edi, %ebp
	movd	%xmm5, %eax
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	movd	%xmm5, %edx
	addl	%eax, %edx
	leal	(%rdx,%rbp), %eax
	movl	%eax, 8(%rcx)
	subl	%edx, %ebp
	movl	%ebp, 136(%rcx)
	movd	%xmm4, %eax
	pshufd	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3]
	movd	%xmm1, %ebx
	addl	%eax, %ebx
	movd	%xmm6, %eax
	pshufd	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3]
	movd	%xmm0, %ebp
	addl	%eax, %ebp
	leal	(%rbp,%rbx), %eax
	movl	%eax, 12(%rcx)
	movl	%ebx, %eax
	subl	%ebp, %eax
	movl	%eax, 140(%rcx)
	movdqa	%xmm2, %xmm0
	paddd	%xmm0, %xmm0
	paddd	%xmm3, %xmm0
	movdqu	%xmm0, 64(%rcx)
	movdqa	%xmm3, %xmm0
	paddd	%xmm0, %xmm0
	movdqa	%xmm2, %xmm1
	psubd	%xmm0, %xmm1
	movdqu	%xmm1, 192(%rcx)
	addq	$4, %r12
	movslq	15544(%r15), %rdx
	addq	$16, %rcx
	cmpq	%rdx, %r12
	jl	.LBB4_10
# BB#11:                                # %._crit_edge972
                                        #   in Loop: Header=BB4_8 Depth=1
	movl	%ebx, dct_chroma.m5(%rip)
	movl	%ebp, dct_chroma.m5+4(%rip)
	pshufd	$231, %xmm3, %xmm0      # xmm0 = xmm3[3,1,2,3]
	movd	%xmm0, dct_chroma.m5+8(%rip)
	pshufd	$231, %xmm2, %xmm0      # xmm0 = xmm2[3,1,2,3]
	movd	%xmm0, dct_chroma.m5+12(%rip)
	movl	15548(%r15), %eax
	movq	-40(%rsp), %r12         # 8-byte Reload
.LBB4_12:                               #   in Loop: Header=BB4_8 Depth=1
	addq	$4, %r14
	movslq	%eax, %rcx
	addq	$256, %r9               # imm = 0x100
	cmpq	%rcx, %r14
	jl	.LBB4_8
.LBB4_13:                               # %.loopexit860
	movq	(%rsp), %rax            # 8-byte Reload
	sarl	%eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$1, %eax
	movq	%r10, -64(%rsp)         # 8-byte Spill
	je	.LBB4_37
# BB#14:                                # %.loopexit860
	cmpl	$2, %eax
	je	.LBB4_41
# BB#15:                                # %.loopexit860
	cmpl	$3, %eax
	jne	.LBB4_55
# BB#16:                                # %.preheader855
	movl	%r13d, -124(%rsp)       # 4-byte Spill
	movq	img(%rip), %r8
	movslq	15548(%r8), %r9
	testq	%r9, %r9
	jle	.LBB4_25
# BB#17:                                # %.preheader854.lr.ph
	movslq	15544(%r8), %r13
	testq	%r13, %r13
	jle	.LBB4_25
# BB#18:                                # %.preheader854.us.preheader
	leaq	-1(%r13), %r10
	shrq	$2, %r10
	movl	%r10d, %r14d
	andl	$1, %r14d
	xorl	%r12d, %r12d
	movq	%r8, %r15
	.p2align	4, 0x90
.LBB4_19:                               # %.preheader854.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_23 Depth 2
	movl	%r12d, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rbx
	testq	%r14, %r14
	jne	.LBB4_21
# BB#20:                                #   in Loop: Header=BB4_19 Depth=1
	movq	%r12, %rcx
	shlq	$6, %rcx
	movl	13136(%r8,%rcx), %ecx
	movl	%ecx, dct_chroma.m4(,%rbx,4)
	movl	$4, %ecx
	testq	%r10, %r10
	jne	.LBB4_22
	jmp	.LBB4_24
	.p2align	4, 0x90
.LBB4_21:                               #   in Loop: Header=BB4_19 Depth=1
	xorl	%ecx, %ecx
	testq	%r10, %r10
	je	.LBB4_24
.LBB4_22:                               # %.preheader854.us.new
                                        #   in Loop: Header=BB4_19 Depth=1
	leaq	(%r15,%rcx,4), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_23:                               #   Parent Loop BB4_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	13136(%rsi,%rdi,4), %eax
	leaq	(%rcx,%rdi), %rbp
	movl	%ebp, %edx
	sarl	$2, %edx
	movslq	%edx, %rdx
	shlq	$4, %rdx
	movl	%eax, dct_chroma.m4(%rdx,%rbx,4)
	movl	13152(%rsi,%rdi,4), %eax
	addl	$4, %ebp
	sarl	$2, %ebp
	movslq	%ebp, %rdx
	shlq	$4, %rdx
	movl	%eax, dct_chroma.m4(%rdx,%rbx,4)
	leaq	8(%rcx,%rdi), %rax
	addq	$8, %rdi
	cmpq	%r13, %rax
	jl	.LBB4_23
.LBB4_24:                               # %._crit_edge959.us
                                        #   in Loop: Header=BB4_19 Depth=1
	addq	$4, %r12
	addq	$256, %r15              # imm = 0x100
	cmpq	%r9, %r12
	jl	.LBB4_19
.LBB4_25:                               # %.preheader853
	cmpb	$0, -72(%rsp)           # 1-byte Folded Reload
	jne	.LBB4_27
# BB#26:                                # %.critedge.preheader
	movl	dct_chroma.m4(%rip), %r15d
	movl	dct_chroma.m4+48(%rip), %eax
	leal	(%rax,%r15), %r8d
	movl	dct_chroma.m4+16(%rip), %ecx
	movl	dct_chroma.m4+32(%rip), %edx
	leal	(%rdx,%rcx), %edi
	subl	%edx, %ecx
	subl	%eax, %r15d
	leal	(%rdi,%r8), %edx
	subl	%edi, %r8d
	leal	(%rcx,%r15), %r11d
	subl	%ecx, %r15d
	movl	dct_chroma.m4+4(%rip), %esi
	movl	dct_chroma.m4+52(%rip), %eax
	leal	(%rax,%rsi), %r14d
	movl	dct_chroma.m4+20(%rip), %edi
	movl	dct_chroma.m4+36(%rip), %ebp
	leal	(%rbp,%rdi), %ebx
	subl	%ebp, %edi
	subl	%eax, %esi
	leal	(%rbx,%r14), %ecx
	subl	%ebx, %r14d
	leal	(%rdi,%rsi), %r12d
	subl	%edi, %esi
	movl	dct_chroma.m4+8(%rip), %r10d
	movl	dct_chroma.m4+56(%rip), %r9d
	leal	(%r9,%r10), %eax
	movl	dct_chroma.m4+24(%rip), %ebx
	movl	dct_chroma.m4+40(%rip), %ebp
	leal	(%rbp,%rbx), %edi
	subl	%ebp, %ebx
	subl	%r9d, %r10d
	leal	(%rdi,%rax), %r9d
	subl	%edi, %eax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	leal	(%rbx,%r10), %eax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	subl	%ebx, %r10d
	movl	dct_chroma.m4+28(%rip), %ebx
	movl	dct_chroma.m4+44(%rip), %edi
	leal	(%rdi,%rbx), %r13d
	subl	%edi, %ebx
	movl	dct_chroma.m4+12(%rip), %edi
	movl	dct_chroma.m4+60(%rip), %eax
	leal	(%rax,%rdi), %ebp
	subl	%eax, %edi
	leal	(%r13,%rbp), %eax
	subl	%r13d, %ebp
	leal	(%rbx,%rdi), %r13d
	subl	%ebx, %edi
	leal	(%r9,%rcx), %ebx
	subl	%r9d, %ecx
	leal	(%rax,%rdx), %r9d
	subl	%eax, %edx
	leal	(%rbx,%r9), %eax
	subl	%ebx, %r9d
	leal	(%rcx,%rdx), %ebx
	subl	%ecx, %edx
	movd	%edx, %xmm8
	movd	%ebx, %xmm14
	movd	%r9d, %xmm9
	movd	%eax, %xmm2
	movq	-32(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%r12), %eax
	subl	%ecx, %r12d
	leal	(%r13,%r11), %ecx
	subl	%r13d, %r11d
	leal	(%rax,%rcx), %edx
	subl	%eax, %ecx
	leal	(%r12,%r11), %eax
	subl	%r12d, %r11d
	movd	%r11d, %xmm10
	movd	%eax, %xmm15
	movd	%ecx, %xmm11
	movd	%edx, %xmm6
	movq	-96(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%r14), %eax
	subl	%ecx, %r14d
	leal	(%rbp,%r8), %ecx
	subl	%ebp, %r8d
	leal	(%rax,%rcx), %edx
	subl	%eax, %ecx
	leal	(%r14,%r8), %eax
	subl	%r14d, %r8d
	movd	%r8d, %xmm12
	movd	%eax, %xmm3
	movd	%ecx, %xmm13
	movd	%edx, %xmm4
	leal	(%r10,%rsi), %eax
	subl	%r10d, %esi
	leal	(%rdi,%r15), %ecx
	subl	%edi, %r15d
	leal	(%rax,%rcx), %edx
	movl	%ecx, dct_chroma.m5(%rip)
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%eax, %ecx
	movl	%eax, dct_chroma.m5+4(%rip)
	leal	(%rsi,%r15), %eax
	movl	%esi, dct_chroma.m5+8(%rip)
	movl	%r15d, dct_chroma.m5+12(%rip)
	movl	%r15d, %edi
	subl	%esi, %edi
	movd	%edi, %xmm0
	movd	%eax, %xmm7
	movd	%ecx, %xmm1
	movd	%edx, %xmm5
	punpckldq	%xmm8, %xmm14   # xmm14 = xmm14[0],xmm8[0],xmm14[1],xmm8[1]
	punpckldq	%xmm9, %xmm2    # xmm2 = xmm2[0],xmm9[0],xmm2[1],xmm9[1]
	punpckldq	%xmm14, %xmm2   # xmm2 = xmm2[0],xmm14[0],xmm2[1],xmm14[1]
	psrad	$1, %xmm2
	movdqa	%xmm2, dct_chroma.m4(%rip)
	punpckldq	%xmm10, %xmm15  # xmm15 = xmm15[0],xmm10[0],xmm15[1],xmm10[1]
	punpckldq	%xmm11, %xmm6   # xmm6 = xmm6[0],xmm11[0],xmm6[1],xmm11[1]
	punpckldq	%xmm15, %xmm6   # xmm6 = xmm6[0],xmm15[0],xmm6[1],xmm15[1]
	psrad	$1, %xmm6
	movdqa	%xmm6, dct_chroma.m4+16(%rip)
	punpckldq	%xmm12, %xmm3   # xmm3 = xmm3[0],xmm12[0],xmm3[1],xmm12[1]
	punpckldq	%xmm13, %xmm4   # xmm4 = xmm4[0],xmm13[0],xmm4[1],xmm13[1]
	punpckldq	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	psrad	$1, %xmm4
	movdqa	%xmm4, dct_chroma.m4+32(%rip)
	punpckldq	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1]
	punpckldq	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	punpckldq	%xmm7, %xmm5    # xmm5 = xmm5[0],xmm7[0],xmm5[1],xmm7[1]
	psrad	$1, %xmm5
	movdqa	%xmm5, dct_chroma.m4+48(%rip)
.LBB4_27:                               # %.critedge6.preheader
	movl	-84(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ecx
	shlb	$4, %cl
	movl	$4294901760, %r9d       # imm = 0xFFFF0000
	shlq	%cl, %r9
	movq	-40(%rsp), %r12         # 8-byte Reload
	movq	-8(%rsp), %rax          # 8-byte Reload
	leaq	368(%r12,%rax), %r8
	movq	-80(%rsp), %rax         # 8-byte Reload
	leal	16(%rax), %ecx
	movl	$-1, %r11d
	xorl	%r14d, %r14d
	movq	$-32, %rdx
	xorl	%eax, %eax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	movl	-124(%rsp), %r13d       # 4-byte Reload
	movq	-112(%rsp), %r15        # 8-byte Reload
	.p2align	4, 0x90
.LBB4_28:                               # =>This Inner Loop Header: Depth=1
	movzbl	SNGL_SCAN+32(%rdx), %esi
	movzbl	SNGL_SCAN+33(%rdx), %r10d
	shlq	$4, %rsi
	movl	dct_chroma.m4(%rsi,%r10,4), %ebx
	movl	%ebx, %eax
	negl	%eax
	cmovll	%ebx, %eax
	cmpb	$0, -72(%rsp)           # 1-byte Folded Reload
	jne	.LBB4_30
# BB#29:                                #   in Loop: Header=BB4_28 Depth=1
	movq	-48(%rsp), %rdi         # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%rdi,8), %rbp
	movq	(%rbp), %rbp
	imull	(%rbp), %eax
	movq	-80(%rsp), %rdi         # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%rdi,8), %rbp
	movq	(%rbp), %rbp
	movl	(%rbp), %ebp
	leal	(%rax,%rbp,2), %eax
	sarl	%cl, %eax
.LBB4_30:                               #   in Loop: Header=BB4_28 Depth=1
	incl	%r11d
	testl	%eax, %eax
	je	.LBB4_32
# BB#31:                                #   in Loop: Header=BB4_28 Depth=1
	orq	%r9, (%r8)
	testl	%r13d, %r13d
	movl	$1, %ebp
	movl	$1, %edi
	movq	%rdi, -104(%rsp)        # 8-byte Spill
	cmovlel	%ebp, %r13d
	movl	%eax, %ebp
	negl	%ebp
	cmovll	%eax, %ebp
	movl	%ebp, %edi
	negl	%edi
	testl	%ebx, %ebx
	cmovnsl	%ebp, %edi
	movslq	%r14d, %r14
	movl	%edi, (%r15,%r14,4)
	movq	-120(%rsp), %rdi        # 8-byte Reload
	movl	%r11d, (%rdi,%r14,4)
	incl	%r14d
	movl	$-1, %r11d
.LBB4_32:                               #   in Loop: Header=BB4_28 Depth=1
	cmpb	$0, -72(%rsp)           # 1-byte Folded Reload
	jne	.LBB4_34
# BB#33:                                #   in Loop: Header=BB4_28 Depth=1
	leaq	dct_chroma.m4(%rsi,%r10,4), %rsi
	movl	%eax, %edi
	negl	%edi
	cmovll	%eax, %edi
	movl	%edi, %eax
	negl	%eax
	testl	%ebx, %ebx
	cmovnsl	%edi, %eax
	movl	%eax, (%rsi)
.LBB4_34:                               # %.critedge6
                                        #   in Loop: Header=BB4_28 Depth=1
	addq	$2, %rdx
	jne	.LBB4_28
# BB#35:
	movslq	%r14d, %rax
	movl	$0, (%r15,%rax,4)
	cmpb	$0, -72(%rsp)           # 1-byte Folded Reload
	je	.LBB4_56
# BB#36:
	movq	-64(%rsp), %r10         # 8-byte Reload
	movq	-56(%rsp), %r8          # 8-byte Reload
	jmp	.LBB4_106
.LBB4_37:
	movq	img(%rip), %r10
	movl	13136(%r10), %edi
	cmpb	$0, -72(%rsp)           # 1-byte Folded Reload
	je	.LBB4_62
# BB#38:
	movl	%edi, dct_chroma.m1(%rip)
	movl	%edi, %ebx
	negl	%ebx
	cmovll	%edi, %ebx
	movl	13152(%r10), %edx
	movl	%edx, dct_chroma.m1+4(%rip)
	movl	13392(%r10), %esi
	movl	%esi, dct_chroma.m1+8(%rip)
	movl	13408(%r10), %r14d
	movl	%r14d, dct_chroma.m1+12(%rip)
	movq	input(%rip), %r11
	movl	-84(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ecx
	shlb	$2, %cl
	movl	$983040, %eax           # imm = 0xF0000
	shll	%cl, %eax
	movslq	%eax, %r8
	movq	-8(%rsp), %rax          # 8-byte Reload
	leaq	368(%r12,%rax), %r9
	movl	4008(%r11), %ecx
	testl	%ecx, %ecx
	je	.LBB4_86
.LBB4_39:
	testl	%ebx, %ebx
	movl	%ebx, %eax
	jne	.LBB4_88
# BB#40:
	movl	$1, %edi
	xorl	%eax, %eax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	movq	-112(%rsp), %rbp        # 8-byte Reload
	jmp	.LBB4_89
.LBB4_41:                               # %.preheader849
	movq	%r8, -16(%rsp)          # 8-byte Spill
	movl	%r13d, -124(%rsp)       # 4-byte Spill
	movq	img(%rip), %r9
	movslq	15548(%r9), %r14
	testq	%r14, %r14
	jle	.LBB4_50
# BB#42:                                # %.preheader848.lr.ph
	movslq	15544(%r9), %rdx
	testq	%rdx, %rdx
	jle	.LBB4_50
# BB#43:                                # %.preheader848.us.preheader
	leaq	-1(%rdx), %r15
	shrq	$2, %r15
	movl	%r15d, %r12d
	andl	$1, %r12d
	xorl	%r11d, %r11d
	movq	%r9, %r13
	.p2align	4, 0x90
.LBB4_44:                               # %.preheader848.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_48 Depth 2
	movl	%r11d, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rbx
	testq	%r12, %r12
	jne	.LBB4_46
# BB#45:                                #   in Loop: Header=BB4_44 Depth=1
	movq	%r11, %rcx
	shlq	$6, %rcx
	movl	13136(%r9,%rcx), %ecx
	movl	%ecx, dct_chroma.m3(,%rbx,4)
	movl	$4, %ecx
	testq	%r15, %r15
	jne	.LBB4_47
	jmp	.LBB4_49
	.p2align	4, 0x90
.LBB4_46:                               #   in Loop: Header=BB4_44 Depth=1
	xorl	%ecx, %ecx
	testq	%r15, %r15
	je	.LBB4_49
.LBB4_47:                               # %.preheader848.us.new
                                        #   in Loop: Header=BB4_44 Depth=1
	leaq	(%r13,%rcx,4), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_48:                               #   Parent Loop BB4_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	13136(%rsi,%rdi,4), %eax
	leaq	(%rcx,%rdi), %r10
	movl	%r10d, %r8d
	sarl	$2, %r8d
	movslq	%r8d, %rbp
	shlq	$4, %rbp
	movl	%eax, dct_chroma.m3(%rbp,%rbx,4)
	movl	13152(%rsi,%rdi,4), %eax
	addl	$4, %r10d
	sarl	$2, %r10d
	movslq	%r10d, %rbp
	shlq	$4, %rbp
	movl	%eax, dct_chroma.m3(%rbp,%rbx,4)
	leaq	8(%rcx,%rdi), %rax
	addq	$8, %rdi
	cmpq	%rdx, %rax
	jl	.LBB4_48
.LBB4_49:                               # %._crit_edge931.us
                                        #   in Loop: Header=BB4_44 Depth=1
	addq	$4, %r11
	addq	$256, %r13              # imm = 0x100
	cmpq	%r14, %r11
	jl	.LBB4_44
.LBB4_50:                               # %._crit_edge933
	cmpb	$0, -72(%rsp)           # 1-byte Folded Reload
	je	.LBB4_72
# BB#51:                                # %.preheader847.thread
	movl	-84(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ecx
	shlb	$3, %cl
	movl	$16711680, %eax         # imm = 0xFF0000
	shll	%cl, %eax
	movslq	%eax, %r8
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	-8(%rsp), %rcx          # 8-byte Reload
	leaq	368(%rax,%rcx), %r9
	movl	$-1, %r10d
	xorl	%r11d, %r11d
	movq	$-16, %rsi
	xorl	%eax, %eax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	.p2align	4, 0x90
.LBB4_52:                               # %.preheader847.split.us
                                        # =>This Inner Loop Header: Depth=1
	incl	%r10d
	movzbl	SCAN_YUV422+16(%rsi), %ecx
	movzbl	SCAN_YUV422+17(%rsi), %edx
	shlq	$4, %rcx
	movl	dct_chroma.m3(%rcx,%rdx,4), %edi
	movl	%edi, %ebp
	sarl	$31, %ebp
	leal	(%rdi,%rbp), %ebx
	xorl	%ebp, %ebx
	movl	%edi, dct_chroma.m4(%rcx,%rdx,4)
	je	.LBB4_54
# BB#53:                                #   in Loop: Header=BB4_52 Depth=1
	orq	%r8, (%r9)
	movl	-124(%rsp), %eax        # 4-byte Reload
	testl	%eax, %eax
	movl	$1, %edx
	movl	$1, %ecx
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	cmovlel	%edx, %eax
	movl	%eax, -124(%rsp)        # 4-byte Spill
	negl	%ebx
	testl	%edi, %edi
	cmovnsl	%edi, %ebx
	movslq	%r11d, %r11
	movq	-112(%rsp), %rax        # 8-byte Reload
	movl	%ebx, (%rax,%r11,4)
	movq	-120(%rsp), %rax        # 8-byte Reload
	movl	%r10d, (%rax,%r11,4)
	incl	%r11d
	movl	$-1, %r10d
.LBB4_54:                               #   in Loop: Header=BB4_52 Depth=1
	addq	$2, %rsi
	jne	.LBB4_52
	jmp	.LBB4_77
.LBB4_55:
	xorl	%eax, %eax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	movq	-56(%rsp), %r8          # 8-byte Reload
	jmp	.LBB4_106
.LBB4_56:                               # %.preheader852.preheader
	movl	%r13d, -124(%rsp)       # 4-byte Spill
	movl	dct_chroma.m4(%rip), %eax
	movl	dct_chroma.m4+32(%rip), %ecx
	leal	(%rcx,%rax), %edx
	subl	%ecx, %eax
	movl	dct_chroma.m4+16(%rip), %ecx
	movl	dct_chroma.m4+48(%rip), %edi
	movl	%ecx, %ebp
	subl	%edi, %ebp
	addl	%ecx, %edi
	leal	(%rdx,%rdi), %esi
	movl	%esi, dct_chroma.m4(%rip)
	leal	(%rbp,%rax), %ecx
	movl	%ecx, dct_chroma.m4+16(%rip)
	subl	%ebp, %eax
	movl	%eax, dct_chroma.m4+32(%rip)
	subl	%edi, %edx
	movl	%edx, dct_chroma.m4+48(%rip)
	movl	dct_chroma.m4+4(%rip), %eax
	movl	dct_chroma.m4+36(%rip), %ecx
	leal	(%rcx,%rax), %edx
	subl	%ecx, %eax
	movl	dct_chroma.m4+20(%rip), %ecx
	movl	dct_chroma.m4+52(%rip), %edi
	movl	%ecx, %ebp
	subl	%edi, %ebp
	addl	%ecx, %edi
	leal	(%rdx,%rdi), %r15d
	movl	%r15d, dct_chroma.m4+4(%rip)
	leal	(%rbp,%rax), %ecx
	movl	%ecx, dct_chroma.m4+20(%rip)
	subl	%ebp, %eax
	movl	%eax, dct_chroma.m4+36(%rip)
	subl	%edi, %edx
	movl	%edx, dct_chroma.m4+52(%rip)
	movl	dct_chroma.m4+8(%rip), %eax
	movl	dct_chroma.m4+40(%rip), %ecx
	leal	(%rcx,%rax), %edx
	subl	%ecx, %eax
	movl	dct_chroma.m4+24(%rip), %ecx
	movl	dct_chroma.m4+56(%rip), %ebp
	movl	%ecx, %ebx
	subl	%ebp, %ebx
	addl	%ecx, %ebp
	leal	(%rdx,%rbp), %edi
	movl	%edi, dct_chroma.m4+8(%rip)
	leal	(%rbx,%rax), %ecx
	movl	%ecx, dct_chroma.m4+24(%rip)
	subl	%ebx, %eax
	movl	%eax, dct_chroma.m4+40(%rip)
	subl	%ebp, %edx
	movl	%edx, dct_chroma.m4+56(%rip)
	movl	dct_chroma.m4+12(%rip), %ecx
	movl	dct_chroma.m4+44(%rip), %eax
	leal	(%rax,%rcx), %r8d
	subl	%eax, %ecx
	movl	dct_chroma.m4+28(%rip), %eax
	movl	dct_chroma.m4+60(%rip), %ebp
	movl	%eax, %ebx
	subl	%ebp, %ebx
	addl	%eax, %ebp
	leal	(%r8,%rbp), %eax
	movl	%eax, dct_chroma.m4+12(%rip)
	leal	(%rbx,%rcx), %edx
	movl	%edx, dct_chroma.m4+28(%rip)
	movl	%ecx, dct_chroma.m6.1(%rip)
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%ebx, %ecx
	movl	%ecx, dct_chroma.m4+44(%rip)
	movl	%r8d, dct_chroma.m6.0(%rip)
	movl	%r8d, %ecx
	subl	%ebp, %ecx
	movl	%ecx, dct_chroma.m4+60(%rip)
	movl	%ebx, dct_chroma.m6.2(%rip)
	movl	%ebp, dct_chroma.m6.3(%rip)
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%rcx,8), %rcx
	movq	(%rcx), %r12
	movl	$3, %ecx
	movq	-80(%rsp), %rdx         # 8-byte Reload
	subl	%edx, %ecx
	movl	$1, %r8d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r8d
	movl	$4, %r10d
	subl	%edx, %r10d
	movq	img(%rip), %r13
	leal	-4(%rdx), %r9d
	xorl	%edx, %edx
	movl	$3, %r11d
	xorl	%r14d, %r14d
	jmp	.LBB4_58
	.p2align	4, 0x90
.LBB4_57:                               # %._crit_edge1112
                                        #   in Loop: Header=BB4_58 Depth=1
	incq	%r14
	movl	dct_chroma.m4+16(,%rdx,4), %esi
	movl	dct_chroma.m4+24(,%rdx,4), %edi
	movl	dct_chroma.m4+20(,%rdx,4), %r15d
	movl	dct_chroma.m4+28(,%rdx,4), %eax
	addq	$4, %rdx
	decq	%r11
.LBB4_58:                               # =>This Inner Loop Header: Depth=1
	leal	(%rdi,%rsi), %ebx
	movl	%ebx, dct_chroma.m6.0(%rip)
	subl	%edi, %esi
	movl	%esi, dct_chroma.m6.1(%rip)
	movl	%r15d, %edi
	subl	%eax, %edi
	movl	%edi, dct_chroma.m6.2(%rip)
	addl	%r15d, %eax
	movl	%eax, dct_chroma.m6.3(%rip)
	leal	(%rbx,%rax), %ebp
	imull	(%r12), %ebp
	cmpl	$3, -80(%rsp)           # 4-byte Folded Reload
	jg	.LBB4_60
# BB#59:                                #   in Loop: Header=BB4_58 Depth=1
	addl	%r8d, %ebp
	movl	%r10d, %ecx
	sarl	%cl, %ebp
	addl	$2, %ebp
	sarl	$2, %ebp
	leaq	(,%r14,4), %r15
	movl	%ebp, 13136(%r13,%rdx,4)
	leal	(%rdi,%rsi), %ebp
	imull	(%r12), %ebp
	addl	%r8d, %ebp
	sarl	%cl, %ebp
	addl	$2, %ebp
	sarl	$2, %ebp
	movl	%ebp, 13392(%r13,%rdx,4)
	subl	%edi, %esi
	imull	(%r12), %esi
	addl	%r8d, %esi
	sarl	%cl, %esi
	addl	$2, %esi
	sarl	$2, %esi
	movl	%esi, 13648(%r13,%rdx,4)
	subl	%eax, %ebx
	imull	(%r12), %ebx
	addl	%r8d, %ebx
	sarl	%cl, %ebx
	jmp	.LBB4_61
	.p2align	4, 0x90
.LBB4_60:                               #   in Loop: Header=BB4_58 Depth=1
	movl	%r9d, %ecx
	shll	%cl, %ebp
	addl	$2, %ebp
	sarl	$2, %ebp
	movl	%ebp, 13136(%r13,%rdx,4)
	leal	(%rdi,%rsi), %ebp
	imull	(%r12), %ebp
	shll	%cl, %ebp
	addl	$2, %ebp
	sarl	$2, %ebp
	movl	%ebp, 13392(%r13,%rdx,4)
	subl	%edi, %esi
	imull	(%r12), %esi
	shll	%cl, %esi
	addl	$2, %esi
	sarl	$2, %esi
	movl	%esi, 13648(%r13,%rdx,4)
	subl	%eax, %ebx
	imull	(%r12), %ebx
	shll	%cl, %ebx
	movq	%rdx, %r15
.LBB4_61:                               #   in Loop: Header=BB4_58 Depth=1
	addl	$2, %ebx
	sarl	$2, %ebx
	movl	%ebx, 13904(%r13,%r15,4)
	testq	%r11, %r11
	jne	.LBB4_57
	jmp	.LBB4_84
.LBB4_62:
	movl	13152(%r10), %eax
	movl	13392(%r10), %ecx
	leal	(%rax,%rdi), %edx
	leal	(%rdx,%rcx), %ebx
	movl	13408(%r10), %esi
	addl	%esi, %ebx
	movl	%ebx, dct_chroma.m1(%rip)
	subl	%eax, %edi
	leal	(%rdi,%rcx), %eax
	subl	%esi, %eax
	movl	%eax, dct_chroma.m1+4(%rip)
	subl	%ecx, %edx
	subl	%esi, %edx
	movl	%edx, dct_chroma.m1+8(%rip)
	subl	%ecx, %edi
	addl	%esi, %edi
	movl	%edi, dct_chroma.m1+12(%rip)
	movq	-48(%rsp), %rax         # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %r14
	movq	-80(%rsp), %rcx         # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %r15
	leal	16(%rcx), %r11d
	movq	%r12, %rax
	movq	input(%rip), %r12
	movl	-84(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlb	$2, %cl
	movl	$983040, %edx           # imm = 0xF0000
	shll	%cl, %edx
	movslq	%edx, %r8
	movq	-8(%rsp), %rcx          # 8-byte Reload
	leaq	368(%rax,%rcx), %r9
	movl	$-1, %esi
	xorl	%edx, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	jmp	.LBB4_64
	.p2align	4, 0x90
.LBB4_63:                               # %._crit_edge1133
                                        #   in Loop: Header=BB4_64 Depth=1
	movl	dct_chroma.m1+4(%rdx), %ebx
	addq	$4, %rdx
.LBB4_64:                               # =>This Inner Loop Header: Depth=1
	incl	%esi
	movl	%ebx, %ecx
	negl	%ecx
	cmovll	%ebx, %ecx
	imull	(%r14), %ecx
	movl	(%r15), %ebp
	leal	(%rcx,%rbp,2), %ebp
	movl	%r11d, %ecx
	sarl	%cl, %ebp
	cmpl	$0, 4008(%r12)
	je	.LBB4_67
.LBB4_65:                               #   in Loop: Header=BB4_64 Depth=1
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB4_69
# BB#66:                                #   in Loop: Header=BB4_64 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB4_70
	.p2align	4, 0x90
.LBB4_67:                               #   in Loop: Header=BB4_64 Depth=1
	cmpl	$2064, %ebp             # imm = 0x810
	jl	.LBB4_65
# BB#68:                                #   in Loop: Header=BB4_64 Depth=1
	movl	$2063, %eax             # imm = 0x80F
	cmpl	$4, 36(%r10)
	jge	.LBB4_65
.LBB4_69:                               # %.thread
                                        #   in Loop: Header=BB4_64 Depth=1
	orq	%r8, (%r9)
	testl	%r13d, %r13d
	movl	$1, %ebp
	movl	$1, %ecx
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	cmovlel	%ebp, %r13d
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %ebp
	negl	%ebp
	testl	%ebx, %ebx
	cmovnsl	%ecx, %ebp
	movslq	%edi, %rdi
	movq	-112(%rsp), %rax        # 8-byte Reload
	movl	%ebp, (%rax,%rdi,4)
	movq	-120(%rsp), %rax        # 8-byte Reload
	movl	%esi, (%rax,%rdi,4)
	incl	%edi
	movl	$-1, %esi
.LBB4_70:                               #   in Loop: Header=BB4_64 Depth=1
	movl	%ebp, dct_chroma.m1(%rdx)
	cmpq	$12, %rdx
	jne	.LBB4_63
# BB#71:
	movslq	%edi, %rax
	movq	-112(%rsp), %rcx        # 8-byte Reload
	movl	$0, (%rcx,%rax,4)
	movl	dct_chroma.m1(%rip), %eax
	movl	dct_chroma.m1+4(%rip), %ecx
	leal	(%rcx,%rax), %edx
	movl	dct_chroma.m1+8(%rip), %ebp
	leal	(%rdx,%rbp), %esi
	movl	dct_chroma.m1+12(%rip), %ebx
	addl	%ebx, %esi
	movl	%esi, dct_chroma.m5(%rip)
	subl	%ecx, %eax
	leal	(%rax,%rbp), %edi
	subl	%ebx, %edi
	movl	%edi, dct_chroma.m5+4(%rip)
	subl	%ebp, %edx
	subl	%ebx, %edx
	movl	%edx, dct_chroma.m5+8(%rip)
	subl	%ebp, %eax
	addl	%ebx, %eax
	movl	%eax, dct_chroma.m5+12(%rip)
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%rcx,8), %rcx
	movq	(%rcx), %rbp
	imull	(%rbp), %esi
	movq	-80(%rsp), %rcx         # 8-byte Reload
	shll	%cl, %esi
	sarl	$5, %esi
	movl	%esi, dct_chroma.m1(%rip)
	imull	(%rbp), %edi
	shll	%cl, %edi
	sarl	$5, %edi
	movl	%edi, dct_chroma.m1+4(%rip)
	imull	(%rbp), %edx
	shll	%cl, %edx
	sarl	$5, %edx
	movl	%edx, dct_chroma.m1+8(%rip)
	imull	(%rbp), %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	sarl	$5, %eax
	movl	%eax, dct_chroma.m1+12(%rip)
	movq	img(%rip), %rcx
	movl	%esi, 13136(%rcx)
	movl	%edi, 13152(%rcx)
	movl	%edx, 13392(%rcx)
	movl	%eax, 13408(%rcx)
	jmp	.LBB4_85
.LBB4_72:                               # %.preheader847
	movl	dct_chroma.m3(%rip), %eax
	movl	dct_chroma.m3+16(%rip), %ebp
	leal	(%rbp,%rax), %esi
	movl	dct_chroma.m3+4(%rip), %ecx
	movl	dct_chroma.m3+20(%rip), %r8d
	leal	(%r8,%rcx), %edi
	movl	dct_chroma.m3+8(%rip), %r11d
	movl	dct_chroma.m3+24(%rip), %r10d
	leal	(%r10,%r11), %r9d
	subl	%ebp, %eax
	movl	dct_chroma.m3+12(%rip), %ebp
	subl	%r8d, %ecx
	movl	dct_chroma.m3+28(%rip), %ebx
	subl	%r10d, %r11d
	leal	(%rbx,%rbp), %r8d
	subl	%ebx, %ebp
	leal	(%r9,%rdi), %r10d
	subl	%r9d, %edi
	leal	(%r8,%rsi), %edx
	subl	%r8d, %esi
	leal	(%r10,%rdx), %ebx
	movl	%ebx, dct_chroma.m4(%rip)
	subl	%r10d, %edx
	movl	%edx, dct_chroma.m4+8(%rip)
	leal	(%rdi,%rsi), %edx
	movl	%edx, dct_chroma.m4+4(%rip)
	subl	%edi, %esi
	movl	%esi, dct_chroma.m4+12(%rip)
	leal	(%rbp,%rax), %edx
	leal	(%r11,%rcx), %esi
	subl	%r11d, %ecx
	subl	%ebp, %eax
	leal	(%rsi,%rdx), %edi
	movl	%edi, dct_chroma.m4+16(%rip)
	movl	%edx, dct_chroma.m5(%rip)
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	subl	%esi, %edx
	movl	%edx, dct_chroma.m4+24(%rip)
	leal	(%rcx,%rax), %edx
	movl	%edx, dct_chroma.m4+20(%rip)
	movl	%esi, dct_chroma.m5+4(%rip)
	movl	%ecx, dct_chroma.m5+8(%rip)
	movl	%eax, dct_chroma.m5+12(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%ecx, %eax
	movl	%eax, dct_chroma.m4+28(%rip)
	movl	-84(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ecx
	shlb	$3, %cl
	movl	$16711680, %eax         # imm = 0xFF0000
	shll	%cl, %eax
	movslq	%eax, %r9
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	-8(%rsp), %rcx          # 8-byte Reload
	leaq	368(%rax,%rcx), %r14
	movslq	-96(%rsp), %rax         # 4-byte Folded Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	-16(%rsp), %rdx         # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx), %r15
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %r12
	movl	$-1, %r13d
	xorl	%r11d, %r11d
	movq	$-16, %rbx
	xorl	%eax, %eax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	movl	-32(%rsp), %ecx         # 4-byte Reload
	.p2align	4, 0x90
.LBB4_73:                               # %.preheader847.split
                                        # =>This Inner Loop Header: Depth=1
	incl	%r13d
	movzbl	SCAN_YUV422+16(%rbx), %r10d
	movzbl	SCAN_YUV422+17(%rbx), %r8d
	shlq	$4, %r10
	movl	dct_chroma.m4(%r10,%r8,4), %ebp
	movl	%ebp, %eax
	negl	%eax
	cmovll	%ebp, %eax
	imull	(%r15), %eax
	movl	(%r12), %esi
	leal	(%rax,%rsi,2), %eax
	sarl	%cl, %eax
	testl	%eax, %eax
	je	.LBB4_75
# BB#74:                                #   in Loop: Header=BB4_73 Depth=1
	orq	%r9, (%r14)
	movl	-124(%rsp), %edi        # 4-byte Reload
	testl	%edi, %edi
	movl	$1, %esi
	movl	$1, %edx
	movq	%rdx, -104(%rsp)        # 8-byte Spill
	cmovlel	%esi, %edi
	movl	%edi, -124(%rsp)        # 4-byte Spill
	movl	%eax, %edi
	negl	%edi
	movl	%eax, %edx
	cmovgel	%edi, %edx
	movl	%edx, %esi
	negl	%esi
	testl	%ebp, %ebp
	cmovnsl	%edx, %esi
	movslq	%r11d, %r11
	movq	-112(%rsp), %rdx        # 8-byte Reload
	movl	%esi, (%rdx,%r11,4)
	movq	-120(%rsp), %rdx        # 8-byte Reload
	movl	%r13d, (%rdx,%r11,4)
	incl	%r11d
	movl	$-1, %r13d
	jmp	.LBB4_76
	.p2align	4, 0x90
.LBB4_75:                               # %.preheader847.split._crit_edge
                                        #   in Loop: Header=BB4_73 Depth=1
	movl	%eax, %edi
	negl	%edi
.LBB4_76:                               #   in Loop: Header=BB4_73 Depth=1
	testl	%eax, %eax
	cmovnsl	%eax, %edi
	movl	%edi, %eax
	negl	%eax
	testl	%ebp, %ebp
	cmovnsl	%edi, %eax
	movl	%eax, dct_chroma.m3(%r10,%r8,4)
	addq	$2, %rbx
	jne	.LBB4_73
.LBB4_77:                               # %.us-lcssa923.us
	movslq	%r11d, %rax
	movq	-112(%rsp), %rcx        # 8-byte Reload
	movl	$0, (%rcx,%rax,4)
	cmpb	$0, -72(%rsp)           # 1-byte Folded Reload
	jne	.LBB4_84
# BB#78:
	movdqa	dct_chroma.m3(%rip), %xmm0
	movdqa	dct_chroma.m3+16(%rip), %xmm1
	movd	%xmm0, %eax
	movd	%xmm1, %ebp
	addl	%eax, %ebp
	movl	%ebp, dct_chroma.m4(%rip)
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	movd	%xmm2, %eax
	pshufd	$229, %xmm1, %xmm2      # xmm2 = xmm1[1,1,2,3]
	movd	%xmm2, %esi
	addl	%eax, %esi
	movl	%esi, dct_chroma.m4+4(%rip)
	pshufd	$78, %xmm0, %xmm2       # xmm2 = xmm0[2,3,0,1]
	movd	%xmm2, %eax
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	movd	%xmm2, %edi
	addl	%eax, %edi
	movl	%edi, dct_chroma.m4+8(%rip)
	pshufd	$231, %xmm0, %xmm2      # xmm2 = xmm0[3,1,2,3]
	movd	%xmm2, %ecx
	pshufd	$231, %xmm1, %xmm2      # xmm2 = xmm1[3,1,2,3]
	movd	%xmm2, %eax
	addl	%ecx, %eax
	movl	%eax, dct_chroma.m4+12(%rip)
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, dct_chroma.m4+16(%rip)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	-16(%rsp), %rdx         # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx), %r15
	movl	$3, %ecx
	movq	-96(%rsp), %rdx         # 8-byte Reload
	subl	%edx, %ecx
	movl	$1, %r8d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r8d
	movl	$4, %r10d
	subl	%edx, %r10d
	movq	img(%rip), %r12
	leal	-4(%rdx), %r9d
	xorl	%edx, %edx
	movl	$1, %r14d
	xorl	%r11d, %r11d
	jmp	.LBB4_80
	.p2align	4, 0x90
.LBB4_79:                               # %._crit_edge1124
                                        #   in Loop: Header=BB4_80 Depth=1
	incq	%r11
	movl	dct_chroma.m4+16(,%rdx,4), %ebp
	movl	dct_chroma.m4+24(,%rdx,4), %edi
	movl	dct_chroma.m4+20(,%rdx,4), %esi
	movl	dct_chroma.m4+28(,%rdx,4), %eax
	addq	$4, %rdx
	decq	%r14
.LBB4_80:                               # =>This Inner Loop Header: Depth=1
	leal	(%rdi,%rbp), %ebx
	movl	%ebx, dct_chroma.m6.0(%rip)
	subl	%edi, %ebp
	movl	%ebp, dct_chroma.m6.1(%rip)
	movl	%esi, %edi
	subl	%eax, %edi
	movl	%edi, dct_chroma.m6.2(%rip)
	addl	%esi, %eax
	movl	%eax, dct_chroma.m6.3(%rip)
	leal	(%rbx,%rax), %esi
	imull	(%r15), %esi
	cmpl	$3, -96(%rsp)           # 4-byte Folded Reload
	jg	.LBB4_82
# BB#81:                                #   in Loop: Header=BB4_80 Depth=1
	addl	%r8d, %esi
	movl	%r10d, %ecx
	sarl	%cl, %esi
	addl	$2, %esi
	sarl	$2, %esi
	leaq	(,%r11,4), %r13
	movl	%esi, 13136(%r12,%rdx,4)
	leal	(%rdi,%rbp), %esi
	imull	(%r15), %esi
	addl	%r8d, %esi
	sarl	%cl, %esi
	addl	$2, %esi
	sarl	$2, %esi
	movl	%esi, 13392(%r12,%rdx,4)
	subl	%edi, %ebp
	imull	(%r15), %ebp
	addl	%r8d, %ebp
	sarl	%cl, %ebp
	addl	$2, %ebp
	sarl	$2, %ebp
	movl	%ebp, 13648(%r12,%rdx,4)
	subl	%eax, %ebx
	imull	(%r15), %ebx
	addl	%r8d, %ebx
	sarl	%cl, %ebx
	jmp	.LBB4_83
	.p2align	4, 0x90
.LBB4_82:                               #   in Loop: Header=BB4_80 Depth=1
	movl	%r9d, %ecx
	shll	%cl, %esi
	addl	$2, %esi
	sarl	$2, %esi
	movl	%esi, 13136(%r12,%rdx,4)
	leal	(%rdi,%rbp), %esi
	imull	(%r15), %esi
	shll	%cl, %esi
	addl	$2, %esi
	sarl	$2, %esi
	movl	%esi, 13392(%r12,%rdx,4)
	subl	%edi, %ebp
	imull	(%r15), %ebp
	shll	%cl, %ebp
	addl	$2, %ebp
	sarl	$2, %ebp
	movl	%ebp, 13648(%r12,%rdx,4)
	subl	%eax, %ebx
	imull	(%r15), %ebx
	shll	%cl, %ebx
	movq	%rdx, %r13
.LBB4_83:                               #   in Loop: Header=BB4_80 Depth=1
	addl	$2, %ebx
	sarl	$2, %ebx
	movl	%ebx, 13904(%r12,%r13,4)
	testq	%r14, %r14
	jne	.LBB4_79
.LBB4_84:
	movl	-124(%rsp), %r13d       # 4-byte Reload
.LBB4_85:                               # %.loopexit846
	movq	-64(%rsp), %r10         # 8-byte Reload
	movq	-56(%rsp), %r8          # 8-byte Reload
	movq	-40(%rsp), %r12         # 8-byte Reload
	jmp	.LBB4_106
.LBB4_86:
	cmpl	$2064, %ebx             # imm = 0x810
	jl	.LBB4_39
# BB#87:
	movl	$2063, %eax             # imm = 0x80F
	cmpl	$4, 36(%r10)
	jge	.LBB4_39
.LBB4_88:                               # %.thread819
	orq	%r8, (%r9)
	testl	%r13d, %r13d
	movl	$1, %ebx
	movl	$1, %ecx
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	cmovlel	%ebx, %r13d
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	testl	%edi, %edi
	cmovnsl	%ecx, %eax
	movq	-112(%rsp), %rbp        # 8-byte Reload
	movl	%eax, (%rbp)
	movq	-120(%rsp), %rax        # 8-byte Reload
	movl	$0, (%rax)
	movl	4008(%r11), %ecx
	xorl	%edi, %edi
.LBB4_89:
	movl	%edx, %ebx
	negl	%ebx
	cmovll	%edx, %ebx
	testl	%ecx, %ecx
	je	.LBB4_92
.LBB4_90:
	testl	%ebx, %ebx
	movl	%ebx, %eax
	jne	.LBB4_94
# BB#91:
	movq	-104(%rsp), %rbx        # 8-byte Reload
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	jmp	.LBB4_95
.LBB4_92:
	cmpl	$2064, %ebx             # imm = 0x810
	jl	.LBB4_90
# BB#93:
	movl	$2063, %eax             # imm = 0x80F
	cmpl	$4, 36(%r10)
	jge	.LBB4_90
.LBB4_94:                               # %.thread819.1
	orq	%r8, (%r9)
	testl	%r13d, %r13d
	movl	$1, %ecx
	cmovlel	%ecx, %r13d
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	testl	%edx, %edx
	cmovnsl	%ecx, %eax
	movq	-104(%rsp), %rcx        # 8-byte Reload
	movl	%eax, (%rbp,%rcx,4)
	movq	-120(%rsp), %rax        # 8-byte Reload
	movl	%edi, (%rax,%rcx,4)
	movl	%ecx, %ebx
	incl	%ebx
	movl	4008(%r11), %ecx
	movl	$-1, %edi
	movl	$1, %eax
	movq	%rax, -104(%rsp)        # 8-byte Spill
.LBB4_95:
	incl	%edi
	movl	%esi, %edx
	negl	%edx
	cmovll	%esi, %edx
	testl	%ecx, %ecx
	je	.LBB4_97
.LBB4_96:
	testl	%edx, %edx
	movl	%edx, %eax
	jne	.LBB4_99
	jmp	.LBB4_100
.LBB4_97:
	cmpl	$2064, %edx             # imm = 0x810
	jl	.LBB4_96
# BB#98:
	movl	$2063, %eax             # imm = 0x80F
	cmpl	$4, 36(%r10)
	jge	.LBB4_96
.LBB4_99:                               # %.thread819.2
	orq	%r8, (%r9)
	testl	%r13d, %r13d
	movl	$1, %edx
	movl	$1, %ecx
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	cmovlel	%edx, %r13d
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	testl	%esi, %esi
	cmovnsl	%ecx, %eax
	movslq	%ebx, %rbx
	movl	%eax, (%rbp,%rbx,4)
	movq	-120(%rsp), %rax        # 8-byte Reload
	movl	%edi, (%rax,%rbx,4)
	incl	%ebx
	movl	4008(%r11), %ecx
	movl	$-1, %edi
.LBB4_100:
	movl	%r14d, %edx
	negl	%edx
	cmovll	%r14d, %edx
	testl	%ecx, %ecx
	je	.LBB4_102
.LBB4_101:
	testl	%edx, %edx
	movl	%edx, %ecx
	jne	.LBB4_104
	jmp	.LBB4_105
.LBB4_102:
	cmpl	$2064, %edx             # imm = 0x810
	jl	.LBB4_101
# BB#103:
	movl	$2063, %ecx             # imm = 0x80F
	cmpl	$4, 36(%r10)
	jge	.LBB4_101
.LBB4_104:                              # %.thread819.3
	incl	%edi
	orq	%r8, (%r9)
	testl	%r13d, %r13d
	movl	$1, %edx
	movl	$1, %eax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	cmovlel	%edx, %r13d
	movl	%ecx, %eax
	negl	%eax
	cmovll	%ecx, %eax
	movl	%eax, %ecx
	negl	%ecx
	testl	%r14d, %r14d
	cmovnsl	%eax, %ecx
	movslq	%ebx, %rbx
	movl	%ecx, (%rbp,%rbx,4)
	movq	-120(%rsp), %rax        # 8-byte Reload
	movl	%edi, (%rax,%rbx,4)
	incl	%ebx
.LBB4_105:
	movq	-64(%rsp), %r10         # 8-byte Reload
	movq	-56(%rsp), %r8          # 8-byte Reload
	movslq	%ebx, %rax
	movl	$0, (%rbp,%rax,4)
.LBB4_106:                              # %.loopexit846
	movl	%r13d, -124(%rsp)       # 4-byte Spill
	movq	(%rsp), %rdx            # 8-byte Reload
	imull	-84(%rsp), %edx         # 4-byte Folded Reload
	movq	img(%rip), %r14
	movl	15528(%r14), %eax
	movl	%eax, %ecx
	sarl	%ecx
	testl	%ecx, %ecx
	movq	%rdx, (%rsp)            # 8-byte Spill
	jle	.LBB4_131
# BB#107:                               # %.preheader845.lr.ph
	movq	-80(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rsi
	leal	15(%rsi), %ecx
	leal	4(%rdx), %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	-8(%rsp), %rax          # 8-byte Reload
	leaq	368(%r12,%rax), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leal	16(%rsi), %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movl	$1, %eax
	movl	%ecx, -32(%rsp)         # 4-byte Spill
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movslq	%edx, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	xorl	%r12d, %r12d
	movl	$0, -20(%rsp)           # 4-byte Folded Spill
	movq	8(%rsp), %r13           # 8-byte Reload
	.p2align	4, 0x90
.LBB4_108:                              # %.preheader845
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_123 Depth 2
                                        #       Child Loop BB4_110 Depth 3
                                        #       Child Loop BB4_125 Depth 3
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rdx,%rax), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rdx), %eax
	cltq
	movq	%rax, 104(%rsp)         # 8-byte Spill
	xorl	%esi, %esi
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	jmp	.LBB4_123
	.p2align	4, 0x90
.LBB4_109:                              # %.preheader843.preheader
                                        #   in Loop: Header=BB4_123 Depth=2
	movl	$-1, -120(%rsp)         # 4-byte Folded Spill
	xorl	%eax, %eax
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB4_110:                              # %.preheader843
                                        #   Parent Loop BB4_108 Depth=1
                                        #     Parent Loop BB4_123 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r12d, -96(%rsp)        # 4-byte Spill
	movzbl	2(%r8,%rax,2), %r15d
	movzbl	3(%r8,%rax,2), %esi
	movq	-112(%rsp), %rcx        # 8-byte Reload
	leaq	(%rsi,%rcx), %rbp
	movq	%r13, %rbx
	leaq	(%r15,%r13), %rdx
	movq	%rbp, %r8
	shlq	$6, %r8
	addq	%r14, %r8
	movl	13136(%r8,%rdx,4), %ecx
	movq	%r10, %r13
	movq	%r14, %r10
	movl	%ecx, %r14d
	negl	%r14d
	cmovll	%ecx, %r14d
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi,%rcx,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	imull	(%rcx,%r15,4), %r14d
	movq	-80(%rsp), %rcx         # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi,%rcx,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movl	(%rcx,%r15,4), %r9d
	addl	%r14d, %r9d
	movl	-32(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r9d
	movq	%r10, %r12
	cmpl	$0, 15260(%r10)
	movq	%r13, %r10
	je	.LBB4_115
# BB#111:                               #   in Loop: Header=BB4_110 Depth=3
	testl	%r9d, %r9d
	je	.LBB4_113
# BB#112:                               #   in Loop: Header=BB4_110 Depth=3
	movl	%r9d, %r10d
	movl	-32(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r10d
	subl	%r10d, %r14d
	movq	-64(%rsp), %r10         # 8-byte Reload
	imull	AdaptRndCrWeight(%rip), %r14d
	addl	40(%rsp), %r14d         # 4-byte Folded Reload
	movl	44(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r14d
	jmp	.LBB4_114
	.p2align	4, 0x90
.LBB4_113:                              #   in Loop: Header=BB4_110 Depth=3
	xorl	%r14d, %r14d
.LBB4_114:                              #   in Loop: Header=BB4_110 Depth=3
	movq	14192(%r12), %rcx
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	%r14d, (%rcx,%rdx,4)
.LBB4_115:                              #   in Loop: Header=BB4_110 Depth=3
	incl	-120(%rsp)              # 4-byte Folded Spill
	leaq	13136(%r8,%rdx,4), %rdx
	testl	%r9d, %r9d
	movq	%rbx, %r13
	je	.LBB4_119
# BB#116:                               #   in Loop: Header=BB4_110 Depth=3
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	80(%rsp), %rdi          # 8-byte Reload
	orq	%rdi, (%rcx)
	movl	$999999, %ecx           # imm = 0xF423F
	cmpl	$1, %r9d
	movq	%r12, %r14
	movl	-120(%rsp), %r8d        # 4-byte Reload
	jg	.LBB4_118
# BB#117:                               #   in Loop: Header=BB4_110 Depth=3
	movq	input(%rip), %rcx
	movslq	4180(%rcx), %rcx
	movslq	%r8d, %rbp
	shlq	$4, %rcx
	movzbl	COEFF_COST(%rcx,%rbp), %ecx
.LBB4_118:                              #   in Loop: Header=BB4_110 Depth=3
	movl	-96(%rsp), %r12d        # 4-byte Reload
	addl	%ecx, %r12d
	movl	%r9d, %ecx
	negl	%ecx
	cmovll	%r9d, %ecx
	movl	%ecx, %ebp
	negl	%ebp
	cmpl	$0, (%rdx)
	movl	%ecx, %edi
	cmovsl	%ebp, %edi
	movslq	%r11d, %r11
	movq	-16(%rsp), %rbx         # 8-byte Reload
	movl	%edi, (%rbx,%r11,4)
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	%r8d, (%rdi,%r11,4)
	incl	%r11d
	cmpl	$0, (%rdx)
	cmovnsl	%ecx, %ebp
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi,%rcx,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	imull	(%rcx,%r15,4), %ebp
	movq	-80(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %ebp
	addl	$8, %ebp
	sarl	$4, %ebp
	movl	$-1, -120(%rsp)         # 4-byte Folded Spill
	movl	$2, -20(%rsp)           # 4-byte Folded Spill
	jmp	.LBB4_120
	.p2align	4, 0x90
.LBB4_119:                              #   in Loop: Header=BB4_110 Depth=3
	xorl	%ebp, %ebp
	movq	%r12, %r14
	movl	-96(%rsp), %r12d        # 4-byte Reload
.LBB4_120:                              #   in Loop: Header=BB4_110 Depth=3
	movl	%ebp, (%rdx)
	incq	%rax
	cmpq	$15, %rax
	movq	-56(%rsp), %r8          # 8-byte Reload
	jne	.LBB4_110
.LBB4_121:                              # %.loopexit842
                                        #   in Loop: Header=BB4_123 Depth=2
	movslq	%r11d, %rax
	movq	-16(%rsp), %rcx         # 8-byte Reload
	movl	$0, (%rcx,%rax,4)
	movq	120(%rsp), %rsi         # 8-byte Reload
	incq	%rsi
	cmpq	$4, %rsi
	je	.LBB4_130
# BB#122:                               # %.loopexit842._crit_edge
                                        #   in Loop: Header=BB4_123 Depth=2
	movq	img(%rip), %r14
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
.LBB4_123:                              #   Parent Loop BB4_108 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_110 Depth 3
                                        #       Child Loop BB4_125 Depth 3
	movq	112(%rsp), %rax         # 8-byte Reload
	movb	cbp_blk_chroma(%rsi,%rax,4), %cl
	movl	$1, %eax
	shlq	%cl, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%r13, %rax
	shlq	$4, %rax
	leaq	(%rax,%rdx,4), %rax
	movzbl	hor_offset(%rsi,%rax), %r13d
	movzbl	ver_offset(%rsi,%rax), %eax
	movq	%rax, -112(%rsp)        # 8-byte Spill
	movq	14160(%r14), %rax
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%rsi,8), %rax
	movq	(%rax), %rcx
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	cmpb	$0, -72(%rsp)           # 1-byte Folded Reload
	movq	%rsi, 120(%rsp)         # 8-byte Spill
	je	.LBB4_109
# BB#124:                               # %.preheader841.preheader
                                        #   in Loop: Header=BB4_123 Depth=2
	movl	$-1, %r9d
	xorl	%eax, %eax
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB4_125:                              # %.preheader841
                                        #   Parent Loop BB4_108 Depth=1
                                        #     Parent Loop BB4_123 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	2(%r8,%rax,2), %edx
	movzbl	3(%r8,%rax,2), %ebx
	addq	-112(%rsp), %rbx        # 8-byte Folded Reload
	addq	%r13, %rdx
	movq	%rbx, %rbp
	shlq	$6, %rbp
	addq	%r14, %rbp
	movl	13136(%rbp,%rdx,4), %edi
	movl	%edi, %esi
	negl	%esi
	cmovll	%edi, %esi
	cmpl	$0, 15260(%r14)
	je	.LBB4_127
# BB#126:                               #   in Loop: Header=BB4_125 Depth=3
	movq	14192(%r14), %rdi
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	(%rdi,%rcx,8), %rdi
	movq	(%rdi,%r10,8), %rdi
	movq	(%rdi,%rbx,8), %rdi
	movl	$0, (%rdi,%rdx,4)
.LBB4_127:                              #   in Loop: Header=BB4_125 Depth=3
	incl	%r9d
	testl	%esi, %esi
	je	.LBB4_129
# BB#128:                               #   in Loop: Header=BB4_125 Depth=3
	leaq	13136(%rbp,%rdx,4), %rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	80(%rsp), %rdi          # 8-byte Reload
	orq	%rdi, (%rcx)
	addl	$999999, %r12d          # imm = 0xF423F
	movl	%esi, %edi
	negl	%edi
	cmpl	$0, (%rdx)
	cmovnsl	%esi, %edi
	movslq	%r11d, %r11
	movq	-16(%rsp), %rcx         # 8-byte Reload
	movl	%edi, (%rcx,%r11,4)
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	%r9d, (%rcx,%r11,4)
	incl	%r11d
	movl	$-1, %r9d
	movl	$2, -20(%rsp)           # 4-byte Folded Spill
.LBB4_129:                              #   in Loop: Header=BB4_125 Depth=3
	incq	%rax
	cmpq	$15, %rax
	jne	.LBB4_125
	jmp	.LBB4_121
	.p2align	4, 0x90
.LBB4_130:                              #   in Loop: Header=BB4_108 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movl	15528(%r14), %eax
	movl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rdx
	movq	8(%rsp), %r13           # 8-byte Reload
	jl	.LBB4_108
	jmp	.LBB4_132
.LBB4_131:
	movl	$0, -20(%rsp)           # 4-byte Folded Spill
	xorl	%r12d, %r12d
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB4_132:                              # %._crit_edge903
	cmpb	$0, -72(%rsp)           # 1-byte Folded Reload
	je	.LBB4_142
# BB#133:                               # %.preheader
	movq	img(%rip), %r11
	movslq	15548(%r11), %r10
	testq	%r10, %r10
	jle	.LBB4_200
# BB#134:                               # %.lr.ph863
	movq	enc_picture(%rip), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movslq	15544(%r11), %r13
	movl	%r13d, %eax
	andl	$1, %eax
	movq	%rax, -112(%rsp)        # 8-byte Spill
	leaq	13140(%r11), %r14
	leaq	12626(%r11), %r15
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_135:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_140 Depth 2
	testl	%r13d, %r13d
	jle	.LBB4_141
# BB#136:                               # %.lr.ph
                                        #   in Loop: Header=BB4_135 Depth=1
	movl	188(%r11), %eax
	addl	%r12d, %eax
	cmpq	$0, -112(%rsp)          # 8-byte Folded Reload
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	6472(%rcx), %rcx
	movq	-64(%rsp), %rdx         # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movswq	%ax, %rax
	movq	(%rcx,%rax,8), %rdi
	movl	184(%r11), %eax
	jne	.LBB4_138
# BB#137:                               #   in Loop: Header=BB4_135 Depth=1
	xorl	%edx, %edx
	cmpl	$1, %r13d
	jne	.LBB4_139
	jmp	.LBB4_141
	.p2align	4, 0x90
.LBB4_138:                              #   in Loop: Header=BB4_135 Depth=1
	movq	%r12, %rcx
	shlq	$6, %rcx
	movq	%r12, %rdx
	shlq	$5, %rdx
	movzwl	12624(%r11,%rdx), %edx
	addl	13136(%r11,%rcx), %edx
	movswq	%ax, %rcx
	movw	%dx, (%rdi,%rcx,2)
	movl	$1, %edx
	cmpl	$1, %r13d
	je	.LBB4_141
.LBB4_139:                              # %.lr.ph.new
                                        #   in Loop: Header=BB4_135 Depth=1
	leaq	1(%rax,%rdx), %rcx
	leaq	(%r14,%rdx,4), %rax
	leaq	(%r15,%rdx,2), %rbp
	leaq	1(%rdx), %r9
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_140:                              #   Parent Loop BB4_135 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rcx,%rdx), %esi
	movzwl	-2(%rbp,%rdx,2), %r8d
	addl	-4(%rax,%rdx,4), %r8d
	movswq	%si, %rsi
	movw	%r8w, (%rdi,%rsi,2)
	leal	(%rcx,%rdx), %esi
	movzwl	(%rbp,%rdx,2), %ebx
	addl	(%rax,%rdx,4), %ebx
	movswq	%si, %rsi
	movw	%bx, (%rdi,%rsi,2)
	leaq	1(%r9,%rdx), %rsi
	addq	$2, %rdx
	cmpq	%r13, %rsi
	jl	.LBB4_140
.LBB4_141:                              # %._crit_edge
                                        #   in Loop: Header=BB4_135 Depth=1
	incq	%r12
	addq	$64, %r14
	addq	$32, %r15
	cmpq	%r10, %r12
	jl	.LBB4_135
	jmp	.LBB4_200
.LBB4_142:
	cmpl	$3, %r12d
	movl	-124(%rsp), %ecx        # 4-byte Reload
	movl	-20(%rsp), %edx         # 4-byte Reload
	jg	.LBB4_173
# BB#143:
	sarl	%eax
	testl	%eax, %eax
	jle	.LBB4_172
# BB#144:                               # %.preheader839.lr.ph
	movq	dct_chroma.cbpblk_pattern(,%r13,8), %rdx
	leal	1(%r13), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	movl	-84(%rsp), %eax         # 4-byte Reload
	shll	%cl, %eax
	movl	%eax, %ecx
	shlq	%cl, %rdx
	movq	(%rsp), %r9             # 8-byte Reload
	addl	$4, %r9d
	notq	%rdx
	movq	%rdx, -112(%rsp)        # 8-byte Spill
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	-8(%rsp), %rcx          # 8-byte Reload
	leaq	368(%rax,%rcx), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	xorl	%r12d, %r12d
	shlq	$4, %r13
	.p2align	4, 0x90
.LBB4_145:                              # %.preheader839
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_148 Depth 2
                                        #     Child Loop BB4_151 Depth 2
                                        #     Child Loop BB4_154 Depth 2
                                        #     Child Loop BB4_157 Depth 2
                                        #     Child Loop BB4_160 Depth 2
                                        #     Child Loop BB4_163 Depth 2
                                        #     Child Loop BB4_166 Depth 2
                                        #     Child Loop BB4_169 Depth 2
	movq	%r14, %rcx
	leal	(%r9,%r12), %eax
	cmpl	$0, -104(%rsp)          # 4-byte Folded Reload
	movslq	%eax, %r11
	movzbl	hor_offset(%r13,%r12,4), %r14d
	movzbl	ver_offset(%r13,%r12,4), %r15d
	je	.LBB4_158
# BB#146:                               # %.preheader839.split.preheader
                                        #   in Loop: Header=BB4_145 Depth=1
	movq	%rcx, %rbx
	movq	14160(%rbx), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movl	$0, (%rax)
	movl	$2, %ecx
	movq	-56(%rsp), %rdx         # 8-byte Reload
	jmp	.LBB4_148
	.p2align	4, 0x90
.LBB4_147:                              #   in Loop: Header=BB4_148 Depth=2
	movzbl	(%rdx,%rcx,2), %esi
	movzbl	1(%rdx,%rcx,2), %edi
	addq	%r15, %rdi
	addq	%r14, %rsi
	shlq	$6, %rdi
	addq	%rbx, %rdi
	movl	$0, 13136(%rdi,%rsi,4)
	movl	$0, (%rax,%rcx,4)
	addq	$2, %rcx
.LBB4_148:                              #   Parent Loop BB4_145 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-2(%rdx,%rcx,2), %esi
	movzbl	-1(%rdx,%rcx,2), %edi
	addq	%r15, %rdi
	addq	%r14, %rsi
	shlq	$6, %rdi
	addq	%rbx, %rdi
	movl	$0, 13136(%rdi,%rsi,4)
	movl	$0, -4(%rax,%rcx,4)
	cmpq	$16, %rcx
	jne	.LBB4_147
# BB#149:                               # %.preheader839.split.11030
                                        #   in Loop: Header=BB4_145 Depth=1
	movzbl	hor_offset+1(%r13,%r12,4), %edi
	movzbl	ver_offset+1(%r13,%r12,4), %ebp
	movq	14160(%rbx), %rax
	movq	(%rax,%r11,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movl	$0, (%rax)
	movl	$2, %ecx
	movq	%rbx, %r14
	jmp	.LBB4_151
	.p2align	4, 0x90
.LBB4_150:                              #   in Loop: Header=BB4_151 Depth=2
	movzbl	(%rdx,%rcx,2), %esi
	movzbl	1(%rdx,%rcx,2), %ebx
	addq	%rbp, %rbx
	addq	%rdi, %rsi
	shlq	$6, %rbx
	addq	%r14, %rbx
	movl	$0, 13136(%rbx,%rsi,4)
	movl	$0, (%rax,%rcx,4)
	addq	$2, %rcx
.LBB4_151:                              #   Parent Loop BB4_145 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-2(%rdx,%rcx,2), %esi
	movzbl	-1(%rdx,%rcx,2), %ebx
	addq	%rbp, %rbx
	addq	%rdi, %rsi
	shlq	$6, %rbx
	addq	%r14, %rbx
	movl	$0, 13136(%rbx,%rsi,4)
	movl	$0, -4(%rax,%rcx,4)
	cmpq	$16, %rcx
	jne	.LBB4_150
# BB#152:                               # %.preheader839.split.21031
                                        #   in Loop: Header=BB4_145 Depth=1
	movzbl	hor_offset+2(%r13,%r12,4), %edi
	movzbl	ver_offset+2(%r13,%r12,4), %eax
	movq	14160(%r14), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	16(%rcx), %rcx
	movq	(%rcx), %rbp
	movl	$0, (%rbp)
	movl	$2, %ecx
	jmp	.LBB4_154
	.p2align	4, 0x90
.LBB4_153:                              #   in Loop: Header=BB4_154 Depth=2
	movzbl	(%rdx,%rcx,2), %esi
	movzbl	1(%rdx,%rcx,2), %ebx
	addq	%rax, %rbx
	addq	%rdi, %rsi
	shlq	$6, %rbx
	addq	%r14, %rbx
	movl	$0, 13136(%rbx,%rsi,4)
	movl	$0, (%rbp,%rcx,4)
	addq	$2, %rcx
.LBB4_154:                              #   Parent Loop BB4_145 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-2(%rdx,%rcx,2), %esi
	movzbl	-1(%rdx,%rcx,2), %ebx
	addq	%rax, %rbx
	addq	%rdi, %rsi
	shlq	$6, %rbx
	addq	%r14, %rbx
	movl	$0, 13136(%rbx,%rsi,4)
	movl	$0, -4(%rbp,%rcx,4)
	cmpq	$16, %rcx
	jne	.LBB4_153
# BB#155:                               # %.preheader839.split.31032
                                        #   in Loop: Header=BB4_145 Depth=1
	movzbl	hor_offset+3(%r13,%r12,4), %edi
	movzbl	ver_offset+3(%r13,%r12,4), %eax
	movq	14160(%r14), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	24(%rcx), %rcx
	movq	(%rcx), %rsi
	movl	$0, (%rsi)
	movl	$2, %ecx
	jmp	.LBB4_157
	.p2align	4, 0x90
.LBB4_156:                              #   in Loop: Header=BB4_157 Depth=2
	movzbl	(%rdx,%rcx,2), %ebp
	movzbl	1(%rdx,%rcx,2), %ebx
	addq	%rax, %rbx
	addq	%rdi, %rbp
	shlq	$6, %rbx
	addq	%r14, %rbx
	movl	$0, 13136(%rbx,%rbp,4)
	movl	$0, (%rsi,%rcx,4)
	addq	$2, %rcx
.LBB4_157:                              #   Parent Loop BB4_145 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-2(%rdx,%rcx,2), %ebp
	movzbl	-1(%rdx,%rcx,2), %ebx
	addq	%rax, %rbx
	addq	%rdi, %rbp
	shlq	$6, %rbx
	addq	%r14, %rbx
	movl	$0, 13136(%rbx,%rbp,4)
	movl	$0, -4(%rsi,%rcx,4)
	cmpq	$16, %rcx
	jne	.LBB4_156
	jmp	.LBB4_170
	.p2align	4, 0x90
.LBB4_158:                              # %.preheader839.split.us.preheader
                                        #   in Loop: Header=BB4_145 Depth=1
	movq	%rcx, %r8
	movq	14160(%r8), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	(%rcx), %r10
	andq	-112(%rsp), %r10        # 8-byte Folded Reload
	movq	%r10, (%rcx)
	movl	$0, (%rax)
	movl	$2, %ecx
	movq	-56(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB4_160
	.p2align	4, 0x90
.LBB4_159:                              #   in Loop: Header=BB4_160 Depth=2
	movzbl	(%rbp,%rcx,2), %esi
	movzbl	1(%rbp,%rcx,2), %edi
	addq	%r15, %rdi
	addq	%r14, %rsi
	shlq	$6, %rdi
	addq	%r8, %rdi
	movl	$0, 13136(%rdi,%rsi,4)
	movl	$0, (%rax,%rcx,4)
	addq	$2, %rcx
.LBB4_160:                              #   Parent Loop BB4_145 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-2(%rbp,%rcx,2), %edi
	movzbl	-1(%rbp,%rcx,2), %esi
	addq	%r15, %rsi
	addq	%r14, %rdi
	shlq	$6, %rsi
	addq	%r8, %rsi
	movl	$0, 13136(%rsi,%rdi,4)
	movl	$0, -4(%rax,%rcx,4)
	cmpq	$16, %rcx
	jne	.LBB4_159
# BB#161:                               # %.preheader839.split.us.11041
                                        #   in Loop: Header=BB4_145 Depth=1
	movzbl	hor_offset+1(%r13,%r12,4), %edx
	movzbl	ver_offset+1(%r13,%r12,4), %ebx
	movq	14160(%r8), %rax
	movq	(%rax,%r11,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	%r10, (%rcx)
	movl	$0, (%rax)
	movl	$2, %ecx
	movq	%r8, %r14
	jmp	.LBB4_163
	.p2align	4, 0x90
.LBB4_162:                              #   in Loop: Header=BB4_163 Depth=2
	movzbl	(%rbp,%rcx,2), %esi
	movzbl	1(%rbp,%rcx,2), %edi
	addq	%rbx, %rdi
	addq	%rdx, %rsi
	shlq	$6, %rdi
	addq	%r14, %rdi
	movl	$0, 13136(%rdi,%rsi,4)
	movl	$0, (%rax,%rcx,4)
	addq	$2, %rcx
.LBB4_163:                              #   Parent Loop BB4_145 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-2(%rbp,%rcx,2), %esi
	movzbl	-1(%rbp,%rcx,2), %edi
	addq	%rbx, %rdi
	addq	%rdx, %rsi
	shlq	$6, %rdi
	addq	%r14, %rdi
	movl	$0, 13136(%rdi,%rsi,4)
	movl	$0, -4(%rax,%rcx,4)
	cmpq	$16, %rcx
	jne	.LBB4_162
# BB#164:                               # %.preheader839.split.us.21042
                                        #   in Loop: Header=BB4_145 Depth=1
	movzbl	hor_offset+2(%r13,%r12,4), %edx
	movzbl	ver_offset+2(%r13,%r12,4), %ebx
	movq	14160(%r14), %rax
	movq	(%rax,%r11,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	%r10, (%rcx)
	movl	$0, (%rax)
	movl	$2, %ecx
	jmp	.LBB4_166
	.p2align	4, 0x90
.LBB4_165:                              #   in Loop: Header=BB4_166 Depth=2
	movzbl	(%rbp,%rcx,2), %esi
	movzbl	1(%rbp,%rcx,2), %edi
	addq	%rbx, %rdi
	addq	%rdx, %rsi
	shlq	$6, %rdi
	addq	%r14, %rdi
	movl	$0, 13136(%rdi,%rsi,4)
	movl	$0, (%rax,%rcx,4)
	addq	$2, %rcx
.LBB4_166:                              #   Parent Loop BB4_145 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-2(%rbp,%rcx,2), %esi
	movzbl	-1(%rbp,%rcx,2), %edi
	addq	%rbx, %rdi
	addq	%rdx, %rsi
	shlq	$6, %rdi
	addq	%r14, %rdi
	movl	$0, 13136(%rdi,%rsi,4)
	movl	$0, -4(%rax,%rcx,4)
	cmpq	$16, %rcx
	jne	.LBB4_165
# BB#167:                               # %.preheader839.split.us.31043
                                        #   in Loop: Header=BB4_145 Depth=1
	movzbl	hor_offset+3(%r13,%r12,4), %edx
	movzbl	ver_offset+3(%r13,%r12,4), %ebx
	movq	14160(%r14), %rax
	movq	(%rax,%r11,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	%r10, (%rcx)
	movl	$0, (%rax)
	movl	$2, %ecx
	movq	-64(%rsp), %r10         # 8-byte Reload
	jmp	.LBB4_169
	.p2align	4, 0x90
.LBB4_168:                              #   in Loop: Header=BB4_169 Depth=2
	movzbl	(%rbp,%rcx,2), %esi
	movzbl	1(%rbp,%rcx,2), %edi
	addq	%rbx, %rdi
	addq	%rdx, %rsi
	shlq	$6, %rdi
	addq	%r14, %rdi
	movl	$0, 13136(%rdi,%rsi,4)
	movl	$0, (%rax,%rcx,4)
	addq	$2, %rcx
.LBB4_169:                              #   Parent Loop BB4_145 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-2(%rbp,%rcx,2), %esi
	movzbl	-1(%rbp,%rcx,2), %edi
	addq	%rbx, %rdi
	addq	%rdx, %rsi
	shlq	$6, %rdi
	addq	%r14, %rdi
	movl	$0, 13136(%rdi,%rsi,4)
	movl	$0, -4(%rax,%rcx,4)
	cmpq	$16, %rcx
	jne	.LBB4_168
.LBB4_170:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB4_145 Depth=1
	incq	%r12
	movl	15528(%r14), %eax
	sarl	%eax
	cltq
	cmpq	%rax, %r12
	jl	.LBB4_145
# BB#171:
	xorl	%edx, %edx
	movl	-124(%rsp), %ecx        # 4-byte Reload
	jmp	.LBB4_173
.LBB4_172:
	xorl	%edx, %edx
.LBB4_173:                              # %.loopexit840
	cmpl	$2, %edx
	cmovel	%edx, %ecx
	movl	%ecx, -124(%rsp)        # 4-byte Spill
	cmpl	$0, 15548(%r14)
	jle	.LBB4_182
# BB#174:                               # %.preheader838.lr.ph
	movq	img(%rip), %r13
	movl	15544(%r13), %ecx
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_175:                              # %.preheader838
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_177 Depth 2
                                        #       Child Loop BB4_178 Depth 3
	testl	%ecx, %ecx
	jle	.LBB4_181
# BB#176:                               # %.preheader837.lr.ph
                                        #   in Loop: Header=BB4_175 Depth=1
	movq	-96(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	orq	$1, %rcx
	movq	%rcx, -80(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	orq	$2, %rcx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	orq	$3, %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	movq	%rdx, %r11
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_177:                              # %.preheader837
                                        #   Parent Loop BB4_175 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_178 Depth 3
	movq	-96(%rsp), %rcx         # 8-byte Reload
	shlq	$6, %rcx
	leaq	13136(%r13,%rcx), %rcx
	leaq	(,%rax,4), %r9
	movups	(%rcx,%rax,4), %xmm0
	movaps	%xmm0, dct_chroma.m5(%rip)
	movl	dct_chroma.m5(%rip), %edx
	movl	dct_chroma.m5+8(%rip), %esi
	leal	(%rsi,%rdx), %edi
	subl	%esi, %edx
	movl	dct_chroma.m5+4(%rip), %esi
	movl	%esi, %ebx
	sarl	%ebx
	movl	dct_chroma.m5+12(%rip), %ebp
	subl	%ebp, %ebx
	sarl	%ebp
	addl	%esi, %ebp
	leal	(%rdi,%rbp), %esi
	movl	%esi, (%rcx,%rax,4)
	leal	(%rbx,%rdx), %esi
	movq	%r9, %r8
	orq	$4, %r8
	movl	%esi, (%r8,%rcx)
	subl	%ebx, %edx
	movq	%r9, %r10
	orq	$8, %r10
	movl	%edx, (%r10,%rcx)
	subl	%ebp, %edi
	orq	$12, %r9
	movl	%edi, (%r9,%rcx)
	movq	-80(%rsp), %rcx         # 8-byte Reload
	shlq	$6, %rcx
	leaq	13136(%r13,%rcx), %rcx
	movups	(%rcx,%rax,4), %xmm0
	movaps	%xmm0, dct_chroma.m5(%rip)
	movl	dct_chroma.m5(%rip), %edx
	movl	dct_chroma.m5+8(%rip), %edi
	leal	(%rdi,%rdx), %ebp
	subl	%edi, %edx
	movl	dct_chroma.m5+4(%rip), %edi
	movl	%edi, %ebx
	sarl	%ebx
	movl	dct_chroma.m5+12(%rip), %esi
	subl	%esi, %ebx
	sarl	%esi
	addl	%edi, %esi
	leal	(%rbp,%rsi), %edi
	movl	%edi, (%rcx,%rax,4)
	leal	(%rbx,%rdx), %edi
	movl	%edi, (%r8,%rcx)
	subl	%ebx, %edx
	movl	%edx, (%r10,%rcx)
	subl	%esi, %ebp
	movl	%ebp, (%r9,%rcx)
	movq	-48(%rsp), %rcx         # 8-byte Reload
	shlq	$6, %rcx
	leaq	13136(%r13,%rcx), %rcx
	movups	(%rcx,%rax,4), %xmm0
	movaps	%xmm0, dct_chroma.m5(%rip)
	movl	dct_chroma.m5(%rip), %edx
	movl	dct_chroma.m5+8(%rip), %esi
	leal	(%rsi,%rdx), %edi
	subl	%esi, %edx
	movl	dct_chroma.m5+4(%rip), %esi
	movl	%esi, %ebp
	sarl	%ebp
	movl	dct_chroma.m5+12(%rip), %ebx
	subl	%ebx, %ebp
	sarl	%ebx
	addl	%esi, %ebx
	leal	(%rdi,%rbx), %esi
	movl	%esi, (%rcx,%rax,4)
	leal	(%rbp,%rdx), %esi
	movl	%esi, (%r8,%rcx)
	subl	%ebp, %edx
	movl	%edx, (%r10,%rcx)
	subl	%ebx, %edi
	movl	%edi, (%r9,%rcx)
	movq	-32(%rsp), %rcx         # 8-byte Reload
	shlq	$6, %rcx
	leaq	13136(%r13,%rcx), %rcx
	movdqu	(%rcx,%rax,4), %xmm0
	movdqa	%xmm0, dct_chroma.m5(%rip)
	movl	dct_chroma.m5(%rip), %edx
	movl	dct_chroma.m5+8(%rip), %esi
	leal	(%rsi,%rdx), %edi
	subl	%esi, %edx
	movl	dct_chroma.m5+4(%rip), %esi
	movl	%esi, %ebp
	sarl	%ebp
	movl	dct_chroma.m5+12(%rip), %ebx
	subl	%ebx, %ebp
	sarl	%ebx
	addl	%esi, %ebx
	leal	(%rdi,%rbx), %esi
	movq	%rax, -112(%rsp)        # 8-byte Spill
	movl	%esi, (%rcx,%rax,4)
	leal	(%rbp,%rdx), %esi
	movl	%esi, (%r8,%rcx)
	subl	%ebp, %edx
	movl	%edx, (%r10,%rcx)
	subl	%ebx, %edi
	movl	%edi, (%r9,%rcx)
	movl	$4, %edi
	movq	%r11, -120(%rsp)        # 8-byte Spill
	movq	%r11, %rcx
	.p2align	4, 0x90
.LBB4_178:                              # %.preheader836
                                        #   Parent Loop BB4_175 Depth=1
                                        #     Parent Loop BB4_177 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	13136(%r13,%rcx,4), %r11d
	movl	13200(%r13,%rcx,4), %r8d
	movl	13264(%r13,%rcx,4), %r10d
	movl	13328(%r13,%rcx,4), %r9d
	leal	(%r10,%r11), %r12d
	movl	%r11d, %ebx
	subl	%r10d, %ebx
	movl	%r8d, %ebp
	sarl	%ebp
	subl	%r9d, %ebp
	movl	%r9d, %edx
	sarl	%edx
	addl	%r8d, %edx
	movzwl	12624(%r13,%rcx,2), %r14d
	shll	$6, %r14d
	leal	(%rdx,%r12), %r15d
	leal	32(%r14,%r15), %esi
	xorl	%r14d, %r14d
	movl	15524(%r13), %eax
	sarl	$6, %esi
	cmovsl	%r14d, %esi
	cmpl	%eax, %esi
	cmovgl	%eax, %esi
	movl	%esi, 13136(%r13,%rcx,4)
	movzwl	12656(%r13,%rcx,2), %eax
	shll	$6, %eax
	leal	32(%rbx,%rbp), %esi
	addl	%eax, %esi
	movl	15524(%r13), %eax
	sarl	$6, %esi
	cmovsl	%r14d, %esi
	cmpl	%eax, %esi
	cmovgl	%eax, %esi
	leal	32(%rbx), %eax
	movl	%esi, 13200(%r13,%rcx,4)
	movzwl	12688(%r13,%rcx,2), %esi
	shll	$6, %esi
	subl	%ebp, %eax
	addl	%esi, %eax
	movl	15524(%r13), %esi
	sarl	$6, %eax
	cmovsl	%r14d, %eax
	cmpl	%esi, %eax
	cmovgl	%esi, %eax
	movl	%eax, 13264(%r13,%rcx,4)
	movzwl	12720(%r13,%rcx,2), %eax
	shll	$6, %eax
	leal	32(%r10,%r11), %esi
	subl	%edx, %esi
	addl	%eax, %esi
	movl	15524(%r13), %eax
	sarl	$6, %esi
	cmovsl	%r14d, %esi
	cmpl	%eax, %esi
	cmovgl	%eax, %esi
	movl	%esi, 13328(%r13,%rcx,4)
	incq	%rcx
	decq	%rdi
	jne	.LBB4_178
# BB#179:                               #   in Loop: Header=BB4_177 Depth=2
	movl	%r11d, dct_chroma.m5(%rip)
	movl	%r8d, dct_chroma.m5+4(%rip)
	movl	%r10d, dct_chroma.m5+8(%rip)
	movl	%r9d, dct_chroma.m5+12(%rip)
	movq	-112(%rsp), %rax        # 8-byte Reload
	addq	$4, %rax
	movslq	15544(%r13), %rcx
	movq	-120(%rsp), %r11        # 8-byte Reload
	addq	$4, %r11
	cmpq	%rcx, %rax
	jl	.LBB4_177
# BB#180:                               # %._crit_edge881
                                        #   in Loop: Header=BB4_175 Depth=1
	movl	%r12d, dct_chroma.m6.0(%rip)
	movl	%ebx, dct_chroma.m6.1(%rip)
	movl	%ebp, dct_chroma.m6.2(%rip)
	movl	%edx, dct_chroma.m6.3(%rip)
	movq	-64(%rsp), %r10         # 8-byte Reload
	movq	-16(%rsp), %rdx         # 8-byte Reload
.LBB4_181:                              #   in Loop: Header=BB4_175 Depth=1
	movq	-96(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rdi
	addq	$4, %rdi
	movslq	15548(%r13), %rax
	addq	$64, %rdx
	movq	%rdi, %rsi
	movq	%rsi, -96(%rsp)         # 8-byte Spill
	cmpq	%rax, %rdi
	jl	.LBB4_175
.LBB4_182:                              # %.preheader834
	movq	img(%rip), %rdi
	movslq	15548(%rdi), %r13
	testq	%r13, %r13
	jle	.LBB4_200
# BB#183:                               # %.lr.ph869
	movslq	15544(%rdi), %rdx
	testq	%rdx, %rdx
	jle	.LBB4_200
# BB#184:                               # %.lr.ph869.split.us.preheader
	movq	enc_picture(%rip), %rax
	movl	184(%rdi), %esi
	movl	188(%rdi), %r11d
	movq	6472(%rax), %rax
	movq	(%rax,%r10,8), %r12
	leaq	-1(%rdx), %rax
	leaq	-8(%rdx), %rbp
	shrq	$3, %rbp
	movq	%rdx, %r8
	andq	$-8, %r8
	leal	(%rax,%rsi), %ecx
	cmpw	%si, %cx
	setl	%cl
	cmpq	$65535, %rax            # imm = 0xFFFF
	seta	%r10b
	orb	%cl, %r10b
	movq	%rbp, -120(%rsp)        # 8-byte Spill
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	andl	$1, %ebp
	movq	%rbp, -96(%rsp)         # 8-byte Spill
	movswq	%si, %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	leaq	13184(%rdi), %r9
	movq	%rdi, -80(%rsp)         # 8-byte Spill
	leaq	13136(%rdi), %r14
	xorl	%r15d, %r15d
	movb	%r10b, -112(%rsp)       # 1-byte Spill
	jmp	.LBB4_185
.LBB4_191:                              # %vector.body.preheader
                                        #   in Loop: Header=BB4_185 Depth=1
	movq	%r11, %rbp
	cmpq	$0, -96(%rsp)           # 8-byte Folded Reload
	jne	.LBB4_193
# BB#192:                               # %vector.body.prol
                                        #   in Loop: Header=BB4_185 Depth=1
	movq	%r15, %rax
	shlq	$6, %rax
	movq	-80(%rsp), %rdi         # 8-byte Reload
	movdqu	13136(%rdi,%rax), %xmm0
	movdqu	13152(%rdi,%rax), %xmm1
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	movq	-48(%rsp), %rax         # 8-byte Reload
	movq	%xmm0, (%rcx,%rax,2)
	pshuflw	$232, %xmm1, %xmm0      # xmm0 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	movq	%xmm0, 8(%rcx,%rax,2)
	movl	$8, %r11d
	cmpq	$0, -120(%rsp)          # 8-byte Folded Reload
	jne	.LBB4_194
	jmp	.LBB4_196
.LBB4_193:                              #   in Loop: Header=BB4_185 Depth=1
	xorl	%r11d, %r11d
	cmpq	$0, -120(%rsp)          # 8-byte Folded Reload
	je	.LBB4_196
.LBB4_194:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB4_185 Depth=1
	leaq	(%r9,%r11,4), %r10
	.p2align	4, 0x90
.LBB4_195:                              # %vector.body
                                        #   Parent Loop BB4_185 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rsi,%r11), %rax
	movdqu	-48(%r10), %xmm0
	movdqu	-32(%r10), %xmm1
	movswq	%ax, %rbx
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	movq	%xmm0, (%rcx,%rbx,2)
	pshuflw	$232, %xmm1, %xmm0      # xmm0 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	movq	%xmm0, 8(%rcx,%rbx,2)
	addl	$8, %eax
	movdqu	-16(%r10), %xmm0
	movdqu	(%r10), %xmm1
	movswq	%ax, %rax
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	movq	%xmm0, (%rcx,%rax,2)
	pshuflw	$232, %xmm1, %xmm0      # xmm0 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	movq	%xmm0, 8(%rcx,%rax,2)
	addq	$16, %r11
	addq	$64, %r10
	cmpq	%r11, %r8
	jne	.LBB4_195
.LBB4_196:                              # %middle.block
                                        #   in Loop: Header=BB4_185 Depth=1
	cmpq	%r8, %rdx
	movq	%r8, %rbx
	movq	%rbp, %r11
	movb	-112(%rsp), %r10b       # 1-byte Reload
	je	.LBB4_199
	jmp	.LBB4_197
	.p2align	4, 0x90
.LBB4_185:                              # %.lr.ph869.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_195 Depth 2
                                        #     Child Loop BB4_198 Depth 2
	leal	(%r11,%r15), %eax
	cmpl	$8, %edx
	movswq	%ax, %rax
	movq	(%r12,%rax,8), %rcx
	jae	.LBB4_187
# BB#186:                               #   in Loop: Header=BB4_185 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB4_197
	.p2align	4, 0x90
.LBB4_187:                              # %min.iters.checked
                                        #   in Loop: Header=BB4_185 Depth=1
	testq	%r8, %r8
	je	.LBB4_190
# BB#188:                               # %vector.scevcheck
                                        #   in Loop: Header=BB4_185 Depth=1
	testb	%r10b, %r10b
	je	.LBB4_191
.LBB4_190:                              #   in Loop: Header=BB4_185 Depth=1
	xorl	%ebx, %ebx
.LBB4_197:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB4_185 Depth=1
	leaq	(%r14,%rbx,4), %rax
	.p2align	4, 0x90
.LBB4_198:                              # %scalar.ph
                                        #   Parent Loop BB4_185 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rsi,%rbx), %edi
	movzwl	(%rax), %ebp
	movswq	%di, %rdi
	movw	%bp, (%rcx,%rdi,2)
	incq	%rbx
	addq	$4, %rax
	cmpq	%rdx, %rbx
	jl	.LBB4_198
.LBB4_199:                              # %._crit_edge867.us
                                        #   in Loop: Header=BB4_185 Depth=1
	incq	%r15
	addq	$64, %r9
	addq	$64, %r14
	cmpq	%r13, %r15
	jl	.LBB4_185
.LBB4_200:
	movl	-124(%rsp), %eax        # 4-byte Reload
	addq	$128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	dct_chroma, .Lfunc_end4-dct_chroma
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4613937818241073152     # double 3
.LCPI5_1:
	.quad	4605831338911806259     # double 0.84999999999999998
.LCPI5_2:
	.quad	4616189618054758400     # double 4
	.text
	.globl	dct_luma_sp
	.p2align	4, 0x90
	.type	dct_luma_sp,@function
dct_luma_sp:                            # @dct_luma_sp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi70:
	.cfi_def_cfa_offset 368
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%rdx, 248(%rsp)         # 8-byte Spill
	movl	%esi, %r14d
	movl	%edi, %r13d
	movl	%r13d, %eax
	shrl	$2, %eax
	movl	%r14d, %ecx
	sarl	$3, %ecx
	movl	%r13d, %edx
	sarl	$3, %edx
	leal	(%rdx,%rcx,2), %ecx
	movl	%r14d, %edx
	shrl	%edx
	andl	$2, %edx
	andl	$1, %eax
	orl	%edx, %eax
	movq	img(%rip), %rbx
	movslq	%ecx, %rcx
	movq	14160(%rbx), %rdx
	movq	14224(%rbx), %r15
	movq	(%rdx,%rcx,8), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movslq	12(%rbx), %rax
	imulq	$536, %rax, %r12        # imm = 0x218
	movslq	8(%r15,%r12), %rbp
	leal	-12(%rbp), %eax
	cvtsi2sdl	%eax, %xmm0
	divsd	.LCPI5_0(%rip), %xmm0
	callq	exp2
	mulsd	.LCPI5_1(%rip), %xmm0
	movsd	%xmm0, 120(%rsp)        # 8-byte Spill
	movq	qp_per_matrix(%rip), %rax
	movl	(%rax,%rbp,4), %edx
	movq	qp_rem_matrix(%rip), %rdi
	movslq	(%rdi,%rbp,4), %rsi
	movq	%rsi, 304(%rsp)         # 8-byte Spill
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	leal	15(%rdx), %ecx
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movslq	20(%r15,%r12), %rdx
	movl	(%rax,%rdx,4), %eax
	movslq	(%rdi,%rdx,4), %rdx
	movq	%rdx, 288(%rsp)         # 8-byte Spill
	movq	%rax, 296(%rsp)         # 8-byte Spill
	leal	15(%rax), %esi
	movl	$1, %eax
	movl	$1, %edx
	movl	%ecx, 184(%rsp)         # 4-byte Spill
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	movl	%esi, 20(%rsp)          # 4-byte Spill
	movl	%esi, %ecx
	shll	%cl, %eax
	movq	%r13, %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	movslq	%r13d, %rsi
	movq	%r14, %rcx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movslq	%r14d, %rdi
	leaq	13148(%rbx), %rcx
	shlq	$5, %rdi
	leaq	(%rdi,%rsi,2), %rsi
	leaq	12630(%rbx,%rsi), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_1:                                # %.preheader422
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-6(%rsi,%rdi,8), %ebp
	addl	%ebp, -12(%rcx)
	movzwl	-6(%rsi,%rdi,8), %ebp
	movl	%ebp, 48(%rsp,%rdi)
	movzwl	-4(%rsi,%rdi,8), %ebp
	addl	%ebp, -8(%rcx)
	movzwl	-4(%rsi,%rdi,8), %ebp
	movl	%ebp, 64(%rsp,%rdi)
	movzwl	-2(%rsi,%rdi,8), %ebp
	addl	%ebp, -4(%rcx)
	movzwl	-2(%rsi,%rdi,8), %ebp
	movl	%ebp, 80(%rsp,%rdi)
	movzwl	(%rsi,%rdi,8), %ebp
	addl	%ebp, (%rcx)
	movzwl	(%rsi,%rdi,8), %ebp
	movl	%ebp, 96(%rsp,%rdi)
	addq	$64, %rcx
	addq	$4, %rdi
	cmpq	$16, %rdi
	jne	.LBB5_1
# BB#2:                                 # %.preheader420
	movslq	%edx, %rcx
	imulq	$715827883, %rcx, %rdx  # imm = 0x2AAAAAAB
	movq	%rdx, %rcx
	shrq	$63, %rcx
	shrq	$32, %rdx
	addl	%ecx, %edx
	movq	%rdx, 280(%rsp)         # 8-byte Spill
	movl	%eax, %r15d
	shrl	$31, %r15d
	addl	%eax, %r15d
	movl	13148(%rbx), %eax
	movl	13136(%rbx), %ecx
	movl	13140(%rbx), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	13144(%rbx), %eax
	leal	(%rax,%rdx), %edi
	subl	%eax, %edx
	leal	(%rdi,%rsi), %eax
	movl	%eax, 13136(%rbx)
	subl	%edi, %esi
	movl	%esi, 13144(%rbx)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, 13140(%rbx)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, 13148(%rbx)
	movl	13212(%rbx), %eax
	movl	13200(%rbx), %ecx
	movl	13204(%rbx), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	13208(%rbx), %eax
	leal	(%rax,%rdx), %edi
	subl	%eax, %edx
	leal	(%rdi,%rsi), %eax
	movl	%eax, 13200(%rbx)
	subl	%edi, %esi
	movl	%esi, 13208(%rbx)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, 13204(%rbx)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, 13212(%rbx)
	movl	13276(%rbx), %eax
	movl	13264(%rbx), %ecx
	movl	13268(%rbx), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	13272(%rbx), %eax
	leal	(%rax,%rdx), %edi
	subl	%eax, %edx
	leal	(%rdi,%rsi), %eax
	movl	%eax, 13264(%rbx)
	subl	%edi, %esi
	movl	%esi, 13272(%rbx)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, 13268(%rbx)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, 13276(%rbx)
	movl	13340(%rbx), %edx
	movl	13328(%rbx), %ecx
	movl	13332(%rbx), %eax
	leal	(%rdx,%rcx), %esi
	subl	%edx, %ecx
	movl	13336(%rbx), %edx
	leal	(%rdx,%rax), %edi
	subl	%edx, %eax
	leal	(%rdi,%rsi), %edx
	movl	%edx, 13328(%rbx)
	movl	%esi, %edx
	subl	%edi, %edx
	movl	%edx, 13336(%rbx)
	leal	(%rax,%rcx,2), %edx
	movl	%edx, 13332(%rbx)
	leal	(%rax,%rax), %edx
	movl	%ecx, %ebp
	subl	%edx, %ebp
	movl	%ebp, 13340(%rbx)
	movl	%esi, (%rsp)
	movl	%ecx, 12(%rsp)
	movl	%edi, 4(%rsp)
	movl	%eax, 8(%rsp)
	movdqu	13136(%rbx), %xmm2
	movdqu	13200(%rbx), %xmm3
	movdqu	13264(%rbx), %xmm0
	movdqu	13328(%rbx), %xmm1
	movd	%xmm2, %eax
	movd	%xmm1, %ecx
	addl	%eax, %ecx
	movd	%xmm3, %eax
	movd	%xmm0, %edx
	addl	%eax, %edx
	leal	(%rdx,%rcx), %eax
	movl	%eax, 13136(%rbx)
	subl	%edx, %ecx
	movl	%ecx, 13264(%rbx)
	pshufd	$229, %xmm2, %xmm4      # xmm4 = xmm2[1,1,2,3]
	movd	%xmm4, %ecx
	pshufd	$229, %xmm1, %xmm4      # xmm4 = xmm1[1,1,2,3]
	movd	%xmm4, %eax
	pshufd	$229, %xmm3, %xmm4      # xmm4 = xmm3[1,1,2,3]
	pshufd	$78, %xmm3, %xmm5       # xmm5 = xmm3[2,3,0,1]
	pshufd	$231, %xmm3, %xmm6      # xmm6 = xmm3[3,1,2,3]
	psubd	%xmm0, %xmm3
	movd	%xmm4, %edx
	pshufd	$229, %xmm0, %xmm4      # xmm4 = xmm0[1,1,2,3]
	movd	%xmm4, %esi
	pshufd	$78, %xmm2, %xmm4       # xmm4 = xmm2[2,3,0,1]
	movd	%xmm4, %edi
	pshufd	$78, %xmm1, %xmm4       # xmm4 = xmm1[2,3,0,1]
	movd	%xmm4, %ebp
	pshufd	$231, %xmm2, %xmm4      # xmm4 = xmm2[3,1,2,3]
	psubd	%xmm1, %xmm2
	addl	%ecx, %eax
	addl	%edx, %esi
	leal	(%rsi,%rax), %ecx
	movl	%ecx, 13140(%rbx)
	subl	%esi, %eax
	movl	%eax, 13268(%rbx)
	addl	%edi, %ebp
	movd	%xmm5, %eax
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	movd	%xmm5, %ecx
	addl	%eax, %ecx
	leal	(%rcx,%rbp), %eax
	movl	%eax, 13144(%rbx)
	subl	%ecx, %ebp
	movl	%ebp, 13272(%rbx)
	movd	%xmm4, %eax
	pshufd	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3]
	movd	%xmm1, %ecx
	addl	%eax, %ecx
	movd	%xmm6, %eax
	pshufd	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3]
	movd	%xmm0, %edx
	addl	%eax, %edx
	leal	(%rdx,%rcx), %eax
	movl	%eax, 13148(%rbx)
	subl	%edx, %ecx
	movl	%ecx, 13276(%rbx)
	movdqa	%xmm2, %xmm0
	paddd	%xmm0, %xmm0
	paddd	%xmm3, %xmm0
	movdqu	%xmm0, 13200(%rbx)
	paddd	%xmm3, %xmm3
	psubd	%xmm3, %xmm2
	movdqu	%xmm2, 13328(%rbx)
	movl	96(%rsp), %eax
	movl	48(%rsp), %ecx
	movl	64(%rsp), %esi
	leal	(%rax,%rcx), %edi
	subl	%eax, %ecx
	movl	80(%rsp), %eax
	leal	(%rax,%rsi), %ebp
	subl	%eax, %esi
	leal	(%rbp,%rdi), %edx
	subl	%ebp, %edi
	movl	%edi, 80(%rsp)
	leal	(%rsi,%rcx,2), %eax
	movl	%eax, 64(%rsp)
	addl	%esi, %esi
	subl	%esi, %ecx
	movl	%ecx, 96(%rsp)
	movl	100(%rsp), %eax
	movl	52(%rsp), %ecx
	movl	68(%rsp), %esi
	leal	(%rax,%rcx), %edi
	subl	%eax, %ecx
	movl	84(%rsp), %eax
	leal	(%rax,%rsi), %ebx
	subl	%eax, %esi
	leal	(%rbx,%rdi), %ebp
	subl	%ebx, %edi
	movl	%edi, 84(%rsp)
	leal	(%rsi,%rcx,2), %edi
	addl	%esi, %esi
	subl	%esi, %ecx
	movl	%ecx, 100(%rsp)
	movl	104(%rsp), %esi
	movl	56(%rsp), %r11d
	movl	60(%rsp), %r14d
	leal	(%rsi,%r11), %r10d
	subl	%esi, %r11d
	movl	72(%rsp), %esi
	movl	88(%rsp), %ebx
	leal	(%rbx,%rsi), %eax
	subl	%ebx, %esi
	leal	(%rax,%r10), %r9d
	subl	%eax, %r10d
	leal	(%rsi,%r11,2), %r8d
	addl	%esi, %esi
	subl	%esi, %r11d
	movl	108(%rsp), %eax
	leal	(%rax,%r14), %ebx
	subl	%eax, %r14d
	movl	76(%rsp), %eax
	movl	92(%rsp), %esi
	leal	(%rsi,%rax), %ecx
	subl	%esi, %eax
	leal	(%rcx,%rbx), %esi
	subl	%ecx, %ebx
	leal	(%rax,%r14,2), %r13d
	addl	%eax, %eax
	subl	%eax, %r14d
	leal	(%rsi,%rdx), %eax
	subl	%esi, %edx
	leal	(%r9,%rbp), %esi
	subl	%r9d, %ebp
	leal	(%rsi,%rax), %ecx
	movl	%ecx, 48(%rsp)
	subl	%esi, %eax
	movl	%eax, 56(%rsp)
	leal	(%rbp,%rdx,2), %eax
	movl	%eax, 52(%rsp)
	addl	%ebp, %ebp
	subl	%ebp, %edx
	movl	%edx, 60(%rsp)
	movl	64(%rsp), %eax
	leal	(%r13,%rax), %ecx
	subl	%r13d, %eax
	leal	(%r8,%rdi), %edx
	subl	%r8d, %edi
	leal	(%rdx,%rcx), %esi
	movl	%esi, 64(%rsp)
	subl	%edx, %ecx
	movsd	120(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI5_2(%rip), %xmm0
	movsd	%xmm0, 120(%rsp)        # 8-byte Spill
	sarl	%r15d
	movl	%ecx, 72(%rsp)
	leal	(%rdi,%rax,2), %ecx
	movl	%ecx, 68(%rsp)
	addl	%edi, %edi
	subl	%edi, %eax
	movl	%eax, 76(%rsp)
	movl	80(%rsp), %eax
	movl	84(%rsp), %ecx
	leal	(%rbx,%rax), %edx
	subl	%ebx, %eax
	leal	(%r10,%rcx), %esi
	subl	%r10d, %ecx
	leal	(%rsi,%rdx), %edi
	movl	%edi, 80(%rsp)
	subl	%esi, %edx
	movl	%edx, 88(%rsp)
	leal	(%rcx,%rax,2), %edx
	movl	%edx, 84(%rsp)
	addl	%ecx, %ecx
	subl	%ecx, %eax
	movl	%eax, 92(%rsp)
	movl	96(%rsp), %edx
	leal	(%r14,%rdx), %eax
	movl	%eax, (%rsp)
	subl	%r14d, %edx
	movl	%edx, 12(%rsp)
	movl	100(%rsp), %ecx
	leal	(%r11,%rcx), %esi
	movl	%esi, 4(%rsp)
	subl	%r11d, %ecx
	movl	%ecx, 8(%rsp)
	leal	(%rsi,%rax), %edi
	movl	%edi, 96(%rsp)
	subl	%esi, %eax
	movl	%eax, 104(%rsp)
	leal	(%rcx,%rdx,2), %eax
	movl	%eax, 100(%rsp)
	addl	%ecx, %ecx
	subl	%ecx, %edx
	movl	%edx, 108(%rsp)
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	428(%rax,%r12), %r12
	movl	$-1, %esi
	xorl	%r11d, %r11d
	movq	img(%rip), %r10
	xorl	%eax, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	$0, 112(%rsp)           # 4-byte Folded Spill
	movl	20(%rsp), %r13d         # 4-byte Reload
	movl	%r15d, 36(%rsp)         # 4-byte Spill
	movq	%r12, 232(%rsp)         # 8-byte Spill
	jmp	.LBB5_5
.LBB5_3:                                #   in Loop: Header=BB5_5 Depth=1
	xorl	%eax, %eax
	movq	136(%rsp), %rbx         # 8-byte Reload
	jmp	.LBB5_20
.LBB5_4:                                #   in Loop: Header=BB5_5 Depth=1
	xorl	%eax, %eax
	movq	144(%rsp), %r9          # 8-byte Reload
	movq	136(%rsp), %rbx         # 8-byte Reload
	movq	128(%rsp), %r8          # 8-byte Reload
	movl	40(%rsp), %ebp          # 4-byte Reload
	movq	%r13, %r12
	movl	36(%rsp), %r15d         # 4-byte Reload
	movl	20(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB5_21
	.p2align	4, 0x90
.LBB5_5:                                # =>This Inner Loop Header: Depth=1
	movl	%r13d, %edx
	movq	%r12, %r13
	cmpl	$0, (%r12)
	movl	$FIELD_SCAN, %eax
	movl	$SNGL_SCAN, %ecx
	cmoveq	%rcx, %rax
	movzbl	(%rax,%r11,2), %edi
	movzbl	1(%rax,%r11,2), %r8d
	incl	%esi
	movl	%esi, 24(%rsp)          # 4-byte Spill
	movq	%rdi, %rbx
	shlq	$4, %rbx
	leaq	48(%rsp,%rbx), %r9
	movl	(%r9,%r8,4), %esi
	movl	%esi, %eax
	negl	%eax
	cmovll	%esi, %eax
	movq	288(%rsp), %rcx         # 8-byte Reload
	shlq	$6, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movl	quant_coef(%rcx,%r8,4), %ebp
	imull	%ebp, %eax
	addl	%r15d, %eax
	movl	%edx, %ecx
	sarl	%cl, %eax
	shll	%cl, %eax
	cltd
	movl	%ebp, 40(%rsp)          # 4-byte Spill
	idivl	%ebp
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movq	%r8, %rax
	shlq	$6, %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	addq	%rax, %r10
	movl	%ecx, %eax
	negl	%eax
	testl	%esi, %esi
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	movl	13136(%r10,%rdi,4), %r14d
	movq	%rbx, %r10
	cmovnsl	%ecx, %eax
	movl	%r14d, %ebp
	subl	%eax, %ebp
	movl	%ebp, %r12d
	negl	%r12d
	cmovll	%ebp, %r12d
	movq	304(%rsp), %rdi         # 8-byte Reload
	shlq	$6, %rdi
	addq	%r10, %rdi
	movl	quant_coef(%rdi,%r8,4), %eax
	imull	%eax, %r12d
	movq	280(%rsp), %rdx         # 8-byte Reload
	addl	%edx, %r12d
	movl	184(%rsp), %ecx         # 4-byte Reload
	sarl	%cl, %r12d
	subl	%esi, %r14d
	movl	%r14d, %r15d
	negl	%r15d
	cmovll	%r14d, %r15d
	imull	%eax, %r15d
	movq	%r8, 136(%rsp)          # 8-byte Spill
	leaq	(%r9,%r8,4), %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	addl	%edx, %r15d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r15d
	testl	%r15d, %r15d
	je	.LBB5_11
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=1
	cmpl	%r15d, %r12d
	je	.LBB5_11
# BB#7:                                 #   in Loop: Header=BB5_5 Depth=1
	testl	%r12d, %r12d
	je	.LBB5_11
# BB#8:                                 # %.thread403
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	%r11, 112(%rsp)         # 8-byte Spill
	movl	%r12d, %ebx
	negl	%ebx
	cmovll	%r12d, %ebx
	movl	%ebx, %eax
	negl	%eax
	movl	%ebp, 180(%rsp)         # 4-byte Spill
	testl	%ebp, %ebp
	cmovnsl	%ebx, %eax
	movq	136(%rsp), %rcx         # 8-byte Reload
	movq	%r10, 272(%rsp)         # 8-byte Spill
	movl	A(%r10,%rcx,4), %edx
	movq	%rdi, 264(%rsp)         # 8-byte Spill
	imull	dequant_coef(%rdi,%rcx,4), %edx
	movl	%edx, 152(%rsp)         # 4-byte Spill
	imull	%edx, %eax
	movq	168(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	sarl	$6, %eax
	movl	%r14d, %ebp
	subl	%eax, %ebp
	movl	%r12d, %edi
	movl	24(%rsp), %esi          # 4-byte Reload
	leaq	44(%rsp), %rax
	movq	%rax, %rdx
	leaq	188(%rsp), %rax
	movq	%rax, %rcx
	callq	levrun_linfo_inter
	movq	img(%rip), %rax
	addq	224(%rsp), %rax         # 8-byte Folded Reload
	movl	%r15d, %r13d
	negl	%r13d
	cmovll	%r15d, %r13d
	movl	%r13d, %edx
	negl	%edx
	testl	%r14d, %r14d
	cvtsi2sdl	%ebp, %xmm0
	mulsd	%xmm0, %xmm0
	cvtsi2sdl	44(%rsp), %xmm1
	mulsd	120(%rsp), %xmm1        # 8-byte Folded Reload
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 256(%rsp)        # 8-byte Spill
	movq	144(%rsp), %rcx         # 8-byte Reload
	movl	13136(%rax,%rcx,4), %eax
	cmovnsl	%r13d, %edx
	imull	152(%rsp), %edx         # 4-byte Folded Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edx
	sarl	$6, %edx
	movq	216(%rsp), %rcx         # 8-byte Reload
	subl	(%rcx), %eax
	subl	%edx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 152(%rsp)        # 8-byte Spill
	movl	%r15d, %edi
	movl	24(%rsp), %esi          # 4-byte Reload
	leaq	44(%rsp), %rdx
	leaq	188(%rsp), %rcx
	callq	levrun_linfo_inter
	movsd	152(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	44(%rsp), %xmm0
	mulsd	120(%rsp), %xmm0        # 8-byte Folded Reload
	addsd	%xmm1, %xmm0
	movq	256(%rsp), %xmm1        # 8-byte Folded Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	seta	%al
	cmpl	%r13d, %ebx
	setl	%cl
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_13
# BB#9:                                 # %.thread403
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	128(%rsp), %r8          # 8-byte Reload
	jp	.LBB5_14
# BB#10:                                # %.thread403
                                        #   in Loop: Header=BB5_5 Depth=1
	movl	%ecx, %eax
	jmp	.LBB5_14
	.p2align	4, 0x90
.LBB5_11:                               #   in Loop: Header=BB5_5 Depth=1
	cmpl	%r15d, %r12d
	jne	.LBB5_15
# BB#12:                                #   in Loop: Header=BB5_5 Depth=1
	movl	%ebp, %r14d
	movl	%r12d, %r15d
	jmp	.LBB5_16
	.p2align	4, 0x90
.LBB5_13:                               #   in Loop: Header=BB5_5 Depth=1
	movq	128(%rsp), %r8          # 8-byte Reload
.LBB5_14:                               # %.thread403
                                        #   in Loop: Header=BB5_5 Depth=1
	testb	%al, %al
	cmovnel	%r12d, %r15d
	cmpl	%r12d, %r15d
	cmovel	180(%rsp), %r14d        # 4-byte Folded Reload
	movl	20(%rsp), %r13d         # 4-byte Reload
	movq	232(%rsp), %r12         # 8-byte Reload
	movq	112(%rsp), %r11         # 8-byte Reload
	movl	24(%rsp), %ebx          # 4-byte Reload
	movq	144(%rsp), %r9          # 8-byte Reload
	movl	40(%rsp), %ebp          # 4-byte Reload
	movq	272(%rsp), %r10         # 8-byte Reload
	movq	264(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB5_17
	.p2align	4, 0x90
.LBB5_15:                               #   in Loop: Header=BB5_5 Depth=1
	testl	%r12d, %r12d
	je	.LBB5_4
.LBB5_16:                               #   in Loop: Header=BB5_5 Depth=1
	testl	%r15d, %r15d
	movq	144(%rsp), %r9          # 8-byte Reload
	movq	128(%rsp), %r8          # 8-byte Reload
	movl	40(%rsp), %ebp          # 4-byte Reload
	movq	%r13, %r12
	movl	20(%rsp), %r13d         # 4-byte Reload
	movl	24(%rsp), %ebx          # 4-byte Reload
	je	.LBB5_3
.LBB5_17:                               #   in Loop: Header=BB5_5 Depth=1
	movl	$999999, %eax           # imm = 0xF423F
	cmpl	$1, %r15d
	jg	.LBB5_19
# BB#18:                                #   in Loop: Header=BB5_5 Depth=1
	movq	input(%rip), %rax
	movslq	4180(%rax), %rax
	movslq	%ebx, %rcx
	shlq	$4, %rax
	movzbl	COEFF_COST(%rax,%rcx), %eax
.LBB5_19:                               #   in Loop: Header=BB5_5 Depth=1
	movq	248(%rsp), %rcx         # 8-byte Reload
	addl	%eax, (%rcx)
	movl	%r15d, %ecx
	negl	%ecx
	cmovll	%r15d, %ecx
	movl	%ecx, %eax
	negl	%eax
	testl	%r14d, %r14d
	cmovnsl	%ecx, %eax
	movq	160(%rsp), %rdx         # 8-byte Reload
	movslq	%edx, %rdx
	movq	208(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx,%rdx,4)
	movq	240(%rsp), %rcx         # 8-byte Reload
	movl	%ebx, (%rcx,%rdx,4)
	incl	%edx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movq	136(%rsp), %rbx         # 8-byte Reload
	imull	dequant_coef(%rdi,%rbx,4), %eax
	imull	A(%r10,%rbx,4), %eax
	movq	168(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	sarl	$6, %eax
	movq	216(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %esi
	movl	$-1, 24(%rsp)           # 4-byte Folded Spill
	movl	$1, 112(%rsp)           # 4-byte Folded Spill
.LBB5_20:                               # %.thread
                                        #   in Loop: Header=BB5_5 Depth=1
	movl	36(%rsp), %r15d         # 4-byte Reload
.LBB5_21:                               # %.thread
                                        #   in Loop: Header=BB5_5 Depth=1
	addl	%eax, %esi
	movl	sp2_frame_indicator(%rip), %eax
	orl	si_frame_indicator(%rip), %eax
	je	.LBB5_23
# BB#22:                                # %.thread._crit_edge
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	img(%rip), %r10
	movl	%esi, %eax
	negl	%eax
	jmp	.LBB5_24
	.p2align	4, 0x90
.LBB5_23:                               #   in Loop: Header=BB5_5 Depth=1
	movl	%esi, %eax
	negl	%eax
	movl	%esi, %edx
	cmovgel	%eax, %edx
	imull	%ebp, %edx
	addl	%r15d, %edx
	movl	%r13d, %ecx
	sarl	%cl, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%esi, %esi
	cmovnsl	%ecx, %edx
	movq	lrec(%rip), %rcx
	movq	img(%rip), %r10
	movq	200(%rsp), %rdi         # 8-byte Reload
	leal	(%rbx,%rdi), %edi
	addl	180(%r10), %edi
	movslq	%edi, %rdi
	movq	(%rcx,%rdi,8), %rcx
	movq	192(%rsp), %rdi         # 8-byte Reload
	leal	(%r9,%rdi), %edi
	addl	176(%r10), %edi
	movslq	%edi, %rdi
	movl	%edx, (%rcx,%rdi,4)
.LBB5_24:                               #   in Loop: Header=BB5_5 Depth=1
	testl	%esi, %esi
	cmovnsl	%esi, %eax
	imull	%eax, %ebp
	addl	%r15d, %ebp
	movl	%r13d, %ecx
	sarl	%cl, %ebp
	movl	%ebp, %eax
	negl	%eax
	cmovll	%ebp, %eax
	movl	%eax, %edx
	negl	%edx
	testl	%esi, %esi
	cmovnsl	%eax, %edx
	imull	dequant_coef(%r8,%rbx,4), %edx
	movq	296(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edx
	movq	224(%rsp), %rax         # 8-byte Reload
	addq	%r10, %rax
	movl	%edx, 13136(%rax,%r9,4)
	incq	%r11
	cmpq	$16, %r11
	movl	24(%rsp), %esi          # 4-byte Reload
	jne	.LBB5_5
# BB#25:                                # %.preheader412
	movslq	160(%rsp), %rax         # 4-byte Folded Reload
	movq	208(%rsp), %rcx         # 8-byte Reload
	movl	$0, (%rcx,%rax,4)
	movups	13136(%r10), %xmm0
	movaps	%xmm0, (%rsp)
	movl	8(%rsp), %eax
	movl	(%rsp), %ecx
	movl	4(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	12(%rsp), %edi
	subl	%edi, %eax
	sarl	%edi
	addl	%edx, %edi
	leal	(%rdi,%rsi), %edx
	movl	%edx, 13136(%r10)
	subl	%edi, %esi
	movl	%esi, 13148(%r10)
	leal	(%rax,%rcx), %edx
	movl	%edx, 13140(%r10)
	subl	%eax, %ecx
	movl	%ecx, 13144(%r10)
	movups	13200(%r10), %xmm0
	movaps	%xmm0, (%rsp)
	movl	8(%rsp), %eax
	movl	(%rsp), %ecx
	movl	4(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	12(%rsp), %edi
	subl	%edi, %eax
	sarl	%edi
	addl	%edx, %edi
	leal	(%rdi,%rsi), %edx
	movl	%edx, 13200(%r10)
	subl	%edi, %esi
	movl	%esi, 13212(%r10)
	leal	(%rax,%rcx), %edx
	movl	%edx, 13204(%r10)
	subl	%eax, %ecx
	movl	%ecx, 13208(%r10)
	movups	13264(%r10), %xmm0
	movaps	%xmm0, (%rsp)
	movl	8(%rsp), %eax
	movl	(%rsp), %ecx
	movl	4(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	12(%rsp), %edi
	subl	%edi, %eax
	sarl	%edi
	addl	%edx, %edi
	leal	(%rdi,%rsi), %edx
	movl	%edx, 13264(%r10)
	subl	%edi, %esi
	movl	%esi, 13276(%r10)
	leal	(%rax,%rcx), %edx
	movl	%edx, 13268(%r10)
	subl	%eax, %ecx
	movl	%ecx, 13272(%r10)
	movups	13328(%r10), %xmm0
	movaps	%xmm0, (%rsp)
	movl	8(%rsp), %eax
	movl	(%rsp), %ecx
	movl	4(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	12(%rsp), %edi
	subl	%edi, %eax
	sarl	%edi
	addl	%edx, %edi
	leal	(%rdi,%rsi), %edx
	movl	%edx, 13328(%r10)
	subl	%edi, %esi
	movl	%esi, 13340(%r10)
	leal	(%rax,%rcx), %edx
	movl	%edx, 13332(%r10)
	subl	%eax, %ecx
	movl	%ecx, 13336(%r10)
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_26:                               # %.preheader410
                                        # =>This Inner Loop Header: Depth=1
	movl	13136(%r10,%rcx,4), %r12d
	movl	13200(%r10,%rcx,4), %r11d
	movl	13264(%r10,%rcx,4), %r15d
	movl	13328(%r10,%rcx,4), %r14d
	movl	%r12d, %ebp
	subl	%r15d, %ebp
	movl	%r11d, %eax
	sarl	%eax
	subl	%r14d, %eax
	movl	%r14d, %edx
	sarl	%edx
	addl	%r11d, %edx
	movl	15520(%r10), %esi
	leal	32(%r15,%r12), %ebx
	leal	(%rbx,%rdx), %edi
	sarl	$6, %edi
	cmovsl	%r9d, %edi
	cmpl	%esi, %edi
	cmovgl	%esi, %edi
	movl	%edi, 13136(%r10,%rcx,4)
	movl	15520(%r10), %esi
	subl	%edx, %ebx
	sarl	$6, %ebx
	cmovsl	%r9d, %ebx
	cmpl	%esi, %ebx
	cmovgl	%esi, %ebx
	movl	%ebx, 13328(%r10,%rcx,4)
	movl	15520(%r10), %edx
	leal	32(%rbp), %esi
	leal	32(%rbp,%rax), %edi
	sarl	$6, %edi
	cmovsl	%r9d, %edi
	cmpl	%edx, %edi
	cmovgl	%edx, %edi
	movl	%edi, 13200(%r10,%rcx,4)
	movl	15520(%r10), %edx
	subl	%eax, %esi
	sarl	$6, %esi
	cmovsl	%r9d, %esi
	cmpl	%edx, %esi
	cmovgl	%edx, %esi
	movl	%esi, 13264(%r10,%rcx,4)
	incq	%rcx
	cmpq	$4, %rcx
	jne	.LBB5_26
# BB#27:                                # %.preheader409
	movl	%r12d, (%rsp)
	movl	%r11d, 4(%rsp)
	movl	%r15d, 8(%rsp)
	movl	%r14d, 12(%rsp)
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %r9
	movl	176(%r10), %ebp
	movl	180(%r10), %r11d
	movq	200(%rsp), %r14         # 8-byte Reload
	leal	(%r11,%r14), %edx
	movslq	%edx, %rdx
	movq	(%r9,%rdx,8), %rbx
	movzwl	13136(%r10), %esi
	movq	192(%rsp), %rcx         # 8-byte Reload
	leal	(%rbp,%rcx), %edx
	movslq	%edx, %rdx
	movw	%si, (%rbx,%rdx,2)
	movzwl	13140(%r10), %edi
	leal	1(%rbp,%rcx), %esi
	movslq	%esi, %rsi
	movw	%di, (%rbx,%rsi,2)
	movzwl	13144(%r10), %eax
	leal	2(%rbp,%rcx), %edi
	movslq	%edi, %rdi
	movw	%ax, (%rbx,%rdi,2)
	movzwl	13148(%r10), %eax
	leal	3(%rbp,%rcx), %ebp
	movslq	%ebp, %rbp
	movw	%ax, (%rbx,%rbp,2)
	leal	1(%r11,%r14), %eax
	cltq
	movq	(%r9,%rax,8), %rax
	movzwl	13200(%r10), %ebx
	movw	%bx, (%rax,%rdx,2)
	movzwl	13204(%r10), %ebx
	movw	%bx, (%rax,%rsi,2)
	movzwl	13208(%r10), %ebx
	movw	%bx, (%rax,%rdi,2)
	movzwl	13212(%r10), %ebx
	movw	%bx, (%rax,%rbp,2)
	leal	2(%r11,%r14), %eax
	cltq
	movq	(%r9,%rax,8), %rax
	movzwl	13264(%r10), %ebx
	movw	%bx, (%rax,%rdx,2)
	movzwl	13268(%r10), %ebx
	movw	%bx, (%rax,%rsi,2)
	movzwl	13272(%r10), %ebx
	movw	%bx, (%rax,%rdi,2)
	movzwl	13276(%r10), %ebx
	movw	%bx, (%rax,%rbp,2)
	leal	3(%r11,%r14), %eax
	cltq
	movq	(%r9,%rax,8), %rax
	movzwl	13328(%r10), %ecx
	movw	%cx, (%rax,%rdx,2)
	movzwl	13332(%r10), %ecx
	movw	%cx, (%rax,%rsi,2)
	movzwl	13336(%r10), %ecx
	movw	%cx, (%rax,%rdi,2)
	movzwl	13340(%r10), %ecx
	movw	%cx, (%rax,%rbp,2)
	movl	112(%rsp), %eax         # 4-byte Reload
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	dct_luma_sp, .Lfunc_end5-dct_luma_sp
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4613937818241073152     # double 3
.LCPI6_1:
	.quad	4605831338911806259     # double 0.84999999999999998
.LCPI6_2:
	.quad	4616189618054758400     # double 4
	.text
	.globl	dct_chroma_sp
	.p2align	4, 0x90
	.type	dct_chroma_sp,@function
dct_chroma_sp:                          # @dct_chroma_sp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	subq	$632, %rsp              # imm = 0x278
.Lcfi83:
	.cfi_def_cfa_offset 688
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movl	%esi, %r13d
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	img(%rip), %r15
	movq	14168(%r15), %rax
	movq	14224(%r15), %rsi
	movslq	12(%r15), %rcx
	movq	%rdi, %rdx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movslq	%edi, %rdx
	movq	%rdx, 280(%rsp)         # 8-byte Spill
	movq	8(%rax,%rdx,8), %rax
	movq	(%rax), %rdx
	movq	%rdx, 256(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	imulq	$536, %rcx, %rax        # imm = 0x218
	movl	8(%rsi,%rax), %r12d
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movl	20(%rsi,%rax), %ebp
	leal	-12(%r12), %eax
	cvtsi2sdl	%eax, %xmm0
	divsd	.LCPI6_0(%rip), %xmm0
	callq	exp2
	movsd	%xmm0, 88(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	subl	15456(%r15), %edx
	movq	active_pps(%rip), %rax
	movl	208(%rax), %eax
	addl	%eax, %r12d
	cmpl	%edx, %r12d
	cmovll	%edx, %r12d
	cmpl	$52, %r12d
	movl	$51, %esi
	movl	$51, %ecx
	cmovll	%r12d, %ecx
	addl	%ebp, %eax
	cmpl	%edx, %eax
	cmovll	%edx, %eax
	cmpl	$52, %eax
	cmovgel	%esi, %eax
	testl	%ecx, %ecx
	js	.LBB6_1
# BB#2:
	movslq	%ecx, %rcx
	movzbl	QP_SCALE_CR(%rcx), %r12d
	imull	$171, %r12d, %edi
	andl	$64512, %edi            # imm = 0xFC00
	shrl	$10, %edi
	jmp	.LBB6_3
.LBB6_1:                                # %.thread
	movslq	%ecx, %rcx
	imulq	$715827883, %rcx, %rdi  # imm = 0x2AAAAAAB
	movq	%rdi, %rcx
	shrq	$63, %rcx
	shrq	$32, %rdi
	addl	%ecx, %edi
.LBB6_3:
	movsd	88(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI6_1(%rip), %xmm0
	movsd	%xmm0, 88(%rsp)         # 8-byte Spill
	movslq	%r12d, %rcx
	imulq	$715827883, %rcx, %rcx  # imm = 0x2AAAAAAB
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$32, %rcx
	addl	%edx, %ecx
	addl	%ecx, %ecx
	leal	(%rcx,%rcx,2), %edx
	leal	15(%rdi), %ecx
	movl	$1, %esi
	movl	%ecx, 308(%rsp)         # 4-byte Spill
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movslq	%esi, %rcx
	imulq	$715827883, %rcx, %rsi  # imm = 0x2AAAAAAB
	movq	%rsi, %rcx
	shrq	$63, %rcx
	shrq	$32, %rsi
	movq	%rsi, 272(%rsp)         # 8-byte Spill
	testl	%eax, %eax
	js	.LBB6_4
# BB#5:
	cltq
	movzbl	QP_SCALE_CR(%rax), %ebp
	imull	$171, %ebp, %esi
	andl	$64512, %esi            # imm = 0xFC00
	shrl	$10, %esi
	jmp	.LBB6_6
.LBB6_4:
	movslq	%ebp, %rax
	imulq	$715827883, %rax, %rsi  # imm = 0x2AAAAAAB
	movq	%rsi, %rax
	shrq	$63, %rax
	shrq	$32, %rsi
	addl	%eax, %esi
.LBB6_6:
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movsd	88(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI6_2(%rip), %xmm0
	movsd	%xmm0, 88(%rsp)         # 8-byte Spill
	subl	%edx, %r12d
	movq	272(%rsp), %rax         # 8-byte Reload
	addl	%ecx, %eax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movq	%rsi, 296(%rsp)         # 8-byte Spill
	leal	15(%rsi), %ecx
	movl	$1, %edx
	movl	%ecx, 36(%rsp)          # 4-byte Spill
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	leaq	592(%rsp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_7:                                # %.preheader708
                                        # =>This Inner Loop Header: Depth=1
	movzwl	12624(%r15,%rcx), %esi
	addl	%esi, 13136(%r15,%rcx,2)
	movzwl	12624(%r15,%rcx), %esi
	movl	%esi, -224(%rax)
	movzwl	12626(%r15,%rcx), %esi
	addl	%esi, 13140(%r15,%rcx,2)
	movl	%esi, -192(%rax)
	movzwl	12628(%r15,%rcx), %esi
	addl	%esi, 13144(%r15,%rcx,2)
	movl	%esi, -160(%rax)
	movzwl	12630(%r15,%rcx), %esi
	addl	%esi, 13148(%r15,%rcx,2)
	movl	%esi, -128(%rax)
	movzwl	12632(%r15,%rcx), %esi
	addl	%esi, 13152(%r15,%rcx,2)
	movl	%esi, -96(%rax)
	movzwl	12634(%r15,%rcx), %esi
	addl	%esi, 13156(%r15,%rcx,2)
	movl	%esi, -64(%rax)
	movzwl	12636(%r15,%rcx), %esi
	addl	%esi, 13160(%r15,%rcx,2)
	movl	%esi, -32(%rax)
	movzwl	12638(%r15,%rcx), %esi
	addl	%esi, 13164(%r15,%rcx,2)
	movl	%esi, (%rax)
	addq	$32, %rcx
	addq	$4, %rax
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB6_7
# BB#8:                                 # %.preheader707
	movslq	%ebp, %r8
	imulq	$715827883, %r8, %rax   # imm = 0x2AAAAAAB
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %r8d
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	leaq	13340(%r15), %r10
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB6_9:                                # %.preheader706
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_10 Depth 2
	movq	%r10, %rsi
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB6_10:                               # %.preheader704
                                        #   Parent Loop BB6_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-192(%rsi), %eax
	movl	-204(%rsi), %edx
	movl	-200(%rsi), %ebp
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	movl	-196(%rsi), %eax
	leal	(%rax,%rbp), %ecx
	subl	%eax, %ebp
	leal	(%rcx,%rbx), %eax
	movl	%eax, -204(%rsi)
	subl	%ecx, %ebx
	movl	%ebx, -196(%rsi)
	leal	(%rbp,%rdx,2), %eax
	movl	%eax, -200(%rsi)
	addl	%ebp, %ebp
	subl	%ebp, %edx
	movl	%edx, -192(%rsi)
	movl	-128(%rsi), %eax
	movl	-140(%rsi), %ecx
	movl	-136(%rsi), %edx
	leal	(%rax,%rcx), %ebp
	subl	%eax, %ecx
	movl	-132(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rbp), %eax
	movl	%eax, -140(%rsi)
	subl	%ebx, %ebp
	movl	%ebp, -132(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -136(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -128(%rsi)
	movl	-64(%rsi), %eax
	movl	-76(%rsi), %ecx
	movl	-72(%rsi), %edx
	leal	(%rax,%rcx), %ebp
	subl	%eax, %ecx
	movl	-68(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rbp), %eax
	movl	%eax, -76(%rsi)
	subl	%ebx, %ebp
	movl	%ebp, -68(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -72(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -64(%rsi)
	movl	(%rsi), %eax
	movl	-12(%rsi), %ecx
	movl	-8(%rsi), %edx
	leal	(%rax,%rcx), %ebp
	subl	%eax, %ecx
	movl	-4(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rbp), %eax
	movl	%eax, -12(%rsi)
	subl	%ebx, %ebp
	movl	%ebp, -4(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -8(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, (%rsi)
	movl	-12(%rsi), %eax
	movl	-204(%rsi), %ecx
	movl	-140(%rsi), %edx
	leal	(%rax,%rcx), %ebp
	subl	%eax, %ecx
	movl	-76(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rbp), %eax
	movl	%eax, -204(%rsi)
	subl	%ebx, %ebp
	movl	%ebp, -76(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -140(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -12(%rsi)
	movl	-8(%rsi), %eax
	movl	-200(%rsi), %ecx
	movl	-136(%rsi), %edx
	leal	(%rax,%rcx), %ebp
	subl	%eax, %ecx
	movl	-72(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rbp), %eax
	movl	%eax, -200(%rsi)
	subl	%ebx, %ebp
	movl	%ebp, -72(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -136(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -8(%rsi)
	movl	-4(%rsi), %eax
	movl	-196(%rsi), %ecx
	movl	-132(%rsi), %edx
	leal	(%rax,%rcx), %ebp
	subl	%eax, %ecx
	movl	-68(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rbp), %eax
	movl	%eax, -196(%rsi)
	subl	%ebx, %ebp
	movl	%ebp, -68(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -132(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -4(%rsi)
	movl	(%rsi), %ecx
	movl	-192(%rsi), %eax
	movl	-128(%rsi), %ebp
	leal	(%rcx,%rax), %ebx
	subl	%ecx, %eax
	movl	-64(%rsi), %ecx
	leal	(%rcx,%rbp), %edx
	subl	%ecx, %ebp
	leal	(%rdx,%rbx), %ecx
	movl	%ecx, -192(%rsi)
	movl	%ebx, %ecx
	subl	%edx, %ecx
	movl	%ecx, -64(%rsi)
	leal	(%rbp,%rax,2), %ecx
	movl	%ecx, -128(%rsi)
	leal	(%rbp,%rbp), %ecx
	movl	%eax, %edi
	subl	%ecx, %edi
	movl	%edi, (%rsi)
	addq	$4, %r11
	addq	$16, %rsi
	cmpq	$5, %r11
	jl	.LBB6_10
# BB#11:                                #   in Loop: Header=BB6_9 Depth=1
	addq	$4, %r9
	addq	$256, %r10              # imm = 0x100
	cmpq	$5, %r9
	jl	.LBB6_9
# BB#12:                                # %.preheader702.preheader
	movl	%ebx, 16(%rsp)
	movl	%eax, 28(%rsp)
	movl	%edx, 20(%rsp)
	movl	%ebp, 24(%rsp)
	leaq	476(%rsp), %r10
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB6_13:                               # %.preheader702
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_14 Depth 2
	movq	%r10, %rsi
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB6_14:                               # %.preheader700
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rsi), %eax
	movl	-108(%rsi), %edx
	movl	-76(%rsi), %ebp
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	movl	-44(%rsi), %eax
	leal	(%rax,%rbp), %ecx
	subl	%eax, %ebp
	leal	(%rcx,%rbx), %eax
	movl	%eax, -108(%rsi)
	subl	%ecx, %ebx
	movl	%ebx, -44(%rsi)
	leal	(%rbp,%rdx,2), %eax
	movl	%eax, -76(%rsi)
	addl	%ebp, %ebp
	subl	%ebp, %edx
	movl	%edx, -12(%rsi)
	movl	-8(%rsi), %eax
	movl	-104(%rsi), %ecx
	movl	-72(%rsi), %edx
	leal	(%rax,%rcx), %ebp
	subl	%eax, %ecx
	movl	-40(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rbp), %eax
	movl	%eax, -104(%rsi)
	subl	%ebx, %ebp
	movl	%ebp, -40(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -72(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -8(%rsi)
	movl	-4(%rsi), %eax
	movl	-100(%rsi), %ecx
	movl	-68(%rsi), %edx
	leal	(%rax,%rcx), %ebp
	subl	%eax, %ecx
	movl	-36(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rbp), %eax
	movl	%eax, -100(%rsi)
	subl	%ebx, %ebp
	movl	%ebp, -36(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -68(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -4(%rsi)
	movl	(%rsi), %eax
	movl	-96(%rsi), %ecx
	movl	-64(%rsi), %edx
	leal	(%rax,%rcx), %ebp
	subl	%eax, %ecx
	movl	-32(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rbp), %eax
	movl	%eax, -96(%rsi)
	subl	%ebx, %ebp
	movl	%ebp, -32(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -64(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, (%rsi)
	movl	-96(%rsi), %eax
	movl	-108(%rsi), %ecx
	movl	-104(%rsi), %edx
	leal	(%rax,%rcx), %ebp
	subl	%eax, %ecx
	movl	-100(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rbp), %eax
	movl	%eax, -108(%rsi)
	subl	%ebx, %ebp
	movl	%ebp, -100(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -104(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -96(%rsi)
	movl	-64(%rsi), %eax
	movl	-76(%rsi), %ecx
	movl	-72(%rsi), %edx
	leal	(%rax,%rcx), %ebp
	subl	%eax, %ecx
	movl	-68(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rbp), %eax
	movl	%eax, -76(%rsi)
	subl	%ebx, %ebp
	movl	%ebp, -68(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -72(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -64(%rsi)
	movl	-32(%rsi), %eax
	movl	-44(%rsi), %ecx
	movl	-40(%rsi), %edx
	leal	(%rax,%rcx), %ebp
	subl	%eax, %ecx
	movl	-36(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rbp), %eax
	movl	%eax, -44(%rsi)
	subl	%ebx, %ebp
	movl	%ebp, -36(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -40(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -32(%rsi)
	movl	(%rsi), %ecx
	movl	-12(%rsi), %eax
	movl	-8(%rsi), %ebp
	leal	(%rcx,%rax), %ebx
	subl	%ecx, %eax
	movl	-4(%rsi), %ecx
	leal	(%rcx,%rbp), %edx
	subl	%ecx, %ebp
	leal	(%rdx,%rbx), %ecx
	movl	%ecx, -12(%rsi)
	movl	%ebx, %ecx
	subl	%edx, %ecx
	movl	%ecx, -4(%rsi)
	leal	(%rbp,%rax,2), %ecx
	movl	%ecx, -8(%rsi)
	leal	(%rbp,%rbp), %ecx
	movl	%eax, %edi
	subl	%ecx, %edi
	movl	%edi, (%rsi)
	addq	$4, %r11
	subq	$-128, %rsi
	cmpq	$5, %r11
	jl	.LBB6_14
# BB#15:                                #   in Loop: Header=BB6_13 Depth=1
	addq	$4, %r9
	addq	$16, %r10
	cmpq	$5, %r9
	jl	.LBB6_13
# BB#16:
	movl	%ebx, 16(%rsp)
	movl	%eax, 28(%rsp)
	movl	%edx, 20(%rsp)
	movl	%ebp, 24(%rsp)
	movl	13136(%r15), %eax
	movl	13152(%r15), %ecx
	leal	(%rcx,%rax), %edx
	movl	13392(%r15), %esi
	leal	(%rdx,%rsi), %edi
	movl	13408(%r15), %ebp
	addl	%ebp, %edi
	movl	%edi, 224(%rsp)
	subl	%ecx, %eax
	leal	(%rax,%rsi), %ecx
	subl	%ebp, %ecx
	movl	%ecx, 228(%rsp)
	subl	%esi, %edx
	subl	%ebp, %edx
	movl	%edx, 232(%rsp)
	subl	%esi, %eax
	addl	%ebp, %eax
	movl	%eax, 236(%rsp)
	movl	496(%rsp), %eax
	movl	368(%rsp), %ecx
	movl	384(%rsp), %edx
	leal	(%rax,%rcx), %esi
	leal	(%rsi,%rdx), %ebp
	movl	512(%rsp), %ebx
	addl	%ebx, %ebp
	movl	%ebp, 320(%rsp)
	subl	%eax, %ecx
	leal	(%rcx,%rdx), %eax
	subl	%ebx, %eax
	movl	%eax, 324(%rsp)
	subl	%edx, %esi
	subl	%ebx, %esi
	movl	%esi, 328(%rsp)
	subl	%edx, %ecx
	addl	%ebx, %ecx
	movl	%ecx, 332(%rsp)
	movslq	%r8d, %rdx
	shlq	$6, %rdx
	movl	quant_coef(%rdx), %r10d
	movq	192(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rax), %r11d
	movq	296(%rsp), %rax         # 8-byte Reload
	leal	16(%rax), %r9d
	movslq	%r12d, %rcx
	shlq	$6, %rcx
	movl	quant_coef(%rcx), %eax
	movl	%eax, 144(%rsp)         # 4-byte Spill
	movq	272(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rax), %eax
	movl	%eax, 120(%rsp)         # 4-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	leal	16(%rax), %eax
	movl	%eax, 112(%rsp)         # 4-byte Spill
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	leaq	dequant_coef(%rcx), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	160(%rsp), %rax         # 8-byte Reload
	leal	(,%rax,4), %ecx
	movl	$983040, %eax           # imm = 0xF0000
	movl	%ecx, 220(%rsp)         # 4-byte Spill
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	cltq
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	248(%rsp), %rcx         # 8-byte Reload
	leaq	368(%rax,%rcx), %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movq	%rdx, 360(%rsp)         # 8-byte Spill
	leaq	dequant_coef(%rdx), %rax
	movl	(%rax), %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	$-1, %esi
	movl	36(%rsp), %r8d          # 4-byte Reload
	movl	%r9d, 12(%rsp)          # 4-byte Spill
	movl	%r10d, 176(%rsp)        # 4-byte Spill
	movl	%r11d, 168(%rsp)        # 4-byte Spill
	jmp	.LBB6_17
	.p2align	4, 0x90
.LBB6_36:                               # %._crit_edge
                                        #   in Loop: Header=BB6_17 Depth=1
	movl	324(%rsp,%r12,4), %ebp
	movl	228(%rsp,%r12,4), %edi
	incq	%r12
	movq	%r12, 48(%rsp)          # 8-byte Spill
.LBB6_17:                               # =>This Inner Loop Header: Depth=1
	incl	%esi
	movl	%ebp, %eax
	negl	%eax
	cmovll	%ebp, %eax
	imull	%r10d, %eax
	addl	%r11d, %eax
	movl	%r9d, %ecx
	sarl	%cl, %eax
	shll	%cl, %eax
	cltd
	idivl	%r10d
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	testl	%ebp, %ebp
	cmovnsl	%ecx, %eax
	movl	%edi, %ebx
	subl	%eax, %ebx
	movl	%ebx, %r15d
	negl	%r15d
	cmovll	%ebx, %r15d
	movl	144(%rsp), %eax         # 4-byte Reload
	imull	%eax, %r15d
	movl	120(%rsp), %edx         # 4-byte Reload
	addl	%edx, %r15d
	movl	112(%rsp), %ecx         # 4-byte Reload
	sarl	%cl, %r15d
	subl	%ebp, %edi
	movl	%edi, %r12d
	negl	%r12d
	cmovll	%edi, %r12d
	imull	%eax, %r12d
	addl	%edx, %r12d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r12d
	testl	%r12d, %r12d
	je	.LBB6_25
# BB#18:                                #   in Loop: Header=BB6_17 Depth=1
	cmpl	%r12d, %r15d
	je	.LBB6_25
# BB#19:                                #   in Loop: Header=BB6_17 Depth=1
	testl	%r15d, %r15d
	je	.LBB6_25
# BB#20:                                #   in Loop: Header=BB6_17 Depth=1
	movl	%ebp, 96(%rsp)          # 4-byte Spill
	movl	%r13d, 84(%rsp)         # 4-byte Spill
	movl	%r15d, %r13d
	negl	%r13d
	cmovll	%r15d, %r13d
	movl	%r13d, %eax
	negl	%eax
	movl	%ebx, 264(%rsp)         # 4-byte Spill
	testl	%ebx, %ebx
	cmovnsl	%r13d, %eax
	movq	128(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %ecx
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	imull	%ecx, %eax
	shll	$4, %eax
	movq	64(%rsp), %rcx          # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	sarl	$5, %eax
	movl	%edi, %ebx
	movl	%ebx, %r14d
	subl	%eax, %r14d
	movl	%r15d, %edi
	movl	%esi, 208(%rsp)         # 4-byte Spill
	leaq	40(%rsp), %rax
	movq	%rax, %rdx
	leaq	140(%rsp), %rax
	movq	%rax, %rcx
	callq	levrun_linfo_c2x2
	movl	%r12d, %ebp
	negl	%ebp
	cmovll	%r12d, %ebp
	movl	%ebp, %eax
	negl	%eax
	testl	%ebx, %ebx
	cvtsi2sdl	%r14d, %xmm0
	mulsd	%xmm0, %xmm0
	movl	40(%rsp), %ecx
	cvtsi2sdl	%ecx, %xmm1
	mulsd	88(%rsp), %xmm1         # 8-byte Folded Reload
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 200(%rsp)        # 8-byte Spill
	cmovnsl	%ebp, %eax
	imull	56(%rsp), %eax          # 4-byte Folded Reload
	shll	$4, %eax
	movq	64(%rsp), %rcx          # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	sarl	$5, %eax
	movl	%ebx, 44(%rsp)          # 4-byte Spill
	subl	%eax, %ebx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movl	%r12d, %edi
	movl	208(%rsp), %esi         # 4-byte Reload
	movl	%esi, %r14d
	leaq	40(%rsp), %rdx
	leaq	140(%rsp), %rcx
	callq	levrun_linfo_c2x2
	movsd	56(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	40(%rsp), %xmm0
	mulsd	88(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	%xmm1, %xmm0
	movsd	200(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	seta	%al
	cmpl	%ebp, %r13d
	setl	%cl
	ucomisd	%xmm0, %xmm1
	jne	.LBB6_21
# BB#22:                                #   in Loop: Header=BB6_17 Depth=1
	movl	84(%rsp), %r13d         # 4-byte Reload
	movl	36(%rsp), %r8d          # 4-byte Reload
	movl	176(%rsp), %r10d        # 4-byte Reload
	movl	168(%rsp), %r11d        # 4-byte Reload
	movl	12(%rsp), %r9d          # 4-byte Reload
	movl	96(%rsp), %ebp          # 4-byte Reload
	movl	%r14d, %esi
	movl	44(%rsp), %edx          # 4-byte Reload
	movl	264(%rsp), %ebx         # 4-byte Reload
	jp	.LBB6_24
# BB#23:                                #   in Loop: Header=BB6_17 Depth=1
	movl	%ecx, %eax
	jmp	.LBB6_24
	.p2align	4, 0x90
.LBB6_25:                               #   in Loop: Header=BB6_17 Depth=1
	cmpl	%r12d, %r15d
	je	.LBB6_27
# BB#26:                                #   in Loop: Header=BB6_17 Depth=1
	testl	%r15d, %r15d
	cmovnel	%r12d, %r15d
	cmovnel	%edi, %ebx
	jmp	.LBB6_27
	.p2align	4, 0x90
.LBB6_21:                               #   in Loop: Header=BB6_17 Depth=1
	movl	84(%rsp), %r13d         # 4-byte Reload
	movl	36(%rsp), %r8d          # 4-byte Reload
	movl	176(%rsp), %r10d        # 4-byte Reload
	movl	168(%rsp), %r11d        # 4-byte Reload
	movl	12(%rsp), %r9d          # 4-byte Reload
	movl	96(%rsp), %ebp          # 4-byte Reload
	movl	%r14d, %esi
	movl	44(%rsp), %edx          # 4-byte Reload
	movl	264(%rsp), %ebx         # 4-byte Reload
.LBB6_24:                               #   in Loop: Header=BB6_17 Depth=1
	testb	%al, %al
	cmovnel	%r15d, %r12d
	cmpl	%r15d, %r12d
	cmovnel	%edx, %ebx
	movl	%r12d, %r15d
.LBB6_27:                               #   in Loop: Header=BB6_17 Depth=1
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	input(%rip), %rax
	cmpl	$0, 4008(%rax)
	je	.LBB6_28
.LBB6_30:                               #   in Loop: Header=BB6_17 Depth=1
	testl	%r15d, %r15d
	movl	%r15d, %eax
	jne	.LBB6_32
# BB#31:                                #   in Loop: Header=BB6_17 Depth=1
	xorl	%eax, %eax
	jmp	.LBB6_33
	.p2align	4, 0x90
.LBB6_28:                               #   in Loop: Header=BB6_17 Depth=1
	cmpl	$2064, %r15d            # imm = 0x810
	jl	.LBB6_30
# BB#29:                                #   in Loop: Header=BB6_17 Depth=1
	movq	img(%rip), %rcx
	movl	$2063, %eax             # imm = 0x80F
	cmpl	$4, 36(%rcx)
	jge	.LBB6_30
.LBB6_32:                               # %.thread661
                                        #   in Loop: Header=BB6_17 Depth=1
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	288(%rsp), %rdx         # 8-byte Reload
	orq	%rdx, (%rcx)
	testl	%r13d, %r13d
	movl	$1, %ecx
	cmovlel	%ecx, %r13d
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	testl	%ebx, %ebx
	cmovnsl	%ecx, %eax
	movq	72(%rsp), %rdx          # 8-byte Reload
	movslq	%edx, %rdx
	movq	256(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx,%rdx,4)
	movq	184(%rsp), %rcx         # 8-byte Reload
	movl	%esi, (%rcx,%rdx,4)
	incl	%edx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	128(%rsp), %rcx         # 8-byte Reload
	imull	(%rcx), %eax
	shll	$4, %eax
	movq	64(%rsp), %rcx          # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	sarl	$5, %eax
	movl	$-1, %esi
.LBB6_33:                               #   in Loop: Header=BB6_17 Depth=1
	addl	%eax, %ebp
	movl	%ebp, %eax
	negl	%eax
	cmovll	%ebp, %eax
	imull	%r10d, %eax
	addl	%r11d, %eax
	movl	%r9d, %ecx
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	testl	%ebp, %ebp
	cmovnsl	%ecx, %eax
	movl	%eax, %edx
	imull	104(%rsp), %edx         # 4-byte Folded Reload
	movq	296(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edx
	movl	%edx, 224(%rsp,%r12,4)
	movl	sp2_frame_indicator(%rip), %ecx
	orl	si_frame_indicator(%rip), %ecx
	jne	.LBB6_35
# BB#34:                                #   in Loop: Header=BB6_17 Depth=1
	movq	lrec_uv(%rip), %rcx
	movq	280(%rsp), %rdx         # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movq	img(%rip), %rdx
	movl	%esi, %r14d
	movslq	188(%rdx), %rsi
	movl	%r12d, %edi
	shrl	$31, %edi
	addl	%r12d, %edi
	movl	%edi, %ebp
	andl	$1073741822, %ebp       # imm = 0x3FFFFFFE
	movl	%r12d, %ebx
	subl	%ebp, %ebx
	shll	$2, %ebx
	movslq	%ebx, %rbp
	addq	%rsi, %rbp
	movq	(%rcx,%rbp,8), %rcx
	movslq	184(%rdx), %rdx
	addl	%edi, %edi
	andl	$-4, %edi
	movslq	%edi, %rsi
	movl	12(%rsp), %r9d          # 4-byte Reload
	addq	%rdx, %rsi
	movl	%eax, (%rcx,%rsi,4)
	movl	%r14d, %esi
.LBB6_35:                               #   in Loop: Header=BB6_17 Depth=1
	cmpq	$3, %r12
	jne	.LBB6_36
# BB#37:
	movl	%r13d, 84(%rsp)         # 4-byte Spill
	movslq	72(%rsp), %rax          # 4-byte Folded Reload
	movq	256(%rsp), %rcx         # 8-byte Reload
	movl	$0, (%rcx,%rax,4)
	movl	224(%rsp), %eax
	movl	228(%rsp), %edi
	leal	(%rdi,%rax), %esi
	movl	232(%rsp), %ecx
	leal	(%rsi,%rcx), %ebx
	movl	236(%rsp), %edx
	addl	%edx, %ebx
	movl	%ebx, %ebp
	shrl	$31, %ebp
	addl	%ebx, %ebp
	sarl	%ebp
	movq	img(%rip), %r13
	movl	%ebp, 13136(%r13)
	subl	%edi, %eax
	leal	(%rax,%rcx), %ebp
	subl	%edx, %ebp
	movl	%ebp, %edi
	shrl	$31, %edi
	addl	%ebp, %edi
	sarl	%edi
	movl	%edi, 13152(%r13)
	subl	%ecx, %esi
	subl	%edx, %esi
	movl	%esi, %edi
	shrl	$31, %edi
	addl	%esi, %edi
	sarl	%edi
	movl	%edi, 13392(%r13)
	subl	%ecx, %eax
	addl	%edx, %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movl	%ecx, 13408(%r13)
	movq	160(%rsp), %rax         # 8-byte Reload
	addl	$4, %eax
	cltq
	movq	%rax, 336(%rsp)         # 8-byte Spill
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	248(%rsp), %rcx         # 8-byte Reload
	leaq	428(%rax,%rcx), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	addl	$16, 220(%rsp)          # 4-byte Folded Spill
	xorl	%r14d, %r14d
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movq	192(%rsp), %rsi         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_38:                               # %.preheader699
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_39 Depth 2
                                        #       Child Loop BB6_40 Depth 3
	movl	%r14d, %eax
	sarl	%eax
	movl	%eax, 304(%rsp)         # 4-byte Spill
	xorl	%r12d, %r12d
	movl	%r14d, 44(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB6_39:                               #   Parent Loop BB6_38 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_40 Depth 3
	movl	%r12d, %eax
	sarl	$2, %eax
	addl	304(%rsp), %eax         # 4-byte Folded Reload
	movq	14160(%r13), %rcx
	movq	336(%rsp), %rdx         # 8-byte Reload
	movq	(%rcx,%rdx,8), %rdx
	movslq	%eax, %rcx
	movq	(%rdx,%rcx,8), %rax
	movq	(%rax), %rdx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	addl	220(%rsp), %ecx         # 4-byte Folded Reload
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	movl	$-1, %r11d
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%r8d, %r15d
	movl	%r12d, 96(%rsp)         # 4-byte Spill
	jmp	.LBB6_40
.LBB6_53:                               #   in Loop: Header=BB6_40 Depth=3
	xorl	%eax, %eax
	movl	44(%rsp), %r14d         # 4-byte Reload
	movl	96(%rsp), %r12d         # 4-byte Reload
	movq	120(%rsp), %rbp         # 8-byte Reload
	movl	48(%rsp), %r11d         # 4-byte Reload
	jmp	.LBB6_49
.LBB6_55:                               #   in Loop: Header=BB6_40 Depth=3
	xorl	%eax, %eax
	movl	44(%rsp), %r14d         # 4-byte Reload
	movl	96(%rsp), %r12d         # 4-byte Reload
	movq	120(%rsp), %rbp         # 8-byte Reload
	movl	48(%rsp), %r11d         # 4-byte Reload
	movq	192(%rsp), %rsi         # 8-byte Reload
	movq	112(%rsp), %r15         # 8-byte Reload
	movl	104(%rsp), %ebx         # 4-byte Reload
	movl	36(%rsp), %edi          # 4-byte Reload
	jmp	.LBB6_56
	.p2align	4, 0x90
.LBB6_40:                               #   Parent Loop BB6_38 Depth=1
                                        #     Parent Loop BB6_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	264(%rsp), %rax         # 8-byte Reload
	cmpl	$0, (%rax)
	movl	$FIELD_SCAN, %eax
	movl	$SNGL_SCAN, %ecx
	cmoveq	%rcx, %rax
	movzbl	2(%rax,%rbp,2), %ecx
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	movq	%rsi, %rdx
	movzbl	3(%rax,%rbp,2), %r9d
	incl	%r11d
	movl	%r11d, 48(%rsp)         # 4-byte Spill
	movslq	%r12d, %rbx
	addq	%rcx, %rbx
	movslq	%r14d, %rdi
	addq	%r9, %rdi
	movq	%rbx, %rax
	shlq	$5, %rax
	leaq	368(%rsp,%rax), %r11
	movl	(%r11,%rdi,4), %r8d
	movl	%r8d, %eax
	negl	%eax
	cmovll	%r8d, %eax
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movq	%rcx, %rsi
	shlq	$4, %rsi
	movq	360(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movl	quant_coef(%rcx,%r9,4), %ebp
	imull	%ebp, %eax
	addl	%edx, %eax
	movl	%r15d, %ecx
	sarl	%cl, %eax
	shll	%cl, %eax
	cltd
	movl	%ebp, 104(%rsp)         # 4-byte Spill
	idivl	%ebp
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movq	%rdi, %rax
	shlq	$6, %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	addq	%rax, %r13
	movl	%ecx, %eax
	negl	%eax
	testl	%r8d, %r8d
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movl	13136(%r13,%rbx,4), %r13d
	cmovnsl	%ecx, %eax
	movl	%r13d, %ebp
	subl	%eax, %ebp
	movl	%ebp, %r14d
	negl	%r14d
	cmovll	%ebp, %r14d
	movq	352(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	leaq	(%rax,%rsi), %r15
	movl	quant_coef(%r15,%r9,4), %eax
	imull	%eax, %r14d
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, %rdx
	addl	%edx, %r14d
	movl	308(%rsp), %ecx         # 4-byte Reload
	sarl	%cl, %r14d
	subl	%r8d, %r13d
	movl	%r13d, %r12d
	negl	%r12d
	cmovll	%r13d, %r12d
	imull	%eax, %r12d
	movq	%rdi, 208(%rsp)         # 8-byte Spill
	leaq	(%r11,%rdi,4), %r11
	addl	%edx, %r12d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r12d
	testl	%r12d, %r12d
	je	.LBB6_50
# BB#41:                                #   in Loop: Header=BB6_40 Depth=3
	cmpl	%r12d, %r14d
	je	.LBB6_50
# BB#42:                                #   in Loop: Header=BB6_40 Depth=3
	testl	%r14d, %r14d
	je	.LBB6_50
# BB#43:                                # %.thread665
                                        #   in Loop: Header=BB6_40 Depth=3
	movl	%r14d, %ecx
	negl	%ecx
	cmovll	%r14d, %ecx
	movl	%ecx, 256(%rsp)         # 4-byte Spill
	movl	%ecx, %eax
	negl	%eax
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	testl	%ebp, %ebp
	cmovnsl	%ecx, %eax
	movq	144(%rsp), %rcx         # 8-byte Reload
	movl	A(%rcx,%r9,4), %ecx
	movq	%r9, 176(%rsp)          # 8-byte Spill
	movq	%r15, 168(%rsp)         # 8-byte Spill
	imull	dequant_coef(%r15,%r9,4), %ecx
	movl	%ecx, 184(%rsp)         # 4-byte Spill
	imull	%ecx, %eax
	movq	64(%rsp), %rcx          # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	sarl	$6, %eax
	movl	%r13d, %ebx
	subl	%eax, %ebx
	movl	%r14d, %edi
	movl	48(%rsp), %esi          # 4-byte Reload
	leaq	40(%rsp), %rax
	movq	%rax, %rdx
	leaq	140(%rsp), %rax
	movq	%rax, %rcx
	movq	%r11, %rbp
	callq	levrun_linfo_inter
	movq	img(%rip), %rax
	addq	128(%rsp), %rax         # 8-byte Folded Reload
	movl	%r12d, %r15d
	negl	%r15d
	cmovll	%r12d, %r15d
	movl	%r15d, %edx
	negl	%edx
	testl	%r13d, %r13d
	cvtsi2sdl	%ebx, %xmm0
	movq	64(%rsp), %rcx          # 8-byte Reload
	mulsd	%xmm0, %xmm0
	cvtsi2sdl	40(%rsp), %xmm1
	mulsd	88(%rsp), %xmm1         # 8-byte Folded Reload
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 288(%rsp)        # 8-byte Spill
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	13136(%rax,%rsi,4), %eax
	cmovnsl	%r15d, %edx
	imull	184(%rsp), %edx         # 4-byte Folded Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edx
	sarl	$6, %edx
	movq	%rbp, 160(%rsp)         # 8-byte Spill
	subl	(%rbp), %eax
	subl	%edx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 184(%rsp)        # 8-byte Spill
	movl	%r12d, %edi
	movl	48(%rsp), %esi          # 4-byte Reload
	leaq	40(%rsp), %rdx
	leaq	140(%rsp), %rcx
	callq	levrun_linfo_inter
	movsd	184(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	40(%rsp), %xmm0
	mulsd	88(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	%xmm1, %xmm0
	movsd	288(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	seta	%al
	cmpl	%r15d, 256(%rsp)        # 4-byte Folded Reload
	setl	%cl
	ucomisd	%xmm0, %xmm1
	jne	.LBB6_44
# BB#45:                                # %.thread665
                                        #   in Loop: Header=BB6_40 Depth=3
	movl	36(%rsp), %edi          # 4-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	112(%rsp), %r10         # 8-byte Reload
	movl	104(%rsp), %ebx         # 4-byte Reload
	movq	168(%rsp), %r15         # 8-byte Reload
	movq	160(%rsp), %r11         # 8-byte Reload
	jp	.LBB6_47
# BB#46:                                # %.thread665
                                        #   in Loop: Header=BB6_40 Depth=3
	movl	%ecx, %eax
	jmp	.LBB6_47
	.p2align	4, 0x90
.LBB6_50:                               #   in Loop: Header=BB6_40 Depth=3
	cmpl	%r12d, %r14d
	jne	.LBB6_54
# BB#51:                                #   in Loop: Header=BB6_40 Depth=3
	movl	%r14d, %r12d
	movl	%ebp, %r13d
	jmp	.LBB6_52
	.p2align	4, 0x90
.LBB6_44:                               #   in Loop: Header=BB6_40 Depth=3
	movl	36(%rsp), %edi          # 4-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	112(%rsp), %r10         # 8-byte Reload
	movl	104(%rsp), %ebx         # 4-byte Reload
	movq	168(%rsp), %r15         # 8-byte Reload
	movq	160(%rsp), %r11         # 8-byte Reload
.LBB6_47:                               # %.thread665
                                        #   in Loop: Header=BB6_40 Depth=3
	testb	%al, %al
	cmovnel	%r14d, %r12d
	cmpl	%r14d, %r12d
	cmovel	12(%rsp), %r13d         # 4-byte Folded Reload
	jmp	.LBB6_48
	.p2align	4, 0x90
.LBB6_54:                               #   in Loop: Header=BB6_40 Depth=3
	testl	%r14d, %r14d
	je	.LBB6_55
.LBB6_52:                               #   in Loop: Header=BB6_40 Depth=3
	testl	%r12d, %r12d
	movq	112(%rsp), %r10         # 8-byte Reload
	movl	104(%rsp), %ebx         # 4-byte Reload
	movl	36(%rsp), %edi          # 4-byte Reload
	je	.LBB6_53
.LBB6_48:                               #   in Loop: Header=BB6_40 Depth=3
	movq	312(%rsp), %rax         # 8-byte Reload
	movq	344(%rsp), %rcx         # 8-byte Reload
	orq	%rcx, (%rax)
	movl	%r12d, %ecx
	negl	%ecx
	cmovll	%r12d, %ecx
	movl	%ecx, %eax
	negl	%eax
	testl	%r13d, %r13d
	cmovnsl	%ecx, %eax
	movq	56(%rsp), %rdx          # 8-byte Reload
	movslq	%edx, %rdx
	movq	152(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx,%rdx,4)
	movq	248(%rsp), %rcx         # 8-byte Reload
	movl	48(%rsp), %esi          # 4-byte Reload
	movl	%esi, (%rcx,%rdx,4)
	incl	%edx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	imull	dequant_coef(%r15,%r9,4), %eax
	movq	144(%rsp), %rcx         # 8-byte Reload
	imull	A(%rcx,%r9,4), %eax
	movq	64(%rsp), %rcx          # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	sarl	$6, %eax
	movl	(%r11), %r8d
	movl	$-1, %r11d
	movl	$2, 12(%rsp)            # 4-byte Folded Spill
	movl	44(%rsp), %r14d         # 4-byte Reload
	movl	96(%rsp), %r12d         # 4-byte Reload
	movq	120(%rsp), %rbp         # 8-byte Reload
.LBB6_49:                               # %.thread664
                                        #   in Loop: Header=BB6_40 Depth=3
	movq	192(%rsp), %rsi         # 8-byte Reload
	movq	%r10, %r15
.LBB6_56:                               # %.thread664
                                        #   in Loop: Header=BB6_40 Depth=3
	addl	%eax, %r8d
	movl	sp2_frame_indicator(%rip), %eax
	orl	si_frame_indicator(%rip), %eax
	jne	.LBB6_59
# BB#57:                                #   in Loop: Header=BB6_40 Depth=3
	movq	208(%rsp), %rax         # 8-byte Reload
	orl	72(%rsp), %eax          # 4-byte Folded Reload
	testb	$3, %al
	je	.LBB6_59
# BB#58:                                #   in Loop: Header=BB6_40 Depth=3
	movl	%r8d, %eax
	negl	%eax
	cmovll	%r8d, %eax
	imull	%ebx, %eax
	addl	%esi, %eax
	movl	%edi, %ecx
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	testl	%r8d, %r8d
	cmovnsl	%ecx, %eax
	movq	lrec_uv(%rip), %rcx
	movq	280(%rsp), %rdx         # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movq	img(%rip), %rdx
	movl	%edi, %r10d
	movq	%rsi, %r13
	movl	%r9d, %edi
	addl	%r12d, %edi
	addl	188(%rdx), %edi
	movslq	%edi, %rdi
	movq	(%rcx,%rdi,8), %rcx
	movq	200(%rsp), %rdi         # 8-byte Reload
	addl	%r14d, %edi
	addl	184(%rdx), %edi
	movslq	%edi, %rdx
	movl	%r10d, %edi
	movl	%eax, (%rcx,%rdx,4)
.LBB6_59:                               #   in Loop: Header=BB6_40 Depth=3
	movl	%r8d, %eax
	negl	%eax
	cmovll	%r8d, %eax
	imull	%eax, %ebx
	addl	%esi, %ebx
	movl	%edi, %ecx
	sarl	%cl, %ebx
	movl	%ebx, %eax
	negl	%eax
	cmovll	%ebx, %eax
	movl	%eax, %edx
	negl	%edx
	testl	%r8d, %r8d
	cmovnsl	%eax, %edx
	imull	dequant_coef(%r15,%r9,4), %edx
	movq	296(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edx
	movq	img(%rip), %r13
	movq	128(%rsp), %rcx         # 8-byte Reload
	addq	%r13, %rcx
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	%edx, 13136(%rcx,%rax,4)
	incq	%rbp
	cmpq	$15, %rbp
	movl	%edi, %r15d
	jne	.LBB6_40
# BB#60:                                #   in Loop: Header=BB6_39 Depth=2
	movl	%r15d, %r8d
	movslq	56(%rsp), %rax          # 4-byte Folded Reload
	movq	152(%rsp), %rcx         # 8-byte Reload
	movl	$0, (%rcx,%rax,4)
	addl	$4, %r12d
	cmpl	$5, %r12d
	jl	.LBB6_39
# BB#61:                                #   in Loop: Header=BB6_38 Depth=1
	addl	$4, %r14d
	cmpl	$5, %r14d
	jl	.LBB6_38
# BB#62:
	leaq	13328(%r13), %rax
	xorl	%r10d, %r10d
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_63:                               # %.preheader698
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_64 Depth 2
                                        #       Child Loop BB6_65 Depth 3
	leaq	(,%rcx,4), %rdx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	leaq	1(,%rcx,4), %rdx
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	%rsi, %rdx
	orq	$1, %rdx
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	leaq	2(,%rcx,4), %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	%rsi, %rdx
	orq	$2, %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	3(,%rcx,4), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rsi, %rcx
	orq	$3, %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	%rax, %r9
	xorl	%r12d, %r12d
	xorl	%r8d, %r8d
	movq	%rsi, 128(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB6_64:                               # %.preheader695
                                        #   Parent Loop BB6_63 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_65 Depth 3
	movq	144(%rsp), %rax         # 8-byte Reload
	shlq	$6, %rax
	leaq	13136(%r13,%rax), %rax
	movq	%r8, 72(%rsp)           # 8-byte Spill
	shlq	$4, %r8
	movups	(%r8,%rax), %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	24(%rsp), %ecx
	movl	16(%rsp), %eax
	movl	20(%rsp), %edx
	leal	(%rcx,%rax), %edi
	subl	%ecx, %eax
	movl	%edx, %ecx
	sarl	%ecx
	movl	28(%rsp), %ebp
	subl	%ebp, %ecx
	sarl	%ebp
	addl	%edx, %ebp
	leal	(%rbp,%rdi), %edx
	shlq	$6, %rsi
	leaq	13136(%r13,%rsi), %rbx
	leaq	(,%r12,4), %r11
	movl	%edx, (%rbx,%r12,4)
	subl	%ebp, %edi
	movq	%r11, %r15
	orq	$12, %r15
	movl	%edi, (%r15,%rbx)
	leal	(%rcx,%rax), %edx
	movq	%r11, %r14
	orq	$4, %r14
	movl	%edx, (%r14,%rbx)
	subl	%ecx, %eax
	orq	$8, %r11
	movl	%eax, (%r11,%rbx)
	movq	120(%rsp), %rax         # 8-byte Reload
	shlq	$6, %rax
	leaq	13136(%r13,%rax), %rax
	movups	(%r8,%rax), %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	24(%rsp), %ecx
	movl	16(%rsp), %eax
	movl	20(%rsp), %edx
	leal	(%rcx,%rax), %edi
	subl	%ecx, %eax
	movl	%edx, %ecx
	sarl	%ecx
	movl	28(%rsp), %ebx
	subl	%ebx, %ecx
	sarl	%ebx
	addl	%edx, %ebx
	leal	(%rbx,%rdi), %edx
	movq	112(%rsp), %rbp         # 8-byte Reload
	shlq	$6, %rbp
	leaq	13136(%r13,%rbp), %rbp
	movl	%edx, (%rbp,%r12,4)
	subl	%ebx, %edi
	movl	%edi, (%r15,%rbp)
	leal	(%rcx,%rax), %edx
	movl	%edx, (%r14,%rbp)
	subl	%ecx, %eax
	movl	%eax, (%r11,%rbp)
	movq	104(%rsp), %rax         # 8-byte Reload
	shlq	$6, %rax
	leaq	13136(%r13,%rax), %rax
	movups	(%r8,%rax), %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	24(%rsp), %ecx
	movl	16(%rsp), %eax
	movl	20(%rsp), %edx
	leal	(%rcx,%rax), %edi
	subl	%ecx, %eax
	movl	%edx, %ecx
	sarl	%ecx
	movl	28(%rsp), %ebx
	subl	%ebx, %ecx
	sarl	%ebx
	addl	%edx, %ebx
	leal	(%rbx,%rdi), %edx
	movq	64(%rsp), %rbp          # 8-byte Reload
	shlq	$6, %rbp
	leaq	13136(%r13,%rbp), %rbp
	movl	%edx, (%rbp,%r12,4)
	subl	%ebx, %edi
	movl	%edi, (%r15,%rbp)
	leal	(%rcx,%rax), %edx
	movl	%edx, (%r14,%rbp)
	subl	%ecx, %eax
	movl	%eax, (%r11,%rbp)
	movq	56(%rsp), %rax          # 8-byte Reload
	shlq	$6, %rax
	leaq	13136(%r13,%rax), %rax
	movupd	(%r8,%rax), %xmm0
	movapd	%xmm0, 16(%rsp)
	movl	24(%rsp), %ecx
	movl	16(%rsp), %eax
	movl	20(%rsp), %edx
	leal	(%rcx,%rax), %edi
	movl	%edx, %ebp
	sarl	%ebp
	movl	28(%rsp), %ebx
	subl	%ebx, %ebp
	sarl	%ebx
	addl	%edx, %ebx
	leal	(%rbx,%rdi), %edx
	movq	208(%rsp), %rsi         # 8-byte Reload
	shlq	$6, %rsi
	leaq	13136(%r13,%rsi), %rsi
	movl	%edx, (%rsi,%r12,4)
	subl	%ebx, %edi
	movl	%edi, (%r15,%rsi)
	subl	%ecx, %eax
	leal	(%rbp,%rax), %ecx
	movl	%ecx, (%r14,%rsi)
	subl	%ebp, %eax
	movl	%eax, (%r11,%rsi)
	movl	$4, %esi
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%r9, %rbp
	.p2align	4, 0x90
.LBB6_65:                               # %.preheader694
                                        #   Parent Loop BB6_63 Depth=1
                                        #     Parent Loop BB6_64 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-192(%rbp), %r15d
	movl	-128(%rbp), %r14d
	movl	-64(%rbp), %r8d
	movl	(%rbp), %r11d
	movl	%r15d, %eax
	subl	%r8d, %eax
	movl	%r14d, %edi
	sarl	%edi
	subl	%r11d, %edi
	movl	%r11d, %edx
	sarl	%edx
	addl	%r14d, %edx
	movl	15524(%r13), %ebx
	leal	32(%r8,%r15), %ecx
	leal	(%rcx,%rdx), %r9d
	sarl	$6, %r9d
	cmovsl	%r10d, %r9d
	cmpl	%ebx, %r9d
	cmovgl	%ebx, %r9d
	movl	%r9d, -192(%rbp)
	movl	15524(%r13), %ebx
	subl	%edx, %ecx
	sarl	$6, %ecx
	cmovsl	%r10d, %ecx
	cmpl	%ebx, %ecx
	cmovgl	%ebx, %ecx
	movl	%ecx, (%rbp)
	movl	15524(%r13), %ecx
	leal	32(%rax), %edx
	leal	32(%rax,%rdi), %eax
	sarl	$6, %eax
	cmovsl	%r10d, %eax
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	movl	%eax, -128(%rbp)
	movl	15524(%r13), %eax
	subl	%edi, %edx
	sarl	$6, %edx
	cmovsl	%r10d, %edx
	cmpl	%eax, %edx
	cmovgl	%eax, %edx
	movl	%edx, -64(%rbp)
	addq	$4, %rbp
	decq	%rsi
	jne	.LBB6_65
# BB#66:                                #   in Loop: Header=BB6_64 Depth=2
	movl	%r15d, 16(%rsp)
	movl	%r14d, 20(%rsp)
	movl	%r8d, 24(%rsp)
	movl	%r11d, 28(%rsp)
	addq	$4, %r12
	movq	72(%rsp), %r8           # 8-byte Reload
	incq	%r8
	movq	48(%rsp), %r9           # 8-byte Reload
	addq	$16, %r9
	cmpq	$2, %r8
	movq	128(%rsp), %rsi         # 8-byte Reload
	jne	.LBB6_64
# BB#67:                                #   in Loop: Header=BB6_63 Depth=1
	addq	$4, %rsi
	movq	96(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movq	200(%rsp), %rax         # 8-byte Reload
	addq	$256, %rax              # imm = 0x100
	cmpq	$2, %rcx
	jne	.LBB6_63
# BB#68:                                # %.preheader
	movq	enc_picture(%rip), %rax
	movq	6472(%rax), %rax
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	movslq	184(%r13), %rdi
	movslq	188(%r13), %rdx
	movl	12(%rsp), %esi          # 4-byte Reload
	cmpl	$2, %esi
	movl	84(%rsp), %eax          # 4-byte Reload
	cmovel	%esi, %eax
	movq	(%rcx,%rdx,8), %rsi
	movdqu	13136(%r13), %xmm0
	movdqu	13152(%r13), %xmm1
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	packssdw	%xmm1, %xmm0
	movdqu	%xmm0, (%rsi,%rdi,2)
	movq	8(%rcx,%rdx,8), %rsi
	movdqu	13200(%r13), %xmm0
	movdqu	13216(%r13), %xmm1
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	packssdw	%xmm1, %xmm0
	movdqu	%xmm0, (%rsi,%rdi,2)
	movq	16(%rcx,%rdx,8), %rsi
	movdqu	13264(%r13), %xmm0
	movdqu	13280(%r13), %xmm1
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	packssdw	%xmm1, %xmm0
	movdqu	%xmm0, (%rsi,%rdi,2)
	movq	24(%rcx,%rdx,8), %rsi
	movdqu	13328(%r13), %xmm0
	movdqu	13344(%r13), %xmm1
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	packssdw	%xmm1, %xmm0
	movdqu	%xmm0, (%rsi,%rdi,2)
	movq	32(%rcx,%rdx,8), %rsi
	movdqu	13392(%r13), %xmm0
	movdqu	13408(%r13), %xmm1
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	packssdw	%xmm1, %xmm0
	movdqu	%xmm0, (%rsi,%rdi,2)
	movq	40(%rcx,%rdx,8), %rsi
	movdqu	13456(%r13), %xmm0
	movdqu	13472(%r13), %xmm1
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	packssdw	%xmm1, %xmm0
	movdqu	%xmm0, (%rsi,%rdi,2)
	movq	48(%rcx,%rdx,8), %rsi
	movdqu	13520(%r13), %xmm0
	movdqu	13536(%r13), %xmm1
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	packssdw	%xmm1, %xmm0
	movdqu	%xmm0, (%rsi,%rdi,2)
	movq	56(%rcx,%rdx,8), %rcx
	movdqu	13584(%r13), %xmm0
	movdqu	13600(%r13), %xmm1
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	packssdw	%xmm1, %xmm0
	movdqu	%xmm0, (%rcx,%rdi,2)
	addq	$632, %rsp              # imm = 0x278
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	dct_chroma_sp, .Lfunc_end6-dct_chroma_sp
	.cfi_endproc

	.globl	copyblock_sp
	.p2align	4, 0x90
	.type	copyblock_sp,@function
copyblock_sp:                           # @copyblock_sp
	.cfi_startproc
# BB#0:                                 # %.preheader166
	pushq	%rbp
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi93:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi94:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi96:
	.cfi_def_cfa_offset 112
.Lcfi97:
	.cfi_offset %rbx, -56
.Lcfi98:
	.cfi_offset %r12, -48
.Lcfi99:
	.cfi_offset %r13, -40
.Lcfi100:
	.cfi_offset %r14, -32
.Lcfi101:
	.cfi_offset %r15, -24
.Lcfi102:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	img(%rip), %rax
	movq	14224(%rax), %rcx
	movslq	12(%rax), %rdx
	imulq	$536, %rdx, %rdx        # imm = 0x218
	movslq	20(%rcx,%rdx), %rdx
	imulq	$715827883, %rdx, %rcx  # imm = 0x2AAAAAAB
	movq	%rdx, %r15
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$32, %rcx
	leal	(%rcx,%rdx), %r13d
	leal	15(%rcx,%rdx), %r12d
	movq	%rdi, -64(%rsp)         # 8-byte Spill
	movslq	%edi, %rcx
	movq	%rsi, %rdx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	movslq	%esi, %rdx
	shlq	$5, %rdx
	addq	%rax, %rdx
	movzwl	12624(%rdx,%rcx,2), %ebx
	movzwl	12626(%rdx,%rcx,2), %r11d
	movzwl	12628(%rdx,%rcx,2), %r9d
	movzwl	12630(%rdx,%rcx,2), %r14d
	movzwl	12656(%rdx,%rcx,2), %r10d
	movzwl	12658(%rdx,%rcx,2), %ebp
	movzwl	12660(%rdx,%rcx,2), %eax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movzwl	12662(%rdx,%rcx,2), %edi
	movzwl	12688(%rdx,%rcx,2), %eax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	movzwl	12690(%rdx,%rcx,2), %eax
	movzwl	12692(%rdx,%rcx,2), %esi
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	movzwl	12694(%rdx,%rcx,2), %esi
	movq	%rsi, -88(%rsp)         # 8-byte Spill
	movzwl	12720(%rdx,%rcx,2), %esi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movzwl	12722(%rdx,%rcx,2), %r8d
	movzwl	12724(%rdx,%rcx,2), %esi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movzwl	12726(%rdx,%rcx,2), %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
	leal	(%r13,%r13), %ecx
	leal	(%rcx,%rcx,2), %ecx
	subl	%ecx, %r15d
	movq	%r15, -72(%rsp)         # 8-byte Spill
	movl	$1, %edx
	movl	%r12d, %ecx
	shll	%cl, %edx
	movl	%edx, %r15d
	shrl	$31, %r15d
	addl	%edx, %r15d
	leal	(%r14,%rbx), %r13d
	subl	%r14d, %ebx
	leal	(%r9,%r11), %ecx
	subl	%r9d, %r11d
	leal	(%rcx,%r13), %edx
	subl	%ecx, %r13d
	leal	(%r11,%rbx,2), %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	addl	%r11d, %r11d
	subl	%r11d, %ebx
	movl	%ebx, (%rsp)
	leal	(%rdi,%r10), %ebx
	subl	%edi, %r10d
	movq	-96(%rsp), %rdi         # 8-byte Reload
	leal	(%rdi,%rbp), %ecx
	subl	%edi, %ebp
	leal	(%rcx,%rbx), %r11d
	subl	%ecx, %ebx
	leal	(%rbp,%r10,2), %ecx
	addl	%ebp, %ebp
	subl	%ebp, %r10d
	movq	-104(%rsp), %rdi        # 8-byte Reload
	movq	-88(%rsp), %r9          # 8-byte Reload
	leal	(%r9,%rdi), %ebp
	subl	%r9d, %edi
	movq	-80(%rsp), %r9          # 8-byte Reload
	leal	(%r9,%rax), %r14d
	subl	%r9d, %eax
	leal	(%r14,%rbp), %esi
	movq	%rsi, -96(%rsp)         # 8-byte Spill
	subl	%r14d, %ebp
	movq	%rbp, -80(%rsp)         # 8-byte Spill
	leal	(%rax,%rdi,2), %ebp
	movq	%rbp, -88(%rsp)         # 8-byte Spill
	addl	%eax, %eax
	subl	%eax, %edi
	movq	%rdi, -104(%rsp)        # 8-byte Spill
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	leal	(%rsi,%rdi), %r9d
	subl	%esi, %edi
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r8), %r14d
	subl	%eax, %r8d
	leal	(%r14,%r9), %eax
	subl	%r14d, %r9d
	leal	(%r8,%rdi,2), %r14d
	addl	%r8d, %r8d
	subl	%r8d, %edi
	leal	(%rax,%rdx), %esi
	subl	%eax, %edx
	movq	-96(%rsp), %rbp         # 8-byte Reload
	leal	(%rbp,%r11), %eax
	subl	%ebp, %r11d
	leal	(%rax,%rsi), %ebp
	movl	%ebp, -48(%rsp)
	subl	%eax, %esi
	movl	%esi, -40(%rsp)
	leal	(%r11,%rdx,2), %eax
	movl	%eax, -44(%rsp)
	addl	%r11d, %r11d
	subl	%r11d, %edx
	movl	%edx, -36(%rsp)
	movq	40(%rsp), %rdx          # 8-byte Reload
	leal	(%r14,%rdx), %eax
	subl	%r14d, %edx
	movq	-88(%rsp), %rsi         # 8-byte Reload
	leal	(%rsi,%rcx), %ebp
	subl	%esi, %ecx
	leal	(%rbp,%rax), %esi
	movl	%esi, -32(%rsp)
	subl	%ebp, %eax
	movl	%eax, -24(%rsp)
	leal	(%rcx,%rdx,2), %eax
	movl	%eax, -28(%rsp)
	addl	%ecx, %ecx
	subl	%ecx, %edx
	movl	%edx, -20(%rsp)
	leal	(%r9,%r13), %eax
	subl	%r9d, %r13d
	movq	-80(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rbx), %ecx
	subl	%edx, %ebx
	leal	(%rcx,%rax), %edx
	movl	%edx, -16(%rsp)
	subl	%ecx, %eax
	movl	%eax, -8(%rsp)
	leal	(%rbx,%r13,2), %eax
	movl	%eax, -12(%rsp)
	addl	%ebx, %ebx
	subl	%ebx, %r13d
	movl	%r13d, -4(%rsp)
	movl	(%rsp), %eax
	leal	(%rdi,%rax), %ecx
	subl	%edi, %eax
	movq	-104(%rsp), %rsi        # 8-byte Reload
	leal	(%rsi,%r10), %edx
	subl	%esi, %r10d
	leal	(%rdx,%rcx), %esi
	movl	%esi, (%rsp)
	movl	%ecx, -128(%rsp)
	movl	%eax, -116(%rsp)
	movl	%edx, -124(%rsp)
	subl	%edx, %ecx
	sarl	%r15d
	movl	%r10d, -120(%rsp)
	movl	%ecx, 8(%rsp)
	leal	(%r10,%rax,2), %ecx
	movl	%ecx, 4(%rsp)
	addl	%r10d, %r10d
	subl	%r10d, %eax
	movl	%eax, 12(%rsp)
	movslq	-72(%rsp), %r11         # 4-byte Folded Reload
	movq	img(%rip), %rax
	shlq	$6, %r11
	movq	%rax, -104(%rsp)        # 8-byte Spill
	leaq	13136(%rax), %rcx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB7_1:                                # %.preheader160
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_2 Depth 2
	movq	-56(%rsp), %rax         # 8-byte Reload
	leal	(%r13,%rax), %r14d
	movq	-64(%rsp), %rax         # 8-byte Reload
	movl	%eax, %esi
	movq	%rcx, -72(%rsp)         # 8-byte Spill
	movq	%rcx, %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_2:                                #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-48(%rsp,%rax), %r9
	movl	(%r9,%r13,4), %edi
	movl	%edi, %r10d
	negl	%r10d
	cmovll	%edi, %r10d
	leaq	(%r11,%rax), %r8
	movl	quant_coef(%r8,%r13,4), %edx
	imull	%edx, %r10d
	addl	%r15d, %r10d
	movl	%r12d, %ecx
	sarl	%cl, %r10d
	movl	%r10d, %ecx
	negl	%ecx
	cmovll	%r10d, %ecx
	movl	%ecx, %ebx
	negl	%ebx
	testl	%edi, %edi
	cmovnsl	%ecx, %ebx
	imull	dequant_coef(%r8,%r13,4), %ebx
	movq	48(%rsp), %rcx          # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %ebx
	movl	%ebx, (%rbp)
	movl	sp2_frame_indicator(%rip), %ecx
	orl	si_frame_indicator(%rip), %ecx
	jne	.LBB7_4
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=2
	movl	(%r9,%r13,4), %ebx
	movl	%ebx, %edi
	negl	%edi
	cmovll	%ebx, %edi
	imull	%edx, %edi
	addl	%r15d, %edi
	movl	%r12d, %ecx
	sarl	%cl, %edi
	movl	%edi, %ecx
	negl	%ecx
	cmovll	%edi, %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%ebx, %ebx
	cmovnsl	%ecx, %edx
	movq	lrec(%rip), %rcx
	movq	-104(%rsp), %rbx        # 8-byte Reload
	movl	180(%rbx), %edi
	addl	%r14d, %edi
	movslq	%edi, %rdi
	movq	(%rcx,%rdi,8), %rcx
	movl	176(%rbx), %edi
	addl	%esi, %edi
	movslq	%edi, %rdi
	movl	%edx, (%rcx,%rdi,4)
.LBB7_4:                                #   in Loop: Header=BB7_2 Depth=2
	addq	$16, %rax
	addq	$4, %rbp
	incl	%esi
	cmpq	$64, %rax
	jne	.LBB7_2
# BB#5:                                 #   in Loop: Header=BB7_1 Depth=1
	incq	%r13
	movq	-72(%rsp), %rcx         # 8-byte Reload
	addq	$64, %rcx
	cmpq	$4, %r13
	jne	.LBB7_1
# BB#6:                                 # %.preheader158
	movq	-104(%rsp), %r15        # 8-byte Reload
	movups	13136(%r15), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %edi
	subl	%edi, %eax
	sarl	%edi
	addl	%edx, %edi
	leal	(%rdi,%rsi), %edx
	movl	%edx, 13136(%r15)
	subl	%edi, %esi
	movl	%esi, 13148(%r15)
	leal	(%rax,%rcx), %edx
	movl	%edx, 13140(%r15)
	subl	%eax, %ecx
	movl	%ecx, 13144(%r15)
	movups	13200(%r15), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %edi
	subl	%edi, %eax
	sarl	%edi
	addl	%edx, %edi
	leal	(%rdi,%rsi), %edx
	movl	%edx, 13200(%r15)
	subl	%edi, %esi
	movl	%esi, 13212(%r15)
	leal	(%rax,%rcx), %edx
	movl	%edx, 13204(%r15)
	subl	%eax, %ecx
	movl	%ecx, 13208(%r15)
	movups	13264(%r15), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %edi
	subl	%edi, %eax
	sarl	%edi
	addl	%edx, %edi
	leal	(%rdi,%rsi), %edx
	movl	%edx, 13264(%r15)
	subl	%edi, %esi
	movl	%esi, 13276(%r15)
	leal	(%rax,%rcx), %edx
	movl	%edx, 13268(%r15)
	subl	%eax, %ecx
	movl	%ecx, 13272(%r15)
	movups	13328(%r15), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %edi
	subl	%edi, %eax
	sarl	%edi
	addl	%edx, %edi
	leal	(%rdi,%rsi), %edx
	movl	%edx, 13328(%r15)
	subl	%edi, %esi
	movl	%esi, 13340(%r15)
	leal	(%rax,%rcx), %edx
	movl	%edx, 13332(%r15)
	subl	%eax, %ecx
	movl	%ecx, 13336(%r15)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_7:                                # %.preheader156
                                        # =>This Inner Loop Header: Depth=1
	movl	13136(%r15,%rcx,4), %r14d
	movl	13200(%r15,%rcx,4), %r10d
	movl	13264(%r15,%rcx,4), %r11d
	movl	13328(%r15,%rcx,4), %r9d
	movl	%r14d, %ebp
	subl	%r11d, %ebp
	movl	%r10d, %eax
	sarl	%eax
	subl	%r9d, %eax
	movl	%r9d, %edi
	sarl	%edi
	addl	%r10d, %edi
	movl	15520(%r15), %esi
	leal	32(%r11,%r14), %ebx
	leal	(%rbx,%rdi), %edx
	sarl	$6, %edx
	cmovsl	%r8d, %edx
	cmpl	%esi, %edx
	cmovgl	%esi, %edx
	movl	%edx, 13136(%r15,%rcx,4)
	movl	15520(%r15), %edx
	subl	%edi, %ebx
	sarl	$6, %ebx
	cmovsl	%r8d, %ebx
	cmpl	%edx, %ebx
	cmovgl	%edx, %ebx
	movl	%ebx, 13328(%r15,%rcx,4)
	movl	15520(%r15), %edx
	leal	32(%rbp), %esi
	leal	32(%rbp,%rax), %edi
	sarl	$6, %edi
	cmovsl	%r8d, %edi
	cmpl	%edx, %edi
	cmovgl	%edx, %edi
	movl	%edi, 13200(%r15,%rcx,4)
	movl	15520(%r15), %edx
	subl	%eax, %esi
	sarl	$6, %esi
	cmovsl	%r8d, %esi
	cmpl	%edx, %esi
	cmovgl	%edx, %esi
	movl	%esi, 13264(%r15,%rcx,4)
	incq	%rcx
	cmpq	$4, %rcx
	jne	.LBB7_7
# BB#8:                                 # %.preheader155
	movl	%r14d, -128(%rsp)
	movl	%r10d, -124(%rsp)
	movl	%r11d, -120(%rsp)
	movl	%r9d, -116(%rsp)
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %r8
	movl	176(%r15), %edi
	movl	180(%r15), %r9d
	movq	-56(%rsp), %r10         # 8-byte Reload
	leal	(%r9,%r10), %edx
	movslq	%edx, %rdx
	movq	(%r8,%rdx,8), %rbx
	movzwl	13136(%r15), %esi
	movq	-64(%rsp), %rcx         # 8-byte Reload
	leal	(%rdi,%rcx), %edx
	movslq	%edx, %rdx
	movw	%si, (%rbx,%rdx,2)
	movzwl	13140(%r15), %ebp
	leal	1(%rdi,%rcx), %esi
	movslq	%esi, %rsi
	movw	%bp, (%rbx,%rsi,2)
	movzwl	13144(%r15), %eax
	leal	2(%rdi,%rcx), %ebp
	movslq	%ebp, %rbp
	movw	%ax, (%rbx,%rbp,2)
	movzwl	13148(%r15), %eax
	leal	3(%rdi,%rcx), %edi
	movslq	%edi, %rdi
	movw	%ax, (%rbx,%rdi,2)
	leal	1(%r9,%r10), %eax
	cltq
	movq	(%r8,%rax,8), %rax
	movzwl	13200(%r15), %ebx
	movw	%bx, (%rax,%rdx,2)
	movzwl	13204(%r15), %ebx
	movw	%bx, (%rax,%rsi,2)
	movzwl	13208(%r15), %ebx
	movw	%bx, (%rax,%rbp,2)
	movzwl	13212(%r15), %ebx
	movw	%bx, (%rax,%rdi,2)
	leal	2(%r9,%r10), %eax
	cltq
	movq	(%r8,%rax,8), %rax
	movzwl	13264(%r15), %ebx
	movw	%bx, (%rax,%rdx,2)
	movzwl	13268(%r15), %ebx
	movw	%bx, (%rax,%rsi,2)
	movzwl	13272(%r15), %ebx
	movw	%bx, (%rax,%rbp,2)
	movzwl	13276(%r15), %ebx
	movw	%bx, (%rax,%rdi,2)
	leal	3(%r9,%r10), %eax
	cltq
	movq	(%r8,%rax,8), %rax
	movzwl	13328(%r15), %ecx
	movw	%cx, (%rax,%rdx,2)
	movzwl	13332(%r15), %ecx
	movw	%cx, (%rax,%rsi,2)
	movzwl	13336(%r15), %ecx
	movw	%cx, (%rax,%rbp,2)
	movzwl	13340(%r15), %ecx
	movw	%cx, (%rax,%rdi,2)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	copyblock_sp, .Lfunc_end7-copyblock_sp
	.cfi_endproc

	.globl	writeIPCMBytes
	.p2align	4, 0x90
	.type	writeIPCMBytes,@function
writeIPCMBytes:                         # @writeIPCMBytes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi106:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi107:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi109:
	.cfi_def_cfa_offset 96
.Lcfi110:
	.cfi_offset %rbx, -56
.Lcfi111:
	.cfi_offset %r12, -48
.Lcfi112:
	.cfi_offset %r13, -40
.Lcfi113:
	.cfi_offset %r14, -32
.Lcfi114:
	.cfi_offset %r15, -24
.Lcfi115:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	movq	%rsp, %r15
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB8_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_2 Depth 2
	movq	img(%rip), %rax
	movslq	180(%rax), %rcx
	movslq	%r12d, %r13
	addq	%rcx, %r13
	xorl	%ebp, %ebp
	jmp	.LBB8_2
	.p2align	4, 0x90
.LBB8_3:                                # %._crit_edge51
                                        #   in Loop: Header=BB8_2 Depth=2
	incl	%ebp
	movq	img(%rip), %rax
.LBB8_2:                                #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	15444(%rax), %ecx
	movl	%ecx, 12(%rsp)
	addl	%ecx, %ebx
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movl	176(%rax), %eax
	addl	%ebp, %eax
	cltq
	movzwl	(%rcx,%rax,2), %eax
	movl	%eax, 20(%rsp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	writeSyntaxElement2Buf_Fixed
	cmpl	$15, %ebp
	jne	.LBB8_3
# BB#4:                                 #   in Loop: Header=BB8_1 Depth=1
	incl	%r12d
	cmpl	$16, %r12d
	jne	.LBB8_1
# BB#5:                                 # %.preheader35
	movq	img(%rip), %rax
	cmpl	$0, 15548(%rax)
	jle	.LBB8_17
# BB#6:                                 # %.lr.ph40.preheader
	xorl	%r12d, %r12d
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB8_7:                                # %.lr.ph40
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_9 Depth 2
	cmpl	$0, 15544(%rax)
	jle	.LBB8_10
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB8_7 Depth=1
	movslq	188(%rax), %rcx
	movslq	%r12d, %r13
	addq	%rcx, %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_9:                                #   Parent Loop BB8_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	15448(%rax), %ecx
	movl	%ecx, 12(%rsp)
	addl	%ecx, %ebx
	movq	enc_picture(%rip), %rcx
	movq	6472(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movl	184(%rax), %eax
	addl	%ebp, %eax
	cltq
	movzwl	(%rcx,%rax,2), %eax
	movl	%eax, 20(%rsp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	writeSyntaxElement2Buf_Fixed
	incl	%ebp
	movq	img(%rip), %rax
	cmpl	15544(%rax), %ebp
	jl	.LBB8_9
.LBB8_10:                               # %._crit_edge
                                        #   in Loop: Header=BB8_7 Depth=1
	incl	%r12d
	movl	15548(%rax), %ecx
	cmpl	%ecx, %r12d
	jl	.LBB8_7
# BB#11:                                # %._crit_edge41
	testl	%ecx, %ecx
	jle	.LBB8_17
# BB#12:                                # %.lr.ph40.1.preheader
	xorl	%r12d, %r12d
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB8_13:                               # %.lr.ph40.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_15 Depth 2
	cmpl	$0, 15544(%rax)
	jle	.LBB8_16
# BB#14:                                # %.lr.ph.1
                                        #   in Loop: Header=BB8_13 Depth=1
	movslq	188(%rax), %rcx
	movslq	%r12d, %r13
	addq	%rcx, %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_15:                               #   Parent Loop BB8_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	15448(%rax), %ecx
	movl	%ecx, 12(%rsp)
	addl	%ecx, %ebx
	movq	enc_picture(%rip), %rcx
	movq	6472(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movl	184(%rax), %eax
	addl	%ebp, %eax
	cltq
	movzwl	(%rcx,%rax,2), %eax
	movl	%eax, 20(%rsp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	writeSyntaxElement2Buf_Fixed
	incl	%ebp
	movq	img(%rip), %rax
	cmpl	15544(%rax), %ebp
	jl	.LBB8_15
.LBB8_16:                               # %._crit_edge.1
                                        #   in Loop: Header=BB8_13 Depth=1
	incl	%r12d
	cmpl	15548(%rax), %r12d
	jl	.LBB8_13
.LBB8_17:                               # %._crit_edge41.1
	movl	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	writeIPCMBytes, .Lfunc_end8-writeIPCMBytes
	.cfi_endproc

	.globl	writePCMByteAlign
	.p2align	4, 0x90
	.type	writePCMByteAlign,@function
writePCMByteAlign:                      # @writePCMByteAlign
	.cfi_startproc
# BB#0:
	movslq	4(%rdi), %rdx
	xorl	%eax, %eax
	cmpq	$7, %rdx
	jg	.LBB9_2
# BB#1:
	movl	$8, %eax
	subl	%edx, %eax
	movzbl	8(%rdi), %r8d
	movl	%edx, %ecx
	shll	%cl, %r8d
	movl	$255, %esi
	movl	%eax, %ecx
	shrl	%cl, %esi
	orl	%r8d, %esi
	movb	%sil, 8(%rdi)
	movq	stats(%rip), %r8
	movq	img(%rip), %rcx
	movslq	20(%rcx), %rcx
	addq	%rdx, 1960(%r8,%rcx,8)
	movq	32(%rdi), %r8
	movslq	(%rdi), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rdi)
	movb	%sil, (%r8,%rdx)
	movl	$8, 4(%rdi)
.LBB9_2:
	retq
.Lfunc_end9:
	.size	writePCMByteAlign, .Lfunc_end9-writePCMByteAlign
	.cfi_endproc

	.globl	dct_luma_sp2
	.p2align	4, 0x90
	.type	dct_luma_sp2,@function
dct_luma_sp2:                           # @dct_luma_sp2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi116:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi117:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi118:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi119:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi120:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi121:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi122:
	.cfi_def_cfa_offset 80
.Lcfi123:
	.cfi_offset %rbx, -56
.Lcfi124:
	.cfi_offset %r12, -48
.Lcfi125:
	.cfi_offset %r13, -40
.Lcfi126:
	.cfi_offset %r14, -32
.Lcfi127:
	.cfi_offset %r15, -24
.Lcfi128:
	.cfi_offset %rbp, -16
	movq	%rdx, 8(%rsp)           # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	%edi, %ecx
	shrl	$2, %ecx
	movl	%esi, %eax
	sarl	$3, %eax
	movl	%edi, %edx
	sarl	$3, %edx
	leal	(%rdx,%rax,2), %edx
	movl	%esi, %eax
	shrl	%eax
	andl	$2, %eax
	andl	$1, %ecx
	orl	%eax, %ecx
	movq	img(%rip), %r13
	movq	14160(%r13), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rdx
	movq	(%rdx,%rcx,8), %rcx
	movq	(%rcx), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movq	8(%rcx), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movslq	40(%r13), %r10
	imulq	$715827883, %r10, %rcx  # imm = 0x2AAAAAAB
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$32, %rcx
	leal	(%rcx,%rdx), %eax
	movl	%eax, -36(%rsp)         # 4-byte Spill
	leal	15(%rcx,%rdx), %ecx
	movl	$1, %r15d
	movl	%ecx, -40(%rsp)         # 4-byte Spill
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r15d
	movslq	%edi, %rcx
	movq	%rsi, -24(%rsp)         # 8-byte Spill
	movslq	%esi, %rdx
	leal	1(%rcx), %r11d
	leal	2(%rcx), %r14d
	movl	%edx, %r12d
	shlq	$5, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	leal	3(%rcx), %r8d
	leaq	12630(%r13,%rdx), %rbp
	movq	lrec(%rip), %r9
	leaq	13148(%r13), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB10_1:                               # %.preheader238
                                        # =>This Inner Loop Header: Depth=1
	leal	(%r12,%rsi), %eax
	movl	180(%r13), %ebx
	addl	%eax, %ebx
	movslq	%ebx, %rbx
	movq	(%r9,%rbx,8), %rbx
	movl	176(%r13), %ecx
	addl	%edi, %ecx
	movslq	%ecx, %rcx
	movl	(%rbx,%rcx,4), %ecx
	movl	%ecx, -12(%rdx)
	movzwl	-6(%rbp), %ecx
	movl	%ecx, -112(%rsp,%rsi,4)
	movl	180(%r13), %ecx
	addl	%eax, %ecx
	movslq	%ecx, %rcx
	movq	(%r9,%rcx,8), %rcx
	movl	176(%r13), %ebx
	addl	%r11d, %ebx
	movslq	%ebx, %rbx
	movl	(%rcx,%rbx,4), %ecx
	movl	%ecx, -8(%rdx)
	movzwl	-4(%rbp), %ecx
	movl	%ecx, -96(%rsp,%rsi,4)
	movl	180(%r13), %ecx
	addl	%eax, %ecx
	movslq	%ecx, %rcx
	movq	(%r9,%rcx,8), %rcx
	movl	176(%r13), %ebx
	addl	%r14d, %ebx
	movslq	%ebx, %rbx
	movl	(%rcx,%rbx,4), %ecx
	movl	%ecx, -4(%rdx)
	movzwl	-2(%rbp), %ecx
	movl	%ecx, -80(%rsp,%rsi,4)
	addl	180(%r13), %eax
	movslq	%eax, %rcx
	movq	(%r9,%rcx,8), %rcx
	movl	176(%r13), %eax
	addl	%r8d, %eax
	cltq
	movl	(%rcx,%rax,4), %ecx
	movl	%ecx, (%rdx)
	movzwl	(%rbp), %ecx
	movl	%ecx, -64(%rsp,%rsi,4)
	incq	%rsi
	addq	$64, %rdx
	addq	$32, %rbp
	cmpq	$4, %rsi
	jne	.LBB10_1
# BB#2:                                 # %.preheader237
	movq	%rdi, -16(%rsp)         # 8-byte Spill
	movslq	%r10d, %r11
	imulq	$715827883, %r11, %rax  # imm = 0x2AAAAAAB
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %r11d
	movl	%r15d, %eax
	shrl	$31, %eax
	addl	%r15d, %eax
	sarl	%eax
	movl	%eax, -44(%rsp)         # 4-byte Spill
	movl	-64(%rsp), %eax
	movl	-112(%rsp), %ecx
	movl	-96(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	-80(%rsp), %eax
	leal	(%rax,%rdx), %ebp
	subl	%eax, %edx
	leal	(%rbp,%rsi), %edi
	subl	%ebp, %esi
	movl	%esi, -80(%rsp)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -96(%rsp)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -64(%rsp)
	movl	-60(%rsp), %eax
	movl	-108(%rsp), %ecx
	movl	-92(%rsp), %ebp
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	-76(%rsp), %eax
	leal	(%rax,%rbp), %ebx
	subl	%eax, %ebp
	leal	(%rbx,%rsi), %edx
	subl	%ebx, %esi
	movl	%esi, -76(%rsp)
	leal	(%rbp,%rcx,2), %esi
	addl	%ebp, %ebp
	subl	%ebp, %ecx
	movl	%ecx, -60(%rsp)
	movl	-56(%rsp), %eax
	movl	-104(%rsp), %r9d
	movl	-100(%rsp), %r12d
	leal	(%rax,%r9), %r10d
	subl	%eax, %r9d
	movl	-88(%rsp), %eax
	movl	-72(%rsp), %ebx
	leal	(%rbx,%rax), %ecx
	subl	%ebx, %eax
	leal	(%rcx,%r10), %r14d
	subl	%ecx, %r10d
	leal	(%rax,%r9,2), %r8d
	addl	%eax, %eax
	subl	%eax, %r9d
	movl	-52(%rsp), %ecx
	leal	(%rcx,%r12), %eax
	subl	%ecx, %r12d
	movl	-84(%rsp), %ecx
	movl	-68(%rsp), %ebx
	leal	(%rbx,%rcx), %ebp
	subl	%ebx, %ecx
	leal	(%rbp,%rax), %ebx
	subl	%ebp, %eax
	leal	(%rcx,%r12,2), %r15d
	addl	%ecx, %ecx
	subl	%ecx, %r12d
	leal	(%rbx,%rdi), %ecx
	subl	%ebx, %edi
	leal	(%r14,%rdx), %ebx
	subl	%r14d, %edx
	leal	(%rbx,%rcx), %ebp
	movl	%ebp, -112(%rsp)
	subl	%ebx, %ecx
	movl	%ecx, -104(%rsp)
	leal	(%rdx,%rdi,2), %ecx
	movl	%ecx, -108(%rsp)
	addl	%edx, %edx
	subl	%edx, %edi
	movl	%edi, -100(%rsp)
	movl	-96(%rsp), %ecx
	leal	(%r15,%rcx), %edx
	subl	%r15d, %ecx
	leal	(%r8,%rsi), %edi
	subl	%r8d, %esi
	leal	(%rdi,%rdx), %ebp
	movl	%ebp, -96(%rsp)
	subl	%edi, %edx
	movl	%edx, -88(%rsp)
	leal	(%rsi,%rcx,2), %edx
	movl	%edx, -92(%rsp)
	addl	%esi, %esi
	subl	%esi, %ecx
	movl	%ecx, -84(%rsp)
	movl	-80(%rsp), %ecx
	movl	-76(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	leal	(%r10,%rdx), %eax
	subl	%r10d, %edx
	leal	(%rax,%rsi), %edi
	movl	%edi, -80(%rsp)
	subl	%eax, %esi
	movl	%esi, -72(%rsp)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -76(%rsp)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -68(%rsp)
	movl	-64(%rsp), %eax
	leal	(%r12,%rax), %ecx
	movl	%ecx, -128(%rsp)
	subl	%r12d, %eax
	movl	%eax, -116(%rsp)
	movl	-60(%rsp), %edx
	leal	(%r9,%rdx), %esi
	movl	%esi, -124(%rsp)
	subl	%r9d, %edx
	movl	%edx, -120(%rsp)
	leal	(%rsi,%rcx), %edi
	movl	%edi, -64(%rsp)
	subl	%esi, %ecx
	movl	%ecx, -56(%rsp)
	leal	(%rdx,%rax,2), %ecx
	movl	%ecx, -60(%rsp)
	addl	%edx, %edx
	subl	%edx, %eax
	movl	%eax, -52(%rsp)
	movq	img(%rip), %r14
	movslq	%r11d, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$-1, %r13d
	xorl	%ebp, %ebp
	movq	input(%rip), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_3:                               # =>This Inner Loop Header: Depth=1
	cmpl	$0, 15312(%r14)
	je	.LBB10_5
# BB#4:                                 #   in Loop: Header=BB10_3 Depth=1
	movl	$FIELD_SCAN, %eax
	jmp	.LBB10_9
	.p2align	4, 0x90
.LBB10_5:                               #   in Loop: Header=BB10_3 Depth=1
	cmpl	$0, mb_adaptive(%rip)
	je	.LBB10_8
# BB#6:                                 #   in Loop: Header=BB10_3 Depth=1
	cmpl	$0, 14464(%r14)
	je	.LBB10_8
# BB#7:                                 #   in Loop: Header=BB10_3 Depth=1
	movl	$FIELD_SCAN, %eax
	jmp	.LBB10_9
	.p2align	4, 0x90
.LBB10_8:                               #   in Loop: Header=BB10_3 Depth=1
	movl	$SNGL_SCAN, %eax
.LBB10_9:                               #   in Loop: Header=BB10_3 Depth=1
	incl	%r13d
	movzbl	(%rax,%rbp,2), %edi
	movzbl	1(%rax,%rbp,2), %esi
	movq	%rdi, %rcx
	shlq	$4, %rcx
	leaq	-112(%rsp,%rcx), %r8
	movl	(%r8,%rsi,4), %ebx
	movl	%ebx, %eax
	negl	%eax
	cmovll	%ebx, %eax
	movq	16(%rsp), %r15          # 8-byte Reload
	shlq	$6, %r15
	addq	%rcx, %r15
	imull	quant_coef(%r15,%rsi,4), %eax
	addl	-44(%rsp), %eax         # 4-byte Folded Reload
	movl	-40(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	movl	%eax, %r10d
	negl	%r10d
	cmovll	%eax, %r10d
	movq	%rsi, %rax
	shlq	$6, %rax
	addq	%r14, %rax
	movl	%r10d, %ecx
	negl	%ecx
	testl	%ebx, %ebx
	leaq	13136(%rax,%rdi,4), %r12
	movl	13136(%rax,%rdi,4), %edi
	movl	%r10d, %eax
	cmovsl	%ecx, %eax
	subl	%eax, %edi
	movl	%edi, %eax
	sarl	$31, %eax
	leal	(%rdi,%rax), %r9d
	xorl	%eax, %r9d
	je	.LBB10_13
# BB#10:                                #   in Loop: Header=BB10_3 Depth=1
	leaq	(%r8,%rsi,4), %rax
	movl	$999999, %ebx           # imm = 0xF423F
	cmpl	$1, %r9d
	jg	.LBB10_12
# BB#11:                                #   in Loop: Header=BB10_3 Depth=1
	movq	-8(%rsp), %rdx          # 8-byte Reload
	movslq	4180(%rdx), %rbx
	movslq	%r13d, %rdx
	shlq	$4, %rbx
	movzbl	COEFF_COST(%rbx,%rdx), %ebx
.LBB10_12:                              #   in Loop: Header=BB10_3 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	addl	%ebx, (%rdx)
	negl	%r9d
	testl	%edi, %edi
	cmovnsl	%edi, %r9d
	movslq	%r11d, %r11
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movl	%r9d, (%rdx,%r11,4)
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	%r13d, (%rdx,%r11,4)
	incl	%r11d
	movl	(%rax), %ebx
	movl	$-1, %r13d
	movl	$1, %edx
.LBB10_13:                              #   in Loop: Header=BB10_3 Depth=1
	testl	%ebx, %ebx
	cmovsl	%ecx, %r10d
	addl	%edi, %r10d
	imull	dequant_coef(%r15,%rsi,4), %r10d
	movl	-36(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r10d
	movl	%r10d, (%r12)
	incq	%rbp
	cmpq	$16, %rbp
	jne	.LBB10_3
# BB#14:                                # %.preheader232
	movl	%edx, %r12d
	movslq	%r11d, %rax
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movl	$0, (%rcx,%rax,4)
	movups	13136(%r14), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %edi
	subl	%edi, %eax
	sarl	%edi
	addl	%edx, %edi
	leal	(%rdi,%rsi), %edx
	movl	%edx, 13136(%r14)
	subl	%edi, %esi
	movl	%esi, 13148(%r14)
	leal	(%rax,%rcx), %edx
	movl	%edx, 13140(%r14)
	subl	%eax, %ecx
	movl	%ecx, 13144(%r14)
	movups	13200(%r14), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %edi
	subl	%edi, %eax
	sarl	%edi
	addl	%edx, %edi
	leal	(%rdi,%rsi), %edx
	movl	%edx, 13200(%r14)
	subl	%edi, %esi
	movl	%esi, 13212(%r14)
	leal	(%rax,%rcx), %edx
	movl	%edx, 13204(%r14)
	subl	%eax, %ecx
	movl	%ecx, 13208(%r14)
	movups	13264(%r14), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %edi
	subl	%edi, %eax
	sarl	%edi
	addl	%edx, %edi
	leal	(%rdi,%rsi), %edx
	movl	%edx, 13264(%r14)
	subl	%edi, %esi
	movl	%esi, 13276(%r14)
	leal	(%rax,%rcx), %edx
	movl	%edx, 13268(%r14)
	subl	%eax, %ecx
	movl	%ecx, 13272(%r14)
	movups	13328(%r14), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %edi
	subl	%edi, %eax
	sarl	%edi
	addl	%edx, %edi
	leal	(%rdi,%rsi), %edx
	movl	%edx, 13328(%r14)
	subl	%edi, %esi
	movl	%esi, 13340(%r14)
	leal	(%rax,%rcx), %edx
	movl	%edx, 13332(%r14)
	subl	%eax, %ecx
	movl	%ecx, 13336(%r14)
	xorl	%r8d, %r8d
	movl	$255, %r9d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB10_15:                              # %.preheader230
                                        # =>This Inner Loop Header: Depth=1
	movl	13136(%r14,%rsi,4), %ebx
	movl	13200(%r14,%rsi,4), %r11d
	movl	13264(%r14,%rsi,4), %r10d
	movl	13328(%r14,%rsi,4), %r15d
	movl	%ebx, %ecx
	subl	%r10d, %ecx
	movl	%r11d, %eax
	sarl	%eax
	subl	%r15d, %eax
	movl	%r15d, %edx
	sarl	%edx
	addl	%r11d, %edx
	leal	32(%r10,%rbx), %edi
	leal	(%rdi,%rdx), %ebp
	sarl	$6, %ebp
	cmovsl	%r8d, %ebp
	cmpl	$256, %ebp              # imm = 0x100
	cmovgel	%r9d, %ebp
	movl	%ebp, 13136(%r14,%rsi,4)
	subl	%edx, %edi
	sarl	$6, %edi
	cmovsl	%r8d, %edi
	cmpl	$256, %edi              # imm = 0x100
	cmovgel	%r9d, %edi
	movl	%edi, 13328(%r14,%rsi,4)
	leal	32(%rcx), %edx
	leal	32(%rcx,%rax), %ecx
	sarl	$6, %ecx
	cmovsl	%r8d, %ecx
	cmpl	$256, %ecx              # imm = 0x100
	cmovgel	%r9d, %ecx
	movl	%ecx, 13200(%r14,%rsi,4)
	subl	%eax, %edx
	sarl	$6, %edx
	cmovsl	%r8d, %edx
	cmpl	$256, %edx              # imm = 0x100
	cmovgel	%r9d, %edx
	movl	%edx, 13264(%r14,%rsi,4)
	incq	%rsi
	cmpq	$4, %rsi
	jne	.LBB10_15
# BB#16:                                # %.preheader229
	movl	%ebx, -128(%rsp)
	movl	%r11d, -124(%rsp)
	movl	%r10d, -120(%rsp)
	movl	%r15d, -116(%rsp)
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rdi
	movl	176(%r14), %r8d
	movl	180(%r14), %ebp
	movq	-16(%rsp), %r9          # 8-byte Reload
	leal	(%r8,%r9), %ecx
	movslq	%ecx, %rbx
	movzwl	13136(%r14), %edx
	movq	-24(%rsp), %rax         # 8-byte Reload
	leal	(%rbp,%rax), %ecx
	movslq	%ecx, %rcx
	movq	(%rdi,%rcx,8), %rcx
	movw	%dx, (%rcx,%rbx,2)
	movzwl	13200(%r14), %esi
	leal	1(%rbp,%rax), %edx
	movslq	%edx, %rdx
	movq	(%rdi,%rdx,8), %rdx
	movw	%si, (%rdx,%rbx,2)
	movzwl	13264(%r14), %r10d
	leal	2(%rbp,%rax), %esi
	movslq	%esi, %rsi
	movq	(%rdi,%rsi,8), %rsi
	movw	%r10w, (%rsi,%rbx,2)
	movzwl	13328(%r14), %r10d
	leal	3(%rbp,%rax), %ebp
	movslq	%ebp, %rbp
	movq	(%rdi,%rbp,8), %rdi
	movw	%r10w, (%rdi,%rbx,2)
	leal	1(%r8,%r9), %eax
	cltq
	movzwl	13140(%r14), %ebp
	movw	%bp, (%rcx,%rax,2)
	movzwl	13204(%r14), %ebp
	movw	%bp, (%rdx,%rax,2)
	movzwl	13268(%r14), %ebp
	movw	%bp, (%rsi,%rax,2)
	movzwl	13332(%r14), %ebp
	movw	%bp, (%rdi,%rax,2)
	leal	2(%r8,%r9), %eax
	cltq
	movzwl	13144(%r14), %ebp
	movw	%bp, (%rcx,%rax,2)
	movzwl	13208(%r14), %ebp
	movw	%bp, (%rdx,%rax,2)
	movzwl	13272(%r14), %ebp
	movw	%bp, (%rsi,%rax,2)
	movzwl	13336(%r14), %ebp
	movw	%bp, (%rdi,%rax,2)
	leal	3(%r8,%r9), %eax
	cltq
	movzwl	13148(%r14), %ebp
	movw	%bp, (%rcx,%rax,2)
	movzwl	13212(%r14), %ecx
	movw	%cx, (%rdx,%rax,2)
	movzwl	13276(%r14), %ecx
	movw	%cx, (%rsi,%rax,2)
	movzwl	13340(%r14), %ecx
	movw	%cx, (%rdi,%rax,2)
	movl	%r12d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	dct_luma_sp2, .Lfunc_end10-dct_luma_sp2
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.long	255                     # 0xff
	.long	255                     # 0xff
	.long	255                     # 0xff
	.long	255                     # 0xff
	.text
	.globl	dct_chroma_sp2
	.p2align	4, 0x90
	.type	dct_chroma_sp2,@function
dct_chroma_sp2:                         # @dct_chroma_sp2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi132:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi133:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi135:
	.cfi_def_cfa_offset 352
.Lcfi136:
	.cfi_offset %rbx, -56
.Lcfi137:
	.cfi_offset %r12, -48
.Lcfi138:
	.cfi_offset %r13, -40
.Lcfi139:
	.cfi_offset %r14, -32
.Lcfi140:
	.cfi_offset %r15, -24
.Lcfi141:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	img(%rip), %rax
	movq	14168(%rax), %rcx
	movq	14224(%rax), %rdx
	movq	%rdx, -104(%rsp)        # 8-byte Spill
	movslq	12(%rax), %rdx
	movq	%rdx, -88(%rsp)         # 8-byte Spill
	movq	%rdi, %rdx
	movq	%rdx, -48(%rsp)         # 8-byte Spill
	movslq	%edi, %rdx
	movq	8(%rcx,%rdx,8), %rcx
	movq	(%rcx), %rdi
	movq	%rdi, -80(%rsp)         # 8-byte Spill
	movq	8(%rcx), %r15
	movslq	40(%rax), %r9
	testq	%r9, %r9
	js	.LBB11_1
# BB#2:
	movzbl	QP_SCALE_CR(%r9), %r9d
	imull	$171, %r9d, %edi
	andl	$64512, %edi            # imm = 0xFC00
	shrl	$10, %edi
	jmp	.LBB11_3
.LBB11_1:                               # %.thread
	movslq	%r9d, %rcx
	imulq	$715827883, %rcx, %rdi  # imm = 0x2AAAAAAB
	movq	%rdi, %rcx
	shrq	$63, %rcx
	shrq	$32, %rdi
	addl	%ecx, %edi
.LBB11_3:
	movl	%esi, %r12d
	movq	%rdi, -16(%rsp)         # 8-byte Spill
	leal	15(%rdi), %ecx
	movl	$1, %esi
	movl	%ecx, 4(%rsp)           # 4-byte Spill
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movq	lrec_uv(%rip), %rcx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	(%rcx,%rdx,8), %rcx
	xorl	%edi, %edi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_4:                               # %.preheader378
                                        # =>This Inner Loop Header: Depth=1
	movzwl	12624(%rax,%rdi), %ebx
	movl	%ebx, 32(%rsp,%rbp,4)
	movl	188(%rax), %ebx
	addl	%ebp, %ebx
	movslq	%ebx, %rbx
	movq	(%rcx,%rbx,8), %rbx
	movslq	184(%rax), %rdx
	movl	(%rbx,%rdx,4), %edx
	movl	%edx, 13136(%rax,%rdi,2)
	movzwl	12626(%rax,%rdi), %edx
	movl	%edx, 64(%rsp,%rbp,4)
	movl	188(%rax), %edx
	addl	%ebp, %edx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rdx
	movslq	184(%rax), %rbx
	movl	4(%rdx,%rbx,4), %edx
	movl	%edx, 13140(%rax,%rdi,2)
	movzwl	12628(%rax,%rdi), %edx
	movl	%edx, 96(%rsp,%rbp,4)
	movl	188(%rax), %edx
	addl	%ebp, %edx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rdx
	movslq	184(%rax), %rbx
	movl	8(%rdx,%rbx,4), %edx
	movl	%edx, 13144(%rax,%rdi,2)
	movzwl	12630(%rax,%rdi), %edx
	movl	%edx, 128(%rsp,%rbp,4)
	movl	188(%rax), %edx
	addl	%ebp, %edx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rdx
	movslq	184(%rax), %rbx
	movl	12(%rdx,%rbx,4), %edx
	movl	%edx, 13148(%rax,%rdi,2)
	movzwl	12632(%rax,%rdi), %edx
	movl	%edx, 160(%rsp,%rbp,4)
	movl	188(%rax), %edx
	addl	%ebp, %edx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rdx
	movslq	184(%rax), %rbx
	movl	16(%rdx,%rbx,4), %edx
	movl	%edx, 13152(%rax,%rdi,2)
	movzwl	12634(%rax,%rdi), %edx
	movl	%edx, 192(%rsp,%rbp,4)
	movl	188(%rax), %edx
	addl	%ebp, %edx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rdx
	movslq	184(%rax), %rbx
	movl	20(%rdx,%rbx,4), %edx
	movl	%edx, 13156(%rax,%rdi,2)
	movzwl	12636(%rax,%rdi), %edx
	movl	%edx, 224(%rsp,%rbp,4)
	movl	188(%rax), %edx
	addl	%ebp, %edx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rdx
	movslq	184(%rax), %rbx
	movl	24(%rdx,%rbx,4), %edx
	movl	%edx, 13160(%rax,%rdi,2)
	movzwl	12638(%rax,%rdi), %edx
	movl	%edx, 256(%rsp,%rbp,4)
	movl	188(%rax), %edx
	addl	%ebp, %edx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rdx
	movslq	184(%rax), %rbx
	movl	28(%rdx,%rbx,4), %edx
	movl	%edx, 13164(%rax,%rdi,2)
	incq	%rbp
	addq	$32, %rdi
	cmpq	$8, %rbp
	jne	.LBB11_4
# BB#5:                                 # %.preheader377
	movslq	%r9d, %r10
	imulq	$715827883, %r10, %rax  # imm = 0x2AAAAAAB
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %r10d
	movl	%esi, %eax
	shrl	$31, %eax
	addl	%esi, %eax
	sarl	%eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	140(%rsp), %r11
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB11_6:                               # %.preheader376
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_7 Depth 2
	movq	%r11, %rsi
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB11_7:                               # %.preheader374
                                        #   Parent Loop BB11_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rsi), %ecx
	movl	-108(%rsi), %edx
	movl	-76(%rsi), %edi
	leal	(%rcx,%rdx), %ebx
	subl	%ecx, %edx
	movl	-44(%rsi), %ecx
	leal	(%rcx,%rdi), %eax
	subl	%ecx, %edi
	leal	(%rax,%rbx), %ecx
	movl	%ecx, -108(%rsi)
	subl	%eax, %ebx
	movl	%ebx, -44(%rsi)
	leal	(%rdi,%rdx,2), %eax
	movl	%eax, -76(%rsi)
	addl	%edi, %edi
	subl	%edi, %edx
	movl	%edx, -12(%rsi)
	movl	-8(%rsi), %eax
	movl	-104(%rsi), %ecx
	movl	-72(%rsi), %edx
	leal	(%rax,%rcx), %edi
	subl	%eax, %ecx
	movl	-40(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rdi), %eax
	movl	%eax, -104(%rsi)
	subl	%ebx, %edi
	movl	%edi, -40(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -72(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -8(%rsi)
	movl	-4(%rsi), %eax
	movl	-100(%rsi), %ecx
	movl	-68(%rsi), %edx
	leal	(%rax,%rcx), %edi
	subl	%eax, %ecx
	movl	-36(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rdi), %eax
	movl	%eax, -100(%rsi)
	subl	%ebx, %edi
	movl	%edi, -36(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -68(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -4(%rsi)
	movl	(%rsi), %eax
	movl	-96(%rsi), %ecx
	movl	-64(%rsi), %edx
	leal	(%rax,%rcx), %edi
	subl	%eax, %ecx
	movl	-32(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rdi), %eax
	movl	%eax, -96(%rsi)
	subl	%ebx, %edi
	movl	%edi, -32(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -64(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, (%rsi)
	movl	-96(%rsi), %eax
	movl	-108(%rsi), %ecx
	movl	-104(%rsi), %edx
	leal	(%rax,%rcx), %edi
	subl	%eax, %ecx
	movl	-100(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rdi), %eax
	movl	%eax, -108(%rsi)
	subl	%ebx, %edi
	movl	%edi, -100(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -104(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -96(%rsi)
	movl	-64(%rsi), %eax
	movl	-76(%rsi), %ecx
	movl	-72(%rsi), %edx
	leal	(%rax,%rcx), %edi
	subl	%eax, %ecx
	movl	-68(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rdi), %eax
	movl	%eax, -76(%rsi)
	subl	%ebx, %edi
	movl	%edi, -68(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -72(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -64(%rsi)
	movl	-32(%rsi), %eax
	movl	-44(%rsi), %ecx
	movl	-40(%rsi), %edx
	leal	(%rax,%rcx), %edi
	subl	%eax, %ecx
	movl	-36(%rsi), %eax
	leal	(%rax,%rdx), %ebx
	subl	%eax, %edx
	leal	(%rbx,%rdi), %eax
	movl	%eax, -44(%rsi)
	subl	%ebx, %edi
	movl	%edi, -36(%rsi)
	leal	(%rdx,%rcx,2), %eax
	movl	%eax, -40(%rsi)
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, -32(%rsi)
	movl	(%rsi), %eax
	movl	-12(%rsi), %ebx
	movl	-8(%rsi), %edi
	leal	(%rax,%rbx), %ecx
	subl	%eax, %ebx
	movl	-4(%rsi), %eax
	leal	(%rax,%rdi), %edx
	subl	%eax, %edi
	leal	(%rdx,%rcx), %eax
	movl	%eax, -12(%rsi)
	movl	%ecx, %eax
	subl	%edx, %eax
	movl	%eax, -4(%rsi)
	leal	(%rdi,%rbx,2), %eax
	movl	%eax, -8(%rsi)
	leal	(%rdi,%rdi), %eax
	movl	%ebx, %ebp
	subl	%eax, %ebp
	movl	%ebp, (%rsi)
	addq	$4, %r14
	subq	$-128, %rsi
	cmpq	$5, %r14
	jl	.LBB11_7
# BB#8:                                 #   in Loop: Header=BB11_6 Depth=1
	addq	$4, %r9
	addq	$16, %r11
	cmpq	$5, %r9
	jl	.LBB11_6
# BB#9:
	movq	%r15, -64(%rsp)         # 8-byte Spill
	movl	%ecx, -128(%rsp)
	movl	%ebx, -116(%rsp)
	movl	%edx, -124(%rsp)
	movl	%edi, -120(%rsp)
	movq	img(%rip), %r15
	movl	160(%rsp), %ecx
	movl	32(%rsp), %r14d
	movl	48(%rsp), %r11d
	leal	(%rcx,%r14), %r9d
	leal	(%r9,%r11), %edx
	movl	176(%rsp), %esi
	addl	%esi, %edx
	movl	%edx, %eax
	negl	%eax
	cmovll	%edx, %eax
	subl	%ecx, %r14d
	leal	(%r14,%r11), %r13d
	movl	%esi, -92(%rsp)         # 4-byte Spill
	subl	%esi, %r13d
	subl	%r11d, %r9d
	movslq	%r10d, %r8
	shlq	$6, %r8
	movl	quant_coef(%r8), %ebx
	movq	-16(%rsp), %rcx         # 8-byte Reload
	leal	16(%rcx), %ebp
	movq	-48(%rsp), %rcx         # 8-byte Reload
	leal	(,%rcx,4), %ecx
	movl	$983040, %esi           # imm = 0xF0000
	movl	%ecx, -28(%rsp)         # 4-byte Spill
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	imulq	$536, -88(%rsp), %rcx   # 8-byte Folded Reload
                                        # imm = 0x218
	movq	-104(%rsp), %rdi        # 8-byte Reload
	leaq	368(%rdi,%rcx), %r10
	movl	%ebx, -112(%rsp)        # 4-byte Spill
	imull	%ebx, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	leal	(%rax,%rdi,2), %eax
	movl	%ebp, -72(%rsp)         # 4-byte Spill
	movl	%ebp, %ecx
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %ebx
	negl	%ebx
	testl	%edx, %edx
	movl	13136(%r15), %ebp
	cmovnsl	%ecx, %ebx
	movl	%ebp, %eax
	subl	%ebx, %eax
	movq	%r15, %rbx
	movl	%eax, %edx
	sarl	$31, %edx
	leal	(%rax,%rdx), %ecx
	xorl	%edx, %ecx
	movslq	%esi, %rsi
	movl	13152(%rbx), %r15d
	movl	13392(%rbx), %edx
	movq	%rdx, -88(%rsp)         # 8-byte Spill
	movq	%rbx, -8(%rsp)          # 8-byte Spill
	movl	13408(%rbx), %edx
	movl	%edx, -108(%rsp)        # 4-byte Spill
	leal	(%rdi,%rdi), %edx
	movl	%edx, -40(%rsp)         # 4-byte Spill
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movl	dequant_coef(%r8), %edx
	movl	%edx, -104(%rsp)        # 4-byte Spill
	movq	%r10, -24(%rsp)         # 8-byte Spill
	movq	%rsi, -56(%rsp)         # 8-byte Spill
	je	.LBB11_10
# BB#11:
	orq	%rsi, (%r10)
	testl	%r12d, %r12d
	movl	$1, %r10d
	cmovlel	%r10d, %r12d
	negl	%ecx
	testl	%eax, %eax
	cmovnsl	%eax, %ecx
	movq	-80(%rsp), %rax         # 8-byte Reload
	movl	%ecx, (%rax)
	movq	-64(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
	xorl	%r8d, %r8d
	jmp	.LBB11_12
.LBB11_10:
	xorl	%r10d, %r10d
	movl	$1, %r8d
.LBB11_12:
	subl	%r11d, %r14d
	subl	-92(%rsp), %r9d         # 4-byte Folded Reload
	imull	-104(%rsp), %ebp        # 4-byte Folded Reload
	movq	-16(%rsp), %r11         # 8-byte Reload
	movl	%r11d, %ecx
	shll	%cl, %ebp
	movl	%r13d, %eax
	negl	%eax
	cmovll	%r13d, %eax
	imull	-112(%rsp), %eax        # 4-byte Folded Reload
	addl	-40(%rsp), %eax         # 4-byte Folded Reload
	movl	-72(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%r13d, %r13d
	cmovnsl	%ecx, %edx
	movq	-88(%rsp), %rdi         # 8-byte Reload
	movl	%edi, %eax
	subl	%edx, %eax
	movl	%eax, %edx
	sarl	$31, %edx
	leal	(%rax,%rdx), %ecx
	xorl	%edx, %ecx
	je	.LBB11_13
# BB#33:
	movq	-24(%rsp), %rdx         # 8-byte Reload
	movq	-56(%rsp), %rsi         # 8-byte Reload
	orq	%rsi, (%rdx)
	testl	%r12d, %r12d
	movl	$1, %edx
	cmovlel	%edx, %r12d
	negl	%ecx
	testl	%eax, %eax
	cmovnsl	%eax, %ecx
	movq	-80(%rsp), %rax         # 8-byte Reload
	movl	%ecx, (%rax,%r10,4)
	movq	-64(%rsp), %r13         # 8-byte Reload
	movl	%r8d, (%r13,%r10,4)
	incl	%r10d
	movl	$-1, %r8d
	jmp	.LBB11_34
.LBB11_13:
	movq	-64(%rsp), %r13         # 8-byte Reload
.LBB11_34:
	addl	-92(%rsp), %r14d        # 4-byte Folded Reload
	imull	-104(%rsp), %edi        # 4-byte Folded Reload
	movl	%r11d, %ecx
	shll	%cl, %edi
	movq	%rdi, -88(%rsp)         # 8-byte Spill
	incl	%r8d
	movl	%r9d, %eax
	negl	%eax
	cmovll	%r9d, %eax
	imull	-112(%rsp), %eax        # 4-byte Folded Reload
	addl	-40(%rsp), %eax         # 4-byte Folded Reload
	movl	-72(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%r9d, %r9d
	cmovnsl	%ecx, %edx
	movl	%r15d, %eax
	subl	%edx, %eax
	movl	%eax, %edx
	sarl	$31, %edx
	leal	(%rax,%rdx), %ecx
	xorl	%edx, %ecx
	je	.LBB11_35
# BB#36:
	movq	-24(%rsp), %rdx         # 8-byte Reload
	movq	-56(%rsp), %rsi         # 8-byte Reload
	orq	%rsi, (%rdx)
	testl	%r12d, %r12d
	movl	$1, %edx
	cmovlel	%edx, %r12d
	negl	%ecx
	testl	%eax, %eax
	cmovnsl	%eax, %ecx
	movslq	%r10d, %r10
	movq	-80(%rsp), %r9          # 8-byte Reload
	movl	%ecx, (%r9,%r10,4)
	movl	%r8d, (%r13,%r10,4)
	incl	%r10d
	movl	$-1, %r8d
	movl	-108(%rsp), %edi        # 4-byte Reload
	jmp	.LBB11_37
.LBB11_35:
	movl	-108(%rsp), %edi        # 4-byte Reload
	movq	-80(%rsp), %r9          # 8-byte Reload
.LBB11_37:
	imull	-104(%rsp), %r15d       # 4-byte Folded Reload
	movl	%r11d, %ecx
	shll	%cl, %r15d
	movl	%r14d, %eax
	negl	%eax
	cmovll	%r14d, %eax
	imull	-112(%rsp), %eax        # 4-byte Folded Reload
	addl	-40(%rsp), %eax         # 4-byte Folded Reload
	movl	-72(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%r14d, %r14d
	cmovnsl	%ecx, %edx
	movl	%edi, %eax
	subl	%edx, %eax
	movl	%eax, %edx
	sarl	$31, %edx
	leal	(%rax,%rdx), %ecx
	xorl	%edx, %ecx
	je	.LBB11_38
# BB#39:
	incl	%r8d
	movq	-24(%rsp), %rdx         # 8-byte Reload
	movq	-56(%rsp), %rsi         # 8-byte Reload
	orq	%rsi, (%rdx)
	testl	%r12d, %r12d
	movl	$1, %edx
	cmovlel	%edx, %r12d
	movl	%r12d, -108(%rsp)       # 4-byte Spill
	negl	%ecx
	testl	%eax, %eax
	cmovnsl	%eax, %ecx
	movslq	%r10d, %r10
	movl	%ecx, (%r9,%r10,4)
	movl	%r8d, (%r13,%r10,4)
	incl	%r10d
	jmp	.LBB11_40
.LBB11_38:
	movl	%r12d, -108(%rsp)       # 4-byte Spill
.LBB11_40:
	imull	-104(%rsp), %edi        # 4-byte Folded Reload
	movl	%r11d, %ecx
	shll	%cl, %edi
	movslq	%r10d, %rax
	movl	$0, (%r9,%rax,4)
	movq	-88(%rsp), %rbx         # 8-byte Reload
	leal	(%rbx,%rbp), %eax
	leal	(%rax,%r15), %ecx
	addl	%edi, %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movq	-8(%rsp), %r11          # 8-byte Reload
	movl	%edx, 13136(%r11)
	subl	%ebx, %ebp
	leal	(%rbp,%r15), %ecx
	subl	%edi, %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movl	%edx, 13392(%r11)
	subl	%r15d, %eax
	subl	%edi, %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movl	%ecx, 13152(%r11)
	subl	%r15d, %ebp
	addl	%edi, %ebp
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	sarl	%eax
	movl	%eax, 13408(%r11)
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	$4, %eax
	cltq
	movq	%rax, -64(%rsp)         # 8-byte Spill
	addl	$16, -28(%rsp)          # 4-byte Folded Spill
	xorl	%edi, %edi
	movl	$0, -48(%rsp)           # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB11_14:                              # %.preheader373
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_15 Depth 2
                                        #       Child Loop BB11_16 Depth 3
	movl	%edi, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%edi, %eax
	sarl	$2, %eax
	addl	%eax, %eax
	movl	%eax, -92(%rsp)         # 4-byte Spill
	movl	%edi, %eax
	sarl	%eax
	addl	-28(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, -56(%rsp)         # 4-byte Spill
	xorl	%ebp, %ebp
	movl	%edi, -112(%rsp)        # 4-byte Spill
	.p2align	4, 0x90
.LBB11_15:                              #   Parent Loop BB11_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_16 Depth 3
	movl	%ebp, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%ebp, %eax
	sarl	$2, %eax
	addl	-92(%rsp), %eax         # 4-byte Folded Reload
	movq	14160(%r11), %rcx
	movq	-64(%rsp), %rdx         # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movq	(%rax), %rcx
	movq	%rcx, -72(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movl	%ebp, %ecx
	shrl	$2, %ecx
	addl	-56(%rsp), %ecx         # 4-byte Folded Reload
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	movl	$-1, %ebx
	xorl	%r14d, %r14d
	xorl	%r10d, %r10d
	movl	%ebp, -88(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB11_16:                              #   Parent Loop BB11_14 Depth=1
                                        #     Parent Loop BB11_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$0, 15312(%r11)
	je	.LBB11_18
# BB#17:                                #   in Loop: Header=BB11_16 Depth=3
	movl	$FIELD_SCAN, %eax
	jmp	.LBB11_22
	.p2align	4, 0x90
.LBB11_18:                              #   in Loop: Header=BB11_16 Depth=3
	cmpl	$0, mb_adaptive(%rip)
	je	.LBB11_21
# BB#19:                                #   in Loop: Header=BB11_16 Depth=3
	cmpl	$0, 14464(%r11)
	je	.LBB11_21
# BB#20:                                #   in Loop: Header=BB11_16 Depth=3
	movl	$FIELD_SCAN, %eax
	jmp	.LBB11_22
	.p2align	4, 0x90
.LBB11_21:                              #   in Loop: Header=BB11_16 Depth=3
	movl	$SNGL_SCAN, %eax
.LBB11_22:                              #   in Loop: Header=BB11_16 Depth=3
	movzbl	3(%rax,%r14,2), %esi
	movzbl	2(%rax,%r14,2), %r15d
	incl	%ebx
	movl	%ebx, -104(%rsp)        # 4-byte Spill
	movslq	%ebp, %rdx
	leaq	(%r15,%rdx), %rcx
	movslq	%edi, %rdi
	leaq	(%rsi,%rdi), %rax
	shlq	$5, %rcx
	leaq	32(%rsp,%rcx), %rbp
	movl	(%rbp,%rax,4), %r9d
	movl	%r9d, %ebx
	negl	%ebx
	cmovll	%r9d, %ebx
	movq	%rsi, %r12
	shlq	$4, %r12
	addq	24(%rsp), %r12          # 8-byte Folded Reload
	imull	quant_coef(%r12,%r15,4), %ebx
	addl	8(%rsp), %ebx           # 4-byte Folded Reload
	movl	4(%rsp), %ecx           # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %ebx
	movl	%ebx, %r13d
	negl	%r13d
	cmovll	%ebx, %r13d
	addq	%rdx, %rsi
	addq	%r15, %rdi
	shlq	$6, %rsi
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %r11
	addq	%rcx, %rsi
	movl	%r13d, %ecx
	negl	%ecx
	testl	%r9d, %r9d
	leaq	13136(%rsi,%rdi,4), %rdx
	movl	13136(%rsi,%rdi,4), %r8d
	movl	%r13d, %esi
	cmovsl	%ecx, %esi
	subl	%esi, %r8d
	movl	%r8d, %edi
	sarl	$31, %edi
	leal	(%r8,%rdi), %esi
	xorl	%edi, %esi
	je	.LBB11_23
# BB#24:                                #   in Loop: Header=BB11_16 Depth=3
	leaq	(%rbp,%rax,4), %rax
	movq	-24(%rsp), %rdi         # 8-byte Reload
	movq	-80(%rsp), %rbp         # 8-byte Reload
	orq	%rbp, (%rdi)
	negl	%esi
	testl	%r8d, %r8d
	cmovnsl	%r8d, %esi
	movslq	%r10d, %r10
	movq	-72(%rsp), %rdi         # 8-byte Reload
	movl	%esi, (%rdi,%r10,4)
	movq	-40(%rsp), %rsi         # 8-byte Reload
	movl	-104(%rsp), %edi        # 4-byte Reload
	movl	%edi, (%rsi,%r10,4)
	incl	%r10d
	movl	(%rax), %r9d
	movl	$2, -48(%rsp)           # 4-byte Folded Spill
	movl	-112(%rsp), %edi        # 4-byte Reload
	movl	$-1, %ebx
	movl	-88(%rsp), %ebp         # 4-byte Reload
	jmp	.LBB11_25
	.p2align	4, 0x90
.LBB11_23:                              #   in Loop: Header=BB11_16 Depth=3
	movl	-112(%rsp), %edi        # 4-byte Reload
	movl	-88(%rsp), %ebp         # 4-byte Reload
	movl	-104(%rsp), %ebx        # 4-byte Reload
.LBB11_25:                              #   in Loop: Header=BB11_16 Depth=3
	testl	%r9d, %r9d
	cmovsl	%ecx, %r13d
	addl	%r8d, %r13d
	imull	dequant_coef(%r12,%r15,4), %r13d
	movq	-16(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %r13d
	movl	%r13d, (%rdx)
	incq	%r14
	cmpq	$15, %r14
	jne	.LBB11_16
# BB#26:                                #   in Loop: Header=BB11_15 Depth=2
	movslq	%r10d, %rax
	movq	-72(%rsp), %rcx         # 8-byte Reload
	movl	$0, (%rcx,%rax,4)
	addl	$4, %ebp
	cmpl	$5, %ebp
	jl	.LBB11_15
# BB#27:                                #   in Loop: Header=BB11_14 Depth=1
	addl	$4, %edi
	cmpl	$5, %edi
	jl	.LBB11_14
# BB#28:
	leaq	13136(%r11), %r9
	xorl	%r8d, %r8d
	pxor	%xmm0, %xmm0
	movdqa	.LCPI11_0(%rip), %xmm1  # xmm1 = [255,255,255,255]
	.p2align	4, 0x90
.LBB11_29:                              # %.preheader372
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_30 Depth 2
	movl	$2, %r10d
	movq	%r9, %rcx
	.p2align	4, 0x90
.LBB11_30:                              # %.preheader368
                                        #   Parent Loop BB11_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %eax
	movl	64(%rcx), %esi
	movl	128(%rcx), %edi
	movl	192(%rcx), %edx
	leal	(%rdi,%rax), %ebx
	subl	%edi, %eax
	movl	%esi, %edi
	sarl	%edi
	subl	%edx, %edi
	sarl	%edx
	addl	%esi, %edx
	leal	(%rdx,%rbx), %esi
	movl	%esi, (%rcx)
	subl	%edx, %ebx
	movl	%ebx, 192(%rcx)
	leal	(%rdi,%rax), %edx
	movl	%edx, 64(%rcx)
	subl	%edi, %eax
	movl	%eax, 128(%rcx)
	movl	4(%rcx), %eax
	movl	68(%rcx), %edx
	movl	132(%rcx), %esi
	movl	196(%rcx), %edi
	leal	(%rsi,%rax), %ebx
	subl	%esi, %eax
	movl	%edx, %esi
	sarl	%esi
	subl	%edi, %esi
	sarl	%edi
	addl	%edx, %edi
	leal	(%rdi,%rbx), %edx
	movl	%edx, 4(%rcx)
	subl	%edi, %ebx
	movl	%ebx, 196(%rcx)
	leal	(%rsi,%rax), %edx
	movl	%edx, 68(%rcx)
	subl	%esi, %eax
	movl	%eax, 132(%rcx)
	movl	8(%rcx), %eax
	movl	72(%rcx), %edx
	movl	136(%rcx), %esi
	movl	200(%rcx), %edi
	leal	(%rsi,%rax), %ebx
	subl	%esi, %eax
	movl	%edx, %esi
	sarl	%esi
	subl	%edi, %esi
	sarl	%edi
	addl	%edx, %edi
	leal	(%rdi,%rbx), %edx
	movl	%edx, 8(%rcx)
	subl	%edi, %ebx
	movl	%ebx, 200(%rcx)
	leal	(%rsi,%rax), %edx
	movl	%edx, 72(%rcx)
	subl	%esi, %eax
	movl	%eax, 136(%rcx)
	movl	12(%rcx), %ebp
	movl	76(%rcx), %r11d
	movl	140(%rcx), %r14d
	movl	204(%rcx), %r15d
	leal	(%r14,%rbp), %ebx
	movl	%ebp, %eax
	subl	%r14d, %eax
	movl	%r11d, %edx
	sarl	%edx
	subl	%r15d, %edx
	movl	%r15d, %esi
	sarl	%esi
	addl	%r11d, %esi
	leal	(%rsi,%rbx), %edi
	movl	%edi, 12(%rcx)
	subl	%esi, %ebx
	movl	%ebx, 204(%rcx)
	leal	(%rdx,%rax), %esi
	movl	%esi, 76(%rcx)
	subl	%edx, %eax
	movl	%eax, 140(%rcx)
	movl	%ebp, -128(%rsp)
	movl	%r11d, -124(%rsp)
	movl	%r14d, -120(%rsp)
	movl	%r15d, -116(%rsp)
	movups	(%rcx), %xmm2
	movaps	%xmm2, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %edx
	movl	-124(%rsp), %esi
	leal	32(%rax,%rdx), %edi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%eax, %edx
	movl	%esi, %eax
	sarl	%eax
	movl	-116(%rsp), %ebp
	subl	%ebp, %eax
	sarl	%ebp
	addl	%esi, %ebp
	leal	(%rdi,%rbp), %esi
	subl	%ebp, %edi
	leal	32(%rdx), %ebp
	leal	32(%rdx,%rax), %edx
	subl	%eax, %ebp
	movd	%ebp, %xmm2
	movd	%esi, %xmm3
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movd	%edi, %xmm2
	movd	%edx, %xmm4
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	psrad	$6, %xmm3
	movdqa	%xmm3, %xmm2
	pcmpgtd	%xmm0, %xmm2
	pand	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	pcmpgtd	%xmm2, %xmm3
	pand	%xmm3, %xmm2
	pandn	%xmm1, %xmm3
	por	%xmm2, %xmm3
	movdqu	%xmm3, (%rcx)
	movups	64(%rcx), %xmm2
	movaps	%xmm2, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %edx
	movl	-124(%rsp), %esi
	leal	32(%rax,%rdx), %edi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%eax, %edx
	movl	%esi, %eax
	sarl	%eax
	movl	-116(%rsp), %ebp
	subl	%ebp, %eax
	sarl	%ebp
	addl	%esi, %ebp
	leal	(%rdi,%rbp), %esi
	subl	%ebp, %edi
	leal	32(%rdx), %ebp
	leal	32(%rdx,%rax), %edx
	subl	%eax, %ebp
	movd	%ebp, %xmm2
	movd	%esi, %xmm3
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movd	%edi, %xmm2
	movd	%edx, %xmm4
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	psrad	$6, %xmm3
	movdqa	%xmm3, %xmm2
	pcmpgtd	%xmm0, %xmm2
	pand	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	pcmpgtd	%xmm2, %xmm3
	pand	%xmm3, %xmm2
	pandn	%xmm1, %xmm3
	por	%xmm2, %xmm3
	movdqu	%xmm3, 64(%rcx)
	movups	128(%rcx), %xmm2
	movaps	%xmm2, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %edx
	movl	-124(%rsp), %esi
	leal	32(%rax,%rdx), %edi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%eax, %edx
	movl	%esi, %eax
	sarl	%eax
	movl	-116(%rsp), %ebp
	subl	%ebp, %eax
	sarl	%ebp
	addl	%esi, %ebp
	leal	(%rdi,%rbp), %esi
	subl	%ebp, %edi
	leal	32(%rdx), %ebp
	leal	32(%rdx,%rax), %edx
	subl	%eax, %ebp
	movd	%ebp, %xmm2
	movd	%esi, %xmm3
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movd	%edi, %xmm2
	movd	%edx, %xmm4
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	psrad	$6, %xmm3
	movdqa	%xmm3, %xmm2
	pcmpgtd	%xmm0, %xmm2
	pand	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	pcmpgtd	%xmm2, %xmm3
	pand	%xmm3, %xmm2
	pandn	%xmm1, %xmm3
	por	%xmm2, %xmm3
	movdqu	%xmm3, 128(%rcx)
	movups	192(%rcx), %xmm2
	movaps	%xmm2, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %edx
	movl	-124(%rsp), %esi
	leal	32(%rax,%rdx), %edi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%eax, %edx
	movl	%esi, %eax
	sarl	%eax
	movl	-116(%rsp), %ebp
	subl	%ebp, %eax
	sarl	%ebp
	addl	%esi, %ebp
	leal	(%rdi,%rbp), %esi
	subl	%ebp, %edi
	leal	32(%rdx), %ebp
	leal	32(%rdx,%rax), %edx
	subl	%eax, %ebp
	movd	%ebp, %xmm2
	movd	%esi, %xmm3
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movd	%edi, %xmm2
	movd	%edx, %xmm4
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	psrad	$6, %xmm3
	movdqa	%xmm3, %xmm2
	pcmpgtd	%xmm0, %xmm2
	pand	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	pcmpgtd	%xmm2, %xmm3
	pand	%xmm3, %xmm2
	pandn	%xmm1, %xmm3
	por	%xmm2, %xmm3
	movdqu	%xmm3, 192(%rcx)
	addq	$256, %rcx              # imm = 0x100
	decq	%r10
	jne	.LBB11_30
# BB#31:                                #   in Loop: Header=BB11_29 Depth=1
	incq	%r8
	addq	$16, %r9
	cmpq	$2, %r8
	jne	.LBB11_29
# BB#32:                                # %.preheader
	movq	enc_picture(%rip), %rax
	movq	6472(%rax), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	-8(%rsp), %rbx          # 8-byte Reload
	movslq	188(%rbx), %rbp
	movslq	184(%rbx), %rcx
	movq	(%rax,%rbp,8), %rsi
	leal	4(%rbp), %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rdi
	movq	8(%rax,%rbp,8), %r15
	leal	5(%rbp), %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %r14
	movq	16(%rax,%rbp,8), %r10
	leal	6(%rbp), %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %r11
	movq	24(%rax,%rbp,8), %r8
	leal	7(%rbp), %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %r9
	movl	-48(%rsp), %edx         # 4-byte Reload
	cmpl	$2, %edx
	movl	-108(%rsp), %eax        # 4-byte Reload
	cmovel	%edx, %eax
	leal	4(%rcx), %edx
	movslq	%edx, %rbp
	movzwl	13136(%rbx), %edx
	movw	%dx, (%rsi,%rcx,2)
	movzwl	13392(%rbx), %edx
	movw	%dx, (%rsi,%rbp,2)
	movzwl	13152(%rbx), %edx
	movw	%dx, (%rdi,%rcx,2)
	movzwl	13408(%rbx), %edx
	movw	%dx, (%rdi,%rbp,2)
	movzwl	13200(%rbx), %edx
	movw	%dx, (%r15,%rcx,2)
	movzwl	13456(%rbx), %edx
	movw	%dx, (%r15,%rbp,2)
	movzwl	13216(%rbx), %edx
	movw	%dx, (%r14,%rcx,2)
	movzwl	13472(%rbx), %edx
	movw	%dx, (%r14,%rbp,2)
	movzwl	13264(%rbx), %edx
	movw	%dx, (%r10,%rcx,2)
	movzwl	13520(%rbx), %edx
	movw	%dx, (%r10,%rbp,2)
	movzwl	13280(%rbx), %edx
	movw	%dx, (%r11,%rcx,2)
	movzwl	13536(%rbx), %edx
	movw	%dx, (%r11,%rbp,2)
	movzwl	13328(%rbx), %edx
	movw	%dx, (%r8,%rcx,2)
	movzwl	13584(%rbx), %edx
	movw	%dx, (%r8,%rbp,2)
	movzwl	13344(%rbx), %edx
	movw	%dx, (%r9,%rcx,2)
	movzwl	13600(%rbx), %edx
	movw	%dx, (%r9,%rbp,2)
	leal	5(%rcx), %edx
	movslq	%edx, %rbp
	movzwl	13140(%rbx), %edx
	movw	%dx, 2(%rsi,%rcx,2)
	movzwl	13396(%rbx), %edx
	movw	%dx, (%rsi,%rbp,2)
	movzwl	13156(%rbx), %edx
	movw	%dx, 2(%rdi,%rcx,2)
	movzwl	13412(%rbx), %edx
	movw	%dx, (%rdi,%rbp,2)
	movzwl	13204(%rbx), %edx
	movw	%dx, 2(%r15,%rcx,2)
	movzwl	13460(%rbx), %edx
	movw	%dx, (%r15,%rbp,2)
	movzwl	13220(%rbx), %edx
	movw	%dx, 2(%r14,%rcx,2)
	movzwl	13476(%rbx), %edx
	movw	%dx, (%r14,%rbp,2)
	movzwl	13268(%rbx), %edx
	movw	%dx, 2(%r10,%rcx,2)
	movzwl	13524(%rbx), %edx
	movw	%dx, (%r10,%rbp,2)
	movzwl	13284(%rbx), %edx
	movw	%dx, 2(%r11,%rcx,2)
	movzwl	13540(%rbx), %edx
	movw	%dx, (%r11,%rbp,2)
	movzwl	13332(%rbx), %edx
	movw	%dx, 2(%r8,%rcx,2)
	movzwl	13588(%rbx), %edx
	movw	%dx, (%r8,%rbp,2)
	movzwl	13348(%rbx), %edx
	movw	%dx, 2(%r9,%rcx,2)
	movzwl	13604(%rbx), %edx
	movw	%dx, (%r9,%rbp,2)
	leal	6(%rcx), %edx
	movslq	%edx, %rbp
	movzwl	13144(%rbx), %edx
	movw	%dx, 4(%rsi,%rcx,2)
	movzwl	13400(%rbx), %edx
	movw	%dx, (%rsi,%rbp,2)
	movzwl	13160(%rbx), %edx
	movw	%dx, 4(%rdi,%rcx,2)
	movzwl	13416(%rbx), %edx
	movw	%dx, (%rdi,%rbp,2)
	movzwl	13208(%rbx), %edx
	movw	%dx, 4(%r15,%rcx,2)
	movzwl	13464(%rbx), %edx
	movw	%dx, (%r15,%rbp,2)
	movzwl	13224(%rbx), %edx
	movw	%dx, 4(%r14,%rcx,2)
	movzwl	13480(%rbx), %edx
	movw	%dx, (%r14,%rbp,2)
	movzwl	13272(%rbx), %edx
	movw	%dx, 4(%r10,%rcx,2)
	movzwl	13528(%rbx), %edx
	movw	%dx, (%r10,%rbp,2)
	movzwl	13288(%rbx), %edx
	movw	%dx, 4(%r11,%rcx,2)
	movzwl	13544(%rbx), %edx
	movw	%dx, (%r11,%rbp,2)
	movzwl	13336(%rbx), %edx
	movw	%dx, 4(%r8,%rcx,2)
	movzwl	13592(%rbx), %edx
	movw	%dx, (%r8,%rbp,2)
	movzwl	13352(%rbx), %edx
	movw	%dx, 4(%r9,%rcx,2)
	movzwl	13608(%rbx), %edx
	movw	%dx, (%r9,%rbp,2)
	leal	7(%rcx), %edx
	movslq	%edx, %rbp
	movzwl	13148(%rbx), %edx
	movw	%dx, 6(%rsi,%rcx,2)
	movzwl	13404(%rbx), %edx
	movw	%dx, (%rsi,%rbp,2)
	movzwl	13164(%rbx), %edx
	movw	%dx, 6(%rdi,%rcx,2)
	movzwl	13420(%rbx), %edx
	movw	%dx, (%rdi,%rbp,2)
	movzwl	13212(%rbx), %edx
	movw	%dx, 6(%r15,%rcx,2)
	movzwl	13468(%rbx), %edx
	movw	%dx, (%r15,%rbp,2)
	movzwl	13228(%rbx), %edx
	movw	%dx, 6(%r14,%rcx,2)
	movzwl	13484(%rbx), %edx
	movw	%dx, (%r14,%rbp,2)
	movzwl	13276(%rbx), %edx
	movw	%dx, 6(%r10,%rcx,2)
	movzwl	13532(%rbx), %edx
	movw	%dx, (%r10,%rbp,2)
	movzwl	13292(%rbx), %edx
	movw	%dx, 6(%r11,%rcx,2)
	movzwl	13548(%rbx), %edx
	movw	%dx, (%r11,%rbp,2)
	movzwl	13340(%rbx), %edx
	movw	%dx, 6(%r8,%rcx,2)
	movzwl	13596(%rbx), %edx
	movw	%dx, (%r8,%rbp,2)
	movzwl	13356(%rbx), %edx
	movw	%dx, 6(%r9,%rcx,2)
	movzwl	13612(%rbx), %ecx
	movw	%cx, (%r9,%rbp,2)
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	dct_chroma_sp2, .Lfunc_end11-dct_chroma_sp2
	.cfi_endproc

	.type	SNGL_SCAN,@object       # @SNGL_SCAN
	.section	.rodata,"a",@progbits
	.globl	SNGL_SCAN
	.p2align	4
SNGL_SCAN:
	.zero	2
	.asciz	"\001"
	.ascii	"\000\001"
	.ascii	"\000\002"
	.zero	2,1
	.asciz	"\002"
	.asciz	"\003"
	.ascii	"\002\001"
	.ascii	"\001\002"
	.ascii	"\000\003"
	.ascii	"\001\003"
	.zero	2,2
	.ascii	"\003\001"
	.ascii	"\003\002"
	.ascii	"\002\003"
	.zero	2,3
	.size	SNGL_SCAN, 32

	.type	FIELD_SCAN,@object      # @FIELD_SCAN
	.globl	FIELD_SCAN
	.p2align	4
FIELD_SCAN:
	.zero	2
	.ascii	"\000\001"
	.asciz	"\001"
	.ascii	"\000\002"
	.ascii	"\000\003"
	.zero	2,1
	.ascii	"\001\002"
	.ascii	"\001\003"
	.asciz	"\002"
	.ascii	"\002\001"
	.zero	2,2
	.ascii	"\002\003"
	.asciz	"\003"
	.ascii	"\003\001"
	.ascii	"\003\002"
	.zero	2,3
	.size	FIELD_SCAN, 32

	.type	COEFF_COST,@object      # @COEFF_COST
	.globl	COEFF_COST
	.p2align	4
COEFF_COST:
	.asciz	"\003\002\002\001\001\001\000\000\000\000\000\000\000\000\000"
	.zero	16,9
	.size	COEFF_COST, 32

	.type	COEFF_BIT_COST,@object  # @COEFF_BIT_COST
	.globl	COEFF_BIT_COST
	.p2align	4
COEFF_BIT_COST:
	.ascii	"\003\005\007\t\t\013\013\013\013\r\r\r\r\r\r\r"
	.ascii	"\005\007\t\t\013\013\013\013\r\r\r\r\r\r\r\r"
	.ascii	"\007\t\t\013\013\013\013\r\r\r\r\r\r\r\r\017"
	.ascii	"\007\t\t\013\013\013\013\r\r\r\r\r\r\r\r\017"
	.ascii	"\007\007\t\t\t\t\013\013\013\013\013\013\013\013\r\r"
	.ascii	"\007\007\t\t\t\t\013\013\013\013\013\013\013\013\r\r"
	.ascii	"\007\007\t\t\t\t\013\013\013\013\013\013\013\013\r\r"
	.ascii	"\007\007\t\t\t\t\013\013\013\013\013\013\013\013\r\r"
	.ascii	"\007\007\t\t\t\t\013\013\013\013\013\013\013\013\r\r"
	.ascii	"\007\007\t\t\t\t\013\013\013\013\013\013\013\013\r\r"
	.ascii	"\007\007\t\t\t\t\013\013\013\013\013\013\013\013\r\r"
	.ascii	"\007\007\t\t\t\t\013\013\013\013\013\013\013\013\r\r"
	.ascii	"\007\007\t\t\t\t\013\013\013\013\013\013\013\013\r\r"
	.ascii	"\007\007\t\t\t\t\013\013\013\013\013\013\013\013\r\r"
	.ascii	"\007\007\t\t\t\t\013\013\013\013\013\013\013\013\r\r"
	.ascii	"\007\007\t\t\t\t\013\013\013\013\013\013\013\013\r\r"
	.ascii	"\003\005\007\007\007\t\t\t\t\013\013\r\r\r\r\017"
	.ascii	"\005\t\t\013\013\r\r\r\r\017\017\017\017\017\017\017"
	.ascii	"\007\013\013\r\r\r\r\017\017\017\017\017\017\017\017\021"
	.ascii	"\t\013\013\r\r\r\r\017\017\017\017\017\017\017\017\021"
	.ascii	"\t\013\013\r\r\r\r\017\017\017\017\017\017\017\017\021"
	.ascii	"\013\013\r\r\r\r\017\017\017\017\017\017\017\017\021\021"
	.ascii	"\013\013\r\r\r\r\017\017\017\017\017\017\017\017\021\021"
	.ascii	"\013\013\r\r\r\r\017\017\017\017\017\017\017\017\021\021"
	.ascii	"\013\013\r\r\r\r\017\017\017\017\017\017\017\017\021\021"
	.ascii	"\013\013\r\r\r\r\017\017\017\017\017\017\017\017\021\021"
	.ascii	"\013\013\r\r\r\r\017\017\017\017\017\017\017\017\021\021"
	.ascii	"\013\013\r\r\r\r\017\017\017\017\017\017\017\017\021\021"
	.ascii	"\013\013\r\r\r\r\017\017\017\017\017\017\017\017\021\021"
	.ascii	"\013\013\r\r\r\r\017\017\017\017\017\017\017\017\021\021"
	.zero	16
	.zero	16
	.ascii	"\003\007\t\t\013\r\r\017\017\017\017\021\021\021\021\021"
	.ascii	"\005\t\013\r\r\017\017\017\017\021\021\021\021\021\021\021"
	.ascii	"\005\t\013\r\r\017\017\017\017\021\021\021\021\021\021\021"
	.ascii	"\007\013\r\r\017\017\017\017\021\021\021\021\021\021\021\021"
	.ascii	"\007\013\r\r\017\017\017\017\021\021\021\021\021\021\021\021"
	.ascii	"\007\013\r\r\017\017\017\017\021\021\021\021\021\021\021\021"
	.ascii	"\t\013\r\r\017\017\017\017\021\021\021\021\021\021\021\021"
	.ascii	"\t\013\r\r\017\017\017\017\021\021\021\021\021\021\021\021"
	.ascii	"\t\013\r\r\017\017\017\017\021\021\021\021\021\021\021\021"
	.ascii	"\t\013\r\r\017\017\017\017\021\021\021\021\021\021\021\021"
	.ascii	"\013\r\r\017\017\017\017\021\021\021\021\021\021\021\021\023"
	.ascii	"\013\r\r\017\017\017\017\021\021\021\021\021\021\021\021\023"
	.ascii	"\013\r\r\017\017\017\017\021\021\021\021\021\021\021\021\023"
	.ascii	"\013\r\r\017\017\017\017\021\021\021\021\021\021\021\021\023"
	.ascii	"\013\r\r\017\017\017\017\021\021\021\021\021\021\021\021\023"
	.ascii	"\013\r\r\017\017\017\017\021\021\021\021\021\021\021\021\023"
	.size	COEFF_BIT_COST, 768

	.type	SCAN_YUV422,@object     # @SCAN_YUV422
	.globl	SCAN_YUV422
	.p2align	4
SCAN_YUV422:
	.zero	2
	.ascii	"\000\001"
	.asciz	"\001"
	.ascii	"\000\002"
	.ascii	"\000\003"
	.zero	2,1
	.ascii	"\001\002"
	.ascii	"\001\003"
	.size	SCAN_YUV422, 16

	.type	hor_offset,@object      # @hor_offset
	.globl	hor_offset
	.p2align	4
hor_offset:
	.zero	16
	.ascii	"\000\004\000\004"
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\000\004\000\004"
	.ascii	"\000\004\000\004"
	.zero	4
	.zero	4
	.ascii	"\000\004\000\004"
	.ascii	"\b\f\b\f"
	.ascii	"\000\004\000\004"
	.ascii	"\b\f\b\f"
	.size	hor_offset, 64

	.type	ver_offset,@object      # @ver_offset
	.globl	ver_offset
	.p2align	4
ver_offset:
	.zero	16
	.ascii	"\000\000\004\004"
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\000\000\004\004"
	.ascii	"\b\b\f\f"
	.zero	4
	.zero	4
	.ascii	"\000\000\004\004"
	.ascii	"\000\000\004\004"
	.ascii	"\b\b\f\f"
	.ascii	"\b\b\f\f"
	.size	ver_offset, 64

	.type	quant_coef,@object      # @quant_coef
	.globl	quant_coef
	.p2align	4
quant_coef:
	.long	13107                   # 0x3333
	.long	8066                    # 0x1f82
	.long	13107                   # 0x3333
	.long	8066                    # 0x1f82
	.long	8066                    # 0x1f82
	.long	5243                    # 0x147b
	.long	8066                    # 0x1f82
	.long	5243                    # 0x147b
	.long	13107                   # 0x3333
	.long	8066                    # 0x1f82
	.long	13107                   # 0x3333
	.long	8066                    # 0x1f82
	.long	8066                    # 0x1f82
	.long	5243                    # 0x147b
	.long	8066                    # 0x1f82
	.long	5243                    # 0x147b
	.long	11916                   # 0x2e8c
	.long	7490                    # 0x1d42
	.long	11916                   # 0x2e8c
	.long	7490                    # 0x1d42
	.long	7490                    # 0x1d42
	.long	4660                    # 0x1234
	.long	7490                    # 0x1d42
	.long	4660                    # 0x1234
	.long	11916                   # 0x2e8c
	.long	7490                    # 0x1d42
	.long	11916                   # 0x2e8c
	.long	7490                    # 0x1d42
	.long	7490                    # 0x1d42
	.long	4660                    # 0x1234
	.long	7490                    # 0x1d42
	.long	4660                    # 0x1234
	.long	10082                   # 0x2762
	.long	6554                    # 0x199a
	.long	10082                   # 0x2762
	.long	6554                    # 0x199a
	.long	6554                    # 0x199a
	.long	4194                    # 0x1062
	.long	6554                    # 0x199a
	.long	4194                    # 0x1062
	.long	10082                   # 0x2762
	.long	6554                    # 0x199a
	.long	10082                   # 0x2762
	.long	6554                    # 0x199a
	.long	6554                    # 0x199a
	.long	4194                    # 0x1062
	.long	6554                    # 0x199a
	.long	4194                    # 0x1062
	.long	9362                    # 0x2492
	.long	5825                    # 0x16c1
	.long	9362                    # 0x2492
	.long	5825                    # 0x16c1
	.long	5825                    # 0x16c1
	.long	3647                    # 0xe3f
	.long	5825                    # 0x16c1
	.long	3647                    # 0xe3f
	.long	9362                    # 0x2492
	.long	5825                    # 0x16c1
	.long	9362                    # 0x2492
	.long	5825                    # 0x16c1
	.long	5825                    # 0x16c1
	.long	3647                    # 0xe3f
	.long	5825                    # 0x16c1
	.long	3647                    # 0xe3f
	.long	8192                    # 0x2000
	.long	5243                    # 0x147b
	.long	8192                    # 0x2000
	.long	5243                    # 0x147b
	.long	5243                    # 0x147b
	.long	3355                    # 0xd1b
	.long	5243                    # 0x147b
	.long	3355                    # 0xd1b
	.long	8192                    # 0x2000
	.long	5243                    # 0x147b
	.long	8192                    # 0x2000
	.long	5243                    # 0x147b
	.long	5243                    # 0x147b
	.long	3355                    # 0xd1b
	.long	5243                    # 0x147b
	.long	3355                    # 0xd1b
	.long	7282                    # 0x1c72
	.long	4559                    # 0x11cf
	.long	7282                    # 0x1c72
	.long	4559                    # 0x11cf
	.long	4559                    # 0x11cf
	.long	2893                    # 0xb4d
	.long	4559                    # 0x11cf
	.long	2893                    # 0xb4d
	.long	7282                    # 0x1c72
	.long	4559                    # 0x11cf
	.long	7282                    # 0x1c72
	.long	4559                    # 0x11cf
	.long	4559                    # 0x11cf
	.long	2893                    # 0xb4d
	.long	4559                    # 0x11cf
	.long	2893                    # 0xb4d
	.size	quant_coef, 384

	.type	dequant_coef,@object    # @dequant_coef
	.globl	dequant_coef
	.p2align	4
dequant_coef:
	.long	10                      # 0xa
	.long	13                      # 0xd
	.long	10                      # 0xa
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	10                      # 0xa
	.long	13                      # 0xd
	.long	10                      # 0xa
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	11                      # 0xb
	.long	14                      # 0xe
	.long	11                      # 0xb
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	11                      # 0xb
	.long	14                      # 0xe
	.long	11                      # 0xb
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	25                      # 0x19
	.long	20                      # 0x14
	.long	25                      # 0x19
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	25                      # 0x19
	.long	20                      # 0x14
	.long	25                      # 0x19
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	23                      # 0x17
	.long	29                      # 0x1d
	.long	23                      # 0x17
	.long	29                      # 0x1d
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	23                      # 0x17
	.long	29                      # 0x1d
	.long	23                      # 0x17
	.long	29                      # 0x1d
	.size	dequant_coef, 384

	.type	QP_SCALE_CR,@object     # @QP_SCALE_CR
	.globl	QP_SCALE_CR
	.p2align	4
QP_SCALE_CR:
	.ascii	"\000\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022\023\024\025\026\027\030\031\032\033\034\035\035\036\037  !\"\"##$$%%%&&&''''"
	.size	QP_SCALE_CR, 52

	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	dct_luma_16x16.M1,@object # @dct_luma_16x16.M1
	.local	dct_luma_16x16.M1
	.comm	dct_luma_16x16.M1,1024,16
	.type	dct_luma_16x16.M4,@object # @dct_luma_16x16.M4
	.local	dct_luma_16x16.M4
	.comm	dct_luma_16x16.M4,64,16
	.type	dct_luma_16x16.M0,@object # @dct_luma_16x16.M0
	.local	dct_luma_16x16.M0
	.comm	dct_luma_16x16.M0,1024,16
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	dct_luma.m4,@object     # @dct_luma.m4
	.local	dct_luma.m4
	.comm	dct_luma.m4,64,16
	.type	dct_chroma.m1,@object   # @dct_chroma.m1
	.local	dct_chroma.m1
	.comm	dct_chroma.m1,16,16
	.type	dct_chroma.m5,@object   # @dct_chroma.m5
	.local	dct_chroma.m5
	.comm	dct_chroma.m5,16,16
	.type	dct_chroma.cbpblk_pattern,@object # @dct_chroma.cbpblk_pattern
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
dct_chroma.cbpblk_pattern:
	.quad	0                       # 0x0
	.quad	983040                  # 0xf0000
	.quad	16711680                # 0xff0000
	.quad	4294901760              # 0xffff0000
	.size	dct_chroma.cbpblk_pattern, 32

	.type	dct_chroma.m3,@object   # @dct_chroma.m3
	.local	dct_chroma.m3
	.comm	dct_chroma.m3,64,16
	.type	dct_chroma.m4,@object   # @dct_chroma.m4
	.local	dct_chroma.m4
	.comm	dct_chroma.m4,64,16
	.type	cbp_blk_chroma,@object  # @cbp_blk_chroma
	.p2align	4
cbp_blk_chroma:
	.ascii	"\020\021\022\023"
	.ascii	"\024\025\026\027"
	.ascii	"\030\031\032\033"
	.ascii	"\034\035\036\037"
	.ascii	" !\"#"
	.ascii	"$%&'"
	.ascii	"()*+"
	.ascii	",-./"
	.size	cbp_blk_chroma, 32

	.type	A,@object               # @A
	.section	.rodata,"a",@progbits
	.p2align	4
A:
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	25                      # 0x19
	.long	20                      # 0x14
	.long	25                      # 0x19
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	25                      # 0x19
	.long	20                      # 0x14
	.long	25                      # 0x19
	.size	A, 64

	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	dct_luma_16x16.M5.0,@object # @dct_luma_16x16.M5.0
	.local	dct_luma_16x16.M5.0
	.comm	dct_luma_16x16.M5.0,4,16
	.type	dct_luma_16x16.M5.1,@object # @dct_luma_16x16.M5.1
	.local	dct_luma_16x16.M5.1
	.comm	dct_luma_16x16.M5.1,4,16
	.type	dct_luma_16x16.M5.2,@object # @dct_luma_16x16.M5.2
	.local	dct_luma_16x16.M5.2
	.comm	dct_luma_16x16.M5.2,4,8
	.type	dct_luma_16x16.M5.3,@object # @dct_luma_16x16.M5.3
	.local	dct_luma_16x16.M5.3
	.comm	dct_luma_16x16.M5.3,4,16
	.type	dct_luma_16x16.M6.0,@object # @dct_luma_16x16.M6.0
	.local	dct_luma_16x16.M6.0
	.comm	dct_luma_16x16.M6.0,4,16
	.type	dct_luma_16x16.M6.1,@object # @dct_luma_16x16.M6.1
	.local	dct_luma_16x16.M6.1
	.comm	dct_luma_16x16.M6.1,4,16
	.type	dct_luma_16x16.M6.2,@object # @dct_luma_16x16.M6.2
	.local	dct_luma_16x16.M6.2
	.comm	dct_luma_16x16.M6.2,4,8
	.type	dct_luma_16x16.M6.3,@object # @dct_luma_16x16.M6.3
	.local	dct_luma_16x16.M6.3
	.comm	dct_luma_16x16.M6.3,4,16
	.type	dct_luma.m5.0,@object   # @dct_luma.m5.0
	.local	dct_luma.m5.0
	.comm	dct_luma.m5.0,4,16
	.type	dct_luma.m5.1,@object   # @dct_luma.m5.1
	.local	dct_luma.m5.1
	.comm	dct_luma.m5.1,4,16
	.type	dct_luma.m5.2,@object   # @dct_luma.m5.2
	.local	dct_luma.m5.2
	.comm	dct_luma.m5.2,4,8
	.type	dct_luma.m5.3,@object   # @dct_luma.m5.3
	.local	dct_luma.m5.3
	.comm	dct_luma.m5.3,4,16
	.type	dct_luma.m6.0,@object   # @dct_luma.m6.0
	.local	dct_luma.m6.0
	.comm	dct_luma.m6.0,4,16
	.type	dct_luma.m6.1,@object   # @dct_luma.m6.1
	.local	dct_luma.m6.1
	.comm	dct_luma.m6.1,4,16
	.type	dct_luma.m6.2,@object   # @dct_luma.m6.2
	.local	dct_luma.m6.2
	.comm	dct_luma.m6.2,4,8
	.type	dct_luma.m6.3,@object   # @dct_luma.m6.3
	.local	dct_luma.m6.3
	.comm	dct_luma.m6.3,4,16
	.type	dct_chroma.m6.0,@object # @dct_chroma.m6.0
	.local	dct_chroma.m6.0
	.comm	dct_chroma.m6.0,4,16
	.type	dct_chroma.m6.1,@object # @dct_chroma.m6.1
	.local	dct_chroma.m6.1
	.comm	dct_chroma.m6.1,4,16
	.type	dct_chroma.m6.2,@object # @dct_chroma.m6.2
	.local	dct_chroma.m6.2
	.comm	dct_chroma.m6.2,4,8
	.type	dct_chroma.m6.3,@object # @dct_chroma.m6.3
	.local	dct_chroma.m6.3
	.comm	dct_chroma.m6.3,4,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
