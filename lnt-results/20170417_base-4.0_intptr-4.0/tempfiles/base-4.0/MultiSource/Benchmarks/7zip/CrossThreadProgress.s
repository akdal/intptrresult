	.text
	.file	"CrossThreadProgress.bc"
	.globl	_ZN20CCrossThreadProgress12SetRatioInfoEPKyS1_
	.p2align	4, 0x90
	.type	_ZN20CCrossThreadProgress12SetRatioInfoEPKyS1_,@function
_ZN20CCrossThreadProgress12SetRatioInfoEPKyS1_: # @_ZN20CCrossThreadProgress12SetRatioInfoEPKyS1_
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	%rsi, 16(%rbx)
	movq	%rdx, 24(%rbx)
	leaq	40(%rbx), %rdi
	callq	Event_Set
	leaq	144(%rbx), %rdi
	callq	Event_Wait
	movl	32(%rbx), %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN20CCrossThreadProgress12SetRatioInfoEPKyS1_, .Lfunc_end0-_ZN20CCrossThreadProgress12SetRatioInfoEPKyS1_
	.cfi_endproc

	.section	.text._ZN20CCrossThreadProgress14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN20CCrossThreadProgress14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN20CCrossThreadProgress14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN20CCrossThreadProgress14QueryInterfaceERK4GUIDPPv,@function
_ZN20CCrossThreadProgress14QueryInterfaceERK4GUIDPPv: # @_ZN20CCrossThreadProgress14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB1_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB1_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB1_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB1_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB1_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB1_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB1_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB1_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB1_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB1_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB1_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB1_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB1_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB1_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB1_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB1_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB1_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end1:
	.size	_ZN20CCrossThreadProgress14QueryInterfaceERK4GUIDPPv, .Lfunc_end1-_ZN20CCrossThreadProgress14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN20CCrossThreadProgress6AddRefEv,"axG",@progbits,_ZN20CCrossThreadProgress6AddRefEv,comdat
	.weak	_ZN20CCrossThreadProgress6AddRefEv
	.p2align	4, 0x90
	.type	_ZN20CCrossThreadProgress6AddRefEv,@function
_ZN20CCrossThreadProgress6AddRefEv:     # @_ZN20CCrossThreadProgress6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end2:
	.size	_ZN20CCrossThreadProgress6AddRefEv, .Lfunc_end2-_ZN20CCrossThreadProgress6AddRefEv
	.cfi_endproc

	.section	.text._ZN20CCrossThreadProgress7ReleaseEv,"axG",@progbits,_ZN20CCrossThreadProgress7ReleaseEv,comdat
	.weak	_ZN20CCrossThreadProgress7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN20CCrossThreadProgress7ReleaseEv,@function
_ZN20CCrossThreadProgress7ReleaseEv:    # @_ZN20CCrossThreadProgress7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB3_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB3_2:
	popq	%rcx
	retq
.Lfunc_end3:
	.size	_ZN20CCrossThreadProgress7ReleaseEv, .Lfunc_end3-_ZN20CCrossThreadProgress7ReleaseEv
	.cfi_endproc

	.section	.text._ZN20CCrossThreadProgressD2Ev,"axG",@progbits,_ZN20CCrossThreadProgressD2Ev,comdat
	.weak	_ZN20CCrossThreadProgressD2Ev
	.p2align	4, 0x90
	.type	_ZN20CCrossThreadProgressD2Ev,@function
_ZN20CCrossThreadProgressD2Ev:          # @_ZN20CCrossThreadProgressD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 32
.Lcfi7:
	.cfi_offset %rbx, -24
.Lcfi8:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV20CCrossThreadProgress+16, (%rbx)
	leaq	144(%rbx), %rdi
.Ltmp0:
	callq	Event_Close
.Ltmp1:
# BB#1:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit
	addq	$40, %rbx
.Ltmp6:
	movq	%rbx, %rdi
	callq	Event_Close
.Ltmp7:
# BB#2:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit3
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_4:
.Ltmp8:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_3:
.Ltmp2:
	movq	%rax, %r14
	addq	$40, %rbx
.Ltmp3:
	movq	%rbx, %rdi
	callq	Event_Close
.Ltmp4:
# BB#5:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit4
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_6:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN20CCrossThreadProgressD2Ev, .Lfunc_end4-_ZN20CCrossThreadProgressD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp7           #   Call between .Ltmp7 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end4-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN20CCrossThreadProgressD0Ev,"axG",@progbits,_ZN20CCrossThreadProgressD0Ev,comdat
	.weak	_ZN20CCrossThreadProgressD0Ev
	.p2align	4, 0x90
	.type	_ZN20CCrossThreadProgressD0Ev,@function
_ZN20CCrossThreadProgressD0Ev:          # @_ZN20CCrossThreadProgressD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV20CCrossThreadProgress+16, (%rbx)
	leaq	144(%rbx), %rdi
.Ltmp9:
	callq	Event_Close
.Ltmp10:
# BB#1:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit.i
	leaq	40(%rbx), %rdi
.Ltmp15:
	callq	Event_Close
.Ltmp16:
# BB#2:                                 # %_ZN20CCrossThreadProgressD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB5_5:
.Ltmp17:
	movq	%rax, %r14
	jmp	.LBB5_6
.LBB5_3:
.Ltmp11:
	movq	%rax, %r14
	leaq	40(%rbx), %rdi
.Ltmp12:
	callq	Event_Close
.Ltmp13:
.LBB5_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_4:
.Ltmp14:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN20CCrossThreadProgressD0Ev, .Lfunc_end5-_ZN20CCrossThreadProgressD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	1                       #   On action: 1
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp13     #   Call between .Ltmp13 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end6:
	.size	__clang_call_terminate, .Lfunc_end6-__clang_call_terminate

	.type	_ZTV20CCrossThreadProgress,@object # @_ZTV20CCrossThreadProgress
	.section	.rodata,"a",@progbits
	.globl	_ZTV20CCrossThreadProgress
	.p2align	3
_ZTV20CCrossThreadProgress:
	.quad	0
	.quad	_ZTI20CCrossThreadProgress
	.quad	_ZN20CCrossThreadProgress14QueryInterfaceERK4GUIDPPv
	.quad	_ZN20CCrossThreadProgress6AddRefEv
	.quad	_ZN20CCrossThreadProgress7ReleaseEv
	.quad	_ZN20CCrossThreadProgressD2Ev
	.quad	_ZN20CCrossThreadProgressD0Ev
	.quad	_ZN20CCrossThreadProgress12SetRatioInfoEPKyS1_
	.size	_ZTV20CCrossThreadProgress, 64

	.type	_ZTS20CCrossThreadProgress,@object # @_ZTS20CCrossThreadProgress
	.globl	_ZTS20CCrossThreadProgress
	.p2align	4
_ZTS20CCrossThreadProgress:
	.asciz	"20CCrossThreadProgress"
	.size	_ZTS20CCrossThreadProgress, 23

	.type	_ZTS21ICompressProgressInfo,@object # @_ZTS21ICompressProgressInfo
	.section	.rodata._ZTS21ICompressProgressInfo,"aG",@progbits,_ZTS21ICompressProgressInfo,comdat
	.weak	_ZTS21ICompressProgressInfo
	.p2align	4
_ZTS21ICompressProgressInfo:
	.asciz	"21ICompressProgressInfo"
	.size	_ZTS21ICompressProgressInfo, 24

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI21ICompressProgressInfo,@object # @_ZTI21ICompressProgressInfo
	.section	.rodata._ZTI21ICompressProgressInfo,"aG",@progbits,_ZTI21ICompressProgressInfo,comdat
	.weak	_ZTI21ICompressProgressInfo
	.p2align	4
_ZTI21ICompressProgressInfo:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21ICompressProgressInfo
	.quad	_ZTI8IUnknown
	.size	_ZTI21ICompressProgressInfo, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI20CCrossThreadProgress,@object # @_ZTI20CCrossThreadProgress
	.section	.rodata,"a",@progbits
	.globl	_ZTI20CCrossThreadProgress
	.p2align	4
_ZTI20CCrossThreadProgress:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS20CCrossThreadProgress
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI21ICompressProgressInfo
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI20CCrossThreadProgress, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
