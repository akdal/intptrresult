	.text
	.file	"output.bc"
	.globl	output_headers
	.p2align	4, 0x90
	.type	output_headers,@function
output_headers:                         # @output_headers
	.cfi_startproc
# BB#0:
	cmpl	$0, semantic_parser(%rip)
	je	.LBB0_1
# BB#2:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	fguard(%rip), %rdi
	movq	attrsfile(%rip), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	fprintf
	cmpl	$0, semantic_parser(%rip)
	movl	$.L.str.1, %eax
	movl	$.L.str.2, %esi
	cmovneq	%rax, %rsi
	addq	$8, %rsp
	jmp	.LBB0_3
.LBB0_1:
	movl	$.L.str.2, %esi
.LBB0_3:
	movq	faction(%rip), %rdi
	movq	attrsfile(%rip), %rdx
	xorl	%eax, %eax
	jmp	fprintf                 # TAILCALL
.Lfunc_end0:
	.size	output_headers, .Lfunc_end0-output_headers
	.cfi_endproc

	.globl	output_trailers
	.p2align	4, 0x90
	.type	output_trailers,@function
output_trailers:                        # @output_trailers
	.cfi_startproc
# BB#0:
	cmpl	$0, semantic_parser(%rip)
	je	.LBB1_2
# BB#1:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	movq	fguard(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
	movq	faction(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$9, %esi
	movl	$1, %edx
	popq	%rax
	jmp	fwrite                  # TAILCALL
.LBB1_2:
	movq	faction(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$3, %esi
	movl	$1, %edx
	jmp	fwrite                  # TAILCALL
.Lfunc_end1:
	.size	output_trailers, .Lfunc_end1-output_trailers
	.cfi_endproc

	.globl	output
	.p2align	4, 0x90
	.type	output,@function
output:                                 # @output
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %rbp, -16
	cmpl	$0, semantic_parser(%rip)
	jne	.LBB2_4
# BB#1:
	movq	fattrs(%rip), %rdi
	callq	rewind
	jmp	.LBB2_3
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	ftable(%rip), %rsi
	movl	%eax, %edi
	callq	_IO_putc
.LBB2_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	fattrs(%rip), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB2_2
.LBB2_4:                                # %.loopexit
	cmpl	$0, debugflag(%rip)
	je	.LBB2_6
# BB#5:
	movq	ftable(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$16, %esi
	movl	$1, %edx
	callq	fwrite
.LBB2_6:
	cmpl	$0, semantic_parser(%rip)
	je	.LBB2_8
# BB#7:
	movq	ftable(%rip), %rdi
	movq	attrsfile(%rip), %rdx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB2_8:
	movq	ftable(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$20, %esi
	movl	$1, %edx
	callq	fwrite
	movq	ftable(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$39, %esi
	movl	$1, %edx
	callq	fwrite
	movq	state_table(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_10
# BB#9:
	callq	free
.LBB2_10:
	movq	first_state(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_12
	.p2align	4, 0x90
.LBB2_11:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB2_11
.LBB2_12:                               # %free_itemsets.exit
	movq	ftable(%rip), %rdi
	movl	final_state(%rip), %edx
	movl	$.L.str.29, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	ftable(%rip), %rdi
	movl	$.L.str.30, %esi
	movl	$-32768, %edx           # imm = 0x8000
	xorl	%eax, %eax
	callq	fprintf
	movq	ftable(%rip), %rdi
	movl	ntokens(%rip), %edx
	movl	$.L.str.31, %esi
	xorl	%eax, %eax
	callq	fprintf
	callq	output_token_translations
	cmpl	$0, semantic_parser(%rip)
	je	.LBB2_14
# BB#13:
	callq	output_gram
.LBB2_14:
	movq	ritem(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_16
# BB#15:
	callq	free
.LBB2_16:
	cmpl	$0, semantic_parser(%rip)
	je	.LBB2_18
# BB#17:
	callq	output_stos
.LBB2_18:
	callq	output_rule_data
	callq	output_actions
	callq	output_parser
	movq	ftable(%rip), %r14
	movl	lineno(%rip), %ebp
	movq	infile(%rip), %rbx
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rcx
	cmoveq	%rbx, %rcx
	movl	$.L.str.47, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	fprintf
	jmp	.LBB2_20
	.p2align	4, 0x90
.LBB2_19:                               # %.lr.ph.i2
                                        #   in Loop: Header=BB2_20 Depth=1
	movq	ftable(%rip), %rsi
	movl	%eax, %edi
	callq	_IO_putc
.LBB2_20:                               # %.lr.ph.i2
                                        # =>This Inner Loop Header: Depth=1
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB2_19
# BB#21:                                # %output_program.exit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	output, .Lfunc_end2-output
	.cfi_endproc

	.globl	free_itemsets
	.p2align	4, 0x90
	.type	free_itemsets,@function
free_itemsets:                          # @free_itemsets
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	movq	state_table(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_2
# BB#1:
	callq	free
.LBB3_2:
	movq	first_state(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB3_3
.LBB3_4:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end3:
	.size	free_itemsets, .Lfunc_end3-free_itemsets
	.cfi_endproc

	.globl	output_defines
	.p2align	4, 0x90
	.type	output_defines,@function
output_defines:                         # @output_defines
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 16
	movq	ftable(%rip), %rdi
	movl	final_state(%rip), %edx
	movl	$.L.str.29, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	ftable(%rip), %rdi
	movl	$.L.str.30, %esi
	movl	$-32768, %edx           # imm = 0x8000
	xorl	%eax, %eax
	callq	fprintf
	movq	ftable(%rip), %rdi
	movl	ntokens(%rip), %edx
	movl	$.L.str.31, %esi
	xorl	%eax, %eax
	popq	%rcx
	jmp	fprintf                 # TAILCALL
.Lfunc_end4:
	.size	output_defines, .Lfunc_end4-output_defines
	.cfi_endproc

	.globl	output_token_translations
	.p2align	4, 0x90
	.type	output_token_translations,@function
output_token_translations:              # @output_token_translations
	.cfi_startproc
# BB#0:
	cmpl	$0, translations(%rip)
	movq	ftable(%rip), %r8
	je	.LBB5_11
# BB#1:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movl	max_user_token_number(%rip), %edx
	movl	nsyms(%rip), %ecx
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	%r8, %rdi
	callq	fprintf
	movq	ftable(%rip), %rcx
	cmpl	$126, ntokens(%rip)
	jg	.LBB5_3
# BB#2:
	movl	$.L.str.10, %edi
	movl	$42, %esi
	jmp	.LBB5_4
.LBB5_11:
	movl	$.L.str.14, %edi
	movl	$28, %esi
	movl	$1, %edx
	movq	%r8, %rcx
	jmp	fwrite                  # TAILCALL
.LBB5_3:
	movl	$.L.str.11, %edi
	movl	$43, %esi
.LBB5_4:                                # %.preheader
	movl	$1, %edx
	callq	fwrite
	cmpl	$0, max_user_token_number(%rip)
	movq	ftable(%rip), %rax
	jle	.LBB5_10
# BB#5:                                 # %.lr.ph.preheader
	movl	$10, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$44, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	cmpl	$10, %ebp
	jl	.LBB5_8
# BB#7:                                 #   in Loop: Header=BB5_6 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %ebp
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_8:                                #   in Loop: Header=BB5_6 Depth=1
	incl	%ebp
.LBB5_9:                                #   in Loop: Header=BB5_6 Depth=1
	movq	ftable(%rip), %rdi
	movq	token_translations(%rip), %rax
	movswl	2(%rax,%rbx,2), %edx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
	movslq	max_user_token_number(%rip), %rcx
	movq	ftable(%rip), %rax
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB5_6
.LBB5_10:                               # %._crit_edge
	movl	$.L.str.13, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end5:
	.size	output_token_translations, .Lfunc_end5-output_token_translations
	.cfi_endproc

	.globl	output_gram
	.p2align	4, 0x90
	.type	output_gram,@function
output_gram:                            # @output_gram
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	ftable(%rip), %rcx
	movl	$.L.str.15, %edi
	movl	$38, %esi
	movl	$1, %edx
	callq	fwrite
	cmpl	$0, nrules(%rip)
	movq	ftable(%rip), %rcx
	jle	.LBB6_6
# BB#1:                                 # %.lr.ph24.preheader
	movl	$10, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph24
                                        # =>This Inner Loop Header: Depth=1
	movl	$44, %edi
	movq	%rcx, %rsi
	callq	_IO_putc
	cmpl	$10, %ebp
	jl	.LBB6_4
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %ebp
	jmp	.LBB6_5
	.p2align	4, 0x90
.LBB6_4:                                #   in Loop: Header=BB6_2 Depth=1
	incl	%ebp
.LBB6_5:                                #   in Loop: Header=BB6_2 Depth=1
	movq	ftable(%rip), %rdi
	movq	rrhs(%rip), %rax
	movswl	2(%rax,%rbx,2), %edx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
	movslq	nrules(%rip), %rax
	movq	ftable(%rip), %rcx
	incq	%rbx
	cmpq	%rax, %rbx
	jl	.LBB6_2
.LBB6_6:                                # %._crit_edge25
	movq	ritem(%rip), %rax
	movswl	(%rax), %edx
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
	movq	ritem(%rip), %rbx
	cmpw	$0, 2(%rbx)
	movq	ftable(%rip), %rax
	je	.LBB6_14
# BB#7:                                 # %.lr.ph.preheader
	addq	$4, %rbx
	movl	$10, %ebp
	.p2align	4, 0x90
.LBB6_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$44, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	cmpl	$10, %ebp
	jl	.LBB6_10
# BB#9:                                 #   in Loop: Header=BB6_8 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %ebp
	jmp	.LBB6_11
	.p2align	4, 0x90
.LBB6_10:                               #   in Loop: Header=BB6_8 Depth=1
	incl	%ebp
.LBB6_11:                               #   in Loop: Header=BB6_8 Depth=1
	movswl	-2(%rbx), %edx
	testl	%edx, %edx
	movq	ftable(%rip), %rcx
	jle	.LBB6_15
# BB#12:                                #   in Loop: Header=BB6_8 Depth=1
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
	jmp	.LBB6_13
	.p2align	4, 0x90
.LBB6_15:                               #   in Loop: Header=BB6_8 Depth=1
	movl	$.L.str.17, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
.LBB6_13:                               # %.backedge
                                        #   in Loop: Header=BB6_8 Depth=1
	cmpw	$0, (%rbx)
	leaq	2(%rbx), %rbx
	movq	ftable(%rip), %rax
	jne	.LBB6_8
.LBB6_14:                               # %._crit_edge
	movl	$.L.str.13, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end6:
	.size	output_gram, .Lfunc_end6-output_gram
	.cfi_endproc

	.globl	output_stos
	.p2align	4, 0x90
	.type	output_stos,@function
output_stos:                            # @output_stos
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	ftable(%rip), %rcx
	movl	$.L.str.18, %edi
	movl	$38, %esi
	movl	$1, %edx
	callq	fwrite
	movq	ftable(%rip), %rax
	cmpl	$2, nstates(%rip)
	jl	.LBB7_6
# BB#1:                                 # %.lr.ph.preheader
	movl	$10, %ebp
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$44, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	cmpl	$10, %ebp
	jl	.LBB7_4
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %ebp
	jmp	.LBB7_5
	.p2align	4, 0x90
.LBB7_4:                                #   in Loop: Header=BB7_2 Depth=1
	incl	%ebp
.LBB7_5:                                #   in Loop: Header=BB7_2 Depth=1
	movq	ftable(%rip), %rdi
	movq	accessing_symbol(%rip), %rax
	movswl	(%rax,%rbx,2), %edx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
	incq	%rbx
	movslq	nstates(%rip), %rcx
	movq	ftable(%rip), %rax
	cmpq	%rcx, %rbx
	jl	.LBB7_2
.LBB7_6:                                # %._crit_edge
	movl	$.L.str.13, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end7:
	.size	output_stos, .Lfunc_end7-output_stos
	.cfi_endproc

	.globl	output_rule_data
	.p2align	4, 0x90
	.type	output_rule_data,@function
output_rule_data:                       # @output_rule_data
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	ftable(%rip), %rcx
	movl	$.L.str.19, %edi
	movl	$39, %esi
	movl	$1, %edx
	callq	fwrite
	cmpl	$0, nrules(%rip)
	movq	ftable(%rip), %rax
	jle	.LBB8_6
# BB#1:                                 # %.lr.ph65.preheader
	movl	$10, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph65
                                        # =>This Inner Loop Header: Depth=1
	movl	$44, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	cmpl	$10, %ebp
	jl	.LBB8_4
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %ebp
	jmp	.LBB8_5
	.p2align	4, 0x90
.LBB8_4:                                #   in Loop: Header=BB8_2 Depth=1
	incl	%ebp
.LBB8_5:                                #   in Loop: Header=BB8_2 Depth=1
	movq	ftable(%rip), %rdi
	movq	rline(%rip), %rax
	movswl	2(%rax,%rbx,2), %edx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
	movslq	nrules(%rip), %rcx
	movq	ftable(%rip), %rax
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB8_2
.LBB8_6:                                # %._crit_edge66
	movl	$.L.str.20, %edi
	movl	$50, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	cmpl	$0, ntokens(%rip)
	movq	ftable(%rip), %rax
	jle	.LBB8_26
# BB#7:                                 # %.lr.ph59.preheader
	movl	$10, %r14d
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB8_8:                                # %.lr.ph59
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_13 Depth 2
	movl	$44, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	cmpl	$10, %r14d
	jl	.LBB8_10
# BB#9:                                 #   in Loop: Header=BB8_8 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %r14d
	jmp	.LBB8_11
	.p2align	4, 0x90
.LBB8_10:                               #   in Loop: Header=BB8_8 Depth=1
	incl	%r14d
.LBB8_11:                               #   in Loop: Header=BB8_8 Depth=1
	movq	ftable(%rip), %rsi
	movl	$34, %edi
	callq	_IO_putc
	movq	tags(%rip), %rax
	movq	(%rax,%rbx,8), %rbp
	movb	(%rbp), %al
	testb	%al, %al
	je	.LBB8_25
# BB#12:                                # %.lr.ph54.preheader
                                        #   in Loop: Header=BB8_8 Depth=1
	incq	%rbp
	jmp	.LBB8_13
.LBB8_22:                               #   in Loop: Header=BB8_13 Depth=2
	movl	$.L.str.25, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
	jmp	.LBB8_24
	.p2align	4, 0x90
.LBB8_13:                               # %.lr.ph54
                                        #   Parent Loop BB8_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	%al, %edx
	movl	%edx, %ecx
	addb	$-8, %cl
	cmpb	$26, %cl
	ja	.LBB8_14
# BB#41:                                # %.lr.ph54
                                        #   in Loop: Header=BB8_13 Depth=2
	movzbl	%cl, %ecx
	jmpq	*.LJTI8_0(,%rcx,8)
.LBB8_19:                               #   in Loop: Header=BB8_13 Depth=2
	movq	ftable(%rip), %rcx
	movl	$.L.str.24, %edi
	jmp	.LBB8_17
	.p2align	4, 0x90
.LBB8_14:                               # %.lr.ph54
                                        #   in Loop: Header=BB8_13 Depth=2
	cmpb	$92, %al
	jne	.LBB8_20
.LBB8_15:                               #   in Loop: Header=BB8_13 Depth=2
	movq	ftable(%rip), %rdi
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB8_24
.LBB8_20:                               #   in Loop: Header=BB8_13 Depth=2
	cmpb	$32, %al
	movq	ftable(%rip), %rcx
	jl	.LBB8_22
# BB#21:                                #   in Loop: Header=BB8_13 Depth=2
	cmpb	$127, %al
	je	.LBB8_22
# BB#23:                                #   in Loop: Header=BB8_13 Depth=2
	movl	%edx, %edi
	movq	%rcx, %rsi
	callq	_IO_putc
	jmp	.LBB8_24
.LBB8_18:                               #   in Loop: Header=BB8_13 Depth=2
	movq	ftable(%rip), %rcx
	movl	$.L.str.23, %edi
	jmp	.LBB8_17
.LBB8_16:                               #   in Loop: Header=BB8_13 Depth=2
	movq	ftable(%rip), %rcx
	movl	$.L.str.22, %edi
.LBB8_17:                               #   in Loop: Header=BB8_13 Depth=2
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
.LBB8_24:                               #   in Loop: Header=BB8_13 Depth=2
	movzbl	(%rbp), %eax
	incq	%rbp
	testb	%al, %al
	jne	.LBB8_13
.LBB8_25:                               # %._crit_edge55
                                        #   in Loop: Header=BB8_8 Depth=1
	movq	ftable(%rip), %rsi
	movl	$34, %edi
	callq	_IO_putc
	movslq	ntokens(%rip), %rcx
	movq	ftable(%rip), %rax
	cmpq	%rcx, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB8_8
.LBB8_26:                               # %._crit_edge60
	movl	$.L.str.26, %edi
	movl	$40, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	cmpl	$0, nrules(%rip)
	jle	.LBB8_32
# BB#27:                                # %.lr.ph50.preheader
	movl	$10, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_28:                               # %.lr.ph50
                                        # =>This Inner Loop Header: Depth=1
	movq	ftable(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	cmpl	$10, %ebp
	jl	.LBB8_30
# BB#29:                                #   in Loop: Header=BB8_28 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %ebp
	jmp	.LBB8_31
	.p2align	4, 0x90
.LBB8_30:                               #   in Loop: Header=BB8_28 Depth=1
	incl	%ebp
.LBB8_31:                               #   in Loop: Header=BB8_28 Depth=1
	movq	ftable(%rip), %rdi
	movq	rlhs(%rip), %rax
	movswl	2(%rax,%rbx,2), %edx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
	movslq	nrules(%rip), %rax
	incq	%rbx
	cmpq	%rax, %rbx
	jl	.LBB8_28
.LBB8_32:                               # %._crit_edge51
	movq	rlhs(%rip), %rdi
	addq	$2, %rdi
	callq	free
	movq	ftable(%rip), %rcx
	movl	$.L.str.27, %edi
	movl	$40, %esi
	movl	$1, %edx
	callq	fwrite
	movl	nrules(%rip), %ebx
	movq	ftable(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	cmpl	$2, %ebx
	jl	.LBB8_39
# BB#33:                                # %.lr.ph.preheader
	movl	$10, %r14d
	movb	$1, %al
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB8_34:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testb	$1, %al
	je	.LBB8_36
# BB#35:                                #   in Loop: Header=BB8_34 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %r14d
	jmp	.LBB8_37
	.p2align	4, 0x90
.LBB8_36:                               #   in Loop: Header=BB8_34 Depth=1
	incl	%r14d
.LBB8_37:                               #   in Loop: Header=BB8_34 Depth=1
	movq	ftable(%rip), %rdi
	movq	rrhs(%rip), %rax
	movswl	2(%rax,%rbx,2), %edx
	movswl	(%rax,%rbx,2), %eax
	incq	%rbx
	decl	%edx
	subl	%eax, %edx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
	movslq	nrules(%rip), %rbp
	movq	ftable(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	cmpl	$9, %r14d
	setg	%al
	cmpq	%rbp, %rbx
	jl	.LBB8_34
# BB#38:                                # %._crit_edge
	cmpl	$10, %r14d
	jl	.LBB8_40
.LBB8_39:                               # %.critedge
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
.LBB8_40:
	movq	ftable(%rip), %rdi
	movl	nitems(%rip), %edx
	movq	rrhs(%rip), %rax
	movslq	nrules(%rip), %rcx
	movswl	(%rax,%rcx,2), %eax
	decl	%edx
	subl	%eax, %edx
	movl	$.L.str.28, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	rrhs(%rip), %rdi
	addq	$2, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end8:
	.size	output_rule_data, .Lfunc_end8-output_rule_data
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_19
	.quad	.LBB8_18
	.quad	.LBB8_16
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_20
	.quad	.LBB8_15

	.text
	.globl	output_actions
	.p2align	4, 0x90
	.type	output_actions,@function
output_actions:                         # @output_actions
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 16
.Lcfi33:
	.cfi_offset %rbx, -16
	movl	nvars(%rip), %edi
	addl	nstates(%rip), %edi
	movl	%edi, nvectors(%rip)
	shll	$3, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, froms(%rip)
	movl	nvectors(%rip), %edi
	shll	$3, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, tos(%rip)
	movl	nvectors(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, tally(%rip)
	movl	nvectors(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, width(%rip)
	callq	token_actions
	movq	shift_table(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB9_2
# BB#1:
	callq	free
.LBB9_2:
	movq	first_shift(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB9_4
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB9_3
.LBB9_4:                                # %free_shifts.exit
	movq	reduction_table(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB9_6
# BB#5:
	callq	free
.LBB9_6:
	movq	first_reduction(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB9_8
	.p2align	4, 0x90
.LBB9_7:                                # %.lr.ph.i2
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB9_7
.LBB9_8:                                # %free_reductions.exit
	movq	lookaheads(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB9_10
# BB#9:
	callq	free
.LBB9_10:
	movq	LA(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB9_12
# BB#11:
	callq	free
.LBB9_12:
	movq	LAruleno(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB9_14
# BB#13:
	callq	free
.LBB9_14:
	movq	accessing_symbol(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB9_16
# BB#15:
	callq	free
.LBB9_16:
	callq	goto_actions
	movslq	ntokens(%rip), %rdi
	addq	%rdi, %rdi
	addq	goto_map(%rip), %rdi
	je	.LBB9_18
# BB#17:
	callq	free
.LBB9_18:
	movq	from_state(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB9_20
# BB#19:
	callq	free
.LBB9_20:
	movq	to_state(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB9_22
# BB#21:
	callq	free
.LBB9_22:
	callq	sort_actions
	callq	pack_table
	callq	output_base
	callq	output_table
	popq	%rbx
	jmp	output_check            # TAILCALL
.Lfunc_end9:
	.size	output_actions, .Lfunc_end9-output_actions
	.cfi_endproc

	.globl	output_parser
	.p2align	4, 0x90
	.type	output_parser,@function
output_parser:                          # @output_parser
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 16
.Lcfi35:
	.cfi_offset %rbx, -16
	cmpl	$0, pure_parser(%rip)
	movq	ftable(%rip), %rcx
	je	.LBB10_2
# BB#1:
	movl	$.L.str.41, %edi
	movl	$20, %esi
	jmp	.LBB10_3
.LBB10_2:
	movl	$.L.str.42, %edi
	movl	$18, %esi
.LBB10_3:
	movl	$1, %edx
	callq	fwrite
	movq	fparser(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB10_26
# BB#4:
	callq	feof
	testl	%eax, %eax
	jne	.LBB10_26
	jmp	.LBB10_5
.LBB10_15:                              #   in Loop: Header=BB10_5 Depth=1
	movq	fparser(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$36, %ebx
	je	.LBB10_22
	jmp	.LBB10_25
.LBB10_20:                              #   in Loop: Header=BB10_5 Depth=1
	movq	ftable(%rip), %rsi
	movl	$35, %edi
	callq	fputc
	cmpl	$36, %ebx
	je	.LBB10_22
	jmp	.LBB10_25
.LBB10_19:                              #   in Loop: Header=BB10_5 Depth=1
	movq	ftable(%rip), %rcx
	movl	$.L.str.45, %edi
	movl	$2, %esi
	jmp	.LBB10_17
.LBB10_18:                              #   in Loop: Header=BB10_5 Depth=1
	movq	ftable(%rip), %rcx
	movl	$.L.str.44, %edi
	movl	$3, %esi
	jmp	.LBB10_17
.LBB10_16:                              #   in Loop: Header=BB10_5 Depth=1
	movq	ftable(%rip), %rcx
	movl	$.L.str.43, %edi
	movl	$4, %esi
.LBB10_17:                              #   in Loop: Header=BB10_5 Depth=1
	movl	$1, %edx
	callq	fwrite
	.p2align	4, 0x90
.LBB10_21:                              #   in Loop: Header=BB10_5 Depth=1
	cmpl	$36, %ebx
	jne	.LBB10_25
.LBB10_22:                              #   in Loop: Header=BB10_5 Depth=1
	movq	faction(%rip), %rdi
	callq	rewind
	jmp	.LBB10_23
	.p2align	4, 0x90
.LBB10_24:                              # %.lr.ph
                                        #   in Loop: Header=BB10_23 Depth=2
	movq	ftable(%rip), %rsi
	movl	%eax, %edi
	callq	_IO_putc
.LBB10_23:                              #   Parent Loop BB10_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	faction(%rip), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB10_24
	jmp	.LBB10_5
	.p2align	4, 0x90
.LBB10_25:                              #   in Loop: Header=BB10_5 Depth=1
	movq	ftable(%rip), %rsi
	movl	%ebx, %edi
	callq	_IO_putc
.LBB10_5:                               # %.preheader18
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_13 Depth 2
                                        #     Child Loop BB10_23 Depth 2
	movq	fparser(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB10_26
# BB#6:                                 # %.lr.ph20
                                        #   in Loop: Header=BB10_5 Depth=1
	cmpl	$10, %ebx
	jne	.LBB10_21
# BB#7:                                 # %.lr.ph20
                                        #   in Loop: Header=BB10_5 Depth=1
	movl	nolinesflag(%rip), %eax
	testl	%eax, %eax
	je	.LBB10_21
# BB#8:                                 #   in Loop: Header=BB10_5 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	fparser(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$35, %ebx
	jne	.LBB10_21
# BB#9:                                 #   in Loop: Header=BB10_5 Depth=1
	movq	fparser(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$108, %ebx
	jne	.LBB10_20
# BB#10:                                #   in Loop: Header=BB10_5 Depth=1
	movq	fparser(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$105, %ebx
	jne	.LBB10_19
# BB#11:                                #   in Loop: Header=BB10_5 Depth=1
	movq	fparser(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$110, %ebx
	jne	.LBB10_18
# BB#12:                                #   in Loop: Header=BB10_5 Depth=1
	movq	fparser(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$101, %ebx
	jne	.LBB10_16
	.p2align	4, 0x90
.LBB10_13:                              # %.preheader
                                        #   Parent Loop BB10_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	fparser(%rip), %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB10_15
# BB#14:                                # %.preheader
                                        #   in Loop: Header=BB10_13 Depth=2
	testl	%eax, %eax
	jns	.LBB10_13
	jmp	.LBB10_15
.LBB10_26:                              # %.loopexit19
	popq	%rbx
	retq
.Lfunc_end10:
	.size	output_parser, .Lfunc_end10-output_parser
	.cfi_endproc

	.globl	output_program
	.p2align	4, 0x90
	.type	output_program,@function
output_program:                         # @output_program
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 32
.Lcfi39:
	.cfi_offset %rbx, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	ftable(%rip), %r14
	movl	lineno(%rip), %ebp
	movq	infile(%rip), %rbx
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rcx
	cmoveq	%rbx, %rcx
	movl	$.L.str.47, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	fprintf
	jmp	.LBB11_2
	.p2align	4, 0x90
.LBB11_1:                               # %.lr.ph
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	ftable(%rip), %rsi
	movl	%eax, %edi
	callq	_IO_putc
.LBB11_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB11_1
# BB#3:                                 # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end11:
	.size	output_program, .Lfunc_end11-output_program
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	token_actions
	.p2align	4, 0x90
	.type	token_actions,@function
token_actions:                          # @token_actions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 48
.Lcfi47:
	.cfi_offset %rbx, -48
.Lcfi48:
	.cfi_offset %r12, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	ntokens(%rip), %edi
	addl	%edi, %edi
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, actrow(%rip)
	xorl	%edi, %edi
	callq	action_row
	movl	%eax, %ecx
	movq	ftable(%rip), %rdi
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	fprintf
	movslq	ntokens(%rip), %rax
	testq	%rax, %rax
	jle	.LBB12_28
# BB#1:                                 # %.lr.ph41.i
	movq	actrow(%rip), %rcx
	cmpl	$8, %eax
	jae	.LBB12_3
# BB#2:
	xorl	%edx, %edx
	jmp	.LBB12_11
.LBB12_3:                               # %min.iters.checked
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB12_6
# BB#4:                                 # %vector.body.preheader
	leaq	-8(%rdx), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	btl	$3, %edi
	jb	.LBB12_7
# BB#5:                                 # %vector.body.prol
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	pxor	%xmm2, %xmm2
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	8(%rcx), %xmm1          # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	pcmpeqd	%xmm2, %xmm0
	movdqa	.LCPI12_0(%rip), %xmm3  # xmm3 = [1,1,1,1]
	pandn	%xmm3, %xmm0
	pcmpeqd	%xmm2, %xmm1
	pandn	%xmm3, %xmm1
	movl	$8, %edi
	testq	%rsi, %rsi
	jne	.LBB12_8
	jmp	.LBB12_10
.LBB12_6:
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	jmp	.LBB12_11
.LBB12_7:
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	pxor	%xmm1, %xmm1
	testq	%rsi, %rsi
	je	.LBB12_10
.LBB12_8:                               # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	leaq	24(%rcx,%rdi,2), %rdi
	pxor	%xmm2, %xmm2
	movdqa	.LCPI12_0(%rip), %xmm3  # xmm3 = [1,1,1,1]
	.p2align	4, 0x90
.LBB12_9:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rdi), %xmm4        # xmm4 = mem[0],zero
	punpcklwd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	movq	-16(%rdi), %xmm5        # xmm5 = mem[0],zero
	punpcklwd	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3]
	pcmpeqd	%xmm2, %xmm4
	pandn	%xmm3, %xmm4
	pcmpeqd	%xmm2, %xmm5
	pandn	%xmm3, %xmm5
	paddd	%xmm0, %xmm4
	paddd	%xmm1, %xmm5
	movq	-8(%rdi), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rdi), %xmm1           # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	pcmpeqd	%xmm2, %xmm0
	pandn	%xmm3, %xmm0
	pcmpeqd	%xmm2, %xmm1
	pandn	%xmm3, %xmm1
	paddd	%xmm4, %xmm0
	paddd	%xmm5, %xmm1
	addq	$32, %rdi
	addq	$-16, %rsi
	jne	.LBB12_9
.LBB12_10:                              # %middle.block
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ebx
	cmpq	%rdx, %rax
	je	.LBB12_13
.LBB12_11:                              # %scalar.ph.preheader
	subq	%rdx, %rax
	leaq	(%rcx,%rdx,2), %rcx
	.p2align	4, 0x90
.LBB12_12:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpw	$1, (%rcx)
	sbbl	$-1, %ebx
	addq	$2, %rcx
	decq	%rax
	jne	.LBB12_12
.LBB12_13:                              # %._crit_edge42.i
	testl	%ebx, %ebx
	je	.LBB12_28
# BB#14:
	leal	(%rbx,%rbx), %ebp
	xorl	%eax, %eax
	movl	%ebp, %edi
	callq	mallocate
	movq	%rax, %r14
	movq	froms(%rip), %rax
	movq	%r14, (%rax)
	xorl	%eax, %eax
	movl	%ebp, %edi
	callq	mallocate
	movq	tos(%rip), %rcx
	movq	%rax, (%rcx)
	movslq	ntokens(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB12_18
# BB#15:                                # %.lr.ph.i
	movq	actrow(%rip), %rdx
	testb	$1, %cl
	jne	.LBB12_19
# BB#16:
	xorl	%edi, %edi
	jmp	.LBB12_17
.LBB12_18:
	movq	%r14, %rsi
	jmp	.LBB12_27
.LBB12_19:
	cmpw	$0, (%rdx)
	je	.LBB12_21
# BB#20:
	movq	%r14, %rsi
	addq	$2, %rsi
	movw	$0, (%r14)
	movzwl	(%rdx), %edi
	movw	%di, (%rax)
	addq	$2, %rax
	movl	$1, %edi
	cmpl	$1, %ecx
	je	.LBB12_27
	jmp	.LBB12_22
.LBB12_21:
	movl	$1, %edi
.LBB12_17:
	movq	%r14, %rsi
	cmpl	$1, %ecx
	je	.LBB12_27
	.p2align	4, 0x90
.LBB12_22:                              # =>This Inner Loop Header: Depth=1
	cmpw	$0, (%rdx,%rdi,2)
	je	.LBB12_24
# BB#23:                                #   in Loop: Header=BB12_22 Depth=1
	movw	%di, (%rsi)
	addq	$2, %rsi
	movzwl	(%rdx,%rdi,2), %ebp
	movw	%bp, (%rax)
	addq	$2, %rax
.LBB12_24:                              #   in Loop: Header=BB12_22 Depth=1
	cmpw	$0, 2(%rdx,%rdi,2)
	je	.LBB12_26
# BB#25:                                #   in Loop: Header=BB12_22 Depth=1
	leal	1(%rdi), %ebp
	movw	%bp, (%rsi)
	addq	$2, %rsi
	movzwl	2(%rdx,%rdi,2), %ebp
	movw	%bp, (%rax)
	addq	$2, %rax
.LBB12_26:                              #   in Loop: Header=BB12_22 Depth=1
	addq	$2, %rdi
	cmpq	%rdi, %rcx
	jne	.LBB12_22
.LBB12_27:                              # %._crit_edge.i
	movq	tally(%rip), %rax
	movw	%bx, (%rax)
	movzwl	-2(%rsi), %eax
	incl	%eax
	subw	(%r14), %ax
	movq	width(%rip), %rcx
	movw	%ax, (%rcx)
.LBB12_28:                              # %save_row.exit.preheader
	movq	ftable(%rip), %rax
	cmpl	$2, nstates(%rip)
	jl	.LBB12_61
# BB#29:                                # %.lr.ph.preheader
	movl	$10, %r15d
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB12_30:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_50 Depth 2
                                        #     Child Loop BB12_40 Depth 2
                                        #     Child Loop BB12_54 Depth 2
	movl	$44, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	cmpl	$10, %r15d
	jl	.LBB12_32
# BB#31:                                #   in Loop: Header=BB12_30 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %r15d
	jmp	.LBB12_33
	.p2align	4, 0x90
.LBB12_32:                              #   in Loop: Header=BB12_30 Depth=1
	incl	%r15d
.LBB12_33:                              #   in Loop: Header=BB12_30 Depth=1
	movl	%r12d, %edi
	callq	action_row
	movl	%eax, %ecx
	movq	ftable(%rip), %rdi
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	fprintf
	movslq	ntokens(%rip), %rax
	testq	%rax, %rax
	pxor	%xmm4, %xmm4
	movdqa	.LCPI12_0(%rip), %xmm5  # xmm5 = [1,1,1,1]
	jle	.LBB12_60
# BB#34:                                # %.lr.ph41.i14
                                        #   in Loop: Header=BB12_30 Depth=1
	movq	actrow(%rip), %rcx
	cmpl	$8, %eax
	jb	.LBB12_38
# BB#35:                                # %min.iters.checked51
                                        #   in Loop: Header=BB12_30 Depth=1
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB12_38
# BB#36:                                # %vector.body47.preheader
                                        #   in Loop: Header=BB12_30 Depth=1
	leaq	-8(%rdx), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	btl	$3, %edi
	jb	.LBB12_48
# BB#37:                                # %vector.body47.prol
                                        #   in Loop: Header=BB12_30 Depth=1
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpcklwd	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1],xmm0[2],xmm4[2],xmm0[3],xmm4[3]
	movq	8(%rcx), %xmm1          # xmm1 = mem[0],zero
	punpcklwd	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1],xmm1[2],xmm4[2],xmm1[3],xmm4[3]
	pcmpeqd	%xmm4, %xmm0
	pandn	%xmm5, %xmm0
	pcmpeqd	%xmm4, %xmm1
	pandn	%xmm5, %xmm1
	movl	$8, %edi
	testq	%rsi, %rsi
	jne	.LBB12_49
	jmp	.LBB12_51
	.p2align	4, 0x90
.LBB12_38:                              #   in Loop: Header=BB12_30 Depth=1
	xorl	%edx, %edx
	xorl	%ebx, %ebx
.LBB12_39:                              # %scalar.ph49.preheader
                                        #   in Loop: Header=BB12_30 Depth=1
	subq	%rdx, %rax
	leaq	(%rcx,%rdx,2), %rcx
	.p2align	4, 0x90
.LBB12_40:                              # %scalar.ph49
                                        #   Parent Loop BB12_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpw	$1, (%rcx)
	sbbl	$-1, %ebx
	addq	$2, %rcx
	decq	%rax
	jne	.LBB12_40
.LBB12_41:                              # %._crit_edge42.i20
                                        #   in Loop: Header=BB12_30 Depth=1
	testl	%ebx, %ebx
	je	.LBB12_60
# BB#42:                                #   in Loop: Header=BB12_30 Depth=1
	leal	(%rbx,%rbx), %ebp
	xorl	%eax, %eax
	movl	%ebp, %edi
	callq	mallocate
	movq	%rax, %r14
	movq	froms(%rip), %rax
	movq	%r14, (%rax,%r12,8)
	xorl	%eax, %eax
	movl	%ebp, %edi
	callq	mallocate
	movq	tos(%rip), %rcx
	movq	%rax, (%rcx,%r12,8)
	movslq	ntokens(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB12_45
# BB#43:                                # %.lr.ph.i21
                                        #   in Loop: Header=BB12_30 Depth=1
	movq	actrow(%rip), %rdx
	testb	$1, %cl
	jne	.LBB12_46
# BB#44:                                #   in Loop: Header=BB12_30 Depth=1
	xorl	%edi, %edi
	movq	%r14, %rsi
	cmpl	$1, %ecx
	jne	.LBB12_54
	jmp	.LBB12_59
.LBB12_45:                              #   in Loop: Header=BB12_30 Depth=1
	movq	%r14, %rsi
	jmp	.LBB12_59
.LBB12_46:                              #   in Loop: Header=BB12_30 Depth=1
	cmpw	$0, (%rdx)
	je	.LBB12_52
# BB#47:                                #   in Loop: Header=BB12_30 Depth=1
	movq	%r14, %rsi
	addq	$2, %rsi
	movw	$0, (%r14)
	movzwl	(%rdx), %edi
	movw	%di, (%rax)
	addq	$2, %rax
	jmp	.LBB12_53
.LBB12_48:                              #   in Loop: Header=BB12_30 Depth=1
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	pxor	%xmm1, %xmm1
	testq	%rsi, %rsi
	je	.LBB12_51
.LBB12_49:                              # %vector.body47.preheader.new
                                        #   in Loop: Header=BB12_30 Depth=1
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	leaq	24(%rcx,%rdi,2), %rdi
	.p2align	4, 0x90
.LBB12_50:                              # %vector.body47
                                        #   Parent Loop BB12_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdi), %xmm2        # xmm2 = mem[0],zero
	punpcklwd	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1],xmm2[2],xmm4[2],xmm2[3],xmm4[3]
	movq	-16(%rdi), %xmm3        # xmm3 = mem[0],zero
	punpcklwd	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1],xmm3[2],xmm4[2],xmm3[3],xmm4[3]
	pcmpeqd	%xmm4, %xmm2
	pandn	%xmm5, %xmm2
	pcmpeqd	%xmm4, %xmm3
	pandn	%xmm5, %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movq	-8(%rdi), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1],xmm0[2],xmm4[2],xmm0[3],xmm4[3]
	movq	(%rdi), %xmm1           # xmm1 = mem[0],zero
	punpcklwd	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1],xmm1[2],xmm4[2],xmm1[3],xmm4[3]
	pcmpeqd	%xmm4, %xmm0
	pandn	%xmm5, %xmm0
	pcmpeqd	%xmm4, %xmm1
	pandn	%xmm5, %xmm1
	paddd	%xmm2, %xmm0
	paddd	%xmm3, %xmm1
	addq	$32, %rdi
	addq	$-16, %rsi
	jne	.LBB12_50
.LBB12_51:                              # %middle.block48
                                        #   in Loop: Header=BB12_30 Depth=1
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ebx
	cmpq	%rdx, %rax
	jne	.LBB12_39
	jmp	.LBB12_41
.LBB12_52:                              #   in Loop: Header=BB12_30 Depth=1
	movq	%r14, %rsi
.LBB12_53:                              # %.prol.loopexit
                                        #   in Loop: Header=BB12_30 Depth=1
	movl	$1, %edi
	cmpl	$1, %ecx
	je	.LBB12_59
	.p2align	4, 0x90
.LBB12_54:                              #   Parent Loop BB12_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpw	$0, (%rdx,%rdi,2)
	je	.LBB12_56
# BB#55:                                #   in Loop: Header=BB12_54 Depth=2
	movw	%di, (%rsi)
	addq	$2, %rsi
	movzwl	(%rdx,%rdi,2), %ebp
	movw	%bp, (%rax)
	addq	$2, %rax
.LBB12_56:                              #   in Loop: Header=BB12_54 Depth=2
	cmpw	$0, 2(%rdx,%rdi,2)
	je	.LBB12_58
# BB#57:                                #   in Loop: Header=BB12_54 Depth=2
	leal	1(%rdi), %ebp
	movw	%bp, (%rsi)
	addq	$2, %rsi
	movzwl	2(%rdx,%rdi,2), %ebp
	movw	%bp, (%rax)
	addq	$2, %rax
.LBB12_58:                              #   in Loop: Header=BB12_54 Depth=2
	addq	$2, %rdi
	cmpq	%rdi, %rcx
	jne	.LBB12_54
.LBB12_59:                              # %._crit_edge.i29
                                        #   in Loop: Header=BB12_30 Depth=1
	movq	tally(%rip), %rax
	movw	%bx, (%rax,%r12,2)
	movzwl	-2(%rsi), %eax
	incl	%eax
	subw	(%r14), %ax
	movq	width(%rip), %rcx
	movw	%ax, (%rcx,%r12,2)
.LBB12_60:                              # %save_row.exit30
                                        #   in Loop: Header=BB12_30 Depth=1
	incq	%r12
	movslq	nstates(%rip), %rcx
	movq	ftable(%rip), %rax
	cmpq	%rcx, %r12
	jl	.LBB12_30
.LBB12_61:                              # %save_row.exit._crit_edge
	movl	$.L.str.13, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	actrow(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB12_63
# BB#62:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB12_63:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	token_actions, .Lfunc_end12-token_actions
	.cfi_endproc

	.globl	free_shifts
	.p2align	4, 0x90
	.type	free_shifts,@function
free_shifts:                            # @free_shifts
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 16
.Lcfi53:
	.cfi_offset %rbx, -16
	movq	shift_table(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB13_2
# BB#1:
	callq	free
.LBB13_2:
	movq	first_shift(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB13_4
	.p2align	4, 0x90
.LBB13_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB13_3
.LBB13_4:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end13:
	.size	free_shifts, .Lfunc_end13-free_shifts
	.cfi_endproc

	.globl	free_reductions
	.p2align	4, 0x90
	.type	free_reductions,@function
free_reductions:                        # @free_reductions
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 16
.Lcfi55:
	.cfi_offset %rbx, -16
	movq	reduction_table(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB14_2
# BB#1:
	callq	free
.LBB14_2:
	movq	first_reduction(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB14_4
	.p2align	4, 0x90
.LBB14_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB14_3
.LBB14_4:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end14:
	.size	free_reductions, .Lfunc_end14-free_reductions
	.cfi_endproc

	.globl	goto_actions
	.p2align	4, 0x90
	.type	goto_actions,@function
goto_actions:                           # @goto_actions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 48
.Lcfi61:
	.cfi_offset %rbx, -48
.Lcfi62:
	.cfi_offset %r12, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movl	nstates(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %r14
	movq	%r14, state_count(%rip)
	movslq	ntokens(%rip), %rax
	movq	goto_map(%rip), %rcx
	movswq	(%rcx,%rax,2), %rbx
	movswq	2(%rcx,%rax,2), %r15
	cmpw	%r15w, %bx
	movl	$-1, %ebp
	je	.LBB15_17
# BB#1:                                 # %.preheader35.i
	movl	nstates(%rip), %eax
	testl	%eax, %eax
	jle	.LBB15_3
# BB#2:                                 # %.lr.ph42.i
	decl	%eax
	leaq	2(%rax,%rax), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	memset
.LBB15_3:                               # %.preheader34.i
	cmpw	%r15w, %bx
	jge	.LBB15_10
# BB#4:                                 # %.lr.ph40.i
	movq	to_state(%rip), %rax
	movl	%r15d, %edx
	subl	%ebx, %edx
	leaq	-1(%r15), %rcx
	subq	%rbx, %rcx
	andq	$3, %rdx
	je	.LBB15_7
# BB#5:                                 # %.prol.preheader53
	negq	%rdx
	.p2align	4, 0x90
.LBB15_6:                               # =>This Inner Loop Header: Depth=1
	movswq	(%rax,%rbx,2), %rsi
	incw	(%r14,%rsi,2)
	incq	%rbx
	incq	%rdx
	jne	.LBB15_6
.LBB15_7:                               # %.prol.loopexit54
	cmpq	$3, %rcx
	jb	.LBB15_10
# BB#8:                                 # %.lr.ph40.i.new
	subq	%rbx, %r15
	leaq	6(%rax,%rbx,2), %rax
	.p2align	4, 0x90
.LBB15_9:                               # =>This Inner Loop Header: Depth=1
	movswq	-6(%rax), %rcx
	incw	(%r14,%rcx,2)
	movswq	-4(%rax), %rcx
	incw	(%r14,%rcx,2)
	movswq	-2(%rax), %rcx
	incw	(%r14,%rcx,2)
	movswq	(%rax), %rcx
	incw	(%r14,%rcx,2)
	addq	$8, %rax
	addq	$-4, %r15
	jne	.LBB15_9
.LBB15_10:                              # %.preheader.i
	movslq	nstates(%rip), %rax
	testq	%rax, %rax
	jle	.LBB15_17
# BB#11:                                # %.lr.ph.i
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB15_12
# BB#13:                                # %.prol.preheader48
	movl	$-1, %ebp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB15_14:                              # =>This Inner Loop Header: Depth=1
	movswl	(%r14,%rcx,2), %ebx
	cmpl	%edx, %ebx
	cmovgl	%ecx, %ebp
	cmovgel	%ebx, %edx
	incq	%rcx
	cmpq	%rcx, %rdi
	jne	.LBB15_14
	jmp	.LBB15_15
.LBB15_12:
	xorl	%ecx, %ecx
	movl	$-1, %ebp
	xorl	%edx, %edx
.LBB15_15:                              # %.prol.loopexit49
	cmpq	$3, %rsi
	jb	.LBB15_17
	.p2align	4, 0x90
.LBB15_16:                              # =>This Inner Loop Header: Depth=1
	movswl	(%r14,%rcx,2), %esi
	cmpl	%edx, %esi
	cmovgl	%ecx, %ebp
	cmovgel	%esi, %edx
	movswl	2(%r14,%rcx,2), %esi
	leal	1(%rcx), %edi
	cmpl	%edx, %esi
	cmovlel	%ebp, %edi
	cmovgel	%esi, %edx
	movswl	4(%r14,%rcx,2), %esi
	leal	2(%rcx), %ebx
	cmpl	%edx, %esi
	cmovlel	%edi, %ebx
	cmovgel	%esi, %edx
	movswl	6(%r14,%rcx,2), %esi
	leal	3(%rcx), %ebp
	cmpl	%edx, %esi
	cmovlel	%ebx, %ebp
	cmovgel	%esi, %edx
	addq	$4, %rcx
	cmpq	%rax, %rcx
	jne	.LBB15_16
.LBB15_17:                              # %default_goto.exit
	movq	ftable(%rip), %rdi
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	fprintf
	movl	ntokens(%rip), %edi
	movl	%ebp, %esi
	callq	save_column
	movslq	ntokens(%rip), %r12
	leaq	1(%r12), %r14
	movq	ftable(%rip), %rax
	cmpl	nsyms(%rip), %r14d
	jge	.LBB15_40
# BB#18:                                # %.lr.ph.preheader
	movl	$10, %r15d
	.p2align	4, 0x90
.LBB15_19:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_28 Depth 2
                                        #     Child Loop BB15_31 Depth 2
                                        #     Child Loop BB15_36 Depth 2
                                        #     Child Loop BB15_38 Depth 2
	movl	$44, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	cmpl	$10, %r15d
	jl	.LBB15_21
# BB#20:                                #   in Loop: Header=BB15_19 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %r15d
	jmp	.LBB15_22
	.p2align	4, 0x90
.LBB15_21:                              #   in Loop: Header=BB15_19 Depth=1
	incl	%r15d
.LBB15_22:                              #   in Loop: Header=BB15_19 Depth=1
	movq	goto_map(%rip), %rax
	movswq	(%rax,%r14,2), %rbp
	movslq	%r12d, %rcx
	movswq	4(%rax,%rcx,2), %r12
	movl	$-1, %ebx
	cmpw	%r12w, %bp
	je	.LBB15_39
# BB#23:                                # %.preheader35.i16
                                        #   in Loop: Header=BB15_19 Depth=1
	movl	nstates(%rip), %eax
	testl	%eax, %eax
	jle	.LBB15_25
# BB#24:                                # %.lr.ph42.i17
                                        #   in Loop: Header=BB15_19 Depth=1
	movq	state_count(%rip), %rdi
	decl	%eax
	leaq	2(%rax,%rax), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB15_25:                              # %.preheader34.i18
                                        #   in Loop: Header=BB15_19 Depth=1
	cmpw	%r12w, %bp
	jge	.LBB15_32
# BB#26:                                # %.lr.ph40.i20
                                        #   in Loop: Header=BB15_19 Depth=1
	movq	state_count(%rip), %rax
	movq	to_state(%rip), %rcx
	movl	%r12d, %esi
	subl	%ebp, %esi
	leaq	-1(%r12), %rdx
	subq	%rbp, %rdx
	andq	$3, %rsi
	je	.LBB15_29
# BB#27:                                # %.prol.preheader
                                        #   in Loop: Header=BB15_19 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB15_28:                              #   Parent Loop BB15_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	(%rcx,%rbp,2), %rdi
	incw	(%rax,%rdi,2)
	incq	%rbp
	incq	%rsi
	jne	.LBB15_28
.LBB15_29:                              # %.prol.loopexit
                                        #   in Loop: Header=BB15_19 Depth=1
	cmpq	$3, %rdx
	jb	.LBB15_32
# BB#30:                                # %.lr.ph40.i20.new
                                        #   in Loop: Header=BB15_19 Depth=1
	subq	%rbp, %r12
	leaq	6(%rcx,%rbp,2), %rcx
	.p2align	4, 0x90
.LBB15_31:                              #   Parent Loop BB15_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	-6(%rcx), %rdx
	incw	(%rax,%rdx,2)
	movswq	-4(%rcx), %rdx
	incw	(%rax,%rdx,2)
	movswq	-2(%rcx), %rdx
	incw	(%rax,%rdx,2)
	movswq	(%rcx), %rdx
	incw	(%rax,%rdx,2)
	addq	$8, %rcx
	addq	$-4, %r12
	jne	.LBB15_31
.LBB15_32:                              # %.preheader.i21
                                        #   in Loop: Header=BB15_19 Depth=1
	movslq	nstates(%rip), %r9
	testq	%r9, %r9
	jle	.LBB15_39
# BB#33:                                # %.lr.ph.i22
                                        #   in Loop: Header=BB15_19 Depth=1
	movq	state_count(%rip), %rcx
	leaq	-1(%r9), %r8
	movq	%r9, %rax
	andq	$3, %rax
	je	.LBB15_34
# BB#35:                                # %.prol.preheader43
                                        #   in Loop: Header=BB15_19 Depth=1
	movl	$-1, %ebx
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB15_36:                              #   Parent Loop BB15_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	(%rcx,%rdx,2), %edi
	cmpl	%esi, %edi
	cmovgl	%edx, %ebx
	cmovgel	%edi, %esi
	incq	%rdx
	cmpq	%rdx, %rax
	jne	.LBB15_36
	jmp	.LBB15_37
.LBB15_34:                              #   in Loop: Header=BB15_19 Depth=1
	xorl	%edx, %edx
	movl	$-1, %ebx
	xorl	%esi, %esi
.LBB15_37:                              # %.prol.loopexit44
                                        #   in Loop: Header=BB15_19 Depth=1
	cmpq	$3, %r8
	jb	.LBB15_39
	.p2align	4, 0x90
.LBB15_38:                              #   Parent Loop BB15_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	(%rcx,%rdx,2), %eax
	cmpl	%esi, %eax
	cmovgl	%edx, %ebx
	cmovgel	%eax, %esi
	movswl	2(%rcx,%rdx,2), %eax
	leal	1(%rdx), %edi
	cmpl	%esi, %eax
	cmovlel	%ebx, %edi
	cmovgel	%eax, %esi
	movswl	4(%rcx,%rdx,2), %eax
	leal	2(%rdx), %ebp
	cmpl	%esi, %eax
	cmovlel	%edi, %ebp
	cmovgel	%eax, %esi
	movswl	6(%rcx,%rdx,2), %eax
	leal	3(%rdx), %ebx
	cmpl	%esi, %eax
	cmovlel	%ebp, %ebx
	cmovgel	%eax, %esi
	addq	$4, %rdx
	cmpq	%r9, %rdx
	jne	.LBB15_38
	.p2align	4, 0x90
.LBB15_39:                              # %default_goto.exit33
                                        #   in Loop: Header=BB15_19 Depth=1
	movq	ftable(%rip), %rdi
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	movl	%r14d, %r12d
	movl	%r12d, %edi
	movl	%ebx, %esi
	callq	save_column
	incq	%r14
	movslq	nsyms(%rip), %rcx
	movq	ftable(%rip), %rax
	cmpq	%rcx, %r14
	jl	.LBB15_19
.LBB15_40:                              # %._crit_edge
	movl	$.L.str.13, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	state_count(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB15_41
# BB#42:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB15_41:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	goto_actions, .Lfunc_end15-goto_actions
	.cfi_endproc

	.globl	sort_actions
	.p2align	4, 0x90
	.type	sort_actions,@function
sort_actions:                           # @sort_actions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi72:
	.cfi_def_cfa_offset 64
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movl	nvectors(%rip), %edi
	addl	%edi, %edi
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, order(%rip)
	movl	$0, nentries(%rip)
	movslq	nvectors(%rip), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	testq	%rcx, %rcx
	jle	.LBB16_23
# BB#1:                                 # %.lr.ph45
	movq	tally(%rip), %r11
	movq	width(%rip), %r9
	movabsq	$-4294967296, %r15      # imm = 0xFFFFFFFF00000000
	movabsq	$-8589934592, %rbx      # imm = 0xFFFFFFFE00000000
	movabsq	$-17179869184, %r10     # imm = 0xFFFFFFFC00000000
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB16_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_4 Depth 2
                                        #     Child Loop BB16_9 Depth 2
                                        #     Child Loop BB16_17 Depth 2
                                        #     Child Loop BB16_20 Depth 2
	movzwl	(%r11,%r12,2), %ecx
	testw	%cx, %cx
	jle	.LBB16_22
# BB#3:                                 #   in Loop: Header=BB16_2 Depth=1
	movq	%rbx, %r13
	movzwl	(%r9,%r12,2), %edx
	movslq	%r14d, %rbx
	movq	%rbx, %rdi
	shlq	$32, %rdi
	addq	%r15, %rdi
	.p2align	4, 0x90
.LBB16_4:                               #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %r8
	movq	%rdi, %rbp
	testq	%r8, %r8
	jle	.LBB16_6
# BB#5:                                 #   in Loop: Header=BB16_4 Depth=2
	leaq	-1(%r8), %rbx
	movswq	-2(%rax,%r8,2), %rsi
	leaq	(%rbp,%r15), %rdi
	cmpw	%dx, (%r9,%rsi,2)
	jl	.LBB16_4
.LBB16_6:                               # %.critedge.preheader
                                        #   in Loop: Header=BB16_2 Depth=1
	testl	%r8d, %r8d
	jle	.LBB16_7
# BB#8:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB16_2 Depth=1
	sarq	$32, %rbp
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB16_9:                               # %.lr.ph
                                        #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	(%rax,%rbp,2), %rdi
	cmpw	%dx, (%r9,%rdi,2)
	jne	.LBB16_12
# BB#10:                                #   in Loop: Header=BB16_9 Depth=2
	cmpw	%cx, (%r11,%rdi,2)
	jge	.LBB16_12
# BB#11:                                # %.critedge
                                        #   in Loop: Header=BB16_9 Depth=2
	leaq	-1(%rbp), %r8
	testq	%rbp, %rbp
	movq	%r8, %rbp
	jg	.LBB16_9
	jmp	.LBB16_13
	.p2align	4, 0x90
.LBB16_12:                              # %.lr.ph..critedge1.loopexit_crit_edge
                                        #   in Loop: Header=BB16_2 Depth=1
	movl	%ebp, %r8d
	jmp	.LBB16_13
	.p2align	4, 0x90
.LBB16_7:                               #   in Loop: Header=BB16_2 Depth=1
	decl	%r8d
	movq	%r13, %rbx
.LBB16_13:                              # %.critedge1
                                        #   in Loop: Header=BB16_2 Depth=1
	leal	-1(%r14), %ecx
	movl	%ecx, %edx
	subl	%r8d, %edx
	jle	.LBB16_21
# BB#14:                                # %.lr.ph42.preheader
                                        #   in Loop: Header=BB16_2 Depth=1
	movslq	%ecx, %rdi
	movslq	%r8d, %r13
	leaq	-1(%rdi), %rbp
	subq	%r13, %rbp
	andq	$3, %rdx
	je	.LBB16_15
# BB#16:                                # %.lr.ph42.prol.preheader
                                        #   in Loop: Header=BB16_2 Depth=1
	negq	%rdx
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB16_17:                              # %.lr.ph42.prol
                                        #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rax,%rdi,2), %esi
	movslq	%ecx, %rcx
	movw	%si, (%rax,%rcx,2)
	decq	%rdi
	decl	%ecx
	incq	%rdx
	jne	.LBB16_17
	jmp	.LBB16_18
.LBB16_15:                              #   in Loop: Header=BB16_2 Depth=1
	movl	%r14d, %ecx
.LBB16_18:                              # %.lr.ph42.prol.loopexit
                                        #   in Loop: Header=BB16_2 Depth=1
	cmpq	$3, %rbp
	jb	.LBB16_21
# BB#19:                                # %.lr.ph42.preheader.new
                                        #   in Loop: Header=BB16_2 Depth=1
	movq	%rdi, %rdx
	shlq	$32, %rdx
	.p2align	4, 0x90
.LBB16_20:                              # %.lr.ph42
                                        #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rax,%rdi,2), %esi
	movslq	%ecx, %rcx
	movw	%si, (%rax,%rcx,2)
	movzwl	-2(%rax,%rdi,2), %ecx
	movq	%rdx, %rsi
	sarq	$31, %rsi
	movw	%cx, (%rax,%rsi)
	movzwl	-4(%rax,%rdi,2), %ecx
	leaq	(%rdx,%r15), %rsi
	sarq	$31, %rsi
	movw	%cx, (%rax,%rsi)
	movzwl	-6(%rax,%rdi,2), %ecx
	leaq	(%rdx,%rbx), %rsi
	sarq	$31, %rsi
	movw	%cx, (%rax,%rsi)
	leal	-3(%rdi), %ecx
	leaq	-4(%rdi), %rdi
	addq	%r10, %rdx
	cmpq	%r13, %rdi
	jg	.LBB16_20
.LBB16_21:                              # %._crit_edge
                                        #   in Loop: Header=BB16_2 Depth=1
	movslq	%r8d, %rcx
	movw	%r12w, 2(%rax,%rcx,2)
	incl	%r14d
	movl	%r14d, nentries(%rip)
.LBB16_22:                              #   in Loop: Header=BB16_2 Depth=1
	incq	%r12
	cmpq	(%rsp), %r12            # 8-byte Folded Reload
	jl	.LBB16_2
.LBB16_23:                              # %._crit_edge46
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	sort_actions, .Lfunc_end16-sort_actions
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_0:
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.text
	.globl	pack_table
	.p2align	4, 0x90
	.type	pack_table,@function
pack_table:                             # @pack_table
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi81:
	.cfi_def_cfa_offset 32
.Lcfi82:
	.cfi_offset %rbx, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movl	nvectors(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, base(%rip)
	movl	nentries(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, pos(%rip)
	movl	$65534, %edi            # imm = 0xFFFE
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, table(%rip)
	movl	$65534, %edi            # imm = 0xFFFE
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, check(%rip)
	movl	$0, lowzero(%rip)
	movl	$0, high(%rip)
	movslq	nvectors(%rip), %rbx
	testq	%rbx, %rbx
	jle	.LBB17_13
# BB#1:                                 # %.lr.ph36
	movq	base(%rip), %rcx
	cmpl	$15, %ebx
	jbe	.LBB17_2
# BB#4:                                 # %min.iters.checked
	movq	%rbx, %rdx
	andq	$-16, %rdx
	je	.LBB17_2
# BB#5:                                 # %vector.body.preheader
	leaq	-16(%rdx), %rbp
	movl	%ebp, %esi
	shrl	$4, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB17_6
# BB#7:                                 # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	movaps	.LCPI17_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB17_8:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, (%rcx,%rdi,2)
	movups	%xmm0, 16(%rcx,%rdi,2)
	addq	$16, %rdi
	incq	%rsi
	jne	.LBB17_8
	jmp	.LBB17_9
.LBB17_2:
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB17_3:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movw	$-32768, (%rcx,%rdx,2)  # imm = 0x8000
	incq	%rdx
	cmpq	%rbx, %rdx
	jl	.LBB17_3
.LBB17_13:                              # %.preheader29
	movl	$255, %esi
	movl	$65534, %edx            # imm = 0xFFFE
	movq	%rax, %rdi
	callq	memset
	movl	nentries(%rip), %ebp
	testl	%ebp, %ebp
	jle	.LBB17_18
# BB#14:                                # %.lr.ph32.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB17_15:                              # %.lr.ph32
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edi
	callq	matching_state
	testl	%eax, %eax
	js	.LBB17_16
# BB#31:                                #   in Loop: Header=BB17_15 Depth=1
	movq	base(%rip), %rcx
	cltq
	movswl	(%rcx,%rax,2), %eax
	jmp	.LBB17_32
	.p2align	4, 0x90
.LBB17_16:                              #   in Loop: Header=BB17_15 Depth=1
	movl	%ebx, %edi
	callq	pack_vector
	movq	base(%rip), %rcx
	movl	nentries(%rip), %ebp
.LBB17_32:                              #   in Loop: Header=BB17_15 Depth=1
	movq	pos(%rip), %rdx
	movw	%ax, (%rdx,%rbx,2)
	movq	order(%rip), %rdx
	movswq	(%rdx,%rbx,2), %rdx
	movw	%ax, (%rcx,%rdx,2)
	incq	%rbx
	movslq	%ebp, %rax
	cmpq	%rax, %rbx
	jl	.LBB17_15
# BB#17:                                # %.preheader.loopexit
	movl	nvectors(%rip), %ebx
.LBB17_18:                              # %.preheader
	movq	froms(%rip), %rdi
	testl	%ebx, %ebx
	jle	.LBB17_25
# BB#19:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB17_20:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB17_22
# BB#21:                                #   in Loop: Header=BB17_20 Depth=1
	callq	free
.LBB17_22:                              #   in Loop: Header=BB17_20 Depth=1
	movq	tos(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB17_24
# BB#23:                                #   in Loop: Header=BB17_20 Depth=1
	callq	free
.LBB17_24:                              #   in Loop: Header=BB17_20 Depth=1
	incq	%rbx
	movslq	nvectors(%rip), %rax
	movq	froms(%rip), %rdi
	cmpq	%rax, %rbx
	jl	.LBB17_20
.LBB17_25:                              # %._crit_edge
	testq	%rdi, %rdi
	je	.LBB17_27
# BB#26:
	callq	free
.LBB17_27:
	movq	tos(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB17_29
# BB#28:
	callq	free
.LBB17_29:
	movq	pos(%rip), %rdi
	addq	$8, %rsp
	testq	%rdi, %rdi
	je	.LBB17_33
# BB#30:
	popq	%rbx
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB17_33:
	popq	%rbx
	popq	%rbp
	retq
.LBB17_6:
	xorl	%edi, %edi
.LBB17_9:                               # %vector.body.prol.loopexit
	cmpq	$112, %rbp
	jb	.LBB17_12
# BB#10:                                # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	leaq	240(%rcx,%rdi,2), %rdi
	movaps	.LCPI17_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB17_11:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rdi)
	movups	%xmm0, -224(%rdi)
	movups	%xmm0, -208(%rdi)
	movups	%xmm0, -192(%rdi)
	movups	%xmm0, -176(%rdi)
	movups	%xmm0, -160(%rdi)
	movups	%xmm0, -144(%rdi)
	movups	%xmm0, -128(%rdi)
	movups	%xmm0, -112(%rdi)
	movups	%xmm0, -96(%rdi)
	movups	%xmm0, -80(%rdi)
	movups	%xmm0, -64(%rdi)
	movups	%xmm0, -48(%rdi)
	movups	%xmm0, -32(%rdi)
	movups	%xmm0, -16(%rdi)
	movups	%xmm0, (%rdi)
	addq	$256, %rdi              # imm = 0x100
	addq	$-128, %rsi
	jne	.LBB17_11
.LBB17_12:                              # %middle.block
	cmpq	%rdx, %rbx
	jne	.LBB17_3
	jmp	.LBB17_13
.Lfunc_end17:
	.size	pack_table, .Lfunc_end17-pack_table
	.cfi_endproc

	.globl	output_base
	.p2align	4, 0x90
	.type	output_base,@function
output_base:                            # @output_base
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi86:
	.cfi_def_cfa_offset 32
.Lcfi87:
	.cfi_offset %rbx, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movq	ftable(%rip), %rdi
	movq	base(%rip), %rax
	movswl	(%rax), %edx
	movl	$.L.str.36, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	nstates(%rip), %eax
	movq	ftable(%rip), %rcx
	cmpl	$2, %eax
	jl	.LBB18_6
# BB#1:                                 # %.lr.ph23.preheader
	movl	$10, %ebp
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB18_2:                               # %.lr.ph23
                                        # =>This Inner Loop Header: Depth=1
	movl	$44, %edi
	movq	%rcx, %rsi
	callq	_IO_putc
	cmpl	$10, %ebp
	jl	.LBB18_4
# BB#3:                                 #   in Loop: Header=BB18_2 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %ebp
	jmp	.LBB18_5
	.p2align	4, 0x90
.LBB18_4:                               #   in Loop: Header=BB18_2 Depth=1
	incl	%ebp
.LBB18_5:                               #   in Loop: Header=BB18_2 Depth=1
	movq	ftable(%rip), %rdi
	movq	base(%rip), %rax
	movswl	(%rax,%rbx,2), %edx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
	incq	%rbx
	movslq	nstates(%rip), %rax
	movq	ftable(%rip), %rcx
	cmpq	%rax, %rbx
	jl	.LBB18_2
.LBB18_6:                               # %._crit_edge24
	movq	base(%rip), %rdx
	cltq
	movswl	(%rdx,%rax,2), %edx
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
	movslq	nstates(%rip), %rbx
	incq	%rbx
	movq	ftable(%rip), %rax
	cmpl	nvectors(%rip), %ebx
	jge	.LBB18_12
# BB#7:                                 # %.lr.ph.preheader
	movl	$10, %ebp
	.p2align	4, 0x90
.LBB18_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$44, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	cmpl	$10, %ebp
	jl	.LBB18_10
# BB#9:                                 #   in Loop: Header=BB18_8 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %ebp
	jmp	.LBB18_11
	.p2align	4, 0x90
.LBB18_10:                              #   in Loop: Header=BB18_8 Depth=1
	incl	%ebp
.LBB18_11:                              #   in Loop: Header=BB18_8 Depth=1
	movq	ftable(%rip), %rdi
	movq	base(%rip), %rax
	movswl	(%rax,%rbx,2), %edx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
	incq	%rbx
	movslq	nvectors(%rip), %rcx
	movq	ftable(%rip), %rax
	cmpq	%rcx, %rbx
	jl	.LBB18_8
.LBB18_12:                              # %._crit_edge
	movl	$.L.str.13, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	base(%rip), %rdi
	addq	$8, %rsp
	testq	%rdi, %rdi
	je	.LBB18_13
# BB#14:
	popq	%rbx
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB18_13:
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end18:
	.size	output_base, .Lfunc_end18-output_base
	.cfi_endproc

	.globl	output_table
	.p2align	4, 0x90
	.type	output_table,@function
output_table:                           # @output_table
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 32
.Lcfi92:
	.cfi_offset %rbx, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movq	ftable(%rip), %rdi
	movl	high(%rip), %edx
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	ftable(%rip), %rdi
	movq	table(%rip), %rax
	movswl	(%rax), %edx
	movl	$.L.str.39, %esi
	xorl	%eax, %eax
	callq	fprintf
	cmpl	$0, high(%rip)
	movq	ftable(%rip), %rax
	jle	.LBB19_6
# BB#1:                                 # %.lr.ph.preheader
	movl	$10, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB19_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$44, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	cmpl	$10, %ebp
	jl	.LBB19_4
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %ebp
	jmp	.LBB19_5
	.p2align	4, 0x90
.LBB19_4:                               #   in Loop: Header=BB19_2 Depth=1
	incl	%ebp
.LBB19_5:                               #   in Loop: Header=BB19_2 Depth=1
	movq	ftable(%rip), %rdi
	movq	table(%rip), %rax
	movswl	2(%rax,%rbx,2), %edx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
	movslq	high(%rip), %rcx
	movq	ftable(%rip), %rax
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB19_2
.LBB19_6:                               # %._crit_edge
	movl	$.L.str.13, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	table(%rip), %rdi
	addq	$8, %rsp
	testq	%rdi, %rdi
	je	.LBB19_7
# BB#8:
	popq	%rbx
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB19_7:
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end19:
	.size	output_table, .Lfunc_end19-output_table
	.cfi_endproc

	.globl	output_check
	.p2align	4, 0x90
	.type	output_check,@function
output_check:                           # @output_check
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi96:
	.cfi_def_cfa_offset 32
.Lcfi97:
	.cfi_offset %rbx, -24
.Lcfi98:
	.cfi_offset %rbp, -16
	movq	ftable(%rip), %rdi
	movq	check(%rip), %rax
	movswl	(%rax), %edx
	movl	$.L.str.40, %esi
	xorl	%eax, %eax
	callq	fprintf
	cmpl	$0, high(%rip)
	movq	ftable(%rip), %rax
	jle	.LBB20_6
# BB#1:                                 # %.lr.ph.preheader
	movl	$10, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$44, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	cmpl	$10, %ebp
	jl	.LBB20_4
# BB#3:                                 #   in Loop: Header=BB20_2 Depth=1
	movq	ftable(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, %ebp
	jmp	.LBB20_5
	.p2align	4, 0x90
.LBB20_4:                               #   in Loop: Header=BB20_2 Depth=1
	incl	%ebp
.LBB20_5:                               #   in Loop: Header=BB20_2 Depth=1
	movq	ftable(%rip), %rdi
	movq	check(%rip), %rax
	movswl	2(%rax,%rbx,2), %edx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
	movslq	high(%rip), %rcx
	movq	ftable(%rip), %rax
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB20_2
.LBB20_6:                               # %._crit_edge
	movl	$.L.str.13, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	check(%rip), %rdi
	addq	$8, %rsp
	testq	%rdi, %rdi
	je	.LBB20_7
# BB#8:
	popq	%rbx
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB20_7:
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end20:
	.size	output_check, .Lfunc_end20-output_check
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI21_0:
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.text
	.globl	action_row
	.p2align	4, 0x90
	.type	action_row,@function
action_row:                             # @action_row
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi102:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi103:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 56
	subq	$568, %rsp              # imm = 0x238
.Lcfi105:
	.cfi_def_cfa_offset 624
.Lcfi106:
	.cfi_offset %rbx, -56
.Lcfi107:
	.cfi_offset %r12, -48
.Lcfi108:
	.cfi_offset %r13, -40
.Lcfi109:
	.cfi_offset %r14, -32
.Lcfi110:
	.cfi_offset %r15, -24
.Lcfi111:
	.cfi_offset %rbp, -16
	movl	%edi, %ebx
	movl	ntokens(%rip), %eax
	testl	%eax, %eax
	jle	.LBB21_2
# BB#1:                                 # %.lr.ph177
	movq	actrow(%rip), %rdi
	decl	%eax
	leaq	2(%rax,%rax), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB21_2:                               # %._crit_edge178
	movq	reduction_table(%rip), %rax
	movslq	%ebx, %r12
	movq	(%rax,%r12,8), %r15
	xorl	%r13d, %r13d
	testq	%r15, %r15
	je	.LBB21_3
# BB#4:
	movswl	10(%r15), %eax
	testl	%eax, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jle	.LBB21_5
# BB#7:
	movq	lookaheads(%rip), %rax
	movswl	(%rax,%r12,2), %ecx
	movswl	2(%rax,%r12,2), %r14d
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	cmpw	%cx, %r14w
	jle	.LBB21_25
# BB#8:                                 # %.lr.ph174
	movl	%r14d, 8(%rsp)          # 4-byte Spill
	movl	ntokens(%rip), %r14d
	testl	%r14d, %r14d
	jle	.LBB21_9
# BB#10:                                # %.lr.ph174.split.us.preheader
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	LAruleno(%rip), %r12
	movq	LA(%rip), %rdx
	movq	actrow(%rip), %rcx
	movslq	8(%rsp), %rax           # 4-byte Folded Reload
	movslq	tokensetsize(%rip), %rbp
	movslq	4(%rsp), %rbx           # 4-byte Folded Reload
	movl	%r14d, %esi
	andl	$1, %esi
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	2(%rcx), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	$1, %r8d
	.p2align	4, 0x90
.LBB21_11:                              # %.lr.ph174.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_18 Depth 2
	movzwl	-2(%r12,%rax,2), %ecx
	xorl	%r11d, %r11d
	subw	%cx, %r11w
	decq	%rax
	movq	%rbp, %rcx
	imulq	%rax, %rcx
	testq	%rsi, %rsi
	leaq	(%rdx,%rcx,4), %r10
	jne	.LBB21_13
# BB#12:                                #   in Loop: Header=BB21_11 Depth=1
	xorl	%edi, %edi
	movl	$1, %r9d
	cmpl	$1, %r14d
	jne	.LBB21_17
	jmp	.LBB21_23
	.p2align	4, 0x90
.LBB21_13:                              #   in Loop: Header=BB21_11 Depth=1
	movl	$2, %r9d
	testb	$1, (%r10)
	je	.LBB21_15
# BB#14:                                #   in Loop: Header=BB21_11 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movw	%r11w, (%rcx)
.LBB21_15:                              # %.prol.loopexit318
                                        #   in Loop: Header=BB21_11 Depth=1
	movl	$1, %edi
	cmpl	$1, %r14d
	je	.LBB21_23
.LBB21_17:                              # %.lr.ph174.split.us.new
                                        #   in Loop: Header=BB21_11 Depth=1
	movq	%r14, %rcx
	subq	%rdi, %rcx
	movq	%rsi, %r13
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdi,2), %r15
	movq	%r13, %rsi
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB21_18:                              #   Parent Loop BB21_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	(%r10), %r9d
	je	.LBB21_20
# BB#19:                                #   in Loop: Header=BB21_18 Depth=2
	movw	%r11w, -2(%r15)
.LBB21_20:                              #   in Loop: Header=BB21_18 Depth=2
	addl	%r9d, %r9d
	leaq	4(%r10), %rdi
	testl	%r9d, %r9d
	cmovel	%r8d, %r9d
	cmovneq	%r10, %rdi
	testl	(%rdi), %r9d
	je	.LBB21_22
# BB#21:                                #   in Loop: Header=BB21_18 Depth=2
	movw	%r11w, (%r15)
.LBB21_22:                              #   in Loop: Header=BB21_18 Depth=2
	addl	%r9d, %r9d
	leaq	4(%rdi), %r10
	testl	%r9d, %r9d
	cmovel	%r8d, %r9d
	cmovneq	%rdi, %r10
	addq	$4, %r15
	addq	$-2, %rcx
	jne	.LBB21_18
.LBB21_23:                              # %..loopexit139_crit_edge.us
                                        #   in Loop: Header=BB21_11 Depth=1
	cmpq	%rbx, %rax
	jg	.LBB21_11
# BB#24:
	movq	32(%rsp), %r15          # 8-byte Reload
	movl	8(%rsp), %r14d          # 4-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	jmp	.LBB21_25
.LBB21_3:
                                        # implicit-def: %R14D
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	jmp	.LBB21_6
.LBB21_5:
                                        # implicit-def: %R14D
.LBB21_6:                               # %.loopexit140
                                        # implicit-def: %EAX
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB21_25:                              # %.loopexit140
	movq	shift_table(%rip), %rax
	movq	(%rax,%r12,8), %rax
	testq	%rax, %rax
	je	.LBB21_32
# BB#26:
	movswq	10(%rax), %rcx
	testq	%rcx, %rcx
	jle	.LBB21_32
# BB#27:                                # %.lr.ph162
	movq	accessing_symbol(%rip), %r9
	movl	ntokens(%rip), %r11d
	movq	actrow(%rip), %r10
	xorl	%ebx, %ebx
	movl	error_token_number(%rip), %edi
	movl	$1, %r8d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB21_28:                              # =>This Inner Loop Header: Depth=1
	movswq	12(%rax,%rbx,2), %rdx
	testq	%rdx, %rdx
	je	.LBB21_31
# BB#29:                                #   in Loop: Header=BB21_28 Depth=1
	movswl	(%r9,%rdx,2), %ebp
	cmpl	%r11d, %ebp
	jge	.LBB21_32
# BB#30:                                #   in Loop: Header=BB21_28 Depth=1
	movslq	%ebp, %rsi
	movw	%dx, (%r10,%rsi,2)
	cmpl	%edi, %ebp
	cmovel	%r8d, %r13d
.LBB21_31:                              #   in Loop: Header=BB21_28 Depth=1
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB21_28
.LBB21_32:                              # %.loopexit138
	movq	err_table(%rip), %rax
	movq	(%rax,%r12,8), %rdx
	testq	%rdx, %rdx
	je	.LBB21_39
# BB#33:
	movswq	(%rdx), %rsi
	testq	%rsi, %rsi
	jle	.LBB21_39
# BB#34:                                # %.lr.ph158
	movq	actrow(%rip), %rax
	movl	%esi, %ecx
	leaq	-1(%rcx), %rdi
	xorl	%ebp, %ebp
	andq	$3, %rsi
	je	.LBB21_36
	.p2align	4, 0x90
.LBB21_35:                              # =>This Inner Loop Header: Depth=1
	movswq	2(%rdx,%rbp,2), %rbx
	movw	$-32768, (%rax,%rbx,2)  # imm = 0x8000
	incq	%rbp
	cmpq	%rbp, %rsi
	jne	.LBB21_35
.LBB21_36:                              # %.prol.loopexit
	cmpq	$3, %rdi
	jb	.LBB21_39
# BB#37:                                # %.lr.ph158.new
	subq	%rbp, %rcx
	leaq	8(%rdx,%rbp,2), %rdx
	.p2align	4, 0x90
.LBB21_38:                              # =>This Inner Loop Header: Depth=1
	movswq	-6(%rdx), %rsi
	movw	$-32768, (%rax,%rsi,2)  # imm = 0x8000
	movswq	-4(%rdx), %rsi
	movw	$-32768, (%rax,%rsi,2)  # imm = 0x8000
	movswq	-2(%rdx), %rsi
	movw	$-32768, (%rax,%rsi,2)  # imm = 0x8000
	movswq	(%rdx), %rsi
	movw	$-32768, (%rax,%rsi,2)  # imm = 0x8000
	addq	$8, %rdx
	addq	$-4, %rcx
	jne	.LBB21_38
.LBB21_39:                              # %.loopexit137
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB21_100
# BB#40:                                # %.loopexit137
	testl	%r13d, %r13d
	jne	.LBB21_100
# BB#41:
	movq	consistent(%rip), %rax
	cmpb	$0, (%rax,%r12)
	je	.LBB21_42
# BB#145:
	movswl	12(%r15), %eax
.LBB21_99:
	testl	%eax, %eax
	jne	.LBB21_144
	jmp	.LBB21_100
.LBB21_42:                              # %.preheader136
	cmpl	%r14d, 4(%rsp)          # 4-byte Folded Reload
	jge	.LBB21_100
# BB#43:                                # %.lr.ph152
	movl	ntokens(%rip), %r12d
	testl	%r12d, %r12d
	jle	.LBB21_143
# BB#44:                                # %.lr.ph152.split.us.preheader
	movq	LAruleno(%rip), %r10
	movq	actrow(%rip), %r9
	movslq	4(%rsp), %rsi           # 4-byte Folded Reload
	movslq	%r14d, %r14
	movl	%r12d, %r8d
	andl	$7, %r8d
	movq	%r12, %r15
	subq	%r8, %r15
	leaq	8(%r9), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB21_45:                              # %.lr.ph152.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_49 Depth 2
                                        #     Child Loop BB21_52 Depth 2
	movswl	(%r10,%rsi,2), %ebp
	negl	%ebp
	cmpl	$8, %r12d
	jb	.LBB21_46
# BB#47:                                # %min.iters.checked
                                        #   in Loop: Header=BB21_45 Depth=1
	testq	%r15, %r15
	je	.LBB21_46
# BB#48:                                # %vector.ph
                                        #   in Loop: Header=BB21_45 Depth=1
	movd	%ebp, %xmm0
	pshufd	$0, %xmm0, %xmm2        # xmm2 = xmm0[0,0,0,0]
	pxor	%xmm0, %xmm0
	movq	%r15, %rcx
	movq	16(%rsp), %rbx          # 8-byte Reload
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB21_49:                              # %vector.body
                                        #   Parent Loop BB21_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbx), %xmm3         # xmm3 = mem[0],zero
	punpcklwd	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm3
	movq	(%rbx), %xmm4           # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	pcmpeqd	%xmm2, %xmm3
	psrld	$31, %xmm3
	pcmpeqd	%xmm2, %xmm4
	psrld	$31, %xmm4
	paddd	%xmm3, %xmm0
	paddd	%xmm4, %xmm1
	addq	$16, %rbx
	addq	$-8, %rcx
	jne	.LBB21_49
# BB#50:                                # %middle.block
                                        #   in Loop: Header=BB21_45 Depth=1
	testl	%r8d, %r8d
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %ecx
	movq	%r15, %rdi
	jne	.LBB21_51
	jmp	.LBB21_53
	.p2align	4, 0x90
.LBB21_46:                              #   in Loop: Header=BB21_45 Depth=1
	xorl	%edi, %edi
	xorl	%ecx, %ecx
.LBB21_51:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB21_45 Depth=1
	leaq	(%r9,%rdi,2), %rbx
	movq	%r12, %rdx
	subq	%rdi, %rdx
	.p2align	4, 0x90
.LBB21_52:                              # %scalar.ph
                                        #   Parent Loop BB21_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	(%rbx), %edi
	xorl	%r11d, %r11d
	cmpl	%ebp, %edi
	sete	%r11b
	addl	%r11d, %ecx
	addq	$2, %rbx
	decq	%rdx
	jne	.LBB21_52
.LBB21_53:                              # %._crit_edge148.us
                                        #   in Loop: Header=BB21_45 Depth=1
	cmpl	%r13d, %ecx
	cmovgl	%ebp, %eax
	cmovgel	%ecx, %r13d
	incq	%rsi
	cmpq	%r14, %rsi
	jne	.LBB21_45
# BB#54:                                # %._crit_edge153
	testl	%r13d, %r13d
	jle	.LBB21_99
# BB#55:                                # %.preheader
	movslq	ntokens(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB21_98
# BB#56:                                # %.lr.ph143
	movq	actrow(%rip), %rbx
	cmpl	$16, %ecx
	jae	.LBB21_58
# BB#57:
	xorl	%esi, %esi
	jmp	.LBB21_95
.LBB21_9:
	movl	8(%rsp), %r14d          # 4-byte Reload
	jmp	.LBB21_25
.LBB21_58:                              # %min.iters.checked217
	movq	%rcx, %rsi
	andq	$-16, %rsi
	je	.LBB21_59
# BB#60:                                # %vector.ph221
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	%rsi, %rdi
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB21_61:                              # %vector.body213
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %xmm1           # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	8(%rdx), %xmm2          # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	psrad	$16, %xmm4
	movq	24(%rdx), %xmm2         # xmm2 = mem[0],zero
	movq	16(%rdx), %xmm3         # xmm3 = mem[0],zero
	pcmpeqd	%xmm0, %xmm4
	pshuflw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pcmpeqd	%xmm0, %xmm1
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpcklqdq	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0]
	psllw	$15, %xmm1
	psraw	$15, %xmm1
	movdqa	%xmm1, 544(%rsp)
	testb	$1, 544(%rsp)
	je	.LBB21_63
# BB#62:                                # %pred.store.if
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, (%rdx)
.LBB21_63:                              # %pred.store.continue
                                        #   in Loop: Header=BB21_61 Depth=1
	movdqa	%xmm1, 528(%rsp)
	testb	$1, 530(%rsp)
	je	.LBB21_65
# BB#64:                                # %pred.store.if234
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 2(%rdx)
.LBB21_65:                              # %pred.store.continue235
                                        #   in Loop: Header=BB21_61 Depth=1
	movdqa	%xmm1, 512(%rsp)
	testb	$1, 516(%rsp)
	je	.LBB21_67
# BB#66:                                # %pred.store.if236
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 4(%rdx)
.LBB21_67:                              # %pred.store.continue237
                                        #   in Loop: Header=BB21_61 Depth=1
	movdqa	%xmm1, 496(%rsp)
	testb	$1, 502(%rsp)
	je	.LBB21_69
# BB#68:                                # %pred.store.if238
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 6(%rdx)
.LBB21_69:                              # %pred.store.continue239
                                        #   in Loop: Header=BB21_61 Depth=1
	movdqa	%xmm1, 480(%rsp)
	testb	$1, 488(%rsp)
	je	.LBB21_71
# BB#70:                                # %pred.store.if240
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 8(%rdx)
.LBB21_71:                              # %pred.store.continue241
                                        #   in Loop: Header=BB21_61 Depth=1
	movdqa	%xmm1, 464(%rsp)
	testb	$1, 474(%rsp)
	je	.LBB21_73
# BB#72:                                # %pred.store.if242
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 10(%rdx)
.LBB21_73:                              # %pred.store.continue243
                                        #   in Loop: Header=BB21_61 Depth=1
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	punpcklwd	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1,2,2,3,3]
	movdqa	%xmm1, 448(%rsp)
	testb	$1, 460(%rsp)
	je	.LBB21_75
# BB#74:                                # %pred.store.if244
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 12(%rdx)
.LBB21_75:                              # %pred.store.continue245
                                        #   in Loop: Header=BB21_61 Depth=1
	psrad	$16, %xmm2
	psrad	$16, %xmm3
	movdqa	%xmm1, 432(%rsp)
	testb	$1, 446(%rsp)
	je	.LBB21_77
# BB#76:                                # %pred.store.if246
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 14(%rdx)
.LBB21_77:                              # %pred.store.continue247
                                        #   in Loop: Header=BB21_61 Depth=1
	pcmpeqd	%xmm0, %xmm3
	pshuflw	$232, %xmm3, %xmm1      # xmm1 = xmm3[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pcmpeqd	%xmm0, %xmm2
	pshuflw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpcklqdq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	psllw	$15, %xmm1
	psraw	$15, %xmm1
	movdqa	%xmm1, 416(%rsp)
	testb	$1, 416(%rsp)
	je	.LBB21_79
# BB#78:                                # %pred.store.if248
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 16(%rdx)
.LBB21_79:                              # %pred.store.continue249
                                        #   in Loop: Header=BB21_61 Depth=1
	movdqa	%xmm1, 400(%rsp)
	testb	$1, 402(%rsp)
	je	.LBB21_81
# BB#80:                                # %pred.store.if250
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 18(%rdx)
.LBB21_81:                              # %pred.store.continue251
                                        #   in Loop: Header=BB21_61 Depth=1
	movdqa	%xmm1, 384(%rsp)
	testb	$1, 388(%rsp)
	je	.LBB21_83
# BB#82:                                # %pred.store.if252
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 20(%rdx)
.LBB21_83:                              # %pred.store.continue253
                                        #   in Loop: Header=BB21_61 Depth=1
	movdqa	%xmm1, 368(%rsp)
	testb	$1, 374(%rsp)
	je	.LBB21_85
# BB#84:                                # %pred.store.if254
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 22(%rdx)
.LBB21_85:                              # %pred.store.continue255
                                        #   in Loop: Header=BB21_61 Depth=1
	movdqa	%xmm1, 352(%rsp)
	testb	$1, 360(%rsp)
	je	.LBB21_87
# BB#86:                                # %pred.store.if256
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 24(%rdx)
.LBB21_87:                              # %pred.store.continue257
                                        #   in Loop: Header=BB21_61 Depth=1
	movdqa	%xmm1, 336(%rsp)
	testb	$1, 346(%rsp)
	je	.LBB21_89
# BB#88:                                # %pred.store.if258
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 26(%rdx)
.LBB21_89:                              # %pred.store.continue259
                                        #   in Loop: Header=BB21_61 Depth=1
	movdqa	%xmm1, 320(%rsp)
	testb	$1, 332(%rsp)
	je	.LBB21_91
# BB#90:                                # %pred.store.if260
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 28(%rdx)
.LBB21_91:                              # %pred.store.continue261
                                        #   in Loop: Header=BB21_61 Depth=1
	movdqa	%xmm1, 304(%rsp)
	testb	$1, 318(%rsp)
	je	.LBB21_93
# BB#92:                                # %pred.store.if262
                                        #   in Loop: Header=BB21_61 Depth=1
	movw	$0, 30(%rdx)
.LBB21_93:                              # %pred.store.continue263
                                        #   in Loop: Header=BB21_61 Depth=1
	addq	$32, %rdx
	addq	$-16, %rdi
	jne	.LBB21_61
# BB#94:                                # %middle.block214
	cmpq	%rsi, %rcx
	jne	.LBB21_95
	jmp	.LBB21_98
.LBB21_59:
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB21_95:                              # %scalar.ph215
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%rbx,%rsi,2), %edx
	cmpl	%eax, %edx
	jne	.LBB21_97
# BB#96:                                #   in Loop: Header=BB21_95 Depth=1
	movw	$0, (%rbx,%rsi,2)
.LBB21_97:                              #   in Loop: Header=BB21_95 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB21_95
.LBB21_98:                              # %._crit_edge
	negl	%eax
	testl	%eax, %eax
	jne	.LBB21_144
.LBB21_100:                             # %.thread.preheader
	movslq	ntokens(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB21_143
# BB#101:                               # %.lr.ph
	movq	actrow(%rip), %rdx
	cmpl	$16, %ecx
	jae	.LBB21_103
# BB#102:
	xorl	%esi, %esi
	jmp	.LBB21_140
.LBB21_103:                             # %min.iters.checked268
	movq	%rcx, %rsi
	andq	$-16, %rsi
	je	.LBB21_104
# BB#105:                               # %vector.body264.preheader
	movdqa	.LCPI21_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	movq	%rsi, %rax
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB21_106:                             # %vector.body264
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rdi), %xmm2
	movdqu	16(%rdi), %xmm1
	pcmpeqw	%xmm0, %xmm2
	movdqa	%xmm2, 288(%rsp)
	testb	$1, 288(%rsp)
	je	.LBB21_108
# BB#107:                               # %pred.store.if283
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, (%rdi)
.LBB21_108:                             # %pred.store.continue284
                                        #   in Loop: Header=BB21_106 Depth=1
	movdqa	%xmm2, 272(%rsp)
	testb	$1, 274(%rsp)
	je	.LBB21_110
# BB#109:                               # %pred.store.if285
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 2(%rdi)
.LBB21_110:                             # %pred.store.continue286
                                        #   in Loop: Header=BB21_106 Depth=1
	movdqa	%xmm2, 256(%rsp)
	testb	$1, 260(%rsp)
	je	.LBB21_112
# BB#111:                               # %pred.store.if287
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 4(%rdi)
.LBB21_112:                             # %pred.store.continue288
                                        #   in Loop: Header=BB21_106 Depth=1
	movdqa	%xmm2, 240(%rsp)
	testb	$1, 246(%rsp)
	je	.LBB21_114
# BB#113:                               # %pred.store.if289
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 6(%rdi)
.LBB21_114:                             # %pred.store.continue290
                                        #   in Loop: Header=BB21_106 Depth=1
	movdqa	%xmm2, 224(%rsp)
	testb	$1, 232(%rsp)
	je	.LBB21_116
# BB#115:                               # %pred.store.if291
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 8(%rdi)
.LBB21_116:                             # %pred.store.continue292
                                        #   in Loop: Header=BB21_106 Depth=1
	movdqa	%xmm2, 208(%rsp)
	testb	$1, 218(%rsp)
	je	.LBB21_118
# BB#117:                               # %pred.store.if293
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 10(%rdi)
.LBB21_118:                             # %pred.store.continue294
                                        #   in Loop: Header=BB21_106 Depth=1
	movdqa	%xmm2, 192(%rsp)
	testb	$1, 204(%rsp)
	je	.LBB21_120
# BB#119:                               # %pred.store.if295
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 12(%rdi)
.LBB21_120:                             # %pred.store.continue296
                                        #   in Loop: Header=BB21_106 Depth=1
	movdqa	%xmm2, 176(%rsp)
	testb	$1, 190(%rsp)
	je	.LBB21_122
# BB#121:                               # %pred.store.if297
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 14(%rdi)
.LBB21_122:                             # %pred.store.continue298
                                        #   in Loop: Header=BB21_106 Depth=1
	pcmpeqw	%xmm0, %xmm1
	movdqa	%xmm1, 160(%rsp)
	testb	$1, 160(%rsp)
	je	.LBB21_124
# BB#123:                               # %pred.store.if299
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 16(%rdi)
.LBB21_124:                             # %pred.store.continue300
                                        #   in Loop: Header=BB21_106 Depth=1
	movdqa	%xmm1, 144(%rsp)
	testb	$1, 146(%rsp)
	je	.LBB21_126
# BB#125:                               # %pred.store.if301
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 18(%rdi)
.LBB21_126:                             # %pred.store.continue302
                                        #   in Loop: Header=BB21_106 Depth=1
	movdqa	%xmm1, 128(%rsp)
	testb	$1, 132(%rsp)
	je	.LBB21_128
# BB#127:                               # %pred.store.if303
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 20(%rdi)
.LBB21_128:                             # %pred.store.continue304
                                        #   in Loop: Header=BB21_106 Depth=1
	movdqa	%xmm1, 112(%rsp)
	testb	$1, 118(%rsp)
	je	.LBB21_130
# BB#129:                               # %pred.store.if305
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 22(%rdi)
.LBB21_130:                             # %pred.store.continue306
                                        #   in Loop: Header=BB21_106 Depth=1
	movdqa	%xmm1, 96(%rsp)
	testb	$1, 104(%rsp)
	je	.LBB21_132
# BB#131:                               # %pred.store.if307
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 24(%rdi)
.LBB21_132:                             # %pred.store.continue308
                                        #   in Loop: Header=BB21_106 Depth=1
	movdqa	%xmm1, 80(%rsp)
	testb	$1, 90(%rsp)
	je	.LBB21_134
# BB#133:                               # %pred.store.if309
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 26(%rdi)
.LBB21_134:                             # %pred.store.continue310
                                        #   in Loop: Header=BB21_106 Depth=1
	movdqa	%xmm1, 64(%rsp)
	testb	$1, 76(%rsp)
	je	.LBB21_136
# BB#135:                               # %pred.store.if311
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 28(%rdi)
.LBB21_136:                             # %pred.store.continue312
                                        #   in Loop: Header=BB21_106 Depth=1
	movdqa	%xmm1, 48(%rsp)
	testb	$1, 62(%rsp)
	je	.LBB21_138
# BB#137:                               # %pred.store.if313
                                        #   in Loop: Header=BB21_106 Depth=1
	movw	$0, 30(%rdi)
.LBB21_138:                             # %pred.store.continue314
                                        #   in Loop: Header=BB21_106 Depth=1
	addq	$32, %rdi
	addq	$-16, %rax
	jne	.LBB21_106
# BB#139:                               # %middle.block265
	xorl	%eax, %eax
	cmpq	%rsi, %rcx
	jne	.LBB21_140
	jmp	.LBB21_144
.LBB21_104:
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB21_140:                             # %scalar.ph266
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rdx,%rsi,2), %eax
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB21_142
# BB#141:                               #   in Loop: Header=BB21_140 Depth=1
	movw	$0, (%rdx,%rsi,2)
.LBB21_142:                             # %.thread
                                        #   in Loop: Header=BB21_140 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB21_140
.LBB21_143:
	xorl	%eax, %eax
.LBB21_144:                             # %.loopexit
	addq	$568, %rsp              # imm = 0x238
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	action_row, .Lfunc_end21-action_row
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI22_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	save_row
	.p2align	4, 0x90
	.type	save_row,@function
save_row:                               # @save_row
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi114:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 48
.Lcfi117:
	.cfi_offset %rbx, -40
.Lcfi118:
	.cfi_offset %r14, -32
.Lcfi119:
	.cfi_offset %r15, -24
.Lcfi120:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movslq	ntokens(%rip), %rax
	testq	%rax, %rax
	jle	.LBB22_26
# BB#1:                                 # %.lr.ph41
	movq	actrow(%rip), %rcx
	cmpl	$8, %eax
	jb	.LBB22_5
# BB#2:                                 # %min.iters.checked
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB22_5
# BB#3:                                 # %vector.body.preheader
	leaq	-8(%rdx), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	btl	$3, %edi
	jb	.LBB22_15
# BB#4:                                 # %vector.body.prol
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	pxor	%xmm2, %xmm2
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	8(%rcx), %xmm1          # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	pcmpeqd	%xmm2, %xmm0
	movdqa	.LCPI22_0(%rip), %xmm3  # xmm3 = [1,1,1,1]
	pandn	%xmm3, %xmm0
	pcmpeqd	%xmm2, %xmm1
	pandn	%xmm3, %xmm1
	movl	$8, %edi
	testq	%rsi, %rsi
	jne	.LBB22_16
	jmp	.LBB22_18
.LBB22_5:
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB22_6:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpw	$1, (%rcx,%rdx,2)
	sbbl	$-1, %ebx
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB22_6
.LBB22_7:                               # %._crit_edge42
	testl	%ebx, %ebx
	je	.LBB22_26
# BB#8:
	leal	(%rbx,%rbx), %ebp
	xorl	%eax, %eax
	movl	%ebp, %edi
	callq	mallocate
	movq	%rax, %r15
	movq	froms(%rip), %rax
	movslq	%r14d, %r14
	movq	%r15, (%rax,%r14,8)
	xorl	%eax, %eax
	movl	%ebp, %edi
	callq	mallocate
	movq	tos(%rip), %rcx
	movq	%rax, (%rcx,%r14,8)
	movslq	ntokens(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB22_12
# BB#9:                                 # %.lr.ph
	movq	actrow(%rip), %rdx
	testb	$1, %cl
	jne	.LBB22_13
# BB#10:
	xorl	%edi, %edi
	jmp	.LBB22_11
.LBB22_12:
	movq	%r15, %rsi
	jmp	.LBB22_25
.LBB22_13:
	cmpw	$0, (%rdx)
	je	.LBB22_19
# BB#14:
	movq	%r15, %rsi
	addq	$2, %rsi
	movw	$0, (%r15)
	movzwl	(%rdx), %edi
	movw	%di, (%rax)
	addq	$2, %rax
	movl	$1, %edi
	cmpl	$1, %ecx
	je	.LBB22_25
	jmp	.LBB22_20
.LBB22_15:
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	pxor	%xmm1, %xmm1
	testq	%rsi, %rsi
	je	.LBB22_18
.LBB22_16:                              # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	leaq	24(%rcx,%rdi,2), %rdi
	pxor	%xmm2, %xmm2
	movdqa	.LCPI22_0(%rip), %xmm3  # xmm3 = [1,1,1,1]
	.p2align	4, 0x90
.LBB22_17:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rdi), %xmm4        # xmm4 = mem[0],zero
	punpcklwd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	movq	-16(%rdi), %xmm5        # xmm5 = mem[0],zero
	punpcklwd	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3]
	pcmpeqd	%xmm2, %xmm4
	pandn	%xmm3, %xmm4
	pcmpeqd	%xmm2, %xmm5
	pandn	%xmm3, %xmm5
	paddd	%xmm0, %xmm4
	paddd	%xmm1, %xmm5
	movq	-8(%rdi), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rdi), %xmm1           # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	pcmpeqd	%xmm2, %xmm0
	pandn	%xmm3, %xmm0
	pcmpeqd	%xmm2, %xmm1
	pandn	%xmm3, %xmm1
	paddd	%xmm4, %xmm0
	paddd	%xmm5, %xmm1
	addq	$32, %rdi
	addq	$-16, %rsi
	jne	.LBB22_17
.LBB22_18:                              # %middle.block
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ebx
	cmpq	%rdx, %rax
	jne	.LBB22_6
	jmp	.LBB22_7
.LBB22_19:
	movl	$1, %edi
.LBB22_11:
	movq	%r15, %rsi
	cmpl	$1, %ecx
	je	.LBB22_25
	.p2align	4, 0x90
.LBB22_20:                              # =>This Inner Loop Header: Depth=1
	cmpw	$0, (%rdx,%rdi,2)
	je	.LBB22_22
# BB#21:                                #   in Loop: Header=BB22_20 Depth=1
	movw	%di, (%rsi)
	addq	$2, %rsi
	movzwl	(%rdx,%rdi,2), %ebp
	movw	%bp, (%rax)
	addq	$2, %rax
.LBB22_22:                              #   in Loop: Header=BB22_20 Depth=1
	cmpw	$0, 2(%rdx,%rdi,2)
	je	.LBB22_24
# BB#23:                                #   in Loop: Header=BB22_20 Depth=1
	leal	1(%rdi), %ebp
	movw	%bp, (%rsi)
	addq	$2, %rsi
	movzwl	2(%rdx,%rdi,2), %ebp
	movw	%bp, (%rax)
	addq	$2, %rax
.LBB22_24:                              #   in Loop: Header=BB22_20 Depth=1
	addq	$2, %rdi
	cmpq	%rcx, %rdi
	jl	.LBB22_20
.LBB22_25:                              # %._crit_edge
	movq	tally(%rip), %rax
	movw	%bx, (%rax,%r14,2)
	movzwl	-2(%rsi), %eax
	incl	%eax
	subw	(%r15), %ax
	movq	width(%rip), %rcx
	movw	%ax, (%rcx,%r14,2)
.LBB22_26:                              # %._crit_edge42.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	save_row, .Lfunc_end22-save_row
	.cfi_endproc

	.globl	default_goto
	.p2align	4, 0x90
	.type	default_goto,@function
default_goto:                           # @default_goto
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 32
.Lcfi124:
	.cfi_offset %rbx, -32
.Lcfi125:
	.cfi_offset %r14, -24
.Lcfi126:
	.cfi_offset %r15, -16
	movq	goto_map(%rip), %rax
	movslq	%edi, %rcx
	movswq	(%rax,%rcx,2), %r15
	movswq	2(%rax,%rcx,2), %r14
	movl	$-1, %ebx
	cmpw	%r14w, %r15w
	je	.LBB23_17
# BB#1:                                 # %.preheader35
	movl	nstates(%rip), %eax
	testl	%eax, %eax
	jle	.LBB23_3
# BB#2:                                 # %.lr.ph42
	movq	state_count(%rip), %rdi
	decl	%eax
	leaq	2(%rax,%rax), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB23_3:                               # %.preheader34
	cmpw	%r14w, %r15w
	jge	.LBB23_10
# BB#4:                                 # %.lr.ph40
	movq	state_count(%rip), %rax
	movq	to_state(%rip), %rcx
	movl	%r14d, %esi
	subl	%r15d, %esi
	leaq	-1(%r14), %rdx
	subq	%r15, %rdx
	andq	$3, %rsi
	je	.LBB23_7
# BB#5:                                 # %.prol.preheader47
	negq	%rsi
	.p2align	4, 0x90
.LBB23_6:                               # =>This Inner Loop Header: Depth=1
	movswq	(%rcx,%r15,2), %rdi
	incw	(%rax,%rdi,2)
	incq	%r15
	incq	%rsi
	jne	.LBB23_6
.LBB23_7:                               # %.prol.loopexit48
	cmpq	$3, %rdx
	jb	.LBB23_10
# BB#8:                                 # %.lr.ph40.new
	subq	%r15, %r14
	leaq	6(%rcx,%r15,2), %rcx
	.p2align	4, 0x90
.LBB23_9:                               # =>This Inner Loop Header: Depth=1
	movswq	-6(%rcx), %rdx
	incw	(%rax,%rdx,2)
	movswq	-4(%rcx), %rdx
	incw	(%rax,%rdx,2)
	movswq	-2(%rcx), %rdx
	incw	(%rax,%rdx,2)
	movswq	(%rcx), %rdx
	incw	(%rax,%rdx,2)
	addq	$8, %rcx
	addq	$-4, %r14
	jne	.LBB23_9
.LBB23_10:                              # %.preheader
	movslq	nstates(%rip), %r9
	testq	%r9, %r9
	jle	.LBB23_17
# BB#11:                                # %.lr.ph
	movq	state_count(%rip), %r10
	leaq	-1(%r9), %r8
	movq	%r9, %rax
	andq	$3, %rax
	je	.LBB23_12
# BB#13:                                # %.prol.preheader
	movl	$-1, %ebx
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB23_14:                              # =>This Inner Loop Header: Depth=1
	movswl	(%r10,%rdx,2), %edi
	cmpl	%esi, %edi
	cmovgl	%edx, %ebx
	cmovgel	%edi, %esi
	incq	%rdx
	cmpq	%rdx, %rax
	jne	.LBB23_14
	jmp	.LBB23_15
.LBB23_12:
	xorl	%edx, %edx
	movl	$-1, %ebx
	xorl	%esi, %esi
.LBB23_15:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB23_17
	.p2align	4, 0x90
.LBB23_16:                              # =>This Inner Loop Header: Depth=1
	movswl	(%r10,%rdx,2), %eax
	cmpl	%esi, %eax
	cmovgl	%edx, %ebx
	cmovgel	%eax, %esi
	movswl	2(%r10,%rdx,2), %eax
	leal	1(%rdx), %edi
	cmpl	%esi, %eax
	cmovlel	%ebx, %edi
	cmovgel	%eax, %esi
	movswl	4(%r10,%rdx,2), %eax
	leal	2(%rdx), %ecx
	cmpl	%esi, %eax
	cmovlel	%edi, %ecx
	cmovgel	%eax, %esi
	movswl	6(%r10,%rdx,2), %eax
	leal	3(%rdx), %ebx
	cmpl	%esi, %eax
	cmovlel	%ecx, %ebx
	cmovgel	%eax, %esi
	addq	$4, %rdx
	cmpq	%r9, %rdx
	jl	.LBB23_16
.LBB23_17:                              # %.loopexit
	movl	%ebx, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end23:
	.size	default_goto, .Lfunc_end23-default_goto
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI24_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	save_column
	.p2align	4, 0x90
	.type	save_column,@function
save_column:                            # @save_column
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi129:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi130:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi131:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi132:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi133:
	.cfi_def_cfa_offset 64
.Lcfi134:
	.cfi_offset %rbx, -56
.Lcfi135:
	.cfi_offset %r12, -48
.Lcfi136:
	.cfi_offset %r13, -40
.Lcfi137:
	.cfi_offset %r14, -32
.Lcfi138:
	.cfi_offset %r15, -24
.Lcfi139:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	goto_map(%rip), %rax
	movslq	%edi, %rcx
	movswq	(%rax,%rcx,2), %r10
	movswq	2(%rax,%rcx,2), %r13
	cmpw	%r13w, %r10w
	jge	.LBB24_30
# BB#1:                                 # %.lr.ph57
	movq	to_state(%rip), %r9
	movq	%r13, %rcx
	subq	%r10, %rcx
	xorl	%ebp, %ebp
	cmpq	$8, %rcx
	jae	.LBB24_3
# BB#2:
	movq	%r10, %rsi
	jmp	.LBB24_12
.LBB24_3:                               # %min.iters.checked
	movq	%rcx, %r8
	andq	$-8, %r8
	movq	%rcx, %rdx
	andq	$-8, %rdx
	movq	%r10, %rsi
	je	.LBB24_12
# BB#4:                                 # %vector.ph
	movd	%ebx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	-8(%rdx), %rsi
	movq	%rsi, %rax
	shrq	$3, %rax
	btl	$3, %esi
	jb	.LBB24_5
# BB#6:                                 # %vector.body.prol
	movq	(%r9,%r10,2), %xmm1     # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	8(%r9,%r10,2), %xmm2    # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	pcmpeqd	%xmm0, %xmm1
	movdqa	.LCPI24_0(%rip), %xmm3  # xmm3 = [1,1,1,1]
	pandn	%xmm3, %xmm1
	pcmpeqd	%xmm0, %xmm2
	pandn	%xmm3, %xmm2
	movl	$8, %ebp
	testq	%rax, %rax
	jne	.LBB24_8
	jmp	.LBB24_10
.LBB24_5:
	pxor	%xmm1, %xmm1
	xorl	%ebp, %ebp
	pxor	%xmm2, %xmm2
	testq	%rax, %rax
	je	.LBB24_10
.LBB24_8:                               # %vector.ph.new
	movq	%rdx, %rsi
	subq	%rbp, %rsi
	addq	%r10, %rbp
	leaq	24(%r9,%rbp,2), %rbp
	movdqa	.LCPI24_0(%rip), %xmm3  # xmm3 = [1,1,1,1]
	.p2align	4, 0x90
.LBB24_9:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rbp), %xmm4        # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	movq	-16(%rbp), %xmm5        # xmm5 = mem[0],zero
	punpcklwd	%xmm5, %xmm5    # xmm5 = xmm5[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm5
	pcmpeqd	%xmm0, %xmm4
	pandn	%xmm3, %xmm4
	pcmpeqd	%xmm0, %xmm5
	pandn	%xmm3, %xmm5
	paddd	%xmm1, %xmm4
	paddd	%xmm2, %xmm5
	movq	-8(%rbp), %xmm1         # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	(%rbp), %xmm2           # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	pcmpeqd	%xmm0, %xmm1
	pandn	%xmm3, %xmm1
	pcmpeqd	%xmm0, %xmm2
	pandn	%xmm3, %xmm2
	paddd	%xmm4, %xmm1
	paddd	%xmm5, %xmm2
	addq	$32, %rbp
	addq	$-16, %rsi
	jne	.LBB24_9
.LBB24_10:                              # %middle.block
	paddd	%xmm2, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %ebp
	cmpq	%rdx, %rcx
	je	.LBB24_14
# BB#11:
	addq	%r10, %r8
	movq	%r8, %rsi
.LBB24_12:                              # %scalar.ph.preheader
	leaq	(%r9,%rsi,2), %rax
	movq	%r13, %rcx
	subq	%rsi, %rcx
	.p2align	4, 0x90
.LBB24_13:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%rax), %edx
	xorl	%esi, %esi
	cmpl	%ebx, %edx
	setne	%sil
	addl	%esi, %ebp
	addq	$2, %rax
	decq	%rcx
	jne	.LBB24_13
.LBB24_14:                              # %._crit_edge58
	testl	%ebp, %ebp
	je	.LBB24_30
# BB#15:
	subl	ntokens(%rip), %edi
	movslq	nstates(%rip), %rax
	movslq	%edi, %r12
	addq	%rax, %r12
	leal	(%rbp,%rbp), %r15d
	xorl	%eax, %eax
	movl	%r15d, %edi
	movq	%r10, (%rsp)            # 8-byte Spill
	callq	mallocate
	movq	%rax, %r14
	movq	froms(%rip), %rax
	movq	%r14, (%rax,%r12,8)
	xorl	%eax, %eax
	movl	%r15d, %edi
	callq	mallocate
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	tos(%rip), %rcx
	movq	%rax, (%rcx,%r12,8)
	cmpw	%r13w, %dx
	movq	%r14, %rcx
	jge	.LBB24_29
# BB#16:                                # %.lr.ph
	movq	to_state(%rip), %r11
	movq	from_state(%rip), %r8
	movl	%r13d, %ecx
	subl	%edx, %ecx
	leaq	-1(%r13), %rdi
	testb	$1, %cl
	jne	.LBB24_18
# BB#17:
	movq	%rdx, %rsi
	movq	%r14, %rcx
	cmpq	%rdx, %rdi
	jne	.LBB24_23
	jmp	.LBB24_29
.LBB24_18:
	movswl	(%r11,%rdx,2), %ecx
	cmpl	%ebx, %ecx
	jne	.LBB24_20
# BB#19:
	movq	%r14, %rcx
	jmp	.LBB24_21
.LBB24_20:
	movzwl	(%r8,%rdx,2), %esi
	movq	%r14, %rcx
	addq	$2, %rcx
	movw	%si, (%r14)
	movzwl	(%r11,%rdx,2), %esi
	movw	%si, (%rax)
	addq	$2, %rax
.LBB24_21:
	leaq	1(%rdx), %rsi
	cmpq	%rdx, %rdi
	je	.LBB24_29
.LBB24_23:                              # %.lr.ph.new
	subq	%rsi, %r13
	leaq	2(%r11,%rsi,2), %rdx
	leaq	2(%r8,%rsi,2), %rsi
	.p2align	4, 0x90
.LBB24_24:                              # =>This Inner Loop Header: Depth=1
	movswl	-2(%rdx), %edi
	cmpl	%ebx, %edi
	je	.LBB24_26
# BB#25:                                #   in Loop: Header=BB24_24 Depth=1
	movzwl	-2(%rsi), %edi
	movw	%di, (%rcx)
	addq	$2, %rcx
	movzwl	-2(%rdx), %edi
	movw	%di, (%rax)
	addq	$2, %rax
.LBB24_26:                              #   in Loop: Header=BB24_24 Depth=1
	movswl	(%rdx), %edi
	cmpl	%ebx, %edi
	je	.LBB24_28
# BB#27:                                #   in Loop: Header=BB24_24 Depth=1
	movzwl	(%rsi), %edi
	movw	%di, (%rcx)
	addq	$2, %rcx
	movzwl	(%rdx), %edi
	movw	%di, (%rax)
	addq	$2, %rax
.LBB24_28:                              #   in Loop: Header=BB24_24 Depth=1
	addq	$4, %rdx
	addq	$4, %rsi
	addq	$-2, %r13
	jne	.LBB24_24
.LBB24_29:                              # %._crit_edge
	movq	tally(%rip), %rax
	movw	%bp, (%rax,%r12,2)
	movzwl	-2(%rcx), %eax
	incl	%eax
	subw	(%r14), %ax
	movq	width(%rip), %rcx
	movw	%ax, (%rcx,%r12,2)
.LBB24_30:                              # %._crit_edge58.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	save_column, .Lfunc_end24-save_column
	.cfi_endproc

	.globl	matching_state
	.p2align	4, 0x90
	.type	matching_state,@function
matching_state:                         # @matching_state
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi143:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi144:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 56
.Lcfi146:
	.cfi_offset %rbx, -56
.Lcfi147:
	.cfi_offset %r12, -48
.Lcfi148:
	.cfi_offset %r13, -40
.Lcfi149:
	.cfi_offset %r14, -32
.Lcfi150:
	.cfi_offset %r15, -24
.Lcfi151:
	.cfi_offset %rbp, -16
	movq	order(%rip), %r11
	movslq	%edi, %r14
	movswl	(%r11,%r14,2), %ecx
	movl	$-1, %eax
	cmpl	nstates(%rip), %ecx
	jge	.LBB25_15
# BB#1:
	movslq	%ecx, %r15
	movq	tally(%rip), %rdx
	movswq	(%rdx,%r15,2), %r13
	testq	%r13, %r13
	movq	width(%rip), %r10
	movzwl	(%r10,%r15,2), %esi
	jle	.LBB25_12
# BB#2:                                 # %.split.us.preheader
	testl	%edi, %edi
	jle	.LBB25_15
# BB#3:
	movq	tos(%rip), %rcx
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	movq	froms(%rip), %rbx
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	movw	%si, -18(%rsp)          # 2-byte Spill
.LBB25_5:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_8 Depth 2
	movq	%r14, %r12
	movswq	-2(%r11,%r12,2), %rdi
	cmpw	%si, (%r10,%rdi,2)
	jne	.LBB25_15
# BB#6:                                 #   in Loop: Header=BB25_5 Depth=1
	cmpw	%r13w, (%rdx,%rdi,2)
	jne	.LBB25_15
# BB#7:                                 # %.lr.ph.us
                                        #   in Loop: Header=BB25_5 Depth=1
	leaq	-1(%r12), %r14
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rdi,8), %r8
	movq	(%rcx,%r15,8), %rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB25_8:                               #   Parent Loop BB25_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%r8,%rbp,2), %r9d
	cmpw	(%rsi,%rbp,2), %r9w
	jne	.LBB25_4
# BB#9:                                 #   in Loop: Header=BB25_8 Depth=2
	movq	(%rbx,%rdi,8), %rdx
	movzwl	(%rdx,%rbp,2), %edx
	movq	(%rbx,%r15,8), %rcx
	cmpw	(%rcx,%rbp,2), %dx
	jne	.LBB25_4
# BB#10:                                #   in Loop: Header=BB25_8 Depth=2
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB25_8
	jmp	.LBB25_11
	.p2align	4, 0x90
.LBB25_4:                               # %.split.us.loopexit
                                        #   in Loop: Header=BB25_5 Depth=1
	cmpq	$2, %r12
	movq	-16(%rsp), %rdx         # 8-byte Reload
	movzwl	-18(%rsp), %esi         # 2-byte Folded Reload
	jge	.LBB25_5
	jmp	.LBB25_15
.LBB25_12:                              # %.split
	testl	%edi, %edi
	jle	.LBB25_15
# BB#13:
	movslq	%edi, %rcx
	movswq	-2(%r11,%rcx,2), %rcx
	cmpw	%si, (%r10,%rcx,2)
	jne	.LBB25_15
# BB#14:
	cmpw	%r13w, (%rdx,%rcx,2)
	movl	$-1, %eax
	cmovel	%ecx, %eax
	jmp	.LBB25_15
.LBB25_11:                              # %.loopexit.loopexit
	movl	%edi, %eax
.LBB25_15:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	matching_state, .Lfunc_end25-matching_state
	.cfi_endproc

	.globl	pack_vector
	.p2align	4, 0x90
	.type	pack_vector,@function
pack_vector:                            # @pack_vector
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi155:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi156:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi158:
	.cfi_def_cfa_offset 80
.Lcfi159:
	.cfi_offset %rbx, -56
.Lcfi160:
	.cfi_offset %r12, -48
.Lcfi161:
	.cfi_offset %r13, -40
.Lcfi162:
	.cfi_offset %r14, -32
.Lcfi163:
	.cfi_offset %r15, -24
.Lcfi164:
	.cfi_offset %rbp, -16
	movq	order(%rip), %rax
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movslq	%edi, %r15
	movq	tally(%rip), %rcx
	movswq	(%rax,%r15,2), %rbp
	movswq	(%rcx,%rbp,2), %r13
	testq	%r13, %r13
	jne	.LBB26_2
# BB#1:
	movl	$.L.str.34, %edi
	callq	berror
.LBB26_2:
	movq	froms(%rip), %rax
	movq	(%rax,%rbp,8), %r12
	movl	lowzero(%rip), %ebx
	movswl	(%r12), %eax
	subl	%eax, %ebx
	cmpl	$32766, %ebx            # imm = 0x7FFE
	jg	.LBB26_40
# BB#3:                                 # %.preheader65.lr.ph
	movq	tos(%rip), %rax
	movq	(%rax,%rbp,8), %rax
	testw	%r13w, %r13w
	jle	.LBB26_17
# BB#4:                                 # %.preheader65.us.preheader
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB26_5:                               # %.preheader65.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_6 Depth 2
                                        #     Child Loop BB26_13 Depth 2
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB26_6:                               #   Parent Loop BB26_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	-2(%r12,%r14,2), %rax
	movslq	%ebx, %rbp
	addq	%rax, %rbp
	cmpl	$32768, %ebp            # imm = 0x8000
	jl	.LBB26_8
# BB#7:                                 #   in Loop: Header=BB26_6 Depth=2
	subq	$8, %rsp
.Lcfi165:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.35, %edi
	movl	$32767, %esi            # imm = 0x7FFF
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
.Lcfi166:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi167:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi168:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi169:
	.cfi_adjust_cfa_offset -32
.LBB26_8:                               #   in Loop: Header=BB26_6 Depth=2
	movq	table(%rip), %rax
	movzwl	(%rax,%rbp,2), %ecx
	testw	%cx, %cx
	sete	%al
	cmpq	%r13, %r14
	jge	.LBB26_10
# BB#9:                                 #   in Loop: Header=BB26_6 Depth=2
	incq	%r14
	testw	%cx, %cx
	je	.LBB26_6
.LBB26_10:                              # %..critedge.preheader_crit_edge.us
                                        #   in Loop: Header=BB26_5 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB26_11
# BB#15:                                # %..critedge.preheader_crit_edge.us
                                        #   in Loop: Header=BB26_5 Depth=1
	testw	%cx, %cx
	movl	$0, %edi
	jne	.LBB26_11
# BB#16:                                # %.lr.ph79.us
                                        #   in Loop: Header=BB26_5 Depth=1
	movl	$1, %ecx
	movq	pos(%rip), %rdx
	movl	$1, %esi
	.p2align	4, 0x90
.LBB26_13:                              # %.critedge.us
                                        #   Parent Loop BB26_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	-2(%rdx,%rsi,2), %eax
	cmpl	%ebx, %eax
	cmovel	%edi, %ecx
	testl	%ecx, %ecx
	setne	%al
	cmpq	%r15, %rsi
	jge	.LBB26_11
# BB#14:                                # %.critedge.us
                                        #   in Loop: Header=BB26_13 Depth=2
	incq	%rsi
	testl	%ecx, %ecx
	jne	.LBB26_13
.LBB26_11:                              # %.critedge1.us
                                        #   in Loop: Header=BB26_5 Depth=1
	testb	%al, %al
	jne	.LBB26_28
# BB#12:                                #   in Loop: Header=BB26_5 Depth=1
	incl	%ebx
	cmpl	$32767, %ebx            # imm = 0x7FFF
	jl	.LBB26_5
	jmp	.LBB26_40
.LBB26_17:                              # %.preheader65.lr.ph.split
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB26_18
# BB#33:                                # %.lr.ph79.us106.preheader
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	pos(%rip), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB26_34:                              # %.lr.ph79.us106
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_31 Depth 2
	movl	$1, %edx
	movl	$1, %esi
	.p2align	4, 0x90
.LBB26_31:                              # %.critedge.us89
                                        #   Parent Loop BB26_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	-2(%rax,%rsi,2), %edi
	cmpl	%ebx, %edi
	cmovel	%ecx, %edx
	cmpq	%r15, %rsi
	jge	.LBB26_26
# BB#32:                                # %.critedge.us89
                                        #   in Loop: Header=BB26_31 Depth=2
	incq	%rsi
	testl	%edx, %edx
	jne	.LBB26_31
.LBB26_26:                              # %.critedge1.us87
                                        #   in Loop: Header=BB26_34 Depth=1
	testl	%edx, %edx
	jne	.LBB26_27
# BB#39:                                #   in Loop: Header=BB26_34 Depth=1
	incl	%ebx
	cmpl	$32767, %ebx            # imm = 0x7FFF
	jl	.LBB26_34
.LBB26_40:                              # %._crit_edge83
	movl	$.L.str.34, %edi
	callq	berror
	xorl	%ebx, %ebx
.LBB26_41:
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB26_18:
                                        # implicit-def: %EBP
	jmp	.LBB26_19
.LBB26_27:
                                        # implicit-def: %EBP
.LBB26_28:                              # %.preheader64
	testw	%r13w, %r13w
	jle	.LBB26_19
# BB#29:                                # %.lr.ph69
	movq	table(%rip), %r8
	movq	check(%rip), %r9
	movl	%r13d, %edx
	testb	$1, %r13b
	jne	.LBB26_35
# BB#30:
	xorl	%eax, %eax
                                        # implicit-def: %EBP
	cmpq	$1, %rdx
	jne	.LBB26_37
	jmp	.LBB26_20
.LBB26_19:                              # %.preheader64..preheader_crit_edge
	movq	table(%rip), %r8
	jmp	.LBB26_20
.LBB26_35:
	movswq	(%r12), %rax
	movslq	%ebx, %rbp
	addq	%rax, %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
	movzwl	(%rax), %eax
	movw	%ax, (%r8,%rbp,2)
	movzwl	(%r12), %eax
	movw	%ax, (%r9,%rbp,2)
	movl	$1, %eax
	cmpq	$1, %rdx
	je	.LBB26_20
.LBB26_37:                              # %.lr.ph69.new
	subq	%rax, %rdx
	leaq	2(%r12,%rax,2), %rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	2(%rcx,%rax,2), %rdi
	movslq	%ebx, %rax
	.p2align	4, 0x90
.LBB26_38:                              # =>This Inner Loop Header: Depth=1
	movswq	-2(%rsi), %rbp
	addq	%rax, %rbp
	movzwl	-2(%rdi), %ecx
	movw	%cx, (%r8,%rbp,2)
	movzwl	-2(%rsi), %ecx
	movw	%cx, (%r9,%rbp,2)
	movswq	(%rsi), %rbp
	addq	%rax, %rbp
	movzwl	(%rdi), %ecx
	movw	%cx, (%r8,%rbp,2)
	movzwl	(%rsi), %ecx
	movw	%cx, (%r9,%rbp,2)
	addq	$4, %rsi
	addq	$4, %rdi
	addq	$-2, %rdx
	jne	.LBB26_38
.LBB26_20:                              # %.preheader
	movl	lowzero(%rip), %eax
	movslq	%eax, %rcx
	cmpw	$0, (%r8,%rcx,2)
	je	.LBB26_24
# BB#21:                                # %.lr.ph.preheader
	leaq	2(%r8,%rcx,2), %rcx
	.p2align	4, 0x90
.LBB26_22:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	cmpw	$0, (%rcx)
	leaq	2(%rcx), %rcx
	jne	.LBB26_22
# BB#23:                                # %._crit_edge
	movl	%eax, lowzero(%rip)
.LBB26_24:
	cmpl	high(%rip), %ebp
	jle	.LBB26_41
# BB#25:
	movl	%ebp, high(%rip)
	jmp	.LBB26_41
.Lfunc_end26:
	.size	pack_vector, .Lfunc_end26-pack_vector
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n#include \"%s\"\nextern int yyerror;\nextern int yycost;\nextern char * yymsg;\nextern YYSTYPE yyval;\n\nyyguard(n, yyvsp, yylsp)\nregister int n;\nregister YYSTYPE *yyvsp;\nregister YYLTYPE *yylsp;\n{\n  yyerror = 0;\nyycost = 0;\n  yymsg = 0;\nswitch (n)\n    {"
	.size	.L.str, 248

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n#include \"%s\"\nextern YYSTYPE yyval;\nextern int yychar;yyaction(n, yyvsp, yylsp)\nregister int n;\nregister YYSTYPE *yyvsp;\nregister YYLTYPE *yylsp;\n{\n  switch (n)\n{"
	.size	.L.str.1, 164

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n  switch (yyn) {\n"
	.size	.L.str.2, 19

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n    }\n}\n"
	.size	.L.str.3, 10

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n}\n"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"#define YYDEBUG\n"
	.size	.L.str.5, 17

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"#include \"%s\"\n"
	.size	.L.str.6, 15

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"#include <stdio.h>\n\n"
	.size	.L.str.7, 21

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"#ifndef __STDC__\n#define const\n#endif\n\n"
	.size	.L.str.8, 40

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\n#define YYTRANSLATE(x) ((unsigned)(x) <= %d ? yytranslate[x] : %d)\n"
	.size	.L.str.9, 69

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"\nstatic const char yytranslate[] = {     0"
	.size	.L.str.10, 43

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\nstatic const short yytranslate[] = {     0"
	.size	.L.str.11, 44

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"%6d"
	.size	.L.str.12, 4

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"\n};\n"
	.size	.L.str.13, 5

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"\n#define YYTRANSLATE(x) (x)\n"
	.size	.L.str.14, 29

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"\nstatic const short yyprhs[] = {     0"
	.size	.L.str.15, 39

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"\n};\n\nstatic const short yyrhs[] = {%6d"
	.size	.L.str.16, 39

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"     0"
	.size	.L.str.17, 7

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\nstatic const short yystos[] = {     0"
	.size	.L.str.18, 39

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"\nstatic const short yyrline[] = {     0"
	.size	.L.str.19, 40

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"\n};\n\nstatic const char * const yytname[] = {     0"
	.size	.L.str.20, 51

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"\\%c"
	.size	.L.str.21, 4

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"\\n"
	.size	.L.str.22, 3

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"\\t"
	.size	.L.str.23, 3

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"\\b"
	.size	.L.str.24, 3

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"\\%03o"
	.size	.L.str.25, 6

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"\n};\n\nstatic const short yyr1[] = {     0"
	.size	.L.str.26, 41

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"\n};\n\nstatic const short yyr2[] = {     0"
	.size	.L.str.27, 41

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"%6d\n};\n"
	.size	.L.str.28, 8

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"\n\n#define\tYYFINAL\t\t%d\n"
	.size	.L.str.29, 23

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"#define\tYYFLAG\t\t%d\n"
	.size	.L.str.30, 20

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"#define\tYYNTBASE\t%d\n"
	.size	.L.str.31, 21

	.type	nvectors,@object        # @nvectors
	.local	nvectors
	.comm	nvectors,4,4
	.type	froms,@object           # @froms
	.local	froms
	.comm	froms,8,8
	.type	tos,@object             # @tos
	.local	tos
	.comm	tos,8,8
	.type	tally,@object           # @tally
	.local	tally
	.comm	tally,8,8
	.type	width,@object           # @width
	.local	width
	.comm	width,8,8
	.type	actrow,@object          # @actrow
	.local	actrow
	.comm	actrow,8,8
	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"\nstatic const short yydefact[] = {%6d"
	.size	.L.str.32, 38

	.type	state_count,@object     # @state_count
	.local	state_count
	.comm	state_count,8,8
	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"\nstatic const short yydefgoto[] = {%6d"
	.size	.L.str.33, 39

	.type	order,@object           # @order
	.local	order
	.comm	order,8,8
	.type	nentries,@object        # @nentries
	.local	nentries
	.comm	nentries,4,4
	.type	base,@object            # @base
	.local	base
	.comm	base,8,8
	.type	pos,@object             # @pos
	.local	pos
	.comm	pos,8,8
	.type	table,@object           # @table
	.local	table
	.comm	table,8,8
	.type	check,@object           # @check
	.local	check
	.comm	check,8,8
	.type	lowzero,@object         # @lowzero
	.local	lowzero
	.comm	lowzero,4,4
	.type	high,@object            # @high
	.local	high
	.comm	high,4,4
	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"pack_vector"
	.size	.L.str.34, 12

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"maximum table size (%d) exceeded"
	.size	.L.str.35, 33

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"\nstatic const short yypact[] = {%6d"
	.size	.L.str.36, 36

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"\n};\n\nstatic const short yypgoto[] = {%6d"
	.size	.L.str.37, 41

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"\n\n#define\tYYLAST\t\t%d\n\n"
	.size	.L.str.38, 23

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"\nstatic const short yytable[] = {%6d"
	.size	.L.str.39, 37

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"\nstatic const short yycheck[] = {%6d"
	.size	.L.str.40, 37

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"#define YYIMPURE 1\n\n"
	.size	.L.str.41, 21

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"#define YYPURE 1\n\n"
	.size	.L.str.42, 19

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"#lin"
	.size	.L.str.43, 5

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"#li"
	.size	.L.str.44, 4

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"#l"
	.size	.L.str.45, 3

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"#line %d \"%s\"\n"
	.size	.L.str.47, 15


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
