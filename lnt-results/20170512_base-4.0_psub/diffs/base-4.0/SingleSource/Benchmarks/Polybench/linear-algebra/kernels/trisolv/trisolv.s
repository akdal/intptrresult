	.text
	.file	"trisolv.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4661014508095930368     # double 4000
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -48
.Lcfi8:
	.cfi_offset %r12, -40
.Lcfi9:
	.cfi_offset %r13, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$128000000, %edx        # imm = 0x7A12000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_33
# BB#1:
	movq	8(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_33
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_33
# BB#3:                                 # %polybench_alloc_data.exit
	movq	8(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_33
# BB#4:                                 # %polybench_alloc_data.exit28
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_33
# BB#5:                                 # %polybench_alloc_data.exit28
	movq	8(%rsp), %r13
	testq	%r13, %r13
	je	.LBB6_33
# BB#6:                                 # %polybench_alloc_data.exit30
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_33
# BB#7:                                 # %polybench_alloc_data.exit30
	movq	8(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_33
# BB#8:                                 # %polybench_alloc_data.exit32
	leaq	8(%r14), %rax
	xorl	%ecx, %ecx
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_10 Depth 2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	divsd	%xmm0, %xmm2
	movsd	%xmm2, (%r15,%rcx,8)
	movsd	%xmm2, (%r13,%rcx,8)
	movsd	%xmm2, (%r12,%rcx,8)
	movq	%rax, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_10:                               #   Parent Loop BB6_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	divsd	%xmm0, %xmm2
	movsd	%xmm2, -8(%rdx)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	divsd	%xmm0, %xmm2
	movsd	%xmm2, (%rdx)
	addq	$2, %rsi
	addq	$16, %rdx
	cmpq	$4000, %rsi             # imm = 0xFA0
	jne	.LBB6_10
# BB#11:                                #   in Loop: Header=BB6_9 Depth=1
	incq	%rcx
	addq	$32000, %rax            # imm = 0x7D00
	cmpq	$4000, %rcx             # imm = 0xFA0
	jne	.LBB6_9
# BB#12:                                # %init_array.exit.preheader
	leaq	8(%r15), %r8
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB6_13:                               # %init_array.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_19 Depth 2
	movq	(%r12,%rcx,8), %rax
	movq	%rax, (%r15,%rcx,8)
	movd	%rax, %xmm0
	testq	%rcx, %rcx
	jle	.LBB6_20
# BB#14:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	testb	$1, %cl
	jne	.LBB6_16
# BB#15:                                #   in Loop: Header=BB6_13 Depth=1
	xorl	%eax, %eax
	cmpq	$1, %rcx
	jne	.LBB6_18
	jmp	.LBB6_20
	.p2align	4, 0x90
.LBB6_16:                               # %.lr.ph.i.prol
                                        #   in Loop: Header=BB6_13 Depth=1
	imulq	$32000, %rcx, %rax      # imm = 0x7D00
	movsd	(%r14,%rax), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%r15), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%r15,%rcx,8)
	movl	$1, %eax
	cmpq	$1, %rcx
	je	.LBB6_20
.LBB6_18:                               # %.lr.ph.i.preheader.new
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	%rcx, %rsi
	subq	%rax, %rsi
	leaq	(%r8,%rax,8), %rdi
	leaq	(%rdx,%rax,8), %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_19:                               # %.lr.ph.i
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	-8(%rdi,%rax,8), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%r15,%rcx,8)
	movsd	8(%rbx,%rax,8), %xmm1   # xmm1 = mem[0],zero
	mulsd	(%rdi,%rax,8), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%r15,%rcx,8)
	addq	$2, %rax
	cmpq	%rax, %rsi
	jne	.LBB6_19
.LBB6_20:                               # %._crit_edge.i
                                        #   in Loop: Header=BB6_13 Depth=1
	imulq	$32000, %rcx, %rax      # imm = 0x7D00
	addq	%r14, %rax
	divsd	(%rax,%rcx,8), %xmm0
	movsd	%xmm0, (%r15,%rcx,8)
	incq	%rcx
	addq	$32000, %rdx            # imm = 0x7D00
	cmpq	$4000, %rcx             # imm = 0xFA0
	jne	.LBB6_13
# BB#21:                                # %kernel_trisolv.exit.preheader
	leaq	8(%r13), %r8
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB6_22:                               # %kernel_trisolv.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_28 Depth 2
	movq	(%r12,%rcx,8), %rax
	movq	%rax, (%r13,%rcx,8)
	movd	%rax, %xmm0
	testq	%rcx, %rcx
	jle	.LBB6_29
# BB#23:                                # %.lr.ph.i41.preheader
                                        #   in Loop: Header=BB6_22 Depth=1
	testb	$1, %cl
	jne	.LBB6_25
# BB#24:                                #   in Loop: Header=BB6_22 Depth=1
	xorl	%eax, %eax
	cmpq	$1, %rcx
	jne	.LBB6_27
	jmp	.LBB6_29
	.p2align	4, 0x90
.LBB6_25:                               # %.lr.ph.i41.prol
                                        #   in Loop: Header=BB6_22 Depth=1
	imulq	$32000, %rcx, %rax      # imm = 0x7D00
	movsd	(%r14,%rax), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%r13), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%r13,%rcx,8)
	movl	$1, %eax
	cmpq	$1, %rcx
	je	.LBB6_29
.LBB6_27:                               # %.lr.ph.i41.preheader.new
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	%rcx, %rsi
	subq	%rax, %rsi
	leaq	(%r8,%rax,8), %rdi
	leaq	(%rdx,%rax,8), %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_28:                               # %.lr.ph.i41
                                        #   Parent Loop BB6_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	-8(%rdi,%rax,8), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%r13,%rcx,8)
	movsd	8(%rbx,%rax,8), %xmm1   # xmm1 = mem[0],zero
	mulsd	(%rdi,%rax,8), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%r13,%rcx,8)
	addq	$2, %rax
	cmpq	%rax, %rsi
	jne	.LBB6_28
.LBB6_29:                               # %._crit_edge.i45
                                        #   in Loop: Header=BB6_22 Depth=1
	imulq	$32000, %rcx, %rax      # imm = 0x7D00
	addq	%r14, %rax
	divsd	(%rax,%rcx,8), %xmm0
	movsd	%xmm0, (%r13,%rcx,8)
	incq	%rcx
	addq	$32000, %rdx            # imm = 0x7D00
	cmpq	$4000, %rcx             # imm = 0xFA0
	jne	.LBB6_22
# BB#30:                                # %kernel_trisolv.exit46
	movl	$64001, %edi            # imm = 0xFA01
	callq	malloc
	movq	%rax, %rbx
	movb	$0, 64000(%rbx)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_31:                               # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rax), %rcx
	movl	%ecx, %edx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, (%rbx,%rax,2)
	movb	%dl, 1(%rbx,%rax,2)
	movq	%rcx, %rdx
	shrq	$8, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 2(%rbx,%rax,2)
	movb	%dl, 3(%rbx,%rax,2)
	movq	%rcx, %rdx
	shrq	$16, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 4(%rbx,%rax,2)
	movb	%dl, 5(%rbx,%rax,2)
	movl	%ecx, %edx
	shrl	$24, %edx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 6(%rbx,%rax,2)
	movb	%dl, 7(%rbx,%rax,2)
	movq	%rcx, %rdx
	shrq	$32, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 8(%rbx,%rax,2)
	movb	%dl, 9(%rbx,%rax,2)
	movq	%rcx, %rdx
	shrq	$40, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 10(%rbx,%rax,2)
	movb	%dl, 11(%rbx,%rax,2)
	movq	%rcx, %rdx
	shrq	$48, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 12(%rbx,%rax,2)
	movb	%dl, 13(%rbx,%rax,2)
	shrq	$56, %rcx
	andb	$15, %cl
	orb	$48, %cl
	movb	%cl, 14(%rbx,%rax,2)
	movb	%cl, 15(%rbx,%rax,2)
	addq	$8, %rax
	cmpq	$32000, %rax            # imm = 0x7D00
	jne	.LBB6_31
# BB#32:                                # %print_array.exit
	movq	stderr(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
	movq	%rbx, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB6_33:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
