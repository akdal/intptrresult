	.text
	.file	"ZHandler.bc"
	.globl	_ZN8NArchive2NZ8CHandler21GetNumberOfPropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandler21GetNumberOfPropertiesEPj,@function
_ZN8NArchive2NZ8CHandler21GetNumberOfPropertiesEPj: # @_ZN8NArchive2NZ8CHandler21GetNumberOfPropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	_ZN8NArchive2NZ8CHandler21GetNumberOfPropertiesEPj, .Lfunc_end0-_ZN8NArchive2NZ8CHandler21GetNumberOfPropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive2NZ8CHandler15GetPropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandler15GetPropertyInfoEjPPwPjPt,@function
_ZN8NArchive2NZ8CHandler15GetPropertyInfoEjPPwPjPt: # @_ZN8NArchive2NZ8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	testl	%esi, %esi
	jne	.LBB1_2
# BB#1:
	movl	_ZN8NArchive2NZ6kPropsE+8(%rip), %eax
	movl	%eax, (%rcx)
	movzwl	_ZN8NArchive2NZ6kPropsE+12(%rip), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB1_2:
	retq
.Lfunc_end1:
	.size	_ZN8NArchive2NZ8CHandler15GetPropertyInfoEjPPwPjPt, .Lfunc_end1-_ZN8NArchive2NZ8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive2NZ8CHandler28GetNumberOfArchivePropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandler28GetNumberOfArchivePropertiesEPj,@function
_ZN8NArchive2NZ8CHandler28GetNumberOfArchivePropertiesEPj: # @_ZN8NArchive2NZ8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$0, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	_ZN8NArchive2NZ8CHandler28GetNumberOfArchivePropertiesEPj, .Lfunc_end2-_ZN8NArchive2NZ8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive2NZ8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandler22GetArchivePropertyInfoEjPPwPjPt,@function
_ZN8NArchive2NZ8CHandler22GetArchivePropertyInfoEjPPwPjPt: # @_ZN8NArchive2NZ8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147467263, %eax      # imm = 0x80004001
	retq
.Lfunc_end3:
	.size	_ZN8NArchive2NZ8CHandler22GetArchivePropertyInfoEjPPwPjPt, .Lfunc_end3-_ZN8NArchive2NZ8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive2NZ8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandler18GetArchivePropertyEjP14tagPROPVARIANT,@function
_ZN8NArchive2NZ8CHandler18GetArchivePropertyEjP14tagPROPVARIANT: # @_ZN8NArchive2NZ8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.cfi_startproc
# BB#0:
	movw	$0, (%rdx)
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	_ZN8NArchive2NZ8CHandler18GetArchivePropertyEjP14tagPROPVARIANT, .Lfunc_end4-_ZN8NArchive2NZ8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.cfi_endproc

	.globl	_ZN8NArchive2NZ8CHandler16GetNumberOfItemsEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandler16GetNumberOfItemsEPj,@function
_ZN8NArchive2NZ8CHandler16GetNumberOfItemsEPj: # @_ZN8NArchive2NZ8CHandler16GetNumberOfItemsEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	_ZN8NArchive2NZ8CHandler16GetNumberOfItemsEPj, .Lfunc_end5-_ZN8NArchive2NZ8CHandler16GetNumberOfItemsEPj
	.cfi_endproc

	.globl	_ZN8NArchive2NZ8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandler11GetPropertyEjjP14tagPROPVARIANT,@function
_ZN8NArchive2NZ8CHandler11GetPropertyEjjP14tagPROPVARIANT: # @_ZN8NArchive2NZ8CHandler11GetPropertyEjjP14tagPROPVARIANT
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rcx, %rbx
	movl	$0, (%rsp)
	cmpl	$8, %edx
	jne	.LBB6_2
# BB#1:
	movq	32(%rdi), %rsi
.Ltmp0:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp1:
.LBB6_2:
.Ltmp2:
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp3:
# BB#3:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB6_4:
.Ltmp4:
	movq	%rax, %rbx
.Ltmp5:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp6:
# BB#5:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_6:
.Ltmp7:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN8NArchive2NZ8CHandler11GetPropertyEjjP14tagPROPVARIANT, .Lfunc_end6-_ZN8NArchive2NZ8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp5-.Ltmp3           #   Call between .Ltmp3 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end7:
	.size	__clang_call_terminate, .Lfunc_end7-__clang_call_terminate

	.text
	.globl	_ZN8NArchive2NZ8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback,@function
_ZN8NArchive2NZ8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback: # @_ZN8NArchive2NZ8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 48
.Lcfi7:
	.cfi_offset %rbx, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%rbx), %rax
	leaq	24(%r14), %rcx
.Ltmp8:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	*48(%rax)
.Ltmp9:
# BB#1:
	testl	%eax, %eax
	jne	.LBB8_19
# BB#2:
.Ltmp11:
	leaq	5(%rsp), %rsi
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	movl	%eax, %ebp
.Ltmp12:
# BB#3:
	movl	$1, %eax
	testl	%ebp, %ebp
	jne	.LBB8_13
# BB#4:
	cmpb	$31, 5(%rsp)
	jne	.LBB8_5
# BB#6:
	cmpb	$-99, 6(%rsp)
	movl	$1, %ebp
	jne	.LBB8_13
# BB#7:
	movb	7(%rsp), %al
	movb	%al, 40(%r14)
	movq	(%rbx), %rax
.Ltmp14:
	leaq	8(%rsp), %rcx
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp15:
# BB#8:
	movl	$1, %eax
	testl	%ebp, %ebp
	jne	.LBB8_13
# BB#9:
	movq	8(%rsp), %rax
	addq	$-3, %rax
	subq	24(%r14), %rax
	movq	%rax, 32(%r14)
	movq	(%rbx), %rax
.Ltmp16:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp17:
# BB#10:                                # %.noexc
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB8_12
# BB#11:
	movq	(%rdi), %rax
.Ltmp18:
	callq	*16(%rax)
.Ltmp19:
.LBB8_12:                               # %_ZN9CMyComPtrI9IInStreamEaSEPS0_.exit
	movq	%rbx, 16(%r14)
	xorl	%eax, %eax
	jmp	.LBB8_13
.LBB8_5:
	movl	$1, %ebp
.LBB8_13:
	testl	%eax, %eax
	cmovel	%eax, %ebp
	movl	%ebp, %eax
.LBB8_19:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB8_22:
.Ltmp20:
	jmp	.LBB8_15
.LBB8_14:
.Ltmp13:
	jmp	.LBB8_15
.LBB8_21:
.Ltmp10:
.LBB8_15:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbp
	cmpl	$2, %ebx
	je	.LBB8_16
# BB#18:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB8_19
.LBB8_16:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbp, (%rax)
.Ltmp21:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp22:
# BB#20:
.LBB8_17:
.Ltmp23:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN8NArchive2NZ8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback, .Lfunc_end8-_ZN8NArchive2NZ8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\334"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin1   #     jumps to .Ltmp10
	.byte	3                       #   On action: 2
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	3                       #   On action: 2
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp19-.Ltmp14         #   Call between .Ltmp14 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin1   #     jumps to .Ltmp20
	.byte	3                       #   On action: 2
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp21-.Ltmp19         #   Call between .Ltmp19 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin1   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Lfunc_end8-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive2NZ8CHandler5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandler5CloseEv,@function
_ZN8NArchive2NZ8CHandler5CloseEv:       # @_ZN8NArchive2NZ8CHandler5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 16(%rbx)
.LBB9_2:                                # %_ZN9CMyComPtrI9IInStreamE7ReleaseEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	_ZN8NArchive2NZ8CHandler5CloseEv, .Lfunc_end9-_ZN8NArchive2NZ8CHandler5CloseEv
	.cfi_endproc

	.globl	_ZN8NArchive2NZ8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandler7ExtractEPKjjiP23IArchiveExtractCallback,@function
_ZN8NArchive2NZ8CHandler7ExtractEPKjjiP23IArchiveExtractCallback: # @_ZN8NArchive2NZ8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 80
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movl	%ecx, %ebx
	movq	%rdi, %r12
	cmpl	$-1, %edx
	je	.LBB10_6
# BB#1:
	testl	%edx, %edx
	je	.LBB10_2
# BB#3:
	cmpl	$1, %edx
	jne	.LBB10_5
# BB#4:
	cmpl	$0, (%rsi)
	je	.LBB10_6
.LBB10_5:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	jmp	.LBB10_75
.LBB10_6:
	movq	(%r15), %rax
	movq	32(%r12), %rsi
.Ltmp24:
	movq	%r15, %rdi
	callq	*40(%rax)
.Ltmp25:
# BB#7:
	movq	$0, 16(%rsp)
	movq	(%r15), %rax
.Ltmp27:
	leaq	16(%rsp), %rsi
	movq	%r15, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp28:
# BB#8:
	testl	%ebp, %ebp
	jne	.LBB10_75
# BB#9:
	movq	$0, (%rsp)
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	setne	%r14b
	movq	(%r15), %rax
.Ltmp30:
	movq	%rsp, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%r14d, %ecx
	callq	*56(%rax)
	movl	%eax, %ebp
.Ltmp31:
# BB#10:
	testl	%ebp, %ebp
	je	.LBB10_11
.LBB10_56:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit116
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_75
# BB#57:
	movq	(%rdi), %rax
.Ltmp94:
	callq	*16(%rax)
.Ltmp95:
	jmp	.LBB10_75
.LBB10_2:
	xorl	%ebp, %ebp
.LBB10_75:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_11:
	testl	%ebx, %ebx
	sete	%al
	cmpq	$0, (%rsp)
	jne	.LBB10_13
# BB#12:
	xorl	%ebp, %ebp
	testb	%al, %al
	jne	.LBB10_75
.LBB10_13:
	movq	(%r15), %rax
.Ltmp32:
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	*64(%rax)
.Ltmp33:
# BB#14:
.Ltmp34:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp35:
# BB#15:
	movl	$0, 8(%r14)
	movq	$_ZTV15CDummyOutStream+16, (%r14)
	movq	$0, 16(%r14)
.Ltmp37:
	movq	%r14, %rdi
	callq	*_ZTV15CDummyOutStream+24(%rip)
.Ltmp38:
# BB#16:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit
	movq	(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB10_18
# BB#17:
	movq	(%rbx), %rax
.Ltmp40:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp41:
.LBB10_18:                              # %.noexc111
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB10_20
# BB#19:
	movq	(%rdi), %rax
.Ltmp42:
	callq	*16(%rax)
.Ltmp43:
.LBB10_20:
	movq	%rbx, 16(%r14)
	movq	$0, 24(%r14)
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_23
# BB#21:
	movq	(%rdi), %rax
.Ltmp44:
	callq	*16(%rax)
.Ltmp45:
# BB#22:                                # %.noexc113
	movq	$0, (%rsp)
.LBB10_23:                              # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
.Ltmp46:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp47:
# BB#24:
.Ltmp49:
	movq	%rbx, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp50:
# BB#25:
	movq	(%rbx), %rax
.Ltmp52:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp53:
# BB#26:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit
.Ltmp55:
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp56:
# BB#27:
	movq	16(%r12), %rdi
	movq	24(%r12), %rsi
	movq	(%rdi), %rax
	addq	$3, %rsi
.Ltmp57:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp58:
# BB#28:
	testl	%ebp, %ebp
	jne	.LBB10_53
# BB#29:
.Ltmp60:
	movl	$56, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp61:
# BB#30:
	movl	$0, 16(%r13)
	movl	$_ZTVN9NCompress2NZ8CDecoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress2NZ8CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r13)
	movl	$0, 52(%r13)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 33(%r13)
	movdqu	%xmm0, 24(%r13)
.Ltmp63:
	movq	%r13, %rdi
	callq	*_ZTVN9NCompress2NZ8CDecoderE+24(%rip)
.Ltmp64:
# BB#31:                                # %_ZN9CMyComPtrI14ICompressCoderEaSEPS0_.exit
	movq	(%r13), %rax
	leaq	40(%r12), %rsi
.Ltmp66:
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
	movl	$1, %edx
	movq	%r13, %rdi
	callq	*48(%rax)
.Ltmp67:
# BB#32:
	testl	%eax, %eax
	je	.LBB10_33
.LBB10_50:
	movq	(%r14), %rax
.Ltmp71:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp72:
# BB#51:                                # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit125
	movq	(%r15), %rax
	xorl	%r14d, %r14d
.Ltmp73:
	movq	%r15, %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	callq	*72(%rax)
	movl	%eax, %ebp
.Ltmp74:
.LBB10_52:
	movq	(%r13), %rax
.Ltmp78:
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp79:
.LBB10_53:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit123
	movq	(%rbx), %rax
.Ltmp83:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp84:
# BB#54:                                # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit118
	testq	%r14, %r14
	je	.LBB10_56
# BB#55:
	movq	(%r14), %rax
.Ltmp88:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp89:
	jmp	.LBB10_56
.LBB10_33:
	movq	(%r13), %rax
	movq	16(%r12), %rsi
.Ltmp69:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%rbx, %r9
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp70:
# BB#34:
	testl	%ebp, %ebp
	je	.LBB10_49
# BB#35:
	movl	$2, 12(%rsp)            # 4-byte Folded Spill
	cmpl	$1, %ebp
	je	.LBB10_50
	jmp	.LBB10_52
.LBB10_49:                              # %.thread
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	jmp	.LBB10_50
.LBB10_58:
.Ltmp80:
	jmp	.LBB10_45
.LBB10_59:
.Ltmp68:
	jmp	.LBB10_60
.LBB10_46:                              # %.thread149
.Ltmp65:
	jmp	.LBB10_45
.LBB10_47:                              # %.thread142
.Ltmp62:
	jmp	.LBB10_45
.LBB10_62:
.Ltmp90:
	jmp	.LBB10_38
.LBB10_48:                              # %.thread146
.Ltmp75:
.LBB10_60:
	movq	%rdx, %r12
	movq	%rax, %rbp
	movq	(%r13), %rax
.Ltmp76:
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp77:
	jmp	.LBB10_61
.LBB10_63:
.Ltmp85:
	movq	%rdx, %r12
	movq	%rax, %rbp
	testq	%r14, %r14
	jne	.LBB10_65
	jmp	.LBB10_66
.LBB10_43:
.Ltmp54:
	jmp	.LBB10_41
.LBB10_42:
.Ltmp51:
	movq	%rdx, %r12
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB10_65
.LBB10_39:
.Ltmp39:
	jmp	.LBB10_38
.LBB10_44:
.Ltmp59:
.LBB10_45:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	%rdx, %r12
	movq	%rax, %rbp
.LBB10_61:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	(%rbx), %rax
.Ltmp81:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp82:
# BB#64:                                # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	testq	%r14, %r14
	jne	.LBB10_65
	jmp	.LBB10_66
.LBB10_68:
.Ltmp96:
	jmp	.LBB10_70
.LBB10_40:
.Ltmp48:
.LBB10_41:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.thread
	movq	%rdx, %r12
	movq	%rax, %rbp
.LBB10_65:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.thread
	movq	(%r14), %rax
.Ltmp86:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp87:
	jmp	.LBB10_66
.LBB10_37:
.Ltmp36:
.LBB10_38:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit108
	movq	%rdx, %r12
	movq	%rax, %rbp
.LBB10_66:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit108
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_71
# BB#67:
	movq	(%rdi), %rax
.Ltmp91:
	callq	*16(%rax)
.Ltmp92:
	jmp	.LBB10_71
.LBB10_76:
.Ltmp93:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_69:
.Ltmp29:
	jmp	.LBB10_70
.LBB10_36:
.Ltmp26:
.LBB10_70:
	movq	%rdx, %r12
	movq	%rax, %rbp
.LBB10_71:
	movq	%rbp, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r12d
	je	.LBB10_72
# BB#74:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB10_75
.LBB10_72:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp97:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp98:
# BB#77:
.LBB10_73:
.Ltmp99:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN8NArchive2NZ8CHandler7ExtractEPKjjiP23IArchiveExtractCallback, .Lfunc_end10-_ZN8NArchive2NZ8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\255\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\236\002"              # Call site table length
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin2   #     jumps to .Ltmp26
	.byte	3                       #   On action: 2
	.long	.Ltmp27-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin2   #     jumps to .Ltmp29
	.byte	3                       #   On action: 2
	.long	.Ltmp30-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp36-.Lfunc_begin2   #     jumps to .Ltmp36
	.byte	3                       #   On action: 2
	.long	.Ltmp94-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin2   #     jumps to .Ltmp96
	.byte	3                       #   On action: 2
	.long	.Ltmp32-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp35-.Ltmp32         #   Call between .Ltmp32 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin2   #     jumps to .Ltmp36
	.byte	3                       #   On action: 2
	.long	.Ltmp37-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin2   #     jumps to .Ltmp39
	.byte	3                       #   On action: 2
	.long	.Ltmp40-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp47-.Ltmp40         #   Call between .Ltmp40 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin2   #     jumps to .Ltmp48
	.byte	3                       #   On action: 2
	.long	.Ltmp49-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin2   #     jumps to .Ltmp51
	.byte	3                       #   On action: 2
	.long	.Ltmp52-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin2   #     jumps to .Ltmp54
	.byte	3                       #   On action: 2
	.long	.Ltmp55-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp58-.Ltmp55         #   Call between .Ltmp55 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin2   #     jumps to .Ltmp59
	.byte	3                       #   On action: 2
	.long	.Ltmp60-.Lfunc_begin2   # >> Call Site 11 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin2   #     jumps to .Ltmp62
	.byte	3                       #   On action: 2
	.long	.Ltmp63-.Lfunc_begin2   # >> Call Site 12 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin2   #     jumps to .Ltmp65
	.byte	3                       #   On action: 2
	.long	.Ltmp66-.Lfunc_begin2   # >> Call Site 13 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin2   #     jumps to .Ltmp68
	.byte	3                       #   On action: 2
	.long	.Ltmp71-.Lfunc_begin2   # >> Call Site 14 <<
	.long	.Ltmp74-.Ltmp71         #   Call between .Ltmp71 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin2   #     jumps to .Ltmp75
	.byte	3                       #   On action: 2
	.long	.Ltmp78-.Lfunc_begin2   # >> Call Site 15 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin2   #     jumps to .Ltmp80
	.byte	3                       #   On action: 2
	.long	.Ltmp83-.Lfunc_begin2   # >> Call Site 16 <<
	.long	.Ltmp84-.Ltmp83         #   Call between .Ltmp83 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin2   #     jumps to .Ltmp85
	.byte	3                       #   On action: 2
	.long	.Ltmp88-.Lfunc_begin2   # >> Call Site 17 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin2   #     jumps to .Ltmp90
	.byte	3                       #   On action: 2
	.long	.Ltmp69-.Lfunc_begin2   # >> Call Site 18 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp75-.Lfunc_begin2   #     jumps to .Ltmp75
	.byte	3                       #   On action: 2
	.long	.Ltmp76-.Lfunc_begin2   # >> Call Site 19 <<
	.long	.Ltmp92-.Ltmp76         #   Call between .Ltmp76 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin2   #     jumps to .Ltmp93
	.byte	1                       #   On action: 1
	.long	.Ltmp92-.Lfunc_begin2   # >> Call Site 20 <<
	.long	.Ltmp97-.Ltmp92         #   Call between .Ltmp92 and .Ltmp97
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin2   # >> Call Site 21 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin2   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin2   # >> Call Site 22 <<
	.long	.Lfunc_end10-.Ltmp98    #   Call between .Ltmp98 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive2NZ8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive2NZ8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive2NZ8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive2NZ8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive2NZ8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB11_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB11_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB11_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB11_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB11_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB11_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB11_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB11_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB11_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB11_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB11_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB11_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB11_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB11_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB11_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB11_32
.LBB11_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IInArchive(%rip), %cl
	jne	.LBB11_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_IInArchive+1(%rip), %cl
	jne	.LBB11_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_IInArchive+2(%rip), %cl
	jne	.LBB11_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_IInArchive+3(%rip), %cl
	jne	.LBB11_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_IInArchive+4(%rip), %cl
	jne	.LBB11_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_IInArchive+5(%rip), %cl
	jne	.LBB11_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_IInArchive+6(%rip), %cl
	jne	.LBB11_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_IInArchive+7(%rip), %cl
	jne	.LBB11_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_IInArchive+8(%rip), %cl
	jne	.LBB11_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_IInArchive+9(%rip), %cl
	jne	.LBB11_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_IInArchive+10(%rip), %cl
	jne	.LBB11_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_IInArchive+11(%rip), %cl
	jne	.LBB11_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_IInArchive+12(%rip), %cl
	jne	.LBB11_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_IInArchive+13(%rip), %cl
	jne	.LBB11_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_IInArchive+14(%rip), %cl
	jne	.LBB11_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %cl
	cmpb	IID_IInArchive+15(%rip), %cl
	jne	.LBB11_33
.LBB11_32:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB11_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZN8NArchive2NZ8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end11-_ZN8NArchive2NZ8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive2NZ8CHandler6AddRefEv,"axG",@progbits,_ZN8NArchive2NZ8CHandler6AddRefEv,comdat
	.weak	_ZN8NArchive2NZ8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandler6AddRefEv,@function
_ZN8NArchive2NZ8CHandler6AddRefEv:      # @_ZN8NArchive2NZ8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end12:
	.size	_ZN8NArchive2NZ8CHandler6AddRefEv, .Lfunc_end12-_ZN8NArchive2NZ8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive2NZ8CHandler7ReleaseEv,"axG",@progbits,_ZN8NArchive2NZ8CHandler7ReleaseEv,comdat
	.weak	_ZN8NArchive2NZ8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandler7ReleaseEv,@function
_ZN8NArchive2NZ8CHandler7ReleaseEv:     # @_ZN8NArchive2NZ8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB13_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB13_2:
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_ZN8NArchive2NZ8CHandler7ReleaseEv, .Lfunc_end13-_ZN8NArchive2NZ8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive2NZ8CHandlerD2Ev,"axG",@progbits,_ZN8NArchive2NZ8CHandlerD2Ev,comdat
	.weak	_ZN8NArchive2NZ8CHandlerD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandlerD2Ev,@function
_ZN8NArchive2NZ8CHandlerD2Ev:           # @_ZN8NArchive2NZ8CHandlerD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTVN8NArchive2NZ8CHandlerE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB14_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB14_1:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	retq
.Lfunc_end14:
	.size	_ZN8NArchive2NZ8CHandlerD2Ev, .Lfunc_end14-_ZN8NArchive2NZ8CHandlerD2Ev
	.cfi_endproc

	.section	.text._ZN8NArchive2NZ8CHandlerD0Ev,"axG",@progbits,_ZN8NArchive2NZ8CHandlerD0Ev,comdat
	.weak	_ZN8NArchive2NZ8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZ8CHandlerD0Ev,@function
_ZN8NArchive2NZ8CHandlerD0Ev:           # @_ZN8NArchive2NZ8CHandlerD0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -24
.Lcfi31:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive2NZ8CHandlerE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp100:
	callq	*16(%rax)
.Ltmp101:
.LBB15_2:                               # %_ZN8NArchive2NZ8CHandlerD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB15_3:
.Ltmp102:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end15:
	.size	_ZN8NArchive2NZ8CHandlerD0Ev, .Lfunc_end15-_ZN8NArchive2NZ8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp100-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin3  #     jumps to .Ltmp102
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Lfunc_end15-.Ltmp101   #   Call between .Ltmp101 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive2NZL9CreateArcEv,@function
_ZN8NArchive2NZL9CreateArcEv:           # @_ZN8NArchive2NZL9CreateArcEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 16
	movl	$48, %edi
	callq	_Znwm
	movl	$0, 8(%rax)
	movq	$_ZTVN8NArchive2NZ8CHandlerE+16, (%rax)
	movq	$0, 16(%rax)
	popq	%rcx
	retq
.Lfunc_end16:
	.size	_ZN8NArchive2NZL9CreateArcEv, .Lfunc_end16-_ZN8NArchive2NZL9CreateArcEv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_ZHandler.ii,@function
_GLOBAL__sub_I_ZHandler.ii:             # @_GLOBAL__sub_I_ZHandler.ii
	.cfi_startproc
# BB#0:
	movl	$_ZN8NArchive2NZL9g_ArcInfoE, %edi
	jmp	_Z11RegisterArcPK8CArcInfo # TAILCALL
.Lfunc_end17:
	.size	_GLOBAL__sub_I_ZHandler.ii, .Lfunc_end17-_GLOBAL__sub_I_ZHandler.ii
	.cfi_endproc

	.type	_ZN8NArchive2NZ6kPropsE,@object # @_ZN8NArchive2NZ6kPropsE
	.data
	.globl	_ZN8NArchive2NZ6kPropsE
	.p2align	4
_ZN8NArchive2NZ6kPropsE:
	.quad	0
	.long	8                       # 0x8
	.short	21                      # 0x15
	.zero	2
	.size	_ZN8NArchive2NZ6kPropsE, 16

	.type	_ZTVN8NArchive2NZ8CHandlerE,@object # @_ZTVN8NArchive2NZ8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive2NZ8CHandlerE
	.p2align	3
_ZTVN8NArchive2NZ8CHandlerE:
	.quad	0
	.quad	_ZTIN8NArchive2NZ8CHandlerE
	.quad	_ZN8NArchive2NZ8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive2NZ8CHandler6AddRefEv
	.quad	_ZN8NArchive2NZ8CHandler7ReleaseEv
	.quad	_ZN8NArchive2NZ8CHandlerD2Ev
	.quad	_ZN8NArchive2NZ8CHandlerD0Ev
	.quad	_ZN8NArchive2NZ8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.quad	_ZN8NArchive2NZ8CHandler5CloseEv
	.quad	_ZN8NArchive2NZ8CHandler16GetNumberOfItemsEPj
	.quad	_ZN8NArchive2NZ8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.quad	_ZN8NArchive2NZ8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.quad	_ZN8NArchive2NZ8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.quad	_ZN8NArchive2NZ8CHandler21GetNumberOfPropertiesEPj
	.quad	_ZN8NArchive2NZ8CHandler15GetPropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive2NZ8CHandler28GetNumberOfArchivePropertiesEPj
	.quad	_ZN8NArchive2NZ8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.size	_ZTVN8NArchive2NZ8CHandlerE, 136

	.type	_ZTSN8NArchive2NZ8CHandlerE,@object # @_ZTSN8NArchive2NZ8CHandlerE
	.globl	_ZTSN8NArchive2NZ8CHandlerE
	.p2align	4
_ZTSN8NArchive2NZ8CHandlerE:
	.asciz	"N8NArchive2NZ8CHandlerE"
	.size	_ZTSN8NArchive2NZ8CHandlerE, 24

	.type	_ZTS10IInArchive,@object # @_ZTS10IInArchive
	.section	.rodata._ZTS10IInArchive,"aG",@progbits,_ZTS10IInArchive,comdat
	.weak	_ZTS10IInArchive
_ZTS10IInArchive:
	.asciz	"10IInArchive"
	.size	_ZTS10IInArchive, 13

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI10IInArchive,@object # @_ZTI10IInArchive
	.section	.rodata._ZTI10IInArchive,"aG",@progbits,_ZTI10IInArchive,comdat
	.weak	_ZTI10IInArchive
	.p2align	4
_ZTI10IInArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IInArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI10IInArchive, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive2NZ8CHandlerE,@object # @_ZTIN8NArchive2NZ8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive2NZ8CHandlerE
	.p2align	4
_ZTIN8NArchive2NZ8CHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive2NZ8CHandlerE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI10IInArchive
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN8NArchive2NZ8CHandlerE, 56

	.type	_ZN8NArchive2NZL9g_ArcInfoE,@object # @_ZN8NArchive2NZL9g_ArcInfoE
	.data
	.p2align	3
_ZN8NArchive2NZL9g_ArcInfoE:
	.quad	.L.str
	.quad	.L.str.1
	.quad	.L.str.2
	.byte	5                       # 0x5
	.asciz	"\037\235\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.zero	3
	.long	2                       # 0x2
	.byte	0                       # 0x0
	.zero	3
	.quad	_ZN8NArchive2NZL9CreateArcEv
	.quad	0
	.size	_ZN8NArchive2NZL9g_ArcInfoE, 80

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	90                      # 0x5a
	.long	0                       # 0x0
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	122                     # 0x7a
	.long	32                      # 0x20
	.long	116                     # 0x74
	.long	97                      # 0x61
	.long	122                     # 0x7a
	.long	0                       # 0x0
	.size	.L.str.1, 24

	.type	.L.str.2,@object        # @.str.2
	.p2align	2
.L.str.2:
	.long	42                      # 0x2a
	.long	32                      # 0x20
	.long	46                      # 0x2e
	.long	116                     # 0x74
	.long	97                      # 0x61
	.long	114                     # 0x72
	.long	0                       # 0x0
	.size	.L.str.2, 28

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_ZHandler.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
