	.text
	.file	"hash.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 160
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	$500000, %ebx           # imm = 0x7A120
	cmpl	$2, %edi
	jne	.LBB0_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbx
.LBB0_2:
.Ltmp0:
	leaq	32(%rsp), %rdi
	leaq	24(%rsp), %rdx
	leaq	16(%rsp), %rcx
	leaq	8(%rsp), %r8
	movl	$100, %esi
	callq	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E
.Ltmp1:
# BB#3:                                 # %_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEC2Ev.exit
	testl	%ebx, %ebx
	jle	.LBB0_4
# BB#9:                                 # %.lr.ph92
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB0_10:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_14 Depth 2
                                        #     Child Loop BB0_17 Depth 2
	movl	$.L.str, %esi
	xorl	%eax, %eax
	leaq	80(%rsp), %rbx
	movq	%rbx, %rdi
	movl	%r12d, %edx
	callq	sprintf
	movq	%rbx, %rdi
	callq	strdup
	movq	%rax, %rbx
	movq	64(%rsp), %rsi
	incq	%rsi
.Ltmp3:
	leaq	32(%rsp), %rdi
	callq	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm
.Ltmp4:
# BB#11:                                # %.noexc
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	40(%rsp), %rcx
	movq	48(%rsp), %rsi
	subq	%rcx, %rsi
	sarq	$3, %rsi
	movb	(%rbx), %dl
	testb	%dl, %dl
	je	.LBB0_12
# BB#13:                                # %.lr.ph.i.i.i.i.i.i.i.preheader
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	%rbx, %rdi
	incq	%rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph.i.i.i.i.i.i.i
                                        #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax,4), %rbp
	movsbq	%dl, %rax
	addq	%rbp, %rax
	movzbl	(%rdi), %edx
	incq	%rdi
	testb	%dl, %dl
	jne	.LBB0_14
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_12:                               #   in Loop: Header=BB0_10 Depth=1
	xorl	%eax, %eax
.LBB0_15:                               # %_ZNK9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE10_M_bkt_numERKS5_.exit.i.i
                                        #   in Loop: Header=BB0_10 Depth=1
	xorl	%edx, %edx
	divq	%rsi
	movq	%rdx, %r13
	movq	(%rcx,%r13,8), %r15
	testq	%r15, %r15
	je	.LBB0_20
# BB#16:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	%r15, %r14
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph.i.i
                                        #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r14), %rdi
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_18
# BB#19:                                #   in Loop: Header=BB0_17 Depth=2
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB0_17
.LBB0_20:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_10 Depth=1
.Ltmp5:
	movl	$24, %edi
	callq	_Znwm
.Ltmp6:
# BB#21:                                # %.noexc32
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	%rax, %r14
	addq	$8, %r14
	movq	%rbx, 8(%rax)
	movl	$0, 16(%rax)
	movq	%r15, (%rax)
	movq	40(%rsp), %rcx
	movq	%rax, (%rcx,%r13,8)
	incq	64(%rsp)
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_10 Depth=1
	addq	$8, %r14
.LBB0_22:                               # %.loopexit80
                                        #   in Loop: Header=BB0_10 Depth=1
	movl	%r12d, 8(%r14)
	cmpl	72(%rsp), %r12d         # 4-byte Folded Reload
	leal	1(%r12), %eax
	movl	%eax, %r12d
	jl	.LBB0_10
# BB#5:                                 # %.preheader
	movq	72(%rsp), %r12          # 8-byte Reload
	xorl	%r13d, %r13d
	testl	%r12d, %r12d
	jle	.LBB0_34
	.p2align	4, 0x90
.LBB0_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_25 Depth 2
                                        #     Child Loop BB0_28 Depth 2
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	leaq	80(%rsp), %rbx
	movq	%rbx, %rdi
	movl	%r12d, %edx
	callq	sprintf
	movq	%rbx, %rdi
	callq	strdup
	movq	%rax, %rbx
	movq	64(%rsp), %rsi
	incq	%rsi
.Ltmp8:
	leaq	32(%rsp), %rdi
	callq	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm
.Ltmp9:
# BB#7:                                 # %.noexc45
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	40(%rsp), %rcx
	movq	48(%rsp), %rsi
	subq	%rcx, %rsi
	sarq	$3, %rsi
	movb	(%rbx), %dl
	testb	%dl, %dl
	je	.LBB0_8
# BB#24:                                # %.lr.ph.i.i.i.i.i.i.i35.preheader
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	%rbx, %rdi
	incq	%rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_25:                               # %.lr.ph.i.i.i.i.i.i.i35
                                        #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax,4), %rbp
	movsbq	%dl, %rax
	addq	%rbp, %rax
	movzbl	(%rdi), %edx
	incq	%rdi
	testb	%dl, %dl
	jne	.LBB0_25
	jmp	.LBB0_26
	.p2align	4, 0x90
.LBB0_8:                                #   in Loop: Header=BB0_6 Depth=1
	xorl	%eax, %eax
.LBB0_26:                               # %_ZNK9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE10_M_bkt_numERKS5_.exit.i.i37
                                        #   in Loop: Header=BB0_6 Depth=1
	xorl	%edx, %edx
	divq	%rsi
	movq	%rdx, %rbp
	movq	(%rcx,%rbp,8), %r15
	testq	%r15, %r15
	je	.LBB0_31
# BB#27:                                # %.lr.ph.i.i39.preheader
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	%r15, %r14
	.p2align	4, 0x90
.LBB0_28:                               # %.lr.ph.i.i39
                                        #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r14), %rdi
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_29
# BB#30:                                #   in Loop: Header=BB0_28 Depth=2
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB0_28
.LBB0_31:                               # %._crit_edge.i.i43
                                        #   in Loop: Header=BB0_6 Depth=1
.Ltmp10:
	movl	$24, %edi
	callq	_Znwm
.Ltmp11:
# BB#32:                                # %.noexc46
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	%rax, %r14
	addq	$8, %r14
	movq	%rbx, 8(%rax)
	movl	$0, 16(%rax)
	movq	%r15, (%rax)
	movq	40(%rsp), %rcx
	movq	%rax, (%rcx,%rbp,8)
	incq	64(%rsp)
	jmp	.LBB0_33
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_6 Depth=1
	addq	$8, %r14
.LBB0_33:                               # %.loopexit
                                        #   in Loop: Header=BB0_6 Depth=1
	xorl	%eax, %eax
	cmpl	$0, 8(%r14)
	setne	%al
	addl	%eax, %r13d
	cmpl	$1, %r12d
	leal	-1(%r12), %eax
	movl	%eax, %r12d
	jg	.LBB0_6
	jmp	.LBB0_34
.LBB0_4:
	xorl	%r13d, %r13d
.LBB0_34:                               # %._crit_edge
.Ltmp13:
	movl	$_ZSt4cout, %edi
	movl	%r13d, %esi
	callq	_ZNSolsEi
	movq	%rax, %rbx
.Ltmp14:
# BB#35:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	movq	240(%rbx,%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB0_36
# BB#38:                                # %.noexc69
	cmpb	$0, 56(%rbp)
	je	.LBB0_40
# BB#39:
	movb	67(%rbp), %al
	jmp	.LBB0_42
.LBB0_40:
.Ltmp15:
	movq	%rbp, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp16:
# BB#41:                                # %.noexc71
	movq	(%rbp), %rax
.Ltmp17:
	movl	$10, %esi
	movq	%rbp, %rdi
	callq	*48(%rax)
.Ltmp18:
.LBB0_42:                               # %.noexc49
.Ltmp19:
	movsbl	%al, %esi
	movq	%rbx, %rdi
	callq	_ZNSo3putEc
.Ltmp20:
# BB#43:                                # %.noexc50
.Ltmp21:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp22:
# BB#44:                                # %_ZNSolsEPFRSoS_E.exit
	cmpq	$0, 64(%rsp)
	je	.LBB0_45
# BB#46:                                # %.preheader.i.i.i
	movq	40(%rsp), %rax
	movq	48(%rsp), %rdi
	cmpq	%rax, %rdi
	je	.LBB0_52
# BB#47:                                # %.lr.ph16.i.i.i
	xorl	%ebx, %ebx
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB0_48:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_49 Depth 2
	movq	(%rdi,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB0_51
	.p2align	4, 0x90
.LBB0_49:                               # %.lr.ph.i.i.i
                                        #   Parent Loop BB0_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbp
	movq	%rax, %rdi
	callq	_ZdlPv
	testq	%rbp, %rbp
	movq	%rbp, %rax
	jne	.LBB0_49
# BB#50:                                # %._crit_edge.loopexit.i.i.i
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	40(%rsp), %rdi
.LBB0_51:                               # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	$0, (%rdi,%rbx,8)
	incq	%rbx
	movq	40(%rsp), %rdi
	movq	48(%rsp), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.LBB0_48
.LBB0_52:                               # %._crit_edge17.i.i.i
	movq	$0, 64(%rsp)
	testq	%rdi, %rdi
	je	.LBB0_55
.LBB0_54:
	callq	_ZdlPv
.LBB0_55:                               # %_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEED2Ev.exit
	xorl	%eax, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_45:                               # %._ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE5clearEv.exit_crit_edge.i.i
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_54
	jmp	.LBB0_55
.LBB0_36:
.Ltmp23:
	callq	_ZSt16__throw_bad_castv
.Ltmp24:
# BB#37:                                # %.noexc73
.LBB0_69:
.Ltmp2:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_56:
.Ltmp25:
	jmp	.LBB0_57
.LBB0_70:
.Ltmp12:
	jmp	.LBB0_57
.LBB0_23:
.Ltmp7:
.LBB0_57:
	movq	%rax, %r14
	cmpq	$0, 64(%rsp)
	je	.LBB0_58
# BB#59:                                # %.preheader.i.i.i56
	movq	40(%rsp), %rax
	movq	48(%rsp), %rdi
	cmpq	%rax, %rdi
	je	.LBB0_65
# BB#60:                                # %.lr.ph16.i.i.i57
	xorl	%ebp, %ebp
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB0_61:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_62 Depth 2
	movq	(%rdi,%rbp,8), %rax
	testq	%rax, %rax
	je	.LBB0_64
	.p2align	4, 0x90
.LBB0_62:                               # %.lr.ph.i.i.i63
                                        #   Parent Loop BB0_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbx
	movq	%rax, %rdi
	callq	_ZdlPv
	testq	%rbx, %rbx
	movq	%rbx, %rax
	jne	.LBB0_62
# BB#63:                                # %._crit_edge.loopexit.i.i.i65
                                        #   in Loop: Header=BB0_61 Depth=1
	movq	40(%rsp), %rdi
.LBB0_64:                               # %._crit_edge.i.i.i66
                                        #   in Loop: Header=BB0_61 Depth=1
	movq	$0, (%rdi,%rbp,8)
	incq	%rbp
	movq	40(%rsp), %rdi
	movq	48(%rsp), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbp
	jb	.LBB0_61
.LBB0_65:                               # %._crit_edge17.i.i.i59
	movq	$0, 64(%rsp)
	testq	%rdi, %rdi
	je	.LBB0_68
.LBB0_67:
	callq	_ZdlPv
.LBB0_68:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_58:                               # %._ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE5clearEv.exit_crit_edge.i.i55
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_67
	jmp	.LBB0_68
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp11-.Ltmp8          #   Call between .Ltmp8 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp24-.Ltmp13         #   Call between .Ltmp13 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Lfunc_end0-.Ltmp24     #   Call between .Ltmp24 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E,"axG",@progbits,_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E,comdat
	.weak	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E
	.p2align	4, 0x90
	.type	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E,@function
_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E: # @_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r12, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	8(%r15), %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%r15)
	movups	%xmm0, 8(%r15)
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE, %eax
	movl	$29, %ecx
	jmp	.LBB1_1
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	leaq	(%rax,%rcx,8), %rax
	addq	$8, %rax
	decq	%rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	testq	%rdx, %rdx
	jle	.LBB1_4
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	movq	%rdx, %rcx
	shrq	%rcx
	cmpq	%rsi, (%rax,%rcx,8)
	jae	.LBB1_1
	jmp	.LBB1_3
.LBB1_4:                                # %_ZNK9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE12_M_next_sizeEm.exit.i
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE+232, %ecx
	cmpq	%rcx, %rax
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE+224, %ecx
	cmovneq	%rax, %rcx
	movq	(%rcx), %rbx
	movq	%rbx, %rax
	shrq	$61, %rax
	jne	.LBB1_5
# BB#7:
	testq	%rbx, %rbx
	je	.LBB1_8
# BB#9:                                 # %_ZNSt12_Vector_baseIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE11_M_allocateEm.exit.i.i
	leaq	(,%rbx,8), %rdi
.Ltmp26:
	callq	_Znwm
	movq	%rax, %r12
.Ltmp27:
# BB#10:                                # %_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE20_M_allocate_and_copyIPS8_EESC_mT_SD_.exit.i
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_12
# BB#11:
	callq	_ZdlPv
.LBB1_12:                               # %_ZNSt12_Vector_baseIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE13_M_deallocateEPS8_m.exit.i
	movq	%r12, 8(%r15)
	movq	%r12, 16(%r15)
	leaq	(%r12,%rbx,8), %rax
	movq	%rax, 24(%r15)
	jmp	.LBB1_13
.LBB1_8:
	xorl	%r12d, %r12d
.LBB1_13:                               # %.noexc
	movq	$0, (%rsp)
.Ltmp28:
	movq	%rsp, %rcx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_
.Ltmp29:
# BB#14:
	movq	$0, 32(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB1_5:
.Ltmp30:
	movl	$.L.str.2, %edi
	callq	_ZSt20__throw_length_errorPKc
.Ltmp31:
# BB#6:                                 # %.noexc9
.LBB1_15:
.Ltmp32:
	movq	%rax, %rbx
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_17
# BB#16:
	callq	_ZdlPv
.LBB1_17:                               # %_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E, .Lfunc_end1-_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp26-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp31-.Ltmp26         #   Call between .Ltmp26 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin1   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end1-.Ltmp31     #   Call between .Ltmp31 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_,"axG",@progbits,_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_,comdat
	.weak	_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_
	.p2align	4, 0x90
	.type	_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_,@function
_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_: # @_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 64
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB2_78
# BB#1:
	movq	8(%r15), %r12
	movq	16(%r15), %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%r14, %rax
	jae	.LBB2_2
# BB#52:
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	movq	%r15, %rbp
	movq	(%r15), %r15
	subq	%r15, %r12
	sarq	$3, %r12
	movq	%rax, %rcx
	subq	%r12, %rcx
	cmpq	%r14, %rcx
	jb	.LBB2_79
# BB#53:                                # %_ZNKSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE12_M_check_lenEmS4_.exit
	cmpq	%r14, %r12
	movq	%r12, %rcx
	cmovbq	%r14, %rcx
	leaq	(%rcx,%r12), %rdx
	cmpq	%rax, %rdx
	cmovaq	%rax, %rdx
	addq	%r12, %rcx
	cmovbq	%rax, %rdx
	testq	%rdx, %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	je	.LBB2_54
# BB#55:
	cmpq	%rax, %rdx
	ja	.LBB2_80
# BB#56:                                # %_ZN9__gnu_cxx14__alloc_traitsISaIPNS_15_Hashtable_nodeISt4pairIKPKciEEEEE8allocateERS9_m.exit.i
	leaq	(,%rdx,8), %rdi
	callq	_Znwm
	movq	%rax, %r12
	jmp	.LBB2_57
.LBB2_2:
	movabsq	$4611686018427387900, %r9 # imm = 0x3FFFFFFFFFFFFFFC
	movq	(%r13), %rbp
	movq	%r12, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %r13
	sarq	$3, %r13
	cmpq	%r14, %r13
	jbe	.LBB2_21
# BB#3:
	leaq	(,%r14,8), %rdx
	movq	%r12, %r13
	subq	%rdx, %r13
	testq	%rdx, %rdx
	je	.LBB2_4
# BB#5:
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	memmove
	movabsq	$4611686018427387900, %r9 # imm = 0x3FFFFFFFFFFFFFFC
	movq	8(%r15), %rax
	jmp	.LBB2_6
.LBB2_21:
	subq	%r13, %r14
	je	.LBB2_22
# BB#23:                                # %.lr.ph.i.i.i.i.i64.preheader
	cmpq	$3, %r14
	movq	%r14, %rsi
	movq	%r12, %rax
	jbe	.LBB2_35
# BB#24:                                # %min.iters.checked
	movq	%r14, %r8
	andq	$-4, %r8
	movq	%r14, %rcx
	andq	$-4, %rcx
	je	.LBB2_25
# BB#26:                                # %vector.ph
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%rcx), %rax
	movl	%eax, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB2_27
# BB#28:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_29:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%r12,%rdi,8)
	movdqu	%xmm0, 16(%r12,%rdi,8)
	addq	$4, %rdi
	incq	%rsi
	jne	.LBB2_29
# BB#30:                                # %vector.body.prol.loopexit
	cmpq	$28, %rax
	jae	.LBB2_31
	jmp	.LBB2_33
.LBB2_54:
	xorl	%r12d, %r12d
.LBB2_57:                               # %_ZNSt12_Vector_baseIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE11_M_allocateEm.exit
	movq	%rbx, %rdi
	subq	%r15, %rdi
	sarq	$3, %rdi
	leaq	(%r12,%rdi,8), %rax
	movq	(%r13), %rcx
	cmpq	$4, %r14
	jae	.LBB2_59
# BB#58:
	movq	%r14, %rdx
	movq	%rbp, %r15
	jmp	.LBB2_70
.LBB2_59:                               # %min.iters.checked138
	movq	%r14, %r8
	andq	$-4, %r8
	movq	%r14, %r9
	andq	$-4, %r9
	movq	%rbp, %r15
	je	.LBB2_60
# BB#61:                                # %vector.ph142
	movd	%rcx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %r10
	movl	%r10d, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB2_62
# BB#63:                                # %vector.body134.prol.preheader
	leaq	16(%r12,%rdi,8), %rbp
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_64:                               # %vector.body134.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbp,%rsi,8)
	movdqu	%xmm0, (%rbp,%rsi,8)
	addq	$4, %rsi
	incq	%rdx
	jne	.LBB2_64
# BB#65:                                # %vector.body134.prol.loopexit
	cmpq	$28, %r10
	jae	.LBB2_66
	jmp	.LBB2_68
.LBB2_4:
	movq	%r12, %rax
.LBB2_6:                                # %_ZSt22__uninitialized_move_aIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEES9_SaIS8_EET0_T_SC_SB_RT1_.exit
	leaq	(%rax,%r14,8), %rax
	movq	%rax, 8(%r15)
	subq	%rbx, %r13
	movq	%r13, %rax
	sarq	$3, %rax
	je	.LBB2_8
# BB#7:
	shlq	$3, %rax
	subq	%rax, %r12
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	%r9, %r15
	callq	memmove
	movq	%r15, %r9
.LBB2_8:                                # %.lr.ph.i.i68.preheader
	leaq	-8(,%r14,8), %rax
	shrq	$3, %rax
	incq	%rax
	cmpq	$4, %rax
	movq	%rbx, %rcx
	jb	.LBB2_19
# BB#9:                                 # %min.iters.checked120
	andq	%rax, %r9
	movq	%rbx, %rcx
	je	.LBB2_19
# BB#10:                                # %vector.ph124
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %rcx
	movl	%ecx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB2_11
# BB#12:                                # %vector.body116.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_13:                               # %vector.body116.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rbx,%rdx,8)
	movdqu	%xmm0, 16(%rbx,%rdx,8)
	addq	$4, %rdx
	incq	%rsi
	jne	.LBB2_13
# BB#14:                                # %vector.body116.prol.loopexit
	cmpq	$28, %rcx
	jae	.LBB2_15
	jmp	.LBB2_17
.LBB2_22:
	movq	%r12, %rdi
	jmp	.LBB2_37
.LBB2_60:
	movq	%r14, %rdx
	jmp	.LBB2_70
.LBB2_62:
	xorl	%esi, %esi
	cmpq	$28, %r10
	jb	.LBB2_68
.LBB2_66:                               # %vector.ph142.new
	movq	%r9, %rdx
	subq	%rsi, %rdx
	addq	%rsi, %rdi
	leaq	240(%r12,%rdi,8), %rsi
	.p2align	4, 0x90
.LBB2_67:                               # %vector.body134
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rsi)
	movdqu	%xmm0, -224(%rsi)
	movdqu	%xmm0, -208(%rsi)
	movdqu	%xmm0, -192(%rsi)
	movdqu	%xmm0, -176(%rsi)
	movdqu	%xmm0, -160(%rsi)
	movdqu	%xmm0, -144(%rsi)
	movdqu	%xmm0, -128(%rsi)
	movdqu	%xmm0, -112(%rsi)
	movdqu	%xmm0, -96(%rsi)
	movdqu	%xmm0, -80(%rsi)
	movdqu	%xmm0, -64(%rsi)
	movdqu	%xmm0, -48(%rsi)
	movdqu	%xmm0, -32(%rsi)
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	addq	$256, %rsi              # imm = 0x100
	addq	$-32, %rdx
	jne	.LBB2_67
.LBB2_68:                               # %middle.block135
	cmpq	%r14, %r9
	je	.LBB2_71
# BB#69:
	movq	%r14, %rdx
	subq	%r8, %rdx
	leaq	(%rax,%r9,8), %rax
	.p2align	4, 0x90
.LBB2_70:                               # %.lr.ph.i.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rax)
	addq	$8, %rax
	decq	%rdx
	jne	.LBB2_70
.LBB2_71:                               # %_ZSt24__uninitialized_fill_n_aIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEEmS8_S8_ET_SA_T0_RKT1_RSaIT2_E.exit
	movq	(%r15), %r13
	movq	%rbx, %rdx
	subq	%r13, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB2_73
# BB#72:
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	memmove
.LBB2_73:
	leaq	(%r12,%rbp,8), %rax
	leaq	(%rax,%r14,8), %r14
	movq	8(%r15), %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB2_75
# BB#74:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	memmove
.LBB2_75:
	leaq	(%r14,%rbp,8), %rbx
	testq	%r13, %r13
	je	.LBB2_77
# BB#76:
	movq	%r13, %rdi
	callq	_ZdlPv
.LBB2_77:                               # %_ZNSt12_Vector_baseIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE13_M_deallocateEPS8_m.exit57
	movq	%r12, (%r15)
	movq	%rbx, 8(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%r12,%rax,8), %rax
	movq	%rax, 16(%r15)
	jmp	.LBB2_78
.LBB2_25:
	movq	%r14, %rsi
	movq	%r12, %rax
	jmp	.LBB2_35
.LBB2_11:
	xorl	%edx, %edx
	cmpq	$28, %rcx
	jb	.LBB2_17
.LBB2_15:                               # %vector.ph124.new
	movq	%r9, %rcx
	subq	%rdx, %rcx
	leaq	240(%rbx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB2_16:                               # %vector.body116
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdx)
	movdqu	%xmm0, -224(%rdx)
	movdqu	%xmm0, -208(%rdx)
	movdqu	%xmm0, -192(%rdx)
	movdqu	%xmm0, -176(%rdx)
	movdqu	%xmm0, -160(%rdx)
	movdqu	%xmm0, -144(%rdx)
	movdqu	%xmm0, -128(%rdx)
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm0, -96(%rdx)
	movdqu	%xmm0, -80(%rdx)
	movdqu	%xmm0, -64(%rdx)
	movdqu	%xmm0, -48(%rdx)
	movdqu	%xmm0, -32(%rdx)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-32, %rcx
	jne	.LBB2_16
.LBB2_17:                               # %middle.block117
	cmpq	%r9, %rax
	je	.LBB2_78
# BB#18:
	leaq	(%rbx,%r9,8), %rcx
.LBB2_19:                               # %.lr.ph.i.i68.preheader159
	leaq	(%rbx,%r14,8), %rax
	.p2align	4, 0x90
.LBB2_20:                               # %.lr.ph.i.i68
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB2_20
	jmp	.LBB2_78
.LBB2_27:
	xorl	%edi, %edi
	cmpq	$28, %rax
	jb	.LBB2_33
.LBB2_31:                               # %vector.ph.new
	movq	%rcx, %rsi
	subq	%rdi, %rsi
	leaq	240(%r12,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB2_32:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdi)
	movdqu	%xmm0, -224(%rdi)
	movdqu	%xmm0, -208(%rdi)
	movdqu	%xmm0, -192(%rdi)
	movdqu	%xmm0, -176(%rdi)
	movdqu	%xmm0, -160(%rdi)
	movdqu	%xmm0, -144(%rdi)
	movdqu	%xmm0, -128(%rdi)
	movdqu	%xmm0, -112(%rdi)
	movdqu	%xmm0, -96(%rdi)
	movdqu	%xmm0, -80(%rdi)
	movdqu	%xmm0, -64(%rdi)
	movdqu	%xmm0, -48(%rdi)
	movdqu	%xmm0, -32(%rdi)
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	addq	$256, %rdi              # imm = 0x100
	addq	$-32, %rsi
	jne	.LBB2_32
.LBB2_33:                               # %middle.block
	cmpq	%rcx, %r14
	je	.LBB2_36
# BB#34:
	movq	%r14, %rsi
	subq	%r8, %rsi
	leaq	(%r12,%rcx,8), %rax
	.p2align	4, 0x90
.LBB2_35:                               # %.lr.ph.i.i.i.i.i64
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rax)
	addq	$8, %rax
	decq	%rsi
	jne	.LBB2_35
.LBB2_36:                               # %._crit_edge.loopexit.i.i.i.i.i61
	leaq	(%r12,%r14,8), %rdi
.LBB2_37:                               # %_ZSt24__uninitialized_fill_n_aIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEEmS8_S8_ET_SA_T0_RKT1_RSaIT2_E.exit66
	movq	%rdi, 8(%r15)
	testq	%r13, %r13
	je	.LBB2_39
# BB#38:
	movq	%rbx, %rsi
	movq	%r9, %r14
	callq	memmove
	movq	%r14, %r9
	movq	8(%r15), %rdi
.LBB2_39:                               # %_ZSt22__uninitialized_move_aIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEES9_SaIS8_EET0_T_SC_SB_RT1_.exit59
	leaq	(%rdi,%r13,8), %rax
	movq	%rax, 8(%r15)
	cmpq	%rbx, %r12
	je	.LBB2_78
# BB#40:                                # %.lr.ph.i.i.preheader
	leaq	-8(%r12), %rax
	subq	%rbx, %rax
	shrq	$3, %rax
	incq	%rax
	cmpq	$4, %rax
	jb	.LBB2_51
# BB#41:                                # %min.iters.checked102
	andq	%rax, %r9
	je	.LBB2_51
# BB#42:                                # %vector.ph106
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %rcx
	movl	%ecx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB2_43
# BB#44:                                # %vector.body96.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_45:                               # %vector.body96.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rbx,%rdx,8)
	movdqu	%xmm0, 16(%rbx,%rdx,8)
	addq	$4, %rdx
	incq	%rsi
	jne	.LBB2_45
# BB#46:                                # %vector.body96.prol.loopexit
	cmpq	$28, %rcx
	jae	.LBB2_47
	jmp	.LBB2_49
.LBB2_43:
	xorl	%edx, %edx
	cmpq	$28, %rcx
	jb	.LBB2_49
.LBB2_47:                               # %vector.ph106.new
	movq	%r9, %rcx
	subq	%rdx, %rcx
	leaq	240(%rbx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB2_48:                               # %vector.body96
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdx)
	movdqu	%xmm0, -224(%rdx)
	movdqu	%xmm0, -208(%rdx)
	movdqu	%xmm0, -192(%rdx)
	movdqu	%xmm0, -176(%rdx)
	movdqu	%xmm0, -160(%rdx)
	movdqu	%xmm0, -144(%rdx)
	movdqu	%xmm0, -128(%rdx)
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm0, -96(%rdx)
	movdqu	%xmm0, -80(%rdx)
	movdqu	%xmm0, -64(%rdx)
	movdqu	%xmm0, -48(%rdx)
	movdqu	%xmm0, -32(%rdx)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-32, %rcx
	jne	.LBB2_48
.LBB2_49:                               # %middle.block97
	cmpq	%r9, %rax
	je	.LBB2_78
# BB#50:
	leaq	(%rbx,%r9,8), %rbx
	.p2align	4, 0x90
.LBB2_51:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.LBB2_51
.LBB2_78:                               # %_ZSt4fillIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEES8_EvT_SA_RKT0_.exit69
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_79:
	movl	$.L.str.3, %edi
	callq	_ZSt20__throw_length_errorPKc
.LBB2_80:
	callq	_ZSt17__throw_bad_allocv
.Lfunc_end2:
	.size	_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_, .Lfunc_end2-_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_
	.cfi_endproc

	.section	.text._ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm,"axG",@progbits,_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm,comdat
	.weak	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm
	.p2align	4, 0x90
	.type	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm,@function
_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm: # @_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 64
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	16(%r14), %r12
	subq	8(%r14), %r12
	sarq	$3, %r12
	cmpq	%rsi, %r12
	jae	.LBB3_18
# BB#1:                                 # %.outer.i.i.i.i.preheader
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE, %eax
	movl	$29, %ecx
	jmp	.LBB3_2
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	leaq	(%rax,%rcx,8), %rax
	addq	$8, %rax
	decq	%rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	testq	%rdx, %rdx
	jle	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	%rdx, %rcx
	shrq	%rcx
	cmpq	%rsi, (%rax,%rcx,8)
	jae	.LBB3_2
	jmp	.LBB3_4
.LBB3_5:                                # %_ZNK9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE12_M_next_sizeEm.exit
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE+232, %ecx
	cmpq	%rcx, %rax
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE+224, %ecx
	cmovneq	%rax, %rcx
	movq	(%rcx), %r13
	cmpq	%r12, %r13
	jbe	.LBB3_18
# BB#6:
	movq	%r13, %rax
	shrq	$61, %rax
	jne	.LBB3_19
# BB#7:                                 # %_ZN9__gnu_cxx14__alloc_traitsISaIPNS_15_Hashtable_nodeISt4pairIKPKciEEEEE8allocateERS9_m.exit.i.i.i.i
	leaq	(,%r13,8), %rbx
	movq	%rbx, %rdi
	callq	_Znwm
	movq	%rax, %r15
	leaq	(%r15,%r13,8), %rbp
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	memset
	testq	%r12, %r12
	movq	8(%r14), %rdi
	je	.LBB3_8
# BB#9:                                 # %.preheader.preheader
	xorl	%ecx, %ecx
	movq	%rbp, %r8
	jmp	.LBB3_10
	.p2align	4, 0x90
.LBB3_15:                               # %.loopexit
                                        #   in Loop: Header=BB3_10 Depth=1
	xorl	%edx, %edx
	divq	%r13
	movq	(%rsi), %rax
	movq	%rax, (%rdi,%rcx,8)
	movq	(%r15,%rdx,8), %rax
	movq	%rax, (%rsi)
	movq	%rsi, (%r15,%rdx,8)
	movq	8(%r14), %rdi
.LBB3_10:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_14 Depth 2
	movq	(%rdi,%rcx,8), %rsi
	testq	%rsi, %rsi
	je	.LBB3_16
# BB#11:                                # %.lr.ph
                                        #   in Loop: Header=BB3_10 Depth=1
	movq	8(%rsi), %rdx
	movb	(%rdx), %bl
	testb	%bl, %bl
	je	.LBB3_12
# BB#13:                                # %.lr.ph.i.i.i.i.preheader
                                        #   in Loop: Header=BB3_10 Depth=1
	incq	%rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_14:                               # %.lr.ph.i.i.i.i
                                        #   Parent Loop BB3_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax,4), %rbp
	movsbq	%bl, %rax
	addq	%rbp, %rax
	movzbl	(%rdx), %ebx
	incq	%rdx
	testb	%bl, %bl
	jne	.LBB3_14
	jmp	.LBB3_15
	.p2align	4, 0x90
.LBB3_12:                               #   in Loop: Header=BB3_10 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_15
.LBB3_16:                               # %._crit_edge
                                        #   in Loop: Header=BB3_10 Depth=1
	incq	%rcx
	cmpq	%r12, %rcx
	jne	.LBB3_10
	jmp	.LBB3_17
.LBB3_8:
	movq	%rbp, %r8
.LBB3_17:                               # %._crit_edge85
	movq	%r15, 8(%r14)
	movq	%r8, 16(%r14)
	movq	%r8, 24(%r14)
	testq	%rdi, %rdi
	je	.LBB3_18
# BB#20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZdlPv                  # TAILCALL
.LBB3_18:                               # %_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_19:                               # %.noexc.i.i
	callq	_ZSt17__throw_bad_allocv
.Lfunc_end3:
	.size	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm, .Lfunc_end3-_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_hash.ii,@function
_GLOBAL__sub_I_hash.ii:                 # @_GLOBAL__sub_I_hash.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end4:
	.size	_GLOBAL__sub_I_hash.ii, .Lfunc_end4-_GLOBAL__sub_I_hash.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%x"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%d"
	.size	.L.str.1, 3

	.type	_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE,@object # @_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE
	.section	.rodata._ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE,"aG",@progbits,_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE,comdat
	.weak	_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE
	.p2align	4
_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE:
	.quad	5                       # 0x5
	.quad	53                      # 0x35
	.quad	97                      # 0x61
	.quad	193                     # 0xc1
	.quad	389                     # 0x185
	.quad	769                     # 0x301
	.quad	1543                    # 0x607
	.quad	3079                    # 0xc07
	.quad	6151                    # 0x1807
	.quad	12289                   # 0x3001
	.quad	24593                   # 0x6011
	.quad	49157                   # 0xc005
	.quad	98317                   # 0x1800d
	.quad	196613                  # 0x30005
	.quad	393241                  # 0x60019
	.quad	786433                  # 0xc0001
	.quad	1572869                 # 0x180005
	.quad	3145739                 # 0x30000b
	.quad	6291469                 # 0x60000d
	.quad	12582917                # 0xc00005
	.quad	25165843                # 0x1800013
	.quad	50331653                # 0x3000005
	.quad	100663319               # 0x6000017
	.quad	201326611               # 0xc000013
	.quad	402653189               # 0x18000005
	.quad	805306457               # 0x30000059
	.quad	1610612741              # 0x60000005
	.quad	3221225473              # 0xc0000001
	.quad	4294967291              # 0xfffffffb
	.size	_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE, 232

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"vector::reserve"
	.size	.L.str.2, 16

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"vector::_M_fill_insert"
	.size	.L.str.3, 23

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_hash.ii

	.ident	"clang version 5.0.0 (https://github.com/aqjune/clang-intptr.git 8e2e88f063bfae95e3be980dbf26473d5a086d27) (https://github.com/aqjune/llvm-intptr.git 51d41dcad277119de970b737044ce4a7e91ad33d)"
	.section	".note.GNU-stack","",@progbits
