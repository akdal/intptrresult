	.text
	.file	"bigfib.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	_ZN9Fibonacci10get_numberEj
	.p2align	4, 0x90
	.type	_ZN9Fibonacci10get_numberEj,@function
_ZN9Fibonacci10get_numberEj:            # @_ZN9Fibonacci10get_numberEj
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 240
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, 152(%rsp)         # 8-byte Spill
	leal	1(%rbx), %esi
	movq	%r14, %rdi
	callq	_ZNSt6vectorI6BigIntSaIS0_EE7reserveEm
	movq	(%r14), %r13
	movq	8(%r14), %rax
	subq	%r13, %rax
	shrq	$3, %rax
	imull	$-1431655765, %eax, %r12d # imm = 0xAAAAAAAB
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	cmpl	%ebx, %r12d
	ja	.LBB0_9
# BB#1:
	leaq	128(%rsp), %r15
	leaq	104(%rsp), %rbx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	cmpl	$1, %r12d
	je	.LBB0_34
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	testl	%r12d, %r12d
	jne	.LBB0_75
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	$0, 160(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movq	$0, 16(%rsp)
.Ltmp22:
	xorl	%esi, %esi
	movq	%rsp, %rdi
	leaq	160(%rsp), %rdx
	callq	_ZNSt6vectorImSaImEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPmS1_EERKm
.Ltmp23:
# BB#5:                                 # %_ZN6BigIntC2Em.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	8(%r14), %r13
	cmpq	16(%r14), %r13
	je	.LBB0_25
# BB#6:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	8(%rsp), %rdi
	subq	(%rsp), %rdi
	movq	%rdi, %rbx
	sarq	$3, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	movq	$0, 16(%r13)
	je	.LBB0_7
# BB#17:                                #   in Loop: Header=BB0_2 Depth=1
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	cmpq	%rax, %rbx
	ja	.LBB0_18
# BB#20:                                # %_ZN9__gnu_cxx14__alloc_traitsISaImEE8allocateERS1_m.exit.i.i.i.i.i.i.i.i
                                        #   in Loop: Header=BB0_2 Depth=1
.Ltmp25:
	callq	_Znwm
.Ltmp26:
# BB#21:                                # %.noexc20
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %r15
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_34:                               #   in Loop: Header=BB0_2 Depth=1
	movq	(%r14), %rax
	cmpq	8(%r14), %rax
	jne	.LBB0_52
# BB#35:                                #   in Loop: Header=BB0_2 Depth=1
	movq	$0, 168(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 64(%rsp)
	movq	$0, 80(%rsp)
.Ltmp0:
	xorl	%esi, %esi
	leaq	64(%rsp), %rdi
	leaq	168(%rsp), %rdx
	callq	_ZNSt6vectorImSaImEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPmS1_EERKm
.Ltmp1:
# BB#36:                                # %_ZN6BigIntC2Em.exit26
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	8(%r14), %rbp
	cmpq	16(%r14), %rbp
	je	.LBB0_48
# BB#37:                                #   in Loop: Header=BB0_2 Depth=1
	movq	72(%rsp), %rdi
	subq	64(%rsp), %rdi
	movq	%rdi, %rbx
	sarq	$3, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movq	$0, 16(%rbp)
	je	.LBB0_38
# BB#40:                                #   in Loop: Header=BB0_2 Depth=1
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	cmpq	%rax, %rbx
	ja	.LBB0_41
# BB#43:                                # %_ZN9__gnu_cxx14__alloc_traitsISaImEE8allocateERS1_m.exit.i.i.i.i.i.i.i.i28
                                        #   in Loop: Header=BB0_2 Depth=1
.Ltmp3:
	callq	_Znwm
.Ltmp4:
# BB#44:                                # %.noexc31
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %r15
	jmp	.LBB0_45
	.p2align	4, 0x90
.LBB0_75:                               #   in Loop: Header=BB0_2 Depth=1
	leal	-2(%r12), %edx
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_ZN9Fibonacci10get_numberEj
	leal	-1(%r12), %edx
.Ltmp33:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN9Fibonacci10get_numberEj
.Ltmp34:
# BB#76:                                #   in Loop: Header=BB0_2 Depth=1
.Ltmp36:
	movq	%rsp, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	_ZN6BigIntC2ES_S_
.Ltmp37:
# BB#77:                                #   in Loop: Header=BB0_2 Depth=1
	movq	8(%r14), %rbp
	cmpq	16(%r14), %rbp
	je	.LBB0_88
# BB#78:                                #   in Loop: Header=BB0_2 Depth=1
	movq	8(%rsp), %rdi
	subq	(%rsp), %rdi
	movq	%rdi, %rbx
	sarq	$3, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movq	$0, 16(%rbp)
	je	.LBB0_79
# BB#80:                                #   in Loop: Header=BB0_2 Depth=1
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	cmpq	%rax, %rbx
	ja	.LBB0_81
# BB#83:                                # %_ZN9__gnu_cxx14__alloc_traitsISaImEE8allocateERS1_m.exit.i.i.i.i.i.i.i.i54
                                        #   in Loop: Header=BB0_2 Depth=1
.Ltmp39:
	callq	_Znwm
.Ltmp40:
# BB#84:                                # %.noexc57
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %r15
	jmp	.LBB0_85
.LBB0_25:                               #   in Loop: Header=BB0_2 Depth=1
.Ltmp30:
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rsp, %rdx
	callq	_ZNSt6vectorI6BigIntSaIS0_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS0_S2_EERKS0_
.Ltmp31:
# BB#26:                                # %._ZNSt6vectorI6BigIntSaIS0_EE9push_backERKS0_.exit_crit_edge
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	(%rsp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_28
	jmp	.LBB0_30
.LBB0_88:                               #   in Loop: Header=BB0_2 Depth=1
.Ltmp44:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%rsp, %rdx
	callq	_ZNSt6vectorI6BigIntSaIS0_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS0_S2_EERKS0_
.Ltmp45:
# BB#89:                                # %._ZNSt6vectorI6BigIntSaIS0_EE9push_backERKS0_.exit59_crit_edge
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	(%rsp), %r13
	testq	%r13, %r13
	jne	.LBB0_91
	jmp	.LBB0_92
.LBB0_7:                                #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	xorl	%r15d, %r15d
.LBB0_22:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, (%r13)
	movq	%rax, 8(%r13)
	leaq	(%r15,%rbx,8), %rcx
	movq	%rcx, 16(%r13)
	movq	(%rsp), %rbp
	movq	8(%rsp), %rdx
	subq	%rbp, %rdx
	movq	%rdx, %rbx
	sarq	$3, %rbx
	je	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	memmove
.LBB0_24:                               # %_ZN9__gnu_cxx14__alloc_traitsISaI6BigIntEE9constructIS1_EEvRS2_PS1_RKT_.exit.i
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	(%r15,%rbx,8), %rax
	movq	%rax, 8(%r13)
	addq	$24, 8(%r14)
	leaq	128(%rsp), %r15
	leaq	104(%rsp), %rbx
	testq	%rbp, %rbp
	je	.LBB0_30
.LBB0_28:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rbp, %rdi
	jmp	.LBB0_29
.LBB0_79:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	xorl	%r15d, %r15d
.LBB0_85:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, (%rbp)
	movq	%rax, 8(%rbp)
	leaq	(%r15,%rbx,8), %rcx
	movq	%rcx, 16(%rbp)
	movq	(%rsp), %r13
	movq	8(%rsp), %rdx
	subq	%r13, %rdx
	movq	%rdx, %rbx
	sarq	$3, %rbx
	je	.LBB0_87
# BB#86:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	memmove
.LBB0_87:                               # %_ZN9__gnu_cxx14__alloc_traitsISaI6BigIntEE9constructIS1_EEvRS2_PS1_RKT_.exit.i55
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	(%r15,%rbx,8), %rax
	movq	%rax, 8(%rbp)
	addq	$24, 8(%r14)
	leaq	128(%rsp), %r15
	leaq	104(%rsp), %rbx
	testq	%r13, %r13
	je	.LBB0_92
.LBB0_91:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %rdi
	callq	_ZdlPv
.LBB0_92:                               # %_ZN6BigIntD2Ev.exit61
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_94
# BB#93:                                #   in Loop: Header=BB0_2 Depth=1
	callq	_ZdlPv
.LBB0_94:                               # %_ZN6BigIntD2Ev.exit63
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_29
	jmp	.LBB0_30
.LBB0_48:                               #   in Loop: Header=BB0_2 Depth=1
.Ltmp8:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	leaq	64(%rsp), %rdx
	callq	_ZNSt6vectorI6BigIntSaIS0_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS0_S2_EERKS0_
.Ltmp9:
# BB#49:                                # %._ZNSt6vectorI6BigIntSaIS0_EE9push_backERKS0_.exit33_crit_edge
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	64(%rsp), %r13
	testq	%r13, %r13
	jne	.LBB0_51
	jmp	.LBB0_52
.LBB0_38:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	xorl	%r15d, %r15d
.LBB0_45:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, (%rbp)
	movq	%rax, 8(%rbp)
	leaq	(%r15,%rbx,8), %rcx
	movq	%rcx, 16(%rbp)
	movq	64(%rsp), %r13
	movq	72(%rsp), %rdx
	subq	%r13, %rdx
	movq	%rdx, %rbx
	sarq	$3, %rbx
	je	.LBB0_47
# BB#46:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	memmove
.LBB0_47:                               # %_ZN9__gnu_cxx14__alloc_traitsISaI6BigIntEE9constructIS1_EEvRS2_PS1_RKT_.exit.i29
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	(%r15,%rbx,8), %rax
	movq	%rax, 8(%rbp)
	addq	$24, 8(%r14)
	leaq	128(%rsp), %r15
	leaq	104(%rsp), %rbx
	testq	%r13, %r13
	je	.LBB0_52
.LBB0_51:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %rdi
	callq	_ZdlPv
.LBB0_52:                               #   in Loop: Header=BB0_2 Depth=1
	movq	$1, 176(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movq	$0, 48(%rsp)
.Ltmp11:
	xorl	%esi, %esi
	leaq	32(%rsp), %rdi
	leaq	176(%rsp), %rdx
	callq	_ZNSt6vectorImSaImEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPmS1_EERKm
.Ltmp12:
# BB#53:                                # %_ZN6BigIntC2Em.exit39
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	8(%r14), %rbp
	cmpq	16(%r14), %rbp
	je	.LBB0_68
# BB#54:                                #   in Loop: Header=BB0_2 Depth=1
	movq	40(%rsp), %rdi
	subq	32(%rsp), %rdi
	movq	%rdi, %rbx
	sarq	$3, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movq	$0, 16(%rbp)
	je	.LBB0_55
# BB#60:                                #   in Loop: Header=BB0_2 Depth=1
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	cmpq	%rax, %rbx
	ja	.LBB0_61
# BB#63:                                # %_ZN9__gnu_cxx14__alloc_traitsISaImEE8allocateERS1_m.exit.i.i.i.i.i.i.i.i41
                                        #   in Loop: Header=BB0_2 Depth=1
.Ltmp14:
	callq	_Znwm
.Ltmp15:
# BB#64:                                # %.noexc44
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %r15
	jmp	.LBB0_65
.LBB0_68:                               #   in Loop: Header=BB0_2 Depth=1
.Ltmp19:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	leaq	32(%rsp), %rdx
	callq	_ZNSt6vectorI6BigIntSaIS0_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS0_S2_EERKS0_
.Ltmp20:
# BB#69:                                # %._ZNSt6vectorI6BigIntSaIS0_EE9push_backERKS0_.exit46_crit_edge
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	32(%rsp), %r13
	testq	%r13, %r13
	jne	.LBB0_71
	jmp	.LBB0_30
.LBB0_55:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	xorl	%r15d, %r15d
.LBB0_65:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, (%rbp)
	movq	%rax, 8(%rbp)
	leaq	(%r15,%rbx,8), %rcx
	movq	%rcx, 16(%rbp)
	movq	32(%rsp), %r13
	movq	40(%rsp), %rdx
	subq	%r13, %rdx
	movq	%rdx, %rbx
	sarq	$3, %rbx
	je	.LBB0_67
# BB#66:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	memmove
.LBB0_67:                               # %_ZN9__gnu_cxx14__alloc_traitsISaI6BigIntEE9constructIS1_EEvRS2_PS1_RKT_.exit.i42
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	(%r15,%rbx,8), %rax
	movq	%rax, 8(%rbp)
	addq	$24, 8(%r14)
	leaq	128(%rsp), %r15
	leaq	104(%rsp), %rbx
	testq	%r13, %r13
	je	.LBB0_30
.LBB0_71:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %rdi
.LBB0_29:                               #   in Loop: Header=BB0_2 Depth=1
	callq	_ZdlPv
.LBB0_30:                               #   in Loop: Header=BB0_2 Depth=1
	incl	%r12d
	cmpl	96(%rsp), %r12d         # 4-byte Folded Reload
	jbe	.LBB0_2
# BB#8:                                 # %._crit_edge.loopexit
	movq	(%r14), %r13
.LBB0_9:                                # %._crit_edge
	movl	96(%rsp), %eax          # 4-byte Reload
	leaq	(%rax,%rax,2), %r14
	movq	8(%r13,%r14,8), %rdi
	subq	(%r13,%r14,8), %rdi
	movq	%rdi, %rbp
	sarq	$3, %rbp
	xorps	%xmm0, %xmm0
	movq	152(%rsp), %rbx         # 8-byte Reload
	movups	%xmm0, (%rbx)
	movq	$0, 16(%rbx)
	je	.LBB0_10
# BB#11:
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	cmpq	%rax, %rbp
	ja	.LBB0_107
# BB#12:                                # %_ZN9__gnu_cxx14__alloc_traitsISaImEE8allocateERS1_m.exit.i.i.i.i.i
	callq	_Znwm
	movq	%rax, %r15
	jmp	.LBB0_13
.LBB0_10:
	xorl	%r15d, %r15d
.LBB0_13:
	leaq	(%r13,%r14,8), %rax
	movq	%r15, (%rbx)
	movq	%r15, 8(%rbx)
	leaq	(%r15,%rbp,8), %rcx
	movq	%rcx, 16(%rbx)
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	subq	%rsi, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB0_15
# BB#14:
	movq	%r15, %rdi
	callq	memmove
.LBB0_15:                               # %_ZN6BigIntC2ERKS_.exit
	leaq	(%r15,%rbp,8), %rax
	movq	%rax, 8(%rbx)
	movq	%rbx, %rax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_18:                               # %.noexc.i.i.i.i.i.i
.Ltmp27:
	callq	_ZSt17__throw_bad_allocv
.Ltmp28:
# BB#19:                                # %.noexc
.LBB0_61:                               # %.noexc.i.i.i.i.i.i40
.Ltmp16:
	callq	_ZSt17__throw_bad_allocv
.Ltmp17:
# BB#62:                                # %.noexc43
.LBB0_81:                               # %.noexc.i.i.i.i.i.i53
.Ltmp41:
	callq	_ZSt17__throw_bad_allocv
.Ltmp42:
# BB#82:                                # %.noexc56
.LBB0_41:                               # %.noexc.i.i.i.i.i.i27
.Ltmp5:
	callq	_ZSt17__throw_bad_allocv
.Ltmp6:
# BB#42:                                # %.noexc30
.LBB0_107:                              # %.noexc.i.i.i
	callq	_ZSt17__throw_bad_allocv
.LBB0_57:                               # %.loopexit.split-lp
.Ltmp7:
	jmp	.LBB0_58
.LBB0_98:                               # %.loopexit.split-lp81
.Ltmp43:
	jmp	.LBB0_99
.LBB0_73:                               # %.loopexit.split-lp71
.Ltmp18:
	jmp	.LBB0_74
.LBB0_32:                               # %.loopexit.split-lp76
.Ltmp29:
	jmp	.LBB0_33
.LBB0_56:                               # %.loopexit
.Ltmp10:
	jmp	.LBB0_58
.LBB0_39:
.Ltmp2:
.LBB0_58:
	movq	%rax, %rbx
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_105
	jmp	.LBB0_106
.LBB0_31:                               # %.loopexit75
.Ltmp32:
	jmp	.LBB0_33
.LBB0_97:                               # %.loopexit80
.Ltmp46:
.LBB0_99:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_101
# BB#100:
	callq	_ZdlPv
	jmp	.LBB0_101
.LBB0_72:                               # %.loopexit70
.Ltmp21:
	jmp	.LBB0_74
.LBB0_96:
.Ltmp38:
	movq	%rax, %rbx
.LBB0_101:                              # %_ZN6BigIntD2Ev.exit67
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_103
# BB#102:
	callq	_ZdlPv
	jmp	.LBB0_103
.LBB0_95:
.Ltmp35:
	movq	%rax, %rbx
.LBB0_103:                              # %_ZN6BigIntD2Ev.exit69
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_105
	jmp	.LBB0_106
.LBB0_16:
.Ltmp24:
.LBB0_33:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_105
	jmp	.LBB0_106
.LBB0_59:
.Ltmp13:
.LBB0_74:
	movq	%rax, %rbx
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_106
.LBB0_105:
	callq	_ZdlPv
.LBB0_106:                              # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZN9Fibonacci10get_numberEj, .Lfunc_end0-_ZN9Fibonacci10get_numberEj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\241\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\236\002"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp22-.Lfunc_begin0   #   Call between .Lfunc_begin0 and .Ltmp22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 2 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp32-.Lfunc_begin0   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp33-.Ltmp4          #   Call between .Ltmp4 and .Ltmp33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin0   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin0   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp46-.Lfunc_begin0   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin0   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin0   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp8-.Ltmp45          #   Call between .Ltmp45 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 13 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 14 <<
	.long	.Ltmp11-.Ltmp9          #   Call between .Ltmp9 and .Ltmp11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp20-.Ltmp14         #   Call between .Ltmp14 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin0   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp27-.Ltmp20         #   Call between .Ltmp20 and .Ltmp27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin0   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin0   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin0   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 21 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 22 <<
	.long	.Lfunc_end0-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNSt6vectorI6BigIntSaIS0_EE7reserveEm,"axG",@progbits,_ZNSt6vectorI6BigIntSaIS0_EE7reserveEm,comdat
	.weak	_ZNSt6vectorI6BigIntSaIS0_EE7reserveEm
	.p2align	4, 0x90
	.type	_ZNSt6vectorI6BigIntSaIS0_EE7reserveEm,@function
_ZNSt6vectorI6BigIntSaIS0_EE7reserveEm: # @_ZNSt6vectorI6BigIntSaIS0_EE7reserveEm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movabsq	$768614336404564651, %rax # imm = 0xAAAAAAAAAAAAAAB
	cmpq	%rax, %r14
	jae	.LBB1_11
# BB#1:
	movq	(%r12), %rdx
	movq	16(%r12), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	movabsq	$-6148914691236517205, %rcx # imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rcx, %rax
	cmpq	%r14, %rax
	jae	.LBB1_10
# BB#2:
	movq	8(%r12), %rcx
	movq	%rcx, %r13
	subq	%rdx, %r13
	sarq	$3, %r13
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZNSt6vectorI6BigIntSaIS0_EE20_M_allocate_and_copyIPS0_EES4_mT_S5_
	movq	%rax, %r15
	movq	(%r12), %rbx
	movq	8(%r12), %rbp
	cmpq	%rbp, %rbx
	je	.LBB1_7
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	callq	_ZdlPv
.LBB1_5:                                # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i
                                        #   in Loop: Header=BB1_3 Depth=1
	addq	$24, %rbx
	cmpq	%rbx, %rbp
	jne	.LBB1_3
# BB#6:                                 # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exitthread-pre-split
	movq	(%r12), %rbx
.LBB1_7:                                # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit
	movabsq	$-6148914691236517205, %rax # imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rax, %r13
	testq	%rbx, %rbx
	je	.LBB1_9
# BB#8:
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB1_9:                                # %_ZNSt12_Vector_baseI6BigIntSaIS0_EE13_M_deallocateEPS0_m.exit
	movq	%r15, (%r12)
	leaq	(%r13,%r13,2), %rax
	leaq	(%r15,%rax,8), %rax
	movq	%rax, 8(%r12)
	leaq	(%r14,%r14,2), %rax
	leaq	(%r15,%rax,8), %rax
	movq	%rax, 16(%r12)
.LBB1_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_11:
	movl	$.L.str.18, %edi
	callq	_ZSt20__throw_length_errorPKc
.Lfunc_end1:
	.size	_ZNSt6vectorI6BigIntSaIS0_EE7reserveEm, .Lfunc_end1-_ZNSt6vectorI6BigIntSaIS0_EE7reserveEm
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.section	.text._ZN6BigIntC2ES_S_,"axG",@progbits,_ZN6BigIntC2ES_S_,comdat
	.weak	_ZN6BigIntC2ES_S_
	.p2align	4, 0x90
	.type	_ZN6BigIntC2ES_S_,@function
_ZN6BigIntC2ES_S_:                      # @_ZN6BigIntC2ES_S_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 80
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movq	$0, 16(%r14)
	movq	(%r15), %rcx
	movq	8(%r15), %rsi
	movq	%rsi, %rdi
	subq	%rcx, %rdi
	movq	(%r12), %rax
	movq	8(%r12), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rdi
	movq	%r12, %rdx
	cmovaq	%r15, %rdx
	movq	8(%rdx), %rbx
	subq	(%rdx), %rbx
	sarq	$3, %rbx
	movq	$0, (%rsp)
	sarq	$3, %rdi
	movq	%rbx, %rdx
	subq	%rdi, %rdx
	jbe	.LBB3_3
# BB#1:
.Ltmp47:
	movq	%rsp, %rcx
	movq	%r15, %rdi
	callq	_ZNSt6vectorImSaImEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPmS1_EEmRKm
.Ltmp48:
# BB#2:                                 # %._crit_edge
	movq	(%r12), %rax
	jmp	.LBB3_5
.LBB3_3:
	jae	.LBB3_5
# BB#4:
	leaq	(%rcx,%rbx,8), %rcx
	movq	%rcx, 8(%r15)
.LBB3_5:
	movq	$0, 8(%rsp)
	movq	8(%r12), %rsi
	movq	%rsi, %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	jbe	.LBB3_7
# BB#6:
.Ltmp49:
	leaq	8(%rsp), %rcx
	movq	%r12, %rdi
	callq	_ZNSt6vectorImSaImEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPmS1_EEmRKm
.Ltmp50:
	jmp	.LBB3_9
.LBB3_7:
	jae	.LBB3_9
# BB#8:
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 8(%r12)
.LBB3_9:
	movq	$0, 16(%rsp)
	movq	(%r14), %r13
	movq	8(%r14), %rdx
	movq	%rdx, %rcx
	subq	%r13, %rcx
	sarq	$3, %rcx
	movq	%rbx, %rax
	subq	%rcx, %rax
	jbe	.LBB3_12
# BB#10:
.Ltmp51:
	leaq	16(%rsp), %rcx
	movq	%r14, %rdi
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	_ZNSt6vectorImSaImEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPmS1_EEmRKm
.Ltmp52:
# BB#11:                                # %._crit_edge22
	movq	(%r14), %r13
	movq	8(%r14), %rdx
	jmp	.LBB3_14
.LBB3_12:
	jae	.LBB3_14
# BB#13:
	leaq	(%r13,%rbx,8), %rdx
	movq	%rdx, 8(%r14)
.LBB3_14:
	movq	$0, _ZN6BigInt6head_sE(%rip)
	movq	(%r15), %rbx
	movq	8(%r15), %r15
	movq	(%r12), %rbp
	movq	%rdx, %rdi
	subq	%r13, %rdi
	movq	%rdi, %rax
	sarq	$3, %rax
	je	.LBB3_15
# BB#16:
	shrq	$61, %rax
	jne	.LBB3_17
# BB#19:                                # %_ZN9__gnu_cxx14__alloc_traitsISaImEE8allocateERS1_m.exit.i.i.i.i.i
.Ltmp53:
	callq	_Znwm
	movq	%rax, %r12
.Ltmp54:
# BB#20:                                # %.noexc20
	movq	(%r14), %rsi
	movq	8(%r14), %rdx
	subq	%rsi, %rdx
	jne	.LBB3_22
	jmp	.LBB3_23
.LBB3_15:
	xorl	%r12d, %r12d
	movq	%r13, %rsi
	subq	%rsi, %rdx
	je	.LBB3_23
.LBB3_22:
	movq	%r12, %rdi
	callq	memmove
.LBB3_23:                               # %_ZN6BigIntC2ERKS_.exit
	cmpq	%r15, %rbx
	je	.LBB3_26
# BB#24:                                # %.lr.ph.i.preheader
	movabsq	$19342813113834067, %rcx # imm = 0x44B82FA09B5A53
	.p2align	4, 0x90
.LBB3_25:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rsi
	addq	(%rbx), %rsi
	addq	_ZN6BigInt6head_sE(%rip), %rsi
	movq	%rsi, %rax
	shrq	$9, %rax
	mulq	%rcx
	shrq	$11, %rdx
	movq	%rdx, _ZN6BigInt6head_sE(%rip)
	imulq	$1000000000, %rdx, %rax # imm = 0x3B9ACA00
	subq	%rax, %rsi
	movq	%rsi, (%r13)
	addq	$8, %rbx
	addq	$8, %rbp
	addq	$8, %r13
	cmpq	%rbx, %r15
	jne	.LBB3_25
.LBB3_26:                               # %_ZSt9transformIN9__gnu_cxx17__normal_iteratorIPmSt6vectorImSaImEEEES6_S6_6BigIntET1_T_S9_T0_S8_T2_.exit
	testq	%r12, %r12
	je	.LBB3_28
# BB#27:
	movq	%r12, %rdi
	callq	_ZdlPv
.LBB3_28:                               # %_ZN6BigIntD2Ev.exit18
	movq	_ZN6BigInt6head_sE(%rip), %rax
	testq	%rax, %rax
	je	.LBB3_32
# BB#29:
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	je	.LBB3_31
# BB#30:
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r14)
	jmp	.LBB3_32
.LBB3_31:
.Ltmp57:
	movl	$_ZN6BigInt6head_sE, %edx
	movq	%r14, %rdi
	callq	_ZNSt6vectorImSaImEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPmS1_EERKm
.Ltmp58:
.LBB3_32:                               # %_ZNSt6vectorImSaImEE9push_backERKm.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_17:                               # %.noexc.i.i.i
.Ltmp55:
	callq	_ZSt17__throw_bad_allocv
.Ltmp56:
# BB#18:                                # %.noexc19
.LBB3_33:
.Ltmp59:
	movq	%rax, %rbx
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB3_35
# BB#34:
	callq	_ZdlPv
.LBB3_35:                               # %_ZNSt6vectorImSaImEED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN6BigIntC2ES_S_, .Lfunc_end3-_ZN6BigIntC2ES_S_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp47-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp54-.Ltmp47         #   Call between .Ltmp47 and .Ltmp54
	.long	.Ltmp59-.Lfunc_begin1   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp57-.Ltmp54         #   Call between .Ltmp54 and .Ltmp57
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp56-.Ltmp57         #   Call between .Ltmp57 and .Ltmp56
	.long	.Ltmp59-.Lfunc_begin1   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp56     #   Call between .Ltmp56 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZNK9Fibonacci16show_all_numbersEv
	.p2align	4, 0x90
	.type	_ZNK9Fibonacci16show_all_numbersEv,@function
_ZNK9Fibonacci16show_all_numbersEv:     # @_ZNK9Fibonacci16show_all_numbersEv
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$424, %rsp              # imm = 0x1A8
.Lcfi45:
	.cfi_def_cfa_offset 480
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	48(%rsp), %rdi
	movl	$16, %esi
	callq	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1ESt13_Ios_Openmode
	movq	8(%rbx), %rax
	cmpq	(%rbx), %rax
	je	.LBB4_20
# BB#1:                                 # %.lr.ph
	xorl	%r12d, %r12d
	leaq	48(%rsp), %rbp
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB4_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_7 Depth 2
.Ltmp60:
	movl	$.L.str, %esi
	movl	$5, %edx
	movq	%rbp, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp61:
# BB#3:                                 # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
                                        #   in Loop: Header=BB4_2 Depth=1
.Ltmp62:
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	_ZNSo9_M_insertImEERSoT_
	movq	%rax, %r13
.Ltmp63:
# BB#4:                                 # %_ZNSolsEj.exit
                                        #   in Loop: Header=BB4_2 Depth=1
.Ltmp64:
	movl	$.L.str.1, %esi
	movl	$4, %edx
	movq	%r13, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp65:
# BB#5:                                 # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit11
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%rbx), %rcx
	leaq	(%r12,%r12,2), %rdx
	movq	8(%rcx,%rdx,8), %r15
	movq	(%rcx,%rdx,8), %rax
	subq	%rax, %r15
	sarq	$3, %r15
	decq	%r15
	je	.LBB4_17
# BB#6:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	leaq	(%rcx,%rdx,8), %r14
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph.i
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%r15,8), %rsi
.Ltmp66:
	movq	%r13, %rdi
	callq	_ZNSo9_M_insertImEERSoT_
	movq	%rax, %rbx
.Ltmp67:
# BB#8:                                 # %.noexc12
                                        #   in Loop: Header=BB4_7 Depth=2
	movq	%rbx, %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	movq	$9, 16(%rax,%rcx)
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rbx
	cmpb	$0, 225(%rax,%rcx)
	je	.LBB4_10
# BB#9:                                 # %._crit_edge.i.i.i.i
                                        #   in Loop: Header=BB4_7 Depth=2
	addq	$224, %rbx
	jmp	.LBB4_16
	.p2align	4, 0x90
.LBB4_10:                               #   in Loop: Header=BB4_7 Depth=2
	movq	240(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB4_27
# BB#11:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i.i.i.i.i
                                        #   in Loop: Header=BB4_7 Depth=2
	cmpb	$0, 56(%rbp)
	je	.LBB4_13
# BB#12:                                #   in Loop: Header=BB4_7 Depth=2
	movzbl	89(%rbp), %eax
	jmp	.LBB4_15
.LBB4_13:                               #   in Loop: Header=BB4_7 Depth=2
.Ltmp68:
	movq	%rbp, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp69:
# BB#14:                                # %.noexc14
                                        #   in Loop: Header=BB4_7 Depth=2
	movq	(%rbp), %rax
.Ltmp70:
	movl	$32, %esi
	movq	%rbp, %rdi
	callq	*48(%rax)
.Ltmp71:
.LBB4_15:                               # %_ZNKSt9basic_iosIcSt11char_traitsIcEE5widenEc.exit.i.i.i.i
                                        #   in Loop: Header=BB4_7 Depth=2
	movb	%al, 224(%rbx)
	movb	$1, 225(%rbx)
	leaq	224(%rbx), %rbx
.LBB4_16:                               # %_ZStlsIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_St8_SetfillIS3_E.exit.i
                                        #   in Loop: Header=BB4_7 Depth=2
	movb	$48, (%rbx)
	decq	%r15
	movq	(%r14), %rax
	jne	.LBB4_7
.LBB4_17:                               # %._crit_edge.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%rax), %rsi
.Ltmp76:
	movq	%r13, %rdi
	callq	_ZNSo9_M_insertImEERSoT_
.Ltmp77:
	movq	8(%rsp), %rbx           # 8-byte Reload
	leaq	48(%rsp), %rbp
# BB#18:                                # %_ZlsRSoRK6BigInt.exit
                                        #   in Loop: Header=BB4_2 Depth=1
.Ltmp78:
	movl	$.L.str.2, %esi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp79:
# BB#19:                                # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit16
                                        #   in Loop: Header=BB4_2 Depth=1
	incl	%r12d
	movq	8(%rbx), %rax
	subq	(%rbx), %rax
	sarq	$3, %rax
	movabsq	$-6148914691236517205, %rcx # imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rcx, %rax
	cmpq	%rax, %r12
	jb	.LBB4_2
.LBB4_20:                               # %._crit_edge
	leaq	56(%rsp), %rsi
.Ltmp81:
	leaq	16(%rsp), %rdi
	callq	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv
.Ltmp82:
# BB#21:                                # %_ZNKSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEE3strEv.exit
	movq	16(%rsp), %rsi
	movq	24(%rsp), %rdx
.Ltmp84:
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp85:
# BB#22:                                # %_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE.exit
	movq	16(%rsp), %rdi
	leaq	32(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_24
# BB#23:
	callq	_ZdlPv
.LBB4_24:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	movq	_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, 48(%rsp)
	movq	_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rcx, 48(%rsp,%rax)
	movq	$_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16, 56(%rsp)
	movq	128(%rsp), %rdi
	leaq	144(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_26
# BB#25:
	callq	_ZdlPv
.LBB4_26:                               # %_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEED1Ev.exit
	movq	$_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16, 56(%rsp)
	leaq	112(%rsp), %rdi
	callq	_ZNSt6localeD1Ev
	leaq	160(%rsp), %rdi
	callq	_ZNSt8ios_baseD2Ev
	addq	$424, %rsp              # imm = 0x1A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_27:
.Ltmp73:
	callq	_ZSt16__throw_bad_castv
.Ltmp74:
# BB#28:                                # %.noexc13
.LBB4_29:                               # %.loopexit.split-lp.loopexit.split-lp
.Ltmp75:
	jmp	.LBB4_35
.LBB4_30:
.Ltmp86:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	leaq	32(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_36
# BB#31:
	callq	_ZdlPv
	jmp	.LBB4_36
.LBB4_32:
.Ltmp83:
	jmp	.LBB4_35
.LBB4_33:                               # %.loopexit.split-lp.loopexit
.Ltmp80:
	jmp	.LBB4_35
.LBB4_34:                               # %.loopexit
.Ltmp72:
.LBB4_35:
	movq	%rax, %rbx
.LBB4_36:
	movq	_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, 48(%rsp)
	movq	_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rcx, 48(%rsp,%rax)
	movq	$_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16, 56(%rsp)
	movq	128(%rsp), %rdi
	leaq	144(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB4_38
# BB#37:
	callq	_ZdlPv
.LBB4_38:
	movq	$_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16, 56(%rsp)
	leaq	112(%rsp), %rdi
	callq	_ZNSt6localeD1Ev
	leaq	160(%rsp), %rdi
.Ltmp87:
	callq	_ZNSt8ios_baseD2Ev
.Ltmp88:
# BB#39:                                # %_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEED1Ev.exit19
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_40:
.Ltmp89:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZNK9Fibonacci16show_all_numbersEv, .Lfunc_end4-_ZNK9Fibonacci16show_all_numbersEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp60-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp60
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp65-.Ltmp60         #   Call between .Ltmp60 and .Ltmp65
	.long	.Ltmp80-.Lfunc_begin2   #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp71-.Ltmp66         #   Call between .Ltmp66 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin2   #     jumps to .Ltmp72
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp79-.Ltmp76         #   Call between .Ltmp76 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin2   #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin2   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin2   #     jumps to .Ltmp86
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp73-.Ltmp85         #   Call between .Ltmp85 and .Ltmp73
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin2   #     jumps to .Ltmp75
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin2   #     jumps to .Ltmp89
	.byte	1                       #   On action: 1
	.long	.Ltmp88-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Lfunc_end4-.Ltmp88     #   Call between .Ltmp88 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZNK9Fibonacci16show_last_numberEv
	.p2align	4, 0x90
	.type	_ZNK9Fibonacci16show_last_numberEv,@function
_ZNK9Fibonacci16show_last_numberEv:     # @_ZNK9Fibonacci16show_last_numberEv
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 48
	subq	$416, %rsp              # imm = 0x1A0
.Lcfi57:
	.cfi_def_cfa_offset 464
.Lcfi58:
	.cfi_offset %rbx, -48
.Lcfi59:
	.cfi_offset %r12, -40
.Lcfi60:
	.cfi_offset %r13, -32
.Lcfi61:
	.cfi_offset %r14, -24
.Lcfi62:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	leaq	40(%rsp), %r14
	movl	$16, %esi
	movq	%r14, %rdi
	callq	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1ESt13_Ios_Openmode
.Ltmp90:
	movl	$.L.str, %esi
	movl	$5, %edx
	movq	%r14, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp91:
# BB#1:                                 # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
	movq	8(%rbx), %rax
	subq	(%rbx), %rax
	sarq	$3, %rax
	movabsq	$-6148914691236517205, %rsi # imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rax, %rsi
	decq	%rsi
.Ltmp92:
	leaq	40(%rsp), %rdi
	callq	_ZNSo9_M_insertImEERSoT_
	movq	%rax, %r14
.Ltmp93:
# BB#2:                                 # %_ZNSolsEm.exit
.Ltmp94:
	movl	$.L.str.1, %esi
	movl	$4, %edx
	movq	%r14, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp95:
# BB#3:
	movq	8(%rbx), %r12
	movq	-24(%r12), %rax
	movq	-16(%r12), %r13
	subq	%rax, %r13
	sarq	$3, %r13
	decq	%r13
	je	.LBB5_14
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax,%r13,8), %rsi
.Ltmp96:
	movq	%r14, %rdi
	callq	_ZNSo9_M_insertImEERSoT_
	movq	%rax, %rbx
.Ltmp97:
# BB#5:                                 # %.noexc5
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	%rbx, %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	movq	$9, 16(%rax,%rcx)
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rbx
	cmpb	$0, 225(%rax,%rcx)
	je	.LBB5_7
# BB#6:                                 # %._crit_edge.i.i.i.i
                                        #   in Loop: Header=BB5_4 Depth=1
	addq	$224, %rbx
	jmp	.LBB5_13
	.p2align	4, 0x90
.LBB5_7:                                #   in Loop: Header=BB5_4 Depth=1
	movq	240(%rbx), %r15
	testq	%r15, %r15
	je	.LBB5_23
# BB#8:                                 # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i.i.i.i.i
                                        #   in Loop: Header=BB5_4 Depth=1
	cmpb	$0, 56(%r15)
	je	.LBB5_10
# BB#9:                                 #   in Loop: Header=BB5_4 Depth=1
	movzbl	89(%r15), %eax
	jmp	.LBB5_12
.LBB5_10:                               #   in Loop: Header=BB5_4 Depth=1
.Ltmp98:
	movq	%r15, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp99:
# BB#11:                                # %.noexc7
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	(%r15), %rax
.Ltmp100:
	movl	$32, %esi
	movq	%r15, %rdi
	callq	*48(%rax)
.Ltmp101:
.LBB5_12:                               # %_ZNKSt9basic_iosIcSt11char_traitsIcEE5widenEc.exit.i.i.i.i
                                        #   in Loop: Header=BB5_4 Depth=1
	movb	%al, 224(%rbx)
	movb	$1, 225(%rbx)
	leaq	224(%rbx), %rbx
.LBB5_13:                               # %_ZStlsIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_St8_SetfillIS3_E.exit.i
                                        #   in Loop: Header=BB5_4 Depth=1
	movb	$48, (%rbx)
	decq	%r13
	movq	-24(%r12), %rax
	jne	.LBB5_4
.LBB5_14:                               # %._crit_edge.i
	movq	(%rax), %rsi
.Ltmp105:
	movq	%r14, %rdi
	callq	_ZNSo9_M_insertImEERSoT_
.Ltmp106:
# BB#15:                                # %_ZlsRSoRK6BigInt.exit
.Ltmp107:
	movl	$.L.str.2, %esi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp108:
# BB#16:                                # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit9
	leaq	48(%rsp), %rsi
.Ltmp110:
	leaq	8(%rsp), %rdi
	callq	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv
.Ltmp111:
# BB#17:                                # %_ZNKSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEE3strEv.exit
	movq	8(%rsp), %rsi
	movq	16(%rsp), %rdx
.Ltmp113:
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp114:
# BB#18:                                # %_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE.exit
	movq	8(%rsp), %rdi
	leaq	24(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB5_20
# BB#19:
	callq	_ZdlPv
.LBB5_20:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	movq	_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, 40(%rsp)
	movq	_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rcx, 40(%rsp,%rax)
	movq	$_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16, 48(%rsp)
	movq	120(%rsp), %rdi
	leaq	136(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB5_22
# BB#21:
	callq	_ZdlPv
.LBB5_22:                               # %_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEED1Ev.exit
	movq	$_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16, 48(%rsp)
	leaq	104(%rsp), %rdi
	callq	_ZNSt6localeD1Ev
	leaq	152(%rsp), %rdi
	callq	_ZNSt8ios_baseD2Ev
	addq	$416, %rsp              # imm = 0x1A0
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB5_23:
.Ltmp103:
	callq	_ZSt16__throw_bad_castv
.Ltmp104:
# BB#24:                                # %.noexc6
.LBB5_25:
.Ltmp115:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	leaq	24(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB5_31
# BB#26:
	callq	_ZdlPv
	jmp	.LBB5_31
.LBB5_27:
.Ltmp112:
	jmp	.LBB5_30
.LBB5_28:                               # %.loopexit.split-lp
.Ltmp109:
	jmp	.LBB5_30
.LBB5_29:                               # %.loopexit
.Ltmp102:
.LBB5_30:
	movq	%rax, %rbx
.LBB5_31:
	movq	_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, 40(%rsp)
	movq	_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rcx, 40(%rsp,%rax)
	movq	$_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16, 48(%rsp)
	movq	120(%rsp), %rdi
	leaq	136(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB5_33
# BB#32:
	callq	_ZdlPv
.LBB5_33:
	movq	$_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16, 48(%rsp)
	leaq	104(%rsp), %rdi
	callq	_ZNSt6localeD1Ev
	leaq	152(%rsp), %rdi
.Ltmp116:
	callq	_ZNSt8ios_baseD2Ev
.Ltmp117:
# BB#34:                                # %_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEED1Ev.exit12
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB5_35:
.Ltmp118:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZNK9Fibonacci16show_last_numberEv, .Lfunc_end5-_ZNK9Fibonacci16show_last_numberEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp90-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp90
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp90-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp95-.Ltmp90         #   Call between .Ltmp90 and .Ltmp95
	.long	.Ltmp109-.Lfunc_begin3  #     jumps to .Ltmp109
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp101-.Ltmp96        #   Call between .Ltmp96 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin3  #     jumps to .Ltmp102
	.byte	0                       #   On action: cleanup
	.long	.Ltmp105-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp108-.Ltmp105       #   Call between .Ltmp105 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin3  #     jumps to .Ltmp109
	.byte	0                       #   On action: cleanup
	.long	.Ltmp110-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp111-.Ltmp110       #   Call between .Ltmp110 and .Ltmp111
	.long	.Ltmp112-.Lfunc_begin3  #     jumps to .Ltmp112
	.byte	0                       #   On action: cleanup
	.long	.Ltmp113-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Ltmp114-.Ltmp113       #   Call between .Ltmp113 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin3  #     jumps to .Ltmp115
	.byte	0                       #   On action: cleanup
	.long	.Ltmp114-.Lfunc_begin3  # >> Call Site 7 <<
	.long	.Ltmp103-.Ltmp114       #   Call between .Ltmp114 and .Ltmp103
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp103-.Lfunc_begin3  # >> Call Site 8 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp109-.Lfunc_begin3  #     jumps to .Ltmp109
	.byte	0                       #   On action: cleanup
	.long	.Ltmp116-.Lfunc_begin3  # >> Call Site 9 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin3  #     jumps to .Ltmp118
	.byte	1                       #   On action: 1
	.long	.Ltmp117-.Lfunc_begin3  # >> Call Site 10 <<
	.long	.Lfunc_end5-.Ltmp117    #   Call between .Ltmp117 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9Fibonacci11show_numberEm
	.p2align	4, 0x90
	.type	_ZN9Fibonacci11show_numberEm,@function
_ZN9Fibonacci11show_numberEm:           # @_ZN9Fibonacci11show_numberEm
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 48
	subq	$432, %rsp              # imm = 0x1B0
.Lcfi68:
	.cfi_def_cfa_offset 480
.Lcfi69:
	.cfi_offset %rbx, -48
.Lcfi70:
	.cfi_offset %r12, -40
.Lcfi71:
	.cfi_offset %r13, -32
.Lcfi72:
	.cfi_offset %r14, -24
.Lcfi73:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	leaq	56(%rsp), %rdi
	movl	$16, %esi
	callq	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1ESt13_Ios_Openmode
	movq	8(%r15), %rax
	subq	(%r15), %rax
	sarq	$3, %rax
	movabsq	$-6148914691236517205, %rcx # imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rax, %rcx
	cmpq	%r14, %rcx
	ja	.LBB6_4
# BB#1:
.Ltmp119:
	leaq	32(%rsp), %rdi
	movq	%r15, %rsi
	movl	%r14d, %edx
	callq	_ZN9Fibonacci10get_numberEj
.Ltmp120:
# BB#2:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_4
# BB#3:
	callq	_ZdlPv
.LBB6_4:                                # %_ZN6BigIntD2Ev.exit
.Ltmp121:
	leaq	56(%rsp), %rdi
	movl	$.L.str, %esi
	movl	$5, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp122:
# BB#5:                                 # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
.Ltmp123:
	leaq	56(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZNSo9_M_insertImEERSoT_
	movq	%rax, %r12
.Ltmp124:
# BB#6:                                 # %_ZNSolsEm.exit
.Ltmp125:
	movl	$.L.str.1, %esi
	movl	$4, %edx
	movq	%r12, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp126:
# BB#7:                                 # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit8
	movq	(%r15), %rcx
	leaq	(%r14,%r14,2), %rdx
	movq	8(%rcx,%rdx,8), %r15
	movq	(%rcx,%rdx,8), %rax
	subq	%rax, %r15
	sarq	$3, %r15
	decq	%r15
	je	.LBB6_19
# BB#8:                                 # %.lr.ph.i.preheader
	leaq	(%rcx,%rdx,8), %r13
	.p2align	4, 0x90
.LBB6_9:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax,%r15,8), %rsi
.Ltmp127:
	movq	%r12, %rdi
	callq	_ZNSo9_M_insertImEERSoT_
	movq	%rax, %rbx
.Ltmp128:
# BB#10:                                # %.noexc9
                                        #   in Loop: Header=BB6_9 Depth=1
	movq	%rbx, %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	movq	$9, 16(%rax,%rcx)
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rbx
	cmpb	$0, 225(%rax,%rcx)
	je	.LBB6_12
# BB#11:                                # %._crit_edge.i.i.i.i
                                        #   in Loop: Header=BB6_9 Depth=1
	addq	$224, %rbx
	jmp	.LBB6_18
	.p2align	4, 0x90
.LBB6_12:                               #   in Loop: Header=BB6_9 Depth=1
	movq	240(%rbx), %r14
	testq	%r14, %r14
	je	.LBB6_28
# BB#13:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i.i.i.i.i
                                        #   in Loop: Header=BB6_9 Depth=1
	cmpb	$0, 56(%r14)
	je	.LBB6_15
# BB#14:                                #   in Loop: Header=BB6_9 Depth=1
	movzbl	89(%r14), %eax
	jmp	.LBB6_17
.LBB6_15:                               #   in Loop: Header=BB6_9 Depth=1
.Ltmp129:
	movq	%r14, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp130:
# BB#16:                                # %.noexc11
                                        #   in Loop: Header=BB6_9 Depth=1
	movq	(%r14), %rax
.Ltmp131:
	movl	$32, %esi
	movq	%r14, %rdi
	callq	*48(%rax)
.Ltmp132:
.LBB6_17:                               # %_ZNKSt9basic_iosIcSt11char_traitsIcEE5widenEc.exit.i.i.i.i
                                        #   in Loop: Header=BB6_9 Depth=1
	movb	%al, 224(%rbx)
	movb	$1, 225(%rbx)
	leaq	224(%rbx), %rbx
.LBB6_18:                               # %_ZStlsIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_St8_SetfillIS3_E.exit.i
                                        #   in Loop: Header=BB6_9 Depth=1
	movb	$48, (%rbx)
	decq	%r15
	movq	(%r13), %rax
	jne	.LBB6_9
.LBB6_19:                               # %._crit_edge.i
	movq	(%rax), %rsi
.Ltmp136:
	movq	%r12, %rdi
	callq	_ZNSo9_M_insertImEERSoT_
.Ltmp137:
# BB#20:                                # %_ZlsRSoRK6BigInt.exit
.Ltmp138:
	movl	$.L.str.2, %esi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp139:
# BB#21:                                # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit13
	leaq	64(%rsp), %rsi
.Ltmp141:
	movq	%rsp, %rdi
	callq	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv
.Ltmp142:
# BB#22:                                # %_ZNKSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEE3strEv.exit
	movq	(%rsp), %rsi
	movq	8(%rsp), %rdx
.Ltmp144:
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp145:
# BB#23:                                # %_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE.exit
	movq	(%rsp), %rdi
	leaq	16(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB6_25
# BB#24:
	callq	_ZdlPv
.LBB6_25:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	movq	_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, 56(%rsp)
	movq	_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rcx, 56(%rsp,%rax)
	movq	$_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16, 64(%rsp)
	movq	136(%rsp), %rdi
	leaq	152(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB6_27
# BB#26:
	callq	_ZdlPv
.LBB6_27:                               # %_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEED1Ev.exit
	movq	$_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16, 64(%rsp)
	leaq	120(%rsp), %rdi
	callq	_ZNSt6localeD1Ev
	leaq	168(%rsp), %rdi
	callq	_ZNSt8ios_baseD2Ev
	addq	$432, %rsp              # imm = 0x1B0
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB6_28:
.Ltmp134:
	callq	_ZSt16__throw_bad_castv
.Ltmp135:
# BB#29:                                # %.noexc10
.LBB6_30:
.Ltmp146:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	leaq	16(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB6_36
# BB#31:
	callq	_ZdlPv
	jmp	.LBB6_36
.LBB6_32:
.Ltmp143:
	jmp	.LBB6_35
.LBB6_33:                               # %.loopexit.split-lp
.Ltmp140:
	jmp	.LBB6_35
.LBB6_34:                               # %.loopexit
.Ltmp133:
.LBB6_35:
	movq	%rax, %rbx
.LBB6_36:
	movq	_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, 56(%rsp)
	movq	_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rcx, 56(%rsp,%rax)
	movq	$_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16, 64(%rsp)
	movq	136(%rsp), %rdi
	leaq	152(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB6_38
# BB#37:
	callq	_ZdlPv
.LBB6_38:
	movq	$_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16, 64(%rsp)
	leaq	120(%rsp), %rdi
	callq	_ZNSt6localeD1Ev
	leaq	168(%rsp), %rdi
.Ltmp147:
	callq	_ZNSt8ios_baseD2Ev
.Ltmp148:
# BB#39:                                # %_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEED1Ev.exit16
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_40:
.Ltmp149:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN9Fibonacci11show_numberEm, .Lfunc_end6-_ZN9Fibonacci11show_numberEm
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp119-.Lfunc_begin4  #   Call between .Lfunc_begin4 and .Ltmp119
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp119-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp126-.Ltmp119       #   Call between .Ltmp119 and .Ltmp126
	.long	.Ltmp140-.Lfunc_begin4  #     jumps to .Ltmp140
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp132-.Ltmp127       #   Call between .Ltmp127 and .Ltmp132
	.long	.Ltmp133-.Lfunc_begin4  #     jumps to .Ltmp133
	.byte	0                       #   On action: cleanup
	.long	.Ltmp136-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp139-.Ltmp136       #   Call between .Ltmp136 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin4  #     jumps to .Ltmp140
	.byte	0                       #   On action: cleanup
	.long	.Ltmp141-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp142-.Ltmp141       #   Call between .Ltmp141 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin4  #     jumps to .Ltmp143
	.byte	0                       #   On action: cleanup
	.long	.Ltmp144-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp145-.Ltmp144       #   Call between .Ltmp144 and .Ltmp145
	.long	.Ltmp146-.Lfunc_begin4  #     jumps to .Ltmp146
	.byte	0                       #   On action: cleanup
	.long	.Ltmp145-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp134-.Ltmp145       #   Call between .Ltmp145 and .Ltmp134
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp134-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp135-.Ltmp134       #   Call between .Ltmp134 and .Ltmp135
	.long	.Ltmp140-.Lfunc_begin4  #     jumps to .Ltmp140
	.byte	0                       #   On action: cleanup
	.long	.Ltmp147-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Ltmp148-.Ltmp147       #   Call between .Ltmp147 and .Ltmp148
	.long	.Ltmp149-.Lfunc_begin4  #     jumps to .Ltmp149
	.byte	1                       #   On action: 1
	.long	.Ltmp148-.Lfunc_begin4  # >> Call Site 10 <<
	.long	.Lfunc_end6-.Ltmp148    #   Call between .Ltmp148 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_Z5usagePPc
	.p2align	4, 0x90
	.type	_Z5usagePPc,@function
_Z5usagePPc:                            # @_Z5usagePPc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 32
.Lcfi77:
	.cfi_offset %rbx, -32
.Lcfi78:
	.cfi_offset %r14, -24
.Lcfi79:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	$.L.str.3, (%r14)
	movl	$_ZSt4cerr, %edi
	movl	$.L.str.4, %esi
	movl	$8, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cerr(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cerr+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB7_33
# BB#1:                                 # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i
	cmpb	$0, 56(%rbx)
	je	.LBB7_3
# BB#2:
	movb	67(%rbx), %al
	jmp	.LBB7_4
.LBB7_3:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB7_4:                                # %_ZNKSt9basic_iosIcSt11char_traitsIcEE5widenEc.exit
	movsbl	%al, %esi
	movl	$_ZSt4cerr, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	%rax, %r15
	movl	$.L.str.5, %esi
	movl	$2, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB7_5
# BB#6:
	movq	%rbx, %rdi
	callq	strlen
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB7_7
.LBB7_5:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	%r15, %rdi
	addq	%rax, %rdi
	movl	32(%r15,%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB7_7:                                # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
	movl	$.L.str.6, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	$4, 16(%r15,%rax)
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movl	$-177, %ecx
	andl	24(%r15,%rax), %ecx
	orl	$32, %ecx
	movl	%ecx, 24(%r15,%rax)
	movl	$.L.str.7, %esi
	movl	$3, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$.L.str.8, %esi
	movl	$40, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB7_33
# BB#8:                                 # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i11
	cmpb	$0, 56(%rbx)
	je	.LBB7_10
# BB#9:
	movb	67(%rbx), %al
	jmp	.LBB7_11
.LBB7_10:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB7_11:                               # %_ZNKSt9basic_iosIcSt11char_traitsIcEE5widenEc.exit13
	movsbl	%al, %esi
	movq	%r15, %rdi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	%rax, %r15
	movl	$.L.str.5, %esi
	movl	$2, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB7_12
# BB#13:
	movq	%rbx, %rdi
	callq	strlen
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB7_14
.LBB7_12:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	%r15, %rdi
	addq	%rax, %rdi
	movl	32(%r15,%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB7_14:                               # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit8
	movl	$.L.str.6, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movl	$-177, %ecx
	andl	24(%r15,%rax), %ecx
	orl	$32, %ecx
	movl	%ecx, 24(%r15,%rax)
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	$4, 16(%r15,%rax)
	movl	$.L.str.9, %esi
	movl	$2, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$.L.str.10, %esi
	movl	$36, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB7_33
# BB#15:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i14
	cmpb	$0, 56(%rbx)
	je	.LBB7_17
# BB#16:
	movb	67(%rbx), %al
	jmp	.LBB7_18
.LBB7_17:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB7_18:                               # %_ZNKSt9basic_iosIcSt11char_traitsIcEE5widenEc.exit16
	movsbl	%al, %esi
	movq	%r15, %rdi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	%rax, %r15
	movl	$.L.str.5, %esi
	movl	$2, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB7_19
# BB#20:
	movq	%rbx, %rdi
	callq	strlen
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB7_21
.LBB7_19:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	%r15, %rdi
	addq	%rax, %rdi
	movl	32(%r15,%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB7_21:                               # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit9
	movl	$.L.str.6, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movl	$-177, %ecx
	andl	24(%r15,%rax), %ecx
	orl	$32, %ecx
	movl	%ecx, 24(%r15,%rax)
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	$4, 16(%r15,%rax)
	movl	$.L.str.11, %esi
	movl	$4, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$.L.str.12, %esi
	movl	$58, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB7_33
# BB#22:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i17
	cmpb	$0, 56(%rbx)
	je	.LBB7_24
# BB#23:
	movb	67(%rbx), %al
	jmp	.LBB7_25
.LBB7_24:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB7_25:                               # %_ZNKSt9basic_iosIcSt11char_traitsIcEE5widenEc.exit19
	movsbl	%al, %esi
	movq	%r15, %rdi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	%rax, %rbx
	movl	$.L.str.5, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.LBB7_26
# BB#27:
	movq	%r14, %rdi
	callq	strlen
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB7_28
.LBB7_26:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	movq	%rbx, %rdi
	addq	%rax, %rdi
	movl	32(%rbx,%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB7_28:                               # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit10
	movl	$.L.str.6, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	movl	$-177, %ecx
	andl	24(%rbx,%rax), %ecx
	orl	$32, %ecx
	movl	%ecx, 24(%rbx,%rax)
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	movq	$4, 16(%rbx,%rax)
	movl	$.L.str.13, %esi
	movl	$4, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$.L.str.14, %esi
	movl	$67, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$25000, %esi            # imm = 0x61A8
	movq	%rbx, %rdi
	callq	_ZNSolsEi
	movq	%rax, %r14
	movl	$.L.str.15, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB7_33
# BB#29:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i20
	cmpb	$0, 56(%rbx)
	je	.LBB7_31
# BB#30:
	movb	67(%rbx), %al
	jmp	.LBB7_32
.LBB7_31:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB7_32:                               # %_ZNKSt9basic_iosIcSt11char_traitsIcEE5widenEc.exit22
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZNSo5flushEv           # TAILCALL
.LBB7_33:
	callq	_ZSt16__throw_bad_castv
.Lfunc_end7:
	.size	_Z5usagePPc, .Lfunc_end7-_Z5usagePPc
	.cfi_endproc

	.globl	_Z5checkB5cxx11iPPc
	.p2align	4, 0x90
	.type	_Z5checkB5cxx11iPPc,@function
_Z5checkB5cxx11iPPc:                    # @_Z5checkB5cxx11iPPc
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi82:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi84:
	.cfi_def_cfa_offset 96
.Lcfi85:
	.cfi_offset %rbx, -40
.Lcfi86:
	.cfi_offset %r12, -32
.Lcfi87:
	.cfi_offset %r14, -24
.Lcfi88:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpl	$2, %esi
	jg	.LBB8_2
# BB#1:
	leaq	16(%r15), %rax
	movq	%rax, (%r15)
	movq	$0, 8(%r15)
	movb	$0, 16(%r15)
	jmp	.LBB8_38
.LBB8_2:
	movq	8(%rdx), %r14
	leaq	40(%rsp), %r12
	movq	%r12, 24(%rsp)
	testq	%r14, %r14
	je	.LBB8_5
# BB#3:                                 # %.thread.i
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%rbx, 8(%rsp)
	cmpq	$15, %rbx
	jbe	.LBB8_4
# BB#7:                                 # %.noexc7.i
.Ltmp150:
	leaq	24(%rsp), %rdi
	leaq	8(%rsp), %rsi
	xorl	%edx, %edx
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm
.Ltmp151:
# BB#8:                                 # %.noexc4
	movq	%rax, 24(%rsp)
	movq	8(%rsp), %rcx
	movq	%rcx, 40(%rsp)
	testq	%rbx, %rbx
	jne	.LBB8_10
	jmp	.LBB8_13
.LBB8_4:                                # %._crit_edge.i.i.i.i
	movq	%r12, %rax
	testq	%rbx, %rbx
	je	.LBB8_13
.LBB8_10:
	cmpq	$1, %rbx
	jne	.LBB8_12
# BB#11:
	movb	(%r14), %cl
	movb	%cl, (%rax)
	jmp	.LBB8_13
.LBB8_12:
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memcpy
.LBB8_13:
	movq	8(%rsp), %rax
	movq	%rax, 32(%rsp)
	movq	24(%rsp), %rcx
	movb	$0, (%rcx,%rax)
.Ltmp152:
	leaq	24(%rsp), %rdi
	movl	$.L.str.7, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
.Ltmp153:
# BB#14:
	testl	%eax, %eax
	je	.LBB8_21
# BB#15:
.Ltmp154:
	leaq	24(%rsp), %rdi
	movl	$.L.str.9, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
.Ltmp155:
# BB#16:
	testl	%eax, %eax
	je	.LBB8_21
# BB#17:
.Ltmp156:
	leaq	24(%rsp), %rdi
	movl	$.L.str.11, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
.Ltmp157:
# BB#18:
	testl	%eax, %eax
	je	.LBB8_21
# BB#19:
.Ltmp158:
	leaq	24(%rsp), %rdi
	movl	$.L.str.13, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
.Ltmp159:
# BB#20:
	testl	%eax, %eax
	je	.LBB8_21
# BB#35:
	leaq	16(%r15), %rax
	movq	%rax, (%r15)
	movq	$0, 8(%r15)
	movb	$0, 16(%r15)
	jmp	.LBB8_36
.LBB8_21:
	leaq	16(%r15), %rax
	movq	%rax, (%r15)
	movq	24(%rsp), %r14
	movq	32(%rsp), %rbx
	testq	%r14, %r14
	jne	.LBB8_25
# BB#22:
	testq	%rbx, %rbx
	jne	.LBB8_23
.LBB8_25:
	movq	%rbx, 16(%rsp)
	cmpq	$15, %rbx
	jbe	.LBB8_28
# BB#26:                                # %.noexc6.i
.Ltmp162:
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm
.Ltmp163:
# BB#27:                                # %.noexc17
	movq	%rax, (%r15)
	movq	16(%rsp), %rcx
	movq	%rcx, 16(%r15)
.LBB8_28:
	testq	%rbx, %rbx
	je	.LBB8_32
# BB#29:
	cmpq	$1, %rbx
	jne	.LBB8_31
# BB#30:
	movb	(%r14), %cl
	movb	%cl, (%rax)
	jmp	.LBB8_32
.LBB8_31:
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memcpy
.LBB8_32:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2ERKS4_.exit
	movq	16(%rsp), %rax
	movq	%rax, 8(%r15)
	movq	(%r15), %rcx
	movb	$0, (%rcx,%rax)
.LBB8_36:
	movq	24(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB8_38
# BB#37:
	callq	_ZdlPv
.LBB8_38:
	movq	%r15, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB8_5:                                # %.noexc.i
.Ltmp165:
	movl	$.L.str.19, %edi
	callq	_ZSt19__throw_logic_errorPKc
.Ltmp166:
# BB#6:                                 # %.noexc
.LBB8_23:                               # %.noexc.i12
.Ltmp160:
	movl	$.L.str.19, %edi
	callq	_ZSt19__throw_logic_errorPKc
.Ltmp161:
# BB#24:                                # %.noexc16
.LBB8_39:
.Ltmp167:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB8_33:
.Ltmp164:
	movq	%rax, %rbx
	movq	24(%rsp), %rdi
	cmpq	%r12, %rdi
	jne	.LBB8_34
# BB#40:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB8_34:
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_Z5checkB5cxx11iPPc, .Lfunc_end8-_Z5checkB5cxx11iPPc
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp150-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp151-.Ltmp150       #   Call between .Ltmp150 and .Ltmp151
	.long	.Ltmp167-.Lfunc_begin5  #     jumps to .Ltmp167
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp152-.Ltmp151       #   Call between .Ltmp151 and .Ltmp152
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp152-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp163-.Ltmp152       #   Call between .Ltmp152 and .Ltmp163
	.long	.Ltmp164-.Lfunc_begin5  #     jumps to .Ltmp164
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp165-.Ltmp163       #   Call between .Ltmp163 and .Ltmp165
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp165-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp166-.Ltmp165       #   Call between .Ltmp165 and .Ltmp166
	.long	.Ltmp167-.Lfunc_begin5  #     jumps to .Ltmp167
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp164-.Lfunc_begin5  #     jumps to .Ltmp164
	.byte	0                       #   On action: cleanup
	.long	.Ltmp161-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Lfunc_end8-.Ltmp161    #   Call between .Ltmp161 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi92:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi93:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi95:
	.cfi_def_cfa_offset 304
.Lcfi96:
	.cfi_offset %rbx, -56
.Lcfi97:
	.cfi_offset %r12, -48
.Lcfi98:
	.cfi_offset %r13, -40
.Lcfi99:
	.cfi_offset %r14, -32
.Lcfi100:
	.cfi_offset %r15, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movl	%edi, %r15d
	leaq	120(%rsp), %rdi
	movl	%r15d, %esi
	movq	%r12, %rdx
	callq	_Z5checkB5cxx11iPPc
	cmpq	$0, 128(%rsp)
	je	.LBB9_2
# BB#1:
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
	jmp	.LBB9_3
.LBB9_2:
	movl	$50000, %r14d           # imm = 0xC350
.Ltmp168:
	leaq	120(%rsp), %rdi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$.L.str.9, %ecx
	movl	$2, %r8d
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm
.Ltmp169:
.LBB9_3:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEPKc.exit
.Ltmp170:
	leaq	120(%rsp), %rdi
	movl	$.L.str.7, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
.Ltmp171:
# BB#4:
	testl	%eax, %eax
	jne	.LBB9_16
# BB#5:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 96(%rsp)
	movq	$0, 112(%rsp)
.Ltmp172:
	leaq	152(%rsp), %rdi
	leaq	96(%rsp), %rsi
	movl	%r14d, %edx
	callq	_ZN9Fibonacci10get_numberEj
.Ltmp173:
# BB#6:
	movq	152(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_8
# BB#7:
	callq	_ZdlPv
.LBB9_8:
.Ltmp175:
	leaq	96(%rsp), %rdi
	callq	_ZNK9Fibonacci16show_all_numbersEv
.Ltmp176:
# BB#9:
	movq	96(%rsp), %rbp
	movq	104(%rsp), %rbx
	cmpq	%rbx, %rbp
	je	.LBB9_14
	.p2align	4, 0x90
.LBB9_10:                               # %.lr.ph.i.i.i.i.i39
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_12
# BB#11:                                #   in Loop: Header=BB9_10 Depth=1
	callq	_ZdlPv
.LBB9_12:                               # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i.i.i40
                                        #   in Loop: Header=BB9_10 Depth=1
	addq	$24, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB9_10
# BB#13:                                # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit.loopexit.i.i42
	movq	96(%rsp), %rbp
.LBB9_14:                               # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit.i.i43
	testq	%rbp, %rbp
	je	.LBB9_16
# BB#15:
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB9_16:
.Ltmp178:
	leaq	120(%rsp), %rdi
	movl	$.L.str.9, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
.Ltmp179:
# BB#17:
	testl	%eax, %eax
	jne	.LBB9_29
# BB#18:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 64(%rsp)
	movq	$0, 80(%rsp)
.Ltmp180:
	leaq	176(%rsp), %rdi
	leaq	64(%rsp), %rsi
	movl	%r14d, %edx
	callq	_ZN9Fibonacci10get_numberEj
.Ltmp181:
# BB#19:
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_21
# BB#20:
	callq	_ZdlPv
.LBB9_21:
.Ltmp183:
	leaq	64(%rsp), %rdi
	callq	_ZNK9Fibonacci16show_last_numberEv
.Ltmp184:
# BB#22:
	movq	64(%rsp), %rbp
	movq	72(%rsp), %rbx
	cmpq	%rbx, %rbp
	je	.LBB9_27
	.p2align	4, 0x90
.LBB9_23:                               # %.lr.ph.i.i.i.i.i65
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_25
# BB#24:                                #   in Loop: Header=BB9_23 Depth=1
	callq	_ZdlPv
.LBB9_25:                               # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i.i.i66
                                        #   in Loop: Header=BB9_23 Depth=1
	addq	$24, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB9_23
# BB#26:                                # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit.loopexit.i.i68
	movq	64(%rsp), %rbp
.LBB9_27:                               # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit.i.i69
	testq	%rbp, %rbp
	je	.LBB9_29
# BB#28:
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB9_29:
.Ltmp186:
	leaq	120(%rsp), %rdi
	movl	$.L.str.11, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
.Ltmp187:
# BB#30:
	testl	%eax, %eax
	jne	.LBB9_45
# BB#31:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movq	$0, 48(%rsp)
.Ltmp188:
	leaq	200(%rsp), %rdi
	leaq	32(%rsp), %rsi
	xorl	%edx, %edx
	callq	_ZN9Fibonacci10get_numberEj
.Ltmp189:
# BB#32:
	movq	200(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_34
# BB#33:
	callq	_ZdlPv
.LBB9_34:                               # %_ZN9FibonacciC2Ej.exit90
	cmpl	$3, %r15d
	jl	.LBB9_38
# BB#35:                                # %.lr.ph137.preheader
	movslq	%r15d, %rbx
	movl	$2, %ebp
	leaq	32(%rsp), %r13
	.p2align	4, 0x90
.LBB9_36:                               # %.lr.ph137
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbp,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movslq	%eax, %rsi
.Ltmp191:
	movq	%r13, %rdi
	callq	_ZN9Fibonacci11show_numberEm
.Ltmp192:
# BB#37:                                #   in Loop: Header=BB9_36 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB9_36
.LBB9_38:                               # %._crit_edge138
	movq	32(%rsp), %rbp
	movq	40(%rsp), %rbx
	cmpq	%rbx, %rbp
	je	.LBB9_43
	.p2align	4, 0x90
.LBB9_39:                               # %.lr.ph.i.i.i.i.i92
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_41
# BB#40:                                #   in Loop: Header=BB9_39 Depth=1
	callq	_ZdlPv
.LBB9_41:                               # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i.i.i93
                                        #   in Loop: Header=BB9_39 Depth=1
	addq	$24, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB9_39
# BB#42:                                # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit.loopexit.i.i95
	movq	32(%rsp), %rbp
.LBB9_43:                               # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit.i.i96
	testq	%rbp, %rbp
	je	.LBB9_45
# BB#44:
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB9_45:
.Ltmp194:
	leaq	120(%rsp), %rdi
	movl	$.L.str.13, %esi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
.Ltmp195:
# BB#46:
	testl	%eax, %eax
	jne	.LBB9_63
# BB#47:
	movl	$25000, %ebp            # imm = 0x61A8
	cmpl	$3, %r15d
	je	.LBB9_49
# BB#48:
	movq	24(%r12), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbp
.LBB9_49:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movq	$0, 16(%rsp)
.Ltmp197:
	leaq	224(%rsp), %rdi
	movq	%rsp, %rsi
	xorl	%edx, %edx
	callq	_ZN9Fibonacci10get_numberEj
.Ltmp198:
# BB#50:
	movq	224(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_52
# BB#51:
	callq	_ZdlPv
.LBB9_52:                               # %_ZN9FibonacciC2Ej.exit117
	testl	%r14d, %r14d
	je	.LBB9_56
# BB#53:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB9_54:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	rand
	cltd
	idivl	%ebp
	movslq	%edx, %rsi
.Ltmp200:
	movq	%r15, %rdi
	callq	_ZN9Fibonacci11show_numberEm
.Ltmp201:
# BB#55:                                #   in Loop: Header=BB9_54 Depth=1
	incl	%ebx
	cmpl	%r14d, %ebx
	jb	.LBB9_54
.LBB9_56:                               # %._crit_edge
	movq	(%rsp), %rbx
	movq	8(%rsp), %rbp
	cmpq	%rbp, %rbx
	je	.LBB9_61
	.p2align	4, 0x90
.LBB9_57:                               # %.lr.ph.i.i.i.i.i119
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_59
# BB#58:                                #   in Loop: Header=BB9_57 Depth=1
	callq	_ZdlPv
.LBB9_59:                               # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i.i.i120
                                        #   in Loop: Header=BB9_57 Depth=1
	addq	$24, %rbx
	cmpq	%rbx, %rbp
	jne	.LBB9_57
# BB#60:                                # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit.loopexit.i.i122
	movq	(%rsp), %rbx
.LBB9_61:                               # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit.i.i123
	testq	%rbx, %rbx
	je	.LBB9_63
# BB#62:
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB9_63:
	movq	120(%rsp), %rdi
	leaq	136(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB9_65
# BB#64:
	callq	_ZdlPv
.LBB9_65:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	xorl	%eax, %eax
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_66:
.Ltmp199:
	movq	%rax, %r14
	movq	(%rsp), %rbp
	movq	8(%rsp), %rbx
	cmpq	%rbx, %rbp
	je	.LBB9_106
	.p2align	4, 0x90
.LBB9_67:                               # %.lr.ph.i.i.i.i.i109
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_69
# BB#68:                                #   in Loop: Header=BB9_67 Depth=1
	callq	_ZdlPv
.LBB9_69:                               # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i.i.i110
                                        #   in Loop: Header=BB9_67 Depth=1
	addq	$24, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB9_67
	jmp	.LBB9_105
.LBB9_70:
.Ltmp190:
	movq	%rax, %r14
	movq	32(%rsp), %rbp
	movq	40(%rsp), %rbx
	cmpq	%rbx, %rbp
	je	.LBB9_106
	.p2align	4, 0x90
.LBB9_71:                               # %.lr.ph.i.i.i.i.i82
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_73
# BB#72:                                #   in Loop: Header=BB9_71 Depth=1
	callq	_ZdlPv
.LBB9_73:                               # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i.i.i83
                                        #   in Loop: Header=BB9_71 Depth=1
	addq	$24, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB9_71
	jmp	.LBB9_74
.LBB9_75:
.Ltmp185:
	movq	%rax, %r14
	movq	64(%rsp), %rbp
	movq	72(%rsp), %rbx
	cmpq	%rbx, %rbp
	je	.LBB9_106
	.p2align	4, 0x90
.LBB9_76:                               # %.lr.ph.i.i.i.i.i73
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_78
# BB#77:                                #   in Loop: Header=BB9_76 Depth=1
	callq	_ZdlPv
.LBB9_78:                               # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i.i.i74
                                        #   in Loop: Header=BB9_76 Depth=1
	addq	$24, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB9_76
	jmp	.LBB9_84
.LBB9_80:
.Ltmp182:
	movq	%rax, %r14
	movq	64(%rsp), %rbp
	movq	72(%rsp), %rbx
	cmpq	%rbx, %rbp
	je	.LBB9_106
	.p2align	4, 0x90
.LBB9_81:                               # %.lr.ph.i.i.i.i.i55
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_83
# BB#82:                                #   in Loop: Header=BB9_81 Depth=1
	callq	_ZdlPv
.LBB9_83:                               # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i.i.i56
                                        #   in Loop: Header=BB9_81 Depth=1
	addq	$24, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB9_81
.LBB9_84:                               # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit.loopexit.i.i58
	movq	64(%rsp), %rbp
	testq	%rbp, %rbp
	jne	.LBB9_107
	jmp	.LBB9_108
.LBB9_85:
.Ltmp177:
	movq	%rax, %r14
	movq	96(%rsp), %rbp
	movq	104(%rsp), %rbx
	cmpq	%rbx, %rbp
	je	.LBB9_106
	.p2align	4, 0x90
.LBB9_86:                               # %.lr.ph.i.i.i.i.i46
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_88
# BB#87:                                #   in Loop: Header=BB9_86 Depth=1
	callq	_ZdlPv
.LBB9_88:                               # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i.i.i47
                                        #   in Loop: Header=BB9_86 Depth=1
	addq	$24, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB9_86
	jmp	.LBB9_94
.LBB9_90:
.Ltmp174:
	movq	%rax, %r14
	movq	96(%rsp), %rbp
	movq	104(%rsp), %rbx
	cmpq	%rbx, %rbp
	je	.LBB9_106
	.p2align	4, 0x90
.LBB9_91:                               # %.lr.ph.i.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_93
# BB#92:                                #   in Loop: Header=BB9_91 Depth=1
	callq	_ZdlPv
.LBB9_93:                               # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i.i.i
                                        #   in Loop: Header=BB9_91 Depth=1
	addq	$24, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB9_91
.LBB9_94:                               # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit.loopexit.i.i
	movq	96(%rsp), %rbp
	testq	%rbp, %rbp
	jne	.LBB9_107
	jmp	.LBB9_108
.LBB9_95:
.Ltmp196:
	movq	%rax, %r14
	jmp	.LBB9_108
.LBB9_96:
.Ltmp193:
	movq	%rax, %r14
	movq	32(%rsp), %rbp
	movq	40(%rsp), %rbx
	cmpq	%rbx, %rbp
	je	.LBB9_106
	.p2align	4, 0x90
.LBB9_97:                               # %.lr.ph.i.i.i.i.i100
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_99
# BB#98:                                #   in Loop: Header=BB9_97 Depth=1
	callq	_ZdlPv
.LBB9_99:                               # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i.i.i101
                                        #   in Loop: Header=BB9_97 Depth=1
	addq	$24, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB9_97
.LBB9_74:                               # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit.loopexit.i.i85
	movq	32(%rsp), %rbp
	testq	%rbp, %rbp
	jne	.LBB9_107
	jmp	.LBB9_108
.LBB9_101:
.Ltmp202:
	movq	%rax, %r14
	movq	(%rsp), %rbp
	movq	8(%rsp), %rbx
	cmpq	%rbx, %rbp
	je	.LBB9_106
	.p2align	4, 0x90
.LBB9_102:                              # %.lr.ph.i.i.i.i.i127
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_104
# BB#103:                               #   in Loop: Header=BB9_102 Depth=1
	callq	_ZdlPv
.LBB9_104:                              # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i.i.i128
                                        #   in Loop: Header=BB9_102 Depth=1
	addq	$24, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB9_102
.LBB9_105:                              # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit.loopexit.i.i130
	movq	(%rsp), %rbp
.LBB9_106:                              # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit.i.i131
	testq	%rbp, %rbp
	je	.LBB9_108
.LBB9_107:
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB9_108:
	movq	120(%rsp), %rdi
	leaq	136(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB9_110
# BB#109:
	callq	_ZdlPv
.LBB9_110:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit134
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end9:
	.size	main, .Lfunc_end9-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\271\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\266\001"              # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp168-.Lfunc_begin6  #   Call between .Lfunc_begin6 and .Ltmp168
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp168-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp171-.Ltmp168       #   Call between .Ltmp168 and .Ltmp171
	.long	.Ltmp196-.Lfunc_begin6  #     jumps to .Ltmp196
	.byte	0                       #   On action: cleanup
	.long	.Ltmp172-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	.Ltmp174-.Lfunc_begin6  #     jumps to .Ltmp174
	.byte	0                       #   On action: cleanup
	.long	.Ltmp175-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp176-.Ltmp175       #   Call between .Ltmp175 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin6  #     jumps to .Ltmp177
	.byte	0                       #   On action: cleanup
	.long	.Ltmp178-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp196-.Lfunc_begin6  #     jumps to .Ltmp196
	.byte	0                       #   On action: cleanup
	.long	.Ltmp180-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp181-.Ltmp180       #   Call between .Ltmp180 and .Ltmp181
	.long	.Ltmp182-.Lfunc_begin6  #     jumps to .Ltmp182
	.byte	0                       #   On action: cleanup
	.long	.Ltmp183-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp184-.Ltmp183       #   Call between .Ltmp183 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin6  #     jumps to .Ltmp185
	.byte	0                       #   On action: cleanup
	.long	.Ltmp186-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp187-.Ltmp186       #   Call between .Ltmp186 and .Ltmp187
	.long	.Ltmp196-.Lfunc_begin6  #     jumps to .Ltmp196
	.byte	0                       #   On action: cleanup
	.long	.Ltmp188-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp189-.Ltmp188       #   Call between .Ltmp188 and .Ltmp189
	.long	.Ltmp190-.Lfunc_begin6  #     jumps to .Ltmp190
	.byte	0                       #   On action: cleanup
	.long	.Ltmp191-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp192-.Ltmp191       #   Call between .Ltmp191 and .Ltmp192
	.long	.Ltmp193-.Lfunc_begin6  #     jumps to .Ltmp193
	.byte	0                       #   On action: cleanup
	.long	.Ltmp194-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp195-.Ltmp194       #   Call between .Ltmp194 and .Ltmp195
	.long	.Ltmp196-.Lfunc_begin6  #     jumps to .Ltmp196
	.byte	0                       #   On action: cleanup
	.long	.Ltmp197-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Ltmp198-.Ltmp197       #   Call between .Ltmp197 and .Ltmp198
	.long	.Ltmp199-.Lfunc_begin6  #     jumps to .Ltmp199
	.byte	0                       #   On action: cleanup
	.long	.Ltmp200-.Lfunc_begin6  # >> Call Site 13 <<
	.long	.Ltmp201-.Ltmp200       #   Call between .Ltmp200 and .Ltmp201
	.long	.Ltmp202-.Lfunc_begin6  #     jumps to .Ltmp202
	.byte	0                       #   On action: cleanup
	.long	.Ltmp201-.Lfunc_begin6  # >> Call Site 14 <<
	.long	.Lfunc_end9-.Ltmp201    #   Call between .Ltmp201 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNSt6vectorImSaImEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPmS1_EERKm,"axG",@progbits,_ZNSt6vectorImSaImEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPmS1_EERKm,comdat
	.weak	_ZNSt6vectorImSaImEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPmS1_EERKm
	.p2align	4, 0x90
	.type	_ZNSt6vectorImSaImEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPmS1_EERKm,@function
_ZNSt6vectorImSaImEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPmS1_EERKm: # @_ZNSt6vectorImSaImEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPmS1_EERKm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi105:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi106:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi108:
	.cfi_def_cfa_offset 64
.Lcfi109:
	.cfi_offset %rbx, -56
.Lcfi110:
	.cfi_offset %r12, -48
.Lcfi111:
	.cfi_offset %r13, -40
.Lcfi112:
	.cfi_offset %r14, -32
.Lcfi113:
	.cfi_offset %r15, -24
.Lcfi114:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	cmpq	16(%rbx), %rdi
	je	.LBB10_4
# BB#1:
	leaq	-8(%rdi), %rdx
	movq	-8(%rdi), %rax
	movq	%rax, (%rdi)
	leaq	8(%rdi), %rax
	movq	%rax, 8(%rbx)
	movq	(%r15), %rbx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	je	.LBB10_3
# BB#2:
	shlq	$3, %rax
	subq	%rax, %rdi
	movq	%r14, %rsi
	callq	memmove
.LBB10_3:                               # %_ZSt13copy_backwardIPmS0_ET0_T_S2_S1_.exit
	movq	%rbx, (%r14)
	jmp	.LBB10_15
.LBB10_4:                               # %_ZNKSt6vectorImSaImEE12_M_check_lenEmPKc.exit
	movq	(%rbx), %rbp
	subq	%rbp, %rdi
	sarq	$3, %rdi
	movl	$1, %ecx
	cmovneq	%rdi, %rcx
	leaq	(%rcx,%rdi), %r13
	movq	%r13, %rax
	shrq	$61, %rax
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	cmovneq	%rax, %r13
	addq	%rdi, %rcx
	cmovbq	%rax, %r13
	testq	%r13, %r13
	je	.LBB10_5
# BB#6:
	cmpq	%rax, %r13
	ja	.LBB10_16
# BB#7:                                 # %_ZN9__gnu_cxx14__alloc_traitsISaImEE8allocateERS1_m.exit.i
	leaq	(,%r13,8), %rdi
	callq	_Znwm
	movq	%rax, %r12
	movq	(%rbx), %rsi
	jmp	.LBB10_8
.LBB10_5:
	xorl	%r12d, %r12d
	movq	%rbp, %rsi
.LBB10_8:
	movq	%r14, %rax
	subq	%rbp, %rax
	sarq	$3, %rax
	movq	(%r15), %rcx
	movq	%rcx, (%r12,%rax,8)
	movq	%r14, %rdx
	subq	%rsi, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB10_10
# BB#9:
	movq	%r12, %rdi
	movq	%rsi, %r15
	callq	memmove
	movq	%r15, %rsi
.LBB10_10:
	leaq	8(%r12,%rbp,8), %r15
	movq	8(%rbx), %rdx
	subq	%r14, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB10_12
# BB#11:
	movq	%r15, %rdi
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%r14, %rsi
	callq	memmove
	movq	(%rsp), %rsi            # 8-byte Reload
.LBB10_12:
	leaq	(%r15,%rbp,8), %rbp
	testq	%rsi, %rsi
	je	.LBB10_14
# BB#13:
	movq	%rsi, %rdi
	callq	_ZdlPv
.LBB10_14:                              # %_ZNSt12_Vector_baseImSaImEE13_M_deallocateEPmm.exit37
	movq	%r12, (%rbx)
	movq	%rbp, 8(%rbx)
	leaq	(%r12,%r13,8), %rax
	movq	%rax, 16(%rbx)
.LBB10_15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_16:
	callq	_ZSt17__throw_bad_allocv
.Lfunc_end10:
	.size	_ZNSt6vectorImSaImEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPmS1_EERKm, .Lfunc_end10-_ZNSt6vectorImSaImEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPmS1_EERKm
	.cfi_endproc

	.section	.text._ZNSt6vectorImSaImEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPmS1_EEmRKm,"axG",@progbits,_ZNSt6vectorImSaImEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPmS1_EEmRKm,comdat
	.weak	_ZNSt6vectorImSaImEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPmS1_EEmRKm
	.p2align	4, 0x90
	.type	_ZNSt6vectorImSaImEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPmS1_EEmRKm,@function
_ZNSt6vectorImSaImEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPmS1_EEmRKm: # @_ZNSt6vectorImSaImEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPmS1_EEmRKm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi118:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi119:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi121:
	.cfi_def_cfa_offset 64
.Lcfi122:
	.cfi_offset %rbx, -56
.Lcfi123:
	.cfi_offset %r12, -48
.Lcfi124:
	.cfi_offset %r13, -40
.Lcfi125:
	.cfi_offset %r14, -32
.Lcfi126:
	.cfi_offset %r15, -24
.Lcfi127:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB11_78
# BB#1:
	movq	8(%r15), %r12
	movq	16(%r15), %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%r14, %rax
	jae	.LBB11_2
# BB#52:
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	movq	%r15, %rbp
	movq	(%r15), %r15
	subq	%r15, %r12
	sarq	$3, %r12
	movq	%rax, %rcx
	subq	%r12, %rcx
	cmpq	%r14, %rcx
	jb	.LBB11_79
# BB#53:                                # %_ZNKSt6vectorImSaImEE12_M_check_lenEmPKc.exit
	cmpq	%r14, %r12
	movq	%r12, %rcx
	cmovbq	%r14, %rcx
	leaq	(%rcx,%r12), %rdx
	cmpq	%rax, %rdx
	cmovaq	%rax, %rdx
	addq	%r12, %rcx
	cmovbq	%rax, %rdx
	testq	%rdx, %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	je	.LBB11_54
# BB#55:
	cmpq	%rax, %rdx
	ja	.LBB11_80
# BB#56:                                # %_ZN9__gnu_cxx14__alloc_traitsISaImEE8allocateERS1_m.exit.i
	leaq	(,%rdx,8), %rdi
	callq	_Znwm
	movq	%rax, %r12
	jmp	.LBB11_57
.LBB11_2:
	movabsq	$4611686018427387900, %r9 # imm = 0x3FFFFFFFFFFFFFFC
	movq	(%r13), %rbp
	movq	%r12, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %r13
	sarq	$3, %r13
	cmpq	%r14, %r13
	jbe	.LBB11_21
# BB#3:
	leaq	(,%r14,8), %rdx
	movq	%r12, %r13
	subq	%rdx, %r13
	testq	%rdx, %rdx
	je	.LBB11_4
# BB#5:
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	memmove
	movabsq	$4611686018427387900, %r9 # imm = 0x3FFFFFFFFFFFFFFC
	movq	8(%r15), %rax
	jmp	.LBB11_6
.LBB11_21:
	subq	%r13, %r14
	je	.LBB11_22
# BB#23:                                # %.lr.ph.i.i.i.i.i64.preheader
	cmpq	$3, %r14
	movq	%r14, %rsi
	movq	%r12, %rax
	jbe	.LBB11_35
# BB#24:                                # %min.iters.checked
	movq	%r14, %r8
	andq	$-4, %r8
	movq	%r14, %rcx
	andq	$-4, %rcx
	je	.LBB11_25
# BB#26:                                # %vector.ph
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%rcx), %rax
	movl	%eax, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB11_27
# BB#28:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB11_29:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%r12,%rdi,8)
	movdqu	%xmm0, 16(%r12,%rdi,8)
	addq	$4, %rdi
	incq	%rsi
	jne	.LBB11_29
	jmp	.LBB11_30
.LBB11_54:
	xorl	%r12d, %r12d
.LBB11_57:                              # %_ZNSt12_Vector_baseImSaImEE11_M_allocateEm.exit
	movq	%rbx, %rdi
	subq	%r15, %rdi
	sarq	$3, %rdi
	leaq	(%r12,%rdi,8), %rax
	movq	(%r13), %rcx
	cmpq	$4, %r14
	jae	.LBB11_59
# BB#58:
	movq	%r14, %rdx
	movq	%rbp, %r15
	jmp	.LBB11_70
.LBB11_59:                              # %min.iters.checked137
	movq	%r14, %r8
	andq	$-4, %r8
	movq	%r14, %r9
	andq	$-4, %r9
	movq	%rbp, %r15
	je	.LBB11_60
# BB#61:                                # %vector.ph141
	movd	%rcx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %r10
	movl	%r10d, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB11_62
# BB#63:                                # %vector.body133.prol.preheader
	leaq	16(%r12,%rdi,8), %rbp
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB11_64:                              # %vector.body133.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbp,%rsi,8)
	movdqu	%xmm0, (%rbp,%rsi,8)
	addq	$4, %rsi
	incq	%rdx
	jne	.LBB11_64
	jmp	.LBB11_65
.LBB11_4:
	movq	%r12, %rax
.LBB11_6:                               # %_ZSt22__uninitialized_move_aIPmS0_SaImEET0_T_S3_S2_RT1_.exit
	leaq	(%rax,%r14,8), %rax
	movq	%rax, 8(%r15)
	subq	%rbx, %r13
	movq	%r13, %rax
	sarq	$3, %rax
	je	.LBB11_8
# BB#7:
	shlq	$3, %rax
	subq	%rax, %r12
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	%r9, %r15
	callq	memmove
	movq	%r15, %r9
.LBB11_8:                               # %.lr.ph.i.i68.preheader
	leaq	-8(,%r14,8), %rax
	shrq	$3, %rax
	incq	%rax
	cmpq	$4, %rax
	movq	%rbx, %rcx
	jb	.LBB11_19
# BB#9:                                 # %min.iters.checked119
	andq	%rax, %r9
	movq	%rbx, %rcx
	je	.LBB11_19
# BB#10:                                # %vector.ph123
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %rcx
	movl	%ecx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB11_11
# BB#12:                                # %vector.body115.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB11_13:                              # %vector.body115.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rbx,%rdx,8)
	movdqu	%xmm0, 16(%rbx,%rdx,8)
	addq	$4, %rdx
	incq	%rsi
	jne	.LBB11_13
	jmp	.LBB11_14
.LBB11_22:
	movq	%r12, %rdi
	jmp	.LBB11_37
.LBB11_60:
	movq	%r14, %rdx
	jmp	.LBB11_70
.LBB11_62:
	xorl	%esi, %esi
.LBB11_65:                              # %vector.body133.prol.loopexit
	cmpq	$28, %r10
	jb	.LBB11_68
# BB#66:                                # %vector.ph141.new
	movq	%r9, %rdx
	subq	%rsi, %rdx
	addq	%rsi, %rdi
	leaq	240(%r12,%rdi,8), %rsi
	.p2align	4, 0x90
.LBB11_67:                              # %vector.body133
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rsi)
	movdqu	%xmm0, -224(%rsi)
	movdqu	%xmm0, -208(%rsi)
	movdqu	%xmm0, -192(%rsi)
	movdqu	%xmm0, -176(%rsi)
	movdqu	%xmm0, -160(%rsi)
	movdqu	%xmm0, -144(%rsi)
	movdqu	%xmm0, -128(%rsi)
	movdqu	%xmm0, -112(%rsi)
	movdqu	%xmm0, -96(%rsi)
	movdqu	%xmm0, -80(%rsi)
	movdqu	%xmm0, -64(%rsi)
	movdqu	%xmm0, -48(%rsi)
	movdqu	%xmm0, -32(%rsi)
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	addq	$256, %rsi              # imm = 0x100
	addq	$-32, %rdx
	jne	.LBB11_67
.LBB11_68:                              # %middle.block134
	cmpq	%r14, %r9
	je	.LBB11_71
# BB#69:
	movq	%r14, %rdx
	subq	%r8, %rdx
	leaq	(%rax,%r9,8), %rax
	.p2align	4, 0x90
.LBB11_70:                              # %.lr.ph.i.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rax)
	addq	$8, %rax
	decq	%rdx
	jne	.LBB11_70
.LBB11_71:                              # %_ZSt24__uninitialized_fill_n_aIPmmmmET_S1_T0_RKT1_RSaIT2_E.exit
	movq	(%r15), %r13
	movq	%rbx, %rdx
	subq	%r13, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB11_73
# BB#72:
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	memmove
.LBB11_73:
	leaq	(%r12,%rbp,8), %rax
	leaq	(%rax,%r14,8), %r14
	movq	8(%r15), %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB11_75
# BB#74:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	memmove
.LBB11_75:
	leaq	(%r14,%rbp,8), %rbx
	testq	%r13, %r13
	je	.LBB11_77
# BB#76:
	movq	%r13, %rdi
	callq	_ZdlPv
.LBB11_77:                              # %_ZNSt12_Vector_baseImSaImEE13_M_deallocateEPmm.exit57
	movq	%r12, (%r15)
	movq	%rbx, 8(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%r12,%rax,8), %rax
	movq	%rax, 16(%r15)
	jmp	.LBB11_78
.LBB11_25:
	movq	%r14, %rsi
	movq	%r12, %rax
	jmp	.LBB11_35
.LBB11_11:
	xorl	%edx, %edx
.LBB11_14:                              # %vector.body115.prol.loopexit
	cmpq	$28, %rcx
	jb	.LBB11_17
# BB#15:                                # %vector.ph123.new
	movq	%r9, %rcx
	subq	%rdx, %rcx
	leaq	240(%rbx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB11_16:                              # %vector.body115
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdx)
	movdqu	%xmm0, -224(%rdx)
	movdqu	%xmm0, -208(%rdx)
	movdqu	%xmm0, -192(%rdx)
	movdqu	%xmm0, -176(%rdx)
	movdqu	%xmm0, -160(%rdx)
	movdqu	%xmm0, -144(%rdx)
	movdqu	%xmm0, -128(%rdx)
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm0, -96(%rdx)
	movdqu	%xmm0, -80(%rdx)
	movdqu	%xmm0, -64(%rdx)
	movdqu	%xmm0, -48(%rdx)
	movdqu	%xmm0, -32(%rdx)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-32, %rcx
	jne	.LBB11_16
.LBB11_17:                              # %middle.block116
	cmpq	%r9, %rax
	je	.LBB11_78
# BB#18:
	leaq	(%rbx,%r9,8), %rcx
.LBB11_19:                              # %.lr.ph.i.i68.preheader158
	leaq	(%rbx,%r14,8), %rax
	.p2align	4, 0x90
.LBB11_20:                              # %.lr.ph.i.i68
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB11_20
	jmp	.LBB11_78
.LBB11_27:
	xorl	%edi, %edi
.LBB11_30:                              # %vector.body.prol.loopexit
	cmpq	$28, %rax
	jb	.LBB11_33
# BB#31:                                # %vector.ph.new
	movq	%rcx, %rsi
	subq	%rdi, %rsi
	leaq	240(%r12,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB11_32:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdi)
	movdqu	%xmm0, -224(%rdi)
	movdqu	%xmm0, -208(%rdi)
	movdqu	%xmm0, -192(%rdi)
	movdqu	%xmm0, -176(%rdi)
	movdqu	%xmm0, -160(%rdi)
	movdqu	%xmm0, -144(%rdi)
	movdqu	%xmm0, -128(%rdi)
	movdqu	%xmm0, -112(%rdi)
	movdqu	%xmm0, -96(%rdi)
	movdqu	%xmm0, -80(%rdi)
	movdqu	%xmm0, -64(%rdi)
	movdqu	%xmm0, -48(%rdi)
	movdqu	%xmm0, -32(%rdi)
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	addq	$256, %rdi              # imm = 0x100
	addq	$-32, %rsi
	jne	.LBB11_32
.LBB11_33:                              # %middle.block
	cmpq	%rcx, %r14
	je	.LBB11_36
# BB#34:
	movq	%r14, %rsi
	subq	%r8, %rsi
	leaq	(%r12,%rcx,8), %rax
	.p2align	4, 0x90
.LBB11_35:                              # %.lr.ph.i.i.i.i.i64
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rax)
	addq	$8, %rax
	decq	%rsi
	jne	.LBB11_35
.LBB11_36:                              # %._crit_edge.loopexit.i.i.i.i.i61
	leaq	(%r12,%r14,8), %rdi
.LBB11_37:                              # %_ZSt24__uninitialized_fill_n_aIPmmmmET_S1_T0_RKT1_RSaIT2_E.exit66
	movq	%rdi, 8(%r15)
	testq	%r13, %r13
	je	.LBB11_39
# BB#38:
	movq	%rbx, %rsi
	movq	%r9, %r14
	callq	memmove
	movq	%r14, %r9
	movq	8(%r15), %rdi
.LBB11_39:                              # %_ZSt22__uninitialized_move_aIPmS0_SaImEET0_T_S3_S2_RT1_.exit59
	leaq	(%rdi,%r13,8), %rax
	movq	%rax, 8(%r15)
	cmpq	%rbx, %r12
	je	.LBB11_78
# BB#40:                                # %.lr.ph.i.i.preheader
	leaq	-8(%r12), %rax
	subq	%rbx, %rax
	shrq	$3, %rax
	incq	%rax
	cmpq	$4, %rax
	jb	.LBB11_51
# BB#41:                                # %min.iters.checked101
	andq	%rax, %r9
	je	.LBB11_51
# BB#42:                                # %vector.ph105
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %rcx
	movl	%ecx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB11_43
# BB#44:                                # %vector.body95.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB11_45:                              # %vector.body95.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rbx,%rdx,8)
	movdqu	%xmm0, 16(%rbx,%rdx,8)
	addq	$4, %rdx
	incq	%rsi
	jne	.LBB11_45
	jmp	.LBB11_46
.LBB11_43:
	xorl	%edx, %edx
.LBB11_46:                              # %vector.body95.prol.loopexit
	cmpq	$28, %rcx
	jb	.LBB11_49
# BB#47:                                # %vector.ph105.new
	movq	%r9, %rcx
	subq	%rdx, %rcx
	leaq	240(%rbx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB11_48:                              # %vector.body95
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdx)
	movdqu	%xmm0, -224(%rdx)
	movdqu	%xmm0, -208(%rdx)
	movdqu	%xmm0, -192(%rdx)
	movdqu	%xmm0, -176(%rdx)
	movdqu	%xmm0, -160(%rdx)
	movdqu	%xmm0, -144(%rdx)
	movdqu	%xmm0, -128(%rdx)
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm0, -96(%rdx)
	movdqu	%xmm0, -80(%rdx)
	movdqu	%xmm0, -64(%rdx)
	movdqu	%xmm0, -48(%rdx)
	movdqu	%xmm0, -32(%rdx)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-32, %rcx
	jne	.LBB11_48
.LBB11_49:                              # %middle.block96
	cmpq	%r9, %rax
	je	.LBB11_78
# BB#50:
	leaq	(%rbx,%r9,8), %rbx
	.p2align	4, 0x90
.LBB11_51:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.LBB11_51
.LBB11_78:                              # %_ZSt4fillIPmmEvT_S1_RKT0_.exit69
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_79:
	movl	$.L.str.17, %edi
	callq	_ZSt20__throw_length_errorPKc
.LBB11_80:
	callq	_ZSt17__throw_bad_allocv
.Lfunc_end11:
	.size	_ZNSt6vectorImSaImEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPmS1_EEmRKm, .Lfunc_end11-_ZNSt6vectorImSaImEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPmS1_EEmRKm
	.cfi_endproc

	.section	.text._ZNSt6vectorI6BigIntSaIS0_EE20_M_allocate_and_copyIPS0_EES4_mT_S5_,"axG",@progbits,_ZNSt6vectorI6BigIntSaIS0_EE20_M_allocate_and_copyIPS0_EES4_mT_S5_,comdat
	.weak	_ZNSt6vectorI6BigIntSaIS0_EE20_M_allocate_and_copyIPS0_EES4_mT_S5_
	.p2align	4, 0x90
	.type	_ZNSt6vectorI6BigIntSaIS0_EE20_M_allocate_and_copyIPS0_EES4_mT_S5_,@function
_ZNSt6vectorI6BigIntSaIS0_EE20_M_allocate_and_copyIPS0_EES4_mT_S5_: # @_ZNSt6vectorI6BigIntSaIS0_EE20_M_allocate_and_copyIPS0_EES4_mT_S5_
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r15
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 32
.Lcfi131:
	.cfi_offset %rbx, -32
.Lcfi132:
	.cfi_offset %r14, -24
.Lcfi133:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	testq	%rsi, %rsi
	je	.LBB12_1
# BB#2:
	movabsq	$768614336404564651, %rax # imm = 0xAAAAAAAAAAAAAAB
	cmpq	%rax, %rsi
	jae	.LBB12_13
# BB#3:                                 # %_ZN9__gnu_cxx14__alloc_traitsISaI6BigIntEE8allocateERS2_m.exit.i
	shlq	$3, %rsi
	leaq	(%rsi,%rsi,2), %rdi
	callq	_Znwm
	movq	%rax, %rbx
	jmp	.LBB12_4
.LBB12_1:
	xorl	%ebx, %ebx
.LBB12_4:                               # %_ZNSt12_Vector_baseI6BigIntSaIS0_EE11_M_allocateEm.exit
.Ltmp203:
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIP6BigIntS3_EET0_T_S5_S4_
.Ltmp204:
# BB#5:                                 # %_ZSt22__uninitialized_copy_aIP6BigIntS1_S0_ET0_T_S3_S2_RSaIT1_E.exit
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB12_13:
	callq	_ZSt17__throw_bad_allocv
.LBB12_6:
.Ltmp205:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	testq	%rbx, %rbx
	je	.LBB12_8
# BB#7:
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB12_8:                               # %_ZNSt12_Vector_baseI6BigIntSaIS0_EE13_M_deallocateEPS0_m.exit
.Ltmp206:
	callq	__cxa_rethrow
.Ltmp207:
# BB#12:
.LBB12_9:
.Ltmp208:
	movq	%rax, %rbx
.Ltmp209:
	callq	__cxa_end_catch
.Ltmp210:
# BB#10:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB12_11:
.Ltmp211:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZNSt6vectorI6BigIntSaIS0_EE20_M_allocate_and_copyIPS0_EES4_mT_S5_, .Lfunc_end12-_ZNSt6vectorI6BigIntSaIS0_EE20_M_allocate_and_copyIPS0_EES4_mT_S5_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin7-.Lfunc_begin7 # >> Call Site 1 <<
	.long	.Ltmp203-.Lfunc_begin7  #   Call between .Lfunc_begin7 and .Ltmp203
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp203-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp204-.Ltmp203       #   Call between .Ltmp203 and .Ltmp204
	.long	.Ltmp205-.Lfunc_begin7  #     jumps to .Ltmp205
	.byte	1                       #   On action: 1
	.long	.Ltmp204-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp206-.Ltmp204       #   Call between .Ltmp204 and .Ltmp206
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp206-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp207-.Ltmp206       #   Call between .Ltmp206 and .Ltmp207
	.long	.Ltmp208-.Lfunc_begin7  #     jumps to .Ltmp208
	.byte	0                       #   On action: cleanup
	.long	.Ltmp209-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp210-.Ltmp209       #   Call between .Ltmp209 and .Ltmp210
	.long	.Ltmp211-.Lfunc_begin7  #     jumps to .Ltmp211
	.byte	1                       #   On action: 1
	.long	.Ltmp210-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Lfunc_end12-.Ltmp210   #   Call between .Ltmp210 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.zero	16
	.section	.text._ZNSt20__uninitialized_copyILb0EE13__uninit_copyIP6BigIntS3_EET0_T_S5_S4_,"axG",@progbits,_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIP6BigIntS3_EET0_T_S5_S4_,comdat
	.weak	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIP6BigIntS3_EET0_T_S5_S4_
	.p2align	4, 0x90
	.type	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIP6BigIntS3_EET0_T_S5_S4_,@function
_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIP6BigIntS3_EET0_T_S5_S4_: # @_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIP6BigIntS3_EET0_T_S5_S4_
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%rbp
.Lcfi134:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi135:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi136:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi137:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi138:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi140:
	.cfi_def_cfa_offset 64
.Lcfi141:
	.cfi_offset %rbx, -56
.Lcfi142:
	.cfi_offset %r12, -48
.Lcfi143:
	.cfi_offset %r13, -40
.Lcfi144:
	.cfi_offset %r14, -32
.Lcfi145:
	.cfi_offset %r15, -24
.Lcfi146:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rdi, %rbp
	cmpq	%rsi, %rbp
	je	.LBB13_13
# BB#1:                                 # %.lr.ph.preheader
	subq	%rbp, %rsi
	movq	%rsi, (%rsp)            # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB13_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp,%r12), %rdi
	subq	(%rbp,%r12), %rdi
	movq	%rdi, %r13
	sarq	$3, %r13
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx,%r12)
	movq	$0, 16(%rbx,%r12)
	je	.LBB13_3
# BB#4:                                 #   in Loop: Header=BB13_2 Depth=1
	movq	%r13, %rax
	shrq	$61, %rax
	jne	.LBB13_5
# BB#7:                                 # %_ZN9__gnu_cxx14__alloc_traitsISaImEE8allocateERS1_m.exit.i.i.i.i.i.i
                                        #   in Loop: Header=BB13_2 Depth=1
.Ltmp212:
	callq	_Znwm
.Ltmp213:
# BB#8:                                 # %.noexc14
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	%rax, %r15
	jmp	.LBB13_9
	.p2align	4, 0x90
.LBB13_3:                               #   in Loop: Header=BB13_2 Depth=1
	xorl	%eax, %eax
	xorl	%r15d, %r15d
.LBB13_9:                               #   in Loop: Header=BB13_2 Depth=1
	leaq	(%rbx,%r12), %r14
	movq	%r15, (%r14)
	movq	%rax, 8(%r14)
	leaq	(%r15,%r13,8), %rcx
	movq	%rcx, 16(%r14)
	movq	(%rbp,%r12), %rsi
	movq	%rbp, %r13
	movq	8(%rbp,%r12), %rdx
	subq	%rsi, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB13_11
# BB#10:                                #   in Loop: Header=BB13_2 Depth=1
	movq	%rax, %rdi
	callq	memmove
.LBB13_11:                              #   in Loop: Header=BB13_2 Depth=1
	leaq	(%r15,%rbp,8), %rax
	movq	%rax, 8(%r14)
	addq	$24, %r12
	cmpq	%r12, (%rsp)            # 8-byte Folded Reload
	movq	%r13, %rbp
	jne	.LBB13_2
# BB#12:                                # %._crit_edge.loopexit
	addq	%r12, %rbx
.LBB13_13:                              # %._crit_edge
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_5:                               # %.noexc.i.i.i.i
.Ltmp215:
	callq	_ZSt17__throw_bad_allocv
.Ltmp216:
# BB#6:                                 # %.noexc
.LBB13_15:                              # %.loopexit.split-lp
.Ltmp217:
	jmp	.LBB13_16
.LBB13_14:                              # %.loopexit
.Ltmp214:
.LBB13_16:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	testq	%r12, %r12
	je	.LBB13_20
	.p2align	4, 0x90
.LBB13_17:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_19
# BB#18:                                #   in Loop: Header=BB13_17 Depth=1
	callq	_ZdlPv
.LBB13_19:                              # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i
                                        #   in Loop: Header=BB13_17 Depth=1
	addq	$24, %rbx
	addq	$-24, %r12
	jne	.LBB13_17
.LBB13_20:                              # %_ZSt8_DestroyIP6BigIntEvT_S2_.exit
.Ltmp218:
	callq	__cxa_rethrow
.Ltmp219:
# BB#24:
.LBB13_21:
.Ltmp220:
	movq	%rax, %rbx
.Ltmp221:
	callq	__cxa_end_catch
.Ltmp222:
# BB#22:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB13_23:
.Ltmp223:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIP6BigIntS3_EET0_T_S5_S4_, .Lfunc_end13-_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIP6BigIntS3_EET0_T_S5_S4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp212-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp213-.Ltmp212       #   Call between .Ltmp212 and .Ltmp213
	.long	.Ltmp214-.Lfunc_begin8  #     jumps to .Ltmp214
	.byte	1                       #   On action: 1
	.long	.Ltmp213-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp215-.Ltmp213       #   Call between .Ltmp213 and .Ltmp215
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp215-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp216-.Ltmp215       #   Call between .Ltmp215 and .Ltmp216
	.long	.Ltmp217-.Lfunc_begin8  #     jumps to .Ltmp217
	.byte	1                       #   On action: 1
	.long	.Ltmp216-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp218-.Ltmp216       #   Call between .Ltmp216 and .Ltmp218
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp218-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp219-.Ltmp218       #   Call between .Ltmp218 and .Ltmp219
	.long	.Ltmp220-.Lfunc_begin8  #     jumps to .Ltmp220
	.byte	0                       #   On action: cleanup
	.long	.Ltmp221-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp222-.Ltmp221       #   Call between .Ltmp221 and .Ltmp222
	.long	.Ltmp223-.Lfunc_begin8  #     jumps to .Ltmp223
	.byte	1                       #   On action: 1
	.long	.Ltmp222-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Lfunc_end13-.Ltmp222   #   Call between .Ltmp222 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNSt6vectorI6BigIntSaIS0_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS0_S2_EERKS0_,"axG",@progbits,_ZNSt6vectorI6BigIntSaIS0_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS0_S2_EERKS0_,comdat
	.weak	_ZNSt6vectorI6BigIntSaIS0_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS0_S2_EERKS0_
	.p2align	4, 0x90
	.type	_ZNSt6vectorI6BigIntSaIS0_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS0_S2_EERKS0_,@function
_ZNSt6vectorI6BigIntSaIS0_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS0_S2_EERKS0_: # @_ZNSt6vectorI6BigIntSaIS0_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS0_S2_EERKS0_
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%rbp
.Lcfi147:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi148:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi149:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi150:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi151:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi153:
	.cfi_def_cfa_offset 112
.Lcfi154:
	.cfi_offset %rbx, -56
.Lcfi155:
	.cfi_offset %r12, -48
.Lcfi156:
	.cfi_offset %r13, -40
.Lcfi157:
	.cfi_offset %r14, -32
.Lcfi158:
	.cfi_offset %r15, -24
.Lcfi159:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r13
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	movq	8(%r13), %rbp
	cmpq	16(%r13), %rbp
	je	.LBB14_24
# BB#1:
	movq	-24(%rbp), %rsi
	movq	-16(%rbp), %rdx
	movq	%rdx, %rdi
	subq	%rsi, %rdi
	movq	%rdi, %r14
	sarq	$3, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movq	$0, 16(%rbp)
	je	.LBB14_2
# BB#3:
	cmpq	%rax, %r14
	ja	.LBB14_63
# BB#4:                                 # %_ZN9__gnu_cxx14__alloc_traitsISaImEE8allocateERS1_m.exit.i.i.i.i.i.i.i
	callq	_Znwm
	movq	%rax, %r15
	movq	-24(%rbp), %rsi
	movq	-16(%rbp), %rdx
	jmp	.LBB14_5
.LBB14_24:                              # %_ZNKSt6vectorI6BigIntSaIS0_EE12_M_check_lenEmPKc.exit
	movq	(%r13), %rbx
	movq	%rbp, %rcx
	subq	%rbx, %rcx
	sarq	$3, %rcx
	movabsq	$-6148914691236517205, %r14 # imm = 0xAAAAAAAAAAAAAAAB
	imulq	%r14, %rcx
	cmpq	%rbx, %rbp
	movl	$1, %edx
	cmovneq	%rcx, %rdx
	leaq	(%rdx,%rcx), %rsi
	movabsq	$768614336404564650, %rax # imm = 0xAAAAAAAAAAAAAAA
	cmpq	%rax, %rsi
	cmovaq	%rax, %rsi
	addq	%rcx, %rdx
	cmovbq	%rax, %rsi
	testq	%rsi, %rsi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	je	.LBB14_25
# BB#26:
	cmpq	%rax, %rsi
	ja	.LBB14_63
# BB#27:                                # %_ZN9__gnu_cxx14__alloc_traitsISaI6BigIntEE8allocateERS2_m.exit.i
	leaq	(,%rsi,8), %rax
	leaq	(%rax,%rax,2), %rdi
	callq	_Znwm
	jmp	.LBB14_28
.LBB14_2:                               # %._crit_edge
	xorl	%r15d, %r15d
.LBB14_5:
	movq	%r15, (%rbp)
	movq	%r15, 8(%rbp)
	leaq	(%r15,%r14,8), %rax
	movq	%rax, 16(%rbp)
	subq	%rsi, %rdx
	movq	%rdx, %rbx
	sarq	$3, %rbx
	je	.LBB14_7
# BB#6:
	movq	%r15, %rdi
	callq	memmove
.LBB14_7:                               # %_ZN9__gnu_cxx14__alloc_traitsISaI6BigIntEE9constructIS1_EEvRS2_PS1_RKT_.exit
	leaq	(%r15,%rbx,8), %rax
	movq	%rax, 8(%rbp)
	addq	$24, 8(%r13)
	movq	(%r12), %rsi
	movq	8(%r12), %rdx
	movq	%rdx, %rdi
	subq	%rsi, %rdi
	movq	%rdi, %rbp
	sarq	$3, %rbp
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movq	$0, 32(%rsp)
	je	.LBB14_8
# BB#9:
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	cmpq	%rax, %rbp
	ja	.LBB14_63
# BB#10:                                # %_ZN9__gnu_cxx14__alloc_traitsISaImEE8allocateERS1_m.exit.i.i.i.i.i
	callq	_Znwm
	movq	%rax, %rbx
	movq	(%r12), %rsi
	movq	8(%r12), %rdx
	jmp	.LBB14_11
.LBB14_8:                               # %_ZN9__gnu_cxx14__alloc_traitsISaI6BigIntEE9constructIS1_EEvRS2_PS1_RKT_.exit._crit_edge
	xorl	%ebx, %ebx
.LBB14_11:
	movq	%rbx, 16(%rsp)
	movq	%rbx, 24(%rsp)
	leaq	(%rbx,%rbp,8), %rax
	movq	%rax, 32(%rsp)
	subq	%rsi, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB14_13
# BB#12:
	movq	%rbx, %rdi
	callq	memmove
.LBB14_13:                              # %_ZN6BigIntC2ERKS_.exit
	leaq	(%rbx,%rbp,8), %rax
	movq	%rax, 24(%rsp)
	movq	8(%r13), %r15
	leaq	-48(%r15), %rax
	subq	8(%rsp), %rax           # 8-byte Folded Reload
	testq	%rax, %rax
	jle	.LBB14_17
# BB#14:                                # %.lr.ph.preheader.i.i.i.i
	movabsq	$-6148914691236517205, %rcx # imm = 0xAAAAAAAAAAAAAAAB
	mulq	%rcx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	incq	%rbx
	addq	$-72, %r15
	.p2align	4, 0x90
.LBB14_15:                              # %.lr.ph.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	24(%r15), %rdi
.Ltmp224:
	movq	%r15, %rsi
	callq	_ZNSt6vectorImSaImEEaSERKS1_
.Ltmp225:
# BB#16:                                # %.noexc
                                        #   in Loop: Header=BB14_15 Depth=1
	decq	%rbx
	addq	$-24, %r15
	cmpq	$1, %rbx
	jg	.LBB14_15
.LBB14_17:                              # %_ZSt13copy_backwardIP6BigIntS1_ET0_T_S3_S2_.exit
.Ltmp227:
	leaq	16(%rsp), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZNSt6vectorImSaImEEaSERKS1_
.Ltmp228:
# BB#18:                                # %_ZN6BigIntaSERKS_.exit
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_47
# BB#19:
	callq	_ZdlPv
	jmp	.LBB14_47
.LBB14_25:
	xorl	%eax, %eax
.LBB14_28:                              # %_ZNSt12_Vector_baseI6BigIntSaIS0_EE11_M_allocateEm.exit
	movq	8(%rsp), %rcx           # 8-byte Reload
	subq	%rbx, %rcx
	sarq	$3, %rcx
	imulq	%r14, %rcx
	leaq	(%rcx,%rcx,2), %r15
	leaq	(%rax,%r15,8), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	(%r12), %rsi
	movq	8(%r12), %rdx
	movq	%rdx, %rdi
	subq	%rsi, %rdi
	movq	%rdi, %rbp
	sarq	$3, %rbp
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax,%r15,8)
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	$0, 16(%rax,%r15,8)
	je	.LBB14_29
# BB#30:
	movq	%r12, %rbx
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	cmpq	%rax, %rbp
	ja	.LBB14_31
# BB#33:                                # %_ZN9__gnu_cxx14__alloc_traitsISaImEE8allocateERS1_m.exit.i.i.i.i.i.i.i39
.Ltmp230:
	movq	(%rsp), %r14            # 8-byte Reload
	callq	_Znwm
	movq	%rax, %r12
.Ltmp231:
# BB#34:                                # %.noexc41
	movq	(%rbx), %rsi
	movq	8(%rbx), %rdx
	jmp	.LBB14_35
.LBB14_29:                              # %_ZNSt12_Vector_baseI6BigIntSaIS0_EE11_M_allocateEm.exit._crit_edge
	xorl	%r12d, %r12d
.LBB14_35:
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%r12, (%rax)
	movq	(%rsp), %rbx            # 8-byte Reload
	leaq	8(%rbx,%r15,8), %r14
	movq	%r12, 8(%rbx,%r15,8)
	leaq	(%r12,%rbp,8), %rax
	movq	%rax, 16(%rbx,%r15,8)
	subq	%rsi, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB14_37
# BB#36:
	movq	%r12, %rdi
	callq	memmove
.LBB14_37:
	leaq	(%r12,%rbp,8), %rax
	movq	%rax, (%r14)
	movq	(%r13), %rdi
	xorl	%r14d, %r14d
.Ltmp234:
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rbx, %rdx
	callq	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIP6BigIntS3_EET0_T_S5_S4_
.Ltmp235:
# BB#38:                                # %_ZSt34__uninitialized_move_if_noexcept_aIP6BigIntS1_SaIS0_EET0_T_S4_S3_RT1_.exit
	addq	$24, %rax
	movq	8(%r13), %rsi
.Ltmp236:
	movq	%rax, %r14
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, %rdx
	callq	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIP6BigIntS3_EET0_T_S5_S4_
	movq	%rax, %r12
.Ltmp237:
# BB#39:                                # %_ZSt34__uninitialized_move_if_noexcept_aIP6BigIntS1_SaIS0_EET0_T_S4_S3_RT1_.exit45
	movq	(%r13), %rbx
	movq	8(%r13), %rbp
	cmpq	%rbp, %rbx
	je	.LBB14_44
	.p2align	4, 0x90
.LBB14_40:                              # %.lr.ph.i.i.i48
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_42
# BB#41:                                #   in Loop: Header=BB14_40 Depth=1
	callq	_ZdlPv
.LBB14_42:                              # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i49
                                        #   in Loop: Header=BB14_40 Depth=1
	addq	$24, %rbx
	cmpq	%rbx, %rbp
	jne	.LBB14_40
# BB#43:                                # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit50thread-pre-split
	movq	(%r13), %rbx
.LBB14_44:                              # %_ZSt8_DestroyIP6BigIntS0_EvT_S2_RSaIT0_E.exit50
	testq	%rbx, %rbx
	je	.LBB14_46
# BB#45:
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB14_46:                              # %_ZNSt12_Vector_baseI6BigIntSaIS0_EE13_M_deallocateEPS0_m.exit46
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%r13)
	movq	%r12, 8(%r13)
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 16(%r13)
.LBB14_47:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_63:
	callq	_ZSt17__throw_bad_allocv
.LBB14_31:                              # %.noexc.i.i.i.i.i38
.Ltmp232:
	movq	(%rsp), %r14            # 8-byte Reload
	callq	_ZSt17__throw_bad_allocv
.Ltmp233:
# BB#32:                                # %.noexc40
.LBB14_21:                              # %.loopexit.split-lp
.Ltmp229:
	jmp	.LBB14_22
.LBB14_48:
.Ltmp238:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	testq	%r14, %r14
	je	.LBB14_49
# BB#51:
	cmpq	%r14, (%rsp)            # 8-byte Folded Reload
	je	.LBB14_56
# BB#52:                                # %.lr.ph.i.i.i.preheader
	movq	(%rsp), %rbx            # 8-byte Reload
	.p2align	4, 0x90
.LBB14_53:                              # %.lr.ph.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_55
# BB#54:                                #   in Loop: Header=BB14_53 Depth=1
	callq	_ZdlPv
.LBB14_55:                              # %_ZSt8_DestroyI6BigIntEvPT_.exit.i.i.i
                                        #   in Loop: Header=BB14_53 Depth=1
	addq	$24, %rbx
	cmpq	%rbx, %r14
	jne	.LBB14_53
.LBB14_56:                              # %_ZN9__gnu_cxx14__alloc_traitsISaI6BigIntEE7destroyERS2_PS1_.exit
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB14_58
.LBB14_57:                              # %_ZN9__gnu_cxx14__alloc_traitsISaI6BigIntEE7destroyERS2_PS1_.exit.thread
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZdlPv
.LBB14_58:                              # %_ZNSt12_Vector_baseI6BigIntSaIS0_EE13_M_deallocateEPS0_m.exit
.Ltmp239:
	callq	__cxa_rethrow
.Ltmp240:
# BB#62:
.LBB14_49:
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB14_57
# BB#50:
	callq	_ZdlPv
	jmp	.LBB14_57
.LBB14_59:
.Ltmp241:
	movq	%rax, %rbx
.Ltmp242:
	callq	__cxa_end_catch
.Ltmp243:
	jmp	.LBB14_60
.LBB14_61:
.Ltmp244:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB14_20:                              # %.loopexit
.Ltmp226:
.LBB14_22:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB14_23
.LBB14_60:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB14_23:
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZNSt6vectorI6BigIntSaIS0_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS0_S2_EERKS0_, .Lfunc_end14-_ZNSt6vectorI6BigIntSaIS0_EE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPS0_S2_EERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Lfunc_begin9-.Lfunc_begin9 # >> Call Site 1 <<
	.long	.Ltmp224-.Lfunc_begin9  #   Call between .Lfunc_begin9 and .Ltmp224
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp224-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp225-.Ltmp224       #   Call between .Ltmp224 and .Ltmp225
	.long	.Ltmp226-.Lfunc_begin9  #     jumps to .Ltmp226
	.byte	0                       #   On action: cleanup
	.long	.Ltmp227-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp228-.Ltmp227       #   Call between .Ltmp227 and .Ltmp228
	.long	.Ltmp229-.Lfunc_begin9  #     jumps to .Ltmp229
	.byte	0                       #   On action: cleanup
	.long	.Ltmp230-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Ltmp231-.Ltmp230       #   Call between .Ltmp230 and .Ltmp231
	.long	.Ltmp238-.Lfunc_begin9  #     jumps to .Ltmp238
	.byte	1                       #   On action: 1
	.long	.Ltmp231-.Lfunc_begin9  # >> Call Site 5 <<
	.long	.Ltmp234-.Ltmp231       #   Call between .Ltmp231 and .Ltmp234
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp234-.Lfunc_begin9  # >> Call Site 6 <<
	.long	.Ltmp237-.Ltmp234       #   Call between .Ltmp234 and .Ltmp237
	.long	.Ltmp238-.Lfunc_begin9  #     jumps to .Ltmp238
	.byte	1                       #   On action: 1
	.long	.Ltmp237-.Lfunc_begin9  # >> Call Site 7 <<
	.long	.Ltmp232-.Ltmp237       #   Call between .Ltmp237 and .Ltmp232
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp232-.Lfunc_begin9  # >> Call Site 8 <<
	.long	.Ltmp233-.Ltmp232       #   Call between .Ltmp232 and .Ltmp233
	.long	.Ltmp238-.Lfunc_begin9  #     jumps to .Ltmp238
	.byte	1                       #   On action: 1
	.long	.Ltmp233-.Lfunc_begin9  # >> Call Site 9 <<
	.long	.Ltmp239-.Ltmp233       #   Call between .Ltmp233 and .Ltmp239
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp239-.Lfunc_begin9  # >> Call Site 10 <<
	.long	.Ltmp240-.Ltmp239       #   Call between .Ltmp239 and .Ltmp240
	.long	.Ltmp241-.Lfunc_begin9  #     jumps to .Ltmp241
	.byte	0                       #   On action: cleanup
	.long	.Ltmp242-.Lfunc_begin9  # >> Call Site 11 <<
	.long	.Ltmp243-.Ltmp242       #   Call between .Ltmp242 and .Ltmp243
	.long	.Ltmp244-.Lfunc_begin9  #     jumps to .Ltmp244
	.byte	1                       #   On action: 1
	.long	.Ltmp243-.Lfunc_begin9  # >> Call Site 12 <<
	.long	.Lfunc_end14-.Ltmp243   #   Call between .Ltmp243 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNSt6vectorImSaImEEaSERKS1_,"axG",@progbits,_ZNSt6vectorImSaImEEaSERKS1_,comdat
	.weak	_ZNSt6vectorImSaImEEaSERKS1_
	.p2align	4, 0x90
	.type	_ZNSt6vectorImSaImEEaSERKS1_,@function
_ZNSt6vectorImSaImEEaSERKS1_:           # @_ZNSt6vectorImSaImEEaSERKS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi162:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi163:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi164:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi166:
	.cfi_def_cfa_offset 64
.Lcfi167:
	.cfi_offset %rbx, -56
.Lcfi168:
	.cfi_offset %r12, -48
.Lcfi169:
	.cfi_offset %r13, -40
.Lcfi170:
	.cfi_offset %r14, -32
.Lcfi171:
	.cfi_offset %r15, -24
.Lcfi172:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	cmpq	%r15, %rbx
	je	.LBB15_17
# BB#1:
	movq	(%rbx), %r14
	movq	8(%rbx), %rdx
	movq	%rdx, %r12
	subq	%r14, %r12
	movq	%r12, %r13
	sarq	$3, %r13
	movq	(%r15), %rdi
	movq	16(%r15), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rax, %r13
	jbe	.LBB15_8
# BB#2:
	movq	%r13, %rax
	shrq	$61, %rax
	jne	.LBB15_18
# BB#3:                                 # %_ZNSt12_Vector_baseImSaImEE11_M_allocateEm.exit.i
	movq	%r12, %rdi
	callq	_Znwm
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.LBB15_5
# BB#4:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	memcpy
.LBB15_5:                               # %_ZNSt6vectorImSaImEE20_M_allocate_and_copyIN9__gnu_cxx17__normal_iteratorIPKmS1_EEEEPmmT_S9_.exit
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB15_7
# BB#6:
	callq	_ZdlPv
.LBB15_7:                               # %_ZNSt12_Vector_baseImSaImEE13_M_deallocateEPmm.exit
	movq	%rbx, (%r15)
	leaq	(%rbx,%r13,8), %rax
	movq	%rax, 16(%r15)
	leaq	8(%r15), %rbp
	jmp	.LBB15_16
.LBB15_8:
	leaq	8(%r15), %rbp
	movq	8(%r15), %rax
	movq	%rax, %rcx
	subq	%rdi, %rcx
	movq	%rcx, %rsi
	sarq	$3, %rsi
	cmpq	%r13, %rsi
	jae	.LBB15_9
# BB#11:
	testq	%rcx, %rcx
	je	.LBB15_13
# BB#12:
	movq	%r14, %rsi
	movq	%rcx, %rdx
	callq	memmove
	movq	(%rbx), %r14
	movq	8(%rbx), %rdx
	movq	(%r15), %rdi
	movq	8(%r15), %rax
.LBB15_13:                              # %_ZSt4copyIPmS0_ET0_T_S2_S1_.exit
	movq	%rax, %rsi
	subq	%rdi, %rsi
	addq	%r14, %rsi
	subq	%rsi, %rdx
	je	.LBB15_16
# BB#14:
	movq	%rax, %rdi
	jmp	.LBB15_15
.LBB15_9:
	testq	%r12, %r12
	je	.LBB15_16
# BB#10:
	movq	%r14, %rsi
	movq	%r12, %rdx
.LBB15_15:                              # %_ZSt22__uninitialized_copy_aIPmS0_mET0_T_S2_S1_RSaIT1_E.exit
	callq	memmove
.LBB15_16:                              # %_ZSt22__uninitialized_copy_aIPmS0_mET0_T_S2_S1_RSaIT1_E.exit
	shlq	$3, %r13
	addq	(%r15), %r13
	movq	%r13, (%rbp)
.LBB15_17:
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_18:
	callq	_ZSt17__throw_bad_allocv
.Lfunc_end15:
	.size	_ZNSt6vectorImSaImEEaSERKS1_, .Lfunc_end15-_ZNSt6vectorImSaImEEaSERKS1_
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_bigfib.ii,@function
_GLOBAL__sub_I_bigfib.ii:               # @_GLOBAL__sub_I_bigfib.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi173:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end16:
	.size	_GLOBAL__sub_I_bigfib.ii, .Lfunc_end16-_GLOBAL__sub_I_bigfib.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Fib ["
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"] = "
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n"
	.size	.L.str.2, 2

	.type	_ZN6BigInt6head_sE,@object # @_ZN6BigInt6head_sE
	.bss
	.globl	_ZN6BigInt6head_sE
	.p2align	3
_ZN6BigInt6head_sE:
	.quad	0                       # 0x0
	.size	_ZN6BigInt6head_sE, 8

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"bigfib"
	.size	.L.str.3, 7

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"USAGE : "
	.size	.L.str.4, 9

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"  "
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	" "
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"all"
	.size	.L.str.7, 4

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" <N>              ---> Fibonacci [0 - N]"
	.size	.L.str.8, 41

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"th"
	.size	.L.str.9, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	" <N>              ---> Fibonacci [N]"
	.size	.L.str.10, 37

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"some"
	.size	.L.str.11, 5

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	" <N1> [<N2> ...]  ---> Fibonacci [N1], Fibonacci [N2], ..."
	.size	.L.str.12, 59

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"rand"
	.size	.L.str.13, 5

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	" <K>  [<M>]       ---> K random Fibonacci numbers ( < M; Default = "
	.size	.L.str.14, 68

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	" )"
	.size	.L.str.15, 3

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"vector::_M_fill_insert"
	.size	.L.str.17, 23

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"vector::reserve"
	.size	.L.str.18, 16

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"basic_string::_M_construct null not valid"
	.size	.L.str.19, 42

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_bigfib.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
