	.text
	.file	"init.bc"
	.globl	init__safe_count
	.p2align	4, 0x90
	.type	init__safe_count,@function
init__safe_count:                       # @init__safe_count
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movslq	%edi, %r14
	leaq	(%r14,%r14,2), %rax
	movl	$0, g_info_totals(,%rax,4)
	cmpl	$0, g_board_size(,%r14,4)
	jle	.LBB0_3
# BB#1:                                 # %.lr.ph.preheader
	shlq	$7, %rax
	leaq	g_info+12(%rax), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbx
	movl	$0, (%r15)
	movl	%r14d, %edi
	movl	%ebx, %esi
	callq	update_safe
	movslq	g_board_size(,%r14,4), %rax
	addq	$12, %r15
	cmpq	%rax, %rbx
	jl	.LBB0_2
.LBB0_3:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	init__safe_count, .Lfunc_end0-init__safe_count
	.cfi_endproc

	.globl	init__real_count
	.p2align	4, 0x90
	.type	init__real_count,@function
init__real_count:                       # @init__real_count
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movslq	%edi, %r14
	leaq	(%r14,%r14,2), %rax
	movl	$0, g_info_totals+4(,%rax,4)
	cmpl	$0, g_board_size(,%r14,4)
	jle	.LBB1_3
# BB#1:                                 # %.lr.ph.preheader
	shlq	$7, %rax
	leaq	g_info+16(%rax), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbx
	movl	$0, (%r15)
	movl	%r14d, %edi
	movl	%ebx, %esi
	callq	update_real
	movslq	g_board_size(,%r14,4), %rax
	addq	$12, %r15
	cmpq	%rax, %rbx
	jl	.LBB1_2
.LBB1_3:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	init__real_count, .Lfunc_end1-init__real_count
	.cfi_endproc

	.globl	initialize_solver
	.p2align	4, 0x90
	.type	initialize_solver,@function
initialize_solver:                      # @initialize_solver
	.cfi_startproc
# BB#0:
	cmpq	$0, g_trans_table(%rip)
	jne	.LBB2_6
# BB#1:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -40
.Lcfi18:
	.cfi_offset %r12, -32
.Lcfi19:
	.cfi_offset %r14, -24
.Lcfi20:
	.cfi_offset %r15, -16
	movl	$8388608, %edi          # imm = 0x800000
	movl	$24, %esi
	callq	calloc
	movq	%rax, g_trans_table(%rip)
	movl	$1, %edi
	callq	srandom
	movl	$1, %r14d
	movl	$g_zobrist+132, %r15d
	.p2align	4, 0x90
.LBB2_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
	movl	$30, %r12d
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB2_3:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	random
	andl	$8388607, %eax          # imm = 0x7FFFFF
	movl	%eax, (%rbx)
	addq	$4, %rbx
	decq	%r12
	jne	.LBB2_3
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	incq	%r14
	subq	$-128, %r15
	cmpq	$31, %r14
	jne	.LBB2_2
# BB#5:
	xorl	%eax, %eax
	callq	init_static_tables
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
.LBB2_6:
	xorl	%eax, %eax
	jmp	init_less_static_tables # TAILCALL
.Lfunc_end2:
	.size	initialize_solver, .Lfunc_end2-initialize_solver
	.cfi_endproc

	.globl	initialize_board
	.p2align	4, 0x90
	.type	initialize_board,@function
initialize_board:                       # @initialize_board
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 96
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movl	%esi, %ebx
	movl	%edi, %r13d
	leal	-1(%r13), %r14d
	leal	-1(%rbx), %eax
	cmpl	$29, %r14d
	movl	%eax, 36(%rsp)          # 4-byte Spill
	ja	.LBB3_2
# BB#1:
	cmpl	$30, %eax
	jb	.LBB3_3
.LBB3_2:
	movl	$.L.str, %edi
	movl	$102, %esi
	movl	$1, %edx
	movl	$.L.str.1, %ecx
	xorl	%eax, %eax
	movl	%r13d, %r8d
	movl	%ebx, %r9d
	callq	_fatal_error_aux
.LBB3_3:
	movl	%ebx, %r15d
	imull	%r13d, %r15d
	cmpl	$128, %r15d
	jl	.LBB3_5
# BB#4:
	movl	$.L.str, %edi
	movl	$105, %esi
	movl	$1, %edx
	movl	$.L.str.1, %ecx
	xorl	%eax, %eax
	movl	%r13d, %r8d
	movl	%ebx, %r9d
	callq	_fatal_error_aux
.LBB3_5:
	movq	g_trans_table(%rip), %rax
	movl	g_board_size(%rip), %edx
	movl	g_board_size+4(%rip), %ecx
	movl	%r13d, g_board_size(%rip)
	movl	%ebx, g_board_size+4(%rip)
	testq	%rax, %rax
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	je	.LBB3_42
# BB#6:
	cmpl	%r13d, %edx
	jne	.LBB3_42
# BB#7:
	cmpl	%ebx, %ecx
	je	.LBB3_8
.LBB3_42:                               # %.critedge
	testq	%rax, %rax
	jne	.LBB3_48
# BB#43:
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movl	$8388608, %edi          # imm = 0x800000
	movl	$24, %esi
	callq	calloc
	movq	%rax, g_trans_table(%rip)
	movl	$1, %edi
	callq	srandom
	movl	$1, %r12d
	movl	$g_zobrist+132, %ebx
	.p2align	4, 0x90
.LBB3_44:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_45 Depth 2
	movq	%rbx, %rbp
	movl	$30, %r13d
	.p2align	4, 0x90
.LBB3_45:                               #   Parent Loop BB3_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	random
	andl	$8388607, %eax          # imm = 0x7FFFFF
	movl	%eax, (%rbp)
	addq	$4, %rbp
	decq	%r13
	jne	.LBB3_45
# BB#46:                                #   in Loop: Header=BB3_44 Depth=1
	incq	%r12
	subq	$-128, %rbx
	cmpq	$31, %r12
	jne	.LBB3_44
# BB#47:
	xorl	%eax, %eax
	callq	init_static_tables
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB3_48:                               # %initialize_solver.exit
	xorl	%eax, %eax
	callq	init_less_static_tables
.LBB3_8:                                # %.preheader149.preheader
	pcmpeqd	%xmm0, %xmm0
	movdqa	%xmm0, g_board+240(%rip)
	movdqa	%xmm0, g_board+224(%rip)
	movdqa	%xmm0, g_board+208(%rip)
	movdqa	%xmm0, g_board+192(%rip)
	movdqa	%xmm0, g_board+176(%rip)
	movdqa	%xmm0, g_board+160(%rip)
	movdqa	%xmm0, g_board+144(%rip)
	movdqa	%xmm0, g_board+128(%rip)
	movdqa	%xmm0, g_board+112(%rip)
	movdqa	%xmm0, g_board+96(%rip)
	movdqa	%xmm0, g_board+80(%rip)
	movdqa	%xmm0, g_board+64(%rip)
	movdqa	%xmm0, g_board+48(%rip)
	movdqa	%xmm0, g_board+32(%rip)
	movdqa	%xmm0, g_board+16(%rip)
	movdqa	%xmm0, g_board(%rip)
	testl	%r13d, %r13d
	jle	.LBB3_16
# BB#9:                                 # %.preheader149.preheader
	testl	%ebx, %ebx
	jle	.LBB3_16
# BB#10:                                # %.preheader147.us.preheader
	movl	%r13d, %r8d
	movl	%ebx, %esi
	xorl	%r11d, %r11d
	movq	%r12, %r10
	.p2align	4, 0x90
.LBB3_11:                               # %.preheader147.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_12 Depth 2
	leaq	1(%r11), %r9
	movl	$-2, %ebx
	movl	%r9d, %ecx
	roll	%cl, %ebx
	movq	%r10, %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_12:                               #   Parent Loop BB3_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%rbp), %rdx
	cmpl	$0, (%rdi)
	jne	.LBB3_14
# BB#13:                                #   in Loop: Header=BB3_12 Depth=2
	leal	1(%rbp), %ecx
	movl	$-2, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	roll	%cl, %eax
	andl	%eax, g_board+4(,%r11,4)
	andl	%ebx, g_board+132(,%rbp,4)
.LBB3_14:                               # %._crit_edge
                                        #   in Loop: Header=BB3_12 Depth=2
	addq	$4, %rdi
	cmpq	%rdx, %rsi
	movq	%rdx, %rbp
	jne	.LBB3_12
# BB#15:                                # %._crit_edge159.us
                                        #   in Loop: Header=BB3_11 Depth=1
	addq	$120, %r10
	cmpq	%r8, %r9
	movq	%r9, %r11
	jne	.LBB3_11
.LBB3_16:                               # %._crit_edge161
	movl	$0, g_info_totals+16(%rip)
	movl	g_board_size+4(%rip), %eax
	testl	%eax, %eax
	jle	.LBB3_19
# BB#17:                                # %.lr.ph.i.preheader
	xorl	%ebx, %ebx
	movl	$g_info+400, %ebp
	.p2align	4, 0x90
.LBB3_18:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbx
	movl	$0, (%rbp)
	movl	$1, %edi
	movl	%ebx, %esi
	callq	update_real
	movslq	g_board_size+4(%rip), %rax
	addq	$12, %rbp
	cmpq	%rax, %rbx
	jl	.LBB3_18
.LBB3_19:                               # %init__real_count.exit
	movl	$0, g_info_totals+4(%rip)
	movl	g_board_size(%rip), %r10d
	testl	%r10d, %r10d
	jle	.LBB3_23
# BB#20:                                # %.lr.ph.i93.preheader
	xorl	%ebx, %ebx
	movl	$g_info+16, %ebp
	.p2align	4, 0x90
.LBB3_21:                               # %.lr.ph.i93
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbx
	movl	$0, (%rbp)
	xorl	%edi, %edi
	movl	%ebx, %esi
	callq	update_real
	movslq	g_board_size(%rip), %r10
	addq	$12, %rbp
	cmpq	%r10, %rbx
	jl	.LBB3_21
# BB#22:                                # %init__real_count.exit94.loopexit
	movl	g_board_size+4(%rip), %eax
.LBB3_23:                               # %init__real_count.exit94
	movl	$0, g_info_totals+12(%rip)
	testl	%eax, %eax
	jle	.LBB3_27
# BB#24:                                # %.lr.ph.i97.preheader
	xorl	%ebx, %ebx
	movl	$g_info+396, %ebp
	.p2align	4, 0x90
.LBB3_25:                               # %.lr.ph.i97
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbx
	movl	$0, (%rbp)
	movl	$1, %edi
	movl	%ebx, %esi
	callq	update_safe
	movslq	g_board_size+4(%rip), %rax
	addq	$12, %rbp
	cmpq	%rax, %rbx
	jl	.LBB3_25
# BB#26:                                # %init__safe_count.exit.loopexit
	movl	g_board_size(%rip), %r10d
.LBB3_27:                               # %init__safe_count.exit
	movl	$0, g_info_totals(%rip)
	testl	%r10d, %r10d
	jle	.LBB3_30
# BB#28:                                # %.lr.ph.i100.preheader
	xorl	%ebx, %ebx
	movl	$g_info+12, %ebp
	.p2align	4, 0x90
.LBB3_29:                               # %.lr.ph.i100
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbx
	movl	$0, (%rbp)
	xorl	%edi, %edi
	movl	%ebx, %esi
	callq	update_safe
	movslq	g_board_size(%rip), %r10
	addq	$12, %rbp
	cmpq	%r10, %rbx
	jl	.LBB3_29
.LBB3_30:                               # %init__safe_count.exit101.preheader
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, g_norm_hashkey(%rip)
	movdqu	%xmm0, g_flipV_hashkey(%rip)
	movdqu	%xmm0, g_flipH_hashkey(%rip)
	movdqu	%xmm0, g_flipVH_hashkey(%rip)
	testl	%r13d, %r13d
	movl	36(%rsp), %ecx          # 4-byte Reload
	jle	.LBB3_38
# BB#31:                                # %init__safe_count.exit101.preheader
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB3_38
# BB#32:                                # %.preheader.us.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %edx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	%r13d, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	imull	%eax, %r14d
	decl	%r15d
	xorl	%r11d, %r11d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_33:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_34 Depth 2
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%r12, %r8
	movl	%r11d, %eax
	movl	%ecx, %r9d
	movl	%ecx, %edx
	movl	%r15d, %esi
	movl	%r14d, %edi
	.p2align	4, 0x90
.LBB3_34:                               #   Parent Loop BB3_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%r12)
	je	.LBB3_36
# BB#35:                                #   in Loop: Header=BB3_34 Depth=2
	movl	$1, %ebp
	movl	%eax, %ecx
	shll	%cl, %ebp
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$27, %ecx
	addl	%eax, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%ebp, g_norm_hashkey(,%rcx,4)
	movl	$1, %ebp
	movl	%edx, %ecx
	shll	%cl, %ebp
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$27, %ecx
	addl	%edx, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%ebp, g_flipV_hashkey(,%rcx,4)
	movl	$1, %ebp
	movl	%edi, %ecx
	shll	%cl, %ebp
	movl	%edi, %ecx
	sarl	$31, %ecx
	shrl	$27, %ecx
	addl	%edi, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%ebp, g_flipH_hashkey(,%rcx,4)
	movl	$1, %ebp
	movl	%esi, %ecx
	shll	%cl, %ebp
	movl	%esi, %ecx
	sarl	$31, %ecx
	shrl	$27, %ecx
	addl	%esi, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%ebp, g_flipVH_hashkey(,%rcx,4)
.LBB3_36:                               #   in Loop: Header=BB3_34 Depth=2
	incl	%edi
	decl	%esi
	decl	%edx
	incl	%eax
	addq	$4, %r12
	decq	%rbx
	jne	.LBB3_34
# BB#37:                                # %._crit_edge.us
                                        #   in Loop: Header=BB3_33 Depth=1
	incq	%r13
	movq	8(%rsp), %rax           # 8-byte Reload
	subl	%eax, %r14d
	subl	%eax, %r15d
	movl	%r9d, %ecx
	addl	%eax, %ecx
	addl	%eax, %r11d
	movq	%r8, %r12
	addq	$120, %r12
	cmpq	16(%rsp), %r13          # 8-byte Folded Reload
	jne	.LBB3_33
.LBB3_38:                               # %._crit_edge155
	movl	g_board_size+4(%rip), %r11d
	movl	$0, g_norm_hashkey+16(%rip)
	testl	%r10d, %r10d
	setg	%al
	testl	%r11d, %r11d
	setg	%r8b
	andb	%al, %r8b
	je	.LBB3_39
# BB#49:                                # %.preheader.us.preheader.i
	movl	%r10d, %r9d
	xorl	%esi, %esi
	movl	$g_zobrist+132, %edx
	xorl	%ebp, %ebp
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB3_50:                               # %.preheader.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_51 Depth 2
	incq	%r10
	movq	%rdx, %rdi
	movq	%r11, %rax
	movl	%esi, %ebx
	.p2align	4, 0x90
.LBB3_51:                               #   Parent Loop BB3_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$27, %ecx
	addl	%ebx, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	g_norm_hashkey(,%rcx,4), %ecx
	btl	%ebx, %ecx
	jae	.LBB3_53
# BB#52:                                #   in Loop: Header=BB3_51 Depth=2
	xorl	(%rdi), %ebp
	movl	%ebp, g_norm_hashkey+16(%rip)
.LBB3_53:                               # %._crit_edge.i
                                        #   in Loop: Header=BB3_51 Depth=2
	incl	%ebx
	addq	$4, %rdi
	decq	%rax
	jne	.LBB3_51
# BB#54:                                # %._crit_edge.us.i
                                        #   in Loop: Header=BB3_50 Depth=1
	addl	%r11d, %esi
	subq	$-128, %rdx
	cmpq	%r9, %r10
	jne	.LBB3_50
# BB#55:                                # %init_hashkey_code.exit
	movl	$0, g_flipV_hashkey+16(%rip)
	testb	$1, %r8b
	je	.LBB3_40
# BB#56:                                # %.preheader.us.preheader.i107
	xorl	%esi, %esi
	movl	$g_zobrist+132, %edi
	xorl	%ebp, %ebp
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB3_57:                               # %.preheader.us.i110
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_58 Depth 2
	incq	%r10
	movq	%rdi, %rdx
	movq	%r11, %rax
	movl	%esi, %ebx
	.p2align	4, 0x90
.LBB3_58:                               #   Parent Loop BB3_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$27, %ecx
	addl	%ebx, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	g_flipV_hashkey(,%rcx,4), %ecx
	btl	%ebx, %ecx
	jae	.LBB3_60
# BB#59:                                #   in Loop: Header=BB3_58 Depth=2
	xorl	(%rdx), %ebp
	movl	%ebp, g_flipV_hashkey+16(%rip)
.LBB3_60:                               # %._crit_edge.i114
                                        #   in Loop: Header=BB3_58 Depth=2
	incl	%ebx
	addq	$4, %rdx
	decq	%rax
	jne	.LBB3_58
# BB#61:                                # %._crit_edge.us.i116
                                        #   in Loop: Header=BB3_57 Depth=1
	addl	%r11d, %esi
	subq	$-128, %rdi
	cmpq	%r9, %r10
	jne	.LBB3_57
# BB#62:                                # %init_hashkey_code.exit117
	movl	$0, g_flipH_hashkey+16(%rip)
	testb	$1, %r8b
	je	.LBB3_41
# BB#63:                                # %.preheader.us.preheader.i121
	xorl	%esi, %esi
	movl	$g_zobrist+132, %edi
	xorl	%ebp, %ebp
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB3_64:                               # %.preheader.us.i124
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_65 Depth 2
	incq	%r10
	movq	%rdi, %rdx
	movq	%r11, %rax
	movl	%esi, %ebx
	.p2align	4, 0x90
.LBB3_65:                               #   Parent Loop BB3_64 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$27, %ecx
	addl	%ebx, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	g_flipH_hashkey(,%rcx,4), %ecx
	btl	%ebx, %ecx
	jae	.LBB3_67
# BB#66:                                #   in Loop: Header=BB3_65 Depth=2
	xorl	(%rdx), %ebp
	movl	%ebp, g_flipH_hashkey+16(%rip)
.LBB3_67:                               # %._crit_edge.i128
                                        #   in Loop: Header=BB3_65 Depth=2
	incl	%ebx
	addq	$4, %rdx
	decq	%rax
	jne	.LBB3_65
# BB#68:                                # %._crit_edge.us.i130
                                        #   in Loop: Header=BB3_64 Depth=1
	addl	%r11d, %esi
	subq	$-128, %rdi
	cmpq	%r9, %r10
	jne	.LBB3_64
# BB#69:                                # %init_hashkey_code.exit131
	movl	$0, g_flipVH_hashkey+16(%rip)
	testb	$1, %r8b
	je	.LBB3_76
# BB#70:                                # %.preheader.us.preheader.i135
	xorl	%edx, %edx
	movl	$g_zobrist+132, %ebx
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB3_71:                               # %.preheader.us.i138
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_72 Depth 2
	incq	%r8
	movq	%rbx, %rsi
	movq	%r11, %rax
	movl	%edx, %ebp
	.p2align	4, 0x90
.LBB3_72:                               #   Parent Loop BB3_71 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %ecx
	sarl	$31, %ecx
	shrl	$27, %ecx
	addl	%ebp, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	g_flipVH_hashkey(,%rcx,4), %ecx
	btl	%ebp, %ecx
	jae	.LBB3_74
# BB#73:                                #   in Loop: Header=BB3_72 Depth=2
	xorl	(%rsi), %edi
	movl	%edi, g_flipVH_hashkey+16(%rip)
.LBB3_74:                               # %._crit_edge.i142
                                        #   in Loop: Header=BB3_72 Depth=2
	incl	%ebp
	addq	$4, %rsi
	decq	%rax
	jne	.LBB3_72
# BB#75:                                # %._crit_edge.us.i144
                                        #   in Loop: Header=BB3_71 Depth=1
	addl	%r11d, %edx
	subq	$-128, %rbx
	cmpq	%r9, %r8
	jne	.LBB3_71
	jmp	.LBB3_76
.LBB3_39:                               # %init_hashkey_code.exit.thread
	movl	$0, g_flipV_hashkey+16(%rip)
.LBB3_40:                               # %init_hashkey_code.exit117.thread
	movl	$0, g_flipH_hashkey+16(%rip)
.LBB3_41:                               # %init_hashkey_code.exit131.thread
	movl	$0, g_flipVH_hashkey+16(%rip)
.LBB3_76:                               # %init_hashkey_code.exit145
	xorl	%edi, %edi
	callq	print_board
	movl	$10, %edi
	callq	putchar
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	print_board_info
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	check_hash_code_sanity  # TAILCALL
.Lfunc_end3:
	.size	initialize_board, .Lfunc_end3-initialize_board
	.cfi_endproc

	.type	g_board_size,@object    # @g_board_size
	.data
	.globl	g_board_size
	.p2align	2
g_board_size:
	.zero	8,255
	.size	g_board_size, 8

	.type	g_trans_table,@object   # @g_trans_table
	.bss
	.globl	g_trans_table
	.p2align	3
g_trans_table:
	.quad	0
	.size	g_trans_table, 8

	.type	g_info_totals,@object   # @g_info_totals
	.comm	g_info_totals,24,16
	.type	g_info,@object          # @g_info
	.comm	g_info,768,16
	.type	g_zobrist,@object       # @g_zobrist
	.comm	g_zobrist,4096,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/obsequi/init.c"
	.size	.L.str, 78

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Invalid board size %dX%d.\n"
	.size	.L.str.1, 27

	.type	g_board,@object         # @g_board
	.comm	g_board,256,16
	.type	norm_hashkey,@object    # @norm_hashkey
	.comm	norm_hashkey,20,4
	.type	flipV_hashkey,@object   # @flipV_hashkey
	.comm	flipV_hashkey,20,4
	.type	flipH_hashkey,@object   # @flipH_hashkey
	.comm	flipH_hashkey,20,4
	.type	flipVH_hashkey,@object  # @flipVH_hashkey
	.comm	flipVH_hashkey,20,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
