	.text
	.file	"shared_cdiff.bc"
	.globl	cdiff_apply
	.p2align	4, 0x90
	.type	cdiff_apply,@function
cdiff_apply:                            # @cdiff_apply
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$9432, %rsp             # imm = 0x24D8
.Lcfi6:
	.cfi_def_cfa_offset 9488
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movl	%edi, %ebp
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movq	$0, 48(%rsp)
	callq	dup
	movl	%eax, %r15d
	cmpl	$-1, %r15d
	je	.LBB0_12
# BB#1:
	movzwl	%bx, %eax
	cmpl	$1, %eax
	jne	.LBB0_13
# BB#2:
	movq	$-350, %rsi             # imm = 0xFEA2
	movl	$2, %edx
	movl	%r15d, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_22
# BB#3:
	leaq	208(%rsp), %rbx
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	movq	%rbx, %rdi
	callq	memset
	movl	$350, %edx              # imm = 0x15E
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	read
	cmpq	$350, %rax              # imm = 0x15E
	jne	.LBB0_25
# BB#4:                                 # %.preheader98.preheader
	movl	$349, %ebx              # imm = 0x15D
	movl	$345, %eax              # imm = 0x159
	.p2align	4, 0x90
.LBB0_5:                                # %.preheader98
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$58, 212(%rsp,%rax)
	je	.LBB0_37
# BB#6:                                 # %.preheader98.1225
                                        #   in Loop: Header=BB0_5 Depth=1
	cmpb	$58, 211(%rsp,%rax)
	je	.LBB0_33
# BB#7:                                 # %.preheader98.2226
                                        #   in Loop: Header=BB0_5 Depth=1
	cmpb	$58, 210(%rsp,%rax)
	je	.LBB0_34
# BB#8:                                 # %.preheader98.3227
                                        #   in Loop: Header=BB0_5 Depth=1
	cmpb	$58, 209(%rsp,%rax)
	je	.LBB0_35
# BB#9:                                 # %.preheader98.4228
                                        #   in Loop: Header=BB0_5 Depth=1
	cmpb	$58, 208(%rsp,%rax)
	je	.LBB0_36
# BB#10:                                #   in Loop: Header=BB0_5 Depth=1
	addq	$-5, %rbx
	leaq	-5(%rax), %rcx
	addq	$4, %rax
	cmpq	$4, %rax
	movq	%rcx, %rax
	jg	.LBB0_5
# BB#11:                                # %.thread
	movl	$.L.str.3, %edi
	jmp	.LBB0_66
.LBB0_12:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	logg
	jmp	.LBB0_68
.LBB0_13:
	movl	$.L.str.13, %esi
	movl	%r15d, %edi
	callq	fdopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_23
# BB#14:                                # %.preheader99
	leaq	208(%rsp), %rdi
	movl	$1024, %esi             # imm = 0x400
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB0_28
# BB#15:                                # %.lr.ph126.preheader
	xorl	%ebx, %ebx
	leaq	208(%rsp), %rbp
	leaq	16(%rsp), %r12
	xorl	%r15d, %r15d
.LBB0_16:                               # %.lr.ph126
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_17 Depth 2
	incl	%ebx
	movl	%ebx, %ecx
	.p2align	4, 0x90
.LBB0_17:                               #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %ebx
	movq	%rbp, %rdi
	callq	cli_chomp
	cmpb	$35, 208(%rsp)
	je	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_17 Depth=2
	movq	%rbp, %rdi
	callq	strlen
	testq	%rax, %rax
	jne	.LBB0_20
.LBB0_19:                               # %.backedge
                                        #   in Loop: Header=BB0_17 Depth=2
	movl	$1024, %esi             # imm = 0x400
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	fgets
	leal	1(%rbx), %ecx
	testq	%rax, %rax
	jne	.LBB0_17
	jmp	.LBB0_29
.LBB0_20:                               #   in Loop: Header=BB0_16 Depth=1
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	cdiff_execute
	cmpl	$-1, %eax
	je	.LBB0_71
# BB#21:                                # %.outer
                                        #   in Loop: Header=BB0_16 Depth=1
	incl	%r15d
	movl	$1024, %esi             # imm = 0x400
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB0_16
	jmp	.LBB0_29
.LBB0_22:
	movl	$.L.str.1, %edi
	movl	$-350, %esi             # imm = 0xFEA2
	jmp	.LBB0_26
.LBB0_23:
	movl	$.L.str.14, %edi
.LBB0_24:
	xorl	%eax, %eax
	movl	%r15d, %esi
	jmp	.LBB0_27
.LBB0_25:
	movl	$.L.str.2, %edi
	movl	$350, %esi              # imm = 0x15E
.LBB0_26:
	xorl	%eax, %eax
.LBB0_27:
	callq	logg
	jmp	.LBB0_67
.LBB0_28:
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
.LBB0_29:                               # %.outer._crit_edge
	movq	%r14, %rdi
	callq	fclose
.LBB0_30:
	movq	16(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB0_32
# BB#31:
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	logg
	leaq	16(%rsp), %rdi
	callq	cdiff_ctx_free
	jmp	.LBB0_68
.LBB0_32:
	xorl	%ebp, %ebp
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	%r15d, %edx
	callq	logg
	jmp	.LBB0_69
.LBB0_33:
	decq	%rbx
	jmp	.LBB0_37
.LBB0_34:
	addq	$-2, %rbx
	jmp	.LBB0_37
.LBB0_35:
	addq	$-3, %rbx
	jmp	.LBB0_37
.LBB0_36:
	movq	%rax, %rbx
.LBB0_37:
	leaq	64(%rsp), %rdx
	movl	$1, %edi
	movl	%r15d, %esi
	callq	__fxstat
	cmpl	$-1, %eax
	je	.LBB0_49
# BB#38:
	addl	112(%rsp), %ebx
	addl	$-350, %ebx             # imm = 0xFEA2
	js	.LBB0_51
# BB#39:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r15d, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_50
# BB#40:
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r15d, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_50
# BB#41:                                # %.preheader.preheader
	xorl	%r12d, %r12d
.LBB0_42:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_43 Depth 2
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	movq	%rbx, (%rsp)            # 8-byte Spill
	movslq	%ebx, %r13
	leaq	1232(%rsp,%r13), %r14
	incq	%r13
	movl	%r12d, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_43:                               #   Parent Loop BB0_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r14,%rbp), %rbx
	movl	$1, %edx
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	read
	testq	%rax, %rax
	jle	.LBB0_52
# BB#44:                                #   in Loop: Header=BB0_43 Depth=2
	cmpb	$58, (%rbx)
	jne	.LBB0_47
# BB#45:                                #   in Loop: Header=BB0_43 Depth=2
	leal	(%r12,%rbp), %ecx
	leaq	1(%rbp), %rax
	cmpl	$2, %ecx
	je	.LBB0_53
# BB#46:                                #   in Loop: Header=BB0_43 Depth=2
	addq	%r13, %rbp
	cmpq	$8191, %rbp             # imm = 0x1FFF
	movq	%rax, %rbp
	jl	.LBB0_43
	jmp	.LBB0_53
.LBB0_47:                               # %.loopexit
                                        #   in Loop: Header=BB0_42 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	leal	1(%rax,%rbp), %ebx
	addl	%ebp, %r12d
	cmpl	$8190, %ebx             # imm = 0x1FFE
	jle	.LBB0_42
# BB#48:                                # %.critedge.loopexit204
	leal	1(%rax,%rbp), %eax
	jmp	.LBB0_54
.LBB0_49:
	movl	$.L.str.4, %edi
	jmp	.LBB0_66
.LBB0_50:
	movl	$.L.str.6, %edi
	jmp	.LBB0_66
.LBB0_51:
	movl	$.L.str.5, %edi
	jmp	.LBB0_66
.LBB0_52:                               # %..critedge.loopexit_crit_edge
	movq	(%rsp), %rax            # 8-byte Reload
	addl	%ebp, %eax
	jmp	.LBB0_54
.LBB0_53:                               # %.critedge.loopexitsplit
	movq	(%rsp), %rcx            # 8-byte Reload
	addl	%eax, %ecx
	movq	%rcx, %rax
.LBB0_54:                               # %.critedge
	cltq
	movb	$0, 1232(%rsp,%rax)
	leaq	1232(%rsp), %rdi
	leaq	12(%rsp), %rdx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB0_65
# BB#55:
	movl	$.L.str.9, %esi
	movl	%r15d, %edi
	callq	gzdopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_70
# BB#56:
	movl	12(%rsp), %ebp
	testl	%ebp, %ebp
	je	.LBB0_72
# BB#57:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	movl	$1024, %r12d            # imm = 0x400
	leaq	208(%rsp), %r13
	xorl	%r15d, %r15d
.LBB0_58:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_59 Depth 2
	incl	%ebx
	movl	%ebx, %eax
.LBB0_59:                               #   Parent Loop BB0_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebx
	leal	1(%rbp), %edx
	cmpl	$1024, %ebp             # imm = 0x400
	cmovael	%r12d, %edx
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	gzgets
	testq	%rax, %rax
	je	.LBB0_74
# BB#60:                                #   in Loop: Header=BB0_59 Depth=2
	movq	%r13, %rdi
	callq	strlen
	subl	%eax, %ebp
	movq	%r13, %rdi
	callq	cli_chomp
	cmpb	$35, 208(%rsp)
	je	.LBB0_62
# BB#61:                                #   in Loop: Header=BB0_59 Depth=2
	movq	%r13, %rdi
	callq	strlen
	testq	%rax, %rax
	jne	.LBB0_63
.LBB0_62:                               # %.thread95.backedge
                                        #   in Loop: Header=BB0_59 Depth=2
	leal	1(%rbx), %eax
	testl	%ebp, %ebp
	jne	.LBB0_59
	jmp	.LBB0_73
.LBB0_63:                               #   in Loop: Header=BB0_58 Depth=1
	movq	%r13, %rdi
	leaq	16(%rsp), %rsi
	callq	cdiff_execute
	cmpl	$-1, %eax
	je	.LBB0_76
# BB#64:                                # %.thread95.outer
                                        #   in Loop: Header=BB0_58 Depth=1
	incl	%r15d
	testl	%ebp, %ebp
	jne	.LBB0_58
	jmp	.LBB0_73
.LBB0_65:
	movl	$.L.str.8, %edi
.LBB0_66:
	xorl	%eax, %eax
	callq	logg
.LBB0_67:
	movl	%r15d, %edi
	callq	close
.LBB0_68:
	movl	$-1, %ebp
.LBB0_69:
	movl	%ebp, %eax
	addq	$9432, %rsp             # imm = 0x24D8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_70:
	movl	$.L.str.10, %edi
	jmp	.LBB0_24
.LBB0_71:
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	logg
	leaq	16(%rsp), %rdi
	callq	cdiff_ctx_free
	movq	%r14, %rdi
	callq	fclose
	jmp	.LBB0_68
.LBB0_72:
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
.LBB0_73:                               # %.thread95.outer._crit_edge
	movq	%r14, %rdi
	callq	gzclose
	jmp	.LBB0_30
.LBB0_74:
	movl	$.L.str.11, %edi
.LBB0_75:
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	logg
	leaq	16(%rsp), %rdi
	callq	cdiff_ctx_free
	movq	%r14, %rdi
	callq	gzclose
	jmp	.LBB0_68
.LBB0_76:
	movl	$.L.str.12, %edi
	jmp	.LBB0_75
.Lfunc_end0:
	.size	cdiff_apply, .Lfunc_end0-cdiff_apply
	.cfi_endproc

	.p2align	4, 0x90
	.type	cdiff_ctx_free,@function
cdiff_ctx_free:                         # @cdiff_ctx_free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_3
# BB#1:
	callq	free
	movq	$0, (%rbx)
	jmp	.LBB1_3
	.p2align	4, 0x90
.LBB1_2:                                #   in Loop: Header=BB1_3 Depth=1
	movq	8(%rax), %rdi
	callq	free
	movq	8(%rbx), %rdi
	movq	24(%rdi), %rax
	movq	%rax, 8(%rbx)
	callq	free
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB1_2
# BB#4:                                 # %._crit_edge27
	movq	$0, 16(%rbx)
	jmp	.LBB1_5
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_5 Depth=1
	movq	8(%rax), %rdi
	callq	free
	movq	24(%rbx), %rdi
	movq	24(%rdi), %rax
	movq	%rax, 24(%rbx)
	callq	free
.LBB1_5:                                # %._crit_edge27
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB1_6
	jmp	.LBB1_8
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_8 Depth=1
	movq	8(%rax), %rdi
	callq	free
	movq	32(%rbx), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	32(%rbx), %rdi
	movq	24(%rdi), %rax
	movq	%rax, 32(%rbx)
	callq	free
.LBB1_8:                                # =>This Inner Loop Header: Depth=1
	movq	32(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB1_7
# BB#9:                                 # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end1:
	.size	cdiff_ctx_free, .Lfunc_end1-cdiff_ctx_free
	.cfi_endproc

	.p2align	4, 0x90
	.type	cdiff_execute,@function
cdiff_execute:                          # @cdiff_execute
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 64
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movb	(%rbx), %al
	testb	%al, %al
	je	.LBB2_14
# BB#1:                                 # %.preheader.i.preheader
	xorl	%ebp, %ebp
	jmp	.LBB2_3
	.p2align	4, 0x90
.LBB2_2:                                #   in Loop: Header=BB2_3 Depth=1
	incl	%ebp
	movzbl	(%rbx,%rbp), %eax
.LBB2_3:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	orb	$32, %al
	cmpb	$32, %al
	jne	.LBB2_2
# BB#4:
	testl	%ebp, %ebp
	je	.LBB2_14
# BB#5:
	leal	1(%rbp), %edi
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB2_14
# BB#6:                                 # %cdiff_token.exit
	movl	%ebp, %r12d
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	strncpy
	movb	$0, (%r15,%r12)
	movl	$.L.str.21, %edi
	movq	%r15, %rsi
	callq	strcmp
	movb	$1, %r12b
	testl	%eax, %eax
	je	.LBB2_15
# BB#7:
	movl	$.L.str.22, %edi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_16
# BB#8:
	movl	$.L.str.23, %edi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_17
# BB#9:
	movl	$.L.str.24, %edi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_18
# BB#10:
	movl	$.L.str.25, %edi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_19
# BB#11:
	movl	$.L.str.26, %edi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_20
# BB#12:
	movl	$.L.str.27, %edi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_21
# BB#13:                                # %.thread
	movl	$.L.str.18, %edi
	jmp	.LBB2_33
.LBB2_14:
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	logg
	jmp	.LBB2_34
.LBB2_15:
	xorl	%r13d, %r13d
	jmp	.LBB2_22
.LBB2_16:
	movl	$1, %r13d
	jmp	.LBB2_22
.LBB2_17:
	movl	$2, %r13d
	jmp	.LBB2_22
.LBB2_18:
	movl	$3, %r13d
	jmp	.LBB2_22
.LBB2_19:
	xorl	%r12d, %r12d
	movl	$4, %r13d
	jmp	.LBB2_22
.LBB2_20:
	movl	$5, %r13d
	jmp	.LBB2_22
.LBB2_21:
	movl	$6, %r13d
.LBB2_22:
	shlq	$3, %r13
	movb	(%rbx), %al
	testb	%al, %al
	sete	%dl
	setne	%cl
	testb	%r12b, %r12b
	je	.LBB2_27
# BB#23:
	testb	%cl, %cl
	movq	%rbx, %rdi
	je	.LBB2_28
# BB#24:                                # %.lr.ph.i.preheader
	movzwl	commands+8(%r13,%r13,2), %r8d
	xorl	%ebp, %ebp
	movl	$1, %esi
	.p2align	4, 0x90
.LBB2_25:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ecx
	xorl	%ebp, %ebp
	cmpb	$32, %al
	sete	%bpl
	addl	%ecx, %ebp
	movl	%esi, %eax
	leaq	(%rbx,%rax), %rdi
	movzbl	(%rbx,%rax), %eax
	testb	%al, %al
	sete	%dl
	setne	%cl
	cmpl	%r8d, %ebp
	je	.LBB2_28
# BB#26:                                # %.lr.ph.i
                                        #   in Loop: Header=BB2_25 Depth=1
	incl	%esi
	testb	%cl, %cl
	jne	.LBB2_25
	jmp	.LBB2_28
.LBB2_27:
	movq	%rbx, %rdi
.LBB2_28:                               # %._crit_edge.i
	testb	%dl, %dl
	jne	.LBB2_32
# BB#29:                                # %cdiff_token.exit35
	callq	__strdup
	testq	%rax, %rax
	je	.LBB2_32
# BB#30:
	movq	commands+16(%r13,%r13,2), %rbp
	movq	%rax, %rdi
	callq	free
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*%rbp
	testl	%eax, %eax
	je	.LBB2_36
# BB#31:
	movl	$.L.str.20, %edi
	jmp	.LBB2_33
.LBB2_32:                               # %cdiff_token.exit35.thread
	movl	$.L.str.19, %edi
.LBB2_33:
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	logg
	movq	%r15, %rdi
	callq	free
.LBB2_34:
	movl	$-1, %eax
.LBB2_35:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_36:
	movq	%r15, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB2_35
.Lfunc_end2:
	.size	cdiff_execute, .Lfunc_end2-cdiff_execute
	.cfi_endproc

	.p2align	4, 0x90
	.type	cdiff_cmd_open,@function
cdiff_cmd_open:                         # @cdiff_cmd_open
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 48
.Lcfi33:
	.cfi_offset %rbx, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movb	(%rdi), %cl
	testb	%cl, %cl
	je	.LBB3_9
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	xorl	%edx, %edx
	cmpb	$32, %cl
	sete	%dl
	addl	%esi, %edx
	movl	%eax, %esi
	movzbl	(%rdi,%rsi), %ecx
	testb	%cl, %cl
	setne	%bl
	cmpl	$1, %edx
	je	.LBB3_4
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB3_2 Depth=1
	incl	%eax
	testb	%bl, %bl
	jne	.LBB3_2
.LBB3_4:                                # %._crit_edge.i
	testb	%cl, %cl
	je	.LBB3_9
# BB#5:                                 # %cdiff_token.exit
	addq	%rsi, %rdi
	callq	__strdup
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB3_9
# BB#6:
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB3_12
# BB#7:
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	logg
.LBB3_8:
	movq	%r15, %rdi
	callq	free
	jmp	.LBB3_10
.LBB3_9:                                # %cdiff_token.exit.thread
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	callq	logg
.LBB3_10:
	movl	$-1, %eax
.LBB3_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_12:                               # %.preheader
	movb	(%r15), %bl
	testb	%bl, %bl
	je	.LBB3_18
# BB#13:                                # %.lr.ph.preheader
	movl	$1, %ebp
	cmpb	$46, %bl
	je	.LBB3_16
	.p2align	4, 0x90
.LBB3_15:
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	%bl, %rcx
	testb	$8, (%rax,%rcx,2)
	jne	.LBB3_16
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_14:                               # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB3_16 Depth=1
	movzbl	(%r15,%rbx), %ebx
	incl	%ebp
	cmpb	$46, %bl
	jne	.LBB3_15
.LBB3_16:                               # =>This Inner Loop Header: Depth=1
	movsbl	%bl, %esi
	movl	$.L.str.30, %edi
	movl	$3, %edx
	callq	memchr
	testq	%rax, %rax
	jne	.LBB3_19
# BB#17:                                #   in Loop: Header=BB3_16 Depth=1
	movl	%ebp, %ebx
	movq	%r15, %rdi
	callq	strlen
	cmpq	%rax, %rbx
	jb	.LBB3_14
.LBB3_18:                               # %._crit_edge
	movq	%r15, (%r14)
	xorl	%eax, %eax
	jmp	.LBB3_11
.LBB3_19:
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	callq	logg
	jmp	.LBB3_8
.Lfunc_end3:
	.size	cdiff_cmd_open, .Lfunc_end3-cdiff_cmd_open
	.cfi_endproc

	.p2align	4, 0x90
	.type	cdiff_cmd_add,@function
cdiff_cmd_add:                          # @cdiff_cmd_add
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movb	(%rdi), %cl
	testb	%cl, %cl
	je	.LBB4_9
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	xorl	%edx, %edx
	cmpb	$32, %cl
	sete	%dl
	addl	%esi, %edx
	movl	%eax, %esi
	movzbl	(%rdi,%rsi), %ecx
	testb	%cl, %cl
	setne	%bl
	cmpl	$1, %edx
	je	.LBB4_4
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB4_2 Depth=1
	incl	%eax
	testb	%bl, %bl
	jne	.LBB4_2
.LBB4_4:                                # %._crit_edge.i
	testb	%cl, %cl
	je	.LBB4_9
# BB#5:                                 # %cdiff_token.exit
	addq	%rsi, %rdi
	callq	__strdup
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB4_9
# BB#6:
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	testq	%rax, %rax
	je	.LBB4_12
# BB#7:
	movq	%rbx, 8(%rax)
	movq	16(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB4_13
# BB#8:
	addq	$16, %r14
	movq	%rax, 24(%rcx)
	jmp	.LBB4_14
.LBB4_9:                                # %cdiff_token.exit.thread
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	logg
.LBB4_10:
	movl	$-1, %eax
.LBB4_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_12:
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	callq	logg
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB4_10
.LBB4_13:
	movq	%rax, 16(%r14)
	addq	$8, %r14
.LBB4_14:
	movq	%rax, (%r14)
	xorl	%eax, %eax
	jmp	.LBB4_11
.Lfunc_end4:
	.size	cdiff_cmd_add, .Lfunc_end4-cdiff_cmd_add
	.cfi_endproc

	.p2align	4, 0x90
	.type	cdiff_cmd_del,@function
cdiff_cmd_del:                          # @cdiff_cmd_del
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 64
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbp
	movb	(%rbp), %cl
	testb	%cl, %cl
	je	.LBB5_20
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%eax, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	xorl	%edx, %edx
	cmpb	$32, %cl
	sete	%dl
	addl	%esi, %edx
	incl	%eax
	movzbl	(%rbp,%rax), %ecx
	testb	%cl, %cl
	setne	%bl
	cmpl	$1, %edx
	je	.LBB5_4
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB5_2 Depth=1
	testb	%bl, %bl
	jne	.LBB5_2
.LBB5_4:                                # %._crit_edge.i
	testb	%cl, %cl
	je	.LBB5_20
# BB#5:                                 # %.preheader.i.preheader
	leaq	(%rbp,%rax), %r15
	xorl	%ebx, %ebx
	jmp	.LBB5_7
	.p2align	4, 0x90
.LBB5_6:                                #   in Loop: Header=BB5_7 Depth=1
	incl	%ebx
.LBB5_7:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rbx), %ecx
	movzbl	(%rbp,%rcx), %ecx
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB5_6
# BB#8:
	testl	%ebx, %ebx
	je	.LBB5_20
# BB#9:
	leal	1(%rbx), %edi
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB5_20
# BB#10:
	movl	%ebx, %r13d
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	strncpy
	movb	$0, (%r12,%r13)
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	callq	strtol
	movq	%rax, %r15
	movq	%r12, %rdi
	callq	free
	movb	(%rbp), %cl
	testb	%cl, %cl
	je	.LBB5_31
# BB#11:                                # %.lr.ph.i49.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB5_12:                               # %.lr.ph.i49
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edx
	xorl	%ebx, %ebx
	cmpb	$32, %cl
	sete	%bl
	addl	%edx, %ebx
	movl	%eax, %esi
	movzbl	(%rbp,%rsi), %ecx
	testb	%cl, %cl
	setne	%dl
	cmpl	$2, %ebx
	je	.LBB5_14
# BB#13:                                # %.lr.ph.i49
                                        #   in Loop: Header=BB5_12 Depth=1
	incl	%eax
	testb	%dl, %dl
	jne	.LBB5_12
.LBB5_14:                               # %._crit_edge.i53
	testb	%cl, %cl
	je	.LBB5_31
# BB#15:                                # %cdiff_token.exit55
	addq	%rsi, %rbp
	movq	%rbp, %rdi
	callq	__strdup
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_31
# BB#16:
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	testq	%rax, %rax
	je	.LBB5_32
# BB#17:
	movq	%rbx, 8(%rax)
	movl	%r15d, (%rax)
	movq	24(%r14), %rcx
	addq	$24, %r14
	testq	%rcx, %rcx
	je	.LBB5_30
# BB#18:
	movl	(%rcx), %esi
	cmpl	%esi, %r15d
	jae	.LBB5_25
# BB#19:
	movq	%rcx, 24(%rax)
	jmp	.LBB5_30
.LBB5_20:                               # %._crit_edge.i.thread
	movl	$.L.str.34, %edi
.LBB5_21:
	xorl	%eax, %eax
	callq	logg
.LBB5_22:
	movl	$-1, %eax
.LBB5_23:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_24:                               # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB5_25 Depth=1
	movl	(%rdx), %esi
	movq	%rdx, %rcx
.LBB5_25:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rcx), %rdx
	cmpl	%r15d, %esi
	jae	.LBB5_28
# BB#26:                                #   in Loop: Header=BB5_25 Depth=1
	testq	%rdx, %rdx
	je	.LBB5_29
# BB#27:                                #   in Loop: Header=BB5_25 Depth=1
	cmpl	(%rdx), %r15d
	jae	.LBB5_24
	jmp	.LBB5_29
.LBB5_28:                               #   in Loop: Header=BB5_25 Depth=1
	testq	%rdx, %rdx
	jne	.LBB5_24
.LBB5_29:                               # %._crit_edge
	movq	24(%rcx), %rdx
	addq	$24, %rcx
	movq	%rdx, 24(%rax)
	movq	%rcx, %r14
.LBB5_30:
	movq	%rax, (%r14)
	xorl	%eax, %eax
	jmp	.LBB5_23
.LBB5_31:                               # %cdiff_token.exit55.thread
	movl	$.L.str.35, %edi
	jmp	.LBB5_21
.LBB5_32:
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	callq	logg
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB5_22
.Lfunc_end5:
	.size	cdiff_cmd_del, .Lfunc_end5-cdiff_cmd_del
	.cfi_endproc

	.p2align	4, 0x90
	.type	cdiff_cmd_xchg,@function
cdiff_cmd_xchg:                         # @cdiff_cmd_xchg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 64
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	movb	(%r14), %cl
	testb	%cl, %cl
	je	.LBB6_30
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%eax, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	xorl	%edx, %edx
	cmpb	$32, %cl
	sete	%dl
	addl	%esi, %edx
	incl	%eax
	movzbl	(%r14,%rax), %ecx
	testb	%cl, %cl
	setne	%bl
	cmpl	$1, %edx
	je	.LBB6_4
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB6_2 Depth=1
	testb	%bl, %bl
	jne	.LBB6_2
.LBB6_4:                                # %._crit_edge.i
	testb	%cl, %cl
	je	.LBB6_30
# BB#5:                                 # %.preheader.i.preheader
	leaq	(%r14,%rax), %r15
	xorl	%ebp, %ebp
	jmp	.LBB6_7
	.p2align	4, 0x90
.LBB6_6:                                #   in Loop: Header=BB6_7 Depth=1
	incl	%ebp
.LBB6_7:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rbp), %ecx
	movzbl	(%r14,%rcx), %ecx
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB6_6
# BB#8:
	testl	%ebp, %ebp
	je	.LBB6_30
# BB#9:
	leal	1(%rbp), %edi
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB6_30
# BB#10:
	movq	%r13, %rbx
	movl	%ebp, %r13d
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	strncpy
	movb	$0, (%r12,%r13)
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	callq	strtol
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r12, %rdi
	callq	free
	movb	(%r14), %al
	testb	%al, %al
	je	.LBB6_34
# BB#11:                                # %.lr.ph.i56.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_12:                               # %.lr.ph.i56
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	xorl	%ecx, %ecx
	cmpb	$32, %al
	sete	%cl
	addl	%edx, %ecx
	incl	%ebp
	movzbl	(%r14,%rbp), %eax
	testb	%al, %al
	setne	%dl
	cmpl	$2, %ecx
	je	.LBB6_14
# BB#13:                                # %.lr.ph.i56
                                        #   in Loop: Header=BB6_12 Depth=1
	testb	%dl, %dl
	jne	.LBB6_12
.LBB6_14:                               # %._crit_edge.i60
	testb	%al, %al
	je	.LBB6_34
# BB#15:                                # %.preheader.i62.preheader
	leaq	(%r14,%rbp), %r13
	xorl	%r15d, %r15d
	jmp	.LBB6_17
	.p2align	4, 0x90
.LBB6_16:                               #   in Loop: Header=BB6_17 Depth=1
	incl	%r15d
.LBB6_17:                               # %.preheader.i62
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbp,%r15), %eax
	movzbl	(%r14,%rax), %eax
	orb	$32, %al
	cmpb	$32, %al
	jne	.LBB6_16
# BB#18:
	testl	%r15d, %r15d
	je	.LBB6_34
# BB#19:
	leal	1(%r15), %edi
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB6_34
# BB#20:
	movl	%r15d, %ebp
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	strncpy
	movb	$0, (%r12,%rbp)
	movb	(%r14), %dil
	testb	%dil, %dil
	movq	%rbx, %rbp
	je	.LBB6_42
# BB#21:                                # %.lr.ph.i71.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB6_22:                               # %.lr.ph.i71
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	xorl	%edx, %edx
	cmpb	$32, %dil
	sete	%dl
	addl	%ecx, %edx
	movl	%eax, %esi
	movzbl	(%r14,%rsi), %edi
	testb	%dil, %dil
	setne	%cl
	cmpl	$3, %edx
	je	.LBB6_24
# BB#23:                                # %.lr.ph.i71
                                        #   in Loop: Header=BB6_22 Depth=1
	incl	%eax
	testb	%cl, %cl
	jne	.LBB6_22
.LBB6_24:                               # %._crit_edge.i75
	testb	%dil, %dil
	je	.LBB6_42
# BB#25:                                # %cdiff_token.exit77
	addq	%rsi, %r14
	movq	%r14, %rdi
	callq	__strdup
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB6_42
# BB#26:
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	testq	%rax, %rax
	je	.LBB6_43
# BB#27:
	movq	%r12, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%edi, (%rax)
	movq	32(%rbp), %rcx
	addq	$32, %rbp
	testq	%rcx, %rcx
	je	.LBB6_41
# BB#28:
	movl	(%rcx), %esi
	cmpl	%esi, %edi
	jae	.LBB6_36
# BB#29:
	movq	%rcx, 24(%rax)
	jmp	.LBB6_41
.LBB6_30:                               # %._crit_edge.i.thread
	movl	$.L.str.37, %edi
.LBB6_31:
	xorl	%eax, %eax
	callq	logg
.LBB6_32:
	movl	$-1, %eax
.LBB6_33:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_34:                               # %._crit_edge.i60.thread
	movl	$.L.str.38, %edi
	jmp	.LBB6_31
.LBB6_35:                               # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB6_36 Depth=1
	movl	(%rdx), %esi
	movq	%rdx, %rcx
.LBB6_36:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rcx), %rdx
	cmpl	%edi, %esi
	jae	.LBB6_39
# BB#37:                                #   in Loop: Header=BB6_36 Depth=1
	testq	%rdx, %rdx
	je	.LBB6_40
# BB#38:                                #   in Loop: Header=BB6_36 Depth=1
	cmpl	(%rdx), %edi
	jae	.LBB6_35
	jmp	.LBB6_40
.LBB6_39:                               #   in Loop: Header=BB6_36 Depth=1
	testq	%rdx, %rdx
	jne	.LBB6_35
.LBB6_40:                               # %._crit_edge
	movq	24(%rcx), %rdx
	addq	$24, %rcx
	movq	%rdx, 24(%rax)
	movq	%rcx, %rbp
.LBB6_41:
	movq	%rax, (%rbp)
	xorl	%eax, %eax
	jmp	.LBB6_33
.LBB6_42:                               # %cdiff_token.exit77.thread
	movq	%r12, %rdi
	callq	free
	movl	$.L.str.38, %edi
	jmp	.LBB6_31
.LBB6_43:
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	callq	logg
	movq	%r12, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB6_32
.Lfunc_end6:
	.size	cdiff_cmd_xchg, .Lfunc_end6-cdiff_cmd_xchg
	.cfi_endproc

	.p2align	4, 0x90
	.type	cdiff_cmd_close,@function
cdiff_cmd_close:                        # @cdiff_cmd_close
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	subq	$1080, %rsp             # imm = 0x438
.Lcfi74:
	.cfi_def_cfa_offset 1136
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB7_7
# BB#1:
	movq	8(%r13), %r14
	movq	24(%r13), %rbx
	movq	32(%r13), %r12
	movq	%rbx, %rax
	orq	%r12, %rax
	je	.LBB7_45
# BB#2:
	movl	$.L.str.13, %esi
	callq	fopen
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB7_8
# BB#3:
	movl	$.L.str.42, %edi
	callq	cli_gentemp
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB7_9
# BB#4:
	movl	$.L.str.44, %esi
	movq	%rbp, %rdi
	callq	fopen
	testq	%rax, %rax
	je	.LBB7_10
# BB#5:                                 # %.outer.preheader
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	testq	%rbx, %rbx
	je	.LBB7_11
# BB#6:                                 # %.outer102.preheader.preheader
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	leaq	48(%rsp), %rbx
	incl	%ebp
	jmp	.LBB7_26
.LBB7_7:
	movl	$.L.str.40, %edi
	xorl	%eax, %eax
	callq	logg
	jmp	.LBB7_60
.LBB7_8:
	movq	(%r13), %rsi
	movl	$.L.str.41, %edi
	jmp	.LBB7_54
.LBB7_9:
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	callq	logg
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	jmp	.LBB7_60
.LBB7_10:
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	logg
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	jmp	.LBB7_58
.LBB7_11:
	xorl	%ebp, %ebp
.LBB7_12:                               # %.outer102.us.preheader
	testq	%r12, %r12
	je	.LBB7_22
# BB#13:
	leaq	48(%rsp), %rbx
.LBB7_14:                               # %.outer102.split.us..outer102.split.us.split_crit_edge.us.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_15 Depth 2
	incl	%ebp
	.p2align	4, 0x90
.LBB7_15:                               # %.outer102.split.us..outer102.split.us.split_crit_edge.us
                                        #   Parent Loop BB7_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1024, %esi             # imm = 0x400
	movq	%rbx, %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	fgets
	testq	%rax, %rax
	je	.LBB7_38
# BB#16:                                #   in Loop: Header=BB7_15 Depth=2
	cmpl	(%r12), %ebp
	je	.LBB7_18
# BB#17:                                #   in Loop: Header=BB7_15 Depth=2
	movq	%rbx, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	fputs
	incl	%ebp
	cmpl	$-1, %eax
	jne	.LBB7_15
	jmp	.LBB7_36
.LBB7_18:                               # %.us-lcssa118.us.us-lcssa.us128
                                        #   in Loop: Header=BB7_14 Depth=1
	movq	8(%r12), %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB7_63
# BB#19:                                #   in Loop: Header=BB7_14 Depth=1
	movq	16(%r12), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	fputs
	cmpl	$-1, %eax
	je	.LBB7_36
# BB#20:                                #   in Loop: Header=BB7_14 Depth=1
	movl	$10, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	fputc
	cmpl	$-1, %eax
	je	.LBB7_36
# BB#21:                                # %.outer102.us
                                        #   in Loop: Header=BB7_14 Depth=1
	movq	24(%r12), %r12
	testq	%r12, %r12
	jne	.LBB7_14
.LBB7_22:                               # %.outer102.split.us.split.us.us
                                        # =>This Inner Loop Header: Depth=1
	leaq	48(%rsp), %rdi
	movl	$1024, %esi             # imm = 0x400
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	fgets
	testq	%rax, %rax
	je	.LBB7_39
# BB#23:                                #   in Loop: Header=BB7_22 Depth=1
	leaq	48(%rsp), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	fputs
	cmpl	$-1, %eax
	jne	.LBB7_22
	jmp	.LBB7_36
.LBB7_24:                               # %.outer102.preheader
                                        #   in Loop: Header=BB7_26 Depth=1
	incl	%ebp
	jmp	.LBB7_26
.LBB7_25:                               #   in Loop: Header=BB7_26 Depth=1
	movq	24(%r12), %r12
	incl	%ebp
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB7_26:                               # =>This Inner Loop Header: Depth=1
	movl	$1024, %esi             # imm = 0x400
	movq	%rbx, %r15
	movq	%rbx, %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	fgets
	testq	%rax, %rax
	je	.LBB7_37
# BB#27:                                #   in Loop: Header=BB7_26 Depth=1
	movq	32(%rsp), %rbx          # 8-byte Reload
	cmpl	(%rbx), %ebp
	je	.LBB7_31
# BB#28:                                #   in Loop: Header=BB7_26 Depth=1
	testq	%r12, %r12
	je	.LBB7_30
# BB#29:                                #   in Loop: Header=BB7_26 Depth=1
	cmpl	(%r12), %ebp
	je	.LBB7_33
.LBB7_30:                               #   in Loop: Header=BB7_26 Depth=1
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	fputs
	movq	%r15, %rbx
	incl	%ebp
	cmpl	$-1, %eax
	jne	.LBB7_26
	jmp	.LBB7_36
.LBB7_31:                               # %.us-lcssa117
                                        #   in Loop: Header=BB7_26 Depth=1
	movq	8(%rbx), %rdi
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	callq	strlen
	movq	%r15, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rax, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB7_62
# BB#32:                                # %.outer
                                        #   in Loop: Header=BB7_26 Depth=1
	movq	24(%rbx), %rbx
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	testq	%rbx, %rbx
	movq	%r15, %rbx
	jne	.LBB7_24
	jmp	.LBB7_12
.LBB7_33:                               # %.us-lcssa118
                                        #   in Loop: Header=BB7_26 Depth=1
	movq	8(%r12), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB7_63
# BB#34:                                #   in Loop: Header=BB7_26 Depth=1
	movq	16(%r12), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	fputs
	cmpl	$-1, %eax
	je	.LBB7_36
# BB#35:                                #   in Loop: Header=BB7_26 Depth=1
	movl	$10, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	fputc
	cmpl	$-1, %eax
	jne	.LBB7_25
.LBB7_36:                               # %.us-lcssa121.us
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fclose
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	unlink
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	logg
	movq	%rbx, %rdi
	jmp	.LBB7_59
.LBB7_37:
	movq	32(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB7_40
.LBB7_38:
	xorl	%ebx, %ebx
	jmp	.LBB7_40
.LBB7_39:
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
.LBB7_40:                               # %.us-lcssa.us
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fclose
	orq	%r12, %rbx
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB7_42
# BB#41:
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	callq	logg
	jmp	.LBB7_57
.LBB7_42:
	movq	(%r13), %rdi
	callq	unlink
	movq	(%r13), %rsi
	cmpl	$-1, %eax
	je	.LBB7_55
# BB#43:
	movq	%rbp, %rdi
	callq	rename
	cmpl	$-1, %eax
	je	.LBB7_56
# BB#44:
	movq	%rbp, %rdi
	callq	free
.LBB7_45:
	testq	%r14, %r14
	je	.LBB7_51
# BB#46:
	movq	(%r13), %rdi
	movl	$.L.str.52, %esi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB7_53
	.p2align	4, 0x90
.LBB7_47:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rdi
	movq	%rbp, %rsi
	callq	fputs
	cmpl	$-1, %eax
	je	.LBB7_52
# BB#48:                                #   in Loop: Header=BB7_47 Depth=1
	movl	$10, %edi
	movq	%rbp, %rsi
	callq	fputc
	cmpl	$-1, %eax
	je	.LBB7_52
# BB#49:                                #   in Loop: Header=BB7_47 Depth=1
	movq	24(%r14), %r14
	testq	%r14, %r14
	jne	.LBB7_47
# BB#50:                                # %._crit_edge
	movq	%rbp, %rdi
	callq	fclose
.LBB7_51:
	movq	%r13, %rdi
	callq	cdiff_ctx_free
	xorl	%eax, %eax
	jmp	.LBB7_61
.LBB7_52:
	movq	%rbp, %rdi
	callq	fclose
	movq	(%r13), %rsi
	movl	$.L.str.48, %edi
	jmp	.LBB7_54
.LBB7_53:
	movq	(%r13), %rsi
	movl	$.L.str.53, %edi
.LBB7_54:
	xorl	%eax, %eax
	callq	logg
	jmp	.LBB7_60
.LBB7_55:
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	logg
	jmp	.LBB7_57
.LBB7_56:
	movq	(%r13), %rdx
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	logg
.LBB7_57:
	movq	%rbp, %rdi
	callq	unlink
.LBB7_58:
	movq	%rbp, %rdi
.LBB7_59:
	callq	free
.LBB7_60:
	movl	$-1, %eax
.LBB7_61:
	addq	$1080, %rsp             # imm = 0x438
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_62:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fclose
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	callq	unlink
	movq	%r14, %rdi
	callq	free
	movq	(%r13), %rdx
	movl	$.L.str.46, %edi
	jmp	.LBB7_64
.LBB7_63:                               # %.us-lcssa120.us
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fclose
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	callq	unlink
	movq	%r14, %rdi
	callq	free
	movq	(%r13), %rdx
	movl	$.L.str.47, %edi
.LBB7_64:
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	logg
	jmp	.LBB7_60
.Lfunc_end7:
	.size	cdiff_cmd_close, .Lfunc_end7-cdiff_cmd_close
	.cfi_endproc

	.p2align	4, 0x90
	.type	cdiff_cmd_move,@function
cdiff_cmd_move:                         # @cdiff_cmd_move
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi85:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 56
	subq	$1080, %rsp             # imm = 0x438
.Lcfi87:
	.cfi_def_cfa_offset 1136
.Lcfi88:
	.cfi_offset %rbx, -56
.Lcfi89:
	.cfi_offset %r12, -48
.Lcfi90:
	.cfi_offset %r13, -40
.Lcfi91:
	.cfi_offset %r14, -32
.Lcfi92:
	.cfi_offset %r15, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB8_2
# BB#1:
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	logg
	jmp	.LBB8_26
.LBB8_2:
	movb	(%r13), %cl
	testb	%cl, %cl
	je	.LBB8_24
# BB#3:                                 # %.lr.ph.i.preheader
	xorl	%eax, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	xorl	%edx, %edx
	cmpb	$32, %cl
	sete	%dl
	addl	%esi, %edx
	incl	%eax
	movzbl	(%r13,%rax), %ecx
	testb	%cl, %cl
	setne	%bl
	cmpl	$3, %edx
	je	.LBB8_6
# BB#5:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB8_4 Depth=1
	testb	%bl, %bl
	jne	.LBB8_4
.LBB8_6:                                # %._crit_edge.i
	testb	%cl, %cl
	je	.LBB8_24
# BB#7:                                 # %.preheader.i.preheader
	leaq	(%r13,%rax), %r14
	xorl	%ebp, %ebp
	jmp	.LBB8_9
	.p2align	4, 0x90
.LBB8_8:                                #   in Loop: Header=BB8_9 Depth=1
	incl	%ebp
.LBB8_9:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rbp), %ecx
	movzbl	(%r13,%rcx), %ecx
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB8_8
# BB#10:
	testl	%ebp, %ebp
	je	.LBB8_24
# BB#11:
	leal	1(%rbp), %edi
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB8_24
# BB#12:
	movl	%ebp, %r15d
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	strncpy
	movb	$0, (%r12,%r15)
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	callq	strtol
	movq	%rax, %r15
	movq	%r12, %rdi
	callq	free
	movb	(%r13), %al
	testb	%al, %al
	je	.LBB8_28
# BB#13:                                # %.lr.ph.i156.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_14:                               # %.lr.ph.i156
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	xorl	%ecx, %ecx
	cmpb	$32, %al
	sete	%cl
	addl	%edx, %ecx
	incl	%ebp
	movzbl	(%r13,%rbp), %eax
	testb	%al, %al
	setne	%dl
	cmpl	$5, %ecx
	je	.LBB8_16
# BB#15:                                # %.lr.ph.i156
                                        #   in Loop: Header=BB8_14 Depth=1
	testb	%dl, %dl
	jne	.LBB8_14
.LBB8_16:                               # %._crit_edge.i160
	testb	%al, %al
	je	.LBB8_28
# BB#17:                                # %.preheader.i162.preheader
	leaq	(%r13,%rbp), %r14
	xorl	%ebx, %ebx
	jmp	.LBB8_19
	.p2align	4, 0x90
.LBB8_18:                               #   in Loop: Header=BB8_19 Depth=1
	incl	%ebx
.LBB8_19:                               # %.preheader.i162
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbp,%rbx), %eax
	movzbl	(%r13,%rax), %eax
	orb	$32, %al
	cmpb	$32, %al
	jne	.LBB8_18
# BB#20:
	testl	%ebx, %ebx
	je	.LBB8_28
# BB#21:
	leal	1(%rbx), %edi
	callq	malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB8_28
# BB#22:
	movl	%ebx, %r12d
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	strncpy
	movb	$0, (%rbp,%r12)
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strtol
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	free
	cmpl	%r15d, %r12d
	jae	.LBB8_29
# BB#23:
	movl	$.L.str.57, %edi
	jmp	.LBB8_25
.LBB8_24:                               # %._crit_edge.i.thread
	movl	$.L.str.55, %edi
.LBB8_25:
	xorl	%eax, %eax
	callq	logg
.LBB8_26:
	movl	$-1, %eax
.LBB8_27:
	addq	$1080, %rsp             # imm = 0x438
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_28:                               # %._crit_edge.i160.thread
	movl	$.L.str.56, %edi
	jmp	.LBB8_25
.LBB8_29:
	movb	(%r13), %cl
	testb	%cl, %cl
	je	.LBB8_55
# BB#30:                                # %.lr.ph.i171.preheader
	xorl	%eax, %eax
	xorl	%edx, %edx
.LBB8_31:                               # %.lr.ph.i171
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	xorl	%edx, %edx
	cmpb	$32, %cl
	sete	%dl
	addl	%esi, %edx
	incl	%eax
	movzbl	(%r13,%rax), %ecx
	testb	%cl, %cl
	setne	%bl
	cmpl	$4, %edx
	je	.LBB8_33
# BB#32:                                # %.lr.ph.i171
                                        #   in Loop: Header=BB8_31 Depth=1
	testb	%bl, %bl
	jne	.LBB8_31
.LBB8_33:                               # %._crit_edge.i175
	testb	%cl, %cl
	je	.LBB8_55
# BB#34:                                # %.preheader.i177.preheader
	leaq	(%r13,%rax), %rbx
	xorl	%ebp, %ebp
	jmp	.LBB8_36
.LBB8_35:                               #   in Loop: Header=BB8_36 Depth=1
	incl	%ebp
.LBB8_36:                               # %.preheader.i177
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rbp), %ecx
	movzbl	(%r13,%rcx), %ecx
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB8_35
# BB#37:
	testl	%ebp, %ebp
	je	.LBB8_55
# BB#38:
	leal	1(%rbp), %edi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB8_55
# BB#39:
	movl	%ebp, %ebp
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	strncpy
	movb	$0, (%r14,%rbp)
	movb	(%r13), %cl
	testb	%cl, %cl
	je	.LBB8_67
# BB#40:                                # %.lr.ph.i186.preheader
	xorl	%eax, %eax
	xorl	%edx, %edx
.LBB8_41:                               # %.lr.ph.i186
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	xorl	%edx, %edx
	cmpb	$32, %cl
	sete	%dl
	addl	%esi, %edx
	incl	%eax
	movzbl	(%r13,%rax), %ecx
	testb	%cl, %cl
	setne	%bl
	cmpl	$6, %edx
	je	.LBB8_43
# BB#42:                                # %.lr.ph.i186
                                        #   in Loop: Header=BB8_41 Depth=1
	testb	%bl, %bl
	jne	.LBB8_41
.LBB8_43:                               # %._crit_edge.i190
	testb	%cl, %cl
	je	.LBB8_67
# BB#44:                                # %.preheader.i192.preheader
	leaq	(%r13,%rax), %rbp
	xorl	%ebx, %ebx
	jmp	.LBB8_46
.LBB8_45:                               #   in Loop: Header=BB8_46 Depth=1
	incl	%ebx
.LBB8_46:                               # %.preheader.i192
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rbx), %ecx
	movzbl	(%r13,%rcx), %ecx
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB8_45
# BB#47:
	testl	%ebx, %ebx
	je	.LBB8_67
# BB#48:
	leal	1(%rbx), %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB8_67
# BB#49:
	movl	%ebx, %ebx
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	%rax, %rbp
	callq	strncpy
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movb	$0, (%rbp,%rbx)
	movb	(%r13), %cl
	testb	%cl, %cl
	je	.LBB8_88
# BB#50:                                # %.lr.ph.i201.preheader
	xorl	%eax, %eax
	xorl	%edx, %edx
.LBB8_51:                               # %.lr.ph.i201
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	xorl	%edx, %edx
	cmpb	$32, %cl
	sete	%dl
	addl	%esi, %edx
	incl	%eax
	movzbl	(%r13,%rax), %ecx
	testb	%cl, %cl
	setne	%bl
	cmpl	$1, %edx
	je	.LBB8_53
# BB#52:                                # %.lr.ph.i201
                                        #   in Loop: Header=BB8_51 Depth=1
	testb	%bl, %bl
	jne	.LBB8_51
.LBB8_53:                               # %._crit_edge.i205
	testb	%cl, %cl
	je	.LBB8_88
# BB#54:                                # %.preheader.i207.preheader
	leaq	(%r13,%rax), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	xorl	%ebp, %ebp
	jmp	.LBB8_57
.LBB8_55:                               # %._crit_edge.i175.thread
	movl	$.L.str.58, %edi
	jmp	.LBB8_25
.LBB8_56:                               #   in Loop: Header=BB8_57 Depth=1
	incl	%ebp
.LBB8_57:                               # %.preheader.i207
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rbp), %ecx
	movzbl	(%r13,%rcx), %ecx
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB8_56
# BB#58:
	testl	%ebp, %ebp
	je	.LBB8_88
# BB#59:
	leal	1(%rbp), %edi
	callq	malloc
	movq	%rax, (%rsp)            # 8-byte Spill
	testq	%rax, %rax
	je	.LBB8_88
# BB#60:
	movl	%ebp, %ebp
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rbp, %rdx
	callq	strncpy
	movb	$0, (%rbx,%rbp)
	movl	$.L.str.13, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testq	%rax, %rax
	je	.LBB8_91
# BB#61:
	movb	(%r13), %cl
	testb	%cl, %cl
	je	.LBB8_89
# BB#62:                                # %.lr.ph.i216.preheader
	xorl	%eax, %eax
	xorl	%edx, %edx
.LBB8_63:                               # %.lr.ph.i216
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	xorl	%edx, %edx
	cmpb	$32, %cl
	sete	%dl
	addl	%esi, %edx
	incl	%eax
	movzbl	(%r13,%rax), %ecx
	testb	%cl, %cl
	setne	%bl
	cmpl	$2, %edx
	je	.LBB8_65
# BB#64:                                # %.lr.ph.i216
                                        #   in Loop: Header=BB8_63 Depth=1
	testb	%bl, %bl
	jne	.LBB8_63
.LBB8_65:                               # %._crit_edge.i220
	testb	%cl, %cl
	je	.LBB8_89
# BB#66:                                # %.preheader.i222.preheader
	leaq	(%r13,%rax), %rbp
	xorl	%ebx, %ebx
	jmp	.LBB8_70
.LBB8_67:                               # %._crit_edge.i190.thread
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	callq	logg
	movq	%r14, %rdi
.LBB8_68:
	callq	free
	jmp	.LBB8_26
.LBB8_69:                               #   in Loop: Header=BB8_70 Depth=1
	incl	%ebx
.LBB8_70:                               # %.preheader.i222
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rbx), %ecx
	movzbl	(%r13,%rcx), %ecx
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB8_69
# BB#71:
	testl	%ebx, %ebx
	je	.LBB8_89
# BB#72:
	leal	1(%rbx), %edi
	callq	malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB8_89
# BB#73:
	movl	%ebx, %ebx
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	strncpy
	movb	$0, (%r13,%rbx)
	movl	$.L.str.52, %esi
	movq	%r13, %rdi
	callq	fopen
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB8_93
# BB#74:
	movl	$.L.str.42, %edi
	callq	cli_gentemp
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB8_94
# BB#75:
	movl	$.L.str.44, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	fopen
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB8_95
# BB#76:                                # %.outer.preheader
	xorl	%ebx, %ebx
.LBB8_77:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_78 Depth 2
                                        #     Child Loop BB8_82 Depth 2
	movl	%r15d, %ebp
	subl	%ebx, %ebp
.LBB8_78:                               #   Parent Loop BB8_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1024, %esi             # imm = 0x400
	leaq	48(%rsp), %rdi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	fgets
	testq	%rax, %rax
	je	.LBB8_96
# BB#79:                                #   in Loop: Header=BB8_78 Depth=2
	decl	%ebp
	je	.LBB8_81
# BB#80:                                #   in Loop: Header=BB8_78 Depth=2
	leaq	48(%rsp), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	fputs
	cmpl	$-1, %eax
	jne	.LBB8_78
	jmp	.LBB8_98
.LBB8_81:                               #   in Loop: Header=BB8_77 Depth=1
	movq	%r14, %rdi
	callq	strlen
	leaq	48(%rsp), %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	strncmp
	testl	%eax, %eax
	movl	%r15d, %ebx
	jne	.LBB8_105
.LBB8_82:                               # %.preheader
                                        #   Parent Loop BB8_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	48(%rsp), %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	fputs
	cmpl	$-1, %eax
	je	.LBB8_102
# BB#83:                                #   in Loop: Header=BB8_82 Depth=2
	cmpl	%r12d, %ebx
	jae	.LBB8_86
# BB#84:                                #   in Loop: Header=BB8_82 Depth=2
	movl	$1024, %esi             # imm = 0x400
	leaq	48(%rsp), %rdi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	fgets
	testq	%rax, %rax
	je	.LBB8_86
# BB#85:                                #   in Loop: Header=BB8_82 Depth=2
	incl	%ebx
	cmpl	$1, %ebx
	jne	.LBB8_82
.LBB8_86:                               # %.critedge
                                        #   in Loop: Header=BB8_77 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	%r13, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	strlen
	leaq	48(%rsp), %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	strncmp
	movl	%eax, %r13d
	movq	%rbp, %rdi
	callq	free
	testl	%r13d, %r13d
	movl	$0, %r13d
	je	.LBB8_77
# BB#87:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fclose
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	unlink
	movq	%rbp, %rdi
	callq	free
	movl	$.L.str.66, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	jmp	.LBB8_106
.LBB8_88:                               # %._crit_edge.i205.thread
	movl	$.L.str.60, %edi
	xorl	%eax, %eax
	callq	logg
	movq	%r14, %rdi
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB8_68
.LBB8_89:                               # %._crit_edge.i220.thread
	movl	$.L.str.62, %edi
	xorl	%eax, %eax
	callq	logg
	movq	%r14, %rdi
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB8_90:
	callq	fclose
	jmp	.LBB8_26
.LBB8_91:
	movl	$.L.str.61, %edi
	xorl	%eax, %eax
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rsi
	callq	logg
	movq	%r14, %rdi
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
.LBB8_92:
	movq	%rbx, %rdi
	jmp	.LBB8_68
.LBB8_93:
	movl	$.L.str.63, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	logg
	movq	%r14, %rdi
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fclose
	movq	%r13, %rdi
	jmp	.LBB8_68
.LBB8_94:
	movl	$.L.str.64, %edi
	xorl	%eax, %eax
	callq	logg
	movq	%r14, %rdi
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fclose
	movq	%r13, %rdi
	callq	free
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB8_90
.LBB8_95:
	movl	$.L.str.65, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	callq	logg
	movq	%r14, %rdi
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fclose
	movq	%r13, %rdi
	callq	free
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	jmp	.LBB8_92
.LBB8_96:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fclose
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	testq	%r13, %r13
	je	.LBB8_99
# BB#97:
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	%r14, %rdi
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	unlink
	movq	%rbx, %rdi
	callq	free
	movl	$.L.str.68, %edi
	xorl	%eax, %eax
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	logg
	movq	%rbx, %rdi
	callq	free
	movq	%r13, %rdi
	jmp	.LBB8_68
.LBB8_98:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fclose
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	unlink
	movl	$.L.str.67, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	logg
	jmp	.LBB8_92
.LBB8_99:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	unlink
	cmpl	$-1, %eax
	je	.LBB8_103
# BB#100:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	rename
	cmpl	$-1, %eax
	je	.LBB8_104
# BB#101:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	xorl	%eax, %eax
	jmp	.LBB8_27
.LBB8_102:
	movq	%r14, %rdi
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fclose
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	unlink
	movq	%rbx, %rdi
	callq	free
	movl	$.L.str.67, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	logg
	movq	%r13, %rdi
	jmp	.LBB8_68
.LBB8_103:
	movl	$.L.str.69, %edi
	xorl	%eax, %eax
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rsi
	callq	logg
	movq	%rbx, %rdi
	callq	free
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	unlink
	jmp	.LBB8_92
.LBB8_104:
	movl	$.L.str.70, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rdx
	callq	logg
	movq	%rbx, %rdi
	callq	free
	movq	%rbp, %rdi
	callq	unlink
	movq	%rbp, %rdi
	jmp	.LBB8_68
.LBB8_105:
	movq	%r14, %rdi
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fclose
	movq	%r13, %rdi
	callq	free
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	unlink
	movq	%rbx, %rdi
	callq	free
	movl	$.L.str.66, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
.LBB8_106:
	callq	logg
	jmp	.LBB8_26
.Lfunc_end8:
	.size	cdiff_cmd_move, .Lfunc_end8-cdiff_cmd_move
	.cfi_endproc

	.p2align	4, 0x90
	.type	cdiff_cmd_unlink,@function
cdiff_cmd_unlink:                       # @cdiff_cmd_unlink
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 32
.Lcfi97:
	.cfi_offset %rbx, -32
.Lcfi98:
	.cfi_offset %r14, -24
.Lcfi99:
	.cfi_offset %rbp, -16
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB9_2
# BB#1:
	movl	$.L.str.71, %edi
	xorl	%eax, %eax
	callq	logg
	jmp	.LBB9_17
.LBB9_2:
	movb	(%rdi), %cl
	testb	%cl, %cl
	je	.LBB9_16
# BB#3:                                 # %.lr.ph.i.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB9_4:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	xorl	%edx, %edx
	cmpb	$32, %cl
	sete	%dl
	addl	%esi, %edx
	movl	%eax, %esi
	movzbl	(%rdi,%rsi), %ecx
	testb	%cl, %cl
	setne	%bl
	cmpl	$1, %edx
	je	.LBB9_6
# BB#5:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB9_4 Depth=1
	incl	%eax
	testb	%bl, %bl
	jne	.LBB9_4
.LBB9_6:                                # %._crit_edge.i
	testb	%cl, %cl
	je	.LBB9_16
# BB#7:                                 # %cdiff_token.exit
	addq	%rsi, %rdi
	callq	__strdup
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB9_16
# BB#8:                                 # %.preheader
	movb	(%r14), %bl
	testb	%bl, %bl
	je	.LBB9_14
# BB#9:                                 # %.lr.ph.preheader
	movl	$1, %ebp
	cmpb	$46, %bl
	je	.LBB9_12
	.p2align	4, 0x90
.LBB9_11:
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	%bl, %rcx
	testb	$8, (%rax,%rcx,2)
	jne	.LBB9_12
	jmp	.LBB9_19
	.p2align	4, 0x90
.LBB9_10:                               # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB9_12 Depth=1
	movzbl	(%r14,%rbx), %ebx
	incl	%ebp
	cmpb	$46, %bl
	jne	.LBB9_11
.LBB9_12:                               # =>This Inner Loop Header: Depth=1
	movsbl	%bl, %esi
	movl	$.L.str.30, %edi
	movl	$3, %edx
	callq	memchr
	testq	%rax, %rax
	jne	.LBB9_19
# BB#13:                                #   in Loop: Header=BB9_12 Depth=1
	movl	%ebp, %ebx
	movq	%r14, %rdi
	callq	strlen
	cmpq	%rax, %rbx
	jb	.LBB9_10
.LBB9_14:                               # %._crit_edge
	movq	%r14, %rdi
	callq	unlink
	cmpl	$-1, %eax
	je	.LBB9_20
# BB#15:
	movq	%r14, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB9_18
.LBB9_16:                               # %cdiff_token.exit.thread
	movl	$.L.str.72, %edi
	xorl	%eax, %eax
	callq	logg
.LBB9_17:
	movl	$-1, %eax
.LBB9_18:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB9_19:
	movl	$.L.str.73, %edi
	xorl	%eax, %eax
	callq	logg
	jmp	.LBB9_21
.LBB9_20:
	movl	$.L.str.74, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	logg
.LBB9_21:
	movq	%r14, %rdi
	callq	free
	jmp	.LBB9_17
.Lfunc_end9:
	.size	cdiff_cmd_unlink, .Lfunc_end9-cdiff_cmd_unlink
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"!cdiff_apply: Can't duplicate descriptor %d\n"
	.size	.L.str, 45

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"!cdiff_apply: lseek(desc, %d, SEEK_END) failed\n"
	.size	.L.str.1, 48

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"!cdiff_apply: Can't read %d bytes\n"
	.size	.L.str.2, 35

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"!cdiff_apply: No digital signature in cdiff file\n"
	.size	.L.str.3, 50

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"!cdiff_apply: Can't fstat file\n"
	.size	.L.str.4, 32

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"!cdiff_apply: compressed data end offset < 0\n"
	.size	.L.str.5, 46

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"!cdiff_apply: lseek(desc, 0, SEEK_SET) failed\n"
	.size	.L.str.6, 47

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"ClamAV-Diff:%*u:%u:"
	.size	.L.str.7, 20

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"!cdiff_apply: Incorrect file format\n"
	.size	.L.str.8, 37

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"rb"
	.size	.L.str.9, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"!cdiff_apply: Can't gzdopen descriptor %d\n"
	.size	.L.str.10, 43

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"!cdiff_apply: Premature EOF at line %d\n"
	.size	.L.str.11, 40

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"!cdiff_apply: Error executing command at line %d\n"
	.size	.L.str.12, 50

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"r"
	.size	.L.str.13, 2

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"!cdiff_apply: fdopen() failed for descriptor %d\n"
	.size	.L.str.14, 49

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"*cdiff_apply: File %s was not properly closed\n"
	.size	.L.str.15, 47

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"*cdiff_apply: Parsed %d lines and executed %d commands\n"
	.size	.L.str.16, 56

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"!cdiff_apply: Problem parsing line\n"
	.size	.L.str.17, 36

	.type	commands,@object        # @commands
	.section	.rodata,"a",@progbits
	.p2align	4
commands:
	.quad	.L.str.21
	.short	1                       # 0x1
	.zero	6
	.quad	cdiff_cmd_open
	.quad	.L.str.22
	.short	1                       # 0x1
	.zero	6
	.quad	cdiff_cmd_add
	.quad	.L.str.23
	.short	2                       # 0x2
	.zero	6
	.quad	cdiff_cmd_del
	.quad	.L.str.24
	.short	3                       # 0x3
	.zero	6
	.quad	cdiff_cmd_xchg
	.quad	.L.str.25
	.short	0                       # 0x0
	.zero	6
	.quad	cdiff_cmd_close
	.quad	.L.str.26
	.short	6                       # 0x6
	.zero	6
	.quad	cdiff_cmd_move
	.quad	.L.str.27
	.short	1                       # 0x1
	.zero	6
	.quad	cdiff_cmd_unlink
	.zero	24
	.size	commands, 192

	.type	.L.str.18,@object       # @.str.18
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.18:
	.asciz	"!cdiff_apply: Unknown command %s\n"
	.size	.L.str.18, 34

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"!cdiff_apply: Not enough arguments for %s\n"
	.size	.L.str.19, 43

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"!cdiff_apply: Can't execute command %s\n"
	.size	.L.str.20, 40

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"OPEN"
	.size	.L.str.21, 5

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"ADD"
	.size	.L.str.22, 4

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"DEL"
	.size	.L.str.23, 4

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"XCHG"
	.size	.L.str.24, 5

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"CLOSE"
	.size	.L.str.25, 6

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"MOVE"
	.size	.L.str.26, 5

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"UNLINK"
	.size	.L.str.27, 7

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"!cdiff_cmd_open: Can't get first argument\n"
	.size	.L.str.28, 43

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"!cdiff_cmd_open: %s not closed before opening %s\n"
	.size	.L.str.29, 50

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"/\\"
	.size	.L.str.30, 3

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"!cdiff_cmd_open: Forbidden characters found in database name\n"
	.size	.L.str.31, 62

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"!cdiff_cmd_add: Can't get first argument\n"
	.size	.L.str.32, 42

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"!cdiff_cmd_add: Can't allocate memory for cdiff_node\n"
	.size	.L.str.33, 54

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"!cdiff_cmd_del: Can't get first argument\n"
	.size	.L.str.34, 42

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"!cdiff_cmd_del: Can't get second argument\n"
	.size	.L.str.35, 43

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"!cdiff_cmd_del: Can't allocate memory for cdiff_node\n"
	.size	.L.str.36, 54

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"!cdiff_cmd_xchg: Can't get first argument\n"
	.size	.L.str.37, 43

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"!cdiff_cmd_xchg: Can't get second argument\n"
	.size	.L.str.38, 44

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"!cdiff_cmd_xchg: Can't allocate memory for cdiff_node\n"
	.size	.L.str.39, 55

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"!cdiff_cmd_close: No database to close\n"
	.size	.L.str.40, 40

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"!cdiff_cmd_close: Can't open file %s for reading\n"
	.size	.L.str.41, 50

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"."
	.size	.L.str.42, 2

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"!cdiff_cmd_close: Can't generate temporary name\n"
	.size	.L.str.43, 49

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"w"
	.size	.L.str.44, 2

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"!cdiff_cmd_close: Can't open file %s for writing\n"
	.size	.L.str.45, 50

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"!cdiff_cmd_close: Can't apply DEL at line %d of %s\n"
	.size	.L.str.46, 52

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"!cdiff_cmd_close: Can't apply XCHG at line %d of %s\n"
	.size	.L.str.47, 53

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"!cdiff_cmd_close: Can't write to %s\n"
	.size	.L.str.48, 37

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"!cdiff_cmd_close: Not all DEL/XCHG have been executed\n"
	.size	.L.str.49, 55

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"!cdiff_cmd_close: Can't unlink %s\n"
	.size	.L.str.50, 35

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"!cdiff_cmd_close: Can't rename %s to %s\n"
	.size	.L.str.51, 41

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"a"
	.size	.L.str.52, 2

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"!cdiff_cmd_close: Can't open file %s for appending\n"
	.size	.L.str.53, 52

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"!cdiff_cmd_move: Database %s is still open\n"
	.size	.L.str.54, 44

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"!cdiff_cmd_move: Can't get third argument\n"
	.size	.L.str.55, 43

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"!cdiff_cmd_move: Can't get fifth argument\n"
	.size	.L.str.56, 43

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"!cdiff_cmd_move: end_line < start_line\n"
	.size	.L.str.57, 40

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"!cdiff_cmd_move: Can't get fourth argument\n"
	.size	.L.str.58, 44

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"!cdiff_cmd_move: Can't get sixth argument\n"
	.size	.L.str.59, 43

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"!cdiff_cmd_move: Can't get first argument\n"
	.size	.L.str.60, 43

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"!cdiff_cmd_move: Can't open %s for reading\n"
	.size	.L.str.61, 44

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"!cdiff_cmd_move: Can't get second argument\n"
	.size	.L.str.62, 44

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"!cdiff_cmd_move: Can't open %s for appending\n"
	.size	.L.str.63, 46

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"!cdiff_cmd_move: Can't generate temporary name\n"
	.size	.L.str.64, 48

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"!cdiff_cmd_move: Can't open file %s for writing\n"
	.size	.L.str.65, 49

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"!cdiff_cmd_close: Can't apply MOVE due to conflict at line %d\n"
	.size	.L.str.66, 63

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"!cdiff_cmd_move: Can't write to %s\n"
	.size	.L.str.67, 36

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"!cdiff_cmd_move: No data was moved from %s to %s\n"
	.size	.L.str.68, 50

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"!cdiff_cmd_move: Can't unlink %s\n"
	.size	.L.str.69, 34

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"!cdiff_cmd_move: Can't rename %s to %s\n"
	.size	.L.str.70, 40

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"!cdiff_cmd_unlink: Database %s is still open\n"
	.size	.L.str.71, 46

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"!cdiff_cmd_unlink: Can't get first argument\n"
	.size	.L.str.72, 45

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"!cdiff_cmd_unlink: Forbidden characters found in database name\n"
	.size	.L.str.73, 64

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"!cdiff_cmd_unlink: Can't unlink %s\n"
	.size	.L.str.74, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
