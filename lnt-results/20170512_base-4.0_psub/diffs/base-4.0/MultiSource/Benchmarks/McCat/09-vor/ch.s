	.text
	.file	"ch.bc"
	.globl	empty
	.p2align	4, 0x90
	.type	empty,@function
empty:                                  # @empty
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpq	$0, Splaytree(%rip)
	sete	%al
	retq
.Lfunc_end0:
	.size	empty, .Lfunc_end0-empty
	.cfi_endproc

	.globl	point_equal
	.p2align	4, 0x90
	.type	point_equal,@function
point_equal:                            # @point_equal
	.cfi_startproc
# BB#0:
	cmpl	%esi, %edi
	jne	.LBB1_1
# BB#2:
	shrq	$32, %rsi
	shrq	$32, %rdi
	cmpl	%esi, %edi
	sete	%al
	movzbl	%al, %eax
	retq
.LBB1_1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end1:
	.size	point_equal, .Lfunc_end1-point_equal
	.cfi_endproc

	.globl	determinant
	.p2align	4, 0x90
	.type	determinant,@function
determinant:                            # @determinant
	.cfi_startproc
# BB#0:
	movq	%rdi, %r8
	shrq	$32, %r8
	movq	%rsi, %rcx
	shrq	$32, %rcx
	movq	%rdx, %r9
	shrq	$32, %r9
	movl	%ecx, %eax
	subl	%r9d, %eax
	imull	%eax, %edi
	imull	%esi, %r9d
	subl	%edx, %esi
	imull	%r8d, %esi
	imull	%edx, %ecx
	addl	%esi, %ecx
	addl	%r9d, %edi
	subl	%ecx, %edi
	movl	%edi, %eax
	retq
.Lfunc_end2:
	.size	determinant, .Lfunc_end2-determinant
	.cfi_endproc

	.globl	visible
	.p2align	4, 0x90
	.type	visible,@function
visible:                                # @visible
	.cfi_startproc
# BB#0:
	movq	%rdx, %rax
	shrq	$32, %rax
	movq	%rcx, %r8
	shrq	$32, %r8
	movl	%eax, %r9d
	subl	%r8d, %r9d
	imull	%esi, %r9d
	shrq	$32, %rsi
	imull	%edx, %r8d
	subl	%ecx, %edx
	imull	%ecx, %eax
	imull	%esi, %edx
	addl	%eax, %edx
	addl	%r9d, %r8d
	subl	%edx, %r8d
	movl	%r8d, %ecx
	shrl	$31, %ecx
	xorl	%eax, %eax
	testl	%r8d, %r8d
	setg	%al
	testl	%edi, %edi
	cmovel	%ecx, %eax
	retq
.Lfunc_end3:
	.size	visible, .Lfunc_end3-visible
	.cfi_endproc

	.globl	get_points_on_hull
	.p2align	4, 0x90
	.type	get_points_on_hull,@function
get_points_on_hull:                     # @get_points_on_hull
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	$0, 8(%rsp)
	movq	8(%r14), %rsi
	leaq	8(%rsp), %rdi
	callq	point_list_insert
	leaq	24(%r14), %rax
	cmpq	%r15, 32(%r14)
	leaq	16(%r14), %rcx
	cmoveq	%rax, %rcx
	movq	(%rcx), %rax
	cmpq	%r15, %rax
	je	.LBB4_9
# BB#1:                                 # %.preheader
	cmpq	%r14, 32(%rax)
	sete	%cl
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	testb	$1, %cl
	je	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	callq	point_list_insert
	leaq	24(%rbx), %rax
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	point_list_insert
	leaq	16(%rbx), %rax
.LBB4_5:                                #   in Loop: Header=BB4_2 Depth=1
	movq	(%rax), %rax
	movq	32(%rax), %rdx
	cmpq	%rbx, %rdx
	sete	%cl
	cmpq	%r15, %rax
	jne	.LBB4_2
# BB#6:                                 # %._crit_edge
	cmpq	%rbx, %rdx
	je	.LBB4_7
# BB#8:
	movq	8(%r15), %rsi
	jmp	.LBB4_11
.LBB4_9:
	movq	(%r14), %rsi
	movq	8(%r14), %rax
	cmpl	%esi, %eax
	jne	.LBB4_11
# BB#10:                                # %point_equal.exit
	movq	%rsi, %rcx
	shrq	$32, %rcx
	shrq	$32, %rax
	cmpl	%ecx, %eax
	jne	.LBB4_11
	jmp	.LBB4_12
.LBB4_7:
	movq	(%r15), %rsi
.LBB4_11:                               # %point_equal.exit.thread
	leaq	8(%rsp), %rdi
	callq	point_list_insert
.LBB4_12:
	movq	8(%rsp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	get_points_on_hull, .Lfunc_end4-get_points_on_hull
	.cfi_endproc

	.globl	add_segments
	.p2align	4, 0x90
	.type	add_segments,@function
add_segments:                           # @add_segments
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 48
.Lcfi12:
	.cfi_offset %rbx, -48
.Lcfi13:
	.cfi_offset %r12, -40
.Lcfi14:
	.cfi_offset %r13, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	testl	%ecx, %ecx
	jne	.LBB5_5
	jmp	.LBB5_1
	.p2align	4, 0x90
.LBB5_8:                                # %point_equal.exit72
                                        #   in Loop: Header=BB5_5 Depth=1
	movl	$48, %edi
	callq	malloc
	movq	%r13, (%rax)
	movq	8(%r15), %rcx
	movq	%rcx, 8(%rax)
	movq	32(%r12), %rcx
	movq	%rcx, 32(%rax)
	movq	%r15, 40(%rax)
	movq	%r12, 16(%rax)
	movq	%r14, 24(%rax)
	movq	%rax, 24(%r15)
	movq	32(%r12), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, %rsi
	shrq	$32, %rsi
	leaq	24(%rcx), %rdi
	addq	$16, %rcx
	cmpl	%ebx, %esi
	cmovneq	%rdi, %rcx
	cmpl	%r13d, %edx
	cmovneq	%rdi, %rcx
	movq	%rax, (%rcx)
	movq	%rax, 32(%r12)
	movq	%rax, 40(%r14)
	movq	32(%rax), %r12
	movq	%rax, %r15
.LBB5_5:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdx
	movq	(%r15), %rcx
	movq	%rcx, %rax
	shrq	$32, %rax
	cmpl	%ecx, %edx
	movq	%r12, %rsi
	jne	.LBB5_7
# BB#6:                                 # %point_equal.exit
                                        #   in Loop: Header=BB5_5 Depth=1
	shrq	$32, %rdx
	leaq	8(%r12), %rsi
	cmpl	%eax, %edx
	cmovneq	%r12, %rsi
.LBB5_7:                                # %point_equal.exit.thread
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	(%rsi), %r13
	movq	8(%r15), %rdx
	movq	%rdx, %rsi
	shrq	$32, %rsi
	movq	%r13, %rbx
	shrq	$32, %rbx
	movl	%esi, %edi
	subl	%ebx, %edi
	imull	%ecx, %edi
	movl	%edx, %ecx
	subl	%r13d, %ecx
	imull	%ebx, %edx
	imull	%eax, %ecx
	imull	%r13d, %esi
	addl	%ecx, %esi
	addl	%edi, %edx
	subl	%esi, %edx
	testl	%edx, %edx
	jg	.LBB5_8
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_4:                                # %point_equal.exit65.us
                                        #   in Loop: Header=BB5_1 Depth=1
	movl	$48, %edi
	callq	malloc
	movq	%r13, (%rax)
	movq	8(%r15), %rcx
	movq	%rcx, 8(%rax)
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
	movq	%r15, 24(%rax)
	movq	%r12, 32(%rax)
	movq	%r14, 40(%rax)
	movq	%rax, 40(%r15)
	movq	16(%r12), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, %rsi
	shrq	$32, %rsi
	leaq	40(%rcx), %rdi
	addq	$32, %rcx
	cmpl	%ebx, %esi
	cmovneq	%rdi, %rcx
	cmpl	%r13d, %edx
	cmovneq	%rdi, %rcx
	movq	%rax, (%rcx)
	movq	%rax, 16(%r12)
	movq	%rax, 24(%r14)
	movq	16(%rax), %r12
	movq	%rax, %r15
.LBB5_1:                                # %tailrecurse.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdx
	movq	(%r15), %rcx
	movq	%rcx, %rax
	shrq	$32, %rax
	cmpl	%ecx, %edx
	movq	%r12, %rsi
	jne	.LBB5_3
# BB#2:                                 # %point_equal.exit.us
                                        #   in Loop: Header=BB5_1 Depth=1
	shrq	$32, %rdx
	leaq	8(%r12), %rsi
	cmpl	%eax, %edx
	cmovneq	%r12, %rsi
.LBB5_3:                                # %point_equal.exit.us.thread
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	(%rsi), %r13
	movq	8(%r15), %rdx
	movq	%rdx, %rsi
	shrq	$32, %rsi
	movq	%r13, %rbx
	shrq	$32, %rbx
	movl	%esi, %edi
	subl	%ebx, %edi
	imull	%ecx, %edi
	movl	%edx, %ecx
	subl	%r13d, %ecx
	imull	%ebx, %edx
	imull	%eax, %ecx
	imull	%r13d, %esi
	addl	%ecx, %esi
	addl	%edi, %edx
	cmpl	%esi, %edx
	js	.LBB5_4
.LBB5_9:                                # %.us-lcssa.us
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	add_segments, .Lfunc_end5-add_segments
	.cfi_endproc

	.globl	construct_ch
	.p2align	4, 0x90
	.type	construct_ch,@function
construct_ch:                           # @construct_ch
	.cfi_startproc
# BB#0:
	movl	$0, CHno(%rip)
	cmpq	$0, Splaytree(%rip)
	je	.LBB6_15
# BB#1:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 64
.Lcfi24:
	.cfi_offset %rbx, -56
.Lcfi25:
	.cfi_offset %r12, -48
.Lcfi26:
	.cfi_offset %r13, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r14
	movl	$Splaytree, %edi
	callq	delete_min
	movq	%rax, (%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 32(%r14)
	movq	%r14, 40(%r14)
	cmpq	$0, Splaytree(%rip)
	je	.LBB6_16
# BB#2:
	movl	$Splaytree, %edi
	callq	delete_min
	movq	%rax, 8(%r14)
	cmpq	$0, Splaytree(%rip)
	je	.LBB6_17
# BB#3:                                 # %.lr.ph.preheader
	movq	%r14, %r15
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_6 Depth 2
                                        #     Child Loop BB6_11 Depth 2
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r12
	movq	8(%r15), %rax
	movq	%rax, (%r12)
	movl	$Splaytree, %edi
	callq	delete_min
	movq	%rax, 8(%r12)
	movq	%r15, 16(%r12)
	movq	%r14, 32(%r12)
	movq	%r12, 24(%r12)
	movq	%r12, 40(%r12)
	movq	%r12, 24(%r14)
	movq	%r12, 40(%r15)
	movq	%r12, %r13
	jmp	.LBB6_6
	.p2align	4, 0x90
.LBB6_5:                                # %point_equal.exit65.us.i
                                        #   in Loop: Header=BB6_6 Depth=2
	movl	$48, %edi
	callq	malloc
	movq	%rbx, (%rax)
	movq	8(%r13), %rcx
	movq	%rcx, 8(%rax)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax)
	movq	%r13, 24(%rax)
	movq	%r15, 32(%rax)
	movq	%r12, 40(%rax)
	movq	%rax, 40(%r13)
	movq	16(%r15), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, %rsi
	shrq	$32, %rsi
	leaq	40(%rcx), %rdi
	addq	$32, %rcx
	cmpl	%ebp, %esi
	cmovneq	%rdi, %rcx
	cmpl	%ebx, %edx
	cmovneq	%rdi, %rcx
	movq	%rax, (%rcx)
	movq	%rax, 16(%r15)
	movq	%rax, 24(%r12)
	movq	16(%rax), %r15
	movq	%rax, %r13
.LBB6_6:                                # %tailrecurse.us.i
                                        #   Parent Loop BB6_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %rbx
	movq	(%r13), %rcx
	movq	%rcx, %rax
	shrq	$32, %rax
	cmpl	%ecx, %ebx
	jne	.LBB6_8
# BB#7:                                 # %point_equal.exit.us.i
                                        #   in Loop: Header=BB6_6 Depth=2
	shrq	$32, %rbx
	leaq	8(%r15), %rdx
	cmpl	%eax, %ebx
	cmovneq	%r15, %rdx
	movq	(%rdx), %rbx
.LBB6_8:                                # %point_equal.exit.us.thread.i
                                        #   in Loop: Header=BB6_6 Depth=2
	movq	8(%r13), %rdx
	movq	%rdx, %rsi
	shrq	$32, %rsi
	movq	%rbx, %rbp
	shrq	$32, %rbp
	movl	%esi, %edi
	subl	%ebp, %edi
	imull	%ecx, %edi
	movl	%edx, %ecx
	subl	%ebx, %ecx
	imull	%ebp, %edx
	imull	%ebx, %esi
	imull	%eax, %ecx
	addl	%esi, %ecx
	addl	%edi, %edx
	cmpl	%ecx, %edx
	js	.LBB6_5
# BB#9:                                 # %add_segments.exit51
                                        #   in Loop: Header=BB6_4 Depth=1
	movq	24(%r12), %r15
	jmp	.LBB6_11
	.p2align	4, 0x90
.LBB6_10:                               # %point_equal.exit72.i
                                        #   in Loop: Header=BB6_11 Depth=2
	movl	$48, %edi
	callq	malloc
	movq	%r13, (%rax)
	movq	8(%r12), %rcx
	movq	%rcx, 8(%rax)
	movq	32(%r14), %rcx
	movq	%rcx, 32(%rax)
	movq	%r12, 40(%rax)
	movq	%r14, 16(%rax)
	movq	%r15, 24(%rax)
	movq	%rax, 24(%r12)
	movq	32(%r14), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, %rsi
	shrq	$32, %rsi
	leaq	24(%rcx), %rdi
	addq	$16, %rcx
	cmpl	%ebx, %esi
	cmovneq	%rdi, %rcx
	cmpl	%r13d, %edx
	cmovneq	%rdi, %rcx
	movq	%rax, (%rcx)
	movq	%rax, 32(%r14)
	movq	%rax, 40(%r15)
	movq	32(%rax), %r14
	movq	%rax, %r12
.LBB6_11:                               # %tailrecurse.i
                                        #   Parent Loop BB6_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %r13
	movq	(%r12), %rcx
	movq	%rcx, %rax
	shrq	$32, %rax
	cmpl	%ecx, %r13d
	jne	.LBB6_13
# BB#12:                                # %point_equal.exit.i
                                        #   in Loop: Header=BB6_11 Depth=2
	shrq	$32, %r13
	leaq	8(%r14), %rdx
	cmpl	%eax, %r13d
	cmovneq	%r14, %rdx
	movq	(%rdx), %r13
.LBB6_13:                               # %point_equal.exit.thread.i
                                        #   in Loop: Header=BB6_11 Depth=2
	movq	8(%r12), %rdx
	movq	%rdx, %rsi
	shrq	$32, %rsi
	movq	%r13, %rbx
	shrq	$32, %rbx
	movl	%esi, %edi
	subl	%ebx, %edi
	imull	%ecx, %edi
	movl	%edx, %ecx
	subl	%r13d, %ecx
	imull	%ebx, %edx
	imull	%r13d, %esi
	imull	%eax, %ecx
	addl	%esi, %ecx
	addl	%edi, %edx
	subl	%ecx, %edx
	testl	%edx, %edx
	jg	.LBB6_10
# BB#14:                                # %add_segments.exit
                                        #   in Loop: Header=BB6_4 Depth=1
	movq	40(%r15), %r14
	cmpq	$0, Splaytree(%rip)
	jne	.LBB6_4
	jmp	.LBB6_18
.LBB6_15:
	retq
.LBB6_16:
	movq	%rax, 8(%r14)
.LBB6_17:
	movq	%r14, %r15
.LBB6_18:                               # %.loopexit
	xorl	%edi, %edi
	callq	free_tree
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	get_points_on_hull
	movq	%rax, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	remove_points           # TAILCALL
.Lfunc_end6:
	.size	construct_ch, .Lfunc_end6-construct_ch
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
