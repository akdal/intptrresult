	.text
	.file	"gxpath.bc"
	.globl	gx_path_init
	.p2align	4, 0x90
	.type	gx_path_init,@function
gx_path_init:                           # @gx_path_init
	.cfi_startproc
# BB#0:
	movups	(%rsi), %xmm0
	movups	%xmm0, (%rdi)
	movq	$0, 48(%rdi)
	movw	$0, 136(%rdi)
	movb	$0, 138(%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 100(%rdi)
	movups	%xmm0, 88(%rdi)
	retq
.Lfunc_end0:
	.size	gx_path_init, .Lfunc_end0-gx_path_init
	.cfi_endproc

	.globl	gx_path_release
	.p2align	4, 0x90
	.type	gx_path_release,@function
gx_path_release:                        # @gx_path_release
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	cmpq	$0, 88(%r14)
	je	.LBB1_6
# BB#1:
	cmpb	$0, 138(%r14)
	jne	.LBB1_6
# BB#2:
	movq	96(%r14), %rax
	movq	40(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB1_5
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movslq	16(%rbx), %rax
	cmpq	$4, %rax
	jae	.LBB1_7
# BB#4:                                 # %switch.lookup
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	(%rbx), %r15
	movl	.Lswitch.table(,%rax,4), %edx
	movl	$1, %esi
	movl	$.L.str.3, %ecx
	movq	%rbx, %rdi
	callq	*8(%r14)
	testq	%r15, %r15
	movq	%r15, %rbx
	jne	.LBB1_3
.LBB1_5:                                # %._crit_edge
	movq	$0, 88(%r14)
.LBB1_6:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB1_7:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$75, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	16(%rbx), %edx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	gx_path_release, .Lfunc_end1-gx_path_release
	.cfi_endproc

	.globl	gx_path_share
	.p2align	4, 0x90
	.type	gx_path_share,@function
gx_path_share:                          # @gx_path_share
	.cfi_startproc
# BB#0:
	cmpq	$0, 88(%rdi)
	je	.LBB2_2
# BB#1:
	movb	$1, 138(%rdi)
.LBB2_2:
	retq
.Lfunc_end2:
	.size	gx_path_share, .Lfunc_end2-gx_path_share
	.cfi_endproc

	.globl	gx_path_new_subpath
	.p2align	4, 0x90
	.type	gx_path_new_subpath,@function
gx_path_new_subpath:                    # @gx_path_new_subpath
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	subq	$152, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 176
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpb	$0, 138(%rbx)
	je	.LBB3_4
# BB#1:
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	callq	gx_path_copy
	testl	%eax, %eax
	js	.LBB3_9
# BB#2:                                 # %path_alloc_copy.exit
	leaq	8(%rsp), %rsi
	movl	$144, %edx
	movq	%rbx, %rdi
	callq	memcpy
	movb	$0, 138(%rbx)
	movq	96(%rbx), %r14
	testq	%r14, %r14
	jne	.LBB3_5
# BB#3:
	movl	$-13, %eax
	jmp	.LBB3_12
.LBB3_4:
	movq	96(%rbx), %r14
.LBB3_5:
	movl	$1, %edi
	movl	$64, %esi
	movl	$.L.str.4, %edx
	callq	*(%rbx)
	testq	%rax, %rax
	je	.LBB3_9
# BB#6:
	movl	$0, 16(%rax)
	movq	$0, 8(%rax)
	movq	%rax, 40(%rax)
	movb	$0, 56(%rax)
	movq	$0, 48(%rax)
	movups	120(%rbx), %xmm0
	movups	%xmm0, 24(%rax)
	movb	$1, 137(%rbx)
	testq	%r14, %r14
	je	.LBB3_10
# BB#7:
	movq	40(%r14), %rcx
	leaq	8(%rcx), %rdx
	jmp	.LBB3_11
.LBB3_9:
	movl	$-13, %eax
	jmp	.LBB3_12
.LBB3_10:
	leaq	88(%rbx), %rdx
	xorl	%ecx, %ecx
.LBB3_11:
	movq	%rax, (%rdx)
	movq	%rcx, (%rax)
	movq	%rax, 96(%rbx)
	incl	104(%rbx)
	xorl	%eax, %eax
.LBB3_12:
	addq	$152, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	gx_path_new_subpath, .Lfunc_end3-gx_path_new_subpath
	.cfi_endproc

	.globl	path_alloc_copy
	.p2align	4, 0x90
	.type	path_alloc_copy,@function
path_alloc_copy:                        # @path_alloc_copy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
	subq	$144, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 160
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	%rsp, %rsi
	callq	gx_path_copy
	testl	%eax, %eax
	js	.LBB4_1
# BB#2:
	movq	%rsp, %rsi
	movl	$144, %edx
	movq	%rbx, %rdi
	callq	memcpy
	movb	$0, 138(%rbx)
	movq	96(%rbx), %rax
	jmp	.LBB4_3
.LBB4_1:
	xorl	%eax, %eax
.LBB4_3:
	addq	$144, %rsp
	popq	%rbx
	retq
.Lfunc_end4:
	.size	path_alloc_copy, .Lfunc_end4-path_alloc_copy
	.cfi_endproc

	.globl	gx_path_add_point
	.p2align	4, 0x90
	.type	gx_path_add_point,@function
gx_path_add_point:                      # @gx_path_add_point
	.cfi_startproc
# BB#0:
	movb	$0, 137(%rdi)
	movb	$1, 136(%rdi)
	movq	%rsi, 120(%rdi)
	movq	%rdx, 128(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	gx_path_add_point, .Lfunc_end5-gx_path_add_point
	.cfi_endproc

	.globl	gx_path_add_relative_point
	.p2align	4, 0x90
	.type	gx_path_add_relative_point,@function
gx_path_add_relative_point:             # @gx_path_add_relative_point
	.cfi_startproc
# BB#0:
	cmpb	$0, 136(%rdi)
	je	.LBB6_1
# BB#2:
	movb	$0, 137(%rdi)
	movdqu	120(%rdi), %xmm0
	movd	%rdx, %xmm1
	movd	%rsi, %xmm2
	punpcklqdq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	paddq	%xmm0, %xmm2
	movdqu	%xmm2, 120(%rdi)
	xorl	%eax, %eax
	retq
.LBB6_1:
	movl	$-14, %eax
	retq
.Lfunc_end6:
	.size	gx_path_add_relative_point, .Lfunc_end6-gx_path_add_relative_point
	.cfi_endproc

	.globl	gx_path_add_line
	.p2align	4, 0x90
	.type	gx_path_add_line,@function
gx_path_add_line:                       # @gx_path_add_line
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 40
	subq	$152, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 192
.Lcfi19:
	.cfi_offset %rbx, -40
.Lcfi20:
	.cfi_offset %r12, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	96(%rbx), %r12
	cmpb	$0, 137(%rbx)
	je	.LBB7_6
# BB#1:                                 # %._crit_edge
	leaq	138(%rbx), %rcx
	cmpb	$0, (%rcx)
	je	.LBB7_4
.LBB7_2:
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	callq	gx_path_copy
	testl	%eax, %eax
	js	.LBB7_19
# BB#3:                                 # %path_alloc_copy.exit
	leaq	8(%rsp), %rsi
	movl	$144, %edx
	movq	%rbx, %rdi
	callq	memcpy
	movb	$0, 138(%rbx)
	movq	96(%rbx), %r12
	testq	%r12, %r12
	je	.LBB7_19
.LBB7_4:
	movl	$1, %edi
	movl	$40, %esi
	movl	$.L.str.5, %edx
	callq	*(%rbx)
	testq	%rax, %rax
	je	.LBB7_19
# BB#5:
	movl	$1, 16(%rax)
	movq	$0, 8(%rax)
	movq	40(%r12), %rcx
	movq	%rax, 8(%rcx)
	movq	%rcx, (%rax)
	movq	%rax, 40(%r12)
	movq	%r15, 120(%rbx)
	movq	%r15, 24(%rax)
	movq	%r14, 128(%rbx)
	movq	%r14, 32(%rax)
	incl	48(%r12)
	incl	108(%rbx)
	xorl	%eax, %eax
	jmp	.LBB7_20
.LBB7_6:
	cmpb	$0, 136(%rbx)
	je	.LBB7_15
# BB#7:
	cmpb	$0, 138(%rbx)
	je	.LBB7_10
# BB#8:
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	callq	gx_path_copy
	testl	%eax, %eax
	js	.LBB7_19
# BB#9:                                 # %path_alloc_copy.exit.i
	leaq	8(%rsp), %rsi
	movl	$144, %edx
	movq	%rbx, %rdi
	callq	memcpy
	movb	$0, 138(%rbx)
	movq	96(%rbx), %r12
	testq	%r12, %r12
	je	.LBB7_19
.LBB7_10:
	movl	$1, %edi
	movl	$64, %esi
	movl	$.L.str.4, %edx
	callq	*(%rbx)
	testq	%rax, %rax
	je	.LBB7_19
# BB#11:
	movl	$0, 16(%rax)
	movq	$0, 8(%rax)
	movq	%rax, 40(%rax)
	movb	$0, 56(%rax)
	movq	$0, 48(%rax)
	movups	120(%rbx), %xmm0
	movups	%xmm0, 24(%rax)
	movb	$1, 137(%rbx)
	testq	%r12, %r12
	je	.LBB7_21
# BB#12:
	movq	40(%r12), %rdx
	leaq	8(%rdx), %rsi
	jmp	.LBB7_22
.LBB7_19:
	movl	$-13, %eax
	jmp	.LBB7_20
.LBB7_15:
	movl	$-14, %eax
.LBB7_20:                               # %.thread
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB7_21:
	leaq	88(%rbx), %rsi
	xorl	%edx, %edx
.LBB7_22:
	leaq	138(%rbx), %rcx
	movq	%rax, (%rsi)
	movq	%rdx, (%rax)
	movq	%rax, 96(%rbx)
	incl	104(%rbx)
	movq	%rax, %r12
	cmpb	$0, (%rcx)
	jne	.LBB7_2
	jmp	.LBB7_4
.Lfunc_end7:
	.size	gx_path_add_line, .Lfunc_end7-gx_path_add_line
	.cfi_endproc

	.globl	gx_path_add_rectangle
	.p2align	4, 0x90
	.type	gx_path_add_rectangle,@function
gx_path_add_rectangle:                  # @gx_path_add_rectangle
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -40
.Lcfi29:
	.cfi_offset %r12, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%r8, %r12
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movb	$0, 137(%rbx)
	movb	$1, 136(%rbx)
	movq	%rsi, 120(%rbx)
	movq	%r14, 128(%rbx)
	movq	%r12, %rdx
	callq	gx_path_add_line
	testl	%eax, %eax
	js	.LBB8_4
# BB#1:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	gx_path_add_line
	testl	%eax, %eax
	js	.LBB8_4
# BB#2:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	gx_path_add_line
	testl	%eax, %eax
	js	.LBB8_4
# BB#3:
	movq	%rbx, %rdi
	callq	gx_path_close_subpath
	movl	%eax, %ecx
	sarl	$31, %eax
	andl	%ecx, %eax
.LBB8_4:                                # %gx_path_add_pgram.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	gx_path_add_rectangle, .Lfunc_end8-gx_path_add_rectangle
	.cfi_endproc

	.globl	gx_path_add_pgram
	.p2align	4, 0x90
	.type	gx_path_add_pgram,@function
gx_path_add_pgram:                      # @gx_path_add_pgram
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movq	%r8, %r12
	movq	%rcx, %rbp
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movb	$0, 137(%rbx)
	movb	$1, 136(%rbx)
	movq	%r15, 120(%rbx)
	movq	%r14, 128(%rbx)
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	gx_path_add_line
	testl	%eax, %eax
	js	.LBB9_4
# BB#1:
	movq	64(%rsp), %rdx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	gx_path_add_line
	testl	%eax, %eax
	js	.LBB9_4
# BB#2:
	subq	%rbp, %r15
	addq	%r13, %r15
	subq	%r12, %r14
	addq	64(%rsp), %r14
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	gx_path_add_line
	testl	%eax, %eax
	js	.LBB9_4
# BB#3:
	movq	%rbx, %rdi
	callq	gx_path_close_subpath
	movl	%eax, %ecx
	sarl	$31, %ecx
	andl	%eax, %ecx
	movl	%ecx, %eax
.LBB9_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	gx_path_add_pgram, .Lfunc_end9-gx_path_add_pgram
	.cfi_endproc

	.globl	gx_path_close_subpath
	.p2align	4, 0x90
	.type	gx_path_close_subpath,@function
gx_path_close_subpath:                  # @gx_path_close_subpath
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 32
	subq	$144, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 176
.Lcfi49:
	.cfi_offset %rbx, -32
.Lcfi50:
	.cfi_offset %r14, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	cmpb	$0, 137(%rbx)
	je	.LBB10_18
# BB#1:
	movq	96(%rbx), %rbp
	movq	120(%rbx), %rax
	cmpq	24(%rbp), %rax
	jne	.LBB10_9
# BB#2:
	movq	128(%rbx), %rax
	cmpq	32(%rbp), %rax
	jne	.LBB10_9
# BB#3:
	movq	40(%rbp), %rax
	cmpl	$1, 16(%rax)
	jne	.LBB10_9
# BB#4:
	cmpb	$0, 138(%rbx)
	je	.LBB10_8
# BB#5:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	gx_path_copy
	testl	%eax, %eax
	js	.LBB10_17
# BB#6:                                 # %path_alloc_copy.exit
	movq	%rsp, %rsi
	movl	$144, %edx
	movq	%rbx, %rdi
	callq	memcpy
	movb	$0, 138(%rbx)
	cmpq	$0, 96(%rbx)
	je	.LBB10_17
# BB#7:                                 # %path_alloc_copy.exit._crit_edge
	movq	40(%rbp), %rax
.LBB10_8:
	movl	$2, 16(%rax)
	jmp	.LBB10_14
.LBB10_9:
	cmpb	$0, 138(%rbx)
	je	.LBB10_12
# BB#10:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	gx_path_copy
	testl	%eax, %eax
	js	.LBB10_17
# BB#11:                                # %path_alloc_copy.exit39
	movq	%rsp, %rsi
	movl	$144, %edx
	movq	%rbx, %rdi
	callq	memcpy
	movb	$0, 138(%rbx)
	movq	96(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB10_17
.LBB10_12:
	movl	$1, %edi
	movl	$40, %esi
	movl	$.L.str.7, %edx
	callq	*(%rbx)
	testq	%rax, %rax
	je	.LBB10_17
# BB#13:
	movl	$2, 16(%rax)
	movq	$0, 8(%rax)
	movq	40(%rbp), %rcx
	movq	%rax, 8(%rcx)
	movq	%rcx, (%rax)
	movq	%rax, 40(%rbp)
	movq	24(%rbp), %rcx
	movq	%rcx, 120(%rbx)
	movq	%rcx, 24(%rax)
	movq	32(%rbp), %rcx
	movq	%rcx, 128(%rbx)
	movq	%rcx, 32(%rax)
	incl	48(%rbp)
	incl	108(%rbx)
.LBB10_14:
	movb	$1, 56(%rbp)
	movb	$0, 137(%rbx)
	jmp	.LBB10_18
.LBB10_17:
	movl	$-13, %r14d
.LBB10_18:
	movl	%r14d, %eax
	addq	$144, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end10:
	.size	gx_path_close_subpath, .Lfunc_end10-gx_path_close_subpath
	.cfi_endproc

	.globl	gx_path_add_curve
	.p2align	4, 0x90
	.type	gx_path_add_curve,@function
gx_path_add_curve:                      # @gx_path_add_curve
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 208
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	96(%rbx), %r15
	cmpb	$0, 137(%rbx)
	je	.LBB11_6
# BB#1:                                 # %._crit_edge
	movq	%r8, (%rsp)             # 8-byte Spill
	leaq	138(%rbx), %rcx
	cmpb	$0, (%rcx)
	je	.LBB11_4
.LBB11_2:
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	callq	gx_path_copy
	testl	%eax, %eax
	js	.LBB11_19
# BB#3:                                 # %path_alloc_copy.exit
	leaq	8(%rsp), %rsi
	movl	$144, %edx
	movq	%rbx, %rdi
	callq	memcpy
	movb	$0, 138(%rbx)
	movq	96(%rbx), %r15
	testq	%r15, %r15
	je	.LBB11_19
.LBB11_4:
	movl	$1, %edi
	movl	$72, %esi
	movl	$.L.str.6, %edx
	callq	*(%rbx)
	testq	%rax, %rax
	je	.LBB11_19
# BB#5:
	movq	208(%rsp), %rcx
	movl	$3, 16(%rax)
	movq	$0, 8(%rax)
	movq	40(%r15), %rdx
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	movq	%rax, 40(%r15)
	movq	%rbp, 40(%rax)
	movq	%r13, 48(%rax)
	movq	%r12, 56(%rax)
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%rdx, 64(%rax)
	movq	%r14, 120(%rbx)
	movq	%r14, 24(%rax)
	movq	%rcx, 128(%rbx)
	movq	%rcx, 32(%rax)
	incl	52(%r15)
	incl	108(%rbx)
	incl	112(%rbx)
	xorl	%eax, %eax
	jmp	.LBB11_20
.LBB11_6:
	cmpb	$0, 136(%rbx)
	je	.LBB11_15
# BB#7:
	movq	%r8, (%rsp)             # 8-byte Spill
	cmpb	$0, 138(%rbx)
	je	.LBB11_10
# BB#8:
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	callq	gx_path_copy
	testl	%eax, %eax
	js	.LBB11_19
# BB#9:                                 # %path_alloc_copy.exit.i
	leaq	8(%rsp), %rsi
	movl	$144, %edx
	movq	%rbx, %rdi
	callq	memcpy
	movb	$0, 138(%rbx)
	movq	96(%rbx), %r15
	testq	%r15, %r15
	je	.LBB11_19
.LBB11_10:
	movl	$1, %edi
	movl	$64, %esi
	movl	$.L.str.4, %edx
	callq	*(%rbx)
	testq	%rax, %rax
	je	.LBB11_19
# BB#11:
	movl	$0, 16(%rax)
	movq	$0, 8(%rax)
	movq	%rax, 40(%rax)
	movb	$0, 56(%rax)
	movq	$0, 48(%rax)
	movups	120(%rbx), %xmm0
	movups	%xmm0, 24(%rax)
	movb	$1, 137(%rbx)
	testq	%r15, %r15
	je	.LBB11_21
# BB#12:
	movq	40(%r15), %rdx
	leaq	8(%rdx), %rsi
	jmp	.LBB11_22
.LBB11_19:
	movl	$-13, %eax
	jmp	.LBB11_20
.LBB11_15:
	movl	$-14, %eax
.LBB11_20:                              # %.thread
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_21:
	leaq	88(%rbx), %rsi
	xorl	%edx, %edx
.LBB11_22:
	leaq	138(%rbx), %rcx
	movq	%rax, (%rsi)
	movq	%rdx, (%rax)
	movq	%rax, 96(%rbx)
	incl	104(%rbx)
	movq	%rax, %r15
	cmpb	$0, (%rcx)
	jne	.LBB11_2
	jmp	.LBB11_4
.Lfunc_end11:
	.size	gx_path_add_curve, .Lfunc_end11-gx_path_add_curve
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI12_0:
	.quad	4603129179135383962     # double 0.55000000000000004
.LCPI12_1:
	.quad	4601778099247172812     # double 0.44999999999999996
	.text
	.globl	gx_path_add_arc
	.p2align	4, 0x90
	.type	gx_path_add_arc,@function
gx_path_add_arc:                        # @gx_path_add_arc
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 16
	movq	%rcx, %r10
	cvtsi2sdq	%r9, %xmm0
	movsd	.LCPI12_0(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	16(%rsp), %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %r9
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
	movsd	.LCPI12_1(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %rsi
	addq	%rax, %rsi
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %rdx
	addq	%r9, %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%r10, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %rcx
	addq	%rax, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%r8, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %rax
	addq	%r9, %rax
	movq	%r8, (%rsp)
	movq	%rax, %r8
	movq	%r10, %r9
	callq	gx_path_add_curve
	popq	%rcx
	retq
.Lfunc_end12:
	.size	gx_path_add_arc, .Lfunc_end12-gx_path_add_arc
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s(%d): "
	.size	.L.str, 9

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Benchmarks/MallocBench/gs/gxpath.c"
	.size	.L.str.1, 85

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"bad type in gx_path_release: %x!\n"
	.size	.L.str.2, 34

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"gx_path_release"
	.size	.L.str.3, 16

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"gx_path_new_subpath"
	.size	.L.str.4, 20

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"gx_path_add_line"
	.size	.L.str.5, 17

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"gx_path_add_curve"
	.size	.L.str.6, 18

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"gx_path_close_subpath"
	.size	.L.str.7, 22

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	2
.Lswitch.table:
	.long	64                      # 0x40
	.long	40                      # 0x28
	.long	40                      # 0x28
	.long	72                      # 0x48
	.size	.Lswitch.table, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
