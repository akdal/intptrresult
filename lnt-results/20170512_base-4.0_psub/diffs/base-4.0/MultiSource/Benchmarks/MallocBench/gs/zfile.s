	.text
	.file	"zfile.bc"
	.globl	zfile_init
	.p2align	4, 0x90
	.type	zfile_init,@function
zfile_init:                             # @zfile_init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	std_files(%rip), %rdi
	movq	stdin(%rip), %rsi
	movl	$stdin_buf, %edx
	movl	$1, %ecx
	callq	sread_file
	movq	std_files+32(%rip), %rdi
	movq	stdout(%rip), %rsi
	movl	$stdout_buf, %edx
	movl	$128, %ecx
	callq	swrite_file
	movq	std_files+64(%rip), %rdi
	movq	stderr(%rip), %rsi
	movl	$stderr_buf, %edx
	movl	$128, %ecx
	callq	swrite_file
	movq	$0, std_files+16(%rip)
	movw	$32, std_files+24(%rip)
	movq	std_file_names(%rip), %rdi
	movl	$std_files+16, %esi
	movl	$.L.str.5, %edx
	xorl	%eax, %eax
	callq	string_to_ref
	testl	%eax, %eax
	js	.LBB0_1
.LBB0_2:
	movq	$0, std_files+48(%rip)
	movw	$32, std_files+56(%rip)
	movq	std_file_names+8(%rip), %rdi
	movl	$std_files+48, %esi
	movl	$.L.str.5, %edx
	xorl	%eax, %eax
	callq	string_to_ref
	testl	%eax, %eax
	js	.LBB0_3
.LBB0_4:
	movq	$0, std_files+80(%rip)
	movw	$32, std_files+88(%rip)
	movq	std_file_names+16(%rip), %rdi
	movl	$std_files+80, %esi
	movl	$.L.str.5, %edx
	xorl	%eax, %eax
	callq	string_to_ref
	testl	%eax, %eax
	js	.LBB0_5
.LBB0_6:
	movq	$0, std_files+112(%rip)
	movw	$32, std_files+120(%rip)
	movq	std_file_names+24(%rip), %rdi
	movl	$std_files+112, %esi
	movl	$.L.str.5, %edx
	xorl	%eax, %eax
	callq	string_to_ref
	testl	%eax, %eax
	js	.LBB0_7
.LBB0_8:
	movq	$0, std_files+144(%rip)
	movw	$32, std_files+152(%rip)
	movq	std_file_names+32(%rip), %rdi
	movl	$std_files+144, %esi
	movl	$.L.str.5, %edx
	xorl	%eax, %eax
	callq	string_to_ref
	testl	%eax, %eax
	js	.LBB0_10
# BB#9:
	popq	%rax
	retq
.LBB0_10:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	movl	$.L.str.7, %edx
	movl	$149, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	xorl	%eax, %eax
	popq	%rcx
	jmp	gs_exit                 # TAILCALL
.LBB0_1:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	movl	$.L.str.7, %edx
	movl	$149, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	xorl	%eax, %eax
	callq	gs_exit
	jmp	.LBB0_2
.LBB0_3:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	movl	$.L.str.7, %edx
	movl	$149, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	xorl	%eax, %eax
	callq	gs_exit
	jmp	.LBB0_4
.LBB0_5:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	movl	$.L.str.7, %edx
	movl	$149, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	xorl	%eax, %eax
	callq	gs_exit
	jmp	.LBB0_6
.LBB0_7:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	movl	$.L.str.7, %edx
	movl	$149, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	xorl	%eax, %eax
	callq	gs_exit
	jmp	.LBB0_8
.Lfunc_end0:
	.size	zfile_init, .Lfunc_end0-zfile_init
	.cfi_endproc

	.globl	zfile
	.p2align	4, 0x90
	.type	zfile,@function
zfile:                                  # @zfile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 48
.Lcfi6:
	.cfi_offset %rbx, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	-8(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$52, %ecx
	jne	.LBB1_12
# BB#1:
	movzwl	8(%rbx), %ecx
	andl	$252, %ecx
	cmpl	$52, %ecx
	jne	.LBB1_12
# BB#2:
	movl	$-9, %eax
	movzwl	10(%rbx), %ecx
	cmpl	$1, %ecx
	jne	.LBB1_12
# BB#3:
	movq	-16(%rbx), %r14
	movzwl	-6(%rbx), %r15d
	movq	(%rbx), %rcx
	movb	(%rcx), %cl
	cmpb	$114, %cl
	je	.LBB1_4
# BB#5:
	cmpb	$119, %cl
	jne	.LBB1_12
# BB#6:
	movl	$.L.str.10, %ebp
	jmp	.LBB1_7
.LBB1_4:
	movl	$.L.str.9, %ebp
.LBB1_7:
	addq	$-16, %rbx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	open_std_file
	cmpl	$-22, %eax
	je	.LBB1_10
# BB#8:
	testl	%eax, %eax
	jne	.LBB1_12
# BB#9:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
	jmp	.LBB1_12
.LBB1_10:
	movq	%r14, %rdi
	movl	%r15d, %esi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	callq	file_open
	testl	%eax, %eax
	js	.LBB1_12
# BB#11:
	addq	$-16, osp(%rip)
.LBB1_12:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	zfile, .Lfunc_end1-zfile
	.cfi_endproc

	.globl	open_std_file
	.p2align	4, 0x90
	.type	open_std_file,@function
open_std_file:                          # @open_std_file
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 80
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %rbx
	movq	(%rbx), %r12
	movzwl	10(%rbx), %r13d
	movq	std_file_names(%rip), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rcx
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	bytes_compare
	testl	%eax, %eax
	movq	%r14, 16(%rsp)          # 8-byte Spill
	je	.LBB2_1
# BB#21:
	movq	(%rbx), %r12
	movzwl	10(%rbx), %r13d
	movq	std_file_names+8(%rip), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	bytes_compare
	testl	%eax, %eax
	je	.LBB2_22
# BB#23:
	movq	(%rbx), %r12
	movzwl	10(%rbx), %r13d
	movq	std_file_names+16(%rip), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rcx
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	bytes_compare
	testl	%eax, %eax
	je	.LBB2_24
# BB#25:
	movq	(%rbx), %r12
	movzwl	10(%rbx), %r13d
	movq	std_file_names+24(%rip), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	bytes_compare
	testl	%eax, %eax
	je	.LBB2_26
# BB#27:
	movq	(%rbx), %r12
	movzwl	10(%rbx), %ebx
	movq	std_file_names+32(%rip), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	bytes_compare
	movl	%eax, %ecx
	movl	$-22, %eax
	testl	%ecx, %ecx
	jne	.LBB2_18
# BB#28:
	movl	$1, %r14d
	movl	$4, %r15d
	jmp	.LBB2_2
.LBB2_1:
	movl	$-3, %r14d
	jmp	.LBB2_2
.LBB2_22:
	movl	$-2, %r14d
	movl	$1, %r15d
	jmp	.LBB2_2
.LBB2_24:
	movl	$-1, %r14d
	movl	$2, %r15d
	jmp	.LBB2_2
.LBB2_26:
	movl	$3, %r15d
.LBB2_2:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpb	$114, (%rax)
	movl	$514, %eax              # imm = 0x202
	movl	$258, %ecx              # imm = 0x102
	cmovel	%eax, %ecx
	movl	$-7, %eax
	cmpl	std_file_attrs(,%r15,4), %ecx
	jne	.LBB2_18
# BB#3:
	movq	%r15, %rax
	shlq	$5, %rax
	leaq	std_files(%rax), %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
	orl	$12, %ecx
	movw	%cx, 8(%rax)
	xorl	%eax, %eax
	cmpl	$1, %r14d
	ja	.LBB2_18
# BB#4:
	movq	std_files(%rip), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	8(%r14), %rax
	jae	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	1(%rax), %eax
	cmpl	$-1, %eax
	jne	.LBB2_9
	jmp	.LBB2_17
	.p2align	4, 0x90
.LBB2_7:                                #   in Loop: Header=BB2_5 Depth=1
	movq	%r14, %rdi
	callq	*40(%r14)
	cmpl	$-1, %eax
	je	.LBB2_17
.LBB2_9:                                #   in Loop: Header=BB2_5 Depth=1
	cmpl	$10, %eax
	je	.LBB2_17
# BB#10:                                #   in Loop: Header=BB2_5 Depth=1
	cmpl	$13, %eax
	je	.LBB2_11
# BB#19:                                #   in Loop: Header=BB2_5 Depth=1
	movb	%al, lineedit_buf(%rbx)
	incq	%rbx
	cmpl	$160, %ebx
	jb	.LBB2_5
# BB#20:
	movl	$-15, %eax
	jmp	.LBB2_18
.LBB2_11:
	movq	(%r14), %rax
	cmpq	8(%r14), %rax
	jae	.LBB2_13
# BB#12:
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	1(%rax), %eax
	cmpl	$-1, %eax
	jne	.LBB2_15
	jmp	.LBB2_17
.LBB2_13:
	movq	%r14, %rdi
	callq	*40(%r14)
	cmpl	$-1, %eax
	je	.LBB2_17
.LBB2_15:
	cmpl	$10, %eax
	je	.LBB2_17
# BB#16:
	decq	(%r14)
.LBB2_17:                               # %.sink.split.i.loopexit.i
	imulq	$120, %r15, %rax
	leaq	std_file_streams(%rax), %rdi
	movq	%rdi, (%rbp)
	movl	$lineedit_buf, %esi
	movl	%ebx, %edx
	callq	sread_string
	xorl	%eax, %eax
.LBB2_18:                               # %zreadline_stdin.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	open_std_file, .Lfunc_end2-open_std_file
	.cfi_endproc

	.globl	zclosefile
	.p2align	4, 0x90
	.type	zclosefile,@function
zclosefile:                             # @zclosefile
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -40
.Lcfi29:
	.cfi_offset %r12, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movzwl	8(%r14), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$12, %ecx
	jne	.LBB3_15
# BB#1:
	movq	(%r14), %r12
	movq	(%r12), %rbx
	movl	$-7, %eax
	testq	%rbx, %rbx
	je	.LBB3_15
# BB#2:
	movl	8(%r12), %ecx
	testl	%ecx, %ecx
	je	.LBB3_15
# BB#3:
	cmpl	$-1, %ecx
	jne	.LBB3_5
# BB#4:
	movq	%rbx, %rdi
	callq	*80(%rbx)
	jmp	.LBB3_7
.LBB3_5:
	movq	16(%rbx), %r15
	movq	%rbx, %rdi
	callq	*80(%rbx)
	movl	%eax, %ecx
	movl	$-12, %eax
	testl	%ecx, %ecx
	jne	.LBB3_15
# BB#6:
	movl	$1, %esi
	movl	$120, %edx
	movl	$.L.str.46, %ecx
	movq	%rbx, %rdi
	callq	alloc_free
	movl	$512, %esi              # imm = 0x200
	movl	$1, %edx
	movl	$.L.str.47, %ecx
	movq	%r15, %rdi
	callq	alloc_free
.LBB3_7:
	movq	$0, (%r12)
	movq	esp(%rip), %rax
	movl	$estack, %ecx
	cmpq	%rcx, %rax
	jb	.LBB3_14
# BB#8:                                 # %.lr.ph.i.preheader
	movl	$estack, %ecx
	.p2align	4, 0x90
.LBB3_9:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	8(%rax), %edx
	andl	$253, %edx
	cmpl	$13, %edx
	je	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_9 Depth=1
	addq	$-16, %rax
	cmpq	%rcx, %rax
	jae	.LBB3_9
	jmp	.LBB3_14
.LBB3_11:                               # %get_current_file.exit
	testq	%rax, %rax
	je	.LBB3_14
# BB#12:
	movq	(%rax), %rcx
	cmpq	(%r14), %rcx
	jne	.LBB3_14
# BB#13:
	movq	$0, (%rax)
	movl	$3, 8(%rax)
.LBB3_14:                               # %get_current_file.exit.thread
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB3_15:                               # %file_close.exit.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	zclosefile, .Lfunc_end3-zclosefile
	.cfi_endproc

	.globl	zread
	.p2align	4, 0x90
	.type	zread,@function
zread:                                  # @zread
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 16
.Lcfi33:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$12, %edx
	jne	.LBB4_11
# BB#1:
	movq	(%rbx), %rax
	movq	(%rax), %rdi
	movl	$-7, %eax
	testq	%rdi, %rdi
	je	.LBB4_11
# BB#2:
	testb	$2, %ch
	je	.LBB4_11
# BB#3:
	movb	28(%rdi), %cl
	testb	%cl, %cl
	jne	.LBB4_11
# BB#4:
	movq	(%rdi), %rax
	cmpq	8(%rdi), %rax
	jae	.LBB4_8
# BB#5:                                 # %.thread
	leaq	1(%rax), %rcx
	movq	%rcx, (%rdi)
	movzbl	1(%rax), %eax
	jmp	.LBB4_6
.LBB4_8:
	callq	*40(%rdi)
	cmpl	$-1, %eax
	je	.LBB4_9
.LBB4_6:
	cltq
	movq	%rax, (%rbx)
	movw	$20, 8(%rbx)
	leaq	16(%rbx), %rax
	movq	%rax, osp(%rip)
	movw	$1, %cx
	cmpq	ostop(%rip), %rax
	jbe	.LBB4_10
# BB#7:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	popq	%rbx
	retq
.LBB4_9:
	xorl	%ecx, %ecx
	movq	%rbx, %rax
.LBB4_10:
	movw	%cx, (%rax)
	movw	$4, 8(%rax)
	xorl	%eax, %eax
.LBB4_11:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	zread, .Lfunc_end4-zread
	.cfi_endproc

	.globl	zunread
	.p2align	4, 0x90
	.type	zunread,@function
zunread:                                # @zunread
	.cfi_startproc
# BB#0:
	movq	%rdi, %rcx
	movzwl	-8(%rcx), %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$12, %edx
	jne	.LBB5_8
# BB#1:
	movq	-16(%rcx), %rax
	movq	(%rax), %rdi
	movl	$-7, %eax
	testq	%rdi, %rdi
	je	.LBB5_8
# BB#2:
	cmpb	$0, 28(%rdi)
	jne	.LBB5_8
# BB#3:                                 # %.critedge
	movzwl	8(%rcx), %eax
	andl	$252, %eax
	cmpl	$20, %eax
	movl	$-20, %eax
	jne	.LBB5_8
# BB#4:
	movq	(%rcx), %rcx
	movl	$-15, %eax
	cmpq	$255, %rcx
	ja	.LBB5_8
# BB#5:
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 16
	movzbl	%cl, %esi
	callq	sungetc
	testl	%eax, %eax
	leaq	8(%rsp), %rsp
	js	.LBB5_6
# BB#7:
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
.LBB5_8:
	retq
.LBB5_6:
	movl	$-12, %eax
	retq
.Lfunc_end5:
	.size	zunread, .Lfunc_end5-zunread
	.cfi_endproc

	.globl	zwrite
	.p2align	4, 0x90
	.type	zwrite,@function
zwrite:                                 # @zwrite
	.cfi_startproc
# BB#0:
	movq	%rdi, %rcx
	movzwl	-8(%rcx), %edx
	movl	%edx, %esi
	andl	$252, %esi
	movl	$-20, %eax
	cmpl	$12, %esi
	jne	.LBB6_10
# BB#1:
	movq	-16(%rcx), %rax
	movq	(%rax), %rdi
	movl	$-7, %eax
	testq	%rdi, %rdi
	je	.LBB6_10
# BB#2:
	testb	$1, %dh
	je	.LBB6_10
# BB#3:
	movb	28(%rdi), %dl
	testb	%dl, %dl
	je	.LBB6_10
# BB#4:
	movzwl	8(%rcx), %eax
	andl	$252, %eax
	cmpl	$20, %eax
	movl	$-20, %eax
	jne	.LBB6_10
# BB#5:
	movq	(%rcx), %rcx
	movl	$-15, %eax
	cmpq	$255, %rcx
	ja	.LBB6_10
# BB#6:
	movq	(%rdi), %rax
	cmpq	8(%rdi), %rax
	jae	.LBB6_8
# BB#7:
	leaq	1(%rax), %rdx
	movq	%rdx, (%rdi)
	movb	%cl, 1(%rax)
	jmp	.LBB6_9
.LBB6_8:
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 16
	movzbl	%cl, %esi
	callq	*48(%rdi)
	addq	$8, %rsp
.LBB6_9:
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
.LBB6_10:
	retq
.Lfunc_end6:
	.size	zwrite, .Lfunc_end6-zwrite
	.cfi_endproc

	.globl	zreadhexstring
	.p2align	4, 0x90
	.type	zreadhexstring,@function
zreadhexstring:                         # @zreadhexstring
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 32
	subq	$128, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 160
.Lcfi40:
	.cfi_offset %rbx, -32
.Lcfi41:
	.cfi_offset %r14, -24
.Lcfi42:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	leaq	-16(%rbx), %r15
	movl	$-1, 4(%rsp)
	movzwl	-8(%rbx), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	shrl	$2, %edx
	movl	$-20, %eax
	cmpb	$13, %dl
	je	.LBB7_6
# BB#1:
	cmpb	$3, %dl
	jne	.LBB7_16
# BB#2:
	cmpl	$3, %edx
	jne	.LBB7_16
# BB#3:
	movq	(%r15), %rax
	movq	(%rax), %r14
	movl	$-7, %eax
	testq	%r14, %r14
	je	.LBB7_16
# BB#4:
	testb	$2, %ch
	je	.LBB7_16
# BB#5:
	movb	28(%r14), %cl
	testb	%cl, %cl
	je	.LBB7_7
	jmp	.LBB7_16
.LBB7_6:
	movq	-16(%rbx), %rsi
	movzwl	-6(%rbx), %edx
	leaq	8(%rsp), %r14
	movq	%r14, %rdi
	callq	sread_string
.LBB7_7:
	movzwl	8(%rbx), %ecx
	movl	%ecx, %eax
	andl	$252, %eax
	cmpl	$52, %eax
	movl	$-20, %eax
	jne	.LBB7_16
# BB#8:
	movl	$-7, %eax
	testb	$1, %ch
	je	.LBB7_16
# BB#9:
	movq	(%rbx), %rsi
	movzwl	10(%rbx), %edx
	movq	%rsp, %rcx
	leaq	4(%rsp), %r8
	movq	%r14, %rdi
	callq	sreadhex
	movl	%eax, %ecx
	testl	%ecx, %ecx
	je	.LBB7_12
# BB#10:
	movl	$-12, %eax
	cmpl	$1, %ecx
	jne	.LBB7_16
# BB#11:
	movzwl	(%rsp), %eax
	movw	%ax, 10(%rbx)
	orb	$-128, 9(%rbx)
.LBB7_12:
	leaq	8(%rsp), %rax
	cmpq	%rax, %r14
	je	.LBB7_13
# BB#14:
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r15)
	jmp	.LBB7_15
.LBB7_13:
	movq	8(%rsp), %rax
	incq	%rax
	subq	24(%rsp), %rax
	addq	40(%rsp), %rax
	subw	%ax, -6(%rbx)
	movl	%eax, %eax
	addq	%rax, -16(%rbx)
	orb	$-128, -7(%rbx)
	leaq	16(%rbx), %rdx
	cmpq	ostop(%rip), %rdx
	cmovbeq	%rdx, %rbx
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	movq	%rdx, %rbx
	ja	.LBB7_16
.LBB7_15:
	movl	$1, %eax
	subl	%ecx, %eax
	movw	%ax, (%rbx)
	movw	$4, 8(%rbx)
	xorl	%eax, %eax
.LBB7_16:
	addq	$128, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	zreadhexstring, .Lfunc_end7-zreadhexstring
	.cfi_endproc

	.globl	zwritehexstring
	.p2align	4, 0x90
	.type	zwritehexstring,@function
zwritehexstring:                        # @zwritehexstring
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 48
.Lcfi48:
	.cfi_offset %rbx, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movzwl	-8(%rdi), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$12, %edx
	jne	.LBB8_19
# BB#1:
	movq	-16(%rdi), %rdx
	movq	(%rdx), %r15
	testq	%r15, %r15
	je	.LBB8_18
# BB#2:
	testb	$1, %ch
	je	.LBB8_18
# BB#3:
	movb	28(%r15), %cl
	testb	%cl, %cl
	je	.LBB8_18
# BB#4:
	movzwl	8(%rdi), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	cmpl	$52, %edx
	jne	.LBB8_19
# BB#5:
	testb	$2, %ch
	movl	$-7, %eax
	je	.LBB8_19
# BB#6:
	movzwl	10(%rdi), %r14d
	testl	%r14d, %r14d
	je	.LBB8_15
# BB#7:                                 # %.lr.ph
	movq	(%rdi), %rbp
	negl	%r14d
	.p2align	4, 0x90
.LBB8_8:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %ebx
	movq	(%r15), %rax
	cmpq	8(%r15), %rax
	jae	.LBB8_10
# BB#9:                                 #   in Loop: Header=BB8_8 Depth=1
	movq	%rbx, %rcx
	shrq	$4, %rcx
	movzbl	.L.str.11(%rcx), %ecx
	leaq	1(%rax), %rdx
	movq	%rdx, (%r15)
	movb	%cl, 1(%rax)
	jmp	.LBB8_11
	.p2align	4, 0x90
.LBB8_10:                               #   in Loop: Header=BB8_8 Depth=1
	movq	%rbx, %rax
	shrq	$4, %rax
	movzbl	.L.str.11(%rax), %esi
	movq	%r15, %rdi
	callq	*48(%r15)
.LBB8_11:                               #   in Loop: Header=BB8_8 Depth=1
	movq	(%r15), %rax
	cmpq	8(%r15), %rax
	jae	.LBB8_13
# BB#12:                                #   in Loop: Header=BB8_8 Depth=1
	andl	$15, %ebx
	movzbl	.L.str.11(%rbx), %ecx
	leaq	1(%rax), %rdx
	movq	%rdx, (%r15)
	movb	%cl, 1(%rax)
	jmp	.LBB8_14
	.p2align	4, 0x90
.LBB8_13:                               #   in Loop: Header=BB8_8 Depth=1
	andl	$15, %ebx
	movzbl	.L.str.11(%rbx), %esi
	movq	%r15, %rdi
	callq	*48(%r15)
.LBB8_14:                               # %.backedge
                                        #   in Loop: Header=BB8_8 Depth=1
	incq	%rbp
	incl	%r14d
	jne	.LBB8_8
.LBB8_15:                               # %._crit_edge
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
	jmp	.LBB8_19
.LBB8_18:
	movl	$-7, %eax
.LBB8_19:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	zwritehexstring, .Lfunc_end8-zwritehexstring
	.cfi_endproc

	.globl	zreadstring
	.p2align	4, 0x90
	.type	zreadstring,@function
zreadstring:                            # @zreadstring
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 32
.Lcfi55:
	.cfi_offset %rbx, -32
.Lcfi56:
	.cfi_offset %r14, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	-8(%rbx), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$12, %edx
	jne	.LBB9_10
# BB#1:
	leaq	-16(%rbx), %rbp
	movq	(%rbp), %rdx
	movq	(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_9
# BB#2:
	testb	$2, %ch
	je	.LBB9_9
# BB#3:
	movb	28(%rdi), %cl
	testb	%cl, %cl
	jne	.LBB9_9
# BB#4:
	movzwl	8(%rbx), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	cmpl	$52, %edx
	jne	.LBB9_10
# BB#5:
	testb	$1, %ch
	movl	$-7, %eax
	je	.LBB9_10
# BB#6:
	movzwl	10(%rbx), %r14d
	movq	(%rbx), %rsi
	movl	%r14d, %edx
	callq	sgets
	movw	%ax, 10(%rbx)
	orb	$-128, 9(%rbx)
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rbp)
	xorl	%ecx, %ecx
	cmpl	%r14d, %eax
	sete	%cl
	movw	%cx, (%rbx)
	movw	$4, 8(%rbx)
	xorl	%eax, %eax
	jmp	.LBB9_10
.LBB9_9:
	movl	$-7, %eax
.LBB9_10:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end9:
	.size	zreadstring, .Lfunc_end9-zreadstring
	.cfi_endproc

	.globl	zwritestring
	.p2align	4, 0x90
	.type	zwritestring,@function
zwritestring:                           # @zwritestring
	.cfi_startproc
# BB#0:
	movzwl	-8(%rdi), %edx
	movl	%edx, %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$12, %ecx
	jne	.LBB10_11
# BB#1:
	movq	-16(%rdi), %rcx
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB10_2
# BB#3:
	testb	$1, %dh
	je	.LBB10_4
# BB#5:
	movb	28(%rcx), %dl
	testb	%dl, %dl
	je	.LBB10_6
# BB#7:
	movzwl	8(%rdi), %edx
	movl	%edx, %esi
	andl	$252, %esi
	cmpl	$52, %esi
	jne	.LBB10_11
# BB#8:
	testb	$2, %dh
	movl	$-7, %eax
	je	.LBB10_11
# BB#9:
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 16
.Lcfi59:
	.cfi_offset %rbx, -16
	movzwl	10(%rdi), %ebx
	movq	(%rdi), %rsi
	movq	%rcx, %rdi
	movl	%ebx, %edx
	callq	sputs
	movl	%eax, %ecx
	movl	$-12, %eax
	cmpl	%ebx, %ecx
	popq	%rbx
	jne	.LBB10_11
# BB#10:                                # %write_string.exit
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
.LBB10_11:                              # %write_string.exit.thread
	retq
.LBB10_2:
	movl	$-7, %eax
	retq
.LBB10_4:
	movl	$-7, %eax
	retq
.LBB10_6:
	movl	$-7, %eax
	retq
.Lfunc_end10:
	.size	zwritestring, .Lfunc_end10-zwritestring
	.cfi_endproc

	.globl	write_string
	.p2align	4, 0x90
	.type	write_string,@function
write_string:                           # @write_string
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$52, %edx
	jne	.LBB11_3
# BB#1:
	movl	$-7, %eax
	testb	$2, %ch
	je	.LBB11_3
# BB#2:
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 16
.Lcfi61:
	.cfi_offset %rbx, -16
	movzwl	10(%rdi), %ebx
	movq	(%rdi), %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	movl	%ebx, %edx
	callq	sputs
	xorl	%ecx, %ecx
	cmpl	%ebx, %eax
	movl	$-12, %eax
	cmovel	%ecx, %eax
	popq	%rbx
.LBB11_3:
	retq
.Lfunc_end11:
	.size	write_string, .Lfunc_end11-write_string
	.cfi_endproc

	.globl	zreadline
	.p2align	4, 0x90
	.type	zreadline,@function
zreadline:                              # @zreadline
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 64
.Lcfi69:
	.cfi_offset %rbx, -56
.Lcfi70:
	.cfi_offset %r12, -48
.Lcfi71:
	.cfi_offset %r13, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movzwl	-8(%r14), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	movl	$-20, %ebp
	cmpl	$12, %ecx
	jne	.LBB12_17
# BB#1:
	leaq	-16(%r14), %rdx
	movq	(%rdx), %rcx
	movq	(%rcx), %r12
	testq	%r12, %r12
	je	.LBB12_16
# BB#2:
	testb	$2, %ah
	je	.LBB12_16
# BB#3:
	movb	28(%r12), %al
	testb	%al, %al
	jne	.LBB12_16
# BB#4:
	movzwl	8(%r14), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	cmpl	$52, %ecx
	jne	.LBB12_17
# BB#5:
	testb	$1, %ah
	movl	$-7, %ebp
	je	.LBB12_17
# BB#6:
	movzwl	10(%r14), %r15d
	testl	%r15d, %r15d
	movl	$-15, %ebp
	je	.LBB12_17
# BB#7:                                 # %.lr.ph.i
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	(%r14), %r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_8:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	cmpq	8(%r12), %rax
	jae	.LBB12_10
# BB#9:                                 #   in Loop: Header=BB12_8 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movzbl	1(%rax), %eax
	cmpl	$-1, %eax
	jne	.LBB12_11
	jmp	.LBB12_18
.LBB12_10:                              #   in Loop: Header=BB12_8 Depth=1
	movq	%r12, %rdi
	callq	*40(%r12)
	cmpl	$-1, %eax
	je	.LBB12_18
.LBB12_11:                              #   in Loop: Header=BB12_8 Depth=1
	cmpl	$10, %eax
	je	.LBB12_19
# BB#12:                                #   in Loop: Header=BB12_8 Depth=1
	cmpl	$13, %eax
	je	.LBB12_22
# BB#13:                                #   in Loop: Header=BB12_8 Depth=1
	movb	%al, (%r13,%rbx)
	incq	%rbx
	cmpl	%r15d, %ebx
	jb	.LBB12_8
	jmp	.LBB12_17
.LBB12_16:
	movl	$-7, %ebp
.LBB12_17:                              # %zreadline_from.exit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_18:                              # %.sink.split.loopexit.i.loopexit
	xorl	%ecx, %ecx
	jmp	.LBB12_20
.LBB12_19:                              # %.sink.split.loopexit.i.loopexit48
	movw	$1, %cx
.LBB12_20:                              # %.sink.split.loopexit.i
	movq	(%rsp), %rdx            # 8-byte Reload
.LBB12_21:                              # %.sink.split.loopexit.i
	movw	%bx, 10(%r14)
	orb	$-128, 9(%r14)
	movups	(%r14), %xmm0
	movups	%xmm0, (%rdx)
	movw	%cx, (%r14)
	movw	$4, 8(%r14)
	xorl	%ebp, %ebp
	jmp	.LBB12_17
.LBB12_22:
	movq	(%r12), %rax
	cmpq	8(%r12), %rax
	jae	.LBB12_24
# BB#23:
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movzbl	1(%rax), %eax
	jmp	.LBB12_25
.LBB12_24:
	movq	%r12, %rdi
	callq	*40(%r12)
.LBB12_25:
	movq	(%rsp), %rdx            # 8-byte Reload
	movw	$1, %cx
	cmpl	$-1, %eax
	je	.LBB12_21
# BB#26:
	cmpl	$10, %eax
	je	.LBB12_21
# BB#27:
	decq	(%r12)
	jmp	.LBB12_21
.Lfunc_end12:
	.size	zreadline, .Lfunc_end12-zreadline
	.cfi_endproc

	.globl	zreadline_from
	.p2align	4, 0x90
	.type	zreadline_from,@function
zreadline_from:                         # @zreadline_from
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi79:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi81:
	.cfi_def_cfa_offset 64
.Lcfi82:
	.cfi_offset %rbx, -56
.Lcfi83:
	.cfi_offset %r12, -48
.Lcfi84:
	.cfi_offset %r13, -40
.Lcfi85:
	.cfi_offset %r14, -32
.Lcfi86:
	.cfi_offset %r15, -24
.Lcfi87:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movl	%esi, %r13d
	movq	%rdi, %r12
	movl	$-15, %r15d
	testl	%r13d, %r13d
	je	.LBB13_18
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB13_4
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	cmpl	$-1, %eax
	jne	.LBB13_6
	jmp	.LBB13_16
	.p2align	4, 0x90
.LBB13_4:                               #   in Loop: Header=BB13_2 Depth=1
	movq	%rbx, %rdi
	callq	*40(%rbx)
	cmpl	$-1, %eax
	je	.LBB13_16
.LBB13_6:                               #   in Loop: Header=BB13_2 Depth=1
	cmpl	$10, %eax
	je	.LBB13_15
# BB#7:                                 #   in Loop: Header=BB13_2 Depth=1
	cmpl	$13, %eax
	je	.LBB13_8
# BB#14:                                #   in Loop: Header=BB13_2 Depth=1
	movb	%al, (%r12,%rbp)
	incq	%rbp
	cmpl	%r13d, %ebp
	jb	.LBB13_2
	jmp	.LBB13_18
.LBB13_16:                              # %.sink.split.loopexit
	xorl	%r15d, %r15d
	jmp	.LBB13_17
.LBB13_15:                              # %.sink.split.loopexit48
	movl	$1, %r15d
	jmp	.LBB13_17
.LBB13_8:
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB13_10
# BB#9:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB13_11
.LBB13_10:
	movq	%rbx, %rdi
	callq	*40(%rbx)
.LBB13_11:
	movl	$1, %r15d
	cmpl	$-1, %eax
	je	.LBB13_17
# BB#12:
	cmpl	$10, %eax
	je	.LBB13_17
# BB#13:
	decq	(%rbx)
.LBB13_17:                              # %.sink.split
	movl	%ebp, (%r14)
.LBB13_18:                              # %.loopexit
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	zreadline_from, .Lfunc_end13-zreadline_from
	.cfi_endproc

	.globl	zreadline_stdin
	.p2align	4, 0x90
	.type	zreadline_stdin,@function
zreadline_stdin:                        # @zreadline_stdin
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi91:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi92:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 64
.Lcfi95:
	.cfi_offset %rbx, -56
.Lcfi96:
	.cfi_offset %r12, -48
.Lcfi97:
	.cfi_offset %r13, -40
.Lcfi98:
	.cfi_offset %r14, -32
.Lcfi99:
	.cfi_offset %r15, -24
.Lcfi100:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r13d
	movq	%rdi, %r12
	movl	$-15, %r15d
	testl	%r13d, %r13d
	je	.LBB14_18
# BB#1:                                 # %.lr.ph.i
	movq	std_files(%rip), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB14_4
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	cmpl	$-1, %eax
	jne	.LBB14_6
	jmp	.LBB14_15
	.p2align	4, 0x90
.LBB14_4:                               #   in Loop: Header=BB14_2 Depth=1
	movq	%rbx, %rdi
	callq	*40(%rbx)
	cmpl	$-1, %eax
	je	.LBB14_15
.LBB14_6:                               #   in Loop: Header=BB14_2 Depth=1
	cmpl	$10, %eax
	je	.LBB14_16
# BB#7:                                 #   in Loop: Header=BB14_2 Depth=1
	cmpl	$13, %eax
	je	.LBB14_8
# BB#14:                                #   in Loop: Header=BB14_2 Depth=1
	movb	%al, (%r12,%rbp)
	incq	%rbp
	cmpl	%r13d, %ebp
	jb	.LBB14_2
	jmp	.LBB14_18
.LBB14_15:                              # %.sink.split.i.loopexit15
	xorl	%r15d, %r15d
	jmp	.LBB14_17
.LBB14_16:                              # %.sink.split.i.loopexit
	movl	$1, %r15d
	jmp	.LBB14_17
.LBB14_8:
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB14_10
# BB#9:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB14_11
.LBB14_10:
	movq	%rbx, %rdi
	callq	*40(%rbx)
.LBB14_11:
	movl	$1, %r15d
	cmpl	$-1, %eax
	je	.LBB14_17
# BB#12:
	cmpl	$10, %eax
	je	.LBB14_17
# BB#13:
	decq	(%rbx)
.LBB14_17:                              # %.sink.split.i
	movl	%ebp, (%r14)
.LBB14_18:                              # %zreadline_from.exit
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	zreadline_stdin, .Lfunc_end14-zreadline_stdin
	.cfi_endproc

	.globl	ztoken_file
	.p2align	4, 0x90
	.type	ztoken_file,@function
ztoken_file:                            # @ztoken_file
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi103:
	.cfi_def_cfa_offset 48
.Lcfi104:
	.cfi_offset %rbx, -24
.Lcfi105:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	movl	$-20, %ebp
	cmpl	$12, %ecx
	jne	.LBB15_10
# BB#1:
	movq	(%rbx), %rcx
	movq	(%rcx), %rdi
	movl	$-7, %ebp
	testq	%rdi, %rdi
	je	.LBB15_10
# BB#2:
	testb	$2, %ah
	je	.LBB15_10
# BB#3:
	movb	28(%rdi), %al
	testb	%al, %al
	jne	.LBB15_10
# BB#4:
	xorl	%ebp, %ebp
	leaq	8(%rsp), %rdx
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	scan_token
	cmpl	$1, %eax
	je	.LBB15_9
# BB#5:
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.LBB15_10
# BB#6:
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%rbx)
	leaq	16(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB15_8
# BB#7:
	movq	%rbx, osp(%rip)
	movl	$-16, %ebp
	jmp	.LBB15_10
.LBB15_9:
	movw	$0, (%rbx)
	movw	$4, 8(%rbx)
	jmp	.LBB15_10
.LBB15_8:
	movw	$1, 16(%rbx)
	movw	$4, 24(%rbx)
	xorl	%ebp, %ebp
.LBB15_10:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end15:
	.size	ztoken_file, .Lfunc_end15-ztoken_file
	.cfi_endproc

	.globl	zbytesavailable
	.p2align	4, 0x90
	.type	zbytesavailable,@function
zbytesavailable:                        # @zbytesavailable
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi107:
	.cfi_def_cfa_offset 32
.Lcfi108:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$12, %ecx
	jne	.LBB16_6
# BB#1:
	movq	(%rbx), %rax
	movq	(%rax), %rdi
	movl	$-7, %eax
	testq	%rdi, %rdi
	je	.LBB16_6
# BB#2:
	cmpb	$0, 28(%rdi)
	jne	.LBB16_6
# BB#3:                                 # %.critedge
	leaq	8(%rsp), %rsi
	callq	*56(%rdi)
	testl	%eax, %eax
	js	.LBB16_4
# BB#5:
	movq	8(%rsp), %rax
	movq	%rax, (%rbx)
	movw	$20, 8(%rbx)
	xorl	%eax, %eax
	jmp	.LBB16_6
.LBB16_4:
	movl	$-12, %eax
.LBB16_6:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end16:
	.size	zbytesavailable, .Lfunc_end16-zbytesavailable
	.cfi_endproc

	.globl	zflush
	.p2align	4, 0x90
	.type	zflush,@function
zflush:                                 # @zflush
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi109:
	.cfi_def_cfa_offset 16
	movq	std_files+32(%rip), %rdi
	callq	*72(%rdi)
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end17:
	.size	zflush, .Lfunc_end17-zflush
	.cfi_endproc

	.globl	zflushfile
	.p2align	4, 0x90
	.type	zflushfile,@function
zflushfile:                             # @zflushfile
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 16
.Lcfi111:
	.cfi_offset %rbx, -16
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$12, %ecx
	jne	.LBB18_6
# BB#1:
	movq	(%rdi), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB18_2
# BB#3:
	movq	%rbx, %rdi
	callq	*72(%rbx)
	cmpb	$0, 28(%rbx)
	jne	.LBB18_5
# BB#4:
	movq	96(%rbx), %rdi
	xorl	%esi, %esi
	movl	$2, %edx
	callq	fseek
.LBB18_5:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB18_6:
	popq	%rbx
	retq
.LBB18_2:
	movl	$-7, %eax
	popq	%rbx
	retq
.Lfunc_end18:
	.size	zflushfile, .Lfunc_end18-zflushfile
	.cfi_endproc

	.globl	zresetfile
	.p2align	4, 0x90
	.type	zresetfile,@function
zresetfile:                             # @zresetfile
	.cfi_startproc
# BB#0:
	movl	$-21, %eax
	retq
.Lfunc_end19:
	.size	zresetfile, .Lfunc_end19-zresetfile
	.cfi_endproc

	.globl	zstatus
	.p2align	4, 0x90
	.type	zstatus,@function
zstatus:                                # @zstatus
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$12, %ecx
	jne	.LBB20_2
# BB#1:
	movq	(%rdi), %rax
	xorl	%ecx, %ecx
	cmpq	$0, (%rax)
	setne	%cl
	movw	%cx, (%rdi)
	movw	$4, 8(%rdi)
	xorl	%eax, %eax
.LBB20_2:
	retq
.Lfunc_end20:
	.size	zstatus, .Lfunc_end20-zstatus
	.cfi_endproc

	.globl	zrun
	.p2align	4, 0x90
	.type	zrun,@function
zrun:                                   # @zrun
	.cfi_startproc
# BB#0:
	movl	$-21, %eax
	retq
.Lfunc_end21:
	.size	zrun, .Lfunc_end21-zrun
	.cfi_endproc

	.globl	zcurrentfile
	.p2align	4, 0x90
	.type	zcurrentfile,@function
zcurrentfile:                           # @zcurrentfile
	.cfi_startproc
# BB#0:
	leaq	16(%rdi), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB22_2
# BB#1:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB22_2:
	movq	esp(%rip), %rcx
	movl	$estack, %edx
	cmpq	%rdx, %rcx
	jb	.LBB22_7
# BB#3:                                 # %.lr.ph.i.preheader
	movl	$estack, %edx
	.p2align	4, 0x90
.LBB22_4:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	8(%rcx), %esi
	andl	$253, %esi
	cmpl	$13, %esi
	je	.LBB22_6
# BB#5:                                 #   in Loop: Header=BB22_4 Depth=1
	addq	$-16, %rcx
	cmpq	%rdx, %rcx
	jae	.LBB22_4
	jmp	.LBB22_7
.LBB22_6:                               # %get_current_file.exit
	testq	%rcx, %rcx
	je	.LBB22_7
# BB#8:
	movups	(%rcx), %xmm0
	movups	%xmm0, (%rax)
	movzwl	24(%rdi), %eax
	addq	$24, %rdi
	andl	$65534, %eax            # imm = 0xFFFE
	jmp	.LBB22_9
.LBB22_7:                               # %get_current_file.exit.thread
	movq	$invalid_file_entry, 16(%rdi)
	movw	$12, 24(%rdi)
	addq	$24, %rdi
	movw	$12, %ax
.LBB22_9:
	movw	%ax, (%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end22:
	.size	zcurrentfile, .Lfunc_end22-zcurrentfile
	.cfi_endproc

	.globl	zprint
	.p2align	4, 0x90
	.type	zprint,@function
zprint:                                 # @zprint
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$52, %edx
	jne	.LBB23_4
# BB#1:
	movl	$-7, %eax
	testb	$2, %ch
	je	.LBB23_4
# BB#2:
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 16
.Lcfi113:
	.cfi_offset %rbx, -16
	movq	std_files+32(%rip), %rax
	movzwl	10(%rdi), %ebx
	movq	(%rdi), %rsi
	movq	%rax, %rdi
	movl	%ebx, %edx
	callq	sputs
	movl	%eax, %ecx
	movl	$-12, %eax
	cmpl	%ebx, %ecx
	popq	%rbx
	jne	.LBB23_4
# BB#3:                                 # %write_string.exit
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB23_4:                               # %write_string.exit.thread
	retq
.Lfunc_end23:
	.size	zprint, .Lfunc_end23-zprint
	.cfi_endproc

	.globl	zecho
	.p2align	4, 0x90
	.type	zecho,@function
zecho:                                  # @zecho
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$4, %ecx
	jne	.LBB24_2
# BB#1:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB24_2:
	retq
.Lfunc_end24:
	.size	zecho, .Lfunc_end24-zecho
	.cfi_endproc

	.globl	zsetfileposition
	.p2align	4, 0x90
	.type	zsetfileposition,@function
zsetfileposition:                       # @zsetfileposition
	.cfi_startproc
# BB#0:
	movzwl	-8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$12, %ecx
	jne	.LBB25_7
# BB#1:
	movq	-16(%rdi), %rcx
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB25_2
# BB#3:
	movzwl	8(%rdi), %edx
	andl	$252, %edx
	cmpl	$20, %edx
	jne	.LBB25_7
# BB#4:
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 16
	movq	(%rdi), %rsi
	movq	%rcx, %rdi
	callq	*64(%rcx)
	testl	%eax, %eax
	leaq	8(%rsp), %rsp
	js	.LBB25_5
# BB#6:
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
.LBB25_7:
	retq
.LBB25_2:
	movl	$-7, %eax
	retq
.LBB25_5:
	movl	$-12, %eax
	retq
.Lfunc_end25:
	.size	zsetfileposition, .Lfunc_end25-zsetfileposition
	.cfi_endproc

	.globl	zfileposition
	.p2align	4, 0x90
	.type	zfileposition,@function
zfileposition:                          # @zfileposition
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$12, %ecx
	jne	.LBB26_6
# BB#1:
	movq	(%rdi), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB26_2
# BB#3:
	movq	32(%rax), %rcx
	testq	%rcx, %rcx
	js	.LBB26_4
# BB#5:
	movq	(%rax), %rdx
	subq	16(%rax), %rcx
	leaq	1(%rcx,%rdx), %rax
	movq	%rax, (%rdi)
	movw	$20, 8(%rdi)
	xorl	%eax, %eax
.LBB26_6:
	retq
.LBB26_2:
	movl	$-7, %eax
	retq
.LBB26_4:
	movl	$-12, %eax
	retq
.Lfunc_end26:
	.size	zfileposition, .Lfunc_end26-zfileposition
	.cfi_endproc

	.globl	zdeletefile
	.p2align	4, 0x90
	.type	zdeletefile,@function
zdeletefile:                            # @zdeletefile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 32
.Lcfi118:
	.cfi_offset %rbx, -32
.Lcfi119:
	.cfi_offset %r14, -24
.Lcfi120:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$52, %edx
	jne	.LBB27_6
# BB#1:
	movl	$-7, %eax
	testb	$2, %ch
	je	.LBB27_6
# BB#2:
	movl	$.L.str.12, %esi
	movq	%rbx, %rdi
	callq	ref_to_string
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB27_3
# BB#4:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	unlink
	movl	%eax, %ebp
	movzwl	10(%rbx), %esi
	incl	%esi
	movl	$1, %edx
	movl	$.L.str.12, %ecx
	movq	%r14, %rdi
	callq	alloc_free
	movl	$-12, %eax
	testl	%ebp, %ebp
	jne	.LBB27_6
# BB#5:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
	jmp	.LBB27_6
.LBB27_3:
	movl	$-25, %eax
.LBB27_6:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end27:
	.size	zdeletefile, .Lfunc_end27-zdeletefile
	.cfi_endproc

	.globl	zrenamefile
	.p2align	4, 0x90
	.type	zrenamefile,@function
zrenamefile:                            # @zrenamefile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 32
.Lcfi124:
	.cfi_offset %rbx, -32
.Lcfi125:
	.cfi_offset %r14, -24
.Lcfi126:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	movl	$-20, %ebp
	cmpl	$52, %ecx
	jne	.LBB28_13
# BB#1:
	testb	$2, %ah
	jne	.LBB28_3
# BB#2:
	movl	$-7, %ebp
	jmp	.LBB28_13
.LBB28_3:
	movzwl	-8(%rbx), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	cmpl	$52, %ecx
	jne	.LBB28_13
# BB#4:
	testb	$2, %ah
	movl	$-7, %ebp
	je	.LBB28_13
# BB#5:
	leaq	-16(%rbx), %rdi
	movl	$.L.str.13, %esi
	callq	ref_to_string
	movq	%rax, %rbp
	movl	$.L.str.14, %esi
	movq	%rbx, %rdi
	callq	ref_to_string
	movq	%rax, %r14
	testq	%rbp, %rbp
	je	.LBB28_9
# BB#6:
	testq	%r14, %r14
	je	.LBB28_9
# BB#7:
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	rename
	testl	%eax, %eax
	jne	.LBB28_9
# BB#8:
	addq	$-32, osp(%rip)
.LBB28_9:
	testq	%rbp, %rbp
	je	.LBB28_11
# BB#10:
	movzwl	-6(%rbx), %esi
	incl	%esi
	movl	$1, %edx
	movl	$.L.str.13, %ecx
	movq	%rbp, %rdi
	callq	alloc_free
.LBB28_11:
	xorl	%ebp, %ebp
	testq	%r14, %r14
	je	.LBB28_13
# BB#12:
	movzwl	10(%rbx), %esi
	incl	%esi
	movl	$1, %edx
	movl	$.L.str.14, %ecx
	movq	%r14, %rdi
	callq	alloc_free
.LBB28_13:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end28:
	.size	zrenamefile, .Lfunc_end28-zrenamefile
	.cfi_endproc

	.globl	zfilename
	.p2align	4, 0x90
	.type	zfilename,@function
zfilename:                              # @zfilename
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$12, %ecx
	jne	.LBB29_4
# BB#1:
	movq	(%rdi), %rax
	cmpq	$0, (%rax)
	je	.LBB29_2
# BB#3:
	movups	16(%rax), %xmm0
	movups	%xmm0, (%rdi)
	xorl	%eax, %eax
.LBB29_4:
	retq
.LBB29_2:
	movl	$-7, %eax
	retq
.Lfunc_end29:
	.size	zfilename, .Lfunc_end29-zfilename
	.cfi_endproc

	.globl	zfindlibfile
	.p2align	4, 0x90
	.type	zfindlibfile,@function
zfindlibfile:                           # @zfindlibfile
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 16
.Lcfi128:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$52, %ecx
	jne	.LBB30_9
# BB#1:
	movl	$.L.str.9, %esi
	movq	%rbx, %rdi
	movq	%rbx, %rdx
	callq	open_std_file
	cmpl	$-22, %eax
	je	.LBB30_6
# BB#2:
	testl	%eax, %eax
	jne	.LBB30_9
# BB#3:
	leaq	16(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	ja	.LBB30_4
# BB#5:
	movw	$1, 16(%rbx)
	jmp	.LBB30_8
.LBB30_6:
	movq	(%rbx), %rdi
	movzwl	10(%rbx), %esi
	movq	%rbx, %rdx
	callq	lib_file_open
	leaq	16(%rbx), %rcx
	movq	%rcx, osp(%rip)
	cmpq	ostop(%rip), %rcx
	jbe	.LBB30_7
.LBB30_4:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	popq	%rbx
	retq
.LBB30_7:
	shrl	$31, %eax
	xorl	$1, %eax
	movw	%ax, 16(%rbx)
.LBB30_8:
	movw	$4, 24(%rbx)
	xorl	%eax, %eax
.LBB30_9:
	popq	%rbx
	retq
.Lfunc_end30:
	.size	zfindlibfile, .Lfunc_end30-zfindlibfile
	.cfi_endproc

	.globl	lib_file_open
	.p2align	4, 0x90
	.type	lib_file_open,@function
lib_file_open:                          # @lib_file_open
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi132:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi133:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 56
	subq	$4168, %rsp             # imm = 0x1048
.Lcfi135:
	.cfi_def_cfa_offset 4224
.Lcfi136:
	.cfi_offset %rbx, -56
.Lcfi137:
	.cfi_offset %r12, -48
.Lcfi138:
	.cfi_offset %r13, -40
.Lcfi139:
	.cfi_offset %r14, -32
.Lcfi140:
	.cfi_offset %r15, -24
.Lcfi141:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %r15d
	movq	%rdi, %rbp
	movl	$.L.str.9, %edx
	movq	%rbx, %rcx
	callq	file_open
	movl	%eax, %r12d
	testl	%r12d, %r12d
	js	.LBB31_2
# BB#1:
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	jmp	.LBB31_19
.LBB31_2:
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	movl	%r15d, %esi
	callq	gp_file_name_is_absolute
	movl	$-22, 12(%rsp)          # 4-byte Folded Spill
	testl	%eax, %eax
	je	.LBB31_3
.LBB31_19:                              # %.loopexit
	movl	12(%rsp), %eax          # 4-byte Reload
	addq	$4168, %rsp             # imm = 0x1048
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB31_3:
	movq	gs_lib_paths(%rip), %rax
	movq	(%rax), %r14
	testq	%r14, %r14
	je	.LBB31_4
# BB#5:                                 # %.preheader.lr.ph
	movl	%r15d, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	negq	%rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
                                        # implicit-def: %ECX
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	movl	%r15d, 20(%rsp)         # 4-byte Spill
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB31_6:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_7 Depth 2
                                        #       Child Loop BB31_8 Depth 3
	movq	%rax, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB31_7:                               #   Parent Loop BB31_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB31_8 Depth 3
	movb	gp_file_name_list_separator(%rip), %al
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB31_8:                               #   Parent Loop BB31_6 Depth=1
                                        #     Parent Loop BB31_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbx), %ecx
	incq	%rbx
	testb	%cl, %cl
	je	.LBB31_10
# BB#9:                                 #   in Loop: Header=BB31_8 Depth=3
	cmpb	%al, %cl
	jne	.LBB31_8
.LBB31_10:                              #   in Loop: Header=BB31_7 Depth=2
	leaq	-1(%rbx), %r13
	subq	%r14, %r13
	movq	%r14, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	movl	%r15d, %ecx
	callq	gp_file_name_concat_string
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%r13, %r12
	addq	24(%rsp), %r12          # 8-byte Folded Reload
	addq	%rax, %r12
	cmpl	$4096, %r12d            # imm = 0x1000
	jg	.LBB31_13
# BB#11:                                #   in Loop: Header=BB31_7 Depth=2
	movl	%r13d, %ebp
	leaq	64(%rsp), %r13
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	64(%rsp,%rbp), %rdi
	movq	%r15, %rsi
	callq	strcpy
	movslq	%r12d, %rax
	leaq	64(%rsp,%rax), %rdi
	addq	48(%rsp), %rdi          # 8-byte Folded Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	memcpy
	movl	$.L.str.9, %edx
	movq	%r13, %rdi
	movl	%r12d, %esi
	movq	56(%rsp), %rcx          # 8-byte Reload
	callq	file_open
	movl	$1, %edx
	movl	%eax, 12(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	js	.LBB31_13
# BB#12:                                #   in Loop: Header=BB31_7 Depth=2
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jmp	.LBB31_14
	.p2align	4, 0x90
.LBB31_13:                              #   in Loop: Header=BB31_7 Depth=2
	xorl	%eax, %eax
	cmpb	$0, -1(%rbx)
	sete	%al
	leal	(%rax,%rax,4), %edx
	cmovneq	%rbx, %r14
.LBB31_14:                              #   in Loop: Header=BB31_7 Depth=2
	movl	%edx, %ecx
	andb	$7, %cl
	movl	20(%rsp), %r15d         # 4-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB31_7
# BB#15:                                #   in Loop: Header=BB31_6 Depth=1
	cmpb	$5, %cl
	je	.LBB31_18
# BB#16:                                #   in Loop: Header=BB31_6 Depth=1
	testl	%edx, %edx
	jne	.LBB31_17
.LBB31_18:                              # %.thread
                                        #   in Loop: Header=BB31_6 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %r14
	addq	$8, %rax
	testq	%r14, %r14
	jne	.LBB31_6
	jmp	.LBB31_19
.LBB31_4:
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	jmp	.LBB31_19
.LBB31_17:
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB31_19
.Lfunc_end31:
	.size	lib_file_open, .Lfunc_end31-lib_file_open
	.cfi_endproc

	.globl	zwriteppmfile
	.p2align	4, 0x90
	.type	zwriteppmfile,@function
zwriteppmfile:                          # @zwriteppmfile
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi144:
	.cfi_def_cfa_offset 32
.Lcfi145:
	.cfi_offset %rbx, -24
.Lcfi146:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movzwl	-8(%rbx), %ecx
	movl	%ecx, %eax
	andl	$252, %eax
	cmpl	$12, %eax
	jne	.LBB32_8
# BB#1:
	movq	-16(%rbx), %rax
	movq	(%rax), %r14
	movl	$-7, %eax
	testq	%r14, %r14
	je	.LBB32_9
# BB#2:
	testb	$1, %ch
	je	.LBB32_9
# BB#3:
	movb	28(%r14), %cl
	testb	%cl, %cl
	je	.LBB32_9
# BB#4:
	movzwl	8(%rbx), %eax
	andl	$252, %eax
	cmpl	$60, %eax
	jne	.LBB32_8
# BB#5:
	movq	(%rbx), %rdi
	callq	gs_device_is_memory
	testl	%eax, %eax
	movl	$-20, %eax
	je	.LBB32_9
# BB#6:
	movq	%r14, %rdi
	callq	*72(%r14)
	movq	(%rbx), %rdi
	movq	96(%r14), %rsi
	xorl	%eax, %eax
	callq	gs_writeppmfile
	testl	%eax, %eax
	js	.LBB32_9
# BB#7:
	addq	$-32, osp(%rip)
	jmp	.LBB32_9
.LBB32_8:
	movl	$-20, %eax
.LBB32_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end32:
	.size	zwriteppmfile, .Lfunc_end32-zwriteppmfile
	.cfi_endproc

	.globl	ztype1decryptfile
	.p2align	4, 0x90
	.type	ztype1decryptfile,@function
ztype1decryptfile:                      # @ztype1decryptfile
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi147:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi148:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi149:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi151:
	.cfi_def_cfa_offset 64
.Lcfi152:
	.cfi_offset %rbx, -40
.Lcfi153:
	.cfi_offset %r12, -32
.Lcfi154:
	.cfi_offset %r14, -24
.Lcfi155:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movzwl	-8(%rbx), %eax
	andl	$252, %eax
	movl	$-20, %r14d
	cmpl	$20, %eax
	jne	.LBB33_8
# BB#1:
	leaq	-16(%rbx), %r12
	movq	(%r12), %rax
	movzwl	%ax, %r15d
	movl	$-15, %r14d
	cmpq	%r15, %rax
	jne	.LBB33_8
# BB#2:
	movzwl	8(%rbx), %eax
	andl	$252, %eax
	cmpl	$12, %eax
	movl	$-20, %r14d
	jne	.LBB33_8
# BB#3:
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movl	$-7, %r14d
	testq	%rax, %rax
	je	.LBB33_8
# BB#4:
	cmpb	$0, 28(%rax)
	jne	.LBB33_8
# BB#5:                                 # %.critedge
	xorl	%r14d, %r14d
	leaq	8(%rsp), %rcx
	xorl	%edi, %edi
	xorl	%esi, %esi
	movl	$.L.str.9, %edx
	callq	file_open
	testl	%eax, %eax
	js	.LBB33_6
# BB#7:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	(%rbx), %rax
	movq	(%rax), %rsi
	movq	16(%rdi), %rdx
	movl	24(%rdi), %ecx
	movl	%r15d, %r8d
	callq	sread_decrypt
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%r12)
	addq	$-16, osp(%rip)
	jmp	.LBB33_8
.LBB33_6:
	movl	%eax, %r14d
.LBB33_8:
	movl	%r14d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end33:
	.size	ztype1decryptfile, .Lfunc_end33-ztype1decryptfile
	.cfi_endproc

	.globl	zfile_op_init
	.p2align	4, 0x90
	.type	zfile_op_init,@function
zfile_op_init:                          # @zfile_op_init
	.cfi_startproc
# BB#0:
	movl	$zfile_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end34:
	.size	zfile_op_init, .Lfunc_end34-zfile_op_init
	.cfi_endproc

	.globl	file_open
	.p2align	4, 0x90
	.type	file_open,@function
file_open:                              # @file_open
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi156:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi157:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi158:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi159:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi160:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi162:
	.cfi_def_cfa_offset 80
.Lcfi163:
	.cfi_offset %rbx, -56
.Lcfi164:
	.cfi_offset %r12, -48
.Lcfi165:
	.cfi_offset %r13, -40
.Lcfi166:
	.cfi_offset %r14, -32
.Lcfi167:
	.cfi_offset %r15, -24
.Lcfi168:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r12
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	$-13, %ebp
	cmpl	$511, %r14d             # imm = 0x1FF
	ja	.LBB35_17
# BB#1:
	movl	$1, %edi
	movl	$32, %esi
	movl	$.L.str.42, %edx
	callq	alloc
	movq	%rax, %r15
	movl	$-25, %ebp
	testq	%r15, %r15
	je	.LBB35_17
# BB#2:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	$512, %edi              # imm = 0x200
	movl	$1, %esi
	movl	$.L.str.43, %edx
	callq	alloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB35_13
# BB#3:
	movl	$1, %edi
	movl	$120, %esi
	movl	$.L.str.44, %edx
	callq	alloc
	testq	%rax, %rax
	je	.LBB35_12
# BB#4:
	testq	%r13, %r13
	je	.LBB35_9
# BB#5:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%r14d, %ebp
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movb	$0, (%rbx,%rbp)
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB35_10
# BB#6:
	movq	%r15, %rsi
	addq	$16, %rsi
	movl	$.L.str.45, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	string_to_ref
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB35_11
# BB#7:
	cmpb	$114, (%r12)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	$512, %ecx              # imm = 0x200
	movq	%r14, %rsi
	movq	%rbx, %rdx
	jne	.LBB35_14
# BB#8:
	callq	sread_file
	jmp	.LBB35_15
.LBB35_9:
	movq	%rbx, 16(%rax)
	movl	$512, 24(%rax)          # imm = 0x200
	jmp	.LBB35_16
.LBB35_10:
	movl	$-22, %ebp
.LBB35_11:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	$1, %esi
	movl	$120, %edx
	movl	$.L.str.44, %ecx
	callq	alloc_free
.LBB35_12:
	movl	$512, %esi              # imm = 0x200
	movl	$1, %edx
	movl	$.L.str.43, %ecx
	movq	%rbx, %rdi
	callq	alloc_free
.LBB35_13:
	movl	$1, %esi
	movl	$32, %edx
	movl	$.L.str.42, %ecx
	movq	%r15, %rdi
	callq	alloc_free
	jmp	.LBB35_17
.LBB35_14:
	callq	swrite_file
.LBB35_15:                              # %.thread
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB35_16:                              # %.thread
	movq	%rax, (%r15)
	movl	$1, 8(%r15)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r15, (%rdx)
	cmpb	$114, (%r12)
	movw	$526, %ax               # imm = 0x20E
	movw	$270, %cx               # imm = 0x10E
	cmovew	%ax, %cx
	movw	%cx, 8(%rdx)
	xorl	%ebp, %ebp
.LBB35_17:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end35:
	.size	file_open, .Lfunc_end35-file_open
	.cfi_endproc

	.globl	file_check_read
	.p2align	4, 0x90
	.type	file_check_read,@function
file_check_read:                        # @file_check_read
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	(%rax), %rcx
	movl	$-7, %eax
	testq	%rcx, %rcx
	je	.LBB36_3
# BB#1:
	cmpb	$0, 28(%rcx)
	jne	.LBB36_3
# BB#2:                                 # %.critedge
	movq	%rcx, (%rsi)
	xorl	%eax, %eax
.LBB36_3:
	retq
.Lfunc_end36:
	.size	file_check_read, .Lfunc_end36-file_check_read
	.cfi_endproc

	.globl	get_current_file
	.p2align	4, 0x90
	.type	get_current_file,@function
get_current_file:                       # @get_current_file
	.cfi_startproc
# BB#0:
	movq	esp(%rip), %rax
	movl	$estack, %ecx
	cmpq	%rcx, %rax
	jb	.LBB37_4
# BB#1:                                 # %.lr.ph.preheader
	movl	$estack, %ecx
	.p2align	4, 0x90
.LBB37_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	8(%rax), %edx
	andl	$253, %edx
	cmpl	$13, %edx
	je	.LBB37_5
# BB#3:                                 #   in Loop: Header=BB37_2 Depth=1
	addq	$-16, %rax
	cmpq	%rcx, %rax
	jae	.LBB37_2
.LBB37_4:
	xorl	%eax, %eax
.LBB37_5:                               # %._crit_edge
	retq
.Lfunc_end37:
	.size	get_current_file, .Lfunc_end37-get_current_file
	.cfi_endproc

	.globl	file_close
	.p2align	4, 0x90
	.type	file_close,@function
file_close:                             # @file_close
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi171:
	.cfi_def_cfa_offset 32
.Lcfi172:
	.cfi_offset %rbx, -32
.Lcfi173:
	.cfi_offset %r14, -24
.Lcfi174:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	(%rdi), %r15
	movl	8(%r15), %eax
	testl	%eax, %eax
	je	.LBB38_1
# BB#2:
	cmpl	$-1, %eax
	jne	.LBB38_4
# BB#3:
	movq	%rbx, %rdi
	callq	*80(%rbx)
	jmp	.LBB38_6
.LBB38_1:
	movl	$-7, %eax
	jmp	.LBB38_7
.LBB38_4:
	movq	16(%rbx), %r14
	movq	%rbx, %rdi
	callq	*80(%rbx)
	movl	%eax, %ecx
	movl	$-12, %eax
	testl	%ecx, %ecx
	jne	.LBB38_7
# BB#5:
	movl	$1, %esi
	movl	$120, %edx
	movl	$.L.str.46, %ecx
	movq	%rbx, %rdi
	callq	alloc_free
	movl	$512, %esi              # imm = 0x200
	movl	$1, %edx
	movl	$.L.str.47, %ecx
	movq	%r14, %rdi
	callq	alloc_free
.LBB38_6:
	movq	$0, (%r15)
	xorl	%eax, %eax
.LBB38_7:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end38:
	.size	file_close, .Lfunc_end38-file_close
	.cfi_endproc

	.type	invalid_file_entry,@object # @invalid_file_entry
	.bss
	.globl	invalid_file_entry
	.p2align	3
invalid_file_entry:
	.zero	32
	.size	invalid_file_entry, 32

	.type	std_file_streams,@object # @std_file_streams
	.comm	std_file_streams,600,16
	.type	std_files,@object       # @std_files
	.data
	.globl	std_files
	.p2align	4
std_files:
	.quad	std_file_streams
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.quad	std_file_streams+120
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.quad	std_file_streams+240
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.quad	std_file_streams+360
	.long	4294967295              # 0xffffffff
	.zero	4
	.zero	16
	.quad	std_file_streams+480
	.long	4294967295              # 0xffffffff
	.zero	4
	.zero	16
	.size	std_files, 160

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%stdin"
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%stdout"
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%stderr"
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%lineedit"
	.size	.L.str.3, 10

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%statementedit"
	.size	.L.str.4, 15

	.type	std_file_names,@object  # @std_file_names
	.data
	.globl	std_file_names
	.p2align	4
std_file_names:
	.quad	.L.str
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	.L.str.3
	.quad	.L.str.4
	.size	std_file_names, 40

	.type	std_file_attrs,@object  # @std_file_attrs
	.globl	std_file_attrs
	.p2align	4
std_file_attrs:
	.long	514                     # 0x202
	.long	258                     # 0x102
	.long	258                     # 0x102
	.long	514                     # 0x202
	.long	514                     # 0x202
	.size	std_file_attrs, 20

	.type	stdin_buf,@object       # @stdin_buf
	.comm	stdin_buf,1,1
	.type	stdout_buf,@object      # @stdout_buf
	.comm	stdout_buf,128,16
	.type	stderr_buf,@object      # @stderr_buf
	.comm	stderr_buf,128,16
	.type	.L.str.5,@object        # @.str.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.5:
	.asciz	"zfile_init"
	.size	.L.str.5, 11

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%s(%d): "
	.size	.L.str.6, 9

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Benchmarks/MallocBench/gs/zfile.c"
	.size	.L.str.7, 84

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"alloc failed in zfile_init!\n"
	.size	.L.str.8, 29

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"r"
	.size	.L.str.9, 2

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"w"
	.size	.L.str.10, 2

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"0123456789abcdef"
	.size	.L.str.11, 17

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"deletefile"
	.size	.L.str.12, 11

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"renamefile(from)"
	.size	.L.str.13, 17

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"renamefile(to)"
	.size	.L.str.14, 15

	.type	zfile_op_init.my_defs,@object # @zfile_op_init.my_defs
	.data
	.p2align	4
zfile_op_init.my_defs:
	.quad	.L.str.15
	.quad	zbytesavailable
	.quad	.L.str.16
	.quad	zclosefile
	.quad	.L.str.17
	.quad	zcurrentfile
	.quad	.L.str.18
	.quad	zdeletefile
	.quad	.L.str.19
	.quad	zecho
	.quad	.L.str.20
	.quad	zfile
	.quad	.L.str.21
	.quad	zfilename
	.quad	.L.str.22
	.quad	zfileposition
	.quad	.L.str.23
	.quad	zfindlibfile
	.quad	.L.str.24
	.quad	zflush
	.quad	.L.str.25
	.quad	zflushfile
	.quad	.L.str.26
	.quad	zprint
	.quad	.L.str.27
	.quad	zread
	.quad	.L.str.28
	.quad	zreadhexstring
	.quad	.L.str.29
	.quad	zreadline
	.quad	.L.str.30
	.quad	zreadstring
	.quad	.L.str.31
	.quad	zrenamefile
	.quad	.L.str.32
	.quad	zresetfile
	.quad	.L.str.33
	.quad	zrun
	.quad	.L.str.34
	.quad	zsetfileposition
	.quad	.L.str.35
	.quad	ztype1decryptfile
	.quad	.L.str.36
	.quad	zunread
	.quad	.L.str.37
	.quad	zstatus
	.quad	.L.str.38
	.quad	zwrite
	.quad	.L.str.39
	.quad	zwritehexstring
	.quad	.L.str.40
	.quad	zwriteppmfile
	.quad	.L.str.41
	.quad	zwritestring
	.zero	16
	.size	zfile_op_init.my_defs, 448

	.type	.L.str.15,@object       # @.str.15
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.15:
	.asciz	"1bytesavailable"
	.size	.L.str.15, 16

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"1closefile"
	.size	.L.str.16, 11

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"0currentfile"
	.size	.L.str.17, 13

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"1deletefile"
	.size	.L.str.18, 12

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"1echo"
	.size	.L.str.19, 6

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"2file"
	.size	.L.str.20, 6

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"1filename"
	.size	.L.str.21, 10

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"1fileposition"
	.size	.L.str.22, 14

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"1findlibfile"
	.size	.L.str.23, 13

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"0flush"
	.size	.L.str.24, 7

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"1flushfile"
	.size	.L.str.25, 11

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"1print"
	.size	.L.str.26, 7

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"1read"
	.size	.L.str.27, 6

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"2readhexstring"
	.size	.L.str.28, 15

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"2readline"
	.size	.L.str.29, 10

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"2readstring"
	.size	.L.str.30, 12

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"2renamefile"
	.size	.L.str.31, 12

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"1resetfile"
	.size	.L.str.32, 11

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"1run"
	.size	.L.str.33, 5

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"2setfileposition"
	.size	.L.str.34, 17

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"2type1decryptfile"
	.size	.L.str.35, 18

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"2unread"
	.size	.L.str.36, 8

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"1status"
	.size	.L.str.37, 8

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"2write"
	.size	.L.str.38, 7

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"2writehexstring"
	.size	.L.str.39, 16

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"2writeppmfile"
	.size	.L.str.40, 14

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"2writestring"
	.size	.L.str.41, 13

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"file_open(file_entry)"
	.size	.L.str.42, 22

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"file_open(buffer)"
	.size	.L.str.43, 18

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"file_open(stream)"
	.size	.L.str.44, 18

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"file_open(file_name)"
	.size	.L.str.45, 21

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"file_close(stream)"
	.size	.L.str.46, 19

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"file_close(buffer)"
	.size	.L.str.47, 19

	.type	lineedit_buf,@object    # @lineedit_buf
	.comm	lineedit_buf,160,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
