	.text
	.file	"btOptimizedBvh.bc"
	.globl	_ZN14btOptimizedBvhC2Ev
	.p2align	4, 0x90
	.type	_ZN14btOptimizedBvhC2Ev,@function
_ZN14btOptimizedBvhC2Ev:                # @_ZN14btOptimizedBvhC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN14btQuantizedBvhC2Ev
	movq	$_ZTV14btOptimizedBvh+16, (%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN14btOptimizedBvhC2Ev, .Lfunc_end0-_ZN14btOptimizedBvhC2Ev
	.cfi_endproc

	.globl	_ZN14btOptimizedBvhD2Ev
	.p2align	4, 0x90
	.type	_ZN14btOptimizedBvhD2Ev,@function
_ZN14btOptimizedBvhD2Ev:                # @_ZN14btOptimizedBvhD2Ev
	.cfi_startproc
# BB#0:
	jmp	_ZN14btQuantizedBvhD2Ev # TAILCALL
.Lfunc_end1:
	.size	_ZN14btOptimizedBvhD2Ev, .Lfunc_end1-_ZN14btOptimizedBvhD2Ev
	.cfi_endproc

	.globl	_ZN14btOptimizedBvhD0Ev
	.p2align	4, 0x90
	.type	_ZN14btOptimizedBvhD0Ev,@function
_ZN14btOptimizedBvhD0Ev:                # @_ZN14btOptimizedBvhD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN14btQuantizedBvhD2Ev
.Ltmp1:
# BB#1:                                 # %_ZN14btOptimizedBvhD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB2_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
# BB#3:                                 # %_ZN14btOptimizedBvhdlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN14btOptimizedBvhD0Ev, .Lfunc_end2-_ZN14btOptimizedBvhD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_,@function
_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_: # @_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 176
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movb	%dl, 64(%r15)
	testb	%dl, %dl
	je	.LBB4_33
# BB#1:
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movq	%r15, %rdi
	movq	%rcx, %rsi
	movq	%r8, %rdx
	callq	_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f
	leaq	136(%r15), %rax
	movq	$_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback+16, 16(%rsp)
	movq	%rax, 24(%rsp)
	movq	%r15, 32(%rsp)
	movq	(%rbx), %rax
	leaq	8(%r15), %rdx
	leaq	24(%r15), %rcx
.Ltmp16:
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp17:
# BB#2:
	movslq	140(%r15), %r14
	movq	%r14, %r12
	addq	%r12, %r12
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	172(%r15), %ebp
	cmpl	%r12d, %ebp
	jge	.LBB4_28
# BB#3:
	cmpl	%r12d, 176(%r15)
	jge	.LBB4_4
# BB#5:
	testl	%r14d, %r14d
	je	.LBB4_6
# BB#7:
	movq	%r12, %rdi
	shlq	$4, %rdi
.Ltmp19:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r13
.Ltmp20:
# BB#8:                                 # %.noexc
	movl	172(%r15), %eax
	jmp	.LBB4_9
.LBB4_33:
	leaq	72(%r15), %rax
	movq	$_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback+16, (%rsp)
	movq	%rax, 8(%rsp)
	movabsq	$-2495544585613341845, %rax # imm = 0xDD5E0B6BDD5E0B6B
	movq	%rax, 104(%rsp)
	movl	$3713928043, %eax       # imm = 0xDD5E0B6B
	movq	%rax, 112(%rsp)
	movabsq	$6727827449093950315, %rax # imm = 0x5D5E0B6B5D5E0B6B
	movq	%rax, 88(%rsp)
	movq	$1566444395, 96(%rsp)   # imm = 0x5D5E0B6B
	movq	(%rbx), %rax
.Ltmp6:
	movq	%rsp, %rsi
	leaq	104(%rsp), %rdx
	leaq	88(%rsp), %rcx
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp7:
# BB#34:
	movslq	76(%r15), %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movq	%r14, %r12
	addq	%r12, %r12
	movl	108(%r15), %ebp
	cmpl	%r12d, %ebp
	jge	.LBB4_60
# BB#35:
	cmpl	%r12d, 112(%r15)
	jge	.LBB4_36
# BB#37:
	testl	%r14d, %r14d
	je	.LBB4_38
# BB#39:
	movq	%r12, %rdi
	shlq	$6, %rdi
.Ltmp9:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp10:
# BB#40:                                # %.noexc45
	movl	108(%r15), %eax
	jmp	.LBB4_41
.LBB4_4:                                # %..lr.ph.i_crit_edge
	leaq	184(%r15), %rbx
	jmp	.LBB4_22
.LBB4_36:                               # %..lr.ph.i41_crit_edge
	leaq	120(%r15), %r13
	jmp	.LBB4_54
.LBB4_6:
	xorl	%r13d, %r13d
	movl	%ebp, %eax
.LBB4_9:                                # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi.exit.i.i
	leaq	184(%r15), %rbx
	testl	%eax, %eax
	jle	.LBB4_17
# BB#10:                                # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB4_11
# BB#12:                                # %.prol.preheader76
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_13:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%r13,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB4_13
	jmp	.LBB4_14
.LBB4_38:
	xorl	%ebx, %ebx
	movl	%ebp, %eax
.LBB4_41:                               # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE8allocateEi.exit.i.i
	leaq	120(%r15), %r13
	testl	%eax, %eax
	jle	.LBB4_49
# BB#42:                                # %.lr.ph.i.i.i35
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB4_43
# BB#44:                                # %.prol.preheader84
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_45:                               # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	16(%rdx,%rdi), %xmm1
	movups	32(%rdx,%rdi), %xmm2
	movups	48(%rdx,%rdi), %xmm3
	movups	%xmm3, 48(%rbx,%rdi)
	movups	%xmm2, 32(%rbx,%rdi)
	movups	%xmm1, 16(%rbx,%rdi)
	movups	%xmm0, (%rbx,%rdi)
	incq	%rcx
	addq	$64, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB4_45
	jmp	.LBB4_46
.LBB4_11:
	xorl	%ecx, %ecx
.LBB4_14:                               # %.prol.loopexit77
	cmpq	$3, %r8
	jb	.LBB4_17
# BB#15:                                # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB4_16:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%r13,%rcx)
	movq	(%rbx), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%r13,%rcx)
	movq	(%rbx), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%r13,%rcx)
	movq	(%rbx), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%r13,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB4_16
.LBB4_17:                               # %_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_.exit.i.i
	movq	184(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_21
# BB#18:
	cmpb	$0, 192(%r15)
	je	.LBB4_20
# BB#19:
.Ltmp21:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp22:
.LBB4_20:                               # %.noexc22
	movq	$0, (%rbx)
.LBB4_21:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi.exit.preheader.i
	movb	$1, 192(%r15)
	movq	%r13, 184(%r15)
	movl	%r12d, 176(%r15)
.LBB4_22:                               # %.lr.ph.i
	movslq	%ebp, %rax
	movslq	%r12d, %rcx
	movl	%r12d, %esi
	subl	%ebp, %esi
	leaq	-1(%rcx), %rdx
	subq	%rax, %rdx
	andq	$3, %rsi
	je	.LBB4_25
# BB#23:                                # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi.exit.i.prol.preheader
	movq	%rax, %rdi
	shlq	$4, %rdi
	negq	%rsi
	.p2align	4, 0x90
.LBB4_24:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbp
	movaps	(%rsp), %xmm0
	movups	%xmm0, (%rbp,%rdi)
	incq	%rax
	addq	$16, %rdi
	incq	%rsi
	jne	.LBB4_24
.LBB4_25:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi.exit.i.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB4_28
# BB#26:                                # %.lr.ph.i.new
	subq	%rax, %rcx
	shlq	$4, %rax
	addq	$48, %rax
	.p2align	4, 0x90
.LBB4_27:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movaps	(%rsp), %xmm0
	movups	%xmm0, -48(%rdx,%rax)
	movq	(%rbx), %rdx
	movaps	(%rsp), %xmm0
	movups	%xmm0, -32(%rdx,%rax)
	movq	(%rbx), %rdx
	movaps	(%rsp), %xmm0
	movups	%xmm0, -16(%rdx,%rax)
	movq	(%rbx), %rdx
	movaps	(%rsp), %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$64, %rax
	addq	$-4, %rcx
	jne	.LBB4_27
.LBB4_28:                               # %.loopexit
	movl	%r12d, 172(%r15)
	leaq	16(%rsp), %rdi
	jmp	.LBB4_61
.LBB4_43:
	xorl	%ecx, %ecx
.LBB4_46:                               # %.prol.loopexit85
	cmpq	$3, %r8
	jb	.LBB4_49
# BB#47:                                # %.lr.ph.i.i.i35.new
	subq	%rcx, %rax
	shlq	$6, %rcx
	addq	$192, %rcx
	.p2align	4, 0x90
.LBB4_48:                               # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdx
	movups	-192(%rdx,%rcx), %xmm0
	movups	-176(%rdx,%rcx), %xmm1
	movups	-160(%rdx,%rcx), %xmm2
	movups	-144(%rdx,%rcx), %xmm3
	movups	%xmm3, -144(%rbx,%rcx)
	movups	%xmm2, -160(%rbx,%rcx)
	movups	%xmm1, -176(%rbx,%rcx)
	movups	%xmm0, -192(%rbx,%rcx)
	movq	(%r13), %rdx
	movups	-128(%rdx,%rcx), %xmm0
	movups	-112(%rdx,%rcx), %xmm1
	movups	-96(%rdx,%rcx), %xmm2
	movups	-80(%rdx,%rcx), %xmm3
	movups	%xmm3, -80(%rbx,%rcx)
	movups	%xmm2, -96(%rbx,%rcx)
	movups	%xmm1, -112(%rbx,%rcx)
	movups	%xmm0, -128(%rbx,%rcx)
	movq	(%r13), %rdx
	movups	-64(%rdx,%rcx), %xmm0
	movups	-48(%rdx,%rcx), %xmm1
	movups	-32(%rdx,%rcx), %xmm2
	movups	-16(%rdx,%rcx), %xmm3
	movups	%xmm3, -16(%rbx,%rcx)
	movups	%xmm2, -32(%rbx,%rcx)
	movups	%xmm1, -48(%rbx,%rcx)
	movups	%xmm0, -64(%rbx,%rcx)
	movq	(%r13), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	16(%rdx,%rcx), %xmm1
	movups	32(%rdx,%rcx), %xmm2
	movups	48(%rdx,%rcx), %xmm3
	movups	%xmm3, 48(%rbx,%rcx)
	movups	%xmm2, 32(%rbx,%rcx)
	movups	%xmm1, 16(%rbx,%rcx)
	movups	%xmm0, (%rbx,%rcx)
	addq	$256, %rcx              # imm = 0x100
	addq	$-4, %rax
	jne	.LBB4_48
.LBB4_49:                               # %_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4copyEiiPS0_.exit.i.i
	movq	120(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_53
# BB#50:
	cmpb	$0, 128(%r15)
	je	.LBB4_52
# BB#51:
.Ltmp11:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp12:
.LBB4_52:                               # %.noexc46
	movq	$0, (%r13)
.LBB4_53:                               # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi.exit.preheader.i
	movb	$1, 128(%r15)
	movq	%rbx, 120(%r15)
	movl	%r12d, 112(%r15)
.LBB4_54:                               # %.lr.ph.i41
	movslq	%ebp, %rax
	movslq	%r12d, %rcx
	movl	%r12d, %esi
	subl	%ebp, %esi
	leaq	-1(%rcx), %rdx
	subq	%rax, %rdx
	andq	$3, %rsi
	je	.LBB4_57
# BB#55:                                # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi.exit.i.prol.preheader
	movq	%rax, %rdi
	shlq	$6, %rdi
	negq	%rsi
	.p2align	4, 0x90
.LBB4_56:                               # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rbp
	movaps	16(%rsp), %xmm0
	movaps	32(%rsp), %xmm1
	movaps	48(%rsp), %xmm2
	movaps	64(%rsp), %xmm3
	movups	%xmm3, 48(%rbp,%rdi)
	movups	%xmm2, 32(%rbp,%rdi)
	movups	%xmm1, 16(%rbp,%rdi)
	movups	%xmm0, (%rbp,%rdi)
	incq	%rax
	addq	$64, %rdi
	incq	%rsi
	jne	.LBB4_56
.LBB4_57:                               # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi.exit.i.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB4_60
# BB#58:                                # %.lr.ph.i41.new
	subq	%rax, %rcx
	shlq	$6, %rax
	addq	$192, %rax
	.p2align	4, 0x90
.LBB4_59:                               # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdx
	movaps	16(%rsp), %xmm0
	movaps	32(%rsp), %xmm1
	movaps	48(%rsp), %xmm2
	movaps	64(%rsp), %xmm3
	movups	%xmm3, -144(%rdx,%rax)
	movups	%xmm2, -160(%rdx,%rax)
	movups	%xmm1, -176(%rdx,%rax)
	movups	%xmm0, -192(%rdx,%rax)
	movq	(%r13), %rdx
	movaps	16(%rsp), %xmm0
	movaps	32(%rsp), %xmm1
	movaps	48(%rsp), %xmm2
	movaps	64(%rsp), %xmm3
	movups	%xmm3, -80(%rdx,%rax)
	movups	%xmm2, -96(%rdx,%rax)
	movups	%xmm1, -112(%rdx,%rax)
	movups	%xmm0, -128(%rdx,%rax)
	movq	(%r13), %rdx
	movaps	16(%rsp), %xmm0
	movaps	32(%rsp), %xmm1
	movaps	48(%rsp), %xmm2
	movaps	64(%rsp), %xmm3
	movups	%xmm3, -16(%rdx,%rax)
	movups	%xmm2, -32(%rdx,%rax)
	movups	%xmm1, -48(%rdx,%rax)
	movups	%xmm0, -64(%rdx,%rax)
	movq	(%r13), %rdx
	movaps	16(%rsp), %xmm0
	movaps	32(%rsp), %xmm1
	movaps	48(%rsp), %xmm2
	movaps	64(%rsp), %xmm3
	movups	%xmm3, 48(%rdx,%rax)
	movups	%xmm2, 32(%rdx,%rax)
	movups	%xmm1, 16(%rdx,%rax)
	movups	%xmm0, (%rdx,%rax)
	addq	$256, %rax              # imm = 0x100
	addq	$-4, %rcx
	jne	.LBB4_59
.LBB4_60:                               # %.loopexit68
	movl	%r12d, 108(%r15)
	movq	%rsp, %rdi
.LBB4_61:
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
	movl	$0, 60(%r15)
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%r14d, %edx
	callq	_ZN14btQuantizedBvh9buildTreeEii
	cmpb	$0, 64(%r15)
	je	.LBB4_81
# BB#62:
	cmpl	$0, 212(%r15)
	jne	.LBB4_81
# BB#63:
	movl	$1, %r14d
	cmpl	$0, 216(%r15)
	movl	$1, %eax
	jne	.LBB4_80
# BB#64:                                # %_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE8allocateEi.exit.i.i
	movl	$32, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movslq	212(%r15), %rax
	testq	%rax, %rax
	jle	.LBB4_75
# BB#65:                                # %.lr.ph.i.i.i27
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB4_66
# BB#70:                                # %.prol.preheader
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_71:                               # =>This Inner Loop Header: Depth=1
	movq	224(%r15), %rbp
	movups	(%rbp,%rdi), %xmm0
	movups	16(%rbp,%rdi), %xmm1
	movups	%xmm1, 16(%rbx,%rdi)
	movups	%xmm0, (%rbx,%rdi)
	incq	%rcx
	addq	$32, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB4_71
	jmp	.LBB4_72
.LBB4_66:
	xorl	%ecx, %ecx
.LBB4_72:                               # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB4_75
# BB#73:                                # %.lr.ph.i.i.i27.new
	subq	%rcx, %rax
	shlq	$5, %rcx
	addq	$96, %rcx
	.p2align	4, 0x90
.LBB4_74:                               # =>This Inner Loop Header: Depth=1
	movq	224(%r15), %rdx
	movups	-96(%rdx,%rcx), %xmm0
	movups	-80(%rdx,%rcx), %xmm1
	movups	%xmm1, -80(%rbx,%rcx)
	movups	%xmm0, -96(%rbx,%rcx)
	movq	224(%r15), %rdx
	movups	-64(%rdx,%rcx), %xmm0
	movups	-48(%rdx,%rcx), %xmm1
	movups	%xmm1, -48(%rbx,%rcx)
	movups	%xmm0, -64(%rbx,%rcx)
	movq	224(%r15), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	-16(%rdx,%rcx), %xmm1
	movups	%xmm1, -16(%rbx,%rcx)
	movups	%xmm0, -32(%rbx,%rcx)
	movq	224(%r15), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	16(%rdx,%rcx), %xmm1
	movups	%xmm1, 16(%rbx,%rcx)
	movups	%xmm0, (%rbx,%rcx)
	subq	$-128, %rcx
	addq	$-4, %rax
	jne	.LBB4_74
.LBB4_75:                               # %_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_.exit.i.i
	movq	224(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_79
# BB#76:
	cmpb	$0, 232(%r15)
	je	.LBB4_78
# BB#77:
	callq	_Z21btAlignedFreeInternalPv
.LBB4_78:
	movq	$0, 224(%r15)
.LBB4_79:                               # %_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv.exit.i.i
	movb	$1, 232(%r15)
	movq	%rbx, 224(%r15)
	movl	$1, 216(%r15)
	movl	212(%r15), %eax
	incl	%eax
.LBB4_80:                               # %_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_.exit
	movl	%eax, 212(%r15)
	movq	224(%r15), %rax
	movups	16(%rsp), %xmm0
	movups	32(%rsp), %xmm1
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	224(%r15), %rax
	movq	184(%r15), %rcx
	movzwl	(%rcx), %edx
	movw	%dx, (%rax)
	movzwl	2(%rcx), %edx
	movw	%dx, 2(%rax)
	movzwl	4(%rcx), %edx
	movw	%dx, 4(%rax)
	movzwl	6(%rcx), %edx
	movw	%dx, 6(%rax)
	movzwl	8(%rcx), %edx
	movw	%dx, 8(%rax)
	movzwl	10(%rcx), %edx
	movw	%dx, 10(%rax)
	movl	$0, 12(%rax)
	movl	12(%rcx), %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%ecx, %ecx
	cmovnsl	%r14d, %edx
	movl	%edx, 16(%rax)
.LBB4_81:                               # %._crit_edge
	movl	212(%r15), %eax
	movl	%eax, 240(%r15)
	movq	152(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_85
# BB#82:
	cmpb	$0, 160(%r15)
	je	.LBB4_84
# BB#83:
	callq	_Z21btAlignedFreeInternalPv
.LBB4_84:
	movq	$0, 152(%r15)
.LBB4_85:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv.exit
	movb	$1, 160(%r15)
	movq	$0, 152(%r15)
	movq	$0, 140(%r15)
	movq	88(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_89
# BB#86:
	cmpb	$0, 96(%r15)
	je	.LBB4_88
# BB#87:
	callq	_Z21btAlignedFreeInternalPv
.LBB4_88:
	movq	$0, 88(%r15)
.LBB4_89:                               # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv.exit
	movb	$1, 96(%r15)
	movq	$0, 88(%r15)
	movq	$0, 76(%r15)
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_68:
.Ltmp13:
	jmp	.LBB4_69
.LBB4_30:
.Ltmp23:
	jmp	.LBB4_31
.LBB4_67:
.Ltmp8:
.LBB4_69:
	movq	%rax, %rbx
.Ltmp14:
	movq	%rsp, %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp15:
	jmp	.LBB4_32
.LBB4_29:
.Ltmp18:
.LBB4_31:
	movq	%rax, %rbx
.Ltmp24:
	leaq	16(%rsp), %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp25:
.LBB4_32:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_90:
.Ltmp26:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_, .Lfunc_end4-_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp16-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp23-.Lfunc_begin1   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 4 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 5 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin1   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp14-.Ltmp12         #   Call between .Ltmp12 and .Ltmp14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp25-.Ltmp14         #   Call between .Ltmp14 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin1   #     jumps to .Ltmp26
	.byte	1                       #   On action: 1
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Lfunc_end4-.Ltmp25     #   Call between .Ltmp25 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN14btOptimizedBvh5refitEP23btStridingMeshInterfaceRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZN14btOptimizedBvh5refitEP23btStridingMeshInterfaceRK9btVector3S4_,@function
_ZN14btOptimizedBvh5refitEP23btStridingMeshInterfaceRK9btVector3S4_: # @_ZN14btOptimizedBvh5refitEP23btStridingMeshInterfaceRK9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpb	$0, 64(%rbx)
	je	.LBB5_4
# BB#1:
	movss	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movq	%rbx, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	callq	_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f
	movl	60(%rbx), %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN14btOptimizedBvh14updateBvhNodesEP23btStridingMeshInterfaceiii
	movslq	212(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB5_4
# BB#2:                                 # %.lr.ph
	movq	184(%rbx), %rcx
	movq	224(%rbx), %rdx
	addq	$12, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_3:                                # =>This Inner Loop Header: Depth=1
	movslq	(%rdx), %rdi
	shlq	$4, %rdi
	movzwl	(%rcx,%rdi), %ebx
	movw	%bx, -12(%rdx)
	movzwl	2(%rcx,%rdi), %ebx
	movw	%bx, -10(%rdx)
	movzwl	4(%rcx,%rdi), %ebx
	movw	%bx, -8(%rdx)
	movzwl	6(%rcx,%rdi), %ebx
	movw	%bx, -6(%rdx)
	movzwl	8(%rcx,%rdi), %ebx
	movw	%bx, -4(%rdx)
	movzwl	10(%rcx,%rdi), %edi
	movw	%di, -2(%rdx)
	incq	%rsi
	addq	$32, %rdx
	cmpq	%rax, %rsi
	jl	.LBB5_3
.LBB5_4:                                # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_ZN14btOptimizedBvh5refitEP23btStridingMeshInterfaceRK9btVector3S4_, .Lfunc_end5-_ZN14btOptimizedBvh5refitEP23btStridingMeshInterfaceRK9btVector3S4_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1566444395              # float 9.99999984E+17
.LCPI6_1:
	.long	3713928043              # float -9.99999984E+17
.LCPI6_2:
	.long	1065353216              # float 1
	.text
	.globl	_ZN14btOptimizedBvh14updateBvhNodesEP23btStridingMeshInterfaceiii
	.p2align	4, 0x90
	.type	_ZN14btOptimizedBvh14updateBvhNodesEP23btStridingMeshInterfaceiii,@function
_ZN14btOptimizedBvh14updateBvhNodesEP23btStridingMeshInterfaceiii: # @_ZN14btOptimizedBvh14updateBvhNodesEP23btStridingMeshInterfaceiii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 112
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rsi, %r8
	movq	%rdi, %r15
	movq	$0, 48(%rsp)
	movl	$0, 36(%rsp)
	movl	$2, 24(%rsp)
	movl	$0, 4(%rsp)
	movq	$0, 40(%rsp)
	movl	$0, 20(%rsp)
	movl	$0, 32(%rsp)
	movl	$2, 16(%rsp)
	cmpl	%edx, %ecx
	jle	.LBB6_18
# BB#1:                                 # %.lr.ph.lr.ph
	movl	%edx, 28(%rsp)          # 4-byte Spill
	movslq	%edx, %r13
	movl	$-1, %esi
	movss	.LCPI6_0(%rip), %xmm10  # xmm10 = mem[0],zero,zero,zero
	movss	.LCPI6_1(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movss	.LCPI6_2(%rip), %xmm12  # xmm12 = mem[0],zero,zero,zero
	movq	%r8, 8(%rsp)            # 8-byte Spill
.LBB6_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_3 Depth 2
	movslq	%ecx, %rbx
	movq	184(%r15), %rax
	movq	%rbx, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %r12
	.p2align	4, 0x90
.LBB6_3:                                #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	12(%r12), %r14d
	testl	%r14d, %r14d
	jns	.LBB6_4
# BB#15:                                # %.loopexit241
                                        #   in Loop: Header=BB6_3 Depth=2
	movq	%rbx, %rcx
	decq	%rcx
	movslq	%ebx, %rdx
	shlq	$4, %rdx
	movl	12(%rax,%rdx), %edi
	cmpl	$-2, %edi
	movl	$-1, %ebp
	cmovgl	%edi, %ebp
	subl	%edi, %ebp
	leal	1(%rbx,%rbp), %edi
	movslq	%edi, %rdi
	movzwl	(%rax,%rdx), %ebp
	movw	%bp, (%r12)
	shlq	$4, %rdi
	movzwl	(%rax,%rdi), %ebx
	cmpw	%bx, %bp
	cmovaw	%bx, %bp
	movw	%bp, (%r12)
	movzwl	6(%rax,%rdx), %ebp
	movw	%bp, 6(%r12)
	movzwl	6(%rax,%rdi), %ebx
	cmpw	%bx, %bp
	cmovbw	%bx, %bp
	movw	%bp, 6(%r12)
	movzwl	2(%rax,%rdx), %ebp
	movw	%bp, 2(%r12)
	movzwl	2(%rax,%rdi), %ebx
	cmpw	%bx, %bp
	cmovaw	%bx, %bp
	movw	%bp, 2(%r12)
	movzwl	8(%rax,%rdx), %ebp
	movw	%bp, 8(%r12)
	movzwl	8(%rax,%rdi), %ebx
	cmpw	%bx, %bp
	cmovbw	%bx, %bp
	movw	%bp, 8(%r12)
	movzwl	4(%rax,%rdx), %ebp
	movw	%bp, 4(%r12)
	movzwl	4(%rax,%rdi), %ebx
	cmpw	%bx, %bp
	cmovaw	%bx, %bp
	movw	%bp, 4(%r12)
	movzwl	10(%rax,%rdx), %edx
	movw	%dx, 10(%r12)
	movzwl	10(%rax,%rdi), %edi
	cmpw	%di, %dx
	cmovbw	%di, %dx
	movw	%dx, 10(%r12)
	addq	$-16, %r12
	cmpq	%r13, %rcx
	movq	%rcx, %rbx
	jg	.LBB6_3
	jmp	.LBB6_16
	.p2align	4, 0x90
.LBB6_4:                                #   in Loop: Header=BB6_2 Depth=1
	movl	%r14d, %ebp
	shrl	$21, %ebp
	andl	$2097151, %r14d         # imm = 0x1FFFFF
	cmpl	%esi, %ebp
	je	.LBB6_8
# BB#5:                                 #   in Loop: Header=BB6_2 Depth=1
	testl	%esi, %esi
	js	.LBB6_7
# BB#6:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	(%r8), %rax
	movq	%r8, %rdi
	callq	*48(%rax)
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB6_7:                                #   in Loop: Header=BB6_2 Depth=1
	movq	(%r8), %r10
	movq	%r8, %rdi
	leaq	48(%rsp), %rsi
	leaq	36(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	4(%rsp), %r8
	leaq	40(%rsp), %r9
	pushq	%rbp
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	leaq	24(%rsp), %rax
	pushq	%rax
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	leaq	48(%rsp), %rax
	pushq	%rax
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	leaq	44(%rsp), %rax
	pushq	%rax
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	callq	*32(%r10)
	addq	$32, %rsp
.Lcfi42:
	.cfi_adjust_cfa_offset -32
	movl	%ebp, %esi
	movss	.LCPI6_0(%rip), %xmm10  # xmm10 = mem[0],zero,zero,zero
	movss	.LCPI6_1(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movss	.LCPI6_2(%rip), %xmm12  # xmm12 = mem[0],zero,zero,zero
.LBB6_8:                                #   in Loop: Header=BB6_2 Depth=1
	leal	-1(%rbx), %ecx
	movq	40(%rsp), %r8
	movslq	20(%rsp), %rax
	movslq	%r14d, %rdi
	imulq	%rax, %rdi
	movl	24(%rsp), %edx
	movq	48(%rsp), %rax
	cmpl	$3, 16(%rsp)
	jne	.LBB6_11
# BB#9:                                 # %.split.us.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	movzwl	4(%r8,%rdi), %ebp
	movl	4(%rsp), %ebx
	imull	%ebx, %ebp
	testl	%edx, %edx
	movslq	%ebp, %rdx
	je	.LBB6_19
# BB#10:                                #   in Loop: Header=BB6_2 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
	movss	8(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	16(%rbp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	cvtps2pd	%xmm2, %xmm4
	movsd	(%rax,%rdx), %xmm5      # xmm5 = mem[0],zero
	movsd	16(%rax,%rdx), %xmm2    # xmm2 = mem[0],zero
	movhpd	8(%rax,%rdx), %xmm2     # xmm2 = xmm2[0],mem[0]
	mulpd	%xmm4, %xmm2
	cvtpd2ps	%xmm2, %xmm9
	movzwl	2(%r8,%rdi), %edx
	imull	%ebx, %edx
	movslq	%edx, %rdx
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	cvtps2pd	%xmm1, %xmm1
	movsd	8(%rax,%rdx), %xmm2     # xmm2 = mem[0],zero
	movhpd	(%rax,%rdx), %xmm2      # xmm2 = xmm2[0],mem[0]
	mulpd	%xmm1, %xmm2
	cvtpd2ps	%xmm2, %xmm2
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm1
	unpcklps	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	cvtps2pd	%xmm0, %xmm0
	movhpd	16(%rax,%rdx), %xmm5    # xmm5 = xmm5[0],mem[0]
	mulpd	%xmm0, %xmm5
	cvtpd2ps	%xmm5, %xmm0
	movzwl	(%r8,%rdi), %edx
	imull	%ebx, %edx
	movslq	%edx, %rdx
	mulsd	(%rax,%rdx), %xmm1
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm1, %xmm3
	movsd	16(%rax,%rdx), %xmm1    # xmm1 = mem[0],zero
	movhpd	8(%rax,%rdx), %xmm1     # xmm1 = xmm1[0],mem[0]
	mulpd	%xmm4, %xmm1
	cvtpd2ps	%xmm1, %xmm4
	jmp	.LBB6_14
	.p2align	4, 0x90
.LBB6_11:                               # %.split
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	4(%rsp), %ebx
	movl	8(%r8,%rdi), %ebp
	imull	%ebx, %ebp
	testl	%edx, %edx
	movslq	%ebp, %rdx
	je	.LBB6_13
# BB#12:                                # %.split.split.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	4(%r8,%rdi), %ebp
	imull	%ebx, %ebp
	imull	(%r8,%rdi), %ebx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm3
	movss	16(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	cvtps2pd	%xmm0, %xmm0
	movsd	16(%rax,%rdx), %xmm1    # xmm1 = mem[0],zero
	movhpd	8(%rax,%rdx), %xmm1     # xmm1 = xmm1[0],mem[0]
	mulpd	%xmm0, %xmm1
	movslq	%ebp, %rdi
	movsd	8(%rax,%rdi), %xmm2     # xmm2 = mem[0],zero
	movhpd	(%rax,%rdi), %xmm2      # xmm2 = xmm2[0],mem[0]
	movapd	%xmm3, %xmm4
	unpcklpd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0]
	movslq	%ebx, %rbp
	movsd	16(%rax,%rbp), %xmm5    # xmm5 = mem[0],zero
	movhpd	8(%rax,%rbp), %xmm5     # xmm5 = xmm5[0],mem[0]
	mulpd	%xmm0, %xmm5
	movapd	%xmm0, %xmm6
	shufpd	$1, %xmm3, %xmm6        # xmm6 = xmm6[1],xmm3[0]
	mulpd	%xmm2, %xmm6
	movsd	(%rax,%rdx), %xmm7      # xmm7 = mem[0],zero
	cvtpd2ps	%xmm1, %xmm9
	cvtpd2ps	%xmm6, %xmm2
	movhpd	16(%rax,%rdi), %xmm7    # xmm7 = xmm7[0],mem[0]
	mulpd	%xmm7, %xmm4
	cvtpd2ps	%xmm4, %xmm0
	mulsd	(%rax,%rbp), %xmm3
	cvtsd2ss	%xmm3, %xmm3
	cvtpd2ps	%xmm5, %xmm4
	jmp	.LBB6_14
.LBB6_19:                               #   in Loop: Header=BB6_2 Depth=1
	movss	(%rax,%rdx), %xmm4      # xmm4 = mem[0],zero,zero,zero
	movss	4(%rax,%rdx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	movss	8(%rax,%rdx), %xmm9     # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm9    # xmm9 = xmm9[0],xmm0[0],xmm9[1],xmm0[1]
	movq	8(%rsp), %rdx           # 8-byte Reload
	movss	8(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	12(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	16(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	mulps	%xmm1, %xmm9
	movzwl	2(%r8,%rdi), %edx
	imull	%ebx, %edx
	movslq	%edx, %rdx
	movss	(%rax,%rdx), %xmm5      # xmm5 = mem[0],zero,zero,zero
	movss	4(%rax,%rdx), %xmm6     # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	mulps	%xmm6, %xmm2
	movss	8(%rax,%rdx), %xmm5     # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	mulps	%xmm4, %xmm0
	movzwl	(%r8,%rdi), %edx
	imull	%ebx, %edx
	movslq	%edx, %rdx
	mulss	(%rax,%rdx), %xmm3
	movss	4(%rax,%rdx), %xmm5     # xmm5 = mem[0],zero,zero,zero
	movss	8(%rax,%rdx), %xmm4     # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	mulps	%xmm1, %xmm4
	jmp	.LBB6_14
.LBB6_13:                               # %.split.split.us.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	movss	(%rax,%rdx), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movss	4(%rax,%rdx), %xmm2     # xmm2 = mem[0],zero,zero,zero
	movss	8(%rax,%rdx), %xmm9     # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm9    # xmm9 = xmm9[0],xmm2[0],xmm9[1],xmm2[1]
	movq	8(%rsp), %rdx           # 8-byte Reload
	movss	8(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	12(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	16(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	unpcklps	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	unpcklps	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1]
	mulps	%xmm5, %xmm9
	movl	4(%r8,%rdi), %edx
	imull	%ebx, %edx
	movslq	%edx, %rdx
	movss	(%rax,%rdx), %xmm6      # xmm6 = mem[0],zero,zero,zero
	movss	4(%rax,%rdx), %xmm7     # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1]
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	mulps	%xmm7, %xmm2
	movss	8(%rax,%rdx), %xmm6     # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0],xmm0[1],xmm6[1]
	mulps	%xmm4, %xmm0
	imull	(%r8,%rdi), %ebx
	movslq	%ebx, %rdx
	mulss	(%rax,%rdx), %xmm3
	movss	4(%rax,%rdx), %xmm6     # xmm6 = mem[0],zero,zero,zero
	movss	8(%rax,%rdx), %xmm4     # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm4    # xmm4 = xmm4[0],xmm6[0],xmm4[1],xmm6[1]
	mulps	%xmm5, %xmm4
	.p2align	4, 0x90
.LBB6_14:                               # %_Z8btSetMinIfEvRT_RKS0_.exit.i117
                                        #   in Loop: Header=BB6_2 Depth=1
	ucomiss	%xmm3, %xmm10
	movd	%xmm3, %eax
	movl	$1566444395, %r8d       # imm = 0x5D5E0B6B
	cmoval	%eax, %r8d
	movapd	%xmm4, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	ucomiss	%xmm5, %xmm10
	movd	%xmm5, %edx
	movl	$1566444395, %ebp       # imm = 0x5D5E0B6B
	cmoval	%edx, %ebp
	ucomiss	%xmm4, %xmm10
	movd	%xmm4, %ebx
	movl	$1566444395, %edi       # imm = 0x5D5E0B6B
	cmoval	%ebx, %edi
	ucomiss	%xmm11, %xmm3
	movl	$-581039253, %r9d       # imm = 0xDD5E0B6B
	cmovbel	%r9d, %eax
	ucomiss	%xmm11, %xmm5
	cmovbel	%r9d, %edx
	ucomiss	%xmm11, %xmm4
	cmovbel	%r9d, %ebx
	movd	%r8d, %xmm3
	movapd	%xmm2, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	ucomiss	%xmm4, %xmm3
	movd	%xmm4, %r9d
	cmoval	%r9d, %r8d
	movd	%ebp, %xmm3
	ucomiss	%xmm2, %xmm3
	movd	%xmm2, %r10d
	cmoval	%r10d, %ebp
	movd	%edi, %xmm3
	movapd	%xmm0, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	ucomiss	%xmm5, %xmm3
	movd	%xmm5, %r11d
	cmoval	%r11d, %edi
	movd	%eax, %xmm3
	ucomiss	%xmm3, %xmm4
	cmoval	%r9d, %eax
	movd	%edx, %xmm3
	ucomiss	%xmm3, %xmm2
	cmoval	%r10d, %edx
	movd	%ebx, %xmm2
	ucomiss	%xmm2, %xmm5
	cmoval	%r11d, %ebx
	movd	%r8d, %xmm6
	movd	%ebp, %xmm7
	movapd	%xmm9, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movd	%edi, %xmm3
	movd	%eax, %xmm5
	movd	%edx, %xmm4
	movd	%ebx, %xmm8
	movapd	%xmm0, %xmm1
	minss	%xmm6, %xmm1
	subss	8(%r15), %xmm1
	movaps	%xmm2, %xmm6
	minss	%xmm7, %xmm6
	subss	12(%r15), %xmm6
	movapd	%xmm9, %xmm7
	minss	%xmm3, %xmm7
	subss	16(%r15), %xmm7
	mulss	40(%r15), %xmm1
	mulss	44(%r15), %xmm6
	mulss	48(%r15), %xmm7
	cvttss2si	%xmm1, %eax
	andl	$65534, %eax            # imm = 0xFFFE
	movw	%ax, (%r12)
	cvttss2si	%xmm6, %eax
	andl	$65534, %eax            # imm = 0xFFFE
	cvttss2si	%xmm7, %edx
	andl	$65534, %edx            # imm = 0xFFFE
	movw	%ax, 2(%r12)
	movw	%dx, 4(%r12)
	maxss	%xmm5, %xmm0
	subss	8(%r15), %xmm0
	maxss	%xmm4, %xmm2
	subss	12(%r15), %xmm2
	maxss	%xmm8, %xmm9
	subss	16(%r15), %xmm9
	mulss	40(%r15), %xmm0
	mulss	44(%r15), %xmm2
	mulss	48(%r15), %xmm9
	addss	%xmm12, %xmm0
	cvttss2si	%xmm0, %eax
	orl	$1, %eax
	movw	%ax, 6(%r12)
	addss	%xmm12, %xmm2
	cvttss2si	%xmm2, %eax
	orl	$1, %eax
	addss	%xmm12, %xmm9
	cvttss2si	%xmm9, %edx
	orl	$1, %edx
	movw	%ax, 8(%r12)
	movw	%dx, 10(%r12)
	cmpl	28(%rsp), %ecx          # 4-byte Folded Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	jg	.LBB6_2
.LBB6_16:                               # %.outer._crit_edge
	testl	%esi, %esi
	js	.LBB6_18
# BB#17:
	movq	(%r8), %rax
	movq	%r8, %rdi
	callq	*48(%rax)
.LBB6_18:                               # %.outer._crit_edge.thread
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN14btOptimizedBvh14updateBvhNodesEP23btStridingMeshInterfaceiii, .Lfunc_end6-_ZN14btOptimizedBvh14updateBvhNodesEP23btStridingMeshInterfaceiii
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN14btOptimizedBvh12refitPartialEP23btStridingMeshInterfaceRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZN14btOptimizedBvh12refitPartialEP23btStridingMeshInterfaceRK9btVector3S4_,@function
_ZN14btOptimizedBvh12refitPartialEP23btStridingMeshInterfaceRK9btVector3S4_: # @_ZN14btOptimizedBvh12refitPartialEP23btStridingMeshInterfaceRK9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 80
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movl	212(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB7_10
# BB#1:                                 # %.lr.ph
	movss	(%rdx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	subss	%xmm8, %xmm3
	movss	8(%rdx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm4
	movss	40(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	movss	44(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	movss	48(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	cvttss2si	%xmm2, %ebp
	andl	$65534, %ebp            # imm = 0xFFFE
	cvttss2si	%xmm3, %esi
	andl	$65534, %esi            # imm = 0xFFFE
	movl	%esi, 4(%rsp)           # 4-byte Spill
	cvttss2si	%xmm4, %esi
	andl	$65534, %esi            # imm = 0xFFFE
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	subss	%xmm8, %xmm3
	movss	8(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm1
	mulss	%xmm6, %xmm2
	mulss	%xmm7, %xmm3
	mulss	%xmm0, %xmm1
	movss	.LCPI7_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm2
	cvttss2si	%xmm2, %r15d
	orl	$1, %r15d
	addss	%xmm0, %xmm3
	cvttss2si	%xmm3, %ecx
	orl	$1, %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	addss	%xmm0, %xmm1
	cvttss2si	%xmm1, %ecx
	orl	$1, %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movq	224(%rbx), %r13
	cmpw	6(%r13,%r14), %bp
	ja	.LBB7_9
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	cmpw	(%r13,%r14), %r15w
	jb	.LBB7_9
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmpw	10(%r13,%r14), %cx
	ja	.LBB7_9
# BB#5:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	8(%rsp), %ecx           # 4-byte Reload
	cmpw	4(%r13,%r14), %cx
	jb	.LBB7_9
# BB#6:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	4(%rsp), %ecx           # 4-byte Reload
	cmpw	8(%r13,%r14), %cx
	ja	.LBB7_9
# BB#7:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	(%rsp), %ecx            # 4-byte Reload
	cmpw	2(%r13,%r14), %cx
	jb	.LBB7_9
# BB#8:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	12(%r13,%r14), %edx
	movl	16(%r13,%r14), %ecx
	addl	%edx, %ecx
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	_ZN14btOptimizedBvh14updateBvhNodesEP23btStridingMeshInterfaceiii
	movq	184(%rbx), %rax
	movslq	12(%r13,%r14), %rcx
	shlq	$4, %rcx
	movzwl	(%rax,%rcx), %edx
	movw	%dx, (%r13,%r14)
	movzwl	2(%rax,%rcx), %edx
	movw	%dx, 2(%r13,%r14)
	movzwl	4(%rax,%rcx), %edx
	movw	%dx, 4(%r13,%r14)
	movzwl	6(%rax,%rcx), %edx
	movw	%dx, 6(%r13,%r14)
	movzwl	8(%rax,%rcx), %edx
	movw	%dx, 8(%r13,%r14)
	movzwl	10(%rax,%rcx), %eax
	movw	%ax, 10(%r13,%r14)
	movl	212(%rbx), %eax
.LBB7_9:                                #   in Loop: Header=BB7_2 Depth=1
	incq	%r12
	movslq	%eax, %rcx
	addq	$32, %r14
	cmpq	%rcx, %r12
	jl	.LBB7_2
.LBB7_10:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN14btOptimizedBvh12refitPartialEP23btStridingMeshInterfaceRK9btVector3S4_, .Lfunc_end7-_ZN14btOptimizedBvh12refitPartialEP23btStridingMeshInterfaceRK9btVector3S4_
	.cfi_endproc

	.globl	_ZN14btOptimizedBvh18deSerializeInPlaceEPvjb
	.p2align	4, 0x90
	.type	_ZN14btOptimizedBvh18deSerializeInPlaceEPvjb,@function
_ZN14btOptimizedBvh18deSerializeInPlaceEPvjb: # @_ZN14btOptimizedBvh18deSerializeInPlaceEPvjb
	.cfi_startproc
# BB#0:
	movzbl	%dl, %edx
	jmp	_ZN14btQuantizedBvh18deSerializeInPlaceEPvjb # TAILCALL
.Lfunc_end8:
	.size	_ZN14btOptimizedBvh18deSerializeInPlaceEPvjb, .Lfunc_end8-_ZN14btOptimizedBvh18deSerializeInPlaceEPvjb
	.cfi_endproc

	.section	.text._ZN14btOptimizedBvh9serializeEPvjb,"axG",@progbits,_ZN14btOptimizedBvh9serializeEPvjb,comdat
	.weak	_ZN14btOptimizedBvh9serializeEPvjb
	.p2align	4, 0x90
	.type	_ZN14btOptimizedBvh9serializeEPvjb,@function
_ZN14btOptimizedBvh9serializeEPvjb:     # @_ZN14btOptimizedBvh9serializeEPvjb
	.cfi_startproc
# BB#0:
	movzbl	%cl, %ecx
	jmp	_ZN14btQuantizedBvh9serializeEPvjb # TAILCALL
.Lfunc_end9:
	.size	_ZN14btOptimizedBvh9serializeEPvjb, .Lfunc_end9-_ZN14btOptimizedBvh9serializeEPvjb
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackD0Ev,@function
_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackD0Ev: # @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi58:
	.cfi_def_cfa_offset 32
.Lcfi59:
	.cfi_offset %rbx, -24
.Lcfi60:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp27:
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp28:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB10_2:
.Ltmp29:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackD0Ev, .Lfunc_end10-_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp27-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin2   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end10-.Ltmp28    #   Call between .Ltmp28 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_0:
	.long	1566444395              # float 9.99999984E+17
.LCPI11_1:
	.long	3713928043              # float -9.99999984E+17
.LCPI11_2:
	.long	990057071               # float 0.00200000009
.LCPI11_3:
	.long	981668463               # float 0.00100000005
.LCPI11_4:
	.long	3129152111              # float -0.00100000005
.LCPI11_5:
	.long	1065353216              # float 1
	.text
	.p2align	4, 0x90
	.type	_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallback28internalProcessTriangleIndexEPS2_ii,@function
_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallback28internalProcessTriangleIndexEPS2_ii: # @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallback28internalProcessTriangleIndexEPS2_ii
	.cfi_startproc
# BB#0:                                 # %_Z8btSetMinIfEvRT_RKS0_.exit.i23
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi67:
	.cfi_def_cfa_offset 80
.Lcfi68:
	.cfi_offset %rbx, -56
.Lcfi69:
	.cfi_offset %r12, -48
.Lcfi70:
	.cfi_offset %r13, -40
.Lcfi71:
	.cfi_offset %r14, -32
.Lcfi72:
	.cfi_offset %r15, -24
.Lcfi73:
	.cfi_offset %rbp, -16
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%edx, %ecx
	movd	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movd	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI11_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	movd	%xmm0, %r8d
	movl	$1566444395, %r10d      # imm = 0x5D5E0B6B
	movl	$1566444395, %r14d      # imm = 0x5D5E0B6B
	cmoval	%r8d, %r14d
	ucomiss	%xmm1, %xmm2
	movd	%xmm1, %r9d
	movl	$1566444395, %r11d      # imm = 0x5D5E0B6B
	cmoval	%r9d, %r11d
	movd	8(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm2
	movd	%xmm3, %ebp
	cmoval	%ebp, %r10d
	movss	.LCPI11_1(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	movl	$-581039253, %ebx       # imm = 0xDD5E0B6B
	cmovbel	%ebx, %r8d
	ucomiss	%xmm2, %xmm1
	cmovbel	%ebx, %r9d
	ucomiss	%xmm2, %xmm3
	cmoval	%ebp, %ebx
	movd	16(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movd	%r14d, %xmm1
	ucomiss	%xmm0, %xmm1
	movd	%xmm0, %ebp
	cmoval	%ebp, %r14d
	movd	20(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movd	%r11d, %xmm2
	ucomiss	%xmm1, %xmm2
	movd	%xmm1, %edx
	cmoval	%edx, %r11d
	movd	24(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movd	%r10d, %xmm3
	ucomiss	%xmm2, %xmm3
	movd	%xmm2, %eax
	cmoval	%eax, %r10d
	movd	%r8d, %xmm3
	ucomiss	%xmm3, %xmm0
	cmoval	%ebp, %r8d
	movd	%r9d, %xmm0
	ucomiss	%xmm0, %xmm1
	cmoval	%edx, %r9d
	movd	%ebx, %xmm0
	ucomiss	%xmm0, %xmm2
	cmoval	%eax, %ebx
	movd	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movd	%r14d, %xmm1
	ucomiss	%xmm0, %xmm1
	movd	%xmm0, %eax
	cmoval	%eax, %r14d
	movd	36(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movd	%r11d, %xmm2
	ucomiss	%xmm1, %xmm2
	movd	%xmm1, %edx
	cmoval	%edx, %r11d
	movd	40(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movd	%r10d, %xmm3
	ucomiss	%xmm2, %xmm3
	movd	%xmm2, %esi
	cmoval	%esi, %r10d
	movd	%r8d, %xmm3
	ucomiss	%xmm3, %xmm0
	cmoval	%eax, %r8d
	movd	%r9d, %xmm0
	ucomiss	%xmm0, %xmm1
	cmoval	%edx, %r9d
	movd	%ebx, %xmm0
	ucomiss	%xmm0, %xmm2
	cmoval	%esi, %ebx
	movd	%r8d, %xmm2
	movd	%r14d, %xmm1
	movdqa	%xmm2, %xmm3
	subss	%xmm1, %xmm3
	movss	.LCPI11_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm0
	jbe	.LBB11_2
# BB#1:
	addss	.LCPI11_3(%rip), %xmm2
	movd	%xmm2, %r8d
	addss	.LCPI11_4(%rip), %xmm1
	movd	%xmm1, %r14d
.LBB11_2:
	movd	%r9d, %xmm2
	movd	%r11d, %xmm1
	movdqa	%xmm2, %xmm3
	subss	%xmm1, %xmm3
	ucomiss	%xmm3, %xmm0
	jbe	.LBB11_4
# BB#3:
	addss	.LCPI11_3(%rip), %xmm2
	movd	%xmm2, %r9d
	addss	.LCPI11_4(%rip), %xmm1
	movd	%xmm1, %r11d
.LBB11_4:
	movd	%ebx, %xmm2
	movd	%r10d, %xmm1
	movdqa	%xmm2, %xmm3
	subss	%xmm1, %xmm3
	ucomiss	%xmm3, %xmm0
	jbe	.LBB11_6
# BB#5:
	addss	.LCPI11_3(%rip), %xmm2
	movd	%xmm2, %ebx
	addss	.LCPI11_4(%rip), %xmm1
	movd	%xmm1, %r10d
.LBB11_6:
	movq	8(%rdi), %rbp
	movq	16(%rdi), %rax
	movd	%r14d, %xmm2
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	movd	%r11d, %xmm3
	subss	%xmm8, %xmm3
	movd	%r10d, %xmm4
	movss	16(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm4
	movss	40(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	movss	44(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	movss	48(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	cvttss2si	%xmm2, %esi
	andl	$65534, %esi            # imm = 0xFFFE
	cvttss2si	%xmm3, %edi
	andl	$65534, %edi            # imm = 0xFFFE
	cvttss2si	%xmm4, %r13d
	andl	$65534, %r13d           # imm = 0xFFFE
	movd	%r8d, %xmm2
	subss	%xmm1, %xmm2
	movd	%r9d, %xmm1
	subss	%xmm8, %xmm1
	movd	%ebx, %xmm3
	subss	%xmm5, %xmm3
	mulss	%xmm6, %xmm2
	mulss	%xmm7, %xmm1
	mulss	%xmm0, %xmm3
	movss	.LCPI11_5(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm2
	cvttss2si	%xmm2, %r14d
	orl	$1, %r14d
	addss	%xmm0, %xmm1
	cvttss2si	%xmm1, %r15d
	orl	$1, %r15d
	addss	%xmm0, %xmm3
	cvttss2si	%xmm3, %r12d
	orl	$1, %r12d
	movl	%ecx, %edx
	shll	$21, %edx
	orl	8(%rsp), %edx           # 4-byte Folded Reload
	movl	4(%rbp), %eax
	cmpl	8(%rbp), %eax
	jne	.LBB11_24
# BB#7:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebx
	cmovnel	%ecx, %ebx
	cmpl	%ebx, %eax
	jge	.LBB11_24
# BB#8:
	testl	%ebx, %ebx
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movl	%esi, 20(%rsp)          # 4-byte Spill
	movl	%edi, 16(%rsp)          # 4-byte Spill
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	je	.LBB11_9
# BB#10:
	movslq	%ebx, %rdi
	shlq	$4, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	4(%rbp), %eax
	testl	%eax, %eax
	jg	.LBB11_12
	jmp	.LBB11_19
.LBB11_9:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB11_19
.LBB11_12:                              # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB11_13
# BB#14:                                # %.prol.preheader
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_15:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%rbx,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB11_15
	jmp	.LBB11_16
.LBB11_13:
	xorl	%ecx, %ecx
.LBB11_16:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB11_19
# BB#17:                                # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB11_18:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbx,%rcx)
	movq	16(%rbp), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbx,%rcx)
	movq	16(%rbp), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbx,%rcx)
	movq	16(%rbp), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbx,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB11_18
.LBB11_19:                              # %_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_.exit.i.i
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_23
# BB#20:
	cmpb	$0, 24(%rbp)
	je	.LBB11_22
# BB#21:
	callq	_Z21btAlignedFreeInternalPv
.LBB11_22:
	movq	$0, 16(%rbp)
.LBB11_23:                              # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv.exit.i.i
	movb	$1, 24(%rbp)
	movq	%rbx, 16(%rbp)
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 8(%rbp)
	movl	4(%rbp), %eax
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	20(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %edi          # 4-byte Reload
.LBB11_24:                              # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9push_backERKS0_.exit
	movq	16(%rbp), %rcx
	cltq
	shlq	$4, %rax
	movw	%si, (%rcx,%rax)
	movw	%di, 2(%rcx,%rax)
	movw	%r13w, 4(%rcx,%rax)
	movw	%r14w, 6(%rcx,%rax)
	movw	%r15w, 8(%rcx,%rax)
	movw	%r12w, 10(%rcx,%rax)
	movl	%edx, 12(%rcx,%rax)
	incl	4(%rbp)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallback28internalProcessTriangleIndexEPS2_ii, .Lfunc_end11-_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallback28internalProcessTriangleIndexEPS2_ii
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackD0Ev,@function
_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackD0Ev: # @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackD0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi76:
	.cfi_def_cfa_offset 32
.Lcfi77:
	.cfi_offset %rbx, -24
.Lcfi78:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp30:
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp31:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB12_2:
.Ltmp32:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackD0Ev, .Lfunc_end12-_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp30-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin3   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end12-.Ltmp31    #   Call between .Ltmp31 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI13_0:
	.long	1566444395              # float 9.99999984E+17
.LCPI13_1:
	.long	3713928043              # float -9.99999984E+17
	.text
	.p2align	4, 0x90
	.type	_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallback28internalProcessTriangleIndexEPS2_ii,@function
_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallback28internalProcessTriangleIndexEPS2_ii: # @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallback28internalProcessTriangleIndexEPS2_ii
	.cfi_startproc
# BB#0:                                 # %_Z8btSetMinIfEvRT_RKS0_.exit.i20
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi82:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi83:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi85:
	.cfi_def_cfa_offset 112
.Lcfi86:
	.cfi_offset %rbx, -56
.Lcfi87:
	.cfi_offset %r12, -48
.Lcfi88:
	.cfi_offset %r13, -40
.Lcfi89:
	.cfi_offset %r14, -32
.Lcfi90:
	.cfi_offset %r15, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movd	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movd	4(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI13_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	movd	%xmm1, %r13d
	movl	$1566444395, %r9d       # imm = 0x5D5E0B6B
	movl	$1566444395, %r10d      # imm = 0x5D5E0B6B
	cmoval	%r13d, %r10d
	ucomiss	%xmm2, %xmm0
	movd	%xmm2, %r15d
	movl	$1566444395, %r11d      # imm = 0x5D5E0B6B
	cmoval	%r15d, %r11d
	movd	8(%rsi), %xmm4          # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm0
	movd	%xmm4, %eax
	cmoval	%eax, %r9d
	movss	12(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm5, %xmm5
	movaps	%xmm0, %xmm3
	minss	%xmm5, %xmm3
	movss	.LCPI13_1(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm1
	movl	$-581039253, %r12d      # imm = 0xDD5E0B6B
	cmovbel	%r12d, %r13d
	ucomiss	%xmm6, %xmm2
	cmovbel	%r12d, %r15d
	ucomiss	%xmm6, %xmm4
	cmoval	%eax, %r12d
	maxss	%xmm5, %xmm0
	movd	16(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movd	%r10d, %xmm1
	ucomiss	%xmm4, %xmm1
	movd	%xmm4, %eax
	cmoval	%eax, %r10d
	movd	20(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movd	%r11d, %xmm1
	ucomiss	%xmm5, %xmm1
	movd	%xmm5, %ebx
	cmoval	%ebx, %r11d
	movd	24(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movd	%r9d, %xmm1
	ucomiss	%xmm6, %xmm1
	movd	%xmm6, %ebp
	cmoval	%ebp, %r9d
	movss	28(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	minss	%xmm3, %xmm2
	movd	%r13d, %xmm3
	ucomiss	%xmm3, %xmm4
	cmoval	%eax, %r13d
	movd	%r15d, %xmm3
	ucomiss	%xmm3, %xmm5
	cmoval	%ebx, %r15d
	movd	%r12d, %xmm3
	ucomiss	%xmm3, %xmm6
	cmoval	%ebp, %r12d
	maxss	%xmm0, %xmm1
	movd	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movd	%r10d, %xmm3
	ucomiss	%xmm0, %xmm3
	movd	%xmm0, %eax
	cmoval	%eax, %r10d
	movd	36(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movd	%r11d, %xmm4
	ucomiss	%xmm3, %xmm4
	movd	%xmm3, %ebx
	cmoval	%ebx, %r11d
	movd	40(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movd	%r9d, %xmm5
	ucomiss	%xmm4, %xmm5
	movd	%xmm4, %ebp
	cmoval	%ebp, %r9d
	movss	44(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm6
	minss	%xmm2, %xmm6
	movd	%r13d, %xmm2
	ucomiss	%xmm2, %xmm0
	cmoval	%eax, %r13d
	movd	%r15d, %xmm0
	ucomiss	%xmm0, %xmm3
	cmoval	%ebx, %r15d
	movd	%r12d, %xmm0
	ucomiss	%xmm0, %xmm4
	cmoval	%ebp, %r12d
	maxss	%xmm1, %xmm5
	movq	8(%rdi), %r14
	movl	4(%r14), %eax
	cmpl	8(%r14), %eax
	jne	.LBB13_18
# BB#1:
	leal	(%rax,%rax), %esi
	testl	%eax, %eax
	movl	$1, %edi
	cmovnel	%esi, %edi
	cmpl	%edi, %eax
	jge	.LBB13_18
# BB#2:
	testl	%edi, %edi
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	movl	%r11d, 20(%rsp)         # 4-byte Spill
	movss	%xmm5, 16(%rsp)         # 4-byte Spill
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	movl	%edi, 28(%rsp)          # 4-byte Spill
	je	.LBB13_3
# BB#4:
	movslq	%edi, %rdi
	shlq	$6, %rdi
	movl	$16, %esi
	movl	%edx, %ebx
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	movl	%r10d, %ebp
	callq	_Z22btAlignedAllocInternalmi
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movl	20(%rsp), %r11d         # 4-byte Reload
	movl	%ebp, %r10d
	movl	8(%rsp), %r9d           # 4-byte Reload
	movl	%ebx, %edx
	movq	%rax, %rbx
	movl	4(%r14), %eax
	testl	%eax, %eax
	jg	.LBB13_6
	jmp	.LBB13_13
.LBB13_3:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB13_13
.LBB13_6:                               # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB13_7
# BB#8:                                 # %.prol.preheader
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB13_9:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rbp
	movdqu	(%rbp,%rdi), %xmm0
	movups	16(%rbp,%rdi), %xmm1
	movdqu	32(%rbp,%rdi), %xmm2
	movdqu	48(%rbp,%rdi), %xmm3
	movdqu	%xmm3, 48(%rbx,%rdi)
	movdqu	%xmm2, 32(%rbx,%rdi)
	movups	%xmm1, 16(%rbx,%rdi)
	movdqu	%xmm0, (%rbx,%rdi)
	incq	%rcx
	addq	$64, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB13_9
	jmp	.LBB13_10
.LBB13_7:
	xorl	%ecx, %ecx
.LBB13_10:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB13_13
# BB#11:                                # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$6, %rcx
	addq	$192, %rcx
	.p2align	4, 0x90
.LBB13_12:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rsi
	movups	-192(%rsi,%rcx), %xmm0
	movups	-176(%rsi,%rcx), %xmm1
	movups	-160(%rsi,%rcx), %xmm2
	movups	-144(%rsi,%rcx), %xmm3
	movups	%xmm3, -144(%rbx,%rcx)
	movups	%xmm2, -160(%rbx,%rcx)
	movups	%xmm1, -176(%rbx,%rcx)
	movups	%xmm0, -192(%rbx,%rcx)
	movq	16(%r14), %rsi
	movups	-128(%rsi,%rcx), %xmm0
	movups	-112(%rsi,%rcx), %xmm1
	movups	-96(%rsi,%rcx), %xmm2
	movups	-80(%rsi,%rcx), %xmm3
	movups	%xmm3, -80(%rbx,%rcx)
	movups	%xmm2, -96(%rbx,%rcx)
	movups	%xmm1, -112(%rbx,%rcx)
	movups	%xmm0, -128(%rbx,%rcx)
	movq	16(%r14), %rsi
	movups	-64(%rsi,%rcx), %xmm0
	movups	-48(%rsi,%rcx), %xmm1
	movups	-32(%rsi,%rcx), %xmm2
	movups	-16(%rsi,%rcx), %xmm3
	movups	%xmm3, -16(%rbx,%rcx)
	movups	%xmm2, -32(%rbx,%rcx)
	movups	%xmm1, -48(%rbx,%rcx)
	movups	%xmm0, -64(%rbx,%rcx)
	movq	16(%r14), %rsi
	movdqu	(%rsi,%rcx), %xmm0
	movups	16(%rsi,%rcx), %xmm1
	movdqu	32(%rsi,%rcx), %xmm2
	movdqu	48(%rsi,%rcx), %xmm3
	movdqu	%xmm3, 48(%rbx,%rcx)
	movdqu	%xmm2, 32(%rbx,%rcx)
	movups	%xmm1, 16(%rbx,%rcx)
	movdqu	%xmm0, (%rbx,%rcx)
	addq	$256, %rcx              # imm = 0x100
	addq	$-4, %rax
	jne	.LBB13_12
.LBB13_13:                              # %_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4copyEiiPS0_.exit.i.i
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB13_17
# BB#14:
	cmpb	$0, 24(%r14)
	je	.LBB13_16
# BB#15:
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movl	%r9d, %ebp
	movl	%r10d, 24(%rsp)         # 4-byte Spill
	callq	_Z21btAlignedFreeInternalPv
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movl	20(%rsp), %r11d         # 4-byte Reload
	movl	24(%rsp), %r10d         # 4-byte Reload
	movl	%ebp, %r9d
	movl	8(%rsp), %edx           # 4-byte Reload
.LBB13_16:
	movq	$0, 16(%r14)
.LBB13_17:                              # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv.exit.i.i
	movb	$1, 24(%r14)
	movq	%rbx, 16(%r14)
	movl	28(%rsp), %eax          # 4-byte Reload
	movl	%eax, 8(%r14)
	movl	4(%r14), %eax
	movl	32(%rsp), %ecx          # 4-byte Reload
.LBB13_18:                              # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE9push_backERKS0_.exit
	movq	16(%r14), %rsi
	cltq
	shlq	$6, %rax
	movl	%r10d, (%rsi,%rax)
	movl	%r11d, 4(%rsi,%rax)
	movl	%r9d, 8(%rsi,%rax)
	movss	%xmm6, 12(%rsi,%rax)
	movl	%r13d, 16(%rsi,%rax)
	movl	%r15d, 20(%rsi,%rax)
	movl	%r12d, 24(%rsi,%rax)
	movss	%xmm5, 28(%rsi,%rax)
	movl	$-1, 32(%rsi,%rax)
	movl	%edx, 36(%rsi,%rax)
	movl	%ecx, 40(%rsi,%rax)
	movl	52(%rsp), %edx
	movl	%edx, 60(%rsi,%rax)
	movups	36(%rsp), %xmm0
	movups	%xmm0, 44(%rsi,%rax)
	incl	4(%r14)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallback28internalProcessTriangleIndexEPS2_ii, .Lfunc_end13-_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallback28internalProcessTriangleIndexEPS2_ii
	.cfi_endproc

	.type	_ZTV14btOptimizedBvh,@object # @_ZTV14btOptimizedBvh
	.section	.rodata,"a",@progbits
	.globl	_ZTV14btOptimizedBvh
	.p2align	3
_ZTV14btOptimizedBvh:
	.quad	0
	.quad	_ZTI14btOptimizedBvh
	.quad	_ZN14btOptimizedBvhD2Ev
	.quad	_ZN14btOptimizedBvhD0Ev
	.quad	_ZN14btOptimizedBvh9serializeEPvjb
	.size	_ZTV14btOptimizedBvh, 40

	.type	_ZTS14btOptimizedBvh,@object # @_ZTS14btOptimizedBvh
	.globl	_ZTS14btOptimizedBvh
	.p2align	4
_ZTS14btOptimizedBvh:
	.asciz	"14btOptimizedBvh"
	.size	_ZTS14btOptimizedBvh, 17

	.type	_ZTI14btOptimizedBvh,@object # @_ZTI14btOptimizedBvh
	.globl	_ZTI14btOptimizedBvh
	.p2align	4
_ZTI14btOptimizedBvh:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14btOptimizedBvh
	.quad	_ZTI14btQuantizedBvh
	.size	_ZTI14btOptimizedBvh, 24

	.type	_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback,@object # @_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback
	.p2align	3
_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback:
	.quad	0
	.quad	_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback
	.quad	_ZN31btInternalTriangleIndexCallbackD2Ev
	.quad	_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackD0Ev
	.quad	_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallback28internalProcessTriangleIndexEPS2_ii
	.size	_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback, 40

	.type	_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback,@object # @_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback
	.p2align	4
_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback:
	.asciz	"ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback"
	.size	_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback, 100

	.type	_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback,@object # @_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback
	.p2align	4
_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback
	.quad	_ZTI31btInternalTriangleIndexCallback
	.size	_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback, 24

	.type	_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback,@object # @_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback
	.p2align	3
_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback:
	.quad	0
	.quad	_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback
	.quad	_ZN31btInternalTriangleIndexCallbackD2Ev
	.quad	_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackD0Ev
	.quad	_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallback28internalProcessTriangleIndexEPS2_ii
	.size	_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback, 40

	.type	_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback,@object # @_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback
	.p2align	4
_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback:
	.asciz	"ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback"
	.size	_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback, 91

	.type	_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback,@object # @_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback
	.p2align	4
_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback
	.quad	_ZTI31btInternalTriangleIndexCallback
	.size	_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback, 24


	.globl	_ZN14btOptimizedBvhC1Ev
	.type	_ZN14btOptimizedBvhC1Ev,@function
_ZN14btOptimizedBvhC1Ev = _ZN14btOptimizedBvhC2Ev
	.globl	_ZN14btOptimizedBvhD1Ev
	.type	_ZN14btOptimizedBvhD1Ev,@function
_ZN14btOptimizedBvhD1Ev = _ZN14btOptimizedBvhD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
