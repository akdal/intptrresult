	.text
	.file	"jfdctflt.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1060439283              # float 0.707106769
.LCPI0_1:
	.long	1053028117              # float 0.382683426
.LCPI0_2:
	.long	1057655764              # float 0.541196108
.LCPI0_3:
	.long	1067924853              # float 1.30656302
	.text
	.globl	jpeg_fdct_float
	.p2align	4, 0x90
	.type	jpeg_fdct_float,@function
jpeg_fdct_float:                        # @jpeg_fdct_float
	.cfi_startproc
# BB#0:
	movl	$8, %eax
	movss	.LCPI0_0(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movss	.LCPI0_1(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movss	.LCPI0_2(%rip), %xmm9   # xmm9 = mem[0],zero,zero,zero
	movss	.LCPI0_3(%rip), %xmm10  # xmm10 = mem[0],zero,zero,zero
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movss	28(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm13          # xmm13 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm1
	addss	%xmm4, %xmm1
	subss	%xmm4, %xmm13
	movss	24(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	addss	%xmm6, %xmm4
	subss	%xmm6, %xmm2
	movss	8(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm3
	addss	%xmm7, %xmm3
	subss	%xmm7, %xmm6
	movss	12(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	16(%rcx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	addss	%xmm12, %xmm0
	subss	%xmm12, %xmm7
	movaps	%xmm1, %xmm5
	addss	%xmm0, %xmm5
	subss	%xmm0, %xmm1
	movaps	%xmm4, %xmm0
	addss	%xmm3, %xmm0
	subss	%xmm3, %xmm4
	movaps	%xmm0, %xmm3
	addss	%xmm5, %xmm3
	movss	%xmm3, (%rcx)
	subss	%xmm0, %xmm5
	movss	%xmm5, 16(%rcx)
	addss	%xmm1, %xmm4
	mulss	%xmm11, %xmm4
	movaps	%xmm1, %xmm0
	addss	%xmm4, %xmm0
	movss	%xmm0, 8(%rcx)
	subss	%xmm4, %xmm1
	movss	%xmm1, 24(%rcx)
	addss	%xmm6, %xmm7
	addss	%xmm2, %xmm6
	addss	%xmm13, %xmm2
	movaps	%xmm7, %xmm0
	subss	%xmm2, %xmm0
	mulss	%xmm8, %xmm0
	mulss	%xmm9, %xmm7
	addss	%xmm0, %xmm7
	mulss	%xmm10, %xmm2
	addss	%xmm0, %xmm2
	mulss	%xmm11, %xmm6
	movaps	%xmm13, %xmm0
	addss	%xmm6, %xmm0
	subss	%xmm6, %xmm13
	movaps	%xmm13, %xmm1
	addss	%xmm7, %xmm1
	movss	%xmm1, 20(%rcx)
	subss	%xmm7, %xmm13
	movss	%xmm13, 12(%rcx)
	movaps	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	%xmm1, 4(%rcx)
	subss	%xmm2, %xmm0
	movss	%xmm0, 28(%rcx)
	addq	$32, %rcx
	decl	%eax
	jg	.LBB0_1
# BB#2:                                 # %.preheader.preheader
	movl	$8, %eax
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movss	224(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	(%rdi), %xmm13          # xmm13 = mem[0],zero,zero,zero
	movss	32(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm5
	addss	%xmm0, %xmm5
	subss	%xmm0, %xmm13
	movss	192(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm2
	addss	%xmm0, %xmm2
	subss	%xmm0, %xmm4
	movss	64(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	160(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm3
	addss	%xmm0, %xmm3
	subss	%xmm0, %xmm6
	movss	96(%rdi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	128(%rdi), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	addss	%xmm12, %xmm0
	subss	%xmm12, %xmm7
	movaps	%xmm5, %xmm1
	addss	%xmm0, %xmm1
	subss	%xmm0, %xmm5
	movaps	%xmm2, %xmm0
	addss	%xmm3, %xmm0
	subss	%xmm3, %xmm2
	movaps	%xmm0, %xmm3
	addss	%xmm1, %xmm3
	movss	%xmm3, (%rdi)
	subss	%xmm0, %xmm1
	movss	%xmm1, 128(%rdi)
	addss	%xmm5, %xmm2
	mulss	%xmm11, %xmm2
	movaps	%xmm5, %xmm0
	addss	%xmm2, %xmm0
	movss	%xmm0, 64(%rdi)
	subss	%xmm2, %xmm5
	movss	%xmm5, 192(%rdi)
	addss	%xmm6, %xmm7
	addss	%xmm4, %xmm6
	addss	%xmm13, %xmm4
	movaps	%xmm7, %xmm0
	subss	%xmm4, %xmm0
	mulss	%xmm8, %xmm0
	mulss	%xmm9, %xmm7
	addss	%xmm0, %xmm7
	mulss	%xmm10, %xmm4
	addss	%xmm0, %xmm4
	mulss	%xmm11, %xmm6
	movaps	%xmm13, %xmm0
	addss	%xmm6, %xmm0
	subss	%xmm6, %xmm13
	movaps	%xmm13, %xmm1
	addss	%xmm7, %xmm1
	movss	%xmm1, 160(%rdi)
	subss	%xmm7, %xmm13
	movss	%xmm13, 96(%rdi)
	movaps	%xmm0, %xmm1
	addss	%xmm4, %xmm1
	movss	%xmm1, 32(%rdi)
	subss	%xmm4, %xmm0
	movss	%xmm0, 224(%rdi)
	addq	$4, %rdi
	decl	%eax
	jg	.LBB0_3
# BB#4:
	retq
.Lfunc_end0:
	.size	jpeg_fdct_float, .Lfunc_end0-jpeg_fdct_float
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
