	.text
	.file	"Sort.bc"
	.globl	HeapSort
	.p2align	4, 0x90
	.type	HeapSort,@function
HeapSort:                               # @HeapSort
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	cmpl	$2, %esi
	jb	.LBB0_27
# BB#1:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	leaq	-4(%rdi), %r14
	movl	%esi, %r8d
	shrl	%r8d
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
	movl	%r8d, %eax
	movl	(%r14,%rax,4), %r9d
	leal	(%r8,%r8), %eax
	cmpl	%esi, %eax
	movl	%r8d, %ecx
	ja	.LBB0_10
# BB#3:                                 # %.lr.ph115.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%r8d, %edx
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph115
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%esi, %eax
	jae	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=2
	movl	%eax, %ecx
	orl	$1, %ecx
	movl	(%r14,%rcx,4), %r10d
	movl	%eax, %ebx
	cmpl	(%r14,%rbx,4), %r10d
	cmovbel	%eax, %ecx
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_6:                                #   in Loop: Header=BB0_4 Depth=2
	movl	%eax, %ecx
.LBB0_7:                                #   in Loop: Header=BB0_4 Depth=2
	movl	%ecx, %eax
	movl	(%r14,%rax,4), %eax
	cmpl	%eax, %r9d
	jae	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_4 Depth=2
	movl	%edx, %edx
	movl	%eax, (%r14,%rdx,4)
	leal	(%rcx,%rcx), %eax
	cmpl	%esi, %eax
	movl	%ecx, %edx
	jbe	.LBB0_4
	jmp	.LBB0_10
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_2 Depth=1
	movl	%edx, %ecx
.LBB0_10:                               # %._crit_edge116
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%ecx, %eax
	movl	%r9d, (%r14,%rax,4)
	decl	%r8d
	jne	.LBB0_2
# BB#11:                                # %.preheader
	movl	%esi, %r9d
	leaq	(%r14,%r9,4), %r10
	movl	(%r14,%r9,4), %r11d
	cmpl	$4, %esi
	jb	.LBB0_23
# BB#12:                                # %.lr.ph108
	leal	-1(%rsi), %r8d
	decq	%r8
	addl	$-4, %esi
	subq	%rsi, %r8
	.p2align	4, 0x90
.LBB0_13:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_15 Depth 2
	movl	(%rdi), %eax
	movl	4(%rdi), %ecx
	cmpl	8(%rdi), %ecx
	sbbl	%edx, %edx
	andl	$1, %edx
	leal	2(%rdx), %ecx
	leal	-1(%r9), %ebp
	movl	%eax, (%r10)
	movl	-4(%rdi,%rcx,4), %eax
	movl	%eax, (%rdi)
	leal	4(%rdx,%rdx), %eax
	cmpl	%ebp, %eax
	ja	.LBB0_20
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph
                                        #   Parent Loop BB0_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebp, %eax
	jae	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_15 Depth=2
	movl	%eax, %edx
	orl	$1, %edx
	movl	(%r14,%rdx,4), %ebx
	movl	%eax, %esi
	cmpl	(%r14,%rsi,4), %ebx
	cmovbel	%eax, %edx
	jmp	.LBB0_18
	.p2align	4, 0x90
.LBB0_17:                               #   in Loop: Header=BB0_15 Depth=2
	movl	%eax, %edx
.LBB0_18:                               #   in Loop: Header=BB0_15 Depth=2
	movl	%edx, %eax
	movl	(%r14,%rax,4), %eax
	cmpl	%eax, %r11d
	jae	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_15 Depth=2
	movl	%ecx, %ecx
	movl	%eax, (%r14,%rcx,4)
	leal	(%rdx,%rdx), %eax
	cmpl	%ebp, %eax
	movl	%edx, %ecx
	jbe	.LBB0_15
	jmp	.LBB0_21
	.p2align	4, 0x90
.LBB0_20:                               #   in Loop: Header=BB0_13 Depth=1
	movl	%ecx, %edx
.LBB0_21:                               # %._crit_edge
                                        #   in Loop: Header=BB0_13 Depth=1
	movl	%edx, %eax
	movl	%r11d, (%r14,%rax,4)
	movl	%ebp, %eax
	leaq	(%r14,%rax,4), %r10
	movl	(%r14,%rax,4), %r11d
	decq	%r9
	cmpl	$3, %ebp
	ja	.LBB0_13
# BB#22:                                # %._crit_edge109.thread
	movl	(%rdi), %eax
	movl	%eax, (%rdi,%r8,4)
	jmp	.LBB0_24
.LBB0_23:                               # %._crit_edge109
	movl	(%rdi), %eax
	movl	%eax, (%r10)
	cmpl	$3, %esi
	jne	.LBB0_26
.LBB0_24:
	movl	4(%rdi), %eax
	cmpl	%r11d, %eax
	jae	.LBB0_26
# BB#25:
	movl	%eax, (%rdi)
	addq	$4, %rdi
.LBB0_26:
	movl	%r11d, (%rdi)
	popq	%rbx
	popq	%r14
	popq	%rbp
.LBB0_27:
	retq
.Lfunc_end0:
	.size	HeapSort, .Lfunc_end0-HeapSort
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
