	.text
	.file	"rc4.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI0_1:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI0_2:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI0_3:
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	12                      # 0xc
.LCPI0_4:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
.LCPI0_5:
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	20                      # 0x14
.LCPI0_6:
	.long	24                      # 0x18
	.long	24                      # 0x18
	.long	24                      # 0x18
	.long	24                      # 0x18
.LCPI0_7:
	.long	28                      # 0x1c
	.long	28                      # 0x1c
	.long	28                      # 0x1c
	.long	28                      # 0x1c
.LCPI0_8:
	.long	32                      # 0x20
	.long	32                      # 0x20
	.long	32                      # 0x20
	.long	32                      # 0x20
	.text
	.globl	rc4_setup
	.p2align	4, 0x90
	.type	rc4_setup,@function
rc4_setup:                              # @rc4_setup
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movq	$0, (%rdi)
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	xorl	%eax, %eax
	movdqa	.LCPI0_1(%rip), %xmm8   # xmm8 = [4,4,4,4]
	movdqa	.LCPI0_2(%rip), %xmm9   # xmm9 = [8,8,8,8]
	movdqa	.LCPI0_3(%rip), %xmm10  # xmm10 = [12,12,12,12]
	movdqa	.LCPI0_4(%rip), %xmm4   # xmm4 = [16,16,16,16]
	movdqa	.LCPI0_5(%rip), %xmm5   # xmm5 = [20,20,20,20]
	movdqa	.LCPI0_6(%rip), %xmm6   # xmm6 = [24,24,24,24]
	movdqa	.LCPI0_7(%rip), %xmm7   # xmm7 = [28,28,28,28]
	movdqa	.LCPI0_8(%rip), %xmm1   # xmm1 = [32,32,32,32]
	.p2align	4, 0x90
.LBB0_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm2
	paddd	%xmm8, %xmm2
	movdqu	%xmm0, 8(%rdi,%rax,4)
	movdqu	%xmm2, 24(%rdi,%rax,4)
	movdqa	%xmm0, %xmm2
	paddd	%xmm9, %xmm2
	movdqa	%xmm0, %xmm3
	paddd	%xmm10, %xmm3
	movdqu	%xmm2, 40(%rdi,%rax,4)
	movdqu	%xmm3, 56(%rdi,%rax,4)
	movdqa	%xmm0, %xmm2
	paddd	%xmm4, %xmm2
	movdqa	%xmm0, %xmm3
	paddd	%xmm5, %xmm3
	movdqu	%xmm2, 72(%rdi,%rax,4)
	movdqu	%xmm3, 88(%rdi,%rax,4)
	movdqa	%xmm0, %xmm2
	paddd	%xmm6, %xmm2
	movdqa	%xmm0, %xmm3
	paddd	%xmm7, %xmm3
	movdqu	%xmm2, 104(%rdi,%rax,4)
	movdqu	%xmm3, 120(%rdi,%rax,4)
	addq	$32, %rax
	paddd	%xmm1, %xmm0
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB0_1
# BB#2:                                 # %.preheader.preheader
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rdi,%r9,4), %r10d
	addl	%r10d, %r11d
	cltq
	movzbl	(%rsi,%rax), %ecx
	addl	%r11d, %ecx
	movzbl	%cl, %r11d
	movl	8(%rdi,%r11,4), %ecx
	movl	%ecx, 8(%rdi,%r9,4)
	movl	%r10d, 8(%rdi,%r11,4)
	incl	%eax
	cmpl	%edx, %eax
	cmovgel	%r8d, %eax
	incq	%r9
	cmpq	$256, %r9               # imm = 0x100
	jne	.LBB0_3
# BB#4:
	retq
.Lfunc_end0:
	.size	rc4_setup, .Lfunc_end0-rc4_setup
	.cfi_endproc

	.globl	rc4_crypt
	.p2align	4, 0x90
	.type	rc4_crypt,@function
rc4_crypt:                              # @rc4_crypt
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	movl	4(%rdi), %ecx
	testl	%edx, %edx
	jle	.LBB1_3
# BB#1:                                 # %.lr.ph.preheader
	movl	%edx, %r8d
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%eax
	movzbl	%al, %eax
	movl	8(%rdi,%rax,4), %r9d
	addl	%r9d, %ecx
	movzbl	%cl, %ecx
	movl	8(%rdi,%rcx,4), %edx
	movl	%edx, 8(%rdi,%rax,4)
	movl	%r9d, 8(%rdi,%rcx,4)
	addl	%r9d, %edx
	movzbl	%dl, %r9d
	movzbl	(%rsi), %edx
	xorl	8(%rdi,%r9,4), %edx
	movb	%dl, (%rsi)
	incq	%rsi
	decq	%r8
	jne	.LBB1_2
.LBB1_3:                                # %._crit_edge
	movl	%eax, (%rdi)
	movl	%ecx, 4(%rdi)
	retq
.Lfunc_end1:
	.size	rc4_crypt, .Lfunc_end1-rc4_crypt
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI2_1:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI2_2:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI2_3:
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	12                      # 0xc
.LCPI2_4:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
.LCPI2_5:
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	20                      # 0x14
.LCPI2_6:
	.long	24                      # 0x18
	.long	24                      # 0x18
	.long	24                      # 0x18
	.long	24                      # 0x18
.LCPI2_7:
	.long	28                      # 0x1c
	.long	28                      # 0x1c
	.long	28                      # 0x1c
	.long	28                      # 0x1c
.LCPI2_8:
	.long	32                      # 0x20
	.long	32                      # 0x20
	.long	32                      # 0x20
	.long	32                      # 0x20
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1112, %rsp             # imm = 0x458
.Lcfi6:
	.cfi_def_cfa_offset 1168
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	$200000, %eax           # imm = 0x30D40
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpl	$2, %edi
	jne	.LBB2_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB2_2:
	movl	$.Lstr, %edi
	callq	puts
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
                                        # implicit-def: %EBX
                                        # implicit-def: %R13D
	.p2align	4, 0x90
.LBB2_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_6 Depth 2
                                        #       Child Loop BB2_7 Depth 3
                                        #       Child Loop BB2_9 Depth 3
                                        #       Child Loop BB2_11 Depth 3
	leaq	1(%r15), %r14
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_4
# BB#5:                                 # %.lr.ph.split.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%r14, 32(%rsp)          # 8-byte Spill
	imulq	$30, %r15, %rax
	leaq	data(%rax), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movb	data_len(%r15), %cl
	movb	%cl, 15(%rsp)           # 1-byte Spill
	movzbl	%cl, %edx
	leaq	keys+1(%rax), %r15
	movzbl	keys(%rax), %r14d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph.split
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_7 Depth 3
                                        #       Child Loop BB2_9 Depth 3
                                        #       Child Loop BB2_11 Depth 3
	leaq	48(%rsp), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rdx, %rbx
	callq	memcpy
	xorl	%eax, %eax
	movdqa	.LCPI2_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI2_1(%rip), %xmm3   # xmm3 = [4,4,4,4]
	movdqa	%xmm3, %xmm8
	movdqa	.LCPI2_2(%rip), %xmm4   # xmm4 = [8,8,8,8]
	movdqa	%xmm4, %xmm9
	movdqa	.LCPI2_3(%rip), %xmm5   # xmm5 = [12,12,12,12]
	movdqa	%xmm5, %xmm10
	movdqa	.LCPI2_4(%rip), %xmm6   # xmm6 = [16,16,16,16]
	movdqa	.LCPI2_5(%rip), %xmm7   # xmm7 = [20,20,20,20]
	movdqa	.LCPI2_6(%rip), %xmm3   # xmm3 = [24,24,24,24]
	movdqa	.LCPI2_7(%rip), %xmm4   # xmm4 = [28,28,28,28]
	movdqa	.LCPI2_8(%rip), %xmm5   # xmm5 = [32,32,32,32]
	.p2align	4, 0x90
.LBB2_7:                                # %vector.body
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqa	%xmm0, %xmm1
	paddd	%xmm8, %xmm1
	movdqu	%xmm0, 88(%rsp,%rax,4)
	movdqu	%xmm1, 104(%rsp,%rax,4)
	movdqa	%xmm0, %xmm1
	paddd	%xmm9, %xmm1
	movdqa	%xmm0, %xmm2
	paddd	%xmm10, %xmm2
	movdqu	%xmm1, 120(%rsp,%rax,4)
	movdqu	%xmm2, 136(%rsp,%rax,4)
	movdqa	%xmm0, %xmm1
	paddd	%xmm6, %xmm1
	movdqa	%xmm0, %xmm2
	paddd	%xmm7, %xmm2
	movdqu	%xmm1, 152(%rsp,%rax,4)
	movdqu	%xmm2, 168(%rsp,%rax,4)
	movdqa	%xmm0, %xmm1
	paddd	%xmm3, %xmm1
	movdqa	%xmm0, %xmm2
	paddd	%xmm4, %xmm2
	movdqu	%xmm1, 184(%rsp,%rax,4)
	movdqu	%xmm2, 200(%rsp,%rax,4)
	addq	$32, %rax
	paddd	%xmm5, %xmm0
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB2_7
# BB#8:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB2_6 Depth=2
	xorl	%eax, %eax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_9:                                # %.preheader.i
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	88(%rsp,%rax,4), %esi
	addl	%esi, %edx
	movslq	%ecx, %rcx
	movzbl	(%r15,%rcx), %edi
	addl	%edx, %edi
	movzbl	%dil, %edx
	movl	88(%rsp,%rdx,4), %edi
	movl	%edi, 88(%rsp,%rax,4)
	movl	%esi, 88(%rsp,%rdx,4)
	incl	%ecx
	cmpl	%r14d, %ecx
	cmovgel	%ebp, %ecx
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB2_9
# BB#10:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB2_6 Depth=2
	xorl	%r13d, %r13d
	movq	%rbx, %rdx
	movq	%rdx, %rax
	leaq	48(%rsp), %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_11:                               # %.lr.ph.i
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%r13d
	movzbl	%r13b, %r13d
	movl	88(%rsp,%r13,4), %edi
	addl	%edi, %ebx
	movzbl	%bl, %ebx
	movl	88(%rsp,%rbx,4), %esi
	movl	%esi, 88(%rsp,%r13,4)
	movl	%edi, 88(%rsp,%rbx,4)
	addl	%edi, %esi
	movzbl	%sil, %edi
	movzbl	(%rcx), %esi
	xorl	88(%rsp,%rdi,4), %esi
	movb	%sil, (%rcx)
	incq	%rcx
	decq	%rax
	jne	.LBB2_11
# BB#12:                                # %rc4_crypt.exit
                                        #   in Loop: Header=BB2_6 Depth=2
	incl	%r12d
	cmpl	16(%rsp), %r12d         # 4-byte Folded Reload
	jne	.LBB2_6
# BB#13:                                #   in Loop: Header=BB2_3 Depth=1
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	movb	15(%rsp), %cl           # 1-byte Reload
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_4:                                # %.._crit_edge_crit_edge
                                        #   in Loop: Header=BB2_3 Depth=1
	movb	data_len(%r15), %cl
.LBB2_14:                               # %._crit_edge
                                        #   in Loop: Header=BB2_3 Depth=1
	imulq	$30, %r15, %rax
	leaq	output(%rax), %rsi
	movzbl	%cl, %edx
	leaq	48(%rsp), %rdi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB2_15
# BB#16:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$.Lstr.1, %edi
	callq	puts
	cmpq	$6, %r14
	movq	%r14, %r15
	jl	.LBB2_3
# BB#17:
	movl	%r13d, 80(%rsp)
	movl	%ebx, 84(%rsp)
	movl	$10, %edi
	callq	putchar
	xorl	%eax, %eax
	jmp	.LBB2_18
.LBB2_15:
	movl	%r13d, 80(%rsp)
	movl	%ebx, 84(%rsp)
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$1, %eax
.LBB2_18:
	addq	$1112, %rsp             # imm = 0x458
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	" Test %d "
	.size	.L.str.1, 10

	.type	data,@object            # @data
	.section	.rodata,"a",@progbits
	.p2align	4
data:
	.asciz	"\001#Eg\211\253\315\357\377\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\000\000\000\000\000\000\000\000\377\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\000\000\000\000\000\000\000\000\377\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\377\000\000\000\000\000\000\000\000"
	.asciz	"\0224Vx\232\274\336\360\0224Vx\232\274\336\360\0224Vx\232\274\336\360\0224Vx\377"
	.asciz	"\000\000\000\000\000\000\000\000\000\000\377\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.zero	30
	.size	data, 210

	.type	data_len,@object        # @data_len
	.section	.rodata.str1.1,"aMS",@progbits,1
data_len:
	.asciz	"\b\b\b\024\034\n"
	.size	data_len, 7

	.type	keys,@object            # @keys
	.data
	.p2align	4
keys:
	.asciz	"\b\001#Eg\211\253\315\357\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\b\001#Eg\211\253\315\357\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\b\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\004\357\001#E\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\b\001#Eg\211\253\315\357\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\004\357\001#E\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.zero	30
	.size	keys, 210

	.type	output,@object          # @output
	.p2align	4
output:
	.asciz	"u\267\207\200\231\340\305\226\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"t\224\302\347\020K\by\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\336\030\211A\2437]:\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\326\241A\247\354<8\337\275aZ\021b\341\307\2726\266xX\000\000\000\000\000\000\000\000\000"
	.asciz	"f\240\224\237\212\367\326\211\037\177\203+\2503\300\f\211.\2760\024<\342\207@\001\036\317\000"
	.asciz	"\326\241A\247\354<8\337\275a\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.zero	30
	.size	output, 210

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"\n RC4 Validation Tests:\n"
	.size	.Lstr, 25

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.1:
	.asciz	"passed."
	.size	.Lstr.1, 8

	.type	.Lstr.2,@object         # @str.2
.Lstr.2:
	.asciz	"failed!"
	.size	.Lstr.2, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
