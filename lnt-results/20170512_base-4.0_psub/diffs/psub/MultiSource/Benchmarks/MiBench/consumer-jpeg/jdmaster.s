	.text
	.file	"jdmaster.bc"
	.globl	jpeg_calc_output_dimensions
	.p2align	4, 0x90
	.type	jpeg_calc_output_dimensions,@function
jpeg_calc_output_dimensions:            # @jpeg_calc_output_dimensions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	28(%r14), %eax
	cmpl	$202, %eax
	je	.LBB0_2
# BB#1:
	movq	(%r14), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%r14, %rdi
	callq	*(%rcx)
.LBB0_2:
	movl	60(%r14), %eax
	movl	64(%r14), %ecx
	leal	(,%rax,8), %edx
	cmpl	%ecx, %edx
	jbe	.LBB0_3
# BB#4:
	leal	(,%rax,4), %edx
	cmpl	%ecx, %edx
	jbe	.LBB0_5
# BB#6:
	addl	%eax, %eax
	cmpl	%ecx, %eax
	movl	40(%r14), %edi
	jbe	.LBB0_7
# BB#8:
	movl	%edi, 128(%r14)
	movl	44(%r14), %eax
	movl	$8, %r10d
	jmp	.LBB0_9
.LBB0_3:
	movl	40(%r14), %edi
	movl	$8, %esi
	callq	jdiv_round_up
	movl	%eax, 128(%r14)
	movl	44(%r14), %edi
	movl	$8, %esi
	callq	jdiv_round_up
	movl	$1, %r10d
	jmp	.LBB0_9
.LBB0_5:
	movl	40(%r14), %edi
	movl	$4, %esi
	callq	jdiv_round_up
	movl	%eax, 128(%r14)
	movl	44(%r14), %edi
	movl	$4, %esi
	callq	jdiv_round_up
	movl	$2, %r10d
	jmp	.LBB0_9
.LBB0_7:
	movl	$2, %esi
	callq	jdiv_round_up
	movl	%eax, 128(%r14)
	movl	44(%r14), %edi
	movl	$2, %esi
	callq	jdiv_round_up
	movl	$4, %r10d
.LBB0_9:
	movl	%eax, 132(%r14)
	movl	%r10d, 396(%r14)
	movl	48(%r14), %r8d
	testl	%r8d, %r8d
	jle	.LBB0_25
# BB#10:                                # %.lr.ph105
	movq	296(%r14), %r15
	cmpl	$8, %r10d
	jae	.LBB0_11
# BB#16:                                # %.lr.ph105.split.us.preheader
	movl	388(%r14), %edx
	imull	%r10d, %edx
	xorl	%r9d, %r9d
	movq	%r15, %rdi
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph105.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_18 Depth 2
	movl	8(%rdi), %eax
	movl	%r10d, %ebp
	.p2align	4, 0x90
.LBB0_18:                               #   Parent Loop BB0_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rbp,%rbp), %esi
	movl	%esi, %ebx
	imull	%eax, %ebx
	cmpl	%edx, %ebx
	jg	.LBB0_21
# BB#19:                                #   in Loop: Header=BB0_18 Depth=2
	movl	12(%rdi), %ebx
	imull	%esi, %ebx
	movl	392(%r14), %ecx
	imull	%r10d, %ecx
	cmpl	%ecx, %ebx
	jg	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_18 Depth=2
	cmpl	$8, %esi
	movl	%esi, %ebp
	jl	.LBB0_18
.LBB0_21:                               # %.critedge.us
                                        #   in Loop: Header=BB0_17 Depth=1
	movl	%ebp, 36(%rdi)
	incl	%r9d
	addq	$96, %rdi
	cmpl	%r8d, %r9d
	jl	.LBB0_17
	jmp	.LBB0_22
.LBB0_11:                               # %.lr.ph105.split.preheader
	leal	-1(%r8), %edi
	movl	%r8d, %eax
	xorl	%edx, %edx
	movq	%r15, %rsi
	andl	$7, %eax
	je	.LBB0_13
	.p2align	4, 0x90
.LBB0_12:                               # %.lr.ph105.split.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	%r10d, 36(%rsi)
	incl	%edx
	addq	$96, %rsi
	cmpl	%edx, %eax
	jne	.LBB0_12
.LBB0_13:                               # %.lr.ph105.split.prol.loopexit
	cmpl	$7, %edi
	jb	.LBB0_22
# BB#14:                                # %.lr.ph105.split.preheader.new
	addq	$708, %rsi              # imm = 0x2C4
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph105.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%r10d, -672(%rsi)
	movl	%r10d, -576(%rsi)
	movl	%r10d, -480(%rsi)
	movl	%r10d, -384(%rsi)
	movl	%r10d, -288(%rsi)
	movl	%r10d, -192(%rsi)
	movl	%r10d, -96(%rsi)
	movl	%r10d, (%rsi)
	addl	$8, %edx
	addq	$768, %rsi              # imm = 0x300
	cmpl	%r8d, %edx
	jl	.LBB0_15
.LBB0_22:                               # %._crit_edge106
	testl	%r8d, %r8d
	jle	.LBB0_25
# BB#23:                                # %.lr.ph
	addq	$44, %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_24:                               # =>This Inner Loop Header: Depth=1
	movl	40(%r14), %eax
	movslq	-8(%r15), %rcx
	movslq	-36(%r15), %rdi
	imulq	%rcx, %rdi
	imulq	%rax, %rdi
	movslq	388(%r14), %rsi
	shlq	$3, %rsi
	callq	jdiv_round_up
	movl	%eax, -4(%r15)
	movl	44(%r14), %eax
	movslq	-8(%r15), %rcx
	movslq	-32(%r15), %rdi
	imulq	%rcx, %rdi
	imulq	%rax, %rdi
	movslq	392(%r14), %rsi
	shlq	$3, %rsi
	callq	jdiv_round_up
	movl	%eax, (%r15)
	incl	%ebp
	movl	48(%r14), %r8d
	addq	$96, %r15
	cmpl	%r8d, %ebp
	jl	.LBB0_24
.LBB0_25:                               # %._crit_edge
	movl	56(%r14), %edx
	leal	-1(%rdx), %ecx
	cmpl	$4, %ecx
	movl	%r8d, %eax
	ja	.LBB0_27
# BB#26:                                # %switch.lookup
	movslq	%ecx, %rax
	movl	.Lswitch.table(,%rax,4), %eax
.LBB0_27:
	movl	%eax, 136(%r14)
	cmpl	$0, 100(%r14)
	movl	$1, %ecx
	movl	$1, %esi
	cmovel	%eax, %esi
	movl	%esi, 140(%r14)
	cmpl	$0, 92(%r14)
	jne	.LBB0_43
# BB#28:
	cmpl	$0, 384(%r14)
	je	.LBB0_29
.LBB0_43:                               # %use_merged_upsample.exit.thread
	movl	%ecx, 144(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_29:
	cmpl	$3, 52(%r14)
	jne	.LBB0_43
# BB#30:
	cmpl	$3, %r8d
	jne	.LBB0_43
# BB#31:
	cmpl	$2, %edx
	jne	.LBB0_43
# BB#32:
	cmpl	$3, %eax
	jne	.LBB0_43
# BB#33:
	movq	296(%r14), %rax
	cmpl	$2, 8(%rax)
	jne	.LBB0_43
# BB#34:
	cmpl	$1, 104(%rax)
	jne	.LBB0_43
# BB#35:
	cmpl	$1, 200(%rax)
	jne	.LBB0_43
# BB#36:
	cmpl	$2, 12(%rax)
	jg	.LBB0_43
# BB#37:
	cmpl	$1, 108(%rax)
	jne	.LBB0_43
# BB#38:
	cmpl	$1, 204(%rax)
	jne	.LBB0_43
# BB#39:
	movl	36(%rax), %edx
	cmpl	396(%r14), %edx
	jne	.LBB0_43
# BB#40:
	cmpl	%edx, 132(%rax)
	jne	.LBB0_43
# BB#41:                                # %use_merged_upsample.exit
	cmpl	%edx, 228(%rax)
	jne	.LBB0_43
# BB#42:
	movl	392(%r14), %ecx
	jmp	.LBB0_43
.Lfunc_end0:
	.size	jpeg_calc_output_dimensions, .Lfunc_end0-jpeg_calc_output_dimensions
	.cfi_endproc

	.globl	jpeg_new_colormap
	.p2align	4, 0x90
	.type	jpeg_new_colormap,@function
jpeg_new_colormap:                      # @jpeg_new_colormap
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	528(%rbx), %r14
	movl	28(%rbx), %eax
	cmpl	$207, %eax
	je	.LBB1_2
# BB#1:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB1_2:
	cmpl	$0, 100(%rbx)
	je	.LBB1_6
# BB#3:
	cmpl	$0, 120(%rbx)
	je	.LBB1_6
# BB#4:
	cmpq	$0, 152(%rbx)
	je	.LBB1_6
# BB#5:
	movq	40(%r14), %rax
	movq	%rax, 608(%rbx)
	movq	%rbx, %rdi
	callq	*24(%rax)
	movl	$0, 16(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_6:
	movq	(%rbx), %rax
	movl	$45, 40(%rax)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end1:
	.size	jpeg_new_colormap, .Lfunc_end1-jpeg_new_colormap
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	10                      # 0xa
	.byte	11                      # 0xb
	.byte	12                      # 0xc
	.byte	13                      # 0xd
	.byte	14                      # 0xe
	.byte	15                      # 0xf
.LCPI2_1:
	.byte	16                      # 0x10
	.byte	17                      # 0x11
	.byte	18                      # 0x12
	.byte	19                      # 0x13
	.byte	20                      # 0x14
	.byte	21                      # 0x15
	.byte	22                      # 0x16
	.byte	23                      # 0x17
	.byte	24                      # 0x18
	.byte	25                      # 0x19
	.byte	26                      # 0x1a
	.byte	27                      # 0x1b
	.byte	28                      # 0x1c
	.byte	29                      # 0x1d
	.byte	30                      # 0x1e
	.byte	31                      # 0x1f
.LCPI2_2:
	.byte	32                      # 0x20
	.byte	33                      # 0x21
	.byte	34                      # 0x22
	.byte	35                      # 0x23
	.byte	36                      # 0x24
	.byte	37                      # 0x25
	.byte	38                      # 0x26
	.byte	39                      # 0x27
	.byte	40                      # 0x28
	.byte	41                      # 0x29
	.byte	42                      # 0x2a
	.byte	43                      # 0x2b
	.byte	44                      # 0x2c
	.byte	45                      # 0x2d
	.byte	46                      # 0x2e
	.byte	47                      # 0x2f
.LCPI2_3:
	.byte	48                      # 0x30
	.byte	49                      # 0x31
	.byte	50                      # 0x32
	.byte	51                      # 0x33
	.byte	52                      # 0x34
	.byte	53                      # 0x35
	.byte	54                      # 0x36
	.byte	55                      # 0x37
	.byte	56                      # 0x38
	.byte	57                      # 0x39
	.byte	58                      # 0x3a
	.byte	59                      # 0x3b
	.byte	60                      # 0x3c
	.byte	61                      # 0x3d
	.byte	62                      # 0x3e
	.byte	63                      # 0x3f
.LCPI2_4:
	.byte	64                      # 0x40
	.byte	65                      # 0x41
	.byte	66                      # 0x42
	.byte	67                      # 0x43
	.byte	68                      # 0x44
	.byte	69                      # 0x45
	.byte	70                      # 0x46
	.byte	71                      # 0x47
	.byte	72                      # 0x48
	.byte	73                      # 0x49
	.byte	74                      # 0x4a
	.byte	75                      # 0x4b
	.byte	76                      # 0x4c
	.byte	77                      # 0x4d
	.byte	78                      # 0x4e
	.byte	79                      # 0x4f
.LCPI2_5:
	.byte	80                      # 0x50
	.byte	81                      # 0x51
	.byte	82                      # 0x52
	.byte	83                      # 0x53
	.byte	84                      # 0x54
	.byte	85                      # 0x55
	.byte	86                      # 0x56
	.byte	87                      # 0x57
	.byte	88                      # 0x58
	.byte	89                      # 0x59
	.byte	90                      # 0x5a
	.byte	91                      # 0x5b
	.byte	92                      # 0x5c
	.byte	93                      # 0x5d
	.byte	94                      # 0x5e
	.byte	95                      # 0x5f
.LCPI2_6:
	.byte	96                      # 0x60
	.byte	97                      # 0x61
	.byte	98                      # 0x62
	.byte	99                      # 0x63
	.byte	100                     # 0x64
	.byte	101                     # 0x65
	.byte	102                     # 0x66
	.byte	103                     # 0x67
	.byte	104                     # 0x68
	.byte	105                     # 0x69
	.byte	106                     # 0x6a
	.byte	107                     # 0x6b
	.byte	108                     # 0x6c
	.byte	109                     # 0x6d
	.byte	110                     # 0x6e
	.byte	111                     # 0x6f
.LCPI2_7:
	.byte	112                     # 0x70
	.byte	113                     # 0x71
	.byte	114                     # 0x72
	.byte	115                     # 0x73
	.byte	116                     # 0x74
	.byte	117                     # 0x75
	.byte	118                     # 0x76
	.byte	119                     # 0x77
	.byte	120                     # 0x78
	.byte	121                     # 0x79
	.byte	122                     # 0x7a
	.byte	123                     # 0x7b
	.byte	124                     # 0x7c
	.byte	125                     # 0x7d
	.byte	126                     # 0x7e
	.byte	127                     # 0x7f
.LCPI2_8:
	.byte	128                     # 0x80
	.byte	129                     # 0x81
	.byte	130                     # 0x82
	.byte	131                     # 0x83
	.byte	132                     # 0x84
	.byte	133                     # 0x85
	.byte	134                     # 0x86
	.byte	135                     # 0x87
	.byte	136                     # 0x88
	.byte	137                     # 0x89
	.byte	138                     # 0x8a
	.byte	139                     # 0x8b
	.byte	140                     # 0x8c
	.byte	141                     # 0x8d
	.byte	142                     # 0x8e
	.byte	143                     # 0x8f
.LCPI2_9:
	.byte	144                     # 0x90
	.byte	145                     # 0x91
	.byte	146                     # 0x92
	.byte	147                     # 0x93
	.byte	148                     # 0x94
	.byte	149                     # 0x95
	.byte	150                     # 0x96
	.byte	151                     # 0x97
	.byte	152                     # 0x98
	.byte	153                     # 0x99
	.byte	154                     # 0x9a
	.byte	155                     # 0x9b
	.byte	156                     # 0x9c
	.byte	157                     # 0x9d
	.byte	158                     # 0x9e
	.byte	159                     # 0x9f
.LCPI2_10:
	.byte	160                     # 0xa0
	.byte	161                     # 0xa1
	.byte	162                     # 0xa2
	.byte	163                     # 0xa3
	.byte	164                     # 0xa4
	.byte	165                     # 0xa5
	.byte	166                     # 0xa6
	.byte	167                     # 0xa7
	.byte	168                     # 0xa8
	.byte	169                     # 0xa9
	.byte	170                     # 0xaa
	.byte	171                     # 0xab
	.byte	172                     # 0xac
	.byte	173                     # 0xad
	.byte	174                     # 0xae
	.byte	175                     # 0xaf
.LCPI2_11:
	.byte	176                     # 0xb0
	.byte	177                     # 0xb1
	.byte	178                     # 0xb2
	.byte	179                     # 0xb3
	.byte	180                     # 0xb4
	.byte	181                     # 0xb5
	.byte	182                     # 0xb6
	.byte	183                     # 0xb7
	.byte	184                     # 0xb8
	.byte	185                     # 0xb9
	.byte	186                     # 0xba
	.byte	187                     # 0xbb
	.byte	188                     # 0xbc
	.byte	189                     # 0xbd
	.byte	190                     # 0xbe
	.byte	191                     # 0xbf
.LCPI2_12:
	.byte	192                     # 0xc0
	.byte	193                     # 0xc1
	.byte	194                     # 0xc2
	.byte	195                     # 0xc3
	.byte	196                     # 0xc4
	.byte	197                     # 0xc5
	.byte	198                     # 0xc6
	.byte	199                     # 0xc7
	.byte	200                     # 0xc8
	.byte	201                     # 0xc9
	.byte	202                     # 0xca
	.byte	203                     # 0xcb
	.byte	204                     # 0xcc
	.byte	205                     # 0xcd
	.byte	206                     # 0xce
	.byte	207                     # 0xcf
.LCPI2_13:
	.byte	208                     # 0xd0
	.byte	209                     # 0xd1
	.byte	210                     # 0xd2
	.byte	211                     # 0xd3
	.byte	212                     # 0xd4
	.byte	213                     # 0xd5
	.byte	214                     # 0xd6
	.byte	215                     # 0xd7
	.byte	216                     # 0xd8
	.byte	217                     # 0xd9
	.byte	218                     # 0xda
	.byte	219                     # 0xdb
	.byte	220                     # 0xdc
	.byte	221                     # 0xdd
	.byte	222                     # 0xde
	.byte	223                     # 0xdf
.LCPI2_14:
	.byte	224                     # 0xe0
	.byte	225                     # 0xe1
	.byte	226                     # 0xe2
	.byte	227                     # 0xe3
	.byte	228                     # 0xe4
	.byte	229                     # 0xe5
	.byte	230                     # 0xe6
	.byte	231                     # 0xe7
	.byte	232                     # 0xe8
	.byte	233                     # 0xe9
	.byte	234                     # 0xea
	.byte	235                     # 0xeb
	.byte	236                     # 0xec
	.byte	237                     # 0xed
	.byte	238                     # 0xee
	.byte	239                     # 0xef
.LCPI2_15:
	.byte	240                     # 0xf0
	.byte	241                     # 0xf1
	.byte	242                     # 0xf2
	.byte	243                     # 0xf3
	.byte	244                     # 0xf4
	.byte	245                     # 0xf5
	.byte	246                     # 0xf6
	.byte	247                     # 0xf7
	.byte	248                     # 0xf8
	.byte	249                     # 0xf9
	.byte	250                     # 0xfa
	.byte	251                     # 0xfb
	.byte	252                     # 0xfc
	.byte	253                     # 0xfd
	.byte	254                     # 0xfe
	.byte	255                     # 0xff
	.text
	.globl	jinit_master_decompress
	.p2align	4, 0x90
	.type	jinit_master_decompress,@function
jinit_master_decompress:                # @jinit_master_decompress
	.cfi_startproc
# BB#0:                                 # %vector.body
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$48, %edx
	callq	*(%rax)
	movq	%rax, %r14
	movq	%r14, 528(%r15)
	movl	$finish_output_pass, %eax
	movd	%rax, %xmm0
	movl	$prepare_for_output_pass, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r14)
	movl	$0, 16(%r14)
	movq	%r15, %rdi
	callq	jpeg_calc_output_dimensions
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$1408, %edx             # imm = 0x580
	movq	%r15, %rdi
	callq	*(%rax)
	movq	%rax, %rbx
	leaq	256(%rbx), %rax
	movq	%rax, 408(%r15)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 240(%rbx)
	movdqu	%xmm0, 224(%rbx)
	movdqu	%xmm0, 208(%rbx)
	movdqu	%xmm0, 192(%rbx)
	movdqu	%xmm0, 176(%rbx)
	movdqu	%xmm0, 160(%rbx)
	movdqu	%xmm0, 144(%rbx)
	movdqu	%xmm0, 128(%rbx)
	movdqu	%xmm0, 112(%rbx)
	movdqu	%xmm0, 96(%rbx)
	movdqu	%xmm0, 80(%rbx)
	movdqu	%xmm0, 64(%rbx)
	movdqu	%xmm0, 48(%rbx)
	movdqu	%xmm0, 32(%rbx)
	movdqu	%xmm0, 16(%rbx)
	movdqu	%xmm0, (%rbx)
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
	movups	%xmm0, 256(%rbx)
	movaps	.LCPI2_1(%rip), %xmm0   # xmm0 = [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
	movups	%xmm0, 272(%rbx)
	movaps	.LCPI2_2(%rip), %xmm0   # xmm0 = [32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47]
	movups	%xmm0, 288(%rbx)
	movaps	.LCPI2_3(%rip), %xmm0   # xmm0 = [48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63]
	movups	%xmm0, 304(%rbx)
	movaps	.LCPI2_4(%rip), %xmm0   # xmm0 = [64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79]
	movups	%xmm0, 320(%rbx)
	movaps	.LCPI2_5(%rip), %xmm0   # xmm0 = [80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95]
	movups	%xmm0, 336(%rbx)
	movaps	.LCPI2_6(%rip), %xmm0   # xmm0 = [96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111]
	movups	%xmm0, 352(%rbx)
	movaps	.LCPI2_7(%rip), %xmm0   # xmm0 = [112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127]
	movups	%xmm0, 368(%rbx)
	movaps	.LCPI2_8(%rip), %xmm0   # xmm0 = [128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143]
	movups	%xmm0, 384(%rbx)
	movaps	.LCPI2_9(%rip), %xmm0   # xmm0 = [144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159]
	movups	%xmm0, 400(%rbx)
	movaps	.LCPI2_10(%rip), %xmm0  # xmm0 = [160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175]
	movups	%xmm0, 416(%rbx)
	movaps	.LCPI2_11(%rip), %xmm0  # xmm0 = [176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191]
	movups	%xmm0, 432(%rbx)
	movaps	.LCPI2_12(%rip), %xmm0  # xmm0 = [192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207]
	movups	%xmm0, 448(%rbx)
	movaps	.LCPI2_13(%rip), %xmm0  # xmm0 = [208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223]
	movups	%xmm0, 464(%rbx)
	movaps	.LCPI2_14(%rip), %xmm0  # xmm0 = [224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239]
	movups	%xmm0, 480(%rbx)
	movaps	.LCPI2_15(%rip), %xmm0  # xmm0 = [240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255]
	movups	%xmm0, 496(%rbx)
	leaq	512(%rbx), %rdi
	movl	$255, %esi
	movl	$384, %edx              # imm = 0x180
	callq	memset
	leaq	896(%rbx), %rdi
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$384, %edx              # imm = 0x180
	callq	memset
	movq	408(%r15), %rax
	movups	112(%rax), %xmm0
	movups	%xmm0, 1392(%rbx)
	movups	96(%rax), %xmm0
	movups	%xmm0, 1376(%rbx)
	movups	80(%rax), %xmm0
	movups	%xmm0, 1360(%rbx)
	movups	64(%rax), %xmm0
	movups	%xmm0, 1344(%rbx)
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	48(%rax), %xmm3
	movups	%xmm3, 1328(%rbx)
	movups	%xmm2, 1312(%rbx)
	movups	%xmm1, 1296(%rbx)
	movups	%xmm0, 1280(%rbx)
	movl	128(%r15), %eax
	movslq	136(%r15), %rcx
	imulq	%rax, %rcx
	movl	%ecx, %eax
	cmpq	%rcx, %rax
	je	.LBB2_2
# BB#1:
	movq	(%r15), %rax
	movl	$69, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB2_2:
	movl	$0, 24(%r14)
	cmpl	$0, 92(%r15)
	jne	.LBB2_17
# BB#3:
	cmpl	$0, 384(%r15)
	jne	.LBB2_17
# BB#4:
	cmpl	$3, 52(%r15)
	jne	.LBB2_17
# BB#5:
	cmpl	$3, 48(%r15)
	jne	.LBB2_17
# BB#6:
	cmpl	$2, 56(%r15)
	jne	.LBB2_17
# BB#7:
	cmpl	$3, 136(%r15)
	jne	.LBB2_17
# BB#8:
	movq	296(%r15), %rax
	cmpl	$2, 8(%rax)
	jne	.LBB2_17
# BB#9:
	cmpl	$1, 104(%rax)
	jne	.LBB2_17
# BB#10:
	cmpl	$1, 200(%rax)
	jne	.LBB2_17
# BB#11:
	cmpl	$2, 12(%rax)
	jg	.LBB2_17
# BB#12:
	cmpl	$1, 108(%rax)
	jne	.LBB2_17
# BB#13:
	cmpl	$1, 204(%rax)
	jne	.LBB2_17
# BB#14:
	movl	36(%rax), %ecx
	cmpl	396(%r15), %ecx
	jne	.LBB2_17
# BB#15:
	cmpl	%ecx, 132(%rax)
	jne	.LBB2_17
# BB#16:
	xorl	%ebp, %ebp
	cmpl	%ecx, 228(%rax)
	sete	%bpl
.LBB2_17:                               # %use_merged_upsample.exit.i
	movl	%ebp, 28(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r14)
	cmpl	$0, 100(%r15)
	je	.LBB2_51
# BB#18:
	cmpl	$0, 80(%r15)
	jne	.LBB2_20
# BB#19:
	movq	$0, 116(%r15)
	movl	$0, 124(%r15)
.LBB2_20:                               # %.thread.i
	cmpl	$0, 84(%r15)
	je	.LBB2_22
# BB#21:
	movq	(%r15), %rax
	movl	$46, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB2_22:
	cmpl	$3, 136(%r15)
	jne	.LBB2_23
# BB#24:
	cmpq	$0, 152(%r15)
	je	.LBB2_26
# BB#25:
	movl	$1, 120(%r15)
	leaq	116(%r15), %rax
	cmpl	$0, (%rax)
	jne	.LBB2_28
	jmp	.LBB2_29
.LBB2_51:                               # %._crit_edge.i
	movq	$0, 116(%r15)
	movl	$0, 124(%r15)
	leaq	84(%r15), %rbx
	cmpl	$0, (%rbx)
	jne	.LBB2_37
	jmp	.LBB2_33
.LBB2_23:
	leaq	116(%r15), %rax
	movl	$1, 116(%r15)
	movl	$0, 120(%r15)
	movl	$0, 124(%r15)
	movq	$0, 152(%r15)
	cmpl	$0, (%rax)
	jne	.LBB2_28
	jmp	.LBB2_29
.LBB2_26:
	leaq	116(%r15), %rax
	leaq	124(%r15), %rcx
	cmpl	$0, 108(%r15)
	cmoveq	%rax, %rcx
	movl	$1, (%rcx)
	cmpl	$0, (%rax)
	je	.LBB2_29
.LBB2_28:
	movq	%r14, %rbx
	addq	$32, %rbx
	movq	%r15, %rdi
	callq	jinit_1pass_quantizer
	movq	608(%r15), %rax
	movq	%rax, (%rbx)
.LBB2_29:
	leaq	84(%r15), %rbx
	cmpl	$0, 124(%r15)
	jne	.LBB2_31
# BB#30:
	cmpl	$0, 120(%r15)
	je	.LBB2_32
.LBB2_31:
	movq	%r15, %rdi
	callq	jinit_2pass_quantizer
	movq	608(%r15), %rax
	movq	%rax, 40(%r14)
.LBB2_32:
	cmpl	$0, (%rbx)
	jne	.LBB2_37
.LBB2_33:
	cmpl	$0, 28(%r14)
	je	.LBB2_35
# BB#34:
	movq	%r15, %rdi
	callq	jinit_merged_upsampler
	jmp	.LBB2_36
.LBB2_35:
	movq	%r15, %rdi
	callq	jinit_color_deconverter
	movq	%r15, %rdi
	callq	jinit_upsampler
.LBB2_36:
	movl	124(%r15), %esi
	movq	%r15, %rdi
	callq	jinit_d_post_controller
.LBB2_37:
	movq	%r15, %rdi
	callq	jinit_inverse_dct
	cmpl	$0, 308(%r15)
	je	.LBB2_39
# BB#38:
	movq	(%r15), %rax
	movl	$1, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	jmp	.LBB2_42
.LBB2_39:
	cmpl	$0, 304(%r15)
	je	.LBB2_41
# BB#40:
	movq	%r15, %rdi
	callq	jinit_phuff_decoder
	jmp	.LBB2_42
.LBB2_41:
	movq	%r15, %rdi
	callq	jinit_huff_decoder
.LBB2_42:
	movq	560(%r15), %rcx
	movb	$1, %al
	cmpl	$0, 32(%rcx)
	jne	.LBB2_44
# BB#43:
	cmpl	$0, 80(%r15)
	setne	%al
.LBB2_44:
	movzbl	%al, %esi
	movq	%r15, %rdi
	callq	jinit_d_coef_controller
	cmpl	$0, (%rbx)
	jne	.LBB2_46
# BB#45:
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	jinit_d_main_controller
.LBB2_46:
	movq	8(%r15), %rax
	movq	%r15, %rdi
	callq	*48(%rax)
	movq	560(%r15), %rax
	movq	%r15, %rdi
	callq	*16(%rax)
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.LBB2_50
# BB#47:
	cmpl	$0, 80(%r15)
	jne	.LBB2_50
# BB#48:
	movq	560(%r15), %rcx
	cmpl	$0, 32(%rcx)
	je	.LBB2_50
# BB#49:
	cmpl	$0, 304(%r15)
	movl	48(%r15), %ecx
	leal	2(%rcx,%rcx,2), %edx
	cmovel	%ecx, %edx
	movq	$0, 8(%rax)
	movl	400(%r15), %ecx
	movslq	%edx, %rdx
	imulq	%rcx, %rdx
	movq	%rdx, 16(%rax)
	movl	$0, 24(%rax)
	xorl	%ecx, %ecx
	cmpl	$0, 124(%r15)
	setne	%cl
	orl	$2, %ecx
	movl	%ecx, 28(%rax)
	incl	24(%r14)
.LBB2_50:                               # %master_selection.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	jinit_master_decompress, .Lfunc_end2-jinit_master_decompress
	.cfi_endproc

	.p2align	4, 0x90
	.type	prepare_for_output_pass,@function
prepare_for_output_pass:                # @prepare_for_output_pass
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	528(%rbx), %r14
	cmpl	$0, 16(%r14)
	je	.LBB3_2
# BB#1:
	movl	$0, 16(%r14)
	movq	608(%rbx), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	552(%rbx), %rax
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	536(%rbx), %rax
	movl	$2, %esi
.LBB3_16:
	movq	%rbx, %rdi
	callq	*(%rax)
	jmp	.LBB3_17
.LBB3_2:
	cmpl	$0, 100(%rbx)
	je	.LBB3_10
# BB#3:
	cmpq	$0, 152(%rbx)
	je	.LBB3_4
.LBB3_10:
	movq	584(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	544(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
	cmpl	$0, 84(%rbx)
	je	.LBB3_11
.LBB3_17:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB3_21
# BB#18:
	movl	24(%r14), %ecx
	movl	%ecx, 24(%rax)
	cmpl	$1, 16(%r14)
	sbbl	$-1, %ecx
	incl	%ecx
	movl	%ecx, 28(%rax)
	cmpl	$0, 80(%rbx)
	je	.LBB3_21
# BB#19:
	movq	560(%rbx), %rdx
	cmpl	$0, 36(%rdx)
	jne	.LBB3_21
# BB#20:
	xorl	%edx, %edx
	cmpl	$0, 124(%rbx)
	setne	%dl
	leal	1(%rcx,%rdx), %ecx
	movl	%ecx, 28(%rax)
.LBB3_21:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB3_11:
	cmpl	$0, 28(%r14)
	jne	.LBB3_13
# BB#12:
	movq	600(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB3_13:
	movq	592(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$0, 100(%rbx)
	je	.LBB3_15
# BB#14:
	movq	608(%rbx), %rax
	movl	16(%r14), %esi
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB3_15:
	movq	552(%rbx), %rax
	xorl	%ecx, %ecx
	cmpl	$0, 16(%r14)
	setne	%cl
	leal	(%rcx,%rcx,2), %esi
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	536(%rbx), %rax
	xorl	%esi, %esi
	jmp	.LBB3_16
.LBB3_4:
	cmpl	$0, 108(%rbx)
	je	.LBB3_7
# BB#5:
	cmpl	$0, 124(%rbx)
	je	.LBB3_7
# BB#6:
	movq	40(%r14), %rax
	movq	%rax, 608(%rbx)
	movl	$1, 16(%r14)
	jmp	.LBB3_10
.LBB3_7:
	cmpl	$0, 116(%rbx)
	je	.LBB3_9
# BB#8:
	movq	32(%r14), %rax
	movq	%rax, 608(%rbx)
	jmp	.LBB3_10
.LBB3_9:
	movq	(%rbx), %rax
	movl	$45, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
	jmp	.LBB3_10
.Lfunc_end3:
	.size	prepare_for_output_pass, .Lfunc_end3-prepare_for_output_pass
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_output_pass,@function
finish_output_pass:                     # @finish_output_pass
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	movq	528(%rdi), %rbx
	cmpl	$0, 100(%rdi)
	je	.LBB4_2
# BB#1:
	movq	608(%rdi), %rax
	callq	*16(%rax)
.LBB4_2:
	incl	24(%rbx)
	popq	%rbx
	retq
.Lfunc_end4:
	.size	finish_output_pass, .Lfunc_end4-finish_output_pass
	.cfi_endproc

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.size	.Lswitch.table, 20


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
