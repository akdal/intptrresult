	.text
	.file	"code.bc"
	.globl	Gsm_Coder
	.p2align	4, 0x90
	.type	Gsm_Coder,@function
Gsm_Coder:                              # @Gsm_Coder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$408, %rsp              # imm = 0x198
.Lcfi6:
	.cfi_def_cfa_offset 464
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	%r8, %r13
	movq	%rcx, %rbp
	movq	%rdx, %r14
	movq	%rdi, %rbx
	leaq	240(%rbx), %r15
	leaq	80(%rsp), %r12
	movq	%r12, %rdx
	callq	Gsm_Preprocess
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	Gsm_LPC_Analysis
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	Gsm_Short_Term_Analysis_Filter
	leaq	2(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%r13, (%rsp)
	movl	$Gsm_Coder.e+10, %ecx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	472(%rsp), %r12
	movq	%r15, %rdx
	movq	%r15, %r8
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	movq	%rbp, %r9
	callq	Gsm_Long_Term_Predictor
	movq	464(%rsp), %rdx
	leaq	2(%rdx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$Gsm_Coder.e+10, %esi
	movq	%rbx, %rdi
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%r15, %rcx
	movq	%r12, %r8
	callq	Gsm_RPE_Encoding
	movl	$242, %eax
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movswq	Gsm_Coder.e-232(%rax), %rcx
	movswq	-2(%rbx,%rax), %rdx
	leaq	(%rdx,%rcx), %rsi
	leaq	32768(%rdx,%rcx), %rcx
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%si, %dx
	movw	%dx, -2(%rbx,%rax)
	movswq	Gsm_Coder.e-230(%rax), %rcx
	movswq	(%rbx,%rax), %rdx
	leaq	(%rdx,%rcx), %rsi
	leaq	32768(%rdx,%rcx), %rcx
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%si, %dx
	movw	%dx, (%rbx,%rax)
	addq	$4, %rax
	cmpq	$322, %rax              # imm = 0x142
	jne	.LBB0_1
# BB#2:
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	2(%rax), %rax
	leaq	2(%r15), %r14
	leaq	320(%rbx), %rdx
	leaq	26(%r12), %rbp
	leaq	160(%rsp), %rsi
	movq	72(%rsp), %r13          # 8-byte Reload
	leaq	4(%r13), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rax, (%rsp)
	movl	$Gsm_Coder.e+10, %ecx
	movq	%rbx, %rdi
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rdx, %r8
	movq	16(%rsp), %r9           # 8-byte Reload
	callq	Gsm_Long_Term_Predictor
	movq	464(%rsp), %rax
	leaq	4(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$Gsm_Coder.e+10, %esi
	movq	%rbx, %rdi
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	movq	%rbp, %r8
	callq	Gsm_RPE_Encoding
	movl	$322, %eax              # imm = 0x142
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movswq	Gsm_Coder.e-312(%rax), %rcx
	movswq	-2(%rbx,%rax), %rdx
	leaq	(%rdx,%rcx), %rsi
	leaq	32768(%rdx,%rcx), %rcx
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%si, %dx
	movw	%dx, -2(%rbx,%rax)
	movswq	Gsm_Coder.e-310(%rax), %rcx
	movswq	(%rbx,%rax), %rdx
	leaq	(%rdx,%rcx), %rsi
	leaq	32768(%rdx,%rcx), %rcx
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%si, %dx
	movw	%dx, (%rbx,%rax)
	addq	$4, %rax
	cmpq	$402, %rax              # imm = 0x192
	jne	.LBB0_3
# BB#4:
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	4(%rax), %rax
	leaq	4(%r15), %r14
	leaq	400(%rbx), %rdx
	leaq	52(%r12), %rbp
	leaq	240(%rsp), %rsi
	addq	$6, %r13
	movq	%rax, (%rsp)
	movl	$Gsm_Coder.e+10, %ecx
	movq	%rbx, %rdi
	movq	%rdx, %r8
	movq	56(%rsp), %r9           # 8-byte Reload
	callq	Gsm_Long_Term_Predictor
	addq	$6, 32(%rsp)            # 8-byte Folded Spill
	movl	$Gsm_Coder.e+10, %esi
	movq	%rbx, %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	movq	%rbp, %r8
	callq	Gsm_RPE_Encoding
	movl	$402, %eax              # imm = 0x192
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movswq	Gsm_Coder.e-392(%rax), %rcx
	movswq	-2(%rbx,%rax), %rdx
	leaq	(%rdx,%rcx), %rsi
	leaq	32768(%rdx,%rcx), %rcx
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%si, %dx
	movw	%dx, -2(%rbx,%rax)
	movswq	Gsm_Coder.e-390(%rax), %rcx
	movswq	(%rbx,%rax), %rdx
	leaq	(%rdx,%rcx), %rsi
	leaq	32768(%rdx,%rcx), %rcx
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%si, %dx
	movw	%dx, (%rbx,%rax)
	addq	$4, %rax
	cmpq	$482, %rax              # imm = 0x1E2
	jne	.LBB0_5
# BB#6:
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	$6, %rax
	addq	$6, %r15
	leaq	480(%rbx), %rdx
	addq	$78, %r12
	leaq	320(%rsp), %rsi
	movq	%rax, (%rsp)
	movl	$Gsm_Coder.e+10, %ecx
	movq	%rbx, %rdi
	movq	%rdx, %r8
	movq	%r13, %r9
	callq	Gsm_Long_Term_Predictor
	movl	$Gsm_Coder.e+10, %esi
	movq	%rbx, %rdi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movq	%r12, %r8
	callq	Gsm_RPE_Encoding
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	movswq	Gsm_Coder.e+10(%rax), %rcx
	movswq	480(%rbx,%rax), %rdx
	leaq	(%rdx,%rcx), %rsi
	leaq	32768(%rdx,%rcx), %rcx
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%si, %dx
	movw	%dx, 480(%rbx,%rax)
	movswq	Gsm_Coder.e+12(%rax), %rcx
	movswq	482(%rbx,%rax), %rdx
	leaq	(%rdx,%rcx), %rsi
	leaq	32768(%rdx,%rcx), %rcx
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%si, %dx
	movw	%dx, 482(%rbx,%rax)
	addq	$4, %rax
	cmpq	$80, %rax
	jne	.LBB0_7
# BB#8:
	movl	$240, %edx
	movq	%rbx, %rdi
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	memcpy
	addq	$408, %rsp              # imm = 0x198
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Gsm_Coder, .Lfunc_end0-Gsm_Coder
	.cfi_endproc

	.type	Gsm_Coder.e,@object     # @Gsm_Coder.e
	.local	Gsm_Coder.e
	.comm	Gsm_Coder.e,100,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
