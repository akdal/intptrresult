	.text
	.file	"libclamav_readdb.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	42                      # 0x2a
	.long	42                      # 0x2a
	.long	42                      # 0x2a
	.long	42                      # 0x2a
	.text
	.globl	cli_parse_add
	.p2align	4, 0x90
	.type	cli_parse_add,@function
cli_parse_add:                          # @cli_parse_add
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movq	%r8, %r13
	movl	%ecx, %ebp
	movq	%rdx, %r12
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	movl	$123, %esi
	movq	%r12, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB0_52
# BB#1:
	incl	64(%r15)
	movq	%r12, %rdi
	callq	cli_strdup
	testq	%rax, %rax
	je	.LBB0_61
# BB#2:
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r12, %rdi
	callq	strlen
	testl	%eax, %eax
	je	.LBB0_59
# BB#3:                                 # %.lr.ph231.preheader
	movl	%eax, %ecx
	leaq	-1(%rcx), %rsi
	xorl	%edx, %edx
	xorl	%r13d, %r13d
	andq	$3, %rax
	je	.LBB0_8
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph231.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r12,%rdx), %ebx
	cmpb	$123, %bl
	je	.LBB0_6
# BB#5:                                 # %.lr.ph231.prol
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpb	$42, %bl
	jne	.LBB0_7
.LBB0_6:                                #   in Loop: Header=BB0_4 Depth=1
	incl	%r13d
.LBB0_7:                                #   in Loop: Header=BB0_4 Depth=1
	incq	%rdx
	cmpq	%rdx, %rax
	jne	.LBB0_4
.LBB0_8:                                # %.lr.ph231.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB0_23
# BB#9:                                 # %.lr.ph231.preheader.new
	subq	%rdx, %rcx
	leaq	3(%r12,%rdx), %rax
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph231
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rax), %edx
	cmpb	$123, %dl
	je	.LBB0_12
# BB#11:                                # %.lr.ph231
                                        #   in Loop: Header=BB0_10 Depth=1
	cmpb	$42, %dl
	jne	.LBB0_13
.LBB0_12:                               #   in Loop: Header=BB0_10 Depth=1
	incl	%r13d
.LBB0_13:                               # %.lr.ph231.1286
                                        #   in Loop: Header=BB0_10 Depth=1
	movzbl	-2(%rax), %edx
	cmpb	$42, %dl
	je	.LBB0_15
# BB#14:                                # %.lr.ph231.1286
                                        #   in Loop: Header=BB0_10 Depth=1
	cmpb	$123, %dl
	jne	.LBB0_16
.LBB0_15:                               #   in Loop: Header=BB0_10 Depth=1
	incl	%r13d
.LBB0_16:                               # %.lr.ph231.2287
                                        #   in Loop: Header=BB0_10 Depth=1
	movzbl	-1(%rax), %edx
	cmpb	$123, %dl
	je	.LBB0_18
# BB#17:                                # %.lr.ph231.2287
                                        #   in Loop: Header=BB0_10 Depth=1
	cmpb	$42, %dl
	jne	.LBB0_19
.LBB0_18:                               #   in Loop: Header=BB0_10 Depth=1
	incl	%r13d
.LBB0_19:                               # %.lr.ph231.3288
                                        #   in Loop: Header=BB0_10 Depth=1
	movzbl	(%rax), %edx
	cmpb	$123, %dl
	je	.LBB0_21
# BB#20:                                # %.lr.ph231.3288
                                        #   in Loop: Header=BB0_10 Depth=1
	cmpb	$42, %dl
	jne	.LBB0_22
.LBB0_21:                               #   in Loop: Header=BB0_10 Depth=1
	incl	%r13d
.LBB0_22:                               #   in Loop: Header=BB0_10 Depth=1
	addq	$4, %rax
	addq	$-4, %rcx
	jne	.LBB0_10
.LBB0_23:                               # %._crit_edge232
	testl	%r13d, %r13d
	je	.LBB0_59
# BB#24:                                # %._crit_edge232
	incl	%r13d
	je	.LBB0_59
# BB#25:                                # %.lr.ph227
	movl	%ebp, %eax
	movl	$1, %ebp
	xorl	%ecx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movzbl	%r14b, %ecx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movzwl	%ax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movzwl	%r13w, %eax
	movl	%eax, 68(%rsp)          # 4-byte Spill
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rbx
	xorl	%r12d, %r12d
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movq	%r15, 24(%rsp)          # 8-byte Spill
	jmp	.LBB0_38
	.p2align	4, 0x90
.LBB0_26:                               #   in Loop: Header=BB0_38 Depth=1
	cmpl	%r13d, %ebp
	je	.LBB0_59
# BB#27:                                #   in Loop: Header=BB0_38 Depth=1
	xorl	%r12d, %r12d
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movl	$0, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %r14
	je	.LBB0_29
.LBB0_28:                               #   in Loop: Header=BB0_38 Depth=1
	incl	%ebp
	cmpl	%r13d, %ebp
	movq	24(%rsp), %r15          # 8-byte Reload
	jbe	.LBB0_38
	jmp	.LBB0_59
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_38 Depth=1
	movl	$125, %esi
	movq	%rbx, %rdi
	callq	strchr
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_93
# BB#30:                                #   in Loop: Header=BB0_38 Depth=1
	movb	$0, (%r14)
	testq	%rbx, %rbx
	je	.LBB0_93
# BB#31:                                #   in Loop: Header=BB0_38 Depth=1
	incq	%r14
	movl	$45, %esi
	movq	%rbx, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB0_36
# BB#32:                                #   in Loop: Header=BB0_38 Depth=1
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movl	$.L.str.1, %edx
	movq	%rbx, %rdi
	callq	cli_strtok
	movq	%rbx, %r15
	movq	%rax, %rbx
	testq	%rbx, %rbx
	movl	$0, %r12d
	je	.LBB0_34
# BB#33:                                #   in Loop: Header=BB0_38 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %r12
	movq	%rbx, %rdi
	callq	free
	testl	%r12d, %r12d
	js	.LBB0_93
.LBB0_34:                               #   in Loop: Header=BB0_38 Depth=1
	movl	$1, %esi
	movl	$.L.str.1, %edx
	movq	%r15, 56(%rsp)          # 8-byte Spill
	movq	%r15, %rdi
	callq	cli_strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_37
# BB#35:                                #   in Loop: Header=BB0_38 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	free
	movq	%r15, 16(%rsp)          # 8-byte Spill
	testl	%r15d, %r15d
	movq	56(%rsp), %rbx          # 8-byte Reload
	jns	.LBB0_28
	jmp	.LBB0_93
.LBB0_36:                               #   in Loop: Header=BB0_38 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %r12
	testl	%r12d, %r12d
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jns	.LBB0_28
	jmp	.LBB0_93
.LBB0_37:                               #   in Loop: Header=BB0_38 Depth=1
	movq	56(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_28
	.p2align	4, 0x90
.LBB0_38:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_42 Depth 2
	cmpl	%r13d, %ebp
	je	.LBB0_50
# BB#39:                                # %.preheader
                                        #   in Loop: Header=BB0_38 Depth=1
	movb	(%r14), %al
	testb	%al, %al
	je	.LBB0_49
# BB#40:                                # %.lr.ph214.preheader
                                        #   in Loop: Header=BB0_38 Depth=1
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movl	$1, %r15d
	xorl	%ebx, %ebx
	jmp	.LBB0_42
	.p2align	4, 0x90
.LBB0_41:                               # %..lr.ph214_crit_edge
                                        #   in Loop: Header=BB0_42 Depth=2
	movzbl	(%r14,%rbx), %eax
	incl	%r15d
.LBB0_42:                               # %.lr.ph214
                                        #   Parent Loop BB0_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%r14, %rbx
	cmpb	$42, %al
	je	.LBB0_46
# BB#43:                                # %.lr.ph214
                                        #   in Loop: Header=BB0_42 Depth=2
	cmpb	$123, %al
	je	.LBB0_47
# BB#44:                                #   in Loop: Header=BB0_42 Depth=2
	movl	%r15d, %ebx
	movq	%r14, %rdi
	callq	strlen
	cmpq	%rax, %rbx
	jb	.LBB0_41
# BB#45:                                #   in Loop: Header=BB0_38 Depth=1
	movq	56(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_48
.LBB0_46:                               # %.loopexit202.loopexit257
                                        #   in Loop: Header=BB0_38 Depth=1
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_48
.LBB0_47:                               #   in Loop: Header=BB0_38 Depth=1
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
.LBB0_48:                               # %.loopexit202
                                        #   in Loop: Header=BB0_38 Depth=1
	movq	24(%rsp), %r15          # 8-byte Reload
.LBB0_49:                               # %.loopexit202
                                        #   in Loop: Header=BB0_38 Depth=1
	movb	$0, (%rbx)
	incq	%rbx
.LBB0_50:                               #   in Loop: Header=BB0_38 Depth=1
	movl	64(%r15), %ecx
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movzwl	%bp, %r9d
	movq	%r15, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	movl	76(%rsp), %r8d          # 4-byte Reload
	pushq	88(%rsp)                # 8-byte Folded Reload
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	112(%rsp)               # 8-byte Folded Reload
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	callq	cli_ac_addsig
	addq	$48, %rsp
.Lcfi19:
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	je	.LBB0_26
# BB#51:                                # %.critedge
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_99
.LBB0_52:
	movl	$42, %esi
	movq	%r12, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB0_62
# BB#53:
	incl	64(%r15)
	movq	%r12, %rdi
	callq	strlen
	testl	%eax, %eax
	je	.LBB0_60
# BB#54:                                # %.lr.ph212.preheader
	movl	%eax, %ecx
	cmpq	$8, %rcx
	movq	%r15, 24(%rsp)          # 8-byte Spill
	jb	.LBB0_65
# BB#55:                                # %min.iters.checked
	andl	$7, %eax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	je	.LBB0_65
# BB#56:                                # %vector.body.preheader
	leaq	4(%r12), %rsi
	pxor	%xmm1, %xmm1
	movdqa	.LCPI0_0(%rip), %xmm2   # xmm2 = [42,42,42,42]
	movq	%rdx, %rdi
	pxor	%xmm3, %xmm3
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_57:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3],xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	movd	(%rsi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1],xmm5[2],xmm1[2],xmm5[3],xmm1[3],xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	punpcklwd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1],xmm5[2],xmm1[2],xmm5[3],xmm1[3]
	pcmpeqd	%xmm2, %xmm4
	psrld	$31, %xmm4
	pcmpeqd	%xmm2, %xmm5
	psrld	$31, %xmm5
	paddd	%xmm4, %xmm3
	paddd	%xmm5, %xmm0
	addq	$8, %rsi
	addq	$-8, %rdi
	jne	.LBB0_57
# BB#58:                                # %middle.block
	paddd	%xmm3, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %r15d
	testq	%rax, %rax
	jne	.LBB0_66
	jmp	.LBB0_68
.LBB0_59:                               # %.critedge201
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	free
.LBB0_60:                               # %.loopexit
	xorl	%ebp, %ebp
	jmp	.LBB0_101
.LBB0_61:
	movl	$-114, %ebp
	jmp	.LBB0_101
.LBB0_62:
	cmpb	$0, 2(%r15)
	je	.LBB0_74
.LBB0_63:
	subq	$8, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	movzwl	%bp, %eax
	movzbl	%r14b, %ebx
	xorl	%ebp, %ebp
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%r15, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	pushq	%rbx
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	callq	cli_ac_addsig
	addq	$48, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset -48
	movl	%eax, %ebx
	testl	%ebx, %ebx
	je	.LBB0_101
# BB#64:
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	%ebx, %ebp
	jmp	.LBB0_101
.LBB0_65:
	xorl	%edx, %edx
	xorl	%r15d, %r15d
.LBB0_66:                               # %.lr.ph212.preheader273
	leaq	(%r12,%rdx), %rax
	subq	%rdx, %rcx
	.p2align	4, 0x90
.LBB0_67:                               # %.lr.ph212
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	cmpb	$42, (%rax)
	sete	%dl
	addl	%edx, %r15d
	incq	%rax
	decq	%rcx
	jne	.LBB0_67
.LBB0_68:                               # %._crit_edge
	testl	%r15d, %r15d
	je	.LBB0_60
# BB#69:                                # %._crit_edge
	incl	%r15d
	je	.LBB0_60
# BB#70:                                # %.lr.ph
	movl	$1, %ebx
	movzbl	%r14b, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movzwl	%bp, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movzwl	%r15w, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB0_71:                               # =>This Inner Loop Header: Depth=1
	leal	-1(%rbx), %esi
	movl	$.L.str.2, %edx
	movq	%r12, %rdi
	callq	cli_strtok
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_94
# BB#72:                                #   in Loop: Header=BB0_71 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	64(%rdi), %ecx
	subq	$8, %rsp
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	movzwl	%bx, %r9d
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	movl	20(%rsp), %r8d          # 4-byte Reload
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	movq	%r13, %r14
	pushq	%r13
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	callq	cli_ac_addsig
	addq	$48, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset -48
	movl	%eax, %r13d
	testl	%r13d, %r13d
	jne	.LBB0_95
# BB#73:                                #   in Loop: Header=BB0_71 Depth=1
	movq	%rbp, %rdi
	callq	free
	incl	%ebx
	cmpl	%r15d, %ebx
	movq	%r14, %r13
	jbe	.LBB0_71
	jmp	.LBB0_60
.LBB0_74:
	leaq	-1(%r12), %rax
	.p2align	4, 0x90
.LBB0_75:                               # =>This Inner Loop Header: Depth=1
	movzbl	1(%rax), %ecx
	incq	%rax
	testb	%cl, %cl
	setne	%dl
	cmpb	$63, %cl
	setne	%bl
	cmpb	$40, %cl
	je	.LBB0_77
# BB#76:                                #   in Loop: Header=BB0_75 Depth=1
	andb	%bl, %dl
	jne	.LBB0_75
.LBB0_77:                               # %.critedge.i
	testb	%cl, %cl
	jne	.LBB0_79
# BB#78:
	xorl	%eax, %eax
.LBB0_79:                               # %__strpbrk_c2.exit
	testw	%bp, %bp
	jne	.LBB0_63
# BB#80:                                # %__strpbrk_c2.exit
	testq	%rax, %rax
	jne	.LBB0_63
# BB#81:
	movl	%r14d, 16(%rsp)         # 4-byte Spill
	movl	$1, %edi
	movl	$64, %esi
	callq	cli_calloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_96
# BB#82:
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r12, %rdi
	callq	cli_hex2str
	movq	%rax, %r13
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.LBB0_98
# BB#83:
	movq	%r12, %rdi
	callq	strlen
	shrq	%rax
	movw	%ax, 16(%r14)
	movl	$.L.str.7, %esi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	strstr
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rbp
	testq	%rbx, %rbx
	je	.LBB0_85
# BB#84:
	movq	%rbx, %rdi
	callq	strlen
	decq	%rbp
	subq	%rax, %rbp
.LBB0_85:
	testl	%ebp, %ebp
	jle	.LBB0_97
# BB#86:
	shlq	$32, %rbp
	movabsq	$4294967296, %rdi       # imm = 0x100000000
	addq	%rbp, %rdi
	sarq	$32, %rdi
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, 24(%r14)
	testq	%rax, %rax
	je	.LBB0_102
# BB#87:
	sarq	$32, %rbp
	movq	%rax, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	callq	strncpy
	movq	40(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB0_89
# BB#88:
	callq	cli_strdup
	movq	%rax, 32(%r14)
	testq	%rax, %rax
	je	.LBB0_103
.LBB0_89:
	movl	16(%rsp), %eax          # 4-byte Reload
	movb	%al, 40(%r14)
	movzwl	16(%r14), %eax
	cmpw	(%r15), %ax
	jbe	.LBB0_91
# BB#90:
	movw	%ax, (%r15)
.LBB0_91:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	cli_bm_addpatt
	movl	%eax, %ebp
	testl	%eax, %eax
	je	.LBB0_60
# BB#92:
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	(%r14), %rdi
	callq	free
	movq	24(%r14), %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	jmp	.LBB0_101
.LBB0_93:
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_99
.LBB0_94:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
	jmp	.LBB0_100
.LBB0_95:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	%rbp, %rdi
	callq	free
	movl	%r13d, %ebp
	jmp	.LBB0_101
.LBB0_96:
	movl	$-114, %ebp
	jmp	.LBB0_101
.LBB0_97:
	movq	%r13, %rdi
	callq	free
.LBB0_98:
	movq	%r14, %rdi
.LBB0_99:                               # %.loopexit
	callq	free
.LBB0_100:                              # %.loopexit
	movl	$-116, %ebp
.LBB0_101:                              # %.loopexit
	movl	%ebp, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_102:
	movq	(%r14), %rdi
	jmp	.LBB0_104
.LBB0_103:
	movq	(%r14), %rdi
	callq	free
	movq	24(%r14), %rdi
.LBB0_104:                              # %.loopexit
	callq	free
	movq	%r14, %rdi
	callq	free
	movl	$-114, %ebp
	jmp	.LBB0_101
.Lfunc_end0:
	.size	cli_parse_add, .Lfunc_end0-cli_parse_add
	.cfi_endproc

	.globl	cli_initengine
	.p2align	4, 0x90
	.type	cli_initengine,@function
cli_initengine:                         # @cli_initengine
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 32
.Lcfi37:
	.cfi_offset %rbx, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_1
.LBB1_8:
	testb	$8, %bpl
	je	.LBB1_11
# BB#9:
	movq	80(%rdi), %rax
	testb	$1, 24(%rax)
	je	.LBB1_11
# BB#10:
	callq	phishing_init
	testl	%eax, %eax
	jne	.LBB1_12
.LBB1_11:
	xorl	%eax, %eax
	jmp	.LBB1_12
.LBB1_1:
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %edi
	movl	$88, %esi
	callq	cli_calloc
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB1_2
# BB#4:
	movl	$1, (%rax)
	movl	$7, %edi
	movl	$8, %esi
	callq	cli_calloc
	movq	(%rbx), %rcx
	movq	%rax, 16(%rcx)
	movq	(%rbx), %rax
	cmpq	$0, 16(%rax)
	je	.LBB1_5
# BB#6:
	callq	cli_dconf_init
	movq	(%rbx), %rcx
	movq	%rax, 80(%rcx)
	movq	(%rbx), %rdi
	cmpq	$0, 80(%rdi)
	jne	.LBB1_8
# BB#7:
	movl	$.L.str.12, %edi
	jmp	.LBB1_3
.LBB1_2:
	movl	$.L.str.10, %edi
	jmp	.LBB1_3
.LBB1_5:
	movl	$.L.str.11, %edi
.LBB1_3:
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-114, %eax
.LBB1_12:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	cli_initengine, .Lfunc_end1-cli_initengine
	.cfi_endproc

	.globl	cl_loaddb
	.p2align	4, 0x90
	.type	cl_loaddb,@function
cl_loaddb:                              # @cl_loaddb
	.cfi_startproc
# BB#0:
	movl	$10, %ecx
	jmp	cli_load                # TAILCALL
.Lfunc_end2:
	.size	cl_loaddb, .Lfunc_end2-cl_loaddb
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_load,@function
cli_load:                               # @cli_load
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 48
.Lcfi44:
	.cfi_offset %rbx, -48
.Lcfi45:
	.cfi_offset %r12, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movq	%rdx, %rbp
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$.L.str.45, %esi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB3_1
# BB#2:
	movl	$.L.str.21, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_3
.LBB3_53:                               # %cli_loadwdb.exit
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movl	%r12d, %ecx
	callq	cli_loaddb
	jmp	.LBB3_54
.LBB3_1:
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	movl	$-115, %ebp
	jmp	.LBB3_57
.LBB3_3:
	movl	$.L.str.37, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_5
# BB#4:
	movl	$.L.str.47, %esi
	movq	%rbx, %rdi
	callq	strstr
	xorl	%ecx, %ecx
	testq	%rax, %rax
	setne	%cl
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movl	%r12d, %r8d
	callq	cli_cvdload
	jmp	.LBB3_54
.LBB3_5:
	movl	$.L.str.24, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_8
.LBB3_6:
	xorl	%ecx, %ecx
.LBB3_7:                                # %cli_loadwdb.exit
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movl	%r12d, %r8d
	callq	cli_loadmd5
.LBB3_54:                               # %cli_loadwdb.exit
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB3_50
.LBB3_55:                               # %cli_loadwdb.exit.thread105
	movl	%ebp, %edi
	callq	cl_strerror
	movq	%rax, %rcx
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	callq	cli_errmsg
	jmp	.LBB3_56
.LBB3_8:
	movl	$.L.str.25, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_11
# BB#9:
	testb	$16, %r12b
	jne	.LBB3_6
.LBB3_10:
	xorl	%ebp, %ebp
	movl	$.L.str.51, %edi
	jmp	.LBB3_51
.LBB3_11:
	movl	$.L.str.26, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_13
# BB#12:
	movl	$2, %ecx
	jmp	.LBB3_7
.LBB3_13:
	movl	$.L.str.27, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_15
.LBB3_14:
	movl	$1, %ecx
	jmp	.LBB3_7
.LBB3_15:
	movl	$.L.str.28, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_17
# BB#16:
	testb	$16, %r12b
	je	.LBB3_10
	jmp	.LBB3_14
.LBB3_17:
	movl	$.L.str.29, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_20
.LBB3_18:
	xorl	%ecx, %ecx
.LBB3_19:                               # %cli_loadwdb.exit
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movl	%r12d, %r8d
	callq	cli_loadndb
	jmp	.LBB3_54
.LBB3_20:
	movl	$.L.str.30, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_22
# BB#21:
	testb	$16, %r12b
	je	.LBB3_10
	jmp	.LBB3_18
.LBB3_22:
	movl	$.L.str.31, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_24
# BB#23:
	movl	$1, %ecx
	jmp	.LBB3_19
.LBB3_24:
	movl	$.L.str.32, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_27
# BB#25:
	movl	$1, %ecx
	jmp	.LBB3_26
.LBB3_27:
	movl	$.L.str.33, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_29
# BB#28:
	movl	$2, %ecx
.LBB3_26:                               # %cli_loadwdb.exit
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movl	%r12d, %r8d
	callq	cli_loadmd
	jmp	.LBB3_54
.LBB3_29:
	movl	$.L.str.48, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_31
# BB#30:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	%r12d, %edx
	callq	cli_dconf_load
	jmp	.LBB3_54
.LBB3_31:
	movl	$.L.str.35, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_42
# BB#32:
	testb	$8, %r12b
	je	.LBB3_10
# BB#33:
	movq	%r15, %rdi
	movl	%r12d, %esi
	callq	cli_initengine
	movl	%eax, %ebp
	testl	%ebp, %ebp
	movq	(%r15), %rdi
	je	.LBB3_35
# BB#34:
	callq	cl_free
	jmp	.LBB3_55
.LBB3_42:
	movl	$.L.str.34, %esi
	movq	%rbx, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB3_52
# BB#43:
	testb	$8, %r12b
	je	.LBB3_10
# BB#44:
	movq	%r15, %rdi
	movl	%r12d, %esi
	callq	cli_initengine
	movl	%eax, %ebp
	testl	%ebp, %ebp
	movq	(%r15), %rdi
	je	.LBB3_46
# BB#45:
	callq	cl_free
	jmp	.LBB3_55
.LBB3_52:
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB3_53
.LBB3_35:
	movq	80(%rdi), %rax
	testb	$1, 24(%rax)
	je	.LBB3_50
# BB#36:
	cmpq	$0, 56(%rdi)
	je	.LBB3_37
.LBB3_39:                               # %._crit_edge.i
	movq	56(%rdi), %rdi
	movl	$1, %ecx
	jmp	.LBB3_40
.LBB3_46:
	movq	80(%rdi), %rax
	testb	$1, 24(%rax)
	je	.LBB3_50
# BB#47:
	cmpq	$0, 64(%rdi)
	jne	.LBB3_49
# BB#48:
	callq	init_domainlist
	movl	%eax, %ebp
	testl	%ebp, %ebp
	movq	(%r15), %rdi
	jne	.LBB3_38
.LBB3_49:                               # %._crit_edge.i99
	movq	64(%rdi), %rdi
	xorl	%ecx, %ecx
.LBB3_40:                               # %._crit_edge.i
	movq	%r14, %rsi
	movl	%r12d, %edx
	callq	load_regex_matcher
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB3_50
# BB#41:
	movq	(%r15), %rdi
	jmp	.LBB3_38
.LBB3_50:                               # %.thread
	xorl	%ebp, %ebp
	movl	$.L.str.52, %edi
.LBB3_51:
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
.LBB3_56:
	movq	%r14, %rdi
	callq	fclose
.LBB3_57:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_37:
	callq	init_whitelist
	movl	%eax, %ebp
	testl	%ebp, %ebp
	movq	(%r15), %rdi
	je	.LBB3_39
.LBB3_38:
	callq	phishing_done
	movq	(%r15), %rdi
	callq	cl_free
	jmp	.LBB3_55
.Lfunc_end3:
	.size	cli_load, .Lfunc_end3-cli_load
	.cfi_endproc

	.globl	cl_loaddbdir
	.p2align	4, 0x90
	.type	cl_loaddbdir,@function
cl_loaddbdir:                           # @cl_loaddbdir
	.cfi_startproc
# BB#0:
	movl	$10, %ecx
	jmp	cli_loaddbdir           # TAILCALL
.Lfunc_end4:
	.size	cl_loaddbdir, .Lfunc_end4-cl_loaddbdir
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_loaddbdir,@function
cli_loaddbdir:                          # @cli_loaddbdir
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	subq	$5800, %rsp             # imm = 0x16A8
.Lcfi55:
	.cfi_def_cfa_offset 5856
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rdx, %r14
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	movl	$.L.str.79, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	cli_readlockdb
	movl	%eax, %ebp
	cmpl	$-126, %ebp
	jne	.LBB5_4
# BB#2:                                 #   in Loop: Header=BB5_1 Depth=1
	movl	$5, %edi
	callq	sleep
	incl	%ebx
	cmpl	$25, %ebx
	jl	.LBB5_1
# BB#3:
	movl	$.L.str.80, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	cli_errmsg
	movl	$-126, %ebx
	jmp	.LBB5_89
.LBB5_4:
	movl	$.L.str.81, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	cli_dbgmsg
	movq	%r13, %rdi
	callq	strlen
	leaq	11(%rax), %rdi
	callq	cli_malloc
	movq	%rax, %r12
	movl	$-114, %ebx
	testq	%r12, %r12
	je	.LBB5_87
# BB#5:
	xorl	%ebx, %ebx
	movl	$.L.str.82, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r13, %rdx
	callq	sprintf
	leaq	48(%rsp), %rdx
	movl	$1, %edi
	movq	%r12, %rsi
	callq	__xstat
	cmpl	$-1, %eax
	movq	%r14, 32(%rsp)          # 8-byte Spill
	je	.LBB5_8
# BB#6:
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	movl	%r15d, %ecx
	callq	cli_load
	movl	%eax, %r14d
	testl	%r14d, %r14d
	je	.LBB5_9
# BB#7:
	movq	%r12, %rdi
	callq	free
	movl	%r14d, %ebx
	testl	%ebp, %ebp
	jne	.LBB5_89
	jmp	.LBB5_88
.LBB5_8:
	movl	$-125, %ebx
.LBB5_9:
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movq	%r12, %rdi
	callq	free
	movq	%r13, %rdi
	callq	opendir
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_31
# BB#10:
	leaq	192(%rsp), %rdi
	xorl	%esi, %esi
	movl	$5600, %edx             # imm = 0x15E0
	callq	memset
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	movq	%rbx, (%rsp)            # 8-byte Spill
	leaq	192(%rsp), %rdi
	je	.LBB5_33
# BB#11:
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	je	.LBB5_34
# BB#12:
	leaq	472(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_35
# BB#13:
	leaq	752(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_36
# BB#14:
	leaq	1032(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_37
# BB#15:
	leaq	1312(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_38
# BB#16:
	leaq	1592(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_39
# BB#17:
	leaq	1872(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_40
# BB#18:
	leaq	2152(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_41
# BB#19:
	leaq	2432(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_42
# BB#20:
	leaq	2712(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_43
# BB#21:
	leaq	2992(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_44
# BB#22:
	leaq	3272(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_45
# BB#23:
	leaq	3552(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_46
# BB#24:
	leaq	3832(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_47
# BB#25:
	leaq	4112(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_48
# BB#26:
	leaq	4392(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	%rbx, %rdi
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_49
# BB#27:
	leaq	4672(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_50
# BB#28:
	leaq	4952(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_51
# BB#29:
	leaq	5232(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	readdir
	testq	%rax, %rax
	je	.LBB5_52
# BB#30:
	leaq	5512(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movq	%rax, %rsi
	callq	memcpy
	movl	$.L.str.84, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB5_32
.LBB5_31:
	movl	$.L.str.83, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	cli_errmsg
.LBB5_32:
	movl	$-115, %ebx
	testl	%ebp, %ebp
	jne	.LBB5_89
	jmp	.LBB5_88
.LBB5_33:
	xorl	%esi, %esi
	movl	$280, %edx              # imm = 0x118
	movl	$dirent_compare, %ecx
	callq	qsort
	movl	8(%rsp), %r12d          # 4-byte Reload
	jmp	.LBB5_83
.LBB5_34:
	movl	$1, %ebx
	jmp	.LBB5_53
.LBB5_35:
	movl	$2, %ebx
	jmp	.LBB5_53
.LBB5_36:
	movl	$3, %ebx
	jmp	.LBB5_53
.LBB5_37:
	movl	$4, %ebx
	jmp	.LBB5_53
.LBB5_38:
	movl	$5, %ebx
	jmp	.LBB5_53
.LBB5_39:
	movl	$6, %ebx
	jmp	.LBB5_53
.LBB5_40:
	movl	$7, %ebx
	jmp	.LBB5_53
.LBB5_41:
	movl	$8, %ebx
	jmp	.LBB5_53
.LBB5_42:
	movl	$9, %ebx
	jmp	.LBB5_53
.LBB5_43:
	movl	$10, %ebx
	jmp	.LBB5_53
.LBB5_44:
	movl	$11, %ebx
	jmp	.LBB5_53
.LBB5_45:
	movl	$12, %ebx
	jmp	.LBB5_53
.LBB5_46:
	movl	$13, %ebx
	jmp	.LBB5_53
.LBB5_47:
	movl	$14, %ebx
	jmp	.LBB5_53
.LBB5_48:
	movl	$15, %ebx
	jmp	.LBB5_53
.LBB5_49:
	movl	$16, %ebx
	jmp	.LBB5_53
.LBB5_50:
	movl	$17, %ebx
	jmp	.LBB5_53
.LBB5_51:
	movl	$18, %ebx
	jmp	.LBB5_53
.LBB5_52:
	movl	$19, %ebx
.LBB5_53:                               # %.lr.ph.preheader
	leaq	192(%rsp), %rdi
	movl	$280, %edx              # imm = 0x118
	movl	$dirent_compare, %ecx
	movq	%rbx, %rsi
	callq	qsort
	leaq	211(%rsp), %r15
	xorl	%r14d, %r14d
	movl	8(%rsp), %r12d          # 4-byte Reload
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%r13, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_54:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, -19(%r15)
	je	.LBB5_82
# BB#55:                                #   in Loop: Header=BB5_54 Depth=1
	cmpb	$46, (%r15)
	jne	.LBB5_59
# BB#56:                                #   in Loop: Header=BB5_54 Depth=1
	cmpb	$0, 1(%r15)
	je	.LBB5_82
# BB#57:                                #   in Loop: Header=BB5_54 Depth=1
	cmpb	$46, 1(%r15)
	jne	.LBB5_59
# BB#58:                                #   in Loop: Header=BB5_54 Depth=1
	cmpb	$0, 2(%r15)
	je	.LBB5_82
	.p2align	4, 0x90
.LBB5_59:                               # %.thread
                                        #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.21, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#60:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.22, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#61:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.23, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#62:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.24, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#63:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.25, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#64:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.26, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#65:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.27, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#66:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.28, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#67:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.29, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#68:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.30, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#69:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.31, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#70:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.32, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#71:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.33, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#72:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.34, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#73:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.35, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#74:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.36, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB5_76
# BB#75:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.37, %esi
	movq	%r15, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB5_82
	.p2align	4, 0x90
.LBB5_76:                               #   in Loop: Header=BB5_54 Depth=1
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	strlen
	leaq	2(%rbx,%rax), %rdi
	callq	cli_malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB5_85
# BB#77:                                #   in Loop: Header=BB5_54 Depth=1
	movl	$.L.str.41, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%r15, %rcx
	callq	sprintf
	movl	$.L.str.36, %esi
	movq	%r12, %rdi
	callq	cli_strbcasestr
	movq	%r12, %rdi
	testl	%eax, %eax
	je	.LBB5_79
# BB#78:                                #   in Loop: Header=BB5_54 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	callq	cli_loaddbdir
	jmp	.LBB5_80
.LBB5_79:                               #   in Loop: Header=BB5_54 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	callq	cli_load
.LBB5_80:                               #   in Loop: Header=BB5_54 Depth=1
	movl	%eax, %ebx
	testl	%ebx, %ebx
	jne	.LBB5_86
# BB#81:                                #   in Loop: Header=BB5_54 Depth=1
	movq	%r12, %rdi
	callq	free
	xorl	%r12d, %r12d
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB5_82:                               #   in Loop: Header=BB5_54 Depth=1
	incq	%r14
	addq	$280, %r15              # imm = 0x118
	cmpq	%rbx, %r14
	jb	.LBB5_54
.LBB5_83:                               # %._crit_edge
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	closedir
	cmpl	$-125, %r12d
	movl	%r12d, %ebx
	jne	.LBB5_87
# BB#84:
	movl	$.L.str.87, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	cli_errmsg
	movl	$-125, %ebx
	testl	%ebp, %ebp
	jne	.LBB5_89
	jmp	.LBB5_88
.LBB5_85:
	movl	$.L.str.85, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	closedir
	movl	$-114, %ebx
	testl	%ebp, %ebp
	jne	.LBB5_89
	jmp	.LBB5_88
.LBB5_86:
	movl	$.L.str.86, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
	movq	%r12, %rdi
	callq	free
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	closedir
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB5_87:                               # %cli_loaddbdir_l.exit
	testl	%ebp, %ebp
	jne	.LBB5_89
.LBB5_88:
	movq	%r13, %rdi
	callq	cli_unlockdb
.LBB5_89:
	movl	%ebx, %eax
	addq	$5800, %rsp             # imm = 0x16A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	cli_loaddbdir, .Lfunc_end5-cli_loaddbdir
	.cfi_endproc

	.globl	cl_load
	.p2align	4, 0x90
	.type	cl_load,@function
cl_load:                                # @cl_load
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 48
	subq	$144, %rsp
.Lcfi67:
	.cfi_def_cfa_offset 192
.Lcfi68:
	.cfi_offset %rbx, -48
.Lcfi69:
	.cfi_offset %r12, -40
.Lcfi70:
	.cfi_offset %r14, -32
.Lcfi71:
	.cfi_offset %r15, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rdx, %r14
	movq	%rsi, %rbp
	movq	%rdi, %r12
	movq	%rsp, %rdx
	movl	$1, %edi
	movq	%r12, %rsi
	callq	__xstat
	cmpl	$-1, %eax
	je	.LBB6_1
# BB#2:
	movq	%rbp, %rdi
	movl	%r15d, %esi
	callq	cli_initengine
	movl	%eax, %ebx
	testl	%ebx, %ebx
	movq	(%rbp), %rdi
	je	.LBB6_4
# BB#3:
	callq	cl_free
	jmp	.LBB6_9
.LBB6_1:
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_errmsg
	movl	$-123, %ebx
.LBB6_9:
	movl	%ebx, %eax
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_4:
	movl	%r15d, 8(%rdi)
	movzwl	24(%rsp), %eax
	andl	$61440, %eax            # imm = 0xF000
	cmpl	$16384, %eax            # imm = 0x4000
	je	.LBB6_7
# BB#5:
	movzwl	%ax, %eax
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB6_8
# BB#6:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	movl	%r15d, %ecx
	callq	cli_load
	movl	%eax, %ebx
	jmp	.LBB6_9
.LBB6_7:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	movl	%r15d, %ecx
	callq	cli_loaddbdir
	movl	%eax, %ebx
	jmp	.LBB6_9
.LBB6_8:
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_errmsg
	movl	$-115, %ebx
	jmp	.LBB6_9
.Lfunc_end6:
	.size	cl_load, .Lfunc_end6-cl_load
	.cfi_endproc

	.globl	cl_free
	.p2align	4, 0x90
	.type	cl_free,@function
cl_free:                                # @cl_free
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi77:
	.cfi_def_cfa_offset 48
.Lcfi78:
	.cfi_offset %rbx, -40
.Lcfi79:
	.cfi_offset %r12, -32
.Lcfi80:
	.cfi_offset %r14, -24
.Lcfi81:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB7_55
# BB#1:
	decl	(%r14)
	je	.LBB7_2
# BB#54:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB7_55:
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	cli_errmsg              # TAILCALL
.LBB7_2:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB7_32
# BB#3:                                 # %.preheader66
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB7_7
# BB#4:
	cmpb	$0, 2(%rbx)
	jne	.LBB7_6
# BB#5:
	movq	%rbx, %rdi
	callq	cli_bm_free
.LBB7_6:
	movq	%rbx, %rdi
	callq	cli_ac_free
	movq	%rbx, %rdi
	callq	free
	movq	16(%r14), %rdi
.LBB7_7:
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB7_11
# BB#8:
	cmpb	$0, 2(%rbx)
	jne	.LBB7_10
# BB#9:
	movq	%rbx, %rdi
	callq	cli_bm_free
.LBB7_10:
	movq	%rbx, %rdi
	callq	cli_ac_free
	movq	%rbx, %rdi
	callq	free
	movq	16(%r14), %rdi
.LBB7_11:
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB7_15
# BB#12:
	cmpb	$0, 2(%rbx)
	jne	.LBB7_14
# BB#13:
	movq	%rbx, %rdi
	callq	cli_bm_free
.LBB7_14:
	movq	%rbx, %rdi
	callq	cli_ac_free
	movq	%rbx, %rdi
	callq	free
	movq	16(%r14), %rdi
.LBB7_15:
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB7_19
# BB#16:
	cmpb	$0, 2(%rbx)
	jne	.LBB7_18
# BB#17:
	movq	%rbx, %rdi
	callq	cli_bm_free
.LBB7_18:
	movq	%rbx, %rdi
	callq	cli_ac_free
	movq	%rbx, %rdi
	callq	free
	movq	16(%r14), %rdi
.LBB7_19:
	movq	32(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB7_23
# BB#20:
	cmpb	$0, 2(%rbx)
	jne	.LBB7_22
# BB#21:
	movq	%rbx, %rdi
	callq	cli_bm_free
.LBB7_22:
	movq	%rbx, %rdi
	callq	cli_ac_free
	movq	%rbx, %rdi
	callq	free
	movq	16(%r14), %rdi
.LBB7_23:
	movq	40(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB7_27
# BB#24:
	cmpb	$0, 2(%rbx)
	jne	.LBB7_26
# BB#25:
	movq	%rbx, %rdi
	callq	cli_bm_free
.LBB7_26:
	movq	%rbx, %rdi
	callq	cli_ac_free
	movq	%rbx, %rdi
	callq	free
	movq	16(%r14), %rdi
.LBB7_27:
	movq	48(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB7_31
# BB#28:
	cmpb	$0, 2(%rbx)
	jne	.LBB7_30
# BB#29:
	movq	%rbx, %rdi
	callq	cli_bm_free
.LBB7_30:
	movq	%rbx, %rdi
	callq	cli_ac_free
	movq	%rbx, %rdi
	callq	free
	movq	16(%r14), %rdi
.LBB7_31:
	callq	free
.LBB7_32:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB7_39
# BB#33:                                # %.preheader.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB7_34:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_35 Depth 2
	movq	(%rdi,%r15,8), %rbx
	testq	%rbx, %rbx
	je	.LBB7_37
	.p2align	4, 0x90
.LBB7_35:                               # %.lr.ph75
                                        #   Parent Loop BB7_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movq	24(%rbx), %r12
	callq	free
	movq	(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	testq	%r12, %r12
	movq	%r12, %rbx
	jne	.LBB7_35
# BB#36:                                # %._crit_edge76.loopexit
                                        #   in Loop: Header=BB7_34 Depth=1
	movq	24(%r14), %rdi
.LBB7_37:                               # %._crit_edge76
                                        #   in Loop: Header=BB7_34 Depth=1
	incq	%r15
	cmpq	$256, %r15              # imm = 0x100
	jne	.LBB7_34
# BB#38:
	callq	free
.LBB7_39:
	movq	32(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB7_41
# BB#40:
	movq	%rbx, %rdi
	callq	cli_bm_free
	movq	24(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB7_41:
	movq	40(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB7_45
	.p2align	4, 0x90
.LBB7_42:                               # %.lr.ph71
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rdi
	movq	48(%rbx), %r15
	callq	free
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_44
# BB#43:                                #   in Loop: Header=BB7_42 Depth=1
	callq	free
.LBB7_44:                               #   in Loop: Header=BB7_42 Depth=1
	movq	%rbx, %rdi
	callq	free
	testq	%r15, %r15
	movq	%r15, %rbx
	jne	.LBB7_42
.LBB7_45:                               # %._crit_edge72
	movq	48(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB7_49
	.p2align	4, 0x90
.LBB7_46:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rdi
	movq	48(%rbx), %r15
	callq	free
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_48
# BB#47:                                #   in Loop: Header=BB7_46 Depth=1
	callq	free
.LBB7_48:                               #   in Loop: Header=BB7_46 Depth=1
	movq	%rbx, %rdi
	callq	free
	testq	%r15, %r15
	movq	%r15, %rbx
	jne	.LBB7_46
.LBB7_49:                               # %._crit_edge
	movq	80(%r14), %rdi
	testb	$1, 24(%rdi)
	je	.LBB7_51
# BB#50:
	movq	%r14, %rdi
	callq	phishing_done
	movq	80(%r14), %rdi
.LBB7_51:
	testq	%rdi, %rdi
	je	.LBB7_53
# BB#52:
	callq	free
.LBB7_53:
	callq	cli_freelocks
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end7:
	.size	cl_free, .Lfunc_end7-cl_free
	.cfi_endproc

	.globl	cl_retdbdir
	.p2align	4, 0x90
	.type	cl_retdbdir,@function
cl_retdbdir:                            # @cl_retdbdir
	.cfi_startproc
# BB#0:
	movl	$.L.str.15, %eax
	retq
.Lfunc_end8:
	.size	cl_retdbdir, .Lfunc_end8-cl_retdbdir
	.cfi_endproc

	.globl	cl_statinidir
	.p2align	4, 0x90
	.type	cl_statinidir,@function
cl_statinidir:                          # @cl_statinidir
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 64
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r15
	testq	%r13, %r13
	je	.LBB9_43
# BB#1:
	movl	$0, 8(%r13)
	leaq	16(%r13), %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r13)
	movq	%r15, %rdi
	callq	cli_strdup
	movq	%rax, (%r13)
	movq	%r15, %rdi
	callq	opendir
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB9_2
# BB#6:
	movq	%rbx, (%rsp)            # 8-byte Spill
	xorl	%r12d, %r12d
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	jmp	.LBB9_7
	.p2align	4, 0x90
.LBB9_40:                               #   in Loop: Header=BB9_7 Depth=1
	movl	8(%r13), %eax
	decl	%eax
	leaq	(%rax,%rax,8), %rdx
	shlq	$4, %rdx
	addq	16(%r13), %rdx
	movl	$1, %edi
	movq	%rbx, %rsi
	callq	__xstat
	movq	%rbx, %rdi
	callq	free
.LBB9_7:                                # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	readdir
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB9_41
# BB#8:                                 #   in Loop: Header=BB9_7 Depth=1
	cmpq	$0, (%rbp)
	je	.LBB9_7
# BB#9:                                 #   in Loop: Header=BB9_7 Depth=1
	cmpb	$46, 19(%rbp)
	jne	.LBB9_13
# BB#10:                                #   in Loop: Header=BB9_7 Depth=1
	cmpb	$0, 20(%rbp)
	je	.LBB9_7
# BB#11:                                #   in Loop: Header=BB9_7 Depth=1
	cmpb	$46, 20(%rbp)
	jne	.LBB9_13
# BB#12:                                #   in Loop: Header=BB9_7 Depth=1
	cmpb	$0, 21(%rbp)
	je	.LBB9_7
	.p2align	4, 0x90
.LBB9_13:                               # %.thread
                                        #   in Loop: Header=BB9_7 Depth=1
	addq	$19, %rbp
	movl	$.L.str.21, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#14:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.22, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#15:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.23, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#16:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.24, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#17:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.25, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#18:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.26, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#19:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.27, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#20:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.28, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#21:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.29, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#22:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.30, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#23:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.31, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#24:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.32, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#25:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.33, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#26:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.34, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#27:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.35, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#28:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.36, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB9_30
# BB#29:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.37, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB9_7
	.p2align	4, 0x90
.LBB9_30:                               #   in Loop: Header=BB9_7 Depth=1
	movl	8(%r13), %eax
	incl	%eax
	movl	%eax, 8(%r13)
	movq	16(%r13), %rdi
	shlq	$4, %rax
	leaq	(%rax,%rax,8), %rsi
	callq	cli_realloc2
	movq	%rax, 16(%r13)
	testq	%rax, %rax
	je	.LBB9_31
# BB#34:                                #   in Loop: Header=BB9_7 Depth=1
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	strlen
	leaq	32(%rbx,%rax), %rdi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB9_35
# BB#37:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.36, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB9_39
# BB#38:                                #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.39, %esi
	movq	%rbp, %rdi
	callq	strstr
	testq	%rax, %rax
	movl	$.L.str.40, %r8d
	movl	$.L.str.39, %eax
	cmovneq	%rax, %r8
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	callq	sprintf
	jmp	.LBB9_40
.LBB9_39:                               #   in Loop: Header=BB9_7 Depth=1
	movl	$.L.str.41, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	callq	sprintf
	jmp	.LBB9_40
.LBB9_43:
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-111, %r12d
	jmp	.LBB9_42
.LBB9_41:                               # %._crit_edge
	movq	%r14, %rdi
	callq	closedir
	jmp	.LBB9_42
.LBB9_2:
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_errmsg
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_4
# BB#3:
	callq	free
	movq	$0, (%rbx)
.LBB9_4:
	movl	$0, 8(%r13)
	movq	(%r13), %rdi
	movl	$-115, %r12d
	testq	%rdi, %rdi
	je	.LBB9_42
# BB#5:
	callq	free
	movq	$0, (%r13)
	jmp	.LBB9_42
.LBB9_35:
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_31
# BB#36:
	callq	free
	movq	$0, (%rbx)
.LBB9_31:
	movl	$0, 8(%r13)
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB9_33
# BB#32:
	callq	free
	movq	$0, (%r13)
.LBB9_33:                               # %cl_statfree.exit117
	movq	%r14, %rdi
	callq	closedir
	movl	$-114, %r12d
.LBB9_42:                               # %cl_statfree.exit
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	cl_statinidir, .Lfunc_end9-cl_statinidir
	.cfi_endproc

	.globl	cl_statfree
	.p2align	4, 0x90
	.type	cl_statfree,@function
cl_statfree:                            # @cl_statfree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi97:
	.cfi_def_cfa_offset 32
.Lcfi98:
	.cfi_offset %rbx, -24
.Lcfi99:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB10_5
# BB#1:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_3
# BB#2:
	callq	free
	movq	$0, 16(%rbx)
.LBB10_3:
	movl	$0, 8(%rbx)
	movq	(%rbx), %rdi
	xorl	%ebp, %ebp
	testq	%rdi, %rdi
	je	.LBB10_6
# BB#4:
	callq	free
	movq	$0, (%rbx)
	jmp	.LBB10_6
.LBB10_5:
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-111, %ebp
.LBB10_6:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end10:
	.size	cl_statfree, .Lfunc_end10-cl_statfree
	.cfi_endproc

	.globl	cl_statchkdir
	.p2align	4, 0x90
	.type	cl_statchkdir,@function
cl_statchkdir:                          # @cl_statchkdir
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi103:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi104:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi106:
	.cfi_def_cfa_offset 208
.Lcfi107:
	.cfi_offset %rbx, -56
.Lcfi108:
	.cfi_offset %r12, -48
.Lcfi109:
	.cfi_offset %r13, -40
.Lcfi110:
	.cfi_offset %r14, -32
.Lcfi111:
	.cfi_offset %r15, -24
.Lcfi112:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	testq	%r13, %r13
	je	.LBB11_2
# BB#1:
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB11_2
# BB#3:
	callq	opendir
	movq	%rax, %r14
	movq	(%r13), %rsi
	testq	%r14, %r14
	je	.LBB11_4
# BB#5:
	xorl	%r12d, %r12d
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB11_6:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_36 Depth 2
	movq	%r14, %rdi
	callq	readdir
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB11_42
# BB#7:                                 #   in Loop: Header=BB11_6 Depth=1
	cmpq	$0, (%rbp)
	je	.LBB11_6
# BB#8:                                 #   in Loop: Header=BB11_6 Depth=1
	cmpb	$46, 19(%rbp)
	jne	.LBB11_12
# BB#9:                                 #   in Loop: Header=BB11_6 Depth=1
	cmpb	$0, 20(%rbp)
	je	.LBB11_6
# BB#10:                                #   in Loop: Header=BB11_6 Depth=1
	cmpb	$46, 20(%rbp)
	jne	.LBB11_12
# BB#11:                                #   in Loop: Header=BB11_6 Depth=1
	cmpb	$0, 21(%rbp)
	je	.LBB11_6
	.p2align	4, 0x90
.LBB11_12:                              # %.thread
                                        #   in Loop: Header=BB11_6 Depth=1
	addq	$19, %rbp
	movl	$.L.str.21, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#13:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.22, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#14:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.23, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#15:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.24, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#16:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.25, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#17:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.26, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#18:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.27, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#19:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.28, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#20:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.29, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#21:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.30, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#22:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.31, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#23:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.32, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#24:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.33, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#25:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.34, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#26:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.35, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#27:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.36, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	jne	.LBB11_29
# BB#28:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.37, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	je	.LBB11_6
	.p2align	4, 0x90
.LBB11_29:                              #   in Loop: Header=BB11_6 Depth=1
	movq	(%r13), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	strlen
	leaq	32(%rbx,%rax), %rdi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB11_30
# BB#31:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.36, %esi
	movq	%rbp, %rdi
	callq	cli_strbcasestr
	testl	%eax, %eax
	movq	(%r13), %r15
	je	.LBB11_33
# BB#32:                                #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.39, %esi
	movq	%rbp, %rdi
	callq	strstr
	testq	%rax, %rax
	movl	$.L.str.40, %r8d
	movl	$.L.str.39, %eax
	cmovneq	%rax, %r8
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	callq	sprintf
	jmp	.LBB11_34
.LBB11_33:                              #   in Loop: Header=BB11_6 Depth=1
	movl	$.L.str.41, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	callq	sprintf
.LBB11_34:                              #   in Loop: Header=BB11_6 Depth=1
	movl	$1, %edi
	movq	%rbx, %rsi
	leaq	8(%rsp), %rdx
	callq	__xstat
	movq	%rbx, %rdi
	callq	free
	movl	8(%r13), %eax
	testl	%eax, %eax
	je	.LBB11_38
# BB#35:                                # %.lr.ph
                                        #   in Loop: Header=BB11_6 Depth=1
	movq	16(%r13), %rcx
	movq	16(%rsp), %rdx
	movq	96(%rsp), %rsi
	addq	$88, %rcx
	xorl	%edi, %edi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_36:                              #   Parent Loop BB11_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdx, -80(%rcx)
	jne	.LBB11_40
# BB#37:                                #   in Loop: Header=BB11_36 Depth=2
	movl	$1, %ebp
	cmpq	%rsi, (%rcx)
	jne	.LBB11_38
.LBB11_40:                              #   in Loop: Header=BB11_36 Depth=2
	incl	%edi
	addq	$144, %rcx
	cmpl	%eax, %edi
	jb	.LBB11_36
# BB#41:                                # %._crit_edge
                                        #   in Loop: Header=BB11_6 Depth=1
	testl	%ebp, %ebp
	jne	.LBB11_6
.LBB11_38:
	movq	%r14, %rdi
	callq	closedir
	movl	$1, %r12d
	jmp	.LBB11_39
.LBB11_2:
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-111, %r12d
.LBB11_39:
	movl	%r12d, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_4:
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-115, %r12d
	jmp	.LBB11_39
.LBB11_42:                              # %._crit_edge122
	movq	%r14, %rdi
	callq	closedir
	jmp	.LBB11_39
.LBB11_30:
	movq	%r14, %rdi
	callq	closedir
	movl	$-114, %r12d
	jmp	.LBB11_39
.Lfunc_end11:
	.size	cl_statchkdir, .Lfunc_end11-cl_statchkdir
	.cfi_endproc

	.globl	cl_build
	.p2align	4, 0x90
	.type	cl_build,@function
cl_build:                               # @cl_build
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 16
.Lcfi114:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	cli_addtypesigs
	testl	%eax, %eax
	jne	.LBB12_16
# BB#1:                                 # %.preheader
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB12_3
# BB#2:
	callq	cli_ac_buildtrie
	movq	16(%rbx), %rax
.LBB12_3:
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB12_5
# BB#4:
	callq	cli_ac_buildtrie
	movq	16(%rbx), %rax
.LBB12_5:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB12_7
# BB#6:
	callq	cli_ac_buildtrie
	movq	16(%rbx), %rax
.LBB12_7:
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB12_9
# BB#8:
	callq	cli_ac_buildtrie
	movq	16(%rbx), %rax
.LBB12_9:
	movq	32(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB12_11
# BB#10:
	callq	cli_ac_buildtrie
	movq	16(%rbx), %rax
.LBB12_11:
	movq	40(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB12_13
# BB#12:
	callq	cli_ac_buildtrie
	movq	16(%rbx), %rax
.LBB12_13:
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB12_15
# BB#14:
	callq	cli_ac_buildtrie
.LBB12_15:
	movq	80(%rbx), %rdi
	callq	cli_dconf_print
	xorl	%eax, %eax
.LBB12_16:
	popq	%rbx
	retq
.Lfunc_end12:
	.size	cl_build, .Lfunc_end12-cl_build
	.cfi_endproc

	.globl	cl_dup
	.p2align	4, 0x90
	.type	cl_dup,@function
cl_dup:                                 # @cl_dup
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 16
.Lcfi116:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB13_1
# BB#2:
	incl	(%rbx)
	jmp	.LBB13_3
.LBB13_1:
	xorl	%ebx, %ebx
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB13_3:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end13:
	.size	cl_dup, .Lfunc_end13-cl_dup
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_loaddb,@function
cli_loaddb:                             # @cli_loaddb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi120:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi121:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 56
	subq	$8200, %rsp             # imm = 0x2008
.Lcfi123:
	.cfi_def_cfa_offset 8256
.Lcfi124:
	.cfi_offset %rbx, -56
.Lcfi125:
	.cfi_offset %r12, -48
.Lcfi126:
	.cfi_offset %r13, -40
.Lcfi127:
	.cfi_offset %r14, -32
.Lcfi128:
	.cfi_offset %r15, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	%r15, %rdi
	movl	%ebx, %esi
	callq	cli_initengine
	movl	%eax, %ebp
	testl	%ebp, %ebp
	movq	(%r15), %rdi
	je	.LBB14_2
# BB#1:
	callq	cl_free
	jmp	.LBB14_17
.LBB14_2:
	movl	%ebx, %esi
	callq	cli_initroots
	movl	%eax, %ebp
	testl	%ebp, %ebp
	movq	(%r15), %rdi
	je	.LBB14_4
# BB#3:
	callq	cl_free
	jmp	.LBB14_17
.LBB14_4:
	movq	16(%rdi), %rax
	movq	(%rax), %r13
	movq	%rsp, %rdi
	movl	$8192, %esi             # imm = 0x2000
	movq	%r12, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB14_15
# BB#5:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	movq	%rsp, %rbp
	.p2align	4, 0x90
.LBB14_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	callq	cli_chomp
	movl	$61, %esi
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB14_7
# BB#9:                                 #   in Loop: Header=BB14_6 Depth=1
	movb	$0, (%rax)
	cmpb	$61, 1(%rax)
	je	.LBB14_10
# BB#13:                                #   in Loop: Header=BB14_6 Depth=1
	incq	%rax
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	cli_parse_add
	testl	%eax, %eax
	jne	.LBB14_14
.LBB14_10:                              # %.backedge
                                        #   in Loop: Header=BB14_6 Depth=1
	movl	$8192, %esi             # imm = 0x2000
	movq	%rbp, %rdi
	movq	%r12, %rdx
	callq	fgets
	incl	%ebx
	testq	%rax, %rax
	jne	.LBB14_6
# BB#11:
	xorl	%ebp, %ebp
	testq	%r14, %r14
	je	.LBB14_17
# BB#12:
	movl	(%r14), %eax
	leal	-1(%rax,%rbx), %eax
	movl	%eax, (%r14)
	jmp	.LBB14_17
.LBB14_15:                              # %.loopexit
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB14_16
.LBB14_7:
	movl	$.L.str.53, %edi
	jmp	.LBB14_8
.LBB14_14:
	movl	$.L.str.54, %edi
.LBB14_8:
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
.LBB14_16:
	movq	(%r15), %rdi
	callq	cl_free
	movl	$-116, %ebp
.LBB14_17:
	movl	%ebp, %eax
	addq	$8200, %rsp             # imm = 0x2008
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	cli_loaddb, .Lfunc_end14-cli_loaddb
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_loadmd5,@function
cli_loadmd5:                            # @cli_loadmd5
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi133:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 56
	subq	$8248, %rsp             # imm = 0x2038
.Lcfi136:
	.cfi_def_cfa_offset 8304
.Lcfi137:
	.cfi_offset %rbx, -56
.Lcfi138:
	.cfi_offset %r12, -48
.Lcfi139:
	.cfi_offset %r13, -40
.Lcfi140:
	.cfi_offset %r14, -32
.Lcfi141:
	.cfi_offset %r15, -24
.Lcfi142:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movq	%rdi, %r15
	movq	%rbp, %rdi
	movl	%r8d, %esi
	callq	cli_initengine
	movl	%eax, %r13d
	testl	%r13d, %r13d
	je	.LBB15_3
.LBB15_1:
	movq	(%rbp), %rdi
	callq	cl_free
.LBB15_2:
	movl	%r13d, %eax
	addq	$8248, %rsp             # imm = 0x2038
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_3:
	movq	%rbx, (%rsp)            # 8-byte Spill
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	cmpb	$1, %r12b
	sete	%al
	movl	%eax, 28(%rsp)          # 4-byte Spill
	setne	%cl
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	leaq	48(%rsp), %rdi
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movl	%r12d, 20(%rsp)         # 4-byte Spill
.LBB15_4:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_7 Depth 2
                                        #     Child Loop BB15_23 Depth 2
	movl	$8192, %esi             # imm = 0x2000
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB15_31
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB15_4 Depth=1
	incl	%r14d
	cmpb	$2, %r12b
	je	.LBB15_33
# BB#6:                                 # %.lr.ph.split.preheader
                                        #   in Loop: Header=BB15_4 Depth=1
	movl	%r14d, %ecx
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	48(%rsp), %rbx
.LBB15_7:                               # %.lr.ph.split
                                        #   Parent Loop BB15_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %r14d
	movq	%rbx, %rdi
	callq	cli_chomp
	movl	$1, %edi
	movl	$32, %esi
	callq	cli_calloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB15_55
# BB#8:                                 #   in Loop: Header=BB15_7 Depth=2
	movl	$.L.str.64, %edx
	movq	%rbx, %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	callq	cli_strtok
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB15_42
# BB#9:                                 #   in Loop: Header=BB15_7 Depth=2
	movq	%r15, %rdi
	callq	cli_hex2str
	movq	%rax, 8(%r13)
	testq	%rax, %rax
	je	.LBB15_43
# BB#10:                                #   in Loop: Header=BB15_7 Depth=2
	movq	%r14, %r12
	movq	%r13, %r14
	addq	$8, %r14
	movq	%r15, %rdi
	callq	free
	movl	$.L.str.64, %edx
	leaq	48(%rsp), %rbx
	movq	%rbx, %rdi
	movl	24(%rsp), %esi          # 4-byte Reload
	callq	cli_strtok
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB15_45
# BB#11:                                #   in Loop: Header=BB15_7 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strtol
	movl	%eax, 16(%r13)
	movq	%rbp, %rdi
	callq	free
	movl	$2, %esi
	movl	$.L.str.64, %edx
	movq	%rbx, %rdi
	callq	cli_strtok
	movq	%rax, (%r13)
	testq	%rax, %rax
	je	.LBB15_46
# BB#12:                                #   in Loop: Header=BB15_7 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%rbp), %rax
	cmpb	$1, 20(%rsp)            # 1-byte Folded Reload
	je	.LBB15_16
# BB#13:                                #   in Loop: Header=BB15_7 Depth=2
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.LBB15_15
# BB#14:                                #   in Loop: Header=BB15_7 Depth=2
	movl	$.L.str.70, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$256, %edi              # imm = 0x100
	movl	$8, %esi
	callq	cli_calloc
	movq	(%rbp), %rcx
	movq	%rax, 24(%rcx)
	movq	(%rbp), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.LBB15_49
.LBB15_15:                              #   in Loop: Header=BB15_7 Depth=2
	movq	8(%r13), %rcx
	movzbl	(%rcx), %edx
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 24(%r13)
	movq	(%rbp), %rax
	movq	24(%rax), %rax
	movzbl	(%rcx), %ecx
	movq	%r13, (%rax,%rcx,8)
	movl	$8192, %esi             # imm = 0x2000
	movq	%rbx, %rdi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	fgets
	movq	%r12, %r14
	leal	1(%r14), %ecx
	testq	%rax, %rax
	jne	.LBB15_7
	jmp	.LBB15_30
.LBB15_16:                              # %.us-lcssa210.us
                                        #   in Loop: Header=BB15_4 Depth=1
	cmpq	$0, 32(%rax)
	movq	%r12, %r14
	jne	.LBB15_20
# BB#17:                                #   in Loop: Header=BB15_4 Depth=1
	movl	$80, %edi
	movl	$1, %esi
	callq	cli_calloc
	movq	(%rbp), %rcx
	movq	%rax, 32(%rcx)
	movq	(%rbp), %rax
	movq	32(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB15_52
# BB#18:                                #   in Loop: Header=BB15_4 Depth=1
	callq	cli_bm_init
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB15_63
# BB#19:                                # %._crit_edge
                                        #   in Loop: Header=BB15_4 Depth=1
	movq	(%rbp), %rax
.LBB15_20:                              #   in Loop: Header=BB15_4 Depth=1
	movq	32(%rax), %r15
	movl	$1, %edi
	movl	$64, %esi
	callq	cli_calloc
	movq	%rax, %r12
	testq	%r12, %r12
	movq	%r15, 40(%rsp)          # 8-byte Spill
	je	.LBB15_51
# BB#21:                                #   in Loop: Header=BB15_4 Depth=1
	movq	8(%r13), %rax
	movq	%rax, (%r12)
	movw	$16, 16(%r12)
	movq	(%r13), %rax
	movq	%rax, 24(%r12)
	movl	32(%r15), %esi
	testl	%esi, %esi
	movq	%r15, %r8
	je	.LBB15_25
# BB#22:                                # %.lr.ph213
                                        #   in Loop: Header=BB15_4 Depth=1
	movq	24(%r8), %rdi
	movl	16(%r13), %eax
	xorl	%ecx, %ecx
	movq	32(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB15_23:                              #   Parent Loop BB15_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%eax, (%rdi,%rcx,4)
	je	.LBB15_28
# BB#24:                                #   in Loop: Header=BB15_23 Depth=2
	incq	%rcx
	cmpl	%esi, %ecx
	jb	.LBB15_23
	jmp	.LBB15_26
.LBB15_25:                              # %..critedge_crit_edge
                                        #   in Loop: Header=BB15_4 Depth=1
	movq	24(%r8), %rdi
	xorl	%esi, %esi
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB15_26:                              # %.critedge
                                        #   in Loop: Header=BB15_4 Depth=1
	incl	%esi
	movl	%esi, 32(%r8)
	shlq	$2, %rsi
	callq	cli_realloc2
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	%rax, 24(%r8)
	testq	%rax, %rax
	je	.LBB15_53
# BB#27:                                #   in Loop: Header=BB15_4 Depth=1
	movl	16(%r13), %ecx
	movl	32(%r8), %edx
	decl	%edx
	movl	%ecx, (%rax,%rdx,4)
.LBB15_28:                              # %.loopexit
                                        #   in Loop: Header=BB15_4 Depth=1
	movq	%r13, %rdi
	movq	%r8, %rbx
	callq	free
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	cli_bm_addpatt
	movl	%eax, %r13d
	testl	%r13d, %r13d
	movq	%r12, %rbx
	movl	20(%rsp), %r12d         # 4-byte Reload
	leaq	48(%rsp), %rdi
	je	.LBB15_4
# BB#29:
	movl	$.L.str.69, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	(%rbx), %rdi
	callq	free
	movq	24(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB15_32
.LBB15_30:
	xorl	%r13d, %r13d
	jmp	.LBB15_44
.LBB15_31:
	xorl	%r13d, %r13d
.LBB15_32:                              # %.loopexit119
	movq	(%rsp), %r15            # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	testl	%r14d, %r14d
	jne	.LBB15_56
	jmp	.LBB15_58
.LBB15_33:                              # %.lr.ph.split.us.preheader
	leaq	48(%rsp), %rbx
	movl	%r14d, %ecx
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB15_34:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %r14d
	movq	%rbx, %rdi
	callq	cli_chomp
	movl	$1, %edi
	movl	$32, %esi
	callq	cli_calloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB15_50
# BB#35:                                #   in Loop: Header=BB15_34 Depth=1
	movw	$1, 20(%r13)
	movl	$.L.str.64, %edx
	movq	%rbx, %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	callq	cli_strtok
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB15_42
# BB#36:                                #   in Loop: Header=BB15_34 Depth=1
	movq	%r15, %rdi
	callq	cli_hex2str
	movq	%rax, 8(%r13)
	testq	%rax, %rax
	je	.LBB15_43
# BB#37:                                #   in Loop: Header=BB15_34 Depth=1
	movq	%r14, %r12
	movq	%r13, %r14
	addq	$8, %r14
	movq	%r15, %rdi
	callq	free
	movl	$.L.str.64, %edx
	movq	%rbx, %rdi
	movl	24(%rsp), %esi          # 4-byte Reload
	callq	cli_strtok
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB15_45
# BB#38:                                #   in Loop: Header=BB15_34 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strtol
	movl	%eax, 16(%r13)
	movq	%rbp, %rdi
	callq	free
	movl	$2, %esi
	movl	$.L.str.64, %edx
	movq	%rbx, %rdi
	callq	cli_strtok
	movq	%rax, (%r13)
	testq	%rax, %rax
	je	.LBB15_46
# BB#39:                                #   in Loop: Header=BB15_34 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%rbp), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.LBB15_41
# BB#40:                                #   in Loop: Header=BB15_34 Depth=1
	movl	$.L.str.70, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$256, %edi              # imm = 0x100
	movl	$8, %esi
	callq	cli_calloc
	movq	(%rbp), %rcx
	movq	%rax, 24(%rcx)
	movq	(%rbp), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.LBB15_49
.LBB15_41:                              #   in Loop: Header=BB15_34 Depth=1
	movq	8(%r13), %rcx
	movzbl	(%rcx), %edx
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 24(%r13)
	movq	(%rbp), %rax
	movq	24(%rax), %rax
	movzbl	(%rcx), %ecx
	movq	%r13, (%rax,%rcx,8)
	movl	$8192, %esi             # imm = 0x2000
	movq	%rbx, %rdi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	fgets
	movq	%r12, %r14
	leal	1(%r14), %ecx
	xorl	%r13d, %r13d
	testq	%rax, %rax
	jne	.LBB15_34
	jmp	.LBB15_44
.LBB15_42:
	movq	(%rsp), %r15            # 8-byte Reload
	movq	%r13, %rdi
	callq	free
	movl	$-116, %r13d
	testl	%r14d, %r14d
	jne	.LBB15_56
	jmp	.LBB15_58
.LBB15_43:                              # %.us-lcssa207.us
	movl	$.L.str.65, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_errmsg
	movq	%r15, %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movl	$-116, %r13d
.LBB15_44:                              # %.loopexit119
	movq	(%rsp), %r15            # 8-byte Reload
	testl	%r14d, %r14d
	jne	.LBB15_56
	jmp	.LBB15_58
.LBB15_45:                              # %.us-lcssa208.us
	movq	(%r14), %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movl	$-116, %r13d
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB15_47
.LBB15_46:
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%r14), %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movl	$-116, %r13d
.LBB15_47:                              # %.loopexit119
	movq	(%rsp), %r15            # 8-byte Reload
.LBB15_48:                              # %.loopexit119
	movq	%r12, %r14
	testl	%r14d, %r14d
	jne	.LBB15_56
	jmp	.LBB15_58
.LBB15_49:
	movq	(%rsp), %r15            # 8-byte Reload
	movq	(%r13), %rdi
	callq	free
	movq	(%r14), %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movl	$-114, %r13d
	jmp	.LBB15_48
.LBB15_50:
	movl	$-114, %r13d
	jmp	.LBB15_44
.LBB15_51:
	movl	$.L.str.67, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB15_52:
	movq	(%r13), %rdi
	callq	free
	movq	8(%r13), %rdi
	jmp	.LBB15_54
.LBB15_53:
	movl	$.L.str.68, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	(%r12), %rdi
	callq	free
	movq	24(%r12), %rdi
	callq	free
	movq	%r12, %rdi
.LBB15_54:                              # %.loopexit119
	callq	free
	movq	%r13, %rdi
	callq	free
.LBB15_55:
	movq	(%rsp), %r15            # 8-byte Reload
	movl	$-114, %r13d
	testl	%r14d, %r14d
	je	.LBB15_58
.LBB15_56:
	testl	%r13d, %r13d
	je	.LBB15_59
# BB#57:
	movl	$.L.str.72, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_errmsg
	jmp	.LBB15_1
.LBB15_58:
	movl	$.L.str.71, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	(%rbp), %rdi
	callq	cl_free
	movl	$-116, %r13d
	jmp	.LBB15_2
.LBB15_59:
	testq	%r15, %r15
	je	.LBB15_61
# BB#60:
	addl	%r14d, (%r15)
.LBB15_61:
	xorl	%r13d, %r13d
	movq	40(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB15_2
# BB#62:
	movq	24(%rax), %rdi
	movl	32(%rax), %esi
	movl	$4, %edx
	movl	$scomp, %ecx
	callq	qsort
	jmp	.LBB15_2
.LBB15_63:
	movl	$.L.str.66, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	(%r13), %rdi
	callq	free
	movq	8(%r13), %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movl	%r15d, %r13d
	jmp	.LBB15_44
.Lfunc_end15:
	.size	cli_loadmd5, .Lfunc_end15-cli_loadmd5
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_loadndb,@function
cli_loadndb:                            # @cli_loadndb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi143:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi144:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi145:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi146:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi147:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi148:
	.cfi_def_cfa_offset 56
	subq	$8312, %rsp             # imm = 0x2078
.Lcfi149:
	.cfi_def_cfa_offset 8368
.Lcfi150:
	.cfi_offset %rbx, -56
.Lcfi151:
	.cfi_offset %r12, -48
.Lcfi152:
	.cfi_offset %r13, -40
.Lcfi153:
	.cfi_offset %r14, -32
.Lcfi154:
	.cfi_offset %r15, -24
.Lcfi155:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movl	%ecx, %r12d
	movq	%rdx, %r13
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	cli_initengine
	movl	%eax, %r15d
	testl	%r15d, %r15d
	movq	(%r14), %rdi
	jne	.LBB16_4
# BB#2:
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movl	%ebp, %esi
	callq	cli_initroots
	movl	%eax, %r15d
	testl	%r15d, %r15d
	je	.LBB16_6
.LBB16_3:
	movq	(%r14), %rdi
.LBB16_4:
	callq	cl_free
.LBB16_5:
	movl	%r15d, %eax
	addq	$8312, %rsp             # imm = 0x2078
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_6:                               # %.preheader
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movl	%r12d, 48(%rsp)         # 4-byte Spill
	andl	$2, %ebp
	leaq	112(%rsp), %rbx
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movl	%ebp, 52(%rsp)          # 4-byte Spill
.LBB16_7:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_9 Depth 2
	movl	$8192, %esi             # imm = 0x2000
	movq	%rbx, %rdi
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB16_43
# BB#8:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB16_7 Depth=1
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	movq	16(%rsp), %rbp          # 8-byte Reload
	leal	1(%rbp), %ecx
	movl	%ebp, %eax
	.p2align	4, 0x90
.LBB16_9:                               # %.lr.ph
                                        #   Parent Loop BB16_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %r14d
	incl	%eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movl	$.L.str.73, %esi
	movl	$20, %edx
	movq	%rbx, %r13
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB16_25
# BB#10:                                #   in Loop: Header=BB16_9 Depth=2
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	jne	.LBB16_13
# BB#11:                                #   in Loop: Header=BB16_9 Depth=2
	movl	$.L.str.74, %esi
	movl	$13, %edx
	movq	%r13, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB16_25
# BB#12:                                #   in Loop: Header=BB16_9 Depth=2
	movl	$.L.str.75, %esi
	movl	$14, %edx
	movq	%r13, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB16_25
.LBB16_13:                              #   in Loop: Header=BB16_9 Depth=2
	movq	%r13, %rbx
	movq	%rbx, %rdi
	callq	cli_chomp
	movl	$58, %esi
	movl	$6, %edx
	movq	%rbx, %rdi
	leaq	64(%rsp), %rcx
	callq	cli_strtokenize
	movq	64(%rsp), %r12
	testq	%r12, %r12
	je	.LBB16_32
# BB#14:                                #   in Loop: Header=BB16_9 Depth=2
	movq	96(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB16_21
# BB#15:                                #   in Loop: Header=BB16_9 Depth=2
	callq	__ctype_b_loc
	movq	%rax, %rbp
	movq	(%rbp), %rax
	movsbq	(%rbx), %rcx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB16_32
# BB#16:                                #   in Loop: Header=BB16_9 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %r15
	callq	cl_retflevel
	cmpl	%eax, %r15d
	jbe	.LBB16_18
# BB#17:                                #   in Loop: Header=BB16_9 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %rcx
	movl	$.L.str.76, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	movl	%ecx, %edx
	callq	cli_dbgmsg
	movq	40(%rsp), %r15          # 8-byte Reload
	jmp	.LBB16_25
.LBB16_18:                              #   in Loop: Header=BB16_9 Depth=2
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	movq	40(%rsp), %r15          # 8-byte Reload
	je	.LBB16_21
# BB#19:                                #   in Loop: Header=BB16_9 Depth=2
	movq	(%rbp), %rax
	movsbq	(%rdi), %rcx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB16_32
# BB#20:                                #   in Loop: Header=BB16_9 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbx
	callq	cl_retflevel
	cmpl	%eax, %ebx
	jb	.LBB16_25
	.p2align	4, 0x90
.LBB16_21:                              #   in Loop: Header=BB16_9 Depth=2
	movq	72(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB16_32
# BB#22:                                #   in Loop: Header=BB16_9 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	(%rbx), %rcx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB16_32
# BB#23:                                #   in Loop: Header=BB16_9 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movzwl	%ax, %ecx
	cmpl	$7, %ecx
	jb	.LBB16_26
# BB#24:                                #   in Loop: Header=BB16_9 Depth=2
	movl	$.L.str.77, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
.LBB16_25:                              # %.backedge
                                        #   in Loop: Header=BB16_9 Depth=2
	movl	$8192, %esi             # imm = 0x2000
	movq	%r13, %rbx
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fgets
	leal	1(%r14), %ecx
	testq	%rax, %rax
	movl	36(%rsp), %eax          # 4-byte Reload
	jne	.LBB16_9
	jmp	.LBB16_33
.LBB16_26:                              #   in Loop: Header=BB16_7 Depth=1
	movq	%r14, 16(%rsp)          # 8-byte Spill
	incl	12(%rsp)                # 4-byte Folded Spill
	movq	80(%rsp), %r8
	movl	$-116, %r15d
	testq	%r8, %r8
	movq	24(%rsp), %r14          # 8-byte Reload
	je	.LBB16_45
# BB#27:                                #   in Loop: Header=BB16_7 Depth=1
	movq	(%r14), %rdx
	movq	16(%rdx), %rdx
	movq	(%rdx,%rcx,8), %rdi
	movzbl	(%r8), %edx
	movl	$42, %ecx
	subl	%edx, %ecx
	jne	.LBB16_29
# BB#28:                                #   in Loop: Header=BB16_7 Depth=1
	movzbl	1(%r8), %ecx
	negl	%ecx
.LBB16_29:                              #   in Loop: Header=BB16_7 Depth=1
	movq	88(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB16_34
# BB#30:                                #   in Loop: Header=BB16_7 Depth=1
	testl	%ecx, %ecx
	movl	$0, %ecx
	cmoveq	%rcx, %r8
	movzwl	%ax, %r9d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	callq	cli_parse_add
	testl	%eax, %eax
	movq	%r13, %rbx
	movl	12(%rsp), %r12d         # 4-byte Reload
	je	.LBB16_7
# BB#31:
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_errmsg
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jne	.LBB16_35
	jmp	.LBB16_44
.LBB16_32:                              # %..loopexit.loopexit_crit_edge128
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movl	12(%rsp), %r12d         # 4-byte Reload
	incl	%r12d
	movl	$-116, %r15d
	movq	24(%rsp), %r14          # 8-byte Reload
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jne	.LBB16_35
	jmp	.LBB16_44
.LBB16_33:
	movq	%r14, 16(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB16_34:
	movl	12(%rsp), %r12d         # 4-byte Reload
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB16_44
.LBB16_35:
	testl	%r15d, %r15d
	je	.LBB16_37
# BB#36:
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_errmsg
	jmp	.LBB16_3
.LBB16_37:
	movq	56(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB16_39
# BB#38:
	addl	%r12d, (%rax)
.LBB16_39:
	xorl	%r15d, %r15d
	cmpw	$0, 48(%rsp)            # 2-byte Folded Reload
	je	.LBB16_5
# BB#40:
	testl	%r12d, %r12d
	je	.LBB16_5
# BB#41:
	movq	(%r14), %rax
	cmpw	$0, 4(%rax)
	jne	.LBB16_5
# BB#42:
	movw	$1, 4(%rax)
	xorl	%r15d, %r15d
	movl	$.L.str.78, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB16_5
.LBB16_43:
	xorl	%r15d, %r15d
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jne	.LBB16_35
.LBB16_44:
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	(%r14), %rdi
	callq	cl_free
	movl	$-116, %r15d
	jmp	.LBB16_5
.LBB16_45:
	movl	36(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB16_34
.Lfunc_end16:
	.size	cli_loadndb, .Lfunc_end16-cli_loadndb
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_loadmd,@function
cli_loadmd:                             # @cli_loadmd
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi156:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi157:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi158:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi159:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi160:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 56
	subq	$8248, %rsp             # imm = 0x2038
.Lcfi162:
	.cfi_def_cfa_offset 8304
.Lcfi163:
	.cfi_offset %rbx, -56
.Lcfi164:
	.cfi_offset %r12, -48
.Lcfi165:
	.cfi_offset %r13, -40
.Lcfi166:
	.cfi_offset %r14, -32
.Lcfi167:
	.cfi_offset %r15, -24
.Lcfi168:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	%r15, %rdi
	movl	%r8d, %esi
	callq	cli_initengine
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB17_1
.LBB17_59:
	movq	(%r15), %rdi
	callq	cl_free
	jmp	.LBB17_62
.LBB17_1:                               # %.preheader
	leaq	48(%rsp), %rdi
	movl	$8192, %esi             # imm = 0x2000
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB17_56
# BB#2:                                 # %.lr.ph.lr.ph
	movl	%r12d, 44(%rsp)         # 4-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	xorl	%ecx, %ecx
	leaq	48(%rsp), %rbp
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	notl	%ecx
	jmp	.LBB17_3
.LBB17_5:                               # %.lr.ph
                                        #   in Loop: Header=BB17_3 Depth=1
	notl	%ecx
	.p2align	4, 0x90
.LBB17_3:                               # =>This Inner Loop Header: Depth=1
	movl	%ecx, %r12d
	cmpb	$35, 48(%rsp)
	je	.LBB17_4
# BB#6:                                 #   in Loop: Header=BB17_3 Depth=1
	movq	%rbp, %rdi
	callq	cli_chomp
	movl	$1, %edi
	movl	$56, %esi
	callq	cli_calloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB17_7
# BB#8:                                 #   in Loop: Header=BB17_3 Depth=1
	xorl	%esi, %esi
	movl	$.L.str.64, %edx
	movq	%rbp, %rdi
	callq	cli_strtok
	movq	%rax, 40(%r13)
	testq	%rax, %rax
	je	.LBB17_9
# BB#10:                                #   in Loop: Header=BB17_3 Depth=1
	movl	$1, %esi
	movl	$.L.str.64, %edx
	movq	%rbp, %rdi
	callq	cli_strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB17_11
# BB#12:                                #   in Loop: Header=BB17_3 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movl	%eax, 20(%r13)
	movq	%rbx, %rdi
	callq	free
	movl	$2, %esi
	movl	$.L.str.64, %edx
	movq	%rbp, %rdi
	callq	cli_strtok
	movq	%rax, 32(%r13)
	testq	%rax, %rax
	je	.LBB17_11
# BB#13:                                #   in Loop: Header=BB17_3 Depth=1
	movq	%r12, 16(%rsp)          # 8-byte Spill
	cmpb	$42, (%rax)
	jne	.LBB17_16
# BB#14:                                #   in Loop: Header=BB17_3 Depth=1
	cmpb	$0, 1(%rax)
	jne	.LBB17_16
# BB#15:                                #   in Loop: Header=BB17_3 Depth=1
	movq	%rax, %rdi
	callq	free
	movq	$0, 32(%r13)
	.p2align	4, 0x90
.LBB17_16:                              # %.thread
                                        #   in Loop: Header=BB17_3 Depth=1
	movl	$3, %esi
	movl	$.L.str.64, %edx
	movq	%rbp, %rdi
	callq	cli_strtok
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB17_17
# BB#18:                                #   in Loop: Header=BB17_3 Depth=1
	cmpb	$42, (%r12)
	jne	.LBB17_21
# BB#19:                                #   in Loop: Header=BB17_3 Depth=1
	cmpb	$0, 1(%r12)
	je	.LBB17_20
.LBB17_21:                              # %.thread440
                                        #   in Loop: Header=BB17_3 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	callq	strtol
.LBB17_22:                              #   in Loop: Header=BB17_3 Depth=1
	movl	%eax, 4(%r13)
	movq	%r12, %rdi
	callq	free
	movl	$4, %esi
	movl	$.L.str.64, %edx
	movq	%rbp, %r12
	movq	%rbp, %rdi
	callq	cli_strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB17_17
# BB#23:                                #   in Loop: Header=BB17_3 Depth=1
	cmpb	$42, (%rbx)
	jne	.LBB17_26
# BB#24:                                #   in Loop: Header=BB17_3 Depth=1
	cmpb	$0, 1(%rbx)
	je	.LBB17_25
.LBB17_26:                              # %.thread441
                                        #   in Loop: Header=BB17_3 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
.LBB17_27:                              #   in Loop: Header=BB17_3 Depth=1
	movq	32(%rsp), %r15          # 8-byte Reload
	movl	%eax, (%r13)
	movq	%rbx, %rdi
	callq	free
	movl	$5, %esi
	movl	$.L.str.64, %edx
	movq	%r12, %rdi
	callq	cli_strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB17_28
# BB#29:                                #   in Loop: Header=BB17_3 Depth=1
	cmpb	$42, (%rbx)
	jne	.LBB17_36
# BB#30:                                #   in Loop: Header=BB17_3 Depth=1
	cmpb	$0, 1(%rbx)
	je	.LBB17_31
.LBB17_36:                              # %.thread442
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	%rbx, %rdi
	callq	cli_hex2num
	cmpl	$-1, %eax
	je	.LBB17_37
.LBB17_32:                              #   in Loop: Header=BB17_3 Depth=1
	movl	%eax, 12(%r13)
	movq	%rbx, %rdi
	callq	free
	movl	$6, %esi
	movl	$.L.str.64, %edx
	movq	%r12, %rdi
	callq	cli_strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB17_28
# BB#33:                                #   in Loop: Header=BB17_3 Depth=1
	cmpb	$42, (%rbx)
	jne	.LBB17_38
# BB#34:                                #   in Loop: Header=BB17_3 Depth=1
	cmpb	$0, 1(%rbx)
	je	.LBB17_35
.LBB17_38:                              # %.thread443
                                        #   in Loop: Header=BB17_3 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
.LBB17_39:                              #   in Loop: Header=BB17_3 Depth=1
	movl	%eax, 8(%r13)
	movq	%rbx, %rdi
	callq	free
	movl	$7, %esi
	movl	$.L.str.64, %edx
	movq	%r12, %rdi
	callq	cli_strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB17_28
# BB#40:                                #   in Loop: Header=BB17_3 Depth=1
	cmpb	$42, (%rbx)
	jne	.LBB17_43
# BB#41:                                #   in Loop: Header=BB17_3 Depth=1
	cmpb	$0, 1(%rbx)
	je	.LBB17_42
.LBB17_43:                              # %.thread444
                                        #   in Loop: Header=BB17_3 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
.LBB17_44:                              #   in Loop: Header=BB17_3 Depth=1
	movl	%eax, 16(%r13)
	movq	%rbx, %rdi
	callq	free
	movl	$8, %esi
	movl	$.L.str.64, %edx
	movq	%r12, %rdi
	callq	cli_strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB17_28
# BB#45:                                #   in Loop: Header=BB17_3 Depth=1
	cmpb	$42, (%rbx)
	jne	.LBB17_48
# BB#46:                                #   in Loop: Header=BB17_3 Depth=1
	cmpb	$0, 1(%rbx)
	je	.LBB17_47
.LBB17_48:                              # %.thread445
                                        #   in Loop: Header=BB17_3 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
.LBB17_49:                              #   in Loop: Header=BB17_3 Depth=1
	movl	%eax, 24(%r13)
	movq	%rbx, %rdi
	callq	free
	movq	(%r15), %rax
	cmpl	$1, 44(%rsp)            # 4-byte Folded Reload
	jne	.LBB17_51
# BB#50:                                #   in Loop: Header=BB17_3 Depth=1
	movq	40(%rax), %rax
	movq	%rax, 48(%r13)
	movq	(%r15), %rax
	addq	$40, %rax
	jmp	.LBB17_52
	.p2align	4, 0x90
.LBB17_51:                              #   in Loop: Header=BB17_3 Depth=1
	movq	48(%rax), %rax
	movq	%rax, 48(%r13)
	movq	(%r15), %rax
	addq	$48, %rax
.LBB17_52:                              # %.backedge
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	%r12, %rbp
	movq	%r13, (%rax)
	movl	$8192, %esi             # imm = 0x2000
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	fgets
	movq	16(%rsp), %r12          # 8-byte Reload
	leal	-1(%r12), %ecx
	testq	%rax, %rax
	jne	.LBB17_3
	jmp	.LBB17_53
.LBB17_20:                              #   in Loop: Header=BB17_3 Depth=1
	movl	$-1, %eax
	jmp	.LBB17_22
.LBB17_25:                              #   in Loop: Header=BB17_3 Depth=1
	movl	$-1, %eax
	jmp	.LBB17_27
.LBB17_31:                              #   in Loop: Header=BB17_3 Depth=1
	xorl	%eax, %eax
	jmp	.LBB17_32
.LBB17_35:                              #   in Loop: Header=BB17_3 Depth=1
	movl	$-1, %eax
	jmp	.LBB17_39
.LBB17_42:                              #   in Loop: Header=BB17_3 Depth=1
	xorl	%eax, %eax
	jmp	.LBB17_44
.LBB17_47:                              #   in Loop: Header=BB17_3 Depth=1
	xorl	%eax, %eax
	jmp	.LBB17_49
.LBB17_4:                               # %.outer
                                        #   in Loop: Header=BB17_3 Depth=1
	incl	28(%rsp)                # 4-byte Folded Spill
	movl	$8192, %esi             # imm = 0x2000
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	fgets
	movl	%r12d, %ecx
	negl	%ecx
	testq	%rax, %rax
	jne	.LBB17_5
.LBB17_53:
	xorl	%ebp, %ebp
	jmp	.LBB17_54
.LBB17_28:
	movq	32(%r13), %rdi
	callq	free
	movq	40(%r13), %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
.LBB17_37:
	movl	$-116, %ebp
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	testl	%r12d, %r12d
	jne	.LBB17_57
	jmp	.LBB17_56
.LBB17_11:
	movq	40(%r13), %rdi
	callq	free
.LBB17_9:
	movq	%r13, %rdi
	callq	free
	movl	$-116, %ebp
	jmp	.LBB17_54
.LBB17_17:
	movq	32(%r13), %rdi
	callq	free
	movq	40(%r13), %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movl	$-116, %ebp
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	testl	%r12d, %r12d
	jne	.LBB17_57
	jmp	.LBB17_56
.LBB17_7:
	movl	$-114, %ebp
.LBB17_54:                              # %.loopexit
	movq	8(%rsp), %rcx           # 8-byte Reload
	testl	%r12d, %r12d
	je	.LBB17_56
.LBB17_57:
	testl	%ebp, %ebp
	je	.LBB17_60
# BB#58:
	negl	%r12d
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	cli_errmsg
	jmp	.LBB17_59
.LBB17_56:                              # %.loopexit.thread
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	(%r15), %rdi
	callq	cl_free
	movl	$-116, %ebp
.LBB17_62:
	movl	%ebp, %eax
	addq	$8248, %rsp             # imm = 0x2038
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_60:
	xorl	%ebp, %ebp
	testq	%rcx, %rcx
	je	.LBB17_62
# BB#61:
	movl	(%rcx), %eax
	subl	28(%rsp), %eax          # 4-byte Folded Reload
	subl	%r12d, %eax
	movl	%eax, (%rcx)
	jmp	.LBB17_62
.Lfunc_end17:
	.size	cli_loadmd, .Lfunc_end17-cli_loadmd
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_initroots,@function
cli_initroots:                          # @cli_initroots
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi171:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi172:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi173:
	.cfi_def_cfa_offset 48
.Lcfi174:
	.cfi_offset %rbx, -40
.Lcfi175:
	.cfi_offset %r14, -32
.Lcfi176:
	.cfi_offset %r15, -24
.Lcfi177:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	testb	$4, %sil
	jne	.LBB18_1
	.p2align	4, 0x90
.LBB18_4:                               # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	cmpq	$0, (%rax,%rbx,8)
	jne	.LBB18_9
# BB#5:                                 #   in Loop: Header=BB18_4 Depth=1
	movl	$.L.str.57, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
	movl	$1, %edi
	movl	$80, %esi
	callq	cli_calloc
	movq	%rax, %r15
	movq	16(%r14), %rax
	movq	%r15, (%rax,%rbx,8)
	testq	%r15, %r15
	je	.LBB18_3
# BB#6:                                 #   in Loop: Header=BB18_4 Depth=1
	movl	$.L.str.60, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
	movzbl	cli_ac_maxdepth(%rip), %edx
	movzbl	cli_ac_mindepth(%rip), %esi
	movq	%r15, %rdi
	callq	cli_ac_init
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB18_11
# BB#7:                                 #   in Loop: Header=BB18_4 Depth=1
	cmpb	$0, 2(%r15)
	jne	.LBB18_9
# BB#8:                                 #   in Loop: Header=BB18_4 Depth=1
	movl	$.L.str.62, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	cli_bm_init
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB18_15
	.p2align	4, 0x90
.LBB18_9:                               #   in Loop: Header=BB18_4 Depth=1
	incq	%rbx
	cmpq	$7, %rbx
	jl	.LBB18_4
	jmp	.LBB18_17
	.p2align	4, 0x90
.LBB18_1:                               # %.split
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	cmpq	$0, (%rax,%rbx,8)
	jne	.LBB18_16
# BB#2:                                 #   in Loop: Header=BB18_1 Depth=1
	movl	$.L.str.57, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
	movl	$1, %edi
	movl	$80, %esi
	callq	cli_calloc
	movq	%rax, %r15
	movq	16(%r14), %rax
	movq	%r15, (%rax,%rbx,8)
	testq	%r15, %r15
	je	.LBB18_3
# BB#10:                                #   in Loop: Header=BB18_1 Depth=1
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, 2(%r15)
	movl	$.L.str.60, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
	movzbl	cli_ac_maxdepth(%rip), %edx
	movzbl	cli_ac_mindepth(%rip), %esi
	movq	%r15, %rdi
	callq	cli_ac_init
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB18_11
# BB#13:                                #   in Loop: Header=BB18_1 Depth=1
	cmpb	$0, 2(%r15)
	jne	.LBB18_16
# BB#14:                                #   in Loop: Header=BB18_1 Depth=1
	movl	$.L.str.62, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	cli_bm_init
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB18_15
	.p2align	4, 0x90
.LBB18_16:                              #   in Loop: Header=BB18_1 Depth=1
	incq	%rbx
	cmpq	$7, %rbx
	jl	.LBB18_1
.LBB18_17:
	xorl	%ebp, %ebp
	jmp	.LBB18_18
.LBB18_3:                               # %.us-lcssa.us
	movl	$.L.str.58, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-114, %ebp
	jmp	.LBB18_18
.LBB18_11:                              # %.us-lcssa27.us
	movl	$.L.str.61, %edi
	jmp	.LBB18_12
.LBB18_15:                              # %.us-lcssa28.us
	movl	$.L.str.63, %edi
.LBB18_12:                              # %.loopexit
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB18_18:                              # %.loopexit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	cli_initroots, .Lfunc_end18-cli_initroots
	.cfi_endproc

	.p2align	4, 0x90
	.type	scomp,@function
scomp:                                  # @scomp
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	subl	(%rsi), %eax
	retq
.Lfunc_end19:
	.size	scomp, .Lfunc_end19-scomp
	.cfi_endproc

	.p2align	4, 0x90
	.type	dirent_compare,@function
dirent_compare:                         # @dirent_compare
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi178:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi179:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi180:
	.cfi_def_cfa_offset 32
.Lcfi181:
	.cfi_offset %rbx, -24
.Lcfi182:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	19(%rbx), %rdi
	leaq	19(%r14), %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB20_5
# BB#1:
	movb	18(%r14), %cl
	movl	$-1, %eax
	cmpb	%cl, 18(%rbx)
	jb	.LBB20_5
# BB#2:
	movl	$1, %eax
	ja	.LBB20_5
# BB#3:
	movzwl	16(%rbx), %ecx
	movzwl	16(%r14), %edx
	cmpw	%dx, %cx
	movl	$-1, %eax
	jb	.LBB20_5
# BB#4:
	cmpw	%cx, %dx
	sbbl	%eax, %eax
	andl	$1, %eax
.LBB20_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end20:
	.size	dirent_compare, .Lfunc_end20-dirent_compare
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"cli_parse_add(): Problem adding signature (1).\n"
	.size	.L.str, 48

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"-"
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"*"
	.size	.L.str.2, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Can't extract part %d of partial signature.\n"
	.size	.L.str.3, 45

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"cli_parse_add(): Problem adding signature (2).\n"
	.size	.L.str.4, 48

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"?("
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"cli_parse_add(): Problem adding signature (3).\n"
	.size	.L.str.6, 48

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"(Clam)"
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"cli_parse_add(): Problem adding signature (4).\n"
	.size	.L.str.8, 48

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Initializing the engine (devel-20071218)\n"
	.size	.L.str.9, 42

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Can't allocate memory for the engine structure!\n"
	.size	.L.str.10, 49

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Can't allocate memory for roots!\n"
	.size	.L.str.11, 34

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Can't initialize dynamic configuration\n"
	.size	.L.str.12, 40

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"cl_loaddbdir(): Can't get status of %s\n"
	.size	.L.str.13, 40

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"cl_load(%s): Not supported database file type\n"
	.size	.L.str.14, 47

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"/usr/local/share/clamav"
	.size	.L.str.15, 24

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"cl_statdbdir(): Null argument passed.\n"
	.size	.L.str.16, 39

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"cl_statdbdir(): Can't open directory %s\n"
	.size	.L.str.17, 41

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Stat()ing files in %s\n"
	.size	.L.str.18, 23

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"."
	.size	.L.str.19, 2

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	".."
	.size	.L.str.20, 3

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	".db"
	.size	.L.str.21, 4

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	".db2"
	.size	.L.str.22, 5

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	".db3"
	.size	.L.str.23, 5

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	".hdb"
	.size	.L.str.24, 5

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	".hdu"
	.size	.L.str.25, 5

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	".fp"
	.size	.L.str.26, 4

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	".mdb"
	.size	.L.str.27, 5

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	".mdu"
	.size	.L.str.28, 5

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	".ndb"
	.size	.L.str.29, 5

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	".ndu"
	.size	.L.str.30, 5

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	".sdb"
	.size	.L.str.31, 5

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	".zmd"
	.size	.L.str.32, 5

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	".rmd"
	.size	.L.str.33, 5

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	".pdb"
	.size	.L.str.34, 5

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	".wdb"
	.size	.L.str.35, 5

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	".inc"
	.size	.L.str.36, 5

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	".cvd"
	.size	.L.str.37, 5

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"%s/%s/%s.info"
	.size	.L.str.38, 14

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"daily"
	.size	.L.str.39, 6

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"main"
	.size	.L.str.40, 5

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"%s/%s"
	.size	.L.str.41, 6

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"cl_statfree(): Null argument passed\n"
	.size	.L.str.42, 37

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"cl_free: engine == NULL\n"
	.size	.L.str.43, 25

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"cl_dup: engine == NULL\n"
	.size	.L.str.44, 24

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"rb"
	.size	.L.str.45, 3

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"cli_load(): Can't open file %s\n"
	.size	.L.str.46, 32

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"daily.cvd"
	.size	.L.str.47, 10

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	".cfg"
	.size	.L.str.48, 5

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"cli_load: unknown extension - assuming old database format\n"
	.size	.L.str.49, 60

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"Can't load %s: %s\n"
	.size	.L.str.50, 19

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"%s skipped\n"
	.size	.L.str.51, 12

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"%s loaded\n"
	.size	.L.str.52, 11

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"Malformed pattern line %d\n"
	.size	.L.str.53, 27

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"Problem parsing signature at line %d\n"
	.size	.L.str.54, 38

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"Empty database file\n"
	.size	.L.str.55, 21

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"Problem parsing database at line %d\n"
	.size	.L.str.56, 37

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"Initializing engine->root[%d]\n"
	.size	.L.str.57, 31

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"cli_initroots: Can't allocate memory for cli_matcher\n"
	.size	.L.str.58, 54

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"cli_initroots: Only using AC pattern matcher.\n"
	.size	.L.str.59, 47

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"Initialising AC pattern matcher of root[%d]\n"
	.size	.L.str.60, 45

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"cli_initroots: Can't initialise AC pattern matcher\n"
	.size	.L.str.61, 52

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"cli_initroots: Initializing BM tables of root[%d]\n"
	.size	.L.str.62, 51

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"cli_initroots: Can't initialise BM pattern matcher\n"
	.size	.L.str.63, 52

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	":"
	.size	.L.str.64, 2

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"cli_loadmd5: Malformed MD5 string at line %u\n"
	.size	.L.str.65, 46

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"cli_loadmd5: Can't initialise BM pattern matcher\n"
	.size	.L.str.66, 50

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"cli_loadmd5: Can't allocate memory for bm_new\n"
	.size	.L.str.67, 47

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"cli_loadmd5: Can't realloc md5_sect->soff\n"
	.size	.L.str.68, 43

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"cli_loadmd5: Error adding BM pattern\n"
	.size	.L.str.69, 38

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"cli_loadmd5: Initializing MD5 list structure\n"
	.size	.L.str.70, 46

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"cli_loadmd5: Empty database file\n"
	.size	.L.str.71, 34

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"cli_loadmd5: Problem parsing database at line %u\n"
	.size	.L.str.72, 50

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"Exploit.JPEG.Comment"
	.size	.L.str.73, 21

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"HTML.Phishing"
	.size	.L.str.74, 14

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"Email.Phishing"
	.size	.L.str.75, 15

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"Signature for %s not loaded (required f-level: %d)\n"
	.size	.L.str.76, 52

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"Not supported target type in signature for %s\n"
	.size	.L.str.77, 47

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"*** Self protection mechanism activated.\n"
	.size	.L.str.78, 42

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"cli_loaddbdir: Acquiring dbdir lock\n"
	.size	.L.str.79, 37

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"cl_load(): Unable to lock database directory: %s\n"
	.size	.L.str.80, 50

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"Loading databases from %s\n"
	.size	.L.str.81, 27

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"%s/daily.cfg"
	.size	.L.str.82, 13

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"cli_loaddbdir(): Can't open directory %s\n"
	.size	.L.str.83, 42

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"cli_loaddbdir(): Too many files, increase MAX_DIRENTS\n"
	.size	.L.str.84, 55

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"cli_loaddbdir(): dbfile == NULL\n"
	.size	.L.str.85, 33

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"cli_loaddbdir(): error loading database %s\n"
	.size	.L.str.86, 44

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"cli_loaddb(): No supported database files found in %s\n"
	.size	.L.str.87, 55


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
