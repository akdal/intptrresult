	.text
	.file	"Towers.bc"
	.globl	Initrand
	.p2align	4, 0x90
	.type	Initrand,@function
Initrand:                               # @Initrand
	.cfi_startproc
# BB#0:
	movq	$74755, seed(%rip)      # imm = 0x12403
	retq
.Lfunc_end0:
	.size	Initrand, .Lfunc_end0-Initrand
	.cfi_endproc

	.globl	Rand
	.p2align	4, 0x90
	.type	Rand,@function
Rand:                                   # @Rand
	.cfi_startproc
# BB#0:
	imull	$1309, seed(%rip), %eax # imm = 0x51D
	addl	$13849, %eax            # imm = 0x3619
	movzwl	%ax, %eax
	movq	%rax, seed(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end1:
	.size	Rand, .Lfunc_end1-Rand
	.cfi_endproc

	.globl	Error
	.p2align	4, 0x90
	.type	Error,@function
Error:                                  # @Error
	.cfi_startproc
# BB#0:
	movq	%rdi, %rcx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	jmp	printf                  # TAILCALL
.Lfunc_end2:
	.size	Error, .Lfunc_end2-Error
	.cfi_endproc

	.globl	Makenull
	.p2align	4, 0x90
	.type	Makenull,@function
Makenull:                               # @Makenull
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	movl	$0, stack(,%rax,4)
	retq
.Lfunc_end3:
	.size	Makenull, .Lfunc_end3-Makenull
	.cfi_endproc

	.globl	Getelement
	.p2align	4, 0x90
	.type	Getelement,@function
Getelement:                             # @Getelement
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movslq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jle	.LBB4_2
# BB#1:
	movl	cellspace+4(,%rbx,8), %eax
	movl	%eax, freelist(%rip)
	jmp	.LBB4_3
.LBB4_2:
	xorl	%ebx, %ebx
	movl	$.L.str, %edi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	printf
.LBB4_3:
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	Getelement, .Lfunc_end4-Getelement
	.cfi_endproc

	.globl	Push
	.p2align	4, 0x90
	.type	Push,@function
Push:                                   # @Push
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movslq	%esi, %rbx
	movslq	stack(,%rbx,4), %rax
	testq	%rax, %rax
	jle	.LBB5_2
# BB#1:
	cmpl	%r14d, cellspace(,%rax,8)
	jle	.LBB5_6
.LBB5_2:
	movslq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jle	.LBB5_4
# BB#3:
	movl	cellspace+4(,%rbp,8), %ecx
	movl	%ecx, freelist(%rip)
	jmp	.LBB5_5
.LBB5_6:                                # %.critedge
	movl	$.L.str, %edi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	printf                  # TAILCALL
.LBB5_4:
	xorl	%ebp, %ebp
	movl	$.L.str, %edi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	printf
	movl	stack(,%rbx,4), %eax
.LBB5_5:
	movslq	%ebp, %rcx
	movl	%eax, cellspace+4(,%rcx,8)
	movl	%ecx, stack(,%rbx,4)
	movl	%r14d, cellspace(,%rcx,8)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	Push, .Lfunc_end5-Push
	.cfi_endproc

	.globl	Init
	.p2align	4, 0x90
	.type	Init,@function
Init:                                   # @Init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movslq	%edi, %r14
	movl	$0, stack(,%r14,4)
	testl	%ebp, %ebp
	jle	.LBB6_11
# BB#1:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	testl	%eax, %eax
	jg	.LBB6_3
	jmp	.LBB6_5
	.p2align	4, 0x90
.LBB6_10:                               # %Push.exit..lr.ph_crit_edge
	decl	%ebp
	movl	stack(,%r14,4), %eax
	testl	%eax, %eax
	jle	.LBB6_5
.LBB6_3:
	movslq	%eax, %rcx
	cmpl	%ebp, cellspace(,%rcx,8)
	jle	.LBB6_4
.LBB6_5:
	movslq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jle	.LBB6_7
# BB#6:
	movl	cellspace+4(,%rbx,8), %ecx
	movl	%ecx, freelist(%rip)
	jmp	.LBB6_8
	.p2align	4, 0x90
.LBB6_4:                                # %.critedge.i
	movl	$.L.str, %edi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$2, %ebp
	jge	.LBB6_10
	jmp	.LBB6_11
	.p2align	4, 0x90
.LBB6_7:
	xorl	%ebx, %ebx
	movl	$.L.str, %edi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	printf
	movl	stack(,%r14,4), %eax
.LBB6_8:                                # %Getelement.exit.i
	movslq	%ebx, %rcx
	movl	%eax, cellspace+4(,%rcx,8)
	movl	%ecx, stack(,%r14,4)
	movl	%ebp, cellspace(,%rcx,8)
	cmpl	$2, %ebp
	jge	.LBB6_10
.LBB6_11:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end6:
	.size	Init, .Lfunc_end6-Init
	.cfi_endproc

	.globl	Pop
	.p2align	4, 0x90
	.type	Pop,@function
Pop:                                    # @Pop
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movslq	%edi, %rax
	movslq	stack(,%rax,4), %rcx
	testq	%rcx, %rcx
	jle	.LBB7_2
# BB#1:
	movl	cellspace(,%rcx,8), %ebx
	movl	cellspace+4(,%rcx,8), %edx
	movl	freelist(%rip), %esi
	movl	%esi, cellspace+4(,%rcx,8)
	movl	%ecx, freelist(%rip)
	movl	%edx, stack(,%rax,4)
	jmp	.LBB7_3
.LBB7_2:
	xorl	%ebx, %ebx
	movl	$.L.str, %edi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	printf
.LBB7_3:
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	Pop, .Lfunc_end7-Pop
	.cfi_endproc

	.globl	Move
	.p2align	4, 0x90
	.type	Move,@function
Move:                                   # @Move
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movslq	%edi, %rax
	movslq	stack(,%rax,4), %rcx
	testq	%rcx, %rcx
	jle	.LBB8_2
# BB#1:
	movl	cellspace(,%rcx,8), %r14d
	movl	cellspace+4(,%rcx,8), %edx
	movl	freelist(%rip), %esi
	movl	%esi, cellspace+4(,%rcx,8)
	movl	%ecx, freelist(%rip)
	movl	%edx, stack(,%rax,4)
	jmp	.LBB8_3
.LBB8_2:
	xorl	%r14d, %r14d
	movl	$.L.str, %edi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	printf
.LBB8_3:                                # %Pop.exit
	movslq	%ebx, %rbx
	movslq	stack(,%rbx,4), %rax
	testq	%rax, %rax
	jle	.LBB8_6
# BB#4:
	cmpl	%r14d, cellspace(,%rax,8)
	jle	.LBB8_5
.LBB8_6:
	movslq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jle	.LBB8_8
# BB#7:
	movl	cellspace+4(,%rbp,8), %ecx
	movl	%ecx, freelist(%rip)
	jmp	.LBB8_9
.LBB8_5:                                # %.critedge.i
	movl	$.L.str, %edi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB8_10
.LBB8_8:
	xorl	%ebp, %ebp
	movl	$.L.str, %edi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	printf
	movl	stack(,%rbx,4), %eax
.LBB8_9:                                # %Getelement.exit.i
	movslq	%ebp, %rcx
	movl	%eax, cellspace+4(,%rcx,8)
	movl	%ecx, stack(,%rbx,4)
	movl	%r14d, cellspace(,%rcx,8)
.LBB8_10:                               # %Push.exit
	incl	movesdone(%rip)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	Move, .Lfunc_end8-Move
	.cfi_endproc

	.globl	tower
	.p2align	4, 0x90
	.type	tower,@function
tower:                                  # @tower
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %r14d
	movl	%edi, %r15d
	cmpl	$1, %ebp
	jne	.LBB9_3
# BB#1:
	movl	%r15d, %ebx
	jmp	.LBB9_2
.LBB9_3:                                # %tailrecurse.preheader
	decl	%ebp
	.p2align	4, 0x90
.LBB9_4:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movl	$6, %ebx
	subl	%r15d, %ebx
	subl	%r14d, %ebx
	movl	%r15d, %edi
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	tower
	movl	%r15d, %edi
	movl	%r14d, %esi
	callq	Move
	decl	%ebp
	movl	%ebx, %r15d
	jne	.LBB9_4
.LBB9_2:                                # %tailrecurse._crit_edge
	movl	%ebx, %edi
	movl	%r14d, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	Move                    # TAILCALL
.Lfunc_end9:
	.size	tower, .Lfunc_end9-tower
	.cfi_endproc

	.globl	Towers
	.p2align	4, 0x90
	.type	Towers,@function
Towers:                                 # @Towers
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	$0, cellspace+12(%rip)
	movl	$1, cellspace+20(%rip)
	movl	$2, cellspace+28(%rip)
	movl	$3, cellspace+36(%rip)
	movl	$4, cellspace+44(%rip)
	movl	$5, cellspace+52(%rip)
	movl	$6, cellspace+60(%rip)
	movl	$7, cellspace+68(%rip)
	movl	$8, cellspace+76(%rip)
	movl	$9, cellspace+84(%rip)
	movl	$10, cellspace+92(%rip)
	movl	$11, cellspace+100(%rip)
	movl	$12, cellspace+108(%rip)
	movl	$13, cellspace+116(%rip)
	movl	$14, cellspace+124(%rip)
	movl	$15, cellspace+132(%rip)
	movl	$16, cellspace+140(%rip)
	movl	$17, cellspace+148(%rip)
	movl	$18, freelist(%rip)
	movl	$0, stack+4(%rip)
	movl	$14, %ebp
	xorl	%eax, %eax
	testl	%eax, %eax
	jg	.LBB10_2
	jmp	.LBB10_4
	.p2align	4, 0x90
.LBB10_9:                               # %Push.exit..lr.ph_crit_edge.i
	decl	%ebp
	movl	stack+4(%rip), %eax
	testl	%eax, %eax
	jle	.LBB10_4
.LBB10_2:
	movslq	%eax, %rcx
	cmpl	%ebp, cellspace(,%rcx,8)
	jle	.LBB10_3
.LBB10_4:
	movslq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jle	.LBB10_6
# BB#5:
	movl	cellspace+4(,%rbx,8), %ecx
	movl	%ecx, freelist(%rip)
	jmp	.LBB10_7
	.p2align	4, 0x90
.LBB10_3:                               # %.critedge.i.i
	movl	$.L.str, %edi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	printf
	cmpl	$2, %ebp
	jge	.LBB10_9
	jmp	.LBB10_10
	.p2align	4, 0x90
.LBB10_6:
	xorl	%ebx, %ebx
	movl	$.L.str, %edi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	printf
	movl	stack+4(%rip), %eax
.LBB10_7:                               # %Getelement.exit.i.i
	movslq	%ebx, %rcx
	movl	%eax, cellspace+4(,%rcx,8)
	movl	%ecx, stack+4(%rip)
	movl	%ebp, cellspace(,%rcx,8)
	cmpl	$2, %ebp
	jge	.LBB10_9
.LBB10_10:                              # %Init.exit
	movl	$0, stack+8(%rip)
	movl	$0, stack+12(%rip)
	movl	$0, movesdone(%rip)
	movl	$1, %edi
	movl	$2, %esi
	movl	$14, %edx
	callq	tower
	movl	$16383, %esi            # imm = 0x3FFF
	cmpl	$16383, movesdone(%rip) # imm = 0x3FFF
	je	.LBB10_12
# BB#11:
	movl	$.Lstr, %edi
	callq	puts
	movl	movesdone(%rip), %esi
.LBB10_12:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	printf                  # TAILCALL
.Lfunc_end10:
	.size	Towers, .Lfunc_end10-Towers
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 16
.Lcfi37:
	.cfi_offset %rbx, -16
	movl	$100, %ebx
	.p2align	4, 0x90
.LBB11_1:                               # =>This Inner Loop Header: Depth=1
	callq	Towers
	decl	%ebx
	jne	.LBB11_1
# BB#2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end11:
	.size	main, .Lfunc_end11-main
	.cfi_endproc

	.type	seed,@object            # @seed
	.comm	seed,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" Error in Towers: %s\n"
	.size	.L.str, 22

	.type	stack,@object           # @stack
	.comm	stack,16,16
	.type	freelist,@object        # @freelist
	.comm	freelist,4,4
	.type	cellspace,@object       # @cellspace
	.comm	cellspace,152,16
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"out of space   "
	.size	.L.str.1, 16

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"disc size error"
	.size	.L.str.2, 16

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"nothing to pop "
	.size	.L.str.3, 16

	.type	movesdone,@object       # @movesdone
	.comm	movesdone,4,4
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%d\n"
	.size	.L.str.5, 4

	.type	value,@object           # @value
	.comm	value,4,4
	.type	fixed,@object           # @fixed
	.comm	fixed,4,4
	.type	floated,@object         # @floated
	.comm	floated,4,4
	.type	permarray,@object       # @permarray
	.comm	permarray,44,16
	.type	pctr,@object            # @pctr
	.comm	pctr,4,4
	.type	tree,@object            # @tree
	.comm	tree,8,8
	.type	ima,@object             # @ima
	.comm	ima,6724,16
	.type	imb,@object             # @imb
	.comm	imb,6724,16
	.type	imr,@object             # @imr
	.comm	imr,6724,16
	.type	rma,@object             # @rma
	.comm	rma,6724,16
	.type	rmb,@object             # @rmb
	.comm	rmb,6724,16
	.type	rmr,@object             # @rmr
	.comm	rmr,6724,16
	.type	piececount,@object      # @piececount
	.comm	piececount,16,16
	.type	class,@object           # @class
	.comm	class,52,16
	.type	piecemax,@object        # @piecemax
	.comm	piecemax,52,16
	.type	puzzl,@object           # @puzzl
	.comm	puzzl,2048,16
	.type	p,@object               # @p
	.comm	p,26624,16
	.type	n,@object               # @n
	.comm	n,4,4
	.type	kount,@object           # @kount
	.comm	kount,4,4
	.type	sortlist,@object        # @sortlist
	.comm	sortlist,20004,16
	.type	biggest,@object         # @biggest
	.comm	biggest,4,4
	.type	littlest,@object        # @littlest
	.comm	littlest,4,4
	.type	top,@object             # @top
	.comm	top,4,4
	.type	z,@object               # @z
	.comm	z,2056,16
	.type	w,@object               # @w
	.comm	w,2056,16
	.type	e,@object               # @e
	.comm	e,1040,16
	.type	zr,@object              # @zr
	.comm	zr,4,4
	.type	zi,@object              # @zi
	.comm	zi,4,4
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	" Error in Towers."
	.size	.Lstr, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
