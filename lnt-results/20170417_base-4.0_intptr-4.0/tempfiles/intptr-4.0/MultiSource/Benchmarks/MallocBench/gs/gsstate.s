	.text
	.file	"gsstate.bc"
	.globl	gs_state_alloc
	.p2align	4, 0x90
	.type	gs_state_alloc,@function
gs_state_alloc:                         # @gs_state_alloc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$1, %edi
	movl	$464, %esi              # imm = 0x1D0
	movl	$.L.str, %edx
	callq	*%r15
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_16
# BB#1:
	movl	$1, %edi
	movl	$144, %esi
	movl	$.L.str.4, %edx
	callq	*%r15
	testq	%rax, %rax
	je	.LBB0_16
# BB#2:
	movq	%rax, 256(%rbx)
	movl	$1, %edi
	movl	$144, %esi
	movl	$.L.str.4, %edx
	callq	*%r15
	testq	%rax, %rax
	je	.LBB0_16
# BB#3:
	movq	%rax, 264(%rbx)
	movl	$1, %edi
	movl	$56, %esi
	movl	$.L.str.4, %edx
	callq	*%r15
	testq	%rax, %rax
	je	.LBB0_16
# BB#4:
	movq	%rax, 280(%rbx)
	movl	$1, %edi
	movl	$32, %esi
	movl	$.L.str.4, %edx
	callq	*%r15
	testq	%rax, %rax
	je	.LBB0_16
# BB#5:
	movq	%rax, 288(%rbx)
	movl	$1, %edi
	movl	$10, %esi
	movl	$.L.str.4, %edx
	callq	*%r15
	testq	%rax, %rax
	je	.LBB0_16
# BB#6:
	movq	%rax, 304(%rbx)
	movl	$1, %edi
	movl	$32, %esi
	movl	$.L.str.4, %edx
	callq	*%r15
	testq	%rax, %rax
	je	.LBB0_16
# BB#7:
	movq	%rax, 312(%rbx)
	movl	$1, %edi
	movl	$32, %esi
	movl	$.L.str.4, %edx
	callq	*%r15
	testq	%rax, %rax
	je	.LBB0_16
# BB#8:
	movq	%rax, 448(%rbx)
	movl	$0, 456(%rbx)
	movq	$0, (%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	movq	256(%rbx), %rax
	movq	$0, 88(%rax)
	movq	264(%rbx), %rax
	movq	$0, 88(%rax)
	movq	288(%rbx), %rax
	movl	$0, 24(%rax)
	movl	$0, 12(%rax)
	movl	$0, 8(%rax)
	movq	%rbx, %rdi
	callq	gs_nulldevice
	movl	$1065353216, 440(%rbx)  # imm = 0x3F800000
	movw	$0, 436(%rbx)
	movq	%rbx, %rdi
	callq	gs_initgraphics
	xorl	%ecx, %ecx
	testl	%eax, %eax
	cmovsq	%rcx, %rbx
	jmp	.LBB0_17
.LBB0_16:
	xorl	%ebx, %ebx
.LBB0_17:                               # %alloc_state_contents.exit.thread
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	gs_state_alloc, .Lfunc_end0-gs_state_alloc
	.cfi_endproc

	.globl	alloc_state_contents
	.p2align	4, 0x90
	.type	alloc_state_contents,@function
alloc_state_contents:                   # @alloc_state_contents
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movl	$1, %edi
	movl	$144, %esi
	movl	$.L.str.4, %edx
	callq	*%rbp
	movl	$-1, %r14d
	testq	%rax, %rax
	je	.LBB1_8
# BB#1:
	movq	%rax, 256(%rbx)
	movl	$1, %edi
	movl	$144, %esi
	movl	$.L.str.4, %edx
	callq	*%rbp
	testq	%rax, %rax
	je	.LBB1_8
# BB#2:
	movq	%rax, 264(%rbx)
	movl	$1, %edi
	movl	$56, %esi
	movl	$.L.str.4, %edx
	callq	*%rbp
	testq	%rax, %rax
	je	.LBB1_8
# BB#3:
	movq	%rax, 280(%rbx)
	movl	$1, %edi
	movl	$32, %esi
	movl	$.L.str.4, %edx
	callq	*%rbp
	testq	%rax, %rax
	je	.LBB1_8
# BB#4:
	movq	%rax, 288(%rbx)
	movl	$1, %edi
	movl	$10, %esi
	movl	$.L.str.4, %edx
	callq	*%rbp
	testq	%rax, %rax
	je	.LBB1_8
# BB#5:
	movq	%rax, 304(%rbx)
	movl	$1, %edi
	movl	$32, %esi
	movl	$.L.str.4, %edx
	callq	*%rbp
	testq	%rax, %rax
	je	.LBB1_8
# BB#6:
	movq	%rax, 312(%rbx)
	movl	$1, %edi
	movl	$32, %esi
	movl	$.L.str.4, %edx
	callq	*%rbp
	testq	%rax, %rax
	je	.LBB1_8
# BB#7:
	movq	%rax, 448(%rbx)
	movl	$0, 456(%rbx)
	xorl	%r14d, %r14d
.LBB1_8:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	alloc_state_contents, .Lfunc_end1-alloc_state_contents
	.cfi_endproc

	.globl	gs_setflat
	.p2align	4, 0x90
	.type	gs_setflat,@function
gs_setflat:                             # @gs_setflat
	.cfi_startproc
# BB#0:
	movl	$-15, %eax
	xorps	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jae	.LBB2_2
# BB#1:
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 440(%rdi)
	xorl	%eax, %eax
.LBB2_2:
	retq
.Lfunc_end2:
	.size	gs_setflat, .Lfunc_end2-gs_setflat
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4607182418800017408     # double 1
.LCPI3_1:
	.quad	4621819117588971520     # double 10
	.text
	.globl	gs_initgraphics
	.p2align	4, 0x90
	.type	gs_initgraphics,@function
gs_initgraphics:                        # @gs_initgraphics
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%eax, %eax
	callq	gs_initmatrix
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	gs_newpath
	testl	%eax, %eax
	js	.LBB3_8
# BB#1:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	gs_initclip
	testl	%eax, %eax
	js	.LBB3_8
# BB#2:
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%rbx, %rdi
	callq	gs_setlinewidth
	testl	%eax, %eax
	js	.LBB3_8
# BB#3:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	gs_setlinecap
	testl	%eax, %eax
	js	.LBB3_8
# BB#4:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	gs_setlinejoin
	testl	%eax, %eax
	js	.LBB3_8
# BB#5:
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorps	%xmm0, %xmm0
	movq	%rbx, %rdi
	callq	gs_setdash
	testl	%eax, %eax
	js	.LBB3_8
# BB#6:
	xorps	%xmm0, %xmm0
	movq	%rbx, %rdi
	callq	gs_setgray
	testl	%eax, %eax
	js	.LBB3_8
# BB#7:
	movsd	.LCPI3_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%rbx, %rdi
	callq	gs_setmiterlimit
	movl	%eax, %ecx
	sarl	$31, %ecx
	andl	%eax, %ecx
	movl	%ecx, %eax
.LBB3_8:
	popq	%rbx
	retq
.Lfunc_end3:
	.size	gs_initgraphics, .Lfunc_end3-gs_initgraphics
	.cfi_endproc

	.globl	gs_state_free
	.p2align	4, 0x90
	.type	gs_state_free,@function
gs_state_free:                          # @gs_state_free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	free_state_contents
	movl	$1, %esi
	movl	$464, %edx              # imm = 0x1D0
	movl	$.L.str.1, %ecx
	movq	%rbx, %rdi
	callq	*16(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	gs_state_free, .Lfunc_end4-gs_state_free
	.cfi_endproc

	.globl	free_state_contents
	.p2align	4, 0x90
	.type	free_state_contents,@function
free_state_contents:                    # @free_state_contents
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %r14
	movq	264(%rbx), %rdi
	callq	gx_path_release
	movq	256(%rbx), %rdi
	callq	gx_path_release
	cmpl	$0, 456(%rbx)
	jne	.LBB5_2
# BB#1:
	movq	448(%rbx), %rdi
	movl	$1, %esi
	movl	$32, %edx
	movl	$.L.str.5, %ecx
	movq	%r14, %rax
	callq	*%rax
.LBB5_2:
	movq	312(%rbx), %rdi
	movl	$1, %esi
	movl	$32, %edx
	movl	$.L.str.5, %ecx
	callq	*%r14
	movq	304(%rbx), %rdi
	movl	$1, %esi
	movl	$10, %edx
	movl	$.L.str.5, %ecx
	callq	*%r14
	movq	288(%rbx), %rdi
	movl	$1, %esi
	movl	$32, %edx
	movl	$.L.str.5, %ecx
	callq	*%r14
	movq	280(%rbx), %rdi
	movl	$1, %esi
	movl	$56, %edx
	movl	$.L.str.5, %ecx
	callq	*%r14
	movq	264(%rbx), %rdi
	movl	$1, %esi
	movl	$144, %edx
	movl	$.L.str.5, %ecx
	callq	*%r14
	movq	256(%rbx), %rdi
	movl	$1, %esi
	movl	$144, %edx
	movl	$.L.str.5, %ecx
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*%rax                   # TAILCALL
.Lfunc_end5:
	.size	free_state_contents, .Lfunc_end5-free_state_contents
	.cfi_endproc

	.globl	gs_gsave
	.p2align	4, 0x90
	.type	gs_gsave,@function
gs_gsave:                               # @gs_gsave
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 48
.Lcfi26:
	.cfi_offset %rbx, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	movl	$464, %esi              # imm = 0x1D0
	movl	$.L.str.2, %edx
	callq	*8(%rbx)
	movq	%rax, %r14
	movl	$-25, %r15d
	testq	%r14, %r14
	je	.LBB6_9
# BB#1:
	movl	$464, %edx              # imm = 0x1D0
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	memcpy
	movq	8(%rbx), %rbp
	movl	$1, %edi
	movl	$144, %esi
	movl	$.L.str.4, %edx
	callq	*%rbp
	testq	%rax, %rax
	je	.LBB6_9
# BB#2:
	movq	%rax, 256(%rbx)
	movl	$1, %edi
	movl	$144, %esi
	movl	$.L.str.4, %edx
	callq	*%rbp
	testq	%rax, %rax
	je	.LBB6_9
# BB#3:
	movq	%rax, 264(%rbx)
	movl	$1, %edi
	movl	$56, %esi
	movl	$.L.str.4, %edx
	callq	*%rbp
	testq	%rax, %rax
	je	.LBB6_9
# BB#4:
	movq	%rax, 280(%rbx)
	movl	$1, %edi
	movl	$32, %esi
	movl	$.L.str.4, %edx
	callq	*%rbp
	testq	%rax, %rax
	je	.LBB6_9
# BB#5:
	movq	%rax, 288(%rbx)
	movl	$1, %edi
	movl	$10, %esi
	movl	$.L.str.4, %edx
	callq	*%rbp
	testq	%rax, %rax
	je	.LBB6_9
# BB#6:
	movq	%rax, 304(%rbx)
	movl	$1, %edi
	movl	$32, %esi
	movl	$.L.str.4, %edx
	callq	*%rbp
	testq	%rax, %rax
	je	.LBB6_9
# BB#7:
	movq	%rax, 312(%rbx)
	movl	$1, %edi
	movl	$32, %esi
	movl	$.L.str.4, %edx
	callq	*%rbp
	testq	%rax, %rax
	je	.LBB6_9
# BB#8:
	movq	%rax, 448(%rbx)
	movl	$0, 456(%rbx)
	movq	256(%rbx), %rdi
	movq	256(%r14), %rsi
	movl	$144, %edx
	callq	memcpy
	movq	264(%rbx), %rdi
	movq	264(%r14), %rsi
	movl	$144, %edx
	callq	memcpy
	movq	280(%rbx), %rax
	movq	280(%r14), %rcx
	movq	48(%rcx), %rdx
	movq	%rdx, 48(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	288(%rbx), %rax
	movq	288(%r14), %rcx
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	304(%rbx), %rax
	movq	304(%r14), %rcx
	movzwl	8(%rcx), %edx
	movw	%dx, 8(%rax)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	312(%rbx), %rax
	movq	312(%r14), %rcx
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	448(%rbx), %rax
	movq	448(%r14), %rcx
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	256(%rbx), %rdi
	callq	gx_path_share
	movq	264(%rbx), %rdi
	callq	gx_path_share
	movq	%r14, (%rbx)
	xorl	%r15d, %r15d
.LBB6_9:                                # %alloc_state_contents.exit.thread
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	gs_gsave, .Lfunc_end6-gs_gsave
	.cfi_endproc

	.globl	gs_grestore
	.p2align	4, 0x90
	.type	gs_grestore,@function
gs_grestore:                            # @gs_grestore
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -24
.Lcfi34:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.LBB7_1
# BB#2:
	movq	%rbx, %rdi
	callq	free_state_contents
	movl	$464, %edx              # imm = 0x1D0
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	memcpy
	movl	$1, %esi
	movl	$464, %edx              # imm = 0x1D0
	movl	$.L.str.3, %ecx
	movq	%r14, %rdi
	callq	*16(%rbx)
	xorl	%eax, %eax
	jmp	.LBB7_3
.LBB7_1:
	movl	$-23, %eax
.LBB7_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	gs_grestore, .Lfunc_end7-gs_grestore
	.cfi_endproc

	.globl	gs_grestoreall
	.p2align	4, 0x90
	.type	gs_grestoreall,@function
gs_grestoreall:                         # @gs_grestoreall
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	jmp	.LBB8_2
	.p2align	4, 0x90
.LBB8_1:                                # %gs_grestore.exit.thread
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	%r14, %rdi
	callq	free_state_contents
	movl	$464, %edx              # imm = 0x1D0
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	memcpy
	movl	$1, %esi
	movl	$464, %edx              # imm = 0x1D0
	movl	$.L.str.3, %ecx
	movq	%rbx, %rdi
	callq	*16(%r14)
.LBB8_2:                                # %gs_grestore.exit.thread
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_1
# BB#3:                                 # %gs_grestore.exit
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	gs_grestoreall, .Lfunc_end8-gs_grestoreall
	.cfi_endproc

	.globl	gs_state_swap_saved
	.p2align	4, 0x90
	.type	gs_state_swap_saved,@function
gs_state_swap_saved:                    # @gs_state_swap_saved
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	%rsi, (%rdi)
	retq
.Lfunc_end9:
	.size	gs_state_swap_saved, .Lfunc_end9-gs_state_swap_saved
	.cfi_endproc

	.globl	gs_currentflat
	.p2align	4, 0x90
	.type	gs_currentflat,@function
gs_currentflat:                         # @gs_currentflat
	.cfi_startproc
# BB#0:
	movss	440(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end10:
	.size	gs_currentflat, .Lfunc_end10-gs_currentflat
	.cfi_endproc

	.type	gs_state_sizeof,@object # @gs_state_sizeof
	.data
	.globl	gs_state_sizeof
	.p2align	2
gs_state_sizeof:
	.long	464                     # 0x1d0
	.size	gs_state_sizeof, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"gs_state_alloc"
	.size	.L.str, 15

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"gs_state_free"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"gs_gsave"
	.size	.L.str.2, 9

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"gs_grestore"
	.size	.L.str.3, 12

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"(gs)alloc_state_contents"
	.size	.L.str.4, 25

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"(gs)free_state_contents"
	.size	.L.str.5, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
