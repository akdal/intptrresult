	.text
	.file	"btDbvt.bc"
	.globl	_ZN6btDbvtC2Ev
	.p2align	4, 0x90
	.type	_ZN6btDbvtC2Ev,@function
_ZN6btDbvtC2Ev:                         # @_ZN6btDbvtC2Ev
	.cfi_startproc
# BB#0:
	movb	$1, 56(%rdi)
	movq	$0, 48(%rdi)
	movl	$0, 36(%rdi)
	movl	$0, 40(%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 16(%rdi)
	movl	$0, 24(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN6btDbvtC2Ev, .Lfunc_end0-_ZN6btDbvtC2Ev
	.cfi_endproc

	.globl	_ZN6btDbvtD2Ev
	.p2align	4, 0x90
	.type	_ZN6btDbvtD2Ev,@function
_ZN6btDbvtD2Ev:                         # @_ZN6btDbvtD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB1_2
# BB#1:
.Ltmp0:
	movq	%rbx, %rdi
	callq	_ZL17recursedeletenodeP6btDbvtP10btDbvtNode
.Ltmp1:
.LBB1_2:                                # %.noexc
	movq	8(%rbx), %rdi
.Ltmp2:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp3:
# BB#3:                                 # %.noexc2
	movq	$0, 8(%rbx)
	movl	$-1, 16(%rbx)
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_7
# BB#4:
	cmpb	$0, 56(%rbx)
	je	.LBB1_6
# BB#5:
.Ltmp4:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp5:
.LBB1_6:                                # %.noexc3
	movq	$0, 48(%rbx)
.LBB1_7:                                # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEED2Ev.exit
	movl	$0, 24(%rbx)
	movb	$1, 56(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 36(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_8:
.Ltmp6:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_12
# BB#9:
	cmpb	$0, 56(%rbx)
	je	.LBB1_11
# BB#10:
.Ltmp7:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp8:
.LBB1_11:                               # %.noexc6
	movq	$0, 48(%rbx)
.LBB1_12:
	movb	$1, 56(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 36(%rbx)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB1_13:
.Ltmp9:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end1:
	.size	_ZN6btDbvtD2Ev, .Lfunc_end1-_ZN6btDbvtD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp5-.Ltmp0           #   Call between .Ltmp0 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	1                       #   On action: 1
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp8      #   Call between .Ltmp8 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN6btDbvt5clearEv
	.p2align	4, 0x90
	.type	_ZN6btDbvt5clearEv,@function
_ZN6btDbvt5clearEv:                     # @_ZN6btDbvt5clearEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB2_2
# BB#1:
	movq	%rbx, %rdi
	callq	_ZL17recursedeletenodeP6btDbvtP10btDbvtNode
.LBB2_2:
	movq	8(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	$0, 8(%rbx)
	movl	$-1, 16(%rbx)
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_6
# BB#3:
	cmpb	$0, 56(%rbx)
	je	.LBB2_5
# BB#4:
	callq	_Z21btAlignedFreeInternalPv
.LBB2_5:
	movq	$0, 48(%rbx)
.LBB2_6:                                # %_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE5clearEv.exit
	movb	$1, 56(%rbx)
	movq	$0, 48(%rbx)
	movl	$0, 36(%rbx)
	movl	$0, 40(%rbx)
	movl	$0, 24(%rbx)
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN6btDbvt5clearEv, .Lfunc_end2-_ZN6btDbvt5clearEv
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.text
	.p2align	4, 0x90
	.type	_ZL17recursedeletenodeP6btDbvtP10btDbvtNode,@function
_ZL17recursedeletenodeP6btDbvtP10btDbvtNode: # @_ZL17recursedeletenodeP6btDbvtP10btDbvtNode
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	$0, 48(%r14)
	je	.LBB4_2
# BB#1:
	movq	40(%r14), %rsi
	movq	%rbx, %rdi
	callq	_ZL17recursedeletenodeP6btDbvtP10btDbvtNode
	movq	48(%r14), %rsi
	movq	%rbx, %rdi
	callq	_ZL17recursedeletenodeP6btDbvtP10btDbvtNode
.LBB4_2:
	cmpq	%r14, (%rbx)
	jne	.LBB4_4
# BB#3:
	movq	$0, (%rbx)
.LBB4_4:
	movq	8(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	%r14, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_ZL17recursedeletenodeP6btDbvtP10btDbvtNode, .Lfunc_end4-_ZL17recursedeletenodeP6btDbvtP10btDbvtNode
	.cfi_endproc

	.globl	_ZN6btDbvt16optimizeBottomUpEv
	.p2align	4, 0x90
	.type	_ZN6btDbvt16optimizeBottomUpEv,@function
_ZN6btDbvt16optimizeBottomUpEv:         # @_ZN6btDbvt16optimizeBottomUpEv
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 64
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB5_19
# BB#1:
	movb	$1, 24(%rsp)
	movq	$0, 16(%rsp)
	movq	$0, 4(%rsp)
	movslq	20(%r14), %r15
	testq	%r15, %r15
	jle	.LBB5_13
# BB#2:
	leaq	(,%r15,8), %rdi
.Ltmp10:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp11:
# BB#3:                                 # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE8allocateEi.exit.i
	movslq	4(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB5_8
# BB#4:                                 # %.lr.ph.i.i
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB5_6
	.p2align	4, 0x90
.LBB5_5:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	%rdi, (%rbx,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB5_5
.LBB5_6:                                # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB5_8
	.p2align	4, 0x90
.LBB5_7:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, (%rbx,%rcx,8)
	movq	16(%rsp), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	%rdx, 8(%rbx,%rcx,8)
	movq	16(%rsp), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	%rdx, 16(%rbx,%rcx,8)
	movq	16(%rsp), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%rbx,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB5_7
.LBB5_8:                                # %_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_.exit.i
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_12
# BB#9:
	cmpb	$0, 24(%rsp)
	je	.LBB5_11
# BB#10:
.Ltmp12:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp13:
.LBB5_11:                               # %.noexc2
	movq	$0, 16(%rsp)
.LBB5_12:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE10deallocateEv.exit.i
	movb	$1, 24(%rsp)
	movq	%rbx, 16(%rsp)
	movl	%r15d, 8(%rsp)
	movq	(%r14), %rsi
.LBB5_13:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit
.Ltmp14:
	movq	%rsp, %rdx
	movl	$-1, %ecx
	movq	%r14, %rdi
	callq	_ZL11fetchleavesP6btDbvtP10btDbvtNodeR20btAlignedObjectArrayIS2_Ei
.Ltmp15:
# BB#14:
.Ltmp16:
	movq	%rsp, %rsi
	movq	%r14, %rdi
	callq	_ZL8bottomupP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeE
.Ltmp17:
# BB#15:
	movq	16(%rsp), %rdi
	movq	(%rdi), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.LBB5_19
# BB#16:
	cmpb	$0, 24(%rsp)
	je	.LBB5_18
# BB#17:
	callq	_Z21btAlignedFreeInternalPv
.LBB5_18:
	movq	$0, 16(%rsp)
.LBB5_19:
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB5_20:
.Ltmp18:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_24
# BB#21:
	cmpb	$0, 24(%rsp)
	je	.LBB5_23
# BB#22:
.Ltmp19:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp20:
.LBB5_23:                               # %.noexc4
	movq	$0, 16(%rsp)
.LBB5_24:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB5_25:
.Ltmp21:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN6btDbvt16optimizeBottomUpEv, .Lfunc_end5-_ZN6btDbvt16optimizeBottomUpEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp17-.Ltmp10         #   Call between .Ltmp10 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp19-.Ltmp17         #   Call between .Ltmp17 and .Ltmp19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin1   #     jumps to .Ltmp21
	.byte	1                       #   On action: 1
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp20     #   Call between .Ltmp20 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL11fetchleavesP6btDbvtP10btDbvtNodeR20btAlignedObjectArrayIS2_Ei,@function
_ZL11fetchleavesP6btDbvtP10btDbvtNodeR20btAlignedObjectArrayIS2_Ei: # @_ZL11fetchleavesP6btDbvtP10btDbvtNodeR20btAlignedObjectArrayIS2_Ei
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	testl	%ebp, %ebp
	je	.LBB6_3
# BB#1:
	movq	48(%r14), %rax
	testq	%rax, %rax
	je	.LBB6_3
# BB#2:
	movq	40(%r14), %rsi
	decl	%ebp
	movq	%r15, %rdi
	movq	%rbx, %rdx
	movl	%ebp, %ecx
	callq	_ZL11fetchleavesP6btDbvtP10btDbvtNodeR20btAlignedObjectArrayIS2_Ei
	movq	48(%r14), %rsi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	movl	%ebp, %ecx
	callq	_ZL11fetchleavesP6btDbvtP10btDbvtNodeR20btAlignedObjectArrayIS2_Ei
	movq	8(%r15), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	%r14, 8(%r15)
	jmp	.LBB6_19
.LBB6_3:
	movl	4(%rbx), %eax
	cmpl	8(%rbx), %eax
	jne	.LBB6_18
# BB#4:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB6_18
# BB#5:
	testl	%ebp, %ebp
	je	.LBB6_6
# BB#7:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB6_9
	jmp	.LBB6_13
.LBB6_6:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB6_13
.LBB6_9:                                # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB6_11
	.p2align	4, 0x90
.LBB6_10:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB6_10
.LBB6_11:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB6_13
	.p2align	4, 0x90
.LBB6_12:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB6_12
.LBB6_13:                               # %_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_.exit.i.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_17
# BB#14:
	cmpb	$0, 24(%rbx)
	je	.LBB6_16
# BB#15:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%rbx), %eax
.LBB6_16:
	movq	$0, 16(%rbx)
.LBB6_17:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE10deallocateEv.exit.i.i
	movb	$1, 24(%rbx)
	movq	%r15, 16(%rbx)
	movl	%ebp, 8(%rbx)
.LBB6_18:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE9push_backERKS1_.exit
	movq	16(%rbx), %rcx
	cltq
	movq	%r14, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 4(%rbx)
.LBB6_19:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE9push_backERKS1_.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZL11fetchleavesP6btDbvtP10btDbvtNodeR20btAlignedObjectArrayIS2_Ei, .Lfunc_end6-_ZL11fetchleavesP6btDbvtP10btDbvtNodeR20btAlignedObjectArrayIS2_Ei
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_0:
	.long	2139095039              # float 3.40282347E+38
	.text
	.p2align	4, 0x90
	.type	_ZL8bottomupP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeE,@function
_ZL8bottomupP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeE: # @_ZL8bottomupP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 80
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%rsi, (%rsp)            # 8-byte Spill
	movl	4(%rsi), %eax
	cmpl	$2, %eax
	jl	.LBB7_11
# BB#1:                                 # %.preheader.lr.ph
	movss	.LCPI7_0(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph76
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_3 Depth 2
                                        #       Child Loop BB7_5 Depth 3
	movslq	%eax, %r10
	movl	$-1, %eax
	xorl	%r11d, %r11d
	movl	$1, %esi
	movaps	%xmm11, %xmm5
	movl	$-1, %ecx
	.p2align	4, 0x90
.LBB7_3:                                #   Parent Loop BB7_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_5 Depth 3
	leaq	1(%r11), %r9
	cmpq	%r10, %r9
	jge	.LBB7_7
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB7_3 Depth=2
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	16(%rdx), %r12
	movq	(%r12,%r11,8), %r8
	leaq	16(%r8), %r13
	movss	(%r8), %xmm8            # xmm8 = mem[0],zero,zero,zero
	movss	4(%r8), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	16(%r8), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movss	20(%r8), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	8(%r8), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	24(%r8), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB7_5:                                #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movaps	%xmm5, %xmm7
	movq	(%r12,%rdi,8), %rbp
	leaq	16(%rbp), %r15
	movss	(%rbp), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm8, %xmm5
	movq	%rbp, %rdx
	cmovaq	%r8, %rdx
	ucomiss	16(%rbp), %xmm10
	movq	%r15, %rsi
	cmovaq	%r13, %rsi
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm9, %xmm0
	movq	%rbp, %rsi
	cmovaq	%r8, %rsi
	ucomiss	20(%rbp), %xmm3
	movq	%r15, %r14
	cmovaq	%r13, %r14
	movss	4(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbp), %xmm5          # xmm5 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm5
	movq	%rbp, %rbx
	cmovaq	%r8, %rbx
	ucomiss	24(%rbp), %xmm6
	cmovaq	%r13, %r15
	movss	8(%r15), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	(%rdx), %xmm1
	subss	4(%rsi), %xmm0
	subss	8(%rbx), %xmm2
	movaps	%xmm1, %xmm5
	mulss	%xmm0, %xmm5
	mulss	%xmm2, %xmm5
	addss	%xmm1, %xmm5
	addss	%xmm0, %xmm5
	addss	%xmm2, %xmm5
	ucomiss	%xmm5, %xmm7
	cmoval	%r11d, %ecx
	cmoval	%edi, %eax
	minss	%xmm7, %xmm5
	incq	%rdi
	cmpq	%r10, %rdi
	jl	.LBB7_5
# BB#6:                                 # %.loopexit
                                        #   in Loop: Header=BB7_3 Depth=2
	movq	16(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	cmpq	%r10, %r9
	movq	%r9, %r11
	jl	.LBB7_3
.LBB7_7:                                # %._crit_edge
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	16(%rdx), %rdx
	movslq	%ecx, %r12
	movq	(%rdx,%r12,8), %rbx
	movslq	%eax, %r13
	movq	(%rdx,%r13,8), %rbp
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.LBB7_9
# BB#8:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	$0, 8(%rcx)
	jmp	.LBB7_10
	.p2align	4, 0x90
.LBB7_9:                                #   in Loop: Header=BB7_2 Depth=1
	movl	$56, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movss	.LCPI7_0(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	$0, 48(%rax)
.LBB7_10:                               # %_ZL10createnodeP6btDbvtP10btDbvtNodeRK12btDbvtAabbMmS5_Pv.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	leaq	16(%rbx), %rcx
	leaq	16(%rbp), %rdx
	movq	$0, 32(%rax)
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rbx), %xmm0
	movq	%rbp, %rsi
	cmovaq	%rbx, %rsi
	movl	(%rsi), %esi
	movl	%esi, (%rax)
	movss	16(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	16(%rbp), %xmm0
	movq	%rdx, %rsi
	cmovaq	%rcx, %rsi
	movl	(%rsi), %esi
	movl	%esi, 16(%rax)
	movss	4(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%rbx), %xmm0
	movq	%rbp, %rsi
	cmovaq	%rbx, %rsi
	movl	4(%rsi), %esi
	movl	%esi, 4(%rax)
	movss	20(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	20(%rbp), %xmm0
	movq	%rdx, %rsi
	cmovaq	%rcx, %rsi
	movl	4(%rsi), %esi
	movl	%esi, 20(%rax)
	movss	8(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%rbx), %xmm0
	movq	%rbp, %rsi
	cmovaq	%rbx, %rsi
	movl	8(%rsi), %esi
	movl	%esi, 8(%rax)
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	24(%rbp), %xmm0
	cmovaq	%rcx, %rdx
	movl	8(%rdx), %ecx
	movl	%ecx, 24(%rax)
	movq	%rbx, 40(%rax)
	movq	%rbp, 48(%rax)
	movq	%rax, 32(%rbx)
	movq	%rax, 32(%rbp)
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	16(%rbp), %rcx
	movq	%rax, (%rcx,%r12,8)
	movslq	4(%rbp), %rcx
	leaq	-1(%rcx), %rax
	movq	16(%rbp), %rdx
	movq	(%rdx,%r13,8), %rsi
	movq	-8(%rdx,%rcx,8), %rdi
	movq	%rdi, (%rdx,%r13,8)
	movq	16(%rbp), %rdx
	movq	%rsi, -8(%rdx,%rcx,8)
	movl	%eax, 4(%rbp)
	cmpl	$1, %eax
	jg	.LBB7_2
.LBB7_11:                               # %._crit_edge79
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZL8bottomupP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeE, .Lfunc_end7-_ZL8bottomupP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeE
	.cfi_endproc

	.globl	_ZN6btDbvt15optimizeTopDownEi
	.p2align	4, 0x90
	.type	_ZN6btDbvt15optimizeTopDownEi,@function
_ZN6btDbvt15optimizeTopDownEi:          # @_ZN6btDbvt15optimizeTopDownEi
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 80
.Lcfi46:
	.cfi_offset %rbx, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB8_19
# BB#1:
	movb	$1, 32(%rsp)
	movq	$0, 24(%rsp)
	movq	$0, 12(%rsp)
	movslq	20(%rbx), %r15
	testq	%r15, %r15
	jle	.LBB8_13
# BB#2:
	leaq	(,%r15,8), %rdi
.Ltmp22:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
.Ltmp23:
# BB#3:                                 # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE8allocateEi.exit.i
	movslq	12(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB8_8
# BB#4:                                 # %.lr.ph.i.i
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB8_6
	.p2align	4, 0x90
.LBB8_5:                                # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	%rdi, (%rbp,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB8_5
.LBB8_6:                                # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB8_8
	.p2align	4, 0x90
.LBB8_7:                                # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, (%rbp,%rcx,8)
	movq	24(%rsp), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	%rdx, 8(%rbp,%rcx,8)
	movq	24(%rsp), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	%rdx, 16(%rbp,%rcx,8)
	movq	24(%rsp), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%rbp,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB8_7
.LBB8_8:                                # %_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_.exit.i
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_12
# BB#9:
	cmpb	$0, 32(%rsp)
	je	.LBB8_11
# BB#10:
.Ltmp24:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp25:
.LBB8_11:                               # %.noexc3
	movq	$0, 24(%rsp)
.LBB8_12:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE10deallocateEv.exit.i
	movb	$1, 32(%rsp)
	movq	%rbp, 24(%rsp)
	movl	%r15d, 16(%rsp)
	movq	(%rbx), %rsi
.LBB8_13:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit
.Ltmp26:
	leaq	8(%rsp), %rdx
	movl	$-1, %ecx
	movq	%rbx, %rdi
	callq	_ZL11fetchleavesP6btDbvtP10btDbvtNodeR20btAlignedObjectArrayIS2_Ei
.Ltmp27:
# BB#14:
.Ltmp28:
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	_ZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEi
.Ltmp29:
# BB#15:
	movq	%rax, (%rbx)
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_19
# BB#16:
	cmpb	$0, 32(%rsp)
	je	.LBB8_18
# BB#17:
	callq	_Z21btAlignedFreeInternalPv
.LBB8_18:
	movq	$0, 24(%rsp)
.LBB8_19:
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_20:
.Ltmp30:
	movq	%rax, %rbx
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_24
# BB#21:
	cmpb	$0, 32(%rsp)
	je	.LBB8_23
# BB#22:
.Ltmp31:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp32:
.LBB8_23:                               # %.noexc5
	movq	$0, 24(%rsp)
.LBB8_24:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB8_25:
.Ltmp33:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN6btDbvt15optimizeTopDownEi, .Lfunc_end8-_ZN6btDbvt15optimizeTopDownEi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp29-.Ltmp22         #   Call between .Ltmp22 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin2   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp31-.Ltmp29         #   Call between .Ltmp29 and .Ltmp31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin2   #     jumps to .Ltmp33
	.byte	1                       #   On action: 1
	.long	.Ltmp32-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp32     #   Call between .Ltmp32 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.p2align	4, 0x90
	.type	_ZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEi,@function
_ZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEi: # @_ZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEi
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi56:
	.cfi_def_cfa_offset 256
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movb	_ZGVZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis(%rip), %al
	testb	%al, %al
	jne	.LBB9_3
# BB#1:
	movl	$_ZGVZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB9_3
# BB#2:
	movl	$1065353216, _ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis(%rip) # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis+4(%rip)
	movl	$1065353216, _ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis+20(%rip) # imm = 0x3F800000
	movups	%xmm0, _ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis+24(%rip)
	movq	$1065353216, _ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis+40(%rip) # imm = 0x3F800000
	movl	$_ZGVZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis, %edi
	callq	__cxa_guard_release
.LBB9_3:
	movslq	4(%rbx), %r8
	cmpq	$2, %r8
	jl	.LBB9_193
# BB#4:
	cmpl	%r14d, %r8d
	jle	.LBB9_192
# BB#5:                                 # %.lr.ph.i
	movq	%rbp, 192(%rsp)         # 8-byte Spill
	movl	%r8d, %edx
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movq	16(%rbx), %r9
	movq	(%r9), %rsi
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movaps	%xmm1, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	leaq	32(%rsp), %r10
	movss	16(%rsp), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	36(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movss	40(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	decq	%rdx
	leaq	8(%r9), %rdi
	leaq	16(%rsp), %r12
	.p2align	4, 0x90
.LBB9_6:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rsi
	leaq	16(%rsi), %rcx
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm8, %xmm3
	movq	%rsi, %rbp
	cmovaq	%r12, %rbp
	movl	(%rbp), %r13d
	movl	%r13d, 16(%rsp)
	ucomiss	16(%rsi), %xmm1
	movq	%rcx, %rbx
	cmovaq	%r10, %rbx
	movl	(%rbx), %ebp
	movl	%ebp, 32(%rsp)
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm5, %xmm1
	movq	%rsi, %rax
	cmovaq	%r12, %rax
	movl	4(%rax), %ebx
	movl	%ebx, 20(%rsp)
	ucomiss	20(%rsi), %xmm2
	movq	%rcx, %rax
	cmovaq	%r10, %rax
	movl	4(%rax), %r11d
	movl	%r11d, 36(%rsp)
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm15, %xmm1
	movq	%rsi, %rax
	cmovaq	%r12, %rax
	movl	8(%rax), %eax
	movl	%eax, 24(%rsp)
	ucomiss	24(%rsi), %xmm0
	cmovaq	%r10, %rcx
	movl	8(%rcx), %ecx
	movl	%ecx, 40(%rsp)
	movd	%r13d, %xmm8
	movd	%ebp, %xmm1
	movd	%ebx, %xmm5
	movd	%r11d, %xmm2
	movd	%eax, %xmm15
	movd	%ecx, %xmm0
	addq	$8, %rdi
	decq	%rdx
	jne	.LBB9_6
# BB#7:                                 # %_ZL6boundsRK20btAlignedObjectArrayIP10btDbvtNodeE.exit
	movl	28(%rsp), %edx
	movl	%edx, 172(%rsp)         # 4-byte Spill
	movl	44(%rsp), %edi
	addss	%xmm2, %xmm5
	addss	%xmm0, %xmm15
	movss	.LCPI9_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	movb	$1, 40(%rsp)
	movq	$0, 32(%rsp)
	movl	$0, 20(%rsp)
	movl	$0, 24(%rsp)
	movb	$1, 72(%rsp)
	movq	$0, 64(%rsp)
	movq	$0, 52(%rsp)
	addss	%xmm1, %xmm8
	mulss	%xmm3, %xmm8
	mulss	%xmm3, %xmm15
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 144(%rsp)
	movq	$0, 160(%rsp)
	movl	$-1, %edx
	testl	%r8d, %r8d
	movl	%eax, 184(%rsp)         # 4-byte Spill
	movss	%xmm5, 104(%rsp)        # 4-byte Spill
	movss	%xmm8, 88(%rsp)         # 4-byte Spill
	movl	%ecx, 180(%rsp)         # 4-byte Spill
	movl	%edi, 176(%rsp)         # 4-byte Spill
	jle	.LBB9_8
# BB#11:                                # %.lr.ph276
	movss	_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movss	_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis+4(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	movss	_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis+8(%rip), %xmm10 # xmm10 = mem[0],zero,zero,zero
	movss	_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis+16(%rip), %xmm11 # xmm11 = mem[0],zero,zero,zero
	movss	_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis+20(%rip), %xmm12 # xmm12 = mem[0],zero,zero,zero
	movss	_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis+24(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	movss	_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis+32(%rip), %xmm14 # xmm14 = mem[0],zero,zero,zero
	movss	_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis+36(%rip), %xmm7 # xmm7 = mem[0],zero,zero,zero
	movss	_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis+40(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	xorl	%esi, %esi
	xorps	%xmm1, %xmm1
	movaps	%xmm3, %xmm9
	.p2align	4, 0x90
.LBB9_12:                               # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rsi,8), %rax
	movss	(%rax), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm6          # xmm6 = mem[0],zero,zero,zero
	addss	16(%rax), %xmm3
	addss	20(%rax), %xmm6
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	addss	24(%rax), %xmm4
	mulss	%xmm9, %xmm3
	mulss	%xmm9, %xmm6
	mulss	%xmm9, %xmm4
	subss	%xmm8, %xmm3
	subss	%xmm5, %xmm6
	subss	%xmm15, %xmm4
	movaps	%xmm3, %xmm5
	mulss	12(%rsp), %xmm5         # 4-byte Folded Reload
	movaps	%xmm6, %xmm2
	mulss	96(%rsp), %xmm2         # 4-byte Folded Reload
	addss	%xmm5, %xmm2
	movaps	%xmm4, %xmm5
	mulss	%xmm10, %xmm5
	addss	%xmm2, %xmm5
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm5
	movaps	%xmm3, %xmm2
	mulss	%xmm11, %xmm2
	movaps	%xmm6, %xmm5
	mulss	%xmm12, %xmm5
	addss	%xmm2, %xmm5
	movaps	%xmm4, %xmm2
	mulss	%xmm13, %xmm2
	addss	%xmm5, %xmm2
	movss	104(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	seta	%al
	incl	144(%rsp,%rax,4)
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm2
	mulss	%xmm14, %xmm3
	mulss	%xmm7, %xmm6
	addss	%xmm3, %xmm6
	seta	%al
	incl	152(%rsp,%rax,4)
	mulss	%xmm0, %xmm4
	addss	%xmm6, %xmm4
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm4
	seta	%al
	incl	160(%rsp,%rax,4)
	incq	%rsi
	cmpq	%r8, %rsi
	jl	.LBB9_12
# BB#9:                                 # %.preheader268.preheader
	movl	144(%rsp), %esi
	testl	%esi, %esi
	jle	.LBB9_10
# BB#15:
	movl	148(%rsp), %edi
	testl	%edi, %edi
	movl	%r8d, %ecx
	movss	88(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	.LCPI9_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	jle	.LBB9_17
# BB#16:
	subl	%edi, %esi
	cvtsi2ssl	%esi, %xmm0
	andps	.LCPI9_1(%rip), %xmm0
	cvttss2si	%xmm0, %ecx
	xorl	%eax, %eax
	cmpl	%r8d, %ecx
	movl	$-1, %edx
	cmovll	%eax, %edx
	cmovgl	%r8d, %ecx
	jmp	.LBB9_17
.LBB9_192:
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	_ZL8bottomupP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeE
.LBB9_193:
	movq	16(%rbx), %rax
	movq	(%rax), %rbp
.LBB9_194:
	movq	%rbp, %rax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_8:
	movl	%r8d, %ecx
	jmp	.LBB9_17
.LBB9_10:
	movl	%r8d, %ecx
	movss	88(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	.LCPI9_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
.LBB9_17:                               # %.preheader268.1283
	movl	%ebx, 188(%rsp)         # 4-byte Spill
	movl	152(%rsp), %esi
	testl	%esi, %esi
	jle	.LBB9_18
# BB#198:
	movl	156(%rsp), %edi
	testl	%edi, %edi
	movq	112(%rsp), %rbx         # 8-byte Reload
	jle	.LBB9_200
# BB#199:
	subl	%edi, %esi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%esi, %xmm0
	andps	.LCPI9_1(%rip), %xmm0
	cvttss2si	%xmm0, %eax
	cmpl	%ecx, %eax
	movl	$1, %esi
	cmovll	%esi, %edx
	cmovlel	%eax, %ecx
	jmp	.LBB9_200
.LBB9_18:
	movq	112(%rsp), %rbx         # 8-byte Reload
.LBB9_200:                              # %.preheader268.2284
	movl	160(%rsp), %esi
	testl	%esi, %esi
	movl	%r11d, 12(%rsp)         # 4-byte Spill
	movl	%r14d, 132(%rsp)        # 4-byte Spill
	movl	%r13d, 128(%rsp)        # 4-byte Spill
	movl	%ebp, 124(%rsp)         # 4-byte Spill
	jle	.LBB9_203
# BB#201:
	movl	164(%rsp), %edi
	testl	%edi, %edi
	jle	.LBB9_203
# BB#202:
	subl	%edi, %esi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%esi, %xmm0
	andps	.LCPI9_1(%rip), %xmm0
	cvttss2si	%xmm0, %eax
	movl	$2, %esi
	cmpl	%ecx, %eax
	jl	.LBB9_19
.LBB9_203:
	testl	%edx, %edx
	movl	%edx, %esi
	js	.LBB9_112
.LBB9_19:                               # %.thread
	movslq	%esi, %r15
	movslq	144(%rsp,%r15,8), %r12
	testq	%r12, %r12
	movss	%xmm15, 96(%rsp)        # 4-byte Spill
	jle	.LBB9_20
# BB#21:
	leaq	(,%r12,8), %rdi
.Ltmp47:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movss	.LCPI9_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	88(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movl	12(%rsp), %r11d         # 4-byte Reload
	movq	%rax, %rbx
.Ltmp48:
# BB#22:                                # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE8allocateEi.exit.i134
	movslq	20(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB9_27
# BB#23:                                # %.lr.ph.i.i136
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB9_25
	.p2align	4, 0x90
.LBB9_24:                               # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	%rdi, (%rbx,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB9_24
.LBB9_25:                               # %.prol.loopexit354
	cmpq	$3, %rdx
	jb	.LBB9_27
	.p2align	4, 0x90
.LBB9_26:                               # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, (%rbx,%rcx,8)
	movq	32(%rsp), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	%rdx, 8(%rbx,%rcx,8)
	movq	32(%rsp), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	%rdx, 16(%rbx,%rcx,8)
	movq	32(%rsp), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%rbx,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB9_26
.LBB9_27:                               # %_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_.exit.i141
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_31
# BB#28:
	cmpb	$0, 40(%rsp)
	je	.LBB9_30
# BB#29:
.Ltmp49:
	callq	_Z21btAlignedFreeInternalPv
	movss	.LCPI9_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	88(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movl	12(%rsp), %r11d         # 4-byte Reload
.Ltmp50:
.LBB9_30:                               # %.noexc144
	movq	$0, 32(%rsp)
.LBB9_31:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE10deallocateEv.exit.i142
	movb	$1, 40(%rsp)
	movq	%rbx, 32(%rsp)
	movl	%r12d, 24(%rsp)
	movl	56(%rsp), %eax
	jmp	.LBB9_32
.LBB9_112:
	xorl	%ecx, %ecx
	cmpl	$-1, %r8d
	jl	.LBB9_126
# BB#113:
	movl	%r8d, %r12d
	shrl	$31, %r12d
	addl	%r8d, %r12d
	sarl	%r12d
	incl	%r12d
	je	.LBB9_114
# BB#115:
	movslq	%r12d, %rdi
	shlq	$3, %rdi
.Ltmp34:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movl	12(%rsp), %r11d         # 4-byte Reload
	movq	%rax, %rbx
.Ltmp35:
# BB#116:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE8allocateEi.exit.i118
	movslq	20(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB9_121
# BB#117:                               # %.lr.ph.i.i120
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB9_119
	.p2align	4, 0x90
.LBB9_118:                              # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	%rdi, (%rbx,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB9_118
.LBB9_119:                              # %.prol.loopexit369
	cmpq	$3, %rdx
	jb	.LBB9_121
	.p2align	4, 0x90
.LBB9_120:                              # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, (%rbx,%rcx,8)
	movq	32(%rsp), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	%rdx, 8(%rbx,%rcx,8)
	movq	32(%rsp), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	%rdx, 16(%rbx,%rcx,8)
	movq	32(%rsp), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%rbx,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB9_120
	jmp	.LBB9_121
.LBB9_20:
	xorl	%eax, %eax
.LBB9_32:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit145
	movl	148(%rsp,%r15,8), %r12d
	cmpl	%r12d, %eax
	jge	.LBB9_46
# BB#33:
	movq	%r15, 136(%rsp)         # 8-byte Spill
	testl	%r12d, %r12d
	je	.LBB9_34
# BB#35:
	movslq	%r12d, %rdi
	shlq	$3, %rdi
.Ltmp51:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movss	.LCPI9_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	88(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movl	12(%rsp), %r11d         # 4-byte Reload
	movq	%rax, %rbx
.Ltmp52:
	jmp	.LBB9_36
.LBB9_34:
	xorl	%ebx, %ebx
.LBB9_36:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE8allocateEi.exit.i147
	movslq	52(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB9_41
# BB#37:                                # %.lr.ph.i.i149
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB9_39
	.p2align	4, 0x90
.LBB9_38:                               # =>This Inner Loop Header: Depth=1
	movq	64(%rsp), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	%rdi, (%rbx,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB9_38
.LBB9_39:                               # %.prol.loopexit349
	cmpq	$3, %rdx
	jb	.LBB9_41
	.p2align	4, 0x90
.LBB9_40:                               # =>This Inner Loop Header: Depth=1
	movq	64(%rsp), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, (%rbx,%rcx,8)
	movq	64(%rsp), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	%rdx, 8(%rbx,%rcx,8)
	movq	64(%rsp), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	%rdx, 16(%rbx,%rcx,8)
	movq	64(%rsp), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%rbx,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB9_40
.LBB9_41:                               # %_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_.exit.i154
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_45
# BB#42:
	cmpb	$0, 72(%rsp)
	je	.LBB9_44
# BB#43:
.Ltmp53:
	callq	_Z21btAlignedFreeInternalPv
	movss	.LCPI9_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	88(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movl	12(%rsp), %r11d         # 4-byte Reload
.Ltmp54:
.LBB9_44:                               # %.noexc157
	movq	$0, 64(%rsp)
.LBB9_45:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE10deallocateEv.exit.i155
	movb	$1, 72(%rsp)
	movq	%rbx, 64(%rsp)
	movl	%r12d, 56(%rsp)
	movq	136(%rsp), %r15         # 8-byte Reload
.LBB9_46:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit158
	movslq	20(%rsp), %rbx
	testq	%rbx, %rbx
	leaq	16(%rsp), %r9
	jns	.LBB9_58
# BB#47:
	cmpl	$0, 24(%rsp)
	jns	.LBB9_53
# BB#48:                                # %_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_.exit.i.i.i
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_52
# BB#49:
	cmpb	$0, 40(%rsp)
	je	.LBB9_51
# BB#50:
.Ltmp55:
	callq	_Z21btAlignedFreeInternalPv
	movss	.LCPI9_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	88(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movl	12(%rsp), %r11d         # 4-byte Reload
.Ltmp56:
.LBB9_51:                               # %.noexc169
	movq	$0, 32(%rsp)
	leaq	16(%rsp), %r9
.LBB9_52:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit.preheader.i.i
	movb	$1, 40(%rsp)
	movq	$0, 32(%rsp)
	movl	$0, 24(%rsp)
.LBB9_53:                               # %.lr.ph.i.i159
	movl	%ebx, %ecx
	negl	%ecx
	andq	$7, %rcx
	movq	%rbx, %rax
	je	.LBB9_56
# BB#54:                                # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit.i.i.prol.preheader
	negq	%rcx
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB9_55:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rdx
	movq	$0, (%rdx,%rax,8)
	incq	%rax
	incq	%rcx
	jne	.LBB9_55
.LBB9_56:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit.i.i.prol.loopexit
	cmpl	$-8, %ebx
	ja	.LBB9_58
	.p2align	4, 0x90
.LBB9_57:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	32(%rsp), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	32(%rsp), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	32(%rsp), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	32(%rsp), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	32(%rsp), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	32(%rsp), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	32(%rsp), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	jne	.LBB9_57
.LBB9_58:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE6resizeEiRKS1_.exit.i
	movl	$0, 20(%rsp)
	movslq	52(%rsp), %rbx
	testq	%rbx, %rbx
	jns	.LBB9_70
# BB#59:
	cmpl	$0, 56(%rsp)
	jns	.LBB9_65
# BB#60:                                # %_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_.exit.i.i25.i
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_64
# BB#61:
	cmpb	$0, 72(%rsp)
	je	.LBB9_63
# BB#62:
.Ltmp57:
	callq	_Z21btAlignedFreeInternalPv
	movss	.LCPI9_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	88(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movl	12(%rsp), %r11d         # 4-byte Reload
.Ltmp58:
.LBB9_63:                               # %.noexc170
	movq	$0, 64(%rsp)
	leaq	16(%rsp), %r9
.LBB9_64:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit.preheader.i26.i
	movb	$1, 72(%rsp)
	movq	$0, 64(%rsp)
	movl	$0, 56(%rsp)
.LBB9_65:                               # %.lr.ph.i27.i
	movl	%ebx, %ecx
	negl	%ecx
	andq	$7, %rcx
	movq	%rbx, %rax
	je	.LBB9_68
# BB#66:                                # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit.i31.i.prol.preheader
	negq	%rcx
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB9_67:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit.i31.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	64(%rsp), %rdx
	movq	$0, (%rdx,%rax,8)
	incq	%rax
	incq	%rcx
	jne	.LBB9_67
.LBB9_68:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit.i31.i.prol.loopexit
	cmpl	$-8, %ebx
	ja	.LBB9_70
	.p2align	4, 0x90
.LBB9_69:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit.i31.i
                                        # =>This Inner Loop Header: Depth=1
	movq	64(%rsp), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	64(%rsp), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	64(%rsp), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	64(%rsp), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	64(%rsp), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	64(%rsp), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	64(%rsp), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	64(%rsp), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	jne	.LBB9_69
.LBB9_70:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE6resizeEiRKS1_.exit32.i
	movl	$0, 52(%rsp)
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	4(%rdi), %ebx
	testl	%ebx, %ebx
	jle	.LBB9_160
# BB#71:                                # %.lr.ph.i164
	leaq	20(%rsp), %r10
	shlq	$4, %r15
	leaq	_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis(%r15), %r13
	xorl	%r14d, %r14d
	xorps	%xmm6, %xmm6
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB9_72:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_101 Depth 2
                                        #     Child Loop BB9_103 Depth 2
                                        #     Child Loop BB9_83 Depth 2
                                        #     Child Loop BB9_85 Depth 2
	movq	16(%rdi), %r15
	movq	(%r15,%r14,8), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	16(%rax), %xmm0
	addss	20(%rax), %xmm1
	movss	8(%rax), %xmm2          # xmm2 = mem[0],zero,zero,zero
	addss	24(%rax), %xmm2
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm2
	subss	%xmm8, %xmm0
	subss	%xmm5, %xmm1
	subss	%xmm15, %xmm2
	mulss	(%r13), %xmm0
	mulss	4(%r13), %xmm1
	addss	%xmm0, %xmm1
	mulss	8(%r13), %xmm2
	addss	%xmm1, %xmm2
	ucomiss	%xmm2, %xmm6
	jbe	.LBB9_92
# BB#73:                                #   in Loop: Header=BB9_72 Depth=1
	movl	20(%rsp), %eax
	cmpl	24(%rsp), %eax
	jne	.LBB9_74
# BB#75:                                #   in Loop: Header=BB9_72 Depth=1
	leal	(%rax,%rax), %r12d
	testl	%eax, %eax
	movl	$1, %ecx
	cmovel	%ecx, %r12d
	cmpl	%r12d, %eax
	jge	.LBB9_76
# BB#77:                                #   in Loop: Header=BB9_72 Depth=1
	testl	%r12d, %r12d
	je	.LBB9_78
# BB#79:                                #   in Loop: Header=BB9_72 Depth=1
	movslq	%r12d, %rdi
	shlq	$3, %rdi
.Ltmp63:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp64:
# BB#80:                                # %.noexc171
                                        #   in Loop: Header=BB9_72 Depth=1
	movl	20(%rsp), %eax
	movl	12(%rsp), %r11d         # 4-byte Reload
	movss	96(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	88(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	.LCPI9_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	leaq	20(%rsp), %r10
	xorps	%xmm6, %xmm6
	testl	%eax, %eax
	jg	.LBB9_82
	jmp	.LBB9_86
	.p2align	4, 0x90
.LBB9_92:                               #   in Loop: Header=BB9_72 Depth=1
	movl	52(%rsp), %eax
	cmpl	56(%rsp), %eax
	jne	.LBB9_93
# BB#94:                                #   in Loop: Header=BB9_72 Depth=1
	leal	(%rax,%rax), %ebp
	testl	%eax, %eax
	movl	$1, %ecx
	cmovel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB9_93
# BB#95:                                #   in Loop: Header=BB9_72 Depth=1
	testl	%ebp, %ebp
	je	.LBB9_96
# BB#97:                                #   in Loop: Header=BB9_72 Depth=1
	movslq	%ebp, %rdi
	shlq	$3, %rdi
.Ltmp59:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp60:
# BB#98:                                # %.noexc173
                                        #   in Loop: Header=BB9_72 Depth=1
	movl	52(%rsp), %eax
	movl	12(%rsp), %r11d         # 4-byte Reload
	leaq	16(%rsp), %r9
	movss	96(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	88(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	.LCPI9_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	leaq	20(%rsp), %r10
	xorps	%xmm6, %xmm6
	testl	%eax, %eax
	jg	.LBB9_100
	jmp	.LBB9_104
	.p2align	4, 0x90
.LBB9_93:                               #   in Loop: Header=BB9_72 Depth=1
	leaq	48(%rsp), %rcx
	leaq	52(%rsp), %rdx
	jmp	.LBB9_111
.LBB9_76:                               #   in Loop: Header=BB9_72 Depth=1
	leaq	16(%rsp), %r9
.LBB9_74:                               #   in Loop: Header=BB9_72 Depth=1
	movq	%r9, %rcx
	movq	%r10, %rdx
	jmp	.LBB9_111
.LBB9_78:                               #   in Loop: Header=BB9_72 Depth=1
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB9_86
.LBB9_82:                               # %.lr.ph.i.i.i45.i
                                        #   in Loop: Header=BB9_72 Depth=1
	movslq	%eax, %rcx
	leaq	-1(%rcx), %rsi
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB9_84
	.p2align	4, 0x90
.LBB9_83:                               #   Parent Loop BB9_72 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%rsp), %rbp
	movq	(%rbp,%rdx,8), %rbp
	movq	%rbp, (%rbx,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB9_83
.LBB9_84:                               # %.prol.loopexit338
                                        #   in Loop: Header=BB9_72 Depth=1
	cmpq	$3, %rsi
	jb	.LBB9_86
	.p2align	4, 0x90
.LBB9_85:                               #   Parent Loop BB9_72 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%rsp), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	movq	32(%rsp), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%rbx,%rdx,8)
	movq	32(%rsp), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rbx,%rdx,8)
	movq	32(%rsp), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%rbx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB9_85
.LBB9_86:                               # %_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_.exit.i.i50.i
                                        #   in Loop: Header=BB9_72 Depth=1
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_91
# BB#87:                                #   in Loop: Header=BB9_72 Depth=1
	cmpb	$0, 40(%rsp)
	je	.LBB9_90
# BB#88:                                #   in Loop: Header=BB9_72 Depth=1
.Ltmp65:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp66:
# BB#89:                                # %.noexc172
                                        #   in Loop: Header=BB9_72 Depth=1
	movl	20(%rsp), %eax
	movl	12(%rsp), %r11d         # 4-byte Reload
	movss	96(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	88(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	.LCPI9_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	leaq	20(%rsp), %r10
	xorps	%xmm6, %xmm6
.LBB9_90:                               #   in Loop: Header=BB9_72 Depth=1
	movq	$0, 32(%rsp)
.LBB9_91:                               # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE10deallocateEv.exit.i.i54.i
                                        #   in Loop: Header=BB9_72 Depth=1
	movb	$1, 40(%rsp)
	movq	%rbx, 32(%rsp)
	movl	%r12d, 24(%rsp)
	leaq	16(%rsp), %r9
	movq	%r9, %rcx
	movq	%r10, %rdx
	jmp	.LBB9_110
.LBB9_96:                               #   in Loop: Header=BB9_72 Depth=1
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB9_104
.LBB9_100:                              # %.lr.ph.i.i.i34.i
                                        #   in Loop: Header=BB9_72 Depth=1
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB9_102
	.p2align	4, 0x90
.LBB9_101:                              #   Parent Loop BB9_72 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	64(%rsp), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB9_101
.LBB9_102:                              # %.prol.loopexit
                                        #   in Loop: Header=BB9_72 Depth=1
	cmpq	$3, %r8
	jb	.LBB9_104
	.p2align	4, 0x90
.LBB9_103:                              #   Parent Loop BB9_72 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	64(%rsp), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	movq	64(%rsp), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%rbx,%rdx,8)
	movq	64(%rsp), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rbx,%rdx,8)
	movq	64(%rsp), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%rbx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB9_103
.LBB9_104:                              # %_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_.exit.i.i39.i
                                        #   in Loop: Header=BB9_72 Depth=1
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_109
# BB#105:                               #   in Loop: Header=BB9_72 Depth=1
	cmpb	$0, 72(%rsp)
	je	.LBB9_108
# BB#106:                               #   in Loop: Header=BB9_72 Depth=1
.Ltmp61:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp62:
# BB#107:                               # %.noexc174
                                        #   in Loop: Header=BB9_72 Depth=1
	movl	52(%rsp), %eax
	movl	12(%rsp), %r11d         # 4-byte Reload
	leaq	16(%rsp), %r9
	movss	96(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	88(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	.LCPI9_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	leaq	20(%rsp), %r10
	xorps	%xmm6, %xmm6
.LBB9_108:                              #   in Loop: Header=BB9_72 Depth=1
	movq	$0, 64(%rsp)
.LBB9_109:                              # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE10deallocateEv.exit.i.i.i
                                        #   in Loop: Header=BB9_72 Depth=1
	movb	$1, 72(%rsp)
	movq	%rbx, 64(%rsp)
	movl	%ebp, 56(%rsp)
	leaq	48(%rsp), %rcx
	leaq	52(%rsp), %rdx
.LBB9_110:                              # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE9push_backERKS1_.exit.i
                                        #   in Loop: Header=BB9_72 Depth=1
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	136(%rsp), %rbx         # 8-byte Reload
.LBB9_111:                              # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE9push_backERKS1_.exit.i
                                        #   in Loop: Header=BB9_72 Depth=1
	movq	16(%rcx), %rcx
	cltq
	movq	(%r15,%r14,8), %rsi
	movq	%rsi, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, (%rdx)
	incq	%r14
	cmpq	%rbx, %r14
	jne	.LBB9_72
	jmp	.LBB9_160
.LBB9_114:
	xorl	%ebx, %ebx
.LBB9_121:                              # %_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_.exit.i125
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_125
# BB#122:
	cmpb	$0, 40(%rsp)
	je	.LBB9_124
# BB#123:
.Ltmp36:
	callq	_Z21btAlignedFreeInternalPv
	movl	12(%rsp), %r11d         # 4-byte Reload
.Ltmp37:
.LBB9_124:                              # %.noexc128
	movq	$0, 32(%rsp)
.LBB9_125:                              # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE10deallocateEv.exit.i126
	movb	$1, 40(%rsp)
	movq	%rbx, 32(%rsp)
	movl	%r12d, 24(%rsp)
	movq	112(%rsp), %rbx         # 8-byte Reload
	movl	4(%rbx), %r8d
	movl	56(%rsp), %ecx
.LBB9_126:                              # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit129
	movl	%r8d, %r12d
	shrl	$31, %r12d
	addl	%r8d, %r12d
	sarl	%r12d
	cmpl	%r12d, %ecx
	jge	.LBB9_140
# BB#127:
	incl	%r8d
	cmpl	$3, %r8d
	jae	.LBB9_129
# BB#128:
	xorl	%ebx, %ebx
	jmp	.LBB9_130
.LBB9_129:
	movslq	%r12d, %rdi
	shlq	$3, %rdi
.Ltmp38:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movl	12(%rsp), %r11d         # 4-byte Reload
	movq	%rax, %rbx
.Ltmp39:
.LBB9_130:                              # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE8allocateEi.exit.i
	movslq	52(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB9_135
# BB#131:                               # %.lr.ph.i.i
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB9_133
	.p2align	4, 0x90
.LBB9_132:                              # =>This Inner Loop Header: Depth=1
	movq	64(%rsp), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	%rdi, (%rbx,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB9_132
.LBB9_133:                              # %.prol.loopexit364
	cmpq	$3, %rdx
	jb	.LBB9_135
	.p2align	4, 0x90
.LBB9_134:                              # =>This Inner Loop Header: Depth=1
	movq	64(%rsp), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, (%rbx,%rcx,8)
	movq	64(%rsp), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	%rdx, 8(%rbx,%rcx,8)
	movq	64(%rsp), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	%rdx, 16(%rbx,%rcx,8)
	movq	64(%rsp), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%rbx,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB9_134
.LBB9_135:                              # %_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_.exit.i
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_139
# BB#136:
	cmpb	$0, 72(%rsp)
	je	.LBB9_138
# BB#137:
.Ltmp40:
	callq	_Z21btAlignedFreeInternalPv
	movl	12(%rsp), %r11d         # 4-byte Reload
.Ltmp41:
.LBB9_138:                              # %.noexc110
	movq	$0, 64(%rsp)
.LBB9_139:                              # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE10deallocateEv.exit.i
	movb	$1, 72(%rsp)
	movq	%rbx, 64(%rsp)
	movl	%r12d, 56(%rsp)
	movq	112(%rsp), %rbx         # 8-byte Reload
	movl	4(%rbx), %r8d
.LBB9_140:                              # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi.exit
	testl	%r8d, %r8d
	jle	.LBB9_160
# BB#141:                               # %.lr.ph.preheader
	movslq	%r8d, %rsi
	xorl	%r14d, %r14d
	movl	$1, %edi
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB9_142:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_150 Depth 2
                                        #     Child Loop BB9_152 Depth 2
	movl	%r14d, %r12d
	andl	$1, %r12d
	movq	16(%rbx), %rdx
	shlq	$5, %r12
	leaq	20(%rsp,%r12), %r9
	movl	20(%rsp,%r12), %eax
	cmpl	24(%rsp,%r12), %eax
	jne	.LBB9_159
# BB#143:                               #   in Loop: Header=BB9_142 Depth=1
	leal	(%rax,%rax), %r15d
	testl	%eax, %eax
	cmovel	%edi, %r15d
	cmpl	%r15d, %eax
	jge	.LBB9_159
# BB#144:                               #   in Loop: Header=BB9_142 Depth=1
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	testl	%r15d, %r15d
	je	.LBB9_145
# BB#146:                               #   in Loop: Header=BB9_142 Depth=1
	movq	%r9, %rbp
	movslq	%r15d, %rdi
	shlq	$3, %rdi
.Ltmp42:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp43:
# BB#147:                               # %.noexc105
                                        #   in Loop: Header=BB9_142 Depth=1
	movq	%rbp, %r9
	movl	(%r9), %eax
	movl	12(%rsp), %r11d         # 4-byte Reload
	jmp	.LBB9_148
.LBB9_145:                              #   in Loop: Header=BB9_142 Depth=1
	xorl	%ebx, %ebx
.LBB9_148:                              # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE8allocateEi.exit.i.i
                                        #   in Loop: Header=BB9_142 Depth=1
	leaq	32(%rsp,%r12), %rbp
	testl	%eax, %eax
	jle	.LBB9_153
# BB#149:                               # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB9_142 Depth=1
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB9_151
	.p2align	4, 0x90
.LBB9_150:                              #   Parent Loop BB9_142 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB9_150
.LBB9_151:                              # %.prol.loopexit359
                                        #   in Loop: Header=BB9_142 Depth=1
	cmpq	$3, %r8
	jb	.LBB9_153
	.p2align	4, 0x90
.LBB9_152:                              #   Parent Loop BB9_142 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	movq	(%rbp), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%rbx,%rdx,8)
	movq	(%rbp), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rbx,%rdx,8)
	movq	(%rbp), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%rbx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB9_152
.LBB9_153:                              # %_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_.exit.i.i
                                        #   in Loop: Header=BB9_142 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	leaq	40(%rsp,%r12), %r13
	je	.LBB9_158
# BB#154:                               #   in Loop: Header=BB9_142 Depth=1
	cmpb	$0, (%r13)
	je	.LBB9_157
# BB#155:                               #   in Loop: Header=BB9_142 Depth=1
.Ltmp44:
	movq	%r9, 88(%rsp)           # 8-byte Spill
	callq	_Z21btAlignedFreeInternalPv
.Ltmp45:
# BB#156:                               # %.noexc106
                                        #   in Loop: Header=BB9_142 Depth=1
	movq	88(%rsp), %r9           # 8-byte Reload
	movl	(%r9), %eax
	movl	12(%rsp), %r11d         # 4-byte Reload
.LBB9_157:                              #   in Loop: Header=BB9_142 Depth=1
	movq	$0, (%rbp)
.LBB9_158:                              # %_ZN20btAlignedObjectArrayIP10btDbvtNodeE10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB9_142 Depth=1
	leaq	24(%rsp,%r12), %rcx
	movb	$1, (%r13)
	movq	%rbx, (%rbp)
	movl	%r15d, (%rcx)
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movl	$1, %edi
	movq	104(%rsp), %rdx         # 8-byte Reload
.LBB9_159:                              #   in Loop: Header=BB9_142 Depth=1
	movq	32(%rsp,%r12), %rcx
	cltq
	movq	(%rdx,%r14,8), %rdx
	movq	%rdx, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, (%r9)
	incq	%r14
	cmpq	%rsi, %r14
	jl	.LBB9_142
.LBB9_160:                              # %_ZL5splitRK20btAlignedObjectArrayIP10btDbvtNodeERS2_S5_RK9btVector3S8_.exit
	movq	192(%rsp), %r15         # 8-byte Reload
	movq	8(%r15), %rbp
	testq	%rbp, %rbp
	je	.LBB9_162
# BB#161:
	movq	$0, 8(%r15)
	movl	132(%rsp), %r14d        # 4-byte Reload
	movl	128(%rsp), %eax         # 4-byte Reload
	movl	124(%rsp), %ecx         # 4-byte Reload
	jmp	.LBB9_164
.LBB9_162:
.Ltmp68:
	movl	$56, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movl	12(%rsp), %r11d         # 4-byte Reload
	movq	%rax, %rbp
.Ltmp69:
	movl	132(%rsp), %r14d        # 4-byte Reload
	movl	128(%rsp), %eax         # 4-byte Reload
	movl	124(%rsp), %ecx         # 4-byte Reload
# BB#163:                               # %.noexc102
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movq	$0, 48(%rbp)
.LBB9_164:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbp)
	movq	$0, 48(%rbp)
	movl	%eax, (%rbp)
	movl	188(%rsp), %eax         # 4-byte Reload
	movl	%eax, 4(%rbp)
	movl	184(%rsp), %eax         # 4-byte Reload
	movl	%eax, 8(%rbp)
	movl	172(%rsp), %eax         # 4-byte Reload
	movl	%eax, 12(%rbp)
	movl	%ecx, 16(%rbp)
	movl	%r11d, 20(%rbp)
	movl	180(%rsp), %eax         # 4-byte Reload
	movl	%eax, 24(%rbp)
	movl	176(%rsp), %eax         # 4-byte Reload
	movl	%eax, 28(%rbp)
.Ltmp71:
	leaq	16(%rsp), %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	movl	%r14d, %edx
	callq	_ZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEi
.Ltmp72:
# BB#165:
	movq	%rax, 40(%rbp)
	leaq	48(%rsp), %rbx
.Ltmp73:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movl	%r14d, %edx
	callq	_ZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEi
.Ltmp74:
# BB#166:
	movq	%rax, 48(%rbp)
	movq	40(%rbp), %rax
	movq	%rbp, 32(%rax)
	movq	48(%rbp), %rax
	movq	%rbp, 32(%rax)
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_170
# BB#167:
	cmpb	$0, 72(%rsp)
	je	.LBB9_169
# BB#168:
.Ltmp81:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp82:
.LBB9_169:                              # %.noexc100
	movq	$0, 64(%rsp)
.LBB9_170:
	movb	$1, 72(%rsp)
	movq	$0, 64(%rsp)
	movq	$0, 52(%rsp)
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_194
# BB#171:
	cmpb	$0, 40(%rsp)
	je	.LBB9_173
# BB#172:
.Ltmp83:
	movq	%r12, %rbx
	callq	_Z21btAlignedFreeInternalPv
.Ltmp84:
.LBB9_173:                              # %.noexc100.1
	movq	$0, 32(%rsp)
	jmp	.LBB9_194
.LBB9_185:
.Ltmp85:
	movq	%rax, %rbp
	cmpq	%rbx, %r12
	jne	.LBB9_187
	jmp	.LBB9_184
.LBB9_191:                              #   in Loop: Header=BB9_187 Depth=1
	movb	$1, -8(%rbx)
	movq	$0, -16(%rbx)
	movq	$0, -28(%rbx)
	leaq	-32(%rbx), %rbx
	cmpq	%rbx, %r12
	je	.LBB9_184
.LBB9_187:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	-16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_191
# BB#188:                               #   in Loop: Header=BB9_187 Depth=1
	cmpb	$0, -8(%rbx)
	je	.LBB9_190
# BB#189:                               #   in Loop: Header=BB9_187 Depth=1
.Ltmp86:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp87:
.LBB9_190:                              # %.noexc97
                                        #   in Loop: Header=BB9_187 Depth=1
	movq	$0, -16(%rbx)
	jmp	.LBB9_191
.LBB9_195:                              # %.loopexit
.Ltmp88:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_204:
.Ltmp46:
	jmp	.LBB9_175
.LBB9_14:                               # %.loopexit.split-lp263
.Ltmp70:
	jmp	.LBB9_175
.LBB9_174:
.Ltmp75:
	jmp	.LBB9_175
.LBB9_13:                               # %.loopexit262
.Ltmp67:
.LBB9_175:
	movq	%rax, %rbp
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_179
# BB#176:
	cmpb	$0, 72(%rsp)
	je	.LBB9_178
# BB#177:
.Ltmp76:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp77:
.LBB9_178:                              # %.noexc
	movq	$0, 64(%rsp)
.LBB9_179:
	movb	$1, 72(%rsp)
	movq	$0, 64(%rsp)
	movq	$0, 52(%rsp)
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_183
# BB#180:
	cmpb	$0, 40(%rsp)
	je	.LBB9_182
# BB#181:
.Ltmp78:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp79:
.LBB9_182:                              # %.noexc.1
	movq	$0, 32(%rsp)
.LBB9_183:                              # %.loopexit260.loopexit277280
	movb	$1, 40(%rsp)
	movq	$0, 32(%rsp)
	movq	$0, 20(%rsp)
.LBB9_184:                              # %.loopexit260
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB9_196:                              # %.loopexit.split-lp
.Ltmp80:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEi, .Lfunc_end9-_ZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp47-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp47
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp58-.Ltmp47         #   Call between .Ltmp47 and .Ltmp58
	.long	.Ltmp70-.Lfunc_begin3   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp62-.Ltmp63         #   Call between .Ltmp63 and .Ltmp62
	.long	.Ltmp67-.Lfunc_begin3   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp41-.Ltmp36         #   Call between .Ltmp36 and .Ltmp41
	.long	.Ltmp70-.Lfunc_begin3   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp45-.Ltmp42         #   Call between .Ltmp42 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin3   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin3   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp74-.Ltmp71         #   Call between .Ltmp71 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin3   #     jumps to .Ltmp75
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp84-.Ltmp81         #   Call between .Ltmp81 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin3   #     jumps to .Ltmp85
	.byte	0                       #   On action: cleanup
	.long	.Ltmp86-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp87-.Ltmp86         #   Call between .Ltmp86 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin3   #     jumps to .Ltmp88
	.byte	1                       #   On action: 1
	.long	.Ltmp76-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp79-.Ltmp76         #   Call between .Ltmp76 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin3   #     jumps to .Ltmp80
	.byte	1                       #   On action: 1
	.long	.Ltmp79-.Lfunc_begin3   # >> Call Site 11 <<
	.long	.Lfunc_end9-.Ltmp79     #   Call between .Ltmp79 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN6btDbvt19optimizeIncrementalEi
	.p2align	4, 0x90
	.type	_ZN6btDbvt19optimizeIncrementalEi,@function
_ZN6btDbvt19optimizeIncrementalEi:      # @_ZN6btDbvt19optimizeIncrementalEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi67:
	.cfi_def_cfa_offset 80
.Lcfi68:
	.cfi_offset %rbx, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	testl	%r14d, %r14d
	jns	.LBB10_2
# BB#1:
	movl	20(%r15), %r14d
.LBB10_2:
	testl	%r14d, %r14d
	jle	.LBB10_18
# BB#3:
	movq	(%r15), %rbp
	testq	%rbp, %rbp
	jne	.LBB10_4
	jmp	.LBB10_18
	.p2align	4, 0x90
.LBB10_13:                              # %._crit_edge
                                        #   in Loop: Header=BB10_4 Depth=1
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	_ZL10removeleafP6btDbvtP10btDbvtNode
	testq	%rax, %rax
	je	.LBB10_14
# BB#15:                                #   in Loop: Header=BB10_4 Depth=1
	movq	(%r15), %rsi
	jmp	.LBB10_16
	.p2align	4, 0x90
.LBB10_14:                              #   in Loop: Header=BB10_4 Depth=1
	xorl	%esi, %esi
.LBB10_16:                              # %_ZN6btDbvt6updateEP10btDbvtNodei.exit
                                        #   in Loop: Header=BB10_4 Depth=1
	movq	%r15, %rdi
	movq	%rbp, %rdx
	callq	_ZL10insertleafP6btDbvtP10btDbvtNodeS2_
	incl	24(%r15)
	decl	%r14d
	je	.LBB10_18
# BB#17:                                # %_ZN6btDbvt6updateEP10btDbvtNodei.exit._crit_edge
                                        #   in Loop: Header=BB10_4 Depth=1
	movq	(%r15), %rbp
.LBB10_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_6 Depth 2
	cmpq	$0, 48(%rbp)
	je	.LBB10_13
# BB#5:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB10_4 Depth=1
	leaq	48(%rbp), %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_6:                               # %.lr.ph
                                        #   Parent Loop BB10_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%rbp), %rax
	cmpq	%rbp, %rax
	jbe	.LBB10_7
# BB#8:                                 #   in Loop: Header=BB10_6 Depth=2
	movq	32(%rax), %rbx
	movq	48(%rax), %r9
	xorl	%esi, %esi
	cmpq	%rbp, %r9
	setne	%sil
	movq	40(%rax,%rsi,8), %r8
	testq	%rbx, %rbx
	je	.LBB10_9
# BB#10:                                #   in Loop: Header=BB10_6 Depth=2
	xorl	%edi, %edi
	cmpq	%rax, 48(%rbx)
	sete	%dil
	leaq	40(%rbx,%rdi,8), %r10
	jmp	.LBB10_11
	.p2align	4, 0x90
.LBB10_7:                               #   in Loop: Header=BB10_6 Depth=2
	movq	%rbp, %rax
	jmp	.LBB10_12
.LBB10_9:                               #   in Loop: Header=BB10_6 Depth=2
	movq	%r15, %r10
.LBB10_11:                              #   in Loop: Header=BB10_6 Depth=2
	xorl	%edi, %edi
	cmpq	%rbp, %r9
	sete	%dil
	movq	%rbp, (%r10)
	movq	%rbp, 32(%r8)
	movq	%rbp, 32(%rax)
	movq	%rbx, 32(%rbp)
	movq	40(%rbp), %rbx
	movq	%rbx, 40(%rax)
	movq	(%rdx), %rbx
	movq	%rbx, 48(%rax)
	movq	40(%rbp), %rbx
	movq	%rax, 32(%rbx)
	movq	(%rdx), %rdx
	movq	%rax, 32(%rdx)
	movq	%rax, 40(%rbp,%rdi,8)
	movq	%r8, 40(%rbp,%rsi,8)
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movaps	(%rsp), %xmm0
	movaps	16(%rsp), %xmm1
	movups	%xmm1, 16(%rbp)
	movups	%xmm0, (%rbp)
.LBB10_12:                              # %_ZL4sortP10btDbvtNodeRS0_.exit
                                        #   in Loop: Header=BB10_6 Depth=2
	movl	24(%r15), %edx
	shrl	%cl, %edx
	andl	$1, %edx
	incl	%ecx
	andl	$31, %ecx
	movq	40(%rax,%rdx,8), %rbp
	leaq	48(%rbp), %rdx
	cmpq	$0, 48(%rbp)
	jne	.LBB10_6
	jmp	.LBB10_13
.LBB10_18:                              # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN6btDbvt19optimizeIncrementalEi, .Lfunc_end10-_ZN6btDbvt19optimizeIncrementalEi
	.cfi_endproc

	.globl	_ZN6btDbvt6updateEP10btDbvtNodei
	.p2align	4, 0x90
	.type	_ZN6btDbvt6updateEP10btDbvtNodei,@function
_ZN6btDbvt6updateEP10btDbvtNodei:       # @_ZN6btDbvt6updateEP10btDbvtNodei
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 32
.Lcfi75:
	.cfi_offset %rbx, -32
.Lcfi76:
	.cfi_offset %r14, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZL10removeleafP6btDbvtP10btDbvtNode
	testq	%rax, %rax
	je	.LBB11_1
# BB#2:
	testl	%ebp, %ebp
	js	.LBB11_8
# BB#3:                                 # %.preheader
	je	.LBB11_4
# BB#5:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rsi
	movq	32(%rsi), %rax
	testq	%rax, %rax
	je	.LBB11_9
# BB#6:                                 #   in Loop: Header=BB11_7 Depth=1
	incl	%ecx
	cmpl	%ebp, %ecx
	movq	%rax, %rsi
	jl	.LBB11_7
	jmp	.LBB11_9
.LBB11_1:
	xorl	%esi, %esi
	jmp	.LBB11_9
.LBB11_8:
	movq	(%rbx), %rsi
	jmp	.LBB11_9
.LBB11_4:
	movq	%rax, %rsi
.LBB11_9:                               # %.critedge
	movq	%rbx, %rdi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	_ZL10insertleafP6btDbvtP10btDbvtNodeS2_ # TAILCALL
.Lfunc_end11:
	.size	_ZN6btDbvt6updateEP10btDbvtNodei, .Lfunc_end11-_ZN6btDbvt6updateEP10btDbvtNodei
	.cfi_endproc

	.globl	_ZN6btDbvt6insertERK12btDbvtAabbMmPv
	.p2align	4, 0x90
	.type	_ZN6btDbvt6insertERK12btDbvtAabbMmPv,@function
_ZN6btDbvt6insertERK12btDbvtAabbMmPv:   # @_ZN6btDbvt6insertERK12btDbvtAabbMmPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi82:
	.cfi_def_cfa_offset 48
.Lcfi83:
	.cfi_offset %rbx, -40
.Lcfi84:
	.cfi_offset %r12, -32
.Lcfi85:
	.cfi_offset %r14, -24
.Lcfi86:
	.cfi_offset %r15, -16
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	8(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB12_2
# BB#1:
	movq	$0, 8(%r15)
	jmp	.LBB12_3
.LBB12_2:
	movl	$56, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 48(%rbx)
.LBB12_3:                               # %_ZL10createnodeP6btDbvtP10btDbvtNodeRK12btDbvtAabbMmPv.exit
	movq	$0, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 48(%rbx)
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	(%r15), %rsi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	_ZL10insertleafP6btDbvtP10btDbvtNodeS2_
	incl	20(%r15)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	_ZN6btDbvt6insertERK12btDbvtAabbMmPv, .Lfunc_end12-_ZN6btDbvt6insertERK12btDbvtAabbMmPv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.p2align	4, 0x90
	.type	_ZL10insertleafP6btDbvtP10btDbvtNodeS2_,@function
_ZL10insertleafP6btDbvtP10btDbvtNodeS2_: # @_ZL10insertleafP6btDbvtP10btDbvtNodeS2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi89:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi90:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi91:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 64
.Lcfi94:
	.cfi_offset %rbx, -56
.Lcfi95:
	.cfi_offset %r12, -48
.Lcfi96:
	.cfi_offset %r13, -40
.Lcfi97:
	.cfi_offset %r14, -32
.Lcfi98:
	.cfi_offset %r15, -24
.Lcfi99:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	cmpq	$0, (%r15)
	je	.LBB13_1
# BB#2:
	movq	48(%r12), %rax
	testq	%rax, %rax
	je	.LBB13_5
# BB#3:                                 # %.preheader
	movss	(%r14), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	16(%r14), %xmm8
	addss	20(%r14), %xmm1
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	addss	24(%r14), %xmm2
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movaps	.LCPI13_0(%rip), %xmm3  # xmm3 = [nan,nan,nan,nan]
	.p2align	4, 0x90
.LBB13_4:                               # =>This Inner Loop Header: Depth=1
	movq	40(%r12), %rcx
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	movss	16(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	16(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	addps	%xmm6, %xmm0
	unpcklps	%xmm5, %xmm7    # xmm7 = xmm7[0],xmm5[0],xmm7[1],xmm5[1]
	movss	20(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	addps	%xmm7, %xmm5
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	8(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	movss	24(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	24(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1]
	addps	%xmm6, %xmm7
	movaps	%xmm8, %xmm4
	subps	%xmm0, %xmm4
	movaps	%xmm1, %xmm0
	subps	%xmm5, %xmm0
	movaps	%xmm2, %xmm5
	subps	%xmm7, %xmm5
	andps	%xmm3, %xmm4
	andps	%xmm3, %xmm0
	addps	%xmm4, %xmm0
	andps	%xmm3, %xmm5
	addps	%xmm0, %xmm5
	movaps	%xmm5, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	xorl	%eax, %eax
	ucomiss	%xmm5, %xmm0
	setbe	%al
	movq	40(%r12,%rax,8), %r12
	movq	48(%r12), %rax
	testq	%rax, %rax
	jne	.LBB13_4
.LBB13_5:                               # %.loopexit49
	movq	32(%r12), %rbx
	movq	8(%r15), %rax
	testq	%rax, %rax
	je	.LBB13_7
# BB#6:
	movq	$0, 8(%r15)
	jmp	.LBB13_8
.LBB13_1:
	movq	%r14, (%r15)
	movq	$0, 32(%r14)
	jmp	.LBB13_19
.LBB13_7:
	movl	$56, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	$0, 48(%rax)
.LBB13_8:                               # %_ZL10createnodeP6btDbvtP10btDbvtNodeRK12btDbvtAabbMmS5_Pv.exit
	movq	%rbx, 32(%rax)
	leaq	16(%r14), %r10
	leaq	16(%r12), %rcx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rax)
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%r14), %xmm0
	movq	%r12, %rdx
	cmovaq	%r14, %rdx
	movl	(%rdx), %r13d
	movl	%r13d, (%rax)
	movss	16(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	16(%r12), %xmm0
	movq	%rcx, %rsi
	cmovaq	%r10, %rsi
	movl	(%rsi), %r8d
	movl	%r8d, 16(%rax)
	movss	4(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%r14), %xmm0
	movq	%r12, %rsi
	cmovaq	%r14, %rsi
	movl	4(%rsi), %edi
	movl	%edi, 4(%rax)
	movss	20(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	20(%r12), %xmm0
	movq	%rcx, %rsi
	cmovaq	%r10, %rsi
	movl	4(%rsi), %r9d
	movl	%r9d, 20(%rax)
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%r14), %xmm0
	movq	%r12, %rsi
	cmovaq	%r14, %rsi
	movl	8(%rsi), %esi
	movl	%esi, 8(%rax)
	movd	24(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	24(%r12), %xmm0
	cmovaq	%r10, %rcx
	movl	8(%rcx), %ecx
	movl	%ecx, 24(%rax)
	testq	%rbx, %rbx
	je	.LBB13_18
# BB#9:
	movq	32(%r12), %r10
	xorl	%r11d, %r11d
	cmpq	%r12, 48(%r10)
	sete	%r11b
	movq	%rax, 40(%rbx,%r11,8)
	movq	%r12, 40(%rax)
	movq	%rax, 32(%r12)
	movq	%r14, 48(%rax)
	movq	%rax, 32(%r14)
	.p2align	4, 0x90
.LBB13_10:                              # =>This Inner Loop Header: Depth=1
	movd	%r13d, %xmm0
	ucomiss	(%rbx), %xmm0
	jae	.LBB13_12
# BB#11:                                # %._ZNK12btDbvtAabbMm7ContainERKS_.exit.thread_crit_edge
                                        #   in Loop: Header=BB13_10 Depth=1
	leaq	4(%rbx), %r10
	jmp	.LBB13_17
	.p2align	4, 0x90
.LBB13_12:                              #   in Loop: Header=BB13_10 Depth=1
	movd	%edi, %xmm0
	leaq	4(%rbx), %r10
	ucomiss	4(%rbx), %xmm0
	jb	.LBB13_17
# BB#13:                                #   in Loop: Header=BB13_10 Depth=1
	movd	%esi, %xmm0
	ucomiss	8(%rbx), %xmm0
	jb	.LBB13_17
# BB#14:                                #   in Loop: Header=BB13_10 Depth=1
	movd	%r8d, %xmm0
	movss	16(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB13_17
# BB#15:                                #   in Loop: Header=BB13_10 Depth=1
	movd	%r9d, %xmm0
	movss	20(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB13_17
# BB#16:                                # %_ZNK12btDbvtAabbMm7ContainERKS_.exit
                                        #   in Loop: Header=BB13_10 Depth=1
	movd	%ecx, %xmm0
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB13_19
	.p2align	4, 0x90
.LBB13_17:                              # %_ZNK12btDbvtAabbMm7ContainERKS_.exit.thread
                                        #   in Loop: Header=BB13_10 Depth=1
	movq	40(%rbx), %rdx
	movq	48(%rbx), %rbp
	leaq	16(%rdx), %rcx
	leaq	16(%rbp), %rax
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rdx), %xmm0
	movq	%rbp, %rsi
	cmovaq	%rdx, %rsi
	movl	(%rsi), %r13d
	movl	%r13d, (%rbx)
	movss	16(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	16(%rbp), %xmm0
	movq	%rax, %rsi
	cmovaq	%rcx, %rsi
	movl	(%rsi), %r8d
	movl	%r8d, 16(%rbx)
	movss	4(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%rdx), %xmm0
	movq	%rbp, %rsi
	cmovaq	%rdx, %rsi
	movl	4(%rsi), %edi
	movl	%edi, (%r10)
	movss	20(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	20(%rbp), %xmm0
	movq	%rax, %rsi
	cmovaq	%rcx, %rsi
	movl	4(%rsi), %r9d
	movl	%r9d, 20(%rbx)
	movss	8(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%rdx), %xmm0
	movq	%rbp, %rsi
	cmovaq	%rdx, %rsi
	movl	8(%rsi), %esi
	movl	%esi, 8(%rbx)
	movd	24(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	24(%rbp), %xmm0
	cmovaq	%rcx, %rax
	movl	8(%rax), %ecx
	movl	%ecx, 24(%rbx)
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB13_10
	jmp	.LBB13_19
.LBB13_18:
	movq	%r12, 40(%rax)
	movq	%rax, 32(%r12)
	movq	%r14, 48(%rax)
	movq	%rax, 32(%r14)
	movq	%rax, (%r15)
.LBB13_19:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZL10insertleafP6btDbvtP10btDbvtNodeS2_, .Lfunc_end13-_ZL10insertleafP6btDbvtP10btDbvtNodeS2_
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL10removeleafP6btDbvtP10btDbvtNode,@function
_ZL10removeleafP6btDbvtP10btDbvtNode:   # @_ZL10removeleafP6btDbvtP10btDbvtNode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 32
.Lcfi103:
	.cfi_offset %rbx, -32
.Lcfi104:
	.cfi_offset %r14, -24
.Lcfi105:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	cmpq	%rsi, (%r14)
	je	.LBB14_1
# BB#2:
	movq	32(%rsi), %r15
	xorl	%eax, %eax
	cmpq	%rsi, 48(%r15)
	movq	32(%r15), %rbx
	setne	%al
	movq	40(%r15,%rax,8), %rax
	testq	%rbx, %rbx
	je	.LBB14_11
# BB#3:
	xorl	%ecx, %ecx
	cmpq	%r15, 48(%rbx)
	sete	%cl
	movq	%rax, 40(%rbx,%rcx,8)
	movq	%rbx, 32(%rax)
	movq	8(%r14), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	%r15, 8(%r14)
	.p2align	4, 0x90
.LBB14_4:                               # =>This Inner Loop Header: Depth=1
	movd	(%rbx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movd	4(%rbx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movd	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movd	16(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movd	20(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movq	40(%rbx), %rcx
	movq	48(%rbx), %rdi
	leaq	16(%rcx), %rdx
	leaq	16(%rdi), %rsi
	movss	(%rdi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	ucomiss	(%rcx), %xmm6
	movq	%rdi, %rax
	cmovaq	%rcx, %rax
	movl	(%rax), %r11d
	movl	%r11d, (%rbx)
	movss	16(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	ucomiss	16(%rdi), %xmm6
	movq	%rsi, %rax
	cmovaq	%rdx, %rax
	movl	(%rax), %r8d
	movl	%r8d, 16(%rbx)
	movss	4(%rdi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	ucomiss	4(%rcx), %xmm6
	movq	%rdi, %rax
	cmovaq	%rcx, %rax
	movl	4(%rax), %r10d
	movl	%r10d, 4(%rbx)
	movss	20(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	ucomiss	20(%rdi), %xmm6
	movq	%rsi, %rax
	cmovaq	%rdx, %rax
	movl	4(%rax), %r9d
	movl	%r9d, 20(%rbx)
	movss	8(%rdi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	ucomiss	8(%rcx), %xmm6
	movq	%rdi, %rax
	cmovaq	%rcx, %rax
	movl	8(%rax), %eax
	movl	%eax, 8(%rbx)
	movss	24(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	ucomiss	24(%rdi), %xmm6
	cmovaq	%rdx, %rsi
	movl	8(%rsi), %edx
	movl	%edx, 24(%rbx)
	movd	%r11d, %xmm6
	ucomiss	%xmm6, %xmm5
	jne	.LBB14_10
	jp	.LBB14_10
# BB#5:                                 #   in Loop: Header=BB14_4 Depth=1
	movd	%r10d, %xmm5
	ucomiss	%xmm5, %xmm4
	jne	.LBB14_10
	jp	.LBB14_10
# BB#6:                                 #   in Loop: Header=BB14_4 Depth=1
	movd	%eax, %xmm4
	ucomiss	%xmm4, %xmm3
	jne	.LBB14_10
	jp	.LBB14_10
# BB#7:                                 #   in Loop: Header=BB14_4 Depth=1
	movd	%r8d, %xmm3
	ucomiss	%xmm3, %xmm2
	jne	.LBB14_10
	jp	.LBB14_10
# BB#8:                                 #   in Loop: Header=BB14_4 Depth=1
	movd	%r9d, %xmm2
	ucomiss	%xmm2, %xmm1
	jne	.LBB14_10
	jp	.LBB14_10
# BB#9:                                 #   in Loop: Header=BB14_4 Depth=1
	movd	%edx, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB14_10
	jnp	.LBB14_13
.LBB14_10:                              # %_Z8NotEqualRK12btDbvtAabbMmS1_.exit.thread
                                        #   in Loop: Header=BB14_4 Depth=1
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB14_4
	jmp	.LBB14_12
.LBB14_1:
	movq	$0, (%r14)
	xorl	%ebx, %ebx
	jmp	.LBB14_13
.LBB14_11:
	movq	%rax, (%r14)
	movq	$0, 32(%rax)
	movq	8(%r14), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	%r15, 8(%r14)
.LBB14_12:                              # %.sink.split
	movq	(%r14), %rbx
.LBB14_13:                              # %.thread58
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	_ZL10removeleafP6btDbvtP10btDbvtNode, .Lfunc_end14-_ZL10removeleafP6btDbvtP10btDbvtNode
	.cfi_endproc

	.globl	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm
	.p2align	4, 0x90
	.type	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm,@function
_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm: # @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 32
.Lcfi109:
	.cfi_offset %rbx, -32
.Lcfi110:
	.cfi_offset %r14, -24
.Lcfi111:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	_ZL10removeleafP6btDbvtP10btDbvtNode
	testq	%rax, %rax
	je	.LBB15_1
# BB#2:
	movl	16(%r14), %ecx
	testl	%ecx, %ecx
	js	.LBB15_8
# BB#3:                                 # %.preheader
	je	.LBB15_4
# BB#5:                                 # %.lr.ph.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB15_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rsi
	movq	32(%rsi), %rax
	testq	%rax, %rax
	je	.LBB15_9
# BB#6:                                 #   in Loop: Header=BB15_7 Depth=1
	incl	%edx
	cmpl	%ecx, %edx
	movq	%rax, %rsi
	jl	.LBB15_7
	jmp	.LBB15_9
.LBB15_1:
	xorl	%esi, %esi
	jmp	.LBB15_9
.LBB15_8:
	movq	(%r14), %rsi
	jmp	.LBB15_9
.LBB15_4:
	movq	%rax, %rsi
.LBB15_9:                               # %.critedge
	movups	(%r15), %xmm0
	movups	16(%r15), %xmm1
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	%r14, %rdi
	movq	%rbx, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZL10insertleafP6btDbvtP10btDbvtNodeS2_ # TAILCALL
.Lfunc_end15:
	.size	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm, .Lfunc_end15-_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm
	.cfi_endproc

	.globl	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3f
	.p2align	4, 0x90
	.type	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3f,@function
_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3f: # @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3f
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 32
.Lcfi115:
	.cfi_offset %rbx, -32
.Lcfi116:
	.cfi_offset %r14, -24
.Lcfi117:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r14
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	(%r15), %xmm1
	jae	.LBB16_4
# BB#1:                                 # %._ZNK12btDbvtAabbMm7ContainERKS_.exit.thread_crit_edge
	leaq	4(%rbx), %r8
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB16_2
.LBB16_4:
	leaq	4(%rbx), %r8
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	ucomiss	4(%r15), %xmm2
	jb	.LBB16_2
# BB#5:
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	ucomiss	8(%r15), %xmm3
	jb	.LBB16_2
# BB#6:
	movss	16(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	ucomiss	16(%rbx), %xmm3
	jb	.LBB16_2
# BB#7:
	movss	20(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	ucomiss	20(%rbx), %xmm3
	jb	.LBB16_2
# BB#8:                                 # %_ZNK12btDbvtAabbMm7ContainERKS_.exit
	movss	24(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	ucomiss	24(%rbx), %xmm3
	jae	.LBB16_9
.LBB16_2:                               # %_ZNK12btDbvtAabbMm7ContainERKS_.exit.thread
	subss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
	subss	%xmm0, %xmm2
	movss	%xmm2, (%r8)
	leaq	8(%rbx), %rdx
	movss	8(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 8(%rbx)
	leaq	16(%rbx), %rsi
	movss	16(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 16(%rbx)
	leaq	20(%rbx), %rdi
	movss	20(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 20(%rbx)
	leaq	24(%rbx), %rax
	addss	24(%rbx), %xmm0
	movss	%xmm0, 24(%rbx)
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	cmovbeq	%rbx, %rsi
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	movss	4(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	cmovbeq	%r8, %rdi
	addss	(%rdi), %xmm0
	movss	%xmm0, (%rdi)
	movss	8(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	cmovbeq	%rdx, %rax
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZL10removeleafP6btDbvtP10btDbvtNode
	testq	%rax, %rax
	je	.LBB16_3
# BB#10:
	movl	16(%r14), %ecx
	testl	%ecx, %ecx
	js	.LBB16_16
# BB#11:                                # %.preheader.i
	je	.LBB16_12
# BB#13:                                # %.lr.ph.i.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB16_15:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rsi
	movq	32(%rsi), %rax
	testq	%rax, %rax
	je	.LBB16_17
# BB#14:                                #   in Loop: Header=BB16_15 Depth=1
	incl	%edx
	cmpl	%ecx, %edx
	movq	%rax, %rsi
	jl	.LBB16_15
	jmp	.LBB16_17
.LBB16_3:
	xorl	%esi, %esi
	jmp	.LBB16_17
.LBB16_16:
	movq	(%r14), %rsi
	jmp	.LBB16_17
.LBB16_12:
	movq	%rax, %rsi
.LBB16_17:                              # %_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm.exit
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	%xmm1, 16(%r15)
	movups	%xmm0, (%r15)
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	_ZL10insertleafP6btDbvtP10btDbvtNodeS2_
	movb	$1, %al
.LBB16_18:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB16_9:
	xorl	%eax, %eax
	jmp	.LBB16_18
.Lfunc_end16:
	.size	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3f, .Lfunc_end16-_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3f
	.cfi_endproc

	.globl	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3
	.p2align	4, 0x90
	.type	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3,@function
_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3: # @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 32
.Lcfi121:
	.cfi_offset %rbx, -32
.Lcfi122:
	.cfi_offset %r14, -24
.Lcfi123:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r14
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%r15), %xmm0
	jae	.LBB17_4
# BB#1:                                 # %._ZNK12btDbvtAabbMm7ContainERKS_.exit.thread_crit_edge
	leaq	4(%rbx), %rax
	jmp	.LBB17_2
.LBB17_4:
	leaq	4(%rbx), %rax
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%r15), %xmm0
	jb	.LBB17_2
# BB#5:
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%r15), %xmm0
	jb	.LBB17_2
# BB#6:
	movss	16(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	16(%rbx), %xmm0
	jb	.LBB17_2
# BB#7:
	movss	20(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	20(%rbx), %xmm0
	jb	.LBB17_2
# BB#8:                                 # %_ZNK12btDbvtAabbMm7ContainERKS_.exit
	movss	24(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	24(%rbx), %xmm0
	jae	.LBB17_9
.LBB17_2:                               # %_ZNK12btDbvtAabbMm7ContainERKS_.exit.thread
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	leaq	16(%rbx), %rdx
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	cmovbeq	%rbx, %rdx
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	movss	4(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	leaq	20(%rbx), %rdx
	ucomiss	%xmm1, %xmm0
	cmovbeq	%rax, %rdx
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	movss	8(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	leaq	8(%rbx), %rax
	leaq	24(%rbx), %rcx
	ucomiss	%xmm1, %xmm0
	cmovaq	%rcx, %rax
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZL10removeleafP6btDbvtP10btDbvtNode
	testq	%rax, %rax
	je	.LBB17_3
# BB#10:
	movl	16(%r14), %ecx
	testl	%ecx, %ecx
	js	.LBB17_16
# BB#11:                                # %.preheader.i
	je	.LBB17_12
# BB#13:                                # %.lr.ph.i.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB17_15:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rsi
	movq	32(%rsi), %rax
	testq	%rax, %rax
	je	.LBB17_17
# BB#14:                                #   in Loop: Header=BB17_15 Depth=1
	incl	%edx
	cmpl	%ecx, %edx
	movq	%rax, %rsi
	jl	.LBB17_15
	jmp	.LBB17_17
.LBB17_3:
	xorl	%esi, %esi
	jmp	.LBB17_17
.LBB17_16:
	movq	(%r14), %rsi
	jmp	.LBB17_17
.LBB17_12:
	movq	%rax, %rsi
.LBB17_17:                              # %_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm.exit
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	%xmm1, 16(%r15)
	movups	%xmm0, (%r15)
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	_ZL10insertleafP6btDbvtP10btDbvtNodeS2_
	movb	$1, %al
.LBB17_18:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB17_9:
	xorl	%eax, %eax
	jmp	.LBB17_18
.Lfunc_end17:
	.size	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3, .Lfunc_end17-_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3
	.cfi_endproc

	.globl	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmf
	.p2align	4, 0x90
	.type	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmf,@function
_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmf: # @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmf
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 32
.Lcfi127:
	.cfi_offset %rbx, -32
.Lcfi128:
	.cfi_offset %r14, -24
.Lcfi129:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r14
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	(%r15), %xmm1
	jae	.LBB18_4
# BB#1:                                 # %._ZNK12btDbvtAabbMm7ContainERKS_.exit.thread_crit_edge
	leaq	4(%rbx), %rax
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB18_2
.LBB18_4:
	leaq	4(%rbx), %rax
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	ucomiss	4(%r15), %xmm2
	jb	.LBB18_2
# BB#5:
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	ucomiss	8(%r15), %xmm3
	jb	.LBB18_2
# BB#6:
	movss	16(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	ucomiss	16(%rbx), %xmm3
	jb	.LBB18_2
# BB#7:
	movss	20(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	ucomiss	20(%rbx), %xmm3
	jb	.LBB18_2
# BB#8:                                 # %_ZNK12btDbvtAabbMm7ContainERKS_.exit
	movss	24(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	ucomiss	24(%rbx), %xmm3
	jae	.LBB18_9
.LBB18_2:                               # %_ZNK12btDbvtAabbMm7ContainERKS_.exit.thread
	subss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
	subss	%xmm0, %xmm2
	movss	%xmm2, (%rax)
	movss	8(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 8(%rbx)
	movss	16(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 16(%rbx)
	movss	20(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 20(%rbx)
	addss	24(%rbx), %xmm0
	movss	%xmm0, 24(%rbx)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZL10removeleafP6btDbvtP10btDbvtNode
	testq	%rax, %rax
	je	.LBB18_3
# BB#10:
	movl	16(%r14), %ecx
	testl	%ecx, %ecx
	js	.LBB18_16
# BB#11:                                # %.preheader.i
	je	.LBB18_12
# BB#13:                                # %.lr.ph.i.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB18_15:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rsi
	movq	32(%rsi), %rax
	testq	%rax, %rax
	je	.LBB18_17
# BB#14:                                #   in Loop: Header=BB18_15 Depth=1
	incl	%edx
	cmpl	%ecx, %edx
	movq	%rax, %rsi
	jl	.LBB18_15
	jmp	.LBB18_17
.LBB18_3:
	xorl	%esi, %esi
	jmp	.LBB18_17
.LBB18_16:
	movq	(%r14), %rsi
	jmp	.LBB18_17
.LBB18_12:
	movq	%rax, %rsi
.LBB18_17:                              # %_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm.exit
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	%xmm1, 16(%r15)
	movups	%xmm0, (%r15)
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	_ZL10insertleafP6btDbvtP10btDbvtNodeS2_
	movb	$1, %al
.LBB18_18:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB18_9:
	xorl	%eax, %eax
	jmp	.LBB18_18
.Lfunc_end18:
	.size	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmf, .Lfunc_end18-_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmf
	.cfi_endproc

	.globl	_ZN6btDbvt6removeEP10btDbvtNode
	.p2align	4, 0x90
	.type	_ZN6btDbvt6removeEP10btDbvtNode,@function
_ZN6btDbvt6removeEP10btDbvtNode:        # @_ZN6btDbvt6removeEP10btDbvtNode
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi132:
	.cfi_def_cfa_offset 32
.Lcfi133:
	.cfi_offset %rbx, -24
.Lcfi134:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZL10removeleafP6btDbvtP10btDbvtNode
	movq	8(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	%r14, 8(%rbx)
	decl	20(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end19:
	.size	_ZN6btDbvt6removeEP10btDbvtNode, .Lfunc_end19-_ZN6btDbvt6removeEP10btDbvtNode
	.cfi_endproc

	.globl	_ZNK6btDbvt5writeEPNS_7IWriterE
	.p2align	4, 0x90
	.type	_ZNK6btDbvt5writeEPNS_7IWriterE,@function
_ZNK6btDbvt5writeEPNS_7IWriterE:        # @_ZNK6btDbvt5writeEPNS_7IWriterE
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi135:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi136:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi137:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi139:
	.cfi_def_cfa_offset 80
.Lcfi140:
	.cfi_offset %rbx, -40
.Lcfi141:
	.cfi_offset %r12, -32
.Lcfi142:
	.cfi_offset %r14, -24
.Lcfi143:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	$_ZTV20btDbvtNodeEnumerator+16, (%rsp)
	movb	$1, 32(%rsp)
	movq	$0, 24(%rsp)
	movq	$0, 12(%rsp)
	movslq	20(%r15), %r12
	testq	%r12, %r12
	jle	.LBB20_12
# BB#1:
	addq	%r12, %r12
	leaq	(,%r12,8), %rdi
.Ltmp89:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp90:
# BB#2:                                 # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi.exit.i
	movslq	12(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB20_7
# BB#3:                                 # %.lr.ph.i.i
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB20_5
	.p2align	4, 0x90
.LBB20_4:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	%rdi, (%rbx,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB20_4
.LBB20_5:                               # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB20_7
	.p2align	4, 0x90
.LBB20_6:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, (%rbx,%rcx,8)
	movq	24(%rsp), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	%rdx, 8(%rbx,%rcx,8)
	movq	24(%rsp), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	%rdx, 16(%rbx,%rcx,8)
	movq	24(%rsp), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%rbx,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB20_6
.LBB20_7:                               # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_.exit.i
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_11
# BB#8:
	cmpb	$0, 32(%rsp)
	je	.LBB20_10
# BB#9:
.Ltmp91:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp92:
.LBB20_10:                              # %.noexc41
	movq	$0, 24(%rsp)
.LBB20_11:                              # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv.exit.i
	movb	$1, 32(%rsp)
	movq	%rbx, 24(%rsp)
	movl	%r12d, 16(%rsp)
.LBB20_12:                              # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi.exit
	movq	(%r15), %rdi
.Ltmp93:
	movq	%rsp, %rsi
	callq	_ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE
.Ltmp94:
# BB#13:
	movq	(%r14), %rax
	movq	(%r15), %rsi
	movl	12(%rsp), %edx
.Ltmp95:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp96:
# BB#14:                                # %.preheader
	movl	12(%rsp), %r9d
	testl	%r9d, %r9d
	jle	.LBB20_18
# BB#15:                                # %.lr.ph
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB20_16:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_25 Depth 2
                                        #     Child Loop BB20_32 Depth 2
                                        #     Child Loop BB20_36 Depth 2
	movq	24(%rsp), %rax
	movq	(%rax,%r15,8), %rsi
	movq	32(%rsi), %rdx
	testq	%rdx, %rdx
	je	.LBB20_17
# BB#23:                                #   in Loop: Header=BB20_16 Depth=1
	testl	%r9d, %r9d
	movl	%r9d, %ecx
	jle	.LBB20_28
# BB#24:                                # %.lr.ph.i
                                        #   in Loop: Header=BB20_16 Depth=1
	movslq	%r9d, %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB20_25:                              #   Parent Loop BB20_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdx, (%rax,%rcx,8)
	je	.LBB20_28
# BB#26:                                #   in Loop: Header=BB20_25 Depth=2
	incq	%rcx
	cmpq	%rdi, %rcx
	jl	.LBB20_25
# BB#27:                                #   in Loop: Header=BB20_16 Depth=1
	movl	%r9d, %ecx
	jmp	.LBB20_28
	.p2align	4, 0x90
.LBB20_17:                              #   in Loop: Header=BB20_16 Depth=1
	movl	$-1, %ecx
.LBB20_28:                              # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE16findLinearSearchERKS2_.exit
                                        #   in Loop: Header=BB20_16 Depth=1
	movq	48(%rsi), %rdx
	testq	%rdx, %rdx
	je	.LBB20_42
# BB#29:                                #   in Loop: Header=BB20_16 Depth=1
	testl	%r9d, %r9d
	jle	.LBB20_30
# BB#31:                                # %.lr.ph.i51
                                        #   in Loop: Header=BB20_16 Depth=1
	movq	40(%rsi), %rbx
	movslq	%r9d, %rdi
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB20_32:                              #   Parent Loop BB20_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, (%rax,%r8,8)
	je	.LBB20_35
# BB#33:                                #   in Loop: Header=BB20_32 Depth=2
	incq	%r8
	cmpq	%rdi, %r8
	jl	.LBB20_32
# BB#34:                                #   in Loop: Header=BB20_16 Depth=1
	movl	%r9d, %r8d
.LBB20_35:                              # %.lr.ph.i46
                                        #   in Loop: Header=BB20_16 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_36:                              #   Parent Loop BB20_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdx, (%rax,%rbx,8)
	je	.LBB20_38
# BB#37:                                #   in Loop: Header=BB20_36 Depth=2
	incq	%rbx
	cmpq	%rdi, %rbx
	jl	.LBB20_36
	jmp	.LBB20_39
	.p2align	4, 0x90
.LBB20_42:                              #   in Loop: Header=BB20_16 Depth=1
	movq	(%r14), %rax
.Ltmp101:
	movq	%r14, %rdi
	movl	%r15d, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	*32(%rax)
.Ltmp102:
	jmp	.LBB20_43
	.p2align	4, 0x90
.LBB20_30:                              #   in Loop: Header=BB20_16 Depth=1
	movl	%r9d, %r8d
	jmp	.LBB20_39
.LBB20_38:                              # %._ZNK20btAlignedObjectArrayIPK10btDbvtNodeE16findLinearSearchERKS2_.exit50.loopexit_crit_edge
                                        #   in Loop: Header=BB20_16 Depth=1
	movl	%ebx, %r9d
.LBB20_39:                              # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE16findLinearSearchERKS2_.exit50
                                        #   in Loop: Header=BB20_16 Depth=1
	movq	(%r14), %rax
.Ltmp98:
	movq	%r14, %rdi
	movl	%r15d, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	callq	*24(%rax)
.Ltmp99:
.LBB20_43:                              #   in Loop: Header=BB20_16 Depth=1
	incq	%r15
	movslq	12(%rsp), %r9
	cmpq	%r9, %r15
	jl	.LBB20_16
.LBB20_18:                              # %._crit_edge
	movq	$_ZTV20btDbvtNodeEnumerator+16, (%rsp)
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_22
# BB#19:
	cmpb	$0, 32(%rsp)
	je	.LBB20_21
# BB#20:
	callq	_Z21btAlignedFreeInternalPv
.LBB20_21:                              # %.noexc.i44
	movq	$0, 24(%rsp)
.LBB20_22:                              # %_ZN20btDbvtNodeEnumeratorD2Ev.exit45
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB20_44:
.Ltmp97:
	jmp	.LBB20_45
.LBB20_40:
.Ltmp103:
	jmp	.LBB20_45
.LBB20_41:
.Ltmp100:
.LBB20_45:
	movq	%rax, %rbx
	movq	$_ZTV20btDbvtNodeEnumerator+16, (%rsp)
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_49
# BB#46:
	cmpb	$0, 32(%rsp)
	je	.LBB20_48
# BB#47:
.Ltmp104:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp105:
.LBB20_48:                              # %.noexc.i
	movq	$0, 24(%rsp)
.LBB20_49:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB20_50:
.Ltmp106:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end20:
	.size	_ZNK6btDbvt5writeEPNS_7IWriterE, .Lfunc_end20-_ZNK6btDbvt5writeEPNS_7IWriterE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp89-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp96-.Ltmp89         #   Call between .Ltmp89 and .Ltmp96
	.long	.Ltmp97-.Lfunc_begin4   #     jumps to .Ltmp97
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin4  #     jumps to .Ltmp103
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin4  #     jumps to .Ltmp100
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp104-.Ltmp99        #   Call between .Ltmp99 and .Ltmp104
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin4  #     jumps to .Ltmp106
	.byte	1                       #   On action: 1
	.long	.Ltmp105-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Lfunc_end20-.Ltmp105   #   Call between .Ltmp105 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE,"axG",@progbits,_ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE,comdat
	.weak	_ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE
	.p2align	4, 0x90
	.type	_ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE,@function
_ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE: # @_ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi144:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi145:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi146:
	.cfi_def_cfa_offset 32
.Lcfi147:
	.cfi_offset %rbx, -32
.Lcfi148:
	.cfi_offset %r14, -24
.Lcfi149:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*24(%rax)
	cmpq	$0, 48(%rbx)
	je	.LBB21_3
# BB#1:                                 # %tailrecurse.preheader
	leaq	48(%rbx), %r15
	.p2align	4, 0x90
.LBB21_2:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rdi
	movq	%r14, %rsi
	callq	_ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE
	movq	(%r15), %rbx
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*24(%rax)
	leaq	48(%rbx), %r15
	cmpq	$0, 48(%rbx)
	jne	.LBB21_2
.LBB21_3:                               # %tailrecurse._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	_ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE, .Lfunc_end21-_ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE
	.cfi_endproc

	.section	.text._ZN20btDbvtNodeEnumeratorD2Ev,"axG",@progbits,_ZN20btDbvtNodeEnumeratorD2Ev,comdat
	.weak	_ZN20btDbvtNodeEnumeratorD2Ev
	.p2align	4, 0x90
	.type	_ZN20btDbvtNodeEnumeratorD2Ev,@function
_ZN20btDbvtNodeEnumeratorD2Ev:          # @_ZN20btDbvtNodeEnumeratorD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 16
.Lcfi151:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV20btDbvtNodeEnumerator+16, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_4
# BB#1:
	cmpb	$0, 32(%rbx)
	je	.LBB22_3
# BB#2:
	callq	_Z21btAlignedFreeInternalPv
.LBB22_3:                               # %.noexc
	movq	$0, 24(%rbx)
.LBB22_4:
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
	popq	%rbx
	retq
.Lfunc_end22:
	.size	_ZN20btDbvtNodeEnumeratorD2Ev, .Lfunc_end22-_ZN20btDbvtNodeEnumeratorD2Ev
	.cfi_endproc

	.text
	.globl	_ZNK6btDbvt5cloneERS_PNS_6ICloneE
	.p2align	4, 0x90
	.type	_ZNK6btDbvt5cloneERS_PNS_6ICloneE,@function
_ZNK6btDbvt5cloneERS_PNS_6ICloneE:      # @_ZNK6btDbvt5cloneERS_PNS_6ICloneE
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi155:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi156:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi158:
	.cfi_def_cfa_offset 144
.Lcfi159:
	.cfi_offset %rbx, -56
.Lcfi160:
	.cfi_offset %r12, -48
.Lcfi161:
	.cfi_offset %r13, -40
.Lcfi162:
	.cfi_offset %r14, -32
.Lcfi163:
	.cfi_offset %r15, -24
.Lcfi164:
	.cfi_offset %rbp, -16
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB23_2
# BB#1:
	movq	%rax, %rdi
	callq	_ZL17recursedeletenodeP6btDbvtP10btDbvtNode
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB23_2:
	movq	8(%rax), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	$0, 8(%rax)
	movl	$-1, 16(%rax)
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB23_6
# BB#3:
	cmpb	$0, 56(%rax)
	je	.LBB23_5
# BB#4:
	callq	_Z21btAlignedFreeInternalPv
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB23_5:
	movq	$0, 48(%rax)
.LBB23_6:                               # %_ZN6btDbvt5clearEv.exit
	movb	$1, 56(%rax)
	movq	$0, 48(%rax)
	movl	$0, 36(%rax)
	movl	$0, 40(%rax)
	movl	$0, 24(%rax)
	movq	(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB23_70
# BB#7:
	movslq	20(%rbx), %rdi
	testq	%rdi, %rdi
	jle	.LBB23_10
# BB#8:
	movq	%rdi, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	shlq	$4, %rdi
.Ltmp110:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 8(%rsp)           # 8-byte Spill
.Ltmp111:
# BB#9:                                 # %_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE7reserveEi.exit
	movq	(%rbx), %rbp
	jmp	.LBB23_12
.LBB23_10:
.Ltmp107:
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 8(%rsp)           # 8-byte Spill
.Ltmp108:
# BB#11:
	movl	$1, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB23_12:                              # %_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4copyEiiPS1_.exit.i.i
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%rbp, (%r13)
	movq	$0, 8(%r13)
	movl	$1, %r8d
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB23_13:                              # %._crit_edge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_31 Depth 2
                                        #     Child Loop BB23_34 Depth 2
                                        #     Child Loop BB23_50 Depth 2
                                        #     Child Loop BB23_53 Depth 2
	movslq	%r8d, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	-1(%rax), %r14
	movq	%r14, 64(%rsp)          # 8-byte Spill
	movq	%r14, %r11
	shlq	$4, %r11
	movq	(%r13,%r11), %rbx
	movq	8(%r13,%r11), %r12
	movq	40(%rbx), %r15
	movq	8(%rcx), %rsi
	testq	%rsi, %rsi
	je	.LBB23_15
# BB#14:                                #   in Loop: Header=BB23_13 Depth=1
	movq	$0, 8(%rcx)
	jmp	.LBB23_17
	.p2align	4, 0x90
.LBB23_15:                              #   in Loop: Header=BB23_13 Depth=1
.Ltmp113:
	movq	%r11, %r14
	movq	%r8, %rbp
	movl	$56, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rsi
.Ltmp114:
# BB#16:                                # %.noexc35
                                        #   in Loop: Header=BB23_13 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rsi)
	movups	%xmm0, 16(%rsi)
	movups	%xmm0, (%rsi)
	movq	$0, 48(%rsi)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, %r8
	movq	%r14, %r11
.LBB23_17:                              #   in Loop: Header=BB23_13 Depth=1
	leal	-1(%r8), %ebp
	movq	%r12, 32(%rsi)
	movq	%r15, 40(%rsi)
	movq	$0, 48(%rsi)
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	%xmm1, 16(%rsi)
	movups	%xmm0, (%rsi)
	movl	%ebp, %eax
	andl	$1, %eax
	testq	%r12, %r12
	leaq	40(%r12,%rax,8), %rax
	cmoveq	%rcx, %rax
	movq	%rsi, (%rax)
	cmpq	$0, 48(%rbx)
	je	.LBB23_60
# BB#18:                                #   in Loop: Header=BB23_13 Depth=1
	movq	40(%rbx), %r14
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	%eax, %ebp
	movq	%rax, %rdx
	jne	.LBB23_19
# BB#23:                                #   in Loop: Header=BB23_13 Depth=1
	leal	(%rdx,%rdx), %r15d
	testl	%edx, %edx
	movl	$1, %eax
	cmovel	%eax, %r15d
	cmpl	%r15d, %r8d
	jle	.LBB23_24
.LBB23_19:                              #   in Loop: Header=BB23_13 Depth=1
	movl	%edx, %r15d
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	%r13, %r12
	jmp	.LBB23_38
	.p2align	4, 0x90
.LBB23_60:                              #   in Loop: Header=BB23_13 Depth=1
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp126:
	callq	*16(%rax)
.Ltmp127:
# BB#61:                                #   in Loop: Header=BB23_13 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB23_62
.LBB23_24:                              #   in Loop: Header=BB23_13 Depth=1
	testl	%r15d, %r15d
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	je	.LBB23_25
# BB#26:                                #   in Loop: Header=BB23_13 Depth=1
	movslq	%r15d, %rdi
	shlq	$4, %rdi
.Ltmp116:
	movl	$16, %esi
	movq	%r8, %rbp
	movq	%r14, %r12
	movq	%r11, %r14
	callq	_Z22btAlignedAllocInternalmi
	movq	%r14, %r11
	movq	%r12, %r14
	movq	%rbp, %r8
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, %r10
.Ltmp117:
	jmp	.LBB23_27
.LBB23_25:                              #   in Loop: Header=BB23_13 Depth=1
	xorl	%r10d, %r10d
.LBB23_27:                              # %_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE8allocateEi.exit.i.i42
                                        #   in Loop: Header=BB23_13 Depth=1
	cmpl	$2, %r8d
	jl	.LBB23_36
# BB#28:                                # %.lr.ph.i.i.i44.preheader
                                        #   in Loop: Header=BB23_13 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	-1(%rax), %rcx
	movq	%rax, %rdx
	andq	$3, %rdx
	je	.LBB23_29
# BB#30:                                # %.lr.ph.i.i.i44.prol.preheader
                                        #   in Loop: Header=BB23_13 Depth=1
	movq	%r10, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB23_31:                              # %.lr.ph.i.i.i44.prol
                                        #   Parent Loop BB23_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi), %xmm0
	movups	%xmm0, (%rsi)
	incq	%rax
	addq	$16, %rdi
	addq	$16, %rsi
	cmpq	%rax, %rdx
	jne	.LBB23_31
	jmp	.LBB23_32
.LBB23_36:                              # %_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4copyEiiPS1_.exit.i.i49
                                        #   in Loop: Header=BB23_13 Depth=1
	testq	%r13, %r13
	jne	.LBB23_35
# BB#37:                                #   in Loop: Header=BB23_13 Depth=1
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movq	%r10, 48(%rsp)          # 8-byte Spill
	movq	%r10, 40(%rsp)          # 8-byte Spill
	movq	%r10, %r12
	movq	56(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB23_38
.LBB23_29:                              #   in Loop: Header=BB23_13 Depth=1
	xorl	%eax, %eax
.LBB23_32:                              # %.lr.ph.i.i.i44.prol.loopexit
                                        #   in Loop: Header=BB23_13 Depth=1
	cmpq	$3, %rcx
	jb	.LBB23_35
# BB#33:                                # %.lr.ph.i.i.i44.preheader.new
                                        #   in Loop: Header=BB23_13 Depth=1
	movq	64(%rsp), %rcx          # 8-byte Reload
	subq	%rax, %rcx
	shlq	$4, %rax
	movq	%r10, %rdx
	movq	%r13, %rsi
	.p2align	4, 0x90
.LBB23_34:                              # %.lr.ph.i.i.i44
                                        #   Parent Loop BB23_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rsi,%rax), %xmm0
	movups	%xmm0, (%rdx,%rax)
	movups	16(%rsi,%rax), %xmm0
	movups	%xmm0, 16(%rdx,%rax)
	movups	32(%rsi,%rax), %xmm0
	movups	%xmm0, 32(%rdx,%rax)
	movups	48(%rsi,%rax), %xmm0
	movups	%xmm0, 48(%rdx,%rax)
	addq	$64, %rsi
	addq	$64, %rdx
	addq	$-4, %rcx
	jne	.LBB23_34
.LBB23_35:                              # %_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4copyEiiPS1_.exit.i.i49.thread
                                        #   in Loop: Header=BB23_13 Depth=1
.Ltmp118:
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%r8, %rbp
	movq	%r10, %r12
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r11, %r14
	callq	_Z21btAlignedFreeInternalPv
	movq	%r14, %r11
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r12, %r10
	movq	%rbp, %r8
	movq	16(%rsp), %rcx          # 8-byte Reload
.Ltmp119:
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movq	%r10, 48(%rsp)          # 8-byte Spill
	movq	%r10, 40(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rsi          # 8-byte Reload
.LBB23_38:                              #   in Loop: Header=BB23_13 Depth=1
	movq	%r14, (%r12,%r11)
	movq	%rsi, 8(%r12,%r11)
	movq	48(%rbx), %rdi
	cmpl	%r15d, %r8d
	jne	.LBB23_39
# BB#40:                                #   in Loop: Header=BB23_13 Depth=1
	leal	(%r8,%r8), %r15d
	testl	%r8d, %r8d
	movl	$1, %eax
	cmovel	%eax, %r15d
	cmpl	%r15d, %r8d
	movq	72(%rsp), %rbp          # 8-byte Reload
	jge	.LBB23_41
# BB#42:                                #   in Loop: Header=BB23_13 Depth=1
	testl	%r15d, %r15d
	je	.LBB23_43
# BB#44:                                #   in Loop: Header=BB23_13 Depth=1
	movq	%r10, %rbx
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movslq	%r15d, %rdi
	shlq	$4, %rdi
.Ltmp121:
	movq	%rsi, %r14
	movl	$16, %esi
	movl	%r15d, %r13d
	movq	%r8, %r15
	callq	_Z22btAlignedAllocInternalmi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	movq	%r15, %r8
	movl	%r13d, %r15d
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 8(%rsp)           # 8-byte Spill
.Ltmp122:
# BB#45:                                #   in Loop: Header=BB23_13 Depth=1
	movq	%rbx, %r10
	jmp	.LBB23_46
	.p2align	4, 0x90
.LBB23_39:                              #   in Loop: Header=BB23_13 Depth=1
	movq	%r10, 8(%rsp)           # 8-byte Spill
	movq	%r12, %r13
	movq	72(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB23_57
.LBB23_41:                              #   in Loop: Header=BB23_13 Depth=1
	movl	%r8d, %r15d
	movq	%r10, 8(%rsp)           # 8-byte Spill
	movq	%r12, %r13
	jmp	.LBB23_57
.LBB23_43:                              #   in Loop: Header=BB23_13 Depth=1
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB23_46:                              # %_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE8allocateEi.exit.i.i57
                                        #   in Loop: Header=BB23_13 Depth=1
	testl	%r8d, %r8d
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	jle	.LBB23_55
# BB#47:                                # %.lr.ph.i.i.i59
                                        #   in Loop: Header=BB23_13 Depth=1
	movq	%rbp, %rcx
	andq	$3, %rcx
	je	.LBB23_48
# BB#49:                                # %.prol.preheader
                                        #   in Loop: Header=BB23_13 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r12, %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB23_50:                              #   Parent Loop BB23_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rsi), %xmm0
	movups	%xmm0, (%rdx)
	incq	%rax
	addq	$16, %rsi
	addq	$16, %rdx
	cmpq	%rax, %rcx
	jne	.LBB23_50
	jmp	.LBB23_51
.LBB23_55:                              # %_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4copyEiiPS1_.exit.i.i64
                                        #   in Loop: Header=BB23_13 Depth=1
	testq	%r12, %r12
	jne	.LBB23_54
# BB#56:                                #   in Loop: Header=BB23_13 Depth=1
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB23_57
.LBB23_48:                              #   in Loop: Header=BB23_13 Depth=1
	xorl	%eax, %eax
.LBB23_51:                              # %.prol.loopexit
                                        #   in Loop: Header=BB23_13 Depth=1
	cmpq	$3, 64(%rsp)            # 8-byte Folded Reload
	jb	.LBB23_54
# BB#52:                                # %.lr.ph.i.i.i59.new
                                        #   in Loop: Header=BB23_13 Depth=1
	movq	%rbp, %rcx
	subq	%rax, %rcx
	shlq	$4, %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r12, %rsi
	.p2align	4, 0x90
.LBB23_53:                              #   Parent Loop BB23_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rsi,%rax), %xmm0
	movups	%xmm0, (%rdx,%rax)
	movups	16(%rsi,%rax), %xmm0
	movups	%xmm0, 16(%rdx,%rax)
	movups	32(%rsi,%rax), %xmm0
	movups	%xmm0, 32(%rdx,%rax)
	movups	48(%rsi,%rax), %xmm0
	movups	%xmm0, 48(%rdx,%rax)
	addq	$64, %rsi
	addq	$64, %rdx
	addq	$-4, %rcx
	jne	.LBB23_53
.LBB23_54:                              # %_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4copyEiiPS1_.exit.i.i64.thread
                                        #   in Loop: Header=BB23_13 Depth=1
.Ltmp123:
	movq	%r10, %rbx
	movq	%rdi, %r14
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %r13d
	movq	%r8, %r15
	callq	_Z21btAlignedFreeInternalPv
	movq	%r14, %rdi
	movq	%r15, %r8
	movl	%r13d, %r15d
	movq	16(%rsp), %rcx          # 8-byte Reload
.Ltmp124:
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rsi          # 8-byte Reload
.LBB23_57:                              #   in Loop: Header=BB23_13 Depth=1
	shlq	$4, %rbp
	movq	%rdi, (%r13,%rbp)
	movq	%rsi, 8(%r13,%rbp)
	incl	%r8d
	movl	%r8d, %ebp
	movl	%r15d, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB23_62:                              #   in Loop: Header=BB23_13 Depth=1
	testl	%ebp, %ebp
	movl	%ebp, %r8d
	jg	.LBB23_13
# BB#63:
	testq	%r13, %r13
	je	.LBB23_70
# BB#64:
	movq	32(%rsp), %rdi          # 8-byte Reload
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB23_70:                              # %_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEED2Ev.exit39
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_21:                              # %.thread167
.Ltmp109:
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB23_20:                              # %.thread
.Ltmp112:
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB23_58:
.Ltmp120:
	jmp	.LBB23_66
.LBB23_59:
.Ltmp125:
	movq	%rax, %rbp
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%r12, %r13
	testq	%r13, %r13
	jne	.LBB23_68
	jmp	.LBB23_69
.LBB23_65:
.Ltmp115:
	jmp	.LBB23_66
.LBB23_22:
.Ltmp128:
.LBB23_66:
	movq	%rax, %rbp
	testq	%r13, %r13
	je	.LBB23_69
.LBB23_68:
.Ltmp129:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
.Ltmp130:
.LBB23_69:                              # %_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEED2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB23_71:
.Ltmp131:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end23:
	.size	_ZNK6btDbvt5cloneERS_PNS_6ICloneE, .Lfunc_end23-_ZNK6btDbvt5cloneERS_PNS_6ICloneE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp110-.Lfunc_begin5  #   Call between .Lfunc_begin5 and .Ltmp110
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp110-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp111-.Ltmp110       #   Call between .Ltmp110 and .Ltmp111
	.long	.Ltmp112-.Lfunc_begin5  #     jumps to .Ltmp112
	.byte	0                       #   On action: cleanup
	.long	.Ltmp107-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp108-.Ltmp107       #   Call between .Ltmp107 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin5  #     jumps to .Ltmp109
	.byte	0                       #   On action: cleanup
	.long	.Ltmp113-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp114-.Ltmp113       #   Call between .Ltmp113 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin5  #     jumps to .Ltmp115
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp128-.Lfunc_begin5  #     jumps to .Ltmp128
	.byte	0                       #   On action: cleanup
	.long	.Ltmp116-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp119-.Ltmp116       #   Call between .Ltmp116 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin5  #     jumps to .Ltmp120
	.byte	0                       #   On action: cleanup
	.long	.Ltmp121-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp124-.Ltmp121       #   Call between .Ltmp121 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin5  #     jumps to .Ltmp125
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp129-.Ltmp124       #   Call between .Ltmp124 and .Ltmp129
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp130-.Ltmp129       #   Call between .Ltmp129 and .Ltmp130
	.long	.Ltmp131-.Lfunc_begin5  #     jumps to .Ltmp131
	.byte	1                       #   On action: 1
	.long	.Ltmp130-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Lfunc_end23-.Ltmp130   #   Call between .Ltmp130 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN6btDbvt8maxdepthEPK10btDbvtNode
	.p2align	4, 0x90
	.type	_ZN6btDbvt8maxdepthEPK10btDbvtNode,@function
_ZN6btDbvt8maxdepthEPK10btDbvtNode:     # @_ZN6btDbvt8maxdepthEPK10btDbvtNode
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi165:
	.cfi_def_cfa_offset 16
	movl	$0, 4(%rsp)
	testq	%rdi, %rdi
	je	.LBB24_1
# BB#2:
	leaq	4(%rsp), %rdx
	movl	$1, %esi
	callq	_ZL11getmaxdepthPK10btDbvtNodeiRi
	movl	4(%rsp), %eax
	popq	%rcx
	retq
.LBB24_1:
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZN6btDbvt8maxdepthEPK10btDbvtNode, .Lfunc_end24-_ZN6btDbvt8maxdepthEPK10btDbvtNode
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL11getmaxdepthPK10btDbvtNodeiRi,@function
_ZL11getmaxdepthPK10btDbvtNodeiRi:      # @_ZL11getmaxdepthPK10btDbvtNodeiRi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi166:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi167:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi168:
	.cfi_def_cfa_offset 32
.Lcfi169:
	.cfi_offset %rbx, -32
.Lcfi170:
	.cfi_offset %r14, -24
.Lcfi171:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %rbx
	cmpq	$0, 48(%rbx)
	je	.LBB25_3
	.p2align	4, 0x90
.LBB25_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rdi
	incl	%ebp
	movl	%ebp, %esi
	movq	%r14, %rdx
	callq	_ZL11getmaxdepthPK10btDbvtNodeiRi
	movq	40(%rbx), %rbx
	cmpq	$0, 48(%rbx)
	jne	.LBB25_1
.LBB25_3:                               # %tailrecurse._crit_edge
	movl	(%r14), %eax
	cmpl	%ebp, %eax
	cmovgel	%eax, %ebp
	movl	%ebp, (%r14)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end25:
	.size	_ZL11getmaxdepthPK10btDbvtNodeiRi, .Lfunc_end25-_ZL11getmaxdepthPK10btDbvtNodeiRi
	.cfi_endproc

	.globl	_ZN6btDbvt11countLeavesEPK10btDbvtNode
	.p2align	4, 0x90
	.type	_ZN6btDbvt11countLeavesEPK10btDbvtNode,@function
_ZN6btDbvt11countLeavesEPK10btDbvtNode: # @_ZN6btDbvt11countLeavesEPK10btDbvtNode
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi172:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi173:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi174:
	.cfi_def_cfa_offset 32
.Lcfi175:
	.cfi_offset %rbx, -24
.Lcfi176:
	.cfi_offset %r14, -16
	movq	48(%rdi), %rax
	movl	$1, %ebx
	testq	%rax, %rax
	je	.LBB26_2
	.p2align	4, 0x90
.LBB26_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %r14
	movq	40(%rdi), %rdi
	callq	_ZN6btDbvt11countLeavesEPK10btDbvtNode
	addl	%eax, %ebx
	movq	48(%r14), %rax
	testq	%rax, %rax
	movq	%r14, %rdi
	jne	.LBB26_1
.LBB26_2:                               # %tailrecurse._crit_edge
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end26:
	.size	_ZN6btDbvt11countLeavesEPK10btDbvtNode, .Lfunc_end26-_ZN6btDbvt11countLeavesEPK10btDbvtNode
	.cfi_endproc

	.globl	_ZN6btDbvt13extractLeavesEPK10btDbvtNodeR20btAlignedObjectArrayIS2_E
	.p2align	4, 0x90
	.type	_ZN6btDbvt13extractLeavesEPK10btDbvtNodeR20btAlignedObjectArrayIS2_E,@function
_ZN6btDbvt13extractLeavesEPK10btDbvtNodeR20btAlignedObjectArrayIS2_E: # @_ZN6btDbvt13extractLeavesEPK10btDbvtNodeR20btAlignedObjectArrayIS2_E
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi177:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi178:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi179:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi180:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi181:
	.cfi_def_cfa_offset 48
.Lcfi182:
	.cfi_offset %rbx, -40
.Lcfi183:
	.cfi_offset %r14, -32
.Lcfi184:
	.cfi_offset %r15, -24
.Lcfi185:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	$0, 48(%rbx)
	je	.LBB27_3
# BB#1:                                 # %tailrecurse.preheader
	leaq	48(%rbx), %rbp
	.p2align	4, 0x90
.LBB27_2:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rdi
	movq	%r14, %rsi
	callq	_ZN6btDbvt13extractLeavesEPK10btDbvtNodeR20btAlignedObjectArrayIS2_E
	movq	(%rbp), %rbx
	leaq	48(%rbx), %rbp
	cmpq	$0, 48(%rbx)
	jne	.LBB27_2
.LBB27_3:                               # %tailrecurse._crit_edge
	movl	4(%r14), %eax
	cmpl	8(%r14), %eax
	jne	.LBB27_18
# BB#4:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB27_18
# BB#5:
	testl	%ebp, %ebp
	je	.LBB27_6
# BB#7:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	4(%r14), %eax
	testl	%eax, %eax
	jg	.LBB27_9
	jmp	.LBB27_13
.LBB27_6:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB27_13
.LBB27_9:                               # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB27_11
	.p2align	4, 0x90
.LBB27_10:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB27_10
.LBB27_11:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB27_13
	.p2align	4, 0x90
.LBB27_12:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	16(%r14), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	16(%r14), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	16(%r14), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB27_12
.LBB27_13:                              # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_.exit.i.i
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB27_17
# BB#14:
	cmpb	$0, 24(%r14)
	je	.LBB27_16
# BB#15:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%r14), %eax
.LBB27_16:
	movq	$0, 16(%r14)
.LBB27_17:                              # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv.exit.i.i
	movb	$1, 24(%r14)
	movq	%r15, 16(%r14)
	movl	%ebp, 8(%r14)
.LBB27_18:                              # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_.exit
	movq	16(%r14), %rcx
	cltq
	movq	%rbx, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 4(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	_ZN6btDbvt13extractLeavesEPK10btDbvtNodeR20btAlignedObjectArrayIS2_E, .Lfunc_end27-_ZN6btDbvt13extractLeavesEPK10btDbvtNodeR20btAlignedObjectArrayIS2_E
	.cfi_endproc

	.section	.text._ZN20btDbvtNodeEnumeratorD0Ev,"axG",@progbits,_ZN20btDbvtNodeEnumeratorD0Ev,comdat
	.weak	_ZN20btDbvtNodeEnumeratorD0Ev
	.p2align	4, 0x90
	.type	_ZN20btDbvtNodeEnumeratorD0Ev,@function
_ZN20btDbvtNodeEnumeratorD0Ev:          # @_ZN20btDbvtNodeEnumeratorD0Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi186:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi187:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi188:
	.cfi_def_cfa_offset 32
.Lcfi189:
	.cfi_offset %rbx, -24
.Lcfi190:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV20btDbvtNodeEnumerator+16, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_3
# BB#1:
	cmpb	$0, 32(%rbx)
	je	.LBB28_3
# BB#2:
.Ltmp132:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp133:
.LBB28_3:                               # %.noexc.i
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB28_4:
.Ltmp134:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end28:
	.size	_ZN20btDbvtNodeEnumeratorD0Ev, .Lfunc_end28-_ZN20btDbvtNodeEnumeratorD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp132-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp133-.Ltmp132       #   Call between .Ltmp132 and .Ltmp133
	.long	.Ltmp134-.Lfunc_begin6  #     jumps to .Ltmp134
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Lfunc_end28-.Ltmp133   #   Call between .Ltmp133 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_,"axG",@progbits,_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_,comdat
	.weak	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_
	.p2align	4, 0x90
	.type	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_,@function
_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_: # @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end29:
	.size	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_, .Lfunc_end29-_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_
	.cfi_endproc

	.section	.text._ZN20btDbvtNodeEnumerator7ProcessEPK10btDbvtNode,"axG",@progbits,_ZN20btDbvtNodeEnumerator7ProcessEPK10btDbvtNode,comdat
	.weak	_ZN20btDbvtNodeEnumerator7ProcessEPK10btDbvtNode
	.p2align	4, 0x90
	.type	_ZN20btDbvtNodeEnumerator7ProcessEPK10btDbvtNode,@function
_ZN20btDbvtNodeEnumerator7ProcessEPK10btDbvtNode: # @_ZN20btDbvtNodeEnumerator7ProcessEPK10btDbvtNode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi191:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi192:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi193:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi194:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi195:
	.cfi_def_cfa_offset 48
.Lcfi196:
	.cfi_offset %rbx, -40
.Lcfi197:
	.cfi_offset %r14, -32
.Lcfi198:
	.cfi_offset %r15, -24
.Lcfi199:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	12(%rbx), %eax
	cmpl	16(%rbx), %eax
	jne	.LBB30_15
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB30_15
# BB#2:
	testl	%ebp, %ebp
	je	.LBB30_3
# BB#4:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB30_6
	jmp	.LBB30_10
.LBB30_3:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB30_10
.LBB30_6:                               # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB30_8
	.p2align	4, 0x90
.LBB30_7:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB30_7
.LBB30_8:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB30_10
	.p2align	4, 0x90
.LBB30_9:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	24(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	24(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	24(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB30_9
.LBB30_10:                              # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_.exit.i.i
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB30_14
# BB#11:
	cmpb	$0, 32(%rbx)
	je	.LBB30_13
# BB#12:
	callq	_Z21btAlignedFreeInternalPv
	movl	12(%rbx), %eax
.LBB30_13:
	movq	$0, 24(%rbx)
.LBB30_14:                              # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv.exit.i.i
	movb	$1, 32(%rbx)
	movq	%r15, 24(%rbx)
	movl	%ebp, 16(%rbx)
.LBB30_15:                              # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_.exit
	movq	24(%rbx), %rcx
	cltq
	movq	%r14, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	_ZN20btDbvtNodeEnumerator7ProcessEPK10btDbvtNode, .Lfunc_end30-_ZN20btDbvtNodeEnumerator7ProcessEPK10btDbvtNode
	.cfi_endproc

	.section	.text._ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef,"axG",@progbits,_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef,comdat
	.weak	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef
	.p2align	4, 0x90
	.type	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef,@function
_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef: # @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end31:
	.size	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef, .Lfunc_end31-_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef
	.cfi_endproc

	.section	.text._ZN6btDbvt8ICollide7DescentEPK10btDbvtNode,"axG",@progbits,_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode,comdat
	.weak	_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode
	.p2align	4, 0x90
	.type	_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode,@function
_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode: # @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode
	.cfi_startproc
# BB#0:
	movb	$1, %al
	retq
.Lfunc_end32:
	.size	_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode, .Lfunc_end32-_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode
	.cfi_endproc

	.section	.text._ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode,"axG",@progbits,_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode,comdat
	.weak	_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode
	.p2align	4, 0x90
	.type	_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode,@function
_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode: # @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode
	.cfi_startproc
# BB#0:
	movb	$1, %al
	retq
.Lfunc_end33:
	.size	_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode, .Lfunc_end33-_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode
	.cfi_endproc

	.type	_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis,@object # @_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis
	.local	_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis
	.comm	_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis,48,16
	.type	_ZGVZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis,@object # @_ZGVZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis
	.local	_ZGVZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis
	.comm	_ZGVZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis,8,8
	.type	_ZTV20btDbvtNodeEnumerator,@object # @_ZTV20btDbvtNodeEnumerator
	.section	.rodata._ZTV20btDbvtNodeEnumerator,"aG",@progbits,_ZTV20btDbvtNodeEnumerator,comdat
	.weak	_ZTV20btDbvtNodeEnumerator
	.p2align	3
_ZTV20btDbvtNodeEnumerator:
	.quad	0
	.quad	_ZTI20btDbvtNodeEnumerator
	.quad	_ZN20btDbvtNodeEnumeratorD2Ev
	.quad	_ZN20btDbvtNodeEnumeratorD0Ev
	.quad	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_
	.quad	_ZN20btDbvtNodeEnumerator7ProcessEPK10btDbvtNode
	.quad	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef
	.quad	_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode
	.quad	_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode
	.size	_ZTV20btDbvtNodeEnumerator, 72

	.type	_ZTS20btDbvtNodeEnumerator,@object # @_ZTS20btDbvtNodeEnumerator
	.section	.rodata._ZTS20btDbvtNodeEnumerator,"aG",@progbits,_ZTS20btDbvtNodeEnumerator,comdat
	.weak	_ZTS20btDbvtNodeEnumerator
	.p2align	4
_ZTS20btDbvtNodeEnumerator:
	.asciz	"20btDbvtNodeEnumerator"
	.size	_ZTS20btDbvtNodeEnumerator, 23

	.type	_ZTSN6btDbvt8ICollideE,@object # @_ZTSN6btDbvt8ICollideE
	.section	.rodata._ZTSN6btDbvt8ICollideE,"aG",@progbits,_ZTSN6btDbvt8ICollideE,comdat
	.weak	_ZTSN6btDbvt8ICollideE
	.p2align	4
_ZTSN6btDbvt8ICollideE:
	.asciz	"N6btDbvt8ICollideE"
	.size	_ZTSN6btDbvt8ICollideE, 19

	.type	_ZTIN6btDbvt8ICollideE,@object # @_ZTIN6btDbvt8ICollideE
	.section	.rodata._ZTIN6btDbvt8ICollideE,"aG",@progbits,_ZTIN6btDbvt8ICollideE,comdat
	.weak	_ZTIN6btDbvt8ICollideE
	.p2align	3
_ZTIN6btDbvt8ICollideE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6btDbvt8ICollideE
	.size	_ZTIN6btDbvt8ICollideE, 16

	.type	_ZTI20btDbvtNodeEnumerator,@object # @_ZTI20btDbvtNodeEnumerator
	.section	.rodata._ZTI20btDbvtNodeEnumerator,"aG",@progbits,_ZTI20btDbvtNodeEnumerator,comdat
	.weak	_ZTI20btDbvtNodeEnumerator
	.p2align	4
_ZTI20btDbvtNodeEnumerator:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20btDbvtNodeEnumerator
	.quad	_ZTIN6btDbvt8ICollideE
	.size	_ZTI20btDbvtNodeEnumerator, 24


	.globl	_ZN6btDbvtC1Ev
	.type	_ZN6btDbvtC1Ev,@function
_ZN6btDbvtC1Ev = _ZN6btDbvtC2Ev
	.globl	_ZN6btDbvtD1Ev
	.type	_ZN6btDbvtD1Ev,@function
_ZN6btDbvtD1Ev = _ZN6btDbvtD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
