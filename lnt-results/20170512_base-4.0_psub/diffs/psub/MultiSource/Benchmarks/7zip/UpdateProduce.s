	.text
	.file	"UpdateProduce.bc"
	.globl	_Z13UpdateProduceRK13CRecordVectorI11CUpdatePairERKN14NUpdateArchive10CActionSetERS_I12CUpdatePair2EP22IUpdateProduceCallback
	.p2align	4, 0x90
	.type	_Z13UpdateProduceRK13CRecordVectorI11CUpdatePairERKN14NUpdateArchive10CActionSetERS_I12CUpdatePair2EP22IUpdateProduceCallback,@function
_Z13UpdateProduceRK13CRecordVectorI11CUpdatePairERKN14NUpdateArchive10CActionSetERS_I12CUpdatePair2EP22IUpdateProduceCallback: # @_Z13UpdateProduceRK13CRecordVectorI11CUpdatePairERKN14NUpdateArchive10CActionSetERS_I12CUpdatePair2EP22IUpdateProduceCallback
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r15
	movl	12(%r15), %edi
	testl	%edi, %edi
	jle	.LBB0_25
# BB#1:                                 # %.lr.ph
	testq	%rcx, %rcx
	je	.LBB0_14
# BB#2:                                 # %.lr.ph.split.preheader
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	movl	$8, %r12d
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rdx
	movl	(%rdx,%r12), %eax
	movl	-8(%rdx,%r12), %ecx
	movl	-4(%rdx,%r12), %r14d
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rdx,%rcx,4), %edx
	cmpq	$3, %rdx
	ja	.LBB0_9
# BB#4:                                 # %.lr.ph.split
                                        #   in Loop: Header=BB0_3 Depth=1
	jmpq	*.LJTI0_0(,%rdx,8)
.LBB0_5:                                #   in Loop: Header=BB0_3 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	%r14d, %esi
	callq	*(%rax)
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_6:                                #   in Loop: Header=BB0_3 Depth=1
	cmpl	$2, %ecx
	je	.LBB0_26
# BB#7:                                 #   in Loop: Header=BB0_3 Depth=1
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_8:                                #   in Loop: Header=BB0_3 Depth=1
	cmpl	$1, %ecx
	jbe	.LBB0_26
.LBB0_9:                                #   in Loop: Header=BB0_3 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB0_11
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_3 Depth=1
	movl	$65536, %ecx            # imm = 0x10000
.LBB0_11:                               #   in Loop: Header=BB0_3 Depth=1
	movl	$1, %ebx
.LBB0_12:                               #   in Loop: Header=BB0_3 Depth=1
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	orq	%rdx, %r14
	shlq	$32, %rax
	orq	%rbx, %rcx
	shlq	$8, %rbx
	orq	%rcx, %rbx
	orq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r13), %rax
	movslq	12(%r13), %rcx
	shlq	$4, %rcx
	movq	%rbx, (%rax,%rcx)
	movq	%r14, 8(%rax,%rcx)
	incl	12(%r13)
.LBB0_13:                               #   in Loop: Header=BB0_3 Depth=1
	incq	%rbp
	movslq	12(%r15), %rax
	addq	$12, %r12
	cmpq	%rax, %rbp
	jl	.LBB0_3
	jmp	.LBB0_25
.LBB0_14:                               # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rsi
	movl	(%rsi,%rbp), %eax
	movl	-8(%rsi,%rbp), %edx
	movl	-4(%rsi,%rbp), %r12d
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	(%rsi,%rdx,4), %esi
	cmpq	$3, %rsi
	ja	.LBB0_20
# BB#16:                                # %.lr.ph.split.us
                                        #   in Loop: Header=BB0_15 Depth=1
	jmpq	*.LJTI0_1(,%rsi,8)
.LBB0_17:                               #   in Loop: Header=BB0_15 Depth=1
	cmpl	$2, %edx
	je	.LBB0_26
# BB#18:                                #   in Loop: Header=BB0_15 Depth=1
	xorl	%ecx, %ecx
	xorl	%r14d, %r14d
	jmp	.LBB0_23
	.p2align	4, 0x90
.LBB0_19:                               #   in Loop: Header=BB0_15 Depth=1
	cmpl	$2, %edx
	jb	.LBB0_26
.LBB0_20:                               #   in Loop: Header=BB0_15 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_21:                               #   in Loop: Header=BB0_15 Depth=1
	movl	$65536, %ecx            # imm = 0x10000
.LBB0_22:                               #   in Loop: Header=BB0_15 Depth=1
	movl	$1, %r14d
.LBB0_23:                               #   in Loop: Header=BB0_15 Depth=1
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	orq	%rdx, %r12
	shlq	$32, %rax
	orq	%r14, %rcx
	shlq	$8, %r14
	orq	%rcx, %r14
	orq	%rax, %r14
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r13), %rax
	movslq	12(%r13), %rcx
	shlq	$4, %rcx
	movq	%r14, (%rax,%rcx)
	movq	%r12, 8(%rax,%rcx)
	incl	12(%r13)
	movl	12(%r15), %edi
.LBB0_24:                               #   in Loop: Header=BB0_15 Depth=1
	incq	%rbx
	movslq	%edi, %rax
	addq	$12, %rbp
	cmpq	%rax, %rbx
	jl	.LBB0_15
.LBB0_25:                               # %._crit_edge
	movq	%r13, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector11ReserveDownEv # TAILCALL
.LBB0_26:                               # %.us-lcssa41.us
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end0:
	.size	_Z13UpdateProduceRK13CRecordVectorI11CUpdatePairERKN14NUpdateArchive10CActionSetERS_I12CUpdatePair2EP22IUpdateProduceCallback, .Lfunc_end0-_Z13UpdateProduceRK13CRecordVectorI11CUpdatePairERKN14NUpdateArchive10CActionSetERS_I12CUpdatePair2EP22IUpdateProduceCallback
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_5
	.quad	.LBB0_6
	.quad	.LBB0_8
	.quad	.LBB0_10
.LJTI0_1:
	.quad	.LBB0_24
	.quad	.LBB0_17
	.quad	.LBB0_19
	.quad	.LBB0_21

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Internal collision in update action set"
	.size	.L.str, 40


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
