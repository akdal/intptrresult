	.text
	.file	"clamscan_clamscan.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI0_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_2:
	.quad	4562146422526312448     # double 9.765625E-4
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 64
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	clamscan_shortopt(%rip), %rdx
	movl	$clamscan_longopt, %ecx
	xorl	%r8d, %r8d
	callq	opt_parse
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_11
# BB#1:
	movl	$.L.str.2, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_3
# BB#2:
	movw	$1, mprintf_verbose(%rip)
	movw	$1, logg_verbose(%rip)
.LBB0_3:
	movl	$.L.str.3, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_5
# BB#4:
	movw	$1, mprintf_quiet(%rip)
.LBB0_5:
	movl	$.L.str.4, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_7
# BB#6:
	movw	$1, mprintf_stdout(%rip)
.LBB0_7:
	movl	$.L.str.5, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_9
# BB#8:
	callq	cl_debug
.LBB0_9:
	movl	$.L.str.6, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_12
# BB#10:
	movq	%rbx, %rdi
	callq	opt_free
	callq	print_version
	xorl	%ebp, %ebp
	jmp	.LBB0_58
.LBB0_11:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	mprintf
.LBB0_57:
	movl	$40, %ebp
	jmp	.LBB0_58
.LBB0_12:
	movl	$.L.str.7, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_14
# BB#13:
	movq	%rbx, %rdi
	callq	opt_free
	callq	help
	xorl	%ebp, %ebp
.LBB0_58:
	movl	%ebp, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB0_14:
	movl	$.L.str.8, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_16
# BB#15:
	movw	$1, recursion(%rip)
.LBB0_16:
	movl	$.L.str.9, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_18
# BB#17:
	movw	$1, printinfected(%rip)
.LBB0_18:
	movl	$.L.str.10, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_20
# BB#19:
	movw	$1, bell(%rip)
.LBB0_20:
	movl	$.L.str.11, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_22
# BB#21:
	movl	$.L.str.11, %esi
	movq	%rbx, %rdi
	callq	opt_arg
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	cl_settempdir
.LBB0_22:
	movl	$.L.str.12, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_24
# BB#23:
	xorl	%edi, %edi
	movl	$1, %esi
	callq	cl_settempdir
.LBB0_24:
	movl	$.L.str.13, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_27
# BB#25:
	movl	$.L.str.13, %esi
	movq	%rbx, %rdi
	callq	opt_arg
	movq	%rax, logg_file(%rip)
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	logg
	testl	%eax, %eax
	je	.LBB0_28
# BB#26:
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	mprintf
	movq	%rbx, %rdi
	callq	opt_free
	movl	$62, %ebp
	jmp	.LBB0_58
.LBB0_27:
	movq	$0, logg_file(%rip)
.LBB0_28:
	movl	$.L.str.16, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_32
# BB#29:
	movl	$.L.str.16, %esi
	movq	%rbx, %rdi
	callq	opt_arg
	movq	%rax, %rbp
	movl	$77, %esi
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	jne	.LBB0_32
# BB#30:
	movl	$109, %esi
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	jne	.LBB0_32
# BB#31:
	movq	%rbp, %rdi
	callq	isnumb
	testl	%eax, %eax
	je	.LBB0_55
.LBB0_32:
	movl	$.L.str.18, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_34
# BB#33:
	movl	$.L.str.18, %esi
	movq	%rbx, %rdi
	callq	opt_arg
	movq	%rax, %rdi
	callq	isnumb
	testl	%eax, %eax
	je	.LBB0_50
.LBB0_34:
	movl	$.L.str.20, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_36
# BB#35:
	movl	$.L.str.20, %esi
	movq	%rbx, %rdi
	callq	opt_arg
	movq	%rax, %rdi
	callq	isnumb
	testl	%eax, %eax
	je	.LBB0_51
.LBB0_36:
	movl	$.L.str.22, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_38
# BB#37:
	movl	$.L.str.22, %esi
	movq	%rbx, %rdi
	callq	opt_arg
	movq	%rax, %rdi
	callq	isnumb
	testl	%eax, %eax
	je	.LBB0_52
.LBB0_38:
	movl	$.L.str.24, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_40
# BB#39:
	movl	$.L.str.24, %esi
	movq	%rbx, %rdi
	callq	opt_arg
	movq	%rax, %rdi
	callq	isnumb
	testl	%eax, %eax
	je	.LBB0_53
.LBB0_40:
	movl	$.L.str.26, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	je	.LBB0_42
# BB#41:
	movl	$.L.str.26, %esi
	movq	%rbx, %rdi
	callq	opt_arg
	movq	%rax, %rdi
	callq	isnumb
	testl	%eax, %eax
	je	.LBB0_54
.LBB0_42:
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, info+16(%rip)
	movupd	%xmm0, info(%rip)
	leaq	24(%rsp), %rdi
	movq	%rsp, %rsi
	callq	gettimeofday
	movq	%rbx, %rdi
	callq	scanmanager
	movl	%eax, %ebp
	movl	$.L.str.28, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB0_49
# BB#43:
	movl	$.L.str.29, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	jne	.LBB0_49
# BB#44:
	leaq	8(%rsp), %rdi
	movq	%rsp, %rsi
	callq	gettimeofday
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	logg
	movl	info(%rip), %esi
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	callq	logg
	callq	cl_retver
	movq	%rax, %rcx
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	logg
	movl	info+4(%rip), %esi
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	callq	logg
	movl	info+8(%rip), %esi
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	callq	logg
	movl	info+12(%rip), %esi
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	callq	logg
	movl	info+16(%rip), %esi
	testl	%esi, %esi
	je	.LBB0_46
# BB#45:
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	callq	logg
.LBB0_46:
	cmpl	$0, info+20(%rip)
	je	.LBB0_48
# BB#47:
	movl	$.L.str.38, %esi
	movq	%rbx, %rdi
	callq	opt_check
	testl	%eax, %eax
	movl	$.L.str.39, %eax
	movl	$.L.str.40, %esi
	cmovneq	%rax, %rsi
	movl	info+20(%rip), %edx
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	callq	logg
.LBB0_48:
	movq	info+24(%rip), %rax
	shlq	$2, %rax
	movd	%rax, %xmm1
	punpckldq	.LCPI0_0(%rip), %xmm1 # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	subpd	.LCPI0_1(%rip), %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	mulsd	.LCPI0_2(%rip), %xmm0
	movl	$.L.str.41, %edi
	movb	$1, %al
	callq	logg
.LBB0_49:
	movq	%rbx, %rdi
	callq	opt_free
	jmp	.LBB0_58
.LBB0_50:
	movl	$.L.str.19, %edi
	jmp	.LBB0_56
.LBB0_51:
	movl	$.L.str.21, %edi
	jmp	.LBB0_56
.LBB0_52:
	movl	$.L.str.23, %edi
	jmp	.LBB0_56
.LBB0_53:
	movl	$.L.str.25, %edi
	jmp	.LBB0_56
.LBB0_54:
	movl	$.L.str.27, %edi
	jmp	.LBB0_56
.LBB0_55:
	movl	$.L.str.17, %edi
.LBB0_56:
	xorl	%eax, %eax
	callq	logg
	movq	%rbx, %rdi
	callq	opt_free
	jmp	.LBB0_57
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.globl	help
	.p2align	4, 0x90
	.type	help,@function
help:                                   # @help
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movw	$1, mprintf_stdout(%rip)
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.47, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.53, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.57, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.58, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.60, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.61, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.62, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.63, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.64, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.65, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.66, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.67, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.68, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.69, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.70, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.71, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.72, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.73, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.74, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.75, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.76, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.77, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.78, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.79, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.80, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.81, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.82, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.83, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.84, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.85, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.86, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.87, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.88, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.89, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.90, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.91, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.92, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.93, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.94, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.95, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.96, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.97, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.98, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.99, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.100, %edi
	xorl	%eax, %eax
	callq	mprintf
	movl	$.L.str.101, %edi
	xorl	%eax, %eax
	popq	%rcx
	jmp	mprintf                 # TAILCALL
.Lfunc_end1:
	.size	help, .Lfunc_end1-help
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"hvd:wriVl:m"
	.size	.L.str, 12

	.type	clamscan_shortopt,@object # @clamscan_shortopt
	.data
	.globl	clamscan_shortopt
	.p2align	3
clamscan_shortopt:
	.quad	.L.str
	.size	clamscan_shortopt, 8

	.type	recursion,@object       # @recursion
	.bss
	.globl	recursion
	.p2align	1
recursion:
	.short	0                       # 0x0
	.size	recursion, 2

	.type	printinfected,@object   # @printinfected
	.globl	printinfected
	.p2align	1
printinfected:
	.short	0                       # 0x0
	.size	printinfected, 2

	.type	bell,@object            # @bell
	.globl	bell
	.p2align	1
bell:
	.short	0                       # 0x0
	.size	bell, 2

	.type	clamscan_longopt,@object # @clamscan_longopt
	.data
	.p2align	4
clamscan_longopt:
	.quad	.L.str.7
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	104                     # 0x68
	.zero	4
	.quad	.L.str.3
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.4
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.2
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	118                     # 0x76
	.zero	4
	.quad	.L.str.5
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.6
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	86                      # 0x56
	.zero	4
	.quad	.L.str.11
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.12
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.102
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.103
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	109                     # 0x6d
	.zero	4
	.quad	.L.str.104
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	100                     # 0x64
	.zero	4
	.quad	.L.str.105
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.8
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	114                     # 0x72
	.zero	4
	.quad	.L.str.10
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.28
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.29
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.9
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	105                     # 0x69
	.zero	4
	.quad	.L.str.13
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	108                     # 0x6c
	.zero	4
	.quad	.L.str.106
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.38
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.107
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.108
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.109
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.110
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.111
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.18
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.16
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.26
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.20
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.24
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.22
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.112
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.113
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.114
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.115
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.116
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.117
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.118
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.119
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.120
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.121
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.122
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.123
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.124
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.125
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.126
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.127
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.128
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.129
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.130
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.131
	.long	2                       # 0x2
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.132
	.long	2                       # 0x2
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.133
	.long	2                       # 0x2
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.134
	.long	2                       # 0x2
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.135
	.long	2                       # 0x2
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.136
	.long	2                       # 0x2
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.137
	.long	2                       # 0x2
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.138
	.long	2                       # 0x2
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.139
	.long	2                       # 0x2
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.140
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.141
	.long	1                       # 0x1
	.zero	4
	.quad	0
	.long	0                       # 0x0
	.zero	4
	.zero	32
	.size	clamscan_longopt, 1984

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"!Can't parse the command line\n"
	.size	.L.str.1, 31

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"verbose"
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"quiet"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"stdout"
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"debug"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"version"
	.size	.L.str.6, 8

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"help"
	.size	.L.str.7, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"recursive"
	.size	.L.str.8, 10

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"infected"
	.size	.L.str.9, 9

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"bell"
	.size	.L.str.10, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"tempdir"
	.size	.L.str.11, 8

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"leave-temps"
	.size	.L.str.12, 12

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"log"
	.size	.L.str.13, 4

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"#\n-------------------------------------------------------------------------------\n\n"
	.size	.L.str.14, 84

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"!Problem with internal logger.\n"
	.size	.L.str.15, 32

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"max-space"
	.size	.L.str.16, 10

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"!--max-space requires a natural number\n"
	.size	.L.str.17, 40

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"max-files"
	.size	.L.str.18, 10

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"!--max-files requires a natural number\n"
	.size	.L.str.19, 40

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"max-recursion"
	.size	.L.str.20, 14

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"!--max-recursion requires a natural number\n"
	.size	.L.str.21, 44

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"max-mail-recursion"
	.size	.L.str.22, 19

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"!--max-mail-recursion requires a natural number\n"
	.size	.L.str.23, 49

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"max-dir-recursion"
	.size	.L.str.24, 18

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"!--max-dir-recursion requires a natural number\n"
	.size	.L.str.25, 48

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"max-ratio"
	.size	.L.str.26, 10

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"!--max-ratio requires a natural number\n"
	.size	.L.str.27, 40

	.type	info,@object            # @info
	.comm	info,32,8
	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"disable-summary"
	.size	.L.str.28, 16

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"no-summary"
	.size	.L.str.29, 11

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"\n----------- SCAN SUMMARY -----------\n"
	.size	.L.str.30, 39

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Known viruses: %u\n"
	.size	.L.str.31, 19

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Engine version: %s\n"
	.size	.L.str.32, 20

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Scanned directories: %u\n"
	.size	.L.str.33, 25

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"Scanned files: %u\n"
	.size	.L.str.34, 19

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"Infected files: %u\n"
	.size	.L.str.35, 20

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"Not removed: %u\n"
	.size	.L.str.36, 17

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Not %s: %u\n"
	.size	.L.str.37, 12

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"copy"
	.size	.L.str.38, 5

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"moved"
	.size	.L.str.39, 6

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"copied"
	.size	.L.str.40, 7

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"Data scanned: %2.2lf MB\n"
	.size	.L.str.41, 25

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"\n"
	.size	.L.str.42, 2

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"                       Clam AntiVirus Scanner devel-20071218\n"
	.size	.L.str.43, 62

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"      (C) 2002 - 2007 ClamAV Team - http://www.clamav.net/team\n\n"
	.size	.L.str.44, 65

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"    --help                -h             Print this help screen\n"
	.size	.L.str.45, 65

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"    --version             -V             Print version number\n"
	.size	.L.str.46, 63

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"    --verbose             -v             Be verbose\n"
	.size	.L.str.47, 53

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"    --debug                              Enable libclamav's debug messages\n"
	.size	.L.str.48, 76

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"    --quiet                              Only output error messages\n"
	.size	.L.str.49, 69

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"    --stdout                             Write to stdout instead of stderr\n"
	.size	.L.str.50, 76

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"    --no-summary                         Disable summary at end of scanning\n"
	.size	.L.str.51, 77

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"    --infected            -i             Only print infected files\n"
	.size	.L.str.52, 68

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"    --bell                               Sound bell on virus detection\n"
	.size	.L.str.53, 72

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"    --tempdir=DIRECTORY                  Create temporary files in DIRECTORY\n"
	.size	.L.str.54, 78

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"    --leave-temps                        Do not remove temporary files\n"
	.size	.L.str.55, 72

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"    --database=FILE/DIR   -d FILE/DIR    Load virus database from FILE or load\n"
	.size	.L.str.56, 80

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"                                         all .cvd and .db[2] files from DIR\n"
	.size	.L.str.57, 77

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"    --log=FILE            -l FILE        Save scan report to FILE\n"
	.size	.L.str.58, 67

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"    --recursive           -r             Scan subdirectories recursively\n"
	.size	.L.str.59, 74

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"    --remove                             Remove infected files. Be careful!\n"
	.size	.L.str.60, 77

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"    --move=DIRECTORY                     Move infected files into DIRECTORY\n"
	.size	.L.str.61, 77

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"    --copy=DIRECTORY                     Copy infected files into DIRECTORY\n"
	.size	.L.str.62, 77

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"    --exclude=PATT                       Don't scan file names containing PATT\n"
	.size	.L.str.63, 80

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"    --exclude-dir=PATT                   Don't scan directories containing PATT\n"
	.size	.L.str.64, 81

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"    --include=PATT                       Only scan file names containing PATT\n"
	.size	.L.str.65, 79

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"    --include-dir=PATT                   Only scan directories containing PATT\n"
	.size	.L.str.66, 80

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"    --detect-pua                         Detect Possibly Unwanted Applications\n"
	.size	.L.str.67, 80

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"    --no-mail                            Disable mail file support\n"
	.size	.L.str.68, 68

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"    --no-phishing-sigs                   Disable signature-based phishing detection\n"
	.size	.L.str.69, 85

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"    --no-phishing-scan-urls              Disable url-based phishing detection\n"
	.size	.L.str.70, 79

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"    --no-phishing-restrictedscan         Enable phishing detection for all domains (might lead to false positives!)\n"
	.size	.L.str.71, 117

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"    --phishing-ssl                       Always block SSL mismatches in URLs (phishing module)\n"
	.size	.L.str.72, 96

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"    --phishing-cloak                     Always block cloaked URLs (phishing module)\n"
	.size	.L.str.73, 86

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"    --no-algorithmic                     Disable algorithmic detection\n"
	.size	.L.str.74, 72

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"    --no-pe                              Disable PE analysis\n"
	.size	.L.str.75, 62

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"    --no-elf                             Disable ELF support\n"
	.size	.L.str.76, 62

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"    --no-ole2                            Disable OLE2 support\n"
	.size	.L.str.77, 63

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"    --no-pdf                             Disable PDF support\n"
	.size	.L.str.78, 62

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"    --no-html                            Disable HTML support\n"
	.size	.L.str.79, 63

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"    --no-archive                         Disable libclamav archive support\n"
	.size	.L.str.80, 76

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"    --detect-broken                      Try to detect broken executable files\n"
	.size	.L.str.81, 80

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"    --block-encrypted                    Block encrypted archives\n"
	.size	.L.str.82, 67

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"    --block-max                          Block archives that exceed limits\n"
	.size	.L.str.83, 76

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"    --mail-follow-urls                   Download and scan URLs\n"
	.size	.L.str.84, 65

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"    --max-space=#n                       Only extract first #n kilobytes from\n"
	.size	.L.str.85, 79

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"                                         archived files\n"
	.size	.L.str.86, 57

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"    --max-files=#n                       Only extract first #n files from\n"
	.size	.L.str.87, 75

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"                                         archives\n"
	.size	.L.str.88, 51

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"    --max-ratio=#n                       Maximum compression ratio limit\n"
	.size	.L.str.89, 74

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"    --max-recursion=#n                   Maximum archive recursion level\n"
	.size	.L.str.90, 74

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"    --max-dir-recursion=#n               Maximum directory recursion level\n"
	.size	.L.str.91, 76

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"    --max-mail-recursion=#n              Maximum mail recursion level\n"
	.size	.L.str.92, 71

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"    --unzip[=FULLPATH]                   Enable support for .zip files\n"
	.size	.L.str.93, 72

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"    --unrar[=FULLPATH]                   Enable support for .rar files\n"
	.size	.L.str.94, 72

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"    --arj[=FULLPATH]                     Enable support for .arj files\n"
	.size	.L.str.95, 72

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"    --unzoo[=FULLPATH]                   Enable support for .zoo files\n"
	.size	.L.str.96, 72

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"    --lha[=FULLPATH]                     Enable support for .lha files\n"
	.size	.L.str.97, 72

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"    --jar[=FULLPATH]                     Enable support for .jar files\n"
	.size	.L.str.98, 72

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"    --tar[=FULLPATH]                     Enable support for .tar files\n"
	.size	.L.str.99, 72

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"    --deb[=FULLPATH to ar]               Enable support for .deb files\n"
	.size	.L.str.100, 72

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"    --tgz[=FULLPATH]                     Enable support for .tar.gz, .tgz files\n\n"
	.size	.L.str.101, 82

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"config-file"
	.size	.L.str.102, 12

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"multiscan"
	.size	.L.str.103, 10

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"database"
	.size	.L.str.104, 9

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"force"
	.size	.L.str.105, 6

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"move"
	.size	.L.str.106, 5

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"remove"
	.size	.L.str.107, 7

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"exclude"
	.size	.L.str.108, 8

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"exclude-dir"
	.size	.L.str.109, 12

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"include"
	.size	.L.str.110, 8

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"include-dir"
	.size	.L.str.111, 12

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"detect-pua"
	.size	.L.str.112, 11

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"disable-archive"
	.size	.L.str.113, 16

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"no-archive"
	.size	.L.str.114, 11

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"detect-broken"
	.size	.L.str.115, 14

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"block-encrypted"
	.size	.L.str.116, 16

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"block-max"
	.size	.L.str.117, 10

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"no-pe"
	.size	.L.str.118, 6

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"no-elf"
	.size	.L.str.119, 7

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"no-ole2"
	.size	.L.str.120, 8

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"no-pdf"
	.size	.L.str.121, 7

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"no-html"
	.size	.L.str.122, 8

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"no-mail"
	.size	.L.str.123, 8

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"mail-follow-urls"
	.size	.L.str.124, 17

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"no-phishing-sigs"
	.size	.L.str.125, 17

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"no-phishing-scan-urls"
	.size	.L.str.126, 22

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"no-phishing-restrictedscan"
	.size	.L.str.127, 27

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"phishing-ssl"
	.size	.L.str.128, 13

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"phishing-cloak"
	.size	.L.str.129, 15

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"no-algorithmic"
	.size	.L.str.130, 15

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"unzip"
	.size	.L.str.131, 6

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"unrar"
	.size	.L.str.132, 6

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"arj"
	.size	.L.str.133, 4

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"unzoo"
	.size	.L.str.134, 6

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"lha"
	.size	.L.str.135, 4

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"jar"
	.size	.L.str.136, 4

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"tar"
	.size	.L.str.137, 4

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"tgz"
	.size	.L.str.138, 4

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"deb"
	.size	.L.str.139, 4

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"dev-ac-only"
	.size	.L.str.140, 12

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"dev-ac-depth"
	.size	.L.str.141, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
