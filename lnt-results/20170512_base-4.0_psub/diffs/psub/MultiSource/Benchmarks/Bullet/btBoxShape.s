	.text
	.file	"btBoxShape.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZNK10btBoxShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK10btBoxShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK10btBoxShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	(%r12), %rax
	callq	*88(%rax)
	movss	40(%r12), %xmm10        # xmm10 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm10
	movss	44(%r12), %xmm11        # xmm11 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm11
	addss	48(%r12), %xmm0
	movss	16(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movaps	.LCPI0_0(%rip), %xmm2   # xmm2 = [nan,nan,nan,nan]
	andps	%xmm2, %xmm4
	movss	20(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	andps	%xmm2, %xmm3
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	andps	%xmm2, %xmm6
	movss	32(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm7
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm1
	movss	40(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	andps	%xmm2, %xmm5
	movsd	48(%rbx), %xmm9         # xmm9 = mem[0],zero
	movss	56(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm7
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm4, %xmm10
	mulss	%xmm11, %xmm1
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm3, %xmm11
	addps	%xmm10, %xmm11
	mulss	%xmm0, %xmm5
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm6, %xmm0
	addps	%xmm11, %xmm0
	addss	%xmm7, %xmm1
	addss	%xmm1, %xmm5
	movaps	%xmm9, %xmm1
	subps	%xmm0, %xmm1
	movaps	%xmm8, %xmm2
	subss	%xmm5, %xmm2
	xorps	%xmm3, %xmm3
	xorps	%xmm4, %xmm4
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
	movlps	%xmm1, (%r15)
	movlps	%xmm4, 8(%r15)
	addps	%xmm9, %xmm0
	addss	%xmm8, %xmm5
	movss	%xmm5, %xmm3            # xmm3 = xmm5[0],xmm3[1,2,3]
	movlps	%xmm0, (%r14)
	movlps	%xmm3, 8(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	_ZNK10btBoxShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end0-_ZNK10btBoxShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1094713344              # float 12
	.text
	.globl	_ZNK10btBoxShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK10btBoxShape21calculateLocalInertiaEfR9btVector3: # @_ZNK10btBoxShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 64
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdi, %rbx
	movsd	40(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movss	48(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	callq	*88(%rax)
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movss	%xmm0, (%rsp)           # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movss	4(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	addss	%xmm1, %xmm4
	pshufd	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	addss	(%rsp), %xmm1           # 4-byte Folded Reload
	addss	8(%rsp), %xmm0          # 4-byte Folded Reload
	addss	%xmm4, %xmm4
	addss	%xmm1, %xmm1
	addss	%xmm0, %xmm0
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	divss	.LCPI1_0(%rip), %xmm3
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	movaps	%xmm1, %xmm2
	addss	%xmm0, %xmm2
	mulss	%xmm3, %xmm2
	mulss	%xmm4, %xmm4
	addss	%xmm4, %xmm0
	mulss	%xmm3, %xmm0
	addss	%xmm1, %xmm4
	mulss	%xmm3, %xmm4
	movss	%xmm2, (%r14)
	movss	%xmm0, 4(%r14)
	movss	%xmm4, 8(%r14)
	movl	$0, 12(%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	_ZNK10btBoxShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end1-_ZNK10btBoxShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.text._ZN10btBoxShapeD0Ev,"axG",@progbits,_ZN10btBoxShapeD0Ev,comdat
	.weak	_ZN10btBoxShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN10btBoxShapeD0Ev,@function
_ZN10btBoxShapeD0Ev:                    # @_ZN10btBoxShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB2_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN10btBoxShapeD0Ev, .Lfunc_end2-_ZN10btBoxShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN10btBoxShape15setLocalScalingERK9btVector3,"axG",@progbits,_ZN10btBoxShape15setLocalScalingERK9btVector3,comdat
	.weak	_ZN10btBoxShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN10btBoxShape15setLocalScalingERK9btVector3,@function
_ZN10btBoxShape15setLocalScalingERK9btVector3: # @_ZN10btBoxShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 80
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movsd	40(%rbx), %xmm2         # xmm2 = mem[0],zero
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	unpcklps	16(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	addps	%xmm1, %xmm2
	movss	48(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movsd	24(%rbx), %xmm0         # xmm0 = mem[0],zero
	divps	%xmm0, %xmm2
	movaps	%xmm2, 16(%rsp)         # 16-byte Spill
	divss	32(%rbx), %xmm1
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	movsd	24(%rbx), %xmm0         # xmm0 = mem[0],zero
	mulps	16(%rsp), %xmm0         # 16-byte Folded Reload
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	32(%rbx), %xmm1
	subps	32(%rsp), %xmm0         # 16-byte Folded Reload
	subss	12(%rsp), %xmm1         # 4-byte Folded Reload
	movaps	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, 40(%rbx)
	movlps	%xmm1, 48(%rbx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	_ZN10btBoxShape15setLocalScalingERK9btVector3, .Lfunc_end3-_ZN10btBoxShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end4:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end4-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK10btBoxShape7getNameEv,"axG",@progbits,_ZNK10btBoxShape7getNameEv,comdat
	.weak	_ZNK10btBoxShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape7getNameEv,@function
_ZNK10btBoxShape7getNameEv:             # @_ZNK10btBoxShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end5:
	.size	_ZNK10btBoxShape7getNameEv, .Lfunc_end5-_ZNK10btBoxShape7getNameEv
	.cfi_endproc

	.section	.text._ZN10btBoxShape9setMarginEf,"axG",@progbits,_ZN10btBoxShape9setMarginEf,comdat
	.weak	_ZN10btBoxShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN10btBoxShape9setMarginEf,@function
_ZN10btBoxShape9setMarginEf:            # @_ZN10btBoxShape9setMarginEf
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
	subq	$64, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 80
.Lcfi26:
	.cfi_offset %rbx, -16
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movsd	40(%rbx), %xmm2         # xmm2 = mem[0],zero
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	unpcklps	(%rsp), %xmm1   # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	addps	%xmm2, %xmm1
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	addss	48(%rbx), %xmm0
	movss	%xmm0, (%rsp)           # 4-byte Spill
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 56(%rbx)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	unpcklps	48(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	movaps	32(%rsp), %xmm2         # 16-byte Reload
	subps	%xmm1, %xmm2
	movss	(%rsp), %xmm1           # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm2, 40(%rbx)
	movlps	%xmm0, 48(%rbx)
	addq	$64, %rsp
	popq	%rbx
	retq
.Lfunc_end6:
	.size	_ZN10btBoxShape9setMarginEf, .Lfunc_end6-_ZN10btBoxShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end7:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end7-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK10btBoxShape24localGetSupportingVertexERK9btVector3,"axG",@progbits,_ZNK10btBoxShape24localGetSupportingVertexERK9btVector3,comdat
	.weak	_ZNK10btBoxShape24localGetSupportingVertexERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape24localGetSupportingVertexERK9btVector3,@function
_ZNK10btBoxShape24localGetSupportingVertexERK9btVector3: # @_ZNK10btBoxShape24localGetSupportingVertexERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 80
.Lcfi30:
	.cfi_offset %rbx, -24
.Lcfi31:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movss	40(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movss	44(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movss	48(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	(%rbx), %rax
	callq	*88(%rax)
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	32(%rsp), %xmm5         # 16-byte Reload
	addss	4(%rsp), %xmm5          # 4-byte Folded Reload
	movaps	16(%rsp), %xmm6         # 16-byte Reload
	addss	8(%rsp), %xmm6          # 4-byte Folded Reload
	addss	12(%rsp), %xmm0         # 4-byte Folded Reload
	movaps	.LCPI8_0(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm3, %xmm3
	xorps	%xmm2, %xmm2
	cmpless	(%r14), %xmm2
	movaps	%xmm2, %xmm4
	andps	%xmm5, %xmm4
	xorps	%xmm1, %xmm5
	andnps	%xmm5, %xmm2
	orps	%xmm4, %xmm2
	xorps	%xmm4, %xmm4
	cmpless	4(%r14), %xmm4
	movaps	%xmm4, %xmm5
	andps	%xmm6, %xmm5
	xorps	%xmm1, %xmm6
	andnps	%xmm6, %xmm4
	orps	%xmm5, %xmm4
	xorps	%xmm0, %xmm1
	cmpless	8(%r14), %xmm3
	andps	%xmm3, %xmm0
	andnps	%xmm1, %xmm3
	orps	%xmm0, %xmm3
	unpcklps	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movaps	%xmm2, %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	_ZNK10btBoxShape24localGetSupportingVertexERK9btVector3, .Lfunc_end8-_ZNK10btBoxShape24localGetSupportingVertexERK9btVector3
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK10btBoxShape37localGetSupportingVertexWithoutMarginERK9btVector3,"axG",@progbits,_ZNK10btBoxShape37localGetSupportingVertexWithoutMarginERK9btVector3,comdat
	.weak	_ZNK10btBoxShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK10btBoxShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK10btBoxShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	movss	40(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	44(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	.LCPI9_0(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm0
	cmpless	(%rsi), %xmm0
	movaps	%xmm0, %xmm4
	andps	%xmm2, %xmm0
	movaps	%xmm2, %xmm5
	xorps	%xmm1, %xmm5
	xorps	%xmm2, %xmm2
	andnps	%xmm5, %xmm4
	orps	%xmm4, %xmm0
	xorps	%xmm4, %xmm4
	cmpless	4(%rsi), %xmm4
	movaps	%xmm4, %xmm5
	andps	%xmm3, %xmm4
	xorps	%xmm1, %xmm3
	andnps	%xmm3, %xmm5
	orps	%xmm5, %xmm4
	movss	48(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm1
	cmpless	8(%rsi), %xmm2
	movaps	%xmm2, %xmm5
	andnps	%xmm1, %xmm5
	andps	%xmm3, %xmm2
	orps	%xmm5, %xmm2
	unpcklps	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	retq
.Lfunc_end9:
	.size	_ZNK10btBoxShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end9-_ZNK10btBoxShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK10btBoxShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,"axG",@progbits,_ZNK10btBoxShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,comdat
	.weak	_ZNK10btBoxShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK10btBoxShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK10btBoxShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	testl	%ecx, %ecx
	jle	.LBB10_11
# BB#1:                                 # %.lr.ph
	movl	%ecx, %eax
	cmpl	$3, %ecx
	jbe	.LBB10_2
# BB#5:                                 # %min.iters.checked
	andl	$3, %ecx
	movl	$4, %r8d
	cmovneq	%rcx, %r8
	movq	%rax, %r9
	subq	%r8, %r9
	je	.LBB10_2
# BB#6:                                 # %vector.memcheck
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 16
.Lcfi33:
	.cfi_offset %rbx, -16
	leaq	48(%rdi), %r10
	movq	%rax, %rcx
	shlq	$4, %rcx
	leaq	(%rdx,%rcx), %r11
	leaq	-4(%rsi,%rcx), %rcx
	cmpq	%rcx, %rdx
	sbbb	%cl, %cl
	cmpq	%r11, %rsi
	sbbb	%bl, %bl
	andb	%cl, %bl
	cmpq	%r10, %rdx
	sbbb	%cl, %cl
	cmpq	%r11, %r10
	sbbb	%r11b, %r11b
	xorl	%r10d, %r10d
	testb	$1, %bl
	popq	%rbx
	jne	.LBB10_3
# BB#7:                                 # %vector.memcheck
	andb	%r11b, %cl
	andb	$1, %cl
	jne	.LBB10_3
# BB#8:                                 # %vector.body.preheader
	movss	40(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	44(%rdi), %xmm9         # xmm9 = mem[0],zero,zero,zero
	shufps	$0, %xmm8, %xmm8        # xmm8 = xmm8[0,0,0,0]
	movaps	.LCPI10_0(%rip), %xmm12 # xmm12 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm8, %xmm10
	xorps	%xmm12, %xmm10
	shufps	$0, %xmm9, %xmm9        # xmm9 = xmm9[0,0,0,0]
	movaps	%xmm9, %xmm11
	xorps	%xmm12, %xmm11
	movss	48(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	shufps	$0, %xmm5, %xmm5        # xmm5 = xmm5[0,0,0,0]
	xorps	%xmm5, %xmm12
	movq	%r9, %r10
	movq	%rsi, %r11
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB10_9:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r11), %xmm4
	movups	16(%r11), %xmm2
	movups	32(%r11), %xmm6
	movups	48(%r11), %xmm7
	movaps	%xmm7, %xmm3
	shufps	$0, %xmm6, %xmm3        # xmm3 = xmm3[0,0],xmm6[0,0]
	movaps	%xmm4, %xmm0
	unpcklps	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	shufps	$36, %xmm3, %xmm0       # xmm0 = xmm0[0,1],xmm3[2,0]
	movaps	%xmm6, %xmm3
	unpcklps	%xmm7, %xmm3    # xmm3 = xmm3[0],xmm7[0],xmm3[1],xmm7[1]
	movaps	%xmm2, %xmm1
	shufps	$17, %xmm4, %xmm1       # xmm1 = xmm1[1,0],xmm4[1,0]
	shufps	$226, %xmm3, %xmm1      # xmm1 = xmm1[2,0],xmm3[2,3]
	shufps	$34, %xmm6, %xmm7       # xmm7 = xmm7[2,0],xmm6[2,0]
	unpckhps	%xmm2, %xmm4    # xmm4 = xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	shufps	$36, %xmm7, %xmm4       # xmm4 = xmm4[0,1],xmm7[2,0]
	xorps	%xmm7, %xmm7
	cmpleps	%xmm0, %xmm7
	movaps	%xmm8, %xmm0
	andps	%xmm7, %xmm0
	andnps	%xmm10, %xmm7
	orps	%xmm0, %xmm7
	xorps	%xmm6, %xmm6
	cmpleps	%xmm1, %xmm6
	movaps	%xmm9, %xmm0
	andps	%xmm6, %xmm0
	andnps	%xmm11, %xmm6
	orps	%xmm0, %xmm6
	xorps	%xmm1, %xmm1
	cmpleps	%xmm4, %xmm1
	movaps	%xmm5, %xmm0
	andps	%xmm1, %xmm0
	andnps	%xmm12, %xmm1
	orps	%xmm0, %xmm1
	movaps	%xmm7, %xmm0
	unpckhps	%xmm6, %xmm0    # xmm0 = xmm0[2],xmm6[2],xmm0[3],xmm6[3]
	movaps	%xmm7, %xmm2
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movaps	%xmm6, %xmm3
	shufps	$17, %xmm7, %xmm3       # xmm3 = xmm3[1,0],xmm7[1,0]
	shufps	$210, %xmm1, %xmm3      # xmm3 = xmm3[2,0],xmm1[1,3]
	shufps	$51, %xmm7, %xmm6       # xmm6 = xmm6[3,0],xmm7[3,0]
	shufps	$242, %xmm1, %xmm6      # xmm6 = xmm6[2,0],xmm1[3,3]
	movsd	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1]
	xorps	%xmm0, %xmm0
	shufps	$35, %xmm1, %xmm0       # xmm0 = xmm0[3,0],xmm1[2,0]
	shufps	$36, %xmm0, %xmm1       # xmm1 = xmm1[0,1],xmm0[2,0]
	xorps	%xmm0, %xmm0
	shufps	$35, %xmm2, %xmm0       # xmm0 = xmm0[3,0],xmm2[2,0]
	shufps	$36, %xmm0, %xmm2       # xmm2 = xmm2[0,1],xmm0[2,0]
	xorps	%xmm0, %xmm0
	shufps	$35, %xmm3, %xmm0       # xmm0 = xmm0[3,0],xmm3[2,0]
	shufps	$36, %xmm0, %xmm3       # xmm3 = xmm3[0,1],xmm0[2,0]
	xorps	%xmm0, %xmm0
	shufps	$35, %xmm6, %xmm0       # xmm0 = xmm0[3,0],xmm6[2,0]
	shufps	$36, %xmm0, %xmm6       # xmm6 = xmm6[0,1],xmm0[2,0]
	movups	%xmm6, 48(%rcx)
	movups	%xmm3, 16(%rcx)
	movups	%xmm2, (%rcx)
	movups	%xmm1, 32(%rcx)
	addq	$64, %rcx
	addq	$64, %r11
	addq	$-4, %r10
	jne	.LBB10_9
# BB#10:                                # %middle.block
	testq	%r8, %r8
	movq	%r9, %r10
	jne	.LBB10_3
	jmp	.LBB10_11
.LBB10_2:
	xorl	%r10d, %r10d
.LBB10_3:                               # %scalar.ph.preheader
	subq	%r10, %rax
	shlq	$4, %r10
	leaq	8(%rsi,%r10), %rcx
	leaq	12(%rdx,%r10), %rdx
	movaps	.LCPI10_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB10_4:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	40(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	44(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	cmpless	-8(%rcx), %xmm1
	movaps	%xmm1, %xmm4
	andps	%xmm2, %xmm1
	xorps	%xmm0, %xmm2
	andnps	%xmm2, %xmm4
	orps	%xmm4, %xmm1
	xorps	%xmm2, %xmm2
	cmpless	-4(%rcx), %xmm2
	movaps	%xmm2, %xmm4
	andps	%xmm3, %xmm2
	xorps	%xmm0, %xmm3
	andnps	%xmm3, %xmm4
	orps	%xmm4, %xmm2
	movss	48(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm4, %xmm4
	cmpless	(%rcx), %xmm4
	movaps	%xmm4, %xmm5
	andps	%xmm3, %xmm4
	xorps	%xmm0, %xmm3
	andnps	%xmm3, %xmm5
	orps	%xmm5, %xmm4
	movss	%xmm1, -12(%rdx)
	movss	%xmm2, -8(%rdx)
	movss	%xmm4, -4(%rdx)
	movl	$0, (%rdx)
	addq	$16, %rcx
	addq	$16, %rdx
	decq	%rax
	jne	.LBB10_4
.LBB10_11:                              # %._crit_edge
	retq
.Lfunc_end10:
	.size	_ZNK10btBoxShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end10-_ZNK10btBoxShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.text._ZNK10btBoxShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK10btBoxShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK10btBoxShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK10btBoxShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK10btBoxShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	movl	$6, %eax
	retq
.Lfunc_end11:
	.size	_ZNK10btBoxShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end11-_ZNK10btBoxShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.text._ZNK10btBoxShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK10btBoxShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK10btBoxShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK10btBoxShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK10btBoxShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	cmpl	$5, %esi
	ja	.LBB12_2
# BB#1:                                 # %switch.lookup
	movslq	%esi, %rax
	movl	.Lswitch.table.2(,%rax,4), %ecx
	movl	.Lswitch.table.3(,%rax,4), %esi
	movl	.Lswitch.table.4(,%rax,4), %eax
	movl	%ecx, (%rdx)
	movl	%esi, 4(%rdx)
	movl	%eax, 8(%rdx)
	movl	$0, 12(%rdx)
.LBB12_2:
	retq
.Lfunc_end12:
	.size	_ZNK10btBoxShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end12-_ZNK10btBoxShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text._ZNK10btBoxShape14getNumVerticesEv,"axG",@progbits,_ZNK10btBoxShape14getNumVerticesEv,comdat
	.weak	_ZNK10btBoxShape14getNumVerticesEv
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape14getNumVerticesEv,@function
_ZNK10btBoxShape14getNumVerticesEv:     # @_ZNK10btBoxShape14getNumVerticesEv
	.cfi_startproc
# BB#0:
	movl	$8, %eax
	retq
.Lfunc_end13:
	.size	_ZNK10btBoxShape14getNumVerticesEv, .Lfunc_end13-_ZNK10btBoxShape14getNumVerticesEv
	.cfi_endproc

	.section	.text._ZNK10btBoxShape11getNumEdgesEv,"axG",@progbits,_ZNK10btBoxShape11getNumEdgesEv,comdat
	.weak	_ZNK10btBoxShape11getNumEdgesEv
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape11getNumEdgesEv,@function
_ZNK10btBoxShape11getNumEdgesEv:        # @_ZNK10btBoxShape11getNumEdgesEv
	.cfi_startproc
# BB#0:
	movl	$12, %eax
	retq
.Lfunc_end14:
	.size	_ZNK10btBoxShape11getNumEdgesEv, .Lfunc_end14-_ZNK10btBoxShape11getNumEdgesEv
	.cfi_endproc

	.section	.text._ZNK10btBoxShape7getEdgeEiR9btVector3S1_,"axG",@progbits,_ZNK10btBoxShape7getEdgeEiR9btVector3S1_,comdat
	.weak	_ZNK10btBoxShape7getEdgeEiR9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape7getEdgeEiR9btVector3S1_,@function
_ZNK10btBoxShape7getEdgeEiR9btVector3S1_: # @_ZNK10btBoxShape7getEdgeEiR9btVector3S1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 32
.Lcfi37:
	.cfi_offset %rbx, -32
.Lcfi38:
	.cfi_offset %r14, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%esi, %eax
	movq	%rdi, %rbx
	xorl	%esi, %esi
	cmpl	$11, %eax
	movl	$0, %ebp
	ja	.LBB15_2
# BB#1:                                 # %switch.lookup
	cltq
	movl	.Lswitch.table(,%rax,4), %esi
	movl	.Lswitch.table.1(,%rax,4), %ebp
.LBB15_2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*168(%rax)
	movq	(%rbx), %rax
	movq	168(%rax), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end15:
	.size	_ZNK10btBoxShape7getEdgeEiR9btVector3S1_, .Lfunc_end15-_ZNK10btBoxShape7getEdgeEiR9btVector3S1_
	.cfi_endproc

	.section	.text._ZNK10btBoxShape9getVertexEiR9btVector3,"axG",@progbits,_ZNK10btBoxShape9getVertexEiR9btVector3,comdat
	.weak	_ZNK10btBoxShape9getVertexEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape9getVertexEiR9btVector3,@function
_ZNK10btBoxShape9getVertexEiR9btVector3: # @_ZNK10btBoxShape9getVertexEiR9btVector3
	.cfi_startproc
# BB#0:
	movss	40(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	44(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	48(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movl	%esi, %eax
	andl	$1, %eax
	cvtsi2ssl	%eax, %xmm4
	xorl	$1, %eax
	cvtsi2ssl	%eax, %xmm1
	mulss	%xmm2, %xmm1
	mulss	%xmm2, %xmm4
	subss	%xmm4, %xmm1
	movl	%esi, %eax
	shrl	%eax
	andl	$1, %eax
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%eax, %xmm2
	xorl	$1, %eax
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%eax, %xmm4
	mulss	%xmm3, %xmm4
	mulss	%xmm3, %xmm2
	subss	%xmm2, %xmm4
	shrl	$2, %esi
	andl	$1, %esi
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%esi, %xmm2
	xorl	$1, %esi
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%esi, %xmm3
	mulss	%xmm0, %xmm3
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm3
	movss	%xmm1, (%rdx)
	movss	%xmm4, 4(%rdx)
	movss	%xmm3, 8(%rdx)
	movl	$0, 12(%rdx)
	retq
.Lfunc_end16:
	.size	_ZNK10btBoxShape9getVertexEiR9btVector3, .Lfunc_end16-_ZNK10btBoxShape9getVertexEiR9btVector3
	.cfi_endproc

	.section	.text._ZNK10btBoxShape12getNumPlanesEv,"axG",@progbits,_ZNK10btBoxShape12getNumPlanesEv,comdat
	.weak	_ZNK10btBoxShape12getNumPlanesEv
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape12getNumPlanesEv,@function
_ZNK10btBoxShape12getNumPlanesEv:       # @_ZNK10btBoxShape12getNumPlanesEv
	.cfi_startproc
# BB#0:
	movl	$6, %eax
	retq
.Lfunc_end17:
	.size	_ZNK10btBoxShape12getNumPlanesEv, .Lfunc_end17-_ZNK10btBoxShape12getNumPlanesEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK10btBoxShape8getPlaneER9btVector3S1_i,"axG",@progbits,_ZNK10btBoxShape8getPlaneER9btVector3S1_i,comdat
	.weak	_ZNK10btBoxShape8getPlaneER9btVector3S1_i
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape8getPlaneER9btVector3S1_i,@function
_ZNK10btBoxShape8getPlaneER9btVector3S1_i: # @_ZNK10btBoxShape8getPlaneER9btVector3S1_i
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi43:
	.cfi_def_cfa_offset 64
.Lcfi44:
	.cfi_offset %rbx, -32
.Lcfi45:
	.cfi_offset %r14, -24
.Lcfi46:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	(%r15), %rax
	leaq	16(%rsp), %rsi
	movl	%ecx, %edx
	callq	*200(%rax)
	movq	16(%rsp), %xmm0         # xmm0 = mem[0],zero
	pshufd	$212, %xmm0, %xmm0      # xmm0 = xmm0[0,1,1,3]
	movl	24(%rsp), %eax
	movd	%xmm0, (%rbx)
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	movd	%xmm1, 4(%rbx)
	movl	%eax, 8(%rbx)
	movl	$0, 12(%rbx)
	movq	(%r15), %rcx
	movq	96(%rcx), %rcx
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pxor	.LCPI18_0(%rip), %xmm0
	xorl	$-2147483648, %eax      # imm = 0x80000000
	movd	%eax, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movq	%xmm0, (%rsp)
	movlps	%xmm2, 8(%rsp)
	movq	%rsp, %rsi
	movq	%r15, %rdi
	callq	*%rcx
	movq	%xmm0, (%r14)
	movlps	%xmm1, 8(%r14)
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end18:
	.size	_ZNK10btBoxShape8getPlaneER9btVector3S1_i, .Lfunc_end18-_ZNK10btBoxShape8getPlaneER9btVector3S1_i
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI19_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK10btBoxShape8isInsideERK9btVector3f,"axG",@progbits,_ZNK10btBoxShape8isInsideERK9btVector3f,comdat
	.weak	_ZNK10btBoxShape8isInsideERK9btVector3f
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape8isInsideERK9btVector3f,@function
_ZNK10btBoxShape8isInsideERK9btVector3f: # @_ZNK10btBoxShape8isInsideERK9btVector3f
	.cfi_startproc
# BB#0:
	movss	40(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	addss	%xmm0, %xmm3
	ucomiss	%xmm2, %xmm3
	jae	.LBB19_2
# BB#1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB19_2:
	xorps	.LCPI19_0(%rip), %xmm1
	subss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm2
	jae	.LBB19_4
# BB#3:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB19_4:
	movss	44(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	addss	%xmm0, %xmm3
	ucomiss	%xmm2, %xmm3
	jae	.LBB19_6
# BB#5:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB19_6:
	xorps	.LCPI19_0(%rip), %xmm1
	subss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm2
	jae	.LBB19_8
# BB#7:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB19_8:
	movss	48(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	addss	%xmm0, %xmm3
	ucomiss	%xmm2, %xmm3
	jae	.LBB19_10
# BB#9:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB19_10:
	xorps	.LCPI19_0(%rip), %xmm1
	subss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm2
	setae	%al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end19:
	.size	_ZNK10btBoxShape8isInsideERK9btVector3f, .Lfunc_end19-_ZNK10btBoxShape8isInsideERK9btVector3f
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI20_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK10btBoxShape16getPlaneEquationER9btVector4i,"axG",@progbits,_ZNK10btBoxShape16getPlaneEquationER9btVector4i,comdat
	.weak	_ZNK10btBoxShape16getPlaneEquationER9btVector4i
	.p2align	4, 0x90
	.type	_ZNK10btBoxShape16getPlaneEquationER9btVector4i,@function
_ZNK10btBoxShape16getPlaneEquationER9btVector4i: # @_ZNK10btBoxShape16getPlaneEquationER9btVector4i
	.cfi_startproc
# BB#0:
	cmpl	$5, %edx
	ja	.LBB20_10
# BB#1:
	movss	40(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	44(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	48(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	xorl	%ecx, %ecx
	movl	$-1082130432, %eax      # imm = 0xBF800000
	movl	%edx, %edi
	xorl	%edx, %edx
	jmpq	*.LJTI20_0(,%rdi,8)
.LBB20_2:
	movl	$1065353216, %eax       # imm = 0x3F800000
	xorl	%edx, %edx
	jmp	.LBB20_9
.LBB20_3:
	movl	$1065353216, %ecx       # imm = 0x3F800000
	jmp	.LBB20_4
.LBB20_5:
	movl	$-1082130432, %ecx      # imm = 0xBF800000
.LBB20_4:                               # %.sink.split
	xorl	%eax, %eax
	movaps	%xmm1, %xmm0
	xorl	%edx, %edx
	jmp	.LBB20_9
.LBB20_6:
	movl	$1065353216, %edx       # imm = 0x3F800000
	jmp	.LBB20_8
.LBB20_7:
	movl	$-1082130432, %edx      # imm = 0xBF800000
.LBB20_8:                               # %.sink.split
	xorl	%eax, %eax
	movaps	%xmm2, %xmm0
	xorl	%ecx, %ecx
.LBB20_9:                               # %.sink.split
	xorps	.LCPI20_0(%rip), %xmm0
	movl	%eax, (%rsi)
	movl	%ecx, 4(%rsi)
	movl	%edx, 8(%rsi)
	movss	%xmm0, 12(%rsi)
.LBB20_10:
	retq
.Lfunc_end20:
	.size	_ZNK10btBoxShape16getPlaneEquationER9btVector4i, .Lfunc_end20-_ZNK10btBoxShape16getPlaneEquationER9btVector4i
	.cfi_endproc
	.section	.rodata._ZNK10btBoxShape16getPlaneEquationER9btVector4i,"aG",@progbits,_ZNK10btBoxShape16getPlaneEquationER9btVector4i,comdat
	.p2align	3
.LJTI20_0:
	.quad	.LBB20_2
	.quad	.LBB20_9
	.quad	.LBB20_3
	.quad	.LBB20_5
	.quad	.LBB20_6
	.quad	.LBB20_7

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end21:
	.size	__clang_call_terminate, .Lfunc_end21-__clang_call_terminate

	.type	_ZTV10btBoxShape,@object # @_ZTV10btBoxShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV10btBoxShape
	.p2align	3
_ZTV10btBoxShape:
	.quad	0
	.quad	_ZTI10btBoxShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN10btBoxShapeD0Ev
	.quad	_ZNK10btBoxShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN10btBoxShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK10btBoxShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK10btBoxShape7getNameEv
	.quad	_ZN10btBoxShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK10btBoxShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK10btBoxShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK10btBoxShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK10btBoxShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK10btBoxShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	_ZNK10btBoxShape14getNumVerticesEv
	.quad	_ZNK10btBoxShape11getNumEdgesEv
	.quad	_ZNK10btBoxShape7getEdgeEiR9btVector3S1_
	.quad	_ZNK10btBoxShape9getVertexEiR9btVector3
	.quad	_ZNK10btBoxShape12getNumPlanesEv
	.quad	_ZNK10btBoxShape8getPlaneER9btVector3S1_i
	.quad	_ZNK10btBoxShape8isInsideERK9btVector3f
	.quad	_ZNK10btBoxShape16getPlaneEquationER9btVector4i
	.size	_ZTV10btBoxShape, 224

	.type	_ZTS10btBoxShape,@object # @_ZTS10btBoxShape
	.globl	_ZTS10btBoxShape
_ZTS10btBoxShape:
	.asciz	"10btBoxShape"
	.size	_ZTS10btBoxShape, 13

	.type	_ZTI10btBoxShape,@object # @_ZTI10btBoxShape
	.globl	_ZTI10btBoxShape
	.p2align	4
_ZTI10btBoxShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10btBoxShape
	.quad	_ZTI23btPolyhedralConvexShape
	.size	_ZTI10btBoxShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Box"
	.size	.L.str, 4

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.size	.Lswitch.table, 48

	.type	.Lswitch.table.1,@object # @switch.table.1
	.p2align	4
.Lswitch.table.1:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.size	.Lswitch.table.1, 48

	.type	.Lswitch.table.2,@object # @switch.table.2
	.p2align	4
.Lswitch.table.2:
	.long	1065353216              # 0x3f800000
	.long	3212836864              # 0xbf800000
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	.Lswitch.table.2, 24

	.type	.Lswitch.table.3,@object # @switch.table.3
	.p2align	4
.Lswitch.table.3:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1065353216              # 0x3f800000
	.long	3212836864              # 0xbf800000
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	.Lswitch.table.3, 24

	.type	.Lswitch.table.4,@object # @switch.table.4
	.p2align	4
.Lswitch.table.4:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1065353216              # 0x3f800000
	.long	3212836864              # 0xbf800000
	.size	.Lswitch.table.4, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
