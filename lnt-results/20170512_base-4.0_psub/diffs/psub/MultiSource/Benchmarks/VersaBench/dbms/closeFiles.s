	.text
	.file	"closeFiles.bc"
	.globl	closeFiles
	.p2align	4, 0x90
	.type	closeFiles,@function
closeFiles:                             # @closeFiles
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	callq	fclose
	testl	%eax, %eax
	je	.LBB0_2
# BB#1:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$closeFiles.name, %edi
	movl	$1, %esi
	callq	errorMessage
.LBB0_2:
	movq	%rbx, %rdi
	callq	fclose
	testl	%eax, %eax
	je	.LBB0_4
# BB#3:
	movl	$.L.str.1, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$closeFiles.name, %edi
	movl	$1, %esi
	callq	errorMessage
.LBB0_4:
	movq	%r14, %rdi
	callq	fclose
	testl	%eax, %eax
	je	.LBB0_5
# BB#6:
	movl	$.L.str.2, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$closeFiles.name, %edi
	movl	$1, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	errorMessage            # TAILCALL
.LBB0_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	closeFiles, .Lfunc_end0-closeFiles
	.cfi_endproc

	.type	closeFiles.name,@object # @closeFiles.name
	.data
closeFiles.name:
	.asciz	"closeFiles"
	.size	closeFiles.name, 11

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"error closing input file"
	.size	.L.str, 25

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"error closing output file"
	.size	.L.str.1, 26

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"error closing metric file"
	.size	.L.str.2, 26


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
