	.text
	.file	"read_dmatrix.bc"
	.globl	read_dmatrix
	.p2align	4, 0x90
	.type	read_dmatrix,@function
read_dmatrix:                           # @read_dmatrix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	$.L.str, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %r14
	movl	$0, 4(%rsp)
	movl	$0, (%rsp)
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rax
	cmovneq	%rax, %rbx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	movq	%rsp, %rdx
	leaq	4(%rsp), %rcx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	movslq	(%rsp), %rbp
	testq	%rbp, %rbp
	movl	4(%rsp), %ebx
	movq	%rbp, (%r12)
	je	.LBB0_1
# BB#2:                                 # %.lr.ph.i
	movq	%rbp, %rdi
	shlq	$4, %rdi
	callq	malloc
	movq	%rax, 8(%r12)
	testl	%ebx, %ebx
	je	.LBB0_7
# BB#3:                                 # %.lr.ph.split.i.preheader
	movslq	%ebx, %rcx
	leaq	(,%rcx,8), %r13
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rcx, (%rax)
	movq	%r13, %rdi
	movq	%rax, %r15
	callq	malloc
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%rax, 8(%r15)
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r13, %rdx
	callq	memset
	cmpl	$1, %ebp
	movq	%rbp, %rcx
	je	.LBB0_19
# BB#4:                                 # %.lr.ph.split..lr.ph.split_crit_edge.i.preheader
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rbp)
	movq	%rcx, %r15
	movq	%r13, %rdi
	callq	malloc
	movq	%rax, 24(%rbp)
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r13, %rdx
	callq	memset
	movq	%r15, 8(%rsp)           # 8-byte Spill
	cmpl	$2, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%r13, %r15
	je	.LBB0_17
# BB#5:                                 # %.lr.ph.split..lr.ph.split_crit_edge.i..lr.ph.split..lr.ph.split_crit_edge.i_crit_edge.preheader
	movq	8(%rsp), %rbp           # 8-byte Reload
	addq	$-2, %rbp
	movl	$40, %r13d
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph.split..lr.ph.split_crit_edge.i..lr.ph.split..lr.ph.split_crit_edge.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rax, -8(%rcx,%r13)
	movq	%r15, %rdi
	callq	malloc
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx,%r13)
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r15, %rdx
	callq	memset
	movq	16(%rsp), %rax          # 8-byte Reload
	addq	$16, %r13
	decq	%rbp
	jne	.LBB0_6
	jmp	.LBB0_17
.LBB0_1:                                # %dvarray_init.exit.preheader.thread37
	movq	$0, 8(%r12)
	jmp	.LBB0_18
.LBB0_7:                                # %.lr.ph.split.us.i.preheader
	cmpl	$1, %ebp
	xorps	%xmm0, %xmm0
	movq	%rax, %rdx
	movups	%xmm0, (%rax)
	movq	%rbp, %rcx
	je	.LBB0_19
# BB#8:                                 # %.lr.ph.split.us..lr.ph.split.us_crit_edge.i.preheader
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpl	$2, %ecx
	movups	%xmm0, 16(%rdx)
	je	.LBB0_17
# BB#9:                                 # %.lr.ph.split.us..lr.ph.split.us_crit_edge.i..lr.ph.split.us..lr.ph.split.us_crit_edge.i_crit_edge.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	2(%rax), %edx
	leaq	-3(%rax), %rcx
	andq	$3, %rdx
	je	.LBB0_10
# BB#11:                                # %.lr.ph.split.us..lr.ph.split.us_crit_edge.i..lr.ph.split.us..lr.ph.split.us_crit_edge.i_crit_edge.prol.preheader
	xorl	%eax, %eax
	movl	$32, %esi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_12:                               # %.lr.ph.split.us..lr.ph.split.us_crit_edge.i..lr.ph.split.us..lr.ph.split.us_crit_edge.i_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rdi
	movups	%xmm0, (%rdi,%rsi)
	incq	%rax
	addq	$16, %rsi
	cmpq	%rax, %rdx
	jne	.LBB0_12
# BB#13:                                # %.lr.ph.split.us..lr.ph.split.us_crit_edge.i..lr.ph.split.us..lr.ph.split.us_crit_edge.i_crit_edge.prol.loopexit.unr-lcssa
	addq	$2, %rax
	cmpq	$3, %rcx
	jae	.LBB0_15
	jmp	.LBB0_17
.LBB0_10:
	movl	$2, %eax
	cmpq	$3, %rcx
	jb	.LBB0_17
.LBB0_15:                               # %.lr.ph.split.us..lr.ph.split.us_crit_edge.i..lr.ph.split.us..lr.ph.split.us_crit_edge.i_crit_edge.preheader.new
	movq	8(%rsp), %rcx           # 8-byte Reload
	subq	%rax, %rcx
	shlq	$4, %rax
	addq	$48, %rax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_16:                               # %.lr.ph.split.us..lr.ph.split.us_crit_edge.i..lr.ph.split.us..lr.ph.split.us_crit_edge.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rdx
	movups	%xmm0, -48(%rdx,%rax)
	movq	8(%r12), %rdx
	movups	%xmm0, -32(%rdx,%rax)
	movq	8(%r12), %rdx
	movups	%xmm0, -16(%rdx,%rax)
	movq	8(%r12), %rdx
	movups	%xmm0, (%rdx,%rax)
	addq	$64, %rax
	addq	$-4, %rcx
	jne	.LBB0_16
.LBB0_17:                               # %dvarray_init.exit.preheader
	movq	8(%rsp), %rcx           # 8-byte Reload
	testl	%ecx, %ecx
	jle	.LBB0_18
.LBB0_19:                               # %.preheader.lr.ph
	addq	$8, %r12
	leaq	32(%rsp), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_20:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_22 Depth 2
	testl	%ebx, %ebx
	movl	$0, %r13d
	jle	.LBB0_24
# BB#21:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_20 Depth=1
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	fscanf
	movq	32(%rsp), %rax
	movq	(%r12), %rcx
	movq	%rbp, %rdx
	shlq	$4, %rdx
	movq	8(%rcx,%rdx), %rcx
	movq	%rax, (%rcx,%r13,8)
	incq	%r13
	movslq	4(%rsp), %rbx
	cmpq	%rbx, %r13
	jl	.LBB0_22
# BB#23:                                # %dvarray_init.exit.loopexit
                                        #   in Loop: Header=BB0_20 Depth=1
	movl	(%rsp), %ecx
.LBB0_24:                               # %dvarray_init.exit
                                        #   in Loop: Header=BB0_20 Depth=1
	incq	%rbp
	movslq	%ecx, %rax
	cmpq	%rax, %rbp
	jl	.LBB0_20
	jmp	.LBB0_25
.LBB0_18:                               # %dvarray_init.exit.preheader.dvarray_init.exit._crit_edge_crit_edge
	addq	$8, %r12
	xorl	%ebp, %ebp
                                        # implicit-def: %R13D
.LBB0_25:                               # %dvarray_init.exit._crit_edge
	movq	(%r12), %rax
	movslq	%ebp, %rbx
	movq	%rbx, %rcx
	shlq	$4, %rcx
	movq	-8(%rax,%rcx), %rax
	movslq	%r13d, %rbp
	movsd	-8(%rax,%rbp,8), %xmm0  # xmm0 = mem[0],zero
	movl	$.L.str.4, %edi
	movb	$1, %al
	callq	printf
	movq	%r14, %rdi
	callq	fclose
	movl	$.Lstr, %edi
	callq	puts
	imull	%ebx, %ebp
	movslq	%ebp, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	read_dmatrix, .Lfunc_end0-read_dmatrix
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"rt"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Opened file %s for matrix reading\n"
	.size	.L.str.1, 35

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%d %d"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%lf"
	.size	.L.str.3, 4

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%g\n"
	.size	.L.str.4, 4

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"File read and closed"
	.size	.Lstr, 21


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
