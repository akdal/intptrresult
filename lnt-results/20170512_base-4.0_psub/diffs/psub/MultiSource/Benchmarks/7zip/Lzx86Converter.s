	.text
	.file	"Lzx86Converter.bc"
	.globl	_ZN9NCompress4NLzx20Cx86ConvertOutStream15MakeTranslationEv
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx20Cx86ConvertOutStream15MakeTranslationEv,@function
_ZN9NCompress4NLzx20Cx86ConvertOutStream15MakeTranslationEv: # @_ZN9NCompress4NLzx20Cx86ConvertOutStream15MakeTranslationEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
.Lcfi2:
	.cfi_offset %rbx, -24
.Lcfi3:
	.cfi_offset %rbp, -16
	movl	28(%rdi), %r8d
	cmpl	$11, %r8d
	jb	.LBB0_9
# BB#1:
	addl	$-10, %r8d
	je	.LBB0_9
# BB#2:                                 # %.lr.ph
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	leal	1(%rcx), %eax
	movl	%ecx, %edx
	cmpb	$-24, 37(%rdi,%rdx)
	jne	.LBB0_8
# BB#4:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, %r9d
	movzbl	37(%rdi,%r9), %eax
	leal	2(%rcx), %edx
	movq	%rdx, %r10
	movzbl	37(%rdi,%rdx), %edx
	shll	$8, %edx
	orl	%eax, %edx
	leal	3(%rcx), %eax
	movzbl	37(%rdi,%rax), %ebp
	shll	$16, %ebp
	orl	%edx, %ebp
	leal	4(%rcx), %edx
	movzbl	37(%rdi,%rdx), %esi
	shll	$24, %esi
	orl	%ebp, %esi
	movl	24(%rdi), %ebp
	addl	%ecx, %ebp
	negl	%ebp
	cmpl	%ebp, %esi
	jl	.LBB0_7
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	32(%rdi), %ebx
	cmpl	%ebx, %esi
	jge	.LBB0_7
# BB#6:                                 # %.loopexit.loopexit48
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%esi, %esi
	cmovnsl	%ebp, %ebx
	addl	%esi, %ebx
	movb	%bl, 37(%rdi,%r9)
	movq	%r10, %rsi
	movb	%bh, 37(%rdi,%rsi)  # NOREX
	movl	%ebx, %esi
	shrl	$16, %esi
	movb	%sil, 37(%rdi,%rax)
	shrl	$24, %ebx
	movb	%bl, 37(%rdi,%rdx)
.LBB0_7:                                # %.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	addl	$5, %ecx
	movl	%ecx, %eax
.LBB0_8:                                # %.backedge
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	%r8d, %eax
	movl	%eax, %ecx
	jb	.LBB0_3
.LBB0_9:                                # %.loopexit42
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN9NCompress4NLzx20Cx86ConvertOutStream15MakeTranslationEv, .Lfunc_end0-_ZN9NCompress4NLzx20Cx86ConvertOutStream15MakeTranslationEv
	.cfi_endproc

	.globl	_ZN9NCompress4NLzx20Cx86ConvertOutStream5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx20Cx86ConvertOutStream5WriteEPKvjPj,@function
_ZN9NCompress4NLzx20Cx86ConvertOutStream5WriteEPKvjPj: # @_ZN9NCompress4NLzx20Cx86ConvertOutStream5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi7:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 64
.Lcfi11:
	.cfi_offset %rbx, -56
.Lcfi12:
	.cfi_offset %r12, -48
.Lcfi13:
	.cfi_offset %r13, -40
.Lcfi14:
	.cfi_offset %r14, -32
.Lcfi15:
	.cfi_offset %r15, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movq	%rsi, %r12
	movq	%rdi, %rbx
	testq	%rcx, %rcx
	je	.LBB1_2
# BB#1:
	movl	$0, (%rcx)
.LBB1_2:
	cmpb	$0, 36(%rbx)
	je	.LBB1_12
# BB#3:                                 # %.thread.preheader
	movq	%rcx, (%rsp)            # 8-byte Spill
	testl	%r13d, %r13d
	movl	$0, %ebp
	je	.LBB1_7
# BB#4:                                 # %.lr.ph
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_5:                                # =>This Inner Loop Header: Depth=1
	movl	%r13d, %eax
	subl	%r15d, %eax
	movl	28(%rbx), %r14d
	movl	$32768, %ebp            # imm = 0x8000
	subl	%r14d, %ebp
	cmpl	%ebp, %eax
	cmovbl	%eax, %ebp
	leaq	37(%rbx,%r14), %rdi
	movl	%r15d, %esi
	addq	%r12, %rsi
	movq	%rbp, %rdx
	callq	memmove
	leal	(%rbp,%r14), %eax
	movl	%eax, 28(%rbx)
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB1_6
# BB#10:                                #   in Loop: Header=BB1_5 Depth=1
	movq	%rbx, %rdi
	callq	_ZN9NCompress4NLzx20Cx86ConvertOutStream5FlushEv
	testl	%eax, %eax
	jne	.LBB1_11
.LBB1_6:                                # %.thread.backedge
                                        #   in Loop: Header=BB1_5 Depth=1
	addl	%r15d, %ebp
	cmpl	%r13d, %ebp
	movl	%ebp, %r15d
	jb	.LBB1_5
.LBB1_7:                                # %.thread._crit_edge
	movq	(%rsp), %rax            # 8-byte Reload
	testq	%rax, %rax
	je	.LBB1_9
# BB#8:
	movl	%ebp, (%rax)
.LBB1_9:                                # %.loopexit
	xorl	%eax, %eax
.LBB1_11:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_12:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movq	%r12, %rsi
	movl	%r13d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end1:
	.size	_ZN9NCompress4NLzx20Cx86ConvertOutStream5WriteEPKvjPj, .Lfunc_end1-_ZN9NCompress4NLzx20Cx86ConvertOutStream5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN9NCompress4NLzx20Cx86ConvertOutStream5FlushEv
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx20Cx86ConvertOutStream5FlushEv,@function
_ZN9NCompress4NLzx20Cx86ConvertOutStream5FlushEv: # @_ZN9NCompress4NLzx20Cx86ConvertOutStream5FlushEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 48
.Lcfi21:
	.cfi_offset %rbx, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	28(%rbx), %edx
	testl	%edx, %edx
	je	.LBB2_17
# BB#1:
	cmpl	$11, %edx
	jb	.LBB2_11
# BB#2:
	movb	36(%rbx), %al
	testb	%al, %al
	je	.LBB2_11
# BB#3:
	movl	%edx, %r8d
	addl	$-10, %r8d
	je	.LBB2_11
# BB#4:                                 # %.lr.ph.i
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	leal	1(%rcx), %eax
	movl	%ecx, %esi
	cmpb	$-24, 37(%rbx,%rsi)
	jne	.LBB2_10
# BB#6:                                 # %.preheader.preheader.i
                                        #   in Loop: Header=BB2_5 Depth=1
	movl	%eax, %r9d
	movzbl	37(%rbx,%r9), %eax
	leal	2(%rcx), %esi
	movq	%rsi, %r11
	movzbl	37(%rbx,%rsi), %esi
	shll	$8, %esi
	orl	%eax, %esi
	leal	3(%rcx), %r10d
	movzbl	37(%rbx,%r10), %eax
	shll	$16, %eax
	orl	%esi, %eax
	leal	4(%rcx), %esi
	movzbl	37(%rbx,%rsi), %ebp
	shll	$24, %ebp
	orl	%eax, %ebp
	movl	24(%rbx), %edi
	addl	%ecx, %edi
	negl	%edi
	cmpl	%edi, %ebp
	jl	.LBB2_9
# BB#7:                                 #   in Loop: Header=BB2_5 Depth=1
	movl	32(%rbx), %eax
	cmpl	%eax, %ebp
	jge	.LBB2_9
# BB#8:                                 # %.loopexit.loopexit48.i
                                        #   in Loop: Header=BB2_5 Depth=1
	testl	%ebp, %ebp
	cmovnsl	%edi, %eax
	addl	%ebp, %eax
	movb	%al, 37(%rbx,%r9)
	movq	%r11, %rdi
	movb	%ah, 37(%rbx,%rdi)  # NOREX
	movl	%eax, %edi
	shrl	$16, %edi
	movb	%dil, 37(%rbx,%r10)
	shrl	$24, %eax
	movb	%al, 37(%rbx,%rsi)
.LBB2_9:                                # %.loopexit.i
                                        #   in Loop: Header=BB2_5 Depth=1
	addl	$5, %ecx
	movl	%ecx, %eax
.LBB2_10:                               # %.backedge.i
                                        #   in Loop: Header=BB2_5 Depth=1
	cmpl	%r8d, %eax
	movl	%eax, %ecx
	jb	.LBB2_5
.LBB2_11:                               # %_ZN9NCompress4NLzx20Cx86ConvertOutStream15MakeTranslationEv.exit.preheader
	xorl	%ebp, %ebp
	leaq	12(%rsp), %r14
	.p2align	4, 0x90
.LBB2_12:                               # %_ZN9NCompress4NLzx20Cx86ConvertOutStream15MakeTranslationEv.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movl	%ebp, %ecx
	leaq	37(%rbx,%rcx), %rsi
	subl	%ebp, %edx
	movq	%r14, %rcx
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB2_18
# BB#13:                                #   in Loop: Header=BB2_12 Depth=1
	movl	12(%rsp), %eax
	testl	%eax, %eax
	je	.LBB2_14
# BB#15:                                # %.thread
                                        #   in Loop: Header=BB2_12 Depth=1
	addl	%eax, %ebp
	movl	28(%rbx), %edx
	cmpl	%edx, %ebp
	jb	.LBB2_12
# BB#16:
	addl	24(%rbx), %edx
	movl	%edx, 24(%rbx)
	movl	$0, 28(%rbx)
	cmpl	$1073741824, %edx       # imm = 0x40000000
	sbbb	%al, %al
	cmpb	$0, 36(%rbx)
	setne	%cl
	andb	%al, %cl
	movb	%cl, 36(%rbx)
.LBB2_17:                               # %.thread21
	xorl	%eax, %eax
	jmp	.LBB2_18
.LBB2_14:
	movl	$-2147467259, %eax      # imm = 0x80004005
.LBB2_18:                               # %.thread21
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN9NCompress4NLzx20Cx86ConvertOutStream5FlushEv, .Lfunc_end2-_ZN9NCompress4NLzx20Cx86ConvertOutStream5FlushEv
	.cfi_endproc

	.section	.text._ZN9NCompress4NLzx20Cx86ConvertOutStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress4NLzx20Cx86ConvertOutStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress4NLzx20Cx86ConvertOutStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx20Cx86ConvertOutStream14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress4NLzx20Cx86ConvertOutStream14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress4NLzx20Cx86ConvertOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB3_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB3_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB3_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB3_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB3_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB3_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB3_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB3_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB3_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB3_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB3_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB3_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB3_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB3_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB3_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB3_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB3_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end3:
	.size	_ZN9NCompress4NLzx20Cx86ConvertOutStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end3-_ZN9NCompress4NLzx20Cx86ConvertOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress4NLzx20Cx86ConvertOutStream6AddRefEv,"axG",@progbits,_ZN9NCompress4NLzx20Cx86ConvertOutStream6AddRefEv,comdat
	.weak	_ZN9NCompress4NLzx20Cx86ConvertOutStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx20Cx86ConvertOutStream6AddRefEv,@function
_ZN9NCompress4NLzx20Cx86ConvertOutStream6AddRefEv: # @_ZN9NCompress4NLzx20Cx86ConvertOutStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end4:
	.size	_ZN9NCompress4NLzx20Cx86ConvertOutStream6AddRefEv, .Lfunc_end4-_ZN9NCompress4NLzx20Cx86ConvertOutStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress4NLzx20Cx86ConvertOutStream7ReleaseEv,"axG",@progbits,_ZN9NCompress4NLzx20Cx86ConvertOutStream7ReleaseEv,comdat
	.weak	_ZN9NCompress4NLzx20Cx86ConvertOutStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx20Cx86ConvertOutStream7ReleaseEv,@function
_ZN9NCompress4NLzx20Cx86ConvertOutStream7ReleaseEv: # @_ZN9NCompress4NLzx20Cx86ConvertOutStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB5_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB5_2:
	popq	%rcx
	retq
.Lfunc_end5:
	.size	_ZN9NCompress4NLzx20Cx86ConvertOutStream7ReleaseEv, .Lfunc_end5-_ZN9NCompress4NLzx20Cx86ConvertOutStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress4NLzx20Cx86ConvertOutStreamD2Ev,"axG",@progbits,_ZN9NCompress4NLzx20Cx86ConvertOutStreamD2Ev,comdat
	.weak	_ZN9NCompress4NLzx20Cx86ConvertOutStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx20Cx86ConvertOutStreamD2Ev,@function
_ZN9NCompress4NLzx20Cx86ConvertOutStreamD2Ev: # @_ZN9NCompress4NLzx20Cx86ConvertOutStreamD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTVN9NCompress4NLzx20Cx86ConvertOutStreamE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB6_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB6_1:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	retq
.Lfunc_end6:
	.size	_ZN9NCompress4NLzx20Cx86ConvertOutStreamD2Ev, .Lfunc_end6-_ZN9NCompress4NLzx20Cx86ConvertOutStreamD2Ev
	.cfi_endproc

	.section	.text._ZN9NCompress4NLzx20Cx86ConvertOutStreamD0Ev,"axG",@progbits,_ZN9NCompress4NLzx20Cx86ConvertOutStreamD0Ev,comdat
	.weak	_ZN9NCompress4NLzx20Cx86ConvertOutStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress4NLzx20Cx86ConvertOutStreamD0Ev,@function
_ZN9NCompress4NLzx20Cx86ConvertOutStreamD0Ev: # @_ZN9NCompress4NLzx20Cx86ConvertOutStreamD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN9NCompress4NLzx20Cx86ConvertOutStreamE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp0:
	callq	*16(%rax)
.Ltmp1:
.LBB7_2:                                # %_ZN9NCompress4NLzx20Cx86ConvertOutStreamD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB7_3:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN9NCompress4NLzx20Cx86ConvertOutStreamD0Ev, .Lfunc_end7-_ZN9NCompress4NLzx20Cx86ConvertOutStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end7-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTVN9NCompress4NLzx20Cx86ConvertOutStreamE,@object # @_ZTVN9NCompress4NLzx20Cx86ConvertOutStreamE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress4NLzx20Cx86ConvertOutStreamE
	.p2align	3
_ZTVN9NCompress4NLzx20Cx86ConvertOutStreamE:
	.quad	0
	.quad	_ZTIN9NCompress4NLzx20Cx86ConvertOutStreamE
	.quad	_ZN9NCompress4NLzx20Cx86ConvertOutStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress4NLzx20Cx86ConvertOutStream6AddRefEv
	.quad	_ZN9NCompress4NLzx20Cx86ConvertOutStream7ReleaseEv
	.quad	_ZN9NCompress4NLzx20Cx86ConvertOutStreamD2Ev
	.quad	_ZN9NCompress4NLzx20Cx86ConvertOutStreamD0Ev
	.quad	_ZN9NCompress4NLzx20Cx86ConvertOutStream5WriteEPKvjPj
	.size	_ZTVN9NCompress4NLzx20Cx86ConvertOutStreamE, 64

	.type	_ZTSN9NCompress4NLzx20Cx86ConvertOutStreamE,@object # @_ZTSN9NCompress4NLzx20Cx86ConvertOutStreamE
	.globl	_ZTSN9NCompress4NLzx20Cx86ConvertOutStreamE
	.p2align	4
_ZTSN9NCompress4NLzx20Cx86ConvertOutStreamE:
	.asciz	"N9NCompress4NLzx20Cx86ConvertOutStreamE"
	.size	_ZTSN9NCompress4NLzx20Cx86ConvertOutStreamE, 40

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress4NLzx20Cx86ConvertOutStreamE,@object # @_ZTIN9NCompress4NLzx20Cx86ConvertOutStreamE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress4NLzx20Cx86ConvertOutStreamE
	.p2align	4
_ZTIN9NCompress4NLzx20Cx86ConvertOutStreamE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress4NLzx20Cx86ConvertOutStreamE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI20ISequentialOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN9NCompress4NLzx20Cx86ConvertOutStreamE, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
