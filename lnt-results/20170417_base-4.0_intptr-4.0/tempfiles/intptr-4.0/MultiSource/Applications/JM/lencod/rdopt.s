	.text
	.file	"rdopt.bc"
	.globl	clear_rdopt
	.p2align	4, 0x90
	.type	clear_rdopt,@function
clear_rdopt:                            # @clear_rdopt
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	cofDC(%rip), %rdi
	callq	free_mem_DCcoeff
	movq	cofAC(%rip), %rdi
	callq	free_mem_ACcoeff
	movq	cofAC8x8(%rip), %rdi
	callq	free_mem_ACcoeff
	movq	cofAC4x4intern(%rip), %rdi
	callq	free_mem_ACcoeff
	movq	input(%rip), %rax
	cmpl	$0, 5100(%rax)
	je	.LBB0_2
# BB#1:
	movq	cofAC_8x8ts(%rip), %rdi
	callq	free_mem_ACcoeff
	movq	input(%rip), %rax
.LBB0_2:
	cmpl	$0, 5652(%rax)
	je	.LBB0_4
# BB#3:
	movq	bestInterFAdjust4x4(%rip), %rdi
	callq	free_mem2Dint
	movq	bestIntraFAdjust4x4(%rip), %rdi
	callq	free_mem2Dint
	movq	bestInterFAdjust8x8(%rip), %rdi
	callq	free_mem2Dint
	movq	bestIntraFAdjust8x8(%rip), %rdi
	callq	free_mem2Dint
	movq	bestInterFAdjust4x4Cr(%rip), %rdi
	movl	$2, %esi
	callq	free_mem3Dint
	movq	bestIntraFAdjust4x4Cr(%rip), %rdi
	movl	$2, %esi
	callq	free_mem3Dint
	movq	fadjust8x8(%rip), %rdi
	callq	free_mem2Dint
	movq	fadjust4x4(%rip), %rdi
	callq	free_mem2Dint
	movq	fadjust4x4Cr(%rip), %rdi
	movl	$2, %esi
	callq	free_mem3Dint
	movq	fadjust8x8Cr(%rip), %rdi
	movl	$2, %esi
	callq	free_mem3Dint
.LBB0_4:
	movq	cs_mb(%rip), %rdi
	callq	delete_coding_state
	movq	cs_b8(%rip), %rdi
	callq	delete_coding_state
	movq	cs_cm(%rip), %rdi
	callq	delete_coding_state
	movq	cs_imb(%rip), %rdi
	callq	delete_coding_state
	movq	cs_ib8(%rip), %rdi
	callq	delete_coding_state
	movq	cs_ib4(%rip), %rdi
	callq	delete_coding_state
	movq	cs_pc(%rip), %rdi
	popq	%rax
	jmp	delete_coding_state     # TAILCALL
.Lfunc_end0:
	.size	clear_rdopt, .Lfunc_end0-clear_rdopt
	.cfi_endproc

	.globl	init_rdopt
	.p2align	4, 0x90
	.type	init_rdopt,@function
init_rdopt:                             # @init_rdopt
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	movq	$0, rdopt(%rip)
	movl	$cofDC, %edi
	callq	get_mem_DCcoeff
	movl	$cofAC, %edi
	callq	get_mem_ACcoeff
	movl	$cofAC8x8, %edi
	callq	get_mem_ACcoeff
	movl	$cofAC4x4intern, %edi
	callq	get_mem_ACcoeff
	movq	cofAC4x4intern(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, cofAC4x4(%rip)
	movq	input(%rip), %rax
	cmpl	$0, 5100(%rax)
	je	.LBB1_2
# BB#1:
	movl	$cofAC_8x8ts, %edi
	callq	get_mem_ACcoeff
	movq	input(%rip), %rax
.LBB1_2:
	movslq	4168(%rax), %rcx
	cmpq	$3, %rcx
	ja	.LBB1_3
# BB#4:                                 # %switch.lookup
	movq	.Lswitch.table(,%rcx,8), %rcx
	jmp	.LBB1_5
.LBB1_3:
	movl	$encode_one_macroblock_high, %ecx
.LBB1_5:
	movq	%rcx, encode_one_macroblock(%rip)
	cmpl	$0, 5652(%rax)
	je	.LBB1_7
# BB#6:
	movl	$bestInterFAdjust4x4, %edi
	movl	$16, %esi
	movl	$16, %edx
	callq	get_mem2Dint
	movl	$bestIntraFAdjust4x4, %edi
	movl	$16, %esi
	movl	$16, %edx
	callq	get_mem2Dint
	movl	$bestInterFAdjust8x8, %edi
	movl	$16, %esi
	movl	$16, %edx
	callq	get_mem2Dint
	movl	$bestIntraFAdjust8x8, %edi
	movl	$16, %esi
	movl	$16, %edx
	callq	get_mem2Dint
	movq	img(%rip), %rax
	movl	15544(%rax), %ecx
	movl	15548(%rax), %edx
	movl	$bestInterFAdjust4x4Cr, %edi
	movl	$2, %esi
	callq	get_mem3Dint
	movq	img(%rip), %rax
	movl	15544(%rax), %ecx
	movl	15548(%rax), %edx
	movl	$bestIntraFAdjust4x4Cr, %edi
	movl	$2, %esi
	callq	get_mem3Dint
	movl	$fadjust8x8, %edi
	movl	$16, %esi
	movl	$16, %edx
	callq	get_mem2Dint
	movl	$fadjust4x4, %edi
	movl	$16, %esi
	movl	$16, %edx
	callq	get_mem2Dint
	movq	img(%rip), %rax
	movl	15544(%rax), %ecx
	movl	15548(%rax), %edx
	movl	$fadjust4x4Cr, %edi
	movl	$2, %esi
	callq	get_mem3Dint
	movq	img(%rip), %rax
	movl	15544(%rax), %ecx
	movl	15548(%rax), %edx
	movl	$fadjust8x8Cr, %edi
	movl	$2, %esi
	callq	get_mem3Dint
.LBB1_7:
	callq	create_coding_state
	movq	%rax, cs_mb(%rip)
	callq	create_coding_state
	movq	%rax, cs_b8(%rip)
	callq	create_coding_state
	movq	%rax, cs_cm(%rip)
	callq	create_coding_state
	movq	%rax, cs_imb(%rip)
	callq	create_coding_state
	movq	%rax, cs_ib8(%rip)
	callq	create_coding_state
	movq	%rax, cs_ib4(%rip)
	callq	create_coding_state
	movq	%rax, cs_pc(%rip)
	movq	input(%rip), %rax
	cmpl	$1, 4172(%rax)
	jne	.LBB1_9
# BB#8:
	movabsq	$4647714815446351872, %rax # imm = 0x4080000000000000
	movq	%rax, mb16x16_cost(%rip)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, lambda_mf_factor(%rip)
.LBB1_9:
	popq	%rax
	retq
.Lfunc_end1:
	.size	init_rdopt, .Lfunc_end1-init_rdopt
	.cfi_endproc

	.globl	UpdatePixelMap
	.p2align	4, 0x90
	.type	UpdatePixelMap,@function
UpdatePixelMap:                         # @UpdatePixelMap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movl	68(%rax), %ecx
	cmpl	$2, 20(%rax)
	jne	.LBB2_1
# BB#8:                                 # %.preheader50
	testl	%ecx, %ecx
	jle	.LBB2_18
# BB#9:                                 # %.preheader.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_10:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_12 Depth 2
	cmpl	$0, 52(%rax)
	jle	.LBB2_13
# BB#11:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_10 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_12:                               # %.lr.ph
                                        #   Parent Loop BB2_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	pixel_map(%rip), %rax
	movq	(%rax,%rcx,8), %rax
	movb	$1, (%rax,%rdx)
	incq	%rdx
	movq	img(%rip), %rax
	movslq	52(%rax), %rsi
	cmpq	%rsi, %rdx
	jl	.LBB2_12
.LBB2_13:                               # %._crit_edge
                                        #   in Loop: Header=BB2_10 Depth=1
	incq	%rcx
	movslq	68(%rax), %rdx
	cmpq	%rdx, %rcx
	jl	.LBB2_10
	jmp	.LBB2_18
.LBB2_1:                                # %.preheader58
	sarl	$3, %ecx
	testl	%ecx, %ecx
	jle	.LBB2_18
# BB#2:                                 # %.preheader57.preheader
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader57
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_5 Depth 2
                                        #       Child Loop BB2_6 Depth 3
                                        #       Child Loop BB2_14 Depth 3
	movl	52(%rax), %ecx
	sarl	$3, %ecx
	testl	%ecx, %ecx
	jle	.LBB2_17
# BB#4:                                 # %.lr.ph67
                                        #   in Loop: Header=BB2_3 Depth=1
	movslq	%r8d, %r10
	leal	8(,%r9,8), %eax
	movslq	%eax, %r12
	xorl	%r14d, %r14d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB2_5:                                #   Parent Loop BB2_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_6 Depth 3
                                        #       Child Loop BB2_14 Depth 3
	movslq	%r11d, %rcx
	leal	8(,%r14,8), %esi
	movq	refresh_map(%rip), %rax
	movq	(%rax,%r9,8), %rdi
	movq	%rcx, %rax
	orq	$1, %rax
	movq	%rcx, %rdx
	orq	$3, %rdx
	movq	%rcx, %r15
	orq	$7, %r15
	cmpb	$0, (%rdi,%r14)
	movslq	%esi, %r13
	movq	%r10, %rsi
	movq	%r10, %rbx
	je	.LBB2_14
	.p2align	4, 0x90
.LBB2_6:                                # %.preheader52
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r13, %rax
	movq	pixel_map(%rip), %rbx
	movq	(%rbx,%rsi,8), %rbx
	movb	$1, (%rbx,%rcx)
	jge	.LBB2_7
# BB#19:                                #   in Loop: Header=BB2_6 Depth=3
	movq	pixel_map(%rip), %rdi
	movq	(%rdi,%rsi,8), %rdi
	movb	$1, (%rdi,%rax)
	movq	pixel_map(%rip), %rdi
	movq	(%rdi,%rsi,8), %rdi
	movb	$1, 1(%rdi,%rax)
	movq	pixel_map(%rip), %rdi
	movq	(%rdi,%rsi,8), %rdi
	movb	$1, (%rdi,%rdx)
	movq	pixel_map(%rip), %rdi
	movq	(%rdi,%rsi,8), %rdi
	movb	$1, 1(%rdi,%rdx)
	movq	pixel_map(%rip), %rdi
	movq	(%rdi,%rsi,8), %rdi
	movb	$1, 2(%rdi,%rdx)
	movq	pixel_map(%rip), %rdi
	movq	(%rdi,%rsi,8), %rdi
	movb	$1, 3(%rdi,%rdx)
	movq	pixel_map(%rip), %rdi
	movq	(%rdi,%rsi,8), %rdi
	movb	$1, (%rdi,%r15)
.LBB2_7:                                #   in Loop: Header=BB2_6 Depth=3
	incq	%rsi
	cmpq	%r12, %rsi
	jl	.LBB2_6
	jmp	.LBB2_16
	.p2align	4, 0x90
.LBB2_14:                               # %.preheader51
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	pixel_map(%rip), %rsi
	movq	(%rsi,%rbx,8), %rsi
	movzbl	(%rsi,%rcx), %ebp
	incl	%ebp
	movq	input(%rip), %rdi
	movl	32(%rdi), %edi
	incl	%edi
	cmpl	%edi, %ebp
	cmovll	%ebp, %edi
	cmpq	%r13, %rax
	movb	%dil, (%rsi,%rcx)
	jge	.LBB2_15
# BB#20:                                #   in Loop: Header=BB2_14 Depth=3
	movq	pixel_map(%rip), %rsi
	movq	(%rsi,%rbx,8), %rsi
	movzbl	(%rsi,%rax), %edi
	incl	%edi
	movq	input(%rip), %rbp
	movl	32(%rbp), %ebp
	incl	%ebp
	cmpl	%ebp, %edi
	cmovll	%edi, %ebp
	movb	%bpl, (%rsi,%rax)
	movq	pixel_map(%rip), %rsi
	movq	(%rsi,%rbx,8), %rsi
	movzbl	1(%rsi,%rax), %edi
	incl	%edi
	movq	input(%rip), %rbp
	movl	32(%rbp), %ebp
	incl	%ebp
	cmpl	%ebp, %edi
	cmovll	%edi, %ebp
	movb	%bpl, 1(%rsi,%rax)
	movq	pixel_map(%rip), %rsi
	movq	(%rsi,%rbx,8), %rsi
	movzbl	(%rsi,%rdx), %edi
	incl	%edi
	movq	input(%rip), %rbp
	movl	32(%rbp), %ebp
	incl	%ebp
	cmpl	%ebp, %edi
	cmovll	%edi, %ebp
	movb	%bpl, (%rsi,%rdx)
	movq	pixel_map(%rip), %rsi
	movq	(%rsi,%rbx,8), %rsi
	movzbl	1(%rsi,%rdx), %edi
	incl	%edi
	movq	input(%rip), %rbp
	movl	32(%rbp), %ebp
	incl	%ebp
	cmpl	%ebp, %edi
	cmovll	%edi, %ebp
	movb	%bpl, 1(%rsi,%rdx)
	movq	pixel_map(%rip), %rsi
	movq	(%rsi,%rbx,8), %rsi
	movzbl	2(%rsi,%rdx), %edi
	incl	%edi
	movq	input(%rip), %rbp
	movl	32(%rbp), %ebp
	incl	%ebp
	cmpl	%ebp, %edi
	cmovll	%edi, %ebp
	movb	%bpl, 2(%rsi,%rdx)
	movq	pixel_map(%rip), %rsi
	movq	(%rsi,%rbx,8), %rsi
	movzbl	3(%rsi,%rdx), %edi
	incl	%edi
	movq	input(%rip), %rbp
	movl	32(%rbp), %ebp
	incl	%ebp
	cmpl	%ebp, %edi
	cmovll	%edi, %ebp
	movb	%bpl, 3(%rsi,%rdx)
	movq	pixel_map(%rip), %rsi
	movq	(%rsi,%rbx,8), %rsi
	movzbl	(%rsi,%r15), %edi
	incl	%edi
	movq	input(%rip), %rbp
	movl	32(%rbp), %ebp
	incl	%ebp
	cmpl	%ebp, %edi
	cmovll	%edi, %ebp
	movb	%bpl, (%rsi,%r15)
.LBB2_15:                               #   in Loop: Header=BB2_14 Depth=3
	incq	%rbx
	cmpq	%r12, %rbx
	jl	.LBB2_14
.LBB2_16:                               # %.loopexit54
                                        #   in Loop: Header=BB2_5 Depth=2
	incq	%r14
	movq	img(%rip), %rax
	movl	52(%rax), %ecx
	sarl	$3, %ecx
	movslq	%ecx, %rcx
	addl	$8, %r11d
	cmpq	%rcx, %r14
	jl	.LBB2_5
.LBB2_17:                               # %._crit_edge68
                                        #   in Loop: Header=BB2_3 Depth=1
	incq	%r9
	movl	68(%rax), %ecx
	sarl	$3, %ecx
	movslq	%ecx, %rcx
	addl	$8, %r8d
	cmpq	%rcx, %r9
	jl	.LBB2_3
.LBB2_18:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	UpdatePixelMap, .Lfunc_end2-UpdatePixelMap
	.cfi_endproc

	.globl	CheckReliabilityOfRef
	.p2align	4, 0x90
	.type	CheckReliabilityOfRef,@function
CheckReliabilityOfRef:                  # @CheckReliabilityOfRef
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$32, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 88
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	img(%rip), %rbp
	movl	52(%rbp), %r8d
	movl	68(%rbp), %r9d
	cmpl	$4, %ecx
	jl	.LBB3_2
# BB#1:                                 # %.thread
	movl	%edi, %ebx
	andl	$-2, %ebx
	leal	2(%rbx), %r10d
	addl	%edi, %edi
	andl	$2, %edi
	movl	%edi, %r11d
	jmp	.LBB3_3
.LBB3_2:
	leal	(%rdi,%rdi), %eax
	xorl	%r11d, %r11d
	cmpl	$2, %ecx
	movl	$0, %ebx
	cmovel	%eax, %ebx
	leal	2(%rdi,%rdi), %edi
	movl	$4, %r10d
	cmovel	%edi, %r10d
	cmpl	$3, %ecx
	cmovel	%eax, %r11d
	movl	$1, %eax
	cmpl	%r10d, %ebx
	jge	.LBB3_108
.LBB3_3:                                # %.preheader253.lr.ph
	decl	%r8d
	decl	%r9d
	xorl	%eax, %eax
	cmpl	$3, %ecx
	setl	%al
	leal	2(%r11,%rax,2), %eax
	movq	14384(%rbp), %rdi
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	movslq	%esi, %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movslq	%edx, %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movslq	%ecx, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	pixel_map(%rip), %rcx
	movq	%rcx, -120(%rsp)        # 8-byte Spill
	movslq	%r11d, %rcx
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	movl	168(%rbp), %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movl	172(%rbp), %ecx
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	cltq
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movslq	%ebx, %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	movslq	%r10d, %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
.LBB3_4:                                # %.preheader253
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_5 Depth 2
                                        #       Child Loop BB3_91 Depth 3
                                        #         Child Loop BB3_92 Depth 4
                                        #       Child Loop BB3_65 Depth 3
                                        #         Child Loop BB3_66 Depth 4
                                        #       Child Loop BB3_80 Depth 3
                                        #         Child Loop BB3_81 Depth 4
                                        #           Child Loop BB3_82 Depth 5
                                        #       Child Loop BB3_26 Depth 3
                                        #         Child Loop BB3_27 Depth 4
                                        #           Child Loop BB3_28 Depth 5
                                        #       Child Loop BB3_38 Depth 3
                                        #       Child Loop BB3_14 Depth 3
                                        #         Child Loop BB3_15 Depth 4
                                        #       Child Loop BB3_7 Depth 3
	movq	-8(%rsp), %rax          # 8-byte Reload
	movq	-72(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %r11
	movq	-24(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rcx), %ebx
	shll	$4, %ebx
	movq	-16(%rsp), %rbp         # 8-byte Reload
	movq	%r11, -40(%rsp)         # 8-byte Spill
	movl	%ebx, -60(%rsp)         # 4-byte Spill
	jmp	.LBB3_5
.LBB3_13:                               # %.preheader243.preheader
                                        #   in Loop: Header=BB3_5 Depth=2
	leal	3(%r10), %ecx
	leal	3(%r15), %r11d
.LBB3_14:                               # %.preheader243
                                        #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_15 Depth 4
	testl	%r15d, %r15d
	movl	$0, %eax
	cmovnsl	%r15d, %eax
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	cltq
	movq	-120(%rsp), %rsi        # 8-byte Reload
	movq	(%rsi,%rax,8), %rbx
	movl	%r10d, %ebp
	.p2align	4, 0x90
.LBB3_15:                               # %.preheader
                                        #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        #       Parent Loop BB3_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	xorl	%eax, %eax
	movl	%ebp, %esi
	addl	$-2, %esi
	cmovsl	%eax, %esi
	cmpl	%r8d, %esi
	cmovgl	%r8d, %esi
	movslq	%esi, %rsi
	movzbl	(%rbx,%rsi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#16:                                #   in Loop: Header=BB3_15 Depth=4
	xorl	%eax, %eax
	movl	%ebp, %esi
	decl	%esi
	cmovsl	%eax, %esi
	cmpl	%r8d, %esi
	cmovgl	%r8d, %esi
	movslq	%esi, %rsi
	movzbl	(%rbx,%rsi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#17:                                #   in Loop: Header=BB3_15 Depth=4
	xorl	%eax, %eax
	testl	%ebp, %ebp
	movl	$0, %esi
	cmovnsl	%ebp, %esi
	cmpl	%r8d, %esi
	cmovgl	%r8d, %esi
	movslq	%esi, %rsi
	movzbl	(%rbx,%rsi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#18:                                #   in Loop: Header=BB3_15 Depth=4
	xorl	%eax, %eax
	movl	%ebp, %edi
	incl	%edi
	movl	$0, %esi
	cmovnsl	%edi, %esi
	cmpl	%r8d, %esi
	cmovgl	%r8d, %esi
	movslq	%esi, %rsi
	movzbl	(%rbx,%rsi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#19:                                #   in Loop: Header=BB3_15 Depth=4
	xorl	%eax, %eax
	movl	%ebp, %esi
	addl	$2, %esi
	cmovsl	%eax, %esi
	cmpl	%r8d, %esi
	cmovgl	%r8d, %esi
	movslq	%esi, %rsi
	movzbl	(%rbx,%rsi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#20:                                #   in Loop: Header=BB3_15 Depth=4
	xorl	%eax, %eax
	addl	$3, %ebp
	cmovsl	%eax, %ebp
	cmpl	%r8d, %ebp
	cmovgl	%r8d, %ebp
	movslq	%ebp, %rsi
	movzbl	(%rbx,%rsi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#21:                                #   in Loop: Header=BB3_15 Depth=4
	leal	-1(%rdi), %eax
	cmpl	%ecx, %eax
	movl	%edi, %ebp
	jl	.LBB3_15
# BB#22:                                #   in Loop: Header=BB3_14 Depth=3
	cmpl	%r11d, %r15d
	leal	1(%r15), %eax
	movl	%eax, %r15d
	jl	.LBB3_14
	jmp	.LBB3_106
.LBB3_37:                               # %.preheader239.preheader
                                        #   in Loop: Header=BB3_5 Depth=2
	leal	3(%r15), %eax
	movl	%eax, -88(%rsp)         # 4-byte Spill
	testl	%r10d, %r10d
	movl	$0, %eax
	cmovnsl	%r10d, %eax
	cmpl	%r8d, %eax
	cmovgl	%r8d, %eax
	movslq	%eax, %r12
	movl	%r10d, %eax
	incl	%eax
	movl	$0, %ecx
	cmovsl	%ecx, %eax
	cmpl	%r8d, %eax
	cmovgl	%r8d, %eax
	movslq	%eax, %rbp
	movl	%r10d, %eax
	addl	$2, %eax
	cmovsl	%ecx, %eax
	cmpl	%r8d, %eax
	cmovgl	%r8d, %eax
	movslq	%eax, %rdi
	addl	$3, %r10d
	cmovsl	%ecx, %r10d
	cmpl	%r8d, %r10d
	cmovgl	%r8d, %r10d
	movslq	%r10d, %r11
	movq	%rbp, -112(%rsp)        # 8-byte Spill
	movq	%rdi, -96(%rsp)         # 8-byte Spill
	movq	%r11, -104(%rsp)        # 8-byte Spill
	movq	%r12, -80(%rsp)         # 8-byte Spill
.LBB3_38:                               #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r15d, %edi
	xorl	%eax, %eax
	movl	%edi, %ecx
	addl	$-2, %ecx
	cmovsl	%eax, %ecx
	cmpl	%r9d, %ecx
	cmovgl	%r9d, %ecx
	movslq	%ecx, %rcx
	movq	-120(%rsp), %r11        # 8-byte Reload
	movq	(%r11,%rcx,8), %rbx
	movl	%edi, %ecx
	decl	%ecx
	cmovsl	%eax, %ecx
	cmpl	%r9d, %ecx
	cmovgl	%r9d, %ecx
	testl	%edi, %edi
	movl	$0, %r10d
	cmovnsl	%edi, %r10d
	cmpl	%r9d, %r10d
	cmovgl	%r9d, %r10d
	incl	%r15d
	movl	$0, %ebp
	cmovnsl	%r15d, %ebp
	cmpl	%r9d, %ebp
	cmovgl	%r9d, %ebp
	movl	%edi, %esi
	addl	$2, %esi
	cmovsl	%eax, %esi
	cmpl	%r9d, %esi
	cmovgl	%r9d, %esi
	addl	$3, %edi
	cmovsl	%eax, %edi
	cmpl	%r9d, %edi
	cmovgl	%r9d, %edi
	movq	%rbx, %r13
	movzbl	(%rbx,%r12), %ebx
	cmpl	%edx, %ebx
	jle	.LBB3_108
# BB#39:                                #   in Loop: Header=BB3_38 Depth=3
	movslq	%ecx, %r14
	movq	(%r11,%r14,8), %rbx
	movzbl	(%rbx,%r12), %ebx
	cmpl	%edx, %ebx
	jle	.LBB3_108
# BB#40:                                #   in Loop: Header=BB3_38 Depth=3
	movslq	%r10d, %rcx
	movq	%rcx, -128(%rsp)        # 8-byte Spill
	movq	(%r11,%rcx,8), %rbx
	movzbl	(%rbx,%r12), %ebx
	cmpl	%edx, %ebx
	jle	.LBB3_108
# BB#41:                                #   in Loop: Header=BB3_38 Depth=3
	movslq	%ebp, %rcx
	movq	%rcx, %r10
	movq	(%r11,%rcx,8), %rbp
	movzbl	(%rbp,%r12), %ebp
	cmpl	%edx, %ebp
	jle	.LBB3_108
# BB#42:                                #   in Loop: Header=BB3_38 Depth=3
	movq	%r12, %rbp
	movslq	%esi, %r12
	movq	(%r11,%r12,8), %rsi
	movzbl	(%rsi,%rbp), %esi
	cmpl	%edx, %esi
	movq	%r11, %rbx
	jle	.LBB3_108
# BB#43:                                #   in Loop: Header=BB3_38 Depth=3
	movslq	%edi, %rcx
	movq	%rcx, %rdi
	movq	(%rbx,%rcx,8), %rsi
	movzbl	(%rsi,%rbp), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#44:                                #   in Loop: Header=BB3_38 Depth=3
	movq	-112(%rsp), %rbp        # 8-byte Reload
	movq	%r13, %r11
	movzbl	(%r11,%rbp), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#45:                                #   in Loop: Header=BB3_38 Depth=3
	movq	(%rbx,%r14,8), %rsi
	movzbl	(%rsi,%rbp), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#46:                                #   in Loop: Header=BB3_38 Depth=3
	movq	-128(%rsp), %rcx        # 8-byte Reload
	movq	(%rbx,%rcx,8), %rsi
	movzbl	(%rsi,%rbp), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#47:                                #   in Loop: Header=BB3_38 Depth=3
	movq	%r10, %r13
	movq	(%rbx,%r13,8), %rsi
	movzbl	(%rsi,%rbp), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#48:                                #   in Loop: Header=BB3_38 Depth=3
	movq	(%rbx,%r12,8), %rsi
	movzbl	(%rsi,%rbp), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#49:                                #   in Loop: Header=BB3_38 Depth=3
	movq	%rdi, %r10
	movq	(%rbx,%r10,8), %rsi
	movzbl	(%rsi,%rbp), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#50:                                #   in Loop: Header=BB3_38 Depth=3
	movq	-96(%rsp), %rdi         # 8-byte Reload
	movzbl	(%r11,%rdi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#51:                                #   in Loop: Header=BB3_38 Depth=3
	movq	(%rbx,%r14,8), %rsi
	movzbl	(%rsi,%rdi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#52:                                #   in Loop: Header=BB3_38 Depth=3
	movq	(%rbx,%rcx,8), %rsi
	movzbl	(%rsi,%rdi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#53:                                #   in Loop: Header=BB3_38 Depth=3
	movq	(%rbx,%r13,8), %rsi
	movzbl	(%rsi,%rdi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#54:                                #   in Loop: Header=BB3_38 Depth=3
	movq	(%rbx,%r12,8), %rsi
	movzbl	(%rsi,%rdi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#55:                                #   in Loop: Header=BB3_38 Depth=3
	movq	(%rbx,%r10,8), %rsi
	movzbl	(%rsi,%rdi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#56:                                #   in Loop: Header=BB3_38 Depth=3
	movq	%r11, %rcx
	movq	-104(%rsp), %r11        # 8-byte Reload
	movzbl	(%rcx,%r11), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#57:                                #   in Loop: Header=BB3_38 Depth=3
	movq	(%rbx,%r14,8), %rcx
	movzbl	(%rcx,%r11), %ecx
	cmpl	%edx, %ecx
	jle	.LBB3_108
# BB#58:                                #   in Loop: Header=BB3_38 Depth=3
	movq	-128(%rsp), %rcx        # 8-byte Reload
	movq	(%rbx,%rcx,8), %rcx
	movzbl	(%rcx,%r11), %ecx
	cmpl	%edx, %ecx
	jle	.LBB3_108
# BB#59:                                #   in Loop: Header=BB3_38 Depth=3
	movq	(%rbx,%r13,8), %rcx
	movzbl	(%rcx,%r11), %ecx
	cmpl	%edx, %ecx
	jle	.LBB3_108
# BB#60:                                #   in Loop: Header=BB3_38 Depth=3
	movq	(%rbx,%r12,8), %rcx
	movzbl	(%rcx,%r11), %ecx
	cmpl	%edx, %ecx
	jle	.LBB3_108
# BB#61:                                #   in Loop: Header=BB3_38 Depth=3
	movq	(%rbx,%r10,8), %rcx
	movzbl	(%rcx,%r11), %ecx
	cmpl	%edx, %ecx
	jle	.LBB3_108
# BB#62:                                #   in Loop: Header=BB3_38 Depth=3
	leal	-1(%r15), %eax
	cmpl	-88(%rsp), %eax         # 4-byte Folded Reload
	movq	-80(%rsp), %r12         # 8-byte Reload
	jl	.LBB3_38
	jmp	.LBB3_106
.LBB3_63:                               #   in Loop: Header=BB3_5 Depth=2
	leal	4(%r15), %esi
	movl	%esi, -104(%rsp)        # 4-byte Spill
	leal	4(%r10), %esi
	movl	%esi, -96(%rsp)         # 4-byte Spill
	cmpl	$2, %ecx
	jne	.LBB3_64
.LBB3_80:                               # %.preheader237
                                        #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_81 Depth 4
                                        #           Child Loop BB3_82 Depth 5
	movl	%r15d, %eax
	movl	%eax, %ecx
	addl	$-2, %ecx
	movl	$0, %edi
	cmovsl	%edi, %ecx
	cmpl	%r9d, %ecx
	cmovgl	%r9d, %ecx
	movslq	%ecx, %rcx
	movq	-120(%rsp), %rsi        # 8-byte Reload
	movq	(%rsi,%rcx,8), %rcx
	movq	%rcx, -128(%rsp)        # 8-byte Spill
	movl	%eax, %ecx
	decl	%ecx
	cmovsl	%edi, %ecx
	cmpl	%r9d, %ecx
	cmovgl	%r9d, %ecx
	movslq	%ecx, %rcx
	movq	%rcx, -112(%rsp)        # 8-byte Spill
	testl	%eax, %eax
	movl	$0, %ecx
	cmovnsl	%eax, %ecx
	cmpl	%r9d, %ecx
	cmovgl	%r9d, %ecx
	movslq	%ecx, %r12
	incl	%r15d
	movl	$0, %ecx
	cmovnsl	%r15d, %ecx
	cmpl	%r9d, %ecx
	cmovgl	%r9d, %ecx
	movslq	%ecx, %r13
	movl	%eax, %ecx
	addl	$2, %ecx
	cmovsl	%edi, %ecx
	cmpl	%r9d, %ecx
	cmovgl	%r9d, %ecx
	movslq	%ecx, %rcx
	addl	$3, %eax
	cmovsl	%edi, %eax
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	movslq	%eax, %rdi
	movl	%r10d, %r14d
.LBB3_81:                               # %.preheader229
                                        #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        #       Parent Loop BB3_80 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB3_82 Depth 5
	movl	$-2, %ebx
	.p2align	4, 0x90
.LBB3_82:                               #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        #       Parent Loop BB3_80 Depth=3
                                        #         Parent Loop BB3_81 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	xorl	%eax, %eax
	movl	%r14d, %ebp
	addl	%ebx, %ebp
	cmovsl	%eax, %ebp
	cmpl	%r8d, %ebp
	cmovgl	%r8d, %ebp
	movslq	%ebp, %r11
	movq	-128(%rsp), %rsi        # 8-byte Reload
	movzbl	(%rsi,%r11), %ebp
	cmpl	%edx, %ebp
	jle	.LBB3_108
# BB#83:                                #   in Loop: Header=BB3_82 Depth=5
	movq	-120(%rsp), %rsi        # 8-byte Reload
	movq	-112(%rsp), %rbp        # 8-byte Reload
	movq	(%rsi,%rbp,8), %rbp
	movzbl	(%rbp,%r11), %ebp
	cmpl	%edx, %ebp
	jle	.LBB3_108
# BB#84:                                #   in Loop: Header=BB3_82 Depth=5
	movq	(%rsi,%r12,8), %rbp
	movzbl	(%rbp,%r11), %ebp
	cmpl	%edx, %ebp
	jle	.LBB3_108
# BB#85:                                #   in Loop: Header=BB3_82 Depth=5
	movq	(%rsi,%r13,8), %rbp
	movzbl	(%rbp,%r11), %ebp
	cmpl	%edx, %ebp
	jle	.LBB3_108
# BB#86:                                #   in Loop: Header=BB3_82 Depth=5
	movq	(%rsi,%rcx,8), %rbp
	movzbl	(%rbp,%r11), %ebp
	cmpl	%edx, %ebp
	jle	.LBB3_108
# BB#87:                                #   in Loop: Header=BB3_82 Depth=5
	movq	(%rsi,%rdi,8), %rbp
	movzbl	(%rbp,%r11), %ebp
	cmpl	%edx, %ebp
	jle	.LBB3_108
# BB#88:                                #   in Loop: Header=BB3_82 Depth=5
	incl	%ebx
	cmpl	$4, %ebx
	jl	.LBB3_82
# BB#89:                                #   in Loop: Header=BB3_81 Depth=4
	incl	%r14d
	cmpl	-96(%rsp), %r14d        # 4-byte Folded Reload
	jl	.LBB3_81
# BB#90:                                #   in Loop: Header=BB3_80 Depth=3
	cmpl	-104(%rsp), %r15d       # 4-byte Folded Reload
	jl	.LBB3_80
	jmp	.LBB3_106
.LBB3_64:                               # %.preheader247
                                        #   in Loop: Header=BB3_5 Depth=2
	xorl	%esi, %esi
	cmpl	$1, %ecx
	setne	%sil
	cmpl	$1, %eax
	movl	%esi, -80(%rsp)         # 4-byte Spill
	jne	.LBB3_91
.LBB3_65:                               # %.preheader238.us
                                        #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_66 Depth 4
	movl	%r15d, %eax
	movl	%eax, %ecx
	addl	%esi, %ecx
	movl	$0, %edi
	cmovsl	%edi, %ecx
	cmpl	%r9d, %ecx
	cmovgl	%r9d, %ecx
	movslq	%ecx, %rcx
	movq	-120(%rsp), %rsi        # 8-byte Reload
	movq	(%rsi,%rcx,8), %rcx
	movl	%eax, %esi
	addl	$-2, %esi
	cmovsl	%edi, %esi
	cmpl	%r9d, %esi
	cmovgl	%r9d, %esi
	movslq	%esi, %rsi
	movq	%rsi, -128(%rsp)        # 8-byte Spill
	movl	%eax, %esi
	decl	%esi
	cmovsl	%edi, %esi
	cmpl	%r9d, %esi
	cmovgl	%r9d, %esi
	movslq	%esi, %rsi
	movq	%rsi, -112(%rsp)        # 8-byte Spill
	testl	%eax, %eax
	movl	$0, %esi
	cmovnsl	%eax, %esi
	cmpl	%r9d, %esi
	cmovgl	%r9d, %esi
	movslq	%esi, %rsi
	movq	%rsi, -88(%rsp)         # 8-byte Spill
	incl	%r15d
	movl	$0, %esi
	cmovnsl	%r15d, %esi
	cmpl	%r9d, %esi
	cmovgl	%r9d, %esi
	movslq	%esi, %r11
	movl	%eax, %esi
	addl	$2, %esi
	cmovsl	%edi, %esi
	cmpl	%r9d, %esi
	cmovgl	%r9d, %esi
	movslq	%esi, %r13
	addl	$3, %eax
	cmovsl	%edi, %eax
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	movslq	%eax, %r12
	movl	%r10d, %ebp
.LBB3_66:                               #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        #       Parent Loop BB3_65 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	xorl	%eax, %eax
	movl	%ebp, %edi
	addl	$-2, %edi
	cmovsl	%eax, %edi
	cmpl	%r8d, %edi
	cmovgl	%r8d, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	cmpl	%edx, %edi
	jle	.LBB3_108
# BB#67:                                #   in Loop: Header=BB3_66 Depth=4
	xorl	%eax, %eax
	movl	%ebp, %edi
	decl	%edi
	cmovsl	%eax, %edi
	cmpl	%r8d, %edi
	cmovgl	%r8d, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	cmpl	%edx, %edi
	jle	.LBB3_108
# BB#68:                                #   in Loop: Header=BB3_66 Depth=4
	xorl	%eax, %eax
	testl	%ebp, %ebp
	movl	$0, %edi
	cmovnsl	%ebp, %edi
	cmpl	%r8d, %edi
	cmovgl	%r8d, %edi
	movslq	%edi, %rbx
	movzbl	(%rcx,%rbx), %edi
	cmpl	%edx, %edi
	jle	.LBB3_108
# BB#69:                                #   in Loop: Header=BB3_66 Depth=4
	xorl	%eax, %eax
	movl	%ebp, %edi
	incl	%edi
	movl	$0, %esi
	cmovnsl	%edi, %esi
	cmpl	%r8d, %esi
	cmovgl	%r8d, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#70:                                #   in Loop: Header=BB3_66 Depth=4
	xorl	%eax, %eax
	movl	%ebp, %esi
	addl	$2, %esi
	cmovsl	%eax, %esi
	cmpl	%r8d, %esi
	cmovgl	%r8d, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#71:                                #   in Loop: Header=BB3_66 Depth=4
	xorl	%eax, %eax
	addl	$3, %ebp
	cmovsl	%eax, %ebp
	cmpl	%r8d, %ebp
	cmovgl	%r8d, %ebp
	movslq	%ebp, %rsi
	movzbl	(%rcx,%rsi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#72:                                #   in Loop: Header=BB3_66 Depth=4
	movq	-120(%rsp), %r14        # 8-byte Reload
	movq	-128(%rsp), %rsi        # 8-byte Reload
	movq	(%r14,%rsi,8), %rsi
	movzbl	(%rsi,%rbx), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#73:                                #   in Loop: Header=BB3_66 Depth=4
	movq	-112(%rsp), %rsi        # 8-byte Reload
	movq	(%r14,%rsi,8), %rbp
	movzbl	(%rbp,%rbx), %ebp
	cmpl	%edx, %ebp
	jle	.LBB3_108
# BB#74:                                #   in Loop: Header=BB3_66 Depth=4
	movq	-88(%rsp), %rsi         # 8-byte Reload
	movq	(%r14,%rsi,8), %rsi
	movzbl	(%rsi,%rbx), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#75:                                #   in Loop: Header=BB3_66 Depth=4
	movq	(%r14,%r11,8), %rsi
	movzbl	(%rsi,%rbx), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#76:                                #   in Loop: Header=BB3_66 Depth=4
	movq	(%r14,%r13,8), %rsi
	movzbl	(%rsi,%rbx), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#77:                                #   in Loop: Header=BB3_66 Depth=4
	movq	(%r14,%r12,8), %rsi
	movzbl	(%rsi,%rbx), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#78:                                # %.loopexit230.us.us302
                                        #   in Loop: Header=BB3_66 Depth=4
	cmpl	-96(%rsp), %edi         # 4-byte Folded Reload
	movl	%edi, %ebp
	jl	.LBB3_66
# BB#79:                                # %.us-lcssa.us.us
                                        #   in Loop: Header=BB3_65 Depth=3
	cmpl	-104(%rsp), %r15d       # 4-byte Folded Reload
	movl	-80(%rsp), %esi         # 4-byte Reload
	jl	.LBB3_65
	jmp	.LBB3_106
.LBB3_91:                               # %.preheader238
                                        #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_92 Depth 4
	movl	%r15d, %eax
	movl	%eax, %ecx
	addl	%esi, %ecx
	movl	$0, %edi
	cmovsl	%edi, %ecx
	cmpl	%r9d, %ecx
	cmovgl	%r9d, %ecx
	movslq	%ecx, %rcx
	movq	-120(%rsp), %rsi        # 8-byte Reload
	movq	(%rsi,%rcx,8), %rcx
	movl	%eax, %esi
	addl	$-2, %esi
	cmovsl	%edi, %esi
	cmpl	%r9d, %esi
	cmovgl	%r9d, %esi
	movslq	%esi, %rsi
	movq	%rsi, -128(%rsp)        # 8-byte Spill
	movl	%eax, %esi
	decl	%esi
	cmovsl	%edi, %esi
	cmpl	%r9d, %esi
	cmovgl	%r9d, %esi
	movslq	%esi, %rsi
	movq	%rsi, -112(%rsp)        # 8-byte Spill
	testl	%eax, %eax
	movl	$0, %esi
	cmovnsl	%eax, %esi
	cmpl	%r9d, %esi
	cmovgl	%r9d, %esi
	movslq	%esi, %rsi
	movq	%rsi, -88(%rsp)         # 8-byte Spill
	incl	%r15d
	movl	$0, %esi
	cmovnsl	%r15d, %esi
	cmpl	%r9d, %esi
	cmovgl	%r9d, %esi
	movslq	%esi, %r11
	movl	%eax, %esi
	addl	$2, %esi
	cmovsl	%edi, %esi
	cmpl	%r9d, %esi
	cmovgl	%r9d, %esi
	movslq	%esi, %r13
	addl	$3, %eax
	cmovsl	%edi, %eax
	cmpl	%r9d, %eax
	cmovgl	%r9d, %eax
	movslq	%eax, %r12
	movl	%r10d, %ebp
.LBB3_92:                               #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        #       Parent Loop BB3_91 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	xorl	%eax, %eax
	movl	%ebp, %edi
	addl	$-2, %edi
	cmovsl	%eax, %edi
	cmpl	%r8d, %edi
	cmovgl	%r8d, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	cmpl	%edx, %edi
	jle	.LBB3_108
# BB#93:                                #   in Loop: Header=BB3_92 Depth=4
	xorl	%eax, %eax
	movl	%ebp, %edi
	decl	%edi
	cmovsl	%eax, %edi
	cmpl	%r8d, %edi
	cmovgl	%r8d, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	cmpl	%edx, %edi
	jle	.LBB3_108
# BB#94:                                #   in Loop: Header=BB3_92 Depth=4
	xorl	%eax, %eax
	testl	%ebp, %ebp
	movl	$0, %edi
	cmovnsl	%ebp, %edi
	cmpl	%r8d, %edi
	cmovgl	%r8d, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	cmpl	%edx, %edi
	jle	.LBB3_108
# BB#95:                                #   in Loop: Header=BB3_92 Depth=4
	xorl	%eax, %eax
	movl	%ebp, %ebx
	incl	%ebx
	movl	$0, %edi
	cmovnsl	%ebx, %edi
	cmpl	%r8d, %edi
	cmovgl	%r8d, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#96:                                #   in Loop: Header=BB3_92 Depth=4
	xorl	%eax, %eax
	movl	%ebp, %esi
	addl	$2, %esi
	cmovsl	%eax, %esi
	cmpl	%r8d, %esi
	cmovgl	%r8d, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#97:                                #   in Loop: Header=BB3_92 Depth=4
	xorl	%eax, %eax
	addl	$3, %ebp
	cmovsl	%eax, %ebp
	cmpl	%r8d, %ebp
	cmovgl	%r8d, %ebp
	movslq	%ebp, %rsi
	movzbl	(%rcx,%rsi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#98:                                #   in Loop: Header=BB3_92 Depth=4
	movq	-120(%rsp), %r14        # 8-byte Reload
	movq	-128(%rsp), %rsi        # 8-byte Reload
	movq	(%r14,%rsi,8), %rsi
	movzbl	(%rsi,%rdi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#99:                                #   in Loop: Header=BB3_92 Depth=4
	movq	-112(%rsp), %rsi        # 8-byte Reload
	movq	(%r14,%rsi,8), %rbp
	movzbl	(%rbp,%rdi), %ebp
	cmpl	%edx, %ebp
	jle	.LBB3_108
# BB#100:                               #   in Loop: Header=BB3_92 Depth=4
	movq	-88(%rsp), %rsi         # 8-byte Reload
	movq	(%r14,%rsi,8), %rsi
	movzbl	(%rsi,%rdi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#101:                               #   in Loop: Header=BB3_92 Depth=4
	movq	(%r14,%r11,8), %rsi
	movzbl	(%rsi,%rdi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#102:                               #   in Loop: Header=BB3_92 Depth=4
	movq	(%r14,%r13,8), %rsi
	movzbl	(%rsi,%rdi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#103:                               #   in Loop: Header=BB3_92 Depth=4
	movq	(%r14,%r12,8), %rsi
	movzbl	(%rsi,%rdi), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#104:                               # %.loopexit230301
                                        #   in Loop: Header=BB3_92 Depth=4
	cmpl	-96(%rsp), %ebx         # 4-byte Folded Reload
	movl	%ebx, %ebp
	jl	.LBB3_92
# BB#105:                               # %.us-lcssa
                                        #   in Loop: Header=BB3_91 Depth=3
	cmpl	-104(%rsp), %r15d       # 4-byte Folded Reload
	movl	-80(%rsp), %esi         # 4-byte Reload
	jl	.LBB3_91
	jmp	.LBB3_106
	.p2align	4, 0x90
.LBB3_5:                                #   Parent Loop BB3_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_91 Depth 3
                                        #         Child Loop BB3_92 Depth 4
                                        #       Child Loop BB3_65 Depth 3
                                        #         Child Loop BB3_66 Depth 4
                                        #       Child Loop BB3_80 Depth 3
                                        #         Child Loop BB3_81 Depth 4
                                        #           Child Loop BB3_82 Depth 5
                                        #       Child Loop BB3_26 Depth 3
                                        #         Child Loop BB3_27 Depth 4
                                        #           Child Loop BB3_28 Depth 5
                                        #       Child Loop BB3_38 Depth 3
                                        #       Child Loop BB3_14 Depth 3
                                        #         Child Loop BB3_15 Depth 4
                                        #       Child Loop BB3_7 Depth 3
	movq	(%r11,%rbp,8), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movswl	2(%rax), %r15d
	addl	%ebx, %r15d
	movswl	(%rax), %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	leal	(%rcx,%rbp), %esi
	shll	$4, %esi
	addl	%eax, %esi
	movl	%r15d, %ecx
	andl	$3, %ecx
	movl	%esi, %eax
	andl	$3, %eax
	movl	%esi, %edi
	orl	%r15d, %edi
	subl	%ecx, %r15d
	sarl	$2, %r15d
	movl	%esi, %r10d
	subl	%eax, %r10d
	sarl	$2, %r10d
	testb	$3, %dil
	movq	%rbp, -32(%rsp)         # 8-byte Spill
	je	.LBB3_6
# BB#12:                                #   in Loop: Header=BB3_5 Depth=2
	testl	%ecx, %ecx
	je	.LBB3_13
# BB#23:                                #   in Loop: Header=BB3_5 Depth=2
	andb	$3, %sil
	je	.LBB3_37
# BB#24:                                #   in Loop: Header=BB3_5 Depth=2
	cmpb	$2, %sil
	jne	.LBB3_63
# BB#25:                                # %.preheader240.preheader
                                        #   in Loop: Header=BB3_5 Depth=2
	leal	3(%r10), %eax
	movl	%eax, -96(%rsp)         # 4-byte Spill
	leal	3(%r15), %eax
	movl	%eax, -104(%rsp)        # 4-byte Spill
.LBB3_26:                               # %.preheader240
                                        #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_27 Depth 4
                                        #           Child Loop BB3_28 Depth 5
	movl	%r10d, %ebx
.LBB3_27:                               # %.preheader234
                                        #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        #       Parent Loop BB3_26 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB3_28 Depth 5
	movl	%ebx, %eax
	addl	$-2, %eax
	movl	$0, %esi
	cmovsl	%esi, %eax
	cmpl	%r8d, %eax
	cmovgl	%r8d, %eax
	cltq
	movq	%rax, -128(%rsp)        # 8-byte Spill
	movl	%ebx, %eax
	decl	%eax
	cmovsl	%esi, %eax
	cmpl	%r8d, %eax
	cmovgl	%r8d, %eax
	cltq
	movq	%rax, -112(%rsp)        # 8-byte Spill
	testl	%ebx, %ebx
	movl	$0, %eax
	cmovnsl	%ebx, %eax
	cmpl	%r8d, %eax
	cmovgl	%r8d, %eax
	movslq	%eax, %rcx
	movl	%ebx, %edi
	incl	%edi
	movl	$0, %eax
	movl	%edi, %r13d
	cmovnsl	%edi, %eax
	cmpl	%r8d, %eax
	cmovgl	%r8d, %eax
	movslq	%eax, %rbp
	movl	%ebx, %eax
	addl	$2, %eax
	cmovsl	%esi, %eax
	cmpl	%r8d, %eax
	cmovgl	%r8d, %eax
	movslq	%eax, %r11
	movl	%ebx, %eax
	addl	$3, %eax
	cmovsl	%esi, %eax
	cmpl	%r8d, %eax
	cmovgl	%r8d, %eax
	movslq	%eax, %r14
	movl	$-2, %r12d
	.p2align	4, 0x90
.LBB3_28:                               #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        #       Parent Loop BB3_26 Depth=3
                                        #         Parent Loop BB3_27 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	xorl	%eax, %eax
	movl	%r15d, %esi
	addl	%r12d, %esi
	cmovsl	%eax, %esi
	cmpl	%r9d, %esi
	cmovgl	%r9d, %esi
	movslq	%esi, %rsi
	movq	-120(%rsp), %rdi        # 8-byte Reload
	movq	(%rdi,%rsi,8), %rsi
	movq	-128(%rsp), %rdi        # 8-byte Reload
	movzbl	(%rsi,%rdi), %edi
	cmpl	%edx, %edi
	jle	.LBB3_108
# BB#29:                                #   in Loop: Header=BB3_28 Depth=5
	movq	-112(%rsp), %rdi        # 8-byte Reload
	movzbl	(%rsi,%rdi), %edi
	cmpl	%edx, %edi
	jle	.LBB3_108
# BB#30:                                #   in Loop: Header=BB3_28 Depth=5
	movzbl	(%rsi,%rcx), %edi
	cmpl	%edx, %edi
	jle	.LBB3_108
# BB#31:                                #   in Loop: Header=BB3_28 Depth=5
	movzbl	(%rsi,%rbp), %edi
	cmpl	%edx, %edi
	jle	.LBB3_108
# BB#32:                                #   in Loop: Header=BB3_28 Depth=5
	movzbl	(%rsi,%r11), %edi
	cmpl	%edx, %edi
	jle	.LBB3_108
# BB#33:                                #   in Loop: Header=BB3_28 Depth=5
	movzbl	(%rsi,%r14), %esi
	cmpl	%edx, %esi
	jle	.LBB3_108
# BB#34:                                #   in Loop: Header=BB3_28 Depth=5
	incl	%r12d
	cmpl	$4, %r12d
	jl	.LBB3_28
# BB#35:                                #   in Loop: Header=BB3_27 Depth=4
	cmpl	-96(%rsp), %ebx         # 4-byte Folded Reload
	movl	%r13d, %ebx
	jl	.LBB3_27
# BB#36:                                #   in Loop: Header=BB3_26 Depth=3
	cmpl	-104(%rsp), %r15d       # 4-byte Folded Reload
	leal	1(%r15), %eax
	movl	%eax, %r15d
	jl	.LBB3_26
	jmp	.LBB3_106
	.p2align	4, 0x90
.LBB3_6:                                # %.preheader235.preheader
                                        #   in Loop: Header=BB3_5 Depth=2
	leal	3(%r15), %r11d
	testl	%r10d, %r10d
	movl	$0, %eax
	cmovnsl	%r10d, %eax
	cmpl	%r8d, %eax
	cmovgl	%r8d, %eax
	movslq	%eax, %r14
	movl	%r10d, %eax
	incl	%eax
	movl	$0, %ecx
	cmovsl	%ecx, %eax
	cmpl	%r8d, %eax
	cmovgl	%r8d, %eax
	movslq	%eax, %rbp
	movl	%r10d, %eax
	addl	$2, %eax
	cmovsl	%ecx, %eax
	cmpl	%r8d, %eax
	cmovgl	%r8d, %eax
	movslq	%eax, %rbx
	addl	$3, %r10d
	cmovsl	%ecx, %r10d
	cmpl	%r8d, %r10d
	cmovgl	%r8d, %r10d
	movslq	%r10d, %rsi
	.p2align	4, 0x90
.LBB3_7:                                #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorl	%eax, %eax
	testl	%r15d, %r15d
	movl	$0, %ecx
	cmovnsl	%r15d, %ecx
	cmpl	%r9d, %ecx
	cmovgl	%r9d, %ecx
	movslq	%ecx, %rcx
	movq	-120(%rsp), %rdi        # 8-byte Reload
	movq	(%rdi,%rcx,8), %rcx
	movzbl	(%rcx,%r14), %edi
	cmpl	%edx, %edi
	jle	.LBB3_108
# BB#8:                                 #   in Loop: Header=BB3_7 Depth=3
	movzbl	(%rcx,%rbp), %edi
	cmpl	%edx, %edi
	jle	.LBB3_108
# BB#9:                                 #   in Loop: Header=BB3_7 Depth=3
	movzbl	(%rcx,%rbx), %edi
	cmpl	%edx, %edi
	jle	.LBB3_108
# BB#10:                                #   in Loop: Header=BB3_7 Depth=3
	movzbl	(%rcx,%rsi), %ecx
	cmpl	%edx, %ecx
	jle	.LBB3_108
# BB#11:                                #   in Loop: Header=BB3_7 Depth=3
	cmpl	%r11d, %r15d
	leal	1(%r15), %eax
	movl	%eax, %r15d
	jl	.LBB3_7
.LBB3_106:                              # %.loopexit242
                                        #   in Loop: Header=BB3_5 Depth=2
	movq	-32(%rsp), %rbp         # 8-byte Reload
	incq	%rbp
	cmpq	-48(%rsp), %rbp         # 8-byte Folded Reload
	movq	-40(%rsp), %r11         # 8-byte Reload
	movl	-60(%rsp), %ebx         # 4-byte Reload
	jl	.LBB3_5
# BB#107:                               #   in Loop: Header=BB3_4 Depth=1
	movq	-72(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movl	$1, %eax
	movq	%rcx, -72(%rsp)         # 8-byte Spill
	cmpq	-56(%rsp), %rcx         # 8-byte Folded Reload
	jl	.LBB3_4
.LBB3_108:                              # %.loopexit
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	CheckReliabilityOfRef, .Lfunc_end3-CheckReliabilityOfRef
	.cfi_endproc

	.globl	RDCost_for_4x4IntraBlocks
	.p2align	4, 0x90
	.type	RDCost_for_4x4IntraBlocks,@function
RDCost_for_4x4IntraBlocks:              # @RDCost_for_4x4IntraBlocks
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 160
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movl	%r8d, 8(%rsp)           # 4-byte Spill
	movl	%ecx, 20(%rsp)          # 4-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %rbp
	leal	(,%rsi,8), %eax
	andl	$8, %eax
	movl	%edx, %ecx
	andl	$1, %ecx
	leal	(%rax,%rcx,4), %edi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	leal	(,%rsi,4), %eax
	andl	$-8, %eax
	movl	%edx, %ecx
	andl	$2147483646, %ecx       # imm = 0x7FFFFFFE
	leal	(%rax,%rcx,2), %esi
	movq	img(%rip), %rax
	movslq	176(%rax), %rcx
	leaq	(%rdi,%rcx), %r13
	movslq	180(%rax), %r15
	movslq	%esi, %rcx
	addq	%rcx, %r15
	movslq	196(%rax), %r12
	addq	%rcx, %r12
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %r14
	movq	14216(%rax), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	input(%rip), %rcx
	movslq	4016(%rcx), %rcx
	movq	assignSE2partition(,%rcx,8), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	$0, 4(%rsp)
	cmpl	$3, 20(%rax)
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	jne	.LBB4_1
# BB#2:
	movl	sp2_frame_indicator(%rip), %eax
	orl	si_frame_indicator(%rip), %eax
	je	.LBB4_3
# BB#6:
	leaq	4(%rsp), %rdx
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	dct_luma_sp2
	jmp	.LBB4_7
.LBB4_1:
	leaq	4(%rsp), %rdx
	movl	$1, %ecx
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	dct_luma
	jmp	.LBB4_7
.LBB4_3:
	leaq	4(%rsp), %rdx
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	dct_luma_sp
.LBB4_7:
	movl	%eax, (%rbp)
	movq	img(%rip), %rax
	movq	14232(%rax), %rax
	movq	imgY_org(%rip), %rcx
	movq	(%rcx,%r12,8), %rdx
	movq	(%r14,%r15,8), %rsi
	movzwl	(%rdx,%r13,2), %edi
	movzwl	(%rsi,%r13,2), %ebp
	subq	%rbp, %rdi
	movslq	(%rax,%rdi,4), %rdi
	movzwl	2(%rdx,%r13,2), %ebp
	movzwl	2(%rsi,%r13,2), %ebx
	subq	%rbx, %rbp
	movslq	(%rax,%rbp,4), %rbp
	addq	%rdi, %rbp
	movzwl	4(%rdx,%r13,2), %edi
	movzwl	4(%rsi,%r13,2), %ebx
	subq	%rbx, %rdi
	movslq	(%rax,%rdi,4), %rdi
	addq	%rbp, %rdi
	movzwl	6(%rdx,%r13,2), %edx
	movzwl	6(%rsi,%r13,2), %esi
	subq	%rsi, %rdx
	movslq	(%rax,%rdx,4), %rdx
	addq	%rdi, %rdx
	movq	8(%rcx,%r12,8), %rsi
	movq	8(%r14,%r15,8), %rdi
	movzwl	(%rsi,%r13,2), %ebp
	movzwl	(%rdi,%r13,2), %ebx
	subq	%rbx, %rbp
	movslq	(%rax,%rbp,4), %rbp
	addq	%rdx, %rbp
	movzwl	2(%rsi,%r13,2), %edx
	movzwl	2(%rdi,%r13,2), %ebx
	subq	%rbx, %rdx
	movslq	(%rax,%rdx,4), %rdx
	addq	%rbp, %rdx
	movzwl	4(%rsi,%r13,2), %ebp
	movzwl	4(%rdi,%r13,2), %ebx
	subq	%rbx, %rbp
	movslq	(%rax,%rbp,4), %rbp
	addq	%rdx, %rbp
	movzwl	6(%rsi,%r13,2), %edx
	movzwl	6(%rdi,%r13,2), %esi
	subq	%rsi, %rdx
	movslq	(%rax,%rdx,4), %rdx
	addq	%rbp, %rdx
	movq	16(%rcx,%r12,8), %rsi
	movq	16(%r14,%r15,8), %rdi
	movzwl	(%rsi,%r13,2), %ebp
	movzwl	(%rdi,%r13,2), %ebx
	subq	%rbx, %rbp
	movslq	(%rax,%rbp,4), %rbp
	addq	%rdx, %rbp
	movzwl	2(%rsi,%r13,2), %edx
	movzwl	2(%rdi,%r13,2), %ebx
	subq	%rbx, %rdx
	movslq	(%rax,%rdx,4), %rdx
	addq	%rbp, %rdx
	movzwl	4(%rsi,%r13,2), %ebp
	movzwl	4(%rdi,%r13,2), %ebx
	subq	%rbx, %rbp
	movslq	(%rax,%rbp,4), %rbp
	addq	%rdx, %rbp
	movzwl	6(%rsi,%r13,2), %edx
	movzwl	6(%rdi,%r13,2), %esi
	subq	%rsi, %rdx
	movslq	(%rax,%rdx,4), %rdx
	addq	%rbp, %rdx
	movq	24(%rcx,%r12,8), %rcx
	movq	24(%r14,%r15,8), %rsi
	movzwl	(%rcx,%r13,2), %edi
	movzwl	(%rsi,%r13,2), %ebp
	subq	%rbp, %rdi
	movslq	(%rax,%rdi,4), %rdi
	addq	%rdx, %rdi
	movzwl	2(%rcx,%r13,2), %edx
	movzwl	2(%rsi,%r13,2), %ebp
	subq	%rbp, %rdx
	movslq	(%rax,%rdx,4), %rdx
	addq	%rdi, %rdx
	movzwl	4(%rcx,%r13,2), %edi
	movzwl	4(%rsi,%r13,2), %ebp
	subq	%rbp, %rdi
	movslq	(%rax,%rdi,4), %rdi
	addq	%rdx, %rdi
	movzwl	6(%rcx,%r13,2), %ecx
	movzwl	6(%rsi,%r13,2), %edx
	subq	%rdx, %rcx
	movslq	(%rax,%rcx,4), %rax
	addq	%rdi, %rax
	xorl	%ecx, %ecx
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	20(%rsp), %edi          # 4-byte Reload
	cmpl	%esi, %edi
	setge	%cl
	movl	%edi, %edx
	subl	%ecx, %edx
	cmpl	%edi, %esi
	cvtsi2sdq	%rax, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	$-1, %eax
	cmovnel	%edx, %eax
	movl	%eax, 68(%rsp)
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	leal	(%rbx,%rbp,4), %eax
	movl	%eax, 88(%rsp)
	movl	$4, 64(%rsp)
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	16(%rax), %rax
	imulq	$104, %rax, %rsi
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	24(%rax), %rsi
	leaq	64(%rsp), %rdi
	callq	*writeIntraPredMode(%rip)
	movl	76(%rsp), %r14d
	movq	input(%rip), %rax
	cmpl	$0, 4008(%rax)
	je	.LBB4_8
# BB#4:
	movl	$1, %edx
	movl	%ebp, %edi
	movl	%ebx, %esi
	callq	writeLumaCoeff4x4_CABAC
	jmp	.LBB4_5
.LBB4_8:
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movl	%ebp, %esi
	movl	%ebx, %edx
	callq	writeCoeff4x4_CAVLC
.LBB4_5:
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addl	%r14d, %eax
	cvtsi2sdl	%eax, %xmm2
	mulsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm0
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	RDCost_for_4x4IntraBlocks, .Lfunc_end4-RDCost_for_4x4IntraBlocks
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4616189618054758400     # double 4
.LCPI5_1:
	.quad	5055640609639927018     # double 1.0E+30
	.text
	.globl	Mode_Decision_for_4x4IntraBlocks
	.p2align	4, 0x90
	.type	Mode_Decision_for_4x4IntraBlocks,@function
Mode_Decision_for_4x4IntraBlocks:       # @Mode_Decision_for_4x4IntraBlocks
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$376, %rsp              # imm = 0x178
.Lcfi46:
	.cfi_def_cfa_offset 432
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movapd	%xmm0, %xmm1
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	(,%rdi,8), %r15d
	andl	$8, %r15d
	movl	%esi, %ebx
	andl	$1, %ebx
	leal	(%r15,%rbx,4), %eax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	leal	(,%rdi,4), %r14d
	andl	$-8, %r14d
	movq	%rsi, %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	%esi, %ebp
	andl	$2147483646, %ebp       # imm = 0x7FFFFFFE
	leal	(%r14,%rbp,2), %ecx
	movq	img(%rip), %r12
	movslq	176(%r12), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movslq	180(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%ecx, 128(%rsp)         # 4-byte Spill
	movslq	%ecx, %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movslq	192(%r12), %r13
	movslq	196(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm1, 288(%rsp)        # 8-byte Spill
	mulsd	%xmm1, %xmm0
	callq	floor
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movl	12(%r12), %edi
	movq	280(%rsp), %r12         # 8-byte Reload
	leal	-1(%r15,%rbx,4), %esi
	leaq	256(%rsp), %rcx
	movl	%r12d, %edx
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	leal	-1(%r14,%rbp,2), %edx
	movq	200(%rsp), %r14         # 8-byte Reload
	leaq	232(%rsp), %rcx
	movl	%r14d, %esi
	callq	getLuma4x4Neighbour
	movq	input(%rip), %rax
	cmpl	$0, 272(%rax)
	je	.LBB5_1
# BB#2:
	xorl	%eax, %eax
	cmpl	$0, 256(%rsp)
	movl	$0, %ecx
	je	.LBB5_4
# BB#3:
	movq	img(%rip), %rcx
	movq	14240(%rcx), %rcx
	movslq	260(%rsp), %rdx
	movl	(%rcx,%rdx,4), %ecx
.LBB5_4:
	movl	%ecx, 256(%rsp)
	cmpl	$0, 232(%rsp)
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	je	.LBB5_6
# BB#5:
	movq	img(%rip), %rax
	movq	14240(%rax), %rax
	movslq	236(%rsp), %rcx
	movl	(%rax,%rcx,4), %eax
.LBB5_6:
	movl	%eax, 232(%rsp)
	jmp	.LBB5_7
.LBB5_1:                                # %._crit_edge
	movl	232(%rsp), %eax
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB5_7:
	addq	%r14, %rdi
	addq	%r12, %rsi
	testl	%eax, %eax
	je	.LBB5_8
# BB#9:
	movq	img(%rip), %rax
	movq	128(%rax), %rax
	movslq	252(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	248(%rsp), %rcx
	movb	(%rax,%rcx), %cl
	jmp	.LBB5_10
.LBB5_8:
	movb	$-1, %cl
.LBB5_10:
	movl	$2, %eax
	cmpl	$0, 256(%rsp)
	je	.LBB5_11
# BB#12:
	movq	img(%rip), %rdx
	movq	128(%rdx), %rdx
	movslq	276(%rsp), %rbp
	movq	(%rdx,%rbp,8), %rdx
	movslq	272(%rsp), %rbp
	movb	(%rdx,%rbp), %bl
	movl	%ebx, %edx
	orb	%cl, %dl
	movq	96(%rsp), %rdx          # 8-byte Reload
	js	.LBB5_16
# BB#13:
	cmpb	%bl, %cl
	jle	.LBB5_15
# BB#14:
	movl	%ebx, %ecx
.LBB5_15:
	movsbl	%cl, %eax
	jmp	.LBB5_16
.LBB5_11:
	movq	96(%rsp), %rdx          # 8-byte Reload
.LBB5_16:                               # %.thread
	addq	%r14, %r13
	addq	%r12, %r8
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movl	%edi, %ecx
	sarl	$2, %ecx
	movl	%ecx, 136(%rsp)         # 4-byte Spill
	movl	%esi, %ecx
	sarl	$2, %ecx
	movl	%ecx, 132(%rsp)         # 4-byte Spill
	cvttsd2si	%xmm0, %ecx
	movl	%ecx, 140(%rsp)         # 4-byte Spill
	movl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$2147483647, (%rdx)     # imm = 0x7FFFFFFF
	leaq	152(%rsp), %rdx
	leaq	148(%rsp), %rcx
	leaq	144(%rsp), %r8
	movq	%rdi, 40(%rsp)          # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%rsi, 32(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	intrapred_luma
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	%r14d, %r14d
	movslq	104(%rsp), %rax         # 4-byte Folded Reload
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movslq	112(%rsp), %rax         # 4-byte Folded Reload
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movq	%r12, %rax
	orq	$1, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%r12, %rax
	orq	$2, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r12, %rax
	orq	$3, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	160(%rsp), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	176(%rsp), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	184(%rsp), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movsd	.LCPI5_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm0, 120(%rsp)        # 8-byte Spill
	movl	$0, %r15d
	xorl	%ebp, %ebp
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB5_17:                               # =>This Inner Loop Header: Depth=1
	movb	$1, %al
	cmpq	$2, %rbp
	je	.LBB5_23
# BB#18:                                #   in Loop: Header=BB5_17 Depth=1
	testq	%rbp, %rbp
	sete	%cl
	movl	%ebp, %edx
	orl	$4, %edx
	cmpl	$7, %edx
	sete	%dl
	orb	%cl, %dl
	cmpb	$1, %dl
	jne	.LBB5_20
# BB#19:                                #   in Loop: Header=BB5_17 Depth=1
	movl	148(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.LBB5_23
.LBB5_20:                               #   in Loop: Header=BB5_17 Depth=1
	cmpq	$1, %rbp
	sete	%cl
	cmpq	$8, %rbp
	sete	%dl
	orb	%cl, %dl
	cmpb	$1, %dl
	jne	.LBB5_22
# BB#21:                                #   in Loop: Header=BB5_17 Depth=1
	movl	152(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.LBB5_23
.LBB5_22:                               #   in Loop: Header=BB5_17 Depth=1
	cmpl	$0, 144(%rsp)
	setne	%al
	.p2align	4, 0x90
.LBB5_23:                               #   in Loop: Header=BB5_17 Depth=1
	movq	input(%rip), %rcx
	cmpl	$0, 4048(%rcx)
	je	.LBB5_25
# BB#24:                                #   in Loop: Header=BB5_17 Depth=1
	movq	img(%rip), %rdx
	cmpl	$2, 20(%rdx)
	jne	.LBB5_25
# BB#31:                                #   in Loop: Header=BB5_17 Depth=1
	testb	%al, %al
	jne	.LBB5_32
	jmp	.LBB5_45
	.p2align	4, 0x90
.LBB5_25:                               #   in Loop: Header=BB5_17 Depth=1
	movl	%ebp, %edx
	orl	$1, %edx
	cmpl	$1, %edx
	jne	.LBB5_27
# BB#26:                                #   in Loop: Header=BB5_17 Depth=1
	movl	4052(%rcx), %edx
	testl	%edx, %edx
	jne	.LBB5_45
.LBB5_27:                               #   in Loop: Header=BB5_17 Depth=1
	leal	-3(%rbp), %edx
	cmpl	$1, %edx
	ja	.LBB5_29
# BB#28:                                #   in Loop: Header=BB5_17 Depth=1
	movl	4056(%rcx), %edx
	testl	%edx, %edx
	jne	.LBB5_45
.LBB5_29:                               #   in Loop: Header=BB5_17 Depth=1
	cmpl	$0, 4060(%rcx)
	setne	%dl
	cmpq	$4, %rbp
	setg	%bl
	testb	%dl, %bl
	jne	.LBB5_45
# BB#30:                                #   in Loop: Header=BB5_17 Depth=1
	xorb	$1, %al
	jne	.LBB5_45
.LBB5_32:                               #   in Loop: Header=BB5_17 Depth=1
	cmpl	$0, 4168(%rcx)
	je	.LBB5_33
# BB#35:                                #   in Loop: Header=BB5_17 Depth=1
	movq	img(%rip), %rax
	movq	imgY_org(%rip), %rcx
	movq	%r12, %rdx
	shlq	$5, %rdx
	leaq	12624(%rax,%rdx), %rdx
	movq	208(%rax,%r15), %rsi
	movq	%rsi, (%rdx,%r14,2)
	movq	(%rcx,%r8,8), %rdx
	movzwl	(%rdx,%r13,2), %esi
	movzwl	208(%rax,%r15), %edi
	subl	%edi, %esi
	movl	%esi, 13136(%rax)
	movzwl	2(%rdx,%r13,2), %esi
	movzwl	210(%rax,%r15), %edi
	subl	%edi, %esi
	movl	%esi, 13140(%rax)
	movzwl	4(%rdx,%r13,2), %esi
	movzwl	212(%rax,%r15), %edi
	subl	%edi, %esi
	movl	%esi, 13144(%rax)
	movzwl	6(%rdx,%r13,2), %edx
	movzwl	214(%rax,%r15), %esi
	subl	%esi, %edx
	movl	%edx, 13148(%rax)
	movq	48(%rsp), %rdx          # 8-byte Reload
	shlq	$5, %rdx
	leaq	12624(%rax,%rdx), %rdx
	movq	240(%rax,%r15), %rsi
	movq	%rsi, (%rdx,%r14,2)
	movq	8(%rcx,%r8,8), %rdx
	movzwl	(%rdx,%r13,2), %esi
	movzwl	240(%rax,%r15), %edi
	subl	%edi, %esi
	movl	%esi, 13200(%rax)
	movzwl	2(%rdx,%r13,2), %esi
	movzwl	242(%rax,%r15), %edi
	subl	%edi, %esi
	movl	%esi, 13204(%rax)
	movzwl	4(%rdx,%r13,2), %esi
	movzwl	244(%rax,%r15), %edi
	subl	%edi, %esi
	movl	%esi, 13208(%rax)
	movzwl	6(%rdx,%r13,2), %edx
	movzwl	246(%rax,%r15), %esi
	subl	%esi, %edx
	movl	%edx, 13212(%rax)
	movq	56(%rsp), %rdx          # 8-byte Reload
	shlq	$5, %rdx
	leaq	12624(%rax,%rdx), %rdx
	movq	272(%rax,%r15), %rsi
	movq	%rsi, (%rdx,%r14,2)
	movq	16(%rcx,%r8,8), %rdx
	movzwl	(%rdx,%r13,2), %esi
	movzwl	272(%rax,%r15), %edi
	subl	%edi, %esi
	movl	%esi, 13264(%rax)
	movzwl	2(%rdx,%r13,2), %esi
	movzwl	274(%rax,%r15), %edi
	subl	%edi, %esi
	movl	%esi, 13268(%rax)
	movzwl	4(%rdx,%r13,2), %esi
	movzwl	276(%rax,%r15), %edi
	subl	%edi, %esi
	movl	%esi, 13272(%rax)
	movzwl	6(%rdx,%r13,2), %edx
	movzwl	278(%rax,%r15), %esi
	subl	%esi, %edx
	movl	%edx, 13276(%rax)
	movq	64(%rsp), %rdx          # 8-byte Reload
	shlq	$5, %rdx
	leaq	12624(%rax,%rdx), %rdx
	movq	304(%rax,%r15), %rsi
	movq	%rsi, (%rdx,%r14,2)
	movq	24(%rcx,%r8,8), %rcx
	movzwl	(%rcx,%r13,2), %edx
	movzwl	304(%rax,%r15), %esi
	subl	%esi, %edx
	movl	%edx, 13328(%rax)
	movzwl	2(%rcx,%r13,2), %edx
	movzwl	306(%rax,%r15), %esi
	subl	%esi, %edx
	movl	%edx, 13332(%rax)
	movzwl	4(%rcx,%r13,2), %edx
	movzwl	308(%rax,%r15), %esi
	subl	%esi, %edx
	movl	%edx, 13336(%rax)
	movzwl	6(%rcx,%r13,2), %ecx
	movzwl	310(%rax,%r15), %edx
	subl	%edx, %ecx
	movl	%ecx, 13340(%rax)
	leaq	156(%rsp), %rdi
	movq	104(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	112(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%ebp, %ecx
	movsd	288(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	24(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	RDCost_for_4x4IntraBlocks
	movq	120(%rsp), %xmm1        # 8-byte Folded Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB5_43
# BB#36:                                #   in Loop: Header=BB5_17 Depth=1
	movq	cofAC4x4(%rip), %rax
	movq	(%rax), %rcx
	movq	img(%rip), %rdx
	movq	14160(%rdx), %rdx
	movq	216(%rsp), %rdi         # 8-byte Reload
	movq	(%rdx,%rdi,8), %rdx
	movq	208(%rsp), %rbx         # 8-byte Reload
	movq	(%rdx,%rbx,8), %rdx
	movq	(%rdx), %rdx
	movq	64(%rdx), %rsi
	movq	%rsi, 64(%rcx)
	movups	(%rdx), %xmm1
	movups	16(%rdx), %xmm2
	movups	32(%rdx), %xmm3
	movups	48(%rdx), %xmm4
	movups	%xmm4, 48(%rcx)
	movups	%xmm3, 32(%rcx)
	movups	%xmm2, 16(%rcx)
	movups	%xmm1, (%rcx)
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movq	14160(%rcx), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	8(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movdqu	(%rcx), %xmm1
	movdqu	16(%rcx), %xmm2
	movups	32(%rcx), %xmm3
	movups	48(%rcx), %xmm4
	movups	%xmm4, 48(%rax)
	movups	%xmm3, 32(%rax)
	movdqu	%xmm2, 16(%rax)
	movdqu	%xmm1, (%rax)
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rcx
	movq	img(%rip), %rax
	cmpl	$3, 20(%rax)
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	(%rcx,%rbx,8), %rdx
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdx,%rdi,2), %rdx
	movq	%rdx, 224(%rsp)         # 8-byte Spill
	jne	.LBB5_39
# BB#37:                                #   in Loop: Header=BB5_17 Depth=1
	movl	sp2_frame_indicator(%rip), %edx
	orl	si_frame_indicator(%rip), %edx
	jne	.LBB5_39
# BB#38:                                # %.split.preheader
                                        #   in Loop: Header=BB5_17 Depth=1
	movq	lrec(%rip), %rdx
	movq	(%rdx,%rbx,8), %rsi
	movups	(%rsi,%rdi,4), %xmm1
	movaps	%xmm1, 304(%rsp)
	movq	8(%rcx,%rbx,8), %rsi
	movq	(%rsi,%rdi,2), %rsi
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	8(%rdx,%rbx,8), %rsi
	movups	(%rsi,%rdi,4), %xmm1
	leaq	320(%rsp), %rsi
	movq	%rsi, %r8
	movups	%xmm1, (%r8)
	movq	16(%rcx,%rbx,8), %rsi
	movq	(%rsi,%rdi,2), %rsi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	16(%rdx,%rbx,8), %rsi
	movups	(%rsi,%rdi,4), %xmm1
	movups	%xmm1, 16(%r8)
	movq	24(%rcx,%rbx,8), %rcx
	movq	(%rcx,%rdi,2), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	24(%rdx,%rbx,8), %rcx
	movdqu	(%rcx,%rdi,4), %xmm1
	movdqu	%xmm1, 32(%r8)
	jmp	.LBB5_40
.LBB5_33:                               # %.preheader271
                                        #   in Loop: Header=BB5_17 Depth=1
	movq	imgY_org(%rip), %rcx
	movq	img(%rip), %rax
	movq	(%rcx,%r8,8), %rdx
	movq	(%rdx,%r13,2), %xmm0    # xmm0 = mem[0],zero
	pxor	%xmm2, %xmm2
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	208(%rax,%r15), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff(%rip)
	movq	8(%rcx,%r8,8), %rdx
	movq	(%rdx,%r13,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	240(%rax,%r15), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+16(%rip)
	movq	16(%rcx,%r8,8), %rdx
	movq	(%rdx,%r13,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	272(%rax,%r15), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+32(%rip)
	movq	24(%rcx,%r8,8), %rcx
	movq	(%rcx,%r13,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	304(%rax,%r15), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+48(%rip)
	cmpq	%rbp, 24(%rsp)          # 8-byte Folded Reload
	movl	140(%rsp), %ebx         # 4-byte Reload
	movl	$0, %eax
	cmovel	%eax, %ebx
	movl	$diff, %edi
	callq	distortion4x4
	addl	%ebx, %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx), %eax
	jge	.LBB5_44
# BB#34:                                #   in Loop: Header=BB5_17 Depth=1
	movl	%eax, (%rcx)
	movl	%ebp, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jmp	.LBB5_44
.LBB5_39:                               # %.split.us.preheader
                                        #   in Loop: Header=BB5_17 Depth=1
	movq	8(%rcx,%rbx,8), %rdx
	movq	(%rdx,%rdi,2), %rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	16(%rcx,%rbx,8), %rdx
	movq	(%rdx,%rdi,2), %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	24(%rcx,%rbx,8), %rcx
	movq	(%rcx,%rdi,2), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
.LBB5_40:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB5_17 Depth=1
	movl	156(%rsp), %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	cmpl	$0, 15260(%rax)
	je	.LBB5_42
# BB#41:                                # %.preheader272.preheader
                                        #   in Loop: Header=BB5_17 Depth=1
	movq	fadjust4x4(%rip), %rcx
	movq	(%rcx,%r12,8), %rdx
	movq	14176(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movups	(%rax,%r14,4), %xmm1
	movups	%xmm1, (%rdx,%r14,4)
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	(%rcx,%rsi,8), %rax
	movq	img(%rip), %rdx
	movq	14176(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movups	(%rdx,%r14,4), %xmm1
	movups	%xmm1, (%rax,%r14,4)
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	(%rcx,%rsi,8), %rax
	movq	img(%rip), %rdx
	movq	14176(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movups	(%rdx,%r14,4), %xmm1
	movups	%xmm1, (%rax,%r14,4)
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	(%rcx,%rdx,8), %rax
	movq	img(%rip), %rcx
	movq	14176(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movdqu	(%rcx,%r14,4), %xmm1
	movdqu	%xmm1, (%rax,%r14,4)
.LBB5_42:                               # %.loopexit273
                                        #   in Loop: Header=BB5_17 Depth=1
	movsd	%xmm0, 120(%rsp)        # 8-byte Spill
	movl	%ebp, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB5_43:                               # %.loopexit273
                                        #   in Loop: Header=BB5_17 Depth=1
	movq	cs_cm(%rip), %rdi
	callq	reset_coding_state
.LBB5_44:                               #   in Loop: Header=BB5_17 Depth=1
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB5_45:                               #   in Loop: Header=BB5_17 Depth=1
	incq	%rbp
	addq	$512, %r15              # imm = 0x200
	cmpq	$9, %rbp
	jne	.LBB5_17
# BB#46:
	movq	224(%rsp), %rax         # 8-byte Reload
	movq	%rax, 160(%rsp)
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rax, 168(%rsp)
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, 176(%rsp)
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, 184(%rsp)
	movq	img(%rip), %rax
	movq	128(%rax), %rax
	movslq	132(%rsp), %rcx         # 4-byte Folded Reload
	movq	(%rax,%rcx,8), %rax
	movslq	136(%rsp), %rcx         # 4-byte Folded Reload
	movl	4(%rsp), %ebp           # 4-byte Reload
	movb	%bpl, (%rax,%rcx)
	xorl	%eax, %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpl	%ecx, %ebp
	setge	%dl
	cmpl	%ebp, %ecx
	movb	$-1, %cl
	je	.LBB5_48
# BB#47:
	movb	%dl, %al
	movl	%ebp, %ecx
	subl	%eax, %ecx
.LBB5_48:
	movq	img(%rip), %rax
	movslq	12(%rax), %rdx
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	104(%rsp), %rdi         # 8-byte Reload
	leal	(%rsi,%rdi,4), %esi
	movslq	%esi, %rsi
	imulq	$536, %rdx, %rdx        # imm = 0x218
	addq	14224(%rax), %rdx
	movb	%cl, 332(%rsi,%rdx)
	movq	input(%rip), %rcx
	cmpl	$0, 4168(%rcx)
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	32(%rsp), %r10          # 8-byte Reload
	je	.LBB5_64
# BB#49:                                # %.preheader270
	movq	14160(%rax), %rax
	movq	216(%rsp), %rbx         # 8-byte Reload
	movq	(%rax,%rbx,8), %rax
	movq	208(%rsp), %rdi         # 8-byte Reload
	movq	(%rax,%rdi,8), %rax
	movq	(%rax), %rax
	movq	cofAC4x4(%rip), %rcx
	movq	(%rcx), %rdx
	movq	64(%rdx), %rsi
	movq	%rsi, 64(%rax)
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movups	32(%rdx), %xmm2
	movups	48(%rdx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax,%rdi,8), %rax
	movq	8(%rax), %rax
	movq	8(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movupd	(%rcx), %xmm0
	movdqu	16(%rcx), %xmm1
	movdqu	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movdqu	%xmm2, 32(%rax)
	movdqu	%xmm1, 16(%rax)
	movupd	%xmm0, (%rax)
	movslq	%ebp, %rax
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	160(%rsp), %rdx
	movq	%rdx, (%rcx,%r9,2)
	movq	img(%rip), %rcx
	movq	%r12, %rdx
	shlq	$5, %rdx
	addq	%rcx, %rdx
	shlq	$9, %rax
	movq	208(%rcx,%rax), %rsi
	movq	%rsi, 12624(%rdx,%r14,2)
	cmpl	$3, 20(%rcx)
	jne	.LBB5_52
# BB#50:                                # %.preheader270
	movl	sp2_frame_indicator(%rip), %ecx
	orl	si_frame_indicator(%rip), %ecx
	jne	.LBB5_52
# BB#51:
	movq	lrec(%rip), %rcx
	movq	(%rcx,%r10,8), %rcx
	movapd	304(%rsp), %xmm0
	movupd	%xmm0, (%rcx,%r9,4)
.LBB5_52:
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %rcx
	movq	8(%rcx,%r10,8), %rcx
	movq	168(%rsp), %rdx
	movq	%rdx, (%rcx,%r9,2)
	movq	img(%rip), %rcx
	movq	48(%rsp), %rdx          # 8-byte Reload
	shlq	$5, %rdx
	addq	%rcx, %rdx
	movq	240(%rcx,%rax), %rsi
	movq	%rsi, 12624(%rdx,%r14,2)
	cmpl	$3, 20(%rcx)
	jne	.LBB5_55
# BB#53:
	movl	sp2_frame_indicator(%rip), %ecx
	orl	si_frame_indicator(%rip), %ecx
	jne	.LBB5_55
# BB#54:
	movq	lrec(%rip), %rcx
	movq	8(%rcx,%r10,8), %rcx
	movapd	320(%rsp), %xmm0
	movupd	%xmm0, (%rcx,%r9,4)
.LBB5_55:
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %rcx
	movq	16(%rcx,%r10,8), %rcx
	movq	176(%rsp), %rdx
	movq	%rdx, (%rcx,%r9,2)
	movq	img(%rip), %rcx
	movq	56(%rsp), %rdx          # 8-byte Reload
	shlq	$5, %rdx
	addq	%rcx, %rdx
	movq	272(%rcx,%rax), %rsi
	movq	%rsi, 12624(%rdx,%r14,2)
	cmpl	$3, 20(%rcx)
	jne	.LBB5_58
# BB#56:
	movl	sp2_frame_indicator(%rip), %ecx
	orl	si_frame_indicator(%rip), %ecx
	jne	.LBB5_58
# BB#57:
	movq	lrec(%rip), %rcx
	movq	16(%rcx,%r10,8), %rcx
	movapd	336(%rsp), %xmm0
	movupd	%xmm0, (%rcx,%r9,4)
.LBB5_58:
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %rcx
	movq	24(%rcx,%r10,8), %rcx
	movq	184(%rsp), %rdx
	movq	%rdx, (%rcx,%r9,2)
	movq	img(%rip), %rcx
	movq	64(%rsp), %rdx          # 8-byte Reload
	shlq	$5, %rdx
	addq	%rcx, %rdx
	movq	304(%rcx,%rax), %rax
	movq	%rax, 12624(%rdx,%r14,2)
	cmpl	$3, 20(%rcx)
	jne	.LBB5_61
# BB#59:
	movl	sp2_frame_indicator(%rip), %eax
	orl	si_frame_indicator(%rip), %eax
	jne	.LBB5_61
# BB#60:
	movq	lrec(%rip), %rax
	movq	24(%rax,%r10,8), %rax
	movapd	352(%rsp), %xmm0
	movupd	%xmm0, (%rax,%r9,4)
	movq	img(%rip), %rcx
.LBB5_61:
	cmpl	$0, 15260(%rcx)
	je	.LBB5_63
# BB#62:                                # %.preheader268
	movq	14176(%rcx), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	fadjust4x4(%rip), %rcx
	movq	(%rcx,%r12,8), %rdx
	movups	(%rdx,%r14,4), %xmm0
	movups	%xmm0, (%rax,%r14,4)
	movq	img(%rip), %rax
	movq	14176(%rax), %rax
	movq	8(%rax), %rax
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	(%rcx,%rdx,8), %rdx
	movups	(%rdx,%r14,4), %xmm0
	movups	%xmm0, (%rax,%r14,4)
	movq	img(%rip), %rax
	movq	14176(%rax), %rax
	movq	8(%rax), %rax
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	(%rcx,%rdx,8), %rdx
	movups	(%rdx,%r14,4), %xmm0
	movups	%xmm0, (%rax,%r14,4)
	movq	img(%rip), %rax
	movq	14176(%rax), %rax
	movq	8(%rax), %rax
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	(%rcx,%rdx,8), %rcx
	movupd	(%rcx,%r14,4), %xmm0
	movupd	%xmm0, (%rax,%r14,4)
.LBB5_63:                               # %.loopexit
	movl	20(%rsp), %eax          # 4-byte Reload
	jmp	.LBB5_67
.LBB5_64:                               # %.preheader
	movslq	%ebp, %rcx
	shlq	$9, %rcx
	leaq	214(%rax,%rcx), %rcx
	shlq	$3, %r8
	addq	imgY_org(%rip), %r8
	shlq	$5, %r12
	leaq	(%r12,%r14,2), %rdx
	leaq	12630(%rax,%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_65:                               # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rsi), %rdi
	movzwl	-6(%rcx,%rsi,4), %ebp
	movw	%bp, -6(%rdx,%rsi,4)
	movzwl	(%rdi,%r13,2), %ebp
	movzwl	-6(%rcx,%rsi,4), %ebx
	subl	%ebx, %ebp
	movl	%ebp, 13136(%rax,%rsi,8)
	movzwl	-4(%rcx,%rsi,4), %ebp
	movw	%bp, -4(%rdx,%rsi,4)
	movzwl	2(%rdi,%r13,2), %ebp
	movzwl	-4(%rcx,%rsi,4), %ebx
	subl	%ebx, %ebp
	movl	%ebp, 13140(%rax,%rsi,8)
	movzwl	-2(%rcx,%rsi,4), %ebp
	movw	%bp, -2(%rdx,%rsi,4)
	movzwl	4(%rdi,%r13,2), %ebp
	movzwl	-2(%rcx,%rsi,4), %ebx
	subl	%ebx, %ebp
	movl	%ebp, 13144(%rax,%rsi,8)
	movzwl	(%rcx,%rsi,4), %ebp
	movw	%bp, (%rdx,%rsi,4)
	movzwl	6(%rdi,%r13,2), %edi
	movzwl	(%rcx,%rsi,4), %ebp
	subl	%ebp, %edi
	movl	%edi, 13148(%rax,%rsi,8)
	addq	$8, %rsi
	cmpq	$32, %rsi
	jne	.LBB5_65
# BB#66:
	leaq	300(%rsp), %rdx
	movl	$1, %ecx
	movq	200(%rsp), %rdi         # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	128(%rsp), %esi         # 4-byte Reload
	callq	dct_luma
.LBB5_67:                               # %.loopexit
	addq	$376, %rsp              # imm = 0x178
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	Mode_Decision_for_4x4IntraBlocks, .Lfunc_end5-Mode_Decision_for_4x4IntraBlocks
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4618441417868443648     # double 6
.LCPI6_1:
	.quad	4602677017732795964     # double 0.49990000000000001
	.text
	.globl	Mode_Decision_for_8x8IntraBlocks
	.p2align	4, 0x90
	.type	Mode_Decision_for_8x8IntraBlocks,@function
Mode_Decision_for_8x8IntraBlocks:       # @Mode_Decision_for_8x8IntraBlocks
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi59:
	.cfi_def_cfa_offset 80
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movapd	%xmm0, %xmm1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movl	%edi, %r12d
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	addsd	.LCPI6_1(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, (%r13)
	leaq	12(%rsp), %r14
	xorl	%esi, %esi
	movl	%r12d, %edi
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%r14, %rdx
	callq	Mode_Decision_for_4x4IntraBlocks
	movl	%eax, %r15d
	movl	12(%rsp), %eax
	addl	%eax, (%r13)
	movl	$1, %esi
	movl	%r12d, %edi
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%r14, %rdx
	callq	Mode_Decision_for_4x4IntraBlocks
	movl	%eax, %ebp
	orl	%r15d, %ebp
	movl	12(%rsp), %eax
	addl	%eax, (%r13)
	movl	$2, %esi
	movl	%r12d, %edi
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%r14, %rdx
	callq	Mode_Decision_for_4x4IntraBlocks
	movl	%eax, %ebx
	orl	%ebp, %ebx
	movl	12(%rsp), %eax
	addl	%eax, (%r13)
	movl	$3, %esi
	movl	%r12d, %edi
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%r14, %rdx
	callq	Mode_Decision_for_4x4IntraBlocks
	xorl	%ecx, %ecx
	orl	%ebx, %eax
	setne	%cl
	movl	12(%rsp), %eax
	addl	%eax, (%r13)
	movl	%ecx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	Mode_Decision_for_8x8IntraBlocks, .Lfunc_end6-Mode_Decision_for_8x8IntraBlocks
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_0:
	.quad	4618441417868443648     # double 6
.LCPI7_1:
	.quad	4602677017732795964     # double 0.49990000000000001
	.text
	.globl	Mode_Decision_for_Intra4x4Macroblock
	.p2align	4, 0x90
	.type	Mode_Decision_for_Intra4x4Macroblock,@function
Mode_Decision_for_Intra4x4Macroblock:   # @Mode_Decision_for_Intra4x4Macroblock
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi72:
	.cfi_def_cfa_offset 96
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movl	$0, (%rdi)
	movsd	.LCPI7_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	mulsd	%xmm0, %xmm1
	addsd	.LCPI7_1(%rip), %xmm1
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	leaq	12(%rsp), %r15
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	floor
	cvttsd2si	%xmm0, %r14d
	xorl	%esi, %esi
	movl	%ebx, %edi
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%r15, %rdx
	callq	Mode_Decision_for_4x4IntraBlocks
	movl	%eax, %r12d
	addl	12(%rsp), %r14d
	movl	$1, %esi
	movl	%ebx, %edi
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%r15, %rdx
	callq	Mode_Decision_for_4x4IntraBlocks
	movl	%eax, %ebp
	orl	%r12d, %ebp
	addl	12(%rsp), %r14d
	movl	$2, %esi
	movl	%ebx, %edi
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%r15, %rdx
	callq	Mode_Decision_for_4x4IntraBlocks
	movl	%eax, %r12d
	orl	%ebp, %r12d
	addl	12(%rsp), %r14d
	movl	$3, %esi
	movl	%ebx, %edi
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%r15, %rdx
	callq	Mode_Decision_for_4x4IntraBlocks
	addl	12(%rsp), %r14d
	movl	$1, %edx
	movl	%ebx, %ecx
	shll	%cl, %edx
	orl	%r12d, %eax
	movl	$0, %eax
	cmovel	%eax, %edx
	orl	%edx, %r13d
	movq	32(%rsp), %rax          # 8-byte Reload
	addl	%r14d, (%rax)
	incl	%ebx
	cmpl	$4, %ebx
	jne	.LBB7_1
# BB#2:
	movl	%r13d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	Mode_Decision_for_Intra4x4Macroblock, .Lfunc_end7-Mode_Decision_for_Intra4x4Macroblock
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_0:
	.quad	4906019910204099648     # double 1.0E+20
	.text
	.globl	RDCost_for_8x8blocks
	.p2align	4, 0x90
	.type	RDCost_for_8x8blocks,@function
RDCost_for_8x8blocks:                   # @RDCost_for_8x8blocks
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi82:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi83:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi85:
	.cfi_def_cfa_offset 304
.Lcfi86:
	.cfi_offset %rbx, -56
.Lcfi87:
	.cfi_offset %r12, -48
.Lcfi88:
	.cfi_offset %r13, -40
.Lcfi89:
	.cfi_offset %r14, -32
.Lcfi90:
	.cfi_offset %r15, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movl	%r9d, 32(%rsp)          # 4-byte Spill
	movl	%r8d, %r13d
	movl	%edx, %r15d
	movsd	%xmm0, 136(%rsp)        # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movl	$0, 52(%rsp)
	movl	$0, 48(%rsp)
	leal	(,%r15,8), %ebx
	andl	$8, %ebx
	movq	%rbx, 72(%rsp)          # 8-byte Spill
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	shrl	$2, %ebx
	movl	%r15d, %ebp
	andl	$-2, %ebp
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	sete	%al
	testl	%ecx, %ecx
	sete	%r14b
	andb	%al, %r14b
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	movl	%ecx, %edi
	movl	%r13d, %esi
	callq	B8Mode2Value
	movl	%eax, %edi
	movq	img(%rip), %rcx
	movq	14224(%rcx), %rdx
	movslq	12(%rcx), %rax
	imulq	$536, %rax, %rsi        # imm = 0x218
	movq	14216(%rcx), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	input(%rip), %rax
	movslq	4016(%rax), %rax
	movq	assignSE2partition(,%rax,8), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	movw	$0, 480(%rdx,%rsi)
	movb	%r14b, 23(%rsp)         # 1-byte Spill
	cmpb	$1, %r14b
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	%r13, 128(%rsp)         # 8-byte Spill
	jne	.LBB8_3
# BB#1:
	movq	direct_pdir(%rip), %rdx
	movslq	172(%rcx), %rsi
	movslq	%ebp, %rax
	addq	%rsi, %rax
	movq	(%rdx,%rax,8), %rsi
	movslq	168(%rcx), %rcx
	movslq	%ebx, %rdx
	addq	%rcx, %rdx
	movsbl	(%rsi,%rdx), %ecx
	testw	%cx, %cx
	js	.LBB8_34
# BB#2:
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	direct_ref_idx(%rip), %rsi
	movq	(%rsi), %rdi
	movq	8(%rsi), %rsi
	movq	(%rdi,%rax,8), %rdi
	movsbl	(%rdi,%rdx), %edi
	xorl	%r8d, %r8d
	testl	%edi, %edi
	cmovsl	%r8d, %edi
	movq	(%rsi,%rax,8), %rax
	movsbl	(%rax,%rdx), %eax
	movl	%eax, 8(%rsp)
	movl	%edi, (%rsp)
	movswl	%cx, %ecx
	leaq	48(%rsp), %rdi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	jmp	.LBB8_7
.LBB8_3:
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movzwl	%r13w, %eax
	cmpl	$2, %eax
	jne	.LBB8_6
# BB#4:
	movq	active_pps(%rip), %rax
	cmpl	$1, 196(%rax)
	jne	.LBB8_6
# BB#5:
	movq	wbp_weight(%rip), %rax
	movswq	32(%rsp), %rcx          # 2-byte Folded Reload
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	(%rdx,%rcx,8), %rdx
	movswq	304(%rsp), %rsi
	movq	(%rdx,%rsi,8), %rdx
	movl	(%rdx), %edx
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%rsi,8), %rax
	movl	(%rax), %eax
	leal	128(%rdx,%rax), %eax
	cmpl	$255, %eax
	ja	.LBB8_34
.LBB8_6:
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movl	%r13d, %eax
	orl	$2, %eax
	movzwl	%ax, %eax
	xorl	%r9d, %r9d
	cmpl	$2, %eax
	movl	$0, %r8d
	movl	24(%rsp), %ecx          # 4-byte Reload
	cmovel	%ecx, %r8d
	leal	-1(%r13), %eax
	movzwl	%ax, %eax
	cmpl	$2, %eax
	cmovbl	%ecx, %r9d
	movswl	304(%rsp), %eax
	movl	%eax, 8(%rsp)
	movswl	32(%rsp), %eax          # 2-byte Folded Reload
	movl	%eax, (%rsp)
	movswl	%r13w, %ecx
	leaq	48(%rsp), %rdi
.LBB8_7:
	movq	%r12, %rsi
	movl	%r15d, %edx
	callq	LumaResidualCoding8x8
	leal	(,%r15,4), %r12d
	andl	$-8, %r12d
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movq	input(%rip), %rsi
	cmpl	$3, 4168(%rsi)
	movq	%r15, 176(%rsp)         # 8-byte Spill
	jne	.LBB8_22
# BB#8:
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	je	.LBB8_10
# BB#9:
	movl	$-1, %esi
	movl	%r15d, %edi
	callq	compute_residue_b8block
	movq	input(%rip), %rsi
	cmpl	$3, 4168(%rsi)
	jne	.LBB8_22
.LBB8_10:                               # %.thread
	movq	img(%rip), %rcx
	cmpl	$1, 20(%rcx)
	je	.LBB8_23
# BB#11:                                # %.preheader194
	movl	4728(%rsi), %eax
	testl	%eax, %eax
	jle	.LBB8_35
# BB#12:                                # %.lr.ph214
	movswl	32(%rsp), %eax          # 2-byte Folded Reload
	movl	%eax, 124(%rsp)         # 4-byte Spill
	leal	8(%r12), %eax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	8(%rax), %eax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	movq	%r12, 200(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB8_13:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_15 Depth 2
                                        #       Child Loop BB8_19 Depth 3
	movl	$8, %esi
	movl	%r14d, %edi
	movl	%r15d, %edx
	movl	24(%rsp), %ecx          # 4-byte Reload
	movl	124(%rsp), %r8d         # 4-byte Reload
	callq	decode_one_b8block
	movq	img(%rip), %rdx
	movl	196(%rdx), %eax
	movq	192(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%rax), %edi
	addl	%r12d, %eax
	cmpl	%edi, %eax
	jge	.LBB8_21
# BB#14:                                # %.lr.ph209
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movl	192(%rdx), %ecx
	movq	184(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rcx), %r12d
	movq	imgY_org(%rip), %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	decs(%rip), %rdx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movslq	%eax, %rsi
	movslq	%edi, %r10
	addl	72(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rax
	movslq	%r12d, %rbp
	movl	%ebp, %r11d
	subl	%eax, %r11d
	leaq	-1(%rbp), %rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	andl	$1, %r11d
	leaq	1(%rax), %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB8_15:                               #   Parent Loop BB8_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_19 Depth 3
	cmpl	%r12d, %ecx
	jge	.LBB8_20
# BB#16:                                # %.lr.ph203
                                        #   in Loop: Header=BB8_15 Depth=2
	testq	%r11, %r11
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%rsi,8), %r13
	movq	96(%rsp), %rdx          # 8-byte Reload
	movq	8(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%rsi,8), %r8
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	14232(%rdx), %r9
	jne	.LBB8_18
# BB#17:                                #   in Loop: Header=BB8_15 Depth=2
	movq	%rax, %r15
	cmpq	%rax, 88(%rsp)          # 8-byte Folded Reload
	jne	.LBB8_19
	jmp	.LBB8_20
	.p2align	4, 0x90
.LBB8_18:                               #   in Loop: Header=BB8_15 Depth=2
	movzwl	(%r13,%rax,2), %edx
	movzwl	(%r8,%rax,2), %edi
	subq	%rdi, %rdx
	movslq	(%r9,%rdx,4), %rdx
	addq	%rdx, %rbx
	movq	80(%rsp), %r15          # 8-byte Reload
	cmpq	%rax, 88(%rsp)          # 8-byte Folded Reload
	je	.LBB8_20
	.p2align	4, 0x90
.LBB8_19:                               #   Parent Loop BB8_13 Depth=1
                                        #     Parent Loop BB8_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	(%r13,%r15,2), %edx
	movzwl	(%r8,%r15,2), %edi
	subq	%rdi, %rdx
	movslq	(%r9,%rdx,4), %rdx
	addq	%rbx, %rdx
	movzwl	2(%r13,%r15,2), %edi
	movzwl	2(%r8,%r15,2), %ebx
	subq	%rbx, %rdi
	movslq	(%r9,%rdi,4), %rbx
	addq	%rdx, %rbx
	addq	$2, %r15
	cmpq	%rbp, %r15
	jl	.LBB8_19
.LBB8_20:                               # %._crit_edge204
                                        #   in Loop: Header=BB8_15 Depth=2
	incq	%rsi
	cmpq	%r10, %rsi
	jl	.LBB8_15
.LBB8_21:                               # %._crit_edge210
                                        #   in Loop: Header=BB8_13 Depth=1
	incq	%r14
	movq	input(%rip), %rsi
	movslq	4728(%rsi), %rax
	cmpq	%rax, %r14
	movq	176(%rsp), %r15         # 8-byte Reload
	movq	200(%rsp), %r12         # 8-byte Reload
	jl	.LBB8_13
	jmp	.LBB8_36
.LBB8_22:                               # %..preheader_crit_edge
	movq	img(%rip), %rcx
.LBB8_23:                               # %.preheader
	movl	176(%rcx), %eax
	movq	72(%rsp), %rdx          # 8-byte Reload
	leal	(%rax,%rdx), %ebx
	leal	8(%rax,%rdx), %r10d
	cmpl	%r10d, %ebx
	jge	.LBB8_32
# BB#24:                                # %.preheader.split.us.preheader
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	imgY_org(%rip), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	enc_picture(%rip), %rdx
	movslq	%ebx, %rsi
	movq	6440(%rdx), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	14232(%rcx), %rbp
	movslq	196(%rcx), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movslq	180(%rcx), %r13
	movslq	%r10d, %rdx
	movl	%r12d, %r9d
	orl	$7, %r9d
	subl	%ebx, %r10d
	leaq	-1(%rdx), %r15
	andl	$1, %r10d
	leaq	1(%rsi), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB8_25:                               # %.preheader.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_29 Depth 2
	movq	%r12, %r11
	movslq	%r12d, %rbx
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx), %rcx
	addq	%r13, %rbx
	testq	%r10, %r10
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %r12
	jne	.LBB8_27
# BB#26:                                #   in Loop: Header=BB8_25 Depth=1
	movq	%rsi, %r8
	movq	%r14, %rbx
                                        # implicit-def: %R14
	cmpq	%rsi, %r15
	jne	.LBB8_28
	jmp	.LBB8_30
	.p2align	4, 0x90
.LBB8_27:                               #   in Loop: Header=BB8_25 Depth=1
	movzwl	(%rcx,%rsi,2), %ebx
	movzwl	(%r12,%rsi,2), %edi
	subq	%rdi, %rbx
	movslq	(%rbp,%rbx,4), %rbx
	addq	%r14, %rbx
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %r14
	cmpq	%rsi, %r15
	je	.LBB8_30
.LBB8_28:                               # %.preheader.split.us.new
                                        #   in Loop: Header=BB8_25 Depth=1
	movq	%rbx, %r14
	.p2align	4, 0x90
.LBB8_29:                               #   Parent Loop BB8_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rcx,%r8,2), %edi
	movzwl	(%r12,%r8,2), %ebx
	subq	%rbx, %rdi
	movslq	(%rbp,%rdi,4), %rdi
	addq	%r14, %rdi
	movzwl	2(%rcx,%r8,2), %ebx
	movzwl	2(%r12,%r8,2), %eax
	subq	%rax, %rbx
	movslq	(%rbp,%rbx,4), %r14
	addq	%rdi, %r14
	addq	$2, %r8
	cmpq	%rdx, %r8
	jl	.LBB8_29
.LBB8_30:                               # %._crit_edge.us
                                        #   in Loop: Header=BB8_25 Depth=1
	cmpl	%r9d, %r11d
	leal	1(%r11), %eax
	movl	%eax, %r12d
	jl	.LBB8_25
# BB#31:
	movq	176(%rsp), %r15         # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	movl	28(%rsp), %edi          # 4-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	cmpl	$0, 4008(%rsi)
	jne	.LBB8_37
	jmp	.LBB8_38
.LBB8_32:
	xorl	%r14d, %r14d
	movq	56(%rsp), %rbx          # 8-byte Reload
	movl	28(%rsp), %edi          # 4-byte Reload
	cmpl	$0, 4008(%rsi)
	jne	.LBB8_37
	jmp	.LBB8_38
.LBB8_34:
	movsd	.LCPI8_0(%rip), %xmm0   # xmm0 = mem[0],zero
	jmp	.LBB8_56
.LBB8_35:
	xorl	%ebx, %ebx
.LBB8_36:
	movl	28(%rsp), %edi          # 4-byte Reload
	movslq	%eax, %rcx
	movq	%rbx, %rax
	cqto
	idivq	%rcx
	movq	%rax, %r14
	movq	56(%rsp), %rbx          # 8-byte Reload
	cmpl	$0, 4008(%rsi)
	je	.LBB8_38
.LBB8_37:
	movl	%edi, 212(%rsp)
	movl	$2, 208(%rsp)
	movq	144(%rsp), %rax         # 8-byte Reload
	movslq	8(%rax), %rax
	imulq	$104, %rax, %rsi
	movq	152(%rsp), %rax         # 8-byte Reload
	addq	24(%rax), %rsi
	leaq	208(%rsp), %rdi
	callq	*writeB8_typeInfo(%rip)
	movl	220(%rsp), %r12d
	jmp	.LBB8_39
.LBB8_38:
	movl	52(%rsp), %esi
	leaq	36(%rsp), %rdx
	leaq	52(%rsp), %rcx
	callq	ue_linfo
	movl	36(%rsp), %r12d
.LBB8_39:
	movq	128(%rsp), %rdi         # 8-byte Reload
	movb	23(%rsp), %al           # 1-byte Reload
	testb	%al, %al
	jne	.LBB8_51
# BB#40:
	movq	img(%rip), %rax
	cmpl	$2, 14456(%rax)
	jl	.LBB8_43
# BB#41:
	movl	%edi, %ecx
	orl	$2, %ecx
	movzwl	%cx, %ecx
	cmpl	$2, %ecx
	jne	.LBB8_43
# BB#42:
	movswl	32(%rsp), %r8d          # 2-byte Folded Reload
	movl	$1, %ecx
	movq	%rdi, %rbp
	movl	24(%rsp), %edi          # 4-byte Reload
	movl	%ebx, %esi
	movq	40(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	writeReferenceFrame
	movq	%rbp, %rdi
	addl	%eax, %r12d
	movq	img(%rip), %rax
.LBB8_43:
	cmpl	$2, 14460(%rax)
	jl	.LBB8_47
# BB#44:
	leal	-1(%rdi), %ecx
	movzwl	%cx, %ecx
	cmpl	$1, %ecx
	ja	.LBB8_47
# BB#45:
	cmpl	$1, 20(%rax)
	jne	.LBB8_47
# BB#46:
	movswl	304(%rsp), %r8d
	xorl	%ecx, %ecx
	movq	%rdi, %rbp
	movl	24(%rsp), %edi          # 4-byte Reload
	movl	%ebx, %esi
	movq	40(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	writeReferenceFrame
	movq	%rbp, %rdi
	addl	%eax, %r12d
.LBB8_47:
	movl	%edi, %eax
	orl	$2, %eax
	movzwl	%ax, %eax
	cmpl	$2, %eax
	jne	.LBB8_49
# BB#48:
	leal	2(%rbx), %edx
	movq	40(%rsp), %rsi          # 8-byte Reload
	leal	2(%rsi), %ecx
	movswl	32(%rsp), %r8d          # 2-byte Folded Reload
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	xorl	%r9d, %r9d
	movq	%rdi, %rbp
	movl	%ebx, %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	writeMotionVector8x8
	movq	%rbp, %rdi
	addl	%eax, %r12d
.LBB8_49:
	leal	-1(%rdi), %eax
	movzwl	%ax, %eax
	cmpl	$1, %eax
	ja	.LBB8_51
# BB#50:
	leal	2(%rbx), %edx
	movq	40(%rsp), %rsi          # 8-byte Reload
	leal	2(%rsi), %ecx
	movswl	304(%rsp), %r8d
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movl	$1, %r9d
	movl	%ebx, %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	writeMotionVector8x8
	addl	%eax, %r12d
.LBB8_51:
	movq	input(%rip), %rax
	cmpl	$1, 4008(%rax)
	jne	.LBB8_53
# BB#52:
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	leaq	(%rax,%rcx), %rbp
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	24(%rax), %rax
	movq	144(%rsp), %rcx         # 8-byte Reload
	movslq	24(%rcx), %rcx
	imulq	$104, %rcx, %rcx
	leaq	8(%rax,%rcx), %r13
	movq	%r13, %rdi
	callq	arienco_bits_written
	movl	%eax, 36(%rsp)
	xorl	%esi, %esi
	movq	64(%rsp), %rbx          # 8-byte Reload
	cmpl	$0, (%rbx)
	setg	%sil
	movl	cbp8x8(%rip), %edx
	movl	$1, %r8d
	movl	%r15d, %edi
	movq	%rbp, %rcx
	movq	%r13, %r9
	callq	writeCBP_BIT_CABAC
	movq	%r13, %rdi
	callq	arienco_bits_written
	subl	36(%rsp), %eax
	movl	%eax, 36(%rsp)
	addl	%eax, %r12d
	cmpl	$0, (%rbx)
	jne	.LBB8_54
	jmp	.LBB8_55
.LBB8_53:
	movq	64(%rsp), %rbx          # 8-byte Reload
	cmpl	$0, (%rbx)
	je	.LBB8_55
.LBB8_54:
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movl	472(%rax,%rcx), %edx
	movl	%r15d, %edi
	movl	24(%rsp), %esi          # 4-byte Reload
	callq	writeLumaCoeff8x8
	addl	%eax, %r12d
.LBB8_55:
	movsd	136(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
	cvtsi2sdq	%r14, %xmm1
	cvtsi2sdl	%r12d, %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
.LBB8_56:
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	RDCost_for_8x8blocks, .Lfunc_end8-RDCost_for_8x8blocks
	.cfi_endproc

	.globl	I16Offset
	.p2align	4, 0x90
	.type	I16Offset,@function
I16Offset:                              # @I16Offset
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testb	$15, %dil
	movl	$13, %eax
	movl	$1, %ecx
	cmovnel	%eax, %ecx
	shrl	$2, %edi
	andl	$12, %edi
	leal	(%rdi,%rsi), %eax
	addl	%ecx, %eax
	retq
.Lfunc_end9:
	.size	I16Offset, .Lfunc_end9-I16Offset
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
.LCPI10_1:
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	11                      # 0xb
.LCPI10_2:
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.text
	.globl	SetModesAndRefframeForBlocks
	.p2align	4, 0x90
	.type	SetModesAndRefframeForBlocks,@function
SetModesAndRefframeForBlocks:           # @SetModesAndRefframeForBlocks
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi95:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi96:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi98:
	.cfi_def_cfa_offset 80
.Lcfi99:
	.cfi_offset %rbx, -56
.Lcfi100:
	.cfi_offset %r12, -48
.Lcfi101:
	.cfi_offset %r13, -40
.Lcfi102:
	.cfi_offset %r14, -32
.Lcfi103:
	.cfi_offset %r15, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rcx
	movq	14224(%rcx), %rbp
	movslq	12(%rcx), %rax
	movl	20(%rcx), %esi
	imulq	$536, %rax, %r10        # imm = 0x218
	movl	%edi, 72(%rbp,%r10)
	cmpl	$1, %edi
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	%esi, 12(%rsp)          # 4-byte Spill
	jne	.LBB10_3
# BB#1:                                 # %.thread
	movzwl	14410(%rcx), %eax
	leaq	480(%rbp,%r10), %r14
	movw	%ax, 480(%rbp,%r10)
.LBB10_2:                               # %.loopexit249
	movslq	%edi, %rax
	movl	%edi, 376(%rbp,%r10)
	movsbl	best8x8pdir(,%rax,4), %edx
	movl	%edx, 392(%rbp,%r10)
	movl	%edi, 380(%rbp,%r10)
	movsbl	best8x8pdir+1(,%rax,4), %edx
	movl	%edx, 396(%rbp,%r10)
	movl	%edi, 384(%rbp,%r10)
	movsbl	best8x8pdir+2(,%rax,4), %edx
	movl	%edx, 400(%rbp,%r10)
	movl	%edi, 388(%rbp,%r10)
	movsbl	best8x8pdir+3(,%rax,4), %eax
	movl	%eax, 404(%rbp,%r10)
	cmpl	$1, %esi
	jne	.LBB10_28
.LBB10_48:                              # %.preheader236
	movslq	%edi, %r12
	xorl	%r11d, %r11d
	jmp	.LBB10_49
	.p2align	4, 0x90
.LBB10_76:                              # %.us-lcssa.us._crit_edge
                                        #   in Loop: Header=BB10_49 Depth=1
	movq	img(%rip), %rcx
.LBB10_49:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_50 Depth 2
                                        #     Child Loop BB10_55 Depth 2
	movslq	172(%rcx), %r13
	movslq	%r11d, %r15
	addq	%r15, %r13
	andl	$-2, %r15d
	xorl	%ebp, %ebp
	cmpl	$8, %edi
	je	.LBB10_55
	jmp	.LBB10_50
	.p2align	4, 0x90
.LBB10_68:                              # %..split.us_crit_edge
                                        #   in Loop: Header=BB10_55 Depth=2
	incl	%ebp
	movq	img(%rip), %rcx
.LBB10_55:                              # %.split.us
                                        #   Parent Loop BB10_49 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	168(%rcx), %ecx
	addl	%ebp, %ecx
	movl	%ebp, %eax
	sarl	%eax
	addl	%r15d, %eax
	movslq	%eax, %rsi
	cmpw	$0, best8x8mode(%rsi,%rsi)
	je	.LBB10_66
# BB#56:                                #   in Loop: Header=BB10_55 Depth=2
	cmpl	$1, %edi
	jne	.LBB10_61
# BB#57:                                #   in Loop: Header=BB10_55 Depth=2
	cmpw	$0, (%r14)
	je	.LBB10_61
# BB#58:                                #   in Loop: Header=BB10_55 Depth=2
	movzbl	best8x8pdir(%rsi,%r12,4), %eax
	movl	%eax, %edx
	orb	$2, %dl
	cmpb	$2, %dl
	jne	.LBB10_61
# BB#59:                                #   in Loop: Header=BB10_55 Depth=2
	decb	%al
	cmpb	$1, %al
	ja	.LBB10_61
# BB#60:                                #   in Loop: Header=BB10_55 Depth=2
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movslq	%ecx, %rcx
	movb	$0, (%rax,%rcx)
	xorl	%ebx, %ebx
	jmp	.LBB10_67
	.p2align	4, 0x90
.LBB10_61:                              #   in Loop: Header=BB10_55 Depth=2
	leaq	best8x8pdir(%rsi,%r12,4), %r9
	movzbl	best8x8pdir(%rsi,%r12,4), %edx
	movb	$-1, %bl
	orb	$2, %dl
	cmpb	$2, %dl
	movb	$-1, %dl
	jne	.LBB10_63
# BB#62:                                #   in Loop: Header=BB10_55 Depth=2
	movzbl	best8x8fwref(%rsi,%r12,4), %edx
.LBB10_63:                              #   in Loop: Header=BB10_55 Depth=2
	movq	enc_picture(%rip), %r8
	movq	6488(%r8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movslq	%ecx, %rcx
	movb	%dl, (%rax,%rcx)
	movzbl	(%r9), %eax
	decb	%al
	cmpb	$1, %al
	ja	.LBB10_67
# BB#64:                                #   in Loop: Header=BB10_55 Depth=2
	cmpw	$0, best8x8mode(%rsi,%rsi)
	je	.LBB10_67
# BB#65:                                #   in Loop: Header=BB10_55 Depth=2
	movb	best8x8bwref(%rsi,%r12,4), %bl
	jmp	.LBB10_67
	.p2align	4, 0x90
.LBB10_66:                              #   in Loop: Header=BB10_55 Depth=2
	movq	direct_ref_idx(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %eax
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r13,8), %rdx
	movb	%al, (%rdx,%rcx)
	movq	direct_ref_idx(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movb	(%rax,%rcx), %bl
.LBB10_67:                              #   in Loop: Header=BB10_55 Depth=2
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movb	%bl, (%rax,%rcx)
	cmpl	$3, %ebp
	jne	.LBB10_68
	jmp	.LBB10_75
	.p2align	4, 0x90
.LBB10_74:                              # %..split_crit_edge
                                        #   in Loop: Header=BB10_50 Depth=2
	incl	%ebp
	movq	img(%rip), %rcx
.LBB10_50:                              # %.split
                                        #   Parent Loop BB10_49 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	168(%rcx), %ecx
	addl	%ebp, %ecx
	movl	%ebp, %esi
	sarl	%esi
	addl	%r15d, %esi
	cmpl	$1, %edi
	jne	.LBB10_69
# BB#51:                                #   in Loop: Header=BB10_50 Depth=2
	cmpw	$0, (%r14)
	je	.LBB10_69
# BB#52:                                #   in Loop: Header=BB10_50 Depth=2
	movslq	%esi, %rax
	movzbl	best8x8pdir(%rax,%r12,4), %eax
	movl	%eax, %edx
	orb	$2, %dl
	cmpb	$2, %dl
	jne	.LBB10_69
# BB#53:                                #   in Loop: Header=BB10_50 Depth=2
	decb	%al
	cmpb	$1, %al
	ja	.LBB10_69
# BB#54:                                #   in Loop: Header=BB10_50 Depth=2
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movslq	%ecx, %rcx
	movb	$0, (%rax,%rcx)
	xorl	%r8d, %r8d
	jmp	.LBB10_73
	.p2align	4, 0x90
.LBB10_69:                              #   in Loop: Header=BB10_50 Depth=2
	movslq	%esi, %rbx
	leaq	best8x8pdir(%rbx,%r12,4), %rax
	movzbl	best8x8pdir(%rbx,%r12,4), %edx
	movb	$-1, %r8b
	orb	$2, %dl
	cmpb	$2, %dl
	movb	$-1, %dl
	jne	.LBB10_71
# BB#70:                                #   in Loop: Header=BB10_50 Depth=2
	movzbl	best8x8fwref(%rbx,%r12,4), %edx
.LBB10_71:                              #   in Loop: Header=BB10_50 Depth=2
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%r13,8), %rsi
	movslq	%ecx, %rcx
	movb	%dl, (%rsi,%rcx)
	movzbl	(%rax), %eax
	decb	%al
	cmpb	$1, %al
	ja	.LBB10_73
# BB#72:                                #   in Loop: Header=BB10_50 Depth=2
	movb	best8x8bwref(%rbx,%r12,4), %r8b
.LBB10_73:                              #   in Loop: Header=BB10_50 Depth=2
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movb	%r8b, (%rax,%rcx)
	cmpl	$3, %ebp
	jne	.LBB10_74
.LBB10_75:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB10_49 Depth=1
	incl	%r11d
	cmpl	$4, %r11d
	jne	.LBB10_76
	jmp	.LBB10_17
.LBB10_3:
	movw	$0, 480(%rbp,%r10)
	cmpl	$14, %edi
	ja	.LBB10_12
# BB#4:
	leaq	480(%rbp,%r10), %r14
	movl	%edi, %eax
	jmpq	*.LJTI10_0(,%rax,8)
.LBB10_9:                               # %.preheader250
	cmpl	$1, %esi
	leaq	376(%rbp,%r10), %rax
	jne	.LBB10_10
# BB#22:                                # %.loopexit249.thread.thread
	movl	$0, (%rax)
	movq	direct_pdir(%rip), %rax
	movslq	172(%rcx), %rdx
	movq	(%rax,%rdx,8), %rdx
	movslq	168(%rcx), %rsi
	movsbl	(%rdx,%rsi), %edx
	movl	%edx, 392(%rbp,%r10)
	movl	$0, 380(%rbp,%r10)
	movslq	172(%rcx), %rdx
	movq	(%rax,%rdx,8), %rdx
	movslq	168(%rcx), %rsi
	movsbl	2(%rdx,%rsi), %edx
	movl	%edx, 396(%rbp,%r10)
	movl	$0, 384(%rbp,%r10)
	movslq	172(%rcx), %rdx
	movq	16(%rax,%rdx,8), %rdx
	movslq	168(%rcx), %rsi
	movsbl	(%rdx,%rsi), %edx
	movl	%edx, 400(%rbp,%r10)
	movl	$0, 388(%rbp,%r10)
	movslq	172(%rcx), %rdx
	movq	16(%rax,%rdx,8), %rax
	movslq	168(%rcx), %rdx
	movsbl	2(%rax,%rdx), %eax
	movl	%eax, 404(%rbp,%r10)
	movslq	172(%rcx), %rax
	decq	%rax
	.p2align	4, 0x90
.LBB10_23:                              # %.lr.ph284
                                        # =>This Inner Loop Header: Depth=1
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	8(%rdx,%rax,8), %rdx
	movslq	168(%rcx), %rcx
	movq	direct_ref_idx(%rip), %rsi
	movq	(%rsi), %rsi
	movq	8(%rsi,%rax,8), %rsi
	movl	(%rsi,%rcx), %esi
	movl	%esi, (%rdx,%rcx)
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	8(%rcx,%rax,8), %rcx
	movq	img(%rip), %rdx
	movslq	168(%rdx), %rdx
	movq	direct_ref_idx(%rip), %rsi
	movq	8(%rsi), %rsi
	movq	8(%rsi,%rax,8), %rsi
	movl	(%rsi,%rdx), %esi
	movl	%esi, (%rcx,%rdx)
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdx
	addq	$3, %rdx
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB10_23
	jmp	.LBB10_17
.LBB10_8:                               # %.preheader253
	movq	best8x8mode(%rip), %xmm0 # xmm0 = mem[0],zero
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	movdqu	%xmm0, 376(%rbp,%r10)
	movd	best8x8pdir+32(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$24, %xmm0
	movdqu	%xmm0, 392(%rbp,%r10)
	cmpl	$1, %esi
	je	.LBB10_48
	jmp	.LBB10_28
.LBB10_7:                               # %.preheader255.preheader
	movdqa	.LCPI10_1(%rip), %xmm0  # xmm0 = [11,11,11,11]
	jmp	.LBB10_6
.LBB10_5:                               # %.preheader257.preheader
	pxor	%xmm0, %xmm0
.LBB10_6:                               # %.loopexit249.thread
	movdqu	%xmm0, 376(%rbp,%r10)
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 392(%rbp,%r10)
	jmp	.LBB10_14
.LBB10_13:                              # %.preheader259.preheader
	movdqa	.LCPI10_0(%rip), %xmm0  # xmm0 = [13,13,13,13]
	movdqu	%xmm0, 376(%rbp,%r10)
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 392(%rbp,%r10)
	movl	$1, 472(%rbp,%r10)
.LBB10_14:                              # %.loopexit249.thread
	cmpl	$1, %esi
	movslq	172(%rcx), %rax
	jne	.LBB10_15
# BB#24:                                # %.lr.ph282.preheader
	decq	%rax
	.p2align	4, 0x90
.LBB10_25:                              # %.lr.ph282
                                        # =>This Inner Loop Header: Depth=1
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	8(%rdx,%rax,8), %rdx
	movslq	168(%rcx), %rcx
	movl	$-1, (%rdx,%rcx)
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	8(%rcx,%rax,8), %rcx
	movq	img(%rip), %rdx
	movslq	168(%rdx), %rdx
	movl	$-1, (%rcx,%rdx)
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdx
	addq	$3, %rdx
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB10_25
	jmp	.LBB10_17
.LBB10_15:                              # %.lr.ph286.preheader
	decq	%rax
	.p2align	4, 0x90
.LBB10_16:                              # %.lr.ph286
                                        # =>This Inner Loop Header: Depth=1
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	8(%rdx,%rax,8), %rdx
	movslq	168(%rcx), %rcx
	movl	$-1, (%rdx,%rcx)
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdx
	addq	$3, %rdx
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB10_16
	jmp	.LBB10_17
.LBB10_26:                              # %.preheader252.preheader
	movdqa	.LCPI10_2(%rip), %xmm0  # xmm0 = [14,14,14,14]
	movdqu	%xmm0, 376(%rbp,%r10)
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 392(%rbp,%r10)
	movl	$0, 472(%rbp,%r10)
	cmpl	$1, %esi
	je	.LBB10_48
.LBB10_28:                              # %.preheader238
	movslq	%edi, %rax
	xorl	%edx, %edx
	cmpl	$8, %edi
	je	.LBB10_39
	jmp	.LBB10_29
	.p2align	4, 0x90
.LBB10_47:                              # %.us-lcssa273.us.us..preheader238.split.us_crit_edge
                                        #   in Loop: Header=BB10_39 Depth=1
	incl	%edx
	movq	img(%rip), %rcx
.LBB10_39:                              # %.split272.us..split272.us.split_crit_edge.us.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	172(%rcx), %esi
	addl	%edx, %esi
	movslq	%esi, %rsi
	movl	%edx, %edi
	andl	$-2, %edi
	movslq	168(%rcx), %r14
	movslq	%edi, %rdi
	leaq	best8x8pdir(%rdi,%rax,4), %r11
	movzbl	best8x8pdir(%rdi,%rax,4), %ebx
	movb	$-1, %r15b
	orb	$2, %bl
	cmpb	$2, %bl
	movb	$-1, %bl
	jne	.LBB10_41
# BB#40:                                #   in Loop: Header=BB10_39 Depth=1
	movzbl	best8x8fwref(%rdi,%rax,4), %ebx
.LBB10_41:                              # %.split272.us..split272.us.split_crit_edge.us.1319
                                        #   in Loop: Header=BB10_39 Depth=1
	movq	enc_picture(%rip), %rbp
	movq	6488(%rbp), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%rsi,8), %rbp
	movb	%bl, (%rbp,%r14)
	movq	img(%rip), %rbx
	movslq	168(%rbx), %rbx
	movzbl	(%r11), %ecx
	orb	$2, %cl
	cmpb	$2, %cl
	jne	.LBB10_43
# BB#42:                                #   in Loop: Header=BB10_39 Depth=1
	movzbl	best8x8fwref(%rdi,%rax,4), %r15d
.LBB10_43:                              # %.split272.us..split272.us.split_crit_edge.us.2320
                                        #   in Loop: Header=BB10_39 Depth=1
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movb	%r15b, 1(%rcx,%rbx)
	movq	img(%rip), %rcx
	movslq	168(%rcx), %r8
	movl	%edx, %ecx
	orl	$1, %ecx
	movslq	%ecx, %rdi
	leaq	best8x8pdir(%rdi,%rax,4), %r11
	movzbl	best8x8pdir(%rdi,%rax,4), %ebx
	movb	$-1, %cl
	orb	$2, %bl
	cmpb	$2, %bl
	movb	$-1, %bl
	jne	.LBB10_45
# BB#44:                                #   in Loop: Header=BB10_39 Depth=1
	movzbl	best8x8fwref(%rdi,%rax,4), %ebx
.LBB10_45:                              # %.split272.us..split272.us.split_crit_edge.us.3321
                                        #   in Loop: Header=BB10_39 Depth=1
	movq	enc_picture(%rip), %rbp
	movq	6488(%rbp), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%rsi,8), %rbp
	movb	%bl, 2(%rbp,%r8)
	movq	img(%rip), %rbp
	movslq	168(%rbp), %rbp
	movzbl	(%r11), %ebx
	orb	$2, %bl
	cmpb	$2, %bl
	jne	.LBB10_46
# BB#89:                                #   in Loop: Header=BB10_39 Depth=1
	movzbl	best8x8fwref(%rdi,%rax,4), %ecx
.LBB10_46:                              # %.us-lcssa273.us.us
                                        #   in Loop: Header=BB10_39 Depth=1
	movq	enc_picture(%rip), %rdi
	movq	6488(%rdi), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi,%rsi,8), %rsi
	movb	%cl, 3(%rsi,%rbp)
	cmpl	$3, %edx
	jne	.LBB10_47
	jmp	.LBB10_17
	.p2align	4, 0x90
.LBB10_38:                              # %.us-lcssa273330..preheader238.split_crit_edge
                                        #   in Loop: Header=BB10_29 Depth=1
	incl	%edx
	movq	img(%rip), %rcx
.LBB10_29:                              # %.preheader238.split
                                        # =>This Inner Loop Header: Depth=1
	movl	172(%rcx), %esi
	addl	%edx, %esi
	movl	%edx, %edi
	andl	$-2, %edi
	movslq	%esi, %rsi
	movslq	168(%rcx), %r8
	movslq	%edi, %rdi
	leaq	best8x8pdir(%rdi,%rax,4), %r11
	movzbl	best8x8pdir(%rdi,%rax,4), %ebx
	movb	$-1, %r9b
	orb	$2, %bl
	cmpb	$2, %bl
	movb	$-1, %bl
	jne	.LBB10_31
# BB#30:                                #   in Loop: Header=BB10_29 Depth=1
	movzbl	best8x8fwref(%rdi,%rax,4), %ebx
.LBB10_31:                              #   in Loop: Header=BB10_29 Depth=1
	movq	enc_picture(%rip), %rbp
	movq	6488(%rbp), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%rsi,8), %rbp
	movb	%bl, (%rbp,%r8)
	movq	img(%rip), %rbp
	movslq	168(%rbp), %rbx
	movzbl	(%r11), %ecx
	orb	$2, %cl
	cmpb	$2, %cl
	jne	.LBB10_33
# BB#32:                                #   in Loop: Header=BB10_29 Depth=1
	movzbl	best8x8fwref(%rdi,%rax,4), %r9d
.LBB10_33:                              #   in Loop: Header=BB10_29 Depth=1
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movb	%r9b, 1(%rcx,%rbx)
	movq	img(%rip), %rcx
	movslq	168(%rcx), %r8
	movl	%edx, %ecx
	orl	$1, %ecx
	movslq	%ecx, %rdi
	leaq	best8x8pdir(%rdi,%rax,4), %r11
	movzbl	best8x8pdir(%rdi,%rax,4), %ebx
	movb	$-1, %cl
	orb	$2, %bl
	cmpb	$2, %bl
	movb	$-1, %bl
	jne	.LBB10_35
# BB#34:                                #   in Loop: Header=BB10_29 Depth=1
	movzbl	best8x8fwref(%rdi,%rax,4), %ebx
.LBB10_35:                              #   in Loop: Header=BB10_29 Depth=1
	movq	enc_picture(%rip), %rbp
	movq	6488(%rbp), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%rsi,8), %rbp
	movb	%bl, 2(%rbp,%r8)
	movq	img(%rip), %rbp
	movslq	168(%rbp), %rbp
	movzbl	(%r11), %ebx
	orb	$2, %bl
	cmpb	$2, %bl
	jne	.LBB10_37
# BB#36:                                #   in Loop: Header=BB10_29 Depth=1
	movzbl	best8x8fwref(%rdi,%rax,4), %ecx
.LBB10_37:                              # %.us-lcssa273330
                                        #   in Loop: Header=BB10_29 Depth=1
	movq	enc_picture(%rip), %rdi
	movq	6488(%rdi), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi,%rsi,8), %rsi
	movb	%cl, 3(%rsi,%rbp)
	cmpl	$3, %edx
	jne	.LBB10_38
.LBB10_17:                              # %.loopexit237
	movq	img(%rip), %rcx
	movslq	172(%rcx), %r12
	movslq	168(%rcx), %r14
	leaq	3(%r14), %r15
	leaq	3(%r12), %r11
	cmpl	$1, 12(%rsp)            # 4-byte Folded Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	432(%rax,%r10), %r8
	jne	.LBB10_18
# BB#77:                                # %.lr.ph.preheader
	decq	%r14
	.p2align	4, 0x90
.LBB10_78:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_79 Depth 2
	movq	enc_picture(%rip), %rbp
	movq	6488(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movq	(%rdx,%r12,8), %r9
	movq	(%rcx,%r12,8), %r10
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_79:                              #   Parent Loop BB10_78 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	1(%r9,%rcx), %rdx
	testq	%rdx, %rdx
	movsbq	1(%r10,%rcx), %rdi
	movq	$-1, %rbx
	movq	$-1, %rsi
	js	.LBB10_81
# BB#80:                                #   in Loop: Header=BB10_79 Depth=2
	movslq	(%r8), %rsi
	imulq	$264, %rsi, %rsi        # imm = 0x108
	addq	%rbp, %rsi
	movq	24(%rsi,%rdx,8), %rsi
.LBB10_81:                              #   in Loop: Header=BB10_79 Depth=2
	movq	6496(%rbp), %rdx
	movq	(%rdx), %rax
	movq	(%rax,%r12,8), %rax
	movq	%rsi, 8(%rax,%rcx,8)
	testb	%dil, %dil
	js	.LBB10_83
# BB#82:                                #   in Loop: Header=BB10_79 Depth=2
	movslq	(%r8), %rax
	imulq	$264, %rax, %rax        # imm = 0x108
	addq	%rbp, %rax
	movq	288(%rax,%rdi,8), %rbx
.LBB10_83:                              #   in Loop: Header=BB10_79 Depth=2
	movq	8(%rdx), %rax
	movq	(%rax,%r12,8), %rax
	movq	%rbx, 8(%rax,%rcx,8)
	incq	%rcx
	cmpq	%r15, %rcx
	jl	.LBB10_79
# BB#84:                                # %._crit_edge
                                        #   in Loop: Header=BB10_78 Depth=1
	cmpq	%r11, %r12
	leaq	1(%r12), %r12
	jl	.LBB10_78
	jmp	.LBB10_88
.LBB10_18:                              # %.lr.ph265.preheader
	decq	%r14
	.p2align	4, 0x90
.LBB10_19:                              # %.lr.ph265
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_20 Depth 2
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax,%r12,8), %rsi
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB10_20:                              #   Parent Loop BB10_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	1(%rsi,%rbp), %rdx
	testq	%rdx, %rdx
	js	.LBB10_21
# BB#85:                                #   in Loop: Header=BB10_20 Depth=2
	movslq	(%r8), %rax
	imulq	$264, %rax, %rax        # imm = 0x108
	addq	%rcx, %rax
	movq	24(%rax,%rdx,8), %rdx
	jmp	.LBB10_86
	.p2align	4, 0x90
.LBB10_21:                              #   in Loop: Header=BB10_20 Depth=2
	movq	$-1, %rdx
.LBB10_86:                              #   in Loop: Header=BB10_20 Depth=2
	movq	6496(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	%rdx, 8(%rax,%rbp,8)
	incq	%rbp
	cmpq	%r15, %rbp
	jl	.LBB10_20
# BB#87:                                # %._crit_edge266
                                        #   in Loop: Header=BB10_19 Depth=1
	cmpq	%r11, %r12
	leaq	1(%r12), %r12
	jl	.LBB10_19
.LBB10_88:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_10:                              # %.loopexit249.thread.thread406
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 16(%rax)
	movdqu	%xmm0, (%rax)
	movslq	172(%rcx), %rax
	decq	%rax
	.p2align	4, 0x90
.LBB10_11:                              # %.lr.ph288
                                        # =>This Inner Loop Header: Depth=1
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	8(%rdx,%rax,8), %rdx
	movslq	168(%rcx), %rcx
	movl	$0, (%rdx,%rcx)
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdx
	addq	$3, %rdx
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB10_11
	jmp	.LBB10_17
.LBB10_12:
	movl	$.Lstr, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end10:
	.size	SetModesAndRefframeForBlocks, .Lfunc_end10-SetModesAndRefframeForBlocks
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI10_0:
	.quad	.LBB10_9
	.quad	.LBB10_12
	.quad	.LBB10_2
	.quad	.LBB10_2
	.quad	.LBB10_12
	.quad	.LBB10_12
	.quad	.LBB10_12
	.quad	.LBB10_12
	.quad	.LBB10_8
	.quad	.LBB10_7
	.quad	.LBB10_5
	.quad	.LBB10_12
	.quad	.LBB10_12
	.quad	.LBB10_13
	.quad	.LBB10_26

	.text
	.globl	Intra16x16_Mode_Decision
	.p2align	4, 0x90
	.type	Intra16x16_Mode_Decision,@function
Intra16x16_Mode_Decision:               # @Intra16x16_Mode_Decision
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi107:
	.cfi_def_cfa_offset 32
.Lcfi108:
	.cfi_offset %rbx, -24
.Lcfi109:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	intrapred_luma_16x16
	movq	%rbx, %rdi
	callq	find_sad_16x16
	movl	(%rbx), %edi
	callq	dct_luma_16x16
	movl	%eax, 364(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	Intra16x16_Mode_Decision, .Lfunc_end11-Intra16x16_Mode_Decision
	.cfi_endproc

	.globl	SetCoeffAndReconstruction8x8
	.p2align	4, 0x90
	.type	SetCoeffAndReconstruction8x8,@function
SetCoeffAndReconstruction8x8:           # @SetCoeffAndReconstruction8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 48
	subq	$16432, %rsp            # imm = 0x4030
.Lcfi115:
	.cfi_def_cfa_offset 16480
.Lcfi116:
	.cfi_offset %rbx, -48
.Lcfi117:
	.cfi_offset %r12, -40
.Lcfi118:
	.cfi_offset %r14, -32
.Lcfi119:
	.cfi_offset %r15, -24
.Lcfi120:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	img(%rip), %rcx
	cmpl	$0, 472(%r12)
	je	.LBB12_5
# BB#1:                                 # %.preheader165.preheader
	movl	20(%rcx), %r8d
	cmpl	$1, %r8d
	movswl	tr8x8+6148(%rip), %eax
	movl	%eax, 376(%r12)
	movsbl	tr8x8+6156(%rip), %eax
	movl	%eax, 392(%r12)
	movswl	tr8x8+6150(%rip), %eax
	movl	%eax, 380(%r12)
	movsbl	tr8x8+6157(%rip), %eax
	movl	%eax, 396(%r12)
	movswl	tr8x8+6152(%rip), %eax
	movl	%eax, 384(%r12)
	movsbl	tr8x8+6158(%rip), %eax
	movl	%eax, 400(%r12)
	movswl	tr8x8+6154(%rip), %eax
	movl	%eax, 388(%r12)
	movsbl	tr8x8+6159(%rip), %eax
	movl	%eax, 404(%r12)
	jne	.LBB12_2
# BB#18:                                # %.preheader159.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB12_19:                              # %.preheader159
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %eax
	andl	$-2, %eax
	movslq	%eax, %rdx
	movb	$-1, %bl
	testb	$1, 392(%r12,%rdx,4)
	movb	$-1, %al
	jne	.LBB12_21
# BB#20:                                #   in Loop: Header=BB12_19 Depth=1
	movzbl	tr8x8+6160(%rdx), %eax
.LBB12_21:                              #   in Loop: Header=BB12_19 Depth=1
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rsi
	movq	(%rsi), %rsi
	movq	img(%rip), %rdi
	movl	172(%rdi), %ebp
	addl	%ecx, %ebp
	movslq	%ebp, %rbp
	movq	(%rsi,%rbp,8), %rsi
	movslq	168(%rdi), %rdi
	movb	%al, (%rsi,%rdi)
	cmpl	$0, 392(%r12,%rdx,4)
	jle	.LBB12_23
# BB#22:                                #   in Loop: Header=BB12_19 Depth=1
	movzbl	tr8x8+6164(%rdx), %ebx
.LBB12_23:                              #   in Loop: Header=BB12_19 Depth=1
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rsi
	movl	172(%rsi), %edi
	addl	%ecx, %edi
	movslq	%edi, %rdi
	movq	(%rax,%rdi,8), %rax
	movslq	168(%rsi), %rsi
	movb	%bl, (%rax,%rsi)
	movb	$-1, %bl
	testb	$1, 392(%r12,%rdx,4)
	movb	$-1, %al
	jne	.LBB12_25
# BB#24:                                #   in Loop: Header=BB12_19 Depth=1
	movzbl	tr8x8+6160(%rdx), %eax
.LBB12_25:                              #   in Loop: Header=BB12_19 Depth=1
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rsi
	movq	(%rsi), %rsi
	movq	img(%rip), %rdi
	movl	172(%rdi), %ebp
	addl	%ecx, %ebp
	movslq	%ebp, %rbp
	movq	(%rsi,%rbp,8), %rsi
	movslq	168(%rdi), %rdi
	movb	%al, 1(%rsi,%rdi)
	cmpl	$0, 392(%r12,%rdx,4)
	jle	.LBB12_27
# BB#26:                                #   in Loop: Header=BB12_19 Depth=1
	movzbl	tr8x8+6164(%rdx), %ebx
.LBB12_27:                              #   in Loop: Header=BB12_19 Depth=1
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rdx
	movl	172(%rdx), %esi
	addl	%ecx, %esi
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	movslq	168(%rdx), %rdx
	movb	%bl, 1(%rax,%rdx)
	movl	%ecx, %eax
	orl	$1, %eax
	movslq	%eax, %rdx
	movb	$-1, %bl
	testb	$1, 392(%r12,%rdx,4)
	movb	$-1, %al
	jne	.LBB12_29
# BB#28:                                #   in Loop: Header=BB12_19 Depth=1
	movzbl	tr8x8+6160(%rdx), %eax
.LBB12_29:                              #   in Loop: Header=BB12_19 Depth=1
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rsi
	movq	(%rsi), %rsi
	movq	img(%rip), %rdi
	movl	172(%rdi), %ebp
	addl	%ecx, %ebp
	movslq	%ebp, %rbp
	movq	(%rsi,%rbp,8), %rsi
	movslq	168(%rdi), %rdi
	movb	%al, 2(%rsi,%rdi)
	cmpl	$0, 392(%r12,%rdx,4)
	jle	.LBB12_31
# BB#30:                                #   in Loop: Header=BB12_19 Depth=1
	movzbl	tr8x8+6164(%rdx), %ebx
.LBB12_31:                              #   in Loop: Header=BB12_19 Depth=1
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rsi
	movl	172(%rsi), %edi
	addl	%ecx, %edi
	movslq	%edi, %rdi
	movq	(%rax,%rdi,8), %rax
	movslq	168(%rsi), %rsi
	movb	%bl, 2(%rax,%rsi)
	movb	$-1, %bl
	testb	$1, 392(%r12,%rdx,4)
	movb	$-1, %al
	jne	.LBB12_33
# BB#32:                                #   in Loop: Header=BB12_19 Depth=1
	movzbl	tr8x8+6160(%rdx), %eax
.LBB12_33:                              #   in Loop: Header=BB12_19 Depth=1
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rsi
	movq	(%rsi), %rsi
	movq	img(%rip), %rdi
	movl	172(%rdi), %ebp
	addl	%ecx, %ebp
	movslq	%ebp, %rbp
	movq	(%rsi,%rbp,8), %rsi
	movslq	168(%rdi), %rdi
	movb	%al, 3(%rsi,%rdi)
	cmpl	$0, 392(%r12,%rdx,4)
	jle	.LBB12_35
# BB#34:                                #   in Loop: Header=BB12_19 Depth=1
	movzbl	tr8x8+6164(%rdx), %ebx
.LBB12_35:                              #   in Loop: Header=BB12_19 Depth=1
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rdx
	movl	172(%rdx), %esi
	addl	%ecx, %esi
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	movslq	168(%rdx), %rdx
	movb	%bl, 3(%rax,%rdx)
	incl	%ecx
	cmpl	$4, %ecx
	jne	.LBB12_19
	jmp	.LBB12_36
.LBB12_5:                               # %.preheader152
	cmpl	$-3, 15528(%rcx)
	jl	.LBB12_8
# BB#6:                                 # %.preheader151.preheader
	movq	$-1, %rbp
	movq	cofAC8x8(%rip), %r14
	.p2align	4, 0x90
.LBB12_7:                               # %.preheader151
                                        # =>This Inner Loop Header: Depth=1
	movq	14160(%rcx), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	8(%r14,%rbp,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movq	8(%r14,%rbp,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	movq	8(%r14,%rbp,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	movq	8(%r14,%rbp,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	movq	8(%r14,%rbp,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	8(%r14,%rbp,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	cofAC8x8(%rip), %r14
	movq	8(%r14,%rbp,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rdi
	movq	8(%r14,%rbp,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rcx
	movslq	15528(%rcx), %rax
	addq	$3, %rax
	incq	%rbp
	cmpq	%rax, %rbp
	jl	.LBB12_7
.LBB12_8:                               # %._crit_edge
	cmpl	$5, cnt_nonz_8x8(%rip)
	jg	.LBB12_69
# BB#9:
	cmpl	$3, 20(%rcx)
	jne	.LBB12_10
.LBB12_69:
	movl	cbp8x8(%rip), %eax
	movl	%eax, 364(%r12)
	movslq	cbp_blk8x8(%rip), %rax
	movq	%rax, 368(%r12)
	xorl	%eax, %eax
	movl	$tr4x4+6168, %edx
	movl	$tr4x4+7192, %esi
	jmp	.LBB12_70
	.p2align	4, 0x90
.LBB12_74:                              # %._crit_edge257
                                        #   in Loop: Header=BB12_70 Depth=1
	incq	%rax
	movq	img(%rip), %rcx
	addq	$32, %rdx
	addq	$64, %rsi
.LBB12_70:                              # =>This Inner Loop Header: Depth=1
	movq	enc_picture(%rip), %rdi
	movq	6440(%rdi), %rdi
	movl	180(%rcx), %ebp
	addl	%eax, %ebp
	movslq	%ebp, %rbp
	movq	(%rdi,%rbp,8), %rdi
	movslq	176(%rcx), %rcx
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movups	%xmm1, 16(%rdi,%rcx,2)
	movups	%xmm0, (%rdi,%rcx,2)
	movq	img(%rip), %rcx
	cmpl	$3, 20(%rcx)
	jne	.LBB12_73
# BB#71:                                #   in Loop: Header=BB12_70 Depth=1
	movl	sp2_frame_indicator(%rip), %edi
	orl	si_frame_indicator(%rip), %edi
	jne	.LBB12_73
# BB#72:                                #   in Loop: Header=BB12_70 Depth=1
	movq	lrec(%rip), %rdi
	movl	180(%rcx), %ebp
	addl	%eax, %ebp
	movslq	%ebp, %rbp
	movq	(%rdi,%rbp,8), %rdi
	movslq	176(%rcx), %rcx
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	32(%rsi), %xmm2
	movups	48(%rsi), %xmm3
	movups	%xmm3, 48(%rdi,%rcx,4)
	movups	%xmm2, 32(%rdi,%rcx,4)
	movups	%xmm1, 16(%rdi,%rcx,4)
	movups	%xmm0, (%rdi,%rcx,4)
.LBB12_73:                              #   in Loop: Header=BB12_70 Depth=1
	cmpq	$15, %rax
	jne	.LBB12_74
	jmp	.LBB12_68
.LBB12_2:                               # %.preheader162.preheader
	xorl	%edx, %edx
	jmp	.LBB12_3
	.p2align	4, 0x90
.LBB12_4:                               # %.preheader162..preheader162_crit_edge
                                        #   in Loop: Header=BB12_3 Depth=1
	incl	%edx
	movq	img(%rip), %rcx
.LBB12_3:                               # %.preheader162
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %eax
	andl	$-2, %eax
	cltq
	movzbl	tr8x8+6160(%rax), %ebx
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rsi
	movq	(%rsi), %rsi
	movl	172(%rcx), %edi
	addl	%edx, %edi
	movslq	%edi, %rdi
	movq	(%rsi,%rdi,8), %rsi
	movslq	168(%rcx), %rcx
	movb	%bl, (%rsi,%rcx)
	movzbl	tr8x8+6160(%rax), %eax
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	img(%rip), %rsi
	movl	172(%rsi), %edi
	addl	%edx, %edi
	movslq	%edi, %rdi
	movq	(%rcx,%rdi,8), %rcx
	movslq	168(%rsi), %rsi
	movb	%al, 1(%rcx,%rsi)
	movl	%edx, %eax
	orl	$1, %eax
	cltq
	movzbl	tr8x8+6160(%rax), %ecx
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rsi
	movq	(%rsi), %rsi
	movq	img(%rip), %rdi
	movl	172(%rdi), %ebp
	addl	%edx, %ebp
	movslq	%ebp, %rbp
	movq	(%rsi,%rbp,8), %rsi
	movslq	168(%rdi), %rdi
	movb	%cl, 2(%rsi,%rdi)
	movzbl	tr8x8+6160(%rax), %eax
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	img(%rip), %rsi
	movl	172(%rsi), %edi
	addl	%edx, %edi
	movslq	%edi, %rdi
	movq	(%rcx,%rdi,8), %rcx
	movslq	168(%rsi), %rsi
	movb	%al, 3(%rcx,%rsi)
	cmpl	$3, %edx
	jne	.LBB12_4
.LBB12_36:                              # %.lr.ph188
	movq	img(%rip), %r9
	movslq	172(%r9), %r14
	movslq	168(%r9), %r11
	leaq	3(%r14), %r10
	leaq	3(%r11), %rbp
	decq	%r11
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB12_37:                              # %.lr.ph184
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_38 Depth 2
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%r11, %rsi
	.p2align	4, 0x90
.LBB12_38:                              #   Parent Loop BB12_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	1(%rdx,%rsi), %rdi
	testq	%rdi, %rdi
	js	.LBB12_39
# BB#40:                                #   in Loop: Header=BB12_38 Depth=2
	movslq	432(%r12), %rbx
	imulq	$264, %rbx, %rbx        # imm = 0x108
	addq	%rax, %rbx
	movq	24(%rbx,%rdi,8), %rdi
	jmp	.LBB12_41
	.p2align	4, 0x90
.LBB12_39:                              #   in Loop: Header=BB12_38 Depth=2
	movq	$-1, %rdi
.LBB12_41:                              #   in Loop: Header=BB12_38 Depth=2
	movq	6496(%rax), %rbx
	movq	(%rbx), %rbx
	movq	(%rbx,%rcx,8), %rbx
	movq	%rdi, 8(%rbx,%rsi,8)
	incq	%rsi
	cmpq	%rbp, %rsi
	jl	.LBB12_38
# BB#42:                                # %._crit_edge185
                                        #   in Loop: Header=BB12_37 Depth=1
	cmpq	%r10, %rcx
	leaq	1(%rcx), %rcx
	jl	.LBB12_37
# BB#43:                                # %._crit_edge189
	cmpl	$1, %r8d
	jne	.LBB12_51
# BB#44:                                # %.lr.ph181
	movslq	168(%r9), %rbp
	leal	3(%r14), %eax
	movslq	%eax, %r8
	leaq	3(%rbp), %rsi
	decq	%rbp
	.p2align	4, 0x90
.LBB12_45:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_46 Depth 2
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r14,8), %rdi
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB12_46:                              #   Parent Loop BB12_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	1(%rdi,%rax), %rdx
	testq	%rdx, %rdx
	js	.LBB12_47
# BB#48:                                #   in Loop: Header=BB12_46 Depth=2
	movslq	432(%r12), %rbx
	imulq	$264, %rbx, %rbx        # imm = 0x108
	addq	%rcx, %rbx
	movq	288(%rbx,%rdx,8), %rdx
	jmp	.LBB12_49
	.p2align	4, 0x90
.LBB12_47:                              #   in Loop: Header=BB12_46 Depth=2
	movq	$-1, %rdx
.LBB12_49:                              #   in Loop: Header=BB12_46 Depth=2
	movq	6496(%rcx), %rbx
	movq	8(%rbx), %rbx
	movq	(%rbx,%r14,8), %rbx
	movq	%rdx, 8(%rbx,%rax,8)
	incq	%rax
	cmpq	%rsi, %rax
	jl	.LBB12_46
# BB#50:                                # %._crit_edge179
                                        #   in Loop: Header=BB12_45 Depth=1
	cmpq	%r8, %r14
	leaq	1(%r14), %r14
	jl	.LBB12_45
.LBB12_51:                              # %.loopexit158
	movl	$1, %edi
	callq	StoreMV8x8
	movq	img(%rip), %rax
	xorl	%r15d, %r15d
	cmpl	$1, 20(%rax)
	sete	%r15b
	leaq	8216(%rsp), %r14
	movl	$tr8x8, %esi
	movl	$8216, %edx             # imm = 0x2018
	movq	%r14, %rdi
	callq	memcpy
	movl	$1027, %ecx             # imm = 0x403
	movq	%rsp, %rdi
	movq	%r14, %rsi
	rep;movsq
	xorl	%ebp, %ebp
	xorl	%edi, %edi
	xorl	%esi, %esi
	movl	%r15d, %edx
	callq	RestoreMVBlock8x8
	movl	$tr8x8, %esi
	movl	$8216, %edx             # imm = 0x2018
	movq	%r14, %rdi
	callq	memcpy
	movl	$1027, %ecx             # imm = 0x403
	movq	%rsp, %rdi
	movq	%r14, %rsi
	rep;movsq
	xorl	%edi, %edi
	movl	$1, %esi
	movl	%r15d, %edx
	callq	RestoreMVBlock8x8
	movl	$tr8x8, %esi
	movl	$8216, %edx             # imm = 0x2018
	movq	%r14, %rdi
	callq	memcpy
	movl	$1027, %ecx             # imm = 0x403
	movq	%rsp, %rdi
	movq	%r14, %rsi
	rep;movsq
	xorl	%edi, %edi
	movl	$2, %esi
	movl	%r15d, %edx
	callq	RestoreMVBlock8x8
	movl	$tr8x8, %esi
	movl	$8216, %edx             # imm = 0x2018
	movq	%r14, %rdi
	callq	memcpy
	movl	$1027, %ecx             # imm = 0x403
	movq	%rsp, %rdi
	movq	%r14, %rsi
	rep;movsq
	xorl	%edi, %edi
	movl	$3, %esi
	movl	%r15d, %edx
	callq	RestoreMVBlock8x8
	movq	cofAC_8x8ts(%rip), %rbx
	.p2align	4, 0x90
.LBB12_52:                              # %.preheader156
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	(%rbx,%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rbx,%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbp), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	movq	(%rbx,%rbp), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbp), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rbx,%rbp), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbp), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	movq	(%rbx,%rbp), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbp), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rbx,%rbp), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbp), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	cofAC_8x8ts(%rip), %rbx
	movq	(%rbx,%rbp), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbp), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rbx,%rbp), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	addq	$8, %rbp
	cmpq	$32, %rbp
	jne	.LBB12_52
# BB#53:
	cmpl	$5, cnt_nonz8_8x8ts(%rip)
	jg	.LBB12_63
# BB#54:
	movq	img(%rip), %rsi
	cmpl	$0, 44(%rsi)
	je	.LBB12_55
.LBB12_56:
	cmpl	$3, 20(%rsi)
	je	.LBB12_63
# BB#57:
	movl	$0, 364(%r12)
	movq	$0, 368(%r12)
	xorl	%eax, %eax
	movl	$tr8x8+6680, %ecx
	movl	$tr8x8+7192, %edx
	jmp	.LBB12_58
	.p2align	4, 0x90
.LBB12_62:                              # %._crit_edge252
                                        #   in Loop: Header=BB12_58 Depth=1
	incq	%rax
	movq	img(%rip), %rsi
	addq	$32, %rcx
	addq	$64, %rdx
.LBB12_58:                              # =>This Inner Loop Header: Depth=1
	movq	enc_picture(%rip), %rdi
	movq	6440(%rdi), %rdi
	movl	180(%rsi), %ebp
	addl	%eax, %ebp
	movslq	%ebp, %rbp
	movq	(%rdi,%rbp,8), %rdi
	movslq	176(%rsi), %rsi
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	%xmm1, 16(%rdi,%rsi,2)
	movups	%xmm0, (%rdi,%rsi,2)
	movq	img(%rip), %rsi
	cmpl	$3, 20(%rsi)
	jne	.LBB12_61
# BB#59:                                #   in Loop: Header=BB12_58 Depth=1
	movl	sp2_frame_indicator(%rip), %edi
	orl	si_frame_indicator(%rip), %edi
	jne	.LBB12_61
# BB#60:                                #   in Loop: Header=BB12_58 Depth=1
	movq	lrec(%rip), %rdi
	movl	180(%rsi), %ebp
	addl	%eax, %ebp
	movslq	%ebp, %rbp
	movq	(%rdi,%rbp,8), %rdi
	movslq	176(%rsi), %rsi
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movups	32(%rdx), %xmm2
	movups	48(%rdx), %xmm3
	movups	%xmm3, 48(%rdi,%rsi,4)
	movups	%xmm2, 32(%rdi,%rsi,4)
	movups	%xmm1, 16(%rdi,%rsi,4)
	movups	%xmm0, (%rdi,%rsi,4)
.LBB12_61:                              #   in Loop: Header=BB12_58 Depth=1
	cmpq	$15, %rax
	jne	.LBB12_62
	jmp	.LBB12_68
.LBB12_55:
	cmpl	$0, 15540(%rsi)
	je	.LBB12_56
.LBB12_63:
	movl	cbp8_8x8ts(%rip), %eax
	movl	%eax, 364(%r12)
	movq	cbp_blk8_8x8ts(%rip), %rax
	movq	%rax, 368(%r12)
	xorl	%eax, %eax
	movl	$tr8x8+6168, %ecx
	movl	$tr8x8+7192, %edx
	.p2align	4, 0x90
.LBB12_64:                              # =>This Inner Loop Header: Depth=1
	movq	enc_picture(%rip), %rsi
	movq	6440(%rsi), %rsi
	movq	img(%rip), %rdi
	movl	180(%rdi), %ebp
	addl	%eax, %ebp
	movslq	%ebp, %rbp
	movq	(%rsi,%rbp,8), %rsi
	movslq	176(%rdi), %rdi
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	%xmm1, 16(%rsi,%rdi,2)
	movups	%xmm0, (%rsi,%rdi,2)
	movq	img(%rip), %rsi
	cmpl	$3, 20(%rsi)
	jne	.LBB12_67
# BB#65:                                #   in Loop: Header=BB12_64 Depth=1
	movl	sp2_frame_indicator(%rip), %edi
	orl	si_frame_indicator(%rip), %edi
	jne	.LBB12_67
# BB#66:                                #   in Loop: Header=BB12_64 Depth=1
	movq	lrec(%rip), %rdi
	movl	180(%rsi), %ebp
	addl	%eax, %ebp
	movslq	%ebp, %rbp
	movq	(%rdi,%rbp,8), %rdi
	movslq	176(%rsi), %rsi
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movups	32(%rdx), %xmm2
	movups	48(%rdx), %xmm3
	movups	%xmm3, 48(%rdi,%rsi,4)
	movups	%xmm2, 32(%rdi,%rsi,4)
	movups	%xmm1, 16(%rdi,%rsi,4)
	movups	%xmm0, (%rdi,%rsi,4)
.LBB12_67:                              #   in Loop: Header=BB12_64 Depth=1
	incq	%rax
	addq	$32, %rcx
	addq	$64, %rdx
	cmpq	$16, %rax
	jne	.LBB12_64
.LBB12_68:                              # %.loopexit
	addq	$16432, %rsp            # imm = 0x4030
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_10:
	cmpl	$0, 44(%rcx)
	jne	.LBB12_12
# BB#11:
	cmpl	$0, 15540(%rcx)
	jne	.LBB12_69
.LBB12_12:
	movl	$0, 364(%r12)
	movq	$0, 368(%r12)
	xorl	%eax, %eax
	movl	$tr4x4+6680, %edx
	movl	$tr4x4+7192, %esi
	jmp	.LBB12_13
	.p2align	4, 0x90
.LBB12_17:                              # %._crit_edge255
                                        #   in Loop: Header=BB12_13 Depth=1
	incq	%rax
	movq	img(%rip), %rcx
	addq	$32, %rdx
	addq	$64, %rsi
.LBB12_13:                              # =>This Inner Loop Header: Depth=1
	movq	enc_picture(%rip), %rdi
	movq	6440(%rdi), %rdi
	movl	180(%rcx), %ebp
	addl	%eax, %ebp
	movslq	%ebp, %rbp
	movq	(%rdi,%rbp,8), %rdi
	movslq	176(%rcx), %rcx
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movups	%xmm1, 16(%rdi,%rcx,2)
	movups	%xmm0, (%rdi,%rcx,2)
	movq	img(%rip), %rcx
	cmpl	$3, 20(%rcx)
	jne	.LBB12_16
# BB#14:                                #   in Loop: Header=BB12_13 Depth=1
	movl	sp2_frame_indicator(%rip), %edi
	orl	si_frame_indicator(%rip), %edi
	jne	.LBB12_16
# BB#15:                                #   in Loop: Header=BB12_13 Depth=1
	movq	lrec(%rip), %rdi
	movl	180(%rcx), %ebp
	addl	%eax, %ebp
	movslq	%ebp, %rbp
	movq	(%rdi,%rbp,8), %rdi
	movslq	176(%rcx), %rcx
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	32(%rsi), %xmm2
	movups	48(%rsi), %xmm3
	movups	%xmm3, 48(%rdi,%rcx,4)
	movups	%xmm2, 32(%rdi,%rcx,4)
	movups	%xmm1, 16(%rdi,%rcx,4)
	movups	%xmm0, (%rdi,%rcx,4)
.LBB12_16:                              #   in Loop: Header=BB12_13 Depth=1
	cmpq	$15, %rax
	jne	.LBB12_17
	jmp	.LBB12_68
.Lfunc_end12:
	.size	SetCoeffAndReconstruction8x8, .Lfunc_end12-SetCoeffAndReconstruction8x8
	.cfi_endproc

	.globl	StoreMV8x8
	.p2align	4, 0x90
	.type	StoreMV8x8,@function
StoreMV8x8:                             # @StoreMV8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi123:
	.cfi_def_cfa_offset 32
.Lcfi124:
	.cfi_offset %rbx, -24
.Lcfi125:
	.cfi_offset %rbp, -16
	movl	%edi, %ebx
	movq	img(%rip), %rax
	xorl	%ebp, %ebp
	cmpl	$1, 20(%rax)
	sete	%bpl
	movswl	tr8x8+6148(%rip), %edx
	movsbl	tr8x8+6160(%rip), %ecx
	movsbl	tr8x8+6164(%rip), %r8d
	movsbl	tr8x8+6156(%rip), %r9d
	movl	%ebp, (%rsp)
	xorl	%esi, %esi
	callq	StoreMVBlock8x8
	movswl	tr8x8+6150(%rip), %edx
	movsbl	tr8x8+6161(%rip), %ecx
	movsbl	tr8x8+6165(%rip), %r8d
	movsbl	tr8x8+6157(%rip), %r9d
	movl	%ebp, (%rsp)
	movl	$1, %esi
	movl	%ebx, %edi
	callq	StoreMVBlock8x8
	movswl	tr8x8+6152(%rip), %edx
	movsbl	tr8x8+6162(%rip), %ecx
	movsbl	tr8x8+6166(%rip), %r8d
	movsbl	tr8x8+6158(%rip), %r9d
	movl	%ebp, (%rsp)
	movl	$2, %esi
	movl	%ebx, %edi
	callq	StoreMVBlock8x8
	movswl	tr8x8+6154(%rip), %edx
	movsbl	tr8x8+6163(%rip), %ecx
	movsbl	tr8x8+6167(%rip), %r8d
	movsbl	tr8x8+6159(%rip), %r9d
	movl	%ebp, (%rsp)
	movl	$3, %esi
	movl	%ebx, %edi
	callq	StoreMVBlock8x8
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end13:
	.size	StoreMV8x8, .Lfunc_end13-StoreMV8x8
	.cfi_endproc

	.globl	RestoreMV8x8
	.p2align	4, 0x90
	.type	RestoreMV8x8,@function
RestoreMV8x8:                           # @RestoreMV8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi126:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 32
	subq	$16432, %rsp            # imm = 0x4030
.Lcfi129:
	.cfi_def_cfa_offset 16464
.Lcfi130:
	.cfi_offset %rbx, -32
.Lcfi131:
	.cfi_offset %r14, -24
.Lcfi132:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movq	img(%rip), %rax
	xorl	%ebp, %ebp
	cmpl	$1, 20(%rax)
	sete	%bpl
	leaq	8216(%rsp), %rbx
	movl	$tr8x8, %esi
	movl	$8216, %edx             # imm = 0x2018
	movq	%rbx, %rdi
	callq	memcpy
	movl	$1027, %ecx             # imm = 0x403
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	rep;movsq
	xorl	%esi, %esi
	movl	%r14d, %edi
	movl	%ebp, %edx
	callq	RestoreMVBlock8x8
	movl	$tr8x8, %esi
	movl	$8216, %edx             # imm = 0x2018
	movq	%rbx, %rdi
	callq	memcpy
	movl	$1027, %ecx             # imm = 0x403
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	rep;movsq
	movl	$1, %esi
	movl	%r14d, %edi
	movl	%ebp, %edx
	callq	RestoreMVBlock8x8
	movl	$tr8x8, %esi
	movl	$8216, %edx             # imm = 0x2018
	movq	%rbx, %rdi
	callq	memcpy
	movl	$1027, %ecx             # imm = 0x403
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	rep;movsq
	movl	$2, %esi
	movl	%r14d, %edi
	movl	%ebp, %edx
	callq	RestoreMVBlock8x8
	movl	$tr8x8, %esi
	movl	$8216, %edx             # imm = 0x2018
	movq	%rbx, %rdi
	callq	memcpy
	movl	$1027, %ecx             # imm = 0x403
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	rep;movsq
	movl	$3, %esi
	movl	%r14d, %edi
	movl	%ebp, %edx
	callq	RestoreMVBlock8x8
	addq	$16432, %rsp            # imm = 0x4030
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end14:
	.size	RestoreMV8x8, .Lfunc_end14-RestoreMV8x8
	.cfi_endproc

	.globl	SetMotionVectorsMB
	.p2align	4, 0x90
	.type	SetMotionVectorsMB,@function
SetMotionVectorsMB:                     # @SetMotionVectorsMB
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi135:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi136:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi137:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi139:
	.cfi_def_cfa_offset 176
.Lcfi140:
	.cfi_offset %rbx, -56
.Lcfi141:
	.cfi_offset %r12, -48
.Lcfi142:
	.cfi_offset %r13, -40
.Lcfi143:
	.cfi_offset %r14, -32
.Lcfi144:
	.cfi_offset %r15, -24
.Lcfi145:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movq	img(%rip), %r8
	movq	14376(%r8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	14384(%r8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testl	%esi, %esi
	je	.LBB15_1
# BB#7:                                 # %.preheader252
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB15_8:                               # %._crit_edge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_9 Depth 2
	movslq	172(%r8), %rax
	movslq	%r11d, %r12
	movl	%r11d, %r15d
	andl	$-2, %r15d
	addq	%rax, %r12
	xorl	%r14d, %r14d
	jmp	.LBB15_9
	.p2align	4, 0x90
.LBB15_24:                              # %._crit_edge288
                                        #   in Loop: Header=BB15_9 Depth=2
	incq	%r14
	movq	img(%rip), %r8
.LBB15_9:                               #   Parent Loop BB15_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r14d, %eax
	sarl	%eax
	addl	%r15d, %eax
	cltq
	movslq	376(%r13,%rax,4), %r9
	movl	168(%r8), %ecx
	addl	%r14d, %ecx
	movl	392(%r13,%rax,4), %ebp
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rdi
	movq	(%rdx,%r12,8), %rbx
	movslq	%ecx, %rcx
	movsbl	(%rbx,%rcx), %eax
	movq	(%rdi,%r12,8), %rdx
	movsbl	(%rdx,%rcx), %r10d
	cmpl	$2, %ebp
	jne	.LBB15_15
# BB#10:                                #   in Loop: Header=BB15_9 Depth=2
	movzwl	480(%r13), %edi
	testw	%di, %di
	je	.LBB15_15
# BB#11:                                #   in Loop: Header=BB15_9 Depth=2
	cmpl	$1, 72(%r13)
	jne	.LBB15_13
# BB#12:                                #   in Loop: Header=BB15_9 Depth=2
	movzwl	%di, %eax
	leaq	14400(%r8), %rdx
	addq	$14392, %r8             # imm = 0x3838
	cmpl	$1, %eax
	cmovneq	%rdx, %r8
	movq	(%r8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	cmpl	$2, %ebp
	je	.LBB15_14
	jmp	.LBB15_22
	.p2align	4, 0x90
.LBB15_15:                              #   in Loop: Header=BB15_9 Depth=2
	cmpl	$1, %ebp
	je	.LBB15_20
# BB#16:                                #   in Loop: Header=BB15_9 Depth=2
	testl	%ebp, %ebp
	je	.LBB15_19
# BB#17:                                #   in Loop: Header=BB15_9 Depth=2
	cmpl	$-1, %ebp
	jne	.LBB15_13
# BB#18:                                #   in Loop: Header=BB15_9 Depth=2
	movq	6512(%rsi), %rax
	movq	(%rax), %rdx
	movq	(%rdx,%r12,8), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movl	$0, (%rdx)
	movq	8(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%rcx,8), %rax
	movl	$0, (%rax)
	cmpq	$3, %r14
	jne	.LBB15_24
	jmp	.LBB15_25
	.p2align	4, 0x90
.LBB15_13:                              # %.thread
                                        #   in Loop: Header=BB15_9 Depth=2
	cmpl	$2, %ebp
	jne	.LBB15_22
.LBB15_14:                              #   in Loop: Header=BB15_9 Depth=2
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx), %rdi
	cltq
	movq	(%rdi,%rax,8), %rax
	movq	(%rax,%r9,8), %rax
	movzwl	(%rax), %edi
	movq	6512(%rsi), %rsi
	movq	(%rsi), %rbp
	movq	(%rbp,%r12,8), %rbp
	movq	(%rbp,%rcx,8), %rbp
	movw	%di, (%rbp)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rbp)
	movq	8(%rdx), %rax
	movslq	%r10d, %rdx
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%r9,8), %rax
	movzwl	(%rax), %edx
	jmp	.LBB15_21
	.p2align	4, 0x90
.LBB15_22:                              #   in Loop: Header=BB15_9 Depth=2
	movl	$.L.str.1, %edi
	movl	$255, %esi
	movq	%r11, %rbx
	callq	error
	movq	%rbx, %r11
	cmpq	$3, %r14
	jne	.LBB15_24
	jmp	.LBB15_25
.LBB15_20:                              #   in Loop: Header=BB15_9 Depth=2
	movq	6512(%rsi), %rax
	movq	(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%rcx,8), %rax
	movw	$0, (%rax)
	movw	$0, 2(%rax)
	movb	$-1, (%rbx,%rcx)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%r14,8), %rax
	movq	8(%rax), %rax
	movslq	%r10d, %rdx
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%r9,8), %rax
	movzwl	(%rax), %edx
	movq	enc_picture(%rip), %rsi
	movq	6512(%rsi), %rsi
.LBB15_21:                              #   in Loop: Header=BB15_9 Depth=2
	movq	8(%rsi), %rsi
	movq	(%rsi,%r12,8), %rsi
	movq	(%rsi,%rcx,8), %rcx
	movw	%dx, (%rcx)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rcx)
	cmpq	$3, %r14
	jne	.LBB15_24
	jmp	.LBB15_25
.LBB15_19:                              #   in Loop: Header=BB15_9 Depth=2
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi,%r11,8), %rdi
	movq	(%rdi,%r14,8), %rdi
	movq	(%rdi), %rdi
	cltq
	movq	(%rdi,%rax,8), %rax
	movq	(%rax,%r9,8), %rax
	movzwl	(%rax), %edi
	movq	6512(%rsi), %rsi
	movq	(%rsi), %rbp
	movq	(%rbp,%r12,8), %rbp
	movq	(%rbp,%rcx,8), %rbp
	movw	%di, (%rbp)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rbp)
	movq	8(%rsi), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%rcx,8), %rax
	movw	$0, (%rax)
	movw	$0, 2(%rax)
	movb	$-1, (%rdx,%rcx)
	cmpq	$3, %r14
	jne	.LBB15_24
	.p2align	4, 0x90
.LBB15_25:                              #   in Loop: Header=BB15_8 Depth=1
	incq	%r11
	movq	img(%rip), %r8
	cmpq	$4, %r11
	jne	.LBB15_8
	jmp	.LBB15_26
.LBB15_1:                               # %.preheader250.preheader
	movslq	168(%r8), %r10
	movq	enc_picture(%rip), %rdx
	movl	172(%r8), %r9d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	leal	(%r9,%rbx), %eax
	movl	%ebx, %esi
	andl	$-2, %esi
	movslq	%eax, %rbp
	movslq	%esi, %rdi
	movq	6512(%rdx), %rax
	cmpl	$0, 392(%r13,%rdi,4)
	js	.LBB15_4
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	6488(%rdx), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%rbp,8), %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rbx,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movsbq	(%rsi,%r10), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movslq	376(%r13,%rdi,4), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movzwl	(%rcx), %esi
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%r10,8), %rax
	movw	%si, (%rax)
	movw	2(%rcx), %si
	jmp	.LBB15_5
	.p2align	4, 0x90
.LBB15_4:                               #   in Loop: Header=BB15_2 Depth=1
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%r10,8), %rax
	movw	$0, (%rax)
	xorl	%esi, %esi
.LBB15_5:                               #   in Loop: Header=BB15_2 Depth=1
	movw	%si, 2(%rax)
	cmpl	$0, 392(%r13,%rdi,4)
	movq	6512(%rdx), %rax
	js	.LBB15_6
# BB#37:                                #   in Loop: Header=BB15_2 Depth=1
	movq	6488(%rdx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsi,%rbx,8), %rsi
	movq	8(%rsi), %rsi
	movq	(%rsi), %rsi
	movsbq	1(%rcx,%r10), %rcx
	movq	(%rsi,%rcx,8), %rcx
	movslq	376(%r13,%rdi,4), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movzwl	(%rcx), %esi
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	8(%rax,%r10,8), %rax
	movw	%si, (%rax)
	movw	2(%rcx), %si
	jmp	.LBB15_38
	.p2align	4, 0x90
.LBB15_6:                               #   in Loop: Header=BB15_2 Depth=1
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	8(%rax,%r10,8), %rax
	movw	$0, (%rax)
	xorl	%esi, %esi
.LBB15_38:                              #   in Loop: Header=BB15_2 Depth=1
	movw	%si, 2(%rax)
	movq	%rbx, %rdi
	orq	$1, %rdi
	cmpl	$0, 392(%r13,%rdi,4)
	movq	6512(%rdx), %rax
	js	.LBB15_39
# BB#40:                                #   in Loop: Header=BB15_2 Depth=1
	movq	6488(%rdx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsi,%rbx,8), %rsi
	movq	16(%rsi), %rsi
	movq	(%rsi), %rsi
	movsbq	2(%rcx,%r10), %rcx
	movq	(%rsi,%rcx,8), %rcx
	movslq	376(%r13,%rdi,4), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movzwl	(%rcx), %esi
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	16(%rax,%r10,8), %rax
	movw	%si, (%rax)
	movw	2(%rcx), %si
	jmp	.LBB15_41
	.p2align	4, 0x90
.LBB15_39:                              #   in Loop: Header=BB15_2 Depth=1
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	16(%rax,%r10,8), %rax
	movw	$0, (%rax)
	xorl	%esi, %esi
.LBB15_41:                              #   in Loop: Header=BB15_2 Depth=1
	movw	%si, 2(%rax)
	cmpl	$0, 392(%r13,%rdi,4)
	movq	6512(%rdx), %rax
	js	.LBB15_42
# BB#43:                                #   in Loop: Header=BB15_2 Depth=1
	movq	6488(%rdx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsi,%rbx,8), %rsi
	movq	24(%rsi), %rsi
	movq	(%rsi), %rsi
	movsbq	3(%rcx,%r10), %rcx
	movq	(%rsi,%rcx,8), %rcx
	movslq	376(%r13,%rdi,4), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movzwl	(%rcx), %esi
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax,%r10,8), %rax
	movw	%si, (%rax)
	movw	2(%rcx), %si
	jmp	.LBB15_44
	.p2align	4, 0x90
.LBB15_42:                              #   in Loop: Header=BB15_2 Depth=1
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax,%r10,8), %rax
	movw	$0, (%rax)
	xorl	%esi, %esi
.LBB15_44:                              #   in Loop: Header=BB15_2 Depth=1
	movw	%si, 2(%rax)
	incq	%rbx
	cmpq	$4, %rbx
	jne	.LBB15_2
.LBB15_26:                              # %.loopexit251
	cmpl	$0, 15268(%r8)
	je	.LBB15_36
# BB#27:                                # %.preheader249
	movl	32(%r8), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movq	rdopt(%rip), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	img(%rip), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB15_28:                              # %.preheader248
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_29 Depth 2
                                        #       Child Loop BB15_31 Depth 3
                                        #         Child Loop BB15_32 Depth 4
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB15_29:                              # %.preheader247
                                        #   Parent Loop BB15_28 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_31 Depth 3
                                        #         Child Loop BB15_32 Depth 4
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	jle	.LBB15_34
# BB#30:                                # %.preheader246.us.preheader
                                        #   in Loop: Header=BB15_29 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	1664(%rcx), %rax
	movq	1672(%rcx), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rsi,8), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	(%rax,%rsi,8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	32(%rax), %rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_31:                              # %.preheader246.us
                                        #   Parent Loop BB15_28 Depth=1
                                        #     Parent Loop BB15_29 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB15_32 Depth 4
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %rbx
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %r15
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB15_32:                              # %.preheader.us
                                        #   Parent Loop BB15_28 Depth=1
                                        #     Parent Loop BB15_29 Depth=2
                                        #       Parent Loop BB15_31 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r8,8), %r14
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r8,8), %r11
	movq	(%rbx,%r8,8), %r10
	movq	(%r15,%r8,8), %r9
	movq	(%r14), %r12
	movzwl	(%r12), %r13d
	movq	(%r11), %rdx
	movw	%r13w, (%rdx)
	movq	(%r10), %rax
	movzwl	(%rax), %esi
	movq	(%r9), %rcx
	movw	%si, (%rcx)
	movzwl	2(%r12), %esi
	movw	%si, 2(%rdx)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rcx)
	movq	8(%r14), %rax
	movzwl	(%rax), %ecx
	movq	8(%r11), %rdx
	movw	%cx, (%rdx)
	movq	8(%r10), %rcx
	movzwl	(%rcx), %esi
	movq	8(%r9), %rdi
	movw	%si, (%rdi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rdx)
	movzwl	2(%rcx), %eax
	movw	%ax, 2(%rdi)
	movq	16(%r14), %rax
	movzwl	(%rax), %ecx
	movq	16(%r11), %rdx
	movw	%cx, (%rdx)
	movq	16(%r10), %rcx
	movzwl	(%rcx), %esi
	movq	16(%r9), %rdi
	movw	%si, (%rdi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rdx)
	movzwl	2(%rcx), %eax
	movw	%ax, 2(%rdi)
	movq	24(%r14), %rax
	movzwl	(%rax), %ecx
	movq	24(%r11), %rdx
	movw	%cx, (%rdx)
	movq	24(%r10), %rcx
	movzwl	(%rcx), %esi
	movq	24(%r9), %rdi
	movw	%si, (%rdi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rdx)
	movzwl	2(%rcx), %eax
	movw	%ax, 2(%rdi)
	movq	32(%r14), %rax
	movzwl	(%rax), %ecx
	movq	32(%r11), %rdx
	movw	%cx, (%rdx)
	movq	32(%r10), %rcx
	movzwl	(%rcx), %esi
	movq	32(%r9), %rdi
	movw	%si, (%rdi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rdx)
	movzwl	2(%rcx), %eax
	movw	%ax, 2(%rdi)
	movq	40(%r14), %rax
	movzwl	(%rax), %ecx
	movq	40(%r11), %rdx
	movw	%cx, (%rdx)
	movq	40(%r10), %rcx
	movzwl	(%rcx), %esi
	movq	40(%r9), %rdi
	movw	%si, (%rdi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rdx)
	movzwl	2(%rcx), %eax
	movw	%ax, 2(%rdi)
	movq	48(%r14), %rax
	movzwl	(%rax), %ecx
	movq	48(%r11), %rdx
	movw	%cx, (%rdx)
	movq	48(%r10), %rcx
	movzwl	(%rcx), %esi
	movq	48(%r9), %rdi
	movw	%si, (%rdi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rdx)
	movzwl	2(%rcx), %eax
	movw	%ax, 2(%rdi)
	movq	56(%r14), %rax
	movzwl	(%rax), %ecx
	movq	56(%r11), %rdx
	movw	%cx, (%rdx)
	movq	56(%r10), %rcx
	movzwl	(%rcx), %esi
	movq	56(%r9), %rdi
	movw	%si, (%rdi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rdx)
	movzwl	2(%rcx), %eax
	movw	%ax, 2(%rdi)
	movq	64(%r14), %rax
	movzwl	(%rax), %ecx
	movq	64(%r11), %rdx
	movw	%cx, (%rdx)
	movq	64(%r10), %rcx
	movzwl	(%rcx), %esi
	movq	64(%r9), %rdi
	movw	%si, (%rdi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rdx)
	movzwl	2(%rcx), %eax
	incq	%r8
	cmpq	%rbp, %r8
	movw	%ax, 2(%rdi)
	jl	.LBB15_32
# BB#33:                                # %._crit_edge.us
                                        #   in Loop: Header=BB15_31 Depth=3
	movq	96(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	$2, %rcx
	jne	.LBB15_31
.LBB15_34:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB15_29 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpq	$4, %rcx
	jne	.LBB15_29
# BB#35:                                #   in Loop: Header=BB15_28 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	$4, %rcx
	jne	.LBB15_28
.LBB15_36:                              # %.loopexit
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	SetMotionVectorsMB, .Lfunc_end15-SetMotionVectorsMB
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI16_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	RDCost_for_macroblocks
	.p2align	4, 0x90
	.type	RDCost_for_macroblocks,@function
RDCost_for_macroblocks:                 # @RDCost_for_macroblocks
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi148:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi149:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi150:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi152:
	.cfi_def_cfa_offset 288
.Lcfi153:
	.cfi_offset %rbx, -56
.Lcfi154:
	.cfi_offset %r12, -48
.Lcfi155:
	.cfi_offset %r13, -40
.Lcfi156:
	.cfi_offset %r14, -32
.Lcfi157:
	.cfi_offset %r15, -24
.Lcfi158:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movl	%edi, %ebx
	movsd	%xmm0, 104(%rsp)        # 8-byte Spill
	movl	%ecx, 20(%rsp)
	movl	$0, 16(%rsp)
	movl	$0, 44(%rsp)
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	callq	FmoGetPreviousMBNr
	movq	img(%rip), %rcx
	movq	14224(%rcx), %r13
	movslq	12(%rcx), %r14
	cltq
	imulq	$536, %rax, %rdx        # imm = 0x218
	addq	%r13, %rdx
	xorl	%esi, %esi
	testl	%eax, %eax
	cmovsq	%rsi, %rdx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movl	20(%rcx), %eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	cmpl	$2, %eax
	je	.LBB16_2
# BB#1:
	movq	input(%rip), %rax
	cmpl	$1, 4008(%rax)
	setne	%sil
.LBB16_2:
	movq	%rsi, 224(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	SetModesAndRefframeForBlocks
	movq	img(%rip), %rcx
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	testl	%ebx, %ebx
	jne	.LBB16_17
# BB#3:
	cmpl	$1, 76(%rsp)            # 4-byte Folded Reload
	jne	.LBB16_17
# BB#4:
	movl	176(%rcx), %eax
	movl	180(%rcx), %edi
	sarl	$2, %eax
	sarl	$2, %edi
	movq	direct_pdir(%rip), %rsi
	movslq	%eax, %rdx
	movslq	%edi, %rbp
	movq	(%rsi,%rbp,8), %rdi
	xorl	%eax, %eax
	cmpb	$0, (%rdi,%rdx)
	js	.LBB16_152
# BB#5:
	cmpb	$0, 1(%rdi,%rdx)
	js	.LBB16_152
# BB#6:
	leal	3(%rdx), %ebx
	movslq	%ebx, %r11
	leaq	1(%rdx), %r9
	leaq	2(%rdx), %r8
	cmpq	%r11, %r9
	jge	.LBB16_10
# BB#7:
	cmpb	$0, (%rdi,%r8)
	js	.LBB16_152
# BB#8:
	cmpq	%r11, %r8
	jge	.LBB16_10
# BB#9:
	cmpb	$0, 3(%rdi,%rdx)
	js	.LBB16_152
.LBB16_10:
	movq	8(%rsi,%rbp,8), %rdi
	cmpb	$0, (%rdi,%rdx)
	js	.LBB16_152
# BB#11:
	cmpb	$0, 1(%rdi,%rdx)
	js	.LBB16_152
# BB#12:
	cmpq	%r11, %r9
	jge	.LBB16_16
# BB#13:
	cmpb	$0, (%rdi,%r8)
	js	.LBB16_152
# BB#14:
	cmpq	%r11, %r8
	jge	.LBB16_16
# BB#15:
	cmpb	$0, 3(%rdi,%rdx)
	js	.LBB16_152
.LBB16_16:
	leal	3(%rbp), %edi
	movslq	%edi, %r10
	leaq	1(%rbp), %rdi
	cmpq	%r10, %rdi
	jge	.LBB16_17
# BB#161:
	movq	16(%rsi,%rbp,8), %rdi
	cmpb	$0, (%rdi,%rdx)
	js	.LBB16_152
# BB#162:
	cmpb	$0, 1(%rdi,%rdx)
	js	.LBB16_152
# BB#163:
	cmpq	%r11, %r9
	jge	.LBB16_167
# BB#164:
	cmpb	$0, (%rdi,%r8)
	js	.LBB16_152
# BB#165:
	cmpq	%r11, %r8
	jge	.LBB16_167
# BB#166:
	cmpb	$0, 3(%rdi,%rdx)
	js	.LBB16_152
.LBB16_167:
	leaq	2(%rbp), %rdi
	cmpq	%r10, %rdi
	jge	.LBB16_17
# BB#168:
	movq	24(%rsi,%rbp,8), %rsi
	cmpb	$0, (%rsi,%rdx)
	js	.LBB16_152
# BB#169:
	cmpb	$0, 1(%rsi,%rdx)
	js	.LBB16_152
# BB#170:
	cmpq	%r11, %r9
	jge	.LBB16_17
# BB#171:
	cmpb	$0, (%rsi,%r8)
	js	.LBB16_152
# BB#172:
	cmpq	%r11, %r8
	jge	.LBB16_17
# BB#173:
	cmpb	$0, 3(%rsi,%rdx)
	js	.LBB16_152
.LBB16_17:                              # %.loopexit222
	imulq	$536, %r14, %rbp        # imm = 0x218
	cmpl	$0, 15268(%rcx)
	je	.LBB16_23
# BB#18:
	cmpl	$0, 424(%r13,%rbp)
	jne	.LBB16_23
# BB#19:
	movl	20(%rcx), %eax
	orl	48(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB16_20
.LBB16_23:
	cmpl	$0, 15260(%rcx)
	je	.LBB16_25
# BB#24:
	movq	14176(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	callq	memset
	movq	img(%rip), %rax
	movq	14184(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	callq	memset
	movq	img(%rip), %rax
	movq	14192(%rax), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rdi
	movslq	15544(%rax), %rcx
	movslq	15548(%rax), %rdx
	imulq	%rcx, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
	movq	img(%rip), %rax
	movq	14192(%rax), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx), %rdi
	movslq	15544(%rax), %rcx
	movslq	15548(%rax), %rdx
	imulq	%rcx, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
.LBB16_25:
	leaq	(%r13,%rbp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rbx          # 8-byte Reload
	cmpl	$7, %ebx
	movq	%r15, 152(%rsp)         # 8-byte Spill
	movq	%r12, 144(%rsp)         # 8-byte Spill
	movq	%r13, 96(%rsp)          # 8-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	jg	.LBB16_31
# BB#26:
	callq	LumaResidualCoding
	testl	%ebx, %ebx
	jne	.LBB16_78
# BB#27:
	cmpl	$0, 364(%r13,%rbp)
	je	.LBB16_30
# BB#28:
	movq	img(%rip), %rcx
	xorl	%eax, %eax
	cmpl	$1, 20(%rcx)
	jne	.LBB16_152
# BB#29:
	cmpl	$1, 15256(%rcx)
	jne	.LBB16_78
	jmp	.LBB16_152
.LBB16_31:
	leal	-8(%rbx), %eax
	cmpl	$6, %eax
	ja	.LBB16_78
# BB#32:
	jmpq	*.LJTI16_0(,%rax,8)
.LBB16_33:
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	SetCoeffAndReconstruction8x8
	jmp	.LBB16_78
.LBB16_20:
	movq	14384(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	movzwl	(%rdx), %eax
	addl	$8192, %eax             # imm = 0x2000
	movzwl	%ax, %esi
	xorl	%eax, %eax
	cmpl	$16383, %esi            # imm = 0x3FFF
	ja	.LBB16_152
# BB#21:
	movswl	2(%rdx), %edx
	movslq	8(%rcx), %rsi
	leaq	(%rsi,%rsi,2), %rsi
	cmpl	LEVELMVLIMIT+16(,%rsi,8), %edx
	jl	.LBB16_152
# BB#22:
	cmpl	LEVELMVLIMIT+20(,%rsi,8), %edx
	jle	.LBB16_23
	jmp	.LBB16_152
.LBB16_34:
	leaq	8(%rsp), %rdi
	movsd	104(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	Mode_Decision_for_Intra4x4Macroblock
	movl	%eax, 364(%r13,%rbp)
	jmp	.LBB16_78
.LBB16_35:
	callq	intrapred_luma_16x16
	leaq	20(%rsp), %rdi
	callq	find_sad_16x16
	movl	20(%rsp), %edi
	callq	dct_luma_16x16
	movl	%eax, 364(%r13,%rbp)
	jmp	.LBB16_78
.LBB16_36:
	leaq	8(%rsp), %rdi
	movsd	104(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	Mode_Decision_for_new_Intra8x8Macroblock
	movl	%eax, 364(%r13,%rbp)
	jmp	.LBB16_78
.LBB16_37:                              # %.preheader220
	movq	img(%rip), %rdx
	movslq	192(%rdx), %rax
	movq	imgY_org(%rip), %rsi
	movq	enc_picture(%rip), %rcx
	movslq	180(%rdx), %r8
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movslq	196(%rdx), %r9
	movq	6440(%rcx), %r13
	leaq	15(%rax), %rbp
	leaq	16(%rax), %r11
	movl	$16, %edi
	xorl	%ebx, %ebx
	leaq	8(%rax), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%r11, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB16_38:                              # %.lr.ph276
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_45 Depth 2
                                        #     Child Loop BB16_48 Depth 2
                                        #     Child Loop BB16_51 Depth 2
	movslq	%ebx, %rcx
	leaq	(%r8,%rcx), %rdx
	addq	%r9, %rcx
	cmpq	$16, %rdi
	movq	(%rsi,%rcx,8), %r10
	movq	(%r13,%rdx,8), %r14
	movq	%rax, %rcx
	jb	.LBB16_50
# BB#39:                                # %min.iters.checked
                                        #   in Loop: Header=BB16_38 Depth=1
	testq	%rdi, %rdi
	movq	%rax, %rcx
	je	.LBB16_50
# BB#40:                                # %vector.memcheck
                                        #   in Loop: Header=BB16_38 Depth=1
	leaq	(%r14,%rax,2), %rcx
	leaq	(%r10,%r11,2), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB16_42
# BB#41:                                # %vector.memcheck
                                        #   in Loop: Header=BB16_38 Depth=1
	leaq	(%r14,%r11,2), %rcx
	leaq	(%r10,%rax,2), %rdx
	cmpq	%rcx, %rdx
	movq	%rax, %rcx
	jb	.LBB16_50
.LBB16_42:                              # %vector.body.preheader
                                        #   in Loop: Header=BB16_38 Depth=1
	movl	$1, %ecx
	testq	%rcx, %rcx
	je	.LBB16_43
# BB#44:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB16_38 Depth=1
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%r14,%rcx,2), %rdx
	leaq	(%r10,%rcx,2), %rsi
	movq	$-1, %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB16_45:                              # %vector.body.prol
                                        #   Parent Loop BB16_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%rsi,%rcx,2), %xmm0
	movups	(%rsi,%rcx,2), %xmm1
	movupd	%xmm0, -16(%rdx,%rcx,2)
	movups	%xmm1, (%rdx,%rcx,2)
	addq	$16, %rcx
	incq	%rdi
	jne	.LBB16_45
	jmp	.LBB16_46
.LBB16_43:                              #   in Loop: Header=BB16_38 Depth=1
	xorl	%ecx, %ecx
.LBB16_46:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB16_38 Depth=1
	movb	$1, %dl
	testb	%dl, %dl
	jne	.LBB16_49
# BB#47:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB16_38 Depth=1
	movl	$16, %r11d
	subq	%rcx, %r11
	addq	%rax, %rcx
	leaq	112(%r14,%rcx,2), %r15
	leaq	112(%r10,%rcx,2), %r12
	.p2align	4, 0x90
.LBB16_48:                              # %vector.body
                                        #   Parent Loop BB16_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%r12), %xmm0
	movups	-96(%r12), %xmm1
	movups	%xmm0, -112(%r15)
	movups	%xmm1, -96(%r15)
	movups	-80(%r12), %xmm0
	movups	-64(%r12), %xmm1
	movups	%xmm0, -80(%r15)
	movups	%xmm1, -64(%r15)
	movups	-48(%r12), %xmm0
	movups	-32(%r12), %xmm1
	movups	%xmm0, -48(%r15)
	movups	%xmm1, -32(%r15)
	movupd	-16(%r12), %xmm0
	movups	(%r12), %xmm1
	movupd	%xmm0, -16(%r15)
	movups	%xmm1, (%r15)
	subq	$-128, %r15
	subq	$-128, %r12
	addq	$-64, %r11
	jne	.LBB16_48
.LBB16_49:                              # %middle.block
                                        #   in Loop: Header=BB16_38 Depth=1
	movl	$16, %edi
	cmpq	%rdi, %rdi
	movq	24(%rsp), %r11          # 8-byte Reload
	movq	%r11, %rcx
	movq	32(%rsp), %rsi          # 8-byte Reload
	je	.LBB16_52
	.p2align	4, 0x90
.LBB16_50:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB16_38 Depth=1
	decq	%rcx
	.p2align	4, 0x90
.LBB16_51:                              # %scalar.ph
                                        #   Parent Loop BB16_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	2(%r10,%rcx,2), %edx
	movw	%dx, 2(%r14,%rcx,2)
	incq	%rcx
	cmpq	%rbp, %rcx
	jl	.LBB16_51
.LBB16_52:                              # %._crit_edge277
                                        #   in Loop: Header=BB16_38 Depth=1
	incl	%ebx
	cmpl	$16, %ebx
	jne	.LBB16_38
# BB#53:
	movq	88(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 15536(%rax)
	movq	96(%rsp), %r13          # 8-byte Reload
	je	.LBB16_74
# BB#54:                                # %.preheader219
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	15548(%rax), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB16_74
# BB#55:                                # %.lr.ph273
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	15544(%rax), %ecx
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	movq	imgUV_org(%rip), %rcx
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	movq	enc_picture(%rip), %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movq	img(%rip), %rcx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movslq	200(%rax), %r12
	leaq	1(%r12), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	24(%r12), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movq	%r12, 136(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB16_56:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_69 Depth 2
                                        #     Child Loop BB16_72 Depth 2
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB16_73
# BB#57:                                # %.lr.ph270
                                        #   in Loop: Header=BB16_56 Depth=1
	movq	88(%rsp), %rsi          # 8-byte Reload
	movslq	188(%rsi), %rcx
	movslq	%edx, %rax
	addq	%rax, %rcx
	movslq	204(%rsi), %rsi
	addq	%rax, %rsi
	movq	216(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	(%rax,%rsi,8), %rax
	movq	208(%rsp), %rbp         # 8-byte Reload
	movq	6472(%rbp), %rbp
	movq	(%rbp), %rbx
	movq	8(%rbp), %rbp
	movq	(%rbx,%rcx,8), %r9
	movq	(%rdi,%rsi,8), %r8
	movq	(%rbp,%rcx,8), %r10
	movq	200(%rsp), %rsi         # 8-byte Reload
	movslq	15544(%rsi), %rcx
	movslq	200(%rsi), %r11
	addq	%rcx, %r11
	movq	80(%rsp), %rcx          # 8-byte Reload
	cmpq	%r11, %rcx
	movq	%r11, %r15
	cmovgeq	%rcx, %r15
	subq	%r12, %r15
	cmpq	$16, %r15
	movq	%r12, %rsi
	jb	.LBB16_72
# BB#58:                                # %min.iters.checked376
                                        #   in Loop: Header=BB16_56 Depth=1
	movq	%r15, %r14
	andq	$-16, %r14
	movq	%r12, %rsi
	je	.LBB16_72
# BB#59:                                # %vector.memcheck417
                                        #   in Loop: Header=BB16_56 Depth=1
	leaq	(%r9,%r12,2), %r13
	movq	80(%rsp), %rdi          # 8-byte Reload
	cmpq	%r11, %rdi
	movq	%r11, %rsi
	cmovgeq	%rdi, %rsi
	leaq	(%r9,%rsi,2), %rdi
	leaq	(%r10,%r12,2), %rbp
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	leaq	(%r10,%rsi,2), %rbx
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%rax,%rcx,2), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	leaq	(%rax,%rsi,2), %r12
	movq	%r12, 112(%rsp)         # 8-byte Spill
	leaq	(%r8,%rsi,2), %rsi
	movq	%rsi, 184(%rsp)         # 8-byte Spill
	movq	%r13, %rcx
	cmpq	%rbx, %rcx
	sbbb	%bl, %bl
	cmpq	%rdi, %rbp
	sbbb	%r13b, %r13b
	andb	%bl, %r13b
	cmpq	%r12, %rcx
	sbbb	%bpl, %bpl
	movq	128(%rsp), %r12         # 8-byte Reload
	cmpq	%rdi, %r12
	sbbb	%bl, %bl
	movb	%bl, 15(%rsp)           # 1-byte Spill
	cmpq	%rsi, %rcx
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%r8,%rcx,2), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	sbbb	%bl, %bl
	cmpq	%rdi, %rcx
	sbbb	%cl, %cl
	movb	%cl, 14(%rsp)           # 1-byte Spill
	movq	120(%rsp), %rsi         # 8-byte Reload
	cmpq	112(%rsp), %rsi         # 8-byte Folded Reload
	sbbb	%cl, %cl
	movb	%cl, 112(%rsp)          # 1-byte Spill
	movq	192(%rsp), %rdi         # 8-byte Reload
	cmpq	%rdi, %r12
	movq	136(%rsp), %r12         # 8-byte Reload
	sbbb	%cl, %cl
	movb	%cl, 128(%rsp)          # 1-byte Spill
	cmpq	184(%rsp), %rsi         # 8-byte Folded Reload
	sbbb	%cl, %cl
	movb	%cl, 120(%rsp)          # 1-byte Spill
	cmpq	%rdi, 176(%rsp)         # 8-byte Folded Reload
	sbbb	%cl, %cl
	testb	$1, %r13b
	movq	96(%rsp), %r13          # 8-byte Reload
	movq	%r12, %rsi
	jne	.LBB16_72
# BB#60:                                # %vector.memcheck417
                                        #   in Loop: Header=BB16_56 Depth=1
	andb	15(%rsp), %bpl          # 1-byte Folded Reload
	andb	$1, %bpl
	movq	%r12, %rsi
	jne	.LBB16_72
# BB#61:                                # %vector.memcheck417
                                        #   in Loop: Header=BB16_56 Depth=1
	andb	14(%rsp), %bl           # 1-byte Folded Reload
	andb	$1, %bl
	movq	%r12, %rsi
	jne	.LBB16_72
# BB#62:                                # %vector.memcheck417
                                        #   in Loop: Header=BB16_56 Depth=1
	movb	112(%rsp), %bl          # 1-byte Reload
	andb	128(%rsp), %bl          # 1-byte Folded Reload
	andb	$1, %bl
	movq	%r12, %rsi
	jne	.LBB16_72
# BB#63:                                # %vector.memcheck417
                                        #   in Loop: Header=BB16_56 Depth=1
	movl	%ecx, %ebx
	movb	120(%rsp), %cl          # 1-byte Reload
	andb	%bl, %cl
	andb	$1, %cl
	movq	%r12, %rsi
	jne	.LBB16_72
# BB#64:                                # %vector.body371.preheader
                                        #   in Loop: Header=BB16_56 Depth=1
	leaq	-16(%r14), %rsi
	movq	%rsi, %rcx
	shrq	$4, %rcx
	btl	$4, %esi
	jb	.LBB16_65
# BB#66:                                # %vector.body371.prol
                                        #   in Loop: Header=BB16_56 Depth=1
	movups	(%rax,%r12,2), %xmm0
	movups	16(%rax,%r12,2), %xmm1
	movups	%xmm0, (%r9,%r12,2)
	movups	%xmm1, 16(%r9,%r12,2)
	movupd	(%r8,%r12,2), %xmm0
	movups	16(%r8,%r12,2), %xmm1
	movupd	%xmm0, (%r10,%r12,2)
	movups	%xmm1, 16(%r10,%r12,2)
	movl	$16, %esi
	testq	%rcx, %rcx
	jne	.LBB16_68
	jmp	.LBB16_70
.LBB16_65:                              #   in Loop: Header=BB16_56 Depth=1
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.LBB16_70
.LBB16_68:                              # %vector.body371.preheader.new
                                        #   in Loop: Header=BB16_56 Depth=1
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	(%r10,%rcx,2), %rbp
	leaq	(%r8,%rcx,2), %rdi
	leaq	(%r9,%rcx,2), %rbx
	leaq	(%rax,%rcx,2), %rcx
	.p2align	4, 0x90
.LBB16_69:                              # %vector.body371
                                        #   Parent Loop BB16_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rcx,%rsi,2), %xmm0
	movups	-32(%rcx,%rsi,2), %xmm1
	movups	%xmm0, -48(%rbx,%rsi,2)
	movups	%xmm1, -32(%rbx,%rsi,2)
	movups	-48(%rdi,%rsi,2), %xmm0
	movups	-32(%rdi,%rsi,2), %xmm1
	movups	%xmm0, -48(%rbp,%rsi,2)
	movups	%xmm1, -32(%rbp,%rsi,2)
	movups	-16(%rcx,%rsi,2), %xmm0
	movups	(%rcx,%rsi,2), %xmm1
	movups	%xmm0, -16(%rbx,%rsi,2)
	movups	%xmm1, (%rbx,%rsi,2)
	movupd	-16(%rdi,%rsi,2), %xmm0
	movups	(%rdi,%rsi,2), %xmm1
	movupd	%xmm0, -16(%rbp,%rsi,2)
	movups	%xmm1, (%rbp,%rsi,2)
	addq	$32, %rsi
	cmpq	%rsi, %r14
	jne	.LBB16_69
.LBB16_70:                              # %middle.block372
                                        #   in Loop: Header=BB16_56 Depth=1
	cmpq	%r14, %r15
	je	.LBB16_73
# BB#71:                                #   in Loop: Header=BB16_56 Depth=1
	addq	%r12, %r14
	movq	%r14, %rsi
	.p2align	4, 0x90
.LBB16_72:                              # %scalar.ph373
                                        #   Parent Loop BB16_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rax,%rsi,2), %ecx
	movw	%cx, (%r9,%rsi,2)
	movzwl	(%r8,%rsi,2), %ecx
	movw	%cx, (%r10,%rsi,2)
	incq	%rsi
	cmpq	%r11, %rsi
	jl	.LBB16_72
.LBB16_73:                              # %._crit_edge271
                                        #   in Loop: Header=BB16_56 Depth=1
	incl	%edx
	cmpl	32(%rsp), %edx          # 4-byte Folded Reload
	jl	.LBB16_56
.LBB16_74:                              # %.preheader218
	movq	img(%rip), %rax
	cmpl	$-4, 15528(%rax)
	jle	.LBB16_78
# BB#75:                                # %.lr.ph265
	movq	152(%rax), %rcx
	movq	$-1, %rdx
	.p2align	4, 0x90
.LBB16_76:                              # =>This Inner Loop Header: Depth=1
	movslq	12(%rax), %rsi
	movq	(%rcx,%rsi,8), %rsi
	movq	(%rsi), %rsi
	movl	$16, 4(%rsi,%rdx,4)
	movslq	15528(%rax), %rsi
	leaq	3(%rsi), %rdi
	incq	%rdx
	cmpq	%rdi, %rdx
	jl	.LBB16_76
# BB#77:                                # %._crit_edge266
	cmpl	$-4, %esi
	jle	.LBB16_78
# BB#153:                               # %.lr.ph265.1
	movq	152(%rax), %rcx
	movq	$-1, %rdx
	.p2align	4, 0x90
.LBB16_154:                             # =>This Inner Loop Header: Depth=1
	movslq	12(%rax), %rsi
	movq	(%rcx,%rsi,8), %rsi
	movq	8(%rsi), %rsi
	movl	$16, 4(%rsi,%rdx,4)
	movslq	15528(%rax), %rsi
	leaq	3(%rsi), %rdi
	incq	%rdx
	cmpq	%rdi, %rdx
	jl	.LBB16_154
# BB#155:                               # %._crit_edge266.1
	cmpl	$-3, %esi
	jl	.LBB16_78
# BB#156:                               # %.lr.ph265.2
	movq	152(%rax), %rcx
	movq	$-1, %rdx
.LBB16_157:                             # =>This Inner Loop Header: Depth=1
	movslq	12(%rax), %rsi
	movq	(%rcx,%rsi,8), %rsi
	movq	16(%rsi), %rsi
	movl	$16, 4(%rsi,%rdx,4)
	movslq	15528(%rax), %rsi
	leaq	3(%rsi), %rdi
	incq	%rdx
	cmpq	%rdi, %rdx
	jl	.LBB16_157
# BB#158:                               # %._crit_edge266.2
	cmpl	$-3, %esi
	jl	.LBB16_78
# BB#159:                               # %.lr.ph265.3
	movq	152(%rax), %rcx
	movq	$-1, %rdx
.LBB16_160:                             # =>This Inner Loop Header: Depth=1
	movslq	12(%rax), %rsi
	movq	(%rcx,%rsi,8), %rsi
	movq	24(%rsi), %rsi
	movl	$16, 4(%rsi,%rdx,4)
	movslq	15528(%rax), %rsi
	addq	$3, %rsi
	incq	%rdx
	cmpq	%rsi, %rdx
	jl	.LBB16_160
	jmp	.LBB16_78
.LBB16_30:                              # %.thread
	xorl	%eax, %eax
	cmpl	$1, 472(%r13,%rbp)
	je	.LBB16_152
.LBB16_78:                              # %.critedge212
	movq	input(%rip), %rax
	cmpl	$3, 4168(%rax)
	jne	.LBB16_79
# BB#80:
	movq	img(%rip), %rcx
	cmpl	$1, 20(%rcx)
	movq	48(%rsp), %rbp          # 8-byte Reload
	je	.LBB16_82
# BB#81:
	cmpl	$10, %ebp
	movl	$-1, %edi
	cmovel	20(%rsp), %edi
	callq	compute_residue_mb
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	jne	.LBB16_84
	jmp	.LBB16_83
.LBB16_79:
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB16_82:
	cmpl	$0, 5116(%rax)
	je	.LBB16_83
.LBB16_84:
	movq	img(%rip), %rbx
	cmpl	$10, %ebp
	jne	.LBB16_86
# BB#85:
	movslq	20(%rsp), %rax
	shlq	$9, %rax
	leaq	4816(%rbx,%rax), %rsi
	jmp	.LBB16_87
.LBB16_83:                              # %._crit_edge321
	movq	img(%rip), %rbx
	jmp	.LBB16_88
.LBB16_86:
	leaq	12624(%rbx), %rsi
.LBB16_87:
	movl	$pred, %edi
	movl	$512, %edx              # imm = 0x200
	callq	memcpy
.LBB16_88:
	movl	$0, 15244(%rbx)
	movl	$0, 8(%rsp)
	cmpl	$14, %ebp
	je	.LBB16_91
# BB#89:
	movl	15536(%rbx), %eax
	testl	%eax, %eax
	je	.LBB16_91
# BB#90:
	leaq	8(%rsp), %rdi
	callq	ChromaResidualCoding
.LBB16_91:
	cmpl	$10, %ebp
	movq	64(%rsp), %rax          # 8-byte Reload
	jne	.LBB16_93
# BB#92:
	movl	364(%r13,%rax), %eax
	testb	$15, %al
	movl	$13, %ecx
	movl	$1, %edx
	cmovnel	%ecx, %edx
	shrl	$2, %eax
	andl	$12, %eax
	addl	20(%rsp), %eax
	addl	%edx, %eax
	movq	img(%rip), %rcx
	movl	%eax, 15244(%rcx)
.LBB16_93:
	movq	input(%rip), %rax
	cmpl	$3, 4168(%rax)
	movq	img(%rip), %r10
	jne	.LBB16_94
# BB#105:
	cmpl	$1, 20(%r10)
	jne	.LBB16_106
.LBB16_94:                              # %.preheader213
	movslq	192(%r10), %r12
	movq	imgY_org(%rip), %r9
	movq	enc_picture(%rip), %rax
	movslq	180(%r10), %r11
	movslq	196(%r10), %r14
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	6440(%rax), %r15
	movq	14232(%r10), %rcx
	leaq	15(%r12), %rdx
	decq	%r12
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_95:                              # %.lr.ph234
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_96 Depth 2
	movslq	%r13d, %rax
	leaq	(%r11,%rax), %rbp
	addq	%r14, %rax
	movq	(%r9,%rax,8), %rdi
	movq	(%r15,%rbp,8), %rbp
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB16_96:                              #   Parent Loop BB16_95 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	2(%rdi,%rax,2), %esi
	movzwl	2(%rbp,%rax,2), %r8d
	subq	%r8, %rsi
	movslq	(%rcx,%rsi,4), %rsi
	addq	%rsi, %rbx
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB16_96
# BB#97:                                # %._crit_edge235
                                        #   in Loop: Header=BB16_95 Depth=1
	incl	%r13d
	cmpl	$16, %r13d
	jne	.LBB16_95
# BB#98:
	cmpl	$0, 15536(%r10)
	je	.LBB16_121
# BB#99:                                # %.preheader
	movl	15548(%r10), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB16_121
# BB#100:                               # %.lr.ph229
	movslq	200(%r10), %rax
	movl	15544(%r10), %r14d
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leal	(%r14,%rax), %eax
	movq	imgUV_org(%rip), %r15
	movslq	%eax, %r13
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB16_101:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_103 Depth 2
	testl	%r14d, %r14d
	jle	.LBB16_104
# BB#102:                               # %.lr.ph
                                        #   in Loop: Header=BB16_101 Depth=1
	movslq	188(%r10), %rax
	movslq	%r12d, %rcx
	addq	%rcx, %rax
	movslq	204(%r10), %rsi
	addq	%rcx, %rsi
	movq	(%r15), %rcx
	movq	8(%r15), %rdi
	movq	(%rcx,%rsi,8), %r9
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	6472(%rcx), %rdx
	movq	(%rdx), %rbp
	movq	8(%rdx), %r8
	movq	(%rbp,%rax,8), %r11
	movq	(%rdi,%rsi,8), %rdi
	movq	(%r8,%rax,8), %rbp
	movq	14232(%r10), %rsi
	movq	24(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB16_103:                             #   Parent Loop BB16_101 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%r9,%rax,2), %ecx
	movzwl	(%r11,%rax,2), %edx
	subq	%rdx, %rcx
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rbx, %rcx
	movzwl	(%rdi,%rax,2), %edx
	movzwl	(%rbp,%rax,2), %ebx
	subq	%rbx, %rdx
	movslq	(%rsi,%rdx,4), %rbx
	addq	%rcx, %rbx
	incq	%rax
	cmpq	%r13, %rax
	jl	.LBB16_103
.LBB16_104:                             # %._crit_edge
                                        #   in Loop: Header=BB16_101 Depth=1
	incl	%r12d
	cmpl	56(%rsp), %r12d         # 4-byte Folded Reload
	jl	.LBB16_101
	jmp	.LBB16_121
.LBB16_106:                             # %.preheader216
	movl	4728(%rax), %eax
	testl	%eax, %eax
	jle	.LBB16_107
# BB#108:                               # %.split.us.preheader.preheader
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_109:                             # %.split.us.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_110 Depth 2
                                        #       Child Loop BB16_111 Depth 3
	movl	%r14d, %edi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	decode_one_mb
	movq	img(%rip), %r10
	movslq	192(%r10), %r15
	movq	imgY_org(%rip), %r8
	movq	decs(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r14,8), %r9
	movq	14232(%r10), %r13
	movslq	196(%r10), %r11
	leaq	15(%r15), %rbp
	decq	%r15
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB16_110:                             # %.split.us
                                        #   Parent Loop BB16_109 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB16_111 Depth 3
	movslq	%r12d, %rax
	addq	%r11, %rax
	movq	(%r8,%rax,8), %rcx
	movq	(%r9,%rax,8), %rdi
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB16_111:                             #   Parent Loop BB16_109 Depth=1
                                        #     Parent Loop BB16_110 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	2(%rcx,%rax,2), %edx
	movzwl	2(%rdi,%rax,2), %esi
	subq	%rsi, %rdx
	movslq	(%r13,%rdx,4), %rdx
	addq	%rdx, %rbx
	incq	%rax
	cmpq	%rbp, %rax
	jl	.LBB16_111
# BB#112:                               # %._crit_edge253.us
                                        #   in Loop: Header=BB16_110 Depth=2
	incl	%r12d
	cmpl	$16, %r12d
	jne	.LBB16_110
# BB#113:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB16_109 Depth=1
	incq	%r14
	movq	input(%rip), %rax
	movslq	4728(%rax), %rax
	cmpq	%rax, %r14
	jl	.LBB16_109
	jmp	.LBB16_114
.LBB16_107:
	xorl	%ebx, %ebx
.LBB16_114:                             # %._crit_edge261
	movslq	%eax, %rcx
	movq	%rbx, %rax
	cqto
	idivq	%rcx
	movq	%rax, %rbx
	cmpl	$0, 15536(%r10)
	je	.LBB16_121
# BB#115:                               # %.preheader214
	movl	15548(%r10), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB16_121
# BB#116:                               # %.lr.ph247
	movslq	200(%r10), %rax
	movl	15544(%r10), %r14d
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leal	(%r14,%rax), %eax
	movq	imgUV_org(%rip), %r15
	movq	enc_picture(%rip), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movslq	%eax, %r9
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB16_117:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_119 Depth 2
	testl	%r14d, %r14d
	jle	.LBB16_120
# BB#118:                               # %.lr.ph242
                                        #   in Loop: Header=BB16_117 Depth=1
	movslq	188(%r10), %rdi
	movslq	%r12d, %rax
	addq	%rax, %rdi
	movslq	204(%r10), %rsi
	addq	%rax, %rsi
	movq	(%r15), %rax
	movq	8(%r15), %r8
	movq	(%rax,%rsi,8), %r11
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	6472(%rax), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %r13
	movq	(%rbp,%rdi,8), %rax
	movq	(%r8,%rsi,8), %rsi
	movq	(%r13,%rdi,8), %rdi
	movq	14232(%r10), %rbp
	movq	32(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB16_119:                             #   Parent Loop BB16_117 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%r11,%r8,2), %edx
	movzwl	(%rax,%r8,2), %ecx
	subq	%rcx, %rdx
	movslq	(%rbp,%rdx,4), %rcx
	addq	%rbx, %rcx
	movzwl	(%rsi,%r8,2), %edx
	movzwl	(%rdi,%r8,2), %ebx
	subq	%rbx, %rdx
	movslq	(%rbp,%rdx,4), %rbx
	addq	%rcx, %rbx
	incq	%r8
	cmpq	%r9, %r8
	jl	.LBB16_119
.LBB16_120:                             # %._crit_edge243
                                        #   in Loop: Header=BB16_117 Depth=1
	incl	%r12d
	cmpl	56(%rsp), %r12d         # 4-byte Folded Reload
	jl	.LBB16_117
.LBB16_121:                             # %.loopexit
	movq	cs_cm(%rip), %rdi
	callq	store_coding_state
	cmpb	$0, 224(%rsp)           # 1-byte Folded Reload
	je	.LBB16_127
# BB#122:
	movq	96(%rsp), %r12          # 8-byte Reload
	movq	64(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 72(%r12,%rax)
	movq	152(%rsp), %r14         # 8-byte Reload
	movq	144(%rsp), %r15         # 8-byte Reload
	je	.LBB16_123
.LBB16_125:
	movq	img(%rip), %rax
	movl	144(%rax), %ebp
	leaq	44(%rsp), %rsi
	movl	$1, %edi
	callq	writeMBLayer
	movl	%eax, 16(%rsp)
	movl	8(%rsp), %esi
	leaq	40(%rsp), %rdx
	leaq	8(%rsp), %rcx
	movl	%ebp, %edi
	callq	ue_linfo
	movl	40(%rsp), %eax
	subl	%eax, 16(%rsp)
	movq	img(%rip), %rax
	movl	%ebp, 144(%rax)
	jmp	.LBB16_128
.LBB16_127:
	leaq	44(%rsp), %rsi
	movl	$1, %edi
	callq	writeMBLayer
	movl	%eax, 16(%rsp)
	movq	152(%rsp), %r14         # 8-byte Reload
	movq	144(%rsp), %r15         # 8-byte Reload
	movq	96(%rsp), %r12          # 8-byte Reload
.LBB16_128:
	movq	64(%rsp), %rbp          # 8-byte Reload
.LBB16_129:
	movq	cs_cm(%rip), %rdi
	callq	reset_coding_state
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rbx, %xmm1
	cvtsi2sdl	16(%rsp), %xmm2
	movsd	.LCPI16_0(%rip), %xmm0  # xmm0 = mem[0],zero
	maxsd	%xmm2, %xmm0
	mulsd	104(%rsp), %xmm0        # 8-byte Folded Reload
	addsd	%xmm1, %xmm0
	movsd	(%r15), %xmm1           # xmm1 = mem[0],zero
	movq	img(%rip), %rcx
	ucomisd	%xmm1, %xmm0
	jae	.LBB16_133
# BB#130:
	cmpl	$0, 44(%rcx)
	jne	.LBB16_136
# BB#131:
	testq	%rbx, %rbx
	je	.LBB16_136
# BB#132:
	cmpl	$1, 15540(%rcx)
	jne	.LBB16_136
.LBB16_133:                             # %._crit_edge337
	xorl	%eax, %eax
	movl	20(%rcx), %edx
	orl	48(%rsp), %edx          # 4-byte Folded Reload
	jne	.LBB16_152
# BB#134:                               # %._crit_edge337
	ucomisd	%xmm1, %xmm0
	jne	.LBB16_152
	jp	.LBB16_152
# BB#135:
	movq	input(%rip), %rdx
	cmpl	$99, (%rdx)
	jg	.LBB16_152
.LBB16_136:
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jne	.LBB16_151
# BB#137:
	movl	15268(%rcx), %eax
	testl	%eax, %eax
	je	.LBB16_151
# BB#138:
	movl	20(%rcx), %eax
	cmpl	$1, %eax
	jne	.LBB16_140
# BB#139:
	cmpl	$0, 364(%r12,%rbp)
	jne	.LBB16_151
.LBB16_140:
	movslq	12(%rcx), %rdx
	testb	$1, %dl
	je	.LBB16_151
# BB#141:
	movq	160(%rsp), %rsi         # 8-byte Reload
	cmpl	$0, 72(%rsi)
	je	.LBB16_142
.LBB16_151:
	movsd	%xmm0, (%r15)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	44(%rsp), %xmm0
	mulsd	104(%rsp), %xmm0        # 8-byte Folded Reload
	movsd	%xmm0, (%r14)
	movl	$1, %eax
.LBB16_152:                             # %.critedge
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_123:
	cmpl	$1, 76(%rsp)            # 4-byte Folded Reload
	jne	.LBB16_126
# BB#124:
	cmpl	$0, 364(%r12,%rax)
	jne	.LBB16_125
.LBB16_126:
	movq	img(%rip), %rax
	movl	144(%rax), %edi
	incl	%edi
	movl	8(%rsp), %esi
	leaq	16(%rsp), %rdx
	leaq	8(%rsp), %rbp
	movq	%rbp, %rcx
	callq	ue_linfo
	movq	img(%rip), %rax
	movl	144(%rax), %edi
	movl	8(%rsp), %esi
	leaq	40(%rsp), %rdx
	movq	%rbp, %rcx
	movq	64(%rsp), %rbp          # 8-byte Reload
	callq	ue_linfo
	movl	40(%rsp), %eax
	subl	%eax, 16(%rsp)
	jmp	.LBB16_129
.LBB16_142:
	cmpl	$1, %eax
	jne	.LBB16_144
# BB#143:
	movq	160(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 364(%rax)
	jne	.LBB16_151
.LBB16_144:
	movq	14224(%rcx), %rax
	imulq	$536, %rdx, %rcx        # imm = 0x218
	cmpl	$0, 452(%rax,%rcx)
	je	.LBB16_146
# BB#145:
	leaq	436(%rax,%rcx), %rcx
	jmp	.LBB16_149
.LBB16_146:
	cmpl	$0, 456(%rax,%rcx)
	je	.LBB16_147
# BB#148:
	leaq	440(%rax,%rcx), %rcx
.LBB16_149:                             # %.sink.split.i
	movslq	(%rcx), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	movl	424(%rax,%rcx), %ecx
.LBB16_150:                             # %field_flag_inference.exit
	xorl	%eax, %eax
	cmpl	424(%r12,%rbp), %ecx
	je	.LBB16_151
	jmp	.LBB16_152
.LBB16_147:
	xorl	%ecx, %ecx
	jmp	.LBB16_150
.Lfunc_end16:
	.size	RDCost_for_macroblocks, .Lfunc_end16-RDCost_for_macroblocks
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI16_0:
	.quad	.LBB16_33
	.quad	.LBB16_34
	.quad	.LBB16_35
	.quad	.LBB16_78
	.quad	.LBB16_78
	.quad	.LBB16_36
	.quad	.LBB16_37

	.text
	.globl	field_flag_inference
	.p2align	4, 0x90
	.type	field_flag_inference,@function
field_flag_inference:                   # @field_flag_inference
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rcx
	movq	14224(%rcx), %rax
	movslq	12(%rcx), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	cmpl	$0, 452(%rax,%rcx)
	je	.LBB17_2
# BB#1:
	leaq	436(%rax,%rcx), %rcx
	jmp	.LBB17_5
.LBB17_2:
	cmpl	$0, 456(%rax,%rcx)
	je	.LBB17_3
# BB#4:
	leaq	440(%rax,%rcx), %rcx
.LBB17_5:                               # %.sink.split
	movslq	(%rcx), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	movl	424(%rax,%rcx), %eax
	retq
.LBB17_3:
	xorl	%eax, %eax
	retq
.Lfunc_end17:
	.size	field_flag_inference, .Lfunc_end17-field_flag_inference
	.cfi_endproc

	.globl	store_adaptive_rounding_parameters
	.p2align	4, 0x90
	.type	store_adaptive_rounding_parameters,@function
store_adaptive_rounding_parameters:     # @store_adaptive_rounding_parameters
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi161:
	.cfi_def_cfa_offset 32
.Lcfi162:
	.cfi_offset %rbx, -24
.Lcfi163:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movl	%edi, %ebx
	cmpl	$0, 472(%r14)
	je	.LBB18_3
# BB#1:
	cmpl	$8, %ebx
	jne	.LBB18_5
# BB#2:
	movq	bestInterFAdjust8x8(%rip), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14184(%rax), %rax
	movq	16(%rax), %rax
	jmp	.LBB18_14
.LBB18_3:
	cmpl	$8, %ebx
	jne	.LBB18_8
# BB#4:
	movq	bestInterFAdjust4x4(%rip), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14176(%rax), %rax
	movq	24(%rax), %rax
	jmp	.LBB18_14
.LBB18_5:
	cmpl	$13, %ebx
	ja	.LBB18_11
# BB#6:
	movl	$9728, %eax             # imm = 0x2600
	btl	%ebx, %eax
	jae	.LBB18_11
# BB#7:
	movq	bestIntraFAdjust8x8(%rip), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14184(%rax), %rax
	movq	8(%rax), %rax
	jmp	.LBB18_14
.LBB18_8:
	cmpl	$13, %ebx
	ja	.LBB18_12
# BB#9:
	movl	$9728, %eax             # imm = 0x2600
	btl	%ebx, %eax
	jae	.LBB18_12
# BB#10:
	movq	bestIntraFAdjust4x4(%rip), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14176(%rax), %rax
	xorl	%ecx, %ecx
	cmpl	$9, %ebx
	sete	%cl
	movq	(%rax,%rcx,8), %rax
	jmp	.LBB18_14
.LBB18_11:
	movq	bestInterFAdjust8x8(%rip), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14184(%rax), %rax
	jmp	.LBB18_13
.LBB18_12:
	movq	bestInterFAdjust4x4(%rip), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14176(%rax), %rax
.LBB18_13:
	movq	(%rax), %rax
.LBB18_14:
	movq	(%rax), %rsi
	movl	$1024, %edx             # imm = 0x400
	callq	memcpy
	movq	input(%rip), %rax
	cmpl	$0, 5660(%rax)
	je	.LBB18_32
# BB#15:
	cmpl	$8, %ebx
	jne	.LBB18_20
# BB#16:
	movl	472(%r14), %eax
	testl	%eax, %eax
	je	.LBB18_20
# BB#17:                                # %.preheader
	movq	img(%rip), %rax
	cmpl	$0, 15548(%rax)
	jle	.LBB18_32
# BB#18:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB18_19:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	bestInterFAdjust4x4Cr(%rip), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rdi
	movq	14200(%rax), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movslq	15544(%rax), %rdx
	shlq	$2, %rdx
	callq	memcpy
	movq	bestInterFAdjust4x4Cr(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movq	14200(%rax), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movslq	15544(%rax), %rdx
	shlq	$2, %rdx
	callq	memcpy
	incq	%rbx
	movq	img(%rip), %rax
	movslq	15548(%rax), %rcx
	cmpq	%rcx, %rbx
	jl	.LBB18_19
	jmp	.LBB18_32
.LBB18_20:
	cmpl	$8, %ebx
	jne	.LBB18_24
# BB#21:                                # %.preheader41
	movq	img(%rip), %rax
	cmpl	$0, 15548(%rax)
	jle	.LBB18_32
# BB#22:                                # %.lr.ph49.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB18_23:                              # %.lr.ph49
                                        # =>This Inner Loop Header: Depth=1
	movq	bestInterFAdjust4x4Cr(%rip), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rdi
	movq	14192(%rax), %rcx
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movslq	15544(%rax), %rdx
	shlq	$2, %rdx
	callq	memcpy
	movq	bestInterFAdjust4x4Cr(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movq	14192(%rax), %rcx
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movslq	15544(%rax), %rdx
	shlq	$2, %rdx
	callq	memcpy
	incq	%rbx
	movq	img(%rip), %rax
	movslq	15548(%rax), %rcx
	cmpq	%rcx, %rbx
	jl	.LBB18_23
	jmp	.LBB18_32
.LBB18_24:
	cmpl	$13, %ebx
	ja	.LBB18_29
# BB#25:
	movl	$9728, %eax             # imm = 0x2600
	btl	%ebx, %eax
	jae	.LBB18_29
# BB#26:                                # %.preheader45
	movq	img(%rip), %rax
	cmpl	$0, 15548(%rax)
	jle	.LBB18_32
# BB#27:                                # %.lr.ph53.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB18_28:                              # %.lr.ph53
                                        # =>This Inner Loop Header: Depth=1
	movq	bestIntraFAdjust4x4Cr(%rip), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rdi
	movq	14192(%rax), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movslq	15544(%rax), %rdx
	shlq	$2, %rdx
	callq	memcpy
	movq	bestIntraFAdjust4x4Cr(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movq	14192(%rax), %rcx
	movq	8(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movslq	15544(%rax), %rdx
	shlq	$2, %rdx
	callq	memcpy
	incq	%rbx
	movq	img(%rip), %rax
	movslq	15548(%rax), %rcx
	cmpq	%rcx, %rbx
	jl	.LBB18_28
	jmp	.LBB18_32
.LBB18_29:                              # %.preheader43
	movq	img(%rip), %rax
	cmpl	$0, 15548(%rax)
	jle	.LBB18_32
# BB#30:                                # %.lr.ph51.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB18_31:                              # %.lr.ph51
                                        # =>This Inner Loop Header: Depth=1
	movq	bestInterFAdjust4x4Cr(%rip), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rdi
	movq	14192(%rax), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movslq	15544(%rax), %rdx
	shlq	$2, %rdx
	callq	memcpy
	movq	bestInterFAdjust4x4Cr(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	img(%rip), %rax
	movq	14192(%rax), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movslq	15544(%rax), %rdx
	shlq	$2, %rdx
	callq	memcpy
	incq	%rbx
	movq	img(%rip), %rax
	movslq	15548(%rax), %rcx
	cmpq	%rcx, %rbx
	jl	.LBB18_31
.LBB18_32:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	store_adaptive_rounding_parameters, .Lfunc_end18-store_adaptive_rounding_parameters
	.cfi_endproc

	.globl	store_macroblock_parameters
	.p2align	4, 0x90
	.type	store_macroblock_parameters,@function
store_macroblock_parameters:            # @store_macroblock_parameters
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi166:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi167:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi168:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi169:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi170:
	.cfi_def_cfa_offset 144
.Lcfi171:
	.cfi_offset %rbx, -56
.Lcfi172:
	.cfi_offset %r12, -48
.Lcfi173:
	.cfi_offset %r13, -40
.Lcfi174:
	.cfi_offset %r14, -32
.Lcfi175:
	.cfi_offset %r15, -24
.Lcfi176:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movq	img(%rip), %r12
	movq	14224(%r12), %rdx
	movslq	12(%r12), %rax
	imulq	$536, %rax, %rbx        # imm = 0x218
	movl	20(%r12), %ecx
	movw	%r14w, best_mode(%rip)
	movl	416(%rdx,%rbx), %eax
	movl	%eax, best_c_imode(%rip)
	movl	15244(%r12), %eax
	movl	%eax, best_i16offset(%rip)
	cmpl	$1, %r14d
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	jne	.LBB19_1
# BB#2:
	movw	480(%rdx,%rbx), %ax
	jmp	.LBB19_3
.LBB19_1:
	xorl	%eax, %eax
.LBB19_3:                               # %.preheader94.preheader131
	leaq	(%rdx,%rbx), %rsi
	movw	%ax, bi_pred_me(%rip)
	movups	376(%rdx,%rbx), %xmm0
	movaps	%xmm0, b8mode(%rip)
	movups	392(%rdx,%rbx), %xmm0
	movaps	%xmm0, b8pdir(%rip)
	movups	332(%rdx,%rbx), %xmm0
	movaps	%xmm0, b4_intra_pred_modes(%rip)
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movups	348(%rdx,%rbx), %xmm0
	movaps	%xmm0, b8_intra_pred_modes8x8(%rip)
	movq	128(%r12), %rax
	movslq	172(%r12), %rcx
	movq	(%rax,%rcx,8), %rdx
	movslq	168(%r12), %rdi
	movl	(%rdx,%rdi), %edx
	movl	%edx, b4_ipredmode(%rip)
	movq	136(%r12), %rdx
	movq	(%rdx,%rcx,8), %rbp
	movl	(%rbp,%rdi), %ebp
	movl	%ebp, b8_ipredmode8x8(%rip)
	movq	8(%rax,%rcx,8), %rbp
	movl	(%rbp,%rdi), %ebp
	movl	%ebp, b4_ipredmode+4(%rip)
	movq	8(%rdx,%rcx,8), %rbp
	movl	(%rbp,%rdi), %ebp
	movl	%ebp, b8_ipredmode8x8+4(%rip)
	movq	16(%rax,%rcx,8), %rbp
	movl	(%rbp,%rdi), %ebp
	movl	%ebp, b4_ipredmode+8(%rip)
	movq	16(%rdx,%rcx,8), %rbp
	movl	(%rbp,%rdi), %ebp
	movl	%ebp, b8_ipredmode8x8+8(%rip)
	movq	24(%rax,%rcx,8), %rax
	movl	(%rax,%rdi), %eax
	movl	%eax, b4_ipredmode+12(%rip)
	movq	24(%rdx,%rcx,8), %rax
	movl	(%rax,%rdi), %eax
	movl	%eax, b8_ipredmode8x8+12(%rip)
	movl	$rec_mbY, %eax
	xorl	%ecx, %ecx
	movq	enc_picture(%rip), %rdx
	.p2align	4, 0x90
.LBB19_4:                               # %.preheader94
                                        # =>This Inner Loop Header: Depth=1
	movq	6440(%rdx), %rdi
	movl	180(%r12), %ebp
	addl	%ecx, %ebp
	movslq	%ebp, %rbp
	movq	(%rdi,%rbp,8), %rdi
	movslq	176(%r12), %rbp
	movups	16(%rdi,%rbp,2), %xmm0
	movups	%xmm0, 16(%rax)
	movups	(%rdi,%rbp,2), %xmm0
	movups	%xmm0, (%rax)
	movq	6440(%rdx), %rdi
	movl	180(%r12), %ebp
	leal	1(%rcx,%rbp), %ebp
	movslq	%ebp, %rbp
	movq	(%rdi,%rbp,8), %rdi
	movslq	176(%r12), %rbp
	movups	16(%rdi,%rbp,2), %xmm0
	movups	%xmm0, 48(%rax)
	movups	(%rdi,%rbp,2), %xmm0
	movups	%xmm0, 32(%rax)
	addq	$2, %rcx
	addq	$64, %rax
	cmpq	$16, %rcx
	jne	.LBB19_4
# BB#5:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	cmpl	$3, 20(%r12)
	jne	.LBB19_8
# BB#6:
	movl	sp2_frame_indicator(%rip), %eax
	orl	si_frame_indicator(%rip), %eax
	jne	.LBB19_8
# BB#7:                                 # %.preheader92
	movq	lrec(%rip), %rax
	movslq	180(%r12), %rcx
	movq	(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+48(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+32(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+16(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec(%rip)
	movslq	180(%r12), %rcx
	movq	8(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+112(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+96(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+80(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+64(%rip)
	movslq	180(%r12), %rcx
	movq	16(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+176(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+160(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+144(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+128(%rip)
	movslq	180(%r12), %rcx
	movq	24(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+240(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+224(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+208(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+192(%rip)
	movslq	180(%r12), %rcx
	movq	32(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+304(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+288(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+272(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+256(%rip)
	movslq	180(%r12), %rcx
	movq	40(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+368(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+352(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+336(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+320(%rip)
	movslq	180(%r12), %rcx
	movq	48(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+432(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+416(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+400(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+384(%rip)
	movslq	180(%r12), %rcx
	movq	56(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+496(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+480(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+464(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+448(%rip)
	movslq	180(%r12), %rcx
	movq	64(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+560(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+544(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+528(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+512(%rip)
	movslq	180(%r12), %rcx
	movq	72(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+624(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+608(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+592(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+576(%rip)
	movslq	180(%r12), %rcx
	movq	80(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+688(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+672(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+656(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+640(%rip)
	movslq	180(%r12), %rcx
	movq	88(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+752(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+736(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+720(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+704(%rip)
	movslq	180(%r12), %rcx
	movq	96(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+816(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+800(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+784(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+768(%rip)
	movslq	180(%r12), %rcx
	movq	104(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+880(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+864(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+848(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+832(%rip)
	movslq	180(%r12), %rcx
	movq	112(%rax,%rcx,8), %rcx
	movslq	176(%r12), %rdx
	movups	48(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+944(%rip)
	movups	32(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+928(%rip)
	movups	16(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+912(%rip)
	movups	(%rcx,%rdx,4), %xmm0
	movaps	%xmm0, lrec_rec+896(%rip)
	movslq	180(%r12), %rcx
	movq	120(%rax,%rcx,8), %rax
	movslq	176(%r12), %rcx
	movups	48(%rax,%rcx,4), %xmm0
	movaps	%xmm0, lrec_rec+1008(%rip)
	movups	32(%rax,%rcx,4), %xmm0
	movaps	%xmm0, lrec_rec+992(%rip)
	movups	16(%rax,%rcx,4), %xmm0
	movaps	%xmm0, lrec_rec+976(%rip)
	movups	(%rax,%rcx,4), %xmm0
	movaps	%xmm0, lrec_rec+960(%rip)
.LBB19_8:                               # %.loopexit93
	cmpl	$0, 15260(%r12)
	je	.LBB19_10
# BB#9:
	movl	%r14d, %edi
	callq	store_adaptive_rounding_parameters
	movq	img(%rip), %r12
.LBB19_10:
	movl	%r14d, 8(%rsp)          # 4-byte Spill
	cmpl	$0, 15536(%r12)
	je	.LBB19_19
# BB#11:                                # %.preheader91
	movl	15548(%r12), %eax
	testl	%eax, %eax
	jle	.LBB19_14
# BB#12:                                # %.lr.ph107.preheader
	movl	$rec_mbU, %r15d
	movl	$rec_mbV, %ebp
	movq	enc_picture(%rip), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB19_13:                              # %.lr.ph107
                                        # =>This Inner Loop Header: Depth=1
	movq	6472(%r14), %rax
	movq	(%rax), %rax
	movl	188(%r12), %ecx
	addl	%ebx, %ecx
	movslq	%ecx, %rcx
	movslq	184(%r12), %rsi
	addq	%rsi, %rsi
	addq	(%rax,%rcx,8), %rsi
	movslq	15544(%r12), %rdx
	addq	%rdx, %rdx
	movq	%r15, %rdi
	callq	memcpy
	movq	6472(%r14), %rax
	movq	8(%rax), %rax
	movl	188(%r12), %ecx
	addl	%ebx, %ecx
	movslq	%ecx, %rcx
	movslq	184(%r12), %rsi
	addq	%rsi, %rsi
	addq	(%rax,%rcx,8), %rsi
	movslq	15544(%r12), %rdx
	addq	%rdx, %rdx
	movq	%rbp, %rdi
	callq	memcpy
	incq	%rbx
	movslq	15548(%r12), %rax
	addq	$32, %r15
	addq	$32, %rbp
	cmpq	%rax, %rbx
	jl	.LBB19_13
.LBB19_14:                              # %._crit_edge108
	cmpl	$3, 20(%r12)
	jne	.LBB19_19
# BB#15:                                # %._crit_edge108
	movl	sp2_frame_indicator(%rip), %ecx
	orl	si_frame_indicator(%rip), %ecx
	jne	.LBB19_19
# BB#16:                                # %._crit_edge108
	testl	%eax, %eax
	jle	.LBB19_19
# BB#17:                                # %.lr.ph105.preheader
	movl	$lrec_rec_U, %r15d
	movl	$lrec_rec_V, %ebp
	movq	lrec_uv(%rip), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB19_18:                              # %.lr.ph105
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movl	188(%r12), %ecx
	addl	%ebx, %ecx
	movslq	%ecx, %rcx
	movslq	184(%r12), %rsi
	shlq	$2, %rsi
	addq	(%rax,%rcx,8), %rsi
	movslq	15544(%r12), %rdx
	shlq	$2, %rdx
	movq	%r15, %rdi
	callq	memcpy
	movq	8(%r14), %rax
	movl	188(%r12), %ecx
	addl	%ebx, %ecx
	movslq	%ecx, %rcx
	movslq	184(%r12), %rsi
	shlq	$2, %rsi
	addq	(%rax,%rcx,8), %rsi
	movslq	15544(%r12), %rdx
	shlq	$2, %rdx
	movq	%rbp, %rdi
	callq	memcpy
	incq	%rbx
	movslq	15548(%r12), %rax
	addq	$64, %r15
	addq	$64, %rbp
	cmpq	%rax, %rbx
	jl	.LBB19_18
.LBB19_19:                              # %.loopexit90
	movq	%r12, (%rsp)            # 8-byte Spill
	movq	input(%rip), %rax
	cmpl	$3, 4168(%rax)
	jne	.LBB19_40
# BB#20:
	movq	(%rsp), %rcx            # 8-byte Reload
	cmpl	$1, 20(%rcx)
	je	.LBB19_40
# BB#21:                                # %.preheader87
	movslq	4728(%rax), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB19_40
# BB#22:                                # %.lr.ph103.split.us.preheader
	movq	(%rsp), %rcx            # 8-byte Reload
	movslq	180(%rcx), %rsi
	movq	decs(%rip), %rax
	movslq	176(%rcx), %rdx
	movq	8(%rax), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	24(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	15(%rdx), %rbp
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	leaq	15(%rsi), %rbx
	leaq	16(%rdx), %rsi
	movl	$16, %r12d
	xorl	%ecx, %ecx
	leaq	8(%rdx), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB19_23:                              # %.lr.ph103.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_24 Depth 2
                                        #       Child Loop BB19_33 Depth 3
                                        #       Child Loop BB19_36 Depth 3
                                        #       Child Loop BB19_26 Depth 3
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %r15
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %r8
	movq	48(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB19_24:                              # %.lr.ph.us
                                        #   Parent Loop BB19_23 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB19_33 Depth 3
                                        #       Child Loop BB19_36 Depth 3
                                        #       Child Loop BB19_26 Depth 3
	movq	(%r15,%r11,8), %r10
	movq	(%r8,%r11,8), %r9
	cmpq	$15, %r12
	movq	%rdx, %rax
	jbe	.LBB19_25
# BB#27:                                # %min.iters.checked
                                        #   in Loop: Header=BB19_24 Depth=2
	testq	%r12, %r12
	movq	%rdx, %rax
	je	.LBB19_25
# BB#28:                                # %vector.memcheck
                                        #   in Loop: Header=BB19_24 Depth=2
	leaq	(%r9,%rdx,2), %rax
	leaq	(%r10,%rsi,2), %rcx
	cmpq	%rcx, %rax
	jae	.LBB19_30
# BB#29:                                # %vector.memcheck
                                        #   in Loop: Header=BB19_24 Depth=2
	leaq	(%r9,%rsi,2), %rax
	leaq	(%r10,%rdx,2), %rcx
	cmpq	%rax, %rcx
	movq	%rdx, %rax
	jb	.LBB19_25
.LBB19_30:                              # %vector.body.preheader
                                        #   in Loop: Header=BB19_24 Depth=2
	movl	$1, %eax
	testq	%rax, %rax
	je	.LBB19_31
# BB#32:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB19_24 Depth=2
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	(%r9,%rax,2), %rcx
	leaq	(%r10,%rax,2), %rsi
	movq	$-1, %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB19_33:                              # %vector.body.prol
                                        #   Parent Loop BB19_23 Depth=1
                                        #     Parent Loop BB19_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-16(%rsi,%rax,2), %xmm0
	movups	(%rsi,%rax,2), %xmm1
	movups	%xmm0, -16(%rcx,%rax,2)
	movups	%xmm1, (%rcx,%rax,2)
	addq	$16, %rax
	incq	%rdi
	jne	.LBB19_33
	jmp	.LBB19_34
.LBB19_31:                              #   in Loop: Header=BB19_24 Depth=2
	xorl	%eax, %eax
.LBB19_34:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB19_24 Depth=2
	movb	$1, %cl
	testb	%cl, %cl
	jne	.LBB19_37
# BB#35:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB19_24 Depth=2
	movl	$16, %ecx
	subq	%rax, %rcx
	addq	%rdx, %rax
	leaq	112(%r9,%rax,2), %r13
	leaq	112(%r10,%rax,2), %r14
	.p2align	4, 0x90
.LBB19_36:                              # %vector.body
                                        #   Parent Loop BB19_23 Depth=1
                                        #     Parent Loop BB19_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%r14), %xmm0
	movups	-96(%r14), %xmm1
	movups	%xmm0, -112(%r13)
	movups	%xmm1, -96(%r13)
	movups	-80(%r14), %xmm0
	movups	-64(%r14), %xmm1
	movups	%xmm0, -80(%r13)
	movups	%xmm1, -64(%r13)
	movups	-48(%r14), %xmm0
	movups	-32(%r14), %xmm1
	movups	%xmm0, -48(%r13)
	movups	%xmm1, -32(%r13)
	movups	-16(%r14), %xmm0
	movups	(%r14), %xmm1
	movups	%xmm0, -16(%r13)
	movups	%xmm1, (%r13)
	subq	$-128, %r13
	subq	$-128, %r14
	addq	$-64, %rcx
	jne	.LBB19_36
.LBB19_37:                              # %middle.block
                                        #   in Loop: Header=BB19_24 Depth=2
	cmpq	%r12, %r12
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, %rax
	je	.LBB19_38
	.p2align	4, 0x90
.LBB19_25:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB19_24 Depth=2
	decq	%rax
	.p2align	4, 0x90
.LBB19_26:                              # %scalar.ph
                                        #   Parent Loop BB19_23 Depth=1
                                        #     Parent Loop BB19_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	2(%r10,%rax,2), %ecx
	movw	%cx, 2(%r9,%rax,2)
	incq	%rax
	cmpq	%rbp, %rax
	jl	.LBB19_26
.LBB19_38:                              # %._crit_edge.us
                                        #   in Loop: Header=BB19_24 Depth=2
	cmpq	%rbx, %r11
	leaq	1(%r11), %r11
	jl	.LBB19_24
# BB#39:                                # %._crit_edge101.us
                                        #   in Loop: Header=BB19_23 Depth=1
	movq	64(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	56(%rsp), %rcx          # 8-byte Folded Reload
	jl	.LBB19_23
.LBB19_40:                              # %.loopexit88
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	jne	.LBB19_42
# BB#41:                                # %.loopexit88
	cmpl	$1, %edi
	je	.LBB19_42
# BB#43:
	movl	$0, cbp(%rip)
	xorl	%eax, %eax
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB19_44
.LBB19_42:
	movq	cofAC(%rip), %rax
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	14160(%rbp), %rcx
	movq	%rcx, cofAC(%rip)
	movq	%rax, 14160(%rbp)
	movq	cofDC(%rip), %rax
	movq	14168(%rbp), %rcx
	movq	%rcx, cofDC(%rip)
	movq	%rax, 14168(%rbp)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	364(%rcx,%rdx), %eax
	movl	%eax, cbp(%rip)
	movq	368(%rcx,%rdx), %rax
.LBB19_44:
	movq	%rax, cbp_blk(%rip)
	movl	472(%rcx,%rdx), %eax
	movl	%eax, luma_transform_size_8x8_flag(%rip)
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	(%rax), %rdx
	movslq	172(%rbp), %rcx
	movq	(%rdx,%rcx,8), %rsi
	movslq	168(%rbp), %rdx
	movl	(%rsi,%rdx), %esi
	movl	%esi, frefframe(%rip)
	movq	(%rax), %rsi
	movq	8(%rsi,%rcx,8), %rsi
	movl	(%rsi,%rdx), %esi
	movl	%esi, frefframe+4(%rip)
	movq	(%rax), %rsi
	movq	16(%rsi,%rcx,8), %rsi
	movl	(%rsi,%rdx), %esi
	movl	%esi, frefframe+8(%rip)
	movq	(%rax), %rsi
	movq	24(%rsi,%rcx,8), %rsi
	movl	(%rsi,%rdx), %esi
	movl	%esi, frefframe+12(%rip)
	cmpl	$1, %edi
	jne	.LBB19_46
# BB#45:                                # %.preheader.preheader
	movq	8(%rax), %rsi
	movq	(%rsi,%rcx,8), %rdi
	movl	(%rdi,%rdx), %edi
	movl	%edi, brefframe(%rip)
	movq	8(%rsi,%rcx,8), %rdi
	movl	(%rdi,%rdx), %edi
	movl	%edi, brefframe+4(%rip)
	movq	16(%rsi,%rcx,8), %rsi
	movl	(%rsi,%rdx), %esi
	movl	%esi, brefframe+8(%rip)
	movq	8(%rax), %rax
	movq	24(%rax,%rcx,8), %rax
	movl	(%rax,%rdx), %eax
	movl	%eax, brefframe+12(%rip)
.LBB19_46:                              # %.loopexit
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	store_macroblock_parameters, .Lfunc_end19-store_macroblock_parameters
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI20_0:
	.zero	16,2
	.text
	.globl	set_stored_macroblock_parameters
	.p2align	4, 0x90
	.type	set_stored_macroblock_parameters,@function
set_stored_macroblock_parameters:       # @set_stored_macroblock_parameters
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi177:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi178:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi179:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi180:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi181:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi182:
	.cfi_def_cfa_offset 56
	subq	$16504, %rsp            # imm = 0x4078
.Lcfi183:
	.cfi_def_cfa_offset 16560
.Lcfi184:
	.cfi_offset %rbx, -56
.Lcfi185:
	.cfi_offset %r12, -48
.Lcfi186:
	.cfi_offset %r13, -40
.Lcfi187:
	.cfi_offset %r14, -32
.Lcfi188:
	.cfi_offset %r15, -24
.Lcfi189:
	.cfi_offset %rbp, -16
	movq	img(%rip), %r15
	movq	14224(%r15), %rbp
	movslq	12(%r15), %r14
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %rax
	movq	6472(%rcx), %rcx
	movq	%rcx, 8272(%rsp)        # 8-byte Spill
	movswl	best_mode(%rip), %r12d
	movl	20(%r15), %ecx
	movl	%ecx, 8260(%rsp)        # 4-byte Spill
	movl	$rec_mbY, %ecx
	movq	128(%r15), %rdx
	movq	%rdx, 8280(%rsp)        # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB20_1:                               # =>This Inner Loop Header: Depth=1
	movl	180(%r15), %esi
	addl	%edx, %esi
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rsi
	movslq	176(%r15), %rdi
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	%xmm1, 16(%rsi,%rdi,2)
	movups	%xmm0, (%rsi,%rdi,2)
	movq	img(%rip), %rsi
	movl	180(%rsi), %edi
	leal	1(%rdx,%rdi), %edi
	movslq	%edi, %rdi
	movq	(%rax,%rdi,8), %rdi
	movslq	176(%rsi), %rsi
	movups	32(%rcx), %xmm0
	movups	48(%rcx), %xmm1
	movups	%xmm1, 16(%rdi,%rsi,2)
	movups	%xmm0, (%rdi,%rsi,2)
	addq	$2, %rdx
	movq	img(%rip), %r15
	addq	$64, %rcx
	cmpq	$16, %rdx
	jne	.LBB20_1
# BB#2:
	cmpl	$1, 8260(%rsp)          # 4-byte Folded Reload
	sete	%al
	cmpl	$3, 20(%r15)
	movb	%al, 8219(%rsp)         # 1-byte Spill
	jne	.LBB20_6
# BB#3:
	movl	$0, 8236(%rsp)          # 4-byte Folded Spill
	movl	sp2_frame_indicator(%rip), %eax
	orl	si_frame_indicator(%rip), %eax
	jne	.LBB20_7
# BB#4:                                 # %.preheader383.preheader
	movl	$lrec_rec, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB20_5:                               # %.preheader383
                                        # =>This Inner Loop Header: Depth=1
	movq	lrec(%rip), %rdx
	movl	180(%r15), %esi
	addl	%ecx, %esi
	movslq	%esi, %rsi
	movq	(%rdx,%rsi,8), %rdx
	movslq	176(%r15), %rsi
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	48(%rax), %xmm3
	movups	%xmm3, 48(%rdx,%rsi,4)
	movups	%xmm2, 32(%rdx,%rsi,4)
	movups	%xmm1, 16(%rdx,%rsi,4)
	movups	%xmm0, (%rdx,%rsi,4)
	movq	img(%rip), %rdx
	movq	lrec(%rip), %rsi
	movl	180(%rdx), %edi
	leal	1(%rcx,%rdi), %edi
	movslq	%edi, %rdi
	movq	(%rsi,%rdi,8), %rsi
	movslq	176(%rdx), %rdx
	movups	64(%rax), %xmm0
	movups	80(%rax), %xmm1
	movups	96(%rax), %xmm2
	movups	112(%rax), %xmm3
	movups	%xmm3, 48(%rsi,%rdx,4)
	movups	%xmm2, 32(%rsi,%rdx,4)
	movups	%xmm1, 16(%rsi,%rdx,4)
	movups	%xmm0, (%rsi,%rdx,4)
	addq	$2, %rcx
	movq	img(%rip), %r15
	subq	$-128, %rax
	cmpq	$16, %rcx
	jne	.LBB20_5
	jmp	.LBB20_7
.LBB20_6:
	movl	$0, 8236(%rsp)          # 4-byte Folded Spill
.LBB20_7:                               # %.loopexit384
	cmpl	$0, 15268(%r15)
	je	.LBB20_9
# BB#8:                                 # %.preheader381
	movq	rdopt(%rip), %rax
	movaps	rec_mbY+16(%rip), %xmm0
	movups	%xmm0, 24(%rax)
	movaps	rec_mbY(%rip), %xmm0
	movups	%xmm0, 8(%rax)
	movaps	rec_mbY+48(%rip), %xmm0
	movups	%xmm0, 56(%rax)
	movaps	rec_mbY+32(%rip), %xmm0
	movups	%xmm0, 40(%rax)
	movaps	rec_mbY+80(%rip), %xmm0
	movups	%xmm0, 88(%rax)
	movaps	rec_mbY+64(%rip), %xmm0
	movups	%xmm0, 72(%rax)
	movaps	rec_mbY+112(%rip), %xmm0
	movups	%xmm0, 120(%rax)
	movaps	rec_mbY+96(%rip), %xmm0
	movups	%xmm0, 104(%rax)
	movaps	rec_mbY+144(%rip), %xmm0
	movups	%xmm0, 152(%rax)
	movaps	rec_mbY+128(%rip), %xmm0
	movups	%xmm0, 136(%rax)
	movaps	rec_mbY+176(%rip), %xmm0
	movups	%xmm0, 184(%rax)
	movaps	rec_mbY+160(%rip), %xmm0
	movups	%xmm0, 168(%rax)
	movaps	rec_mbY+208(%rip), %xmm0
	movups	%xmm0, 216(%rax)
	movaps	rec_mbY+192(%rip), %xmm0
	movups	%xmm0, 200(%rax)
	movaps	rec_mbY+240(%rip), %xmm0
	movups	%xmm0, 248(%rax)
	movaps	rec_mbY+224(%rip), %xmm0
	movups	%xmm0, 232(%rax)
	movaps	rec_mbY+272(%rip), %xmm0
	movups	%xmm0, 280(%rax)
	movaps	rec_mbY+256(%rip), %xmm0
	movups	%xmm0, 264(%rax)
	movaps	rec_mbY+304(%rip), %xmm0
	movups	%xmm0, 312(%rax)
	movaps	rec_mbY+288(%rip), %xmm0
	movups	%xmm0, 296(%rax)
	movaps	rec_mbY+336(%rip), %xmm0
	movups	%xmm0, 344(%rax)
	movaps	rec_mbY+320(%rip), %xmm0
	movups	%xmm0, 328(%rax)
	movaps	rec_mbY+368(%rip), %xmm0
	movups	%xmm0, 376(%rax)
	movaps	rec_mbY+352(%rip), %xmm0
	movups	%xmm0, 360(%rax)
	movaps	rec_mbY+400(%rip), %xmm0
	movups	%xmm0, 408(%rax)
	movaps	rec_mbY+384(%rip), %xmm0
	movups	%xmm0, 392(%rax)
	movaps	rec_mbY+432(%rip), %xmm0
	movups	%xmm0, 440(%rax)
	movaps	rec_mbY+416(%rip), %xmm0
	movups	%xmm0, 424(%rax)
	movaps	rec_mbY+464(%rip), %xmm0
	movups	%xmm0, 472(%rax)
	movaps	rec_mbY+448(%rip), %xmm0
	movups	%xmm0, 456(%rax)
	movaps	rec_mbY+496(%rip), %xmm0
	movups	%xmm0, 504(%rax)
	movaps	rec_mbY+480(%rip), %xmm0
	movups	%xmm0, 488(%rax)
.LBB20_9:                               # %.loopexit382
	cmpl	$0, 15260(%r15)
	je	.LBB20_11
# BB#10:
	movl	luma_transform_size_8x8_flag(%rip), %esi
	movl	%r12d, %edi
	callq	update_offset_params
	movq	img(%rip), %r15
.LBB20_11:
	imulq	$536, %r14, %rax        # imm = 0x218
	movq	%rax, 8248(%rsp)        # 8-byte Spill
	cmpl	$0, 15536(%r15)
	movq	%rbp, 8224(%rsp)        # 8-byte Spill
	movl	%r12d, 8220(%rsp)       # 4-byte Spill
	je	.LBB20_32
# BB#12:                                # %.preheader380
	movl	15548(%r15), %eax
	testl	%eax, %eax
	jle	.LBB20_21
# BB#13:                                # %.lr.ph405
	movl	$lrec_rec_U, %ebp
	movl	$lrec_rec_V, %r13d
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB20_14:                              # =>This Inner Loop Header: Depth=1
	movq	8272(%rsp), %rbx        # 8-byte Reload
	movq	(%rbx), %rax
	movl	188(%r15), %ecx
	addl	%r12d, %ecx
	movslq	%ecx, %rcx
	movslq	184(%r15), %rdi
	addq	%rdi, %rdi
	addq	(%rax,%rcx,8), %rdi
	leaq	rec_mbU(%r14), %rsi
	movslq	15544(%r15), %rdx
	addq	%rdx, %rdx
	movq	%rsi, 8240(%rsp)        # 8-byte Spill
	callq	memcpy
	movq	8(%rbx), %rax
	movq	img(%rip), %rcx
	movl	188(%rcx), %edx
	addl	%r12d, %edx
	movslq	%edx, %rdx
	movslq	184(%rcx), %rdi
	addq	%rdi, %rdi
	addq	(%rax,%rdx,8), %rdi
	leaq	rec_mbV(%r14), %rbx
	movslq	15544(%rcx), %rdx
	addq	%rdx, %rdx
	movq	%rbx, %rsi
	callq	memcpy
	movq	img(%rip), %r15
	cmpl	$3, 20(%r15)
	jne	.LBB20_17
# BB#15:                                #   in Loop: Header=BB20_14 Depth=1
	movl	sp2_frame_indicator(%rip), %eax
	orl	si_frame_indicator(%rip), %eax
	jne	.LBB20_17
# BB#16:                                #   in Loop: Header=BB20_14 Depth=1
	movq	lrec_uv(%rip), %rax
	movq	(%rax), %rax
	movl	188(%r15), %ecx
	addl	%r12d, %ecx
	movslq	%ecx, %rcx
	movslq	184(%r15), %rdi
	shlq	$2, %rdi
	addq	(%rax,%rcx,8), %rdi
	movslq	15544(%r15), %rdx
	shlq	$2, %rdx
	movq	%rbp, %rsi
	callq	memcpy
	movq	lrec_uv(%rip), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movl	188(%rcx), %edx
	addl	%r12d, %edx
	movslq	%edx, %rdx
	movslq	184(%rcx), %rdi
	shlq	$2, %rdi
	addq	(%rax,%rdx,8), %rdi
	movslq	15544(%rcx), %rdx
	shlq	$2, %rdx
	movq	%r13, %rsi
	callq	memcpy
	movq	img(%rip), %r15
.LBB20_17:                              #   in Loop: Header=BB20_14 Depth=1
	cmpl	$0, 15268(%r15)
	je	.LBB20_19
# BB#18:                                #   in Loop: Header=BB20_14 Depth=1
	movq	%rbx, 8264(%rsp)        # 8-byte Spill
	movq	%r13, %rbx
	movq	rdopt(%rip), %r13
	leaq	520(%r13,%r14), %rdi
	movslq	15544(%r15), %rdx
	addq	%rdx, %rdx
	movq	8240(%rsp), %rsi        # 8-byte Reload
	callq	memcpy
	leaq	1032(%r13,%r14), %rdi
	movq	%rbx, %r13
	movslq	15544(%r15), %rdx
	addq	%rdx, %rdx
	movq	8264(%rsp), %rsi        # 8-byte Reload
	callq	memcpy
.LBB20_19:                              #   in Loop: Header=BB20_14 Depth=1
	incq	%r12
	movslq	15548(%r15), %rax
	addq	$32, %r14
	addq	$64, %rbp
	addq	$64, %r13
	cmpq	%rax, %r12
	jl	.LBB20_14
# BB#20:
	movq	8224(%rsp), %rbp        # 8-byte Reload
	movl	8220(%rsp), %r12d       # 4-byte Reload
.LBB20_21:                              # %._crit_edge
	movl	sp2_frame_indicator(%rip), %ecx
	orl	si_frame_indicator(%rip), %ecx
	jne	.LBB20_27
# BB#22:                                # %._crit_edge
	cmpl	$3, 20(%r15)
	jne	.LBB20_27
# BB#23:                                # %._crit_edge
	testl	%eax, %eax
	jle	.LBB20_27
# BB#24:                                # %.lr.ph403.preheader
	movl	$lrec_rec_U, %r14d
	movl	$lrec_rec_V, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_25:                              # %.lr.ph403
                                        # =>This Inner Loop Header: Depth=1
	movq	lrec_uv(%rip), %rax
	movq	(%rax), %rax
	movl	188(%r15), %ecx
	addl	%ebx, %ecx
	movslq	%ecx, %rcx
	movslq	184(%r15), %rdi
	shlq	$2, %rdi
	addq	(%rax,%rcx,8), %rdi
	movslq	15544(%r15), %rdx
	shlq	$2, %rdx
	movq	%r14, %rsi
	callq	memcpy
	movq	lrec_uv(%rip), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movl	188(%rcx), %edx
	addl	%ebx, %edx
	movslq	%edx, %rdx
	movslq	184(%rcx), %rdi
	shlq	$2, %rdi
	addq	(%rax,%rdx,8), %rdi
	movslq	15544(%rcx), %rdx
	shlq	$2, %rdx
	movq	%rbp, %rsi
	callq	memcpy
	incq	%rbx
	movq	img(%rip), %r15
	movslq	15548(%r15), %rax
	addq	$64, %r14
	addq	$64, %rbp
	cmpq	%rax, %rbx
	jl	.LBB20_25
# BB#26:
	movq	8224(%rsp), %rbp        # 8-byte Reload
	movl	8220(%rsp), %r12d       # 4-byte Reload
.LBB20_27:                              # %.loopexit379
	cmpl	$0, 15268(%r15)
	je	.LBB20_32
# BB#28:                                # %.preheader376
	testl	%eax, %eax
	jle	.LBB20_32
# BB#29:                                # %.lr.ph401
	movl	$520, %ebx              # imm = 0x208
	addq	rdopt(%rip), %rbx
	movl	$rec_mbU, %r12d
	movl	$rec_mbV, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB20_30:                              # =>This Inner Loop Header: Depth=1
	movslq	15544(%r15), %rdx
	addq	%rdx, %rdx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcpy
	leaq	512(%rbx), %rdi
	movslq	15544(%r15), %rdx
	addq	%rdx, %rdx
	movq	%r14, %rsi
	callq	memcpy
	incq	%rbp
	movslq	15548(%r15), %rax
	addq	$32, %rbx
	addq	$32, %r12
	addq	$32, %r14
	cmpq	%rax, %rbp
	jl	.LBB20_30
# BB#31:
	movq	8224(%rsp), %rbp        # 8-byte Reload
	movl	8220(%rsp), %r12d       # 4-byte Reload
.LBB20_32:                              # %.loopexit377
	movq	cofAC(%rip), %rax
	movq	14160(%r15), %rcx
	movq	%rcx, cofAC(%rip)
	movq	%rax, 14160(%r15)
	movq	cofDC(%rip), %rdx
	movq	14168(%r15), %rcx
	movq	%rcx, cofDC(%rip)
	movq	%rdx, 14168(%r15)
	movl	cbp(%rip), %esi
	movq	8248(%rsp), %rbx        # 8-byte Reload
	movl	%esi, 364(%rbp,%rbx)
	movq	cbp_blk(%rip), %rdi
	movq	%rdi, 368(%rbp,%rbx)
	movl	%r12d, 72(%rbp,%rbx)
	cmpl	$0, 15268(%r15)
	je	.LBB20_39
# BB#33:
	movq	rdopt(%rip), %rcx
	movl	%r12d, 1656(%rcx)
	movl	15244(%r15), %ebp
	movl	%ebp, 1712(%rcx)
	movq	8224(%rsp), %rbp        # 8-byte Reload
	movl	%esi, 1640(%rcx)
	movq	%rdi, 1648(%rcx)
	movl	%r12d, 1560(%rcx)
	movl	496(%rbp,%rbx), %esi
	movl	%esi, 1732(%rcx)
	movl	500(%rbp,%rbx), %esi
	movl	%esi, 1736(%rcx)
	movl	4(%rbp,%rbx), %esi
	movl	%esi, 1740(%rcx)
	movl	8(%rbp,%rbx), %esi
	movl	%esi, 1728(%rcx)
	movl	504(%rbp,%rbx), %esi
	movl	%esi, 1744(%rcx)
	cmpl	$-3, 15528(%r15)
	jl	.LBB20_38
# BB#34:                                # %.preheader375.preheader
	xorl	%ebx, %ebx
	jmp	.LBB20_36
	.p2align	4, 0x90
.LBB20_35:                              # %.preheader375..preheader375_crit_edge
                                        #   in Loop: Header=BB20_36 Depth=1
	incq	%rbx
	movq	rdopt(%rip), %rcx
	movq	14160(%rax), %rax
.LBB20_36:                              # %.preheader375
                                        # =>This Inner Loop Header: Depth=1
	movq	1544(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rdi
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movslq	15528(%rax), %rcx
	addq	$3, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB20_35
# BB#37:                                # %.preheader371.preheader.loopexit
	movq	rdopt(%rip), %rcx
	movq	14168(%rax), %rdx
.LBB20_38:                              # %.preheader371.preheader
	movq	1552(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rdx), %rcx
	movq	(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	rdopt(%rip), %rax
	movq	1552(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movq	14168(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	rdopt(%rip), %rax
	movq	1552(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movq	img(%rip), %rcx
	movq	14168(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	rdopt(%rip), %rax
	movq	1552(%rax), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movq	14168(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	rdopt(%rip), %rax
	movq	1552(%rax), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	img(%rip), %rcx
	movq	14168(%rcx), %rcx
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	rdopt(%rip), %rax
	movq	1552(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movq	14168(%rcx), %rcx
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	img(%rip), %r15
.LBB20_39:                              # %.loopexit373
	movq	8248(%rsp), %rax        # 8-byte Reload
	leaq	72(%rbp,%rax), %r14
	movaps	b8mode(%rip), %xmm0
	movups	%xmm0, 376(%rbp,%rax)
	movaps	b8pdir(%rip), %xmm0
	movups	%xmm0, 392(%rbp,%rax)
	cmpl	$0, 15268(%r15)
	je	.LBB20_41
# BB#40:
	movq	rdopt(%rip), %rax
	movaps	b8mode(%rip), %xmm0
	movups	%xmm0, 1568(%rax)
	movaps	b8pdir(%rip), %xmm0
	movups	%xmm0, 1584(%rax)
.LBB20_41:
	movq	%rbp, %rcx
	movq	8248(%rsp), %rdx        # 8-byte Reload
	leaq	364(%rcx,%rdx), %rbp
	xorl	%eax, %eax
	cmpl	$1, (%r14)
	cmovew	bi_pred_me(%rip), %ax
	movw	%ax, 480(%rcx,%rdx)
	movzwl	8220(%rsp), %eax        # 2-byte Folded Reload
	cmpl	$8, %eax
	jne	.LBB20_45
# BB#42:
	movl	luma_transform_size_8x8_flag(%rip), %eax
	testl	%eax, %eax
	jne	.LBB20_45
# BB#43:
	movq	input(%rip), %rax
	cmpl	$0, 5100(%rax)
	je	.LBB20_45
# BB#44:
	xorl	%ebx, %ebx
	cmpl	$1, 20(%r15)
	sete	%bl
	leaq	8288(%rsp), %r12
	movl	$tr8x8, %esi
	movl	$8216, %edx             # imm = 0x2018
	movq	%r12, %rdi
	callq	memcpy
	movl	$1027, %ecx             # imm = 0x403
	movq	%rsp, %rdi
	movq	%r12, %rsi
	rep;movsq
	movl	$1, %edi
	xorl	%esi, %esi
	movl	%ebx, %edx
	callq	RestoreMVBlock8x8
	movl	$tr8x8, %esi
	movl	$8216, %edx             # imm = 0x2018
	movq	%r12, %rdi
	callq	memcpy
	movl	$1027, %ecx             # imm = 0x403
	movq	%rsp, %rdi
	movq	%r12, %rsi
	rep;movsq
	movl	$1, %edi
	movl	$1, %esi
	movl	%ebx, %edx
	callq	RestoreMVBlock8x8
	movl	$tr8x8, %esi
	movl	$8216, %edx             # imm = 0x2018
	movq	%r12, %rdi
	callq	memcpy
	movl	$1027, %ecx             # imm = 0x403
	movq	%rsp, %rdi
	movq	%r12, %rsi
	rep;movsq
	movl	$1, %edi
	movl	$2, %esi
	movl	%ebx, %edx
	callq	RestoreMVBlock8x8
	movl	$tr8x8, %esi
	movl	$8216, %edx             # imm = 0x2018
	movq	%r12, %rdi
	callq	memcpy
	movl	$1027, %ecx             # imm = 0x403
	movq	%rsp, %rdi
	movq	%r12, %rsi
	rep;movsq
	movl	$1, %edi
	movl	$3, %esi
	movl	%ebx, %edx
	callq	RestoreMVBlock8x8
.LBB20_45:
	testb	$15, (%rbp)
	jne	.LBB20_47
# BB#46:
	movl	(%r14), %eax
	orl	$4, %eax
	cmpl	$13, %eax
	jne	.LBB20_101
.LBB20_47:
	movl	luma_transform_size_8x8_flag(%rip), %eax
.LBB20_48:
	movq	8224(%rsp), %rsi        # 8-byte Reload
	movq	8248(%rsp), %rbp        # 8-byte Reload
	movl	%eax, 472(%rsi,%rbp)
	movq	rdopt(%rip), %rcx
	movl	%eax, 1720(%rcx)
	movq	input(%rip), %rax
	cmpl	$3, 4168(%rax)
	jne	.LBB20_51
# BB#49:
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	je	.LBB20_51
# BB#50:
	movq	decs(%rip), %rcx
	movq	48(%rcx), %rcx
	movslq	164(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	160(%rax), %rax
	movl	8220(%rsp), %edx        # 4-byte Reload
	movb	%dl, (%rcx,%rax)
.LBB20_51:                              # %.preheader370
	leaq	(%rsi,%rbp), %rdi
	movl	8236(%rsp), %eax        # 4-byte Reload
	movb	8219(%rsp), %cl         # 1-byte Reload
	movb	%cl, %al
	movl	%eax, 8236(%rsp)        # 4-byte Spill
	leaq	480(%rsi,%rbp), %rax
	movq	%rax, 8240(%rsp)        # 8-byte Spill
	movq	img(%rip), %r8
	xorl	%eax, %eax
	movl	$26112, %r11d           # imm = 0x6600
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB20_52:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_61 Depth 2
	movslq	172(%r8), %r13
	movslq	%r10d, %r15
	addq	%r15, %r13
	andl	$-2, %r15d
	movq	%rax, 8272(%rsp)        # 8-byte Spill
	movq	%rax, %rbx
	xorl	%ebp, %ebp
	movq	%r10, 8264(%rsp)        # 8-byte Spill
	jmp	.LBB20_61
.LBB20_53:                              #   in Loop: Header=BB20_61 Depth=2
	cmpl	$1, %esi
	jne	.LBB20_58
# BB#54:                                #   in Loop: Header=BB20_61 Depth=2
	cmpl	$2, %ecx
	jne	.LBB20_58
# BB#55:                                #   in Loop: Header=BB20_61 Depth=2
	movq	8240(%rsp), %rcx        # 8-byte Reload
	movzwl	(%rcx), %ecx
	testw	%cx, %cx
	je	.LBB20_58
# BB#56:                                #   in Loop: Header=BB20_61 Depth=2
	movzwl	%cx, %ecx
	leaq	14400(%r8), %rsi
	addq	$14392, %r8             # imm = 0x3838
	cmpl	$1, %ecx
	cmovneq	%rsi, %r8
	movq	(%r8), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	376(%rdi,%rdx,4), %rdx
	movq	(%rcx,%rdx,8), %r8
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r13,8), %rdx
	movslq	%eax, %rsi
	movb	$0, (%rdx,%rsi)
	movq	enc_picture(%rip), %rdx
	movq	8240(%rsp), %rcx        # 8-byte Reload
	movslq	-48(%rcx), %rcx
	imulq	$264, %rcx, %rcx        # imm = 0x108
	movq	24(%rdx,%rcx), %r10
	movq	6496(%rdx), %r9
	movq	(%r9), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	%r10, (%rcx,%rsi,8)
	movzwl	(%r8), %r9d
	movq	6512(%rdx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movw	%r9w, (%rcx)
	movzwl	2(%r8), %esi
	movw	%si, 2(%rcx)
	movq	img(%rip), %r8
	cmpl	$0, 15268(%r8)
	je	.LBB20_60
# BB#57:                                #   in Loop: Header=BB20_61 Depth=2
	movq	rdopt(%rip), %rcx
	movb	$0, 1680(%rcx,%rbx)
.LBB20_60:                              #   in Loop: Header=BB20_61 Depth=2
	movq	8264(%rsp), %r10        # 8-byte Reload
	cmpl	$0, (%r12)
	jne	.LBB20_67
	jmp	.LBB20_69
.LBB20_58:                              #   in Loop: Header=BB20_61 Depth=2
	movq	14384(%r8), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx), %rcx
	movsbq	frefframe(%rbx), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movslq	376(%rdi,%rdx,4), %rdx
	movq	(%rcx,%rdx,8), %r8
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r13,8), %rdx
	movslq	%eax, %rcx
	movb	%sil, (%rdx,%rcx)
	movq	enc_picture(%rip), %rdx
	movq	8240(%rsp), %rsi        # 8-byte Reload
	movslq	-48(%rsi), %rsi
	movsbq	frefframe(%rbx), %r9
	imulq	$264, %rsi, %rsi        # imm = 0x108
	addq	%rdx, %rsi
	movq	24(%rsi,%r9,8), %r9
	movq	6496(%rdx), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%r13,8), %rsi
	movq	%r9, (%rsi,%rcx,8)
	movzwl	(%r8), %r9d
	movq	6512(%rdx), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%r13,8), %rsi
	movq	(%rsi,%rcx,8), %rcx
	movw	%r9w, (%rcx)
	movzwl	2(%r8), %esi
	movw	%si, 2(%rcx)
	movq	img(%rip), %r8
	cmpl	$0, 15268(%r8)
	je	.LBB20_66
# BB#59:                                #   in Loop: Header=BB20_61 Depth=2
	movzbl	frefframe(%rbx), %ecx
	movq	rdopt(%rip), %rsi
	movb	%cl, 1680(%rsi,%rbx)
	cmpl	$0, (%r12)
	jne	.LBB20_67
	jmp	.LBB20_69
	.p2align	4, 0x90
.LBB20_61:                              #   Parent Loop BB20_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	168(%r8), %eax
	addq	%rbp, %rax
	movl	%ebp, %ecx
	sarl	%ecx
	addl	%r15d, %ecx
	movslq	%ecx, %rdx
	leaq	392(%rdi,%rdx,4), %r12
	movl	392(%rdi,%rdx,4), %ecx
	cmpl	$1, %ecx
	je	.LBB20_64
# BB#62:                                #   in Loop: Header=BB20_61 Depth=2
	movl	(%r14), %esi
	cmpl	$14, %esi
	ja	.LBB20_53
# BB#63:                                #   in Loop: Header=BB20_61 Depth=2
	btl	%esi, %r11d
	jae	.LBB20_53
.LBB20_64:                              #   in Loop: Header=BB20_61 Depth=2
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movslq	%eax, %rsi
	movb	$-1, (%rcx,%rsi)
	movq	enc_picture(%rip), %rdx
	movq	6496(%rdx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	$-1, (%rcx,%rsi,8)
	movq	6512(%rdx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movl	$0, (%rcx)
	movq	img(%rip), %r8
	cmpl	$0, 15268(%r8)
	je	.LBB20_66
# BB#65:                                #   in Loop: Header=BB20_61 Depth=2
	movq	rdopt(%rip), %rcx
	movb	$-1, 1680(%rcx,%rbx)
.LBB20_66:                              #   in Loop: Header=BB20_61 Depth=2
	cmpl	$0, (%r12)
	je	.LBB20_69
.LBB20_67:                              #   in Loop: Header=BB20_61 Depth=2
	movl	(%r14), %ecx
	cmpl	$14, %ecx
	ja	.LBB20_71
# BB#68:                                #   in Loop: Header=BB20_61 Depth=2
	btl	%ecx, %r11d
	jae	.LBB20_71
.LBB20_69:                              #   in Loop: Header=BB20_61 Depth=2
	movq	6488(%rdx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	cltq
	movb	$-1, (%rcx,%rax)
	movq	enc_picture(%rip), %rdx
	movq	6496(%rdx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	$-1, (%rcx,%rax,8)
	movq	6512(%rdx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%rax,8), %rax
	movl	$0, (%rax)
	movq	img(%rip), %r8
	cmpl	$0, 15268(%r8)
	je	.LBB20_71
# BB#70:                                #   in Loop: Header=BB20_61 Depth=2
	movq	rdopt(%rip), %rax
	movb	$-1, 1696(%rax,%rbx)
.LBB20_71:                              #   in Loop: Header=BB20_61 Depth=2
	incq	%rbp
	incq	%rbx
	cmpq	$4, %rbp
	jne	.LBB20_61
# BB#72:                                #   in Loop: Header=BB20_52 Depth=1
	incq	%r10
	movq	8272(%rsp), %rax        # 8-byte Reload
	addq	$4, %rax
	cmpq	$4, %r10
	jne	.LBB20_52
# BB#73:
	cmpl	$1, 8260(%rsp)          # 4-byte Folded Reload
	jne	.LBB20_90
# BB#74:                                # %.preheader.preheader
	xorl	%r11d, %r11d
	movl	$1696, %r9d             # imm = 0x6A0
	movl	$26112, %r15d           # imm = 0x6600
	.p2align	4, 0x90
.LBB20_75:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_84 Depth 2
	movslq	172(%r8), %r13
	movslq	%r11d, %r10
	addq	%r10, %r13
	andl	$-2, %r10d
	movq	%r9, %rsi
	xorl	%eax, %eax
	jmp	.LBB20_84
	.p2align	4, 0x90
.LBB20_76:                              #   in Loop: Header=BB20_84 Depth=2
	movl	%eax, %ecx
	sarl	%ecx
	addl	%r10d, %ecx
	movslq	%ecx, %r12
	movl	392(%rdi,%r12,4), %ecx
	testl	%ecx, %ecx
	je	.LBB20_86
# BB#77:                                #   in Loop: Header=BB20_84 Depth=2
	cmpl	$1, %ebx
	jne	.LBB20_82
# BB#78:                                #   in Loop: Header=BB20_84 Depth=2
	cmpl	$2, %ecx
	jne	.LBB20_82
# BB#79:                                #   in Loop: Header=BB20_84 Depth=2
	movq	8240(%rsp), %rcx        # 8-byte Reload
	movzwl	(%rcx), %ecx
	testw	%cx, %cx
	je	.LBB20_82
# BB#80:                                #   in Loop: Header=BB20_84 Depth=2
	movzwl	%cx, %ecx
	leaq	14400(%r8), %rbx
	addq	$14392, %r8             # imm = 0x3838
	cmpl	$1, %ecx
	cmovneq	%rbx, %r8
	movq	(%r8), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	376(%rdi,%r12,4), %rbx
	movq	(%rcx,%rbx,8), %r8
	movq	6488(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r13,8), %rdx
	movslq	%ebp, %rbp
	movb	$0, (%rdx,%rbp)
	movq	enc_picture(%rip), %rdx
	movq	8240(%rsp), %rcx        # 8-byte Reload
	movslq	-48(%rcx), %rbx
	imulq	$264, %rbx, %rbx        # imm = 0x108
	movq	288(%rdx,%rbx), %rbx
	movq	6496(%rdx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	%rbx, (%rcx,%rbp,8)
	movzwl	(%r8), %ecx
	movq	6512(%rdx), %rbx
	movq	8(%rbx), %rbx
	movq	(%rbx,%r13,8), %rbx
	movq	(%rbx,%rbp,8), %rbx
	movw	%cx, (%rbx)
	movzwl	2(%r8), %ecx
	movw	%cx, 2(%rbx)
	movq	img(%rip), %r8
	cmpl	$0, 15268(%r8)
	je	.LBB20_88
# BB#81:                                #   in Loop: Header=BB20_84 Depth=2
	movq	rdopt(%rip), %rcx
	movb	$0, (%rcx,%rsi)
	jmp	.LBB20_88
.LBB20_82:                              #   in Loop: Header=BB20_84 Depth=2
	movq	14384(%r8), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movq	8(%rcx), %rcx
	movsbq	brefframe-1696(%rsi), %rbx
	movq	(%rcx,%rbx,8), %r8
	movslq	376(%rdi,%r12,4), %rcx
	movq	(%r8,%rcx,8), %r8
	movq	6488(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r13,8), %rdx
	movslq	%ebp, %rbp
	movb	%bl, (%rdx,%rbp)
	movq	enc_picture(%rip), %rdx
	movq	8240(%rsp), %rcx        # 8-byte Reload
	movslq	-48(%rcx), %rbx
	imulq	$264, %rbx, %rbx        # imm = 0x108
	addq	%rdx, %rbx
	movsbq	brefframe-1696(%rsi), %rcx
	movq	288(%rbx,%rcx,8), %rcx
	movq	6496(%rdx), %rbx
	movq	8(%rbx), %rbx
	movq	(%rbx,%r13,8), %rbx
	movq	%rcx, (%rbx,%rbp,8)
	movzwl	(%r8), %ecx
	movq	6512(%rdx), %rbx
	movq	8(%rbx), %rbx
	movq	(%rbx,%r13,8), %rbx
	movq	(%rbx,%rbp,8), %rbx
	movw	%cx, (%rbx)
	movzwl	2(%r8), %ecx
	movw	%cx, 2(%rbx)
	movq	img(%rip), %r8
	cmpl	$0, 15268(%r8)
	je	.LBB20_88
# BB#83:                                #   in Loop: Header=BB20_84 Depth=2
	movzbl	brefframe-1696(%rsi), %ecx
	movq	rdopt(%rip), %rbx
	movb	%cl, (%rbx,%rsi)
	jmp	.LBB20_88
	.p2align	4, 0x90
.LBB20_84:                              #   Parent Loop BB20_75 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	168(%r8), %ebp
	addq	%rax, %rbp
	movl	(%r14), %ebx
	cmpl	$14, %ebx
	ja	.LBB20_76
# BB#85:                                #   in Loop: Header=BB20_84 Depth=2
	btl	%ebx, %r15d
	jae	.LBB20_76
.LBB20_86:                              #   in Loop: Header=BB20_84 Depth=2
	movq	6488(%rdx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movslq	%ebp, %rbp
	movb	$-1, (%rcx,%rbp)
	movq	enc_picture(%rip), %rdx
	movq	6496(%rdx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	$-1, (%rcx,%rbp,8)
	movq	6512(%rdx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	$0, (%rcx)
	movq	img(%rip), %r8
	cmpl	$0, 15268(%r8)
	je	.LBB20_88
# BB#87:                                #   in Loop: Header=BB20_84 Depth=2
	movq	rdopt(%rip), %rcx
	movb	$-1, (%rcx,%rsi)
.LBB20_88:                              #   in Loop: Header=BB20_84 Depth=2
	incq	%rax
	incq	%rsi
	cmpq	$4, %rax
	jne	.LBB20_84
# BB#89:                                #   in Loop: Header=BB20_75 Depth=1
	incq	%r11
	addq	$4, %r9
	cmpq	$4, %r11
	jne	.LBB20_75
.LBB20_90:                              # %.loopexit369
	movl	best_c_imode(%rip), %eax
	movq	8224(%rsp), %rcx        # 8-byte Reload
	movq	8248(%rsp), %rbx        # 8-byte Reload
	movl	%eax, 416(%rcx,%rbx)
	movl	best_i16offset(%rip), %eax
	movl	%eax, 15244(%r8)
	cmpl	$13, (%r14)
	jne	.LBB20_92
# BB#91:                                # %.loopexit366.loopexit415
	movaps	b8_intra_pred_modes8x8(%rip), %xmm0
	movups	%xmm0, 348(%rcx,%rbx)
	movaps	b8_intra_pred_modes8x8(%rip), %xmm0
	movups	%xmm0, 332(%rcx,%rbx)
	movq	128(%r8), %rax
	movslq	172(%r8), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	168(%r8), %rcx
	movl	b8_ipredmode8x8(%rip), %edx
	movl	%edx, (%rax,%rcx)
	movq	img(%rip), %rax
	movq	136(%rax), %rcx
	movslq	172(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	168(%rax), %rax
	movl	b8_ipredmode8x8(%rip), %edx
	movl	%edx, (%rcx,%rax)
	movq	img(%rip), %rax
	movq	128(%rax), %rcx
	movslq	172(%rax), %rdx
	movq	8(%rcx,%rdx,8), %rcx
	movslq	168(%rax), %rax
	movl	b8_ipredmode8x8+4(%rip), %edx
	movl	%edx, (%rcx,%rax)
	movq	img(%rip), %rax
	movq	136(%rax), %rcx
	movslq	172(%rax), %rdx
	movq	8(%rcx,%rdx,8), %rcx
	movslq	168(%rax), %rax
	movl	b8_ipredmode8x8+4(%rip), %edx
	movl	%edx, (%rcx,%rax)
	movq	img(%rip), %rax
	movq	128(%rax), %rcx
	movslq	172(%rax), %rdx
	movq	16(%rcx,%rdx,8), %rcx
	movslq	168(%rax), %rax
	movl	b8_ipredmode8x8+8(%rip), %edx
	movl	%edx, (%rcx,%rax)
	movq	img(%rip), %rax
	movq	136(%rax), %rcx
	movslq	172(%rax), %rdx
	movq	16(%rcx,%rdx,8), %rcx
	movslq	168(%rax), %rax
	movl	b8_ipredmode8x8+8(%rip), %edx
	movl	%edx, (%rcx,%rax)
	movq	img(%rip), %rax
	movq	128(%rax), %rcx
	movslq	172(%rax), %rdx
	movq	24(%rcx,%rdx,8), %rcx
	movslq	168(%rax), %rax
	movl	b8_ipredmode8x8+12(%rip), %edx
	movl	%edx, (%rcx,%rax)
	movq	img(%rip), %rax
	movq	136(%rax), %rcx
	movslq	172(%rax), %rdx
	movq	24(%rcx,%rdx,8), %rcx
	movslq	168(%rax), %rax
	movl	b8_ipredmode8x8+12(%rip), %edx
	movl	%edx, (%rcx,%rax)
	movq	8280(%rsp), %rbp        # 8-byte Reload
	jmp	.LBB20_97
.LBB20_92:
	movzwl	8220(%rsp), %eax        # 2-byte Folded Reload
	cmpl	$9, %eax
	movq	8280(%rsp), %rbp        # 8-byte Reload
	je	.LBB20_96
# BB#93:
	cmpl	$13, %eax
	je	.LBB20_97
# BB#94:                                # %.lr.ph389.preheader
	movaps	.LCPI20_0(%rip), %xmm0  # xmm0 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
	movq	8224(%rsp), %rax        # 8-byte Reload
	movups	%xmm0, 332(%rax,%rbx)
	movslq	172(%r8), %rax
	decq	%rax
	.p2align	4, 0x90
.LBB20_95:                              # %.lr.ph389
                                        # =>This Inner Loop Header: Depth=1
	movq	128(%r8), %rcx
	movq	8(%rcx,%rax,8), %rcx
	movslq	168(%r8), %rdx
	movl	$33686018, (%rcx,%rdx)  # imm = 0x2020202
	movq	img(%rip), %r8
	movslq	172(%r8), %rcx
	addq	$3, %rcx
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB20_95
	jmp	.LBB20_97
.LBB20_96:                              # %.loopexit366.loopexit410420
	movaps	b4_intra_pred_modes(%rip), %xmm0
	movq	8224(%rsp), %rax        # 8-byte Reload
	movups	%xmm0, 332(%rax,%rbx)
	movq	128(%r8), %rax
	movslq	172(%r8), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	168(%r8), %rcx
	movl	b4_ipredmode(%rip), %edx
	movl	%edx, (%rax,%rcx)
	movq	img(%rip), %rax
	movq	128(%rax), %rcx
	movslq	172(%rax), %rdx
	movq	8(%rcx,%rdx,8), %rcx
	movslq	168(%rax), %rax
	movl	b4_ipredmode+4(%rip), %edx
	movl	%edx, (%rcx,%rax)
	movq	img(%rip), %rax
	movq	128(%rax), %rcx
	movslq	172(%rax), %rdx
	movq	16(%rcx,%rdx,8), %rcx
	movslq	168(%rax), %rax
	movl	b4_ipredmode+8(%rip), %edx
	movl	%edx, (%rcx,%rax)
	movq	img(%rip), %rax
	movq	128(%rax), %rcx
	movslq	172(%rax), %rdx
	movq	24(%rcx,%rdx,8), %rcx
	movslq	168(%rax), %rax
	movl	b4_ipredmode+12(%rip), %edx
	movl	%edx, (%rcx,%rax)
.LBB20_97:                              # %.loopexit366
	movq	img(%rip), %rcx
	cmpl	$0, 15268(%rcx)
	je	.LBB20_100
# BB#98:                                # %.lr.ph.preheader
	movq	8224(%rsp), %rsi        # 8-byte Reload
	leaq	416(%rsi,%rbx), %rax
	movl	(%rax), %eax
	movq	rdopt(%rip), %rdx
	movl	%eax, 1716(%rdx)
	movl	15244(%rcx), %eax
	movl	%eax, 1712(%rdx)
	movups	332(%rsi,%rbx), %xmm0
	movups	%xmm0, 1608(%rdx)
	movups	348(%rsi,%rbx), %xmm0
	movups	%xmm0, 1624(%rdx)
	movslq	172(%rcx), %rax
	movq	1600(%rdx), %rdx
	movq	(%rdx,%rax,8), %rdx
	movslq	168(%rcx), %rcx
	movq	(%rbp,%rax,8), %rsi
	movl	(%rsi,%rcx), %esi
	movl	%esi, (%rdx,%rcx)
	movq	img(%rip), %rcx
	movl	172(%rcx), %edx
	addl	$3, %edx
	cmpl	%edx, %eax
	jge	.LBB20_100
	.p2align	4, 0x90
.LBB20_99:                              # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	rdopt(%rip), %rdx
	movq	1600(%rdx), %rdx
	movq	8(%rdx,%rax,8), %rdx
	movslq	168(%rcx), %rcx
	movq	8(%rbp,%rax,8), %rsi
	leaq	1(%rax), %rax
	movl	(%rsi,%rcx), %esi
	movl	%esi, (%rdx,%rcx)
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdx
	addq	$3, %rdx
	cmpq	%rdx, %rax
	jl	.LBB20_99
.LBB20_100:                             # %.loopexit
	movl	8236(%rsp), %esi        # 4-byte Reload
	addq	$16504, %rsp            # imm = 0x4078
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	SetMotionVectorsMB      # TAILCALL
.LBB20_101:
	xorl	%eax, %eax
	jmp	.LBB20_48
.Lfunc_end20:
	.size	set_stored_macroblock_parameters, .Lfunc_end20-set_stored_macroblock_parameters
	.cfi_endproc

	.globl	update_offset_params
	.p2align	4, 0x90
	.type	update_offset_params,@function
update_offset_params:                   # @update_offset_params
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi190:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi191:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi192:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi193:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi194:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi195:
	.cfi_def_cfa_offset 56
.Lcfi196:
	.cfi_offset %rbx, -56
.Lcfi197:
	.cfi_offset %r12, -48
.Lcfi198:
	.cfi_offset %r13, -40
.Lcfi199:
	.cfi_offset %r14, -32
.Lcfi200:
	.cfi_offset %r15, -24
.Lcfi201:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	-9(%rdi), %eax
	cmpl	$1, %eax
	seta	%r9b
	cmpl	$13, %edi
	setne	%r11b
	movq	img(%rip), %r8
	movslq	20(%r8), %r10
	shlq	$2, %r10
	movl	OffsetBits(%rip), %ecx
	decl	%ecx
	movl	$1, %r13d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r13d
	leal	2(%rsi), %ecx
	testl	%esi, %esi
	movl	$OffsetList8x8, %edi
	movl	$OffsetList4x4, %ebp
	cmovneq	%rdi, %rbp
	movl	$bestIntraFAdjust8x8, %edi
	movl	$bestIntraFAdjust4x4, %ebx
	cmovneq	%rdi, %rbx
	movl	$bestInterFAdjust8x8, %edi
	movl	$bestInterFAdjust4x4, %edx
	cmovneq	%rdi, %rdx
	andb	%r9b, %r11b
	movzbl	%r11b, %edi
	leal	(%rsi,%rdi,2), %edi
	movslq	%edi, %rdi
	leaq	(%rdi,%rdi,4), %rdi
	leal	3(,%rsi,4), %r12d
	movq	(%rbp), %rbp
	cmoveq	%rbx, %rdx
	movq	(%rdx), %r9
	movslq	AdaptRndPos(%r10,%rdi,4), %rdx
	movq	(%rbp,%rdx,8), %rdx
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB21_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_2 Depth 2
	movl	%r14d, %ebp
	andl	%r12d, %ebp
	shll	%cl, %ebp
	movq	(%r9,%r14,8), %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB21_2:                               #   Parent Loop BB21_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %esi
	andl	%r12d, %esi
	addl	%ebp, %esi
	movslq	%esi, %rsi
	movzwl	(%rdx,%rsi,2), %eax
	addl	(%rdi,%rbx,4), %eax
	cwtl
	testl	%eax, %eax
	cmovsl	%r15d, %eax
	cmpl	%r13d, %eax
	cmovgew	%r13w, %ax
	movw	%ax, (%rdx,%rsi,2)
	incq	%rbx
	cmpq	$16, %rbx
	jne	.LBB21_2
# BB#3:                                 #   in Loop: Header=BB21_1 Depth=1
	incq	%r14
	cmpq	$16, %r14
	jne	.LBB21_1
# BB#4:
	movq	input(%rip), %rax
	cmpl	$0, 5660(%rax)
	je	.LBB21_11
# BB#5:
	andb	$1, %r11b
	movl	$bestInterFAdjust4x4Cr, %eax
	movl	$bestIntraFAdjust4x4Cr, %ecx
	cmovneq	%rax, %rcx
	movslq	15548(%r8), %r9
	testq	%r9, %r9
	jle	.LBB21_11
# BB#6:                                 # %.lr.ph97
	cmpl	$0, 15544(%r8)
	jle	.LBB21_11
# BB#7:                                 # %.lr.ph97.split.us.preheader
	movzbl	%r11b, %eax
	leaq	(%rax,%rax,4), %rax
	movslq	AdaptRndCrPos(%r10,%rax,4), %rax
	movq	(%rcx), %rcx
	movq	OffsetList4x4(%rip), %rdx
	movq	img(%rip), %rsi
	movq	(%rcx), %r8
	movq	8(%rcx), %r10
	movq	(%rdx,%rax,8), %rdi
	movq	8(%rdx,%rax,8), %rcx
	movslq	15544(%rsi), %r14
	xorl	%r15d, %r15d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB21_8:                               # %.lr.ph97.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_9 Depth 2
	leaq	(,%r11,4), %r12
	andl	$12, %r12d
	movq	(%r8,%r11,8), %rdx
	movq	(%r10,%r11,8), %rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB21_9:                               #   Parent Loop BB21_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %ebx
	andl	$3, %ebx
	orq	%r12, %rbx
	movzwl	(%rdi,%rbx,2), %eax
	addl	(%rdx,%rbp,4), %eax
	cwtl
	testl	%eax, %eax
	cmovsl	%r15d, %eax
	cmpl	%r13d, %eax
	cmovgew	%r13w, %ax
	movw	%ax, (%rdi,%rbx,2)
	movzwl	(%rcx,%rbx,2), %eax
	addl	(%rsi,%rbp,4), %eax
	cwtl
	testl	%eax, %eax
	cmovsl	%r15d, %eax
	cmpl	%r13d, %eax
	cmovgew	%r13w, %ax
	movw	%ax, (%rcx,%rbx,2)
	incq	%rbp
	cmpq	%r14, %rbp
	jl	.LBB21_9
# BB#10:                                # %._crit_edge.us
                                        #   in Loop: Header=BB21_8 Depth=1
	incq	%r11
	cmpq	%r9, %r11
	jl	.LBB21_8
.LBB21_11:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	update_offset_params, .Lfunc_end21-update_offset_params
	.cfi_endproc

	.globl	SetRefAndMotionVectors
	.p2align	4, 0x90
	.type	SetRefAndMotionVectors,@function
SetRefAndMotionVectors:                 # @SetRefAndMotionVectors
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi202:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi203:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi204:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi205:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi206:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi207:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi208:
	.cfi_def_cfa_offset 112
.Lcfi209:
	.cfi_offset %rbx, -56
.Lcfi210:
	.cfi_offset %r12, -48
.Lcfi211:
	.cfi_offset %r13, -40
.Lcfi212:
	.cfi_offset %r14, -32
.Lcfi213:
	.cfi_offset %r15, -24
.Lcfi214:
	.cfi_offset %rbp, -16
	movl	%ecx, 4(%rsp)           # 4-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	%edi, %r14d
	movq	img(%rip), %r9
	leal	-1(%rsi), %ecx
	movl	%r14d, %eax
	andl	$-2, %eax
	leal	(%r14,%r14), %ebx
	andl	$2, %ebx
	cmpl	$3, %ecx
	movq	input(%rip), %rcx
	movslq	%esi, %r15
	movl	$4, %edi
	cmovbq	%r15, %rdi
	movl	140(%rcx,%rdi,8), %ebp
	leal	(%rbp,%rax), %r13d
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	136(%rcx,%rdi,8), %ecx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	leal	(%rcx,%rbx), %r12d
	testl	%edx, %edx
	js	.LBB22_1
# BB#20:
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	14224(%r9), %rcx
	movslq	12(%r9), %rdi
	cmpl	$1, 20(%r9)
	jne	.LBB22_21
# BB#32:                                # %.preheader
	testl	%ebp, %ebp
	movl	%r8d, %r14d
	jle	.LBB22_26
# BB#33:                                # %.lr.ph290
	imulq	$536, %rdi, %rdi        # imm = 0x218
	leaq	480(%rcx,%rdi), %r11
	movl	8(%rsp), %ecx           # 4-byte Reload
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movslq	%r12d, %r8
	movslq	%eax, %r12
	movslq	%r13d, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jg	.LBB22_35
	jmp	.LBB22_54
	.p2align	4, 0x90
.LBB22_55:                              # %._crit_edge282._crit_edge
                                        #   in Loop: Header=BB22_54 Depth=1
	movq	img(%rip), %r9
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB22_54
.LBB22_35:                              # %.lr.ph281
	movslq	172(%r9), %rax
	movslq	%r12d, %r13
	addq	%rax, %r13
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB22_36
	.p2align	4, 0x90
.LBB22_53:                              # %._crit_edge333
                                        #   in Loop: Header=BB22_36 Depth=1
	movq	img(%rip), %r9
.LBB22_36:                              # =>This Inner Loop Header: Depth=1
	movl	168(%r9), %eax
	addq	%rdi, %rax
	testl	%esi, %esi
	jne	.LBB22_38
# BB#37:                                #   in Loop: Header=BB22_36 Depth=1
	movq	direct_pdir(%rip), %rcx
	movq	(%rcx,%r13,8), %rcx
	movslq	%eax, %rbx
	movsbl	(%rcx,%rbx), %edx
	movq	direct_ref_idx(%rip), %rcx
	movq	(%rcx), %rbp
	movq	8(%rcx), %rcx
	movq	(%rbp,%r13,8), %rbp
	movsbl	(%rbp,%rbx), %ebp
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	movq	(%rcx,%r13,8), %rcx
	movsbl	(%rcx,%rbx), %r14d
.LBB22_38:                              #   in Loop: Header=BB22_36 Depth=1
	movl	%edx, %ecx
	orl	$2, %ecx
	cmpl	$2, %ecx
	jne	.LBB22_44
# BB#39:                                #   in Loop: Header=BB22_36 Depth=1
	cmpl	$1, %esi
	jne	.LBB22_43
# BB#40:                                #   in Loop: Header=BB22_36 Depth=1
	cmpl	$2, %edx
	jne	.LBB22_43
# BB#41:                                #   in Loop: Header=BB22_36 Depth=1
	movzwl	(%r11), %ecx
	testw	%cx, %cx
	je	.LBB22_43
# BB#42:                                #   in Loop: Header=BB22_36 Depth=1
	movzwl	%cx, %ecx
	leaq	14400(%r9), %rbx
	addq	$14392, %r9             # imm = 0x3838
	cmpl	$1, %ecx
	cmovneq	%rbx, %r9
	movq	(%r9), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movzwl	(%rcx), %r9d
	movq	enc_picture(%rip), %rbx
	movq	6512(%rbx), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	cltq
	movq	(%rbp,%rax,8), %rbp
	movw	%r9w, (%rbp)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rbp)
	movq	6488(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movb	$0, (%rcx,%rax)
	movq	enc_picture(%rip), %rbx
	movslq	-48(%r11), %rcx
	imulq	$264, %rcx, %rcx        # imm = 0x108
	movq	24(%rbx,%rcx), %rcx
	jmp	.LBB22_45
	.p2align	4, 0x90
.LBB22_44:                              #   in Loop: Header=BB22_36 Depth=1
	movq	enc_picture(%rip), %rcx
	movq	6512(%rcx), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	cltq
	movq	(%rbp,%rax,8), %rbp
	movl	$0, (%rbp)
	movq	6488(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movb	$-1, (%rcx,%rax)
	movq	$-1, %rcx
	movq	enc_picture(%rip), %rbx
	jmp	.LBB22_45
	.p2align	4, 0x90
.LBB22_43:                              #   in Loop: Header=BB22_36 Depth=1
	movq	14384(%r9), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx), %rcx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %rdx
	movq	%r11, %rsi
	movq	%r12, %r11
	movq	%r8, %r12
	movl	%r14d, %r8d
	movslq	4(%rsp), %r14           # 4-byte Folded Reload
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx,%r15,8), %rcx
	movzwl	(%rcx), %r9d
	movq	enc_picture(%rip), %rbx
	movq	6512(%rbx), %r10
	movq	(%r10), %rbp
	movq	(%rbp,%r13,8), %rbp
	cltq
	movq	(%rbp,%rax,8), %rbp
	movw	%r9w, (%rbp)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rbp)
	movq	6488(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movb	%r14b, (%rcx,%rax)
	movl	%r8d, %r14d
	movq	%r12, %r8
	movq	%r11, %r12
	movq	%rsi, %r11
	movq	%rdx, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	enc_picture(%rip), %rbx
	movslq	-48(%r11), %rcx
	movq	6488(%rbx), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movsbq	(%rbp,%rax), %rbp
	imulq	$264, %rcx, %rcx        # imm = 0x108
	addq	%rbx, %rcx
	movq	24(%rcx,%rbp,8), %rcx
.LBB22_45:                              #   in Loop: Header=BB22_36 Depth=1
	movq	6496(%rbx), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	%rcx, (%rbp,%rax,8)
	leal	-1(%rdx), %ecx
	cmpl	$1, %ecx
	ja	.LBB22_51
# BB#46:                                #   in Loop: Header=BB22_36 Depth=1
	cmpl	$1, %esi
	jne	.LBB22_50
# BB#47:                                #   in Loop: Header=BB22_36 Depth=1
	cmpl	$2, %edx
	jne	.LBB22_50
# BB#48:                                #   in Loop: Header=BB22_36 Depth=1
	movzwl	(%r11), %ecx
	testw	%cx, %cx
	je	.LBB22_50
# BB#49:                                #   in Loop: Header=BB22_36 Depth=1
	movzwl	%cx, %ecx
	movq	img(%rip), %rbp
	leaq	14400(%rbp), %r9
	addq	$14392, %rbp            # imm = 0x3838
	cmpl	$1, %ecx
	cmovneq	%r9, %rbp
	movq	(%rbp), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movzwl	(%rcx), %r9d
	movq	6512(%rbx), %rbp
	movq	8(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	(%rbp,%rax,8), %rbp
	movw	%r9w, (%rbp)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rbp)
	movq	6488(%rbx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movb	$0, (%rcx,%rax)
	movq	enc_picture(%rip), %rbx
	movslq	-48(%r11), %rcx
	imulq	$264, %rcx, %rcx        # imm = 0x108
	movq	288(%rbx,%rcx), %rcx
	jmp	.LBB22_52
	.p2align	4, 0x90
.LBB22_51:                              #   in Loop: Header=BB22_36 Depth=1
	movq	6512(%rbx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movl	$0, (%rcx)
	movq	6488(%rbx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movb	$-1, (%rcx,%rax)
	movq	$-1, %rcx
	movq	enc_picture(%rip), %rbx
	jmp	.LBB22_52
	.p2align	4, 0x90
.LBB22_50:                              #   in Loop: Header=BB22_36 Depth=1
	movq	img(%rip), %rcx
	movq	14384(%rcx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	8(%rcx), %rcx
	movslq	%r14d, %r10
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%r15,8), %rcx
	movzwl	(%rcx), %r9d
	movq	6512(%rbx), %rbp
	movq	8(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	(%rbp,%rax,8), %rbp
	movw	%r9w, (%rbp)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rbp)
	movq	6488(%rbx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movb	%r10b, (%rcx,%rax)
	movq	enc_picture(%rip), %rbx
	movslq	-48(%r11), %rcx
	imulq	$264, %rcx, %rcx        # imm = 0x108
	addq	%rbx, %rcx
	movq	6488(%rbx), %rbp
	movq	8(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movsbq	(%rbp,%rax), %rbp
	movq	288(%rcx,%rbp,8), %rcx
.LBB22_52:                              #   in Loop: Header=BB22_36 Depth=1
	movq	6496(%rbx), %rbp
	movq	8(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	%rcx, (%rbp,%rax,8)
	incq	%rdi
	cmpq	%r8, %rdi
	jl	.LBB22_53
.LBB22_54:                              # %._crit_edge282
                                        # =>This Inner Loop Header: Depth=1
	incq	%r12
	cmpq	16(%rsp), %r12          # 8-byte Folded Reload
	jl	.LBB22_55
	jmp	.LBB22_26
.LBB22_1:
	movl	172(%r9), %ecx
	addl	%ecx, %eax
	addl	%r13d, %ecx
	cmpl	%ecx, %eax
	jge	.LBB22_26
# BB#2:                                 # %.lr.ph275.preheader
	movslq	%eax, %r14
	pcmpeqd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB22_3:                               # %.lr.ph275
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_12 Depth 2
                                        #     Child Loop BB22_15 Depth 2
                                        #     Child Loop BB22_18 Depth 2
	movl	168(%r9), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	leal	(%rax,%rcx), %edx
	addl	%r12d, %eax
	cmpl	%eax, %edx
	movq	enc_picture(%rip), %r10
	jge	.LBB22_4
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB22_3 Depth=1
	movq	6496(%r10), %rcx
	movq	(%rcx), %rsi
	movq	8(%rcx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movslq	%edx, %rdi
	movslq	%eax, %rdx
	movq	(%rsi,%r14,8), %rsi
	movq	%rdx, %r11
	subq	%rdi, %r11
	cmpq	$4, %r11
	movq	%rdi, %rax
	jb	.LBB22_18
# BB#6:                                 # %min.iters.checked
                                        #   in Loop: Header=BB22_3 Depth=1
	movq	%r11, %r8
	andq	$-4, %r8
	movq	%rdi, %rax
	je	.LBB22_18
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB22_3 Depth=1
	leaq	(%rsi,%rdi,8), %rax
	leaq	(%rcx,%rdx,8), %rbp
	cmpq	%rbp, %rax
	jae	.LBB22_9
# BB#8:                                 # %vector.memcheck
                                        #   in Loop: Header=BB22_3 Depth=1
	leaq	(%rsi,%rdx,8), %rax
	leaq	(%rcx,%rdi,8), %rbp
	cmpq	%rax, %rbp
	movq	%rdi, %rax
	jb	.LBB22_18
.LBB22_9:                               # %vector.body.preheader
                                        #   in Loop: Header=BB22_3 Depth=1
	leaq	-4(%r8), %r9
	movl	%r9d, %eax
	shrl	$2, %eax
	incl	%eax
	andq	$3, %rax
	je	.LBB22_10
# BB#11:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB22_3 Depth=1
	leaq	16(%rcx,%rdi,8), %r15
	leaq	16(%rsi,%rdi,8), %rbp
	negq	%rax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB22_12:                              # %vector.body.prol
                                        #   Parent Loop BB22_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%rbp,%rbx,8)
	movdqu	%xmm0, (%rbp,%rbx,8)
	movdqu	%xmm0, -16(%r15,%rbx,8)
	movdqu	%xmm0, (%r15,%rbx,8)
	addq	$4, %rbx
	incq	%rax
	jne	.LBB22_12
	jmp	.LBB22_13
	.p2align	4, 0x90
.LBB22_4:                               # %.lr.ph275.._crit_edge_crit_edge
                                        #   in Loop: Header=BB22_3 Depth=1
	movslq	%edx, %rdi
	jmp	.LBB22_19
.LBB22_10:                              #   in Loop: Header=BB22_3 Depth=1
	xorl	%ebx, %ebx
.LBB22_13:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB22_3 Depth=1
	cmpq	$12, %r9
	jb	.LBB22_16
# BB#14:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB22_3 Depth=1
	movq	%r8, %rax
	subq	%rbx, %rax
	addq	%rdi, %rbx
	leaq	112(%rcx,%rbx,8), %rbp
	leaq	112(%rsi,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB22_15:                              # %vector.body
                                        #   Parent Loop BB22_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -112(%rbx)
	movdqu	%xmm0, -96(%rbx)
	movdqu	%xmm0, -112(%rbp)
	movdqu	%xmm0, -96(%rbp)
	movdqu	%xmm0, -80(%rbx)
	movdqu	%xmm0, -64(%rbx)
	movdqu	%xmm0, -80(%rbp)
	movdqu	%xmm0, -64(%rbp)
	movdqu	%xmm0, -48(%rbx)
	movdqu	%xmm0, -32(%rbx)
	movdqu	%xmm0, -48(%rbp)
	movdqu	%xmm0, -32(%rbp)
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	movdqu	%xmm0, -16(%rbp)
	movdqu	%xmm0, (%rbp)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-16, %rax
	jne	.LBB22_15
.LBB22_16:                              # %middle.block
                                        #   in Loop: Header=BB22_3 Depth=1
	cmpq	%r8, %r11
	je	.LBB22_19
# BB#17:                                #   in Loop: Header=BB22_3 Depth=1
	addq	%rdi, %r8
	movq	%r8, %rax
	.p2align	4, 0x90
.LBB22_18:                              # %scalar.ph
                                        #   Parent Loop BB22_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	$-1, (%rsi,%rax,8)
	movq	$-1, (%rcx,%rax,8)
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB22_18
.LBB22_19:                              # %._crit_edge
                                        #   in Loop: Header=BB22_3 Depth=1
	movq	6488(%r10), %rax
	movq	(%rax), %rax
	addq	(%rax,%r14,8), %rdi
	movq	input(%rip), %rax
	movq	16(%rsp), %rbx          # 8-byte Reload
	movslq	136(%rax,%rbx,8), %rdx
	movl	$255, %esi
	callq	memset
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movslq	168(%rcx), %rdi
	movslq	8(%rsp), %rbp           # 4-byte Folded Reload
	addq	%rbp, %rdi
	addq	(%rax,%r14,8), %rdi
	movq	input(%rip), %rax
	movslq	136(%rax,%rbx,8), %rdx
	movl	$255, %esi
	callq	memset
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	img(%rip), %rcx
	movslq	168(%rcx), %rcx
	addq	%rbp, %rcx
	movq	(%rax,%rcx,8), %rdi
	movq	input(%rip), %rax
	movslq	136(%rax,%rbx,8), %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	img(%rip), %rcx
	movslq	168(%rcx), %rcx
	addq	%rbp, %rcx
	movq	(%rax,%rcx,8), %rdi
	movq	input(%rip), %rax
	movslq	136(%rax,%rbx,8), %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
	incq	%r14
	movq	img(%rip), %r9
	movslq	172(%r9), %rax
	movslq	%r13d, %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %r14
	pcmpeqd	%xmm0, %xmm0
	jl	.LBB22_3
	jmp	.LBB22_26
.LBB22_21:                              # %.preheader268
	testl	%ebp, %ebp
	jle	.LBB22_26
# BB#22:                                # %.lr.ph296
	movq	24(%rsp), %rsi          # 8-byte Reload
	testl	%esi, %esi
	jle	.LBB22_23
# BB#27:                                # %.lr.ph296.split.us.preheader
	imulq	$536, %rdi, %rdx        # imm = 0x218
	movl	%r13d, %edi
	movslq	4(%rsp), %r13           # 4-byte Folded Reload
	leaq	432(%rcx,%rdx), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	8(%rsp), %ecx           # 4-byte Reload
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movslq	%r12d, %r14
	movslq	%eax, %r12
	movslq	%edi, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	enc_picture(%rip), %rax
	movl	168(%r9), %r11d
	jmp	.LBB22_28
	.p2align	4, 0x90
.LBB22_31:                              # %._crit_edge294.us..lr.ph296.split.us_crit_edge
                                        #   in Loop: Header=BB22_28 Depth=1
	movq	input(%rip), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	136(%rax,%rcx,8), %esi
	movq	24(%rsp), %rax          # 8-byte Reload
.LBB22_28:                              # %.lr.ph296.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_29 Depth 2
	movslq	172(%r9), %rcx
	movslq	%r12d, %rbp
	addq	%rcx, %rbp
	movq	6488(%rax), %rcx
	movq	(%rcx), %rcx
	addl	8(%rsp), %r11d          # 4-byte Folded Reload
	movslq	%r11d, %rdi
	addq	(%rcx,%rbp,8), %rdi
	movslq	%esi, %rdx
	movzbl	4(%rsp), %esi           # 1-byte Folded Reload
	callq	memset
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	(%rax), %rcx
	movq	img(%rip), %r9
	movl	168(%r9), %r11d
	movq	14384(%r9), %rdx
	movq	(%rdx,%r12,8), %r10
	movq	enc_picture(%rip), %rax
	movq	6496(%rax), %rsi
	movq	6512(%rax), %rdi
	movq	(%rsi), %rsi
	movq	(%rsi,%rbp,8), %rsi
	movq	(%rdi), %rdi
	movq	(%rdi,%rbp,8), %rdi
	imulq	$264, %rcx, %rcx        # imm = 0x108
	movq	%rax, 24(%rsp)          # 8-byte Spill
	addq	%rax, %rcx
	leaq	24(%rcx,%r13,8), %rcx
	movq	48(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB22_29:                              #   Parent Loop BB22_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r11,%rbp), %ebx
	movq	(%r10,%rbp,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%r15,8), %rax
	movzwl	(%rax), %edx
	movslq	%ebx, %rbx
	movq	(%rdi,%rbx,8), %r8
	movw	%dx, (%r8)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%r8)
	movq	(%rcx), %rax
	movq	%rax, (%rsi,%rbx,8)
	incq	%rbp
	cmpq	%r14, %rbp
	jl	.LBB22_29
# BB#30:                                # %._crit_edge294.us
                                        #   in Loop: Header=BB22_28 Depth=1
	incq	%r12
	cmpq	40(%rsp), %r12          # 8-byte Folded Reload
	jl	.LBB22_31
	jmp	.LBB22_26
.LBB22_23:                              # %.lr.ph296.split.preheader
	movslq	%esi, %rdx
	movslq	172(%r9), %rcx
	cltq
	addq	%rcx, %rax
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	168(%r9), %rdi
	movslq	8(%rsp), %rbp           # 4-byte Folded Reload
	addq	%rbp, %rdi
	addq	(%rcx,%rax,8), %rdi
	movzbl	4(%rsp), %ebx           # 1-byte Folded Reload
	movl	%ebx, %esi
	callq	memset
	orl	$1, %r14d
	cmpl	%r13d, %r14d
	jge	.LBB22_26
	.p2align	4, 0x90
.LBB22_24:                              # %.lr.ph296.split..lr.ph296.split_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rax
	movq	input(%rip), %rcx
	movl	172(%rax), %edx
	addl	%r14d, %edx
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rsi
	movq	(%rsi), %rsi
	movslq	%edx, %rdx
	movslq	168(%rax), %rdi
	addq	%rbp, %rdi
	addq	(%rsi,%rdx,8), %rdi
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	136(%rcx,%rax,8), %rdx
	movl	%ebx, %esi
	callq	memset
	incl	%r14d
	cmpl	%r13d, %r14d
	jl	.LBB22_24
.LBB22_26:                              # %.loopexit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	SetRefAndMotionVectors, .Lfunc_end22-SetRefAndMotionVectors
	.cfi_endproc

	.globl	StoreMVBlock8x8
	.p2align	4, 0x90
	.type	StoreMVBlock8x8,@function
StoreMVBlock8x8:                        # @StoreMVBlock8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi215:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi216:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi217:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi218:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi219:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi220:
	.cfi_def_cfa_offset 56
.Lcfi221:
	.cfi_offset %rbx, -56
.Lcfi222:
	.cfi_offset %r12, -48
.Lcfi223:
	.cfi_offset %r13, -40
.Lcfi224:
	.cfi_offset %r14, -32
.Lcfi225:
	.cfi_offset %r15, -24
.Lcfi226:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	img(%rip), %rax
	movq	14376(%rax), %r13
	movq	14384(%rax), %r11
	movslq	%edi, %r10
	leal	(%rsi,%rsi), %edi
	andl	$2, %edi
	andl	$-2, %esi
	leal	2(%rdi), %ebx
	leal	2(%rsi), %ebp
	cmpl	$0, 56(%rsp)
	je	.LBB23_1
# BB#7:
	testl	%r9d, %r9d
	je	.LBB23_17
# BB#8:
	cmpl	$1, %r9d
	je	.LBB23_23
# BB#9:
	cmpl	$2, %r9d
	jne	.LBB23_33
# BB#10:                                # %.preheader247
	movq	%r13, %rax
	movslq	%ecx, %r13
	movslq	%edx, %r9
	movl	%edi, %edx
	movslq	%esi, %r12
	movq	(%r11,%r12,8), %rcx
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movq	(%rax,%r12,8), %rax
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	movq	(%rcx,%rdx,8), %r15
	movq	(%r15), %rsi
	movq	(%rsi,%r13,8), %rsi
	movq	(%rsi,%r9,8), %rsi
	movzwl	(%rsi), %ecx
	shlq	$7, %r10
	movq	%r12, %rdi
	shlq	$4, %rdi
	addq	%r10, %rdi
	movw	%cx, all_mv8x8(%rdi,%rdx,4)
	movzwl	2(%rsi), %ecx
	movw	%cx, all_mv8x8+2(%rdi,%rdx,4)
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movq	(%rax,%rdx,8), %r14
	movq	(%r14), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%r9,8), %rsi
	movzwl	(%rsi), %ecx
	movw	%cx, pred_mv8x8(%rdi,%rdx,4)
	movl	%ebx, %ecx
	movslq	%ebp, %rbp
	movzwl	2(%rsi), %esi
	movw	%si, pred_mv8x8+2(%rdi,%rdx,4)
	movq	%rdx, %rsi
	orq	$1, %rsi
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rsi
	jae	.LBB23_11
# BB#32:
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rsi,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %ebx
	movw	%bx, all_mv8x8(%rdi,%rsi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, all_mv8x8+2(%rdi,%rsi,4)
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rsi,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %ebx
	movw	%bx, pred_mv8x8(%rdi,%rsi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, pred_mv8x8+2(%rdi,%rsi,4)
.LBB23_11:
	orq	$1, %r12
	movq	%rbp, -8(%rsp)          # 8-byte Spill
	cmpq	%rbp, %r12
	jge	.LBB23_14
# BB#12:                                # %.preheader246.1
	movq	(%r11,%r12,8), %rcx
	movq	-24(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r12,8), %rax
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %ebx
	movq	%r12, %rbp
	shlq	$4, %rbp
	addq	%r10, %rbp
	movw	%bx, all_mv8x8(%rbp,%rdx,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, all_mv8x8+2(%rbp,%rdx,4)
	movq	(%rax,%rdx,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %ebx
	movw	%bx, pred_mv8x8(%rbp,%rdx,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, pred_mv8x8+2(%rbp,%rdx,4)
	cmpq	-48(%rsp), %rsi         # 8-byte Folded Reload
	jae	.LBB23_14
# BB#13:
	movq	-16(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rsi,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %ebx
	movw	%bx, all_mv8x8(%rbp,%rsi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, all_mv8x8+2(%rbp,%rsi,4)
	movq	(%rax,%rsi,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %ebx
	movw	%bx, pred_mv8x8(%rbp,%rsi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, pred_mv8x8+2(%rbp,%rsi,4)
.LBB23_14:                              # %.preheader244
	cmpq	-48(%rsp), %rsi         # 8-byte Folded Reload
	movslq	%r8d, %rbp
	movq	8(%r15), %rbx
	movq	(%rbx,%rbp,8), %rbx
	movq	(%rbx,%r9,8), %rbx
	movzwl	(%rbx), %ecx
	movw	%cx, all_mv8x8+64(%rdi,%rdx,4)
	movzwl	2(%rbx), %ecx
	movw	%cx, all_mv8x8+66(%rdi,%rdx,4)
	movq	8(%r14), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %ebx
	movw	%bx, pred_mv8x8+64(%rdi,%rdx,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, pred_mv8x8+66(%rdi,%rdx,4)
	jae	.LBB23_15
# BB#29:
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rsi,8), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %ebx
	movw	%bx, all_mv8x8+64(%rdi,%rsi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, all_mv8x8+66(%rdi,%rsi,4)
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rsi,8), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %ebx
	movw	%bx, pred_mv8x8+64(%rdi,%rsi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, pred_mv8x8+66(%rdi,%rsi,4)
.LBB23_15:
	cmpq	-8(%rsp), %r12          # 8-byte Folded Reload
	movq	-24(%rsp), %rax         # 8-byte Reload
	jge	.LBB23_16
# BB#30:                                # %.preheader243.1
	movq	(%r11,%r12,8), %rbx
	movq	(%rax,%r12,8), %r8
	movq	(%rbx,%rdx,8), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %edi
	shlq	$4, %r12
	addq	%r12, %r10
	movw	%di, all_mv8x8+64(%r10,%rdx,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, all_mv8x8+66(%r10,%rdx,4)
	movq	(%r8,%rdx,8), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %edi
	movw	%di, pred_mv8x8+64(%r10,%rdx,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, pred_mv8x8+66(%r10,%rdx,4)
	cmpq	-48(%rsp), %rsi         # 8-byte Folded Reload
	jae	.LBB23_16
# BB#31:
	movq	(%rbx,%rsi,8), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %edx
	movw	%dx, all_mv8x8+64(%r10,%rsi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, all_mv8x8+66(%r10,%rsi,4)
	movq	(%r8,%rsi,8), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	jmp	.LBB23_28
.LBB23_1:
	testl	%r9d, %r9d
	js	.LBB23_16
# BB#2:                                 # %.preheader236
	movslq	%ecx, %r15
	movl	%edi, %edx
	movl	%ebx, %r8d
	movslq	%esi, %rdi
	movslq	%ebp, %r9
	movq	(%r11,%rdi,8), %rbp
	movq	(%r13,%rdi,8), %r14
	movq	(%rbp,%rdx,8), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%r15,8), %rsi
	movq	32(%rsi), %rsi
	movzwl	(%rsi), %ecx
	shlq	$7, %r10
	movq	%rdi, %rbx
	shlq	$4, %rbx
	addq	%r10, %rbx
	movw	%cx, all_mv8x8(%rbx,%rdx,4)
	movzwl	2(%rsi), %ecx
	movw	%cx, all_mv8x8+2(%rbx,%rdx,4)
	movq	(%r14,%rdx,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	32(%rcx), %rcx
	movzwl	(%rcx), %esi
	movw	%si, pred_mv8x8(%rbx,%rdx,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, pred_mv8x8+2(%rbx,%rdx,4)
	movq	%rdx, %rsi
	orq	$1, %rsi
	cmpq	%r8, %rsi
	jae	.LBB23_3
# BB#21:
	movq	(%rbp,%rsi,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	32(%rcx), %rcx
	movzwl	(%rcx), %ebp
	movw	%bp, all_mv8x8(%rbx,%rsi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, all_mv8x8+2(%rbx,%rsi,4)
	movq	(%r14,%rsi,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	32(%rcx), %rcx
	movzwl	(%rcx), %ebp
	movw	%bp, pred_mv8x8(%rbx,%rsi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, pred_mv8x8+2(%rbx,%rsi,4)
.LBB23_3:
	orq	$1, %rdi
	cmpq	%r9, %rdi
	jge	.LBB23_16
# BB#4:                                 # %.preheader.1
	movq	(%r11,%rdi,8), %rbx
	movq	(%r13,%rdi,8), %r9
	movq	(%rbx,%rdx,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	32(%rcx), %rcx
	movzwl	(%rcx), %ebp
	shlq	$4, %rdi
	addq	%rdi, %r10
	movw	%bp, all_mv8x8(%r10,%rdx,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, all_mv8x8+2(%r10,%rdx,4)
	movq	(%r9,%rdx,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	32(%rcx), %rcx
	movzwl	(%rcx), %edi
	movw	%di, pred_mv8x8(%r10,%rdx,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, pred_mv8x8+2(%r10,%rdx,4)
	cmpq	%r8, %rsi
	jae	.LBB23_16
# BB#5:
	movq	(%rbx,%rsi,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	32(%rcx), %rcx
	movzwl	(%rcx), %edx
	movw	%dx, all_mv8x8(%r10,%rsi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, all_mv8x8+2(%r10,%rsi,4)
	movq	(%r9,%rsi,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	32(%rcx), %rcx
	jmp	.LBB23_6
.LBB23_17:                              # %.preheader238
	movslq	%ecx, %r14
	movslq	%edx, %r12
	movl	%edi, %edi
	movl	%ebx, %r8d
	movslq	%esi, %rcx
	movslq	%ebp, %r9
	movq	(%r11,%rcx,8), %rbp
	movq	(%r13,%rcx,8), %r15
	movq	(%rbp,%rdi,8), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%r14,8), %rsi
	movq	(%rsi,%r12,8), %rsi
	movzwl	(%rsi), %edx
	shlq	$7, %r10
	movq	%rcx, %rbx
	shlq	$4, %rbx
	addq	%r10, %rbx
	movw	%dx, all_mv8x8(%rbx,%rdi,4)
	movzwl	2(%rsi), %edx
	movw	%dx, all_mv8x8+2(%rbx,%rdi,4)
	movq	(%r15,%rdi,8), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r12,8), %rdx
	movzwl	(%rdx), %esi
	movw	%si, pred_mv8x8(%rbx,%rdi,4)
	movzwl	2(%rdx), %edx
	movw	%dx, pred_mv8x8+2(%rbx,%rdi,4)
	movq	%rdi, %rsi
	orq	$1, %rsi
	cmpq	%r8, %rsi
	jae	.LBB23_18
# BB#22:
	movq	(%rbp,%rsi,8), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r12,8), %rdx
	movzwl	(%rdx), %ebp
	movw	%bp, all_mv8x8(%rbx,%rsi,4)
	movzwl	2(%rdx), %edx
	movw	%dx, all_mv8x8+2(%rbx,%rsi,4)
	movq	(%r15,%rsi,8), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r12,8), %rdx
	movzwl	(%rdx), %ebp
	movw	%bp, pred_mv8x8(%rbx,%rsi,4)
	movzwl	2(%rdx), %edx
	movw	%dx, pred_mv8x8+2(%rbx,%rsi,4)
.LBB23_18:
	orq	$1, %rcx
	cmpq	%r9, %rcx
	jge	.LBB23_16
# BB#19:                                # %.preheader237.1
	movq	(%r11,%rcx,8), %rbx
	movq	(%r13,%rcx,8), %r9
	movq	(%rbx,%rdi,8), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r12,8), %rdx
	movzwl	(%rdx), %ebp
	shlq	$4, %rcx
	addq	%rcx, %r10
	movw	%bp, all_mv8x8(%r10,%rdi,4)
	movzwl	2(%rdx), %ecx
	movw	%cx, all_mv8x8+2(%r10,%rdi,4)
	movq	(%r9,%rdi,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx,%r12,8), %rcx
	movzwl	(%rcx), %edx
	movw	%dx, pred_mv8x8(%r10,%rdi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, pred_mv8x8+2(%r10,%rdi,4)
	cmpq	%r8, %rsi
	jae	.LBB23_16
# BB#20:
	movq	(%rbx,%rsi,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx,%r12,8), %rcx
	movzwl	(%rcx), %edx
	movw	%dx, all_mv8x8(%r10,%rsi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, all_mv8x8+2(%r10,%rsi,4)
	movq	(%r9,%rsi,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx,%r12,8), %rcx
.LBB23_6:                               # %.loopexit
	movzwl	(%rcx), %edx
	movw	%dx, pred_mv8x8(%r10,%rsi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, pred_mv8x8+2(%r10,%rsi,4)
	jmp	.LBB23_16
.LBB23_23:                              # %.preheader241
	movslq	%r8d, %r14
	movslq	%edx, %r12
	movl	%edi, %edi
	movl	%ebx, %r8d
	movslq	%esi, %rcx
	movslq	%ebp, %r9
	movq	(%r11,%rcx,8), %rbp
	movq	(%r13,%rcx,8), %r15
	movq	(%rbp,%rdi,8), %rsi
	movq	8(%rsi), %rsi
	movq	(%rsi,%r14,8), %rsi
	movq	(%rsi,%r12,8), %rsi
	movzwl	(%rsi), %edx
	shlq	$7, %r10
	movq	%rcx, %rbx
	shlq	$4, %rbx
	addq	%r10, %rbx
	movw	%dx, all_mv8x8+64(%rbx,%rdi,4)
	movzwl	2(%rsi), %edx
	movw	%dx, all_mv8x8+66(%rbx,%rdi,4)
	movq	(%r15,%rdi,8), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r12,8), %rdx
	movzwl	(%rdx), %esi
	movw	%si, pred_mv8x8+64(%rbx,%rdi,4)
	movzwl	2(%rdx), %edx
	movw	%dx, pred_mv8x8+66(%rbx,%rdi,4)
	movq	%rdi, %rsi
	orq	$1, %rsi
	cmpq	%r8, %rsi
	jae	.LBB23_25
# BB#24:
	movq	(%rbp,%rsi,8), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r12,8), %rdx
	movzwl	(%rdx), %ebp
	movw	%bp, all_mv8x8+64(%rbx,%rsi,4)
	movzwl	2(%rdx), %edx
	movw	%dx, all_mv8x8+66(%rbx,%rsi,4)
	movq	(%r15,%rsi,8), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r12,8), %rdx
	movzwl	(%rdx), %ebp
	movw	%bp, pred_mv8x8+64(%rbx,%rsi,4)
	movzwl	2(%rdx), %edx
	movw	%dx, pred_mv8x8+66(%rbx,%rsi,4)
.LBB23_25:
	orq	$1, %rcx
	cmpq	%r9, %rcx
	jge	.LBB23_16
# BB#26:                                # %.preheader240.1
	movq	(%r11,%rcx,8), %rbx
	movq	(%r13,%rcx,8), %r9
	movq	(%rbx,%rdi,8), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%r14,8), %rdx
	movq	(%rdx,%r12,8), %rdx
	movzwl	(%rdx), %ebp
	shlq	$4, %rcx
	addq	%rcx, %r10
	movw	%bp, all_mv8x8+64(%r10,%rdi,4)
	movzwl	2(%rdx), %ecx
	movw	%cx, all_mv8x8+66(%r10,%rdi,4)
	movq	(%r9,%rdi,8), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx,%r12,8), %rcx
	movzwl	(%rcx), %edx
	movw	%dx, pred_mv8x8+64(%r10,%rdi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, pred_mv8x8+66(%r10,%rdi,4)
	cmpq	%r8, %rsi
	jae	.LBB23_16
# BB#27:
	movq	(%rbx,%rsi,8), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx,%r12,8), %rcx
	movzwl	(%rcx), %edx
	movw	%dx, all_mv8x8+64(%r10,%rsi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, all_mv8x8+66(%r10,%rsi,4)
	movq	(%r9,%rsi,8), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx,%r12,8), %rcx
.LBB23_28:                              # %.loopexit
	movzwl	(%rcx), %edx
	movw	%dx, pred_mv8x8+64(%r10,%rsi,4)
	movzwl	2(%rcx), %ecx
	movw	%cx, pred_mv8x8+66(%r10,%rsi,4)
.LBB23_16:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_33:
	movl	$.L.str.1, %edi
	movl	$255, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.Lfunc_end23:
	.size	StoreMVBlock8x8, .Lfunc_end23-StoreMVBlock8x8
	.cfi_endproc

	.globl	RestoreMVBlock8x8
	.p2align	4, 0x90
	.type	RestoreMVBlock8x8,@function
RestoreMVBlock8x8:                      # @RestoreMVBlock8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi227:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi228:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi229:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi230:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi231:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi232:
	.cfi_def_cfa_offset 56
.Lcfi233:
	.cfi_offset %rbx, -56
.Lcfi234:
	.cfi_offset %r12, -48
.Lcfi235:
	.cfi_offset %r13, -40
.Lcfi236:
	.cfi_offset %r14, -32
.Lcfi237:
	.cfi_offset %r15, -24
.Lcfi238:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movq	14376(%rax), %r8
	movq	14384(%rax), %r9
	movslq	%edi, %r14
	movslq	%esi, %rdi
	movb	6212(%rsp,%rdi), %al
	movsbq	6216(%rsp,%rdi), %r13
	leal	(%rdi,%rdi), %ecx
	andl	$2, %ecx
	movl	%edi, %ebx
	andl	$-2, %ebx
	leal	2(%rcx), %esi
	leal	2(%rbx), %ebp
	testl	%edx, %edx
	je	.LBB24_14
# BB#1:
	movswq	6204(%rsp,%rdi,2), %r11
	testb	%al, %al
	je	.LBB24_20
# BB#2:
	movsbq	6220(%rsp,%rdi), %r10
	cmpb	$1, %al
	je	.LBB24_26
# BB#3:
	cmpb	$2, %al
	jne	.LBB24_33
# BB#4:                                 # %.preheader259
	movl	%ecx, %edi
	movslq	%ebx, %r12
	movq	(%r9,%r12,8), %rdx
	movq	(%r8,%r12,8), %rbx
	shlq	$7, %r14
	movq	%r12, %rcx
	shlq	$4, %rcx
	addq	%r14, %rcx
	movzwl	all_mv8x8(%rcx,%rdi,4), %eax
	movq	%rdx, -40(%rsp)         # 8-byte Spill
	movq	(%rdx,%rdi,8), %rdx
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	movq	(%rdx), %rdx
	movq	(%rdx,%r13,8), %rdx
	movq	(%rdx,%r11,8), %rdx
	movw	%ax, (%rdx)
	movzwl	all_mv8x8+2(%rcx,%rdi,4), %eax
	movw	%ax, 2(%rdx)
	movzwl	pred_mv8x8(%rcx,%rdi,4), %eax
	movq	%rbx, -32(%rsp)         # 8-byte Spill
	movq	(%rbx,%rdi,8), %rdx
	movq	%rdx, -8(%rsp)          # 8-byte Spill
	movq	(%rdx), %rdx
	movq	(%rdx,%r13,8), %rdx
	movq	(%rdx,%r11,8), %rdx
	movw	%ax, (%rdx)
	movzwl	pred_mv8x8+2(%rcx,%rdi,4), %eax
	movw	%ax, 2(%rdx)
	movl	%esi, %eax
	movslq	%ebp, %rbx
	movq	%rdi, %rdx
	orq	$1, %rdx
	cmpq	%rax, %rdx
	jae	.LBB24_6
# BB#5:
	movzwl	all_mv8x8(%rcx,%rdx,4), %esi
	movq	%rbx, %r15
	movq	-40(%rsp), %rbx         # 8-byte Reload
	movq	(%rbx,%rdx,8), %rbp
	movq	%r15, %rbx
	movq	(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	(%rbp,%r11,8), %rbp
	movw	%si, (%rbp)
	movzwl	all_mv8x8+2(%rcx,%rdx,4), %esi
	movw	%si, 2(%rbp)
	movzwl	pred_mv8x8(%rcx,%rdx,4), %esi
	movq	-32(%rsp), %rbp         # 8-byte Reload
	movq	(%rbp,%rdx,8), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	(%rbp,%r11,8), %rbp
	movw	%si, (%rbp)
	movzwl	pred_mv8x8+2(%rcx,%rdx,4), %esi
	movw	%si, 2(%rbp)
.LBB24_6:
	orq	$1, %r12
	cmpq	%rbx, %r12
	jge	.LBB24_9
# BB#7:                                 # %.preheader258.1
	movq	%r9, %rsi
	movq	(%rsi,%r12,8), %r9
	movq	%r8, -48(%rsp)          # 8-byte Spill
	movq	-48(%rsp), %rbp         # 8-byte Reload
	movq	(%rbp,%r12,8), %r8
	movq	%r12, %rbp
	shlq	$4, %rbp
	addq	%r14, %rbp
	movzwl	all_mv8x8(%rbp,%rdi,4), %r15d
	movw	%r15w, -64(%rsp)        # 2-byte Spill
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movq	%r9, -24(%rsp)          # 8-byte Spill
	movq	(%r9,%rdi,8), %rax
	movq	%rsi, %r9
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%r11,8), %rax
	movzwl	-64(%rsp), %esi         # 2-byte Folded Reload
	movw	%si, (%rax)
	movzwl	all_mv8x8+2(%rbp,%rdi,4), %esi
	movw	%si, 2(%rax)
	movzwl	pred_mv8x8(%rbp,%rdi,4), %eax
	movq	%r8, -64(%rsp)          # 8-byte Spill
	movq	(%r8,%rdi,8), %rsi
	movq	-48(%rsp), %r8          # 8-byte Reload
	movq	(%rsi), %rsi
	movq	(%rsi,%r13,8), %rsi
	movq	(%rsi,%r11,8), %rsi
	movw	%ax, (%rsi)
	movzwl	pred_mv8x8+2(%rbp,%rdi,4), %eax
	movw	%ax, 2(%rsi)
	movq	-56(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %rdx
	jae	.LBB24_9
# BB#8:
	movzwl	all_mv8x8(%rbp,%rdx,4), %eax
	movq	-24(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rdx,8), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%r13,8), %rsi
	movq	(%rsi,%r11,8), %rsi
	movw	%ax, (%rsi)
	movzwl	all_mv8x8+2(%rbp,%rdx,4), %eax
	movw	%ax, 2(%rsi)
	movzwl	pred_mv8x8(%rbp,%rdx,4), %eax
	movq	-64(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rdx,8), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%r13,8), %rsi
	movq	(%rsi,%r11,8), %rsi
	movw	%ax, (%rsi)
	movzwl	pred_mv8x8+2(%rbp,%rdx,4), %eax
	movw	%ax, 2(%rsi)
	movq	-56(%rsp), %rax         # 8-byte Reload
.LBB24_9:                               # %.preheader256
	cmpq	%rax, %rdx
	movzwl	all_mv8x8+64(%rcx,%rdi,4), %esi
	movq	-16(%rsp), %rbp         # 8-byte Reload
	movq	8(%rbp), %rbp
	movq	(%rbp,%r10,8), %rbp
	movq	(%rbp,%r11,8), %rbp
	movw	%si, (%rbp)
	movzwl	all_mv8x8+66(%rcx,%rdi,4), %esi
	movw	%si, 2(%rbp)
	movzwl	pred_mv8x8+64(%rcx,%rdi,4), %esi
	movq	-8(%rsp), %rbp          # 8-byte Reload
	movq	8(%rbp), %rbp
	movq	(%rbp,%r10,8), %rbp
	movq	(%rbp,%r11,8), %rbp
	movw	%si, (%rbp)
	movzwl	pred_mv8x8+66(%rcx,%rdi,4), %esi
	movw	%si, 2(%rbp)
	jae	.LBB24_11
# BB#10:
	movzwl	all_mv8x8+64(%rcx,%rdx,4), %r15d
	movq	%rbx, %rsi
	movq	-40(%rsp), %rbx         # 8-byte Reload
	movq	(%rbx,%rdx,8), %rbp
	movq	%rsi, %rbx
	movq	8(%rbp), %rbp
	movq	(%rbp,%r10,8), %rbp
	movq	(%rbp,%r11,8), %rbp
	movw	%r15w, (%rbp)
	movzwl	all_mv8x8+66(%rcx,%rdx,4), %esi
	movw	%si, 2(%rbp)
	movzwl	pred_mv8x8+64(%rcx,%rdx,4), %esi
	movq	-32(%rsp), %rbp         # 8-byte Reload
	movq	(%rbp,%rdx,8), %rbp
	movq	8(%rbp), %rbp
	movq	(%rbp,%r10,8), %rbp
	movq	(%rbp,%r11,8), %rbp
	movw	%si, (%rbp)
	movzwl	pred_mv8x8+66(%rcx,%rdx,4), %ecx
	movw	%cx, 2(%rbp)
.LBB24_11:
	cmpq	%rbx, %r12
	jge	.LBB24_32
# BB#12:                                # %.preheader255.1
	movq	(%r9,%r12,8), %rsi
	movq	(%r8,%r12,8), %rcx
	shlq	$4, %r12
	addq	%r12, %r14
	movzwl	all_mv8x8+64(%r14,%rdi,4), %ebp
	movq	(%rsi,%rdi,8), %rbx
	movq	8(%rbx), %rbx
	movq	(%rbx,%r10,8), %rbx
	movq	(%rbx,%r11,8), %rbx
	movw	%bp, (%rbx)
	movzwl	all_mv8x8+66(%r14,%rdi,4), %ebp
	movw	%bp, 2(%rbx)
	movzwl	pred_mv8x8+64(%r14,%rdi,4), %ebp
	movq	(%rcx,%rdi,8), %rbx
	movq	8(%rbx), %rbx
	movq	(%rbx,%r10,8), %rbx
	movq	(%rbx,%r11,8), %rbx
	movw	%bp, (%rbx)
	movzwl	pred_mv8x8+66(%r14,%rdi,4), %edi
	movw	%di, 2(%rbx)
	cmpq	%rax, %rdx
	jae	.LBB24_32
# BB#13:
	movzwl	all_mv8x8+64(%r14,%rdx,4), %eax
	movq	(%rsi,%rdx,8), %rsi
	movq	8(%rsi), %rsi
	movq	(%rsi,%r10,8), %rsi
	movq	(%rsi,%r11,8), %rsi
	movw	%ax, (%rsi)
	movzwl	all_mv8x8+66(%r14,%rdx,4), %eax
	movw	%ax, 2(%rsi)
	movzwl	pred_mv8x8+64(%r14,%rdx,4), %eax
	movq	(%rcx,%rdx,8), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%r11,8), %rcx
	movw	%ax, (%rcx)
	movzwl	pred_mv8x8+66(%r14,%rdx,4), %eax
	jmp	.LBB24_31
.LBB24_14:
	testb	%al, %al
	js	.LBB24_32
# BB#15:                                # %.preheader248
	movl	%ecx, %ecx
	movl	%esi, %r10d
	movslq	%ebx, %rbx
	movslq	%ebp, %r11
	movq	(%r9,%rbx,8), %rax
	movq	(%r8,%rbx,8), %rdx
	shlq	$7, %r14
	movq	%rbx, %rsi
	shlq	$4, %rsi
	addq	%r14, %rsi
	movzwl	all_mv8x8(%rsi,%rcx,4), %edi
	movq	(%rax,%rcx,8), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	32(%rbp), %rbp
	movw	%di, (%rbp)
	movzwl	all_mv8x8+2(%rsi,%rcx,4), %edi
	movw	%di, 2(%rbp)
	movzwl	pred_mv8x8(%rsi,%rcx,4), %edi
	movq	(%rdx,%rcx,8), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	32(%rbp), %rbp
	movw	%di, (%rbp)
	movzwl	pred_mv8x8+2(%rsi,%rcx,4), %edi
	movw	%di, 2(%rbp)
	movq	%rcx, %rdi
	orq	$1, %rdi
	cmpq	%r10, %rdi
	jae	.LBB24_17
# BB#16:
	movzwl	all_mv8x8(%rsi,%rdi,4), %ebp
	movq	(%rax,%rdi,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	32(%rax), %rax
	movw	%bp, (%rax)
	movzwl	all_mv8x8+2(%rsi,%rdi,4), %ebp
	movw	%bp, 2(%rax)
	movzwl	pred_mv8x8(%rsi,%rdi,4), %eax
	movq	(%rdx,%rdi,8), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r13,8), %rdx
	movq	32(%rdx), %rdx
	movw	%ax, (%rdx)
	movzwl	pred_mv8x8+2(%rsi,%rdi,4), %eax
	movw	%ax, 2(%rdx)
.LBB24_17:
	orq	$1, %rbx
	cmpq	%r11, %rbx
	jge	.LBB24_32
# BB#18:                                # %.preheader.1
	movq	(%r9,%rbx,8), %rax
	movq	(%r8,%rbx,8), %rdx
	shlq	$4, %rbx
	addq	%rbx, %r14
	movzwl	all_mv8x8(%r14,%rcx,4), %esi
	movq	(%rax,%rcx,8), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	32(%rbp), %rbp
	movw	%si, (%rbp)
	movzwl	all_mv8x8+2(%r14,%rcx,4), %esi
	movw	%si, 2(%rbp)
	movzwl	pred_mv8x8(%r14,%rcx,4), %esi
	movq	(%rdx,%rcx,8), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	32(%rbp), %rbp
	movw	%si, (%rbp)
	movzwl	pred_mv8x8+2(%r14,%rcx,4), %ecx
	movw	%cx, 2(%rbp)
	cmpq	%r10, %rdi
	jae	.LBB24_32
# BB#19:
	movzwl	all_mv8x8(%r14,%rdi,4), %ecx
	movq	(%rax,%rdi,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	32(%rax), %rax
	movw	%cx, (%rax)
	movzwl	all_mv8x8+2(%r14,%rdi,4), %ecx
	movw	%cx, 2(%rax)
	movzwl	pred_mv8x8(%r14,%rdi,4), %eax
	movq	(%rdx,%rdi,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	32(%rcx), %rcx
	jmp	.LBB24_25
.LBB24_20:                              # %.preheader250
	movl	%ecx, %ecx
	movl	%esi, %r10d
	movslq	%ebx, %rbx
	movslq	%ebp, %r15
	movq	(%r9,%rbx,8), %rax
	movq	(%r8,%rbx,8), %rdx
	shlq	$7, %r14
	movq	%rbx, %rsi
	shlq	$4, %rsi
	addq	%r14, %rsi
	movzwl	all_mv8x8(%rsi,%rcx,4), %edi
	movq	(%rax,%rcx,8), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	(%rbp,%r11,8), %rbp
	movw	%di, (%rbp)
	movzwl	all_mv8x8+2(%rsi,%rcx,4), %edi
	movw	%di, 2(%rbp)
	movzwl	pred_mv8x8(%rsi,%rcx,4), %edi
	movq	(%rdx,%rcx,8), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	(%rbp,%r11,8), %rbp
	movw	%di, (%rbp)
	movzwl	pred_mv8x8+2(%rsi,%rcx,4), %edi
	movw	%di, 2(%rbp)
	movq	%rcx, %rdi
	orq	$1, %rdi
	cmpq	%r10, %rdi
	jae	.LBB24_22
# BB#21:
	movzwl	all_mv8x8(%rsi,%rdi,4), %ebp
	movq	(%rax,%rdi,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%r11,8), %rax
	movw	%bp, (%rax)
	movzwl	all_mv8x8+2(%rsi,%rdi,4), %ebp
	movw	%bp, 2(%rax)
	movzwl	pred_mv8x8(%rsi,%rdi,4), %eax
	movq	(%rdx,%rdi,8), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r13,8), %rdx
	movq	(%rdx,%r11,8), %rdx
	movw	%ax, (%rdx)
	movzwl	pred_mv8x8+2(%rsi,%rdi,4), %eax
	movw	%ax, 2(%rdx)
.LBB24_22:
	orq	$1, %rbx
	cmpq	%r15, %rbx
	jge	.LBB24_32
# BB#23:                                # %.preheader249.1
	movq	(%r9,%rbx,8), %rax
	movq	(%r8,%rbx,8), %rdx
	shlq	$4, %rbx
	addq	%rbx, %r14
	movzwl	all_mv8x8(%r14,%rcx,4), %esi
	movq	(%rax,%rcx,8), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	(%rbp,%r11,8), %rbp
	movw	%si, (%rbp)
	movzwl	all_mv8x8+2(%r14,%rcx,4), %esi
	movw	%si, 2(%rbp)
	movzwl	pred_mv8x8(%r14,%rcx,4), %esi
	movq	(%rdx,%rcx,8), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%r13,8), %rbp
	movq	(%rbp,%r11,8), %rbp
	movw	%si, (%rbp)
	movzwl	pred_mv8x8+2(%r14,%rcx,4), %ecx
	movw	%cx, 2(%rbp)
	cmpq	%r10, %rdi
	jae	.LBB24_32
# BB#24:
	movzwl	all_mv8x8(%r14,%rdi,4), %ecx
	movq	(%rax,%rdi,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%r11,8), %rax
	movw	%cx, (%rax)
	movzwl	all_mv8x8+2(%r14,%rdi,4), %ecx
	movw	%cx, 2(%rax)
	movzwl	pred_mv8x8(%r14,%rdi,4), %eax
	movq	(%rdx,%rdi,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%r11,8), %rcx
.LBB24_25:                              # %.loopexit
	movw	%ax, (%rcx)
	movzwl	pred_mv8x8+2(%r14,%rdi,4), %eax
	jmp	.LBB24_31
.LBB24_26:                              # %.preheader253
	movl	%ecx, %ecx
	movl	%esi, %r15d
	movslq	%ebx, %rdx
	movslq	%ebp, %r12
	movq	(%r9,%rdx,8), %rax
	movq	(%r8,%rdx,8), %rbp
	shlq	$7, %r14
	movq	%rdx, %rdi
	shlq	$4, %rdi
	addq	%r14, %rdi
	movzwl	all_mv8x8+64(%rdi,%rcx,4), %esi
	movq	(%rax,%rcx,8), %rbx
	movq	8(%rbx), %rbx
	movq	(%rbx,%r10,8), %rbx
	movq	(%rbx,%r11,8), %rbx
	movw	%si, (%rbx)
	movzwl	all_mv8x8+66(%rdi,%rcx,4), %esi
	movw	%si, 2(%rbx)
	movzwl	pred_mv8x8+64(%rdi,%rcx,4), %esi
	movq	(%rbp,%rcx,8), %rbx
	movq	8(%rbx), %rbx
	movq	(%rbx,%r10,8), %rbx
	movq	(%rbx,%r11,8), %rbx
	movw	%si, (%rbx)
	movzwl	pred_mv8x8+66(%rdi,%rcx,4), %esi
	movw	%si, 2(%rbx)
	movq	%rcx, %rsi
	orq	$1, %rsi
	cmpq	%r15, %rsi
	jae	.LBB24_28
# BB#27:
	movzwl	all_mv8x8+64(%rdi,%rsi,4), %ebx
	movq	(%rax,%rsi,8), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%r11,8), %rax
	movw	%bx, (%rax)
	movzwl	all_mv8x8+66(%rdi,%rsi,4), %ebx
	movw	%bx, 2(%rax)
	movzwl	pred_mv8x8+64(%rdi,%rsi,4), %eax
	movq	(%rbp,%rsi,8), %rbx
	movq	8(%rbx), %rbx
	movq	(%rbx,%r10,8), %rbx
	movq	(%rbx,%r11,8), %rbx
	movw	%ax, (%rbx)
	movzwl	pred_mv8x8+66(%rdi,%rsi,4), %eax
	movw	%ax, 2(%rbx)
.LBB24_28:
	orq	$1, %rdx
	cmpq	%r12, %rdx
	jge	.LBB24_32
# BB#29:                                # %.preheader252.1
	movq	(%r9,%rdx,8), %rax
	movq	(%r8,%rdx,8), %rdi
	shlq	$4, %rdx
	addq	%rdx, %r14
	movzwl	all_mv8x8+64(%r14,%rcx,4), %edx
	movq	(%rax,%rcx,8), %rbp
	movq	8(%rbp), %rbp
	movq	(%rbp,%r10,8), %rbp
	movq	(%rbp,%r11,8), %rbp
	movw	%dx, (%rbp)
	movzwl	all_mv8x8+66(%r14,%rcx,4), %edx
	movw	%dx, 2(%rbp)
	movzwl	pred_mv8x8+64(%r14,%rcx,4), %edx
	movq	(%rdi,%rcx,8), %rbp
	movq	8(%rbp), %rbp
	movq	(%rbp,%r10,8), %rbp
	movq	(%rbp,%r11,8), %rbp
	movw	%dx, (%rbp)
	movzwl	pred_mv8x8+66(%r14,%rcx,4), %ecx
	movw	%cx, 2(%rbp)
	cmpq	%r15, %rsi
	jae	.LBB24_32
# BB#30:
	movzwl	all_mv8x8+64(%r14,%rsi,4), %ecx
	movq	(%rax,%rsi,8), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%r11,8), %rax
	movw	%cx, (%rax)
	movzwl	all_mv8x8+66(%r14,%rsi,4), %ecx
	movw	%cx, 2(%rax)
	movzwl	pred_mv8x8+64(%r14,%rsi,4), %eax
	movq	(%rdi,%rsi,8), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%r11,8), %rcx
	movw	%ax, (%rcx)
	movzwl	pred_mv8x8+66(%r14,%rsi,4), %eax
.LBB24_31:                              # %.loopexit
	movw	%ax, 2(%rcx)
.LBB24_32:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_33:
	movl	$.L.str.1, %edi
	movl	$255, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.Lfunc_end24:
	.size	RestoreMVBlock8x8, .Lfunc_end24-RestoreMVBlock8x8
	.cfi_endproc

	.globl	StoreNewMotionVectorsBlock8x8
	.p2align	4, 0x90
	.type	StoreNewMotionVectorsBlock8x8,@function
StoreNewMotionVectorsBlock8x8:          # @StoreNewMotionVectorsBlock8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi239:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi240:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi241:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi242:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi243:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi244:
	.cfi_def_cfa_offset 56
.Lcfi245:
	.cfi_offset %rbx, -56
.Lcfi246:
	.cfi_offset %r12, -48
.Lcfi247:
	.cfi_offset %r13, -40
.Lcfi248:
	.cfi_offset %r14, -32
.Lcfi249:
	.cfi_offset %r15, -24
.Lcfi250:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movslq	%edi, %r11
	leal	(%rsi,%rsi), %ebx
	andl	$2, %ebx
	andl	$-2, %esi
	leal	2(%rsi), %r12d
	testl	%r9d, %r9d
	js	.LBB25_8
# BB#1:
	movl	56(%rsp), %eax
	movq	img(%rip), %rbp
	movq	14376(%rbp), %r13
	movq	14384(%rbp), %r15
	leal	2(%rbx), %edi
	testl	%eax, %eax
	je	.LBB25_10
# BB#2:
	movl	%r9d, %eax
	orl	$2, %eax
	cmpl	$2, %eax
	movq	%r13, -32(%rsp)         # 8-byte Spill
	movq	%r15, -40(%rsp)         # 8-byte Spill
	movl	%r8d, -44(%rsp)         # 4-byte Spill
	movl	%edx, -48(%rsp)         # 4-byte Spill
	movl	%edi, -52(%rsp)         # 4-byte Spill
	jne	.LBB25_16
# BB#3:                                 # %.preheader193
	movslq	%ecx, %rax
	movslq	%edx, %rcx
	movl	%ebx, %ebp
	movl	%edi, %r8d
	movq	%rax, %rdi
	movslq	%esi, %r14
	movq	(%r15,%r14,8), %rax
	movq	%rcx, %r15
	movq	(%r13,%r14,8), %rdx
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movq	(%rax,%rbp,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx,%r15,8), %rax
	movzwl	(%rax), %r13d
	movq	%r11, %rcx
	shlq	$7, %rcx
	movq	%r14, %r10
	shlq	$4, %r10
	addq	%rcx, %r10
	movw	%r13w, all_mv8x8(%r10,%rbp,4)
	movzwl	2(%rax), %eax
	movw	%ax, all_mv8x8+2(%r10,%rbp,4)
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	movq	(%rdx,%rbp,8), %rax
	movq	(%rax), %rax
	movq	%rdi, -64(%rsp)         # 8-byte Spill
	movq	(%rax,%rdi,8), %rax
	movq	(%rax,%r15,8), %rax
	movzwl	(%rax), %edx
	movw	%dx, pred_mv8x8(%r10,%rbp,4)
	movslq	%r12d, %rdx
	movzwl	2(%rax), %eax
	movw	%ax, pred_mv8x8+2(%r10,%rbp,4)
	movq	%rbp, %r13
	orq	$1, %r13
	movq	%r8, -8(%rsp)           # 8-byte Spill
	cmpq	%r8, %r13
	jae	.LBB25_5
# BB#4:
	movq	-24(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r13,8), %rax
	movq	(%rax), %rax
	movq	-64(%rsp), %rdi         # 8-byte Reload
	movq	(%rax,%rdi,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	%rdx, %r8
	movzwl	(%rax), %edx
	movw	%dx, all_mv8x8(%r10,%r13,4)
	movzwl	2(%rax), %eax
	movw	%ax, all_mv8x8+2(%r10,%r13,4)
	movq	-16(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r13,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%rdi,8), %rax
	movq	(%rax,%r15,8), %rax
	movzwl	(%rax), %edx
	movw	%dx, pred_mv8x8(%r10,%r13,4)
	movq	%r8, %rdx
	movzwl	2(%rax), %eax
	movw	%ax, pred_mv8x8+2(%r10,%r13,4)
.LBB25_5:
	orq	$1, %r14
	cmpq	%rdx, %r14
	jge	.LBB25_18
# BB#6:                                 # %.preheader192.1
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r14,8), %r8
	movq	-32(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r14,8), %r10
	movq	(%r8,%rbp,8), %rax
	movq	(%rax), %rax
	movq	-64(%rsp), %rdi         # 8-byte Reload
	movq	(%rax,%rdi,8), %rax
	movq	(%rax,%r15,8), %rax
	movzwl	(%rax), %edx
	shlq	$4, %r14
	addq	%r14, %rcx
	movw	%dx, all_mv8x8(%rcx,%rbp,4)
	movzwl	2(%rax), %eax
	movw	%ax, all_mv8x8+2(%rcx,%rbp,4)
	movq	(%r10,%rbp,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%rdi,8), %rax
	movq	(%rax,%r15,8), %rax
	movzwl	(%rax), %edx
	movw	%dx, pred_mv8x8(%rcx,%rbp,4)
	movzwl	2(%rax), %eax
	movw	%ax, pred_mv8x8+2(%rcx,%rbp,4)
	cmpq	-8(%rsp), %r13          # 8-byte Folded Reload
	jae	.LBB25_18
# BB#7:
	movq	(%r8,%r13,8), %rax
	movq	(%rax), %rax
	movq	-64(%rsp), %rdi         # 8-byte Reload
	movq	(%rax,%rdi,8), %rax
	movq	(%rax,%r15,8), %rax
	movzwl	(%rax), %edx
	movw	%dx, all_mv8x8(%rcx,%r13,4)
	movzwl	2(%rax), %eax
	movw	%ax, all_mv8x8+2(%rcx,%r13,4)
	movq	(%r10,%r13,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%rdi,8), %rax
	movq	(%rax,%r15,8), %rax
	movzwl	(%rax), %edx
	movw	%dx, pred_mv8x8(%rcx,%r13,4)
	movzwl	2(%rax), %eax
	movw	%ax, pred_mv8x8+2(%rcx,%r13,4)
	jmp	.LBB25_18
.LBB25_8:                               # %.preheader
	movl	%ebx, %ecx
	movslq	%esi, %rdx
	movslq	%r12d, %rax
	shlq	$7, %r11
	movq	%rdx, %rsi
	shlq	$4, %rsi
	addq	%r11, %rsi
	movq	$0, all_mv8x8(%rsi,%rcx,4)
	movq	$0, all_mv8x8+64(%rsi,%rcx,4)
	orq	$1, %rdx
	cmpq	%rax, %rdx
	jge	.LBB25_27
# BB#9:
	shlq	$4, %rdx
	addq	%rdx, %r11
	movq	$0, all_mv8x8(%r11,%rcx,4)
	jmp	.LBB25_26
.LBB25_10:                              # %.preheader185
	movslq	%ecx, %r14
	movl	%ebx, %ecx
	movl	%edi, %r8d
	movslq	%esi, %rbp
	movslq	%r12d, %r9
	movq	(%r15,%rbp,8), %rdi
	movq	(%r13,%rbp,8), %r10
	movq	(%rdi,%rcx,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	32(%rax), %rax
	movzwl	(%rax), %esi
	shlq	$7, %r11
	movq	%rbp, %rbx
	shlq	$4, %rbx
	addq	%r11, %rbx
	movw	%si, all_mv8x8(%rbx,%rcx,4)
	movzwl	2(%rax), %eax
	movw	%ax, all_mv8x8+2(%rbx,%rcx,4)
	movq	(%r10,%rcx,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	32(%rax), %rax
	movzwl	(%rax), %esi
	movw	%si, pred_mv8x8(%rbx,%rcx,4)
	movzwl	2(%rax), %eax
	movw	%ax, pred_mv8x8+2(%rbx,%rcx,4)
	movq	%rcx, %rsi
	orq	$1, %rsi
	cmpq	%r8, %rsi
	jae	.LBB25_12
# BB#11:
	movq	(%rdi,%rsi,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	32(%rax), %rax
	movzwl	(%rax), %edi
	movw	%di, all_mv8x8(%rbx,%rsi,4)
	movzwl	2(%rax), %eax
	movw	%ax, all_mv8x8+2(%rbx,%rsi,4)
	movq	(%r10,%rsi,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	32(%rax), %rax
	movzwl	(%rax), %edi
	movw	%di, pred_mv8x8(%rbx,%rsi,4)
	movzwl	2(%rax), %eax
	movw	%ax, pred_mv8x8+2(%rbx,%rsi,4)
.LBB25_12:
	movq	$0, all_mv8x8+64(%rbx,%rcx,4)
	orq	$1, %rbp
	cmpq	%r9, %rbp
	jge	.LBB25_27
# BB#13:                                # %.preheader184.1
	movq	(%r15,%rbp,8), %rbx
	movq	(%r13,%rbp,8), %r9
	movq	(%rbx,%rcx,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	32(%rax), %rax
	movzwl	(%rax), %edi
	shlq	$4, %rbp
	addq	%rbp, %r11
	movw	%di, all_mv8x8(%r11,%rcx,4)
	movzwl	2(%rax), %eax
	movw	%ax, all_mv8x8+2(%r11,%rcx,4)
	movq	(%r9,%rcx,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	32(%rax), %rax
	movzwl	(%rax), %edi
	movw	%di, pred_mv8x8(%r11,%rcx,4)
	movzwl	2(%rax), %eax
	movw	%ax, pred_mv8x8+2(%r11,%rcx,4)
	cmpq	%r8, %rsi
	jae	.LBB25_26
# BB#14:
	movq	(%rbx,%rsi,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	32(%rax), %rax
	movzwl	(%rax), %edi
	movw	%di, all_mv8x8(%r11,%rsi,4)
	movzwl	2(%rax), %eax
	movw	%ax, all_mv8x8+2(%r11,%rsi,4)
	movq	(%r9,%rsi,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	32(%rax), %rax
	movzwl	(%rax), %edx
	movw	%dx, pred_mv8x8(%r11,%rsi,4)
	movzwl	2(%rax), %eax
	movw	%ax, pred_mv8x8+2(%r11,%rsi,4)
	jmp	.LBB25_26
.LBB25_16:                              # %.preheader195
	movl	%ebx, %r8d
	movslq	%esi, %rcx
	movslq	%r12d, %r10
	movq	%r11, %rax
	shlq	$7, %rax
	movq	%rcx, %rbp
	shlq	$4, %rbp
	addq	%rax, %rbp
	movq	$0, all_mv8x8(%rbp,%r8,4)
	orq	$1, %rcx
	cmpq	%r10, %rcx
	jge	.LBB25_18
# BB#17:
	shlq	$4, %rcx
	addq	%rcx, %rax
	movq	$0, all_mv8x8(%rax,%r8,4)
.LBB25_18:                              # %.loopexit194
	decl	%r9d
	cmpl	$2, %r9d
	jae	.LBB25_24
# BB#19:                                # %.preheader188
	movslq	-44(%rsp), %r10         # 4-byte Folded Reload
	movslq	-48(%rsp), %rdx         # 4-byte Folded Reload
	movl	%ebx, %ebp
	movl	-52(%rsp), %r8d         # 4-byte Reload
	movslq	%esi, %rcx
	movslq	%r12d, %r9
	movq	-40(%rsp), %r12         # 8-byte Reload
	movq	(%r12,%rcx,8), %rdi
	movq	-32(%rsp), %r15         # 8-byte Reload
	movq	(%r15,%rcx,8), %r14
	movq	(%rdi,%rbp,8), %rsi
	movq	8(%rsi), %rsi
	movq	(%rsi,%r10,8), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movzwl	(%rsi), %eax
	shlq	$7, %r11
	movq	%rcx, %rbx
	shlq	$4, %rbx
	addq	%r11, %rbx
	movw	%ax, all_mv8x8+64(%rbx,%rbp,4)
	movzwl	2(%rsi), %eax
	movw	%ax, all_mv8x8+66(%rbx,%rbp,4)
	movq	(%r14,%rbp,8), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%rdx,8), %rax
	movzwl	(%rax), %esi
	movw	%si, pred_mv8x8+64(%rbx,%rbp,4)
	movzwl	2(%rax), %eax
	movw	%ax, pred_mv8x8+66(%rbx,%rbp,4)
	movq	%rbp, %rsi
	orq	$1, %rsi
	cmpq	%r8, %rsi
	jae	.LBB25_21
# BB#20:
	movq	(%rdi,%rsi,8), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%rdx,8), %rax
	movzwl	(%rax), %edi
	movw	%di, all_mv8x8+64(%rbx,%rsi,4)
	movzwl	2(%rax), %eax
	movw	%ax, all_mv8x8+66(%rbx,%rsi,4)
	movq	(%r14,%rsi,8), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%rdx,8), %rax
	movzwl	(%rax), %edi
	movw	%di, pred_mv8x8+64(%rbx,%rsi,4)
	movzwl	2(%rax), %eax
	movw	%ax, pred_mv8x8+66(%rbx,%rsi,4)
.LBB25_21:
	orq	$1, %rcx
	cmpq	%r9, %rcx
	jge	.LBB25_27
# BB#22:                                # %.preheader187.1
	movq	(%r12,%rcx,8), %rbx
	movq	(%r15,%rcx,8), %r9
	movq	(%rbx,%rbp,8), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%rdx,8), %rax
	movzwl	(%rax), %edi
	shlq	$4, %rcx
	addq	%rcx, %r11
	movw	%di, all_mv8x8+64(%r11,%rbp,4)
	movzwl	2(%rax), %eax
	movw	%ax, all_mv8x8+66(%r11,%rbp,4)
	movq	(%r9,%rbp,8), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%rdx,8), %rax
	movzwl	(%rax), %ecx
	movw	%cx, pred_mv8x8+64(%r11,%rbp,4)
	movzwl	2(%rax), %eax
	movw	%ax, pred_mv8x8+66(%r11,%rbp,4)
	cmpq	%r8, %rsi
	jae	.LBB25_27
# BB#23:
	movq	(%rbx,%rsi,8), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%rdx,8), %rax
	movzwl	(%rax), %ecx
	movw	%cx, all_mv8x8+64(%r11,%rsi,4)
	movzwl	2(%rax), %eax
	movw	%ax, all_mv8x8+66(%r11,%rsi,4)
	movq	(%r9,%rsi,8), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%rdx,8), %rax
	movzwl	(%rax), %ecx
	movw	%cx, pred_mv8x8+64(%r11,%rsi,4)
	movzwl	2(%rax), %eax
	movw	%ax, pred_mv8x8+66(%r11,%rsi,4)
	jmp	.LBB25_27
.LBB25_24:                              # %.preheader190
	movl	%ebx, %ecx
	movslq	%esi, %rdx
	movslq	%r12d, %rax
	shlq	$7, %r11
	movq	%rdx, %rsi
	shlq	$4, %rsi
	addq	%r11, %rsi
	movq	$0, all_mv8x8+64(%rsi,%rcx,4)
	orq	$1, %rdx
	cmpq	%rax, %rdx
	jge	.LBB25_27
# BB#25:
	shlq	$4, %rdx
	addq	%rdx, %r11
.LBB25_26:                              # %.loopexit
	movq	$0, all_mv8x8+64(%r11,%rcx,4)
.LBB25_27:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	StoreNewMotionVectorsBlock8x8, .Lfunc_end25-StoreNewMotionVectorsBlock8x8
	.cfi_endproc

	.globl	GetBestTransformP8x8
	.p2align	4, 0x90
	.type	GetBestTransformP8x8,@function
GetBestTransformP8x8:                   # @GetBestTransformP8x8
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rcx
	movl	$1, %eax
	cmpl	$2, 5100(%rcx)
	je	.LBB26_9
# BB#1:                                 # %.preheader78.preheader
	pushq	%rbp
.Lcfi251:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi252:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi253:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi254:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi255:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi256:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi257:
	.cfi_def_cfa_offset 128
.Lcfi258:
	.cfi_offset %rbx, -56
.Lcfi259:
	.cfi_offset %r12, -48
.Lcfi260:
	.cfi_offset %r13, -40
.Lcfi261:
	.cfi_offset %r14, -32
.Lcfi262:
	.cfi_offset %r15, -24
.Lcfi263:
	.cfi_offset %rbp, -16
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB26_2:                               # %.preheader78
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_3 Depth 2
                                        #       Child Loop BB26_4 Depth 3
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%eax, %ecx
	andl	$1, %ecx
	movq	%rcx, %rbp
	shlq	$4, %rbp
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leal	(,%rax,4), %edx
	andl	$-8, %edx
	movq	%rdx, %r13
	shlq	$5, %r13
	addq	$6680, %r13             # imm = 0x1A18
	leal	4(,%rcx,8), %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	shlq	$3, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	%edx, %eax
	orl	$4, %eax
	cltq
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB26_3:                               #   Parent Loop BB26_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB26_4 Depth 3
	movq	img(%rip), %rax
	movslq	196(%rax), %rcx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movslq	%edx, %r15
	addq	%rcx, %r15
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	32(%rsp), %r14          # 8-byte Reload
	jmp	.LBB26_4
	.p2align	4, 0x90
.LBB26_5:                               # %._crit_edge
                                        #   in Loop: Header=BB26_4 Depth=3
	addq	$4, %r14
	movq	img(%rip), %rax
	addq	$8, %r13
	addq	$64, %r12
.LBB26_4:                               # %.preheader
                                        #   Parent Loop BB26_2 Depth=1
                                        #     Parent Loop BB26_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movl	192(%rax), %ecx
	addl	%r14d, %ecx
	movq	imgY_org(%rip), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%r15,8), %rdx
	movq	(%rdx,%rcx,2), %xmm0    # xmm0 = mem[0],zero
	pxor	%xmm3, %xmm3
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	movq	tr4x4(%rbp,%r13), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	movdqa	%xmm0, %xmm2
	psubd	%xmm1, %xmm2
	leaq	diff4x4(%r12), %rdi
	movdqa	%xmm2, diff4x4(%r12)
	movq	tr8x8(%rbp,%r13), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff8x8(%r12)
	movq	8(%rax,%r15,8), %rdx
	movq	(%rdx,%rcx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	movq	tr4x4+32(%rbp,%r13), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	movdqa	%xmm0, %xmm2
	psubd	%xmm1, %xmm2
	movdqa	%xmm2, diff4x4+16(%r12)
	movq	tr8x8+32(%rbp,%r13), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff8x8+16(%r12)
	movq	16(%rax,%r15,8), %rdx
	movq	(%rdx,%rcx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	movq	tr4x4+64(%rbp,%r13), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	movdqa	%xmm0, %xmm2
	psubd	%xmm1, %xmm2
	movdqa	%xmm2, diff4x4+32(%r12)
	movq	tr8x8+64(%rbp,%r13), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff8x8+32(%r12)
	movq	24(%rax,%r15,8), %rax
	movq	(%rax,%rcx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	movq	tr4x4+96(%rbp,%r13), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	movdqa	%xmm0, %xmm2
	psubd	%xmm1, %xmm2
	movdqa	%xmm2, diff4x4+48(%r12)
	movq	tr8x8+96(%rbp,%r13), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff8x8+48(%r12)
	callq	distortion4x4
	movl	%eax, %ebx
	addl	12(%rsp), %ebx          # 4-byte Folded Reload
	cmpq	64(%rsp), %r14          # 8-byte Folded Reload
	jl	.LBB26_5
# BB#6:                                 #   in Loop: Header=BB26_3 Depth=2
	movq	48(%rsp), %r13          # 8-byte Reload
	subq	$-128, %r13
	movq	40(%rsp), %r12          # 8-byte Reload
	subq	$-128, %r12
	movq	56(%rsp), %rdx          # 8-byte Reload
	cmpq	24(%rsp), %rdx          # 8-byte Folded Reload
	leaq	4(%rdx), %rdx
	jl	.LBB26_3
# BB#7:                                 #   in Loop: Header=BB26_2 Depth=1
	movl	$diff8x8, %edi
	callq	distortion8x8
	movl	8(%rsp), %ecx           # 4-byte Reload
	addl	%eax, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	incl	%eax
	cmpl	$4, %eax
	jne	.LBB26_2
# BB#8:
	xorl	%eax, %eax
	cmpl	%ebx, %ecx
	setl	%al
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
.LBB26_9:
	retq
.Lfunc_end26:
	.size	GetBestTransformP8x8, .Lfunc_end26-GetBestTransformP8x8
	.cfi_endproc

	.globl	set_mbaff_parameters
	.p2align	4, 0x90
	.type	set_mbaff_parameters,@function
set_mbaff_parameters:                   # @set_mbaff_parameters
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi264:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi265:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi266:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi267:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi268:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi269:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi270:
	.cfi_def_cfa_offset 80
.Lcfi271:
	.cfi_offset %rbx, -56
.Lcfi272:
	.cfi_offset %r12, -48
.Lcfi273:
	.cfi_offset %r13, -40
.Lcfi274:
	.cfi_offset %r14, -32
.Lcfi275:
	.cfi_offset %r15, -24
.Lcfi276:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rbp
	movq	14224(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movslq	12(%rbp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movswl	best_mode(%rip), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	20(%rbp), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	128(%rbp), %r14
	movq	rdopt(%rip), %r13
	movq	enc_picture(%rip), %r15
	leaq	40(%r13), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB27_1:                               # =>This Inner Loop Header: Depth=1
	movq	6440(%r15), %rdx
	movl	180(%rbp), %esi
	addl	%ecx, %esi
	movslq	%esi, %rsi
	movq	(%rdx,%rsi,8), %rdx
	movslq	176(%rbp), %rsi
	movups	16(%rdx,%rsi,2), %xmm0
	movups	%xmm0, -16(%rax)
	movups	(%rdx,%rsi,2), %xmm0
	movups	%xmm0, -32(%rax)
	movq	6440(%r15), %rdx
	movl	180(%rbp), %esi
	leal	1(%rcx,%rsi), %esi
	movslq	%esi, %rsi
	movq	(%rdx,%rsi,8), %rdx
	movslq	176(%rbp), %rsi
	movups	16(%rdx,%rsi,2), %xmm0
	movups	%xmm0, 16(%rax)
	movups	(%rdx,%rsi,2), %xmm0
	movups	%xmm0, (%rax)
	addq	$2, %rcx
	addq	$64, %rax
	cmpq	$16, %rcx
	jne	.LBB27_1
# BB#2:
	cmpl	$0, 15536(%rbp)
	je	.LBB27_6
# BB#3:                                 # %.preheader80
	cmpl	$0, 15548(%rbp)
	jle	.LBB27_6
# BB#4:                                 # %.lr.ph91.preheader
	leaq	520(%r13), %rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB27_5:                               # %.lr.ph91
                                        # =>This Inner Loop Header: Depth=1
	movq	6472(%r15), %rax
	movq	(%rax), %rax
	movl	188(%rbp), %ecx
	addl	%r12d, %ecx
	movslq	%ecx, %rcx
	movslq	184(%rbp), %rsi
	addq	%rsi, %rsi
	addq	(%rax,%rcx,8), %rsi
	movslq	15544(%rbp), %rdx
	addq	%rdx, %rdx
	movq	%rbx, %rdi
	callq	memcpy
	leaq	512(%rbx), %rdi
	movq	6472(%r15), %rax
	movq	8(%rax), %rax
	movl	188(%rbp), %ecx
	addl	%r12d, %ecx
	movslq	%ecx, %rcx
	movslq	184(%rbp), %rsi
	addq	%rsi, %rsi
	addq	(%rax,%rcx,8), %rsi
	movslq	15544(%rbp), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%r12
	movslq	15548(%rbp), %rax
	addq	$32, %rbx
	cmpq	%rax, %r12
	jl	.LBB27_5
.LBB27_6:                               # %.loopexit81
	movl	(%rsp), %edx            # 4-byte Reload
	movl	%edx, 1656(%r13)
	movl	15244(%rbp), %eax
	movl	%eax, 1712(%r13)
	imulq	$536, 8(%rsp), %r15     # 8-byte Folded Reload
                                        # imm = 0x218
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	364(%rbx,%r15), %eax
	movl	%eax, 1640(%r13)
	movq	368(%rbx,%r15), %rax
	movq	%rax, 1648(%r13)
	movl	72(%rbx,%r15), %eax
	movl	%eax, 1560(%r13)
	movl	472(%rbx,%r15), %ecx
	movl	%ecx, 1720(%r13)
	testw	%dx, %dx
	je	.LBB27_9
# BB#7:                                 # %.loopexit81
	testl	%eax, %eax
	jne	.LBB27_9
# BB#8:
	movl	$0, 1656(%r13)
.LBB27_9:                               # %.preheader79
	cmpl	$-3, 15528(%rbp)
	jl	.LBB27_14
# BB#10:                                # %.preheader78.preheader
	xorl	%r12d, %r12d
	jmp	.LBB27_11
	.p2align	4, 0x90
.LBB27_12:                              # %.preheader78..preheader78_crit_edge
                                        #   in Loop: Header=BB27_11 Depth=1
	incq	%r12
	movq	rdopt(%rip), %r13
.LBB27_11:                              # %.preheader78
                                        # =>This Inner Loop Header: Depth=1
	movq	1544(%r13), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	14160(%rbp), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rbp
	movslq	15528(%rbp), %rax
	addq	$3, %rax
	cmpq	%rax, %r12
	jl	.LBB27_12
# BB#13:                                # %.preheader75.preheader.loopexit
	movq	rdopt(%rip), %r13
.LBB27_14:                              # %.preheader75.preheader
	cmpl	$1, 4(%rsp)             # 4-byte Folded Reload
	movq	1552(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	14168(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	rdopt(%rip), %rax
	movq	1552(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movq	14168(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	rdopt(%rip), %rax
	movq	1552(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movq	img(%rip), %rcx
	movq	14168(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	rdopt(%rip), %rax
	movq	1552(%rax), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movq	14168(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	rdopt(%rip), %rax
	movq	1552(%rax), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	img(%rip), %rcx
	movq	14168(%rcx), %rcx
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	rdopt(%rip), %rax
	movq	1552(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movq	14168(%rcx), %rcx
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	rdopt(%rip), %rax
	movups	376(%rbx,%r15), %xmm0
	movups	%xmm0, 1568(%rax)
	movups	392(%rbx,%r15), %xmm0
	movups	%xmm0, 1584(%rax)
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %rcx
	movq	(%rcx), %rsi
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdi
	movq	(%rsi,%rdi,8), %rsi
	movslq	168(%rcx), %rdi
	movl	(%rsi,%rdi), %esi
	movl	%esi, 1680(%rax)
	jne	.LBB27_15
# BB#16:                                # %.preheader.preheader
	movq	6488(%rdx), %rsi
	movq	8(%rsi), %rsi
	movslq	172(%rcx), %rdi
	movq	(%rsi,%rdi,8), %rsi
	movslq	168(%rcx), %rdi
	movl	(%rsi,%rdi), %esi
	movl	%esi, 1696(%rax)
	movq	6488(%rdx), %rsi
	movq	(%rsi), %rsi
	movslq	172(%rcx), %rdi
	movq	8(%rsi,%rdi,8), %rsi
	movslq	168(%rcx), %rdi
	movl	(%rsi,%rdi), %esi
	movl	%esi, 1684(%rax)
	movq	6488(%rdx), %rsi
	movq	8(%rsi), %rsi
	movslq	172(%rcx), %rdi
	movq	8(%rsi,%rdi,8), %rsi
	movslq	168(%rcx), %rdi
	movl	(%rsi,%rdi), %esi
	movl	%esi, 1700(%rax)
	movq	6488(%rdx), %rsi
	movq	(%rsi), %rsi
	movslq	172(%rcx), %rdi
	movq	16(%rsi,%rdi,8), %rsi
	movslq	168(%rcx), %rdi
	movl	(%rsi,%rdi), %esi
	movl	%esi, 1688(%rax)
	movq	6488(%rdx), %rsi
	movq	8(%rsi), %rsi
	movslq	172(%rcx), %rdi
	movq	16(%rsi,%rdi,8), %rsi
	movslq	168(%rcx), %rdi
	movl	(%rsi,%rdi), %esi
	movl	%esi, 1704(%rax)
	movq	6488(%rdx), %rdx
	movq	(%rdx), %rdx
	movslq	172(%rcx), %rsi
	movq	24(%rdx,%rsi,8), %rdx
	movslq	168(%rcx), %rcx
	movl	(%rdx,%rcx), %ecx
	movl	%ecx, 1692(%rax)
	movq	rdopt(%rip), %rax
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	8(%rcx), %rdx
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rsi
	movq	24(%rdx,%rsi,8), %rdx
	movslq	168(%rcx), %rsi
	movl	(%rdx,%rsi), %edx
	movl	%edx, 1708(%rax)
	movzwl	480(%rbx,%r15), %edx
	movw	%dx, 1564(%rax)
	jmp	.LBB27_17
.LBB27_15:                              # %.preheader74.preheader
	movq	6488(%rdx), %rsi
	movq	(%rsi), %rsi
	movslq	172(%rcx), %rdi
	movq	8(%rsi,%rdi,8), %rsi
	movslq	168(%rcx), %rdi
	movl	(%rsi,%rdi), %esi
	movl	%esi, 1684(%rax)
	movq	6488(%rdx), %rsi
	movq	(%rsi), %rsi
	movslq	172(%rcx), %rdi
	movq	16(%rsi,%rdi,8), %rsi
	movslq	168(%rcx), %rdi
	movl	(%rsi,%rdi), %esi
	movl	%esi, 1688(%rax)
	movq	6488(%rdx), %rdx
	movq	(%rdx), %rdx
	movslq	172(%rcx), %rsi
	movq	24(%rdx,%rsi,8), %rdx
	movslq	168(%rcx), %rsi
	movl	(%rdx,%rsi), %edx
	movl	%edx, 1692(%rax)
.LBB27_17:                              # %.lr.ph.preheader
	movups	332(%rbx,%r15), %xmm0
	movups	%xmm0, 1608(%rax)
	movups	348(%rbx,%r15), %xmm0
	movups	%xmm0, 1624(%rax)
	movslq	172(%rcx), %rdx
	movq	1600(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	movslq	168(%rcx), %rcx
	movq	(%r14,%rdx,8), %rsi
	movl	(%rsi,%rcx), %esi
	movl	%esi, (%rax,%rcx)
	movq	img(%rip), %rax
	movl	172(%rax), %ecx
	addl	$3, %ecx
	cmpl	%ecx, %edx
	jge	.LBB27_19
	.p2align	4, 0x90
.LBB27_18:                              # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	rdopt(%rip), %rcx
	movq	1600(%rcx), %rcx
	movq	8(%rcx,%rdx,8), %rcx
	movslq	168(%rax), %rax
	movq	8(%r14,%rdx,8), %rsi
	leaq	1(%rdx), %rdx
	movl	(%rsi,%rax), %esi
	movl	%esi, (%rcx,%rax)
	movq	img(%rip), %rax
	movslq	172(%rax), %rcx
	addq	$3, %rcx
	cmpq	%rcx, %rdx
	jl	.LBB27_18
.LBB27_19:                              # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	set_mbaff_parameters, .Lfunc_end27-set_mbaff_parameters
	.cfi_endproc

	.globl	store_coding_state_cs_cm
	.p2align	4, 0x90
	.type	store_coding_state_cs_cm,@function
store_coding_state_cs_cm:               # @store_coding_state_cs_cm
	.cfi_startproc
# BB#0:
	movq	cs_cm(%rip), %rdi
	jmp	store_coding_state      # TAILCALL
.Lfunc_end28:
	.size	store_coding_state_cs_cm, .Lfunc_end28-store_coding_state_cs_cm
	.cfi_endproc

	.globl	reset_coding_state_cs_cm
	.p2align	4, 0x90
	.type	reset_coding_state_cs_cm,@function
reset_coding_state_cs_cm:               # @reset_coding_state_cs_cm
	.cfi_startproc
# BB#0:
	movq	cs_cm(%rip), %rdi
	jmp	reset_coding_state      # TAILCALL
.Lfunc_end29:
	.size	reset_coding_state_cs_cm, .Lfunc_end29-reset_coding_state_cs_cm
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI30_0:
	.zero	16
	.text
	.globl	assign_enc_picture_params
	.p2align	4, 0x90
	.type	assign_enc_picture_params,@function
assign_enc_picture_params:              # @assign_enc_picture_params
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi277:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi278:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi279:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi280:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi281:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi282:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi283:
	.cfi_def_cfa_offset 112
.Lcfi284:
	.cfi_offset %rbx, -56
.Lcfi285:
	.cfi_offset %r12, -48
.Lcfi286:
	.cfi_offset %r13, -40
.Lcfi287:
	.cfi_offset %r14, -32
.Lcfi288:
	.cfi_offset %r15, -24
.Lcfi289:
	.cfi_offset %rbp, -16
	movl	%r9d, 12(%rsp)          # 4-byte Spill
	movl	%r8d, 16(%rsp)          # 4-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r14d
	movl	%esi, %r13d
	cmpl	$1, %edi
	je	.LBB30_6
# BB#1:
	addl	%r14d, %r14d
	cmpl	$2, %edi
	jne	.LBB30_38
# BB#2:                                 # %.preheader334
	movslq	12(%rsp), %r11          # 4-byte Folded Reload
	leal	1(%rcx), %eax
	cltq
	movslq	16(%rsp), %r12          # 4-byte Folded Reload
	movslq	%ecx, %rdx
	movslq	%r14d, %r8
	xorl	%r10d, %r10d
	imulq	$264, %rax, %r9         # imm = 0x108
	imulq	$264, %rdx, %r14        # imm = 0x108
	.p2align	4, 0x90
.LBB30_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_4 Depth 2
	movq	img(%rip), %rbx
	leaq	(%r10,%r8), %r15
	movl	172(%rbx), %eax
	addl	%r15d, %eax
	movslq	%eax, %rdi
	xorl	%esi, %esi
	jmp	.LBB30_4
	.p2align	4, 0x90
.LBB30_36:                              # %._crit_edge
                                        #   in Loop: Header=BB30_4 Depth=2
	incq	%rsi
	movq	img(%rip), %rbx
.LBB30_4:                               #   Parent Loop BB30_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	168(%rbx), %eax
	addq	%rsi, %rax
	cmpb	$1, %r13b
	jne	.LBB30_29
# BB#5:                                 #   in Loop: Header=BB30_4 Depth=2
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%rdi,8), %rdx
	cltq
	movb	$-1, (%rdx,%rax)
	movq	enc_picture(%rip), %rbx
	movq	6496(%rbx), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movq	$-1, (%rdx,%rax,8)
	movq	6512(%rbx), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movq	(%rdx,%rax,8), %rbp
	movw	$0, (%rbp)
	xorl	%edx, %edx
	jmp	.LBB30_30
	.p2align	4, 0x90
.LBB30_29:                              #   in Loop: Header=BB30_4 Depth=2
	movq	14384(%rbx), %rdx
	movq	(%rdx,%r15,8), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%r12,8), %rdx
	movq	16(%rdx), %rdx
	movq	enc_picture(%rip), %rbx
	movq	6488(%rbx), %rbx
	movq	(%rbx), %rbx
	movq	(%rbx,%rdi,8), %rbx
	cltq
	movb	%r12b, (%rbx,%rax)
	movq	enc_picture(%rip), %rbx
	leaq	(%rbx,%r14), %rbp
	movq	24(%rbp,%r12,8), %rbp
	movq	6496(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	%rbp, (%rcx,%rax,8)
	movzwl	(%rdx), %ecx
	movq	6512(%rbx), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%rdi,8), %rbp
	movq	(%rbp,%rax,8), %rbp
	movw	%cx, (%rbp)
	movw	2(%rdx), %dx
.LBB30_30:                              #   in Loop: Header=BB30_4 Depth=2
	cmpl	$0, 112(%rsp)
	movw	%dx, 2(%rbp)
	je	.LBB30_35
# BB#31:                                #   in Loop: Header=BB30_4 Depth=2
	movq	6488(%rbx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rdi,8), %rdx
	testb	%r13b, %r13b
	je	.LBB30_32
# BB#33:                                #   in Loop: Header=BB30_4 Depth=2
	movl	12(%rsp), %ecx          # 4-byte Reload
	movb	%cl, (%rdx,%rax)
	testl	%ecx, %ecx
	js	.LBB30_35
# BB#34:                                #   in Loop: Header=BB30_4 Depth=2
	movq	img(%rip), %rcx
	movq	14384(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	16(%rcx), %rcx
	movq	enc_picture(%rip), %rdx
	leaq	(%rdx,%r9), %rbx
	movq	24(%rbx,%r11,8), %rbx
	movq	6496(%rdx), %rbp
	movq	8(%rbp), %rbp
	movq	(%rbp,%rdi,8), %rbp
	movq	%rbx, (%rbp,%rax,8)
	movzwl	(%rcx), %ebx
	movq	6512(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movq	(%rdx,%rax,8), %rax
	movw	%bx, (%rax)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rax)
	cmpq	$3, %rsi
	jne	.LBB30_36
	jmp	.LBB30_37
	.p2align	4, 0x90
.LBB30_32:                              #   in Loop: Header=BB30_4 Depth=2
	movb	$-1, (%rdx,%rax)
	movq	enc_picture(%rip), %rcx
	movq	6496(%rcx), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movq	$-1, (%rdx,%rax,8)
	movq	6512(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx,%rax,8), %rax
	movl	$0, (%rax)
.LBB30_35:                              #   in Loop: Header=BB30_4 Depth=2
	cmpq	$3, %rsi
	jne	.LBB30_36
.LBB30_37:                              #   in Loop: Header=BB30_3 Depth=1
	incq	%r10
	cmpq	$2, %r10
	jne	.LBB30_3
	jmp	.LBB30_51
.LBB30_6:
	movq	img(%rip), %r9
	cmpb	$1, %r13b
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	jne	.LBB30_10
# BB#7:
	movl	172(%r9), %ecx
	movl	%r14d, %ebp
	andl	$2, %ebp
	leal	(%rcx,%rbp), %eax
	leal	4(%rbp,%rcx), %ecx
	cmpl	%ecx, %eax
	jge	.LBB30_15
# BB#8:                                 # %.lr.ph349
	orl	$4, %ebp
	leal	(%r14,%r14), %r12d
	andl	$2, %r12d
	movl	%r14d, %r15d
	andl	$1, %r15d
	negl	%r15d
	andl	$-2, %r15d
	leal	3(%r12), %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movslq	%eax, %rbx
	.p2align	4, 0x90
.LBB30_9:                               # =>This Inner Loop Header: Depth=1
	movl	168(%r9), %eax
	leal	(%rax,%r12), %ecx
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movslq	%ecx, %rcx
	movl	$-1, (%rdx,%rcx)
	movq	enc_picture(%rip), %rdx
	movq	6512(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%rbx,8), %rdx
	leaq	(,%rcx,8), %rdi
	movq	(%rdx,%rcx,8), %rdx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rdx)
	movq	enc_picture(%rip), %rdx
	movq	6496(%rdx), %rdx
	movq	(%rdx), %rdx
	addq	(%rdx,%rbx,8), %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%rax), %edx
	cmpl	%ecx, %edx
	cmovgel	%edx, %ecx
	addl	%r15d, %ecx
	subl	%eax, %ecx
	leaq	8(,%rcx,8), %rdx
	movl	$255, %esi
	callq	memset
	incq	%rbx
	movq	img(%rip), %r9
	movl	172(%r9), %eax
	addl	%ebp, %eax
	cltq
	cmpq	%rax, %rbx
	jl	.LBB30_9
	jmp	.LBB30_15
.LBB30_38:                              # %.preheader
	movslq	12(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movslq	%edi, %r15
	leal	1(%rcx), %eax
	cltq
	movslq	16(%rsp), %r10          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movslq	%r14d, %r12
	shlq	$3, %r12
	xorl	%r8d, %r8d
	imulq	$264, %rax, %rax        # imm = 0x108
	movq	%rax, 32(%rsp)          # 8-byte Spill
	imulq	$264, %rcx, %rax        # imm = 0x108
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB30_39:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_40 Depth 2
	movq	img(%rip), %rsi
	movslq	172(%rsi), %rcx
	movslq	%r8d, %rax
	addq	%rcx, %rax
	xorl	%ebx, %ebx
	movl	12(%rsp), %edi          # 4-byte Reload
	jmp	.LBB30_40
	.p2align	4, 0x90
.LBB30_49:                              # %._crit_edge422
                                        #   in Loop: Header=BB30_40 Depth=2
	incq	%rbx
	movq	img(%rip), %rsi
.LBB30_40:                              #   Parent Loop BB30_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	168(%rsi), %ebp
	leaq	(%r14,%rbx), %rcx
	addq	%rbp, %rcx
	cmpb	$1, %r13b
	jne	.LBB30_42
# BB#41:                                #   in Loop: Header=BB30_40 Depth=2
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%rax,8), %rsi
	movslq	%ecx, %rcx
	movb	$-1, (%rsi,%rcx)
	movq	enc_picture(%rip), %r9
	movq	6496(%r9), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%rax,8), %rsi
	movq	$-1, (%rsi,%rcx,8)
	movq	6512(%r9), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%rax,8), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movw	$0, (%rsi)
	xorl	%ebp, %ebp
	jmp	.LBB30_43
	.p2align	4, 0x90
.LBB30_42:                              #   in Loop: Header=BB30_40 Depth=2
	movq	14384(%rsi), %rsi
	movq	(%rsi,%r8,8), %rsi
	addq	%r12, %rsi
	movq	(%rsi,%rbx,8), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%r10,8), %rsi
	movq	(%rsi,%r15,8), %rbp
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%rax,8), %rsi
	movslq	%ecx, %rcx
	movl	16(%rsp), %edx          # 4-byte Reload
	movb	%dl, (%rsi,%rcx)
	movq	enc_picture(%rip), %r9
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%r9,%rdx), %rsi
	movq	24(%rsi,%r10,8), %rsi
	movq	%r14, %r11
	movq	6496(%r9), %r14
	movq	(%r14), %rdx
	movq	%r11, %r14
	movq	(%rdx,%rax,8), %rdx
	movq	%rsi, (%rdx,%rcx,8)
	movzwl	(%rbp), %edx
	movq	6512(%r9), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%rax,8), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movw	%dx, (%rsi)
	movw	2(%rbp), %bp
.LBB30_43:                              #   in Loop: Header=BB30_40 Depth=2
	cmpl	$0, 112(%rsp)
	movw	%bp, 2(%rsi)
	je	.LBB30_48
# BB#44:                                #   in Loop: Header=BB30_40 Depth=2
	movq	6488(%r9), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rax,8), %rsi
	testb	%r13b, %r13b
	je	.LBB30_45
# BB#46:                                #   in Loop: Header=BB30_40 Depth=2
	movb	%dil, (%rsi,%rcx)
	testl	%edi, %edi
	js	.LBB30_48
# BB#47:                                #   in Loop: Header=BB30_40 Depth=2
	movq	img(%rip), %rdx
	movq	14384(%rdx), %rdx
	movq	(%rdx,%r8,8), %rdx
	addq	%r12, %rdx
	movq	(%rdx,%rbx,8), %rdx
	movq	8(%rdx), %rdx
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdx,%rdi,8), %rdx
	movq	(%rdx,%r15,8), %rdx
	movq	enc_picture(%rip), %rsi
	movq	32(%rsp), %rbp          # 8-byte Reload
	leaq	(%rsi,%rbp), %rbp
	movq	24(%rbp,%rdi,8), %rbp
	movq	6496(%rsi), %rdi
	movq	8(%rdi), %rdi
	movq	(%rdi,%rax,8), %rdi
	movq	%rbp, (%rdi,%rcx,8)
	movzwl	(%rdx), %edi
	movq	6512(%rsi), %rsi
	movq	8(%rsi), %rsi
	movq	(%rsi,%rax,8), %rsi
	movq	(%rsi,%rcx,8), %rcx
	movw	%di, (%rcx)
	movl	12(%rsp), %edi          # 4-byte Reload
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rcx)
	cmpq	$1, %rbx
	jne	.LBB30_49
	jmp	.LBB30_50
	.p2align	4, 0x90
.LBB30_45:                              #   in Loop: Header=BB30_40 Depth=2
	movb	$-1, (%rsi,%rcx)
	movq	enc_picture(%rip), %rdx
	movq	6496(%rdx), %rsi
	movq	8(%rsi), %rsi
	movq	(%rsi,%rax,8), %rsi
	movq	$-1, (%rsi,%rcx,8)
	movq	6512(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rax,8), %rdx
	movq	(%rdx,%rcx,8), %rcx
	movl	$0, (%rcx)
.LBB30_48:                              #   in Loop: Header=BB30_40 Depth=2
	cmpq	$1, %rbx
	jne	.LBB30_49
.LBB30_50:                              #   in Loop: Header=BB30_39 Depth=1
	incq	%r8
	cmpq	$4, %r8
	jne	.LBB30_39
	jmp	.LBB30_51
.LBB30_10:
	movl	%r14d, %edx
	andl	$2, %edx
	leal	(%r14,%r14), %eax
	andl	$2, %eax
	cmpw	$0, 14410(%r9)
	je	.LBB30_13
# BB#11:                                # %.preheader332
	movslq	%ecx, %rcx
	movl	%edx, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	enc_picture(%rip), %rbx
	xorl	%edx, %edx
	movslq	%eax, %r15
	imulq	$264, %rcx, %r10        # imm = 0x108
	.p2align	4, 0x90
.LBB30_12:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rdx), %eax
	addl	172(%r9), %eax
	movslq	168(%r9), %rcx
	addq	%r15, %rcx
	movq	6488(%rbx), %rsi
	movq	(%rsi), %rsi
	cltq
	movq	(%rsi,%rax,8), %rsi
	movl	$0, (%rsi,%rcx)
	movq	img(%rip), %r9
	movzwl	14410(%r9), %edi
	leaq	14400(%r9), %rsi
	leaq	14392(%r9), %r11
	cmpl	$1, %edi
	movq	%rsi, %rdi
	cmoveq	%r11, %rdi
	movq	(%rdi), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi,%rdx,8), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi), %rdi
	movq	8(%rdi), %rbp
	movq	enc_picture(%rip), %rbx
	movq	24(%rbx,%r10), %r8
	movq	6496(%rbx), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi,%rax,8), %r12
	movq	%r8, (%r12,%rcx,8)
	movzwl	(%rbp), %r8d
	movq	6512(%rbx), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi,%rax,8), %rax
	movq	(%rax,%rcx,8), %rdi
	movw	%r8w, (%rdi)
	movzwl	2(%rbp), %ebp
	movw	%bp, 2(%rdi)
	movzwl	14410(%r9), %edi
	cmpl	$1, %edi
	movq	%rsi, %rdi
	cmoveq	%r11, %rdi
	movq	(%rdi), %rdi
	movq	8(%rdi), %rdi
	movq	(%rdi,%rdx,8), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi), %rdi
	movq	8(%rdi), %rdi
	movq	24(%rbx,%r10), %rbp
	movq	%rbp, 8(%r12,%rcx,8)
	movzwl	(%rdi), %r8d
	movq	8(%rax,%rcx,8), %rbp
	movw	%r8w, (%rbp)
	movzwl	2(%rdi), %edi
	movw	%di, 2(%rbp)
	movzwl	14410(%r9), %edi
	cmpl	$1, %edi
	movq	%rsi, %rdi
	cmoveq	%r11, %rdi
	movq	(%rdi), %rdi
	movq	16(%rdi), %rdi
	movq	(%rdi,%rdx,8), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi), %rdi
	movq	8(%rdi), %rdi
	movq	24(%rbx,%r10), %rbp
	movq	%rbp, 16(%r12,%rcx,8)
	movzwl	(%rdi), %r8d
	movq	16(%rax,%rcx,8), %rbp
	movw	%r8w, (%rbp)
	movzwl	2(%rdi), %edi
	movw	%di, 2(%rbp)
	movzwl	14410(%r9), %edi
	cmpl	$1, %edi
	cmoveq	%r11, %rsi
	movq	(%rsi), %rsi
	movq	24(%rsi), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi), %rsi
	movq	8(%rsi), %rsi
	movq	24(%rbx,%r10), %rdi
	movq	%rdi, 24(%r12,%rcx,8)
	movzwl	(%rsi), %edi
	movq	24(%rax,%rcx,8), %rax
	movw	%di, (%rax)
	movzwl	2(%rsi), %ecx
	movw	%cx, 2(%rax)
	incq	%rdx
	cmpq	$4, %rdx
	jne	.LBB30_12
	jmp	.LBB30_15
.LBB30_13:                              # %.preheader330
	movl	16(%rsp), %edi          # 4-byte Reload
	movslq	%edi, %r12
	movslq	%ecx, %rbp
	movl	%edx, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	enc_picture(%rip), %r15
	xorl	%esi, %esi
	cltq
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movzbl	%dil, %eax
	imull	$16843009, %eax, %eax   # imm = 0x1010101
	movl	%eax, 16(%rsp)          # 4-byte Spill
	imulq	$264, %rbp, %rax        # imm = 0x108
	movq	%rax, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB30_14:                              # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rsi), %eax
	addl	172(%r9), %eax
	movslq	168(%r9), %rbx
	movq	6488(%r15), %rdx
	movq	(%rdx), %rdx
	addq	40(%rsp), %rbx          # 8-byte Folded Reload
	movslq	%eax, %rbp
	movq	(%rdx,%rbp,8), %rax
	movl	16(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, (%rax,%rbx)
	movq	img(%rip), %r9
	movq	14384(%r9), %rax
	movq	(%rax,%rsi,8), %r8
	movq	(%r8), %rdx
	movq	(%rdx), %rcx
	movq	enc_picture(%rip), %r15
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%r15,%rax), %rdx
	movq	24(%rdx,%r12,8), %r10
	movq	(%rcx,%r12,8), %rcx
	movq	6496(%r15), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi,%rbp,8), %r11
	movq	8(%rcx), %rcx
	movq	%r10, (%r11,%rbx,8)
	movzwl	(%rcx), %r10d
	movq	6512(%r15), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi,%rbp,8), %rdi
	movq	(%rdi,%rbx,8), %rbp
	movw	%r10w, (%rbp)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rbp)
	movq	8(%r8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	8(%rcx), %rcx
	movq	24(%rdx,%r12,8), %rbp
	movq	%rbp, 8(%r11,%rbx,8)
	movzwl	(%rcx), %r10d
	movq	8(%rdi,%rbx,8), %rbp
	movw	%r10w, (%rbp)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rbp)
	movq	16(%r8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	8(%rcx), %rcx
	movq	24(%rdx,%r12,8), %rbp
	movq	%rbp, 16(%r11,%rbx,8)
	movq	16(%rdi,%rbx,8), %rbp
	movzwl	(%rcx), %eax
	movw	%ax, (%rbp)
	movzwl	2(%rcx), %eax
	movw	%ax, 2(%rbp)
	movq	24(%r8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rdx,%r12,8), %rcx
	movq	8(%rax), %rax
	movq	%rcx, 24(%r11,%rbx,8)
	movq	24(%rdi,%rbx,8), %rcx
	movzwl	(%rax), %edx
	movw	%dx, (%rcx)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rcx)
	incq	%rsi
	cmpq	$4, %rsi
	jne	.LBB30_14
.LBB30_15:                              # %.loopexit329
	cmpl	$0, 112(%rsp)
	je	.LBB30_51
# BB#16:
	testb	%r13b, %r13b
	je	.LBB30_17
# BB#20:
	movl	%r14d, %r15d
	andl	$2, %r15d
	addl	%r14d, %r14d
	andl	$2, %r14d
	cmpw	$0, 14410(%r9)
	je	.LBB30_23
# BB#21:                                # %.preheader327
	movq	48(%rsp), %rax          # 8-byte Reload
	incl	%eax
	cltq
	movl	%r15d, %r8d
	movq	enc_picture(%rip), %rbp
	xorl	%edx, %edx
	movslq	%r14d, %r11
	imulq	$264, %rax, %r10        # imm = 0x108
	.p2align	4, 0x90
.LBB30_22:                              # =>This Inner Loop Header: Depth=1
	leal	(%r8,%rdx), %eax
	addl	172(%r9), %eax
	movslq	168(%r9), %rbx
	addq	%r11, %rbx
	movq	6488(%rbp), %rcx
	movq	8(%rcx), %rcx
	cltq
	movq	(%rcx,%rax,8), %rcx
	movl	$0, (%rcx,%rbx)
	movq	img(%rip), %r9
	movzwl	14410(%r9), %edi
	leaq	14400(%r9), %rcx
	leaq	14392(%r9), %r14
	cmpl	$1, %edi
	movq	%rcx, %rdi
	cmoveq	%r14, %rdi
	movq	(%rdi), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi,%rdx,8), %rdi
	movq	8(%rdi), %rdi
	movq	(%rdi), %rdi
	movq	8(%rdi), %rsi
	movq	enc_picture(%rip), %rbp
	movq	24(%rbp,%r10), %r12
	movq	6496(%rbp), %rdi
	movq	8(%rdi), %rdi
	movq	(%rdi,%rax,8), %r15
	movq	%r12, (%r15,%rbx,8)
	movzwl	(%rsi), %r12d
	movq	6512(%rbp), %rdi
	movq	8(%rdi), %rdi
	movq	(%rdi,%rax,8), %r13
	movq	(%r13,%rbx,8), %rdi
	movw	%r12w, (%rdi)
	movzwl	2(%rsi), %esi
	movw	%si, 2(%rdi)
	movzwl	14410(%r9), %esi
	cmpl	$1, %esi
	movq	%rcx, %rsi
	cmoveq	%r14, %rsi
	movq	(%rsi), %rsi
	movq	8(%rsi), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	8(%rsi), %rsi
	movq	(%rsi), %rsi
	movq	8(%rsi), %rsi
	movq	24(%rbp,%r10), %rdi
	movq	%rdi, 8(%r15,%rbx,8)
	movzwl	(%rsi), %edi
	movq	8(%r13,%rbx,8), %rax
	movw	%di, (%rax)
	movzwl	2(%rsi), %esi
	movw	%si, 2(%rax)
	movzwl	14410(%r9), %eax
	cmpl	$1, %eax
	movq	%rcx, %rax
	cmoveq	%r14, %rax
	movq	(%rax), %rax
	movq	16(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	24(%rbp,%r10), %rsi
	movq	%rsi, 16(%r15,%rbx,8)
	movzwl	(%rax), %esi
	movq	16(%r13,%rbx,8), %rdi
	movw	%si, (%rdi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rdi)
	movzwl	14410(%r9), %eax
	cmpl	$1, %eax
	cmoveq	%r14, %rcx
	movq	(%rcx), %rax
	movq	24(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	24(%rbp,%r10), %rcx
	movq	%rcx, 24(%r15,%rbx,8)
	movzwl	(%rax), %ecx
	movq	24(%r13,%rbx,8), %rsi
	movw	%cx, (%rsi)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rsi)
	incq	%rdx
	cmpq	$4, %rdx
	jne	.LBB30_22
	jmp	.LBB30_51
.LBB30_17:
	movslq	172(%r9), %rax
	movl	%r14d, %ebx
	andl	$2, %ebx
	leaq	(%rbx,%rax), %rbp
	movq	img(%rip), %rax
	orl	$4, %ebx
	movl	172(%rax), %ecx
	addl	%ebx, %ecx
	cmpl	%ecx, %ebp
	jge	.LBB30_51
# BB#18:                                # %.lr.ph
	leal	(%r14,%r14), %r15d
	andl	$2, %r15d
	andl	$1, %r14d
	negl	%r14d
	andl	$-2, %r14d
	leal	3(%r15), %r12d
	.p2align	4, 0x90
.LBB30_19:                              # =>This Inner Loop Header: Depth=1
	movl	168(%rax), %eax
	leal	(%rax,%r15), %ecx
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movslq	%ecx, %rcx
	movl	$-1, (%rdx,%rcx)
	movq	enc_picture(%rip), %rdx
	movq	6512(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rbp,8), %rdx
	leaq	(,%rcx,8), %rdi
	movq	(%rdx,%rcx,8), %rdx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rdx)
	movq	enc_picture(%rip), %rdx
	movq	6496(%rdx), %rdx
	movq	8(%rdx), %rdx
	addq	(%rdx,%rbp,8), %rdi
	leal	(%r12,%rax), %edx
	cmpl	%ecx, %edx
	cmovgel	%edx, %ecx
	addl	%r14d, %ecx
	subl	%eax, %ecx
	leaq	8(,%rcx,8), %rdx
	movl	$255, %esi
	callq	memset
	incq	%rbp
	movq	img(%rip), %rax
	movl	172(%rax), %ecx
	addl	%ebx, %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB30_19
	jmp	.LBB30_51
.LBB30_23:                              # %.preheader325
	movq	48(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movslq	%eax, %rbp
	movl	12(%rsp), %ecx          # 4-byte Reload
	movslq	%ecx, %r12
	testl	%ecx, %ecx
	js	.LBB30_24
# BB#27:                                # %.preheader325.split.us.preheader
	movl	%r15d, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	img(%rip), %r15
	movq	enc_picture(%rip), %r13
	xorl	%edx, %edx
	movslq	%r14d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movzbl	%cl, %eax
	imull	$16843009, %eax, %r10d  # imm = 0x1010101
	imulq	$264, %rbp, %r11        # imm = 0x108
	.p2align	4, 0x90
.LBB30_28:                              # %.preheader325.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rdx), %eax
	addl	172(%r15), %eax
	movslq	168(%r15), %rsi
	movq	6488(%r13), %rdi
	movq	8(%rdi), %rdi
	addq	24(%rsp), %rsi          # 8-byte Folded Reload
	movslq	%eax, %rcx
	movq	(%rdi,%rcx,8), %rax
	movl	%r10d, (%rax,%rsi)
	movq	enc_picture(%rip), %r13
	leaq	(%r13,%r11), %r14
	movq	24(%r14,%r12,8), %rax
	movq	6496(%r13), %rbx
	movq	8(%rbx), %rbx
	movq	(%rbx,%rcx,8), %r9
	movq	%rax, (%r9,%rsi,8)
	movq	img(%rip), %r15
	movq	14384(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	(%rax), %rdi
	movq	8(%rdi), %rdi
	movq	(%rdi,%r12,8), %rdi
	movq	8(%rdi), %rdi
	movq	6512(%r13), %rbp
	movq	8(%rbp), %rbp
	movzwl	(%rdi), %r8d
	movq	(%rbp,%rcx,8), %rcx
	movq	(%rcx,%rsi,8), %rbp
	movw	%r8w, (%rbp)
	movzwl	2(%rdi), %edi
	movw	%di, 2(%rbp)
	movq	24(%r14,%r12,8), %rdi
	movq	%rdi, 8(%r9,%rsi,8)
	movq	8(%rax), %rdi
	movq	8(%rdi), %rdi
	movq	(%rdi,%r12,8), %rdi
	movq	8(%rdi), %rdi
	movzwl	(%rdi), %r8d
	movq	8(%rcx,%rsi,8), %rbp
	movw	%r8w, (%rbp)
	movzwl	2(%rdi), %edi
	movw	%di, 2(%rbp)
	movq	24(%r14,%r12,8), %rdi
	movq	%rdi, 16(%r9,%rsi,8)
	movq	16(%rax), %rdi
	movq	8(%rdi), %rdi
	movq	(%rdi,%r12,8), %rdi
	movq	8(%rdi), %rdi
	movq	16(%rcx,%rsi,8), %rbp
	movzwl	(%rdi), %ebx
	movw	%bx, (%rbp)
	movzwl	2(%rdi), %edi
	movw	%di, 2(%rbp)
	movq	24(%r14,%r12,8), %rdi
	movq	%rdi, 24(%r9,%rsi,8)
	movq	24(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	8(%rax), %rax
	movq	24(%rcx,%rsi,8), %rcx
	movzwl	(%rax), %esi
	movw	%si, (%rcx)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rcx)
	incq	%rdx
	cmpq	$4, %rdx
	jne	.LBB30_28
	jmp	.LBB30_51
.LBB30_24:                              # %.preheader325.split.preheader
	xorl	%eax, %eax
	movq	enc_picture(%rip), %rbx
	movslq	%r14d, %r8
	movzbl	%cl, %edi
	imull	$16843009, %edi, %r10d  # imm = 0x1010101
	imulq	$264, %rbp, %r11        # imm = 0x108
	jmp	.LBB30_25
	.p2align	4, 0x90
.LBB30_26:                              # %.preheader325.split..preheader325.split_crit_edge
                                        #   in Loop: Header=BB30_25 Depth=1
	incl	%eax
	movq	img(%rip), %r9
.LBB30_25:                              # %.preheader325.split
                                        # =>This Inner Loop Header: Depth=1
	leal	(%r15,%rax), %ecx
	addl	172(%r9), %ecx
	movslq	168(%r9), %rsi
	addq	%r8, %rsi
	movq	6488(%rbx), %rbx
	movq	8(%rbx), %rbx
	movslq	%ecx, %rcx
	movq	(%rbx,%rcx,8), %rbx
	movl	%r10d, (%rbx,%rsi)
	movq	enc_picture(%rip), %rbx
	leaq	(%rbx,%r11), %rdi
	movq	24(%rdi,%r12,8), %rbp
	movq	6496(%rbx), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rcx,8), %rcx
	movq	%rbp, (%rcx,%rsi,8)
	movq	24(%rdi,%r12,8), %rdx
	movq	%rdx, 8(%rcx,%rsi,8)
	movq	24(%rdi,%r12,8), %rdx
	movq	%rdx, 16(%rcx,%rsi,8)
	movq	24(%rdi,%r12,8), %rdx
	movq	%rdx, 24(%rcx,%rsi,8)
	cmpl	$3, %eax
	jne	.LBB30_26
.LBB30_51:                              # %.loopexit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	assign_enc_picture_params, .Lfunc_end30-assign_enc_picture_params
	.cfi_endproc

	.globl	update_refresh_map
	.p2align	4, 0x90
	.type	update_refresh_map,@function
update_refresh_map:                     # @update_refresh_map
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rax
	movl	4732(%rax), %ecx
	cmpl	$2, %ecx
	je	.LBB31_18
# BB#1:
	cmpl	$1, %ecx
	jne	.LBB31_19
# BB#2:
	movl	4168(%rax), %eax
	cmpl	$1, %eax
	jg	.LBB31_4
# BB#3:
	movq	img(%rip), %rax
	movslq	164(%rax), %rcx
	shlq	$4, %rcx
	testl	%edi, %edi
	movq	refresh_map(%rip), %rdx
	movq	(%rdx,%rcx), %rcx
	movslq	160(%rax), %rax
	setne	(%rcx,%rax,2)
	movq	img(%rip), %rax
	movslq	164(%rax), %rcx
	shlq	$4, %rcx
	testl	%edi, %edi
	movq	refresh_map(%rip), %rdx
	movq	(%rdx,%rcx), %rcx
	movl	160(%rax), %eax
	leal	1(%rax,%rax), %eax
	cltq
	setne	(%rcx,%rax)
	movq	refresh_map(%rip), %rax
	movq	img(%rip), %rcx
	movl	164(%rcx), %edx
	leal	1(%rdx,%rdx), %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
	movslq	160(%rcx), %rcx
	setne	(%rax,%rcx,2)
	movq	refresh_map(%rip), %rax
	movq	img(%rip), %rcx
	movl	160(%rcx), %edx
	movl	164(%rcx), %ecx
	leal	1(%rcx,%rcx), %ecx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rcx
	setne	(%rax,%rcx)
	retq
.LBB31_18:
	movl	72(%rdx), %eax
	addl	$-9, %eax
	cmpl	$2, %eax
	setb	%al
	movq	refresh_map(%rip), %rcx
	movq	img(%rip), %rsi
	movslq	164(%rsi), %rdi
	shlq	$4, %rdi
	movq	(%rcx,%rdi), %rcx
	movslq	160(%rsi), %rsi
	movb	%al, (%rcx,%rsi,2)
	movl	72(%rdx), %eax
	addl	$-9, %eax
	cmpl	$2, %eax
	setb	%al
	movq	refresh_map(%rip), %rcx
	movq	img(%rip), %rsi
	movslq	164(%rsi), %rdi
	shlq	$4, %rdi
	movq	(%rcx,%rdi), %rcx
	movl	160(%rsi), %esi
	leal	1(%rsi,%rsi), %esi
	movslq	%esi, %rsi
	movb	%al, (%rcx,%rsi)
	movl	72(%rdx), %eax
	addl	$-9, %eax
	cmpl	$2, %eax
	setb	%al
	movq	refresh_map(%rip), %rcx
	movq	img(%rip), %rsi
	movl	164(%rsi), %edi
	leal	1(%rdi,%rdi), %edi
	movslq	%edi, %rdi
	movq	(%rcx,%rdi,8), %rcx
	movslq	160(%rsi), %rsi
	movb	%al, (%rcx,%rsi,2)
	movl	72(%rdx), %eax
	addl	$-9, %eax
	cmpl	$2, %eax
	setb	%al
	movq	refresh_map(%rip), %rcx
	movq	img(%rip), %rdx
	movl	160(%rdx), %esi
	movl	164(%rdx), %edx
	leal	1(%rdx,%rdx), %edx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	leal	1(%rsi,%rsi), %edx
	movslq	%edx, %rdx
	movb	%al, (%rcx,%rdx)
.LBB31_19:
	retq
.LBB31_4:
	cmpl	$3, %eax
	jne	.LBB31_19
# BB#5:
	testl	%esi, %esi
	je	.LBB31_7
# BB#6:
	xorl	%r8d, %r8d
	jmp	.LBB31_8
.LBB31_7:
	movl	72(%rdx), %eax
	addl	$-9, %eax
	cmpl	$2, %eax
	setb	%r8b
.LBB31_8:
	movq	refresh_map(%rip), %rcx
	movq	img(%rip), %rdi
	movslq	164(%rdi), %rax
	shlq	$4, %rax
	movq	(%rcx,%rax), %rax
	movslq	160(%rdi), %rcx
	movb	%r8b, (%rax,%rcx,2)
	testl	%esi, %esi
	je	.LBB31_10
# BB#9:
	xorl	%r8d, %r8d
	jmp	.LBB31_11
.LBB31_10:
	movl	72(%rdx), %eax
	addl	$-9, %eax
	cmpl	$2, %eax
	setb	%r8b
.LBB31_11:
	movq	refresh_map(%rip), %rcx
	movq	img(%rip), %rdi
	movslq	164(%rdi), %rax
	shlq	$4, %rax
	movq	(%rcx,%rax), %rax
	movl	160(%rdi), %ecx
	leal	1(%rcx,%rcx), %ecx
	movslq	%ecx, %rcx
	movb	%r8b, (%rax,%rcx)
	testl	%esi, %esi
	je	.LBB31_13
# BB#12:
	xorl	%r8d, %r8d
	jmp	.LBB31_14
.LBB31_13:
	movl	72(%rdx), %eax
	addl	$-9, %eax
	cmpl	$2, %eax
	setb	%r8b
.LBB31_14:
	movq	refresh_map(%rip), %rcx
	movq	img(%rip), %rdi
	movl	164(%rdi), %eax
	leal	1(%rax,%rax), %eax
	cltq
	movq	(%rcx,%rax,8), %rax
	movslq	160(%rdi), %rcx
	movb	%r8b, (%rax,%rcx,2)
	testl	%esi, %esi
	je	.LBB31_16
# BB#15:
	xorl	%eax, %eax
	jmp	.LBB31_17
.LBB31_16:
	movl	72(%rdx), %eax
	addl	$-9, %eax
	cmpl	$2, %eax
	setb	%al
.LBB31_17:
	movq	refresh_map(%rip), %rcx
	movq	img(%rip), %rdx
	movl	160(%rdx), %esi
	movl	164(%rdx), %edx
	leal	1(%rdx,%rdx), %edx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	leal	1(%rsi,%rsi), %edx
	movslq	%edx, %rdx
	movb	%al, (%rcx,%rdx)
	retq
.Lfunc_end31:
	.size	update_refresh_map, .Lfunc_end31-update_refresh_map
	.cfi_endproc

	.type	AdaptRndCrPos,@object   # @AdaptRndCrPos
	.section	.rodata,"a",@progbits
	.globl	AdaptRndCrPos
	.p2align	4
AdaptRndCrPos:
	.long	4                       # 0x4
	.long	7                       # 0x7
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	10                      # 0xa
	.long	13                      # 0xd
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.size	AdaptRndCrPos, 40

	.type	AdaptRndPos,@object     # @AdaptRndPos
	.globl	AdaptRndPos
	.p2align	4
AdaptRndPos:
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	9                       # 0x9
	.long	12                      # 0xc
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.size	AdaptRndPos, 80

	.type	bestInterFAdjust4x4,@object # @bestInterFAdjust4x4
	.bss
	.globl	bestInterFAdjust4x4
	.p2align	3
bestInterFAdjust4x4:
	.quad	0
	.size	bestInterFAdjust4x4, 8

	.type	bestIntraFAdjust4x4,@object # @bestIntraFAdjust4x4
	.globl	bestIntraFAdjust4x4
	.p2align	3
bestIntraFAdjust4x4:
	.quad	0
	.size	bestIntraFAdjust4x4, 8

	.type	bestInterFAdjust8x8,@object # @bestInterFAdjust8x8
	.globl	bestInterFAdjust8x8
	.p2align	3
bestInterFAdjust8x8:
	.quad	0
	.size	bestInterFAdjust8x8, 8

	.type	bestIntraFAdjust8x8,@object # @bestIntraFAdjust8x8
	.globl	bestIntraFAdjust8x8
	.p2align	3
bestIntraFAdjust8x8:
	.quad	0
	.size	bestIntraFAdjust8x8, 8

	.type	bestInterFAdjust4x4Cr,@object # @bestInterFAdjust4x4Cr
	.globl	bestInterFAdjust4x4Cr
	.p2align	3
bestInterFAdjust4x4Cr:
	.quad	0
	.size	bestInterFAdjust4x4Cr, 8

	.type	bestIntraFAdjust4x4Cr,@object # @bestIntraFAdjust4x4Cr
	.globl	bestIntraFAdjust4x4Cr
	.p2align	3
bestIntraFAdjust4x4Cr:
	.quad	0
	.size	bestIntraFAdjust4x4Cr, 8

	.type	fadjust8x8,@object      # @fadjust8x8
	.globl	fadjust8x8
	.p2align	3
fadjust8x8:
	.quad	0
	.size	fadjust8x8, 8

	.type	fadjust4x4,@object      # @fadjust4x4
	.globl	fadjust4x4
	.p2align	3
fadjust4x4:
	.quad	0
	.size	fadjust4x4, 8

	.type	fadjust4x4Cr,@object    # @fadjust4x4Cr
	.globl	fadjust4x4Cr
	.p2align	3
fadjust4x4Cr:
	.quad	0
	.size	fadjust4x4Cr, 8

	.type	fadjust8x8Cr,@object    # @fadjust8x8Cr
	.globl	fadjust8x8Cr
	.p2align	3
fadjust8x8Cr:
	.quad	0
	.size	fadjust8x8Cr, 8

	.type	cofAC,@object           # @cofAC
	.globl	cofAC
	.p2align	3
cofAC:
	.quad	0
	.size	cofAC, 8

	.type	cofAC8x8,@object        # @cofAC8x8
	.globl	cofAC8x8
	.p2align	3
cofAC8x8:
	.quad	0
	.size	cofAC8x8, 8

	.type	cofDC,@object           # @cofDC
	.globl	cofDC
	.p2align	3
cofDC:
	.quad	0
	.size	cofDC, 8

	.type	cofAC4x4,@object        # @cofAC4x4
	.globl	cofAC4x4
	.p2align	3
cofAC4x4:
	.quad	0
	.size	cofAC4x4, 8

	.type	cofAC4x4intern,@object  # @cofAC4x4intern
	.globl	cofAC4x4intern
	.p2align	3
cofAC4x4intern:
	.quad	0
	.size	cofAC4x4intern, 8

	.type	cs_mb,@object           # @cs_mb
	.globl	cs_mb
	.p2align	3
cs_mb:
	.quad	0
	.size	cs_mb, 8

	.type	cs_b8,@object           # @cs_b8
	.globl	cs_b8
	.p2align	3
cs_b8:
	.quad	0
	.size	cs_b8, 8

	.type	cs_cm,@object           # @cs_cm
	.globl	cs_cm
	.p2align	3
cs_cm:
	.quad	0
	.size	cs_cm, 8

	.type	cs_imb,@object          # @cs_imb
	.globl	cs_imb
	.p2align	3
cs_imb:
	.quad	0
	.size	cs_imb, 8

	.type	cs_ib8,@object          # @cs_ib8
	.globl	cs_ib8
	.p2align	3
cs_ib8:
	.quad	0
	.size	cs_ib8, 8

	.type	cs_ib4,@object          # @cs_ib4
	.globl	cs_ib4
	.p2align	3
cs_ib4:
	.quad	0
	.size	cs_ib4, 8

	.type	cs_pc,@object           # @cs_pc
	.globl	cs_pc
	.p2align	3
cs_pc:
	.quad	0
	.size	cs_pc, 8

	.type	cofAC_8x8ts,@object     # @cofAC_8x8ts
	.globl	cofAC_8x8ts
	.p2align	3
cofAC_8x8ts:
	.quad	0
	.size	cofAC_8x8ts, 8

	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	mb16x16_cost,@object    # @mb16x16_cost
	.comm	mb16x16_cost,8,8
	.type	lambda_mf_factor,@object # @lambda_mf_factor
	.comm	lambda_mf_factor,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	diff,@object            # @diff
	.local	diff
	.comm	diff,64,16
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	cbp8x8,@object          # @cbp8x8
	.comm	cbp8x8,4,4
	.type	best8x8pdir,@object     # @best8x8pdir
	.comm	best8x8pdir,60,16
	.type	best8x8mode,@object     # @best8x8mode
	.comm	best8x8mode,8,2
	.type	best8x8fwref,@object    # @best8x8fwref
	.comm	best8x8fwref,60,16
	.type	best8x8bwref,@object    # @best8x8bwref
	.comm	best8x8bwref,60,16
	.type	tr8x8,@object           # @tr8x8
	.comm	tr8x8,8216,4
	.type	cnt_nonz8_8x8ts,@object # @cnt_nonz8_8x8ts
	.comm	cnt_nonz8_8x8ts,4,4
	.type	cbp8_8x8ts,@object      # @cbp8_8x8ts
	.comm	cbp8_8x8ts,4,4
	.type	cbp_blk8_8x8ts,@object  # @cbp_blk8_8x8ts
	.comm	cbp_blk8_8x8ts,8,8
	.type	cnt_nonz_8x8,@object    # @cnt_nonz_8x8
	.comm	cnt_nonz_8x8,4,4
	.type	tr4x4,@object           # @tr4x4
	.comm	tr4x4,8216,4
	.type	cbp_blk8x8,@object      # @cbp_blk8x8
	.comm	cbp_blk8x8,4,4
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"invalid direction mode"
	.size	.L.str.1, 23

	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	pred,@object            # @pred
	.comm	pred,512,16
	.type	best_mode,@object       # @best_mode
	.comm	best_mode,2,2
	.type	best_c_imode,@object    # @best_c_imode
	.comm	best_c_imode,4,4
	.type	best_i16offset,@object  # @best_i16offset
	.comm	best_i16offset,4,4
	.type	bi_pred_me,@object      # @bi_pred_me
	.comm	bi_pred_me,2,2
	.type	b8mode,@object          # @b8mode
	.comm	b8mode,16,16
	.type	b8pdir,@object          # @b8pdir
	.comm	b8pdir,16,16
	.type	b4_intra_pred_modes,@object # @b4_intra_pred_modes
	.comm	b4_intra_pred_modes,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	b4_ipredmode,@object    # @b4_ipredmode
	.comm	b4_ipredmode,16,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	rec_mbY,@object         # @rec_mbY
	.comm	rec_mbY,512,16
	.type	lrec_rec,@object        # @lrec_rec
	.comm	lrec_rec,1024,16
	.type	rec_mbU,@object         # @rec_mbU
	.comm	rec_mbU,512,16
	.type	rec_mbV,@object         # @rec_mbV
	.comm	rec_mbV,512,16
	.type	lrec_rec_U,@object      # @lrec_rec_U
	.comm	lrec_rec_U,1024,16
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	lrec_rec_V,@object      # @lrec_rec_V
	.comm	lrec_rec_V,1024,16
	.type	cbp,@object             # @cbp
	.comm	cbp,4,4
	.type	cbp_blk,@object         # @cbp_blk
	.comm	cbp_blk,8,8
	.type	luma_transform_size_8x8_flag,@object # @luma_transform_size_8x8_flag
	.comm	luma_transform_size_8x8_flag,4,4
	.type	frefframe,@object       # @frefframe
	.comm	frefframe,16,16
	.type	brefframe,@object       # @brefframe
	.comm	brefframe,16,16
	.type	all_mv8x8,@object       # @all_mv8x8
	.comm	all_mv8x8,256,16
	.type	pred_mv8x8,@object      # @pred_mv8x8
	.comm	pred_mv8x8,256,16
	.type	diff4x4,@object         # @diff4x4
	.local	diff4x4
	.comm	diff4x4,256,16
	.type	diff8x8,@object         # @diff8x8
	.local	diff8x8
	.comm	diff8x8,256,16
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	McostState,@object      # @McostState
	.comm	McostState,8,8
	.type	SearchState,@object     # @SearchState
	.comm	SearchState,8,8
	.type	fastme_ref_cost,@object # @fastme_ref_cost
	.comm	fastme_ref_cost,8,8
	.type	fastme_l0_cost,@object  # @fastme_l0_cost
	.comm	fastme_l0_cost,8,8
	.type	fastme_l1_cost,@object  # @fastme_l1_cost
	.comm	fastme_l1_cost,8,8
	.type	fastme_l0_cost_bipred,@object # @fastme_l0_cost_bipred
	.comm	fastme_l0_cost_bipred,8,8
	.type	fastme_l1_cost_bipred,@object # @fastme_l1_cost_bipred
	.comm	fastme_l1_cost_bipred,8,8
	.type	bipred_flag,@object     # @bipred_flag
	.comm	bipred_flag,4,4
	.type	fastme_best_cost,@object # @fastme_best_cost
	.comm	fastme_best_cost,8,8
	.type	pred_SAD,@object        # @pred_SAD
	.comm	pred_SAD,4,4
	.type	pred_MV_ref,@object     # @pred_MV_ref
	.comm	pred_MV_ref,8,4
	.type	pred_MV_uplayer,@object # @pred_MV_uplayer
	.comm	pred_MV_uplayer,8,4
	.type	UMHEX_blocktype,@object # @UMHEX_blocktype
	.comm	UMHEX_blocktype,4,4
	.type	predict_point,@object   # @predict_point
	.comm	predict_point,40,16
	.type	SAD_a,@object           # @SAD_a
	.comm	SAD_a,4,4
	.type	SAD_b,@object           # @SAD_b
	.comm	SAD_b,4,4
	.type	SAD_c,@object           # @SAD_c
	.comm	SAD_c,4,4
	.type	SAD_d,@object           # @SAD_d
	.comm	SAD_d,4,4
	.type	Threshold_DSR_MB,@object # @Threshold_DSR_MB
	.comm	Threshold_DSR_MB,32,16
	.type	Bsize,@object           # @Bsize
	.comm	Bsize,32,16
	.type	AlphaFourth_1,@object   # @AlphaFourth_1
	.comm	AlphaFourth_1,32,16
	.type	AlphaFourth_2,@object   # @AlphaFourth_2
	.comm	AlphaFourth_2,32,16
	.type	flag_intra,@object      # @flag_intra
	.comm	flag_intra,8,8
	.type	flag_intra_SAD,@object  # @flag_intra_SAD
	.comm	flag_intra_SAD,4,4
	.type	diffy,@object           # @diffy
	.comm	diffy,1024,16
	.type	qp_mbaff,@object        # @qp_mbaff
	.comm	qp_mbaff,16,16
	.type	delta_qp_mbaff,@object  # @delta_qp_mbaff
	.comm	delta_qp_mbaff,16,16
	.type	generic_RC,@object      # @generic_RC
	.comm	generic_RC,8,8
	.type	generic_RC_init,@object # @generic_RC_init
	.comm	generic_RC_init,8,8
	.type	generic_RC_best,@object # @generic_RC_best
	.comm	generic_RC_best,8,8
	.type	cost8_8x8ts,@object     # @cost8_8x8ts
	.comm	cost8_8x8ts,4,4
	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	encode_one_macroblock_low
	.quad	encode_one_macroblock_high
	.quad	encode_one_macroblock_highfast
	.quad	encode_one_macroblock_highloss
	.size	.Lswitch.table, 32

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Unsupported mode in SetModesAndRefframeForBlocks!"
	.size	.Lstr, 50


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
