	.text
	.file	"Treesort.bc"
	.globl	Initrand
	.p2align	4, 0x90
	.type	Initrand,@function
Initrand:                               # @Initrand
	.cfi_startproc
# BB#0:
	movq	$74755, seed(%rip)      # imm = 0x12403
	retq
.Lfunc_end0:
	.size	Initrand, .Lfunc_end0-Initrand
	.cfi_endproc

	.globl	Rand
	.p2align	4, 0x90
	.type	Rand,@function
Rand:                                   # @Rand
	.cfi_startproc
# BB#0:
	imull	$1309, seed(%rip), %eax # imm = 0x51D
	addl	$13849, %eax            # imm = 0x3619
	movzwl	%ax, %eax
	movq	%rax, seed(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end1:
	.size	Rand, .Lfunc_end1-Rand
	.cfi_endproc

	.globl	tInitarr
	.p2align	4, 0x90
	.type	tInitarr,@function
tInitarr:                               # @tInitarr
	.cfi_startproc
# BB#0:
	movq	$74755, seed(%rip)      # imm = 0x12403
	movl	$0, biggest(%rip)
	movl	$0, littlest(%rip)
	xorl	%edx, %edx
	movq	$-4999, %rax            # imm = 0xEC79
	movl	$74755, %ecx            # imm = 0x12403
	jmp	.LBB2_1
	.p2align	4, 0x90
.LBB2_7:                                # %._crit_edge
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	biggest(%rip), %edx
	incq	%rax
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	imull	$1309, %ecx, %ecx       # imm = 0x51D
	addl	$13849, %ecx            # imm = 0x3619
	movzwl	%cx, %ecx
	leal	-50000(%rcx), %esi
	movl	%esi, sortlist+20000(,%rax,4)
	cmpl	%edx, %esi
	jle	.LBB2_3
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movl	$biggest, %edx
	jmp	.LBB2_5
	.p2align	4, 0x90
.LBB2_3:                                #   in Loop: Header=BB2_1 Depth=1
	cmpl	littlest(%rip), %esi
	jge	.LBB2_6
# BB#4:                                 #   in Loop: Header=BB2_1 Depth=1
	movl	$littlest, %edx
.LBB2_5:                                # %.sink.split
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	%esi, (%rdx)
.LBB2_6:                                #   in Loop: Header=BB2_1 Depth=1
	testq	%rax, %rax
	jne	.LBB2_7
# BB#8:
	movq	%rcx, seed(%rip)
	retq
.Lfunc_end2:
	.size	tInitarr, .Lfunc_end2-tInitarr
	.cfi_endproc

	.globl	CreateNode
	.p2align	4, 0x90
	.type	CreateNode,@function
CreateNode:                             # @CreateNode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$24, %edi
	callq	malloc
	movq	%rax, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movl	%ebp, 16(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end3:
	.size	CreateNode, .Lfunc_end3-CreateNode
	.cfi_endproc

	.globl	Insert
	.p2align	4, 0x90
	.type	Insert,@function
Insert:                                 # @Insert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	.p2align	4, 0x90
.LBB4_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rbx
	cmpl	%ebp, 16(%rbx)
	jge	.LBB4_4
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.LBB4_1
	jmp	.LBB4_3
	.p2align	4, 0x90
.LBB4_4:                                #   in Loop: Header=BB4_1 Depth=1
	jle	.LBB4_8
# BB#5:                                 #   in Loop: Header=BB4_1 Depth=1
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.LBB4_1
# BB#6:
	movl	$24, %edi
	callq	malloc
	movq	%rax, 8(%rbx)
	jmp	.LBB4_7
.LBB4_3:
	movl	$24, %edi
	callq	malloc
	movq	%rax, (%rbx)
.LBB4_7:                                # %.sink.split
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movl	%ebp, 16(%rax)
.LBB4_8:                                # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	Insert, .Lfunc_end4-Insert
	.cfi_endproc

	.globl	Checktree
	.p2align	4, 0x90
	.type	Checktree,@function
Checktree:                              # @Checktree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_1
# BB#2:
	movl	16(%rdi), %eax
	xorl	%ebp, %ebp
	cmpl	16(%rbx), %eax
	jle	.LBB5_4
# BB#3:
	callq	Checktree
	xorl	%ebp, %ebp
	testl	%eax, %eax
	setne	%bpl
	jmp	.LBB5_4
.LBB5_1:
	movl	$1, %ebp
.LBB5_4:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_5
# BB#6:
	movl	16(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	16(%rbx), %ecx
	jge	.LBB5_8
# BB#7:
	callq	Checktree
	testl	%eax, %eax
	setne	%al
	testl	%ebp, %ebp
	setne	%cl
	andb	%al, %cl
	movzbl	%cl, %eax
	jmp	.LBB5_8
.LBB5_5:
	movl	%ebp, %eax
.LBB5_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end5:
	.size	Checktree, .Lfunc_end5-Checktree
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.zero	16
	.text
	.globl	Trees
	.p2align	4, 0x90
	.type	Trees,@function
Trees:                                  # @Trees
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movq	$74755, seed(%rip)      # imm = 0x12403
	movl	$0, biggest(%rip)
	movl	$0, littlest(%rip)
	xorl	%edx, %edx
	movq	$-4999, %rax            # imm = 0xEC79
	movl	$74755, %ecx            # imm = 0x12403
	jmp	.LBB6_1
	.p2align	4, 0x90
.LBB6_7:                                # %._crit_edge.i
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	biggest(%rip), %edx
	incq	%rax
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	imull	$1309, %ecx, %ecx       # imm = 0x51D
	addl	$13849, %ecx            # imm = 0x3619
	movzwl	%cx, %ecx
	leal	-50000(%rcx), %esi
	movl	%esi, sortlist+20000(,%rax,4)
	cmpl	%edx, %esi
	jle	.LBB6_3
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	movl	$biggest, %edx
	jmp	.LBB6_5
	.p2align	4, 0x90
.LBB6_3:                                #   in Loop: Header=BB6_1 Depth=1
	cmpl	littlest(%rip), %esi
	jge	.LBB6_6
# BB#4:                                 #   in Loop: Header=BB6_1 Depth=1
	movl	$littlest, %edx
.LBB6_5:                                # %.sink.split.i
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	%esi, (%rdx)
.LBB6_6:                                #   in Loop: Header=BB6_1 Depth=1
	testq	%rax, %rax
	jne	.LBB6_7
# BB#8:                                 # %tInitarr.exit
	movq	%rcx, seed(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, tree(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movl	sortlist+4(%rip), %ecx
	movl	%ecx, 16(%rax)
	movl	$2, %r15d
	jmp	.LBB6_9
	.p2align	4, 0x90
.LBB6_13:                               #   in Loop: Header=BB6_10 Depth=2
	jle	.LBB6_17
# BB#14:                                #   in Loop: Header=BB6_10 Depth=2
	movq	8(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB6_10
# BB#15:                                #   in Loop: Header=BB6_9 Depth=1
	movl	$24, %edi
	callq	malloc
	movq	%rax, 8(%rbx)
	jmp	.LBB6_16
	.p2align	4, 0x90
.LBB6_12:                               #   in Loop: Header=BB6_9 Depth=1
	movl	$24, %edi
	callq	malloc
	movq	%rax, (%rbx)
.LBB6_16:                               # %.sink.split.i5
                                        #   in Loop: Header=BB6_9 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movl	%ebp, 16(%rax)
.LBB6_17:                               # %Insert.exit
                                        #   in Loop: Header=BB6_9 Depth=1
	incq	%r15
	cmpq	$5001, %r15             # imm = 0x1389
	je	.LBB6_19
# BB#18:                                # %Insert.exit._crit_edge
                                        #   in Loop: Header=BB6_9 Depth=1
	movq	tree(%rip), %rax
.LBB6_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_10 Depth 2
	movl	sortlist(,%r15,4), %ebp
	.p2align	4, 0x90
.LBB6_10:                               # %tailrecurse.i
                                        #   Parent Loop BB6_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rbx
	cmpl	%ebp, 16(%rbx)
	jge	.LBB6_13
# BB#11:                                #   in Loop: Header=BB6_10 Depth=2
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB6_10
	jmp	.LBB6_12
.LBB6_19:
	movslq	%r14d, %rax
	movl	sortlist+8(,%rax,4), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movq	tree(%rip), %rdi
	callq	Checktree
	testl	%eax, %eax
	je	.LBB6_21
# BB#20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_21:
	movl	$.Lstr, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	puts                    # TAILCALL
.Lfunc_end6:
	.size	Trees, .Lfunc_end6-Trees
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
.Lcfi25:
	.cfi_offset %rbx, -16
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edi
	callq	Trees
	incl	%ebx
	cmpl	$100, %ebx
	jne	.LBB7_1
# BB#2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	main, .Lfunc_end7-main
	.cfi_endproc

	.type	seed,@object            # @seed
	.comm	seed,8,8
	.type	biggest,@object         # @biggest
	.comm	biggest,4,4
	.type	littlest,@object        # @littlest
	.comm	littlest,4,4
	.type	sortlist,@object        # @sortlist
	.comm	sortlist,20004,16
	.type	tree,@object            # @tree
	.comm	tree,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d\n"
	.size	.L.str, 4

	.type	value,@object           # @value
	.comm	value,4,4
	.type	fixed,@object           # @fixed
	.comm	fixed,4,4
	.type	floated,@object         # @floated
	.comm	floated,4,4
	.type	permarray,@object       # @permarray
	.comm	permarray,44,16
	.type	pctr,@object            # @pctr
	.comm	pctr,4,4
	.type	stack,@object           # @stack
	.comm	stack,16,16
	.type	cellspace,@object       # @cellspace
	.comm	cellspace,152,16
	.type	freelist,@object        # @freelist
	.comm	freelist,4,4
	.type	movesdone,@object       # @movesdone
	.comm	movesdone,4,4
	.type	ima,@object             # @ima
	.comm	ima,6724,16
	.type	imb,@object             # @imb
	.comm	imb,6724,16
	.type	imr,@object             # @imr
	.comm	imr,6724,16
	.type	rma,@object             # @rma
	.comm	rma,6724,16
	.type	rmb,@object             # @rmb
	.comm	rmb,6724,16
	.type	rmr,@object             # @rmr
	.comm	rmr,6724,16
	.type	piececount,@object      # @piececount
	.comm	piececount,16,16
	.type	class,@object           # @class
	.comm	class,52,16
	.type	piecemax,@object        # @piecemax
	.comm	piecemax,52,16
	.type	puzzl,@object           # @puzzl
	.comm	puzzl,2048,16
	.type	p,@object               # @p
	.comm	p,26624,16
	.type	n,@object               # @n
	.comm	n,4,4
	.type	kount,@object           # @kount
	.comm	kount,4,4
	.type	top,@object             # @top
	.comm	top,4,4
	.type	z,@object               # @z
	.comm	z,2056,16
	.type	w,@object               # @w
	.comm	w,2056,16
	.type	e,@object               # @e
	.comm	e,1040,16
	.type	zr,@object              # @zr
	.comm	zr,4,4
	.type	zi,@object              # @zi
	.comm	zi,4,4
	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	" Error in Tree."
	.size	.Lstr, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
