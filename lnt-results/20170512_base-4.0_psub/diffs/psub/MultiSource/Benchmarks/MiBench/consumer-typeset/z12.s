	.text
	.file	"z12.bc"
	.globl	SpannerAvailableSpace
	.p2align	4, 0x90
	.type	SpannerAvailableSpace,@function
SpannerAvailableSpace:                  # @SpannerAvailableSpace
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movl	%esi, %ebp
	movq	%rdi, %r15
	movb	32(%r15), %al
	addb	$-13, %al
	cmpb	$2, %al
	jb	.LBB0_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_2:
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	testl	%ebp, %ebp
	sete	%cl
	setne	%al
	movq	24(%r15), %r12
	cmpq	%r15, %r12
	je	.LBB0_55
# BB#3:                                 # %.lr.ph.lr.ph
	movb	%cl, %dl
	addl	$15, %edx
	movl	%edx, 28(%rsp)          # 4-byte Spill
	movb	%al, %r8b
	orl	$18, %r8d
	movslq	%ebp, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
                                        # implicit-def: %EDX
                                        # implicit-def: %EAX
	movl	%eax, 4(%rsp)           # 4-byte Spill
	xorl	%r13d, %r13d
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movl	%ebp, (%rsp)            # 4-byte Spill
	movl	%r8d, 44(%rsp)          # 4-byte Spill
.LBB0_4:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
                                        #       Child Loop BB0_7 Depth 3
                                        #       Child Loop BB0_9 Depth 3
                                        #     Child Loop BB0_13 Depth 2
                                        #       Child Loop BB0_14 Depth 3
                                        #       Child Loop BB0_16 Depth 3
                                        #     Child Loop BB0_24 Depth 2
                                        #     Child Loop BB0_28 Depth 2
                                        #       Child Loop BB0_27 Depth 3
                                        #     Child Loop BB0_32 Depth 2
                                        #       Child Loop BB0_36 Depth 3
                                        #     Child Loop BB0_42 Depth 2
                                        #     Child Loop BB0_46 Depth 2
	movl	%edx, 8(%rsp)           # 4-byte Spill
	testl	%ebp, %ebp
	je	.LBB0_12
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph.split
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_7 Depth 3
                                        #       Child Loop BB0_9 Depth 3
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB0_7:                                #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %edi
	testl	%edi, %edi
	je	.LBB0_7
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=2
	movq	%rbp, %rbx
	addq	$32, %rbx
	movq	16(%rbp), %r13
	.p2align	4, 0x90
.LBB0_9:                                #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13), %r13
	movzbl	32(%r13), %eax
	testl	%eax, %eax
	je	.LBB0_9
# BB#10:                                #   in Loop: Header=BB0_6 Depth=2
	cmpl	28(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB0_20
# BB#11:                                #   in Loop: Header=BB0_6 Depth=2
	callq	Image
	movq	%rax, %rbp
	movl	$12, %edi
	movl	$14, %esi
	movl	$.L.str.4, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
	movq	24(%r12), %r12
	cmpq	%r15, %r12
	movq	%r13, %rcx
	jne	.LBB0_6
	jmp	.LBB0_57
	.p2align	4, 0x90
.LBB0_12:                               # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%r15, %r14
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph.split.us
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_14 Depth 3
                                        #       Child Loop BB0_16 Depth 3
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB0_14:                               #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %edi
	testl	%edi, %edi
	je	.LBB0_14
# BB#15:                                #   in Loop: Header=BB0_13 Depth=2
	movq	%rbp, %rbx
	addq	$32, %rbx
	movq	24(%rbp), %r15
	.p2align	4, 0x90
.LBB0_16:                               #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15), %r15
	movzbl	32(%r15), %eax
	testl	%eax, %eax
	je	.LBB0_16
# BB#17:                                #   in Loop: Header=BB0_13 Depth=2
	cmpl	28(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_13 Depth=2
	callq	Image
	movq	%rax, %rbp
	movl	$12, %edi
	movl	$14, %esi
	movl	$.L.str.4, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
	movq	24(%r12), %r12
	cmpq	%r14, %r12
	movq	%r15, %r13
	jne	.LBB0_13
	jmp	.LBB0_56
.LBB0_19:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%r13, %rcx
	movq	%r15, %r13
	movq	%r14, %r15
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB0_20:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpb	$1, 41(%r13)
	je	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, %r14
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%r14, %rcx
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB0_22:                               #   in Loop: Header=BB0_4 Depth=1
	testq	%rcx, %rcx
	movl	44(%rsp), %r8d          # 4-byte Reload
	je	.LBB0_52
# BB#23:                                #   in Loop: Header=BB0_4 Depth=1
	leaq	16(%r13), %rax
	leaq	24(%r13), %rdx
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	cmoveq	%rdx, %rax
	movq	(%rax), %rdi
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB0_24:                               #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	movzbl	32(%rax), %edx
	testb	%dl, %dl
	je	.LBB0_24
# BB#25:                                # %.preheader54.i
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB0_28
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_26:                               #   in Loop: Header=BB0_28 Depth=2
	movq	16(%rax), %rdi
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB0_27:                               #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rax
	movzbl	32(%rax), %edx
	testb	%dl, %dl
	je	.LBB0_27
.LBB0_28:                               # %.preheader54.split.i
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_27 Depth 3
	movzbl	%dl, %esi
	cmpl	%r8d, %esi
	jne	.LBB0_30
# BB#29:                                #   in Loop: Header=BB0_28 Depth=2
	movq	(%rdi), %rsi
	cmpb	$0, 32(%rsi)
	je	.LBB0_37
.LBB0_30:                               #   in Loop: Header=BB0_28 Depth=2
	cmpq	%rax, 24(%rax)
	jne	.LBB0_26
	jmp	.LBB0_37
	.p2align	4, 0x90
.LBB0_31:                               #   in Loop: Header=BB0_32 Depth=2
	movq	%rsi, %rdi
.LBB0_32:                               # %.preheader54.split.us.i
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_36 Depth 3
	movzbl	%dl, %esi
	cmpl	%r8d, %esi
	jne	.LBB0_34
# BB#33:                                #   in Loop: Header=BB0_32 Depth=2
	movq	(%rdi), %rsi
	cmpb	$0, 32(%rsi)
	je	.LBB0_37
.LBB0_34:                               #   in Loop: Header=BB0_32 Depth=2
	movq	24(%rax), %rsi
	cmpq	%rax, %rsi
	je	.LBB0_37
# BB#35:                                # %.preheader63.i.preheader
                                        #   in Loop: Header=BB0_32 Depth=2
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB0_36:                               # %.preheader63.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_32 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rax
	movzbl	32(%rax), %edx
	testb	%dl, %dl
	je	.LBB0_36
	jmp	.LBB0_31
	.p2align	4, 0x90
.LBB0_37:                               # %.critedge.i
                                        #   in Loop: Header=BB0_4 Depth=1
	movzbl	%dl, %esi
	cmpl	%r8d, %esi
	jne	.LBB0_39
# BB#38:                                #   in Loop: Header=BB0_4 Depth=1
	movq	(%rdi), %r14
	cmpb	$0, 32(%r14)
	je	.LBB0_46
.LBB0_39:                               # %thread-pre-split.i
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpb	$8, %dl
	jne	.LBB0_51
# BB#40:                                #   in Loop: Header=BB0_4 Depth=1
	movzbl	43(%rax), %eax
	andl	$1, %eax
	cmpl	(%rsp), %eax            # 4-byte Folded Reload
	jne	.LBB0_51
# BB#41:                                #   in Loop: Header=BB0_4 Depth=1
	movq	(%rdi), %r14
	cmpb	$0, 32(%r14)
	jne	.LBB0_51
	.p2align	4, 0x90
.LBB0_42:                               # %.preheader51.i
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r14), %r14
	movzbl	32(%r14), %eax
	testb	%al, %al
	je	.LBB0_42
# BB#43:                                # %.preheader51.i
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpb	$1, %al
	je	.LBB0_45
# BB#44:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.47, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB0_45:                               # %.loopexit52.i
                                        #   in Loop: Header=BB0_4 Depth=1
	orb	$-128, 44(%r14)
	testq	%r14, %r14
	jne	.LBB0_50
	jmp	.LBB0_51
	.p2align	4, 0x90
.LBB0_46:                               # %.preheader.i
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r14), %r14
	movzbl	32(%r14), %eax
	testb	%al, %al
	je	.LBB0_46
# BB#47:                                # %.preheader.i
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpb	$1, %al
	je	.LBB0_49
# BB#48:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.46, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB0_49:                               # %FindSpannerGap.exit
                                        #   in Loop: Header=BB0_4 Depth=1
	testq	%r14, %r14
	je	.LBB0_51
.LBB0_50:                               #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	56(%rcx,%rax,4), %edi
	movl	48(%r13,%rax,4), %esi
	leaq	56(%r13,%rax,4), %rbx
	movl	56(%r13,%rax,4), %edx
	addq	$44, %r14
	movq	%r14, %rcx
	callq	MinGap
	addl	%eax, 4(%rsp)           # 4-byte Folded Spill
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_53
	.p2align	4, 0x90
.LBB0_51:                               # %FindSpannerGap.exit.thread
                                        #   in Loop: Header=BB0_4 Depth=1
	addq	$32, %rbp
	movzbl	(%rbx), %edi
	movq	%rcx, %r14
	callq	Image
	movq	%rax, %rbx
	movl	$12, %edi
	movl	$13, %esi
	movl	$.L.str.3, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	%rbx, %r9
	callq	Error
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	4(%rsp), %ecx           # 4-byte Reload
	addl	56(%r14,%rax,4), %ecx
	addl	48(%r13,%rax,4), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	leaq	56(%r13,%rax,4), %rbx
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_53
.LBB0_52:                               #   in Loop: Header=BB0_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	48(%r13,%rax,4), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	leaq	56(%r13,%rax,4), %rbx
.LBB0_53:                               # %.outer.backedge
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	(%rsp), %ebp            # 4-byte Reload
	movl	(%rbx), %edx
	movq	24(%r12), %r12
	cmpq	%r15, %r12
	jne	.LBB0_4
	jmp	.LBB0_58
.LBB0_55:
                                        # implicit-def: %EAX
	movl	%eax, 4(%rsp)           # 4-byte Spill
                                        # implicit-def: %EDX
	jmp	.LBB0_58
.LBB0_56:
	movq	%r14, %r15
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB0_57:                               # %.outer._crit_edge
	movl	8(%rsp), %edx           # 4-byte Reload
.LBB0_58:                               # %.outer._crit_edge
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, (%rax)
	movl	%edx, (%r14)
	movl	$8388607, 64(%r15)      # imm = 0x7FFFFF
	addl	%ecx, %edx
	movl	%edx, 68(%r15)
	movl	$8388607, 72(%r15)      # imm = 0x7FFFFF
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	SpannerAvailableSpace, .Lfunc_end0-SpannerAvailableSpace
	.cfi_endproc

	.globl	MinSize
	.p2align	4, 0x90
	.type	MinSize,@function
MinSize:                                # @MinSize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$648, %rsp              # imm = 0x288
.Lcfi19:
	.cfi_def_cfa_offset 704
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r13d
	movq	%rdi, %r12
	movzbl	32(%r12), %esi
	movl	%esi, %eax
	addb	$-2, %al
	cmpb	$97, %al
	ja	.LBB1_81
# BB#1:
	leaq	32(%r12), %r15
	movzbl	%al, %eax
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_2:
	movq	8(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_3
	jmp	.LBB1_190
.LBB1_4:
	movq	(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_5
	jmp	.LBB1_190
.LBB1_6:
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	%r14, 40(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	jne	.LBB1_421
# BB#7:
	cmpb	$43, %sil
	je	.LBB1_42
	jmp	.LBB1_421
.LBB1_8:
	cmpl	$1, %r13d
	jne	.LBB1_482
# BB#9:
	movzbl	41(%r12), %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_209
# BB#10:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_210
.LBB1_11:
	testl	%r13d, %r13d
	jne	.LBB1_483
# BB#12:
	movq	%r12, %rdi
	callq	FontWordSize
	jmp	.LBB1_483
.LBB1_13:
	cmpb	$13, %sil
	sete	%al
	testl	%r13d, %r13d
	sete	%cl
	cmpb	%al, %cl
	je	.LBB1_15
# BB#14:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_15:
	movq	8(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_16:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_16
	jmp	.LBB1_190
.LBB1_17:
	movq	(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_18:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_18
	jmp	.LBB1_190
.LBB1_19:
	movslq	%r13d, %rax
	movl	$0, 56(%r12,%rax,4)
	movl	$0, 48(%r12,%rax,4)
	movq	8(%r12), %rcx
	.p2align	4, 0x90
.LBB1_20:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rcx), %rcx
	cmpb	$0, 32(%rcx)
	je	.LBB1_20
# BB#21:
	movl	$0, 56(%rcx,%rax,4)
	movl	$0, 48(%rcx,%rax,4)
	jmp	.LBB1_483
.LBB1_22:
	movq	8(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_23:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_23
# BB#24:
	movl	%r13d, %esi
	movq	%r14, %rdx
	callq	MinSize
	movq	%rax, %rbx
	testl	%r13d, %r13d
	sete	%al
	cmpb	$28, (%r15)
	sete	%cl
	xorb	%al, %cl
	je	.LBB1_192
# BB#25:
	movslq	%r13d, %rax
	movl	48(%rbx,%rax,4), %ecx
	movl	%ecx, 48(%r12,%rax,4)
	movl	56(%rbx,%rax,4), %ecx
	movl	%ecx, 56(%r12,%rax,4)
	jmp	.LBB1_483
.LBB1_26:
	movq	8(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_27:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_27
# BB#28:
	movl	%r13d, %esi
	movq	%r14, %rdx
	callq	MinSize
	testl	%r13d, %r13d
	sete	%cl
	cmpb	$30, (%r15)
	sete	%dl
	xorb	%cl, %dl
	je	.LBB1_443
# BB#29:
	movslq	%r13d, %rcx
	movl	48(%rax,%rcx,4), %edx
	movl	%edx, 48(%r12,%rcx,4)
	movl	56(%rax,%rcx,4), %eax
	leaq	56(%r12), %rdx
	movl	%eax, (%rdx,%rcx,4)
	jmp	.LBB1_483
.LBB1_30:
	movq	8(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_31:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_31
# BB#32:
	movl	%r13d, %esi
	movq	%r14, %rdx
	callq	MinSize
	testl	%r13d, %r13d
	sete	%cl
	cmpb	$32, (%r15)
	sete	%dl
	xorb	%cl, %dl
	je	.LBB1_193
# BB#33:
	movslq	%r13d, %rcx
	movl	48(%rax,%rcx,4), %edx
	movl	%edx, 48(%r12,%rcx,4)
	movl	56(%rax,%rcx,4), %edx
	leaq	56(%r12), %rax
	jmp	.LBB1_194
.LBB1_34:
	movq	8(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_35:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_35
# BB#36:
	movl	%r13d, %esi
	movq	%r14, %rdx
	callq	MinSize
	movslq	%r13d, %rcx
	movl	48(%rax,%rcx,4), %edx
	movl	%edx, 48(%r12,%rcx,4)
	movl	56(%rax,%rcx,4), %eax
	movl	%eax, 56(%r12,%rcx,4)
	cmpl	$1, %r13d
	jne	.LBB1_483
# BB#37:
	movzbl	zz_lengths+138(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_228
# BB#38:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_229
.LBB1_39:
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	%r14, 40(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	jne	.LBB1_421
# BB#40:
	addb	$-42, %sil
	cmpb	$3, %sil
	jb	.LBB1_42
# BB#41:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.48, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_42:                               # %.thread2224
	movq	8(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_43:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %r15
	leaq	16(%r15), %rax
	cmpb	$0, 32(%r15)
	je	.LBB1_43
# BB#44:
	movq	24(%r15), %rax
	cmpq	16(%r15), %rax
	je	.LBB1_46
# BB#45:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.49, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	24(%r15), %rax
.LBB1_46:
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_48
# BB#47:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_48:
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_50
# BB#49:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_50:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	48(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %edi
	cmpl	$44, %edi
	je	.LBB1_52
# BB#51:
	cmpb	$42, %dil
	jne	.LBB1_197
.LBB1_52:
	movq	24(%r12), %rax
	.p2align	4, 0x90
.LBB1_53:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB1_53
# BB#54:
	cmpb	$16, %cl
	jne	.LBB1_199
# BB#55:
	movzbl	zz_lengths+13(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_246
# BB#56:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_247
.LBB1_57:
	cmpl	$1, %r13d
	je	.LBB1_483
# BB#58:
	movl	%r13d, 32(%rsp)         # 4-byte Spill
	movq	%r12, %r13
	movq	8(%r12), %rbx
	.p2align	4, 0x90
.LBB1_59:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB1_59
# BB#60:
	leaq	32(%rbx), %rcx
	leaq	64(%rbx), %rdi
	leaq	80(%rsp), %rdx
	leaq	76(%rsp), %r8
	callq	OpenIncGraphicFile
	movq	%rax, %r14
	testq	%r14, %r14
	sete	%cl
	movl	$1, %r12d
	leaq	128(%rsp), %rbp
                                        # implicit-def: %EAX
	movq	%rax, 40(%rsp)          # 8-byte Spill
                                        # implicit-def: %EAX
	movq	%rax, 48(%rsp)          # 8-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 16(%rsp)          # 4-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 64(%rsp)          # 4-byte Spill
	testl	%r12d, %r12d
	jne	.LBB1_70
	jmp	.LBB1_61
.LBB1_75:
	movq	80(%r12), %rax
	movzwl	41(%rax), %eax
	testb	$64, %al
	je	.LBB1_77
# BB#76:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_77:
	cmpl	$1, %r13d
	jne	.LBB1_200
# BB#78:
	movq	80(%r12), %rax
	movzwl	41(%rax), %ecx
	movzbl	43(%rax), %eax
	shll	$16, %eax
	orl	%ecx, %eax
	testb	$2, %ah
	jne	.LBB1_325
# BB#79:
	testb	$4, %ah
	jne	.LBB1_446
# BB#80:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB1_482
.LBB1_81:
	movq	no_fpos(%rip), %rbx
	movl	%esi, %edi
	callq	Image
	movq	%rax, (%rsp)
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.42, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.43, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	jmp	.LBB1_483
.LBB1_82:
	cmpl	$1, %r13d
	jne	.LBB1_482
# BB#83:
	movzbl	zz_lengths+135(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_331
# BB#84:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	movb	$-121, 32(%rbp)
	jmp	.LBB1_211
.LBB1_85:
	cmpl	$1, %r13d
	jne	.LBB1_201
# BB#86:
	movzbl	zz_lengths+5(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_332
# BB#87:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_333
.LBB1_88:
	leaq	8(%r12), %rax
	testl	%r13d, %r13d
	cmovneq	%r12, %rax
	movq	(%rax), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_89:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_89
	jmp	.LBB1_190
.LBB1_90:
	cmpl	$1, %r13d
	je	.LBB1_92
# BB#91:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.27, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_92:
	cmpb	$0, 41(%r12)
	jne	.LBB1_483
# BB#93:
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movl	%r13d, 32(%rsp)         # 4-byte Spill
	movq	8(%r12), %rbp
	cmpq	%r12, %rbp
	jne	.LBB1_95
# BB#94:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.28, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%r12), %rbp
.LBB1_95:
	movl	$0, 28(%rsp)
	movl	$0, 36(%rsp)
	movq	%r12, %r13
	cmpq	%r12, %rbp
	movslq	32(%rsp), %r14          # 4-byte Folded Reload
	je	.LBB1_343
# BB#96:                                # %.preheader1733.preheader
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
.LBB1_97:                               # %.preheader1733
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_98 Depth 2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB1_98:                               #   Parent Loop BB1_97 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_98
# BB#99:                                #   in Loop: Header=BB1_97 Depth=1
	cmpb	$1, %al
	jne	.LBB1_101
# BB#100:                               #   in Loop: Header=BB1_97 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.29, %r9d
	xorl	%eax, %eax
	callq	Error
	movb	32(%rbx), %al
.LBB1_101:                              # %.loopexit1734
                                        #   in Loop: Header=BB1_97 Depth=1
	addb	$-43, %al
	cmpb	$4, %al
	jb	.LBB1_103
# BB#102:                               #   in Loop: Header=BB1_97 Depth=1
	movq	%rbx, %rdi
	movl	32(%rsp), %esi          # 4-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	callq	MinSize
	movl	48(%rax,%r14,4), %ecx
	cmpl	%ecx, %r12d
	cmovll	%ecx, %r12d
	movl	%r12d, 36(%rsp)
	movl	56(%rax,%r14,4), %eax
	cmpl	%eax, %r15d
	cmovll	%eax, %r15d
	movl	%r15d, 28(%rsp)
.LBB1_103:                              #   in Loop: Header=BB1_97 Depth=1
	movq	8(%rbp), %rbp
	cmpq	%r13, %rbp
	jne	.LBB1_97
	jmp	.LBB1_344
.LBB1_104:
	testl	%r13d, %r13d
	je	.LBB1_106
# BB#105:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.24, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_106:
	cmpb	$0, 41(%r12)
	jne	.LBB1_483
# BB#107:
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movl	%r13d, 32(%rsp)         # 4-byte Spill
	movq	8(%r12), %rbp
	cmpq	%r12, %rbp
	jne	.LBB1_109
# BB#108:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.25, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%r12), %rbp
.LBB1_109:
	movl	$0, 28(%rsp)
	movl	$0, 36(%rsp)
	movq	%r12, %r13
	cmpq	%r12, %rbp
	movslq	32(%rsp), %r14          # 4-byte Folded Reload
	je	.LBB1_354
# BB#110:                               # %.preheader1737.preheader
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
.LBB1_111:                              # %.preheader1737
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_112 Depth 2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB1_112:                              #   Parent Loop BB1_111 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_112
# BB#113:                               #   in Loop: Header=BB1_111 Depth=1
	cmpb	$1, %al
	jne	.LBB1_115
# BB#114:                               #   in Loop: Header=BB1_111 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
	movb	32(%rbx), %al
.LBB1_115:                              # %.loopexit1738
                                        #   in Loop: Header=BB1_111 Depth=1
	cmpb	$46, %al
	ja	.LBB1_117
# BB#116:                               # %.loopexit1738
                                        #   in Loop: Header=BB1_111 Depth=1
	movzbl	%al, %eax
	movabsq	$127543348822016, %rcx  # imm = 0x740000000000
	btq	%rax, %rcx
	jb	.LBB1_118
.LBB1_117:                              #   in Loop: Header=BB1_111 Depth=1
	movq	%rbx, %rdi
	movl	32(%rsp), %esi          # 4-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	callq	MinSize
	movl	48(%rax,%r14,4), %ecx
	cmpl	%ecx, %r12d
	cmovll	%ecx, %r12d
	movl	%r12d, 36(%rsp)
	movl	56(%rax,%r14,4), %eax
	cmpl	%eax, %r15d
	cmovll	%eax, %r15d
	movl	%r15d, 28(%rsp)
.LBB1_118:                              #   in Loop: Header=BB1_111 Depth=1
	movq	8(%rbp), %rbp
	cmpq	%r13, %rbp
	jne	.LBB1_111
	jmp	.LBB1_355
.LBB1_119:
	movb	68(%r12), %al
	andb	$12, %al
	cmpb	$4, %al
	jne	.LBB1_637
# BB#120:                               # %.preheader1757
	movq	8(%r12), %rbx
	cmpq	%r12, %rbx
	je	.LBB1_637
# BB#121:                               # %.preheader1755.preheader
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movq	%r12, %r14
.LBB1_122:                              # %.preheader1755
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_123 Depth 2
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB1_123:                              #   Parent Loop BB1_122 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB1_123
# BB#124:                               #   in Loop: Header=BB1_122 Depth=1
	cmpb	$17, %al
	jne	.LBB1_138
# BB#125:                               #   in Loop: Header=BB1_122 Depth=1
	movq	8(%rbp), %r15
	cmpq	%rbp, %r15
	je	.LBB1_130
# BB#126:                               #   in Loop: Header=BB1_122 Depth=1
	cmpb	$0, 32(%r15)
	je	.LBB1_128
# BB#127:                               #   in Loop: Header=BB1_122 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_128:                              #   in Loop: Header=BB1_122 Depth=1
	movq	%r15, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r15), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%r15, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_130
# BB#129:                               #   in Loop: Header=BB1_122 Depth=1
	movq	(%rbx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r15), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_130:                              #   in Loop: Header=BB1_122 Depth=1
	movq	24(%rbp), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_132
# BB#131:                               #   in Loop: Header=BB1_122 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_133
.LBB1_132:                              #   in Loop: Header=BB1_122 Depth=1
	xorl	%ecx, %ecx
.LBB1_133:                              #   in Loop: Header=BB1_122 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_135
# BB#134:                               #   in Loop: Header=BB1_122 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_135:                              #   in Loop: Header=BB1_122 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_137
# BB#136:                               #   in Loop: Header=BB1_122 Depth=1
	callq	DisposeObject
.LBB1_137:                              #   in Loop: Header=BB1_122 Depth=1
	movq	(%r14), %r14
.LBB1_138:                              # %.loopexit1756
                                        #   in Loop: Header=BB1_122 Depth=1
	movq	8(%r14), %r14
	movq	8(%r14), %rbx
	cmpq	%r12, %rbx
	jne	.LBB1_122
# BB#139:                               # %._crit_edge1943
	movq	8(%r12), %rbx
	cmpq	%r12, %rbx
	je	.LBB1_463
# BB#140:                               # %.preheader1754.preheader
	movq	40(%rsp), %r14          # 8-byte Reload
.LBB1_141:                              # %.preheader1754
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_142 Depth 2
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB1_142:                              #   Parent Loop BB1_141 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.LBB1_142
# BB#143:                               #   in Loop: Header=BB1_141 Depth=1
	cmpb	$1, %al
	je	.LBB1_147
# BB#144:                               #   in Loop: Header=BB1_141 Depth=1
	cmpb	$9, %al
	jne	.LBB1_146
# BB#145:                               #   in Loop: Header=BB1_141 Depth=1
	callq	SplitIsDefinite
	testl	%eax, %eax
	je	.LBB1_147
	jmp	.LBB1_520
.LBB1_146:                              #   in Loop: Header=BB1_141 Depth=1
	addb	$-9, %al
	cmpb	$91, %al
	jb	.LBB1_520
.LBB1_147:                              # %.critedge
                                        #   in Loop: Header=BB1_141 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.LBB1_141
	jmp	.LBB1_637
.LBB1_148:
	movq	8(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_149:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_149
# BB#150:
	movl	%r13d, %esi
	movq	%r14, %rdx
	callq	MinSize
	testl	%r13d, %r13d
	jne	.LBB1_191
# BB#151:
	leaq	64(%r12), %rbx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	BreakObject
	movq	%rax, %rbp
	movl	48(%rbp), %eax
	cmpl	64(%r12), %eax
	jg	.LBB1_154
# BB#152:
	movl	56(%rbp), %ecx
	leal	(%rcx,%rax), %edx
	cmpl	68(%r12), %edx
	jg	.LBB1_154
# BB#153:
	cmpl	72(%r12), %ecx
	jle	.LBB1_155
.LBB1_154:                              # %._crit_edge2159
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	48(%rbp), %eax
.LBB1_155:
	leaq	48(%r12), %rdi
	movl	%eax, 48(%r12)
	movl	56(%rbp), %eax
	leaq	56(%r12), %rsi
	movl	%eax, 56(%r12)
	movq	%rbx, %rdx
	callq	EnlargeToConstraint
	jmp	.LBB1_483
.LBB1_156:
	movq	8(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_157:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_157
# BB#158:
	movl	%r13d, %esi
	movq	%r14, %rdx
	callq	MinSize
	movl	%r13d, %ecx
	movq	%rax, %r13
	movslq	%ecx, %rbx
	movl	48(%r13,%rbx,4), %eax
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	cmpl	$1, %ecx
	jne	.LBB1_202
# BB#159:
	leaq	64(%r12), %rdx
	cmpl	64(%r12), %eax
	jle	.LBB1_327
# BB#160:                               # %._crit_edge2086
	movl	68(%r12), %edi
	leaq	56(%r13,%rbx,4), %r14
	jmp	.LBB1_329
.LBB1_161:
	movq	8(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_162:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_162
# BB#163:
	movl	%r13d, %esi
	movq	%r14, %rdx
	callq	MinSize
	movslq	%r13d, %rcx
	movl	48(%rax,%rcx,4), %edx
	testl	%r13d, %r13d
	je	.LBB1_230
# BB#164:
	imull	72(%r12), %edx
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$25, %esi
	addl	%edx, %esi
	sarl	$7, %esi
	movl	%esi, 48(%r12,%rcx,4)
	movl	72(%r12), %edx
	imull	56(%rax,%rcx,4), %edx
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$25, %eax
	addl	%edx, %eax
	sarl	$7, %eax
	movl	%eax, 56(%r12,%rcx,4)
	orb	$1, 42(%r12)
	jmp	.LBB1_483
.LBB1_165:
	movq	(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_166:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_166
# BB#167:
	movl	%r13d, %esi
	movq	%r14, %rdx
	callq	MinSize
	movslq	%r13d, %rbx
	movl	48(%rax,%rbx,4), %ecx
	movl	%ecx, 48(%r12,%rbx,4)
	movl	56(%rax,%rbx,4), %r15d
	movl	%r15d, 56(%r12,%rbx,4)
	testl	%r13d, %r13d
	jne	.LBB1_483
# BB#168:
	movq	8(%r12), %rdx
	addq	$16, %rdx
	.p2align	4, 0x90
.LBB1_169:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rax
	movzbl	32(%rax), %ecx
	leaq	16(%rax), %rdx
	testb	%cl, %cl
	je	.LBB1_169
	jmp	.LBB1_172
	.p2align	4, 0x90
.LBB1_170:                              #   in Loop: Header=BB1_172 Depth=1
	movq	8(%rax), %rdx
	addq	$16, %rdx
	.p2align	4, 0x90
.LBB1_171:                              #   Parent Loop BB1_172 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	leaq	16(%rax), %rdx
	je	.LBB1_171
.LBB1_172:                              # %.preheader1746
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_171 Depth 2
	cmpb	$17, %cl
	je	.LBB1_170
# BB#173:                               # %.preheader1746
	addb	$-11, %cl
	cmpb	$2, %cl
	jae	.LBB1_448
# BB#174:
	movb	64(%rax), %r14b
	jmp	.LBB1_449
.LBB1_175:
	movq	8(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_176:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_176
# BB#177:
	testl	%r13d, %r13d
	je	.LBB1_233
# BB#178:
	movq	88(%r12), %rbp
	movq	8(%rbp), %rbx
	cmpq	%rbp, %rbx
	je	.LBB1_185
# BB#179:
	movq	(%r14), %r14
	cmpb	$0, 32(%rbx)
	je	.LBB1_181
# BB#180:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_181:
	movq	%rbx, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_183
# BB#182:
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_183:
	movq	%rbx, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB1_185
# BB#184:
	movq	(%r14), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_185:
	movq	88(%r12), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB1_483
.LBB1_186:
	movq	8(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_187:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_187
# BB#188:
	movl	%r13d, %esi
	movq	%r14, %rdx
	callq	MinSize
	movq	(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_189:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_189
.LBB1_190:
	movl	%r13d, %esi
	movq	%r14, %rdx
	callq	MinSize
.LBB1_191:
	movslq	%r13d, %rcx
	movl	48(%rax,%rcx,4), %edx
	movl	%edx, 48(%r12,%rcx,4)
	movl	56(%rax,%rcx,4), %eax
	movl	%eax, 56(%r12,%rcx,4)
	jmp	.LBB1_483
.LBB1_192:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movl	%r13d, %edx
	callq	FindShift
	movl	%eax, 28(%rsp)
	movslq	%r13d, %rcx
	xorl	%edx, %edx
	movl	48(%rbx,%rcx,4), %esi
	addl	%eax, %esi
	movl	$0, %edi
	cmovnsl	%esi, %edi
	cmpl	$8388607, %esi          # imm = 0x7FFFFF
	movl	$8388607, %esi          # imm = 0x7FFFFF
	cmovgl	%esi, %edi
	movl	%edi, 48(%r12,%rcx,4)
	movl	56(%rbx,%rcx,4), %edi
	subl	%eax, %edi
	cmovnsl	%edi, %edx
	cmpl	$8388607, %edi          # imm = 0x7FFFFF
	cmovgl	%esi, %edx
	movl	%edx, 56(%r12,%rcx,4)
	jmp	.LBB1_483
.LBB1_193:
	leaq	48(%r12), %rax
	movslq	%r13d, %rcx
	movl	$0, 56(%r12,%rcx,4)
	xorl	%edx, %edx
.LBB1_194:
	movl	%edx, (%rax,%rcx,4)
	cmpl	$1, %r13d
	jne	.LBB1_483
# BB#195:
	movzbl	zz_lengths+137(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_235
# BB#196:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_236
.LBB1_197:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_249
# BB#198:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_250
.LBB1_199:
	callq	Image
	movq	%rax, %rbp
	movl	$12, %edi
	movl	$10, %esi
	movl	$.L.str.50, %edx
	jmp	.LBB1_313
.LBB1_200:
	andb	$-25, 42(%r12)
	jmp	.LBB1_482
.LBB1_201:
	andb	$-25, 42(%r12)
	movq	%r12, %rbp
	jmp	.LBB1_519
.LBB1_202:
	movl	%eax, 48(%r12,%rbx,4)
	movl	56(%r13,%rbx,4), %eax
	movl	%eax, 56(%r12,%rbx,4)
	movl	32(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB1_483
.LBB1_209:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
	movb	41(%r12), %al
.LBB1_210:
	movb	%al, 32(%rbp)
.LBB1_211:
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	%r12, 80(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_213
# BB#212:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_214
.LBB1_213:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_214:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_217
# BB#215:
	testq	%rax, %rax
	je	.LBB1_217
# BB#216:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_217:
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_219
# BB#218:
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_219:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_221
# BB#220:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_222
.LBB1_221:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_222:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%r14), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_225
# BB#223:
	testq	%rcx, %rcx
	je	.LBB1_225
# BB#224:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_225:
	testq	%rbp, %rbp
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_482
# BB#226:
	testq	%rax, %rax
	je	.LBB1_482
# BB#227:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	jmp	.LBB1_481
.LBB1_228:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_229:
	movb	$-118, 32(%rbx)
	jmp	.LBB1_237
.LBB1_230:
	imull	64(%r12), %edx
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$25, %esi
	addl	%edx, %esi
	sarl	$7, %esi
	movl	%esi, 48(%r12,%rcx,4)
	movl	64(%r12), %edx
	imull	56(%rax,%rcx,4), %edx
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$25, %eax
	addl	%edx, %eax
	sarl	$7, %eax
	movl	%eax, 56(%r12,%rcx,4)
	cmpl	$0, 64(%r12)
	jne	.LBB1_483
# BB#231:
	movzbl	zz_lengths+136(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_531
# BB#232:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_532
.LBB1_233:
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	MinSize
	movq	%rax, %rbx
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_365
# BB#234:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_366
.LBB1_235:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_236:
	movb	$-119, 32(%rbx)
.LBB1_237:
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movq	%r12, 80(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_239
# BB#238:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_240
.LBB1_239:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_240:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%r14), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_243
# BB#241:
	testq	%rcx, %rcx
	je	.LBB1_243
# BB#242:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_243:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_483
# BB#244:
	testq	%rax, %rax
	je	.LBB1_483
# BB#245:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB1_483
.LBB1_246:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_247:
	movb	$13, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movzwl	34(%r12), %eax
	movw	%ax, 34(%rbp)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r12), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbp), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbp)
	andl	36(%r12), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	andb	$-65, 42(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_254
# BB#248:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_255
.LBB1_249:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_250:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%rax, %rax
	movq	%r12, 56(%rsp)          # 8-byte Spill
	je	.LBB1_252
# BB#251:
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_252:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_304
# BB#253:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB1_304
.LBB1_254:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_255:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_257
# BB#256:
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_257:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_260
# BB#258:
	testq	%rax, %rax
	je	.LBB1_260
# BB#259:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_260:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_262
# BB#261:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_263
.LBB1_262:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_263:
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_266
# BB#264:
	testq	%rax, %rax
	je	.LBB1_266
# BB#265:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_266:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_269
# BB#267:
	testq	%rax, %rax
	je	.LBB1_269
# BB#268:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_269:
	movq	16(%r12), %rbx
	.p2align	4, 0x90
.LBB1_270:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_270
# BB#271:
	cmpb	$15, %al
	je	.LBB1_274
# BB#272:
	movq	48(%rsp), %r14          # 8-byte Reload
	movzbl	(%r14), %edi
	callq	Image
	movq	%rax, %r9
	movl	$12, %edi
	movl	$11, %esi
	movl	$.L.str.51, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
	cmpb	$15, 32(%rbx)
	je	.LBB1_274
# BB#273:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.52, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_274:                              # %.thread.i
	movl	$-532676608, %eax       # imm = 0xE0400000
	andl	40(%rbp), %eax
	orl	$1, %eax
	movl	%eax, 40(%rbp)
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	16(%r12), %rax
	movq	8(%rax), %r12
	cmpq	%rbx, %r12
	je	.LBB1_282
# BB#275:                               # %.lr.ph306.i.preheader
	movq	%r12, %rax
	movq	%r12, 16(%rsp)          # 8-byte Spill
.LBB1_276:                              # %.lr.ph306.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_278 Depth 2
	leaq	16(%rax), %rcx
	jmp	.LBB1_278
	.p2align	4, 0x90
.LBB1_277:                              #   in Loop: Header=BB1_278 Depth=2
	addq	$16, %rcx
.LBB1_278:                              #   Parent Loop BB1_276 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rcx
	movzbl	32(%rcx), %edx
	cmpq	$46, %rdx
	ja	.LBB1_281
# BB#279:                               #   in Loop: Header=BB1_278 Depth=2
	jmpq	*.LJTI1_1(,%rdx,8)
.LBB1_280:                              #   in Loop: Header=BB1_276 Depth=1
	movq	8(%rax), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
.LBB1_281:                              # %.loopexit284.loopexit.i
                                        #   in Loop: Header=BB1_276 Depth=1
	addq	$8, %rax
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.LBB1_276
	jmp	.LBB1_283
.LBB1_282:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
.LBB1_283:                              # %.loopexit285.i
	cmpq	16(%rsp), %r12          # 8-byte Folded Reload
	je	.LBB1_304
# BB#284:                               # %.preheader282.lr.ph.i
	movl	%r13d, 32(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB1_285:                              # %.preheader282.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_286 Depth 2
	movq	%r12, %r14
	.p2align	4, 0x90
.LBB1_286:                              #   Parent Loop BB1_285 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r14), %r14
	cmpb	$0, 32(%r14)
	je	.LBB1_286
# BB#287:                               #   in Loop: Header=BB1_285 Depth=1
	movzbl	zz_lengths+45(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_289
# BB#288:                               #   in Loop: Header=BB1_285 Depth=1
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_290
.LBB1_289:                              #   in Loop: Header=BB1_285 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_290:                              #   in Loop: Header=BB1_285 Depth=1
	movb	$45, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	34(%r14), %eax
	movw	%ax, 34(%rbx)
	movl	36(%r14), %eax
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %eax
	movl	36(%rbx), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movl	36(%r14), %ecx
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movq	%r14, zz_hold(%rip)
	movq	24(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB1_294
# BB#291:                               #   in Loop: Header=BB1_285 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r14), %rcx
	movq	%rax, 24(%rcx)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%r13b
	je	.LBB1_295
# BB#292:                               #   in Loop: Header=BB1_285 Depth=1
	testq	%rax, %rax
	je	.LBB1_295
# BB#293:                               #   in Loop: Header=BB1_285 Depth=1
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	xorl	%r13d, %r13d
	jmp	.LBB1_295
.LBB1_294:                              # %.thread351.i
                                        #   in Loop: Header=BB1_285 Depth=1
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%r13b
.LBB1_295:                              #   in Loop: Header=BB1_285 Depth=1
	movq	%r14, %rdi
	callq	DisposeObject
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_297
# BB#296:                               #   in Loop: Header=BB1_285 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_298
.LBB1_297:                              #   in Loop: Header=BB1_285 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_298:                              #   in Loop: Header=BB1_285 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	sete	%cl
	orb	%cl, %r13b
	jne	.LBB1_300
# BB#299:                               #   in Loop: Header=BB1_285 Depth=1
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_300:                              #   in Loop: Header=BB1_285 Depth=1
	testq	%rbp, %rbp
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	movl	32(%rsp), %r13d         # 4-byte Reload
	je	.LBB1_303
# BB#301:                               #   in Loop: Header=BB1_285 Depth=1
	testq	%rax, %rax
	je	.LBB1_303
# BB#302:                               #   in Loop: Header=BB1_285 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_303:                              #   in Loop: Header=BB1_285 Depth=1
	movl	40(%rbp), %eax
	leal	1(%rax), %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	andl	$-4096, %eax            # imm = 0xF000
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movq	8(%r12), %r12
	cmpq	16(%rsp), %r12          # 8-byte Folded Reload
	jne	.LBB1_285
.LBB1_304:                              # %.loopexit283.i
	movq	48(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %edi
	movl	%edi, %eax
	addb	$-43, %al
	cmpb	$1, %al
	ja	.LBB1_310
# BB#305:
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	16(%r12), %rax
	.p2align	4, 0x90
.LBB1_306:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB1_306
# BB#307:
	cmpb	$15, %cl
	jne	.LBB1_312
# BB#308:
	movzbl	zz_lengths+14(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r14
	testq	%r14, %r14
	je	.LBB1_367
# BB#309:
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_368
.LBB1_310:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	movq	56(%rsp), %r12          # 8-byte Reload
	je	.LBB1_319
# BB#311:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_320
.LBB1_312:
	callq	Image
	movq	%rax, %rbp
	movl	$12, %edi
	movl	$12, %esi
	movl	$.L.str.53, %edx
.LBB1_313:                              # %BuildSpanner.exit
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
	movl	$11, %edi
	movl	$.L.str.8, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	movq	%r12, zz_hold(%rip)
	movq	24(%r12), %rcx
	cmpq	%r12, %rcx
	je	.LBB1_317
# BB#314:
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%r12), %rdx
	movq	%rcx, 24(%rdx)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_318
# BB#315:
	testq	%rcx, %rcx
	je	.LBB1_318
# BB#316:
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	jmp	.LBB1_318
.LBB1_317:                              # %.thread2227
	movq	$0, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_318:
	movl	$0, 56(%rax)
	movl	$0, 48(%rax)
	movq	%rax, %r12
	jmp	.LBB1_483
.LBB1_319:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_320:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_322
# BB#321:
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_322:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_421
# BB#323:
	testq	%rax, %rax
	je	.LBB1_421
# BB#324:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB1_421
.LBB1_325:
	movzbl	zz_lengths+121(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_470
# BB#326:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_471
.LBB1_327:
	leaq	56(%r13,%rbx,4), %r14
	movl	56(%r13,%rbx,4), %ecx
	leal	(%rcx,%rax), %esi
	movl	68(%r12), %edi
	cmpl	%edi, %esi
	jg	.LBB1_329
# BB#328:
	cmpl	72(%r12), %ecx
	jle	.LBB1_330
.LBB1_329:
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	callq	EchoLength
	movq	%r15, %rbp
	movq	%rax, %r15
	movl	(%r14), %edi
	addl	48(%r13,%rbx,4), %edi
	callq	EchoLength
	movq	%rax, 8(%rsp)
	movq	%r15, (%rsp)
	movl	$12, %edi
	movl	$1, %esi
	movl	$.L.str.14, %edx
	movl	$2, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	$8388607, 64(%r12)      # imm = 0x7FFFFF
	movl	(%r14), %eax
	addl	48(%r13,%rbx,4), %eax
	movl	%eax, 68(%r12)
	movl	$8388607, 72(%r12)      # imm = 0x7FFFFF
	movl	48(%r13,%rbx,4), %eax
.LBB1_330:
	leaq	48(%r12,%rbx,4), %rdi
	movl	%eax, 48(%r12,%rbx,4)
	movl	(%r14), %eax
	leaq	56(%r12,%rbx,4), %rsi
	movl	%eax, 56(%r12,%rbx,4)
	callq	EnlargeToConstraint
	movl	32(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB1_483
.LBB1_331:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
	movb	$-121, 32(%rbp)
	jmp	.LBB1_211
.LBB1_332:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_333:
	movb	$5, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movzwl	34(%r12), %eax
	movw	%ax, 34(%rbp)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r12), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbp), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbp)
	andl	36(%r12), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	movq	%r12, zz_hold(%rip)
	movq	24(%r12), %rax
	cmpq	%r12, %rax
	je	.LBB1_337
# BB#334:
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r12), %rcx
	movq	%rax, 24(%rcx)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%rax, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_338
# BB#335:
	testq	%rax, %rax
	je	.LBB1_338
# BB#336:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbp), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbp)
	movq	%rbp, 24(%rcx)
	jmp	.LBB1_338
.LBB1_337:                              # %.thread2222
	movq	$0, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_338:
	movq	80(%r12), %rax
	movzwl	41(%rax), %eax
	testb	$64, %ah
	jne	.LBB1_341
# BB#339:
	movq	%r12, %rbx
	movzbl	zz_lengths+120(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r12
	testq	%r12, %r12
	je	.LBB1_488
# BB#340:
	movq	%r12, zz_hold(%rip)
	movq	(%r12), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_489
.LBB1_341:
	movzbl	40(%r12), %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_491
# BB#342:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_492
.LBB1_343:
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
.LBB1_344:                              # %._crit_edge
	movq	%r13, %rax
	movl	%r12d, 48(%rax,%r14,4)
	movq	%rax, %r12
	movl	%r15d, 56(%r12,%r14,4)
	movb	$1, 41(%r12)
	movq	8(%r12), %rbp
	cmpq	%r12, %rbp
	je	.LBB1_370
# BB#345:                               # %.preheader.preheader
	leaq	48(%r12), %r15
	leaq	56(%r12), %r12
.LBB1_346:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_347 Depth 2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB1_347:                              #   Parent Loop BB1_346 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_347
# BB#348:                               #   in Loop: Header=BB1_346 Depth=1
	cmpb	$1, %al
	jne	.LBB1_350
# BB#349:                               #   in Loop: Header=BB1_346 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.29, %r9d
	xorl	%eax, %eax
	callq	Error
	movb	32(%rbx), %al
.LBB1_350:                              # %.loopexit1732
                                        #   in Loop: Header=BB1_346 Depth=1
	addb	$-43, %al
	cmpb	$3, %al
	ja	.LBB1_352
# BB#351:                               #   in Loop: Header=BB1_346 Depth=1
	movq	%rbx, %rdi
	movl	32(%rsp), %esi          # 4-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	callq	MinSize
	movl	(%r15,%r14,4), %ecx
	leaq	48(%rax), %rdx
	cmpl	48(%rax,%r14,4), %ecx
	cmovgeq	%r15, %rdx
	movl	(%rdx,%r14,4), %ecx
	movl	%ecx, (%r15,%r14,4)
	movl	(%r12,%r14,4), %ecx
	cmpl	56(%rax,%r14,4), %ecx
	leaq	56(%rax), %rax
	cmovgeq	%r12, %rax
	movl	(%rax,%r14,4), %eax
	movl	%eax, (%r12,%r14,4)
.LBB1_352:                              #   in Loop: Header=BB1_346 Depth=1
	movq	8(%rbp), %rbp
	cmpq	%r13, %rbp
	jne	.LBB1_346
# BB#353:
	movq	%r13, %r12
	movl	32(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB1_483
.LBB1_354:
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
.LBB1_355:                              # %._crit_edge1878
	movq	%r13, %rax
	movl	%r12d, 48(%rax,%r14,4)
	movl	%r15d, 56(%rax,%r14,4)
	movb	$1, 41(%rax)
	movq	8(%rax), %rbp
	cmpq	%rax, %rbp
	je	.LBB1_364
.LBB1_356:                              # %.preheader1735
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_357 Depth 2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB1_357:                              #   Parent Loop BB1_356 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_357
# BB#358:                               #   in Loop: Header=BB1_356 Depth=1
	cmpb	$1, %al
	jne	.LBB1_360
# BB#359:                               #   in Loop: Header=BB1_356 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
	movb	32(%rbx), %al
.LBB1_360:                              # %.loopexit1736
                                        #   in Loop: Header=BB1_356 Depth=1
	cmpb	$46, %al
	ja	.LBB1_363
# BB#361:                               # %.loopexit1736
                                        #   in Loop: Header=BB1_356 Depth=1
	movzbl	%al, %eax
	movabsq	$127543348822016, %rcx  # imm = 0x740000000000
	btq	%rax, %rcx
	jae	.LBB1_363
# BB#362:                               #   in Loop: Header=BB1_356 Depth=1
	movq	%rbx, %rdi
	movl	32(%rsp), %esi          # 4-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	callq	MinSize
	movl	48(%rax,%r14,4), %ecx
	cmpl	%ecx, %r12d
	cmovll	%ecx, %r12d
	movl	%r12d, 36(%rsp)
	movl	56(%rax,%r14,4), %eax
	cmpl	%eax, %r15d
	cmovll	%eax, %r15d
	movl	%r15d, 28(%rsp)
.LBB1_363:                              #   in Loop: Header=BB1_356 Depth=1
	movq	8(%rbp), %rbp
	cmpq	%r13, %rbp
	jne	.LBB1_356
.LBB1_364:                              # %._crit_edge1875
	movl	%r12d, 48(%r13,%r14,4)
	movq	%r13, %r12
	movl	%r15d, 56(%r12,%r14,4)
	movl	32(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB1_483
.LBB1_365:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_366:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	leaq	88(%r12), %rdx
	movq	%rax, 88(%r12)
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	MinSize
	leaq	48(%r12), %rdi
	leaq	56(%r12), %rsi
	leaq	52(%r12), %rdx
	leaq	60(%r12), %rcx
	movl	76(%r12), %r9d
	movq	%rax, %r8
	callq	RotateSize
	jmp	.LBB1_483
.LBB1_367:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB1_368:
	movb	$14, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movzwl	34(%r12), %eax
	movw	%ax, 34(%r14)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r12), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%r14), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%r14)
	andl	36(%r12), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r14)
	andb	$-65, 42(%r14)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_371
# BB#369:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_372
.LBB1_370:
	movl	32(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB1_483
.LBB1_371:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_372:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_374
# BB#373:
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_374:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB1_377
# BB#375:
	testq	%rax, %rax
	je	.LBB1_377
# BB#376:
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_377:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_379
# BB#378:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_380
.LBB1_379:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_380:
	testq	%r14, %r14
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	je	.LBB1_383
# BB#381:
	testq	%rax, %rax
	je	.LBB1_383
# BB#382:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_383:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_386
# BB#384:
	testq	%rax, %rax
	je	.LBB1_386
# BB#385:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_386:
	movq	24(%r12), %rbx
	.p2align	4, 0x90
.LBB1_387:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_387
# BB#388:
	cmpb	$16, %al
	je	.LBB1_390
# BB#389:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.52, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_390:                              # %.loopexit281.i
	movl	$-532676608, %eax       # imm = 0xE0400000
	andl	40(%r14), %eax
	orl	$1, %eax
	movl	%eax, 40(%r14)
	movq	24(%r12), %rax
	movq	8(%rax), %r15
	cmpq	%rbx, %r15
	je	.LBB1_399
# BB#391:                               # %.lr.ph.i.preheader
	movq	%r15, %rax
	movq	%r15, 16(%rsp)          # 8-byte Spill
.LBB1_392:                              # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_394 Depth 2
	leaq	16(%rax), %rcx
	jmp	.LBB1_394
	.p2align	4, 0x90
.LBB1_393:                              #   in Loop: Header=BB1_394 Depth=2
	addq	$16, %rcx
.LBB1_394:                              #   Parent Loop BB1_392 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB1_393
# BB#395:                               #   in Loop: Header=BB1_392 Depth=1
	movl	%edx, %ecx
	addb	$-42, %cl
	cmpb	$4, %cl
	jb	.LBB1_400
# BB#396:                               #   in Loop: Header=BB1_392 Depth=1
	cmpb	$46, %dl
	jne	.LBB1_398
# BB#397:                               #   in Loop: Header=BB1_392 Depth=1
	movq	8(%rax), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
.LBB1_398:                              # %.loopexit278.loopexit.i
                                        #   in Loop: Header=BB1_392 Depth=1
	addq	$8, %rax
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.LBB1_392
	jmp	.LBB1_400
.LBB1_399:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
.LBB1_400:                              # %.loopexit279.i
	cmpq	16(%rsp), %r15          # 8-byte Folded Reload
	je	.LBB1_421
# BB#401:                               # %.preheader.lr.ph.i
	movq	%r12, 56(%rsp)          # 8-byte Spill
.LBB1_402:                              # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_403 Depth 2
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB1_403:                              #   Parent Loop BB1_402 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB1_403
# BB#404:                               #   in Loop: Header=BB1_402 Depth=1
	movzbl	zz_lengths+46(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_406
# BB#405:                               #   in Loop: Header=BB1_402 Depth=1
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_407
.LBB1_406:                              #   in Loop: Header=BB1_402 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_407:                              #   in Loop: Header=BB1_402 Depth=1
	movl	%r13d, %r12d
	movb	$46, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	34(%rbp), %eax
	movw	%ax, 34(%rbx)
	movl	36(%rbp), %eax
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %eax
	movl	36(%rbx), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movl	36(%rbp), %ecx
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movq	%rbp, zz_hold(%rip)
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB1_411
# BB#408:                               #   in Loop: Header=BB1_402 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rbp), %rcx
	movq	%rax, 24(%rcx)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%r13b
	je	.LBB1_412
# BB#409:                               #   in Loop: Header=BB1_402 Depth=1
	testq	%rax, %rax
	je	.LBB1_412
# BB#410:                               #   in Loop: Header=BB1_402 Depth=1
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	xorl	%r13d, %r13d
	jmp	.LBB1_412
.LBB1_411:                              # %.thread353.i
                                        #   in Loop: Header=BB1_402 Depth=1
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%r13b
.LBB1_412:                              #   in Loop: Header=BB1_402 Depth=1
	movq	%rbp, %rdi
	callq	DisposeObject
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_414
# BB#413:                               #   in Loop: Header=BB1_402 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_415
.LBB1_414:                              #   in Loop: Header=BB1_402 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_415:                              #   in Loop: Header=BB1_402 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	sete	%cl
	orb	%cl, %r13b
	jne	.LBB1_417
# BB#416:                               #   in Loop: Header=BB1_402 Depth=1
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_417:                              #   in Loop: Header=BB1_402 Depth=1
	testq	%r14, %r14
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	movl	%r12d, %r13d
	je	.LBB1_420
# BB#418:                               #   in Loop: Header=BB1_402 Depth=1
	testq	%rax, %rax
	je	.LBB1_420
# BB#419:                               #   in Loop: Header=BB1_402 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_420:                              #   in Loop: Header=BB1_402 Depth=1
	movl	40(%r14), %eax
	leal	1(%rax), %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	andl	$-4096, %eax            # imm = 0xF000
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movq	8(%r15), %r15
	cmpq	16(%rsp), %r15          # 8-byte Folded Reload
	movq	56(%rsp), %r12          # 8-byte Reload
	jne	.LBB1_402
.LBB1_421:                              # %BuildSpanner.exit.thread
	movq	48(%rsp), %rbp          # 8-byte Reload
	movb	(%rbp), %al
	cmpb	$44, %al
	jne	.LBB1_423
# BB#422:
	cmpl	$1, %r13d
	je	.LBB1_425
	jmp	.LBB1_430
.LBB1_423:
	cmpl	$1, %r13d
	jne	.LBB1_430
# BB#424:
	cmpb	$42, %al
	jne	.LBB1_430
.LBB1_425:
	movq	8(%r12), %rbx
	.p2align	4, 0x90
.LBB1_426:                              # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_426
# BB#427:
	cmpb	$13, %al
	je	.LBB1_429
# BB#428:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.9, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_429:                              # %.loopexit1758
	leaq	36(%rsp), %rdx
	leaq	28(%rsp), %rcx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	SpannerAvailableSpace
	movl	$8388607, 128(%rsp)     # imm = 0x7FFFFF
	movl	28(%rsp), %eax
	addl	36(%rsp), %eax
	movl	%eax, 132(%rsp)
	movl	$8388607, 136(%rsp)     # imm = 0x7FFFFF
	leaq	128(%rsp), %rsi
	movq	%rbx, %rdi
	callq	BreakObject
	orb	$64, 42(%rax)
.LBB1_430:
	leaq	8(%r12), %rax
	testl	%r13d, %r13d
	cmovneq	%r12, %rax
	movq	(%rax), %rcx
	addq	$16, %rcx
	.p2align	4, 0x90
.LBB1_431:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rbx
	movzbl	32(%rbx), %eax
	leaq	16(%rbx), %rcx
	testb	%al, %al
	je	.LBB1_431
# BB#432:
	movb	(%rbp), %cl
	cmpb	$13, %al
	movq	40(%rsp), %rdx          # 8-byte Reload
	je	.LBB1_434
# BB#433:
	cmpb	$45, %cl
	je	.LBB1_436
.LBB1_434:
	cmpb	$14, %al
	je	.LBB1_438
# BB#435:
	cmpb	$46, %cl
	jne	.LBB1_438
.LBB1_436:
	testl	%r13d, %r13d
	jne	.LBB1_482
# BB#437:
	movzbl	%cl, %edi
	callq	Image
	movq	%rax, %rbx
	movl	$12, %edi
	movl	$15, %esi
	movl	$.L.str.10, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	%rbx, %r9
	callq	Error
	jmp	.LBB1_482
.LBB1_438:
	testl	%r13d, %r13d
	sete	%dil
	cmpb	$45, %cl
	sete	%sil
	cmpl	$1, %r13d
	sete	%r8b
	cmpb	$46, %cl
	sete	%cl
	testb	%sil, %r8b
	jne	.LBB1_443
# BB#439:
	andb	%cl, %dil
	jne	.LBB1_443
# BB#440:
	addb	$-13, %al
	cmpb	$2, %al
	jae	.LBB1_444
# BB#441:
	movl	40(%rbx), %eax
	movl	%eax, %ecx
	shrl	$12, %ecx
	incl	%ecx
	andl	$1023, %ecx             # imm = 0x3FF
	movl	%ecx, %edi
	shll	$12, %edi
	movl	%eax, %esi
	andl	$-4190209, %esi         # imm = 0xFFC00FFF
	orl	%edi, %esi
	movl	%esi, 40(%rbx)
	andl	$4095, %eax             # imm = 0xFFF
	cmpl	%eax, %ecx
	jne	.LBB1_443
# BB#442:
	movq	%rbx, %rdi
	movl	%r13d, %esi
	callq	MinSize
	leaq	36(%rsp), %rdx
	leaq	28(%rsp), %rcx
	movq	%rbx, %rdi
	movl	%r13d, %esi
	callq	SpannerAvailableSpace
	movslq	%r13d, %rax
	movl	$0, 48(%r12,%rax,4)
	movl	56(%rbx,%rax,4), %ecx
	addl	48(%rbx,%rax,4), %ecx
	xorl	%edx, %edx
	subl	36(%rsp), %ecx
	cmovsl	%edx, %ecx
	jmp	.LBB1_445
.LBB1_443:
	leaq	48(%r12), %rax
	movslq	%r13d, %rcx
	movl	$0, 56(%r12,%rcx,4)
	xorl	%edx, %edx
	movl	%edx, (%rax,%rcx,4)
	jmp	.LBB1_483
.LBB1_444:
	movq	%rbx, %rdi
	movl	%r13d, %esi
	callq	MinSize
	movslq	%r13d, %rax
	movl	48(%rbx,%rax,4), %ecx
	movl	%ecx, 48(%r12,%rax,4)
	movl	56(%rbx,%rax,4), %ecx
.LBB1_445:                              # %.thread
	leaq	56(%r12), %rdx
	movl	%ecx, (%rdx,%rax,4)
	jmp	.LBB1_483
.LBB1_446:
	movzbl	zz_lengths+123(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_542
# BB#447:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	movb	$123, 32(%rbx)
	jmp	.LBB1_472
.LBB1_448:                              # %.loopexit1747.loopexit
	xorl	%r14d, %r14d
.LBB1_449:                              # %.loopexit1747
	movq	(%r12), %rcx
	addq	$16, %rcx
	.p2align	4, 0x90
.LBB1_450:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rbp
	movzbl	32(%rbp), %eax
	leaq	16(%rbp), %rcx
	testb	%al, %al
	je	.LBB1_450
	jmp	.LBB1_453
	.p2align	4, 0x90
.LBB1_451:                              #   in Loop: Header=BB1_453 Depth=1
	movq	(%rbp), %rcx
	addq	$16, %rcx
	.p2align	4, 0x90
.LBB1_452:                              #   Parent Loop BB1_453 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	leaq	16(%rbp), %rcx
	je	.LBB1_452
.LBB1_453:                              # %.preheader1743
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_452 Depth 2
	cmpb	$17, %al
	je	.LBB1_451
# BB#454:                               # %.preheader1743
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB1_483
# BB#455:
	leaq	64(%rbp), %rdi
	callq	strlen
	testb	%r14b, %r14b
	je	.LBB1_483
# BB#456:
	movb	63(%rbp,%rax), %al
	testb	%al, %al
	je	.LBB1_483
# BB#457:
	movzbl	%al, %edx
	movl	40(%rbp), %ecx
	movq	finfo(%rip), %rax
	andl	$4095, %ecx             # imm = 0xFFF
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	40(%rax,%rcx), %rsi
	movq	64(%rax,%rcx), %rbp
	movzbl	60(%rsi), %esi
	andl	$127, %esi
	movq	MapTable(,%rsi,8), %rdi
	movzbl	2945(%rdi,%rdx), %edx
	movzwl	(%rbp,%rdx,2), %esi
	xorl	%ebp, %ebp
	testq	%rsi, %rsi
	je	.LBB1_462
# BB#458:
	movzbl	%r14b, %edx
	movb	2945(%rdi,%rdx), %dl
	movq	72(%rax,%rcx), %rdi
.LBB1_459:                              # =>This Inner Loop Header: Depth=1
	cmpb	%dl, (%rdi,%rsi)
	leaq	1(%rsi), %rsi
	ja	.LBB1_459
# BB#460:
	jne	.LBB1_462
# BB#461:
	movq	80(%rax,%rcx), %rdx
	movq	88(%rax,%rcx), %rax
	movzbl	-1(%rdx,%rsi), %ecx
	movswl	(%rax,%rcx,2), %ebp
.LBB1_462:                              # %KernLength.exit
	addl	%ebp, %r15d
	movl	%r15d, 56(%r12,%rbx,4)
	jmp	.LBB1_483
.LBB1_463:
	movq	40(%rsp), %r14          # 8-byte Reload
	jmp	.LBB1_637
.LBB1_470:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_471:
	movb	$121, 32(%rbx)
.LBB1_472:
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movq	%r12, 80(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_474
# BB#473:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_475
.LBB1_474:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_475:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%r14), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_478
# BB#476:
	testq	%rcx, %rcx
	je	.LBB1_478
# BB#477:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_478:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_482
# BB#479:
	testq	%rax, %rax
	je	.LBB1_482
# BB#480:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
.LBB1_481:
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_482:
	movslq	%r13d, %rax
	movl	$0, 56(%r12,%rax,4)
	movl	$0, 48(%r12,%rax,4)
.LBB1_483:                              # %.thread
	movslq	%r13d, %rbx
	cmpl	$0, 48(%r12,%rbx,4)
	jns	.LBB1_485
# BB#484:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.44, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_485:
	cmpl	$0, 56(%r12,%rbx,4)
	jns	.LBB1_487
# BB#486:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.45, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_487:
	movq	%r12, %rax
	addq	$648, %rsp              # imm = 0x288
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_488:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r12
	movq	%r12, zz_hold(%rip)
.LBB1_489:
	movb	$120, 32(%r12)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%r12, 8(%r12)
	movq	%r12, (%r12)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_496
# BB#490:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_497
.LBB1_491:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
	movb	40(%r12), %al
.LBB1_492:
	movb	%al, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movq	%rbp, 88(%rbx)
	movq	8(%r12), %rcx
	addq	$16, %rcx
.LBB1_493:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rsi
	leaq	16(%rsi), %rcx
	cmpb	$0, 32(%rsi)
	je	.LBB1_493
# BB#494:
	movq	88(%r12), %rdi
	movzbl	%al, %edx
	callq	CrossMake
	movq	%rax, 80(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_504
# BB#495:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_505
.LBB1_496:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_497:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB1_500
# BB#498:
	testq	%rax, %rax
	je	.LBB1_500
# BB#499:
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_500:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_502
# BB#501:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_502:
	movq	%rbp, 88(%r12)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_511
# BB#503:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_512
.LBB1_504:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_505:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%r14), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_508
# BB#506:
	testq	%rcx, %rcx
	je	.LBB1_508
# BB#507:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_508:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_510
# BB#509:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_510:
	movq	%r12, %rdi
	callq	DisposeObject
	jmp	.LBB1_518
.LBB1_511:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_512:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%r14), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_515
# BB#513:
	testq	%rcx, %rcx
	je	.LBB1_515
# BB#514:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_515:
	testq	%r12, %r12
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	je	.LBB1_518
# BB#516:
	testq	%rax, %rax
	je	.LBB1_518
# BB#517:
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_518:
	movl	$0, 56(%rbp)
	movl	$0, 48(%rbp)
.LBB1_519:
	movslq	%r13d, %rax
	movl	$0, 56(%rbp,%rax,4)
	movl	$0, 48(%rbp,%rax,4)
	movq	%rbp, %r12
	jmp	.LBB1_483
.LBB1_520:                              # %.critedge40.preheader
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	je	.LBB1_637
# BB#521:                               # %.preheader1752.lr.ph.preheader
	movq	%r12, %r15
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
.LBB1_522:                              # %.preheader1752
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_523 Depth 2
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB1_523:                              #   Parent Loop BB1_522 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.LBB1_523
# BB#524:                               #   in Loop: Header=BB1_522 Depth=1
	cmpb	$9, %al
	je	.LBB1_527
# BB#525:                               #   in Loop: Header=BB1_522 Depth=1
	cmpb	$1, %al
	jne	.LBB1_528
# BB#526:                               # %.critedge40.outer
                                        #   in Loop: Header=BB1_522 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	movq	%rdi, %r12
	movq	%rdi, %rbp
	jne	.LBB1_522
	jmp	.LBB1_627
.LBB1_527:                              #   in Loop: Header=BB1_522 Depth=1
	callq	SplitIsDefinite
	testl	%eax, %eax
	je	.LBB1_529
	jmp	.LBB1_543
.LBB1_528:                              #   in Loop: Header=BB1_522 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB1_543
.LBB1_529:                              # %.critedge40.backedge
                                        #   in Loop: Header=BB1_522 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB1_522
# BB#530:
	movq	%r15, %r12
	jmp	.LBB1_637
.LBB1_531:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_532:
	movb	$-120, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movq	%r12, 80(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_534
# BB#533:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_535
.LBB1_534:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_535:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%r14), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_538
# BB#536:
	testq	%rcx, %rcx
	je	.LBB1_538
# BB#537:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_538:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_541
# BB#539:
	testq	%rax, %rax
	je	.LBB1_541
# BB#540:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_541:
	andb	$-2, 42(%r12)
	jmp	.LBB1_483
.LBB1_542:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
	movb	$123, 32(%rbx)
	jmp	.LBB1_472
.LBB1_543:
	testq	%r12, %r12
	jne	.LBB1_545
# BB#544:
	movq	no_fpos(%rip), %r8
	xorl	%r12d, %r12d
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.16, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_545:                              # %.preheader1750
	cmpq	%r15, %rbx
	je	.LBB1_629
# BB#546:                               # %.lr.ph
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB1_565
.LBB1_547:                              # %.preheader1748.lr.ph.preheader
                                        #   in Loop: Header=BB1_565 Depth=1
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
.LBB1_548:                              # %.preheader1748
                                        #   Parent Loop BB1_565 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_549 Depth 3
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB1_549:                              #   Parent Loop BB1_565 Depth=1
                                        #     Parent Loop BB1_548 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.LBB1_549
# BB#550:                               #   in Loop: Header=BB1_548 Depth=2
	cmpb	$9, %al
	je	.LBB1_553
# BB#551:                               #   in Loop: Header=BB1_548 Depth=2
	cmpb	$1, %al
	jne	.LBB1_554
# BB#552:                               # %.critedge41.outer
                                        #   in Loop: Header=BB1_548 Depth=2
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	movq	%rdi, %r12
	movq	%rdi, %rbp
	jne	.LBB1_548
	jmp	.LBB1_624
.LBB1_553:                              #   in Loop: Header=BB1_548 Depth=2
	callq	SplitIsDefinite
	testl	%eax, %eax
	je	.LBB1_555
	jmp	.LBB1_556
.LBB1_554:                              #   in Loop: Header=BB1_548 Depth=2
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB1_556
.LBB1_555:                              # %.critedge41.backedge
                                        #   in Loop: Header=BB1_548 Depth=2
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB1_548
	jmp	.LBB1_624
.LBB1_556:                              #   in Loop: Header=BB1_565 Depth=1
	testq	%r12, %r12
	jne	.LBB1_558
# BB#557:                               #   in Loop: Header=BB1_565 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.16, %r9d
	xorl	%eax, %eax
	callq	Error
	xorl	%r12d, %r12d
.LBB1_558:                              # %.backedge1751
                                        #   in Loop: Header=BB1_565 Depth=1
	cmpq	%r15, %rbx
	jne	.LBB1_565
	jmp	.LBB1_624
.LBB1_559:                              #   in Loop: Header=BB1_565 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rax, zz_hold(%rip)
.LBB1_560:                              #   in Loop: Header=BB1_565 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	$19, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	andb	$-9, 43(%rax)
	movq	%r15, %rcx
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB1_564
# BB#561:                               #   in Loop: Header=BB1_565 Depth=1
	movq	%r15, %rdx
	movq	16(%rdx), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rdx), %rcx
	movq	%rax, 24(%rcx)
	movq	%rdx, 24(%rdx)
	movq	%rdx, 16(%rdx)
	movq	%rax, xx_tmp(%rip)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB1_568
# BB#562:                               #   in Loop: Header=BB1_565 Depth=1
	testq	%rax, %rax
	je	.LBB1_568
# BB#563:                               #   in Loop: Header=BB1_565 Depth=1
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rsi
	movq	16(%rsi), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rsi), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rsi)
	movq	%rsi, 24(%rcx)
	jmp	.LBB1_568
.LBB1_564:                              # %.thread2229
                                        #   in Loop: Header=BB1_565 Depth=1
	movq	$0, xx_tmp(%rip)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	jmp	.LBB1_568
.LBB1_565:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_548 Depth 2
                                        #       Child Loop BB1_549 Depth 3
	movzwl	44(%rbp), %eax
	cmpl	$8192, %eax             # imm = 0x2000
	jb	.LBB1_570
# BB#566:                               #   in Loop: Header=BB1_565 Depth=1
	cmpb	$0, 42(%r12)
	je	.LBB1_571
.LBB1_567:                              #   in Loop: Header=BB1_565 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB1_574
.LBB1_568:                              #   in Loop: Header=BB1_565 Depth=1
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r14
	testq	%r14, %r14
	movl	%r13d, 32(%rsp)         # 4-byte Spill
	je	.LBB1_576
# BB#569:                               #   in Loop: Header=BB1_565 Depth=1
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_577
.LBB1_570:                              #   in Loop: Header=BB1_565 Depth=1
	movq	%r15, %rcx
	jmp	.LBB1_623
.LBB1_571:                              #   in Loop: Header=BB1_565 Depth=1
	andl	$7168, %eax             # imm = 0x1C00
	cmpl	$2048, %eax             # imm = 0x800
	jne	.LBB1_595
# BB#572:                               #   in Loop: Header=BB1_565 Depth=1
	movswl	46(%rbp), %eax
	cmpl	$4097, %eax             # imm = 0x1001
	jge	.LBB1_567
# BB#573:                               #   in Loop: Header=BB1_565 Depth=1
	movq	%r15, %rcx
	jmp	.LBB1_623
.LBB1_574:                              #   in Loop: Header=BB1_565 Depth=1
	movzbl	zz_lengths+19(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rcx
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rcx, %rcx
	je	.LBB1_559
# BB#575:                               #   in Loop: Header=BB1_565 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_560
.LBB1_576:                              #   in Loop: Header=BB1_565 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB1_577:                              #   in Loop: Header=BB1_565 Depth=1
	movb	$17, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movq	8(%r15), %rax
	movq	24(%r12), %r13
	cmpq	%r13, %rax
	je	.LBB1_584
# BB#578:                               #   in Loop: Header=BB1_565 Depth=1
	movq	%rax, 48(%rsp)          # 8-byte Spill
	cmpb	$0, 32(%rax)
	je	.LBB1_580
# BB#579:                               #   in Loop: Header=BB1_565 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_580:                              #   in Loop: Header=BB1_565 Depth=1
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB1_582
# BB#581:                               #   in Loop: Header=BB1_565 Depth=1
	movq	(%r13), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rsi), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_582:                              #   in Loop: Header=BB1_565 Depth=1
	movq	%rsi, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB1_584
# BB#583:                               #   in Loop: Header=BB1_565 Depth=1
	movq	(%r14), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rsi), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_584:                              #   in Loop: Header=BB1_565 Depth=1
	movq	%r15, %rsi
	movzwl	64(%rsi), %ecx
	andl	$128, %ecx
	movzwl	64(%r14), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 64(%r14)
	movzwl	64(%rsi), %ecx
	andl	$256, %ecx              # imm = 0x100
	movl	%eax, %edx
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 64(%r14)
	movzwl	64(%rsi), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 64(%r14)
	movzwl	64(%rsi), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%ecx, %edx
	movw	%dx, 64(%r14)
	movzwl	64(%rsi), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%ecx, %edx
	movw	%dx, 64(%r14)
	movzwl	66(%rsi), %ecx
	movw	%cx, 66(%r14)
	movb	68(%rsi), %cl
	andb	$3, %cl
	movb	68(%r14), %dl
	andb	$-4, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r14)
	movb	68(%rsi), %cl
	andb	$12, %cl
	andb	$-13, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r14)
	movb	68(%rsi), %cl
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r14)
	movb	64(%rsi), %cl
	andb	$8, %cl
	andb	$-9, %al
	orb	%cl, %al
	movb	%al, 64(%r14)
	movzwl	68(%rsi), %edx
	andl	$128, %edx
	movzwl	68(%r14), %ecx
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%edx, %ecx
	movw	%cx, 68(%r14)
	movzwl	68(%rsi), %edx
	andl	$256, %edx              # imm = 0x100
	andl	$-257, %ecx             # imm = 0xFEFF
	orl	%edx, %ecx
	movw	%cx, 68(%r14)
	movzwl	68(%rsi), %edx
	andl	$512, %edx              # imm = 0x200
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	%edx, %ecx
	movw	%cx, 68(%r14)
	movzwl	68(%rsi), %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$-7169, %ecx            # imm = 0xE3FF
	orl	%edx, %ecx
	movw	%cx, 68(%r14)
	movzwl	68(%rsi), %edx
	andl	$57344, %edx            # imm = 0xE000
	andl	$8191, %ecx             # imm = 0x1FFF
	orl	%edx, %ecx
	movw	%cx, 68(%r14)
	movzwl	70(%rsi), %ecx
	movw	%cx, 70(%r14)
	movl	76(%rsi), %edx
	movl	$4095, %ecx             # imm = 0xFFF
	andl	%ecx, %edx
	movl	76(%r14), %ecx
	movl	$-4096, %edi            # imm = 0xF000
	andl	%edi, %ecx
	orl	%edx, %ecx
	movl	%ecx, 76(%r14)
	movl	76(%rsi), %edx
	movl	$4190208, %edi          # imm = 0x3FF000
	andl	%edi, %edx
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r14)
	movl	76(%rsi), %edx
	movl	$12582912, %edi         # imm = 0xC00000
	andl	%edi, %edx
	andl	$-12582913, %ecx        # imm = 0xFF3FFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r14)
	movl	76(%rsi), %edx
	movl	$1056964608, %edi       # imm = 0x3F000000
	andl	%edi, %edx
	andl	$-1056964609, %ecx      # imm = 0xC0FFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r14)
	movl	76(%rsi), %edx
	movl	$-2147483648, %edi      # imm = 0x80000000
	andl	%edi, %edx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r14)
	movl	76(%rsi), %edx
	movl	$1073741824, %edi       # imm = 0x40000000
	andl	%edi, %edx
	andl	$-1073741825, %ecx      # imm = 0xBFFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r14)
	movb	64(%rsi), %cl
	andb	$1, %cl
	andb	$-2, %al
	orb	%cl, %al
	movb	%al, 64(%r14)
	movb	64(%rsi), %cl
	andb	$2, %cl
	andb	$-3, %al
	orb	%cl, %al
	movb	%al, 64(%r14)
	movb	64(%rsi), %cl
	andb	$4, %cl
	andb	$-5, %al
	orb	%cl, %al
	movb	%al, 64(%r14)
	movb	64(%rsi), %cl
	andb	$112, %cl
	andb	$-113, %al
	orb	%cl, %al
	movb	%al, 64(%r14)
	movzwl	72(%rsi), %eax
	movw	%ax, 72(%r14)
	movzwl	74(%rsi), %eax
	movw	%ax, 74(%r14)
	movb	64(%rsi), %al
	shrb	$2, %al
	movzwl	42(%r14), %ecx
	andb	$1, %al
	movzbl	%al, %eax
	shll	$11, %eax
	andl	$63487, %ecx            # imm = 0xF7FF
	orl	%eax, %ecx
	movw	%cx, 42(%r14)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_586
# BB#585:                               #   in Loop: Header=BB1_565 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_587
.LBB1_586:                              #   in Loop: Header=BB1_565 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_587:                              #   in Loop: Header=BB1_565 Depth=1
	movl	32(%rsp), %r13d         # 4-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB1_590
# BB#588:                               #   in Loop: Header=BB1_565 Depth=1
	testq	%rax, %rax
	je	.LBB1_590
# BB#589:                               #   in Loop: Header=BB1_565 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_590:                              #   in Loop: Header=BB1_565 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_592
# BB#591:                               #   in Loop: Header=BB1_565 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_592:                              #   in Loop: Header=BB1_565 Depth=1
	cmpb	$0, 41(%r12)
	je	.LBB1_615
# BB#593:                               #   in Loop: Header=BB1_565 Depth=1
	leaq	32(%r12), %rdx
	movl	$11, %edi
	movl	$.L.str.8, %esi
	callq	MakeWord
	movq	%rax, %r14
	movq	%r15, %rdx
	movl	76(%rdx), %ecx
	movl	$4095, %eax             # imm = 0xFFF
	andl	%eax, %ecx
	movl	40(%r14), %eax
	movl	$-4096, %esi            # imm = 0xF000
	andl	%esi, %eax
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movl	76(%rdx), %ecx
	movl	$4190208, %esi          # imm = 0x3FF000
	andl	%esi, %ecx
	andl	$-4190209, %eax         # imm = 0xFFC00FFF
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movl	76(%rdx), %ecx
	movl	$4194304, %esi          # imm = 0x400000
	andl	%esi, %ecx
	andl	$-4194305, %eax         # imm = 0xFFBFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movl	76(%rdx), %ecx
	shrl	%ecx
	andl	$528482304, %ecx        # imm = 0x1F800000
	andl	$-528482305, %eax       # imm = 0xE07FFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movb	68(%rdx), %cl
	andb	$3, %cl
	xorl	%edx, %edx
	cmpb	$2, %cl
	sete	%dl
	shll	$31, %edx
	andl	$536870911, %eax        # imm = 0x1FFFFFFF
	leal	536870912(%rax,%rdx), %eax
	movl	%eax, 40(%r14)
	movl	$0, 56(%r14)
	movl	$0, 48(%r14)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_596
# BB#594:                               #   in Loop: Header=BB1_565 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_597
.LBB1_595:                              #   in Loop: Header=BB1_565 Depth=1
	movq	%r15, %rcx
	jmp	.LBB1_623
.LBB1_596:                              #   in Loop: Header=BB1_565 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_597:                              #   in Loop: Header=BB1_565 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r15), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_600
# BB#598:                               #   in Loop: Header=BB1_565 Depth=1
	testq	%rcx, %rcx
	je	.LBB1_600
# BB#599:                               #   in Loop: Header=BB1_565 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_600:                              #   in Loop: Header=BB1_565 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB1_603
# BB#601:                               #   in Loop: Header=BB1_565 Depth=1
	testq	%rax, %rax
	je	.LBB1_603
# BB#602:                               #   in Loop: Header=BB1_565 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_603:                              #   in Loop: Header=BB1_565 Depth=1
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r14
	testq	%r14, %r14
	je	.LBB1_605
# BB#604:                               #   in Loop: Header=BB1_565 Depth=1
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_606
.LBB1_605:                              #   in Loop: Header=BB1_565 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB1_606:                              #   in Loop: Header=BB1_565 Depth=1
	movb	$1, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movb	41(%r12), %al
	movb	%al, 41(%r14)
	movb	$0, 42(%r14)
	movl	40(%r14), %eax
	movzbl	%ah, %ecx  # NOREX
	andl	$-1610612737, %eax      # imm = 0x9FFFFFFF
	orl	$536870912, %eax        # imm = 0x20000000
	movl	%eax, 40(%r14)
	movq	%r15, %rsi
	movzwl	68(%rsi), %eax
	andl	$128, %eax
	movzwl	44(%r14), %edx
	andl	$65407, %edx            # imm = 0xFF7F
	orl	%eax, %edx
	movw	%dx, 44(%r14)
	movzwl	68(%rsi), %eax
	andl	$256, %eax              # imm = 0x100
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%eax, %edx
	movw	%dx, 44(%r14)
	movzwl	68(%rsi), %eax
	andl	$512, %eax              # imm = 0x200
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%eax, %edx
	movw	%dx, 44(%r14)
	movzwl	68(%rsi), %eax
	andl	$7168, %eax             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%eax, %edx
	movw	%dx, 44(%r14)
	movzwl	68(%rsi), %eax
	andl	$57344, %eax            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%eax, %edx
	movw	%dx, 44(%r14)
	imulw	70(%rsi), %cx
	movw	%cx, 46(%r14)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_608
# BB#607:                               #   in Loop: Header=BB1_565 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_609
.LBB1_608:                              #   in Loop: Header=BB1_565 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_609:                              #   in Loop: Header=BB1_565 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r15), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB1_612
# BB#610:                               #   in Loop: Header=BB1_565 Depth=1
	testq	%rax, %rax
	je	.LBB1_612
# BB#611:                               #   in Loop: Header=BB1_565 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_612:                              #   in Loop: Header=BB1_565 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB1_615
# BB#613:                               #   in Loop: Header=BB1_565 Depth=1
	testq	%rax, %rax
	je	.LBB1_615
# BB#614:                               #   in Loop: Header=BB1_565 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_615:                              #   in Loop: Header=BB1_565 Depth=1
	movq	24(%r12), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_617
# BB#616:                               #   in Loop: Header=BB1_565 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_617:                              #   in Loop: Header=BB1_565 Depth=1
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	je	.LBB1_620
# BB#618:                               #   in Loop: Header=BB1_565 Depth=1
	testq	%rax, %rax
	je	.LBB1_620
# BB#619:                               #   in Loop: Header=BB1_565 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_620:                              #   in Loop: Header=BB1_565 Depth=1
	movq	%r15, %rdx
	movzwl	64(%rdx), %ecx
	andl	$128, %ecx
	movzwl	44(%rbp), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movzwl	64(%rdx), %ecx
	andl	$256, %ecx              # imm = 0x100
	andl	$-257, %eax             # imm = 0xFEFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movzwl	64(%rdx), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %eax             # imm = 0xFDFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movzwl	64(%rdx), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %eax            # imm = 0xE3FF
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movzwl	64(%rdx), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %eax             # imm = 0x1FFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbp)
	movb	42(%r12), %al
	cmpb	$1, %al
	ja	.LBB1_622
# BB#621:                               #   in Loop: Header=BB1_565 Depth=1
	movb	$1, %al
.LBB1_622:                              #   in Loop: Header=BB1_565 Depth=1
	movzbl	%al, %eax
	movq	%r15, %rcx
	imulw	66(%rcx), %ax
	movw	%ax, 46(%rbp)
.LBB1_623:                              #   in Loop: Header=BB1_565 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%rcx, %rbx
	jne	.LBB1_547
.LBB1_624:                              # %._crit_edge1918
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB1_629
# BB#625:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_630
# BB#626:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_631
.LBB1_629:
	movq	%r15, %r12
	jmp	.LBB1_637
.LBB1_627:
	movq	%r15, %r12
	jmp	.LBB1_637
.LBB1_68:
	cvttss2si	100(%rsp), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	cvttss2si	96(%rsp), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	cvttss2si	92(%rsp), %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movb	$4, %cl
	xorl	%r12d, %r12d
	cvttss2si	88(%rsp), %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testl	%r12d, %r12d
	jne	.LBB1_70
.LBB1_61:                               # %.outer.split.us
	andb	$7, %cl
	cmpb	$4, %cl
	movq	%r13, %r12
	ja	.LBB1_208
# BB#62:                                # %.outer.split.us
	movzbl	%cl, %eax
	jmpq	*.LJTI1_3(,%rax,8)
.LBB1_63:
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbp, %rdx
	callq	fscanf
	incl	%eax
	cmpl	$2, %eax
	jb	.LBB1_203
.LBB1_64:                               # %.us-lcssa1868.us
	xorl	%r12d, %r12d
	cmpb	$37, 128(%rsp)
	movl	$0, %ecx
	jne	.LBB1_69
# BB#65:
	movl	$.L.str.32, %esi
	movq	%rbp, %rdi
	callq	StringBeginsWith
	xorl	%r12d, %r12d
	testl	%eax, %eax
	movl	$0, %ecx
	je	.LBB1_69
# BB#66:
	movl	$.L.str.33, %esi
	movq	%rbp, %rdi
	callq	StringContains
	xorl	%r12d, %r12d
	testl	%eax, %eax
	movl	$0, %ecx
	jne	.LBB1_69
# BB#67:
	xorl	%r12d, %r12d
	movl	$.L.str.34, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	leaq	100(%rsp), %rdx
	leaq	96(%rsp), %rcx
	leaq	92(%rsp), %r8
	leaq	88(%rsp), %r9
	callq	sscanf
	movb	$3, %cl
	cmpl	$4, %eax
	je	.LBB1_68
.LBB1_69:                               # %.outer
	testl	%r12d, %r12d
	je	.LBB1_61
.LBB1_70:
	movq	%r13, %r12
	.p2align	4, 0x90
.LBB1_71:                               # %.outer.split
                                        # =>This Inner Loop Header: Depth=1
	andb	$7, %cl
	cmpb	$4, %cl
	ja	.LBB1_208
# BB#72:                                # %.outer.split
                                        #   in Loop: Header=BB1_71 Depth=1
	movzbl	%cl, %eax
	jmpq	*.LJTI1_2(,%rax,8)
.LBB1_73:                               #   in Loop: Header=BB1_71 Depth=1
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbp, %rdx
	callq	fscanf
	incl	%eax
	cmpl	$2, %eax
	jb	.LBB1_203
# BB#74:                                #   in Loop: Header=BB1_71 Depth=1
	movl	$.L.str.31, %esi
	movq	%rbp, %rdi
	callq	StringBeginsWith
	movb	$2, %cl
	testl	%eax, %eax
	je	.LBB1_71
	jmp	.LBB1_64
.LBB1_203:                              # %.us-lcssa1867.us
	cmpb	$94, 32(%r12)
	movl	$.L.str.36, %eax
	movl	$.L.str.37, %r9d
	cmoveq	%rax, %r9
	movq	80(%rsp), %rax
	addq	$64, %rax
	movq	%rax, (%rsp)
	movl	$12, %edi
	movl	$6, %esi
	movl	$.L.str.38, %edx
.LBB1_204:                              # %.us-lcssa1867.us
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 48(%r12)
.LBB1_205:
	movb	$1, 41(%r12)
.LBB1_206:
	movq	%r14, %rdi
	callq	fclose
	cmpl	$0, 76(%rsp)
	je	.LBB1_208
# BB#207:
	movl	$.L.str.39, %edi
	callq	remove
.LBB1_208:                              # %.loopexit
	movq	80(%rsp), %rdi
	callq	DisposeObject
	movl	32(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB1_483
.LBB1_464:                              # %.us-lcssa.us
	cmpb	$94, 32(%r12)
	movl	$.L.str.36, %eax
	movl	$.L.str.37, %r9d
	cmoveq	%rax, %r9
	movq	80(%rsp), %rax
	addq	$64, %rax
	movq	%rax, (%rsp)
	movl	$12, %edi
	movl	$5, %esi
	movl	$.L.str.35, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
	movb	$0, 41(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r12)
	jmp	.LBB1_208
.LBB1_465:                              # %.us-lcssa1864.us
	cmpb	$94, 32(%r12)
	movl	$.L.str.36, %eax
	movl	$.L.str.37, %r9d
	cmoveq	%rax, %r9
	movq	80(%rsp), %rax
	addq	$64, %rax
	movq	%rax, (%rsp)
	movl	$12, %edi
	movl	$7, %esi
	movl	$.L.str.40, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
	movb	$0, 41(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r12)
	jmp	.LBB1_206
.LBB1_466:                              # %.us-lcssa1865.us
	cmpb	$94, 32(%r12)
	movl	$.L.str.36, %eax
	movl	$.L.str.37, %r9d
	cmoveq	%rax, %r9
	movq	80(%rsp), %rax
	addq	$64, %rax
	movq	%rax, (%rsp)
	movl	$12, %edi
	movl	$8, %esi
	movl	$.L.str.41, %edx
	jmp	.LBB1_204
.LBB1_467:                              # %.us-lcssa1866.us
	movq	8(%r12), %rax
.LBB1_468:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB1_468
# BB#469:
	movl	64(%rsp), %edx          # 4-byte Reload
	movl	%edx, 48(%rax)
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 56(%rax)
	movl	16(%rsp), %edi          # 4-byte Reload
	movl	%edi, 52(%rax)
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	%esi, 60(%rax)
	subl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,4), %eax
	xorl	%ecx, %ecx
	testl	%eax, %eax
	cmovsl	%ecx, %eax
	cmpl	$8388608, %eax          # imm = 0x800000
	movl	$8388607, %edx          # imm = 0x7FFFFF
	cmovgel	%edx, %eax
	shrl	%eax
	movl	%eax, 56(%r12)
	movl	%eax, 48(%r12)
	subl	%edi, %esi
	shll	$2, %esi
	leal	(%rsi,%rsi,4), %eax
	testl	%eax, %eax
	cmovnsl	%eax, %ecx
	cmpl	$8388608, %ecx          # imm = 0x800000
	cmovgel	%edx, %ecx
	movl	%ecx, 36(%rsp)
	shrl	%ecx
	movl	%ecx, 60(%r12)
	movl	%ecx, 52(%r12)
	jmp	.LBB1_205
.LBB1_630:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_631:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_633
# BB#632:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_633:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_636
# BB#634:
	testq	%rax, %rax
	je	.LBB1_636
# BB#635:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_636:                              # %.critedge1583
	movq	%rbx, %r12
.LBB1_637:                              # %.critedge1583
	cmpl	$1, %r13d
	sete	%al
	cmpb	$19, 32(%r12)
	sete	%cl
	cmpb	%cl, %al
	movq	%r12, 56(%rsp)          # 8-byte Spill
	je	.LBB1_676
# BB#638:                               # %.preheader1740
	movq	8(%r12), %rbx
	cmpq	%r12, %rbx
	je	.LBB1_678
# BB#639:                               # %.preheader1739.lr.ph
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movslq	%r13d, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movl	$0, 64(%rsp)            # 4-byte Folded Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_640:                              # %.preheader1739
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_641 Depth 2
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB1_641:                              #   Parent Loop BB1_640 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB1_641
# BB#642:                               #   in Loop: Header=BB1_640 Depth=1
	movl	%eax, %ecx
	addb	$-119, %cl
	cmpb	$19, %cl
	ja	.LBB1_649
# BB#643:                               #   in Loop: Header=BB1_640 Depth=1
	cmpl	$1, %r13d
	jne	.LBB1_674
# BB#644:                               #   in Loop: Header=BB1_640 Depth=1
	movq	(%rbx), %rbx
	movq	8(%rbx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_646
# BB#645:                               #   in Loop: Header=BB1_640 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_646:                              #   in Loop: Header=BB1_640 Depth=1
	movq	%rax, zz_res(%rip)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_674
# BB#647:                               #   in Loop: Header=BB1_640 Depth=1
	testq	%rcx, %rcx
	je	.LBB1_674
# BB#648:                               #   in Loop: Header=BB1_640 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	jmp	.LBB1_674
	.p2align	4, 0x90
.LBB1_649:                              #   in Loop: Header=BB1_640 Depth=1
	cmpb	32(%r12), %al
	jne	.LBB1_657
# BB#650:                               #   in Loop: Header=BB1_640 Depth=1
	movl	%r13d, %r12d
	movq	(%rbx), %rbx
	movq	8(%rbp), %r13
	cmpq	%rbp, %r13
	je	.LBB1_655
# BB#651:                               #   in Loop: Header=BB1_640 Depth=1
	movq	8(%rbx), %r15
	cmpb	$0, 32(%r13)
	je	.LBB1_653
# BB#652:                               #   in Loop: Header=BB1_640 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_653:                              #   in Loop: Header=BB1_640 Depth=1
	movq	%r13, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r13), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%r13, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_655
# BB#654:                               #   in Loop: Header=BB1_640 Depth=1
	movq	(%r15), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r13), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_655:                              #   in Loop: Header=BB1_640 Depth=1
	movq	24(%rbp), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_665
# BB#656:                               #   in Loop: Header=BB1_640 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_666
.LBB1_657:                              #   in Loop: Header=BB1_640 Depth=1
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jae	.LBB1_660
# BB#658:                               #   in Loop: Header=BB1_640 Depth=1
	testl	%r13d, %r13d
	jne	.LBB1_671
# BB#659:                               #   in Loop: Header=BB1_640 Depth=1
	movq	%rbp, %rdi
	callq	FontWordSize
	testl	%r14d, %r14d
	jne	.LBB1_672
	jmp	.LBB1_673
.LBB1_660:                              #   in Loop: Header=BB1_640 Depth=1
	cmpb	$1, %al
	jne	.LBB1_670
# BB#661:                               #   in Loop: Header=BB1_640 Depth=1
	testl	%r14d, %r14d
	jne	.LBB1_663
# BB#662:                               #   in Loop: Header=BB1_640 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.22, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_663:                              #   in Loop: Header=BB1_640 Depth=1
	testb	$2, 45(%rbp)
	jne	.LBB1_674
# BB#664:                               #   in Loop: Header=BB1_640 Depth=1
	movl	28(%rsp), %eax
	addl	36(%rsp), %eax
	movl	48(%rsp), %ecx          # 4-byte Reload
	cmpl	%eax, %ecx
	cmovll	%eax, %ecx
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	movl	$1, 64(%rsp)            # 4-byte Folded Spill
	xorl	%r14d, %r14d
	jmp	.LBB1_674
.LBB1_665:                              #   in Loop: Header=BB1_640 Depth=1
	xorl	%ecx, %ecx
.LBB1_666:                              #   in Loop: Header=BB1_640 Depth=1
	movl	%r12d, %r13d
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	movq	56(%rsp), %r12          # 8-byte Reload
	je	.LBB1_668
# BB#667:                               #   in Loop: Header=BB1_640 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_668:                              #   in Loop: Header=BB1_640 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_674
# BB#669:                               #   in Loop: Header=BB1_640 Depth=1
	callq	DisposeObject
	jmp	.LBB1_674
.LBB1_670:                              #   in Loop: Header=BB1_640 Depth=1
	movq	%rbp, %rdi
	movl	%r13d, %esi
	movq	40(%rsp), %rdx          # 8-byte Reload
	callq	MinSize
	movq	%rax, %rbp
.LBB1_671:                              #   in Loop: Header=BB1_640 Depth=1
	testl	%r14d, %r14d
	je	.LBB1_673
.LBB1_672:                              #   in Loop: Header=BB1_640 Depth=1
	movl	36(%rsp), %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	48(%rbp,%rdx,4), %ecx
	cmpl	%ecx, %eax
	cmovll	%ecx, %eax
	movl	%eax, 36(%rsp)
	movl	28(%rsp), %eax
	movl	56(%rbp,%rdx,4), %ecx
	cmpl	%ecx, %eax
	cmovll	%ecx, %eax
	movl	%eax, 28(%rsp)
	jmp	.LBB1_674
.LBB1_673:                              #   in Loop: Header=BB1_640 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	48(%rbp,%rcx,4), %eax
	movl	%eax, 36(%rsp)
	movl	56(%rbp,%rcx,4), %eax
	movl	%eax, 28(%rsp)
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB1_674:                              # %.backedge
                                        #   in Loop: Header=BB1_640 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.LBB1_640
# BB#675:                               # %._crit_edge1885
	testl	%r14d, %r14d
	movl	48(%rsp), %ebx          # 4-byte Reload
	movl	64(%rsp), %ebp          # 4-byte Reload
	jne	.LBB1_680
	jmp	.LBB1_679
.LBB1_676:
	andb	$127, 42(%r12)
	movq	8(%r12), %r15
	cmpq	%r12, %r15
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	je	.LBB1_783
# BB#677:                               # %.preheader1741.lr.ph
	movslq	%r13d, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movq	%r14, 40(%rsp)          # 8-byte Spill
	jmp	.LBB1_726
.LBB1_678:
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
.LBB1_679:                              # %._crit_edge1885.thread
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_680:
	testl	%ebp, %ebp
	je	.LBB1_682
# BB#681:
	movslq	%r13d, %rax
	movl	$0, 48(%r12,%rax,4)
	movl	28(%rsp), %ecx
	addl	36(%rsp), %ecx
	cmpl	%ecx, %ebx
	cmovgel	%ebx, %ecx
	cmpl	$8388608, %ecx          # imm = 0x800000
	movl	$8388607, %edx          # imm = 0x7FFFFF
	cmovll	%ecx, %edx
	movl	%edx, 56(%r12,%rax,4)
	jmp	.LBB1_483
.LBB1_682:
	movl	36(%rsp), %eax
	movslq	%r13d, %rcx
	movl	%eax, 48(%r12,%rcx,4)
	movl	28(%rsp), %eax
	movl	%eax, 56(%r12,%rcx,4)
	jmp	.LBB1_483
.LBB1_683:                              #   in Loop: Header=BB1_726 Depth=1
	movw	44(%rbx), %cx
	testb	%cl, %cl
	jns	.LBB1_747
# BB#684:                               #   in Loop: Header=BB1_726 Depth=1
	cmpb	$17, %al
	jne	.LBB1_747
# BB#685:                               #   in Loop: Header=BB1_726 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	32(%rax), %r12b
	movl	%r12d, %eax
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB1_747
# BB#686:                               #   in Loop: Header=BB1_726 Depth=1
	andl	$64768, %ecx            # imm = 0xFD00
	cmpl	$9216, %ecx             # imm = 0x2400
	jne	.LBB1_747
# BB#687:                               #   in Loop: Header=BB1_726 Depth=1
	movq	%rbx, %rcx
	movzbl	42(%rcx), %eax
	movzbl	41(%rcx), %ecx
	negl	%ecx
	cmpl	%ecx, %eax
	jne	.LBB1_747
# BB#688:                               #   in Loop: Header=BB1_726 Depth=1
	movl	40(%rbp), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	xorl	40(%rcx), %eax
	testl	$2147483647, %eax       # imm = 0x7FFFFFFF
	jne	.LBB1_747
# BB#689:                               #   in Loop: Header=BB1_726 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rax
	movq	8(%rax), %rax
	cmpq	%r15, 8(%rax)
	jne	.LBB1_747
# BB#690:                               #   in Loop: Header=BB1_726 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	32(%rax), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	leaq	64(%rax), %rdi
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	callq	strlen
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leaq	64(%rbp), %rdi
	movq	%rdi, %r14
	callq	strlen
	addq	120(%rsp), %rax         # 8-byte Folded Reload
	cmpq	$512, %rax              # imm = 0x200
	jb	.LBB1_692
# BB#691:                               #   in Loop: Header=BB1_726 Depth=1
	movq	%r14, (%rsp)
	movl	$12, %edi
	movl	$2, %esi
	movl	$.L.str.17, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	112(%rsp), %r12         # 8-byte Reload
	movq	%r12, %r8
	movq	104(%rsp), %r9          # 8-byte Reload
	callq	Error
	movb	(%r12), %r12b
.LBB1_692:                              #   in Loop: Header=BB1_726 Depth=1
	cmpb	$12, %r12b
	je	.LBB1_694
# BB#693:                               #   in Loop: Header=BB1_726 Depth=1
	movl	$11, %edi
	cmpb	$12, 32(%rbp)
	jne	.LBB1_695
.LBB1_694:                              # %.thread1673
                                        #   in Loop: Header=BB1_726 Depth=1
	movl	$12, %edi
.LBB1_695:                              #   in Loop: Header=BB1_726 Depth=1
	movq	104(%rsp), %rsi         # 8-byte Reload
	movq	%r14, %rdx
	movq	112(%rsp), %rcx         # 8-byte Reload
	callq	MakeWordTwo
	movq	%rax, %rbp
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	40(%rsi), %ecx
	movl	$4095, %eax             # imm = 0xFFF
	andl	%eax, %ecx
	movl	40(%rbp), %eax
	movl	$-4096, %edx            # imm = 0xF000
	andl	%edx, %eax
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movl	40(%rsi), %ecx
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	%edx, %ecx
	andl	$-4190209, %eax         # imm = 0xFFC00FFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movl	40(%rsi), %ecx
	movl	$4194304, %edx          # imm = 0x400000
	andl	%edx, %ecx
	andl	$-4194305, %eax         # imm = 0xFFBFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movl	40(%rsi), %ecx
	movl	$528482304, %edx        # imm = 0x1F800000
	andl	%edx, %ecx
	andl	$-528482305, %eax       # imm = 0xE07FFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movl	40(%rsi), %ecx
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	%edx, %ecx
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movl	40(%rsi), %ecx
	movl	$1610612736, %edx       # imm = 0x60000000
	andl	%edx, %ecx
	andl	$-1610612737, %eax      # imm = 0x9FFFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbp)
	movq	%rbp, %rdi
	callq	FontWordSize
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_697
# BB#696:                               #   in Loop: Header=BB1_726 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_698
.LBB1_697:                              #   in Loop: Header=BB1_726 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_698:                              #   in Loop: Header=BB1_726 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	24(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_701
# BB#699:                               #   in Loop: Header=BB1_726 Depth=1
	testq	%rcx, %rcx
	je	.LBB1_701
# BB#700:                               #   in Loop: Header=BB1_726 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_701:                              #   in Loop: Header=BB1_726 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_704
# BB#702:                               #   in Loop: Header=BB1_726 Depth=1
	testq	%rax, %rax
	je	.LBB1_704
# BB#703:                               #   in Loop: Header=BB1_726 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_704:                              #   in Loop: Header=BB1_726 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_706
# BB#705:                               #   in Loop: Header=BB1_726 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_707
.LBB1_706:                              #   in Loop: Header=BB1_726 Depth=1
	xorl	%ecx, %ecx
.LBB1_707:                              #   in Loop: Header=BB1_726 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_709
# BB#708:                               #   in Loop: Header=BB1_726 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_709:                              #   in Loop: Header=BB1_726 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_711
# BB#710:                               #   in Loop: Header=BB1_726 Depth=1
	callq	DisposeObject
.LBB1_711:                              #   in Loop: Header=BB1_726 Depth=1
	movq	24(%rbx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_713
# BB#712:                               #   in Loop: Header=BB1_726 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_714
.LBB1_713:                              #   in Loop: Header=BB1_726 Depth=1
	xorl	%ecx, %ecx
.LBB1_714:                              #   in Loop: Header=BB1_726 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_716
# BB#715:                               #   in Loop: Header=BB1_726 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_716:                              #   in Loop: Header=BB1_726 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_718
# BB#717:                               #   in Loop: Header=BB1_726 Depth=1
	callq	DisposeObject
.LBB1_718:                              #   in Loop: Header=BB1_726 Depth=1
	movq	%r15, xx_link(%rip)
	movq	%r15, zz_hold(%rip)
	movq	24(%r15), %rax
	cmpq	%r15, %rax
	je	.LBB1_720
# BB#719:                               #   in Loop: Header=BB1_726 Depth=1
	movq	%rax, zz_res(%rip)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r15), %rcx
	movq	%rax, 24(%rcx)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	jmp	.LBB1_721
.LBB1_720:                              #   in Loop: Header=BB1_726 Depth=1
	xorl	%eax, %eax
.LBB1_721:                              #   in Loop: Header=BB1_726 Depth=1
	movq	%rax, xx_tmp(%rip)
	movq	%r15, zz_hold(%rip)
	movq	8(%r15), %rax
	cmpq	%r15, %rax
	je	.LBB1_723
# BB#722:                               #   in Loop: Header=BB1_726 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%r15), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %r15
.LBB1_723:                              #   in Loop: Header=BB1_726 Depth=1
	movq	%r15, zz_hold(%rip)
	movzbl	32(%r15), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r15), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r15)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_725
# BB#724:                               #   in Loop: Header=BB1_726 Depth=1
	callq	DisposeObject
.LBB1_725:                              #   in Loop: Header=BB1_726 Depth=1
	movq	24(%rbp), %r15
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	%rbx, %r12
	jmp	.LBB1_779
	.p2align	4, 0x90
.LBB1_726:                              # %.preheader1741
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_727 Depth 2
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB1_727:                              #   Parent Loop BB1_726 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB1_727
# BB#728:                               #   in Loop: Header=BB1_726 Depth=1
	movl	%ecx, %eax
	addb	$-119, %al
	cmpb	$19, %al
	ja	.LBB1_735
# BB#729:                               #   in Loop: Header=BB1_726 Depth=1
	cmpl	$1, %r13d
	jne	.LBB1_779
# BB#730:                               #   in Loop: Header=BB1_726 Depth=1
	movq	(%r15), %r15
	movq	8(%r15), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_732
# BB#731:                               #   in Loop: Header=BB1_726 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_732:                              #   in Loop: Header=BB1_726 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%r14), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_779
# BB#733:                               #   in Loop: Header=BB1_726 Depth=1
	testq	%rcx, %rcx
	je	.LBB1_779
# BB#734:                               #   in Loop: Header=BB1_726 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	jmp	.LBB1_779
	.p2align	4, 0x90
.LBB1_735:                              #   in Loop: Header=BB1_726 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movb	32(%rax), %al
	cmpb	%al, %cl
	jne	.LBB1_743
# BB#736:                               #   in Loop: Header=BB1_726 Depth=1
	movl	%r13d, %r14d
	movq	(%r15), %r15
	movq	8(%rbp), %rbx
	cmpq	%rbp, %rbx
	je	.LBB1_741
# BB#737:                               #   in Loop: Header=BB1_726 Depth=1
	movq	8(%r15), %r13
	cmpb	$0, 32(%rbx)
	je	.LBB1_739
# BB#738:                               #   in Loop: Header=BB1_726 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_739:                              #   in Loop: Header=BB1_726 Depth=1
	movq	%rbx, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%rbx, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB1_741
# BB#740:                               #   in Loop: Header=BB1_726 Depth=1
	movq	(%r13), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_741:                              #   in Loop: Header=BB1_726 Depth=1
	movq	24(%rbp), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_750
# BB#742:                               #   in Loop: Header=BB1_726 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_751
.LBB1_743:                              #   in Loop: Header=BB1_726 Depth=1
	movl	%ecx, %edx
	addb	$-11, %dl
	cmpb	$2, %dl
	jae	.LBB1_748
# BB#744:                               #   in Loop: Header=BB1_726 Depth=1
	testl	%r13d, %r13d
	jne	.LBB1_756
# BB#745:                               #   in Loop: Header=BB1_726 Depth=1
	movq	%r12, %rbx
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB1_747
# BB#746:                               #   in Loop: Header=BB1_726 Depth=1
	cmpw	$0, 46(%rbx)
	je	.LBB1_683
.LBB1_747:                              #   in Loop: Header=BB1_726 Depth=1
	movq	%rbp, %rdi
	callq	FontWordSize
	movq	%rbx, %r12
	jmp	.LBB1_756
.LBB1_748:                              #   in Loop: Header=BB1_726 Depth=1
	cmpb	$1, %cl
	jne	.LBB1_755
# BB#749:                               #   in Loop: Header=BB1_726 Depth=1
	movq	%rbp, %r12
	jmp	.LBB1_779
.LBB1_750:                              #   in Loop: Header=BB1_726 Depth=1
	xorl	%ecx, %ecx
.LBB1_751:                              #   in Loop: Header=BB1_726 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_753
# BB#752:                               #   in Loop: Header=BB1_726 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_753:                              #   in Loop: Header=BB1_726 Depth=1
	movl	%r14d, %r13d
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	movq	40(%rsp), %r14          # 8-byte Reload
	jne	.LBB1_779
# BB#754:                               #   in Loop: Header=BB1_726 Depth=1
	callq	DisposeObject
	jmp	.LBB1_779
.LBB1_755:                              #   in Loop: Header=BB1_726 Depth=1
	movq	%rbp, %rdi
	movl	%r13d, %esi
	movq	%r14, %rdx
	callq	MinSize
	movq	%rax, %rbp
.LBB1_756:                              #   in Loop: Header=BB1_726 Depth=1
	movb	32(%rbp), %al
	addb	$-2, %al
	cmpb	$6, %al
	ja	.LBB1_762
# BB#757:                               #   in Loop: Header=BB1_726 Depth=1
	testq	%r12, %r12
	je	.LBB1_768
# BB#758:                               #   in Loop: Header=BB1_726 Depth=1
	addq	$32, %rbp
	movzwl	44(%r12), %eax
	testb	$1, %ah
	je	.LBB1_760
# BB#759:                               #   in Loop: Header=BB1_726 Depth=1
	movl	$12, %edi
	movl	$3, %esi
	movl	$.L.str.18, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
	movzwl	44(%r12), %eax
	andl	$65279, %eax            # imm = 0xFEFF
	movw	%ax, 44(%r12)
.LBB1_760:                              #   in Loop: Header=BB1_726 Depth=1
	andl	$7168, %eax             # imm = 0x1C00
	cmpl	$5120, %eax             # imm = 0x1400
	jne	.LBB1_779
# BB#761:                               #   in Loop: Header=BB1_726 Depth=1
	movl	$12, %edi
	movl	$4, %esi
	movl	$.L.str.19, %edx
	movl	$2, %ecx
	movl	$119, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
	movzwl	44(%r12), %eax
	andl	$58367, %eax            # imm = 0xE3FF
	orl	$1024, %eax             # imm = 0x400
	movw	%ax, 44(%r12)
	movw	$0, 46(%r12)
	jmp	.LBB1_779
.LBB1_762:                              #   in Loop: Header=BB1_726 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB1_769
# BB#763:                               #   in Loop: Header=BB1_726 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	56(%rcx,%rax,4), %edi
	movl	48(%rbp,%rax,4), %esi
	movl	56(%rbp,%rax,4), %edx
	movq	%r12, %rbx
	leaq	44(%rbx), %r12
	movq	%r12, %rcx
	callq	MinGap
	movl	%eax, %r14d
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	testq	%rbx, %rbx
	je	.LBB1_765
# BB#764:                               #   in Loop: Header=BB1_726 Depth=1
	movzwl	(%r12), %eax
	cmpl	$8191, %eax             # imm = 0x1FFF
	ja	.LBB1_766
.LBB1_765:                              # %._crit_edge2153
                                        #   in Loop: Header=BB1_726 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
	movw	(%r12), %ax
.LBB1_766:                              #   in Loop: Header=BB1_726 Depth=1
	movl	%eax, %ecx
	andl	$64512, %ecx            # imm = 0xFC00
	cmpl	$50176, %ecx            # imm = 0xC400
	jne	.LBB1_770
# BB#767:                               #   in Loop: Header=BB1_726 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	movswl	46(%r12), %ecx
	movq	64(%rsp), %rdx          # 8-byte Reload
	addl	48(%rbp,%rdx,4), %ecx
	addl	28(%rsp), %r14d
	cmpl	%r14d, %ecx
	cmovgel	%ecx, %r14d
	jmp	.LBB1_771
.LBB1_768:                              #   in Loop: Header=BB1_726 Depth=1
	xorl	%r12d, %r12d
	jmp	.LBB1_779
.LBB1_769:                              #   in Loop: Header=BB1_726 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	48(%rbp,%rax,4), %eax
	movl	%eax, 36(%rsp)
	movl	$0, 28(%rsp)
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	jmp	.LBB1_779
.LBB1_770:                              #   in Loop: Header=BB1_726 Depth=1
	addl	28(%rsp), %r14d
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB1_771:                              #   in Loop: Header=BB1_726 Depth=1
	movl	%r14d, 28(%rsp)
	movl	%eax, %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	cmpl	$2048, %ecx             # imm = 0x800
	jne	.LBB1_773
# BB#772:                               #   in Loop: Header=BB1_726 Depth=1
	movswl	46(%r12), %ecx
	cmpl	$4096, %ecx             # imm = 0x1000
	movl	48(%rsp), %ecx          # 4-byte Reload
	movl	$1, %edx
	cmovgl	%edx, %ecx
	movl	%ecx, 48(%rsp)          # 4-byte Spill
.LBB1_773:                              #   in Loop: Header=BB1_726 Depth=1
	movl	%eax, %ecx
	andl	$7424, %ecx             # imm = 0x1D00
	cmpl	$3328, %ecx             # imm = 0xD00
	jne	.LBB1_776
# BB#774:                               #   in Loop: Header=BB1_726 Depth=1
	cmpw	$0, 46(%r12)
	jle	.LBB1_776
# BB#775:                               #   in Loop: Header=BB1_726 Depth=1
	leaq	32(%r12), %r8
	movl	$12, %edi
	movl	$9, %esi
	movl	$.L.str.21, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	callq	Error
	movw	44(%r12), %ax
.LBB1_776:                              #   in Loop: Header=BB1_726 Depth=1
	testb	$1, %ah
	je	.LBB1_778
# BB#777:                               #   in Loop: Header=BB1_726 Depth=1
	addl	%r14d, 36(%rsp)
	movl	$0, 28(%rsp)
.LBB1_778:                              #   in Loop: Header=BB1_726 Depth=1
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	40(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_779:                              # %.critedge1631.backedge
                                        #   in Loop: Header=BB1_726 Depth=1
	movq	8(%r15), %r15
	cmpq	56(%rsp), %r15          # 8-byte Folded Reload
	jne	.LBB1_726
# BB#780:                               # %.critedge1631._crit_edge
	movq	16(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB1_782
# BB#781:
	movl	28(%rsp), %edx
	movq	64(%rsp), %rsi          # 8-byte Reload
	addl	56(%rax,%rsi,4), %edx
	movl	%edx, 28(%rsp)
	movl	36(%rsp), %eax
	movq	56(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_784
.LBB1_782:
	movq	56(%rsp), %r12          # 8-byte Reload
.LBB1_783:                              # %.critedge1631._crit_edge.thread
	movl	$0, 28(%rsp)
	movl	$0, 36(%rsp)
	movslq	%r13d, %rsi
	xorl	%eax, %eax
	xorl	%edx, %edx
.LBB1_784:
	cmpl	$8388608, %eax          # imm = 0x800000
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	cmovgel	%ecx, %eax
	movl	%eax, 48(%r12,%rsi,4)
	cmpl	$8388608, %edx          # imm = 0x800000
	cmovll	%edx, %ecx
	movl	%ecx, 56(%r12,%rsi,4)
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	je	.LBB1_483
# BB#785:
	cmpb	$17, 32(%r12)
	jne	.LBB1_483
# BB#786:
	movl	$8388607, 56(%r12)      # imm = 0x7FFFFF
	jmp	.LBB1_483
.Lfunc_end1:
	.size	MinSize, .Lfunc_end1-MinSize
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_75
	.quad	.LBB1_81
	.quad	.LBB1_82
	.quad	.LBB1_482
	.quad	.LBB1_8
	.quad	.LBB1_8
	.quad	.LBB1_85
	.quad	.LBB1_88
	.quad	.LBB1_81
	.quad	.LBB1_11
	.quad	.LBB1_11
	.quad	.LBB1_13
	.quad	.LBB1_13
	.quad	.LBB1_90
	.quad	.LBB1_104
	.quad	.LBB1_119
	.quad	.LBB1_637
	.quad	.LBB1_637
	.quad	.LBB1_17
	.quad	.LBB1_19
	.quad	.LBB1_17
	.quad	.LBB1_19
	.quad	.LBB1_2
	.quad	.LBB1_2
	.quad	.LBB1_148
	.quad	.LBB1_156
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_26
	.quad	.LBB1_26
	.quad	.LBB1_30
	.quad	.LBB1_30
	.quad	.LBB1_161
	.quad	.LBB1_165
	.quad	.LBB1_2
	.quad	.LBB1_2
	.quad	.LBB1_2
	.quad	.LBB1_2
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_39
	.quad	.LBB1_6
	.quad	.LBB1_39
	.quad	.LBB1_6
	.quad	.LBB1_6
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_175
	.quad	.LBB1_186
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_57
	.quad	.LBB1_57
	.quad	.LBB1_4
	.quad	.LBB1_4
	.quad	.LBB1_4
	.quad	.LBB1_4
.LJTI1_1:
	.quad	.LBB1_277
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_281
	.quad	.LBB1_283
	.quad	.LBB1_283
	.quad	.LBB1_283
	.quad	.LBB1_280
	.quad	.LBB1_283
.LJTI1_2:
	.quad	.LBB1_73
	.quad	.LBB1_464
	.quad	.LBB1_465
	.quad	.LBB1_466
	.quad	.LBB1_467
.LJTI1_3:
	.quad	.LBB1_63
	.quad	.LBB1_464
	.quad	.LBB1_465
	.quad	.LBB1_466
	.quad	.LBB1_467

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SpannerAvail!"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"SpannerAvailableSpace: thr_state!"
	.size	.L.str.2, 34

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"search for gap preceding %s failed, using zero"
	.size	.L.str.3, 47

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%s deleted (out of place)"
	.size	.L.str.4, 26

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"MinSize: CLOSURE has target!"
	.size	.L.str.6, 29

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"MinSize: definite non-recursive closure"
	.size	.L.str.7, 40

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.zero	1
	.size	.L.str.8, 1

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"MinSize/SPAN: type(t) != HSPANNER!"
	.size	.L.str.9, 35

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%s replaced by empty object (out of place)"
	.size	.L.str.10, 43

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"MinSize: SPANNER!"
	.size	.L.str.11, 18

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"TransferLinks: start_link!"
	.size	.L.str.12, 27

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"MinSize: BreakObject failed to fit!"
	.size	.L.str.13, 36

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"forced to enlarge %s from %s to %s"
	.size	.L.str.14, 35

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"@High"
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"NextDefiniteWithGap: g == nilobj!"
	.size	.L.str.16, 34

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"word %s%s is too long"
	.size	.L.str.17, 22

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"^ deleted (it may not precede this object)"
	.size	.L.str.18, 43

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"gap replaced by 0i (%c unit not allowed here)"
	.size	.L.str.19, 46

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"MinSize: NO_MODE!"
	.size	.L.str.20, 18

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"mark alignment incompatible with centring or right justification"
	.size	.L.str.21, 65

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"MinSize/VCAT/perp: !found!"
	.size	.L.str.22, 27

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"MinSize/VCAT/perp: !found (2)!"
	.size	.L.str.23, 31

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"MinSize/COL_THR: dim!"
	.size	.L.str.24, 22

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"MinSize/COL_THR: Down(x)!"
	.size	.L.str.25, 26

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"MinSize/COL_THR: GAP_OBJ!"
	.size	.L.str.26, 26

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"MinSize/ROW_THR: dim!"
	.size	.L.str.27, 22

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"MinSize/ROW_THR: Down(x)!"
	.size	.L.str.28, 26

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"MinSize/ROW_THR: GAP_OBJ!"
	.size	.L.str.29, 26

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"%[^\n\r]%*c"
	.size	.L.str.30, 10

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"%!"
	.size	.L.str.31, 3

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"%%BoundingBox:"
	.size	.L.str.32, 15

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"(atend)"
	.size	.L.str.33, 8

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"%%%%BoundingBox: %f %f %f %f"
	.size	.L.str.34, 29

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"%s deleted (cannot open file %s)"
	.size	.L.str.35, 33

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"@IncludeGraphic"
	.size	.L.str.36, 16

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"@SysIncludeGraphic"
	.size	.L.str.37, 19

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"%s given zero size (no BoundingBox line in file %s)"
	.size	.L.str.38, 52

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"lout.eps"
	.size	.L.str.39, 9

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"%s deleted (bad first line in file %s)"
	.size	.L.str.40, 39

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"%s given zero size (bad BoundingBox line in file %s)"
	.size	.L.str.41, 53

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"assert failed in %s %s"
	.size	.L.str.42, 23

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"MinSize"
	.size	.L.str.43, 8

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"MinSize: back(x, dim) < 0!"
	.size	.L.str.44, 27

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"MinSize: fwd(x, dim) < 0!"
	.size	.L.str.45, 26

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"FindSpannerGap: type(*res)!"
	.size	.L.str.46, 28

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"FindSpannerGap (HEAD): type(*res)!"
	.size	.L.str.47, 35

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"BuildSpanner: type(x) != SPAN!"
	.size	.L.str.48, 31

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"BuildSpanner: spanobj!"
	.size	.L.str.49, 23

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"%s deleted (not in column)"
	.size	.L.str.50, 27

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"%s symbol out of place"
	.size	.L.str.51, 23

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"BuildSpanner: type(prnt)!"
	.size	.L.str.52, 26

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"%s deleted (not in row)"
	.size	.L.str.53, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
